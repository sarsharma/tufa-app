README.TXT

FAQ

Naming convention:
Path or files mentioned in <<xxxx>> may be different on your machine

1. How to build and deploy on Wildfly locally?

	- Open a new terminal in your eclipse or in visual studio code
	- If you path is not correctly set, then run cmd "setpath", It is a batch file located in your root <tufamain> folder
	- If you prefer to build the *.war using mvn command then type in your <tufamain> root folder
				mvn clean install -DskipTests
	- Once the war file is created then deploy using the following command
			switch to tufa_web directory. Please make sure Wildfly server is running
			Run
				c:\\<tufamain>\tufa_web\mvn wildfly:deploy-only -DskipTests
			options: Use mvn -q for quiet mode. mvn -X for debug mode when using above deploy command
	- Application is successfully build and deployed
	
	
2. How to build UI only and deploy locally?

	- Sometimes you are only making changes to front-end code. You don't want to build the java code, create war and deploy, as the whole process
	takes time. In this situation, one can build just the front-end code and copy the files directly to <webapp> folder in the deployment directory.
	- Edit "uib.bat" file in your <tufamain> root folder.
	- Change the directory of the deployment folder. Wildfly deployment is usually in a folder like this
		C:\wildflylocal\wildfly-10.1.0.Final\standalone\tmp\vfs\temp\<<temp8378a7c3f0e7adec\content-8b29d70aeade952a>>
	- Find the location of the temp folder in your machine and copy the entire path to uib.bat in the "copy" command line
	- Your last line in "uib.bat" would look like this
		xcopy tufa_sing\dist\*.* C:\wildflylocal\wildfly-10.1.0.Final\standalone\tmp\vfs\temp\<<tempe68be2d36f0318d9\content-a3774c68a270e1af>>\*.* /s /e /q /y
	- Reload the local app instance http://loocalhost:8080/tufamvp4


3. How to build backend only and deploy locally?
	- Sometimes if you are only working in the Java Code and you want to reuse the front-end UI files then you just build 
		c:\\<tufamain>mvn clean install -DskipTests -Dmaven.exec.skip=true
			-Dmaven.exec.skip=true skips the "npm run build" and copying files from <dist> folder to <webapp> folder
	- Then run the wildfly deploy command
			c:\\<tufamain>\tufa_web\mvn wildfly:deploy -DskipTests
			

4. How to build and deploy for tufadev server?

	Steps:
	- When you want to build the war file to be deployed in tufa.dev.fda.gov domain please from the <tufamain> root folder run the following command
		c:\\<tufamain>\mvn -Ptufadev -DskipTests
	- This command will create a tufamvp4-dev-4.0.0.war file.
	- Share the *.war file with Balaji or Jason when you want to deploy on the VM machines. Use Deloitte ShareFile to share with them.
	
	Note: -Ptufadev is a profile build that performs a few additional steps than the ones you build locally.
		- Uses the right persistence properties file. You will notice additional files in 
			C:\<<tufamain>>\tufa_persistence\src\main\resources\persistence folder now. One for each build type (tufadev, tufatst, tufapreprod & tufaprod)
		- Runs a production build of webpack and uses the right url for REST calls.
			For example: const CONTEXT = process.env.CONTEXT || 'http://localhost:8080' is automatically converted to http://tufa.dev.fda.gov. CONTEXT is set appropriately
				in package.json. Please notice "build:tufadev" script in package.json where the context is set correctly
		- Java code is compiled with optimization turned on.
		- Deploys *.map file so you can debug typescript for deployment in tufa.dev.fda.gov
		
	Summary: Why you cannot reuse your local war file in tufadev server? Because
		- CONTEXT & URL is different
		- Angular and Java Compile code is optimized
		- Persistence Properties could be different?
		- Can debug Typescript Code
		- Hibernate, SQL and Application Log Levels are set differently
		

5. How to build and deploy for tufatst server?

	Steps:
	- When you want to build the war file to be deployed in tufa.dev.fda.gov domain please from the <tufamain> root folder run the following command
		c:\\<tufamain>\mvn -Ptufatst -DskipTests
	- This command will create a tufamvp4-test-4.0.0.war file.
	- Share the *.war file with Balaji or Jason when you want to deploy on the VM machines. Use Deloitte ShareFile to share with them.
	
	Note: -Ptufadev is a profile build that performs a few additional steps than the ones you build locally.
		- Uses the right persistence properties file. You will notice additional files in 
			C:\<<tufamain>>\tufa_persistence\src\main\resources\persistence folder now. One for each build type (tufadev, tufatst, tufapreprod & tufaprod)
		- Runs a production build of webpack and uses the right url for REST calls.
			For example: const CONTEXT = process.env.CONTEXT || 'http://localhost:8080' is automatically converted to http://tufa.dev.fda.gov. CONTEXT is set appropriately
				in package.json. Please notice "build:tufadev" script in package.json where the context is set correctly
		- Java code is compiled with optimization turned on.
		- Deploys *.map file so you can debug typescript for deployment in tufa.dev.fda.gov
		
	Summary: Why you cannot reuse your local war file in tufadev server? Because
		- CONTEXT & URL is different
		- Angular and Java Compile code is optimized
		- Persistence Properties could be different
		- Hibernate, SQL and Application Log Levels are set differently		
		

6. How to enable hibernate and SQL log messages?




@echo off
IF /I [%1] == [tufadev] GOTO TUFADEV
IF /I [%1] == [tufatst] GOTO TUFATST
IF /I [%1] == [tufapreprod] GOTO TUFAPREPROD
IF /I [%1] == [tufaprod] GOTO TUFAPROD

npm run build
GOTO DONE:

:TUFADEV
npm run build:tufadev
GOTO DONE

:TUFATST
npm run build:tufatst
GOTO DONE

:TUFAPREPROD
npm run build:tufapreprod
GOTO DONE

:TUFAPROD
npm run build:tufaprod
GOTO DONE

:DONE
/**
 *
 */
package gov.hhs.fda.ctp.biz.addressmgmt;

import java.util.List;
import java.util.Set;

import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.persistence.model.CountryEntity;

/**
 * The Interface AddressMgmtBizComp.
 *
 * @author akari
 */
public interface AddressMgmtBizComp {

	/**
	 * Save or update.
	 *
	 * @param address the address
	 * @return address
	 */
	 public Address saveOrUpdate(Address address);
	 
	

	 /**
 	 * Gets the country cd.
 	 *
 	 * @return the country cd
 	 */
 	public List<String> getCountryCd();

	 /**
 	 * Gets the country nm.
 	 *
 	 * @return the country nm
 	 */
 	public List<String> getCountryNm();

 	 /**
 	 * Gets the state cd.
 	 *
 	 * @return the state cd
 	 */
 	public List<String> getStateCd();

	 /**
 	 * Gets the State nm.
 	 *
 	 * @return the State nm
 	 */
 	public List<String> getStateNm();

	 /**
 	 * Gets the country dial cd.
 	 *
 	 * @return the country dial cd
 	 */
 	public List<String> getCountryDialCd();

	 /**
 	 * Gets the countries.
 	 *
 	 * @return the countries
 	 */
 	public Set<CountryEntity> getCountries();

 	/**
	 * Gets list of addresses from report address table.
	 * @param companyId
	 * @param periodId
	 * @return
	 */
	public List<Address> getReportAddresses(String companyId, int permitId, int periodId);

	public List<Address> saveOrUpdateMultiple(List<Address> model);



	public Address CreateAddress(Address address);
	public Address saveOrUpdatePrimaryAddress(Address address, String isUpdatePrimToAltn, String setAltAddress);
	public String delete(long companyId,  String addressType);

}

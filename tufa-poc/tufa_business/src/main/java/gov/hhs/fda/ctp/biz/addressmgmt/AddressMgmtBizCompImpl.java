/**
 *
 */
package gov.hhs.fda.ctp.biz.addressmgmt;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.persistence.AddressMgmtEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.model.AddressEntity;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.CountryEntity;
import gov.hhs.fda.ctp.persistence.model.ReportAddressEntity;

/**
 * The Class AddressMgmtBizCompImpl.
 *
 * @author akari
 */
@Service
@Transactional
public class AddressMgmtBizCompImpl implements AddressMgmtBizComp {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(AddressMgmtBizCompImpl.class);

	/** The address mgmt entity manager. */
	/* Persistence Managers */
	private AddressMgmtEntityManager addressMgmtEntityManager;

    @Autowired
	private MapperWrapper mapperWrapper;
    @Autowired
	private CompanyMgmtBizComp companyMgmtBizComp;
	
    

	/**
	 * Sets the address mgmt entity manager.
	 *
	 * @param addressMgmtEntityManager            the addressMgmtEntityManager to set
	 */
	@Autowired
	public void setAddressMgmtEntityManager(AddressMgmtEntityManager addressMgmtEntityManager) {
		this.addressMgmtEntityManager = addressMgmtEntityManager;
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.addressmgmt.AddressMgmtBizComp#saveOrUpdate(gov.hhs.fda.ctp.common.beans.Address)
	 */
	/*
	 * update address
	 */
	@Override
	public Address saveOrUpdate(Address address) {
		logger.debug(" - Entering saveOrUpdate - ");
		AddressEntity addressEntity = null;
		Mapper mapper = mapperWrapper.getMapper();
		if(address != null){
			addressEntity = populateAddressEntity(address);
			// save
			addressEntity = addressMgmtEntityManager.saveOrUpdate(addressEntity);
			// map the updated entity back onto the address
			mapper.map(addressEntity, address);
		}
		logger.debug(" - Exiting saveOrUpdate - ");
		return address;
	}
	
	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.addressmgmt.AddressMgmtBizComp#saveOrUpdate(gov.hhs.fda.ctp.common.beans.Address)
	 */
	/*
	 * update address
	 */
	@Override
	public String delete(long companyId,  String addressType) {
		logger.debug(" - Deleting - ");
		addressMgmtEntityManager.delete(companyId, addressType);
		logger.debug(" - Deleting - ");
		return "Done";
	}
	
	@Override
	public Address saveOrUpdatePrimaryAddress(Address address, String isUpdatePrimToAltn, String setAltAddress) {
		logger.debug(" - Entering saveOrUpdate - ");
		AddressEntity addressEntity = null;
		Mapper mapper = mapperWrapper.getMapper();
		if(address != null){
			addressEntity = populateAddressEntity(address);
			// save
			if("PRIM".equalsIgnoreCase(setAltAddress)){
				this.addressMgmtEntityManager.softDeleteAddress(address.getCompanyId(), "ALTN");
				this.addressMgmtEntityManager.updatePrimToAltnAddress(address.getCompanyId());
			}else if("ALTN".equalsIgnoreCase(setAltAddress)){
				this.addressMgmtEntityManager.softDeleteAddress(address.getCompanyId(), "PRIM");
			}else if("Y".equalsIgnoreCase(isUpdatePrimToAltn) && "NA".equalsIgnoreCase(setAltAddress)){
				this.addressMgmtEntityManager.updatePrimToAltnAddress(address.getCompanyId());
			}
			addressEntity = addressMgmtEntityManager.saveOrUpdate(addressEntity);
			// map the updated entity back onto the address
			mapper.map(addressEntity, address);
		}
		logger.debug(" - Exiting saveOrUpdate - ");
		return address;
	}
	
	public AddressEntity populateAddressEntity(Address address) {

		// create stub company
		CompanyEntity companyEntity = new CompanyEntity();
		Mapper mapper = mapperWrapper.getMapper();
		AddressEntity addressEntity = new AddressEntity();
		
        companyEntity.setCompanyId(address.getCompanyId());
        // map the address onto an entity
		mapper.map(address, addressEntity);
		// set the stub company on the address and update
		addressEntity.setCompany(companyEntity);
		// convert the country to country code
		if(addressEntity.getCountryCd()!= null )
		{
			String countryCd = addressMgmtEntityManager.getCountryCd(addressEntity.getCountryCd());
		    addressEntity.setCountryCd(countryCd);
		}
		else
		{ 
			//This is required on the front end 
		    addressEntity.setCountryCd("US");				
		}
		// convert state to state code
		if (addressEntity.getState() != null){  
			if(addressEntity.getState().length()>2) {
				String stateCd = addressMgmtEntityManager.getStateCd(addressEntity.getState());
				addressEntity.setState(stateCd);
			}
		}
		return addressEntity;
	}
	
	public List<Address> saveOrUpdateMultiple(List<Address> addressmodel) {
			
		List<Address> addresses = addressmodel;
		for(Address address : addresses) {			
			saveOrUpdate(address);	
			this.addressMgmtEntityManager.saveStatus(address.getEin());
		}
		return addresses;
	}
	
	@Override
	public Address CreateAddress(Address address) {
		logger.debug(" - Entering saveOrUpdate - ");
		if(address != null){
			// create stub company
			
	        // map the address onto an entity
			Mapper mapper = mapperWrapper.getMapper();
			AddressEntity addressEntity = new AddressEntity();
			mapper.map(address, addressEntity);
		
			// convert the country to country code
			if(addressEntity.getCountryCd()!= null )
			{
				String countryCd = addressMgmtEntityManager.getCountryCd(addressEntity.getCountryCd());
			    addressEntity.setCountryCd(countryCd);
			}
			else
			{ 
				//This is required on the front end 
			    addressEntity.setCountryCd("US");				
			}
			
			
			// convert state to state code
			if (addressEntity.getState() != null)
			{   if(addressEntity.getState().length()>2) {
				String stateCd = addressMgmtEntityManager.getStateCd(addressEntity.getState());
				addressEntity.setState(stateCd);
			}
			}
			// save
			addressEntity = addressMgmtEntityManager.saveOrUpdate(addressEntity);
			// map the updated entity back onto the address
			mapper.map(addressEntity, address);
		}
		logger.debug(" - Exiting saveOrUpdate - ");
		return address;
	}
	
	
	
	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.addressmgmt.AddressMgmtBizComp#getCountryCd()
	 */
	@Override
	public List<String> getCountryCd() {
		return this.addressMgmtEntityManager.getCountryCd();
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.addressmgmt.AddressMgmtBizComp#getCountryNm()
	 */
	@Override
	public List<String> getCountryNm() {
		return this.addressMgmtEntityManager.getCountryNm();
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.addressmgmt.AddressMgmtBizComp#getCountryDialCd()
	 */
	@Override
	public List<String> getCountryDialCd() {
		return this.addressMgmtEntityManager.getCountryDialCd();
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.addressmgmt.AddressMgmtBizComp#getCountries()
	 */
	@Override
	public Set<CountryEntity> getCountries() {
	    return this.addressMgmtEntityManager.getCountries();
	}

	@Override
	public List<String> getStateCd() {
		return this.addressMgmtEntityManager.getStateCd();
	}

	@Override
	public List<String> getStateNm() {
		return this.addressMgmtEntityManager.getStateNm();
	}

	@Override
	public List<Address> getReportAddresses(String companyId, int permitId, int periodId) {
		Mapper mapper = mapperWrapper.getMapper();
		List<Address> addresses = new ArrayList<>();
		long cmpId;
		if(companyId != null && companyId.length() == 9 ) {
			cmpId = companyMgmtBizComp.getCompanybyEIN(companyId).getCompanyId();
		} else {
			cmpId = Long.valueOf(companyId);
		}
		List<ReportAddressEntity> entities = addressMgmtEntityManager.getReportAddresses((int)cmpId, permitId, periodId);
		for(ReportAddressEntity entity : entities) {
			Address address = new Address();
			mapper.map(entity, address);
			address.setCountryNm(addressMgmtEntityManager.getCountryNm(address.getCountryCd()));
			addresses.add(address);
		}
		return addresses;
	}

}

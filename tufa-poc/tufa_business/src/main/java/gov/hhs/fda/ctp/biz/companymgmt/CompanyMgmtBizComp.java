package gov.hhs.fda.ctp.biz.companymgmt;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import gov.hhs.fda.ctp.common.beans.AffectedAssessmentbean;
import gov.hhs.fda.ctp.common.beans.Attachment;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.ExcludePermitBean;
import gov.hhs.fda.ctp.common.beans.ExcludePermitRequest;
import gov.hhs.fda.ctp.common.beans.NewCompaniesExport;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitExcludeComment;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.PermitCommentAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;

/**
 * @author tgunter
 *
 */
public interface CompanyMgmtBizComp {
    /**
     * Create a new company with attached permits.
     * @param company
     * @return company
     */
    public Company createCompany(Company company);
    
    /**
     * Update an existing company.  Will not cascade permits.
     * @param company
     * @return company
     */
    public Company updateCompany(Company company);
    
    /**
     * Get a company by id.
     * @param id
     * @return company
     */
    public Company getCompany(long id);
    
    public Company getCompanybyEIN(String ein);
    
    /**
     * Create a Permit. Permit contains the company Id to which 
     * it will be associated to once created. 
     * 
     */
    public Company createPermit(Permit permit);
    
    /**
     * Update the Permit with the the given permitId.
     */
	public Company updatePermit(Permit permit);
	
	
	/**
	 * Get permit by id with object graph attached.
	 * @param id
	 * @return
	 */
	public Permit getPermit(long id);
		
	/**
	 * Get all the Permits associated with a Company and with a an active/closed status.
	 * 
	 */
	
    public List<Permit> getPermits(String status, long companyId);


	/**
	 * Return a permit with a list of permit history objects attached.
	 *  
	 * @param permitId
	 * @return
	 */
	public Permit getPermitHistory(Permit permit);
	
//	/**
//	 * Paginated permit history.
//	 * @param permit
//	 * @param page
//	 * @param rows
//	 * @return
//	 */
//	public Permit getPaginatedPermitHistory(Permit permit, int page, int rows);
	
	/**
	 * Return a permit with the given companyID and permit number.
	 *  
	 * @param cmpnyId
	 * @param permitNum
	 * @return
	 */	
	public PermitEntity getPermitById(long cmpnyId, String permitNum);

	/**
	 * Return a list of companies given a company search criteria.
	 *  
	 * @param criteria
	 * @return
	 */	
	public CompanySearchCriteria getCompanies(CompanySearchCriteria criteria);
	
	/**
	 * Get all permit histories for all permits for a company.
	 * @param company
	 * @return company
	 */
	public Company getCompanyPermitHistory(Company company);	

	/**
	 * Save / update a comment for company.
	 * @param companyId
	 * @param comment
	 * @return
	 */
	public String updateCompanyComment(long companyId, String comment);

	public Permit getPermitHistory3852(Permit permit);

	/**
	 * Returns an active importer company that matches the EIN
	 * */
	public Company getActvImptCompByEIN(String ein);

	/**
	 * Retrives companies who's welcome letter flag is not set
	 * */
	public List<Company> getCompanyTrackingInformation(CompanySearchCriteria criteria);

	public List<Company> updateCompanyTrackingInformation(List<Company> companies);
	/**
	 * Get all the new companies for export
	 * @return
	 */
	
	public NewCompaniesExport getNewCompaniesExport();
	
    
    /**
     * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * @param companyId
     * @param permitId
     * @param exclusionType
     * @param request
     */
    public boolean excludeAllPermits(long companyId,long permitId,String exclusionType,List<ExcludePermitRequest> request, PermitExcludeComment permitExcludeComment);
    
    /**
     * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * @param permitId
     * @param companyid TODO
     */
    public List<ExcludePermitBean> getExcludePermitDetails(long permitId, long companyid);

	public List<PermitExcludeComment> getExcludePermitComments(long permitid, long companyid);
    
    public List<PermitCommentAttachmentsEntity> findCommentDocbyId(long commentId)throws SQLException;
    
	public Attachment getPermitCommentAttachmentDownload(long docId) throws SQLException;
	
    public byte[] getPermitCommentAttachmentAsByteArray(long docId) throws SQLException;
    
	public void addPermitCommentAttach(PermitCommentAttachmentsEntity permitcommentattachment, MultipartFile file)throws IOException, SQLException;
    /**
     * CTPTUFA-5242 - Comment for Permit Exclude
     * Pass the comment object throw the biz layer to persistence
     * @param comment
     * */
	public void updateExcludeCommentDetails(PermitExcludeComment comment);
	
	void deleteDocById(long docId);

	
	/**
	 * CTPTUFA-4947 - Assessments: Show table under the Assessments page that lists all Permits marked as Excluded Pt2
	 * @param companyId
	 * @param permitId
	 * @return
	 */
	public List<AffectedAssessmentbean> getAffectedAssessmentData();

	/**
	 * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * this will send warning message for affected assessment data for permit being excluded
	 * @param companyId
	 * @param fiscalYear
	 * @param quarter
	 * @param exclusionType
	 * @return
	 */
	public boolean getAffectedAssmtWarning(long permitId,long companyId,String fiscalYear,String quarter,String exclusionType);
	
	/**
	 * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * this will send warning message for COmpany having active permit for the period for which permit is being excluded 
	 * @param companyId
	 * @param permitId
	 * @param fiscalYear
	 * @param quarter
	 * @param exclusionType
	 * @return
	 * @throws ParseException 
	 */
	public boolean getWarningForPermitBeingExcluded(long companyId,long permitId,String fiscalYear,String quarter,String exclusionType) throws ParseException;
	public String getAssociatedCompanyByEIN(long fiscalYear,long fiscalQtr, String tobaccoClassNm, String ein);
	
}

package gov.hhs.fda.ctp.biz.companymgmt;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.hibernate.procedure.ProcedureOutputs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.AffectedAssessmentbean;
import gov.hhs.fda.ctp.common.beans.Attachment;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.CompanyPermitHistory;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.ExcludePermit;
import gov.hhs.fda.ctp.common.beans.ExcludePermitBean;
import gov.hhs.fda.ctp.common.beans.ExcludePermitRequest;
import gov.hhs.fda.ctp.common.beans.NewCompaniesExport;
import gov.hhs.fda.ctp.common.beans.NewCompaniesExportDetail;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitExcludeComment;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.common.util.CsvStringBuilder;
import gov.hhs.fda.ctp.common.util.EinBuilder;
import gov.hhs.fda.ctp.persistence.AddressMgmtEntityManager;
import gov.hhs.fda.ctp.persistence.CompanyMgmtEntityManager;
import gov.hhs.fda.ctp.persistence.PermitEntityManager;
import gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager;
import gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.model.AddressEntity;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.ContactEntity;
import gov.hhs.fda.ctp.persistence.model.PermitCommentAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;
import gov.hhs.fda.ctp.persistence.model.PermitExcludeCommentEntity;

/**
 * The Class CompanyMgmtBizCompImpl.
 *
 * @author tgunter
 */
@Service
@Transactional
public class CompanyMgmtBizCompImpl implements CompanyMgmtBizComp {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(CompanyMgmtBizCompImpl.class);

	/** The simple SQL manager. */
	/* Persistence Managers */
	private SimpleSQLManager simpleSQLManager;
	
	/** The company mgmt entity manager. */
	private CompanyMgmtEntityManager companyMgmtEntityManager;
	
	/** The reference code entity manager. */
	private ReferenceCodeEntityManager referenceCodeEntityManager;
	
	/** The permit entity manager. */
	private PermitEntityManager permitEntityManager;
	
	/** The permit period entity manager */
	private PermitPeriodEntityManager permitPeriodEntityManager;
	
	/** The address mgmt. */
	private AddressMgmtEntityManager addressMgmt;
	
	/** The Constant CONSTANT_PERMIT_TYPE. */
	private static final String CONSTANT_PERMIT_TYPE = "PERMIT_TYPE";

	@Autowired
	private MapperWrapper mapperWrapper;
	/**
	 * Sets the simple SQL manager.
	 *
	 * @param simpleSQLManager the new simple SQL manager
	 */
	
	@Autowired
	public void setSimpleSQLManager(SimpleSQLManager simpleSQLManager) {
		this.simpleSQLManager = simpleSQLManager;
	}

	/**
	 * Sets the company mgmt entity manager.
	 *
	 * @param companyMgmtEntityManager the new company mgmt entity manager
	 */
	@Autowired
	public void setCompanyMgmtEntityManager(CompanyMgmtEntityManager companyMgmtEntityManager) {
		this.companyMgmtEntityManager = companyMgmtEntityManager;
	}

	/**
	 * Sets the reference code entity manager.
	 *
	 * @param referenceCodeEntityManager the new reference code entity manager
	 */
	@Autowired
	public void setReferenceCodeEntityManager(ReferenceCodeEntityManager referenceCodeEntityManager) {
		this.referenceCodeEntityManager = referenceCodeEntityManager;
	}

	/**
	 * Sets the permit entity manager.
	 *
	 * @param permitEntityManager the new permit entity manager
	 */
	@Autowired
	public void setPermitEntityManager(PermitEntityManager permitEntityManager) {
		this.permitEntityManager = permitEntityManager;
	}

	/**
	 * Sets the address mgmt entity manager.
	 *
	 * @param aem the new address mgmt entity manager
	 */
	@Autowired
	public void setAddressMgmtEntityManager(AddressMgmtEntityManager aem) {
		this.addressMgmt = aem;
	}
	
	@Autowired
	public void setPermitPeriodEntityManager(PermitPeriodEntityManager ppem) {
		this.permitPeriodEntityManager = ppem;
	}


	/**
	 * Create company. This will persist a new company and attached permits.
	 *
	 * @param company the company
	 * @return the company
	 */
	@Override
	public Company createCompany(Company company) {
		CompanyEntity companyEntity = this.transformCompanyToEntity(company, true);
		companyEntity.setCompanyStatus("ACTV");
		companyEntity = companyMgmtEntityManager.createCompany(companyEntity);
		return this.transformEntityToCompany(companyEntity);
	}

	/**
	 * Update company. This will persist changes only to the company.
	 *
	 * @param company the company
	 * @return the company
	 */
	@Override
	public Company updateCompany(Company company) {
		logger.debug("UPDATE_COMPANY: dates = {} / {}", company.getCreatedDt(), company.getModifiedDt());
		
		//Priti-- Currently we have 2 dates for Create date, this needs to be refactored later.Temporary fix as this module is also touched from Permit Management Replacing Company"
		if(company.getCreatedDt() !=null || "".equalsIgnoreCase(company.getCreatedDt()) )
		{
			company.setcompanyCreateDt(company.getCreatedDt());
		}
		CompanyEntity companyEntity = this.transformCompanyToEntity(company, false);
		companyEntity = companyMgmtEntityManager.updateCompany(companyEntity);
		return this.transformEntityToCompany(companyEntity);
	}

	/**
	 * Get an existing company.
	 *
	 * @param id the id
	 * @return the company
	 */
	@Override
	public Company getCompany(long id) {
		CompanyEntity companyEntity = companyMgmtEntityManager.getCompanyById(id);
		return this.transformEntityToCompany(companyEntity);
	}

	/**
	 * In lieu of getting dozer to work with entity mapping.
	 *
	 * @param company the company
	 * @param newCompany the new company
	 * @return the company entity
	 */
	private CompanyEntity transformCompanyToEntity(Company company, Boolean newCompany) {
		/* map the company */
		CompanyEntity companyEntity = new CompanyEntity();
		/*
		 * temporarily map this by hand until we get dozer issues resolved TBD
		 */
		companyEntity.setCompanyId(company.getCompanyId());
		companyEntity.setCreatedBy(company.getCreatedBy());
		companyEntity.setCreatedDt(CTPUtil.parseStringToDate(company.getcompanyCreateDt()));
		companyEntity.setEIN(company.getEinNumber());
		companyEntity.setLegalName(company.getLegalName());
		companyEntity.setModifiedBy(company.getModifiedBy());
		companyEntity.setModifiedDt(CTPUtil.parseStringToDate(company.getModifiedDt()));
		companyEntity.setCompanyStatus(company.getCompanyStatus());
		companyEntity.setCompanyComments(company.getCompanyComments());
		/* map the permits for the company */
		Set<PermitEntity> permitEntities = new HashSet<>();
		Set<Permit> permits = company.getPermitSet();
		if (permits != null) {
			for (Permit permit : permits) {
				PermitEntity permitEntity = new PermitEntity();
				/*
				 * temporarily map this by hand until we get dozer issues
				 * resolved
				 */
				permitEntity.setPermitId(permit.getPermitId());
				permitEntity.setPermitNum(permit.getPermitNum());
				permitEntity.setIssueDt(CTPUtil.parseStringToDate(permit.getIssueDt()));
				if(permit.getCloseDt() !=null)
				{permitEntity.setCloseDt(CTPUtil.parseStringToDate(permit.getCloseDt()));}
				permitEntity.setttbstartDt(CTPUtil.parseStringToDate(permit.getttbstartDt()));
				permitEntity.setttbendDt(CTPUtil.parseStringToDate(permit.getttbendDt()));
				permitEntity.setPermitStatusTypeCd(permit.getPermitStatusTypeCd());
				permitEntity.setPermitTypeCd(permit.getPermitTypeCd());
				/* if new company, then set permit status to active */
				//if (newCompany) {
				//	permitEntity.setPermitStatusTypeCd(Constants.ACTIVE_CODE);
				//}
				permitEntity.setCompanyEntity(companyEntity);
				permitEntities.add(permitEntity);
			}
		}
		/* add the permits to the company and persist */
		companyEntity.setPermits(permitEntities);
		return companyEntity;
	}
	
	private List<ExcludePermit> populateExcludePermitList(List<Object[]> list) {
	    List<ExcludePermit> excludePermitList = new ArrayList<ExcludePermit>();
        for(Object [] obj :list) {
            ExcludePermit bean = new ExcludePermit();
            bean.setPermit((Long)obj[0]);
            bean.setExcludedDate((Date)obj[1]);
            excludePermitList.add(bean);
        }
        return excludePermitList;
    }

	/**
	 * In lieu of getting dozer to work with entity mapping.
	 *
	 * @param companyEntity the company entity
	 * @return the company
	 */
	@SuppressWarnings("unchecked")
    private Company transformEntityToCompany(CompanyEntity companyEntity) {
		if (companyEntity == null)
			return null;
		/* map the company */
		Company company = new Company();
		/*
		 * temporarily map this by hand util I figure out the Dozer issues TBD
		 * 
		 */
		logger.debug("COMPANY_ENTITY: retrieved created_by = {} created_dt = {}", companyEntity.getCreatedBy(),
				companyEntity.getCreatedDt());
		company.setCompanyId(companyEntity.getCompanyId());
		company.setEinNumber(companyEntity.getEIN());
		company.setLegalName(companyEntity.getLegalName());
		company.setCreatedBy(companyEntity.getCreatedBy());
		company.setCreatedDt(CTPUtil.parseDateToString(companyEntity.getCreatedDt()));
		company.setModifiedBy(companyEntity.getModifiedBy());
		company.setModifiedDt(CTPUtil.parseDateToString(companyEntity.getModifiedDt()));
		company.setCompanyStatus(companyEntity.getCompanyStatus());
		company.setCompanyComments(companyEntity.getCompanyComments());
		/**
		 * CTPTUFA-5256 - Getting exclude permit details for a Company
		 */
		List<Object[]> list = (List<Object[]>)companyMgmtEntityManager.getExcludePermitsForCompany(companyEntity.getCompanyId());
		List<?> excludePermitList = populateExcludePermitList(list);
//		if(CollectionUtils.isNotEmpty(excludePermitList)) {
//		    excludePermitList = excludePermitList.stream().distinct().collect(Collectors.toList());
//		}
		/* map the permits for the company */
		Set<PermitEntity> permitEntities = companyEntity.getPermits();
		Set<Permit> permits = new HashSet<>();
		if (permitEntities != null) {
			for (PermitEntity permitEntity : permitEntities) {
				Permit permit = new Permit();

				/* temp until Dozer figured out TBD*/
				permit.setPermitId(permitEntity.getPermitId());
				permit.setPermitNum(permitEntity.getPermitNum());
//				permit.setPermitComments(permitEntity.getPermitComments());
				permit.setIssueDt(CTPUtil.parseDateToString(permitEntity.getIssueDt()));
				permit.setCloseDt(CTPUtil.parseDateToString(permitEntity.getCloseDt()));
				permit.setttbstartDt(CTPUtil.parseDateToString(permitEntity.getttbstartDt()));
				permit.setttbendDt(CTPUtil.parseDateToString(permitEntity.getttbendDt()));
				permit.setCreatedBy(permitEntity.getCreatedBy());
				permit.setCreatedDt(CTPUtil.parseDateToString(permitEntity.getCreatedDt()));
				permit.setModifiedBy(permitEntity.getModifiedBy());
				permit.setModifiedDt(CTPUtil.parseDateToString(permitEntity.getModifiedDt()));
				permit.setCompanyId(company.getCompanyId());
				// find permit type value and permit status type value
				String permitType = referenceCodeEntityManager.getValue(CONSTANT_PERMIT_TYPE, permitEntity.getPermitTypeCd());
				String permitStatusType = referenceCodeEntityManager.getValue("PERMIT_STATUS_TYPE",
						permitEntity.getPermitStatusTypeCd());
				permit.setPermitTypeCd(permitType);
				permit.setPermitStatusTypeCd(permitStatusType);
				if(!CollectionUtils.isEmpty(excludePermitList)) {
				    //if(excludePermitList.contains(new BigDecimal(permit.getPermitId()))) {
				        //permit.setPermitExcluded(excludePermitList.contains(new BigDecimal(permit.getPermitId())));
				        for(Object obj : excludePermitList){
				            ExcludePermit ex = (ExcludePermit)obj;
				            if(ex.getPermit() == permit.getPermitId()) {
				                permit.setPermitExcluded(true);
				                permit.setDateExcluded(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(ex.getExcludedDate()));
				            }
				        }
				    //}
				}
				permits.add(permit);
			}
		}
		/* add the permits to the company and persist */
		company.setPermitSet(permits);

		/* map the contacts for the company */
		Set<ContactEntity> contactEntities = companyEntity.getContacts();
//		TreeSet<ContactEntity> contactTree = new TreeSet<ContactEntity>(contactEntities);
		List<Contact> contacts = new ArrayList<Contact>();
		if (contactEntities != null) {
			for (ContactEntity contactEntity : contactEntities) {
				Contact contact = new Contact();

				/* temp until Dozer figured out TBD*/
				contact.setCompanyId(contactEntity.getCompany().getCompanyId());
				contact.setContactId(contactEntity.getContactId());
				contact.setLastNm(contactEntity.getLastNm());
				contact.setFirstNm(contactEntity.getFirstNm());
				contact.setPhoneNum(contactEntity.getPhoneNum());
				contact.setPhoneExt(contactEntity.getPhoneExt());
				contact.setEmailAddress(contactEntity.getEmailAddress());
				contact.setCountryCd(contactEntity.getCountryCd());
				contact.setCntryDialCd(contactEntity.getCntryDialCd());
				contact.setFaxNum(contactEntity.getFaxNum());
				contact.setCntryFaxDialCd(contactEntity.getCntryFaxDialCd());
				contact.setCreatedDt(CTPUtil.parseDateToString(contactEntity.getCreatedDt()));
				contact.setPrimaryContactSequence(contactEntity.getPrimaryContactSequence());

				contacts.add(contact);
			}
		}
		/* add the contacts to the company and persist */
		company.setContactSet(contacts);
		
		/* map the addresses for the company */
		Set<AddressEntity> addressEntities = companyEntity.getAddresses();
		Set<Address> addresses = new HashSet<>();
		if (addressEntities != null) {
			for (AddressEntity addressEntity : addressEntities) {
				Address address = new Address();

				/* temp until Dozer figured out TBD */
				address.setAddressTypeCd(addressEntity.getAddressTypeCd());
				address.setCity(addressEntity.getCity());
				address.setCompanyId(addressEntity.getCompany().getCompanyId());
				if("PRIM".equals(addressEntity.getAddressTypeCd())) {
	                company.setPrimaryAddress(address);
	            }
	            else {
	                company.setAlternateAddress(address);
	            }
				address.setCountryCd(addressEntity.getCountryCd());
				if(StringUtils.isNotBlank(addressEntity.getCountryCd())){
					address.setCountryNm(addressMgmt.getCountryNm(addressEntity.getCountryCd()));
				}
				address.setFaxNum(addressEntity.getFaxNum());
				address.setPostalCd(addressEntity.getPostalCd());
				if(StringUtils.isNotBlank(addressEntity.getState())){
					address.setState(addressMgmt.getStateNm(addressEntity.getState()));
				}
				address.setStreetAddress(addressEntity.getStreetAddress());
				address.setSuite(addressEntity.getSuite());
				address.setAttention(addressEntity.getAttention());
				address.setProvince(addressEntity.getProvince());

				addresses.add(address);
			}
		}
		/* add the addresses to the company and persist */
		company.setAddressSet(addresses);
		

		return company;
	}
		
	

		
	/**
	 * Assemble permit entity.
	 *
	 * @param permit the permit
	 * @return the permit entity
	 */
	public PermitEntity assemblePermitEntity(Permit permit) {

		PermitEntity permitEntity = new PermitEntity();
		permitEntity.setPermitNum(permit.getPermitNum());
		if (permit.getPermitId() > 0)
			permitEntity.setPermitId(permit.getPermitId());

		permitEntity.setIssueDt(CTPUtil.parseStringToDate(permit.getIssueDt()));
		if(permit.getttbstartDt() != null) {
			permitEntity.setttbstartDt(CTPUtil.parseStringToDate(permit.getttbstartDt()));
		} else {
			permitEntity.setttbstartDt(CTPUtil.parseStringToDate(permit.getIssueDt()));
		}
		if(permit.getCloseDt() != null)
		{
		  permitEntity.setCloseDt(CTPUtil.parseStringToDate(permit.getCloseDt()));
		  permitEntity.setttbendDt(CTPUtil.parseStringToDate(permit.getCloseDt()));
		}
		
//		permitEntity.setPermitComments(permit.getPermitComments());
		if (permit.getPermitStatusTypeCd() == null && (permit.getPermitId() <= 0)) {
			permitEntity.setPermitStatusTypeCd("TBD");
		} else {
			permitEntity.setPermitStatusTypeCd(permit.getPermitStatusTypeCd());
		}

		if (permit.getPermitTypeCd() == null && (permit.getPermitId() <= 0)) {
			permitEntity.setPermitTypeCd("ACTV");
		} else {
			permitEntity.setPermitTypeCd(permit.getPermitTypeCd());
		}

		return permitEntity;
	}

	/**
	 * Assemble permit model.
	 *
	 * @param permitEntity the permit entity
	 * @return the permit
	 */
	private Permit assemblePermitModel(PermitEntity permitEntity) {

		Permit permit = new Permit();
		permit.setPermitNum(permitEntity.getPermitNum());
		permit.setPermitStatusTypeCd(permitEntity.getPermitStatusTypeCd());
		permit.setPermitTypeCd(referenceCodeEntityManager.getValue(CONSTANT_PERMIT_TYPE,
				permitEntity.getPermitTypeCd() == null ? "MANU" : permitEntity.getPermitTypeCd().trim()));
		permit.setPermitId(permitEntity.getPermitId());
//		permit.setPermitComments(permitEntity.getPermitComments());
		permit.setIssueDt(CTPUtil.parseDateToString(permitEntity.getIssueDt()));
		permit.setCloseDt(CTPUtil.parseDateToString(permitEntity.getCloseDt()));
		
		permit.setttbstartDt(CTPUtil.parseDateToString(permitEntity.getttbstartDt()));
		permit.setttbendDt(CTPUtil.parseDateToString(permitEntity.getttbendDt()));
		
		CompanyEntity companyEntity = permitEntity.getCompanyEntity();

		companyEntity.setPermits(null);
		Company company = this.transformEntityToCompany(companyEntity);
		permit.setCompany(company);
		return permit;
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp#createPermit(gov.hhs.fda.ctp.common.beans.Permit)
	 */
	@Override
	public Company createPermit(Permit permit) {

		CompanyEntity companyEntity = companyMgmtEntityManager.getCompanyById(permit.getCompanyId());
		PermitEntity permitEntity = this.assemblePermitEntity(permit);
		permitEntity.setCompanyEntity(companyEntity);
		permitEntity = permitEntityManager.createPermit(permitEntity);
		
		Company company = this.transformEntityToCompany(permitEntity.getCompanyEntity());
		if(company != null){
			company.setPermitNumber(permitEntity.getPermitNum());
		}
		return company;
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp#getPermit(long)
	 */
	@Override
	public Permit getPermit(long id) {
		PermitEntity permitEntity = permitEntityManager.getPermit(id);
		return this.assemblePermitModel(permitEntity);
	}
	
	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp#updatePermit(gov.hhs.fda.ctp.common.beans.Permit)
	 */
	@Override
	public Company updatePermit(Permit permit) {

		PermitEntity permEnty = permitEntityManager.getPermit(permit.getPermitId());
		permEnty.setPermitNum(permit.getPermitNum());
		permEnty.setIssueDt(CTPUtil.parseStringToDate(permit.getIssueDt()));	
		permEnty.setttbstartDt(CTPUtil.parseStringToDate(permit.getttbstartDt()));		
		permEnty.setPermitStatusTypeCd(permit.getPermitStatusTypeCd());
		if("ACTV".equals(permit.getPermitStatusTypeCd())) {
			permEnty.setCloseDt(null);		
			permEnty.setttbendDt(null);
		}
		else {
			permEnty.setCloseDt(CTPUtil.parseStringToDate(permit.getCloseDt()));			
			permEnty.setttbendDt(CTPUtil.parseStringToDate(permit.getttbendDt()));
		}
		
		
//		permEnty.setPermitComments(permit.getPermitComments());
		permEnty = permitEntityManager.updatePermit(permEnty);
		Company company = this.transformEntityToCompany(permEnty.getCompanyEntity());
		if(company != null){
			company.setPermitNumber(permEnty.getPermitNum());
		}
		
		return company;
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp#getPermits(java.lang.String, long)
	 */
	@Override
	public List<Permit> getPermits(String status, long companyId) {

		List<PermitEntity> permits = companyMgmtEntityManager.getPermits(status, companyId);

		List<Permit> permitModels = new ArrayList<>();

		for (int i = 0; i < permits.size(); i++) {
			PermitEntity pEntity = permits.get(i);
			Permit permit = this.assemblePermitModel(pEntity);
			permitModels.add(permit);
		}
		return permitModels;
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp#getPermitHistory(gov.hhs.fda.ctp.common.beans.Permit)
	 */
	@Override
	public Permit getPermitHistory(Permit permit) {
		// get category code conversions
		Map<String, String> periodStatusCodes = referenceCodeEntityManager.getValues("PERIOD_STATUS_TYPE");
		Map<String, String> permitTypeCodes = referenceCodeEntityManager.getValues("PERMIT_TYPE");
		
		List<CompanyPermitHistory> permitHistoryList = simpleSQLManager.getPermitHistory(permit.getPermitId());
		for (CompanyPermitHistory permitHistory : permitHistoryList) {
			permitHistory.setPermitNum(permit.getPermitNum());
			String month = permitHistory.getMonth();
			permitHistory.setSortMonthYear(permitHistory.getYear() + Constants.MMMToNumber.get(month));
			permitHistory.setDisplayMonthYear(Constants.MMMToMMMM.get(month) + " FY " + permitHistory.getFiscalYear());
			permitHistory.setReportStatusCd(periodStatusCodes.get(permitHistory.getReportStatusCd().trim()));
			permitHistory.setPermitTypeCd(permitTypeCodes.get(permitHistory.getPermitTypeCd().trim()));
		}
		permit.setPermitHistoryList(permitHistoryList);
		return permit;
	}
	
	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp#getPermitHistory(gov.hhs.fda.ctp.common.beans.Permit)
	 */
	@Override
	public Permit getPermitHistory3852(Permit permit) {
		// get category code conversions
		Map<String, String> periodStatusCodes = referenceCodeEntityManager.getValues("PERIOD_STATUS_TYPE");
		Map<String, String> permitTypeCodes = referenceCodeEntityManager.getValues("PERMIT_TYPE");
		
		List<CompanyPermitHistory> permitHistoryList = simpleSQLManager.getPermitHistory3852(permit.getPermitId());
		for (CompanyPermitHistory permitHistory : permitHistoryList) {
			permitHistory.setPermitNum(permit.getPermitNum());
			String month = permitHistory.getMonth().trim();
			permitHistory.setSortMonthYear(permitHistory.getYear() + Constants.MMMMToNumber.get(month));
			permitHistory.setDisplayMonthYear(month + " FY " + permitHistory.getFiscalYear());
			permitHistory.setReportStatusCd(periodStatusCodes.get(permitHistory.getReportStatusCd().trim()));
			permitHistory.setPermitTypeCd(permitTypeCodes.get(permitHistory.getPermitTypeCd().trim()));
		}
		permit.setPermitHistoryList(permitHistoryList);
		return permit;
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp#getPaginatedPermitHistory(gov.hhs.fda.ctp.common.beans.Permit)
	 */
//	@Override
//	public Permit getPaginatedPermitHistory(Permit permit, int page, int rows) {
//		// get category code conversions
//		Map<String, String> periodStatusCodes = referenceCodeEntityManager.getValues("PERIOD_STATUS_TYPE");
//		Map<String, String> permitTypeCodes = referenceCodeEntityManager.getValues("PERMIT_TYPE");
//		int historyCount = simpleSQLManager.getPermitHistoryCount(permit.getPermitId());
//		List<CompanyPermitHistory> permitHistoryList = simpleSQLManager.getPaginatedPermitHistory(permit.getPermitId(), page, rows);
//		for (CompanyPermitHistory permitHistory : permitHistoryList) {
//			permitHistory.setPermitNum(permit.getPermitNum());
//			String month = permitHistory.getMonth();
//			permitHistory.setSortMonthYear(permitHistory.getYear() + Constants.MMMToNumber.get(month));
//			permitHistory.setDisplayMonthYear(Constants.MMMToMMMM.get(month) + " FY " + permitHistory.getFiscalYear());
//			permitHistory.setReportStatusCd(periodStatusCodes.get(permitHistory.getReportStatusCd().trim()));
//			permitHistory.setPermitTypeCd(permitTypeCodes.get(permitHistory.getPermitTypeCd().trim()));
//		}
//		permit.setPermitHistoryCount(historyCount);
//		permit.setPermitHistoryList(permitHistoryList);
//		return permit;
//	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp#getPermitById(long, java.lang.String)
	 */
	@Override
	public PermitEntity getPermitById(long cmpnyId, String permitNum) {
		return companyMgmtEntityManager.getPermitById(cmpnyId, permitNum);
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp#getCompanies(gov.hhs.fda.ctp.common.beans.CompanySearchCriteria)
	 */
	@Override
	public CompanySearchCriteria getCompanies(CompanySearchCriteria criteria) {		
		CompanySearchCriteria retcriteria = simpleSQLManager.getAllCompanies(criteria);
        if(retcriteria != null){
        	if(retcriteria.getResults() == null)
        		return criteria;
        }
        return retcriteria;
	}
	

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp#getCompanyPermitHistory(gov.hhs.fda.ctp.common.beans.Company)
	 */
	@Override
	public Company getCompanyPermitHistory(Company company) {
		Map<String, String> periodStatusCodes = referenceCodeEntityManager.getValues("PERIOD_STATUS_TYPE");
		Map<String, String> permitTypeCodes = referenceCodeEntityManager.getValues("PERMIT_TYPE");
		List<CompanyPermitHistory> permitHistoryList = simpleSQLManager.getCompanyPermitHistory(company.getCompanyId());
		for(CompanyPermitHistory history : permitHistoryList) {
			String month = history.getMonth();
			history.setSortMonthYear(history.getYear() + Constants.MMMToNumber.get(month));
			history.setDisplayMonthYear(Constants.MMMToMMMM.get(month) + " FY " + history.getFiscalYear());
			history.setReportStatusCd(periodStatusCodes.get(history.getReportStatusCd().trim()));
			history.setPermitTypeCd(permitTypeCodes.get(history.getPermitTypeCd().trim()));
		}
    	company.setPermitHistoryList(permitHistoryList);
    	return company;
	} 
	
	/**
	 * Save / update comment for a company.
	 */
	@Override
	public String updateCompanyComment(long companyId, String comment) {
		CompanyEntity company = companyMgmtEntityManager.getCompanyById(companyId);
		company.setCompanyComments(comment);
		company = companyMgmtEntityManager.updateCompany(company);
		return company.getCompanyComments();
	}

	@Override
	public Company getCompanybyEIN(String ein) {
		CompanyEntity companyEntity = companyMgmtEntityManager.getCompanyByEIN(ein);
		Company result = new Company();
		if(companyEntity != null){
			result = this.transformEntityToCompany(companyEntity);
		}
		return result;
	}

	@Override
	public Company getActvImptCompByEIN(String ein) {
		CompanyEntity companyEntity = companyMgmtEntityManager.getActvCBPCompByEIN(ein);
		Company cmpy = new Company();
		if(companyEntity != null) {
			cmpy = this.transformEntityToCompany(companyEntity);
		}
		return cmpy;
	}
	
	@Override
	public List<Company> getCompanyTrackingInformation(CompanySearchCriteria criteria){
		//We only care about companies who's welcome flag is N
		Mapper mapper = mapperWrapper.getMapper();
		List<CompanyEntity> results = this.simpleSQLManager.getCompanyTrackingInformationForSearch(criteria);
		List<Company> companies = new ArrayList<Company>();
		
		for (CompanyEntity entity : results){
			Company company = new Company();
			mapper.map(entity, company);
			companies.add(company);
		}
		
		return companies;
	}
	
	@Override
	public List<Company> updateCompanyTrackingInformation(List<Company> companies) {
		Mapper mapper = mapperWrapper.getMapper();
		for(Company company : companies){
			//Ensure you have all the current info
			CompanyEntity current = this.companyMgmtEntityManager.getCompanyById(company.getCompanyId());
			//Update the tracking info
			current.setEmailAddressFlag(company.getEmailAddressFlag());
			current.setPhysicalAddressFlag(company.getPhysicalAddressFlag());
			current.setWelcomeLetterFlag(company.getWelcomeLetterFlag());
			if("Y".equalsIgnoreCase(company.getWelcomeLetterFlag()))
				current.setWelcomeLetterSentDt(new Date());
			//Save the updated info
			this.companyMgmtEntityManager.updateCompany(current);
		}
		return companies;
	}
	
		
	@Override
	public NewCompaniesExport getNewCompaniesExport(){

		NewCompaniesExport export = new NewCompaniesExport();
		CsvStringBuilder csv = new CsvStringBuilder();
		// get list of export records
		List<NewCompaniesExportDetail> newCmpExportDetails = simpleSQLManager.getNewCompaniesList();
		// build title
		StringBuilder title = new StringBuilder("New Companies as of ");
		//append current date
		title.append(new SimpleDateFormat("MM/dd/yyyy").format(new Date()));
		//append the file extension
		title.append(".csv");
		
		csv.csvAppendNewLine("Company Name, EIN, Date Created, Email, Address, Welcome Letter Sent Date");
		for(NewCompaniesExportDetail detail : newCmpExportDetails) {
			csv.csvAppendInQuotes(detail.getCompanyName());
			csv.csvAppend(new EinBuilder(detail.getCompanyEIN()).getFormattedEin());
			csv.csvAppend(CTPUtil.parseDateToString(detail.getCreatedDate()));
			csv.csvAppend(detail.getEmailId());
			csv.csvAppendInQuotes(detail.getCompanyPrimAddress());
			csv.csvAppendNewLine(CTPUtil.parseDateToString(detail.getWelcomeLetterSentDate()));
		}
		export.setTitle(title.toString());
		export.setCsv(csv.toString());
		return export;
	
		
	}

	 /**
     *CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     */
    @Override
    public boolean excludeAllPermits(long companyId, long permitId,String exclusionType, List<ExcludePermitRequest> request, PermitExcludeComment comment) {
        /*boolean isValid;
        if(!exclusionType.equalsIgnoreCase("INCLUDE")) {
            isValid = isValidPermitForExclude(companyId,permitId,request);
        } else {
            isValid = true;
        }
        //boolean isValid = true;
        if(!isValid) {
            return false;
        }*/
    	Mapper mapper = this.mapperWrapper.getMapper();
    	PermitExcludeCommentEntity commentEntity = new PermitExcludeCommentEntity();
    	mapper.map(comment, commentEntity);
    	
        companyMgmtEntityManager.updateExcludeAllPermits(companyId, permitId, exclusionType, request, commentEntity);
        return true;
    }

    private int getQuarter(int month) {
        int quarter;
        if(month >= Calendar.OCTOBER && month <= Calendar.DECEMBER) {
            quarter = 1;
        } else if(month >= Calendar.JANUARY && month <= Calendar.MARCH) {
            quarter = 2;
        } else if(month >= Calendar.APRIL && month <= Calendar.JUNE) {
            quarter = 3;
        } else {
            quarter = 4;
        }
        return quarter;
    }
    private boolean isValidPermitForExclude(long companyId, long permitId,List<ExcludePermitRequest> request) {
        PermitEntity entity = companyMgmtEntityManager.getPermitbyId(companyId, permitId);
        Date permitIssueDate = entity.getIssueDt();
        Date permitCloseDate = entity.getCloseDt();
        Calendar cal = Calendar.getInstance();
        Calendar closecal = Calendar.getInstance();
        cal.setTime(permitIssueDate);
        if(null == permitCloseDate) {
            closecal.setTime(new Date());
        } else {
            closecal.setTime(permitCloseDate);
        }
        int month = cal.get(Calendar.MONTH);
        int closeMonth = closecal.get(Calendar.MONTH);
        int issueDateYear = cal.get(Calendar.YEAR);
        int closeDateYear = closecal.get(Calendar.YEAR);
        int issueDataQuarter = getQuarter(month);
        int closeDateQuarter = getQuarter(closeMonth);
        for(ExcludePermitRequest req : request) {
            if(((Integer.parseInt(req.getFiscalYear()) >= issueDateYear &&  (Integer.parseInt(req.getFiscalYear()) <= closeDateYear))) 
            /* && entity.getPermitStatusTypeCd().equalsIgnoreCase("ACTV") */) {
                for(String quarter : req.getQuarterselected()) {
                    if((issueDateYear == Integer.parseInt(req.getFiscalYear()) && Integer.parseInt(quarter)<issueDataQuarter) ||  
                            ((Integer.parseInt(req.getFiscalYear()) == closeDateYear && Integer.parseInt(quarter)>closeDateQuarter))) {
                        //if exclude year is equal to issue date year or close date year then compare quarters
                        return false;
                    } else {
                        // if exclude year is in between range of issue and close date then no need to check quarter
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
        
    }

    //TODO:List generics will be finalized on entity or view level
    @Override
    public List<ExcludePermitBean> getExcludePermitDetails(long permitId, long companyid) {
        List<Object[]> list = companyMgmtEntityManager.getPermitExcludeDetails(permitId, companyid);
        List<ExcludePermitBean> excludeList = new ArrayList<ExcludePermitBean>();
        for(Object[] excludeDetailsList :list) {
            ExcludePermitBean bean = new ExcludePermitBean();
            String quarter = excludeDetailsList[0].toString();
            bean.setQuarter(quarter);
            String assessmentYear = excludeDetailsList[1].toString();
            bean.setAssessmentYear(assessmentYear);
            bean.setUser(excludeDetailsList[2].toString());
            bean.setDatecreated(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format((Date)excludeDetailsList[3]));
            bean.setStatus(excludeDetailsList[4].toString());
            bean.setAssessmentPeriod("Q".concat(quarter).concat(",").concat(assessmentYear));
            bean.setExcludeScopeId((Long)excludeDetailsList[5]);
            excludeList.add(bean);
        }
        return excludeList;
    }
    public List<PermitExcludeComment> getExcludePermitComments(long permitId, long companyId) {
    	List<PermitExcludeCommentEntity> results = companyMgmtEntityManager.getPermitExcludeComments(companyId, permitId);
    	List<PermitExcludeComment> comments = new ArrayList<PermitExcludeComment>();
    	Mapper mapper = this.mapperWrapper.getMapper();
    	
    	for(PermitExcludeCommentEntity result : results) {
    		PermitExcludeComment comment = new PermitExcludeComment();
    		mapper.map(result, comment);
    		comment.setAttachmentsCount(result.getAttachments().size());
    		comments.add(comment);
    	}
    	return comments;
    }
    
    public void updateExcludeCommentDetails(PermitExcludeComment comment) {
    	Mapper mapper = this.mapperWrapper.getMapper();
    	PermitExcludeCommentEntity commentEntity = new PermitExcludeCommentEntity();
    	mapper.map(comment, commentEntity);
    	companyMgmtEntityManager.updatePermitExcludeComment(commentEntity);
    }

    /**
     *CTPTUFA-4947 - Assessments: Show table under the Assessments page that lists all Permits marked as Excluded Pt2
     */
    @Override
    public List<AffectedAssessmentbean> getAffectedAssessmentData() {
        List<Object[]> list = companyMgmtEntityManager.getAffectedAssessmentData();
        List<AffectedAssessmentbean> affectedassmtList = new ArrayList<AffectedAssessmentbean>();
        for(Object[] affectedAssmt :list) {
            AffectedAssessmentbean bean = new AffectedAssessmentbean();
            String assmtfy;
            if(affectedAssmt[1].toString().equalsIgnoreCase("QTRY")) {
                bean.setAssessmentType("Quarterly");
            } else if(affectedAssmt[1].toString().equalsIgnoreCase("CIQT")) {
                bean.setAssessmentType("Cigar");
            } else {
                bean.setAssessmentType("Annual");  
            }
            
            bean.setCompanyName(affectedAssmt[2].toString());
            bean.setEin(affectedAssmt[3].toString());
            bean.setPermitNumber(affectedAssmt[4].toString());
			/*
			 * if(affectedAssmt[5].toString().equalsIgnoreCase("-1")) { assmtfy =
			 * "FY"+affectedAssmt[0].toString(); } else { assmtfy =
			 * "Q".concat(affectedAssmt[5].toString()+" "+"FY"+affectedAssmt[0].toString());
			 * }
			 * 
			 * bean.setAssessmentFY(assmtfy);
			 */
            bean.setAssessmentFY(affectedAssmt[0] != null ? affectedAssmt[0].toString() : "");
            if(affectedAssmt[1].toString().equalsIgnoreCase("ANNU"))
            	bean.setAssessmentQTR("N/A");
            else
            	bean.setAssessmentQTR(affectedAssmt[5] != null ? affectedAssmt[5].toString() : "");
            
            affectedassmtList.add(bean);
        }
        return affectedassmtList;
        
    }
    
    
    @Override
	public List<PermitCommentAttachmentsEntity> findCommentDocbyId(long commentId) {
		return this.companyMgmtEntityManager.findCommentDocbyId(commentId);
	}
    @Override
    public Attachment getPermitCommentAttachmentDownload(long docId) throws SQLException {
    	PermitCommentAttachmentsEntity attachment = this.companyMgmtEntityManager.getPermitCommentAttachment(docId);;
		Attachment result = new Attachment();
		result.setReportPdf(companyMgmtEntityManager.getByteArray(attachment.getReportPdf()));
		result.setDocDesc(attachment.getDocDesc());
		
		return result;
	}
    @Override
    public byte[] getPermitCommentAttachmentAsByteArray(long docId) throws  SQLException{
    	return this.companyMgmtEntityManager.getPermitCommentAttachmentAsByteArray(docId);
    }
    
	public void addPermitCommentAttach(PermitCommentAttachmentsEntity permitcommentattachment, MultipartFile file)throws IOException, SQLException{
		PermitCommentAttachmentsEntity pae = new PermitCommentAttachmentsEntity();
		Blob blob = null;
		if(permitcommentattachment.getDocumentId() != 0){
			pae = this.companyMgmtEntityManager.getPermitCommentAttachment(permitcommentattachment.getDocumentId());
			pae.setDocDesc(permitcommentattachment.getDocDesc());
			pae.setFormTypes(permitcommentattachment.getFormTypes());
		}else {
			//Done to match legacy files 
			byte[] pretext = "data:application/pdf;base64,".getBytes();
			byte[] encodedFile = Base64.encode(file.getBytes());
			
			byte[] combination = new byte[pretext.length + encodedFile.length];
			System.arraycopy(pretext, 0, combination, 0, pretext.length);
			System.arraycopy(encodedFile, 0, combination, pretext.length, encodedFile.length);
			
			blob = this.companyMgmtEntityManager.getBlob(combination);
			permitcommentattachment.setReportPdf(blob);
			pae = permitcommentattachment;
		}
		
		this.companyMgmtEntityManager.addPermitCommentAttachment(pae);
		
		if(blob != null) {
			blob.free();
		}
	}
	
	public void deleteDocById(long docId){
		this.companyMgmtEntityManager.deleteDocById(docId);
	}
	
	/**
     ** CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * this will send warning message for affected assessment data for permit being excluded
     */
    @Override
    public boolean getAffectedAssmtWarning(long permitId,long companyId, String fiscalYear,String quarter, String exclusionType) {
        // TODO Auto-generated method stub
        Long exclusionScopeId;
        if(exclusionType.equals("FULL")) {
            exclusionScopeId = new Long(1);
        } else if (exclusionType.equalsIgnoreCase("INCLUDE")) {
            exclusionScopeId = new Long(3);
        } else {
        	exclusionScopeId = new Long(2);
        }
        String[] selectedQtr =  null;
        if(quarter != null ){
        	selectedQtr = quarter.split(",");
        }
        for(String selQtr:selectedQtr){
        	ProcedureOutputs outputs = companyMgmtEntityManager.getAffectedAssmtwarning(permitId,companyId, Long.parseLong(fiscalYear), Long.parseLong(selQtr), exclusionScopeId);
        	 long size =  (Long)outputs.getOutputParameterValue("affected_assmnt_size");
        	 if( size > 0 ){
        		 return true;
        	 }
        }
        return false;
    }

    /**
     * 
     *  * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * this will send warning message for COmpany having active permit for the period for which permit is being excluded 
     * @throws ParseException 
     *
     */
    @Override
    public boolean getWarningForPermitBeingExcluded(long companyId,
            long permitId, String fiscalYear, String quarter,
            String exclusionType) throws ParseException {
        Long exclusionScopeId;
        if(exclusionType.equals("FULL")) {
            exclusionScopeId = new Long(1);
        } else if(exclusionType.equals("INCLUDE")) {
            exclusionScopeId = new Long(3);
        } else {
            exclusionScopeId = new Long(2);
        }
        return companyMgmtEntityManager.getWarningForPermitBeingExcluded(companyId, permitId, fiscalYear, quarter, exclusionScopeId);
        
    }
    @Override
    public String getAssociatedCompanyByEIN(long fiscalYear,long fiscalQtr, String tobaccoClassNm, String ein) {
    	if ("ROLL YOUR OWN".equalsIgnoreCase(tobaccoClassNm) || "RYO".equalsIgnoreCase(tobaccoClassNm)) {
    		tobaccoClassNm = "Roll-Your-Own";
		} 
    	
    	return companyMgmtEntityManager.getAssociatedCompanyByEIN(fiscalYear, fiscalQtr, tobaccoClassNm, ein);
    }
    
}

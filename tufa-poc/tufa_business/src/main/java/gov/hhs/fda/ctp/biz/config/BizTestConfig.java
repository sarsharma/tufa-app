package gov.hhs.fda.ctp.biz.config;

import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.security.crypto.password.PasswordEncoder;

import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizCompImpl;
import gov.hhs.fda.ctp.biz.permit.PermitBizComp;
import gov.hhs.fda.ctp.biz.permit.PermitBizCompImpl;
import gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp;
import gov.hhs.fda.ctp.biz.quarter.AssessmentBizCompImpl;
import gov.hhs.fda.ctp.biz.registration.RegistrationBizComp;
import gov.hhs.fda.ctp.biz.registration.RegistrationBizCompImpl;
import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.persistence.AddressMgmtEntityManager;
import gov.hhs.fda.ctp.persistence.AssessmentEntityManager;
import gov.hhs.fda.ctp.persistence.BaseEntityManager;
import gov.hhs.fda.ctp.persistence.CompanyMgmtEntityManager;
import gov.hhs.fda.ctp.persistence.PermitEntityManager;
import gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager;
import gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.TrueUpEntityManager;

/**
 * The Class BizTestConfig.
 */
@Profile("test")
public class BizTestConfig { 
	
	/**
	 * Base entity manager.
	 *
	 * @return the base entity manager
	 */
	@Bean
	@Primary 
	public BaseEntityManager baseEntityManager() {
		return Mockito.mock(BaseEntityManager.class);
	}
	
	@Bean
	@Primary
	public PermitPeriodEntityManager permitPeriodEntityManager() {
	    return Mockito.mock(PermitPeriodEntityManager.class);
	}
	
	@Bean
	@Primary
	public ReferenceCodeEntityManager referenceCodeEntityManager() {
	    return Mockito.mock(ReferenceCodeEntityManager.class);
	}
	
	@Bean
	@Primary
	public PermitEntityManager permitEntityManager() {
	    return Mockito.mock(PermitEntityManager.class);
	}
	
	@Bean
	@Primary
	public SimpleSQLManager simpleSQLManager() {
	    return Mockito.mock(SimpleSQLManager.class);
	}
	@Bean
	@Primary
	public AssessmentEntityManager assessmentEntityManager() {
	    return Mockito.mock(AssessmentEntityManager.class);
	}
	
	
	@Bean
	@Primary 
	public PasswordEncoder passwordEncoder() {
		return Mockito.mock(PasswordEncoder.class);
	}
	
	@Bean
	@Primary
	public RegistrationBizComp registrationBizComp() {
		return new RegistrationBizCompImpl();
	}
	
	@Bean
	@Primary
	public CompanyMgmtEntityManager companyMgmtEntityManager() {
	    return Mockito.mock(CompanyMgmtEntityManager.class);
	}
	
	@Bean
	@Primary
	public AddressMgmtEntityManager addressMgmtEntityManager() {
	    return Mockito.mock(AddressMgmtEntityManager.class);
	}
	
	@Bean
	@Primary
	public TrueUpEntityManager trueUpEntityManager() {
		return Mockito.mock(TrueUpEntityManager.class);
	}
	
	@Bean
	@Primary
	public CompanyMgmtBizComp companyMgmtBizComp() {
	    CompanyMgmtBizCompImpl companyMgmtBizCompImpl = new CompanyMgmtBizCompImpl();
        companyMgmtBizCompImpl.setCompanyMgmtEntityManager(this.companyMgmtEntityManager());
        companyMgmtBizCompImpl.setAddressMgmtEntityManager(this.addressMgmtEntityManager());
        return companyMgmtBizCompImpl;
	}
	
	@Bean
	@Primary
	public PermitBizComp permitBizComp() {
	    PermitBizCompImpl permitBizCompImpl = new PermitBizCompImpl();
	    permitBizCompImpl.setPermitEntityManager(this.permitEntityManager());
	    permitBizCompImpl.setPermitPeriodEntityManager(this.permitPeriodEntityManager());
	    return permitBizCompImpl;
	}
	
	@Bean
	@Primary
	public AssessmentBizComp assessmentBizComp() {
		AssessmentBizCompImpl assessmentBizCompImpl = new AssessmentBizCompImpl();
		assessmentBizCompImpl.setAssessmentEntityManager(this.assessmentEntityManager());
	    return assessmentBizCompImpl;
	}

    @Bean(name = "mapperFactory")
    @Primary
    public DozerBeanMapperFactoryBean configDozer() {
        DozerBeanMapperFactoryBean mapperFactory = new DozerBeanMapperFactoryBean();
        Resource[] resources;
        try {
            resources = new PathMatchingResourcePatternResolver().getResources("classpath*:dozerBeanMapping.xml");
        }
        catch(Exception e) {
            throw new TufaException("Could not find dozerBeanMapping.xml on the classpath.", e);
        }
        mapperFactory.setMappingFiles(resources);
        return mapperFactory;
    }
}

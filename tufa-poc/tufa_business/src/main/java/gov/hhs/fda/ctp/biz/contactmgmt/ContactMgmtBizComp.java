/**
 * 
 */
package gov.hhs.fda.ctp.biz.contactmgmt;

import java.util.List;
import java.util.Set;

import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.persistence.model.ReportContactEntity;

/**
 * The Interface ContactMgmtBizComp.
 *
 * @author akari
 */
public interface ContactMgmtBizComp {

	/**
	 * create a contact.
	 *
	 * @param contact
	 *            the contact
	 * @return the contact
	 */
	public Contact create(Contact contact);

	/**
	 * read contact by Id.
	 *
	 * @param id
	 *            the id
	 * @return the contact
	 */
	public Contact readById(long id);

	/**
	 * update contact.
	 *
	 * @param contact
	 *            the contact
	 * @return the contact
	 */
	public Contact update(Contact contact);

	/**
	 * Delete contact by Id.
	 *
	 * @param id
	 *            the id
	 */
	public void deleteById(long id);

	/**
	 * Return contacts for monthly report.
	 * 
	 * @param companyId
	 * @param periodId
	 * @return
	 */
	public List<ReportContactEntity> getReportContacts(int companyId, int permitId, int periodId);

	public List<Contact> saveMultipleContacts(List<Contact> model);
	
	public Contact setAsPrimaryContact(boolean setPrimary, Contact contact);
}

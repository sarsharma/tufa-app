/**
 *
 */
package gov.hhs.fda.ctp.biz.contactmgmt;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.ContactMgmtEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.ContactEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;
import gov.hhs.fda.ctp.persistence.model.ReportContactEntity;

/**
 * @author akari
 *
 */
@Service
@Transactional
public class ContactMgmtBizCompImpl implements ContactMgmtBizComp {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(ContactMgmtBizCompImpl.class);

	/* Persistence Managers */
	private SimpleSQLManager simpleSQLManager;
	private ContactMgmtEntityManager contactMgmtEntityManager;

	/**
	 * Sets the simple SQL manager.
	 *
	 * @param simSQLManager the new simple SQL manager
	 */
	@Autowired
	public void setSimpleSQLManager(SimpleSQLManager simSQLManager) {
		this.simpleSQLManager = simSQLManager;
	}

	/**
	 * @param contactMgmtEntityManager
	 *            the contactMgmtEntityManager to set
	 */
	@Autowired
	public void setContactMgmtEntityManager(ContactMgmtEntityManager contactMgmtEntityManager) {
		this.contactMgmtEntityManager = contactMgmtEntityManager;
	}

	/*
	 * create a contact
	 */
	@Override
	public Contact create(Contact contact) {
		logger.debug(" - Entering Create - ");
		ContactEntity contactEntity = this.transformContactToEntity(contact);
		contactEntity = contactMgmtEntityManager.create(contactEntity);
		
		if(!companyHasPrimaryContact(contact) || contact.getSetAsPrimary()){
			this.setAsPrimaryContact(contactEntity);
		}
		
		logger.debug(" - Exiting Create - ");
		return this.transformEntityToContact(contactEntity);
	}
	
	
     public List<Contact> saveMultipleContacts(List<Contact> model)	
	{	
    	List<Contact> contacts = model;
 		for(Contact cont : contacts) {
 			logger.warn(cont.getEin());		
 			create(cont);	
 			this.contactMgmtEntityManager.saveStatus(cont.getEin());
 		}
 		return contacts;		
	}

	/**
	 * @param contactEntity
	 * @return
	 */
	private Contact transformEntityToContact(ContactEntity contactEntity) {
		/*
		 * temporarily map this by hand until we get dozer issues resolved TBD
		 */
		logger.debug(" - Entering transformEntityToContact - ");
		Contact contact = new Contact();
		CompanyEntity companyEntity = contactEntity.getCompany();
		contact.setCompanyId(companyEntity.getCompanyId());
		contact.setContactId(contactEntity.getContactId());
		contact.setCountryCd(contactEntity.getCountryCd());
		contact.setCntryDialCd(contactEntity.getCntryDialCd());
		contact.setCreatedBy(contactEntity.getCreatedBy());
		contact.setCreatedDt(CTPUtil.parseDateToString(contactEntity.getCreatedDt()));
		contact.setEmailAddress(contactEntity.getEmailAddress());
		contact.setFaxNum(contactEntity.getFaxNum());
		contact.setFirstNm(contactEntity.getFirstNm());
		contact.setLastNm(contactEntity.getLastNm());
		contact.setModifiedBy(contactEntity.getModifiedBy());
		contact.setModifiedDt(CTPUtil.parseDateToString(contactEntity.getModifiedDt()));
		contact.setPhoneExt(contactEntity.getPhoneExt());
		contact.setPhoneNum(contactEntity.getPhoneNum());
		contact.setCntryFaxDialCd(contactEntity.getCntryFaxDialCd());
		contact.setPrimaryContactSequence(contactEntity.getPrimaryContactSequence());
		
		logger.debug(" - Exiting transformEntityToContact - ");
		return contact;
	}

	/**
	 * @param contact
	 * @param b
	 * @return
	 */
	private ContactEntity transformContactToEntity(Contact contact) {
		/*
		 * temporarily map this by hand until we get dozer issues resolved TBD
		 */
		logger.debug(" - Exiting transformContactToEntity - ");
		ContactEntity contactEntity = new ContactEntity();
		if(contact != null){
			if (contact.getCompany() != null) {
				Company company = contact.getCompany();
				boolean newCompanyFlag = (company.getCompanyId() == 0);
				contactEntity.setCompany(transformCompanyToEntity(contact.getCompany(), newCompanyFlag));
			}
			else
			{
				
				CompanyEntity companyEntity = new CompanyEntity();
		        companyEntity.setCompanyId(contact.getCompanyId());
		        contactEntity.setCompany(companyEntity);
				
			}
			contactEntity.setContactId(contact.getContactId());
			contactEntity.setCountryCd(contact.getCountryCd());
			contactEntity.setCntryDialCd(contact.getCntryDialCd());
			contactEntity.setCntryFaxDialCd(contact.getCntryFaxDialCd());
			contactEntity.setEmailAddress(contact.getEmailAddress());
			contactEntity.setFaxNum(contact.getFaxNum());
			contactEntity.setFirstNm(contact.getFirstNm());
			contactEntity.setLastNm(contact.getLastNm());
			contactEntity.setModifiedBy(contact.getModifiedBy());
			contactEntity.setModifiedDt(CTPUtil.parseStringToDate(contact.getModifiedDt()));
			contactEntity.setPhoneExt(contact.getPhoneExt());
			contactEntity.setPhoneNum(contact.getPhoneNum());
			contactEntity.setPrimaryContactSequence(contact.getPrimaryContactSequence());
		}
		logger.debug(" - Exiting transformContactToEntity - ");
		return contactEntity;
	}
	
	
	

	/**
	 * In lieu of getting dozer to work with entity mapping.
	 *
	 * @param company
	 * @return
	 */
	private CompanyEntity transformCompanyToEntity(Company company, Boolean newCompany) {
		logger.debug(" - Entering transformCompanyToEntity - ");

		/* map the company */
		CompanyEntity companyEntity = new CompanyEntity();
		/*
		 * temporarily map this by hand until we get dozer issues resolved TBD
		 */
		companyEntity.setCompanyId(company.getCompanyId());
		companyEntity.setCreatedBy(company.getCreatedBy());
		companyEntity.setCreatedDt(CTPUtil.parseStringToDate(company.getCreatedDt()));
		companyEntity.setEIN(company.getEinNumber());
		companyEntity.setLegalName(company.getLegalName());
		companyEntity.setModifiedBy(company.getModifiedBy());
		companyEntity.setModifiedDt(CTPUtil.parseStringToDate(company.getModifiedDt()));

		/* map the permits for the company */
		Set<PermitEntity> permitEntities = new HashSet<>();
		Set<Permit> permits = company.getPermitSet();
		if (permits != null) {
			for (Permit permit : permits) {
				PermitEntity permitEntity = new PermitEntity();
				/*
				 * temporarily map this by hand until we get dozer issues
				 * resolved TBD
				 */
				permitEntity.setPermitId(permit.getPermitId());
				permitEntity.setPermitNum(permit.getPermitNum());
				permitEntity.setIssueDt(CTPUtil.parseStringToDate(permit.getIssueDt()));
				permitEntity.setPermitStatusTypeCd(permit.getPermitStatusTypeCd());
				permitEntity.setPermitTypeCd(permit.getPermitTypeCd());
				/* if new company, then set permit status to active */
				if (newCompany) {
					permitEntity.setPermitStatusTypeCd(Constants.ACTIVE_CODE);
				}
				permitEntity.setCompanyEntity(companyEntity);

				permitEntities.add(permitEntity);
			}
		}
		/* add the permits to the company and persist */
		companyEntity.setPermits(permitEntities);
		logger.debug(" - Exiting transformCompanyToEntity - ");
		return companyEntity;
	}

	/*
	 * read contact by Id
	 */
	@Override
	public Contact readById(long id) {
		logger.debug(" - Entering readById - ");
		Contact contact = transformEntityToContact(contactMgmtEntityManager.readById(id));
		logger.debug(" - Exiting readById - ");
		return contact;
	}

	/*
	 * update contact
	 */
	@Override
	public Contact update(Contact contact) {
		logger.debug(" - Entering update - ");
		ContactEntity contactEntity = this.transformContactToEntity(contact);
		contactEntity = contactMgmtEntityManager.update(contactEntity);
		
		if(!this.companyHasPrimaryContact(contact) || contact.getSetAsPrimary()){
			this.setAsPrimaryContact(contactEntity);
		}
		
		logger.debug(" - Exiting update - ");
		return transformEntityToContact(contactEntity);
	}

	/*
	 * Set As Primary
	 */
	@Override
	public Contact setAsPrimaryContact(boolean setPrimary, Contact contact){
		logger.debug(" - Entering update - ");
		ContactEntity contactEntity = this.transformContactToEntity(contact);
		contactEntity = contactMgmtEntityManager.update(contactEntity);	
		if(setPrimary)		
			this.setAsPrimaryContact(contactEntity);
		
		logger.debug(" - Exiting update - ");
		return transformEntityToContact(contactEntity);
	}
	
	/*
	 * Delete contact by Id
	 */
	@Override
	public void deleteById(long id) {
		logger.debug(" - Entering deleteById - ");
		ContactEntity result = this.contactMgmtEntityManager.readById(id);
		if (result != null && result.getPrimaryContactSequence() != null){
			this.updatePrimaryContactSequenceForRemoval(result);
		} else {
			contactMgmtEntityManager.delete(result);
		}
		logger.debug(" - Exiting deleteById - ");
	}

	public void setAsPrimaryContact(ContactEntity contact){
		if(contact != null && (contact.getPrimaryContactSequence() == null || contact.getPrimaryContactSequence() != 1)){
			
			//Check if there is already a primary contact
			if(this.contactMgmtEntityManager.companyHasPrimaryContact(contact.getCompany().getCompanyId())){
				//Update the older primary contacts sequence
				this.updatePrimaryContactSequenceForAddition(contact);
			}
			
			contact.setPrimaryContactSequence(1L);
			this.contactMgmtEntityManager.update(contact);
		}
	}
	
	private void updatePrimaryContactSequenceForAddition(ContactEntity contact) {
		if(contact.getPrimaryContactSequence() == null) { 
			//Contact has never been a primary before so the whole sequence needs to be reordered
			List<ContactEntity> contacts = this.contactMgmtEntityManager.getPrimaryContactsHistoryByCompany(contact.getCompany().getCompanyId());
			for (ContactEntity olderPrimary : contacts) {
				this.incrementPrimaryContactSequence(olderPrimary);
			}
		}  else {
			//Contact exists in the sequence so only part of the sequence needs to be reordered
			Long sequenceId = contact.getPrimaryContactSequence();
			List<ContactEntity> contacts = this.contactMgmtEntityManager.getPrimaryContactsHistoryByCompany(contact.getCompany().getCompanyId());
			for (ContactEntity olderPrimary : contacts) {
				if(olderPrimary.getPrimaryContactSequence() < sequenceId){
					this.incrementPrimaryContactSequence(olderPrimary);
				}
			}
		}
	}
	
	private void incrementPrimaryContactSequence(ContactEntity contact) {
		Long updatedSequence = contact.getPrimaryContactSequence() + 1;
		contact.setPrimaryContactSequence(updatedSequence);
		contactMgmtEntityManager.update(contact);
	}
	
	/**
	 * Removes the contacts from the primary sequence and updates the remaining sequence.
	 * 
	 * EX: If im attempting to remove 1
	 * 1,2,3,4,5 -> 2,3,4,5 -> 1,2,3,4
	 * 
	 * EX: If attempting to remove 4
	 * 1,2,3,4,5,6 -> 1,2,3,5,6 -> 1,2,3,4,5
	 * */
	private void updatePrimaryContactSequenceForRemoval(ContactEntity contact) {
		if(contact.getPrimaryContactSequence() != null) {
			Long sequenceId = contact.getPrimaryContactSequence();
			List<ContactEntity> contacts = this.contactMgmtEntityManager.getPrimaryContactsHistoryByCompany(contact.getCompany().getCompanyId());
			for (ContactEntity olderPrimary : contacts) {
				if(olderPrimary.getPrimaryContactSequence() > sequenceId){
					Long updatedSequence = olderPrimary.getPrimaryContactSequence() - 1;
					olderPrimary.setPrimaryContactSequence(updatedSequence);
					contactMgmtEntityManager.update(olderPrimary);
				}
			}
			this.contactMgmtEntityManager.delete(contact);
		}
	}

	/**
	 * get all the contact information to display on the monthly report
	 */
	@Override
	public List<ReportContactEntity> getReportContacts(int companyId, int permitId, int periodId) {
		List<ReportContactEntity> contacts = contactMgmtEntityManager.getReportContacts(companyId, permitId, periodId);
		List<ReportContactEntity> resultSet = new ArrayList<ReportContactEntity>();
		if(contacts != null) {
			for(ReportContactEntity reportContact : contacts) {
				ContactEntity contact = contactMgmtEntityManager.readById(reportContact.getContactId());
				if(contact == null) {
					reportContact.setCreatedDt(new Date());
				}
				else {
					reportContact.setCreatedDt(contact.getCreatedDt());
				}
				resultSet.add(reportContact);
			}
		}
		return resultSet;
	}
	
	private boolean companyHasPrimaryContact(Contact contact) {
		long companyId = 0;
		if(contact.getCompanyId() != null) {
			companyId = contact.getCompanyId();
		} else if(contact.getCompany() != null){
			companyId = contact.getCompany().getCompanyId();
		}
		return contactMgmtEntityManager.companyHasPrimaryContact(companyId);
	}
}

/**
 *
 */
package gov.hhs.fda.ctp.biz.permit;

import java.util.List;
import java.util.Set;

import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.FormComment;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitHistCommentsBean;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCommentResponse;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.common.beans.PermitMfgReport;
import gov.hhs.fda.ctp.common.beans.PermitPeriod;
import gov.hhs.fda.ctp.common.beans.TTBAdjustment;
import gov.hhs.fda.ctp.common.beans.TTBAdjustmentDetail;
import gov.hhs.fda.ctp.common.beans.TobaccoClass;
import gov.hhs.fda.ctp.persistence.model.TTBAdjustmentDetailEntity;

/**
 * The Interface PermitBizComp.
 *
 * @author tgunter
 */
public interface PermitBizComp {

    /**
     * Gets the permit histories.
     *
     * @param criteria the criteria
     * @return the permit histories
     */
    public PermitHistoryCriteria getPermitHistories(PermitHistoryCriteria criteria);

    /**
     * Gets the permit period.
     *
     * @param permitId the permit id
     * @param periodId the period id
     * @return the permit period
     */
    public PermitPeriod getPermitPeriod(long permitId, long periodId);

    /**
     * Save or update permit contacts.
     *
     * @param permit the permit
     * @return the permit
     */
    public Permit saveOrUpdatePermitContacts(Permit permit);

    /**
     * Save submitted forms.
     *
     * @param test the test
     * @return the permit period
     */
    public PermitPeriod saveSubmittedForms(PermitMfgReport test);

    /**
     * Gets the available permit report fiscal years.
     *
     * @return the available permit report fiscal years
     */
    public Set<String> getAvailablePermitReportFiscalYears();

    /**
     * Gets the available permit report calendar years.
     *
     * @return the available permit report calendar years
     */
    public Set<String> getAvailablePermitReportCalendarYears();

    /**
     * Gets the companies.
     *
     * @param criteria the criteria
     * @return the companies
     */
    public CompanySearchCriteria getCompanies(CompanySearchCriteria criteria);

    /**
     * Save / update the comments on the permit.
     * @param permitId
     * @param comment
     * @return
     */
    public String updatePermitComments(long permitId, String comment);

    /**
     * Save a comment to a form (non-editable).  Create the form if it doesn't exist.
     * @param commentEntity
     * @param permitId
     * @param periodId
     * @param tobaccoClassId
     * @param formTypeCd
     * @return
     */
    public FormComment saveFormComment(FormComment comment, long permitId, long periodId);

    /**
     * Delete comments by form id.
     * @param formId
     */
    public void deleteFormCommentsById(long formId);
    
    /**
     * Delete comments for that have no associated form
     * @param formIds
     * @param permitId
     * @param periodId
     */
    public void deleteOrphanedFormComments(List<Long> formIds,long permitId,long periodId);


    /**
     * Get a list of all tobacco classes.
     * @return
     */
    public List<TobaccoClass> getLatestTobaccoClasses();

    public boolean deleteMonthlyReport(long companyId, long permitId, long periodId);

    /**
     * Add new list of all tobacco classes.
     * @return
     */
    public void saveTobaccoClasses(List<TobaccoClass> rates);

    public List<FormComment> getZeroReportComment(long permitId, long periodId);
    
    /**
     * CTPTUFA - 4891 - Permit History: Update comments section to reflect monthly reports comments format
     * @param permitHistComments
     * @param permitId TODO
     * @return
     */
    public PermitHistCommentsBean saveUpdatePermitHistoryComments(PermitHistCommentsBean permitHistComments, long permitId);
    
    public PermitHistoryCommentResponse getAllCommentsForPermit(long permitId);
    
    public PermitHistCommentsBean updatePermitHistoryComment(long permitCommentId, PermitHistCommentsBean permitHistCommentBean);
    
    public void deleteTTBAdjustmentById(long formId, long tobaccoClassId);
    
    public TTBAdjustment saveTTBAdjustments (long permitId, long periodId, TTBAdjustment ttbAdjustment);
   
}

/**
 *
 */
package gov.hhs.fda.ctp.biz.permit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.CompanyPermitHistory;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.FormComment;
import gov.hhs.fda.ctp.common.beans.FormDetail;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitHistCommentsBean;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCommentResponse;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.common.beans.PermitMfgReport;
import gov.hhs.fda.ctp.common.beans.PermitPeriod;
import gov.hhs.fda.ctp.common.beans.SubmittedForm;
import gov.hhs.fda.ctp.common.beans.TTBAdjustment;
import gov.hhs.fda.ctp.common.beans.TTBAdjustmentDetail;
import gov.hhs.fda.ctp.common.beans.TobaccoClass;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.security.UserContext;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.PermitEntityManager;
import gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager;
import gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.model.ActivityPK;
import gov.hhs.fda.ctp.persistence.model.AddressEntity;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.CurrentTobaccoRateViewEntity;
import gov.hhs.fda.ctp.persistence.model.FormCommentEntity;
import gov.hhs.fda.ctp.persistence.model.FormDetailEntity;
import gov.hhs.fda.ctp.persistence.model.FormDetailPK;
import gov.hhs.fda.ctp.persistence.model.PeriodStatusEntity;
import gov.hhs.fda.ctp.persistence.model.PeriodTaxRateViewEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;
import gov.hhs.fda.ctp.persistence.model.PermitHistComments;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;
import gov.hhs.fda.ctp.persistence.model.RptPeriodEntity;
import gov.hhs.fda.ctp.persistence.model.SubmittedFormEntity;
import gov.hhs.fda.ctp.persistence.model.TTBAdjustmentDetailEntity;
import gov.hhs.fda.ctp.persistence.model.TaxRateEntity;
import gov.hhs.fda.ctp.persistence.model.ZeroReportCommentEntity;

/**
 * The Class PermitBizCompImpl.
 *
 * @author tgunter
 */
@Service("permitBizComp")
@Transactional
public class PermitBizCompImpl implements PermitBizComp {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(PermitBizCompImpl.class);

    /** The simple SQL manager. */
    /* Persistence Managers */
    private SimpleSQLManager simpleSQLManager;

    /** The reference code entity manager. */
    private ReferenceCodeEntityManager referenceCodeEntityManager;

    /** The permit entity manager. */
    private PermitEntityManager permitEntityManager;

    /** The permit period entity manager. */
    private PermitPeriodEntityManager permitPeriodEntityManager;

    /** The mapper factory. */
    @Autowired
    private DozerBeanMapperFactoryBean mapperFactory;

	@Autowired
	private MapperWrapper mapperWrapper;

	@Autowired
	private SecurityContextUtil securityContextUtil;

    private static final String FORM_TYPE = "FORM_TYPE";  // Compliant

    /**
     * Sets the permit period entity manager.
     *
     * @param pmtPeriodEntityManager the new permit period entity manager
     */
    @Autowired
    public void setPermitPeriodEntityManager(PermitPeriodEntityManager pmtPeriodEntityManager) {
        this.permitPeriodEntityManager = pmtPeriodEntityManager;
    }

    /**
     * Sets the permit entity manager.
     *
     * @param pmtEntityManager the new permit entity manager
     */
    @Autowired
    public void setPermitEntityManager(PermitEntityManager pmtEntityManager) {
        this.permitEntityManager = pmtEntityManager;
    }

    /**
     * Sets the simple SQL manager.
     *
     * @param simSQLManager the new simple SQL manager
     */
    @Autowired
    public void setSimpleSQLManager(SimpleSQLManager simSQLManager) {
        this.simpleSQLManager = simSQLManager;
    }

    /**
     * Sets the reference code entity manager.
     *
     * @param refCodeEntityManager the new reference code entity manager
     */
    @Autowired
    public void setReferenceCodeEntityManager(ReferenceCodeEntityManager refCodeEntityManager) {
        this.referenceCodeEntityManager = refCodeEntityManager;
    }


    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.biz.permit.PermitBizComp#getPermitHistories(gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria)
     */
    @Override
    public PermitHistoryCriteria getPermitHistories(PermitHistoryCriteria criteria) {
    	PermitHistoryCriteria newcriteria = simpleSQLManager.getAllPermitHistory(criteria);
        if(newcriteria.getResults() == null) {
            return criteria;
        }
        Map<String, String> permitTypes = referenceCodeEntityManager.getValues("PERMIT_TYPE");
        Map<String, String> permitStatusTypes = referenceCodeEntityManager.getValues("PERMIT_STATUS_TYPE");
        for(CompanyPermitHistory history: newcriteria.getResults()) {
            String month = history.getMonth();
            if(month == null){
                history.setSortMonthYear("197001");
                history.setDisplayMonthYear("");
            }else{
            	history.setDisplayMonthYear(Constants.MMMToMMMM.get(month)+ " FY " + history.getFiscalYear());
                history.setSortMonthYear(history.getYear() + Constants.MMMToNumber.get(month));
            }
            history.setPermitTypeCd(permitTypes.get(history.getPermitTypeCd()));
            history.setPermitStatusCd(permitStatusTypes.get(history.getPermitStatusCd()));
        }
        return newcriteria;
    }

    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.biz.permit.PermitBizComp#getCompanies(gov.hhs.fda.ctp.common.beans.CompanySearchCriteria)
     */
    @Override
    public CompanySearchCriteria getCompanies(CompanySearchCriteria criteria){
    	CompanySearchCriteria newcriteria = simpleSQLManager.getAllCompanies(criteria);
        if(newcriteria.getResults() == null) {
            return criteria;
        }
        return newcriteria;
    }
    
    /**
     * Get object graph with permit -> points of contact
     *                              -> company -> addresses.
     *
     * @param permitId the permit id
     * @param periodId the period id
     * @return the permit period
     */
    @Override
    public PermitPeriod getPermitPeriod(long permitId, long periodId) {
        Mapper mapper = mapperWrapper.getMapper();
        ActivityPK activityPK = new ActivityPK(permitId, periodId);
        PermitPeriodEntity permitPeriodEntity = permitPeriodEntityManager.getPermitPeriodByPK(activityPK);
        RptPeriodEntity rptPeriodEntity = permitPeriodEntityManager.getRptPeriodById(periodId);
        PermitEntity permitEntity = permitEntityManager.getPermit(permitId);
        String periodOneTaxId_500024 = null;
        String periodTwoTaxId_500024 = null;
        String periodThreeTaxId_500024 = null;

        /* Now lazy load the company and addresses */
        CompanyEntity companyEntity = permitEntity.getCompanyEntity();
        Set<AddressEntity> addressEntities = companyEntity.getAddresses();

        Company company = new Company();
        mapper.map(companyEntity,  company);

        /* find the primary and alternate address */

        for(AddressEntity addressEntity: addressEntities) {
            Address address = new Address();
            mapper.map(addressEntity, address);
            address.setCompanyId(company.getCompanyId());
            if("PRIM".equals(addressEntity.getAddressTypeCd())) {
                company.setPrimaryAddress(address);
            }
            else {
                company.setAlternateAddress(address);
            }
        }

        /* now map the tobacco classes */
        List<TobaccoClass> tobaccoClasses = new ArrayList<>();
        List<PeriodTaxRateViewEntity> tobaccoRateEntities = this.referenceCodeEntityManager.getTobaccoClasses(periodId);
        for(PeriodTaxRateViewEntity tobaccoRateEntity : tobaccoRateEntities) {
            TobaccoClass tobaccoClass = new TobaccoClass();
            mapper.map(tobaccoRateEntity, tobaccoClass);
            tobaccoClasses.add(tobaccoClass);
        }
        /* map the permit fields */
        Permit permit = new Permit();
        mapper.map(permitEntity, permit);
        permit.setCompany(company);
        permit.setTobaccoClasses(tobaccoClasses);
        /* map the permit period */
        PermitPeriod permitPeriod = new PermitPeriod();
        mapper.map(permitPeriodEntity, permitPeriod);
    	PeriodStatusEntity reportStatus = findReportStatus(permitPeriodEntity);
        if(reportStatus != null) {
            permitPeriod.setStatus(reportStatus.getPeriodStatusTypeCd());
        }
        else {
            permitPeriod.setStatus("NSTD");
        }
        // sort the forms and details
        List<SubmittedForm> forms = permitPeriod.getSubmittedForms();
        Collections.sort(forms);
        SimpleDateFormat inputFormat = new SimpleDateFormat("MMM");
    	Calendar cal = Calendar.getInstance();
    	SimpleDateFormat outputFormat = new SimpleDateFormat("MM");
    	try {
			cal.setTime(inputFormat.parse(rptPeriodEntity.getMonth()));
			String month = outputFormat.format(cal.getTime());
			if(Integer.parseInt(month) < 9) {
				periodOneTaxId_500024 = "TR"+rptPeriodEntity.getYear()+((2*Integer.parseInt(month)-1)<10?"-0"+(2*Integer.parseInt(month)-1):"-"+(2*Integer.parseInt(month)-1));
				periodTwoTaxId_500024 = "TR"+rptPeriodEntity.getYear()+((2*Integer.parseInt(month))<10?"-0"+(2*Integer.parseInt(month)):"-"+(2*Integer.parseInt(month)));
			} else {
				if(Integer.parseInt(month) == 9) {
					periodOneTaxId_500024 = "TR"+rptPeriodEntity.getYear()+"-"+(2*Integer.parseInt(month)-1);
					periodTwoTaxId_500024 = "TR"+rptPeriodEntity.getYear()+"-"+2*Integer.parseInt(month);
					periodThreeTaxId_500024 = "TR"+rptPeriodEntity.getYear()+"-"+(2*Integer.parseInt(month)+1);
					
				} else {
					periodOneTaxId_500024 = "TR"+rptPeriodEntity.getYear()+"-"+(2*Integer.parseInt(month));
					periodTwoTaxId_500024 = "TR"+rptPeriodEntity.getYear()+"-"+(2*Integer.parseInt(month)+1);
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        for(SubmittedForm form: forms) {
        	//7501 Form Details should be pulled in reverse order 8,7,6,5...
        	if ("7501".equals(form.getFormTypeCd())) {
                Collections.sort(form.getFormDetails(),Collections.reverseOrder());
        	} else {
                Collections.sort(form.getFormDetails());
        	}
            List<FormComment> formComments = new ArrayList<>();
            List<FormCommentEntity> commentEntities = permitPeriodEntityManager.getFormComments(form.getFormId());
            for(FormCommentEntity commentEntity : commentEntities) {
            	FormComment comment = new FormComment();
            	mapper.map(commentEntity, comment);
            	formComments.add(comment);
            }
            form.setFormComments(formComments);
            //populating the ttbadjustment
            form.setTtbAdjustments(populateTTBAdjustments(form.getFormId()));
        }
        
        // set the month / year / quarter from the reporting period
        permitPeriod.setMonth(rptPeriodEntity.getMonth());
        permitPeriod.setYear(rptPeriodEntity.getYear());
        permitPeriod.setQuarter(rptPeriodEntity.getQuarter());
        convertCodesToValues(permitPeriod);
        // set the appropriate ids
        permitPeriod.setPermit(permit);
        permitPeriod.setPeriodId(periodId);
        permitPeriod.setPermitId(permitId);
        permitPeriod.setPeriodOneTaxId_500024(periodOneTaxId_500024);
        permitPeriod.setPeriodTwoTaxId_500024(periodTwoTaxId_500024);
        permitPeriod.setPeriodThreeTaxId_500024(periodThreeTaxId_500024);
        /* Combine the separate 5000.24 forms into a single form with multiple details. */
        /* Combine the separate 7501 forms into a single form with multiple details.  */
        return permitPeriod;
    }
    
    public List<TTBAdjustment> populateTTBAdjustments(long formId) {
        Mapper mapper = mapperWrapper.getMapper();

    	List<TTBAdjustment> ttbAdjustments = new ArrayList<>();
        TTBAdjustment ttbAdjustment = null;
        List<TTBAdjustmentDetail> ttbAdjustmentDetails = null;
        //retrieve for all tobaccoType
        List<TTBAdjustmentDetailEntity> ttbAdjustmentDetailEntities = permitPeriodEntityManager.getTTBAdjustmentDetail(formId,0);
        long tobaccoClassId = 0;
        Double adjATaxAmt = 0.0;
        Double adjBTaxAmt = 0.0;
        for(TTBAdjustmentDetailEntity ttbAdjustmentDetailEntity : ttbAdjustmentDetailEntities) {
        	if(tobaccoClassId != ttbAdjustmentDetailEntity.getTobaccoClassId()) {
        		if(null != ttbAdjustmentDetails && ttbAdjustmentDetails.size() > 0) {
        			ttbAdjustment =  new TTBAdjustment();
        			ttbAdjustment.setTtbAdjustmentDetail(ttbAdjustmentDetails);
        			ttbAdjustment.setTobaccoClassId(tobaccoClassId);
        			ttbAdjustment.setTaxAmountAdjusmentA(adjATaxAmt);
        			ttbAdjustment.setTaxAmountAdjusmentB(adjBTaxAmt);
        			ttbAdjustment.setTaxAmountTotalAdjustment(adjATaxAmt - adjBTaxAmt );
        			ttbAdjustment.setFormId(formId);
        			ttbAdjustments.add(ttbAdjustment);
        		}
        		ttbAdjustmentDetails = new ArrayList<>();
        		adjATaxAmt = 0.0;
        		adjBTaxAmt = 0.0;
        	}
        	if("A".equalsIgnoreCase(ttbAdjustmentDetailEntity.getAdjType())) {
        		adjATaxAmt+= ttbAdjustmentDetailEntity.getTaxAmount()!= null ?ttbAdjustmentDetailEntity.getTaxAmount():0;
        	}
        	if("B".equalsIgnoreCase(ttbAdjustmentDetailEntity.getAdjType())) {
        		adjBTaxAmt+= ttbAdjustmentDetailEntity.getTaxAmount()!= null ?ttbAdjustmentDetailEntity.getTaxAmount():0 ;
        	}

        	tobaccoClassId = ttbAdjustmentDetailEntity.getTobaccoClassId();
        	TTBAdjustmentDetail ttbAdjustmentDetail = new TTBAdjustmentDetail();
        	mapper.map(ttbAdjustmentDetailEntity, ttbAdjustmentDetail);
        	ttbAdjustmentDetails.add(ttbAdjustmentDetail);
        }
        //to set the last iteration
        if(null != ttbAdjustmentDetails && ttbAdjustmentDetails.size() > 0) {
        	ttbAdjustment =  new TTBAdjustment();
			ttbAdjustment.setTtbAdjustmentDetail(ttbAdjustmentDetails);
			ttbAdjustment.setTobaccoClassId(ttbAdjustmentDetails.get(0).getTobaccoClassId());
			ttbAdjustment.setTaxAmountAdjusmentA(adjATaxAmt);
			ttbAdjustment.setTaxAmountAdjusmentB(adjBTaxAmt);
			ttbAdjustment.setTaxAmountTotalAdjustment(adjATaxAmt - adjBTaxAmt );
			ttbAdjustment.setFormId(formId);
			ttbAdjustments.add(ttbAdjustment);
        }
        return ttbAdjustments;
    
    }
    /**
     * Save the submitted forms.
     *
     * @param report the report
     * @return the permit period
     */
    @Override
    public PermitPeriod saveSubmittedForms(PermitMfgReport report) {
        /* look up the existing permit period entity */
        Long permitId = report.getPermitId();
        Long periodId = report.getPeriodId();
        ActivityPK activityPK = new ActivityPK(permitId, periodId);
        boolean is3852Modified = report.isIs3852modified();
        PermitPeriodEntity permitPeriodEntity = permitPeriodEntityManager.getPermitPeriodByPK(activityPK);
        setFormLineNumbers(report);
        /* covert the readable values to codes */
        convertValuesToCodes(report);
        /* save or update the status */
        if(permitPeriodEntity.getZeroFlag()== null) {
        	permitPeriodEntity.setZeroFlag("N");
        }
       
        if(!is3852Modified && null == permitPeriodEntity.getSubmittedForms()){
        	List<SubmittedForm> incomingFormList = report.getSubmittedForms();
    		for(SubmittedForm form:incomingFormList){
    			if("3852".equalsIgnoreCase(form.getFormTypeCd())){
    				is3852Modified = true;
    				break;
    			}
    		}
        }
        /* look for deletes (cascade not working because of composite key) */
        Set<FormDetailEntity> deleteDetails = new HashSet<>();
        List<Long> deleteComments = new ArrayList<>();
        is3852Modified =  checkForDeletes(report, permitPeriodEntity, deleteDetails, deleteComments);
        // only when the missing form for 3852 has been not modified 3852 field changes are to be identified
        checkPermitPeriodStatus(report, permitPeriodEntity,is3852Modified);
        /* map the DTO onto the entity */
        Mapper mapper = mapperWrapper.getMapper();
        mapper.map(report, permitPeriodEntity);
        
        /* delete the comments that have been removed */
       for(Long formId : deleteComments) {
         permitPeriodEntityManager.deleteFormCommentsById(formId);
       }
       
        /* delete the details that have been removed */
        permitPeriodEntityManager.deleteFormDetails(deleteDetails);
        
        
        /* cascade save / update on permit period graph */
        permitPeriodEntityManager.saveOrUpdatePermitPeriod(permitPeriodEntity);
        
        return getPermitPeriod(permitId, periodId);
    }

    /**
     * Create a form comment (never editable).  Creates a form if it does not exist yet.
     * @param commentEntity
     * @param permitId
     * @param periodId
     * @param tobaccoClassId
     * @param formTypeCd
     * @return
     */
    @Override
    public FormComment saveFormComment(FormComment comment, long permitId, long periodId) {
    	
    	String formType = referenceCodeEntityManager.getCode(FORM_TYPE, comment.getFormTypeCd());
    	// if special form, get formNum from tobacco type, otherwise formNum = 0
    	long formNum = 0;
    	
    	FormCommentEntity commentEntity = new FormCommentEntity();
    	ZeroReportCommentEntity zerocommentEntity = new ZeroReportCommentEntity();
        Mapper mapper = mapperWrapper.getMapper();
        mapper.map(comment, commentEntity);
        mapper.map(comment, zerocommentEntity);
        
    	if(CTPUtil.is7501Form(comment.getFormTypeCd())) {
    		formNum = comment.getTobaccoClassId();
    	}
    	if (formType == null && formNum == 0  )
    	{   	      
           zerocommentEntity.setUserComment(comment.getUserComment());
//           zerocommentEntity.setCommentDate(new Date());    
           zerocommentEntity.setPeriodId(periodId);
           zerocommentEntity.setPermitId(permitId);      	
           zerocommentEntity = permitPeriodEntityManager.createOrUpdateZeroReportComment(zerocommentEntity); 
           mapper.map(zerocommentEntity, comment);
    	}
    	
    	else
    	{
    	   SubmittedFormEntity form = permitPeriodEntityManager.getOrCreateSubmittedForm( permitId,  periodId,  formNum,  formType);   	
    	   commentEntity.setFormId(form.getFormId());
//    	   commentEntity.setCommentDate(new Date());
    	   commentEntity = permitPeriodEntityManager.createOrUpdateFormComment(commentEntity);
    	   mapper.map(commentEntity, comment);
    	}
    	
    	return comment;
    
    }

  
   	
    public String getUser() {
    	try {
    			UserContext user = securityContextUtil.getPrincipal();
    			return user.getUsername();
    		} catch (Exception e) {
    			return "UNITTEST";
    	}
    	}
	

	@Override
    public List<FormComment> getZeroReportComment( long permitId, long periodId)
    {		
		 Mapper mapper = mapperWrapper.getMapper();
	   	 List<FormComment> formComments = new ArrayList<>();
	     List<ZeroReportCommentEntity> commentEntities = permitPeriodEntityManager.getZeroReportComments(permitId, periodId);
	     for(ZeroReportCommentEntity commentEntity : commentEntities) {
	     	FormComment comment = new FormComment();
	     	mapper.map(commentEntity, comment);
	     	formComments.add(comment);
	     }
	   //  form.setFormComments(formComments);
    	
	     return formComments;
    	
    }
    
	
	 

	
	
	
	
	
    /**
     * Delete form comments by form id.
     * @param formId
     */
    @Override
    public void deleteFormCommentsById(long formId) {
        permitPeriodEntityManager.deleteFormCommentsById(formId);
    }
    
    /**
     * Delete comments for that have no associated form
     * @param formId
     * @param permitId
     * @param periodId
     */
    @Override
    public void deleteOrphanedFormComments(List<Long> formIds,long permitId,long periodId){
    	permitPeriodEntityManager.deleteOrphanedFormComments(formIds, permitId, periodId);
    }
    
    /**
     * Sets the form line numbers.
     *
     * @param report the new form line numbers
     */
    private void setFormLineNumbers(PermitMfgReport report) {
        List<SubmittedForm> forms = report.getSubmittedForms();
        if(forms == null)
        	return;
        for(SubmittedForm form : forms) {
        	String formType = form.getFormTypeCd();
        	// set form num
        	// 7501 is the only form that has one panel per tobacco type.
            if(!CTPUtil.is7501Form(formType)) {
                form.setFormNum(0L);
            }
            else {
                Long tobaccoClassForThisForm = getTobaccoClass(form);
                // fake out the form number by using tobacco class instead
                logger.debug("Setting form number to {} for form type {}.", tobaccoClassForThisForm, form.getFormTypeCd());
                form.setFormNum(tobaccoClassForThisForm);
            }
            // set line numbers on details
            Integer detailType;
            Integer lineNum;
            Map<Integer, Integer> lineNumberMap = new HashMap<>();
            List<FormDetail> details = form.getFormDetails();
            for(FormDetail detail : details) {
                // 5000 form has line numbers per tobacco type
                // all others have line numbers from 0-n
            	if(CTPUtil.is5000Form(formType)) {
            		detailType = detail.getTobaccoClassId().intValue();
            	}
            	else {
            		detailType = 0;
            	}
            	if(lineNumberMap.get(detailType) == null) {
            		lineNum = new Integer(0);
            		lineNumberMap.put(detailType, lineNum);
            	}
            	lineNum = lineNumberMap.get(detailType);
            	// set form id for the detail
            	if(form.getFormId() != null) {
                    detail.setFormId(form.getFormId());
                }
            	// set line number on the detail
            	
            	if(CTPUtil.is7501Form(formType))
            	{	
            	   detail.setLineNum(detail.getLineNum());
            	}
            	else
            	{
            		detail.setLineNum(lineNum.longValue());
                    lineNumberMap.put(detailType, lineNum + 1);
            	}
            }
        }
    }

    /**
     * Gets the tobacco class.
     *
     * @param form the form
     * @return the tobacco class
     */
    private Long getTobaccoClass(SubmittedForm form) {
        // get tobacco classes needed for 5000 forms
        Map<Long, Long> parentClassMap = referenceCodeEntityManager.getTobaccoParentMap(form.getPeriodId());
        // get the first detail to check its class
        FormDetail detail = form.getFormDetails().get(0);
        if("7501".equals(form.getFormTypeCd()))
            // 7501 has one form per child class.
            return detail.getTobaccoClassId();
        else
            // 5000 has all of the subclasses for a parent class on one form.
            return parentClassMap.get(detail.getTobaccoClassId());
    }

    /**
     * Check for deleted form details, or cascade delete all details for a deleted form.
     *
     * @param report the report
     * @param permitPeriodEntity the permit period entity
     * @return the sets the
     */
    private boolean checkForDeletes(PermitMfgReport report,
    							 PermitPeriodEntity permitPeriodEntity,
    							 Set<FormDetailEntity> deleteDetails,
    							 List<Long> deleteComments) {
        List<SubmittedFormEntity> forms = permitPeriodEntity.getSubmittedForms();
        List<SubmittedForm> incomingForms = report.getSubmittedForms();
        Set<Long> formIds = new HashSet<>();
        Map<FormDetailPK, Long> detailIds = new HashMap<>();
        Map<String, FormDetail> detail3852Map = new HashMap<>();
        Map<Long, SubmittedForm> formIdMap = new HashMap<>();
        boolean ismodified3852 = report.isIs3852modified();
        // loop through the incoming forms and take note of each form id we have
        if(incomingForms != null) {
            for(SubmittedForm form: incomingForms) {
                Long formId = form.getFormId();
                logger.debug("INCOMING FORM: formId={}, formType={}, formNum={}", form.getFormId(), form.getFormTypeCd(), form.getFormNum());
                // if formId is null, it should mean a new form creation
                if(formId == null) {
                    Long existingFormId = idLookup(form, forms);
                    if(existingFormId == null) continue; // this is an actual new record
                    else {
                        // however, in this case the user deselected a tobacco type
                        // then reselected it.  we need to keep the form id from the
                        // existing record.
                        form.setFormId(existingFormId);
                        for(FormDetail detail : form.getFormDetails()) {
                            detail.setFormId(existingFormId);
                        }
                        // we will delete the comments since the form has been removed and
                        // then added back.
                        deleteComments.add(existingFormId);
                    }
                }
                formIds.add(formId);
                for(FormDetail detail: form.getFormDetails()) {
                    // occasionally, the form id doesn't come across in the detail.
                    // but we can set it here.
                    if(detail.getFormId() == null) {
                    	detail.setFormId(formId);
                    }
                    logger.debug("    INCOMING DETAIL: formId={}, lineNum={}, filerEntryNum={}, tobaccoClassId={} ",
                            detail.getFormId(), detail.getLineNum(), detail.getFilerEntryNum(), detail.getTobaccoClassId());
                    //from the frontend - linenum starts with index 0
                   // long lineNum = detail.getLineNum() + 1;
                    FormDetailPK pk = new FormDetailPK(detail.getFormId(), detail.getTobaccoClassId(), detail.getLineNum());
                    detailIds.put(pk, form.getFormId());
                    if("3852".equalsIgnoreCase(form.getFormTypeCd())){
                    	String formtobacoIdPk = detail.getFormId()+ "#"+detail.getTobaccoClassId();
                    	detail3852Map.put(formtobacoIdPk, detail);
                    }
                }
                formIdMap.put(form.getFormId(), form);
            }
        }
        if(!ismodified3852){
        	ismodified3852 = check3852FormChange(report, forms, detail3852Map, formIdMap);
        }
        // loop through the existing forms and look for removals of both forms and details
        Iterator<SubmittedFormEntity> iForm = forms.iterator();
        while(iForm.hasNext()) {
            SubmittedFormEntity form = iForm.next();
            logger.debug("EXISTING FORM: formId={}, formType={}, formNum={}", form.getFormId(), form.getFormTypeCd(), form.getFormNum());
            if(form.getFormId() != null) {
                if(!formIds.contains(form.getFormId())) {
                    logger.warn("Form type {} was removed.", form.getFormTypeCd());
                    // add all the details for this form
                    deleteDetails.addAll(form.getFormDetails());
                    // add id for comments delete
                    deleteComments.add(form.getFormId());
                    iForm.remove();
                }
                else {
                    Set<FormDetailEntity> details = form.getFormDetails();
                    Iterator<FormDetailEntity> iDetail = details.iterator();
                    while(iDetail.hasNext()) {
                        FormDetailEntity detail = iDetail.next();
                        FormDetailPK pk = new FormDetailPK(detail.getFormId(), detail.getTobaccoClassId(), detail.getLineNum());
                        if(detailIds.get(pk) == null) {
                            logger.warn("Detail {} was removed from form type {}.", detail.getLineNum(), form.getFormTypeCd());
                            deleteDetails.add(detail);
                            iDetail.remove();
                        }
                    }
                }
            }
        }
        
        return ismodified3852;
    }
    
    public boolean check3852FormChange(PermitMfgReport report, List<SubmittedFormEntity> forms, Map<String, FormDetail> detailMap, Map<Long, SubmittedForm> formIdMap) {

    	List<SubmittedFormEntity> existingformList = forms;
    	List<SubmittedForm> incomingFormList = report.getSubmittedForms();
    	List<Long> processedFormIdList = new ArrayList<>();
    	boolean ismodified3852 = false;
    	if(incomingFormList == null && existingformList != null){
    		ismodified3852 = true;
    		return ismodified3852;
    	}
    	// checking the incoming form against the existing form details
    	for(SubmittedFormEntity existingform: existingformList){
    		logger.debug("EXISTING FORM: formId={}, formType={}, formNum={}", existingform.getFormId(), existingform.getFormTypeCd(), existingform.getFormNum());
    		Long formId = existingform.getFormId();
    		processedFormIdList.add(formId);
    		if(formId != null && "3852".equalsIgnoreCase(existingform.getFormTypeCd()))  {
    			if(formIdMap.keySet().contains(formId)) {
    				//check for changes to the details
    				Set<FormDetailEntity> existingformdetails = existingform.getFormDetails();
    				List<FormDetail> incomingFormDetails = formIdMap.get(formId).getFormDetails();
    				int submittedcount = 0;
    				for(FormDetailEntity formDetailEntity:existingformdetails){
    					String formtobacoIdPk = formDetailEntity.getFormId()+"#"+formDetailEntity.getTobaccoClassId();
    					FormDetail formDetail = detailMap.get(formtobacoIdPk);
    					if(null == formDetail){
    						//detail has been removed
    						return true;
    					}else{
    						//if there is any change in removal qty or taxes paid
    						if(checkifValueModified(formDetailEntity.getRemovalQty(), formDetail.getRemovalQty()) || checkifValueModified(formDetailEntity.getTaxesPaid(), formDetail.getTaxesPaid())){
    							return true;
    						}
    						//this will have details common to incoming forms and submitted forms
    						submittedcount++;
    					}

    				}
    				if(incomingFormDetails.size() != submittedcount){
    					return true;
    				}
    			}
    			else {
    				logger.warn("Form type {} was removed.", existingform.getFormTypeCd());
    				return true;
    			}


    		}
    	}
    	
    	for(SubmittedForm form: incomingFormList){
			if(!processedFormIdList.contains(form.getFormId())&& "3852".equalsIgnoreCase(form.getFormTypeCd())){
				return true;
			}
		}
    	return ismodified3852;
    }

    private boolean checkifValueModified(Double value1, Double value2){
    	boolean retValue = false;
    	if(null == value1 && null == value2){
    		retValue = false;
    	}else if ((null == value1 && null != value2)||((null != value1 && null == value2))){
    		retValue = true;
    	}else if(Double.compare(value1,value2)!=0){
    		retValue = true;
    	}
    	return retValue;
    }
    /**
     * Test new form to see if it's already in the database.
     *
     * @param newForm the new form
     * @param existingForms the existing forms
     * @return the long
     */
    private Long idLookup(SubmittedForm newForm, List<SubmittedFormEntity> existingForms) {
        Long returnVal = null;
        // test the new form to see if it's already in the database
        // "#"+this.permitId+"#"+this.periodId+"#"+this.formTypeCd+ "#" + this.formNum + "#"
        SubmittedFormEntity newFormEntity = new SubmittedFormEntity();
        newFormEntity.setPeriodId(newForm.getPeriodId());
        newFormEntity.setPermitId(newForm.getPermitId());
        newFormEntity.setFormTypeCd(newForm.getFormTypeCd());
        newFormEntity.setFormNum(newForm.getFormNum());
        int index = existingForms.indexOf(newFormEntity);
        if(index > -1) {
            newFormEntity = existingForms.get(index);
            returnVal = newFormEntity.getFormId();
        }
        return returnVal;
    }

    /**
     * Test the incoming save to see if we have a new status, or need to update the old.
     *
     * @param report the report
     * @param permitPeriodEntity the permit period entity
     */
    private void checkPermitPeriodStatus(PermitMfgReport report, PermitPeriodEntity permitPeriodEntity, boolean is3852Modified) {
    	PeriodStatusEntity reportStatus = findReportStatus(permitPeriodEntity);
    	if(reportStatus == null) {
    		/* create a new status, then add it to the list */
            reportStatus = new PeriodStatusEntity();
            reportStatus.setPermitPeriodEntity(permitPeriodEntity);
            reportStatus.setPeriodStatusTypeCd(report.getStatus());
            reportStatus.setStatusDt(new Date());
            reportStatus.setPermitId(permitPeriodEntity.getPermitId());
            reportStatus.setPeriodId(permitPeriodEntity.getPeriodId());
            List<PeriodStatusEntity> statusRecords = findOrCreateStatusList(permitPeriodEntity);
            statusRecords.add(reportStatus);
            /** set recon status to unapproved and clear the reconModifiedDt **/
        	reportStatus.setReconStatusModifiedBy(null);
        	reportStatus.setReconStatusTypeCd("UNAP");
        	reportStatus.setReconStatusModifiedDt(null);
        }
        else {
            /* status record already exists, test to see if it changed */
            if(!reportStatus.getPeriodStatusTypeCd().equals(report.getStatus())) {
                /* status changed, set new status and bump the date */
                reportStatus.setPeriodStatusTypeCd(report.getStatus());
                reportStatus.setStatusDt(new Date());
            }
            if(is3852Modified){
            	/** set recon status to unapproved and clear the reconModifiedDt **/
            	reportStatus.setReconStatusModifiedBy(null);
            	reportStatus.setReconStatusTypeCd("UNAP");
            	reportStatus.setReconStatusModifiedDt(null);
            }
        }
    	
    }

    /**
     * Convert the category codes to their readable values.
     *
     * @param permitPeriod the permit period
     */
    private void convertCodesToValues(PermitPeriod permitPeriod) {
        Map<String, String> periodStatusTypes = referenceCodeEntityManager.getValues("PERIOD_STATUS_TYPE");
        Map<String, String> unitsOfMeasure = referenceCodeEntityManager.getValues("REMOVAL_UOM");
        Map<String, String> activityTypes = referenceCodeEntityManager.getValues("ACTIVITY_TYPE");
        Map<String, String> formTypes = referenceCodeEntityManager.getValues(FORM_TYPE);
        permitPeriod.setStatus(periodStatusTypes.get(permitPeriod.getStatus()));
        List<SubmittedForm> forms = permitPeriod.getSubmittedForms();
        if(forms == null)
        	return;
        for(SubmittedForm form: forms) {
            form.setFormTypeCd(formTypes.get(form.getFormTypeCd()));
            List<FormDetail> details = form.getFormDetails();
            if(details == null)
            	continue;
            for(FormDetail detail : details) {
                detail.setActivityCd(activityTypes.get(detail.getActivityCd()));
                if(detail.getRemovalUom() != null && !detail.getRemovalUom().isEmpty()) {
                    detail.setRemovalUom(unitsOfMeasure.get(detail.getRemovalUom()));
                }
            }
        }
    }

    /**
     * Convert readable values to their category codes.
     *
     * @param report the report
     */
    private void convertValuesToCodes(PermitMfgReport report) {
        Map<String, String> periodStatusKeys = referenceCodeEntityManager.getKeys("PERIOD_STATUS_TYPE");
        Map<String, String> unitOfMeasureKeys = referenceCodeEntityManager.getKeys("REMOVAL_UOM");
        Map<String, String> activityKeys = referenceCodeEntityManager.getKeys("ACTIVITY_TYPE");
        Map<String, String> formTypeKeys = referenceCodeEntityManager.getKeys(FORM_TYPE);
        report.setStatus(periodStatusKeys.get(report.getStatus()));
        List<SubmittedForm> forms = report.getSubmittedForms();
        if(forms == null)
        	return;
        for(SubmittedForm form: forms) {
            form.setFormTypeCd(formTypeKeys.get(form.getFormTypeCd()));
            List<FormDetail> details = form.getFormDetails();
            if(details == null)
            	continue;
            for(FormDetail detail : details) {
                detail.setActivityCd(activityKeys.get(detail.getActivityCd()));
                detail.setCalcTxActCd7501(activityKeys.get(detail.getCalcTxActCd7501()));
                detail.setRemovalActivity(activityKeys.get(detail.getRemovalActivity()));
                if(detail.getRemovalUom() != null && !detail.getRemovalUom().isEmpty()) {
                    detail.setRemovalUom(unitOfMeasureKeys.get(detail.getRemovalUom()));
                }
            }
        }
    }

    /**
     * save/update the permit contacts.
     *
     * @param permit the permit
     * @return the permit
     */
    @Override
    public Permit saveOrUpdatePermitContacts(Permit permit) {
        /*mapping for this is no longer valid as of 11/16/2016 */
        Mapper mapper = mapperWrapper.getMapper();
        PermitEntity permitEntity = new PermitEntity();
        mapper.map(permit, permitEntity);
        /* now save */
        /* retrieve full graph for display */
        return new Permit();
    }

    /**
     * Return all available fiscal years for which permit reports exist.
     *
     * @return the available permit report fiscal years
     */
    @Override
    public Set<String> getAvailablePermitReportFiscalYears() {
        return simpleSQLManager.getAvailablePermitReportFiscalYears();
    }

    /**
     * Return all available calendar years for which permit reports exist.
     *
     * @return the available permit report calendar years
     */
    @Override
    public Set<String> getAvailablePermitReportCalendarYears() {
        return simpleSQLManager.getAvailablePermitReportCalendarYears();
    }

    /**
     * Utility method to find the report status in the list of status records.
     * @param permitPeriodEntity
     * @return
     */
    private PeriodStatusEntity findReportStatus(PermitPeriodEntity permitPeriodEntity) {
    	PeriodStatusEntity periodStatusEntity = null;
    	List<PeriodStatusEntity> periodStatusList = permitPeriodEntity.getPeriodStatusList();
    	if(periodStatusList != null && !periodStatusList.isEmpty()) {
    		for(PeriodStatusEntity status : periodStatusList) {
    			periodStatusEntity = status;
    		}
    	}
    	return periodStatusEntity;
    }

    /**
     * Utility method to get the status list or create a new empty list if null.
     * @param permitPeriodEntity
     * @return
     */
    private List<PeriodStatusEntity> findOrCreateStatusList(PermitPeriodEntity permitPeriodEntity) {
    	List<PeriodStatusEntity> statusRecords = permitPeriodEntity.getPeriodStatusList();
    	if(statusRecords == null) {
    		statusRecords = new ArrayList<>();
    	}
    	return statusRecords;
    }

    /**
     * Save / update comments for permit.
     * @param permitId
     * @param comment
     * @return The persisted comment.
     */
    @Override
    public String updatePermitComments(long permitId, String comment) {
//    	PermitEntity permit = permitEntityManager.getPermit(permitId);
//    	permit.setPermitComments(comment);
//    	permit = permitEntityManager.updatePermit(permit);
//    	return permit.getPermitComments();
    	return "";
    }

    @Override
    public List<TobaccoClass> getLatestTobaccoClasses(){
        Mapper mapper = mapperWrapper.getMapper();
    	List<CurrentTobaccoRateViewEntity> rates = this.referenceCodeEntityManager.getLatestRates();
    	List<TobaccoClass> tobaccoClasses = new ArrayList<TobaccoClass>();
    	for(CurrentTobaccoRateViewEntity rate: rates) {
    		TobaccoClass tobaccoClass = new TobaccoClass();
    		mapper.map(rate, tobaccoClass);
    		tobaccoClasses.add(tobaccoClass);
    	}
    	return tobaccoClasses;
    }

	@Override
	public boolean deleteMonthlyReport(long companyId, long permitId, long periodId) {
		boolean result = permitEntityManager.deleteMonthlyReport(companyId, permitId, periodId);
		return result;
	}

	@Override
	public void saveTobaccoClasses(List<TobaccoClass> rates) {
		/////////////////////////
		int MILLIS_IN_DAY = 1000 * 60 * 60 * 24;
        Mapper mapper = mapperWrapper.getMapper();
		/////////////////////////
		List<TaxRateEntity> taxRates = new ArrayList<TaxRateEntity>();
		List<CurrentTobaccoRateViewEntity> currentRates = this.referenceCodeEntityManager.getLatestRates();
		Map<Long, CurrentTobaccoRateViewEntity> rateMap = new HashMap<Long, CurrentTobaccoRateViewEntity>();
		for(CurrentTobaccoRateViewEntity rate : currentRates) {
			rateMap.put(rate.getTobaccoClassId(), rate);
		}
		for(TobaccoClass rate : rates) {
			// terminate the old rate
			TaxRateEntity oldRate = new TaxRateEntity();
			CurrentTobaccoRateViewEntity currentRate = rateMap.get(rate.getTobaccoClassId());
			mapper.map(currentRate, oldRate);
			oldRate.setTaxRateEndDate(new Date(rate.getTaxRateEffectiveDate().getTime() - MILLIS_IN_DAY));
			// create the new rate
			TaxRateEntity newRate = new TaxRateEntity();
			mapper.map(rate, newRate);
			taxRates.add(oldRate);
			taxRates.add(newRate);
		}
		this.referenceCodeEntityManager.saveTaxRates(taxRates);
	}

    /**
     *This method will be responsible to add comment added for each permit from permit history page.
     */
    @Override
    public PermitHistCommentsBean saveUpdatePermitHistoryComments(PermitHistCommentsBean permitHistComments, long permitId) {
        Mapper mapper = mapperWrapper.getMapper();
        PermitHistComments entity = new PermitHistComments();
        mapper.map(permitHistComments, entity);
        entity.setPermitId(permitId);
        entity = permitEntityManager.saveUpdatePermitHistoryComments(entity, permitId);
        mapper.map(entity, permitHistComments);
        return permitHistComments;
    }

    /**
     *This method will return all the comments for requested Permit
     */
    @Override
    public PermitHistoryCommentResponse getAllCommentsForPermit(long permitId) {
        List<PermitHistComments> commentsList = permitEntityManager.getAllCommentsForPermit(permitId);
        Mapper mapper = mapperWrapper.getMapper();
        PermitHistoryCommentResponse response = new PermitHistoryCommentResponse();
        List<PermitHistCommentsBean> responseCommentsList = new ArrayList<PermitHistCommentsBean>();
        for(PermitHistComments comment : commentsList) {
            PermitHistCommentsBean bean = new PermitHistCommentsBean();
            mapper.map(comment, bean);
            responseCommentsList.add(bean);
        }
        response.setCommentsList(responseCommentsList);
        return response;
    }

    @Override
    public PermitHistCommentsBean updatePermitHistoryComment(long permitCommentId, PermitHistCommentsBean permitHistCommentBean) {
       
//        PermitHistComments entity = permitEntityManager.getPermitHistoryComment(permitCommentId);
//        if(null != entity) {
//            entity.setUserComments(permitHistCommentBean.getUserComments());
//        }
        PermitHistComments entity = permitEntityManager.updatePermitHistoeyComment(permitHistCommentBean.getUserComment(), permitCommentId);
        Mapper mapper = mapperWrapper.getMapper();
        PermitHistCommentsBean bean = new PermitHistCommentsBean();
        mapper.map(entity, bean);
        return bean;
    }
    
    @Override
    public void deleteTTBAdjustmentById(long formId, long tobaccoClassId) { 
        permitPeriodEntityManager.deleteTTBAdjustmentById(formId, tobaccoClassId);
    }
    @Override
    public TTBAdjustment saveTTBAdjustments (long permitId, long periodId,TTBAdjustment ttbAdjustment) {
    	Mapper mapper = mapperWrapper.getMapper();
        TTBAdjustmentDetailEntity entity = null;
    	List<TTBAdjustmentDetail> ttbAdjustmentDetailList = new ArrayList<>();
        List<TTBAdjustmentDetailEntity> ttbAdjustmentDetailEntityList = new ArrayList<>();
    	SubmittedFormEntity form = permitPeriodEntityManager.getOrCreateSubmittedForm(permitId, periodId, 0, referenceCodeEntityManager.getCode(FORM_TYPE, "5000.24"));
    	long formId = form.getFormId();
    	permitPeriodEntityManager.deleteTTBAdjustmentById(formId, ttbAdjustment.getTobaccoClassId());
        for(TTBAdjustmentDetail ttbAdjustmentDetail : ttbAdjustment.getTtbAdjustmentDetail()) {
        	entity = new TTBAdjustmentDetailEntity();
            mapper.map(ttbAdjustmentDetail, entity);
            entity.setAdjFormId(null);
            entity.setFormId(formId);
            ttbAdjustmentDetailEntityList.add(entity);
        }
        permitPeriodEntityManager.saveTTBAdjustments(ttbAdjustmentDetailEntityList);
        return getSavedTTBAdjustmentDetails(formId, ttbAdjustment.getTobaccoClassId());
        
    }
    
    public TTBAdjustment getSavedTTBAdjustmentDetails(long formId, long tobaccoClassId) {
    	Mapper mapper = mapperWrapper.getMapper();
    	// Need to be able to have null values for A and B if no data is provided
    	Double adjATaxAmt = null;
    	Double adjBTaxAmt = null;
    	Double differenceOfAB = null;
    	TTBAdjustment retTTBAdjustment =  new TTBAdjustment();
    	List<TTBAdjustmentDetail> ttbAdjustmentDetails = new ArrayList<>();
    	List<TTBAdjustmentDetailEntity> ttbAdjustmentDetailEntities = permitPeriodEntityManager.getTTBAdjustmentDetail(formId, tobaccoClassId);


    	TTBAdjustmentDetailEntity ttbAdjustmentDetailEntity = ttbAdjustmentDetailEntities.get(0)!=null ?
    																		ttbAdjustmentDetailEntities.get(0): new TTBAdjustmentDetailEntity();
    	retTTBAdjustment.setFormId(ttbAdjustmentDetailEntity.getFormId());
    	retTTBAdjustment.setTobaccoClassId(ttbAdjustmentDetailEntity.getTobaccoClassId());
    	
    	for(TTBAdjustmentDetailEntity ttbAdjustmentDetailEntityItr : ttbAdjustmentDetailEntities) {
    		if("A".equalsIgnoreCase(ttbAdjustmentDetailEntityItr.getAdjType()) && ttbAdjustmentDetailEntityItr.getTaxAmount() != null) {
    			adjATaxAmt = (adjATaxAmt != null ? adjATaxAmt : 0.0) + ttbAdjustmentDetailEntityItr.getTaxAmount();
    		} else if("B".equalsIgnoreCase(ttbAdjustmentDetailEntityItr.getAdjType()) && ttbAdjustmentDetailEntityItr.getTaxAmount() != null) {
    			adjBTaxAmt = (adjBTaxAmt != null ? adjBTaxAmt : 0.0) + ttbAdjustmentDetailEntityItr.getTaxAmount();
    		}

    		TTBAdjustmentDetail ttbAdjDetail = new TTBAdjustmentDetail();
    		mapper.map(ttbAdjustmentDetailEntityItr, ttbAdjDetail);
    		ttbAdjustmentDetails.add(ttbAdjDetail);
    	}
    	
    	// Need to be able to have null values for total
    	if (adjATaxAmt != null && adjBTaxAmt != null) {
    		differenceOfAB = adjATaxAmt - adjBTaxAmt;
    	} else if (adjBTaxAmt != null) {
    		differenceOfAB = -adjBTaxAmt;
    	} else {
    		differenceOfAB = adjATaxAmt;
    	}
    	retTTBAdjustment.setTaxAmountAdjusmentA(adjATaxAmt);
    	retTTBAdjustment.setTaxAmountAdjusmentB(adjBTaxAmt);
    	retTTBAdjustment.setTaxAmountTotalAdjustment(differenceOfAB);
    	retTTBAdjustment.setTtbAdjustmentDetail(ttbAdjustmentDetails);

    	//to set the last iteration

    	return retTTBAdjustment;

    }
    
}

package gov.hhs.fda.ctp.biz.permitmgmt;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitAudit;
import gov.hhs.fda.ctp.common.beans.PermitAuditsExport;
import gov.hhs.fda.ctp.common.beans.PermitUpdateUpload;
import gov.hhs.fda.ctp.common.beans.PermitUpdatesDocument;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateEntity;


public interface PermitMgmtBizComp {
	
	public void ingestPermitUpdates(IngestionOutput output, MultipartFile file, String aliasFileNm) throws IOException;
	
	public PermitAuditsExport exportPermitUpdates();

	public List<PermitUpdatesDocument> getPermitUpdateFiles();
	
	public List<PermitUpdateUpload> getUpdatedPermits(Long docId);
	
	public List<PermitUpdateUpload> saveUpdatedPermits(PermitUpdateEntity updatedPermit);
	
	public List<PermitUpdateUpload> saveUpdatedContacts(PermitUpdateEntity updatedPermit);
 
	public List<PermitUpdateUpload> setIgnrContactStatusandDelete();
	
	public List<PermitUpdateUpload> setIgnrAddressStatusandDelete();


	public PermitUpdatesDocument getPermitUpdatesDocument(Long docId);

	public void updateDocumentName(Long docId, String fileName);
	
	public List<PermitAudit> getPermitUpdatesByDocId(Long docId);

	public PermitAudit insertPermitAudit(PermitAudit pAudit);

	public List<PermitUpdateUpload> getUpdatedPermitsContacts(Long docId);
	
	public List<Address> getIngestedAddress();

	public List<Contact> getIngestedContacts();

	public Company addAndClose(List<Permit> permits, long companyId);

	public void cleanupOutstandingNotStartedReports(Permit permit, long companyId);

	public List<PermitAudit> getTTBPermitListExclEntries(Long docId);

	public PermitAudit resolveTTBPermitExclusion(PermitAudit permitAudit);
}

package gov.hhs.fda.ctp.biz.permitmgmt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizCompImpl;
import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.DocUpload;
import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitAudit;
import gov.hhs.fda.ctp.common.beans.PermitAuditsExport;
import gov.hhs.fda.ctp.common.beans.PermitUpdateUpload;
import gov.hhs.fda.ctp.common.beans.PermitUpdatesDocument;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.CsvStringBuilder;
import gov.hhs.fda.ctp.persistence.PermitEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.model.PermitAuditEntity;
import gov.hhs.fda.ctp.persistence.model.PermitIgnoreEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateEntity;

@Service("permitMgmtBizComp")
@Transactional
public class PermitMgmtBizCompImpl implements PermitMgmtBizComp {

	@Autowired
	private MapperWrapper mapperWrapper;

	@Autowired
	private PermitEntityManager permitEntityManager;

	@Autowired
	private SimpleSQLManager simpleSQLManager;

	@Autowired
	private CompanyMgmtBizComp companyMgmtBizComp;

	@Autowired
	private SecurityContextUtil securityContextUtil;

	@Override
	public void ingestPermitUpdates(IngestionOutput output, MultipartFile file, String aliasFileNm) throws IOException {

		List<DocUpload> ingestion = output.getOutput();
		Mapper mapper = mapperWrapper.getMapper();
		List<PermitUpdateEntity> permitUpdatesList = new ArrayList<>();

		byte[] fileBytes = file.getBytes();
		PermitUpdateDocumentEntity doc = new PermitUpdateDocumentEntity();
		doc.setDocument(fileBytes);
		String fileNm = file.getOriginalFilename();
		doc.setFileNm(fileNm);
		doc.setCreatedBy(securityContextUtil.getPrincipal().getUsername());
		doc.setAliasFileNm(aliasFileNm);
		List<String> permitLists = permitEntityManager.getallPermits();
		for (DocUpload record : ingestion) {
			PermitUpdateUpload pUpdateBean = (PermitUpdateUpload) record;
		    PermitUpdateEntity pUpdateEntity = new PermitUpdateEntity();
            mapper.map(pUpdateBean, pUpdateEntity);
            pUpdateEntity.setDocument(doc);
            pUpdateEntity.setCreatedBy(securityContextUtil.getPrincipal().getUsername());
            permitUpdatesList.add(pUpdateEntity);
		}

		doc.setPermits(permitUpdatesList);
		if (permitUpdatesList.size() > 0) {
			this.permitEntityManager.stagePermitUpdatesList(doc);
		}
	}

	@Override
	public PermitAuditsExport exportPermitUpdates() {

		List<PermitAuditEntity> permitAuditList = this.permitEntityManager.getPermitMgmtAuditList();

		StringBuilder filename = new StringBuilder("PERMIT_UPDATES");
		filename.append(".csv");

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine(
				"COMPANY NAME, EIN, PERMIT NUMBER, PERMIT STATUS,ERROR,CLOSE DATE, ISSUE DATE, MODIFIED DATE");

		for (PermitAuditEntity pAudit : permitAuditList) {
			csv.csvAppendInQuotes(pAudit.getCompanyName());
			csv.csvAppend(pAudit.getEinNum());
			csv.csvAppend(pAudit.getPermitNum());
			csv.csvAppend(pAudit.getPermitAction());
			csv.csvAppendInQuotes(pAudit.getErrors());
			csv.csvAppend(pAudit.getIssueDt());
			csv.csvAppendNewLine(pAudit.getActionDt());

		}

		PermitAuditsExport export = new PermitAuditsExport();
		export.setFilename(filename.toString());
		export.setCsv(csv.toString());
		return export;
	}

	@Override
	public List<PermitUpdatesDocument> getPermitUpdateFiles() {
		return this.simpleSQLManager.getPermitUpdateFiles();
	}

	@Override
	public PermitUpdatesDocument getPermitUpdatesDocument(Long docId) {

		PermitUpdateDocumentEntity pEntityDoc = this.permitEntityManager.getPermitMgmtDocument(docId);
		Mapper mapper = mapperWrapper.getMapper();
		PermitUpdatesDocument pDoc = new PermitUpdatesDocument();
		mapper.map(pEntityDoc, pDoc);

		return pDoc;
	}

	@Override
	public List<PermitAudit> getPermitUpdatesByDocId(Long docId) {

		return this.simpleSQLManager.getPermitMgmtAuditListByDocId(docId);

	}

	@Override
	public void updateDocumentName(Long docId, String fileName) {
		this.permitEntityManager.updatePermitMgmtDocumentFileName(docId, fileName);
	}

	public PermitEntityManager getPermitEntityManager() {
		return permitEntityManager;
	}

	public void setPermitEntityManager(PermitEntityManager permitEntityManager) {
		this.permitEntityManager = permitEntityManager;
	}

	public SimpleSQLManager getSimpleSQLManager() {
		return simpleSQLManager;
	}

	public void setSimpleSQLManager(SimpleSQLManager simpleSQLManager) {
		this.simpleSQLManager = simpleSQLManager;
	}

	@Override
	public List<PermitUpdateUpload> getUpdatedPermits(Long docId) {
		return this.simpleSQLManager.getUpdatedPermits(docId);
	}

	@Override
	public List<PermitUpdateUpload> getUpdatedPermitsContacts(Long docId) {
		
		return this.simpleSQLManager.getUpdatedPermitsContacts(docId);
	}

	@Override
	public List<Address> getIngestedAddress() {
		return this.simpleSQLManager.getIngestedAddress();
	}

	@Override
	public List<Contact> getIngestedContacts() {
		return this.simpleSQLManager.getIngestedContacts();
	}

	@Override
	public List<PermitUpdateUpload> saveUpdatedPermits(PermitUpdateEntity updatedPermit) {
		
		if (updatedPermit.getAddressStatus().equalsIgnoreCase("IGNR")) {
			PermitIgnoreEntity permitIgnore = new PermitIgnoreEntity();
			permitIgnore.setPermitUpdateId(updatedPermit.getPermitUpdateId());
			permitIgnore.setPermitNum(updatedPermit.getPermitNum());
			permitIgnore.setPermitType(updatedPermit.getPermitType());
			permitIgnore.setDocStageId(updatedPermit.getDocStageId());
			permitIgnore.setAddressStatus("IGNR");
			permitIgnore.setEinNum(updatedPermit.getEinNum());
			permitIgnore.setMailingCity(updatedPermit.getMailingCity());
			permitIgnore.setMailingStreet(updatedPermit.getMailingStreet());
			permitIgnore.setMailingState(updatedPermit.getMailingState());
			permitIgnore.setMailingPostalCd(updatedPermit.getMailingPostalCd());					
			//permitIgnore.setEmail(updatedPermit.getEmail());
			//permitIgnore.setPremisePhone(updatedPermit.getPremisePhone());
			permitIgnore.setBusinessName(updatedPermit.getBusinessName());
			PermitIgnoreEntity permitignoreEntity = permitEntityManager.insertIgnoreStatus(permitIgnore);

		}


		return this.simpleSQLManager.saveUpdatedPermits(updatedPermit);
	}

	@Override
	public PermitAudit insertPermitAudit(PermitAudit pAudit) {
		Mapper mapper = mapperWrapper.getMapper();
		PermitAuditEntity pAuditEntity = new PermitAuditEntity();
		mapper.map(pAudit, pAuditEntity);

		pAuditEntity.setActionDt(new Date());
		pAuditEntity.setCreatedDt(new Date());
		PermitAuditEntity permitAuditEntity = permitEntityManager.insertPermitMgmtAudit(pAuditEntity);

		PermitAudit permitAudit = new PermitAudit();

		mapper.map(permitAuditEntity, permitAudit);

		return permitAudit;

	}

	@Override
	public List<PermitUpdateUpload> saveUpdatedContacts(PermitUpdateEntity updatedPermit) {

		if (updatedPermit.getContactStatus().equalsIgnoreCase("IGNR")) {
			PermitIgnoreEntity permitIgnore = new PermitIgnoreEntity();
			permitIgnore.setPermitUpdateId(updatedPermit.getPermitUpdateId());
			permitIgnore.setPermitNum(updatedPermit.getPermitNum());
			permitIgnore.setPermitType(updatedPermit.getPermitType());
			permitIgnore.setDocStageId(updatedPermit.getDocStageId());
			permitIgnore.setContactStatus("IGNR");
			permitIgnore.setEinNum(updatedPermit.getEinNum());
			permitIgnore.setEmail(updatedPermit.getEmail());
			permitIgnore.setPremisePhone(updatedPermit.getPremisePhone());
			permitIgnore.setBusinessName(updatedPermit.getBusinessName());
			PermitIgnoreEntity permitignoreEntity = permitEntityManager.insertIgnoreStatus(permitIgnore);

		}

		return this.simpleSQLManager.saveUpdatedContacts(updatedPermit);
	}

	
	@Override
	public List<PermitUpdateUpload> setIgnrContactStatusandDelete(){
		
		
		List<PermitUpdateUpload> beanList = new ArrayList<>();

		List<PermitUpdateEntity> pEntity = this.simpleSQLManager.setIgnrContactStatusandDelete();	
		
		if (pEntity != null) {
			Mapper mapper = mapperWrapper.getMapper();
			for (PermitUpdateEntity entry : pEntity) {
				PermitUpdateUpload pDoc = new PermitUpdateUpload();
				mapper.map(entry, pDoc);
				beanList.add(pDoc);
			}
		}
		
		return beanList;
	}
	
	
	@Override
	public List<PermitUpdateUpload> setIgnrAddressStatusandDelete(){
		
		
		List<PermitUpdateUpload> beanList = new ArrayList<>();

		List<PermitUpdateEntity> pEntity = this.simpleSQLManager.setIgnrAddrStatusandDelete();	
		
		if (pEntity != null) {
			Mapper mapper = mapperWrapper.getMapper();
			for (PermitUpdateEntity entry : pEntity) {
				PermitUpdateUpload pDoc = new PermitUpdateUpload();
				mapper.map(entry, pDoc);
				beanList.add(pDoc);
			}
		}
		
		return beanList;
	}

	@Override
	public Company addAndClose(List<Permit> permits, long companyId) {
		Permit newPermit = null;
		Permit closedPermit = null;
		
		for (Permit permit : permits) {
			if("Add".equalsIgnoreCase(permit.getPermitAction())){
				newPermit = permit;
			}
			else if ("Update".equalsIgnoreCase(permit.getPermitAction())){
				closedPermit = permit;
			}
		}
		
		if(newPermit == null || closedPermit == null){
			throw new TufaException("Invalid number of permits.");
		}
		
		String latestDate = this.simpleSQLManager.getDateOfLatestStartedreport(closedPermit.getPermitId());
		
		if(latestDate != null) {
			String monthAfter = CTPUtil.getNextMonthDate(latestDate);
			newPermit.setIssueDt(monthAfter);
			closedPermit.setttbendDt(newPermit.getttbstartDt());
			closedPermit.setCloseDt(latestDate);
		} else {
			String monthBefore = CTPUtil.getPrevMonthDate(newPermit.getttbstartDt());
			if(CTPUtil.compareDateStrings(closedPermit.getIssueDt(), monthBefore) > 0){
				throw new TufaException(CTPUtil.formatPermitNumWithDashes(closedPermit.getPermitNum()) + " cannot have an Inactive Date " + monthBefore + " before Active Date " + closedPermit.getIssueDt());
			}
			newPermit.setIssueDt(CTPUtil.getNextMonthDate(monthBefore));
			closedPermit.setttbendDt(newPermit.getttbstartDt());
			
			String dateMinusOneDay = CTPUtil.addDaysToDate(newPermit.getttbstartDt(), -1);
			closedPermit.setCloseDt(dateMinusOneDay);
		}
		
		closedPermit.getPermitAudit().setPermitStatusChangeAction("TTB Permit " + CTPUtil.formatPermitNumWithDashes(newPermit.getPermitNum()) + " Added. "
				+ " Permit " + CTPUtil.formatPermitNumWithDashes(closedPermit.getPermitNum()) + " closed with a Close Date of " + closedPermit.getCloseDt());
		
		//See PermitMgmtAuditUpdateAspect
		Company company = this.companyMgmtBizComp.createPermit(newPermit);
		company = this.companyMgmtBizComp.updatePermit(closedPermit);
		//Delete Monthly Reports outside the new closed date
		this.cleanupOutstandingNotStartedReports(closedPermit, companyId);
		
		return company;
	}
	
	/**
	 * This method deletes any reports for a specific comapny and permit combination that are out side the 
	 * permits Inactive Date.
	 * 
	 * @throws TufaException if there is an outstanding started report (ERROR/COMPLETE)
	 * */
	@Override
	public void cleanupOutstandingNotStartedReports(Permit permit, long companyId){
		String inactiveDate = permit.getCloseDt();
		List<Long> periodIds = this.simpleSQLManager.getPeriodIdsAfterDate(permit.getPermitId(), inactiveDate);
		
		for(Long periodId : periodIds){
			this.permitEntityManager.deleteMonthlyReport(companyId, permit.getPermitId(), periodId);
		}
	}
	
	@Override
    public List<PermitAudit> getTTBPermitListExclEntries(Long docId){
		return this.permitEntityManager.getTTBPermitListExclEntries(docId);
	}
	
	@Override
    public PermitAudit resolveTTBPermitExclusion(PermitAudit permitAudit){
        Mapper mapper = mapperWrapper.getMapper();
        PermitAuditEntity pAuditEntity = new PermitAuditEntity();
        mapper.map(permitAudit,pAuditEntity);
        
        if(permitAudit.getPermitExclResolved()==true)
        	pAuditEntity.setPermitExclResolved("Y");
        else
        	pAuditEntity.setPermitExclResolved("N");
        
        pAuditEntity = permitEntityManager.resolveTTBPermitExclusion(pAuditEntity);
        mapper.map(pAuditEntity,permitAudit);
    	return permitAudit;
    }
}

package gov.hhs.fda.ctp.biz.permitperiod;

import java.util.List;

import gov.hhs.fda.ctp.common.beans.DocExport;
import gov.hhs.fda.ctp.common.beans.PeriodStatus;
import gov.hhs.fda.ctp.common.beans.UnitConverter;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.MissingForms;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;

/**
 * The Interface PermitPeriodBizComp.
 */
public interface PermitPeriodBizComp {
	   
    /**
     * Update permit period.
     *
     * @param permitperiod the permitperiod
     * @param attachs the attachs
     * @return the permit period entity
     */
    public PermitPeriodEntity updatePermitPeriod(PermitPeriodEntity permitperiod, List<AttachmentsEntity> attachs);
    
    /**
     * Gets the permit period by ids.
     *
     * @param periodId the period id
     * @param permitId the permit id
     * @return the permit period by ids
     */
    public PermitPeriodEntity getPermitPeriodByIds(long periodId, long permitId);
    
    /**
     * Delete by ids.
     *
     * @param periodId the period id
     * @param permitId the permit id
     * @return the permit period entity
     */
    public PermitPeriodEntity deleteByIds(long periodId, long permitId);
    
    /**
     * Creates the rpt period.
     *
     * @param monthYear the month year
     * @return the string
     */
    public String createRptPeriod(String monthYear);
    

    public PeriodStatus updatePeriodReconStatus(long periodId,long permitId);
    
    public PeriodStatus getPeriodReconStatus(long periodId,long permitId);
    
	public List<MissingForms> getMissingForms(long permitId, long periodId);
	
	public void saveMissingForms(List<MissingForms> missingForms, long periodId, long permitId);

	DocExport getMonthlyReportExport(int permitId, int periodId, String form, int tobaccoClass);

	public UnitConverter getReferenceValue(String type, String code);
    
}

package gov.hhs.fda.ctp.biz.permitperiod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.DocExport;
import gov.hhs.fda.ctp.common.beans.PeriodStatus;
import gov.hhs.fda.ctp.common.beans.UnitConverter;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.common.util.CsvStringBuilder;
import gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager;
import gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.Export2000FormEntity;
import gov.hhs.fda.ctp.persistence.model.Export3852FormEntity;
import gov.hhs.fda.ctp.persistence.model.Export5000FormEntity;
import gov.hhs.fda.ctp.persistence.model.Export7501FormBodyEntity;
import gov.hhs.fda.ctp.persistence.model.Export7501FormHeaderEntity;
import gov.hhs.fda.ctp.persistence.model.MissingForms;
import gov.hhs.fda.ctp.persistence.model.PeriodStatusEntity;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;
import gov.hhs.fda.ctp.persistence.model.ReferenceValue;

/**
 * The Class PermitPeriodBizCompImpl.
 */
@Service
@Transactional
public class PermitPeriodBizCompImpl implements PermitPeriodBizComp {


	
	private ReferenceCodeEntityManager referenceCodeEntityManager;
	
	/**
	 * Sets the reference code entity manager.
	 *
	 * @param referenceCodeEntityManager the new reference code entity manager
	 */
	@Autowired
	public void setReferenceCodeEntityManager(ReferenceCodeEntityManager referenceCodeEntityManager) {
		this.referenceCodeEntityManager = referenceCodeEntityManager;
	}
	
	/** The permit period entity manager. */
	private PermitPeriodEntityManager permitPeriodEntityManager;

    /** The simple SQL manager. */
    private SimpleSQLManager simpleSQLManager;

    @Autowired
	private MapperWrapper mapperWrapper;

    @Autowired
	private SecurityContextUtil securityContextUtil;
    
    /**
     * Sets the permit period entity manager.
     *
     * @param pmtPeriodEntityManager the new permit period entity manager
     */
    @Autowired
    public void setPermitPeriodEntityManager(PermitPeriodEntityManager pmtPeriodEntityManager) {
        this.permitPeriodEntityManager = pmtPeriodEntityManager;
    }

	/**
	 * @param simpleSQLManager the simpleSQLManager to set
	 */
    @Autowired
	public void setSimpleSQLManager(SimpleSQLManager simpleSQLManager) {
		this.simpleSQLManager = simpleSQLManager;
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.permitperiod.PermitPeriodBizComp#updatePermitPeriod(gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity, java.util.List)
	 */
	@Override
	public PermitPeriodEntity updatePermitPeriod(PermitPeriodEntity permitperiod, List<AttachmentsEntity> attachs) {
		return permitPeriodEntityManager.updatePermitPeriod(permitperiod, attachs);
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.permitperiod.PermitPeriodBizComp#getPermitPeriodByIds(long, long)
	 */
	@Override
	public PermitPeriodEntity getPermitPeriodByIds(long periodId, long permitId) {
		return permitPeriodEntityManager.getPermitPeriodByIds(periodId,permitId);
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.permitperiod.PermitPeriodBizComp#deleteByIds(long, long)
	 */
	@Override
	public PermitPeriodEntity deleteByIds(long periodId, long permitId){	
		return permitPeriodEntityManager.deleteByIds(periodId, permitId);
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.permitperiod.PermitPeriodBizComp#createRptPeriod(java.lang.String)
	 */
	@Override
	public String createRptPeriod(String monthYear) {
		return this.permitPeriodEntityManager.createRptPeriod(monthYear);
	}
	
	
	@Override
	public PeriodStatus updatePeriodReconStatus(long periodId,long permitId) {
		 PeriodStatusEntity periodStatusEntity = permitPeriodEntityManager.updatePeriodReconStatus(periodId, permitId);
		 return getPeriodStatusDTO(periodStatusEntity);
	}
	
	public PeriodStatus getPeriodReconStatus(long periodId,long permitId){
		 PeriodStatusEntity periodStatusEntity = permitPeriodEntityManager.getPeriodReconStatus(periodId, permitId);
		 return getPeriodStatusDTO(periodStatusEntity);
	}
	
	private PeriodStatus getPeriodStatusDTO(PeriodStatusEntity periodStatusEntity){
		 
		 Map<String,String> pStatusMap  = this.referenceCodeEntityManager.getValues("RECON_STATUS_TYPE");
	     
	     PeriodStatus pStatus = new PeriodStatus();
	     
	     Mapper mapper = mapperWrapper.getMapper();
	     mapper.map(periodStatusEntity, pStatus );
	     
	     String status = pStatusMap.get(pStatus.getReconStatusTypeCd());
	     pStatus.setReconStatusTypeCd(status);
	     // we should resolve this by tying every user to a database account
	     // and letting the triggers populate modified by
	     try {
	    	 pStatus.setReconStatusModifiedBy(securityContextUtil.getPrincipal().getUsername());
	     } 
	     catch(NullPointerException ex) {
	    	 pStatus.setReconStatusModifiedBy("UNIT TEST");
	     }
	     
	     return pStatus;
	}

//	Request to pull conversion rates from database
	
	@Override
	public UnitConverter getReferenceValue(String type, String code){
		UnitConverter uc = new UnitConverter();
		String referenceValue = referenceCodeEntityManager.getValue(type, code);
		
		if(null != referenceValue){
			Double coversionValue = Double.parseDouble(referenceValue);
			uc.setKgstopoundConversion(coversionValue);
		}
		
		return uc; 
	}
	
	@Override
	public List<MissingForms> getMissingForms(long permitId, long periodId) {
		return this.permitPeriodEntityManager.getMissingForms(permitId,periodId);
	}

	@Override
	public void saveMissingForms(List<MissingForms> missingForms, long periodId, long permitId) {
		List<MissingForms> eforms = getMissingForms(permitId,periodId);
		Set<MissingForms> existingformSet = eforms != null ? new HashSet<MissingForms>(eforms): new HashSet<MissingForms>();
		Set<MissingForms> newFormSet = new HashSet<MissingForms>(missingForms);
		
		for(MissingForms f: existingformSet){
			if(!newFormSet.contains(f)){
				this.permitPeriodEntityManager.deleteMissingForms(f);
			}
		}
		for(MissingForms form: missingForms){
			if(!existingformSet.contains(form) || !existingformSet.isEmpty()){
				this.permitPeriodEntityManager.saveMissingForms(form);
			}
		}
		
	}

	@Override
	public DocExport getMonthlyReportExport(int permitId, int periodId, String form, int tobaccoClass) {
		DocExport export = new DocExport();
		switch(form) {
		case "3852":
			export = this.get3852FormExport(permitId, periodId);
			break;
		case "5220-6":
			export = this.get2206FormExport(permitId, periodId);
			break;
		case "5210-5":
			export = this.get2105FormExport(permitId, periodId);
			break;
		case "7501":
			export = this.get7501FormExport(permitId, periodId, tobaccoClass);
			break;
		case "5000":
			export = this.get5000FormExport(permitId, periodId);
			break;
		}
		
		return export;
	}
	
	private DocExport get3852FormExport(long permitId, long periodId) {
		CsvStringBuilder csvBuilder = new CsvStringBuilder();
        Map<String, String> activityKeys = referenceCodeEntityManager.getValues("ACTIVITY_TYPE");
		List<Export3852FormEntity> results = this.permitPeriodEntityManager.getExport3852Form(permitId, periodId);
		
		Collections.sort(results, new Comparator<Export3852FormEntity>() {

			private List<String> sorted = new ArrayList<String>(Arrays.asList("Cigarettes","Cigars","Snuff","Chew","Pipe","Roll-Your-Own"));

			@Override
			public int compare(Export3852FormEntity o1, Export3852FormEntity o2) {
				return this.sorted.indexOf(o1.getTobaccoClassNm()) - this.sorted.indexOf(o2.getTobaccoClassNm());
			}
			
		});
		
		csvBuilder.csvAppendNewLine("Tobacco Class,Removals Amount,Tax Rate,Excise Tax,Difference Tax,Calculated Tax Rate,Activity");
		for(Export3852FormEntity result : results) {
			csvBuilder.csvAppend(result.getTobaccoClassNm());
			if("Cigarettes".equals(result.getTobaccoClassNm())|| "Cigars".equals(result.getTobaccoClassNm())) {
				csvBuilder.csvAppendRaw("\"");
				csvBuilder.csvAppendFormatRaw("###,##0",result.getRemovalQty());
				csvBuilder.csvAppendRaw("\",");
			} else {
				csvBuilder.csvAppendRaw("\"");
				csvBuilder.csvAppendFormatRaw("###,##0.000",result.getRemovalQty());
				csvBuilder.csvAppendRaw("\",");
			}
			csvBuilder.csvAppend(result.getTaxRate());
			csvBuilder.csvAppendRaw("\"");
			csvBuilder.csvAppendFormatRaw("$###,###,##0.00",result.getTaxesPaid());
			csvBuilder.csvAppendRaw("\",\"");
			csvBuilder.csvAppendFormatRaw("$###,###,##0.00",result.getDolDifference());
			csvBuilder.csvAppendRaw("\",");
			csvBuilder.csvAppend(result.getCalcTaxRate());
			csvBuilder.csvAppendNewLine(activityKeys.get(result.getActivityCd()));
		}
		String fileContents = csvBuilder.toString();
		
		//Create export
		DocExport doc = new DocExport();
		doc.setCsv(fileContents);
		
		return doc;
	}
	
	private DocExport get2206FormExport(long permitId, long periodId) {
		CsvStringBuilder csvBuilder = new CsvStringBuilder();
        Map<String, String> activityKeys = referenceCodeEntityManager.getValues("ACTIVITY_TYPE");
		List<Export2000FormEntity> results = this.permitPeriodEntityManager.getExport2000Form(permitId, periodId);

		Collections.sort(results, new Comparator<Export2000FormEntity>() {

			private List<String> sorted = new ArrayList<String>(Arrays.asList("Cigarettes","Cigars","Snuff","Chew","Pipe","Roll-Your-Own"));

			@Override
			public int compare(Export2000FormEntity o1, Export2000FormEntity o2) {
				return this.sorted.indexOf(o1.getTobaccoClassNm()) - this.sorted.indexOf(o2.getTobaccoClassNm());
			}
			
		});
		
		csvBuilder.csvAppendNewLine("Tobacco Class,Removals Amount,Difference Removals,Activity");
		for(Export2000FormEntity result : results) {
			csvBuilder.csvAppend(result.getTobaccoClassNm());
			if("Cigarettes".equals(result.getTobaccoClassNm())|| "Cigars".equals(result.getTobaccoClassNm())) {
				csvBuilder.csvAppendRaw("\"");
				csvBuilder.csvAppendRaw("Small - ");
				csvBuilder.csvAppendFormatRaw("###,##0",result.getSmallRemovalQty());
				csvBuilder.csvAppendRaw("; Large - ");
				csvBuilder.csvAppendFormatRaw("###,##0",result.getLargeRemovalQty());
				csvBuilder.csvAppendRaw("; Total - ");
				csvBuilder.csvAppendFormatRaw("###,##0",result.getRemovalQty());
				csvBuilder.csvAppend("\"");
				csvBuilder.csvAppendFormat("###,##0",result.getDolDifference());
			} else {
				csvBuilder.csvAppendRaw("\"");
				csvBuilder.csvAppendFormatRaw("###,##0.000",result.getRemovalQty());
				csvBuilder.csvAppendRaw("\",\"");
				csvBuilder.csvAppendFormatRaw("###,##0.000",result.getDolDifference());
				csvBuilder.csvAppendRaw("\",");
			}
			csvBuilder.csvAppendNewLine(activityKeys.get(result.getActivityCd()));
		}
		String fileContents = csvBuilder.toString();
		
		//Create export
		DocExport doc = new DocExport();
		doc.setCsv(fileContents);
		
		return doc;
	}
	

	private DocExport get2105FormExport(long permitId, long periodId) {
		CsvStringBuilder csvBuilder = new CsvStringBuilder();
        Map<String, String> activityKeys = referenceCodeEntityManager.getValues("ACTIVITY_TYPE");
		List<Export2000FormEntity> results = this.permitPeriodEntityManager.getExport2000Form(permitId, periodId);
		
		Collections.sort(results, new Comparator<Export2000FormEntity>() {

			private List<String> sorted = new ArrayList<String>(Arrays.asList("Cigars","Cigarettes","Chew","Snuff","Pipe","Roll-Your-Own"));

			@Override
			public int compare(Export2000FormEntity o1, Export2000FormEntity o2) {
				return this.sorted.indexOf(o1.getTobaccoClassNm()) - this.sorted.indexOf(o2.getTobaccoClassNm());
			}
			
		});
		
		csvBuilder.csvAppendNewLine("Tobacco Class,Removals Amount,Line 17 Adjustments,Grand Total Removals,Difference Removals,Activity");
		for(Export2000FormEntity result : results) {
			// Tobacco Class Column
			csvBuilder.csvAppend(result.getTobaccoClassNm());
			if("Cigarettes".equals(result.getTobaccoClassNm())|| "Cigars".equals(result.getTobaccoClassNm())) {
				// Removals Column
				csvBuilder.csvAppendRaw("\"");
				csvBuilder.csvAppendRaw("Large - ");
				csvBuilder.csvAppendFormatRaw("###,##0",result.getLargeRemovalQty());
				csvBuilder.csvAppendRaw("; Small - ");
				csvBuilder.csvAppendFormatRaw("###,##0",result.getSmallRemovalQty());
				csvBuilder.csvAppendRaw("; Total - ");
				csvBuilder.csvAppendFormatRaw("###,##0",result.getRemovalQty());
				csvBuilder.csvAppendRaw("\",\"");
				// Line 17 Column
				csvBuilder.csvAppendFormatRaw("###,##0.000",result.getAdjustmentRemoval());
				csvBuilder.csvAppendRaw("\",\"");
				// Grand Total Column
				csvBuilder.csvAppendRaw("Large - ");
				csvBuilder.csvAppendFormatRaw("###,##0",result.getLargeRemovalQty());
				csvBuilder.csvAppendRaw("; Small - ");
				csvBuilder.csvAppendFormatRaw("###,##0",result.getSmallRemovalQty());
				csvBuilder.csvAppendRaw("; Total - ");
				csvBuilder.csvAppendFormatRaw("###,##0",result.getGrandTotalRemoval());
				csvBuilder.csvAppendRaw("\",\"");
				//Dol Diffrence Column
				csvBuilder.csvAppendFormatRaw("###,##0",result.getDolDifference());
				csvBuilder.csvAppendRaw("\",");
			} else {
				csvBuilder.csvAppendRaw("\"");
				csvBuilder.csvAppendFormatRaw("###,##0.000",result.getRemovalQty());
				csvBuilder.csvAppendRaw("\",\"");
				csvBuilder.csvAppendFormatRaw("###,##0.000",result.getAdjustmentRemoval());
				csvBuilder.csvAppendRaw("\",\"");
				csvBuilder.csvAppendFormatRaw("###,##0.000",result.getGrandTotalRemoval());
				csvBuilder.csvAppendRaw("\",\"");
				csvBuilder.csvAppendFormatRaw("###,##0.000",result.getDolDifference());
				csvBuilder.csvAppendRaw("\",");
			}
			csvBuilder.csvAppendNewLine(activityKeys.get(result.getActivityCd()));
		}
		String fileContents = csvBuilder.toString();
		
		//Create export
		DocExport doc = new DocExport();
		doc.setCsv(fileContents);
		
		return doc;
	}
	
	private DocExport get7501FormExport(long permitId, long periodId, long tobaccoClassId) {
		CsvStringBuilder csvBuilder = new CsvStringBuilder();
        Map<String, String> activityKeys = referenceCodeEntityManager.getValues("ACTIVITY_TYPE");
		List<Export7501FormHeaderEntity> results = this.permitPeriodEntityManager.getExport7501FormHeader(permitId, periodId, tobaccoClassId);
		List<Export7501FormBodyEntity> bodyResults = this.permitPeriodEntityManager.getExport7501FormBody(permitId, periodId, tobaccoClassId);
		String volumeHeaderFormat = tobaccoClassId == 7 || tobaccoClassId == 8 ? "###,##0" : "###,##0.000";
		
		csvBuilder.csvAppendNewLine("Tobacco Class,FDA 3852 Removals, CBP 7501 Removals,Difference Removals,Activity,FDA 3852 Tax,CBP 7501 Tax,Difference Tax,Activity");
		
		for(Export7501FormHeaderEntity result : results) {
			csvBuilder.csvAppend(result.getTobaccoClassNm());
			csvBuilder.csvAppendRaw("\"");
			csvBuilder.csvAppendFormatRaw(volumeHeaderFormat, result.getRemoval3852());
			csvBuilder.csvAppendRaw("\",\"");
			csvBuilder.csvAppendFormatRaw(volumeHeaderFormat,result.getRemovalQty());
			csvBuilder.csvAppendRaw("\",\"");
			csvBuilder.csvAppendFormatRaw(volumeHeaderFormat,result.getDolRemovalDifference());
			csvBuilder.csvAppendRaw("\",");
			csvBuilder.csvAppend(activityKeys.get(result.getRemovalActivityCd()));
			csvBuilder.csvAppendRaw("\"");
			csvBuilder.csvAppendFormatRaw("$###,###,##0.00",result.getTax3852());
			csvBuilder.csvAppendRaw("\",\"");
			csvBuilder.csvAppendFormatRaw("$###,###,##0.00",result.getTaxesPaid());
			csvBuilder.csvAppendRaw("\",\"");
			csvBuilder.csvAppendFormatRaw("$###,###,##0.00",result.getDolDifference());
			csvBuilder.csvAppendRaw("\",");
			csvBuilder.csvAppendNewLine(activityKeys.get(result.getActivityCd()));
		}
		
		csvBuilder.csvAppendNewLine("");
		csvBuilder.csvAppendNewLine("Tobacco Class,Line #,Type,Net Quantity,Excise Tax,Calc Tax Rate,Tax Diff");
		for(Export7501FormBodyEntity result : bodyResults) {
			csvBuilder.csvAppend(result.getTobaccoClassNm());
			csvBuilder.csvAppend(result.getLineNum());
			csvBuilder.csvAppend(result.getSubTobaccoClassNm());
			csvBuilder.csvAppendRaw("\"");
			csvBuilder.csvAppendFormatRaw("###,##0.000", result.getRemovalQty());
			csvBuilder.csvAppendRaw("\",\"");
			csvBuilder.csvAppendFormatRaw("$###,###,##0.00",result.getTaxesPaid());
			csvBuilder.csvAppendRaw("\",");
			csvBuilder.csvAppend(result.getCalcTaxRate());
			csvBuilder.csvAppendRaw("\"");
			csvBuilder.csvAppendFormatRaw("$###,###,##0.00",result.getDolDifference());
			csvBuilder.csvAppendRaw("\"");
			csvBuilder.csvAppendNewLine("");
		}
		
		String fileContents = csvBuilder.toString();
		
		//Create export
		DocExport doc = new DocExport();
		doc.setCsv(fileContents);
		
		return doc;
	}
	
	private DocExport get5000FormExport(long permitId, long periodId) {
		CsvStringBuilder csvBuilder = new CsvStringBuilder();
        Map<String, String> activityKeys = referenceCodeEntityManager.getValues("ACTIVITY_TYPE");
		List<Export5000FormEntity> results = this.permitPeriodEntityManager.getExport5000Form(permitId, periodId);
		
		Collections.sort(results, new Comparator<Export5000FormEntity>() {

			private List<String> sorted = new ArrayList<String>(Arrays.asList("Cigars","Cigarettes","Chew/Snuff","Pipe/RYO"));

			@Override
			public int compare(Export5000FormEntity o1, Export5000FormEntity o2) {
				return this.sorted.indexOf(o1.getTobaccoClassNm()) - this.sorted.indexOf(o2.getTobaccoClassNm());
			}
			
		});
		
		csvBuilder.csvAppendNewLine("Tobacco Class,Tax Return,Excise Tax (FDA 3852),Tax Amount Breakdown,Adjustment Total,Grand Total,Difference,Activity");
		for(Export5000FormEntity result : results) {
			csvBuilder.csvAppend(result.getTobaccoClassNm());
			csvBuilder.csvAppend(result.getTaxIds());
			csvBuilder.csvAppendFormat("$###,###,##0.00",result.getTax3852());
			csvBuilder.csvAppend(result.getTaxAmountBreakDown());
			csvBuilder.csvAppendFormat("$###,###,##0.00",result.getAdjustmentTotal());
			csvBuilder.csvAppendFormat("$###,###,##0.00",result.getGrandTotal());
			csvBuilder.csvAppendFormat("$###,###,##0.00",result.getDolDifference());
			csvBuilder.csvAppendNewLine(activityKeys.get(result.getActivityCd()));
		}
		String fileContents = csvBuilder.toString();
		
		//Create export
		DocExport doc = new DocExport();
		doc.setCsv(fileContents);
		
		return doc;
	}
}

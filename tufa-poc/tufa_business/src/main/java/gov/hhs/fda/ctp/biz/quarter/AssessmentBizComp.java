package gov.hhs.fda.ctp.biz.quarter;

import java.util.List;
import java.util.Map;
import java.util.Set;

import gov.hhs.fda.ctp.common.beans.Assessment;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.AssessmentFilter;
import gov.hhs.fda.ctp.common.beans.AssessmentReportStatus;
import gov.hhs.fda.ctp.common.beans.CigarAssessment;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentFilter;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentRptDetail;
import gov.hhs.fda.ctp.common.beans.QtReconRptDetail;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.persistence.model.AssessmentEntity;
import gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity;

/**
 * The Interface AssessmentBizComp.
 */
public interface AssessmentBizComp {

	/**
	 * Adds the assessment.
	 *
	 * @param ae the ae
	 * @return the string
	 */
	public String addAssessment(AssessmentEntity ae);

	/**
	 * Gets the assessment.
	 *
	 * @param id the id
	 * @return the assessment
	 */
	public AssessmentEntity getAssessment(long id);

	/**
	 * Gets the assessments.
	 *
	 * @param filter the filter
	 * @return the assessments
	 */
	public List<AssessmentEntity> getAssessments(AssessmentFilter filter);

	/**
	 * Gets the fiscal years.
	 *
	 * @return the fiscal years
	 */
	public Set<String> getFiscalYears();

	/**
	 * Get the number of unapproved recons in a quarter.
	 *
	 * @param yr
	 * @param qrt
	 * @return
	 */
	public String getUnapprovedReconRecordCount(long yr, long qrt);

	/**
	 * Get all of the recon data for a quarter.
	 * @param yr
	 * @param qrt
	 * @return
	 */
	public List<QtReconRptDetail>  getReconRptData(long yr, long qrt);

	/**
	 * Get all of the recon data for a quarter.
	 * @param yr
	 * @return
	 */
	public List<QtReconRptDetail>  getReconRptDataForFiscalYear(long yr);

	/**
	 * Get a consolidated view of all monthly reports per quarter.
	 * Also categorize each report as Cigar only and mixed.
	 * @param year
	 * @param quarter
	 * @return
	 */
	public Map<Long, AssessmentReportStatus> getReportStatusData(int year, int quarter);


	/**
	 * Get all versions of the quarterly assessment for a given qtr / year.
	 *
	 * @param qtr the qtr
	 * @param fiscalYear the fiscal year
	 * @return the quarterly assessment
	 */
	public Assessment getQuarterlyAssessment(int qtr, int fiscalYear);
	
	/**
	 * Get all versions of the annual assessment for a given qtr / year.
	 * @param qtr
	 * @param fiscalYear
	 * @return
	 */
	public Assessment getAnnualAssessment(int qtr, int fiscalYear);
	
	/**
	 * Get all versions of the quarterly assessment for a given qtr / year.
	 *
	 * @param qtr the qtr
	 * @param fiscalYear the fiscal year
	 * @param addressOnly the address only
	 * @return the quarterly assessment
	 */
	public Assessment getCigarAssessment(int qtr, int fiscalYear);	

	/**
	 * First check the submit flag.  If it's different than the submitted indicator,
	 * then toggle.  Then generate the market share for qtr/fiscalyear.
	 *
	 * @param qtr
	 * @param fiscalYear
	 * @param submit
	 */
	public void generateQuarterlyMarketShare(int qtr, int fiscalYear, String SubmitFlag);
	
	/**
	 * First check the submit flag.  If it's different than the submitted indicator,
	 * then toggle.  Then generate the market share for qtr/fiscalyear.
	 *
	 * @param qtr the qtr
	 * @param fiscalYear the fiscal year
	 * @param SubmitFlag the submit flag
	 * @param accessqtr the accessqtr
	 * @param marketShareType the market share type
	 */
	public void generateCigarMarketShare(int qtr, int fiscalYear, String SubmitFlag, int accessqtr, String marketShareType);	

	/**
	 * Get recon export data.
	 * @param year
	 * @param quarter
	 * @return
	 */
	public AssessmentExport getReconExport(int year, int quarter);

	/**
	 * Get recon export data.
	 * @param year
	 * @return
	 */
	public AssessmentExport getReconExportForFiscalYear(int year);

	/**
	 * Get recon export data.
	 * @param year
	 * @return
	 */
	public AssessmentExport getCigarReconExport(int year);

	
	
	/**
	 * Gets the cigar assessments.
	 *
	 * @param filter the filter
	 * @return the cigar assessments
	 */
	public List<CigarAssessment> getCigarAssessments(CigarAssessmentFilter filter);

	/**
	 * Gets the cigar assessments report detail.
	 *
	 * @param fiscalYr the fiscal yr
	 * @return the cigar assessments report detail
	 */
	public List<CigarAssessmentRptDetail> getCigarAssessmentsReportDetail(long fiscalYr);

    /**
     * Gets the support docs.
     *
     * @param assessmentId the assessment id
     * @return the support docs
     */
    public List<SupportingDocument> getSupportDocs(long assessmentId);
    public List<SupportingDocument> getCigarSupportDocs(long fiscalYr);

    /**
     * Delete by doc id.
     *
     * @param asmtdocId the asmtdoc id
     */
    public void deleteByDocId(long asmtdocId);

    /**
     * Save support document.
     *
     * @param supDoc the sup doc
     */
    public void saveSupportDocument(SupportingDocumentEntity supDoc);

    /**
     * Gets the doc by doc id.
     *
     * @param asmtdocId the asmtdoc id
     * @return the doc by doc id
     */
    public SupportingDocumentEntity getDocByDocId(long asmtdocId);
    
    /**
     * Check if an Annual True up has been created for the Fiscal Year
     * @param year
     * @return
     */
    public String getAnnualTrueUpSubmittedMessage(int fiscalYear);
    

    
    public List<Assessment> getAssessmentByYear(long fiscalYr,String unapprovedReconString);

}

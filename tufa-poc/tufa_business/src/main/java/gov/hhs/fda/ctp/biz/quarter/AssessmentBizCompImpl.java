package gov.hhs.fda.ctp.biz.quarter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.Assessment;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.AssessmentFilter;
import gov.hhs.fda.ctp.common.beans.AssessmentReportStatus;
import gov.hhs.fda.ctp.common.beans.AssessmentVersion;
import gov.hhs.fda.ctp.common.beans.CBPExportDetail;
import gov.hhs.fda.ctp.common.beans.CigarAssessment;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentFilter;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentRptDetail;
import gov.hhs.fda.ctp.common.beans.QtReconRptDetail;
import gov.hhs.fda.ctp.common.beans.ReconExportDetail;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.common.util.CsvStringBuilder;
import gov.hhs.fda.ctp.common.util.EinBuilder;
import gov.hhs.fda.ctp.common.util.PermitBuilder;
import gov.hhs.fda.ctp.common.util.AssessmentExportBuilder;
import gov.hhs.fda.ctp.persistence.AssessmentEntityManager;
import gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.TrueUpEntityManager;
import gov.hhs.fda.ctp.persistence.model.AssessmentEntity;
import gov.hhs.fda.ctp.persistence.model.AssessmentVersionEntity;
import gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.TobaccoClassEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpEntity;

/**
 * The Class AssessmentBizCompImpl.
 */
@Service
@Transactional
public class AssessmentBizCompImpl implements AssessmentBizComp {

	/** The assessment entity manager. */
	@Autowired
	private AssessmentEntityManager assessmentEntityManager;


	private ReferenceCodeEntityManager referenceCodeEntityManager;
	

	/** The simple Sql Manager. */
	private SimpleSQLManager simpleSqlManager;
	
	@Autowired
	private TrueUpEntityManager trueUpEntityManager;

	@Autowired
	private MapperWrapper mapperWrapper;

	@Autowired
	public void setReferenceCodeEntityManager(ReferenceCodeEntityManager referenceCodeEntityManager) {
		this.referenceCodeEntityManager = referenceCodeEntityManager;
	}

	public TrueUpEntityManager getTrueUpEntityManager() {
		return trueUpEntityManager;
	}

	public void setTrueUpEntityManager(TrueUpEntityManager trueUpEntityManager) {
		this.trueUpEntityManager = trueUpEntityManager;
	}

	/**
	 * Sets the assessment entity manager.
	 *
	 * @param assessmentEntityManager            the assessmentEntityManager to set
	 */
	@Autowired
	public void setAssessmentEntityManager(AssessmentEntityManager assessmentEntityManager) {
		this.assessmentEntityManager = assessmentEntityManager;
	}

	/**
	 * @param simpleSqlManager the simpleSqlManager to set
	 */
	@Autowired
	public void setSimpleSqlManager(SimpleSQLManager simpleSqlManager) {
		this.simpleSqlManager = simpleSqlManager;
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp#addAssessment(gov.hhs.fda.ctp.persistence.model.AssessmentEntity)
	 */
	@Override
	public String addAssessment(AssessmentEntity ae) {
		return this.assessmentEntityManager.addAssessment(ae);
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp#getAssessment(long)
	 */
	@Override
	public AssessmentEntity getAssessment(long id) {
		return this.assessmentEntityManager.getAssessment(id);
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp#getAssessments(gov.hhs.fda.ctp.common.beans.AssessmentFilter)
	 */
	@Override
	public List<AssessmentEntity> getAssessments(AssessmentFilter filter) {

		return this.assessmentEntityManager.getAssessments(filter);
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp#getFiscalYears()
	 */
	@Override
	public Set<String> getFiscalYears() {
		return this.assessmentEntityManager.getFiscalYears();
	}

	@Override
	public void generateQuarterlyMarketShare(int qtr, int fiscalYear, String submit) {
		AssessmentEntity assessment = assessmentEntityManager.getAssessmentByYearByQtr(fiscalYear, qtr, Constants.QUARTERLY);
		if("Y".equals(submit)) {
			// user is ready to submit the assessment and promote the temporary
			// version 0 to the next version label.
			if("N".equals(assessment.getSubmittedInd())) {
				assessment.setSubmittedInd("Y");
				assessmentEntityManager.updateAssessment(assessment);
				this.assessmentEntityManager.generateQuarterlyMarketShare(qtr, fiscalYear,"Y");
			}
			else {
				// assessment already submitted.  do not re-submit.
				// ignore this action.
			}
		}
		if("N".equals(submit)) {
			// user simply wants to generate a new temporary market share.
			if("Y".equals(assessment.getSubmittedInd())) {
				// mark the assessment unsubmitted to demote the last version and
				// enable version 0.
				assessment.setSubmittedInd("N");
				assessmentEntityManager.updateAssessment(assessment);
			}
			
			this.assessmentEntityManager.generateQuarterlyMarketShare(qtr, fiscalYear,"N");
		}
	}

	public void generateCigarMarketShare(int qtr, int fiscalYear, String submit, int accessqtr, String marketshareType) {
		AssessmentEntity assessment = assessmentEntityManager.getAssessmentByYearByQtr(fiscalYear, accessqtr, Constants.CIGR_QTRLY);
		if("Y".equals(submit)) {
			// user is ready to submit the assessment and promote the temporary
			// version 0 to the next version label.
			if("N".equals(assessment.getSubmittedInd())) {
				assessment.setSubmittedInd("Y");
				//assessment.setCigarSubmittedInd("Y");
				assessmentEntityManager.updateAssessment(assessment);
				this.assessmentEntityManager.generateCigarMarketShare(accessqtr, fiscalYear,"Y", marketshareType);
			}
			else {
				// assessment already submitted.  do not re-submit.
				// ignore this action.
			}
		}
		if("N".equals(submit)) {
			// user simply wants to generate a new temporary market share.
			if("Y".equals(assessment.getSubmittedInd())) {
				// mark the assessment unsubmitted to demote the last version and
				// enable version 0.
				assessment.setSubmittedInd("N");
				assessmentEntityManager.updateAssessment(assessment);
			}
			
			this.assessmentEntityManager.generateCigarMarketShare(accessqtr, fiscalYear,"N", marketshareType);
		}
	}
	
	private Assessment getAssessment(int qtr, int fiscalYear, String assessmentType){
		// get assessment
		AssessmentEntity assessmentEntity = this.assessmentEntityManager.getAssessmentByYearByQtr(fiscalYear, qtr, assessmentType);

		// translate the market shares attached to the assessment
		Mapper mapper = mapperWrapper.getMapper();
		Assessment assessment = new Assessment();
		mapper.map(assessmentEntity, assessment);
		// sort the versions from highest to lowest.
		List<AssessmentVersion> versions = assessment.getVersions();
		Collections.sort(versions);
		assessment.setVersions(versions);
		// attach the exports to the assessment / versions
		AssessmentExportBuilder exportBuilder = new AssessmentExportBuilder(assessment);
		return exportBuilder.buildAssessmentExports();
	}
	
	@Override
	public Assessment getQuarterlyAssessment(int qtr, int fiscalYear) {
		return getAssessment(qtr, fiscalYear, Constants.QUARTERLY);
	}

	@Override
	public Assessment getAnnualAssessment(int qtr, int fiscalYear) {
		return getAssessment(qtr, fiscalYear, Constants.ANNUAL);
	}
	
	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp#getCigarAssessment(int, int, boolean)
	 */
	@Override
	public Assessment getCigarAssessment(int qtr, int fiscalYear) {
		return getAssessment(qtr, fiscalYear, Constants.CIGR_QTRLY);
	}
	
	/**
	 * Utility class to return the list of tobacco names by type.
	 * @param tobaccoClasses
	 * @return
	 */
	private Map<Long, String> getTobaccoNames(List<TobaccoClassEntity> tobaccoClasses) {
		Map<Long, String> tobaccoNames = new HashMap<Long, String>();
		Map<Long, String> parentNames = new HashMap<Long, String>();
		List<TobaccoClassEntity> nonParents = new ArrayList<TobaccoClassEntity>();
		for(TobaccoClassEntity tobaccoClass: tobaccoClasses) {
			if(tobaccoClass.getParentClassId() == null) {
				parentNames.put(tobaccoClass.getTobaccoClassId(), tobaccoClass.getTobaccoClassNm());
				tobaccoNames.put(tobaccoClass.getTobaccoClassId(), tobaccoClass.getTobaccoClassNm());
			}
			else {
				nonParents.add(tobaccoClass);
			}
		}
		for(TobaccoClassEntity childClass : nonParents) {
			String parentName = parentNames.get(childClass.getParentClassId());
			String childName = childClass.getTobaccoClassNm();
			tobaccoNames.put(childClass.getTobaccoClassId(), parentName + " - " + childName);
		}
		return tobaccoNames;
	}

	@Override
	public String getUnapprovedReconRecordCount(long yr, long qrt) {
		String returnVal = null;
		// return the number of unapproved reports (ignore cigar-only).
		int count = 0;
		// count the unapproved recons in this assessment
		if(qrt != Constants.CIGAR_ASSESSMENT_QTR) {
			List<QtReconRptDetail> reconRptDetails = this.simpleSqlManager.getReconReportData(yr, qrt);
			for(QtReconRptDetail detail : reconRptDetails) {
				// if unapproved and not a cigar-only report, flag it.
				if("UNAP".equals(detail.getReconStatus()) && ! "C".equals(detail.getReportedTobacco())) count++;
			}
		}
		else {
			List<CigarAssessmentRptDetail> reconRptDetails = this.simpleSqlManager.getCigarAssessmentReportStatusData(yr);
			for(CigarAssessmentRptDetail detail : reconRptDetails) {
				if("Not Approved".equals(detail.getReconStatus())) count++;
			}
		}
		// return message if count > 0
		if(count > 0) {
			returnVal = "<b>ACTION REQUIRED:</b> " + count +
			" Reports need to be approved under Reconciliation prior to Submitting the Generated Full Market Share";
		}
		return returnVal;
	}

	@Override
	public List<QtReconRptDetail>  getReconRptData(long yr, long qrt){
		 List<QtReconRptDetail> reconRptDetails = this.simpleSqlManager.getReconReportData(yr, qrt);
		 Map<String,String> pStatus  = this.referenceCodeEntityManager.getValues("PERIOD_STATUS_TYPE");
		 Map<String,String> rStatus  = this.referenceCodeEntityManager.getValues("RECON_STATUS_TYPE");
		 Map<String,String> permitTypes = this.referenceCodeEntityManager.getValues("PERMIT_TYPE");

		 // substitue raw codes with readable strings
		 for (QtReconRptDetail detail : reconRptDetails) {
			detail.setReconStatus(rStatus.get(detail.getReconStatus()));
			detail.setReportStatus(pStatus.get(detail.getReportStatus()));
			detail.setPermitTypeCd(permitTypes.get(detail.getPermitTypeCd()));
		 }

		 return reconRptDetails;
	}

	@Override
	public List<QtReconRptDetail>  getReconRptDataForFiscalYear(long yr){
		 List<QtReconRptDetail> reconRptDetails = this.simpleSqlManager.getReconReportDataForFiscalYear(yr);
		 Map<String,String> pStatus  = this.referenceCodeEntityManager.getValues("PERIOD_STATUS_TYPE");
		 Map<String,String> rStatus  = this.referenceCodeEntityManager.getValues("RECON_STATUS_TYPE");
		 Map<String,String> permitTypes = this.referenceCodeEntityManager.getValues("PERMIT_TYPE");

		 // substitue raw codes with readable strings
		 for (QtReconRptDetail detail : reconRptDetails) {
			detail.setReconStatus(rStatus.get(detail.getReconStatus()));
			detail.setReportStatus(pStatus.get(detail.getReportStatus()));
			detail.setPermitTypeCd(permitTypes.get(detail.getPermitTypeCd()));
		 }

		 return reconRptDetails;
	}

	@Override
	public Map<Long, AssessmentReportStatus> getReportStatusData(int year, int quarter) {
		return simpleSqlManager.getReportStatusData(year, quarter);
	}

	
	
	@Override
	public AssessmentExport getReconExport(int year, int quarter) {
		// get reference codes for inserting into data
		Map<Long, String> tobaccoMap = new HashMap<Long, String>();
		List<TobaccoClassEntity> classes = this.referenceCodeEntityManager.getAllTobaccoClasses();
		for(TobaccoClassEntity tobaccoClass : classes) {
			tobaccoMap.put(tobaccoClass.getTobaccoClassId(), tobaccoClass.getTobaccoClassNm());
		}
		Map<String, String> reconMap = referenceCodeEntityManager.getValues("RECON_STATUS_TYPE");

		// get list of recon records
		List<ReconExportDetail> reconExportDetails = this.simpleSqlManager.getReconExportData(year, quarter);

		// build title
		StringBuilder title = new StringBuilder("FY_");
		title.append(year);
		title.append("_Q");
		title.append(quarter);
		title.append("_Recon");
		title.append(".csv");

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine("Year, Q, Month, Company Name, EIN, TTB Permit, Tobacco Class, Volume Removed, Excise Tax Amount, Recon, User, Date, Time");
		for(ReconExportDetail detail : reconExportDetails) {
			EinBuilder einBuilder = new EinBuilder(detail.getEin());
			PermitBuilder ttbBuilder = new PermitBuilder(detail.getPermitNum());
			csv.csvAppend(detail.getYear());
			csv.csvAppend(detail.getQuarter());
			csv.csvAppend(detail.getMonth());
			csv.csvAppendInQuotes(detail.getLegalNm());
			csv.csvAppend(einBuilder.getFormattedEin());
			csv.csvAppend(ttbBuilder.formatPermit());
			csv.csvAppend(tobaccoMap.get(new Long(detail.getTobaccoClassId())));
			csv.csvAppend(detail.getVolumeRemoved());
			csv.csvAppend(detail.getTax());
			csv.csvAppend(reconMap.get(detail.getReconStatus()));
			csv.csvAppend(detail.getReconUser());
			csv.csvAppend(CTPUtil.parseDateToString(detail.getReconDate()));
			csv.csvAppendNewLine(CTPUtil.parseTimeToString(detail.getReconDate()));
		}

		AssessmentExport export = new AssessmentExport();
		export.setTitle(title.toString());
		export.setCsv(csv.toString());
		return export;
	}

	@Override
	public AssessmentExport getReconExportForFiscalYear(int year) {
		// get reference codes for inserting into data
		Map<Long, String> tobaccoMap = new HashMap<Long, String>();
		List<TobaccoClassEntity> classes = this.referenceCodeEntityManager.getAllTobaccoClasses();
		for(TobaccoClassEntity tobaccoClass : classes) {
			tobaccoMap.put(tobaccoClass.getTobaccoClassId(), tobaccoClass.getTobaccoClassNm());
		}
		Map<String, String> reconMap = referenceCodeEntityManager.getValues("RECON_STATUS_TYPE");

		// get list of recon records
		List<ReconExportDetail> reconExportDetails = this.simpleSqlManager.getReconExportDataForFiscalYear(year);

		// build title
		StringBuilder title = new StringBuilder("FY_");
		title.append(year);
		title.append("_Recon");
		title.append(".csv");

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine("Year, Q, Month, Company Name, EIN, TTB Permit, Tobacco Class, Volume Removed, Excise Tax Amount, Recon, User, Date, Time");
		for(ReconExportDetail detail : reconExportDetails) {
			EinBuilder einBuilder = new EinBuilder(detail.getEin());
			PermitBuilder ttbBuilder = new PermitBuilder(detail.getPermitNum());
			csv.csvAppend(detail.getYear());
			csv.csvAppend(detail.getQuarter());
			csv.csvAppend(detail.getMonth());
			csv.csvAppendInQuotes(detail.getLegalNm());
			csv.csvAppend(einBuilder.getFormattedEin());
			csv.csvAppend(ttbBuilder.formatPermit());
			csv.csvAppend(tobaccoMap.get(new Long(detail.getTobaccoClassId())));
			csv.csvAppend(detail.getVolumeRemoved());
			csv.csvAppend(detail.getTax());
			csv.csvAppend(reconMap.get(detail.getReconStatus()));
			csv.csvAppend(detail.getReconUser());
			csv.csvAppend(CTPUtil.parseDateToString(detail.getReconDate()));
			csv.csvAppendNewLine(CTPUtil.parseTimeToString(detail.getReconDate()));
		}

		AssessmentExport export = new AssessmentExport();
		export.setTitle(title.toString());
		export.setCsv(csv.toString());
		return export;
	}

	@Override
	public AssessmentExport getCigarReconExport(int year) {
		// get reference codes for inserting into data
		Map<Long, String> tobaccoMap = new HashMap<Long, String>();
		List<TobaccoClassEntity> classes = this.referenceCodeEntityManager.getAllTobaccoClasses();
		for(TobaccoClassEntity tobaccoClass : classes) {
			tobaccoMap.put(tobaccoClass.getTobaccoClassId(), tobaccoClass.getTobaccoClassNm());
		}
		Map<String, String> reconMap = referenceCodeEntityManager.getValues("RECON_STATUS_TYPE");

		// get list of recon records
		List<ReconExportDetail> reconExportDetails = this.simpleSqlManager.getCigarReconExportData(year);

		// build title
		StringBuilder title = new StringBuilder("FY_");
		title.append(year);
		title.append("_Cigar_Recon");
		title.append(".csv");

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine("Year, Month, Company Name, EIN, TTB Permit, Tobacco Class, Volume Removed, Excise Tax Amount, Recon, User, Date, Time");
		for(ReconExportDetail detail : reconExportDetails) {
			EinBuilder einBuilder = new EinBuilder(detail.getEin());
			PermitBuilder ttbBuilder = new PermitBuilder(detail.getPermitNum());
			csv.csvAppend(detail.getYear());
			csv.csvAppend(detail.getMonth());
			csv.csvAppendInQuotes(detail.getLegalNm());
			csv.csvAppend(einBuilder.getFormattedEin());
			csv.csvAppend(ttbBuilder.formatPermit());
			csv.csvAppend(tobaccoMap.get(new Long(detail.getTobaccoClassId())));
			csv.csvAppend(detail.getVolumeRemoved());
			csv.csvAppend(detail.getTax());
			csv.csvAppend(reconMap.get(detail.getReconStatus()));
			csv.csvAppend(detail.getReconUser());
			csv.csvAppend(CTPUtil.parseDateToString(detail.getReconDate()));
			csv.csvAppendNewLine(CTPUtil.parseTimeToString(detail.getReconDate()));
		}

		AssessmentExport export = new AssessmentExport();
		export.setTitle(title.toString());
		export.setCsv(csv.toString());
		return export;
	}

	@Override
	public List<CigarAssessment> getCigarAssessments(CigarAssessmentFilter filter) {

		Mapper mapper = mapperWrapper.getMapper();
		List<CigarAssessment> cigarAssessmentBeans = new ArrayList<CigarAssessment>();

		Map<String,String> assessmentType  = this.referenceCodeEntityManager.getKeys("ASSESSMENT_TYPE");
		String assessmentTypeCigar = assessmentType.get("Cigar");

		List<AssessmentEntity> assessmentEntities = this.assessmentEntityManager.getCigarAssessment(filter, assessmentTypeCigar);
		TreeSet<Long> versSorted = null;
		if(assessmentEntities != null) {
			for(AssessmentEntity entity : assessmentEntities) {
				
				boolean exists = false;
				CigarAssessment cigarAssessment = new CigarAssessment();
				String currentEntitySubInd = null;
				mapper.map(entity, cigarAssessment);
				//This is to identify if a cigar assessment is submitted previously
				versSorted = new TreeSet<Long>();
				Set<AssessmentVersionEntity> versions = entity.getVersions();

				for (AssessmentVersionEntity versionEntity : versions){
					versSorted.add(versionEntity.getVersionNum());
				}

				if (versSorted.higher(new Long(0)) != null) {
					currentEntitySubInd = "Y";
				} else{
					currentEntitySubInd = "N";
				}
				cigarAssessment.setSubmittedInd(currentEntitySubInd);
				
				for(CigarAssessment cigar: cigarAssessmentBeans) {
					if(cigar.getAssessmentYr().intValue() == entity.getAssessmentYr()) {
						cigarAssessment = cigar;
						cigarAssessment.setSubmittedInd(cigarAssessment.getSubmittedInd().concat(currentEntitySubInd));
						cigarAssessment.setCigarSubmittedInd(cigarAssessment.getSubmittedInd());
						cigarAssessment.setSubmittedStatusSort(Long.valueOf(StringUtils.countMatches(cigarAssessment.getSubmittedInd(), "Y")));
						exists = true;
					}
				}
				
				
				if("NNNN".equals(cigarAssessment.getSubmittedInd())) {
					cigarAssessment.setSubmittedInd("Not Submitted");
					cigarAssessment.setCigarSubmittedInd("Not Submitted");
				}
				
				cigarAssessment.setCreatedDt(CTPUtil.parseDateToString(entity.getCreatedDt()));
				if(!exists) {
					cigarAssessmentBeans.add(cigarAssessment);
				}
			}
		}
		return cigarAssessmentBeans;

	}


	@Override
	public List<CigarAssessmentRptDetail> getCigarAssessmentsReportDetail(long fiscalYr){

		  return this.simpleSqlManager.getCigarAssessmentReportStatusData(fiscalYr);
	}

	@Override
	public List<SupportingDocument> getSupportDocs(long assessmentId) {
		return assessmentEntityManager.getSupportDocs(assessmentId);
	}
	
	@Override
	public List<SupportingDocument> getCigarSupportDocs(long fiscalYr) {
		return assessmentEntityManager.getCigarSupportDocs(fiscalYr);
	}
	@Override
	public void deleteByDocId(long asmtdocId) {
		assessmentEntityManager.deleteByDocId(asmtdocId);
	}

	@Override
	public void saveSupportDocument(SupportingDocumentEntity supDoc) {
		assessmentEntityManager.saveSupportDocument(supDoc);
	}

	@Override
	public SupportingDocumentEntity getDocByDocId(long asmtdocId) {
		return assessmentEntityManager.getDocByDocId(asmtdocId);
	}
	
	@Override
	public String getAnnualTrueUpSubmittedMessage(int fiscalYear) {
		String msg = null;
		TrueUpEntity trueup = trueUpEntityManager.getTrueUpByYear(fiscalYear);
		//check if the annual true-up has been created for the fiscal year.
		if(null != trueup && "Y".equals(trueup.getSubmittedInd()) && null != trueup.getSubmittedDt()){
			msg = "An Annual True-Up has been created for this Fiscal Year. Please navigate to the Annual True-Up to generate and submit additional Market Share reports.";
		}
		return msg;
	}
	public List<Assessment> getAssessmentByYear(long fiscalYr,String unapprovedReconString){
		List<Assessment> assessmentList = new ArrayList<>();
		List<AssessmentEntity> assessmenEntitytList =  assessmentEntityManager.getAssessmentByYear(fiscalYr);
		Mapper mapper = mapperWrapper.getMapper();
		Assessment assessment = null;
		List<AssessmentVersion> versions = null;
		AssessmentExportBuilder exportBuilder = null;
		for(AssessmentEntity assessmentEntity:assessmenEntitytList){
		// translate the market shares attached to the assessment
				assessment = new Assessment();
				mapper.map(assessmentEntity, assessment);
				// sort the versions from highest to lowest.
				versions = assessment.getVersions();
				Collections.sort(versions);
				assessment.setVersions(versions);
				assessment.setApprovalMessage(unapprovedReconString);
				// attach the exports to the assessment / versions
				exportBuilder = new AssessmentExportBuilder(assessment);
				assessmentList.add(exportBuilder.buildAssessmentExports());
		}
		return assessmentList ;
	}
}

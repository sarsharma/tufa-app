package gov.hhs.fda.ctp.biz.registration;

public class EmploymentDTO {

	private String companyName;
	private String companyAddress;
	
	public EmploymentDTO(){
		companyName = "";
		companyAddress = "";
	}
	
	public EmploymentDTO(String companyName,String companyAddress){
		this.companyName = companyName;
		this.companyAddress = companyAddress;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	
	
}

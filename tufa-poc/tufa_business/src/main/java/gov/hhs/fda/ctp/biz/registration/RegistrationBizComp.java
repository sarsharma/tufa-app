package gov.hhs.fda.ctp.biz.registration;

public interface RegistrationBizComp {

	public void registerUser(RegistrationDTO registration);
	public void registerDummyUser();
	public UserDTO getRegisteredUser(String id);
}

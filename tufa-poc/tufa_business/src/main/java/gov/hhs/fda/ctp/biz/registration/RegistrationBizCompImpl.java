package gov.hhs.fda.ctp.biz.registration;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.persistence.model.EmploymentEntity;
import gov.hhs.fda.ctp.persistence.model.UserEntity;

@Service("registrationBizModelService")
@Transactional
public class RegistrationBizCompImpl implements RegistrationBizComp {
	
	@Override
	public void registerUser(RegistrationDTO registration) {
		// create assembler between domain and model
		 assembleUser(registration.getUser());
		 assembleEmployment(registration.getEmployment());
	}

	private UserEntity assembleUser(UserDTO user) {

		UserEntity userEntity = new UserEntity();
		userEntity.setEmail(user.getEmail());

		return userEntity;
	}

	private EmploymentEntity assembleEmployment(EmploymentDTO employment) {

		EmploymentEntity employmentEntity = new EmploymentEntity();
		employmentEntity.setCompanyAddress(employment.getCompanyAddress());
		employmentEntity.setCompanyName(employment.getCompanyName());

		return employmentEntity;

	}

	@Override
	public void registerDummyUser() {
		throw new UnsupportedOperationException("Method not supported in RegistrationBizComp");
		
	}
	
	@Override
	public UserDTO getRegisteredUser(String id) {
		return null;
	}
			
}

package gov.hhs.fda.ctp.biz.registration;
/*
 * Registration Data Access Object
 * */
public class RegistrationDTO {

	private UserDTO user;
	private EmploymentDTO employment;
	
	public RegistrationDTO(){
		user = new UserDTO();
		employment = new EmploymentDTO();
	}
	
	public RegistrationDTO(UserDTO user, EmploymentDTO employment){
		this.user= user;
		this.employment = employment;
	}
	
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public EmploymentDTO getEmployment() {
		return employment;
	}
	public void setEmployment(EmploymentDTO employment) {
		this.employment = employment;
	}
	
	
}

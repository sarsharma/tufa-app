package gov.hhs.fda.ctp.biz.reporting;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import gov.hhs.fda.ctp.common.beans.Attachment;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitAttachmentsEntity;

/**
 * The Interface AttachmentsBizComp.
 */
public interface AttachmentsBizComp {
	
	/**
	 * Adds the attach.
	 *
	 * @param attach the attach
	 * @param file 
	 * @return the attachments entity
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public void addAttach(AttachmentsEntity attach, MultipartFile file) throws IOException, SQLException;

	/**
	 * Gets the permit using the permitId by using the Hibernate Session Object.
	 *
	 * @param docId the doc id
	 * @return the attachment
	 */
	
	public Attachment getPermitAttachmentDownload(long docId) throws SQLException;
	

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	List<AttachmentsEntity> findAll();
	
	/**
	 * Find by ids.
	 *
	 * @param periodId the period id
	 * @param permitId the permit id
	 * @return the list
	 */
	List<AttachmentsEntity> findByIds(long periodId, long permitId);
	
	/**
	 * Delete by id.
	 *
	 * @param id the id
	 */
	void deleteById(long id,String attachFrm);

	public byte[] getAttachmentAsByteArray(long docId,String attachFor) throws SQLException;

	public Attachment getAttachmentDownload(long docId) throws SQLException;
	
	public List<PermitAttachmentsEntity> findPermitDocbyId(long permitId)throws SQLException;
	public AttachmentsEntity getAttachment(long docId);
	public PermitAttachmentsEntity getPermitAttachment(long docId);
	public void addPermitAttach(PermitAttachmentsEntity permitattachment, MultipartFile file)throws IOException, SQLException;

}

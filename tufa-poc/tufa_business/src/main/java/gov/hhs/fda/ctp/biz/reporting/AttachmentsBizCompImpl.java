package gov.hhs.fda.ctp.biz.reporting;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import gov.hhs.fda.ctp.common.beans.Attachment;
import gov.hhs.fda.ctp.persistence.AttachmentsEntityManager;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitAttachmentsEntity;

/**
 * The Class AttachmentsBizCompImpl.
 */
@Service
@Transactional
public class AttachmentsBizCompImpl implements AttachmentsBizComp {
	
	/** The attachments entity manager. */
	@Autowired
	AttachmentsEntityManager attachmentsEntityManager;
	
	/**
	 * Instantiates a new attachments biz comp impl.
	 *
	 * @param attachmentsEntityManager the attachments entity manager
	 */
	public AttachmentsBizCompImpl(AttachmentsEntityManager attachmentsEntityManager) {
        this.attachmentsEntityManager = attachmentsEntityManager;
    }
	
	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.reporting.AttachmentsBizComp#findAll()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<AttachmentsEntity> findAll() {
		return this.attachmentsEntityManager.findAll();
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.reporting.AttachmentsBizComp#findByIds(long, long)
	 */
	@Override
	public List<AttachmentsEntity> findByIds(long periodId, long permitId) {
		return this.attachmentsEntityManager.findByIds(periodId,permitId);
	}

	
	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.reporting.AttachmentsBizComp#deleteById(long)
	 */
	@Override
	public void deleteById(long id,String attachFrm) {
		this.attachmentsEntityManager.deleteById(id,attachFrm);
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.reporting.AttachmentsBizComp#getAttachment(long)
	 */
	
	@Override
	public AttachmentsEntity getAttachment(long docId) {
		return this.attachmentsEntityManager.getAttachment(docId);
	}
	
	@Override
	public PermitAttachmentsEntity getPermitAttachment(long docId) {
		return this.attachmentsEntityManager.getPermitAttachment(docId);
	}
	
	@Override
	public Attachment getAttachmentDownload(long docId) throws SQLException {
		Attachment result = new Attachment();
		AttachmentsEntity attachment = this.attachmentsEntityManager.getAttachment(docId);
		result.setReportPdf(attachmentsEntityManager.getByteArray(attachment.getReportPdf()));
		result.setDocDesc(attachment.getDocDesc());
		
		return result;
	}
	public Attachment getPermitAttachmentDownload(long docId) throws SQLException {
		PermitAttachmentsEntity attachment = this.attachmentsEntityManager.getPermitAttachment(docId);;
		Attachment result = new Attachment();
		result.setReportPdf(attachmentsEntityManager.getByteArray(attachment.getReportPdf()));
		result.setDocDesc(attachment.getDocDesc());
		
		return result;
	}
	
	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.reporting.AttachmentsBizComp#getAttachment(long)
	 */
	

	@Override
	public byte[] getAttachmentAsByteArray(long docId,String attachFor) throws  SQLException {
		
		if("permit".equalsIgnoreCase(attachFor)){
			return this.attachmentsEntityManager.getPermitAttachmentAsByteArry(docId);
		}else{
			return this.attachmentsEntityManager.getAttachmentAsByteArry(docId);
		}
		
	}
	
	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.biz.reporting.AttachmentsBizComp#addAttach(gov.hhs.fda.ctp.persistence.model.AttachmentsEntity)
	 */
	@Override
	public void addAttach(AttachmentsEntity attachment, MultipartFile file) throws IOException, SQLException {	
		
		AttachmentsEntity ae = new AttachmentsEntity();
		Blob blob = null;
		if(attachment.getDocumentId() != 0){
			ae = this.getAttachment(attachment.getDocumentId());
			ae.setDocDesc(attachment.getDocDesc());
			ae.setFormTypes(attachment.getFormTypes());
		}else {

			//Done to match legacy files 
			byte[] pretext = "data:application/pdf;base64,".getBytes();
			byte[] encodedFile = Base64.encode(file.getBytes());
			
			byte[] combination = new byte[pretext.length + encodedFile.length];
			System.arraycopy(pretext, 0, combination, 0, pretext.length);
			System.arraycopy(encodedFile, 0, combination, pretext.length, encodedFile.length);
			
			blob = this.attachmentsEntityManager.getBlob(combination);
			attachment.setReportPdf(blob);
			ae = attachment;
		}
		
		this.attachmentsEntityManager.addAttach(ae);
		
		if(blob != null) {
			blob.free();
		}
	}
	@Override
	public void addPermitAttach(PermitAttachmentsEntity permitattachment, MultipartFile file) throws IOException, SQLException {	
		
		PermitAttachmentsEntity pae = new PermitAttachmentsEntity();
		Blob blob = null;
		if(permitattachment.getDocumentId() != 0){
			pae = this.getPermitAttachment(permitattachment.getDocumentId());
			pae.setDocDesc(permitattachment.getDocDesc());
			pae.setFormTypes(permitattachment.getFormTypes());
		}else {
			//Done to match legacy files 
			byte[] pretext = "data:application/pdf;base64,".getBytes();
			byte[] encodedFile = Base64.encode(file.getBytes());
			
			byte[] combination = new byte[pretext.length + encodedFile.length];
			System.arraycopy(pretext, 0, combination, 0, pretext.length);
			System.arraycopy(encodedFile, 0, combination, pretext.length, encodedFile.length);
			
			blob = this.attachmentsEntityManager.getBlob(combination);
			permitattachment.setReportPdf(blob);
			pae = permitattachment;
		}
		
		this.attachmentsEntityManager.addPermitAttach(pae);
		
		if(blob != null) {
			blob.free();
		}
	}
	
	@Override
	public List<PermitAttachmentsEntity> findPermitDocbyId(long permitId) {
		return this.attachmentsEntityManager.findPermitDocbyId(permitId);
	}

	
}

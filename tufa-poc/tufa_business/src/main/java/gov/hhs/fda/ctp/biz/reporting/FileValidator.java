package gov.hhs.fda.ctp.biz.reporting;

import org.springframework.stereotype.Component;

/**
 * The Class FileValidator.
 */
@Component
public class FileValidator{
		
	/** The logger. *//*
	static Logger logger = LoggerFactory.getLogger(FileValidator.class);
	
	*//**
	 * Supports.
	 *
	 * @param clazz the clazz
	 * @return true, if successful
	 *//*
	public boolean supports(Class<?> clazz) {
		return FileBucket.class.isAssignableFrom(clazz);
	}

	*//**
	 * Validate.
	 *
	 * @return true, if successful
	 *//*
	
	 * Method that returns true when pdf file is uploaded
	 * 
	public boolean validate() {
		return true;
	}
	
	*//**
	 * Multipart to file.
	 *
	 * @param multipart the multipart
	 * @return the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 *//*
	
	 * Returns a File from a MultipartFile
	 * 
	public File multipartToFile(MultipartFile multipart) throws IOException 
	{
        File convFile = new File( multipart.getOriginalFilename());
        multipart.transferTo(convFile);
        return convFile;
	}
	
	*//**
	 * Filename.
	 *
	 * @param fullName the full name
	 * @param extensionSeparator the extension separator
	 * @param pathSeparator the path separator
	 * @return the string
	 *//*
	
	 * Removes relative path to the file and return the original file name
	 * 
	public String filename(String fullName, char extensionSeparator, char pathSeparator) { // gets filename without extension
	    int dot = fullName.lastIndexOf(extensionSeparator);
	    int sep = fullName.lastIndexOf(pathSeparator);
	    return fullName.substring(sep + 1, dot);
	}
	
	*//**
	 * Checks if is PD fby meta.
	 *
	 * @param by the by
	 * @param name the name
	 * @return true, if is PD fby meta
	 * @throws IOException Signals that an I/O exception has occurred.
	 *//*
	
	 * Return true when metadata returns application/json
	 * 
	public static boolean isPDFbyMeta(byte[] by, String name) throws IOException{
		try(InputStream is = new ByteArrayInputStream(by)) {
	      ContentHandler contenthandler = new BodyContentHandler();
	      Metadata metadata = new Metadata();
	      metadata.set(Metadata.RESOURCE_NAME_KEY, name);
	      Parser parser = new AutoDetectParser();
	      parser.parse(is, contenthandler, metadata);
	      if(("application/pdf").equals(metadata.get(Metadata.CONTENT_TYPE)))
	    	  return true;
	    }
	    catch (Exception e) {
	    	logger.error("error", e);
	    }

	    return false;
	}*/
}



package gov.hhs.fda.ctp.biz.tableaudashboard;

import java.util.List;

import gov.hhs.fda.ctp.common.beans.TableauDashboardRptType;

public interface TableauDashboardBizComp {
	
	public List<TableauDashboardRptType> getDashboardReportTypes();

}

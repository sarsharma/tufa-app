package gov.hhs.fda.ctp.biz.tableaudashboard;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.TableauDashboardRptType;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.persistence.TableauDashboardRptsEntityManager;
import gov.hhs.fda.ctp.persistence.model.TableauDashboardRptTypeEntity;

@Service("tableauDashboardBizComp")
@Transactional
public class TableauDashboardBizCompImpl implements TableauDashboardBizComp {

	
	@Autowired
	private TableauDashboardRptsEntityManager tableauDashboardRptsEntityManager;

	@Autowired
	private MapperWrapper mapperWrapper;
	
	@Override
	public List<TableauDashboardRptType> getDashboardReportTypes() {
		
		Mapper mapper = mapperWrapper.getMapper();
		
		List<TableauDashboardRptType> tbdTypes = new ArrayList<>();
		
		List<TableauDashboardRptTypeEntity>   tbdTypesEntities = this.tableauDashboardRptsEntityManager.getTableauDashboardReportTypes();
		
		for(TableauDashboardRptTypeEntity entity : tbdTypesEntities){
			TableauDashboardRptType tbdType = new TableauDashboardRptType();
			mapper.map(entity,tbdType);
			tbdTypes.add(tbdType);
		}
		
		return tbdTypes;
	}

	public TableauDashboardRptsEntityManager getTableauDashboardRptsEntityManager() {
		return tableauDashboardRptsEntityManager;
	}

	public void setTableauDashboardRptsEntityManager(TableauDashboardRptsEntityManager tableauDashboardRptsEntityManager) {
		this.tableauDashboardRptsEntityManager = tableauDashboardRptsEntityManager;
	}

}

/**
 * 
 */
package gov.hhs.fda.ctp.biz.test;

import java.util.List;
import java.util.Map;

import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.PermitPeriod;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;

/**
 * The Interface TestBizComp.
 *
 * @author tgunter
 */
public interface TestBizComp {
    
    /**
     * Setup.
     *
     * @return the company entity
     */
    public CompanyEntity setup();
    
    /**
     * Tear down.
     *
     * @param companyId the companyId
     */
    public void tearDown(long companyId);
    
    /**
     * Tear down.
     *
     * @param companyName the companyName
     */
    public void tearDownByCompanyName(String companyName);
    
    /**
     * Convert to DTO.
     *
     * @param companyEntity the company entity
     * @return the company
     */
    public Company convertToDTO(CompanyEntity companyEntity);
    
    /**
     * Gets the permit period list.
     *
     * @param company the company
     * @return the permit period list
     */
    public List<PermitPeriod> getPermitPeriodList(Company company);
    
    /**
     * Gets the tobacco parent map.
     *
     * @return the tobacco parent map
     */
    public Map<Long, Long> getTobaccoParentMap(long periodId);
}

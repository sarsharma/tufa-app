/**
 * 
 */
package gov.hhs.fda.ctp.biz.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitPeriod;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManager;
import gov.hhs.fda.ctp.persistence.TestEntityManager;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;

/**
 * The Class TestBizCompImpl.
 *
 * @author tgunter
 */
@Service
@Transactional
public class TestBizCompImpl implements TestBizComp {
    
    /** The reference code entity manager. */
    @Autowired
    private ReferenceCodeEntityManager referenceCodeEntityManager;
    
    /** The test entity manager. */
    @Autowired
    private TestEntityManager testEntityManager;
    
    /** The mapper factory. */
    @Autowired
    DozerBeanMapperFactoryBean mapperFactory;

    @Autowired
	private MapperWrapper mapperWrapper;
    
    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.biz.test.TestBizComp#setup()
     */
    @Override
    public CompanyEntity setup() {
        return testEntityManager.setup();
    }

    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.biz.test.TestBizComp#tearDown(gov.hhs.fda.ctp.persistence.model.CompanyEntity)
     */
    @Override
    public void tearDown(long companyId) {
        testEntityManager.tearDown(companyId);
    }
    
    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.biz.test.TestBizComp#tearDownByCompanyName(gov.hhs.fda.ctp.persistence.model.CompanyEntity)
     */
    @Override
    public void tearDownByCompanyName(String companyName) {
        testEntityManager.tearDownByCompanyName(companyName);
    }    

    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.biz.test.TestBizComp#convertToDTO(gov.hhs.fda.ctp.persistence.model.CompanyEntity)
     */
    @Override
    public Company convertToDTO(CompanyEntity companyEntity) {
        Company company = new Company();
        Mapper mapper = mapperWrapper.getMapper();
        mapper.map(companyEntity, company);
        for(Address address : company.getAddressSet()) {
            if("PRIM".equals(address.getAddressTypeCd())) {
                address.setCompanyId(company.getCompanyId());
                company.setPrimaryAddress(address);
            }
            else {
                address.setCompanyId(company.getCompanyId());
                company.setAlternateAddress(address);
            }
        }
        return company;
    }
    
    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.biz.test.TestBizComp#getPermitPeriodList(gov.hhs.fda.ctp.common.beans.Company)
     */
    @Override
    public List<PermitPeriod> getPermitPeriodList(Company company) {
        Mapper mapper = mapperWrapper.getMapper();
        List<PermitPeriod> permitPeriods = new ArrayList<>();
        Set<Permit> permits = company.getPermitSet();
        for(Permit permit : permits) {
            List<PermitPeriodEntity> permitPeriodEntities = testEntityManager.getPermitPeriods(permit.getPermitId());
            for(PermitPeriodEntity permitPeriodEntity : permitPeriodEntities) {
                PermitPeriod permitPeriod = new PermitPeriod();
                mapper.map(permitPeriodEntity, permitPeriod);
                permitPeriods.add(permitPeriod);
            }
        }
        return permitPeriods;
    }
    
    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.biz.test.TestBizComp#getTobaccoParentMap()
     */
    @Override
    public Map<Long, Long> getTobaccoParentMap(long periodId) {
        return referenceCodeEntityManager.getTobaccoParentMap(periodId);
    }
}

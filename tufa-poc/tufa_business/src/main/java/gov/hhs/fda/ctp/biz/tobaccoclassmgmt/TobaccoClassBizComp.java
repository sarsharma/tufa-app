package gov.hhs.fda.ctp.biz.tobaccoclassmgmt;

import java.util.List;

import gov.hhs.fda.ctp.persistence.model.TobaccoClassEntity;

public interface TobaccoClassBizComp {
	public List<TobaccoClassEntity> getTobaccoClasses();
}

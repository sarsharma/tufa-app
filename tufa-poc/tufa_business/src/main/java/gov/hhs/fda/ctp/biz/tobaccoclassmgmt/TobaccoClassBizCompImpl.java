package gov.hhs.fda.ctp.biz.tobaccoclassmgmt;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.persistence.StaticDataEntityManager;
import gov.hhs.fda.ctp.persistence.model.TobaccoClassEntity;

@Service
@Transactional
public class TobaccoClassBizCompImpl implements TobaccoClassBizComp {

	@Autowired
	private StaticDataEntityManager staticDataEntityManager;
	
	public List<TobaccoClassEntity> getTobaccoClasses() {
		return this.staticDataEntityManager.getTobaccoTypes();
	}
}

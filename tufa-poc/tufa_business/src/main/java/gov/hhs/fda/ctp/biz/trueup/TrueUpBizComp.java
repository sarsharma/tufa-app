/**
 *
 */
package gov.hhs.fda.ctp.biz.trueup;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import gov.hhs.fda.ctp.common.beans.AnnImporterGrid;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.Attachment;
import gov.hhs.fda.ctp.common.beans.CBPAmendment;
import gov.hhs.fda.ctp.common.beans.CBPEntry;
import gov.hhs.fda.ctp.common.beans.CBPImporter;
import gov.hhs.fda.ctp.common.beans.CBPIngestestedSummaryDetailsData;
import gov.hhs.fda.ctp.common.beans.CBPLineEntryUpdateInfo;
import gov.hhs.fda.ctp.common.beans.CBPMetrics;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.CompanyReallocationBeanToUpdate;
import gov.hhs.fda.ctp.common.beans.ComparisonAllDeltaComments;
import gov.hhs.fda.ctp.common.beans.ComparisonAllDeltaStatus;
import gov.hhs.fda.ctp.common.beans.DocExport;
import gov.hhs.fda.ctp.common.beans.ImporterTufaIngestionDetails;
import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.beans.IngestionWorkflowContext;
import gov.hhs.fda.ctp.common.beans.ManufactureQuarterDetail;
import gov.hhs.fda.ctp.common.beans.PreviousTrueupInfo;
import gov.hhs.fda.ctp.common.beans.TTBAmendment;
import gov.hhs.fda.ctp.common.beans.TobaccoTypeTTBVol;
import gov.hhs.fda.ctp.common.beans.TrueUp;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionHTSCodeSummary;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonSummary;
import gov.hhs.fda.ctp.common.beans.TrueUpErrorsExport;
import gov.hhs.fda.ctp.common.beans.TrueUpFilter;
import gov.hhs.fda.ctp.common.beans.UnknownCompanyBucket;
import gov.hhs.fda.ctp.persistence.model.CBPImporterEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonAllDeltaStatusEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonCommentAttachmentEntity;
import gov.hhs.fda.ctp.persistence.model.HTSCodeEntity;
import gov.hhs.fda.ctp.persistence.model.TTBPermitBucket;
import gov.hhs.fda.ctp.persistence.model.TrueUpDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpSubDocumentEntity;

/**
 * @author tgunter
 *
 */
public interface TrueUpBizComp {
	/**
	 * Create a new true up.
	 * @param trueUpEntity
	 * @return (String)result
	 */
	public String createTrueUp(TrueUpEntity trueUpEntity);

	/**
	 * Get the list of annual true-ups.
	 * @param filter
	 * @return List<TrueUpEntity>
	 */
	public List<TrueUp> getTrueUps(TrueUpFilter filter);
	
	/**
	 * Get the list of true up data.
	 * @param filter
	 * @return List<TrueUpEntity>
	 */
	public List<TrueUpEntity> getAllTrueUps();

	/**
	 * Get the true up by fiscal year.
	 * @param fiscalYear
	 * @return
	 */
	public TrueUp getTrueUpByYear(long fiscalYear);
	
	/**
	 * Generate annual market share.
	 * @param fiscalYear
	 * @param submit
	 */
	public void generateAnnualMarketShare(int fiscalYear, String submit);

	/**
	 * Ingest the list of TTBUpload records.
	 * @param ingestion
	 */
	public void ingestTTB(IngestionOutput output, TrueUpDocumentEntity document) throws Exception;

	/**
	 * Ingest the list of CBPUpload records.
	 * @param ingestion
	 * @throws Exception 
	 */
	public void ingestCBP(IngestionOutput output, TrueUpDocumentEntity document) throws Exception;
	
	/**
	 * IngestCBPLineItem
	 * @param output
	 * @param document
	 * @throws Exception
	 */
	public void ingestCBPLineItem(IngestionOutput output, TrueUpDocumentEntity document) throws Exception;
	
	
	/**
	 * Get a csv export of the ingestion errors.
	 * @param fiscalYear
	 * @param type
	 */
	public TrueUpErrorsExport getTrueUpErrors(long fiscalYear, String type);

	/*
	 * Get all the true up documents for the fifscal year
	 */
	public List<TrueUpDocumentEntity> getTrueupDocuments(long fiscalYear);

    List<HTSCodeEntity> getHtsCodes();

	void saveOrEditCodes(HTSCodeEntity htsCode);

	HTSCodeEntity getCodeById(String htscode);

	/**
	 * Get list of importers for company comparison bucket.
	 * @param fiscalYear
	 * @return
	 */
	public List<CBPImporter> getCompanyComparisonBucket(Long fiscalYear);

	/**
	 * Save the decisions made in the company comparison bucket.
	 * @param bucket
	 */
	public List<CBPImporterEntity> updateCompanyComparisonBucket(List<CBPImporter> bucket);

    public List<TrueUpSubDocumentEntity> getSupportDocs(long fiscalYr);

    public void saveSupportDocument(TrueUpSubDocumentEntity supDoc);

    public TrueUpSubDocumentEntity getDocByDocId(long trueupSubdocId);

	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType	 * MANU or IMPT
	 * @return
	 */
	public List<TrueUpComparisonSummary> getCigarComparisonSummary(long companyId, long fiscalYear, String permitType);

	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param quarter
	 * @param permitType	 * MANU or IMPT
	 * @param tobaccoClass
	 * @return
	 */
	public List<TrueUpComparisonSummary> getNonCigarComparisonSummary(long companyId, long fiscalYear, long quarter, String permitType, String tobaccoClass);


	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType	 * MANU or IMPT
	 * @return
	 */
	public List<TrueUpComparisonImporterDetail> getImporterComparisonDetails(String ein, long fiscalYear, long quarter, String permitType, String tobaccoClass);

	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType	 * MANU or IMPT
	 * @return
	 */
	public List<TrueUpComparisonImporterIngestionDetail> getImporterComparisonIngestionDetails(String ein, long fiscalYear, long quarter, String permitType, String tobaccoClass);

	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType	 * MANU or IMPT
	 * @return
	 */
	public List<TrueUpComparisonImporterIngestionHTSCodeSummary> getImporterComparisonIngestionHTSCodeSummary(String ein, long fiscalYear, long quarter, String permitType, String tobaccoClass);
	
	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param quarter
	 * @param permitType	 * MANU or IMPT
	 * @param tobaccoClass
	 * @return
	 */
	public List<TrueUpComparisonManufacturerDetail> getManufacturerComparisonDetails(long companyId, long fiscalYear, long quarter, String permitType, String tobaccoClass);


	/**
	 *
	 * @param fiscalYear
	 * @return
	 */
	public List<TrueUpComparisonResults> getComparisonResults(long fiscalYear, TrueUpCompareSearchCriteria csc);

	/**
	 *
	 * @param fiscalYear
	 * @return
	 */
	public List<TrueUpComparisonResults> getComparisonSingleResults(long fiscalYear, TrueUpCompareSearchCriteria csc);

	
	/**
	 *
	 * @param fiscalYear
	 * @return
	 */
	public List<TrueUpComparisonResults> getAllComparisonResults(long fiscalYear, TrueUpCompareSearchCriteria csc);	

	/**
	 *
	 * @param fiscalYear
	 * @return
	 */
	public List<TrueUpComparisonResults> getAllComparisonSingleResults(long fiscalYear, TrueUpCompareSearchCriteria csc);	

	/**
 	*/
	public List<CBPEntry> getHTSMissingAssociationData(long fiscalYear);

	/**
	 * Get associate HTS codes bucket.
	 * @param fiscalYear
	 * @return
	 */
	public List<CBPEntry> getAssociateHTSBucket(long fiscalYear);

	/**
	 *
	 * @param fiscalYear
	 * @return
	 */
	public List<UnknownCompanyBucket> getUnknownCompanies(long fiscalYr);

	public List<TTBPermitBucket> getTTBPermits(long fiscalYr);

	public void updateTTBPermits(List<TTBPermitBucket> ttbpermits);

	public void ingestTTBRemovals(IngestionOutput output, TrueUpDocumentEntity document) throws Exception;

	//public List<CBPEntry> saveHTSMissingAssociationData(List<CBPEntry> cbpEntries,long fiscalYear);

	public List<CBPEntry> updateAssociateHTSBucket(List<CBPEntry> entries, long fiscalYear);

	public List<CBPEntry> updateAssociateHTSMissingBucket(List<CBPEntry> entries, long fiscalYear);

	public List<CompanyIngestion> getIncludeCompaniesInMktSh(long fiscalYear);
	
	
	public void updateIncludeCompaniesInMktSh(List<CompanyIngestion> cbpEntries, long fiscalYear);
	
	public void deleteIngestionFile(long fiscalYr, String doctype);
	
	public List<TTBAmendment> getTTBAmendments(long fiscalYr,long companyId, List<TTBAmendment> tobaccoTypes);

	public List<TTBAmendment> saveTTBAmendment(List<TTBAmendment> amendments);

	List<TTBAmendment> saveTTBCompareComments( List<TTBAmendment> amendments);
	List<TTBAmendment> saveTTBCompareComments( List<TTBAmendment> amendments, String ein, String tobaccoClass);

	List<TTBAmendment> updateTTBCompareFDAAcceptFlag( List<TTBAmendment> amendments);
	
	public List<CBPAmendment> getCBPAmendments(long fiscalYr,long companyId, List<CBPAmendment> tobaccoTypes);

	public List<CBPAmendment> saveCBPAmendment(List<CBPAmendment> amendments);

	public List<CBPAmendment> saveCBPCompareComments( List<CBPAmendment> amendments);

	public List<CBPAmendment> updateCBPCompareFDAAcceptFlag( List<CBPAmendment> amendments);
	
	public CompanyReallocationBeanToUpdate updateCBPIngestedLineEntryInfo( List<CBPLineEntryUpdateInfo> updateinfo);
	
	public Map<String, String> fetchFDA352MAN(long fiscalYr, long company_id, String classNm);
	
	public Map<String, String> fetchFDA352IMP(long fiscalYr, long company_id , String classNm,String ein);
	
	public Map<String, String> fetchFDA352IMPSingle(long fiscalYr, long company_id, String classNm,String ein);
	
	public Map<String, String> fetchOneSided(long fiscalYr, long company_id, String classNm, String reportType, String ein);

	public AssessmentExport getCBPExport(int year, int quarter,String TobaccoClass,String ein);

	public AssessmentExport getCBPNewCompanyExport(int year) throws SQLException, IOException;

	public CompanyIngestion getCompanyToIncldInMktSh(long fiscalYear,String ein, String type);
	
	//public void updateDeltaAssotiation(TrueUpAllDeltaAssociation deltaAssociation);

	public ComparisonAllDeltaStatusEntity setCompareAllDeltaStatus(List<TrueUpComparisonResults> model);
	
	public ComparisonAllDeltaStatusEntity setCompareAllDeltaStatusSingle(List<TrueUpComparisonResults> model);

	public Boolean checkIfAssociationExists(String ein, long fiscalYear, String tobaccoClass, long qtr);

	public TrueUpComparisonResults getAllComparisonResultsCount(long fiscalYr,String inComingPage);
	
	public TrueUpComparisonResults getAllComparisonSingleResultsCount(long fiscalYr,String inComingPage);

	public List<TobaccoTypeTTBVol> getTTBVolumeTaxPerTobaccoType(TobaccoTypeTTBVol ttbVol);

	public List<TrueUpComparisonResults> getDeltasAffectedByCompareAllAction(long fiscalYr, String ein, String allAction);
	
	public List<TrueUpComparisonResults> getDeltasAffectedByCompareAllActionSingle(long fiscalYr, String ein, String allAction);
	
	public DocExport getDeltaRawDataExport(long fiscalYr,String ein, String qtr,String tobaccoClassNm, String permitType, boolean showOriginalAssociation );

	public void deleteCBPCompareComment(List<CBPAmendment> cbpAmendments);

	public void  deleteTTBCompareComment(List<TTBAmendment> amends);
	
	public DocExport getExportData(long fiscalYear, TrueUpCompareSearchCriteria csc , String selCompareMethod);
	
	public DocExport getExportDataSingle(long fiscalYear, TrueUpCompareSearchCriteria csc , String selCompareMethod);

	public CBPMetrics generateMetricData(long fiscalYr);
	
	public CBPMetrics getGeneratedMetricData(long fiscalYr);
	
	public DocExport getUnmatchedRecordExport(long fiscalYr, String recordType);

	public CBPMetrics insertAcceptedCBPLegacyData(long fiscalYr);

	public CBPIngestestedSummaryDetailsData getImporterSummDetailData(String einType, String ein, long fiscalYear, String quarter,
			String permitType, String tobaccoClass);
	
	public DocExport exportSummaryOfIngestedData(String fileType, String companyId, String fiscalYear, String quarter, String tobaccoType, String tobaccoType2, String IngestionType);
	
	public List<ComparisonAllDeltaStatus> getallDeltaComments(long fiscalYr, String ein, String fiscal_qtr, String tobacco_class_nm,String permit_type);

	public ComparisonAllDeltaComments updateDeltaComments(long commentSeq,
			ComparisonAllDeltaComments comment);

	AnnImporterGrid getImporterComparisonSummary(long companyId, long fiscalYear, long quarter,
			String tobaccoClass);

	public List<CBPEntry> getCBPIngestedLineEntryInfo(long cbEntryId);
	
	public List<IngestionWorkflowContext> saveorUpdateIngestionWorkflowContext(IngestionWorkflowContext workflowModel);

	public List<IngestionWorkflowContext> getIngestionWorkflowContext(long fiscal_yr);
	
	/**
     * @param companyId
     * @param fiscalYr
     * @param quarter
     * @param tobaccoClass
     */
    public ImporterTufaIngestionDetails getImporterCompareDetailsAllTobaccoType(long companyId,long fiscalYr,long quarter,String permitType,String tobaccoClass, String ein);

	public void ingestCBPDetailsSingleFileWF(IngestionOutput output, TrueUpDocumentEntity document) throws Exception;

	public List<IngestionWorkflowContext> getIngestionContexts();
	
    public void saveOrUpdateDateSelection(String ein, long fiscalYr, String tobaccoClass, String selDateType) throws ParseException;

    public String getImporterCompareDetailsSelDate(String tobaccoClass, long fiscalYr,String ein);

	public ManufactureQuarterDetail getManufacturerComparisonDetails(long companyId, long fiscalYear,
			long quarter, String tobaccoClass);
    
    public List<TrueUpComparisonResults> getAllDeltasDetailsForEIN(String ein,String permitType,long fiscalYear);
    
    public List<String> getAllNonZeroDeltaTobaccoClass(String ein,String permitType,long fiscalYear);
    
    public List<CBPAmendment> saveCBPCompareComments( List<CBPAmendment> amendments, String ein);
    
    public void deleteCompareCommentDetailOnly(long commSeqCbp,long commSeqAllDelta, String permitType);
	
	public void updateCompareCommentDetailOnly(long commSeqCbp,long commSeqAllDelta, String usercomment, String permitType);
	

	public List<ComparisonCommentAttachmentEntity> findCommentDocbyId(long cbpCommentSeq, long ttbCommentSeq, long allDeltaCommentSeq);
	
	public void deleteDocById(long docId);
	
	public byte[] getComparisonCommentAttachmenttAsByteArray(long docId) throws  SQLException;
	
	public Attachment comparisonCommentAttachmentDownload(long docId) throws SQLException;
	public void addComparisonCommentAttach(ComparisonCommentAttachmentEntity comparisoncommentattachment, MultipartFile file)throws IOException, SQLException;
	
	public void processAdjustments(String fiscalYaer);
	
	public List<PreviousTrueupInfo> getPreviousTrueupInfo(String type,long currentYear,String ein);

	List<Long> getFiscalYearsWithNewDeltas();

    
}

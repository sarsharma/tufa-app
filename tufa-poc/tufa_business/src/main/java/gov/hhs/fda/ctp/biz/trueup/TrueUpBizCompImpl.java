/**
 *
 */
package gov.hhs.fda.ctp.biz.trueup;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Collections2;

import gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp;
import gov.hhs.fda.ctp.common.beans.AnnImporterGrid;
import gov.hhs.fda.ctp.common.beans.Assessment;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.Attachment;
import gov.hhs.fda.ctp.common.beans.CBPAmendment;
import gov.hhs.fda.ctp.common.beans.CBPDetailsFile;
import gov.hhs.fda.ctp.common.beans.CBPEntry;
import gov.hhs.fda.ctp.common.beans.CBPExportDetail;
import gov.hhs.fda.ctp.common.beans.CBPImporter;
import gov.hhs.fda.ctp.common.beans.CBPIngestestedSummaryDetailsData;
import gov.hhs.fda.ctp.common.beans.CBPLineEntryUpdateInfo;
import gov.hhs.fda.ctp.common.beans.CBPMetrics;
import gov.hhs.fda.ctp.common.beans.CBPNewCompanies;
import gov.hhs.fda.ctp.common.beans.CBPQtrComment;
import gov.hhs.fda.ctp.common.beans.CBPSummaryFile;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.CompanyReallocationBeanToUpdate;
import gov.hhs.fda.ctp.common.beans.ComparisonAllDeltaComments;
import gov.hhs.fda.ctp.common.beans.ComparisonAllDeltaStatus;
import gov.hhs.fda.ctp.common.beans.DocExport;
import gov.hhs.fda.ctp.common.beans.DocUpload;
import gov.hhs.fda.ctp.common.beans.ExcludePermitDetails;
import gov.hhs.fda.ctp.common.beans.ImporterDeltaDetails;
import gov.hhs.fda.ctp.common.beans.ImporterTufaIngestionDetails;
import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.beans.IngestionWorkflowContext;
import gov.hhs.fda.ctp.common.beans.ManufactureQuarterDetail;
import gov.hhs.fda.ctp.common.beans.PreviousTrueupInfo;
import gov.hhs.fda.ctp.common.beans.TTBAmendment;
import gov.hhs.fda.ctp.common.beans.TTBQtrComment;
import gov.hhs.fda.ctp.common.beans.TTBRemovalsUpload;
import gov.hhs.fda.ctp.common.beans.TTBUpload;
import gov.hhs.fda.ctp.common.beans.TobaccoTypeTTBVol;
import gov.hhs.fda.ctp.common.beans.TrueUp;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetailPeriod;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetailPeriod;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionHTSCodeSummary;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManuDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManuIngestionDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetailPermit;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonSummary;
import gov.hhs.fda.ctp.common.beans.TrueUpErrorsExport;
import gov.hhs.fda.ctp.common.beans.TrueUpFilter;
import gov.hhs.fda.ctp.common.beans.UniqueCBPEntryKey;
import gov.hhs.fda.ctp.common.beans.UnknownCompanyBucket;
import gov.hhs.fda.ctp.common.comparators.CBPEntryByHTSComparator;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.common.util.CsvStringBuilder;
import gov.hhs.fda.ctp.common.util.EinBuilder;
import gov.hhs.fda.ctp.common.util.PermitBuilder;
import gov.hhs.fda.ctp.persistence.CBPLegacyEntityManager;
import gov.hhs.fda.ctp.persistence.MetricsEntityManager;
import gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.StaticDataEntityManager;
import gov.hhs.fda.ctp.persistence.TrueUpEntityManager;
import gov.hhs.fda.ctp.persistence.model.CBPAmendmentEntity;
import gov.hhs.fda.ctp.persistence.model.CBPDateConfigEntity;
import gov.hhs.fda.ctp.persistence.model.CBPEntryEntity;
import gov.hhs.fda.ctp.persistence.model.CBPImporterEntity;
import gov.hhs.fda.ctp.persistence.model.CBPLineItemEntity;
import gov.hhs.fda.ctp.persistence.model.CBPMetricsEntity;
import gov.hhs.fda.ctp.persistence.model.CBPSummaryFileEntity;
import gov.hhs.fda.ctp.persistence.model.CBPUploadEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonAllDeltaStatusEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonCommentAttachmentEntity;
import gov.hhs.fda.ctp.persistence.model.HTSCodeEntity;
import gov.hhs.fda.ctp.persistence.model.IngestionWorkflowContextEntity;
import gov.hhs.fda.ctp.persistence.model.RawIngested;
import gov.hhs.fda.ctp.persistence.model.TTBAmendmentEntity;
import gov.hhs.fda.ctp.persistence.model.TTBCompanyEntity;
import gov.hhs.fda.ctp.persistence.model.TTBPermitBucket;
import gov.hhs.fda.ctp.persistence.model.TTBPermitEntity;
import gov.hhs.fda.ctp.persistence.model.TTBRemovalsEntity;
import gov.hhs.fda.ctp.persistence.model.TTBUploadEntity;
import gov.hhs.fda.ctp.persistence.model.TobaccoClassEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpSubDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.TufaEntity;
/**
 * @author tgunter
 *
 */
@Service
@Transactional
public class TrueUpBizCompImpl implements TrueUpBizComp {
	/** The logger. */
	Logger logger = LoggerFactory.getLogger(TrueUpBizCompImpl.class);

	/** The entity layer */
	@Autowired
	private TrueUpEntityManager trueUpEntityManager;

	@Autowired
	private ReferenceCodeEntityManager refCodeEntityManager;

	@Autowired
	private SimpleSQLManager simpleSQLManager;

	@Autowired
	private StaticDataEntityManager staticDataEntityManager;

	@Autowired
	private AssessmentBizComp assessmentBizComp;

	@Autowired
	private MapperWrapper mapperWrapper;

	@Autowired
	private MetricsEntityManager metricsEntityManager;

	@Autowired
	private CBPLegacyEntityManager CBPLegacyEntityManager;
	
	private static final String NA_TOTAL = "NA";
	
	

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * gov.hhs.fda.ctp.biz.trueup.TrueUpBizComp#createTrueUp(gov.hhs.fda.ctp.
	 * persistence.model.TrueUpEntity)
	 */
	@Override
	public String createTrueUp(TrueUpEntity trueUpEntity) {
		return trueUpEntityManager.createTrueUp(trueUpEntity);
	}

	@Override
	public List<TrueUp> getTrueUps(TrueUpFilter filter) {
		Mapper mapper = mapperWrapper.getMapper();
		List<TrueUp> trueUpBeans = new ArrayList<>();
		List<TrueUpEntity> trueUpEntities = trueUpEntityManager.getTrueUps(filter);
		if (trueUpEntities != null) {
			for (TrueUpEntity entity : trueUpEntities) {
				TrueUp trueUp = new TrueUp();
				/*
				 * Created a custom mapper for the recon landing page. Mapping
				 * the assessments was causing major lag. Assessment are not
				 * needed for this call. The custom mapper is located in
				 * dozerBeanMapping.xml
				 */
				mapper.map(entity, trueUp, "trueUpExcludeAssesments");
				if (entity.getSubmittedDt() == null) {
					trueUp.setSubmittedInd("Not Submitted");
				} else {
					trueUp.setSubmittedInd("Submitted");
				}
				trueUpBeans.add(trueUp);
			}
		}
		return trueUpBeans;
	}

	@Override
	public void generateAnnualMarketShare(int fiscalYear, String submit) {
		/**/
		TrueUpEntity trueup = trueUpEntityManager.getTrueUpByYear(fiscalYear);
		if ("Y".equals(submit)) {
			trueup.setSubmittedDt(new Date());
			if ("N".equals(trueup.getSubmittedInd())) {
				trueup.setSubmittedInd("Y");
				trueUpEntityManager.updateTrueUp(trueup);
				this.trueUpEntityManager.generateAnnualMarketShare(fiscalYear);
			} else {
				// assessment already submitted. do not re-submit.
				// ignore this action.
			}
		}
		if ("N".equals(submit)) {
			// user simply wants to generate a new temporary market share.
			if ("Y".equals(trueup.getSubmittedInd())) {
				trueup.setSubmittedInd("N");
				trueUpEntityManager.updateTrueUp(trueup);
			}
			this.trueUpEntityManager.generateAnnualMarketShare(fiscalYear);

		}
	}

	@Override
	public TrueUp getTrueUpByYear(long fiscalYear) {
		TrueUpEntity entity = trueUpEntityManager.getTrueUpByYear(fiscalYear);
		TrueUp trueUp = new TrueUp();
		trueUp.setCreatedBy(entity.getCreatedBy());
		trueUp.setFiscalYear(fiscalYear);
		trueUp.setModifiedBy(entity.getModifiedBy());
		trueUp.setSubmittedDt(entity.getSubmittedDt());
		List<Assessment> assessments = new ArrayList<>();
		// get first three quarters
		for (int quarter = 1; quarter <= 4; quarter++) {
			Assessment assessment = assessmentBizComp.getAnnualAssessment(quarter, (int) fiscalYear);
			assessments.add(assessment);
		}
		// done set the assessments on the trueup.
		trueUp.setAssessments(assessments);
		
		trueUp.initialiazeVersions();
		return trueUp;
	}

	@Override
	public void ingestTTB(IngestionOutput output, TrueUpDocumentEntity document) throws Exception{
		Long fiscalYr = output.getFiscalYear();
		if(simpleSQLManager.isIngestedLinedPresentForFiscalYear(fiscalYr,Constants.TTB)) throw new Exception("Ingested TTB Tax already present for fiscal year "+fiscalYr);
		Long errorCount = output.getErrorCount();
		List<DocUpload> ingestion = output.getOutput();
		Mapper mapper = mapperWrapper.getMapper();
		List<TufaEntity> ttbList = new ArrayList<>();
		Long index = 1L;
		for (DocUpload record : ingestion) {
			TTBUpload ttbBean = (TTBUpload) record;
			if (ttbBean.getFiscalYr().longValue() == fiscalYr.longValue()
					|| !CTPUtil.stringNullOrEmpty(ttbBean.getErrors())) {
				TTBUploadEntity ttb = new TTBUploadEntity();
				mapper.map(record, ttb);
				ttb.setTtbUploadId(index++);
				ttb.setFiscalYr(fiscalYr);
				// add the record if it's still marked for include
				ttbList.add(ttb);
			}
		}
		trueUpEntityManager.stageIngestionData(ttbList, Constants.TTB);
		if (errorCount == 0) {
			trueUpEntityManager.loadTTB(fiscalYr);
			trueUpEntityManager.uploadDocument(output.getFiscalYear(), document);
		}
	}

	@Override
	public void ingestCBP(IngestionOutput output, TrueUpDocumentEntity document) throws Exception {
		Long fiscalYr = output.getFiscalYear();
		Long errorCount = output.getErrorCount();
		List<DocUpload> ingestion = output.getOutput();
		Mapper mapper = mapperWrapper.getMapper();
		List<CBPSummaryFileEntity> cbpList = new ArrayList<>();
		Long index = 1L;
		boolean include;
		for (DocUpload record : ingestion) {
			include = true;
			CBPSummaryFileEntity cbp = new CBPSummaryFileEntity();
			mapper.map(record, cbp);
			cbp.setCbpSummaryId(index++);
			cbp.setFiscalYr(fiscalYr);
			cbp.setcreated_date(new Date());
			cbpList.add(cbp);

		}
		// trueUpEntityManager.stageIngestionData(cbpList, Constants.CBP);
		trueUpEntityManager.stageCBPSummaryFile(cbpList, fiscalYr);

		if (errorCount == 0) {
			// trueUpEntityManager.loadCBP(fiscalYr);
			trueUpEntityManager.uploadDocument(output.getFiscalYear(), document);
		}

	}
	
	
	@Override
	public void ingestCBPDetailsSingleFileWF(IngestionOutput output, TrueUpDocumentEntity document) throws Exception {
		
		Long fiscalYr = output.getFiscalYear();
		Long errorCount = output.getErrorCount();
		List<DocUpload> ingestion = output.getOutput();
		Mapper mapper = mapperWrapper.getMapper();
		List<CBPLineItemEntity> cbpList = new ArrayList<>();
//		List<TufaEntity> cbpList = new ArrayList<>();
		Long index = 1L;
		for (DocUpload record : ingestion) {
			CBPLineItemEntity cbplineItemEntity = new CBPLineItemEntity();
			mapper.map(record, cbplineItemEntity);
			cbplineItemEntity.setLineItemId(index++);
			cbplineItemEntity.setFiscalYr(fiscalYr);
			cbplineItemEntity.setCreated_date(new Date());
			// include if it matches all the criteria
			cbpList.add(cbplineItemEntity);

		}
		trueUpEntityManager.stageCBPLineItemFile(cbpList, fiscalYr);
//		trueUpEntityManager.stageIngestionData(cbpList, Constants.CBP);

		if (errorCount == 0) {
			trueUpEntityManager.loadCBPSingleFileWF(fiscalYr);
			trueUpEntityManager.uploadDocument(output.getFiscalYear(), document);
		}

	}

	@Override
	public void ingestCBPLineItem(IngestionOutput output, TrueUpDocumentEntity document) throws Exception {
		
		Long fiscalYr = output.getFiscalYear();
		Long errorCount = output.getErrorCount();
		List<DocUpload> ingestion = output.getOutput();
		Mapper mapper = mapperWrapper.getMapper();
		List<CBPLineItemEntity> cbpList = new ArrayList<>();
		Long index = 1L;
		for (DocUpload record : ingestion) {
			CBPLineItemEntity cbplineItemEntity = new CBPLineItemEntity();
			mapper.map(record, cbplineItemEntity);
			cbplineItemEntity.setLineItemId(index++);
			cbplineItemEntity.setFiscalYr(fiscalYr);
			cbplineItemEntity.setCreated_date(new Date());
			// include if it matches all the criteria
			cbpList.add(cbplineItemEntity);

		}
		trueUpEntityManager.stageCBPLineItemFile(cbpList, fiscalYr);

		if (errorCount == 0) {
			trueUpEntityManager.uploadDocument(output.getFiscalYear(), document);
		}

	}

	/**
	 * Test cbp record for inclusion based on dates.
	 *
	 * @param entryDate
	 * @param importDate
	 * @param fiscalYr
	 */
	private boolean includeCBPDates(String entryDate, String importDate, Long fiscalYr) {
		boolean returnVal = true;
		Long entryFiscalYear = CTPUtil.getFiscalYearYYYYMMDD(entryDate);
		// Long importFiscalYear = CTPUtil.getFiscalYearYYYYMMDD(importDate);
		Long importFiscalYear = CTPUtil.getFiscalYearYYYYMMDD(importDate, 1, fiscalYr);

		if (entryFiscalYear != null && importFiscalYear != null) {
			if (!fiscalYr.equals(entryFiscalYear) && !fiscalYr.equals(importFiscalYear)) {
				returnVal = false;
			}
		} else if (entryFiscalYear != null) {
			if (!entryFiscalYear.equals(fiscalYr)) {
				returnVal = false;
			}
		} else if (importFiscalYear != null) {
			if (!importFiscalYear.equals(fiscalYr)) {
				returnVal = false;
			}
		} else
			// both dates are bad format or null, return false
			returnVal = false;
		return returnVal;
	}

	@Override
	public TrueUpErrorsExport getTrueUpErrors(long fiscalYear, String type) {

		TrueUpErrorsExport export = new TrueUpErrorsExport();

		List<TufaEntity> uploadErrors = trueUpEntityManager.getIngestionErrors(type, fiscalYear);

		if (uploadErrors != null && !uploadErrors.isEmpty()) {
			if (Constants.CBPSumm.equals(type)) {
				export = exportCBPSummaryErrors(uploadErrors);
			}
			if (Constants.CBPLine.equals(type)) {
				export = exportCBPLineItemErrors(uploadErrors);
			}
			if (Constants.TTB.equals(type)) {
				export = exportTTBErrors(uploadErrors);
			}

			if (Constants.TTB_VOLUME.equals(type)) {
				export = exportTTBRemovalsErrors(uploadErrors);
			}
		}

		return export;
	}

	private TrueUpErrorsExport exportCBPSummaryErrors(List<TufaEntity> uploadErrors) {
		CBPSummaryFileEntity cbp = (CBPSummaryFileEntity) uploadErrors.get(0);
		// build title
		StringBuilder filename = new StringBuilder("FY_");
		filename.append(cbp.getFiscalYr());
		filename.append("_CBPSummary_Errors");
		filename.append(".csv");

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine(
				"Entry Summary Number,Importer Number,Importer Name,Entry Date,Entry Type Code and Short Description,Entry Summary Date,Ultimate Consignee Number,Ultimate Consignee Name,Estimated Tax,Total Ascertained Tax Amount,Errors");
		for (TufaEntity error : uploadErrors) {
			cbp = (CBPSummaryFileEntity) error;
			csv.csvAppend(cbp.getentrySummaryNumber());
			csv.csvAppend(cbp.getImporterNumber());
			csv.csvAppendInQuotes(cbp.getImporterName());
			csv.csvAppend(cbp.getEntryDate());
			csv.csvAppendRawQuotes(cbp.getentryTypeCodeDescription());
			csv.csvAppend(cbp.getentrySummaryDate());
			csv.csvAppend(cbp.getultimateConsigneeNumber());
			csv.csvAppendInQuotes(cbp.getultimateConsigneeName());
			csv.csvAppend(cbp.getEstimatedTax());
			csv.csvAppend(cbp.gettotalAscertainedtax());
			csv.csvAppendNewLine(cbp.getError());
		}
		TrueUpErrorsExport export = new TrueUpErrorsExport();
		export.setFilename(filename.toString());
		export.setCsv(csv.toString());
		return export;
	}

	private TrueUpErrorsExport exportCBPLineItemErrors(List<TufaEntity> uploadErrors) {
		CBPLineItemEntity cbp = (CBPLineItemEntity) uploadErrors.get(0);
		// build title
		StringBuilder filename = new StringBuilder("FY_");
		filename.append(cbp.getFiscalYr());
		filename.append("_CBPLineItem_Errors");
		filename.append(".csv");

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine("Entry Summary Number,Entry Summary Date,Entry Date," + "HTS Number,Line Tariff Qty,"
				+ "Importer Number,Importer Name,Ultimate Consignee Name, Ultimate Consignee Number,"
				+ "HTS Description,Line Tariff UOM1,Line Number,Entry Type Code, Line Tariff Goods Value Amount,"
				+ "IR Tax Amount, Import Date, Errors");
		for (TufaEntity error : uploadErrors) {
			cbp = (CBPLineItemEntity) error;
			csv.csvAppend(cbp.getEntrySummaryNumber());
			csv.csvAppend(cbp.getEntrySummaryDate());
			csv.csvAppend(cbp.getEntryDate());
			csv.csvAppend(cbp.getHtsNumber());
			csv.csvAppend(cbp.getLineTarrifQty1());
			csv.csvAppend(cbp.getImporterNumber());
			csv.csvAppendInQuotes(cbp.getImporterName());
			csv.csvAppend(cbp.getUltimateConsigneeNumber());
			csv.csvAppendInQuotes(cbp.getUltimateConsigneeName());
			csv.csvAppendRawQuotes(cbp.getHtsDescription());
			csv.csvAppend(cbp.getLineTarrifUOM1());
			csv.csvAppend(cbp.getLineNumber());
			csv.csvAppendRawQuotes(cbp.getEntryTypeCode());
			csv.csvAppend(cbp.getLineTariffGoodsValueAmount());
			csv.csvAppend(cbp.getIrTaxAmount());
			csv.csvAppend(cbp.getImportDate());
			csv.csvAppendNewLine(cbp.getErrors());
		}
		TrueUpErrorsExport export = new TrueUpErrorsExport();
		export.setFilename(filename.toString());
		export.setCsv(csv.toString());
		return export;
	}

	@Override
	public List<TrueUpDocumentEntity> getTrueupDocuments(long fiscalYear) {
		return this.trueUpEntityManager.getTrueupDocuments(fiscalYear);
	}

	@Override
	public AssessmentExport getCBPNewCompanyExport(int year) throws SQLException, IOException {
		List<CBPNewCompanies> cbpNewCompanyDetails = this.simpleSQLManager.getCBPNewCompanies(year);

		String EIN = null;

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine("Importer Name, EIN , Tobacco Class, Date, Tax Amount");
		for (CBPNewCompanies detail : cbpNewCompanyDetails) {

			csv.csvAppendInQuotes(CTPUtil.removecharachters(detail.getImporterName().toString()));
			EIN = detail.getImporterEIN();
			EinBuilder einBuilder1 = new EinBuilder(detail.getImporterEIN());
			csv.csvAppend(einBuilder1.getFormattedEin());
			String newtobacco = detail.getTobaccoName().toString().replace(",", ";");
			newtobacco = newtobacco.replace("[", "");
			newtobacco = newtobacco.replace("]", "");
			csv.csvAppend(newtobacco);
			csv.csvAppend(detail.getentrysummddate());
			csv.csvAppendNewLine(detail.gettaxamount());
		}

		// build title
		StringBuilder title = new StringBuilder("Newly_Reported_Company_TrueUp_");
		title.append(year);
		title.append(".csv");

		AssessmentExport export = new AssessmentExport();
		export.setTitle(title.toString());
		export.setCsv(csv.toString());
		return export;
	}

	@Override
	public AssessmentExport getCBPExport(int year, int quarter, String TobaccoClass, String ein) {

		// get list of export records
		List<CBPExportDetail> cbpExportDetails = this.simpleSQLManager.getCBPExportData(year, quarter, TobaccoClass,
				ein);
		String tobclass = null;
		String EIN = null;

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine(
				"FY, Quarter, Importer EIN, Importer Name, Consignee EIN, Consignee Name, HTS Code, Tobacco Class, Entry No, Entry Type, Line No,  Entry Summary Date, Entry Date, QTY_1, UOM_1, Estimated Tax, Excluded,Secondary Date, Reallocated,Missing");
		for (CBPExportDetail detail : cbpExportDetails) {

			tobclass = detail.getTobaccoClass();
			EIN = detail.getImporterEIN();
			EinBuilder importerEIN = new EinBuilder(detail.getImporterEIN());
			EinBuilder consigneeEIN = new EinBuilder(detail.getConsigneeEIN());
			csv.csvAppend(detail.getFiscalYr());
			csv.csvAppend(detail.getFiscalQtr());
			csv.csvAppend(importerEIN.getFormattedEin());
			csv.csvAppendInQuotes(detail.getImporterName());
			csv.csvAppend(consigneeEIN.getFormattedEin());
			csv.csvAppendInQuotes(detail.getConsigneeName());
			csv.csvAppend(detail.getHtsCd());
			csv.csvAppend(detail.getTobaccoClass());
			csv.csvAppend(detail.getEntryNo());
			csv.csvAppend(detail.getEntryTypeCd());
			csv.csvAppend(detail.getLineNo());
			csv.csvAppend(CTPUtil.parseDateToString(detail.getEntrySummDate()));
			csv.csvAppend(CTPUtil.parseDateToString(detail.getEntryDate()));
			csv.csvAppend(detail.getQty1());
			csv.csvAppend(detail.getUom1());
			csv.csvAppend(detail.getEstimatedTax());
			csv.csvAppend(detail.getExcludedFlag());
			csv.csvAppend(detail.getSecondarydate());
			
			csv.csvAppend(detail.getReallocatedFlag());
			csv.csvAppendNewLine(detail.getMissingFlag());
			
		}

		// build title
		StringBuilder title = new StringBuilder("FY_");
		title.append(year);
		if (TobaccoClass.toUpperCase().equals("CIGARS")) {
			title.append("_" + TobaccoClass + "_");
		} else {
			title.append("_" + tobclass + "_");
		}

		title.append(EIN);
		title.append("_CBP_Export");
		title.append(".csv");

		AssessmentExport export = new AssessmentExport();
		export.setTitle(title.toString());
		export.setCsv(csv.toString());
		return export;
	}

	private TrueUpErrorsExport exportCBPErrors(List<TufaEntity> errors) {
		CBPUploadEntity cbp = (CBPUploadEntity) errors.get(0);
		// build title
		StringBuilder filename = new StringBuilder("FY_");
		filename.append(cbp.getFiscalYr());
		filename.append("_CBP_Errors");
		filename.append(".csv");

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine(
				"ENTRY NUMBER,LINE NBR,HTS,IMPORTER,IMPORTER NAME,CONSIGNEE,ENTRY DATE,ENT SUMM DATE,ESTIMATED TAX,UOM_1,Errors");
		for (TufaEntity error : errors) {
			cbp = (CBPUploadEntity) error;
			csv.csvAppend(cbp.getEntryNo());
			csv.csvAppend(cbp.getLineNo());
			csv.csvAppend(cbp.getHtsCd());
			csv.csvAppend(cbp.getImporterEIN());
			csv.csvAppendInQuotes(cbp.getImporterName());
			csv.csvAppend(cbp.getConsigneeEIN());
			csv.csvAppend(cbp.getEntryDate());
			csv.csvAppend(cbp.getEntrySummDate());
			csv.csvAppend(cbp.getEstimatedTax());
			csv.csvAppend(cbp.getUom1());
			csv.csvAppendNewLine(cbp.getErrors());
		}
		TrueUpErrorsExport export = new TrueUpErrorsExport();
		export.setFilename(filename.toString());
		export.setCsv(csv.toString());
		return export;
	}

	private TrueUpErrorsExport exportTTBRemovalsErrors(List<TufaEntity> errors) {
		TTBRemovalsEntity ttb = (TTBRemovalsEntity) errors.get(0);
		// build title
		StringBuilder filename = new StringBuilder("FY_");
		filename.append(ttb.getFiscalYr());
		filename.append("_TTB_Removals_Errors");
		filename.append(".csv");

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine("PERIOD,NAME_OWNER,ID_PERMIT,EIN,PRODUCT,POUNDS,Errors");

		for (TufaEntity error : errors) {
			ttb = (TTBRemovalsEntity) error;
			csv.csvAppend(ttb.getPeriod());
			csv.csvAppendInQuotes(ttb.getNameOwner());
			PermitBuilder permitBuilder = new PermitBuilder(ttb.getIdPermit());
			csv.csvAppend(permitBuilder.formatPermit());
			csv.csvAppend(ttb.getEin());
			csv.csvAppend(ttb.getProduct());
			csv.csvAppend(ttb.getPounds());
			csv.csvAppendNewLine(ttb.getErrors());
		}
		TrueUpErrorsExport export = new TrueUpErrorsExport();
		export.setFilename(filename.toString());
		export.setCsv(csv.toString());
		return export;
	}

	private TrueUpErrorsExport exportTTBErrors(List<TufaEntity> errors) {
		TTBUploadEntity ttb = (TTBUploadEntity) errors.get(0);
		// build title
		StringBuilder filename = new StringBuilder("FY_");
		filename.append(ttb.getFiscalYr());
		filename.append("_TTB_Errors");
		filename.append(".csv");

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine("EIN,REGISTRY_NO,ENTITY_NAME,TPBD,CT1,CT2,CT4,CT5,Errors");

		for (TufaEntity error : errors) {
			ttb = (TTBUploadEntity) error;
			PermitBuilder permitBuilder = new PermitBuilder(ttb.getPermitNum());
			csv.csvAppend(ttb.getEinNum());
			csv.csvAppend(permitBuilder.formatPermit());
			csv.csvAppendInQuotes(ttb.getCompanyNm());
			csv.csvAppend(ttb.getTpbd());
			csv.csvAppend(ttb.getChewSnuffTaxes());
			csv.csvAppend(ttb.getCigaretteTaxes());
			csv.csvAppend(ttb.getCigarTaxes());
			csv.csvAppend(ttb.getPipeRyoTaxes());
			csv.csvAppendNewLine(ttb.getErrors());
		}
		TrueUpErrorsExport export = new TrueUpErrorsExport();
		export.setFilename(filename.toString());
		export.setCsv(csv.toString());
		return export;
	}

	@Override
	public List<HTSCodeEntity> getHtsCodes() {
		return this.refCodeEntityManager.getHtsCodes();
	}

	@Override
	public void saveOrEditCodes(HTSCodeEntity htsCode) {
		this.refCodeEntityManager.saveOrEditCodes(htsCode);
	}

	@Override
	public HTSCodeEntity getCodeById(String htscode) {
		return this.refCodeEntityManager.getCodeById(htscode);
	}

	@Override
	public List<CBPImporter> getCompanyComparisonBucket(Long fiscalYear) {
		return this.simpleSQLManager.getCompanyComparisonBucket(fiscalYear);
	}

	@Override
	public List<CBPImporterEntity> updateCompanyComparisonBucket(List<CBPImporter> bucket) {
		return this.trueUpEntityManager.updateCompanyComparisonBucket(bucket);
	}

	@Override
	public List<TrueUpSubDocumentEntity> getSupportDocs(long fiscalYr) {
		return this.trueUpEntityManager.getSupportDocs(fiscalYr);
	}

	@Override
	public void saveSupportDocument(TrueUpSubDocumentEntity supDoc) {
		this.trueUpEntityManager.saveSupportDocument(supDoc);
	}

	@Override
	public TrueUpSubDocumentEntity getDocByDocId(long trueupSubdocId) {
		return this.trueUpEntityManager.getDocByDocId(trueupSubdocId);
	}

	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType
	 *            * MANU or IMPT
	 * @return
	 */
	@Override
	public List<TrueUpComparisonSummary> getCigarComparisonSummary(long companyId, long fiscalYear, String permitType) {
		return this.simpleSQLManager.getCigarComparisonSummary(companyId, fiscalYear, permitType);
	}

	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType
	 *            * MANU or IMPT
	 * @return
	 */
	@Override
	public List<TrueUpComparisonSummary> getNonCigarComparisonSummary(long companyId, long fiscalYear, long quarter,
			String permitType, String tobaccoClass) {
		if ("MANU".equals(permitType)) {
			return this.simpleSQLManager.getNonCigarManufacturerComparisonSummary(companyId, fiscalYear, quarter,
					permitType, tobaccoClass);
		}
		else return new ArrayList<TrueUpComparisonSummary>();
	}
	
	@Override
	public AnnImporterGrid getImporterComparisonSummary(long companyId, long fiscalYear, long quarter, String tobaccoClass) {
		return this.simpleSQLManager.getImporterComparisonSummary(companyId, fiscalYear, quarter, tobaccoClass);
	}
	
	
	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType
	 *            IMPT
	 * @return
	 */

	@Override
	public CBPIngestestedSummaryDetailsData getImporterSummDetailData(String einType, String ein, long fiscalYear,
			String quarter, String permitType, String tobaccoClass) {

		return this.simpleSQLManager.getImporterSummDetailData(einType, ein, fiscalYear, quarter, permitType,
				tobaccoClass);

	}

	/**
	 *
	 * @param fiscalYear
	 * @return
	 */
	@Override
	public List<TrueUpComparisonResults> getComparisonResults(long fiscalYear, TrueUpCompareSearchCriteria csc) {
		return this.simpleSQLManager.getComparisonResults(fiscalYear, csc);
	}

	/**
	 *
	 * @param fiscalYear
	 * @return
	 */
	@Override
	public List<TrueUpComparisonResults> getComparisonSingleResults(long fiscalYear, TrueUpCompareSearchCriteria csc) {
		return this.simpleSQLManager.getComparisonSingleResults(fiscalYear, csc);
	}

	
	/**
	 *
	 * @param fiscalYear
	 * @return
	 */
	@Override
	public List<TrueUpComparisonResults> getAllComparisonResults(long fiscalYear, TrueUpCompareSearchCriteria csc) {
		return this.simpleSQLManager.getAllComparisonResults(fiscalYear, csc);
	}

	/**
	 *
	 * @param fiscalYear
	 * @return
	 */
	@Override
	public List<TrueUpComparisonResults> getAllComparisonSingleResults(long fiscalYear, TrueUpCompareSearchCriteria csc) {
		return this.simpleSQLManager.getAllComparisonSingleResults(fiscalYear, csc);
	}
	
	@Override
	public List<CBPEntry> getHTSMissingAssociationData(long fiscalYear) {
		List<CBPEntry> missingHTSCodes = this.simpleSQLManager.getHTSMissingAssociationData(fiscalYear);
		return missingHTSCodes;
	}

	@Override
	/**
	 * Get the associate HTS code bucket.
	 * 
	 * @param fiscalYear
	 * @return
	 */
	public List<CBPEntry> getAssociateHTSBucket(long fiscalYear) {
		Map<String, CBPEntry> entryMap = new HashMap<>();
		List<CBPEntryEntity> associateHTSCodes = this.trueUpEntityManager.getAssociateHTSBucket(fiscalYear);
		List<CBPEntry> entries = transformCBPEntryListToBeans(associateHTSCodes);
		// now loop through the values and keep unique combinations
		for (CBPEntry entry : entries) {
			CBPEntry unique = entryMap.get(entry.getHtsCd());
			if (unique == null) {
				logger.debug("Importer:" + entry.getCbpImporter().getImporterEIN() + " Consignee:"
						+ entry.getCbpImporter().getConsigneeEIN() + " HTS CODE:" + entry.getHtsCd());
				// add company name to list
				if (entry.getTobaccoClassId() != null)
					entry.setProvidedAnswer(true);
				Set<String> companies = new TreeSet<>();
				if ("CONS".equals(entry.getCbpImporter().getAssociationTypeCd())) {
					companies.add(this.staticDataEntityManager
							.getCompanyNmByEin(entry.getCbpImporter().getConsigneeEIN()).getLegalName());
				} else {
					companies.add(entry.getCbpImporter().getImporterNm());
				}
				entry.setCompanies(companies);
				/*
				 * if (entry.getCbpImporter().getConsigneeEIN().equals(entry.
				 * getCbpImporter().getImporterEIN()) ||
				 * entry.getCbpImporter().getAssociationTypeCd() != null)
				 */

				entryMap.put(entry.getHtsCd(), entry);
			} else {
				Set<String> companies = unique.getCompanies();
				companies.add(entry.getCbpImporter().getImporterNm());
			}
		}
		List<CBPEntry> returnList = new ArrayList<>(entryMap.values());
		Collections.sort(returnList, new CBPEntryByHTSComparator());
		return returnList;

	}

	/**
	 * Save the association of HTS codes to tobacco class id.
	 * 
	 * @param entries
	 * @return
	 */
	@Override
	public List<CBPEntry> updateAssociateHTSBucket(List<CBPEntry> entries, long fiscalYear) {
		List<CBPEntry> newentries = replaceHTSTobaccoClasses(entries);

		List<CBPEntryEntity> cbpEntries = new ArrayList<>();
		
		Mapper mapper = mapperWrapper.getMapper();
		for(CBPEntry entry : newentries){
			CBPEntryEntity cbpEntryEntity = new CBPEntryEntity();
			mapper.map(entry,cbpEntryEntity);
			cbpEntries.add(cbpEntryEntity);
		}

		List<CBPEntryEntity> returnList = trueUpEntityManager.updateAssociateHTSBucket(cbpEntries,fiscalYear);
		return transformCBPEntryListToBeans(returnList);

	}

	private List<CBPEntry> replaceHTSTobaccoClasses(List<CBPEntry> entries) {
		Map<String, Long> tobaccoClassMap = refCodeEntityManager.getTobaccoClassNamesById();
		for (CBPEntry entry : entries) {
			if (entry.getTobaccoClass() != null) {
				entry.setTobaccoClassId(tobaccoClassMap.get(entry.getTobaccoClass().getTobaccoClassNm()));
			}
		}
		return entries;
	}

	private List<CBPEntry> transformCBPEntryListToBeans(List<CBPEntryEntity> entities) {
		List<CBPEntry> beanList = new ArrayList<>();
		if (entities != null) {
			Mapper mapper = mapperWrapper.getMapper();
			for (CBPEntryEntity entry : entities) {
				CBPEntry entryBean = new CBPEntry();
				mapper.map(entry, entryBean);
				beanList.add(entryBean);
			}
		}
		return beanList;
	}

	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType
	 *            * MANU or IMPT
	 * @return
	 */
	@Override
	public List<TrueUpComparisonImporterDetail> getImporterComparisonDetails(String ein, long fiscalYear,
			long quarter, String permitType, String tobaccoClass) {
		return this.simpleSQLManager.getImporterComparisonDetails(ein, fiscalYear, quarter, permitType,
				tobaccoClass);
	}

	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType
	 *            * MANU or IMPT
	 * @return
	 */
	@Override
	public List<TrueUpComparisonImporterIngestionDetail> getImporterComparisonIngestionDetails(String ein,
			long fiscalYear, long quarter, String permitType, String tobaccoClass) {
		return this.simpleSQLManager.getImporterComparisonIngestionDetails(ein, fiscalYear, quarter, permitType, tobaccoClass, "N");
	}

	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType
	 *            * MANU or IMPT
	 * @return
	 */
	@Override
	public List<TrueUpComparisonImporterIngestionHTSCodeSummary> getImporterComparisonIngestionHTSCodeSummary(
			String ein, long fiscalYear, long quarter, String permitType, String tobaccoClass) {
		return this.simpleSQLManager.getImporterComparisonIngestionHTSCodeSummary(ein, fiscalYear, quarter,
				permitType, tobaccoClass);
	}

	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType
	 *            * MANU or IMPT
	 * @return
	 */
	@Override
	public List<TrueUpComparisonManufacturerDetail> getManufacturerComparisonDetails(long companyId, long fiscalYear,
			long quarter, String permitType, String tobaccoClass) {
		if ("CIGARS".equalsIgnoreCase(tobaccoClass) || "CIGARETTES".equalsIgnoreCase(tobaccoClass)) {
			return this.simpleSQLManager.getCigarManufacturerComparisonDetails(companyId, fiscalYear, quarter,
					permitType, tobaccoClass);
		}
		return this.simpleSQLManager.getNonCigarManufacturerComparisonDetails(companyId, fiscalYear, quarter,
				permitType, tobaccoClass);
	}

	@Override
	public ManufactureQuarterDetail getManufacturerComparisonDetails(long companyId, long fiscalYear, long quarter,
			String tobaccoClass) {
		List<TrueUpComparisonManufacturerDetail> permits;
		if ("CIGARS".equalsIgnoreCase(tobaccoClass)) {
			permits = this.simpleSQLManager.getCigarManufacturerComparisonDetails(companyId, fiscalYear, quarter,
					"MANU", tobaccoClass);
		} else {
			permits = this.simpleSQLManager.getNonCigarManufacturerComparisonDetails(companyId, fiscalYear, quarter,
					"MANU", tobaccoClass);

		}
		ManufactureQuarterDetail details = new ManufactureQuarterDetail();
		details.setPermits(permits);
		if (details.getPermits() != null) {
//         if("MATCHED".equalsIgnoreCase(details.getDataType()) || 
//                                       (Double.parseDouble(details.getPermits().get(0).getIngestedTax()) >= 0 && Double.parseDouble(details.getPermits().get(0).getPeriodTotal()) >= 0 ))
//         {                             

			List<TrueUpComparisonManuDetail> tufaDetailsList = new ArrayList<TrueUpComparisonManuDetail>(3);
			List<TrueUpComparisonManuIngestionDetail> ingestDetailsList = new ArrayList<TrueUpComparisonManuIngestionDetail>(
					3);
			int beanIndex = 0;
			for (TrueUpComparisonManufacturerDetail bean : permits) {
				int index = 0;
				for (TrueUpComparisonManufacturerDetailPermit monthbean : bean.getPermitDetails()) {

					// TUFA Details
					TrueUpComparisonManuDetail tufaDetails = new TrueUpComparisonManuDetail();

					int currentMonthIndex = -1;
					int i = 0;
					for (TrueUpComparisonManuDetail tufaMonthDetails : tufaDetailsList) {
						if (monthbean.getMonth().equalsIgnoreCase(tufaMonthDetails.getMonth()))
							currentMonthIndex = i;
						i++;
					}

					if (currentMonthIndex == -1)
						currentMonthIndex = i;

					boolean newTufaRecord = false;
					if (tufaDetailsList.size() > currentMonthIndex) {
						tufaDetails = tufaDetailsList.get(currentMonthIndex);
					} else
						newTufaRecord = true;

//                                                      if(beanIndex!=0)
//                                                                     tufaDetails = tufaDetailsList.get(index);

					double tufatotalremovalqty = 0;
					double tufatotaltaxamt = 0;
					double tufaremovalqtyClass1 = 0;
					double tufataxamtClass1 = 0;
					double tufaremovalqtyClass2 = 0;
					double tufataxamtClass2 = 0;

					if (tufaDetailsList.size() > currentMonthIndex) {
						tufaremovalqtyClass1 = tufaDetailsList.get(currentMonthIndex).getRemovalQtyTobaccoClass1();
						tufataxamtClass1 = tufaDetailsList.get(currentMonthIndex).getTaxAmountTobaccoClass1();
						tufatotalremovalqty = tufaDetailsList.get(currentMonthIndex).getRemovalQuantity();
						tufatotaltaxamt = tufaDetailsList.get(currentMonthIndex).getPeriodTotal();
						if (tobaccoClass.equalsIgnoreCase("Chew-Snuff")
								|| tobaccoClass.equalsIgnoreCase("Pipe-Roll Your Own")) {
							tufaremovalqtyClass2 = tufaDetailsList.get(currentMonthIndex).getRemovalQtyTobaccoClass2();
							tufataxamtClass2 = tufaDetailsList.get(currentMonthIndex).getTaxAmountTobaccoClass2();
						}

					}
					tufaDetails.setPeriodId(Long.toString(monthbean.getPeriodId()));
					tufaDetails.setMonth(monthbean.getMonth());
					tufaDetails.setTaxAmountTobaccoClass1(
							(monthbean.getTaxAmount().equalsIgnoreCase("NA") ? 0 : Double.parseDouble(monthbean.getTaxAmount()))
									+ tufataxamtClass1);
					tufaDetails.setRemovalQtyTobaccoClass1((monthbean.getTobaccoclass1Tufattbvolume().equalsIgnoreCase("NA") ? 0
							: Double.parseDouble(monthbean.getTobaccoclass1Tufattbvolume())) + tufaremovalqtyClass1);
					if (tobaccoClass.equalsIgnoreCase("Chew-Snuff")
							|| tobaccoClass.equalsIgnoreCase("Pipe-Roll Your Own")) {
						tufaDetails.setTaxAmountTobaccoClass2(
								(monthbean.getTaxAmount2().equalsIgnoreCase("NA") ? 0 : Double.parseDouble(monthbean.getTaxAmount2()))
										+ tufataxamtClass2);
						tufaDetails.setRemovalQtyTobaccoClass2((monthbean.getTobaccoclass2Tufattbvolume().equalsIgnoreCase("NA") ? 0
								: Double.parseDouble(monthbean.getTobaccoclass2Tufattbvolume()))
								+ tufaremovalqtyClass2);
					}
					tufaDetails.setPeriodTotal(
							(monthbean.getTaxTotalAmount() == null || monthbean.getTaxTotalAmount().equalsIgnoreCase("NA") ? 0
									: Double.parseDouble(monthbean.getTaxTotalAmount())) + tufataxamtClass1);
					tufaDetails.setRemovalQuantity(( monthbean.getTobaccoTotalTufattbvolume() == null || monthbean.getTobaccoTotalTufattbvolume().equalsIgnoreCase("NA") ? 0
									: Double.parseDouble(monthbean.getTobaccoTotalTufattbvolume()))
							+ tufatotalremovalqty);

					if (newTufaRecord)
						tufaDetailsList.add(tufaDetails);

					// Ingested Details
					TrueUpComparisonManuIngestionDetail ingestedDetails = new TrueUpComparisonManuIngestionDetail();
					boolean newIngestedRecord = false;
					if (ingestDetailsList.size() > currentMonthIndex) {
						ingestedDetails = ingestDetailsList.get(currentMonthIndex);
					} else
						newIngestedRecord = true;

					double ingestedtotalremovalqty = 0;
					double ingestedtotaltaxamt = 0;
					double ingestedremovalqtyClass1 = 0;
					double ingestedtaxamtClass1 = 0;
					double ingestedremovalqtyClass2 = 0;
					double ingestedtaxamtClass2 = 0;

					if (ingestDetailsList.size() > currentMonthIndex) {
						ingestedremovalqtyClass1 = ingestDetailsList.get(currentMonthIndex)
								.getRemovalQtyTobaccoClass1Ingested();
						ingestedtaxamtClass1 = ingestDetailsList.get(currentMonthIndex)
								.getTaxAmountTobaccoClass1Ingested();
						ingestedremovalqtyClass2 = ingestDetailsList.get(currentMonthIndex)
								.getRemovalQtyTobaccoClass2Ingested();
						ingestedtaxamtClass2 = ingestDetailsList.get(currentMonthIndex)
								.getTaxAmountTobaccoClass2Ingested();
						ingestedtotalremovalqty = ingestDetailsList.get(currentMonthIndex).getRemovalQuantityIngested();
						ingestedtotaltaxamt = ingestDetailsList.get(currentMonthIndex).getPeriodTotalIngested();

					}
					ingestedDetails.setPeriodId(Long.toString(monthbean.getPeriodId()));
					ingestedDetails.setMonth(monthbean.getMonth());
					if (tobaccoClass.equalsIgnoreCase("Chew-Snuff")
							|| tobaccoClass.equalsIgnoreCase("Pipe-Roll Your Own")) {
						ingestedDetails.setTaxAmountTobaccoClassIngested(
								monthbean.getIngestedTax() == null || monthbean.getIngestedTax().equalsIgnoreCase("NA") ? 0
										: Double.parseDouble(monthbean.getIngestedTax()),
								monthbean.getTobaccoClass1TTBvolume() == null
										|| monthbean.getTobaccoClass1TTBvolume().equalsIgnoreCase("NA") ? 0
												: Double.parseDouble(monthbean.getTobaccoClass1TTBvolume()),
								monthbean.getTobaccoClass2TTBvolume() == null
										|| monthbean.getTobaccoClass2TTBvolume().equalsIgnoreCase("NA") ? 0
												: Double.parseDouble(monthbean.getTobaccoClass2TTBvolume()),
								ingestedtaxamtClass1, ingestedtaxamtClass2, tobaccoClass);
						ingestedDetails
								.setRemovalQtyTobaccoClass2Ingested((monthbean.getTobaccoClass2TTBvolume() == null || monthbean.getTobaccoClass2TTBvolume().equalsIgnoreCase("NA") ? 0
												: Double.parseDouble(monthbean.getTobaccoClass2TTBvolume()))
										+ ingestedremovalqtyClass2);
						double volTufa1 = monthbean.getTobaccoClass1TTBvolume() == null
								|| monthbean.getTobaccoClass1TTBvolume().equalsIgnoreCase("NA") ? 0
										: Double.parseDouble(monthbean.getTobaccoClass1TTBvolume());
						double volTufa2 = monthbean.getTobaccoClass2TTBvolume() == null
								|| monthbean.getTobaccoClass2TTBvolume().equalsIgnoreCase("NA") ? 0
										: Double.parseDouble(monthbean.getTobaccoClass2TTBvolume());
						double removalqtyingested = volTufa1 + volTufa2;
						ingestedDetails.setRemovalQuantityIngested(removalqtyingested + ingestedtotalremovalqty);

					} else {
						ingestedDetails.setTaxAmountTobaccoClass1Ingested(
								(monthbean.getIngestedTax() == null || monthbean.getIngestedTax().equalsIgnoreCase("NA") ? 0
										: Double.parseDouble(monthbean.getIngestedTax())) + ingestedtaxamtClass1);
						ingestedDetails.setTaxAmountTobaccoClass2Ingested(0.0);
						ingestedDetails.setRemovalQuantityIngested(
								(monthbean.getTobaccoClass1TTBvolume() == null || monthbean.getTobaccoClass1TTBvolume().equalsIgnoreCase("NA") ? 0
										: Double.parseDouble(monthbean.getTobaccoClass1TTBvolume()))+ ingestedremovalqtyClass1);
					}
					ingestedDetails.setRemovalQtyTobaccoClass1Ingested((monthbean.getTobaccoClass1TTBvolume() == null
							|| monthbean.getTobaccoClass1TTBvolume() == null || monthbean.getTobaccoClass1TTBvolume().equalsIgnoreCase("NA") ? 0
									: Double.parseDouble(monthbean.getTobaccoClass1TTBvolume()))
							+ ingestedremovalqtyClass1);
					ingestedDetails.setPeriodTotalIngested(
							(monthbean.getIngestedTax() == null  || monthbean.getIngestedTax().equalsIgnoreCase("NA")? 0
									: Double.parseDouble(monthbean.getIngestedTax())) + ingestedtotaltaxamt);

					if (newIngestedRecord)
						ingestDetailsList.add(ingestedDetails);

					index++;
				}
				beanIndex++;
			}
			details.setIngestedDetails(ingestDetailsList);
			details.setTufaDetails(tufaDetailsList);

			List<Object[]> list = simpleSQLManager.getMixedActionDetaislForQuarterManu(companyId, fiscalYear, quarter,
					tobaccoClass);
			if (CollectionUtils.isNotEmpty(list)) {
				Map<String, String> mixedActionQuarterDetails = new HashMap<String, String>();
				for (Object[] detail : list) {
					mixedActionQuarterDetails.put(
							detail[0] != null ? detail[0].toString().toUpperCase() : detail[2].toString().toUpperCase(),
							detail[1] != null ? detail[1].toString() : detail[3].toString());
				}
				details.setMixedActionDetailsMap(mixedActionQuarterDetails);
			}
			List<String> mixedActionMonthList = new ArrayList<String>();
			double mixedActionQuantityRemoved = 0;
			double mixedActionTax = 0;
			int mixedIndex = 0;
			if (!tobaccoClass.equalsIgnoreCase("Cigars")) {
				if (details.getMixedActionDetailsMap() != null) {
					for (Map.Entry<String, String> entry : details.getMixedActionDetailsMap().entrySet()) {
						if (entry.getValue().equalsIgnoreCase("I")) {
							mixedActionQuantityRemoved = mixedActionQuantityRemoved
									+ ingestDetailsList.get(mixedIndex).getRemovalQuantityIngested();
							mixedActionTax = mixedActionTax
									+ ingestDetailsList.get(mixedIndex).getPeriodTotalIngested();
						} else if (entry.getValue().equalsIgnoreCase("F")) {
							mixedActionQuantityRemoved = mixedActionQuantityRemoved
									+ tufaDetailsList.get(mixedIndex).getRemovalQuantity();
							mixedActionTax = mixedActionTax + tufaDetailsList.get(mixedIndex).getPeriodTotal();
						}
						mixedIndex++;
					}
				}
				details.setMixedActionQuantityRemoved(mixedActionQuantityRemoved);
				details.setMixedActionTax(mixedActionTax);
			}
			// }
		}
		return details;
	}


	@Override
	public List<UnknownCompanyBucket> getUnknownCompanies(long fiscalYr) {
		return this.simpleSQLManager.getUnknownCompaniesBucket(fiscalYr);
	}

	@Override
	public void ingestTTBRemovals(IngestionOutput output, TrueUpDocumentEntity document) throws Exception{
		Long fiscalYr = output.getFiscalYear();
		if(simpleSQLManager.isIngestedLinedPresentForFiscalYear(fiscalYr,Constants.TTB_VOLUME)) throw new Exception("Ingested TTB Volume Data already present for fiscal year "+fiscalYr);
		Long errorCount = output.getErrorCount();
		List<DocUpload> ingestion = output.getOutput();
		Mapper mapper = mapperWrapper.getMapper();
		List<TufaEntity> ttbRemovalList = new ArrayList<>();
		Long index = 1L;
		for (DocUpload record : ingestion) {
			TTBRemovalsUpload ttbrmvBean = (TTBRemovalsUpload) record;
			if (ttbrmvBean.getFiscalYr() != null && ttbrmvBean.getFiscalYr().longValue() == fiscalYr.longValue()
					|| !CTPUtil.stringNullOrEmpty(ttbrmvBean.getErrors())) {
				TTBRemovalsEntity ttbrmv = new TTBRemovalsEntity();
				mapper.map(record, ttbrmv);
				ttbrmv.setTtbRemovalsId(index++);
				ttbrmv.setFiscalYr(fiscalYr);
				ttbrmv.setUom(this.getUOMForProduct(ttbrmv.getProduct()));
				// include if it matches all the criteria
				ttbRemovalList.add(ttbrmv);
			}
		}

		trueUpEntityManager.stageIngestionData(ttbRemovalList, Constants.TTB_VOLUME);
		if (errorCount == 0) {
			trueUpEntityManager.InsertTTBVolume();
			trueUpEntityManager.uploadDocument(output.getFiscalYear(), document);
		}
	}

	/**
	 * Save the association of HTS codes to tobacco class id.
	 * 
	 * @param entries
	 * @return
	 */
	@Override
	public List<CBPEntry> updateAssociateHTSMissingBucket(List<CBPEntry> entries, long fiscalYear) {
		List<CBPEntry> newentries = replaceHTSTobaccoClasses(entries);
		// build tobacco class id map for UniqueHTSKey
		Map<UniqueCBPEntryKey, Long> tobaccoClassIdMap = new HashMap<>();
		for (CBPEntry entry : newentries) {
			UniqueCBPEntryKey key = new UniqueCBPEntryKey(entry.getCbpImporter().getCbpImporterId(),
					entry.getCbpEntryId());
			tobaccoClassIdMap.put(key, entry.getTobaccoClassId());
		}

		List<CBPEntryEntity> entities = trueUpEntityManager.getAssociateHTSMissingBucket(fiscalYear);
		for (CBPEntryEntity entity : entities) {
			UniqueCBPEntryKey key = new UniqueCBPEntryKey(entity.getCbpImporter().getCbpImporterId(),
					entity.getCbpEntryId());
			Long tobaccoClassId = tobaccoClassIdMap.get(key);
			if (tobaccoClassId != null)
				entity.setTobaccoClassId(tobaccoClassId);
		}

		List<CBPEntryEntity> returnList = trueUpEntityManager.updateAssociateHTSBucket(entities,fiscalYear);
		return transformCBPEntryListToBeans(returnList);

	}

	@Override
	public List<TTBPermitBucket> getTTBPermits(long fiscalYr) {
		return simpleSQLManager.getTTBPermits(fiscalYr);
	}

	@Override
	public void updateTTBPermits(List<TTBPermitBucket> ttbpermits) {
		for (TTBPermitBucket ttbpermit : ttbpermits) {
			TTBPermitEntity permit = trueUpEntityManager.getTTBPermit(ttbpermit.getPermitId());
			permit.setIncludeFlag(ttbpermit.getIncludeFlag());
			trueUpEntityManager.saveTTBPermit(permit);
		}
	}

	@Override
	public List<CompanyIngestion> getIncludeCompaniesInMktSh(long fiscalYear) {
		return this.simpleSQLManager.getCompaniesForIncludeBucket(fiscalYear);
	}

	@Override
	public void updateIncludeCompaniesInMktSh(List<CompanyIngestion> companies, long fiscalYear) {

		this.trueUpEntityManager.updateCompaniesForIncludeBucket(companies, fiscalYear);
	}

	@Override
	public List<TrueUpEntity> getAllTrueUps() {
		return this.trueUpEntityManager.getAllTrueUps();
	}

	@Override
	public void deleteIngestionFile(long fiscalYr, String doctype) {
		this.trueUpEntityManager.deleteIngestionFile(fiscalYr, doctype);
	}

	@Override
	public List<TTBAmendment> getTTBAmendments(long fiscalYr, long companyId, List<TTBAmendment> tobaccoTypes) {

		List<TTBAmendmentEntity> amdmntEntities = this.trueUpEntityManager.getTTBAmendments(fiscalYr, companyId,
				tobaccoTypes);
		List<TTBAmendment> amdmntList = new ArrayList<>();
		Mapper mapper = mapperWrapper.getMapper();

		/**
		 * Populate list of quarters excluded with exclusion range
		 */
		List<ExcludePermitDetails> excludedQuarterList = simpleSQLManager.getPermitExcludeList(String.valueOf(fiscalYr),
				companyId, 3);
		List<Integer> quarterExcluded = new ArrayList<Integer>();
		for (TTBAmendmentEntity entity : amdmntEntities) {
			TTBAmendment admnt = new TTBAmendment();
			mapper.map(entity, admnt);
			for(TTBQtrComment comment : admnt.getUserComments()) {
				comment.setAttachmentsCount(trueUpEntityManager.getDocCount(0, comment.getCommentSeq(), 0));
			}
			admnt.setTobaccoType(entity.getTobaccoClass().getTobaccoClassNm());
			if(admnt.getAcceptanceFlag()!= null && (admnt.getAcceptanceFlag().equalsIgnoreCase("M") || admnt.getAcceptanceFlag().equalsIgnoreCase("X") || admnt.getAcceptanceFlag().equalsIgnoreCase("E"))) {
				admnt.setAmendedTotalTax(null);
				admnt.setAmendedTotalVolume(null);
			}
				
			// admnt.setExcludedPermits(excludedQuarterList);
			amdmntList.add(admnt);
		}

		/**
		 * this block will filter out the qurates from exclusion view with amendment
		 * entries
		 */
		if(CollectionUtils.isNotEmpty(amdmntList)) {
			//for (TTBAmendment admnt : amdmntList) {
				for (ExcludePermitDetails excl : excludedQuarterList) {
					int qtr = 0;
					if (excl.getQuarter() == 1 && excl.getYear() - 1 == fiscalYr) {
						qtr = 4;
					}
					if (excl.getYear() == fiscalYr) {
						qtr = excl.getQuarter() - 1;
					}
					
					if(qtr>0)quarterExcluded.add(qtr);
					
				}
			//}
		} else {
			for (ExcludePermitDetails excl : excludedQuarterList) {
				int qtr = 0;
				if (excl.getQuarter() == 1 && excl.getYear() - 1 == fiscalYr) {
					qtr = 4;
				}
				if (excl.getYear() == fiscalYr) {
					qtr = excl.getQuarter() - 1;
				}
				if(qtr>0)quarterExcluded.add(qtr);
			}
			TTBAmendment admnt = new TTBAmendment();
			admnt.setExcludedPermits(quarterExcluded);
			admnt.setAmendmentPresent(false);
			amdmntList.add(admnt);
		}
		
		/**
		 * this block will extract the list of quarters for the fiscal year
		 *//*
			 * for(ExcludePermitDetails details : excludedQuarterList) {
			 * quarterExcluded.add(details.getQuarter()-1); }
		
			 */
		if(CollectionUtils.isNotEmpty(amdmntList)) {
		for (TTBAmendment admnt : amdmntList) {
			admnt.setExcludedPermits(quarterExcluded);
		}}

		return amdmntList;
	}

	@Override
	public List<TTBAmendment> saveTTBAmendment(List<TTBAmendment> amendments) {

		List<TTBAmendmentEntity> amdmntEntityList = this.transformTTBAmdtBeanToTTBAmdtEntity(amendments);

		List<TTBAmendmentEntity> amndmnts = this.trueUpEntityManager.saveTTBAmendment(amdmntEntityList);

		return this.transformTTBAmdtEntityToTTBAmdtBean(amndmnts);

	}

	@Override
	public List<TTBAmendment> saveTTBCompareComments(List<TTBAmendment> amendments) {

		List<TTBAmendmentEntity> amdmntEntityList = this.transformTTBAmdtBeanToTTBAmdtEntity(amendments);

		List<TTBAmendmentEntity> amndmnts = this.trueUpEntityManager.saveTTBCompareComment(amdmntEntityList);

		return this.transformTTBAmdtEntityToTTBAmdtBean(amndmnts);

	}
	@Override
	public List<TTBAmendment> saveTTBCompareComments(List<TTBAmendment> amendments, String ein, String tobaccoClass) {

		List<TTBAmendmentEntity> amdmntEntityList = this.transformTTBAmdtBeanToTTBAmdtEntity(amendments);

		List<TTBAmendmentEntity> amndmnts = this.trueUpEntityManager.saveTTBCompareComment(amdmntEntityList, ein, tobaccoClass);

		return this.transformTTBAmdtEntityToTTBAmdtBean(amndmnts);

	}

	@Override
	public List<TTBAmendment> updateTTBCompareFDAAcceptFlag(List<TTBAmendment> amendments) {

		List<TTBAmendmentEntity> amdmntEntityList = this.transformTTBAmdtBeanToTTBAmdtEntity(amendments);

		List<TTBAmendmentEntity> amndmnts = this.trueUpEntityManager.updateTTBCompareFDAAcceptFlag(amdmntEntityList);

		return this.transformTTBAmdtEntityToTTBAmdtBean(amndmnts);

	}

	private TobaccoClassEntity getTobaccoTypeByName(String ttname) {
		List<String> tt = new ArrayList<>();
		tt.add(ttname);
		List<TobaccoClassEntity> ttEntities = this.staticDataEntityManager.getTobaccoTypes(tt);
		return ttEntities.get(0);
	}

	private List<TTBAmendmentEntity> transformTTBAmdtBeanToTTBAmdtEntity(List<TTBAmendment> amendments) {

		Mapper mapper = mapperWrapper.getMapper();
		List<TTBAmendmentEntity> amdmntEntityList = new ArrayList<>();

		for (TTBAmendment admnt : amendments) {
			TTBAmendmentEntity admntEntity = new TTBAmendmentEntity();
			mapper.map(admnt, admntEntity);
			if (admnt.getTobaccoClassId() == null && admnt.getTobaccoType() != null) {
				TobaccoClassEntity ttype = this.getTobaccoTypeByName(admnt.getTobaccoType());
				admntEntity.setTobaccoClass(ttype);
				admntEntity.setTobaccoClassId(ttype.getTobaccoClassId());
			}
			amdmntEntityList.add(admntEntity);
		}

		return amdmntEntityList;
	}

	private List<TTBAmendment> transformTTBAmdtEntityToTTBAmdtBean(List<TTBAmendmentEntity> amendments) {

		Mapper mapper = mapperWrapper.getMapper();
		List<TTBAmendment> amdmntList = new ArrayList<>();

		for (TTBAmendmentEntity entity : amendments) {
			TTBAmendment admnt = new TTBAmendment();
			mapper.map(entity, admnt);
			admnt.setTobaccoType(entity.getTobaccoClass().getTobaccoClassNm());
			amdmntList.add(admnt);
		}

		return amdmntList;
	}

	@Override
	public List<CBPAmendment> getCBPAmendments(long fiscalYr, long companyId, List<CBPAmendment> tobaccoTypes) {
		List<CBPAmendmentEntity> amdmntEntities = this.trueUpEntityManager.getCBPAmendments(fiscalYr, companyId,
				tobaccoTypes);
		List<CBPAmendment> amdmntList = new ArrayList<>();
		Mapper mapper = mapperWrapper.getMapper();
		

		/**
		 * Populate list of quarters excluded with exclusion range
		 */
		List<ExcludePermitDetails> excludedQuarterList = simpleSQLManager.getPermitExcludeList(String.valueOf(fiscalYr),companyId, 3);
		for (CBPAmendmentEntity entity : amdmntEntities) {
			CBPAmendment admnt = new CBPAmendment();
			mapper.map(entity, admnt);
			for(CBPQtrComment comment : admnt.getUserComments()) {
				comment.setAttachmentsCount(trueUpEntityManager.getDocCount(comment.getCommentSeq(),0 , 0));
			}
			admnt.setTobaccoType(entity.getTobaccoClass().getTobaccoClassNm());
			amdmntList.add(admnt);
		}
		
		List<Integer> quarterExcluded = new ArrayList<Integer>();
		/**
		 * this block will filter out the qurates from exclusion view with amendment
		 * entries
		 */
		if(CollectionUtils.isNotEmpty(amdmntList)) {
			//for (CBPAmendment admnt : amdmntList) {
				for (ExcludePermitDetails excl : excludedQuarterList) {
					int qtr = 0;
					if (excl.getQuarter() == 1 && excl.getYear() - 1 == fiscalYr) {
						qtr = 4;
					}
					if (excl.getYear() == fiscalYr) {
						qtr = excl.getQuarter() - 1;
					}
					
					if(qtr>0)quarterExcluded.add(qtr);
					
				}
			//}
		} else {
			for (ExcludePermitDetails excl : excludedQuarterList) {
				int qtr = 0;
				if (excl.getQuarter() == 1 && excl.getYear() - 1 == fiscalYr) {
					qtr = 4;
				}
				if (excl.getYear() == fiscalYr) {
					qtr = excl.getQuarter() - 1;
				}
				if(qtr>0)quarterExcluded.add(qtr);
			}
			CBPAmendment admnt = new CBPAmendment();
			admnt.setExcludedPermits(quarterExcluded);
			admnt.setAmendmentPresent(false);
			amdmntList.add(admnt);
		}
		if(CollectionUtils.isNotEmpty(amdmntList)) {
			for (CBPAmendment admnt : amdmntList) {
				admnt.setExcludedPermits(quarterExcluded);
			}}

		return amdmntList;
	}

	@Override
	public List<CBPAmendment> saveCBPAmendment(List<CBPAmendment> amendments) {
		List<CBPAmendmentEntity> amdmntEntityList = this.transformCBPAmdtBeanToCBPAmdtEntity(amendments);

		List<CBPAmendmentEntity> amndmnts = this.trueUpEntityManager.saveCBPAmendment(amdmntEntityList);

		return this.transformCBPAmdtEntityToCBPAmdtBean(amndmnts);
	}

	@Override
	public List<CBPAmendment> saveCBPCompareComments(List<CBPAmendment> amendments) {
		List<CBPAmendmentEntity> amdmntEntityList = this.transformCBPAmdtBeanToCBPAmdtEntity(amendments);

		List<CBPAmendmentEntity> amndmnts = this.trueUpEntityManager.saveCBPCompareComment(amdmntEntityList);

		return this.transformCBPAmdtEntityToCBPAmdtBean(amndmnts);
	}
	
	@Override
	public List<CBPAmendment> saveCBPCompareComments( List<CBPAmendment> amendments, String ein){
		List<CBPAmendmentEntity> amdmntEntityList = this.transformCBPAmdtBeanToCBPAmdtEntity(amendments);

		List<CBPAmendmentEntity> amndmnts = this.trueUpEntityManager.saveCBPCompareComment(amdmntEntityList,ein);

		return this.transformCBPAmdtEntityToCBPAmdtBean(amndmnts);
	
	}
	@Override
	public List<CBPAmendment> updateCBPCompareFDAAcceptFlag(List<CBPAmendment> amendments) {

		List<CBPAmendmentEntity> amdmntEntityList = this.transformCBPAmdtBeanToCBPAmdtEntity(amendments);

		List<CBPAmendmentEntity> amndmnts = this.trueUpEntityManager.updateCBPCompareFDAAcceptFlag(amdmntEntityList);

		return this.transformCBPAmdtEntityToCBPAmdtBean(amndmnts);
	}

	private List<CBPAmendmentEntity> transformCBPAmdtBeanToCBPAmdtEntity(List<CBPAmendment> amendments) {

		Mapper mapper = mapperWrapper.getMapper();
		List<CBPAmendmentEntity> amdmntEntityList = new ArrayList<>();

		for (CBPAmendment admnt : amendments) {
			CBPAmendmentEntity admntEntity = new CBPAmendmentEntity();
			mapper.map(admnt, admntEntity);
			if (admnt.getTobaccoClassId() == null && admnt.getTobaccoType() != null) {
				TobaccoClassEntity ttype = this.getTobaccoTypeByName(admnt.getTobaccoType());
				admntEntity.setTobaccoClass(ttype);
				admntEntity.setTobaccoClassId(ttype.getTobaccoClassId());
			}
			amdmntEntityList.add(admntEntity);
		}

		return amdmntEntityList;
	}

	private List<CBPAmendment> transformCBPAmdtEntityToCBPAmdtBean(List<CBPAmendmentEntity> amendments) {

		Mapper mapper = mapperWrapper.getMapper();
		List<CBPAmendment> amdmntList = new ArrayList<>();

		for (CBPAmendmentEntity entity : amendments) {
			CBPAmendment admnt = new CBPAmendment();
			mapper.map(entity, admnt);
			admnt.setTobaccoType(entity.getTobaccoClass().getTobaccoClassNm());
			amdmntList.add(admnt);
		}

		return amdmntList;
	}

	@Override
	public CompanyReallocationBeanToUpdate updateCBPIngestedLineEntryInfo(List<CBPLineEntryUpdateInfo> updateinfo) {
	    CompanyReallocationBeanToUpdate bean = this.trueUpEntityManager.updateCBPIngestedLineEntryInfo(updateinfo);
		return bean ;
	}

	@Override
	public Map<String, String> fetchFDA352MAN(long fiscalYr, long company_id, String classNm) {
		return this.simpleSQLManager.fetchFDA352MAN(fiscalYr, company_id, classNm);
	}

	@Override
	public CompanyIngestion getCompanyToIncldInMktSh(long fiscalYear, String ein, String type) {

		Mapper mapper = mapperWrapper.getMapper();
		CompanyIngestion cmpyIngestion = new CompanyIngestion();

		if ("Manufacturer".equals(type)) {
			TTBCompanyEntity cmpyEntity = this.trueUpEntityManager.getCompanyToIncldInMktSh(fiscalYear, ein);
			if (cmpyEntity != null) {
				mapper.map(cmpyEntity, cmpyIngestion);
			}

		} else {

			CBPImporterEntity cmpyEntity = this.trueUpEntityManager.getCBPCompanyToIncldInMktSh(fiscalYear, ein);

			if (cmpyEntity != null) {
				mapper.map(cmpyEntity, cmpyIngestion);
			}
		}

		return cmpyIngestion;
	}

	@Override
	public Map<String, String> fetchFDA352IMP(long fiscalYr,long company_id , String classNm, String ein) {
		return this.simpleSQLManager.fetchFDA352IMP(fiscalYr, ein, classNm);
	}
	
	@Override
	public Map<String, String> fetchFDA352IMPSingle(long fiscalYr, long company_id, String classNm,String ein) {
		return this.simpleSQLManager.fetchFDA352IMPSingle(fiscalYr, ein, classNm);
	}
	
	@Override
	public Map<String, String> fetchOneSided(long fiscalYr, long company_id, String classNm, String reportType, String ein) {
		return this.simpleSQLManager.fetchOneSided(fiscalYr, company_id, classNm, reportType, ein);
	}
	  
	  @Override
	    public ComparisonAllDeltaComments updateDeltaComments(long commentSeq,ComparisonAllDeltaComments comment) {	    	
		  return this.simpleSQLManager.updateComment(comment, commentSeq);
	    }

	@Override
	public ComparisonAllDeltaStatusEntity setCompareAllDeltaStatus(List<TrueUpComparisonResults> model) {
		return this.trueUpEntityManager.updateCompareAllDeltaStatus(model);
	}

	@Override
	public ComparisonAllDeltaStatusEntity setCompareAllDeltaStatusSingle(List<TrueUpComparisonResults> model) {
		return this.trueUpEntityManager.updateCompareAllDeltaStatusSingle(model);
	}
	
	@Override
	public List<ComparisonAllDeltaStatus> getallDeltaComments(long fiscalYr, String ein, String fiscal_qtr, String tobacco_class_nm,String permit_type) 
	{				
		Mapper mapper = mapperWrapper.getMapper();
		List<ComparisonAllDeltaStatus> cmpsts = new ArrayList<>();
		List<ComparisonAllDeltaStatusEntity> stsEntity = this.trueUpEntityManager.getallDeltaComments(fiscalYr,ein,fiscal_qtr,tobacco_class_nm,permit_type);
		
		for (ComparisonAllDeltaStatusEntity entity : stsEntity) {
			
			ComparisonAllDeltaStatus sts = new ComparisonAllDeltaStatus();
			mapper.map(entity, sts);
			for(ComparisonAllDeltaComments userComment: sts.getUserComments()) {
				userComment.setAttachmentsCount(trueUpEntityManager.getDocCount(0, 0, userComment.commentSeq));
			}
			cmpsts.add(sts);
		}

		return cmpsts;
	}

	
	@Override
	public Boolean checkIfAssociationExists(String ein, long fiscalYear, String tobaccoClass,long qtr) {
		return this.trueUpEntityManager.checkIfAssociationExists(ein, fiscalYear, tobaccoClass, qtr);
	}

	@Override
	public TrueUpComparisonResults getAllComparisonResultsCount(long fiscalYr, String inComingPage) {
		return this.simpleSQLManager.getAllComparisonResultsCount(fiscalYr, inComingPage);
	}
	
	@Override
	public TrueUpComparisonResults getAllComparisonSingleResultsCount(long fiscalYr, String inComingPage) {
		return this.simpleSQLManager.getAllComparisonSingleResultsCount(fiscalYr, inComingPage);
	}

	@Override
	public List<TobaccoTypeTTBVol> getTTBVolumeTaxPerTobaccoType(TobaccoTypeTTBVol ttbVol) {
		return this.simpleSQLManager.getTTBVolumeTaxPerTobaccoType(ttbVol);
	}

	@Override
	public List<TrueUpComparisonResults> getDeltasAffectedByCompareAllAction(long fiscalYr, String ein,
			String allAction) {
		return this.simpleSQLManager.getDeltasAffectedByCompareAllActions(fiscalYr, ein, allAction);
	}

	@Override
	public List<TrueUpComparisonResults> getDeltasAffectedByCompareAllActionSingle(long fiscalYr, String ein,
			String allAction) {
		return this.simpleSQLManager.getDeltasAffectedByCompareAllActionsSingle(fiscalYr, ein, allAction);
	}
	
	@Override
	public DocExport getDeltaRawDataExport(long fiscalYr, String ein, String qtr, String tobaccoClassNm,
			String permitType, boolean showOriginalAssociation) {
		DocExport doc = new DocExport();
		switch (permitType) {
		case "IMPT":
		case "Importer":
			doc = this.exportCBPDeltaRawData(fiscalYr, ein, qtr, tobaccoClassNm, showOriginalAssociation);
			break;
		case "MANU":
		case "Manufacturer":
			doc = this.exportTTBDeltaRawData(fiscalYr, ein, qtr, tobaccoClassNm);
			break;
		}

		return doc;
	}

	private DocExport exportCBPDeltaRawData(long fiscalYr, String ein, String qtr, String tobaccoClassNm,
			boolean showOriginalAssociation) {
		List<RawIngested> cbpRaw = this.trueUpEntityManager.getRawIngested(fiscalYr, ein, qtr, tobaccoClassNm, "IMPT",
				showOriginalAssociation, false);

		StringBuilder filename = new StringBuilder("Raw_CBP_");
		filename.append(fiscalYr);
		filename.append("_");
		filename.append(ein);
		filename.append("_");
		filename.append(qtr);
		filename.append("_");
		filename.append(tobaccoClassNm);
		filename.append(".csv");

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine("EIN,Company Name, Month, Quarter, Tobacco Class, Reported Taxes, Removals Amount");
		for (RawIngested raw : cbpRaw) {
			// For the associated records...
			String exportEin = raw.getOriginalEin() != null ? raw.getOriginalEin() : raw.getEin();
			EinBuilder einBuilder = new EinBuilder(exportEin);

			csv.csvAppend(einBuilder.getFormattedEin());
			csv.csvAppendRaw("\"");
			csv.csvAppendRaw(
					raw.getOriginalCompanyNm() != null ? raw.getOriginalCompanyNm() : raw.getCompanyNm());
			csv.csvAppendRaw("\",");
			csv.csvAppend(raw.getMonth());
			csv.csvAppend(raw.getQuarter());
			csv.csvAppend(raw.getTobaccoClassNm());
			try {
				NumberFormat formatter = NumberFormat.getCurrencyInstance();
				Double taxes = new Double(raw.getTaxesPaid());
				csv.csvAppendRaw("\"");
				csv.csvAppendRaw(formatter.format(taxes));
				csv.csvAppendRaw("\",");
			} catch (IllegalArgumentException e) {
				csv.csvAppendRaw(raw.getTaxesPaid());
				csv.csvAppendRaw("\",");
			}
			csv.csvAppendNewLine(raw.getRemovalsAmount());
		}

		DocExport doc = new DocExport();
		doc.setFileName(filename.toString());
		doc.setCsv(csv.toString());
		return doc;
	}

	private DocExport exportTTBDeltaRawData(long fiscalYr, String ein, String qtr, String tobaccoClassNm) {
		List<RawIngested> ttbRaw = this.trueUpEntityManager.getRawIngested(fiscalYr, ein, qtr, tobaccoClassNm, "MANU",
				false, true);
		List<RawIngested> ttbRawVol = this.trueUpEntityManager.getRawIngested(fiscalYr, ein, qtr, tobaccoClassNm, "MANU",
				false, false);
		
		StringBuilder filename = new StringBuilder("Raw_TTB_");
		filename.append(fiscalYr);
		filename.append("_");
		filename.append(ein);
		filename.append("_");
		filename.append(qtr);
		filename.append("_");
		filename.append(tobaccoClassNm.replace(" ", "_"));
		filename.append(".csv");

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine(
				"EIN,Company Name,Permit Number, Month,TPBD, Quarter, Class (Volume), Class (Tax), Removals Amount, Reported Taxes, Inc Adj, Dec Adj");
		
		for(String month :Constants.MMMByQuarter.get(Integer.parseInt(qtr))) {
			List<RawIngested> ttbRawTaxFiltered = new ArrayList<>(ttbRaw);
			List<RawIngested> ttbRawVolFiltered = new ArrayList<>(ttbRawVol);
			ttbRawTaxFiltered.removeIf(row -> !month.equalsIgnoreCase(row.getMonth()));
			ttbRawVolFiltered.removeIf(row -> !month.equalsIgnoreCase(row.getMonth()));
			Iterator<RawIngested> taxIterator = ttbRawTaxFiltered.iterator();
			Iterator<RawIngested> volIterator = ttbRawVolFiltered.iterator();
			
			RawIngested taxInfo = null;
			RawIngested volInfo = null;
			RawIngested sharedInfo = null;
			while(taxIterator.hasNext() || volIterator.hasNext()) {
				sharedInfo = null;
				taxInfo = null;
				volInfo = null;
				
				if (volIterator.hasNext()) {
					volInfo = volIterator.next();
					sharedInfo = volInfo;
				}
				
				if(taxIterator.hasNext()) {
					taxInfo = taxIterator.next();
					sharedInfo = taxInfo;
				}

				EinBuilder einBuilder = new EinBuilder(sharedInfo.getEin());
				csv.csvAppend(einBuilder.getFormattedEin());
				csv.csvAppendRaw("\"");
				csv.csvAppendRaw(sharedInfo.getCompanyNm());
				csv.csvAppendRaw("\",");
				PermitBuilder permitBuilder = new PermitBuilder(sharedInfo.getPermitNum());
				csv.csvAppend(permitBuilder.formatPermit());
				csv.csvAppend(sharedInfo.getMonth());
				csv.csvAppend(sharedInfo.getTPBD());
				csv.csvAppend(sharedInfo.getQuarter());
				
				csv.csvAppend(volInfo != null ? volInfo.getTobaccoClassNm() : "");
				csv.csvAppend(taxInfo != null ? taxInfo.getTobaccoClassNm() : "");
				csv.csvAppend(volInfo != null ? volInfo.getRemovalsAmount() : "");
				csv.csvAppendRaw("\"");
				csv.csvAppendFormatRaw("$###,###,##0.00",taxInfo != null && taxInfo.getTaxesPaid() != null ? Double.parseDouble(taxInfo.getTaxesPaid()) : null);
				csv.csvAppendRaw("\",\"");
				csv.csvAppendFormatRaw("$###,###,##0.00",taxInfo != null && taxInfo.getIncAdj() != null ? Double.parseDouble(taxInfo.getIncAdj()) : null);
				csv.csvAppendRaw("\",\"");
				csv.csvAppendFormatRaw("-$###,###,##0.00",taxInfo != null && taxInfo.getDecAdj() != null ? Double.parseDouble(taxInfo.getDecAdj()) : null);
				csv.csvAppendRaw("\",");
				csv.csvAppendNewLine(null);
				
			}
		}

		DocExport doc = new DocExport();
		doc.setFileName(filename.toString());
		doc.setCsv(csv.toString());
		return doc;
	}

	// export compare data
	public DocExport getExportData(long fiscalYr, TrueUpCompareSearchCriteria csc, String selCompareMethod) {
		List<TrueUpComparisonResults> trueUpComparisonResultsList = null;
		DocExport doc = new DocExport();
		boolean isAllDelta = false;
		String exportEin = null;
		EinBuilder einBuilder = null;
		StringBuilder filename = new StringBuilder("Export_Compare_");
		CsvStringBuilder csv = new CsvStringBuilder();
		NumberFormat formatter = null;
		Double taxes = null;
		if ("ALLDELTAS".equalsIgnoreCase(selCompareMethod)) {
			isAllDelta = true;
			trueUpComparisonResultsList = getAllComparisonResults(fiscalYr, csc);
		} else {
			trueUpComparisonResultsList = getComparisonResults(fiscalYr, csc);
		}
		filename.append(fiscalYr);
		filename.append("_");
		filename.append(selCompareMethod);
		filename.append(".csv");
		if (isAllDelta) {
			csv.csvAppendNewLine(
					"Company Name,EIN, Permit, Original Name, Tobacco Class, Quarter, Delta Excise Tax, Type, Data, Status, Comment");
		} else {
			csv.csvAppendNewLine("Company Name,EIN, Permit, Tobacco Class, Quarter, Delta Excise Tax, Type, Action");
		}
		for (TrueUpComparisonResults trueUpComparisonResults : trueUpComparisonResultsList) {
			exportEin = trueUpComparisonResults.getEin();
			einBuilder = new EinBuilder(exportEin);
			csv.csvAppendRawQuotes(trueUpComparisonResults.getLegalName());
			csv.csvAppend(einBuilder.getFormattedEin());
			if (null != trueUpComparisonResults.getPermitNum()) {
				csv.csvAppendRawQuotes(trueUpComparisonResults.getPermitNum());
			} else {
				csv.csvAppend(trueUpComparisonResults.getPermitNum());
			}
			if (isAllDelta) {
				if (null != trueUpComparisonResults.getOriginalName()) {
					csv.csvAppendRawQuotes(trueUpComparisonResults.getOriginalName());
				} else {
					csv.csvAppend(trueUpComparisonResults.getOriginalName());
				}
			}
			csv.csvAppend(trueUpComparisonResults.getTobaccoClass());
			csv.csvAppendRawQuotes(trueUpComparisonResults.getQuarter());
			if("Review".equalsIgnoreCase(trueUpComparisonResults.getDeltaexcisetaxFlag())){
				csv.csvAppend(trueUpComparisonResults.getDeltaexcisetaxFlag());
			}else{
				try {
					formatter = NumberFormat.getCurrencyInstance();
					taxes = new Double(trueUpComparisonResults.getTotalDeltaTax());
					csv.csvAppendRawQuotes(formatter.format(taxes));
				} catch (IllegalArgumentException e) {
					csv.csvAppend(taxes);
				}
			}
			csv.csvAppend(trueUpComparisonResults.getPermitType());
			if (isAllDelta) {
				csv.csvAppend(trueUpComparisonResults.getDataType());
				csv.csvAppend(trueUpComparisonResults.getAllDeltaStatus());
			} else {
				csv.csvAppend(getActionforStatus(trueUpComparisonResults.getStatus(), taxes));
			}

			if (isAllDelta) {
				if (null != trueUpComparisonResults.getDeltaComment()
						&& !("MATCHED".equalsIgnoreCase(trueUpComparisonResults.getDataType()))) {
					csv.csvAppendRawQuotes(trueUpComparisonResults.getDeltaComment());
				} else {
					csv.csvAppend(trueUpComparisonResults.getDeltaComment());
				}
			}
			csv.csvAppendNewLine(null);
		}
		doc.setFileName(filename.toString());
		doc.setCsv(csv.toString());

		return doc;
	}
	
	// export compare data
		public DocExport getExportDataSingle(long fiscalYr, TrueUpCompareSearchCriteria csc, String selCompareMethod) {
			List<TrueUpComparisonResults> trueUpComparisonResultsList = null;
			DocExport doc = new DocExport();
			boolean isAllDelta = false;
			String exportEin = null;
			EinBuilder einBuilder = null;
			StringBuilder filename = new StringBuilder("Export_Compare_");
			CsvStringBuilder csv = new CsvStringBuilder();
			NumberFormat formatter = null;
			Double taxes = null;
			if ("ALLDELTAS".equalsIgnoreCase(selCompareMethod)) {
				isAllDelta = true;
				trueUpComparisonResultsList = getAllComparisonSingleResults(fiscalYr, csc);
			} else {
				trueUpComparisonResultsList = getComparisonSingleResults(fiscalYr, csc);
			}
			filename.append(fiscalYr);
			filename.append("_");
			filename.append(selCompareMethod);
			filename.append(".csv");
			if (isAllDelta) {
				csv.csvAppendNewLine(
						"Company Name,EIN, Permit, Original Name, Tobacco Class, Quarter, Delta Excise Tax, Type, Data, Status, Comment");
			} else {
				csv.csvAppendNewLine("Company Name,EIN, Permit, Tobacco Class, Quarter, Delta Excise Tax, Type, Action");
			}
			for (TrueUpComparisonResults trueUpComparisonResults : trueUpComparisonResultsList) {
				exportEin = trueUpComparisonResults.getEin();
				einBuilder = new EinBuilder(exportEin);
				csv.csvAppendRawQuotes(trueUpComparisonResults.getLegalName());
				csv.csvAppend(einBuilder.getFormattedEin());
				if (null != trueUpComparisonResults.getPermitNum()) {
					csv.csvAppendRawQuotes(trueUpComparisonResults.getPermitNum());
				} else {
					csv.csvAppend(trueUpComparisonResults.getPermitNum());
				}
				if (isAllDelta) {
					if (null != trueUpComparisonResults.getOriginalName()) {
						csv.csvAppendRawQuotes(trueUpComparisonResults.getOriginalName());
					} else {
						csv.csvAppend(trueUpComparisonResults.getOriginalName());
					}
				}
				csv.csvAppend(trueUpComparisonResults.getTobaccoClass());
				csv.csvAppendRawQuotes(trueUpComparisonResults.getQuarter());
					try {
						formatter = NumberFormat.getCurrencyInstance();
						taxes = new Double(trueUpComparisonResults.getTotalDeltaTax());
						csv.csvAppendRawQuotes(formatter.format(taxes));
					} catch (IllegalArgumentException e) {
						csv.csvAppend(taxes);
					}
				csv.csvAppend(trueUpComparisonResults.getPermitType());
				if (isAllDelta) {
					csv.csvAppend(trueUpComparisonResults.getDataType());
					csv.csvAppend(trueUpComparisonResults.getAllDeltaStatus());
				} else {
					csv.csvAppend(getActionforStatus(trueUpComparisonResults.getStatus(), taxes));
				}

				if (isAllDelta) {
					if (null != trueUpComparisonResults.getDeltaComment()
							&& !("MATCHED".equalsIgnoreCase(trueUpComparisonResults.getDataType()))) {
						csv.csvAppendRawQuotes(trueUpComparisonResults.getDeltaComment());
					} else {
						csv.csvAppend(trueUpComparisonResults.getDeltaComment());
					}
				}
				csv.csvAppendNewLine(null);
			}
			doc.setFileName(filename.toString());
			doc.setCsv(csv.toString());

			return doc;
		}

	private String getActionforStatus(String status, Double totalDeltaTax) {
		String action = null;
		if (null == status || "noAction".equalsIgnoreCase(status)) {
			action = "No Action Taken";
		} else if ("FDAaccepted".equalsIgnoreCase(status)) {
			action = "Accepted FDA";
		} else if ("Ingestedaccepted".equalsIgnoreCase(status)) {
			action = "Accepted Ingested";
		} else if ("Amended".equalsIgnoreCase(status)) {
			action = "Amended Delta";
		} 
		else if("ExcludeChange".equalsIgnoreCase(status))
		{
			action = "Delta Change Permit Exclude";
		}	
		else if ("DeltaChange".equalsIgnoreCase(status)) {
			if (Math.abs(totalDeltaTax) > 0.99) {
				action = "Changed Delta";
			} else if (Math.abs(totalDeltaTax) <= 0.99) {
				action = "Delta Change to Zero";
			}
		} else if ("InProgress".equalsIgnoreCase(status)) {
			action = "In Progress";
		}else if ("MixedAction".equalsIgnoreCase(status)) {
			action = "Mixed Action";
		}

		return action;
	}

	@Override
	public void deleteTTBCompareComment(List<TTBAmendment> amends) {
		List<TTBAmendmentEntity> amendEntities = this.transformTTBAmdtBeanToTTBAmdtEntity(amends);
		this.trueUpEntityManager.deleteTTBCompareComment(amendEntities);
	}

	@Override
	public void deleteCBPCompareComment(List<CBPAmendment> amends) {
		List<CBPAmendmentEntity> amendEntities = this.transformCBPAmdtBeanToCBPAmdtEntity(amends);
		this.trueUpEntityManager.deleteCBPCompareComment(amendEntities);
	}

	@Override
	public CBPMetrics generateMetricData(long fiscalYr) {
		CBPMetricsEntity entity = this.metricsEntityManager.generateMetricData(fiscalYr);
		CBPMetrics result = new CBPMetrics();
		if (entity != null) {
			Mapper mapper = this.mapperWrapper.getMapper();
			mapper.map(entity, result);
		}
		return result;
	}

	@Override
	public CBPMetrics getGeneratedMetricData(long fiscalYr) {
		CBPMetricsEntity entity = this.metricsEntityManager.getGeneratedMetricData(fiscalYr);
		CBPMetrics result = new CBPMetrics();
		if (entity != null) {
			Mapper mapper = this.mapperWrapper.getMapper();
			mapper.map(entity, result);
		} else {
			return null;
		}
		return result;
	}

	public DocExport getUnmatchedRecordExport(long fiscalYr, String recordType) {
		DocExport doc = new DocExport();
		if ("CBPLine".equalsIgnoreCase(recordType)) {
			doc = this.exportCBPDetails(fiscalYr);
		} else if ("CBPSummary".equalsIgnoreCase(recordType)) {
			doc = this.exportSummary(fiscalYr);
		}

		return doc;
	}

	private DocExport exportCBPDetails(long fiscalYr) {
		List<CBPLineItemEntity> cbpLineItemEntityList = this.trueUpEntityManager.getUnmatchedDetailsExport(fiscalYr);
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		StringBuilder filename = new StringBuilder("Unmatched_Records_CBP_DETAILS_");
		filename.append(fiscalYr);
		filename.append(".csv");
		CsvStringBuilder csv = new CsvStringBuilder();

		csv.csvAppendNewLine(
				"Importer Name, Importer Number, Ultimate Consignee Name, Ultimate Consignee Number, Entry Summary Number, Entry Type, "
						+ "Entry Summary Date, Entry Date, Import Date, Line Number, HTS Number, HTS Description, Line Tariff Qty 1, "
						+ "Line Tariff UOM1, Line Tariff Goods Value Amount, IR Tax Amount");
		for (CBPLineItemEntity cbpLineItemEntity : cbpLineItemEntityList) {

			Double lineTariffGoodsValueAmount = new Double(cbpLineItemEntity.getLineTariffGoodsValueAmount());
			Double irTaxAmount = new Double(cbpLineItemEntity.getIrTaxAmount());

			csv.csvAppendRawQuotes(cbpLineItemEntity.getImporterName());

			csv.csvAppend(cbpLineItemEntity.getImporterNumber());
			csv.csvAppendRawQuotes(cbpLineItemEntity.getUltimateConsigneeName());
			csv.csvAppend(cbpLineItemEntity.getUltimateConsigneeNumber());
			csv.csvAppend(cbpLineItemEntity.getEntrySummaryNumber());
			csv.csvAppendRawQuotes(cbpLineItemEntity.getEntryTypeCode());
			csv.csvAppendRawQuotes(CTPUtil.getDateStringMMDDYY(cbpLineItemEntity.getEntrySummaryDate()));
			csv.csvAppendRawQuotes(CTPUtil.getDateStringMMDDYY(cbpLineItemEntity.getEntryDate()));
			csv.csvAppendRawQuotes(CTPUtil.getDateStringMMDDYY(cbpLineItemEntity.getImportDate()));
			csv.csvAppend(cbpLineItemEntity.getLineNumber());
			csv.csvAppend(cbpLineItemEntity.getHtsNumber());
			csv.csvAppendRawQuotes(cbpLineItemEntity.getHtsDescription());
			csv.csvAppend(cbpLineItemEntity.getLineTarrifQty1());
			csv.csvAppend(cbpLineItemEntity.getLineTarrifUOM1());
			csv.csvAppendRawQuotes(formatter.format(lineTariffGoodsValueAmount));
			csv.csvAppendRawQuotes(formatter.format(irTaxAmount));
			csv.csvAppendNewLine(null);
		}

		DocExport doc = new DocExport();
		doc.setFileName(filename.toString());
		doc.setCsv(csv.toString());
		return doc;
	}

	private DocExport exportSummary(long fiscalYr) {
		List<CBPSummaryFileEntity> cbpSummaryFileEntityList = this.trueUpEntityManager
				.getUnmatchedSummaryExport(fiscalYr);
		NumberFormat formatter = NumberFormat.getCurrencyInstance();

		StringBuilder filename = new StringBuilder("Unmatched_Records_CBP_SUMMARY_");
		filename.append(fiscalYr);
		filename.append(".csv");

		CsvStringBuilder csv = new CsvStringBuilder();
		csv.csvAppendNewLine("Entry Summary Number, Importer Number, Importer Name, Entry Date,"
				+ " Entry Type Code and Short Description, Entry Summary Date, Ultimate Consignee Number, "
				+ "Ultimate Consignee Name, Estimated Tax, Total Ascertained Tax Amount");
		for (CBPSummaryFileEntity cbpSummaryFileEntity : cbpSummaryFileEntityList) {

			Double estimatedTax = new Double(cbpSummaryFileEntity.getEstimatedTax());
			Double totalAscertainedtax = new Double(cbpSummaryFileEntity.gettotalAscertainedtax());

			csv.csvAppend(cbpSummaryFileEntity.getentrySummaryNumber());
			csv.csvAppend(cbpSummaryFileEntity.getImporterNumber());
			csv.csvAppendRawQuotes(cbpSummaryFileEntity.getImporterName());
			csv.csvAppend(CTPUtil.getDateStringMMDDYY(cbpSummaryFileEntity.getEntryDate()));
			csv.csvAppendRawQuotes(cbpSummaryFileEntity.getentryTypeCodeDescription());
			csv.csvAppend(CTPUtil.getDateStringMMDDYY(cbpSummaryFileEntity.getentrySummaryDate()));
			csv.csvAppend(cbpSummaryFileEntity.getultimateConsigneeNumber());
			csv.csvAppendRawQuotes(cbpSummaryFileEntity.getultimateConsigneeName());
			csv.csvAppendRawQuotes(formatter.format(estimatedTax));
			csv.csvAppendRawQuotes(formatter.format(totalAscertainedtax));
			csv.csvAppendNewLine(null);

		}

		DocExport doc = new DocExport();
		doc.setFileName(filename.toString());
		doc.setCsv(csv.toString());
		return doc;
	}

	@Override
	public CBPMetrics insertAcceptedCBPLegacyData(long fiscalYr) {
		this.CBPLegacyEntityManager.insertAcceptedCBPLegacyData(fiscalYr);
		return getGeneratedMetricData(fiscalYr);
	}

	@Override
	public DocExport exportSummaryOfIngestedData(String fileType, String companyId, String ein, String fiscalYear,
			String quarter, String tobaccoType, String IngestionType) {
		CsvStringBuilder csv = new CsvStringBuilder();
		StringBuilder fileName;

		if(IngestionType.equalsIgnoreCase("missing")) {
			List<CBPDetailsFile> detailsResult = this.simpleSQLManager.getDetailsFileExport(ein, companyId, fiscalYear,
					quarter, tobaccoType,IngestionType);
			this.buildDetailsFileExport(detailsResult, csv);
			fileName = new StringBuilder("CBP_Missing_Entry_").append(fileType).append("_").append(ein).append("_")
					.append(fiscalYear).append("_").append(quarter).append("_").append(tobaccoType).append(".csv");
		} else {
			fileName = new StringBuilder("CBP_Ingested_").append(fileType).append("_").append(ein).append("_")
					.append(fiscalYear).append("_").append(quarter).append("_").append(tobaccoType).append(".csv");
			
			switch (fileType) {
			case "summary":
				List<CBPSummaryFile> summaryResult = this.simpleSQLManager.getSummaryFileExport(ein, companyId, fiscalYear,
						quarter, tobaccoType);
				this.buildSummaryFileExport(summaryResult, csv);
				break;
			case "details":
				List<CBPDetailsFile> detailsResult = this.simpleSQLManager.getDetailsFileExport(ein, companyId, fiscalYear,
						quarter, tobaccoType,IngestionType);
				this.buildDetailsFileExport(detailsResult, csv);
				break;
			}
		}

		

		DocExport export = new DocExport();
		export.setFileName(fileName.toString());
		export.setCsv(csv.toString());
		return export;
	}

	private void buildDetailsFileExport(List<CBPDetailsFile> detailsResult, CsvStringBuilder csv) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		csv.csvAppendNewLine(
				"Entry Summary Number,Importer Number,Importer Name,Ultimate Consignee Number,Ultimate Consignee Name,"
						+ "Entry Summary Date,Entry Date,Import Date,Entry Type,Line Number,HTS Number,HTS Description,Line Tariff Qty 1, "
						+ "Line Tariff Goods Value Amount, IR Tax Amount");
		for (CBPDetailsFile cbpLineItemEntity : detailsResult) {

			Double lineTariffGoodsValueAmount = new Double(cbpLineItemEntity.getLineTariffGoodsValueAmount());
			Double irTaxAmount = new Double(cbpLineItemEntity.getIrTaxAmount());

			csv.csvAppend(cbpLineItemEntity.getEntrySummaryNumber());
			csv.csvAppend(cbpLineItemEntity.getImporterNumber());
			csv.csvAppendRaw("\"");
			csv.csvAppendRaw(cbpLineItemEntity.getImporterName());
			csv.csvAppendRaw("\",");
			csv.csvAppend(cbpLineItemEntity.getUltimateConsigneeNumber());
			csv.csvAppendRaw("\"");
			csv.csvAppendRaw(cbpLineItemEntity.getUltimateConsigneeName());
			csv.csvAppendRaw("\",\"");
			csv.csvAppendRaw(cbpLineItemEntity.getEntrySummaryDate());
			csv.csvAppendRaw("\",\"");
			csv.csvAppendRaw(cbpLineItemEntity.getEntryDate());
			csv.csvAppendRaw("\",\"");
			csv.csvAppendRaw(cbpLineItemEntity.getImportDate());
			csv.csvAppendRaw("\",\"");
			csv.csvAppendRaw(cbpLineItemEntity.getEntryTypeCode());
			csv.csvAppendRaw("\",");
			csv.csvAppend(cbpLineItemEntity.getLineNumber());
			csv.csvAppend(cbpLineItemEntity.getHtsNumber());
			csv.csvAppendRaw("\"");
			csv.csvAppendRaw(cbpLineItemEntity.getHtsDescription());
			csv.csvAppendRaw("\",\"");
			csv.csvAppendFormatRaw("###,##0.##",cbpLineItemEntity.getLineTarrifQty1());
			csv.csvAppendRaw("\",\"");
			csv.csvAppendRaw(formatter.format(lineTariffGoodsValueAmount));
			csv.csvAppendRaw("\",\"");
			csv.csvAppendRaw(formatter.format(irTaxAmount));
			csv.csvAppendRaw("\"");
			csv.csvAppendNewLine(null);
		}
	}

	private void buildSummaryFileExport(List<CBPSummaryFile> summaryResult, CsvStringBuilder csv) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance();

		csv.csvAppendNewLine(
				"Entry Summary Number,Importer Number,Importer Name,Ultimate Consignee Number,Ultimate Consignee Name,Entry Summary Date,Entry Date,"
						+ "Entry Type Code and Short Description,Estimated Tax,Total Ascertained Tax Amount");
		for (CBPSummaryFile cbpSummaryFileEntity : summaryResult) {

			Double estimatedTax = new Double(cbpSummaryFileEntity.getEstimatedTax());
			Double totalAscertainedtax = new Double(cbpSummaryFileEntity.getTotalAscertainedtax());

			csv.csvAppend(cbpSummaryFileEntity.getEntrySummaryNumber());
			csv.csvAppend(cbpSummaryFileEntity.getImporterNumber());
			csv.csvAppendRaw("\"");
			csv.csvAppendRaw(cbpSummaryFileEntity.getImporterName());
			csv.csvAppendRaw("\",");
			csv.csvAppend(cbpSummaryFileEntity.getUltimateConsigneeNumber());
			csv.csvAppendRaw("\"");
			csv.csvAppendRaw(cbpSummaryFileEntity.getUltimateConsigneeName());
			csv.csvAppendRaw("\",");
			csv.csvAppend(cbpSummaryFileEntity.getEntrySummaryDate());
			csv.csvAppend(cbpSummaryFileEntity.getEntryDate());
			csv.csvAppendRaw("\"");
			csv.csvAppendRaw(cbpSummaryFileEntity.getEntryTypeCodeDescription());
			csv.csvAppendRaw("\",\"");
			csv.csvAppendRaw(formatter.format(estimatedTax));
			csv.csvAppendRaw("\",\"");
			csv.csvAppendRaw(formatter.format(totalAscertainedtax));
			csv.csvAppendRaw("\"");
			csv.csvAppendNewLine(null);

			// csvBuilder.csvAppendFormat("###,##0.000",
			// result.getRemovalQty());
			// csvBuilder.csvAppendFormat("$###,###,##0.00",result.getTaxesPaid());

		}
	}

	@Override
	public List<CBPEntry> getCBPIngestedLineEntryInfo(long cbEntryId) {
		// TODO Auto-generated method stub
		 List<CBPEntry> cbpEntry = this.transformCBPEntryListToBeans((List<CBPEntryEntity>)trueUpEntityManager.getCBPIngestedLineEntryInfo(cbEntryId));
		return cbpEntry;
	}
	
	@Override
	public List<IngestionWorkflowContext> saveorUpdateIngestionWorkflowContext(IngestionWorkflowContext model) {
		
		List<IngestionWorkflowContextEntity> ctxEntityList = this.transformBeantoEntity(model);

		List<IngestionWorkflowContextEntity> ctx = this.trueUpEntityManager.saveorUpdateIngestionWorkflowContext(ctxEntityList.get(0));

		return this.transformIngestionWorkflowContextListToBeans(ctx);
	
	}
		 

	private List<IngestionWorkflowContextEntity> transformBeantoEntity(IngestionWorkflowContext ctx) {
		
		List<IngestionWorkflowContextEntity> ctxList = new ArrayList<>();
	
		if (ctx != null) {			
			Mapper mapper = mapperWrapper.getMapper();				
			IngestionWorkflowContextEntity ctxEntity = new IngestionWorkflowContextEntity();				
			mapper.map(ctx, ctxEntity);
            ctxList.add(ctxEntity);		
		}
		
		return ctxList;
	}
	
	
	
	
	@Override
	public List<IngestionWorkflowContext> getIngestionWorkflowContext(long fiscal_yr) {
		
		 List<IngestionWorkflowContext> contexts = this.transformIngestionWorkflowContextListToBeans((List<IngestionWorkflowContextEntity>)trueUpEntityManager.getIngestionWorkflowContext(fiscal_yr));
		 return contexts;
	}
	
	
	 @Override
	 public  List<IngestionWorkflowContext> getIngestionContexts(){
		 List<IngestionWorkflowContext> contexts = this.transformIngestionWorkflowContextListToBeans((List<IngestionWorkflowContextEntity>)trueUpEntityManager.getIngestionContexts());
		 return contexts;
	 }
	 
	private List<IngestionWorkflowContext> transformIngestionWorkflowContextListToBeans(List<IngestionWorkflowContextEntity> entities) {
		
		List<IngestionWorkflowContext> beanList = new ArrayList<>();
		if (entities != null) {
			Mapper mapper = mapperWrapper.getMapper();
			for (IngestionWorkflowContextEntity entry : entities) {
				IngestionWorkflowContext entryBean = new IngestionWorkflowContext();
				mapper.map(entry, entryBean);
				beanList.add(entryBean);
			}
		}
		return beanList;
	}
	
	

	

    /**
     * this business layer will get tufa data ingested data for each quareter and build the response
     * @param selDate TODO
     *
     */
    @Override
    public ImporterTufaIngestionDetails getImporterCompareDetailsAllTobaccoType(long companyId,long fiscalYr, long quarter, String permitType, String tobaccoClass, String ein) {
    	String secondaryDateFlag = "N";
    	if("entryDt".equalsIgnoreCase(getImporterCompareDetailsSelDate(tobaccoClass, fiscalYr,ein))){
    		secondaryDateFlag = "Y";
    	}
        List<TrueUpComparisonImporterDetail> tufaDetailsList = simpleSQLManager.getImporterComparisonDetails(ein, fiscalYr, quarter, permitType, tobaccoClass);
        List<TrueUpComparisonImporterIngestionDetail> ingestDetailsList = simpleSQLManager.getImporterComparisonIngestionDetails(ein, fiscalYr, quarter, permitType, tobaccoClass, secondaryDateFlag);
        ImporterTufaIngestionDetails imptDetails = new ImporterTufaIngestionDetails();
        List<ImporterDeltaDetails>detailsliList = new ArrayList<ImporterDeltaDetails>();
        double tufaTotal = 0.0;
        double ingestTotal=0.0;
        double dletaTotal = 0.0;
        boolean allTufaNa = true;
        boolean allIngestNa = true;
        boolean allDeltaNa = true;
        double tufaremovalQunatity=0.0;
        double ingestedRemovalQunatity=0.0;
        double mixedActionTax = 0.0;
        double mixedActionRemoval = 0.0;
        List<String> mixedActionMonthList = new ArrayList<String>();
        imptDetails.setTufaDetails(tufaDetailsList);
        imptDetails.setIngestedDetails(ingestDetailsList);
        for(TrueUpComparisonImporterIngestionDetail ingest : ingestDetailsList) {
            if(!ingest.getPeriodTotal().equalsIgnoreCase(NA_TOTAL)) {
                ingestTotal+=Double.parseDouble(ingest.getPeriodTotal());
                allIngestNa = false;
            } 
            for(TrueUpComparisonImporterDetail tufa : tufaDetailsList) {
                if(ingest.getPeriod().equalsIgnoreCase(tufa.getPeriod())) {
                    if(!tufa.getPeriodTotal().equalsIgnoreCase(NA_TOTAL)) {
                        tufaTotal+=Double.parseDouble(tufa.getPeriodTotal());
                        allTufaNa = false;
                    } 
                    ImporterDeltaDetails delta = new ImporterDeltaDetails();
                    delta.setPeriod(ingest.getPeriod());
                    double deltaTotal =0.0;
                    if(ingest.getPeriodTotal().equalsIgnoreCase(NA_TOTAL) && tufa.getPeriodTotal().equalsIgnoreCase(NA_TOTAL)) {
                        delta.setPeriodDelta(NA_TOTAL);
                    } else {
                        if(!ingest.getPeriodTotal().equalsIgnoreCase(NA_TOTAL)) {
                            deltaTotal-=Double.parseDouble(ingest.getPeriodTotal()); 
                        } if(!tufa.getPeriodTotal().equalsIgnoreCase(NA_TOTAL)) {
                            deltaTotal+=Double.parseDouble(tufa.getPeriodTotal()); 
                        }
                        delta.setPeriodDelta(String.format("%.2f",(deltaTotal)));
                    }
                    detailsliList.add(delta);
                    break;
                }
            }
        }
        imptDetails.setDetailDeltas(detailsliList);
        for(ImporterDeltaDetails delta : detailsliList) {
            if(!delta.getPeriodDelta().equalsIgnoreCase(NA_TOTAL)) {
                dletaTotal+=Double.parseDouble(delta.getPeriodDelta());
                allDeltaNa = false;
            } 
        }
        if(Math.abs(ingestTotal) >= 0.0 && !allIngestNa) {
            imptDetails.setDetailTotal(String.valueOf(ingestTotal));
        } else {
            imptDetails.setDetailTotal(NA_TOTAL);
        }
        if(Math.abs(ingestTotal) >= 0.0 && !allTufaNa) {
            imptDetails.setTufaTotal(String.valueOf(tufaTotal));
        } else {
            imptDetails.setTufaTotal(NA_TOTAL);
        }
        if(Math.abs(ingestTotal)>=0.0 && !allDeltaNa) {
            imptDetails.setDeltaTotal(String.valueOf(dletaTotal));
        } else {
            imptDetails.setDeltaTotal(NA_TOTAL);
        }
        if(imptDetails.getTufaTotal().equalsIgnoreCase(NA_TOTAL) || imptDetails.getDetailTotal().equalsIgnoreCase(NA_TOTAL)) {
        	if(imptDetails.getTufaTotal().equalsIgnoreCase(NA_TOTAL) && imptDetails.getDetailTotal().equalsIgnoreCase(NA_TOTAL)) {
        		imptDetails.setDataType(null);
        	} else if(imptDetails.getTufaTotal().equalsIgnoreCase(NA_TOTAL)) {
        		imptDetails.setDataType("INGESTED");
        	} else if(imptDetails.getDetailTotal().equalsIgnoreCase(NA_TOTAL)) {
        		imptDetails.setDataType("TUFA");
        	} else {
        		imptDetails.setDataType("MATCHED");
        	}
        } else {
        	imptDetails.setDataType("MATCHED");
        }
        //below block will populate the map of details for quarter for mixed action details for each month.
        if("MATCHED".equalsIgnoreCase(imptDetails.getDataType()) && "IMPT".equalsIgnoreCase(permitType)) {
        	List<Object[]> list = simpleSQLManager.getMixedActionDetaislForQuarter(companyId, fiscalYr, quarter, tobaccoClass);
        	if(CollectionUtils.isNotEmpty(list)) {
        		Map<String,String> mixedActionQuarterDetails = new HashMap<String, String>();
        		for (Object[] detail : list) {
        			mixedActionQuarterDetails.put(detail[0]!= null ?detail[0].toString().toUpperCase():detail[2].toString().toUpperCase(), detail[1] != null ?detail[1].toString():detail[3].toString());
            	}
        		imptDetails.setMixedActionDetailsMap(mixedActionQuarterDetails);
        	}
        }
       
        if(imptDetails.getMixedActionDetailsMap() != null) {
         	mixedActionMonthList = new ArrayList<String>(imptDetails.getMixedActionDetailsMap().keySet());
         }
        for(TrueUpComparisonImporterDetail bean :tufaDetailsList) {
        	 List<String> temp = mixedActionMonthList;
        	if(null != bean.getRemovalQuantity() && StringUtils.isNotEmpty(bean.getRemovalQuantity())) {
        		tufaremovalQunatity+=Double.parseDouble(bean.getRemovalQuantity());
        	}
        	if(CollectionUtils.isNotEmpty(bean.getPeriodDetails())) {
        		for(TrueUpComparisonImporterDetailPeriod periodBean : bean.getPeriodDetails()) {
           		 if(temp.contains(periodBean.getMonth()) && imptDetails.getMixedActionDetailsMap().get(periodBean.getMonth()).equalsIgnoreCase("F")) {
           			 mixedActionTax+=!periodBean.getTaxAmount().equalsIgnoreCase("NA")?Double.parseDouble(periodBean.getTaxAmount()):0.0;
           			 mixedActionRemoval+=(new Long(periodBean.getQuantityRemoved()).doubleValue());
           		 }
           	}
        	}
        	
        } 
		for(TrueUpComparisonImporterIngestionDetail bean : ingestDetailsList) {
			double monthlyRemovalQuantity = 0.0;
		  if(CollectionUtils.isNotEmpty(bean.getPeriodDetails())) {
			  for(TrueUpComparisonImporterIngestionDetailPeriod innerBean:bean.getPeriodDetails()) {
				  monthlyRemovalQuantity+=new Double(innerBean.getQtyRemoved()).doubleValue();
				  if(mixedActionMonthList.contains(innerBean.getMonth().toUpperCase()) &&  imptDetails.getMixedActionDetailsMap().get(innerBean.getMonth().toUpperCase()).equalsIgnoreCase("I")) {
	          			 mixedActionTax+=Double.parseDouble(innerBean.getTaxAmount());
	          			 mixedActionRemoval+=(new Double(innerBean.getQtyRemoved()).doubleValue());
	          		 }
			  }
			  bean.setRemovalQuantity((new Double(monthlyRemovalQuantity)).toString());
			  ingestedRemovalQunatity+=monthlyRemovalQuantity;
		  }
		}
//		for(TrueUpComparisonImporterIngestionDetail bean :ingestDetailsList) {
//	       	List<String> temp = mixedActionMonthList;
//	       	if(CollectionUtils.isNotEmpty(bean.getPeriodDetails())) {
//	       		for(TrueUpComparisonImporterIngestionDetailPeriod periodBean : bean.getPeriodDetails()) {
//	          		 if(temp.contains(periodBean.getMonth()) &&  imptDetails.getMixedActionDetailsMap().get(periodBean.getMonth().toUpperCase()).equalsIgnoreCase("I")) {
//	          			 mixedActionTax+=Double.parseDouble(periodBean.getTaxAmount());
//	          			 mixedActionRemoval+=(new Long(periodBean.getQtyRemoved()).doubleValue());
//	          		 }
//	          	}
//	          }
//       	}
       
    
        imptDetails.setTufaRemovalQunatity((new Double(tufaremovalQunatity)).toString());
        imptDetails.setIngestedRemovalQunatity((new Double(ingestedRemovalQunatity)).toString());
        imptDetails.setMixedActionQuantityRemoved(new Double(mixedActionRemoval).toString());
        imptDetails.setMixedActionTax(new Double(mixedActionTax).toString());
        return imptDetails;
    }
    @Override
    public void saveOrUpdateDateSelection(String ein,long fiscalYr,String tobaccoClass,String selDateType) throws ParseException {
    	if ("ROLL YOUR OWN".equalsIgnoreCase(tobaccoClass) || "RYO".equalsIgnoreCase(tobaccoClass)) {
			tobaccoClass = "Roll-Your-Own";
		}
    	CBPDateConfigEntity cbpDateConfigEntity = new CBPDateConfigEntity();
    	cbpDateConfigEntity.setDateType(selDateType);
    	cbpDateConfigEntity.setEin(ein);
    	cbpDateConfigEntity.setFiscalYr(fiscalYr);
    	cbpDateConfigEntity.setTobaccoClassNm(tobaccoClass);
    	this.trueUpEntityManager.saveOrUpdateDateSelection(cbpDateConfigEntity);
    	
    }
    @Override
    /**
     * This returns the date selection on compare details page
     * @param tobaccoClass
     * @param fiscalYr
     * @return
     */
	public String getImporterCompareDetailsSelDate(String tobaccoClass, long fiscalYr,String ein){
		if ("ROLL YOUR OWN".equalsIgnoreCase(tobaccoClass) || "RYO".equalsIgnoreCase(tobaccoClass)) {
			tobaccoClass = "Roll-Your-Own";
		}
		return this.trueUpEntityManager.getImporterCompareDetailsSelDate(tobaccoClass,fiscalYr,ein);
	}

	@Override
	public List<TrueUpComparisonResults> getAllDeltasDetailsForEIN(String ein, String permitType,long fiscalYear) {
		return this.simpleSQLManager.getAllAllDeltasDetailsForEIN(fiscalYear, permitType,ein);
		
	}

	@Override
	public List<String> getAllNonZeroDeltaTobaccoClass(String ein, String permitType, long fiscalYear) {
		return this.simpleSQLManager.getAllNonZeroDeltaTobaccoClass(fiscalYear, permitType, ein);
	}
	@Override
	public void deleteCompareCommentDetailOnly(long commSeqCbp,long commSeqAllDelta, String permitType) {
		this.trueUpEntityManager.deleteCompareCommentDetailOnly(commSeqCbp,commSeqAllDelta,permitType);
	}
	@Override
	public void updateCompareCommentDetailOnly(long commSeqCbp,long commSeqAllDelta, String usercomment,String permitType) {
		this.trueUpEntityManager.updateCompareCommentDetailOnly(commSeqCbp, commSeqAllDelta, usercomment,permitType);
	}
	
	private String getUOMForProduct(String product) {
		if (product == null) {
			return null;
		} 
		
		product = product.toUpperCase();
		if (product.contains("CIGARS") || product.contains("CIGARETTES")) {
			return "STICKS";
		} else {
			return "POUNDS";
		}
	}

	@Override
	public List<ComparisonCommentAttachmentEntity> findCommentDocbyId(long cbpCommentSeq, long ttbCommentSeq, long allDeltaCommentSeq) {
		return this.trueUpEntityManager.findCommentDocbyId(cbpCommentSeq, ttbCommentSeq, allDeltaCommentSeq);
	}
    @Override
    public Attachment comparisonCommentAttachmentDownload(long docId) throws SQLException {
    	ComparisonCommentAttachmentEntity attachment = this.trueUpEntityManager.getCommentAttachment(docId);
		Attachment result = new Attachment();
		result.setReportPdf(trueUpEntityManager.getByteArray(attachment.getReportPdf()));
		result.setDocDesc(attachment.getDocDesc());
		
		return result;
	}
    @Override
    public byte[] getComparisonCommentAttachmenttAsByteArray(long docId) throws  SQLException{
    	return this.trueUpEntityManager.getComparisonCommentAttachmentAsByteArray(docId);
    }
    
	public void addComparisonCommentAttach(ComparisonCommentAttachmentEntity comparisoncommentattachment, MultipartFile file)throws IOException, SQLException{
		ComparisonCommentAttachmentEntity ccae = new ComparisonCommentAttachmentEntity();
		Blob blob = null;
		if(comparisoncommentattachment.getDocumentId() != 0){
			ccae = this.trueUpEntityManager.getCommentAttachment(comparisoncommentattachment.getDocumentId());
			ccae.setDocDesc(comparisoncommentattachment.getDocDesc());
		}else {
			//Done to match legacy files 
			//byte[] pretext = "data:application/pdf;base64,".getBytes();
			StringBuffer pretextBuffer = new StringBuffer("data:");
			pretextBuffer.append(file.getContentType());
			pretextBuffer.append(";base64,");
			byte[] pretext = pretextBuffer.toString().getBytes();
			byte[] encodedFile = Base64.encode(file.getBytes());
			
			byte[] combination = new byte[pretext.length + encodedFile.length];
			System.arraycopy(pretext, 0, combination, 0, pretext.length);
			System.arraycopy(encodedFile, 0, combination, pretext.length, encodedFile.length);
			
			blob = this.trueUpEntityManager.getBlob(combination);
			comparisoncommentattachment.setReportPdf(blob);
			ccae = comparisoncommentattachment;
		}
		
		this.trueUpEntityManager.addComparisonCommentAttachment(ccae);
		
		if(blob != null) {
			blob.free();
		}
	}
	@Override
	public void deleteDocById(long docId){
		this.trueUpEntityManager.deleteDocById(docId);
	}

	@Override
	public void processAdjustments(String fiscalYaer) {
		simpleSQLManager.processAdjustments(fiscalYaer);
		
	}
	
	@Override
	public List<Long> getFiscalYearsWithNewDeltas() {
		return this.simpleSQLManager.getFiscalYearsWithNewDeltas();
	}

	@Override
	public List<PreviousTrueupInfo> getPreviousTrueupInfo(String type, long currentYear, String ein) {
		return this.simpleSQLManager.getPreviousTrueupInfo(type, currentYear, ein);
		
	}

}

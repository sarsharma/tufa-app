/**
 * 
 */
package gov.hhs.fda.ctp.biz.useradmin;

import java.util.Set;

import gov.hhs.fda.ctp.biz.registration.UserDTO;
import gov.hhs.fda.ctp.persistence.model.RoleEntity;

/**
 * The Interface UserAdminBizComp.
 *
 * @author avitta
 */
public interface UserAdminBizComp  {
	
	/**
	 * Verify.
	 *
	 * @return true, if successful
	 */
	// sample method to verify
	public boolean verify();
	
	/**
	 * Gets the manu data flag.
	 *
	 * @return the manu data flag
	 */
	// sample method to get data
	public boolean getManuDataFlag();
	
	/**
	 * Register user.
	 *
	 * @param u the u
	 */
	public UserDTO registerUser(UserDTO u);
	
	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	public Set<UserDTO> getUsers();
	
	/**
	 * Gets the user.
	 *
	 * @param email the email
	 * @return the user
	 */
	public UserDTO getUser(String email);
	
	/**
	 * Gets the user.
	 *
	 * @param id
	 * @return the user
	 */
	public UserDTO getUser(long id);
	
	/**
	 * Get all the roles.
	 * @return
	 */
	public Set<RoleEntity> getRoles();
	
	/**
	 * Update.
	 *
	 * @param contact the contact
	 * @return contact
	 */
	public UserDTO update(UserDTO user);

	
}
/**
 * 
 */
package gov.hhs.fda.ctp.biz.useradmin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.biz.registration.UserDTO;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.persistence.UserAdminEntityManager;
import gov.hhs.fda.ctp.persistence.model.RoleEntity;
import gov.hhs.fda.ctp.persistence.model.UserEntity;

/**
 * The Class UserAdminBizCompImpl.
 */
@Component
@Transactional
public class UserAdminBizCompImpl implements UserAdminBizComp {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(UserAdminBizCompImpl.class);

	/** The user entity manager. */
	private UserAdminEntityManager userEntityManager;

	/** The mapper factory. */
	@Autowired
	DozerBeanMapperFactoryBean mapperFactory;

	@Autowired
	private MapperWrapper mapperWrapper;

	/**
	 * Sets the user admin entity manager.
	 *
	 * @param userAdminEntityManager
	 *            the new user admin entity manager
	 */
	@Autowired
	public void setUserAdminEntityManager(UserAdminEntityManager userAdminEntityManager) {
		this.userEntityManager = userAdminEntityManager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gov.hhs.fda.ctp.biz.useradmin.UserAdminBizComp#verify()
	 */
	@Override
	public boolean verify() {
		logger.debug("inside verify in biz ");
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gov.hhs.fda.ctp.biz.useradmin.UserAdminBizComp#getManuDataFlag()
	 */
	@Override
	public boolean getManuDataFlag() {
		logger.debug("inside getManuData in biz ");
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.biz.useradmin.UserAdminBizComp#registerUser(gov.hhs.fda.
	 * ctp.biz.registration.UserDTO)
	 */
	@Override
	public UserDTO registerUser(UserDTO userDTO) {
		UserEntity userEntity = transformUserDtoToEntity(userDTO);
		userEntityManager.saveEntity(userEntity);
		return transformEntityToUserDTO(userEntity);
	}

	private UserDTO transformEntityToUserDTO(UserEntity ue) {
		Mapper mapper = mapperWrapper.getMapper();
		UserDTO dto = new UserDTO();
		dto.setUserId(ue.getUserId());
		mapper.map(ue, dto);
		dto.setEnabled(ue.getEnabled() == 0 ? "Inactive" : "Active");
		// current state : Single user is associated with single role
		for (Iterator iterator = ue.getRoles().iterator(); iterator.hasNext();) {
			RoleEntity role = (RoleEntity) iterator.next();
			dto.setRole(role.getName());
		}
		return dto;
	}

	/**
	 * Assemble user entity.
	 *
	 * @param u
	 *            the u
	 * @return the user entity
	 */
	private UserEntity transformUserDtoToEntity(UserDTO userDto) {

		UserEntity userEntity = new UserEntity();

		userEntity.setFirstName(userDto.getFirstName());
		userEntity.setLastName(userDto.getLastName());
		userEntity.setEmail(userDto.getEmail());
		userEntity.setPassword(userDto.getPassword());
		userEntity.setEnabled("Y".equals(userDto.getEnabled()) ? (byte) 1 : (byte) 0);
		userEntity.setEndDate(userDto.getEndDate());
		userEntity.setLastLoginDate(userDto.getLastLoginDate());
		userEntity.setPhone1(userDto.getPhone1());
		userEntity.setUserId(userDto.getUserId());

		RoleEntity roleEntity = userEntityManager.getRoleByName(userDto.getRole());
		List<RoleEntity> listRoles = new ArrayList<>();
		listRoles.add(roleEntity);
		userEntity.setRoles(listRoles);

		return userEntity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gov.hhs.fda.ctp.biz.useradmin.UserAdminBizComp#getUsers()
	 */
	@Override
	public Set<UserDTO> getUsers() {
		Mapper mapper = mapperWrapper.getMapper();
		List<UserEntity> entityUsers = userEntityManager.getUsers();
		Set<UserDTO> users = new TreeSet<>();

		for (UserEntity user : entityUsers) {
			UserDTO dto = new UserDTO();
			mapper.map(user, dto);
			dto.setRole(user.getRoles().get(0).getRoleDescription());
			if (user.getEnabled() == 0) {
				dto.setEnabled("Inactive");
			} else {
				dto.setEnabled("Active");
			}
			users.add(dto);
		}
		return users;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.biz.useradmin.UserAdminBizComp#getUser(java.lang.String)
	 */
	@Override
	public UserDTO getUser(String email) {
		UserEntity userEntity = userEntityManager.getUser(email);
		UserDTO dto = new UserDTO();
		dto.setUserId(userEntity.getUserId());
		dto.setEmail(userEntity.getEmail());
		dto.setFirstName(userEntity.getFirstName());
		dto.setLastName(userEntity.getLastName());
		dto.setRole(userEntity.getRoles().isEmpty() ? "ROLE_ADMIN"
				: ((RoleEntity) (userEntity.getRoles().get(0))).getName());

		return dto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gov.hhs.fda.ctp.biz.useradmin.UserAdminBizComp#getRoles
	 */
	@Override
	public Set<RoleEntity> getRoles() {
		return userEntityManager.getRoles();
	}

	@Override
	public UserDTO getUser(long id) {
		logger.debug(" - Entering readById - ");
		UserDTO dto = transformEntityToUserDTO(userEntityManager.getUser(id));
		logger.debug(" - Exiting readById - ");
		return dto;
	}

	@Override
	public UserDTO update(UserDTO user) {
		logger.debug(" - Entering update - ");
		UserEntity userEntity = userEntityManager.update(transformUserDtoToEntity(user));
		logger.debug(" - Exiting update - ");
		return transformEntityToUserDTO(userEntity);
	}

}
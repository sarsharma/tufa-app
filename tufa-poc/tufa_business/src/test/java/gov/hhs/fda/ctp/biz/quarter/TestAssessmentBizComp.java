package gov.hhs.fda.ctp.biz.quarter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;

import gov.hhs.fda.ctp.biz.config.BizTestConfig;
import gov.hhs.fda.ctp.common.beans.AssessmentFilter;
import gov.hhs.fda.ctp.persistence.model.AssessmentEntity;

/**
 * The Class TestAssessmentBizComp.
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextHierarchy({
    @ContextConfiguration(classes = BizTestConfig.class )
})
public class TestAssessmentBizComp {
	
	/** The assessment biz comp. */
	@Autowired
	private AssessmentBizComp assessmentBizComp; 

	/**
	 * Test get assessments.
	 */
	@Test
	public void testGetAssessments() {
		List<AssessmentEntity> assessments;
		AssessmentFilter filter = new AssessmentFilter();
		
		Set<Integer> quarters = new HashSet<>();
		quarters.add(1);
		quarters.add(2);
		filter.setQuarterFilters(quarters);
		
		Set<Integer> years = new HashSet<>();
		years.add(1801);
		years.add(2014);
		filter.setFiscalYrFilters(years);
		
		Set<String> status = new HashSet<>();
		status.add("N");
		status.add("Y");
		filter.setStatusFilters(status);
		
		assessments = assessmentBizComp.getAssessments(filter);
		assert (assessments != null);
	}

}

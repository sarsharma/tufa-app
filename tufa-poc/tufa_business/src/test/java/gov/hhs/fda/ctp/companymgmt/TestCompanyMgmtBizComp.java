/**
 * 
 */
package gov.hhs.fda.ctp.companymgmt;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;

import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.biz.config.BizTestConfig;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.CompanyHistory;
import gov.hhs.fda.ctp.common.beans.CompanyPermitHistory;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.persistence.CompanyMgmtEntityManager;
import gov.hhs.fda.ctp.persistence.PermitEntityManager;
import gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager;
import gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;

/**
 * The Class TestCompanyMgmtBizComp.
 *
 * @author tgunter
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextHierarchy({
    @ContextConfiguration(classes = BizTestConfig.class )
})
public class TestCompanyMgmtBizComp {
    
    /** The company mgmt entity manager. */
    @Autowired
    CompanyMgmtEntityManager companyMgmtEntityManager;
    
    /** The permit mgmt entity manager. */
    @Autowired
    PermitEntityManager permitMgmtEntityManager;
    
    /** The simple SQL manager. */
    @Autowired
    SimpleSQLManager simpleSQLManager;
    
    /** The company mgmt biz comp. */
    @Autowired
    CompanyMgmtBizComp companyMgmtBizComp;
    
    /** The permit period entity manager. */
    @Autowired
    PermitPeriodEntityManager permitPeriodEntityManager;
    
    /** The reference code entity manager. */
    @Autowired
    ReferenceCodeEntityManager referenceCodeEntityManager;

    /** The mock company. */
    private CompanyEntity mockCompany = this.createMockCompany();
    
    /** The mock permit entity. */
    private PermitEntity mockPermitEntity = this.createMockPermitEntity();
    
    /** The mock permit list. */
    private List<PermitEntity> mockPermitList = this.createMockPermitList();
    
    /** The mock permit history list. */
    private List<CompanyPermitHistory> mockPermitHistoryList = this.creatMockPermitHistoryList();
       
    /**
     * Gets the company by id.
     *
     * @return the company by id
     * @throws Exception the exception
     */
    @Test
    public void getCompanyById() throws Exception {
        when(
                this.referenceCodeEntityManager.getValue(
                  (String)notNull(), (String)notNull()
                )
              ).thenReturn("TBD");

        given(this.companyMgmtEntityManager.getCompanyById(1)).willReturn(mockCompany);
        Company company = companyMgmtBizComp.getCompany(1);
        Assert.assertEquals(company.getLegalName(),mockCompany.getLegalName());
        Assert.assertEquals(company.getPermitSet().size(),1);
    }

    /**
     * Update the company.
     */
    @Test
    public void updateCompany() {
        when(
                this.companyMgmtEntityManager.updateCompany(
                  (CompanyEntity)notNull()
                )
              ).thenReturn(mockCompany);
        Company company = companyMgmtBizComp.updateCompany(new Company());
        Assert.assertEquals(company.getLegalName(),mockCompany.getLegalName());
        Assert.assertEquals(company.getPermitSet().size(),1);
    }

    /**
     * Creates the company.
     */
    @Test
    public void createCompany() {
        when(
                this.companyMgmtEntityManager.createCompany(
                  (CompanyEntity)notNull()
                )
              ).thenReturn(mockCompany);
        Company company = companyMgmtBizComp.createCompany(new Company());
        Assert.assertEquals(company.getLegalName(),mockCompany.getLegalName());
        Assert.assertEquals(company.getPermitSet().size(),1);
    }
        
    /**
     * Gets the permit.
     *
     * @return the permit
     */
    @Test
    public void getPermit(){
    	
    	when(
                this.permitMgmtEntityManager.getPermit(anyLong())
              ).thenReturn(this.mockPermitEntity);
    	
    	
    	Permit permit = companyMgmtBizComp.getPermit(1l);
    	Assert.assertEquals(permit.getPermitNum(),"TTB123");
    	
    }
    
    /**
     * Creates the permit.
     */
    @Test
    public void createPermit(){
    	
    	when(
                this.companyMgmtEntityManager.getCompanyById( anyLong())
              ).thenReturn(this.mockCompany);
    	
    	when(
                this.permitMgmtEntityManager.createPermit(
                  (PermitEntity)notNull()
                )
              ).thenReturn(this.mockPermitEntity);
    	
    	 Company company= companyMgmtBizComp.createPermit(new Permit());
    	 Assert.assertEquals(company.getPermitNumber(),"TTB123");
    	 
    }
    
    
    /**
     * Update permit.
     */
    @Test
    public void updatePermit() {
    	
    	when(
                this.permitMgmtEntityManager.getPermit(
                 anyLong()
                )
              ).thenReturn(this.mockPermitEntity);
    	
        when(
                this.permitMgmtEntityManager.updatePermit(
                  (PermitEntity)notNull()
                )
              ).thenReturn(this.mockPermitEntity);
        
        Permit newPermit= new Permit();
        
        newPermit.setIssueDt("2016-10-26T04:00:00.000Z");
        newPermit.setPermitNum("TTB1212");
        newPermit.setPermitStatusTypeCd("ACTV");
        
        Company company = companyMgmtBizComp.updatePermit(newPermit);
        Assert.assertEquals(company.getPermitNumber(),"TTB1212");
    }
    
    /**
     * Gets the permits.
     *
     * @return the permits
     */
    @Test
    public void getPermits(){
    	
    	 when(
                 this.companyMgmtEntityManager.getPermits(anyString(), anyLong())
               ).thenReturn(this.mockPermitList);
    	
    
    	  List<Permit> permits= companyMgmtBizComp.getPermits("ACTV", 10);
    	 
    	  Assert.assertEquals(permits.size(), 5);
    	 
    }
    
    
    /**
     * Gets the permit history.
     *
     * @return the permit history
     */
    @Test
    public void getPermitHistory(){
    	 when(
                 this.simpleSQLManager.getPermitHistory(anyLong())
               ).thenReturn(this.mockPermitHistoryList);
    	
    	  Permit permit = companyMgmtBizComp.getPermitHistory(new Permit());
    	  Assert.assertEquals(permit.getPermitHistoryList().size(),5);
    	 
    }
    
    /**
     * Gets the companies.
     *
     * @return the companies
     */
    @Test
    public void getCompanies(){
    	CompanySearchCriteria criteria = new CompanySearchCriteria();
    	criteria.setCompanyNameFilter("TESTLEGAL");  	
    	criteria = companyMgmtBizComp.getCompanies(criteria);
    	if(criteria != null){
    		List<CompanyHistory> histories = criteria.getResults();
    		if(histories != null){
    			assert(histories.size() == 1);
    		}
    	}
    }
    
    /**
     * Creates the mock company.
     *
     * @return the company entity
     */
    private CompanyEntity createMockCompany() {
        CompanyEntity mockCompany = new CompanyEntity();
        mockCompany.setCompanyId(1L);
        mockCompany.setLegalName("TESTLEGAL");
        mockCompany.setEIN("061234567");
        PermitEntity mockPermit = new PermitEntity();
        mockPermit.setPermitId(1L);
        mockPermit.setCompanyEntity(mockCompany);
        mockPermit.setPermitNum("TTB123");
        mockPermit.setPermitTypeCd("MANU");
        mockPermit.setPermitStatusTypeCd("ACTV");
        mockPermit.setCloseDt(new Date());
        Set<PermitEntity> permits = new HashSet<PermitEntity>();
        permits.add(mockPermit);
        mockCompany.setPermits(permits);
        return mockCompany;
    }

    
    /**
     * Creates the mock permit list.
     *
     * @return the list
     */
    private List<PermitEntity> createMockPermitList(){
    	
    	List<PermitEntity> permits = new ArrayList<>();
    	for(int i=0;i<5;i++){
    		permits.add(createMockPermitEntity());
    	}
    	return permits;
    	
    }
    
    /**
     * Creates the mock permit entity.
     *
     * @return the permit entity
     */
    private PermitEntity createMockPermitEntity(){
    	
        CompanyEntity mockCompany = new CompanyEntity();
        mockCompany.setCompanyId(1L);
        mockCompany.setLegalName("TESTLEGAL");
        mockCompany.setEIN("061234567");
       
        
        PermitEntity mockPermit = new PermitEntity();
        mockPermit.setPermitId(1L);
        mockPermit.setPermitNum("TTB123");
        mockPermit.setPermitTypeCd("MANU");
        mockPermit.setPermitStatusTypeCd("ACTV");
        mockPermit.setCloseDt(new Date());
        mockPermit.setCompanyEntity(mockCompany);
        
    	return mockPermit;
    }
    
    
    /**
     * Creat mock permit history list.
     *
     * @return the list
     */
    private List<CompanyPermitHistory> creatMockPermitHistoryList(){
    	
    	List<CompanyPermitHistory> pList = new ArrayList<>();
    	for(int i=0;i<5;i++){
    		CompanyPermitHistory pHistory = createMockPermitHistoryEntity();
    	   pList.add(pHistory);
    	}
    
    	return pList;
    }
    
    /**
     * Creates the mock permit history entity.
     *
     * @return the company permit history
     */
    private  CompanyPermitHistory createMockPermitHistoryEntity(){
    	
    	CompanyPermitHistory pHistory = new CompanyPermitHistory();
    	pHistory.setPermitNum("TBB2000");
    	pHistory.setMonth("06");
    	pHistory.setYear("1999");
    	pHistory.setReportStatusCd("NSTD");
    	pHistory.setPermitTypeCd("MANU");
    	
    	return pHistory;
    	
    }

}

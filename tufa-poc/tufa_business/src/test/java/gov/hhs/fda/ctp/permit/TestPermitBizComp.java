/**
 * 
 */
package gov.hhs.fda.ctp.permit;

import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;

import gov.hhs.fda.ctp.biz.config.BizTestConfig;
import gov.hhs.fda.ctp.biz.permit.PermitBizComp;
import gov.hhs.fda.ctp.common.beans.CompanyHistory;
import gov.hhs.fda.ctp.common.beans.CompanyPermitHistory;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.persistence.PermitEntityManager;
import gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.model.AddressEntity;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.ContactEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;

/**
 * The Class TestPermitBizComp.
 *
 * @author tgunter
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextHierarchy({
    @ContextConfiguration(classes = BizTestConfig.class )
})
public class TestPermitBizComp {
    
    /** The permit period entity manager. */
    @Autowired
    PermitPeriodEntityManager permitPeriodEntityManager;
    
    /** The permit entity manager. */
    @Autowired
    PermitEntityManager permitEntityManager;

    /** The permit biz comp. */
    @Autowired
    PermitBizComp permitBizComp;
    
    /** The simple SQL manager. */
    @Autowired
    SimpleSQLManager simpleSQLManager;

    /** The mock permit history criteria. */
    PermitHistoryCriteria mockPermitHistoryCriteria = this.createMockPermitHistoryCriteriaResult();
    
    /** The mock company search criteria. */
    CompanySearchCriteria mockCompanySearchCriteria = this.createMockCompanySearchCriteriaResult();
    
    /** The mock permit entity. */
    PermitEntity mockPermitEntity = this.createMockPermitWithAddressAndPOC();
    
    /** The mock permit. */
    Permit mockPermit = this.createMockPermitWithPOC();
    
    /**
     * Test permit history criteria search.
     *
     * @throws Exception the exception
     */
    @Test
    public void testPermitHistoryCriteriaSearch() throws Exception {
            when(
                    this.simpleSQLManager.getAllPermitHistory(
                      (PermitHistoryCriteria)notNull()
                    )
                  ).thenReturn(mockPermitHistoryCriteria);
            PermitHistoryCriteria criteria = permitBizComp.getPermitHistories(mockPermitHistoryCriteria);
            Assert.assertEquals(criteria.getPermitNameFilter(), mockPermitHistoryCriteria.getPermitNameFilter());
            Assert.assertEquals(criteria.getResults().size(),1);        
    }
    
    /**
     * Test company history criteria search.
     *
     * @throws Exception the exception
     */
    @Test
    public void testCompanyHistoryCriteriaSearch() throws Exception {
            when(
                    this.simpleSQLManager.getAllCompanies(
                      (CompanySearchCriteria)notNull()
                    )
                  ).thenReturn(mockCompanySearchCriteria);
            CompanySearchCriteria criteria = permitBizComp.getCompanies(mockCompanySearchCriteria);
            Assert.assertEquals(criteria.getCompanyNameFilter(), mockCompanySearchCriteria.getCompanyNameFilter());
            Assert.assertEquals(criteria.getResults().size(),1);        
    }    

    @Test
    public void testPermitCompanyInfo() throws Exception {
    }
    
    /**
     * Test save update permit contacts.
     *
     * @throws Exception the exception
     */
    @Test
    public void testSaveUpdatePermitContacts() throws Exception {
    }
        
    /**
     * *****************************************
     * Mock Objects
     * *****************************************.
     *
     * @return the permit history criteria
     */
    private PermitHistoryCriteria createMockPermitHistoryCriteriaResult() {
        /* create results */
        List<CompanyPermitHistory> results = new ArrayList<CompanyPermitHistory>();
        CompanyPermitHistory result = new CompanyPermitHistory();
        result.setCompanyName("TEST COMPANY");
        result.setPermitNum("TEST PERMIT");
        result.setDisplayMonthYear("October 2015");
        results.add(result);

        /* create criteria with results */
        PermitHistoryCriteria criteria = new PermitHistoryCriteria();
        criteria.setPermitNameFilter("TEST");
        criteria.setCompanyIdFilter(1L);
        Set<String> monthFilters = new HashSet<String>();
        monthFilters.add("JAN");
		criteria.setMonthFilters(monthFilters);
        Set<String> reportStatusFilters = new HashSet<String>();
		criteria.setReportStatusFilters(reportStatusFilters);
		reportStatusFilters.add("ERRO");
        Set<String> yearFilters = new HashSet<String>();
        yearFilters.add("2016");
		criteria.setYearFilters(yearFilters);
        criteria.setResults(results);
        return criteria;
    }
    
    /**
     * Creates the mock company search criteria result.
     *
     * @return the company search criteria
     */
    private CompanySearchCriteria createMockCompanySearchCriteriaResult() {
        /* create results */
        List<CompanyHistory> results = new ArrayList<CompanyHistory>();
        CompanyHistory result = new CompanyHistory();
        result.setCompanyName("TEST COMPANY");
        result.setCompanyId(1L);
        result.setCompanyStatus("ACTV");
        result.setPermits("TT-AA-1234, TT-BB-2345");
        results.add(result);

        /* create criteria with results */
        CompanySearchCriteria criteria = new CompanySearchCriteria();
        criteria.setCompanyNameFilter("TEST");
        criteria.setCompanyIdFilter(1L);
        Set<String> companyStatusFilters = new HashSet<String>();
        companyStatusFilters.add("ACTV");
		criteria.setCompanyStatusFilters(companyStatusFilters);
        criteria.setResults(results);
        return criteria;
    }    
    
    /**
     * Creates the mock permit with POC.
     *
     * @return the permit
     */
    private Permit createMockPermitWithPOC() {
        Permit permit = new Permit();
        permit.setPermitId(1);
        permit.setPermitNum("TEST PERMIT");
        permit.setContacts(createMockContactList());
        return permit;        
    }
    
    /**
     * Creates the mock permit with address and POC.
     *
     * @return the permit entity
     */
    private PermitEntity createMockPermitWithAddressAndPOC() {
        PermitEntity permit = new PermitEntity();
        CompanyEntity company = createMockCompanyWithAddresses();
        permit.setPermitNum("TEST PERMIT");
        /* permit.setContacts(createMockContactEntityList()); */
        permit.setCompanyEntity(company);
        return permit;
    }
    
    /**
     * Creates the mock company with addresses.
     *
     * @return the company entity
     */
    private CompanyEntity createMockCompanyWithAddresses() {
        Set<AddressEntity> addresses = new HashSet<AddressEntity>();
        AddressEntity primaryAddress = new AddressEntity();
        AddressEntity alternateAddress = new AddressEntity();
        primaryAddress.setStreetAddress("111 TEST PRIMARY CIRCLE");
        primaryAddress.setAddressTypeCd("PRIM");
        alternateAddress.setStreetAddress("222 TEST ALTERNATE WAY");
        alternateAddress.setAddressTypeCd("ALTN");
        addresses.add(primaryAddress);
        addresses.add(alternateAddress);
        
        /* create company */
        CompanyEntity company = new CompanyEntity();
        company.setEIN("061234567");
        company.setLegalName("TEST COMPANY");
        company.setAddresses(addresses);
        return company;
    }
    
    /**
     * Creates the mock contact entity list.
     *
     * @return the list
     */
    @SuppressWarnings("unused")
	private List<ContactEntity> createMockContactEntityList() {
        List<ContactEntity> contacts = new ArrayList<ContactEntity>();
        ContactEntity contact1 = new ContactEntity();
        ContactEntity contact2 = new ContactEntity();
        contact1.setContactId(new Long(1));
        contact1.setFirstNm("TEST1");
        contact1.setLastNm("TESTER");
        contact2.setContactId(new Long(2));
        contact2.setFirstNm("TEST2");
        contact2.setLastNm("TESTER");
        contacts.add(contact1);
        contacts.add(contact2);
        return contacts;
    }

    /**
     * Creates the mock contact list.
     *
     * @return the list
     */
    private List<Contact> createMockContactList() {
        List<Contact> contacts = new ArrayList<Contact>();
        Contact contact1 = new Contact();
        Contact contact2 = new Contact();
        contact1.setContactId(new Long(1));
        contact1.setFirstNm("TEST1");
        contact1.setLastNm("TESTER");
        contact2.setContactId(new Long(2));
        contact2.setFirstNm("TEST2");
        contact2.setLastNm("TESTER");
        contacts.add(contact1);
        contacts.add(contact2);
        return contacts;        
    }
    
}
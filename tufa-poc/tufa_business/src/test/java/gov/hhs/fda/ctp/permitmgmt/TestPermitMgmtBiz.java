package gov.hhs.fda.ctp.permitmgmt;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import gov.hhs.fda.ctp.biz.permitmgmt.PermitMgmtBizComp;
import gov.hhs.fda.ctp.biz.permitmgmt.PermitMgmtBizCompImpl;
import gov.hhs.fda.ctp.common.beans.DocUpload;
import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.beans.PermitAudit;
import gov.hhs.fda.ctp.common.beans.PermitUpdateUpload;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.security.UserContext;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.persistence.PermitEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.model.PermitAuditEntity;
import gov.hhs.fda.ctp.persistence.model.PermitIgnoreEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateEntity;

@RunWith(MockitoJUnitRunner.class)
public class TestPermitMgmtBiz {

	@Mock
	PermitEntityManager permitEntityManagerMock;

	@Mock
	SimpleSQLManager simpleSQLManager;
	
	@Mock
	MapperWrapper mapperWrapper;
	
	@Mock
	SecurityContextUtil securityContextUtil;

	@InjectMocks
	PermitMgmtBizComp permitMgmtBizComp = new PermitMgmtBizCompImpl();
	
	Mapper mapper;
	UserContext userContext;

	@Before
	public void setUp() {
		mapper = Mockito.mock(Mapper.class);
		Mockito.when(mapperWrapper.getMapper()).thenReturn(mapper);
		
		userContext = Mockito.mock(UserContext.class);
		Mockito.when(securityContextUtil.getPrincipal()).thenReturn(userContext);
		Mockito.when(userContext.getUsername()).thenReturn("JUNIT_TESTER");
	}
	
	@Test
	public void testInsertPermitUpdates() throws Exception {
		
		// Setup arguments
		// Output
		IngestionOutput output = new IngestionOutput();
		List<DocUpload> ingestion = new ArrayList<DocUpload>();
		PermitUpdateUpload upload = new PermitUpdateUpload();
		ingestion.add(upload);
		output.setOutput(ingestion);
		// File
		byte[] content = new byte[] {0x0b};
		MultipartFile file = new MockMultipartFile("TESTING","ORIGINAL.txt","text/plain",content);
		// AliasfileNm
		String alias = "ALIAS";
		
		
		// Setup argument capture
		ArgumentCaptor<PermitUpdateDocumentEntity> argument = ArgumentCaptor.forClass(PermitUpdateDocumentEntity.class);
		
		// Execute
		permitMgmtBizComp.ingestPermitUpdates(output, file, alias);
		
		// Validate the captured data
		Mockito.verify(this.permitEntityManagerMock).stagePermitUpdatesList(argument.capture());
		Assert.assertArrayEquals(content, argument.getValue().getDocument());
		Assert.assertEquals("ORIGINAL.txt", argument.getValue().getFileNm());
		Assert.assertEquals(alias, argument.getValue().getAliasFileNm());
		Assert.assertFalse(argument.getValue().getPermits().isEmpty());
		PermitUpdateEntity permitUpdate = argument.getValue().getPermits().get(0);
		// Verify that data from DocUplaod to PermitUpdateUpload
		
		// Validate method called once
		Mockito.verify(permitEntityManagerMock, Mockito.times(1)).stagePermitUpdatesList((PermitUpdateDocumentEntity) Mockito.any());
	}
	
	@Test
	public void testInsertPermitAudit() throws Exception {
		// Create PermitAudit
		PermitAudit permitAudit = new PermitAudit();
		PermitAuditEntity permitAuditEntity = Mockito.mock(PermitAuditEntity.class);
		// Mock PermitEntityManager
		Mockito.when(permitEntityManagerMock.insertPermitMgmtAudit((PermitAuditEntity) Mockito.any())).thenReturn(permitAuditEntity);
		// Execute
		PermitAudit reuslt = permitMgmtBizComp.insertPermitAudit(permitAudit);
		// Verify returned data matches entered data
		Mockito.verify(permitEntityManagerMock, Mockito.times(1)).insertPermitMgmtAudit((PermitAuditEntity) Mockito.any());
	}
	
	@Test
	public void testUpdatePermit() throws Exception {

		// Setup data
		PermitUpdateEntity mock = this.getMockPermitUpdateEntity();
		mock.setAddressStatus("");
		PermitUpdateUpload uploadMock = Mockito.mock(PermitUpdateUpload.class);
		List<PermitUpdateUpload> uploadArrayMock = new ArrayList<PermitUpdateUpload>();
		uploadArrayMock.add(uploadMock);

		// Setup returns
		Mockito.when(this.simpleSQLManager.saveUpdatedPermits(mock)).thenReturn(uploadArrayMock);

		// Execute
		List<PermitUpdateUpload> result = permitMgmtBizComp.saveUpdatedPermits(mock);

		// Verify
		Mockito.verify(permitEntityManagerMock, Mockito.never()).insertIgnoreStatus((PermitIgnoreEntity) Mockito.any());
		Assert.assertEquals(uploadArrayMock, result);
	}

	@Test
	public void testUpdatePermitIgnore() throws Exception {
		// Setup data
		PermitUpdateEntity mock = this.getMockPermitUpdateEntity();
		mock.setAddressStatus("IGNR");
		PermitUpdateUpload uploadMock = Mockito.mock(PermitUpdateUpload.class);
		List<PermitUpdateUpload> uploadArrayMock = new ArrayList<PermitUpdateUpload>();
		uploadArrayMock.add(uploadMock);
		
		// Set up argument capture 
		ArgumentCaptor<PermitIgnoreEntity> argument = ArgumentCaptor.forClass(PermitIgnoreEntity.class);

		// Setup returns
		Mockito.when(this.simpleSQLManager.saveUpdatedPermits(mock)).thenReturn(uploadArrayMock);

		// Execute
		List<PermitUpdateUpload> result = permitMgmtBizComp.saveUpdatedPermits(mock);

		// Verify data mapping from PermitUpdateEntity to PermitIgnoreEntity
		Mockito.verify(this.permitEntityManagerMock).insertIgnoreStatus(argument.capture());
		Assert.assertEquals(new Long(1), argument.getValue().getPermitUpdateId());
		Assert.assertEquals("PERMIT_NUM", argument.getValue().getPermitNum());
		Assert.assertEquals("PERMIT_TYPE", argument.getValue().getPermitType());
		Assert.assertEquals(new Long(1), argument.getValue().getDocStageId());
		Assert.assertEquals("123456789", argument.getValue().getEinNum());
		Assert.assertEquals("MAILING_CITY", argument.getValue().getMailingCity());
		Assert.assertEquals("MAILING_STREET", argument.getValue().getMailingStreet());
		Assert.assertEquals("MAILING_STATE", argument.getValue().getMailingState());
		Assert.assertEquals("123", argument.getValue().getMailingPostalCd());
		Assert.assertEquals("BUSINESS_NM", argument.getValue().getBusinessName());
		
		//Verify that the ignoreIgnoreStatus() is called
		Mockito.verify(permitEntityManagerMock, Mockito.times(1)).insertIgnoreStatus((PermitIgnoreEntity) Mockito.any());
		
		//Verify the array is what was expected
		Assert.assertEquals(uploadArrayMock, result);
	}
	
	private PermitUpdateEntity getMockPermitUpdateEntity() {
		PermitUpdateEntity result = new PermitUpdateEntity();
		
		//Set data
		result.setPermitUpdateId(1L);
		result.setPermitNum("PERMIT_NUM");
		result.setPermitType("PERMIT_TYPE");
		result.setDocStageId(1L);
		result.setEinNum("123456789");
		result.setMailingCity("MAILING_CITY");
		result.setMailingStreet("MAILING_STREET");
		result.setMailingState("MAILING_STATE");
		result.setMailingPostalCd("123");
		result.setBusinessName("BUSINESS_NM");
		
		return result;
	}

}

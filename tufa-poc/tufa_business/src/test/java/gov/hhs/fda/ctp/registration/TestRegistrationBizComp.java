package gov.hhs.fda.ctp.registration;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import gov.hhs.fda.ctp.biz.config.BizTestConfig;
import gov.hhs.fda.ctp.biz.registration.EmploymentDTO;
import gov.hhs.fda.ctp.biz.registration.RegistrationBizComp;
import gov.hhs.fda.ctp.biz.registration.RegistrationDTO;
import gov.hhs.fda.ctp.biz.registration.UserDTO;
import gov.hhs.fda.ctp.persistence.BaseEntityManager;
import static org.hamcrest.CoreMatchers.is;

/**
 * The Class TestRegistrationBizComp.
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextHierarchy({
	@ContextConfiguration(classes = BizTestConfig.class )
})
@TestPropertySource("/testregistration.properties")
public class TestRegistrationBizComp {
	
	/** The env. */
	@Autowired
	private Environment env;
	
	/** The base entity manager. */
	@Autowired
	BaseEntityManager baseEntityManager;
	
	/** The registration biz comp. */
	@Autowired
	RegistrationBizComp registrationBizComp;
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	/**
	 * Register user.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void registerUser() throws Exception {
		
		String email = env.getProperty("email");
		String password= env.getProperty("password");
		
		String companyName=env.getProperty("companyname");
		String companyAddress=env.getProperty("companyaddress");
		
		UserDTO userDTO = new UserDTO(email,password);
		EmploymentDTO employmentDTO = new EmploymentDTO(companyName, companyAddress);
		RegistrationDTO registrationDTO = new RegistrationDTO(userDTO,employmentDTO);
		registrationBizComp.registerUser(registrationDTO);	
	}
	
	//Another way of verifying  exception test
	/**
	 * Register dummy user.
	 *
	 * @throws UnsupportedOperationException the unsupported operation exception
	 */
	//@Test(expected=UnsupportedOperationException.class)
	@Test
	public void registerDummyUser() throws UnsupportedOperationException{
		thrown.expect(UnsupportedOperationException.class);
		thrown.expectMessage(is("Method not supported in RegistrationBizComp"));
		registrationBizComp.registerDummyUser();
	}

}

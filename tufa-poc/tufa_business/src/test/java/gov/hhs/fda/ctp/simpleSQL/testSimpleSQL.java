package gov.hhs.fda.ctp.simpleSQL;

import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import gov.hhs.fda.ctp.biz.config.BizTestConfig;
import gov.hhs.fda.ctp.common.beans.AssessmentReportStatus;
import gov.hhs.fda.ctp.common.beans.CBPExportDetail;
import gov.hhs.fda.ctp.common.beans.CBPImporter;
import gov.hhs.fda.ctp.common.beans.CBPNewCompanies;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentRptDetail;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.CompanyPermitHistory;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.common.beans.PermitUpdateUpload;
import gov.hhs.fda.ctp.common.beans.PermitUpdatesDocument;
import gov.hhs.fda.ctp.common.beans.QtReconRptDetail;
import gov.hhs.fda.ctp.common.beans.ReconExportDetail;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionHTSCodeSummary;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonSummary;
import gov.hhs.fda.ctp.common.beans.UnknownCompanyBucket;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateEntity;
import gov.hhs.fda.ctp.persistence.model.TTBPermitBucket;
import gov.hhs.fda.ctp.persistence.model.TrueUpEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpSubDocumentEntity;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextHierarchy({
	@ContextConfiguration(classes = BizTestConfig.class )
})
public class testSimpleSQL {
	@Autowired
	SimpleSQLManager manager;
	
	@Test
	public void testGetCBPExportData() {
		List<CBPExportDetail> entities = new ArrayList<>();
		CBPExportDetail mockTrue = new CBPExportDetail();
		mockTrue.setFiscalQtr("2017");
		entities.add(mockTrue);
		when(manager.getCBPExportData(2017, 2, "Pipe", "1")).thenReturn(entities);
		List<CBPExportDetail> results = manager.getCBPExportData(2017, 2, "Pipe", "1");
		Assert.assertEquals(results,entities);		
	}

	@Test
	public void testGetPermitHistory() {
		List<CompanyPermitHistory> entities = new ArrayList<>();
		CompanyPermitHistory mockTrue = new CompanyPermitHistory();
		mockTrue.setFiscalYear("2017");
		entities.add(mockTrue);
		when(manager.getPermitHistory(1)).thenReturn(entities);
		List<CompanyPermitHistory> results = manager.getPermitHistory(1);
		Assert.assertEquals(results,entities);		
	}
	
//	@Test
//	public void testGetPaginatedPermitHistory() {
//		List<CompanyPermitHistory> entities = new ArrayList<>();
//		CompanyPermitHistory mockTrue = new CompanyPermitHistory();
//		mockTrue.setFiscalYear("2017");
//		entities.add(mockTrue);
//		when(manager.getPaginatedPermitHistory(1,1,1)).thenReturn(entities);
//		List<CompanyPermitHistory> results = manager.getPaginatedPermitHistory(1,1,1);
//		Assert.assertEquals(results,entities);
//	}
	
//	@Test
//	public void testGetPermitHistoryCount() {
//		when(manager.getPermitHistoryCount(1)).thenReturn(1);
//		int results = manager.getPermitHistoryCount(1);
//		assert(results == 1);
//	}
	
	@Test
	public void testGetCompanyPermitHistory() {
		List<CompanyPermitHistory> entities = new ArrayList<>();
		CompanyPermitHistory mockTrue = new CompanyPermitHistory();
		mockTrue.setFiscalYear("2017");
		entities.add(mockTrue);
		when(manager.getCompanyPermitHistory(1)).thenReturn(entities);
		List<CompanyPermitHistory> results = manager.getCompanyPermitHistory(1);
		Assert.assertEquals(results,entities);
	}
	
	@Test
	public void testGetAllPermitHistory() {
		PermitHistoryCriteria mockTrue = new PermitHistoryCriteria();
		mockTrue.setFiscalYears("2017");
		when(manager.getAllPermitHistory((PermitHistoryCriteria)notNull())).thenReturn(mockTrue);
		PermitHistoryCriteria results = manager.getAllPermitHistory(mockTrue);
		Assert.assertEquals(results, mockTrue);
	}
	
	@Test
	public void testGetAllCompanies() {
		CompanySearchCriteria mockTrue = new CompanySearchCriteria();
		mockTrue.setCompanyIdFilter(1L);
		when(manager.getAllCompanies(mockTrue)).thenReturn(mockTrue);
		CompanySearchCriteria results = manager.getAllCompanies(mockTrue);
		Assert.assertEquals(results, mockTrue);
	}
	
	@Test
	public void testGetAvailablePermitReportFiscalYears() {
		Set<String> entities = new HashSet<>();
		entities.add("2017");
		when(manager.getAvailablePermitReportFiscalYears()).thenReturn(entities);
		Set<String> results = manager.getAvailablePermitReportFiscalYears();
		Assert.assertEquals(results,entities);
	}
	
	@Test
	public void testGetAvailablePermitReportCalendarYears() {
		Set<String> entities = new HashSet<>();
		entities.add("2017");
		when(manager.getAvailablePermitReportCalendarYears()).thenReturn(entities);
		Set<String> results = manager.getAvailablePermitReportCalendarYears();
		Assert.assertEquals(results,entities);
	}
	
	@Test
	public void testGetReportStatusData() {
		Map<Long, AssessmentReportStatus> entities = new HashMap<>();
		entities.put((long) 2017, new AssessmentReportStatus());
		when(manager.getReportStatusData(2017, 1)).thenReturn(entities);
		Map<Long, AssessmentReportStatus> results = manager.getReportStatusData(2017,1);
		Assert.assertEquals(results,entities);
	}
	
    @Test
    public void testFindByIds() {
    	List<AttachmentsEntity> entities = new ArrayList<>();
    	AttachmentsEntity mockTrue = new AttachmentsEntity();
		mockTrue.setPermitId(1);
		mockTrue.setPeriodId(1);
		entities.add(mockTrue);
		when(manager.findByIds(1,1)).thenReturn(entities);
		List<AttachmentsEntity> results = manager.findByIds(1,1);
		Assert.assertEquals(results,entities);
    }
    
    @Test
    public void testFindSupportingDocumentsByAssessmentId() {
    	List<SupportingDocument> entities = new ArrayList<>();
    	SupportingDocument mockTrue = new SupportingDocument();
		mockTrue.setFilename("TEST.xls");
		entities.add(mockTrue);
		when(manager.findSupportingDocumentsByAssessmentId(1)).thenReturn(entities);
		List<SupportingDocument> results = manager.findSupportingDocumentsByAssessmentId(1);
		Assert.assertEquals(results,entities);
    }
    
    @Test
    public void testFindDocsByFiscalYr() {
    	List<TrueUpSubDocumentEntity> entities = new ArrayList<>();
    	TrueUpSubDocumentEntity mockTrue = new TrueUpSubDocumentEntity();
		mockTrue.setFilename("TEST.xls");
		entities.add(mockTrue);
		when(manager.findDocsByFiscalYr(2017)).thenReturn(entities);
		List<TrueUpSubDocumentEntity> results = manager.findDocsByFiscalYr(2017);
		Assert.assertEquals(results,entities);
    }

    @Test
    public void testGetReconReportData() {
    	List<QtReconRptDetail> entities = new ArrayList<>();
    	QtReconRptDetail mockTrue = new QtReconRptDetail();
		mockTrue.setFiscalYear("2017");
		entities.add(mockTrue);
		when(manager.getReconReportData(2017,1)).thenReturn(entities);
		List<QtReconRptDetail> results = manager.getReconReportData(2017,1);
		Assert.assertEquals(results,entities);
    }
    
    @Test
    public void testGetReconReportDataForFiscalYear() {
    	List<QtReconRptDetail> entities = new ArrayList<>();
    	QtReconRptDetail mockTrue = new QtReconRptDetail();
		mockTrue.setFiscalYear("2017");
		entities.add(mockTrue);
		when(manager.getReconReportDataForFiscalYear(2017)).thenReturn(entities);
		List<QtReconRptDetail> results = manager.getReconReportDataForFiscalYear(2017);
		Assert.assertEquals(results,entities);
    }
    
    @Test
    public void testGetReconExportDataForFiscalYear() {
    	List<ReconExportDetail> entities = new ArrayList<>();
    	ReconExportDetail mockTrue = new ReconExportDetail();
		mockTrue.setYear(2017);
		entities.add(mockTrue);
		when(manager.getReconExportDataForFiscalYear(2017)).thenReturn(entities);
		List<ReconExportDetail> results = manager.getReconExportDataForFiscalYear(2017);
		Assert.assertEquals(results,entities);
    }
    
    public void testGetReconExportData() {
    	List<ReconExportDetail> entities = new ArrayList<>();
    	ReconExportDetail mockTrue = new ReconExportDetail();
		mockTrue.setYear(2017);
		entities.add(mockTrue);
		when(manager.getReconExportData((Long)notNull(), (Long)notNull())).thenReturn(entities);
		List<ReconExportDetail> results = manager.getReconExportData(2017, 2);
		Assert.assertEquals(results,entities);
    }
    
    public void testGetCigarReconExportData() {
    	List<ReconExportDetail> entities = new ArrayList<>();
    	ReconExportDetail mockTrue = new ReconExportDetail();
		mockTrue.setYear(2017);
		entities.add(mockTrue);
		when(manager.getCigarReconExportData((Long)notNull())).thenReturn(entities);
		List<ReconExportDetail> results = manager.getCigarReconExportData(2017);
		Assert.assertEquals(results,entities);
    }

    public void testGetCompanyComparisonBucket() {
    	List<CBPImporter> entities = new ArrayList<>();
    	CBPImporter mockTrue = new CBPImporter();
//		mockTrue.setFiscalYear(2017L);
		entities.add(mockTrue);
		when(manager.getCompanyComparisonBucket((Long)notNull())).thenReturn(entities);
		List<CBPImporter> results = manager.getCompanyComparisonBucket(2017L);
		Assert.assertEquals(results,entities);
    }
    
    public void testGetUnknownCompaniesBucket() {
    	List<UnknownCompanyBucket> entities = new ArrayList<>();
    	UnknownCompanyBucket mockTrue = new UnknownCompanyBucket();
		mockTrue.setFiscalYear(2017L);
		entities.add(mockTrue);
		when(manager.getUnknownCompaniesBucket((Long)notNull())).thenReturn(entities);
		List<UnknownCompanyBucket> results = manager.getUnknownCompaniesBucket(2017L);
		Assert.assertEquals(results,entities);
    }

    public void testGetCigarAssessmentReportStatusData() {
    	List<CigarAssessmentRptDetail> entities = new ArrayList<>();
    	CigarAssessmentRptDetail mockTrue = new CigarAssessmentRptDetail();
		mockTrue.setFiscalYear("2017");
		entities.add(mockTrue);
		when(manager.getCigarAssessmentReportStatusData((Long)notNull())).thenReturn(entities);
		List<CigarAssessmentRptDetail> results = manager.getCigarAssessmentReportStatusData(2017L);
		Assert.assertEquals(results,entities);
    }
    
    public void testGetCigarComparisonSummary() {
    	List<TrueUpComparisonSummary> entities = new ArrayList<>();
    	TrueUpComparisonSummary mockTrue = new TrueUpComparisonSummary();
		mockTrue.setNumColumns(10);
		entities.add(mockTrue);
		when(manager.getCigarComparisonSummary((Long)notNull(),(Long)notNull(),(String)notNull())).thenReturn(entities);
		List<TrueUpComparisonSummary> results = manager.getCigarComparisonSummary(1,2017L,"MANU");
		Assert.assertEquals(results,entities);
    }

    public void testGetNonCigarImporterComparisonSummary() {
//    	List<TrueUpComparisonSummary> entities = new ArrayList<>();
//    	TrueUpComparisonSummary mockTrue = new TrueUpComparisonSummary();
//		mockTrue.setNumColumns(10);
//		entities.add(mockTrue);
//		when(manager.getNonCigarImporterComparisonSummary((Long)notNull(),(Long)notNull(),(Long)notNull(),(String)notNull(),(String)notNull())).thenReturn(entities);
//		List<TrueUpComparisonSummary> results = manager.getNonCigarImporterComparisonSummary(1,2017L,2,"IMPT","Pipe");
//		Assert.assertEquals(results,entities);
    }
    
    public void testGetNonCigarManufacturerComparisonSummary() {
    	List<TrueUpComparisonSummary> entities = new ArrayList<>();
    	TrueUpComparisonSummary mockTrue = new TrueUpComparisonSummary();
		mockTrue.setNumColumns(10);
		entities.add(mockTrue);
		when(manager.getNonCigarManufacturerComparisonSummary((Long)notNull(),(Long)notNull(),(Long)notNull(),(String)notNull(),(String)notNull())).thenReturn(entities);
		List<TrueUpComparisonSummary> results = manager.getNonCigarManufacturerComparisonSummary(1,2017L,2,"MANU","Pipe");
		Assert.assertEquals(results,entities);
    }

    public void testGetImporterComparisonDetails() {
    	List<TrueUpComparisonImporterDetail> entities = new ArrayList<>();
    	TrueUpComparisonImporterDetail mockTrue = new TrueUpComparisonImporterDetail();
		mockTrue.setPeriodTotal("na");
		entities.add(mockTrue);
		when(manager.getImporterComparisonDetails((String)notNull(),(Long)notNull(),(Long)notNull(),(String)notNull(),(String)notNull())).thenReturn(entities);
		List<TrueUpComparisonImporterDetail> results = manager.getImporterComparisonDetails("123456789",2017L,2,"IMPT","Pipe");
		Assert.assertEquals(results,entities);
    }
    
    public void testGetImporterComparisonIngestionDetails() {
    	List<TrueUpComparisonImporterIngestionDetail> entities = new ArrayList<>();
    	TrueUpComparisonImporterIngestionDetail mockTrue = new TrueUpComparisonImporterIngestionDetail();
		mockTrue.setPeriodTotal("na");
		entities.add(mockTrue);
		when(manager.getImporterComparisonIngestionDetails((String)notNull(),(Long)notNull(),(Long)notNull(),(String)notNull(),(String)notNull(),(String)notNull())).thenReturn(entities);
		List<TrueUpComparisonImporterIngestionDetail> results = manager.getImporterComparisonIngestionDetails("123456789",2017L,2,"IMPT","Pipe","singleFile");
		Assert.assertEquals(results,entities);
    }
    
    public void testGetImporterComparisonIngestionHTSCodeSummary() {
    	List<TrueUpComparisonImporterIngestionHTSCodeSummary> entities = new ArrayList<>();
    	TrueUpComparisonImporterIngestionHTSCodeSummary mockTrue = new TrueUpComparisonImporterIngestionHTSCodeSummary();
		mockTrue.setTaxAmount(300d);
		entities.add(mockTrue);
		when(manager.getImporterComparisonIngestionHTSCodeSummary((String)notNull(),(Long)notNull(),(Long)notNull(),(String)notNull(),(String)notNull())).thenReturn(entities);
		List<TrueUpComparisonImporterIngestionHTSCodeSummary> results = manager.getImporterComparisonIngestionHTSCodeSummary("123456789",2017L,2,"IMPT","Pipe");
		Assert.assertEquals(results,entities);
    }

    public void testGetCigarManufacturerComparisonDetails() {
    	List<TrueUpComparisonManufacturerDetail> entities = new ArrayList<>();
    	TrueUpComparisonManufacturerDetail mockTrue = new TrueUpComparisonManufacturerDetail();
		mockTrue.setQuarter(2);
		entities.add(mockTrue);
		when(manager.getCigarManufacturerComparisonDetails((Long)notNull(),(Long)notNull(),(Long)notNull(),(String)notNull(),(String)notNull())).thenReturn(entities);
		List<TrueUpComparisonManufacturerDetail> results = manager.getCigarManufacturerComparisonDetails(1,2017L,2,"MANU","Pipe");
		Assert.assertEquals(results,entities);
    }
    
    public void testGetNonCigarManufacturerComparisonDetails() {
    	List<TrueUpComparisonManufacturerDetail> entities = new ArrayList<>();
    	TrueUpComparisonManufacturerDetail mockTrue = new TrueUpComparisonManufacturerDetail();
		mockTrue.setQuarter(2);
		entities.add(mockTrue);
		when(manager.getNonCigarManufacturerComparisonDetails((Long)notNull(),(Long)notNull(),(Long)notNull(),(String)notNull(),(String)notNull())).thenReturn(entities);
		List<TrueUpComparisonManufacturerDetail> results = manager.getNonCigarManufacturerComparisonDetails(1,2017L,2,"MANU","Pipe");
		Assert.assertEquals(results,entities);
    }
    
    public void testGetComparisonResults() {
    	List<TrueUpComparisonResults> entities = new ArrayList<>();
    	TrueUpComparisonResults mockTrue = new TrueUpComparisonResults();
		mockTrue.setQuarter("2");
		entities.add(mockTrue);
		TrueUpCompareSearchCriteria csc = new TrueUpCompareSearchCriteria();
		csc.setQuarterFilter("2");
		when(manager.getComparisonResults((Long)notNull(),(TrueUpCompareSearchCriteria)notNull())).thenReturn(entities);
		List<TrueUpComparisonResults> results = manager.getComparisonResults(2017L,csc);
		Assert.assertEquals(results,entities);
    }

    public void testGetTTBPermits() {
    	List<TTBPermitBucket> entities = new ArrayList<>();
    	TTBPermitBucket mockTrue = new TTBPermitBucket();
		mockTrue.setProvidedAnswer(true);
		entities.add(mockTrue);
		when(manager.getTTBPermits((Long)notNull())).thenReturn(entities);
		List<TTBPermitBucket> results = manager.getTTBPermits(2017L);
		Assert.assertEquals(results,entities);
    }

    public void testGetCompaniesForIncludeBucket() {
    	List<CompanyIngestion> entities = new ArrayList<>();
    	CompanyIngestion mockTrue = new CompanyIngestion();
		mockTrue.setProvidedAnswer(true);
		entities.add(mockTrue);
		when(manager.getCompaniesForIncludeBucket((Long)notNull())).thenReturn(entities);
		List<CompanyIngestion> results = manager.getCompaniesForIncludeBucket(2017L);
		Assert.assertEquals(results,entities);
    }
	
    public void testFetchFDA352MAN() {
    	Map<String, String> entities = new HashMap<>();
		entities.put("","");
		when(manager.fetchFDA352MAN((Long)notNull(),(long)notNull(),(String)notNull())).thenReturn(entities);
		Map<String, String> results = manager.fetchFDA352MAN(2017L,1,"Pipe");
		Assert.assertEquals(results,entities);
    }
    
    public void testFetchFDA352IMP() {
    	Map<String, String> entities = new HashMap<>();
		entities.put("","");
		when(manager.fetchFDA352IMP((Long)notNull(),(String)notNull(),(String)notNull())).thenReturn(entities);
		Map<String, String> results = manager.fetchFDA352IMP(2017L,"123456789","Pipe");
		Assert.assertEquals(results,entities);
    }
	
    public void testGetCBPNewCompanies() throws SQLException, IOException {
    	List<CBPNewCompanies> entities = new ArrayList<>();
    	CBPNewCompanies mockTrue = new CBPNewCompanies();
		mockTrue.settaxamount(300d);
		entities.add(mockTrue);
		when(manager.getCBPNewCompanies((Long)notNull())).thenReturn(entities);
		List<CBPNewCompanies> results = manager.getCBPNewCompanies(2017L);
		Assert.assertEquals(results,entities);
    }
    
    public void testGetPermitUpdateFiles() {
    	List<PermitUpdatesDocument> entities = new ArrayList<>();
    	PermitUpdatesDocument mockTrue = new PermitUpdatesDocument();
		mockTrue.setFileNm("test.xls");
		entities.add(mockTrue);
		when(manager.getPermitUpdateFiles()).thenReturn(entities);
		List<PermitUpdatesDocument> results = manager.getPermitUpdateFiles();
		Assert.assertEquals(results,entities);
    }
	
//    @Ignore
//    public void testGetUpdatedPermits() {
//    	List<PermitUpdateUpload> entities = new ArrayList<>();
//    	PermitUpdateUpload mockTrue = new PermitUpdateUpload();
//		mockTrue.setBusinessName("Marlboro");
//		entities.add(mockTrue);
//		when(manager.getUpdatedPermits((Long)notNull())).thenReturn(entities);
//		List<PermitUpdateUpload> results = manager.getUpdatedPermits(2L);
//		Assert.assertEquals(results,entities);
//    }
//    
//   @Ignore
//    public void testSaveUpdatedPermits() {
//    	List<PermitUpdateUpload> entities = new ArrayList<>();
//    	PermitUpdateUpload mockTrue = new PermitUpdateUpload();
//		mockTrue.setBusinessName("Marlboro");
//		entities.add(mockTrue);
//		PermitUpdateUpload pue = new PermitUpdateUpload();
//		pue.setBusinessName("Marlboro");
//		when(manager.saveUpdatedPermits((PermitUpdateUpload)notNull())).thenReturn(entities);
//		List<PermitUpdateUpload> results = manager.saveUpdatedPermits(pue);
//		Assert.assertEquals(results,entities);
//    }
//    
    public void testgetPermitHistory3852() {
    	List<CompanyPermitHistory> entities = new ArrayList<>();
    	CompanyPermitHistory mockTrue = new CompanyPermitHistory();
		mockTrue.setCompanyName("Marlboro");
		entities.add(mockTrue);
		when(manager.getPermitHistory3852((Long)notNull())).thenReturn(entities);
		List<CompanyPermitHistory> results = manager.getPermitHistory3852(2L);
		Assert.assertEquals(results,entities);
    }
}

package gov.hhs.fda.ctp.trueup;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import gov.hhs.fda.ctp.biz.config.BizTestConfig;
import gov.hhs.fda.ctp.common.beans.CBPAmendment;
import gov.hhs.fda.ctp.common.beans.CBPEntry;
import gov.hhs.fda.ctp.common.beans.CBPLineEntryUpdateInfo;
import gov.hhs.fda.ctp.common.beans.TTBAmendment;
import gov.hhs.fda.ctp.common.beans.TrueUpDocument;
import gov.hhs.fda.ctp.common.beans.TrueUpFilter;
import gov.hhs.fda.ctp.persistence.TrueUpEntityManager;
import gov.hhs.fda.ctp.persistence.model.CBPAmendmentEntity;
import gov.hhs.fda.ctp.persistence.model.CBPEntryEntity;
import gov.hhs.fda.ctp.persistence.model.TTBAmendmentEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpSubDocumentEntity;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextHierarchy({
	@ContextConfiguration(classes = BizTestConfig.class )
})
public class TestTrueUpBizComp {

	/** The base entity manager. */
	@Autowired
	TrueUpEntityManager trueupEntityManager;
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	@Test
    public void createCompany() {
		TrueUpEntity mockTrue = new TrueUpEntity();
		mockTrue.setFiscalYear(2017L);
        when(
                this.trueupEntityManager.createTrueUp(
                  (TrueUpEntity)notNull()
                )
              ).thenReturn("Successfully Created");
        String result = trueupEntityManager.createTrueUp(new TrueUpEntity());
        Assert.assertEquals(result,"Successfully Created");
    }
	
	@Test
	public void testGetTrueUp() {
		List<TrueUpEntity> entities = new ArrayList<>();
		TrueUpEntity mockTrue = new TrueUpEntity();
		mockTrue.setFiscalYear(2017L);
		entities.add(mockTrue);
		when(this.trueupEntityManager.getAllTrueUps()).thenReturn(entities);
		List<TrueUpEntity> results = trueupEntityManager.getAllTrueUps();
		Assert.assertEquals(results,entities);
	}
	
	@Test
	public void testGetTrueUpByYear() throws Exception {
		TrueUpEntity mockTrue = new TrueUpEntity();
		mockTrue.setFiscalYear(2017L);
	      when(
	              this.trueupEntityManager.getTrueUpByYear(
	                anyLong()
	              )
	            ).thenReturn(mockTrue);

	      TrueUpEntity result = trueupEntityManager.getTrueUpByYear(2017L);
	      assertEquals(result, mockTrue);
	}
	

	
	@Test
	public void testGetDocByDocId() throws Exception {
		TrueUpSubDocumentEntity subdoc = new TrueUpSubDocumentEntity();
		subdoc.setFiscalYr(2017L);
		when(trueupEntityManager.getDocByDocId(anyLong())).thenReturn(subdoc);
		
		TrueUpSubDocumentEntity result = trueupEntityManager.getDocByDocId(1);
		assertEquals(result, subdoc);
	}
	
	@Test
	public void getHTSMissingAssociationData() throws Exception {
		List<CBPEntryEntity> cbpentries = new ArrayList<>();
		CBPEntryEntity entry = new CBPEntryEntity();
		entry.setFiscalYear("2017");
		cbpentries.add(entry);
		/*To be fixed Later
		 when(trueupEntityManager.getHTSMissingAssociationData(anyLong())).thenReturn(cbpentries);
		List<CBPEntryEntity> results = this.trueupEntityManager.getHTSMissingAssociationData(2017L);
		assertEquals(results, cbpentries);*/
	}
	
	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#createTrueUp(gov.hhs.fda.ctp.persistence.model.TrueUpEntity)}.
	 */
	@Test
	public void testCreateTrueUp() throws Exception {
		TrueUpEntity mocktrueUpEntity = this.createMockTrueUpEntity();
		String retcreateTrueUp = "1";

        when(
                this.trueupEntityManager.createTrueUp(
                		(TrueUpEntity)notNull())
              ).thenReturn(retcreateTrueUp);

        String returnString = trueupEntityManager.createTrueUp(mocktrueUpEntity);
        assertEquals(returnString, retcreateTrueUp);
	}



	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getAssessments(gov.hhs.fda.ctp.common.beans.TrueUpFilter)}.
	 */
	@Test
	public void testGetAssessments() throws Exception {
		TrueUpFilter mockTrueUpFilter = this.createMockTrueUpFilter();
		List<TrueUpEntity> mockTrueUpList = this.createTrueUpList();

        when(
                this.trueupEntityManager.getTrueUps(
                		(TrueUpFilter)notNull())
              ).thenReturn(mockTrueUpList);

        List<TrueUpEntity> mockTrueUp = trueupEntityManager.getTrueUps(mockTrueUpFilter);
         assertEquals(mockTrueUp, mockTrueUpList);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getTrueUpDocuments(long)}.
	 */
	@Test
	public void testGetTrueUpDocuments() throws Exception {
		List<TrueUpDocument> mockTrueUpDocuments = this.createMockTrueUpDocuments();
		List<TrueUpDocumentEntity> mockTrueUpDocumentEntities = this.createMockTrueUpDocumentEntities();
		
	      when(
	              this.trueupEntityManager.getTrueupDocuments(
	                anyLong()
	              )
	            ).thenReturn(mockTrueUpDocumentEntities);

	      List<TrueUpDocumentEntity> returnString = trueupEntityManager.getTrueupDocuments(1L);
	      assert (returnString != null);
	}
	
	@Test
	public void testUpdateCompareAcceptFDA () throws Exception{
		List<TTBAmendmentEntity> amendments = new ArrayList<>();
		TTBAmendmentEntity amendment = new TTBAmendmentEntity();
		amendment.setQtr("3"); amendments.add(amendment);
		when(this.trueupEntityManager.updateTTBCompareFDAAcceptFlag(amendments)).thenReturn(amendments);

		List<TTBAmendmentEntity> resamendments = trueupEntityManager.updateTTBCompareFDAAcceptFlag(amendments);
		assert (resamendments != null);
	}
	

	@Test
	public void testGetCBPAmendments() throws Exception {
		List<CBPAmendmentEntity> tobaccoTypes = new ArrayList<>();
		CBPAmendmentEntity amendment = new CBPAmendmentEntity();
		amendment.setQtr("3");
		tobaccoTypes.add(amendment);
		List<CBPAmendment> tobacco = new ArrayList<>();
		CBPAmendment amendments = new CBPAmendment();
		amendments.setQtr("3");
		tobacco.add(amendments);

	      when(
	              this.trueupEntityManager.getCBPAmendments(2017L, 1L, tobacco)
	            ).thenReturn(tobaccoTypes);

	      List<CBPAmendmentEntity> returnString = trueupEntityManager.getCBPAmendments(2017L, 1L, tobacco);
	      assert (returnString != null);
	}
	
	@Test
	public void testSaveCBPAmendments() throws Exception {
		List<CBPAmendmentEntity> tobaccoTypes = new ArrayList<>();
		CBPAmendmentEntity amendment = new CBPAmendmentEntity();
		amendment.setQtr("3");
		tobaccoTypes.add(amendment);
		List<CBPAmendment> tobacco = new ArrayList<>();
		CBPAmendment amendments = new CBPAmendment();
		amendments.setQtr("3");
		tobacco.add(amendments);
		when(this.trueupEntityManager.saveCBPAmendment(tobaccoTypes)).thenReturn(tobaccoTypes);

		List<CBPAmendmentEntity> returnString = trueupEntityManager.saveCBPAmendment(tobaccoTypes);
		assert (returnString != null);
	}
	
	@Test
	public void testUpdateCBPCompareAcceptFDA() throws Exception {
		List<CBPAmendmentEntity> tobaccoTypes = new ArrayList<>();
		CBPAmendmentEntity amendment = new CBPAmendmentEntity();
		amendment.setQtr("3");
		tobaccoTypes.add(amendment);
		when(this.trueupEntityManager.updateCBPCompareFDAAcceptFlag(tobaccoTypes)).thenReturn(tobaccoTypes);
		List<CBPAmendmentEntity> returnString = trueupEntityManager.updateCBPCompareFDAAcceptFlag(tobaccoTypes);
		assert (returnString != null);
	}

	

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getSupportingDocs(long)}.
	 */
	@Test
	public void testGetSupportingDocs() throws Exception {
		List<TrueUpSubDocumentEntity> mockdoc = this.createMockTrueUpSubDocumentEntiies();

        when(
                this.trueupEntityManager.getSupportDocs(
                		anyLong())
              ).thenReturn(mockdoc);

        List<TrueUpSubDocumentEntity> returnString = trueupEntityManager.getSupportDocs(1L);
         assert(returnString!=null);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getDocument(long)}.
	 */
	@Test
	public void testGetDocument() throws Exception {
		TrueUpSubDocumentEntity mockdoc = this.createMockTrueUpSubDocumentEntity();

        when(
                this.trueupEntityManager.getDocByDocId(
                		anyLong())
              ).thenReturn(mockdoc);


        TrueUpSubDocumentEntity returnString = trueupEntityManager.getDocByDocId(1L);
         assert(returnString!=null);
	}
	
	@Test
	public void testGetAnnualMarketShare() throws Exception {
		TrueUpEntity annual = new TrueUpEntity();
		annual.setFiscalYear(2017L);

        when(
                this.trueupEntityManager.getTrueUpByYear(anyLong())
              ).thenReturn(annual);

        TrueUpEntity returnString = trueupEntityManager.getTrueUpByYear(1L);
         assert(returnString!=null);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getHTSMissingAssociations(long)}.
	 */
	@Test
	public void testGetHTSMissingAssociations() throws Exception {
		List<CBPEntryEntity> mockEntries = new ArrayList<>();

       /* when(
                this.trueupEntityManager.getHTSMissingAssociationData(
                		anyLong())
              ).thenReturn(mockEntries);

        List<CBPEntryEntity> returnString = trueupEntityManager.getHTSMissingAssociationData(1L);
         assert(returnString!=null);*/
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#saveHTSMissingAssociations(java.util.List, long)}.
	 */
	@Test
	public void testSaveHTSMissingAssociations() throws Exception {
		List<CBPEntryEntity> mockEntries = new ArrayList<>();
		List<CBPEntry> mockCBPs = new ArrayList<>();

        when(
                this.trueupEntityManager.updateHTSMissingAssociationData(
                		(List<CBPEntry>)notNull(), anyLong())
              ).thenReturn(mockEntries);

        List<CBPEntryEntity> returnString = trueupEntityManager.updateHTSMissingAssociationData(mockCBPs, 2017L);
         assert(returnString!=null);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getAssociatedHTSBucket(long)}.
	 */
	@Test
	public void testGetAssociatedHTSBucket() throws Exception {
		List<CBPEntryEntity> mockEntries = new ArrayList<>(); 

        when(
                this.trueupEntityManager.getAssociateHTSBucket(
                		 anyLong())
              ).thenReturn(mockEntries);

        List<CBPEntryEntity> returnString = trueupEntityManager.getAssociateHTSBucket(1203L);
         assert(returnString!=null);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#saveAssociateHTSBucket(java.util.List, long)}.
	 */
	@Test
	@Ignore
	public void testSaveAssociateHTSBucket() throws Exception {
		List<CBPEntryEntity> mockEntries = new ArrayList<>(); 

        doNothing().when(
                this.trueupEntityManager).updateAssociateHTSBucket(
                		(List<CBPEntryEntity>)notNull(),2017L);

	}
	
	@Test
	@Ignore
	public void testUpdateCBPIngestedLineEntryInfo() throws Exception {
		CBPLineEntryUpdateInfo mockInfo = new CBPLineEntryUpdateInfo();
		mockInfo.setAssignedYear(2017L); 

//        doNothing().when(
//                this.trueupEntityManager).updateCBPIngestedLineEntryInfo(
//                		(CBPLineEntryUpdateInfo)notNull());

	}
		
	@Test
	@Ignore
	public void testdeleteIngestionFile() throws Exception {

		
		 doNothing().when(this.trueupEntityManager).deleteIngestionFile(
				 anyLong(),anyString()
       );
	}

	
	@SuppressWarnings("unchecked")
	@Test
	public void testgetTTBAmendments() throws Exception {
		List<TTBAmendmentEntity> mockEntries = new ArrayList<>();
		List<TTBAmendment> mockAmendments = new ArrayList<>();

        when(
                this.trueupEntityManager.getTTBAmendments(
                		 anyLong(),anyLong(),(List<TTBAmendment>)any())
              ).thenReturn(mockEntries);

        List<TTBAmendmentEntity> returnString = trueupEntityManager.getTTBAmendments(2017L,1L,mockAmendments);
         assert(returnString!=null);
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void testsaveTTBAmendments() throws Exception {
		List<TTBAmendmentEntity> mockEntries = new ArrayList<>(); 

        when(
                this.trueupEntityManager.saveTTBAmendment(
                		 (List<TTBAmendmentEntity>)any())
              ).thenReturn(mockEntries);

        List<TTBAmendmentEntity> returnString = trueupEntityManager.saveTTBAmendment(mockEntries);
         assert(returnString!=null);
	}
	
	
	@Test
	public void testCompareComments() throws Exception {
		List<TTBAmendmentEntity> mockEntries = new ArrayList<>(); 
        when(
                this.trueupEntityManager.saveTTBCompareComment(
                		 (List<TTBAmendmentEntity>)any())
              ).thenReturn(mockEntries);

        List<TTBAmendmentEntity> returnString = trueupEntityManager.saveTTBCompareComment(mockEntries);
         assert(returnString!=null);
	}


	
	/*************************************************************
     * MOCK OBJECTS
    *************************************************************/

	private List<TrueUpEntity> createTrueUpList() {
		List<TrueUpEntity> tulist = new ArrayList<>();
		TrueUpEntity tu = this.createMockTrueUp();
		tulist.add(tu);
		return tulist;
	}

	private TrueUpFilter createMockTrueUpFilter() {
		TrueUpFilter tuf = new TrueUpFilter();
		Set<Long> fiscalYrFilters = new HashSet<>();
		fiscalYrFilters.add(2016L);
		Set<String> statusFilters = new HashSet<>();
		statusFilters.add("Submitted");
		tuf.setFiscalYrFilters(fiscalYrFilters);
		tuf.setStatusFilters(statusFilters);
		return tuf;
	}

	private TrueUpEntity createMockTrueUpEntity() {
		TrueUpEntity te = new TrueUpEntity();
		te.setFiscalYear(2016L);
		te.setSubmittedDt(new Date());
		Set<TrueUpDocumentEntity> documents = new HashSet<>();
		TrueUpDocumentEntity tde = this.createMockTrueUpDocumentEntity();
		documents.add(tde);
		te.setDocuments(documents);
		return te;
	}

	private List<TrueUpDocumentEntity> createMockTrueUpDocumentEntities() {
		List<TrueUpDocumentEntity> docs = new ArrayList<>();
		TrueUpDocumentEntity tde = this.createMockTrueUpDocumentEntity();
		docs.add(tde);
		return docs;
	}

	private List<TrueUpDocument> createMockTrueUpDocuments() {
		List<TrueUpDocument> docs = new ArrayList<>();
		TrueUpDocument td = this.createMockTrueUpDocument();
		docs.add(td);
		return docs;
	}
	
	private TrueUpDocument createMockTrueUpDocument() {
		TrueUpDocument td = new TrueUpDocument();
		td.setVersionNum(1L);
		td.setDocDesc("test description");
		td.setErrorCount(0L);
		td.setFiscalYr(2016L);
		td.setTrueUpFilename("testfilename");
		td.setCreatedDt("");
		td.setCreatedBy("");
		td.setModifiedBy("");
		td.setModifiedDt("");
		return td;
	}

	private TrueUpEntity createMockTrueUp() {
		TrueUpEntity tu = new TrueUpEntity();
		tu.setFiscalYear(2016L);
		tu.setSubmittedDt(new Date());
		tu.setSubmittedInd("Not Submitted");
		return tu;
	}

	private TrueUpSubDocumentEntity createMockTrueUpSubDocumentEntity() {
		TrueUpSubDocumentEntity tsde = new TrueUpSubDocumentEntity();
		tsde.setAssessmentPdf("testpdf");
		tsde.setAuthor("test author");
		tsde.setDateUploaded(new Date());
		tsde.setDescription("test description");
		tsde.setDocumentNumber(1L);
		tsde.setFilename("test filename");
		tsde.setFiscalYr(2017L);
		tsde.setVersionNum(1);
		return tsde;
	}

	private List<TrueUpSubDocumentEntity> createMockTrueUpSubDocumentEntiies() {
		List<TrueUpSubDocumentEntity> docs = new ArrayList<>();
		TrueUpSubDocumentEntity tde = this.createMockTrueUpSubDocumentEntity();
		docs.add(tde);
		return docs;
	}


	private TrueUpDocumentEntity createMockTrueUpDocumentEntity() {
		TrueUpDocumentEntity tde = new TrueUpDocumentEntity();
		tde.setDocDesc("test description");
		tde.setErrorCount(0L);
		tde.setTrueUpDocId(1L);
		tde.setTrueUpFilename("testfilename");
		tde.setCreatedDt(new Date());
		tde.setCreatedBy("");
		tde.setModifiedBy("");
		tde.setModifiedDt(new Date());
		return tde;
	}

}

/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

/**
 * The Class Address.
 *
 * @author tgunter
 */
public class Address extends BaseTufaBean {

	/** The company id. */
	private Long companyId;
	
	private String ein;
	
	private String docStageId;
	private String StageId;
    
    /** The address type cd. */
    private String addressTypeCd;
    
    /** The street address. */
    private String streetAddress;
    
    /** The suite. */
    private String suite;
    
    /** The city. */
    private String city;
    
    /** The state. */
    private String state;
    
    /** The postal cd. */
    private String postalCd;
    
    /** The fax num. */
    private Long faxNum;
    
    /** The country cd. */
    private String countryCd;
    
    /** The country nm. */
    private String countryNm;
    
    /** Attention */
    private String attention;
    
    /** province */
    private String province;

    /**
     * Gets the company id.
     *
     * @return the companyId
     */
    public Long getCompanyId() {
        return companyId;
    }
    
    /**
     * Sets the company id.
     *
     * @param companyId the companyId to set
     */
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
    
    /**
     * Gets the address type cd.
     *
     * @return the addressTypeCd
     */
    public String getAddressTypeCd() {
        return addressTypeCd;
    }
    
    /**
     * Sets the address type cd.
     *
     * @param addressTypeCd the addressTypeCd to set
     */
    public void setAddressTypeCd(String addressTypeCd) {
        this.addressTypeCd = addressTypeCd;
    }
    
    /**
     * Gets the street address.
     *
     * @return the streetAddress
     */
    public String getStreetAddress() {
        return streetAddress;
    }
    
    /**
     * Sets the street address.
     *
     * @param streetAddress the streetAddress to set
     */
    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }
    
    /**
     * Gets the suite.
     *
     * @return the suite
     */
    public String getSuite() {
        return suite;
    }
    
    /**
     * Sets the suite.
     *
     * @param suite the suite to set
     */
    public void setSuite(String suite) {
        this.suite = suite;
    }
    
    /**
     * Gets the city.
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }
    
    /**
     * Sets the city.
     *
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }
    
    /**
     * Gets the state.
     *
     * @return the state
     */
    public String getState() {
        return state;
    }
    
    /**
     * Sets the state.
     *
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }
    
    /**
     * Gets the fax num.
     *
     * @return the faxNum
     */
    public Long getFaxNum() {
        return faxNum;
    }
    
    /**
     * Sets the fax num.
     *
     * @param faxNum the faxNum to set
     */
    public void setFaxNum(Long faxNum) {
        this.faxNum = faxNum;
    }
    
    /**
     * Gets the country cd.
     *
     * @return the countryCd
     */
    public String getCountryCd() {
        return countryCd;
    }
    
    /**
     * Sets the country cd.
     *
     * @param countryCd the countryCd to set
     */
    public void setCountryCd(String countryCd) {
        this.countryCd = countryCd;
    }
    
    /**
     * Gets the country nm.
     *
     * @return the country nm
     */
    public String getCountryNm() {
        return countryNm;
    }
    
    /**
     * Sets the country nm.
     *
     * @param countryNm the countryCd to set
     */
    public void setCountryNm(String countryNm) {
        this.countryNm = countryNm;
    }
	
	/**
	 * Gets the postal cd.
	 *
	 * @return the postalCd
	 */
	public String getPostalCd() {
		return postalCd;
	}
	
	/**
	 * Sets the postal cd.
	 *
	 * @param postalCd the postalCd to set
	 */
	public void setPostalCd(String postalCd) {
		this.postalCd = postalCd;
	}
	
	public String getProvince(){
		return province;
	}
	
	public void setProvince(String province){
		this.province = province;
	}
	
	public String getAttention(){
		return attention;
	}
	
	public void setAttention(String attention){
		this.attention = attention;
	}
	
	public String getDocStageId(){
		return docStageId;
	}
	
	public void setDocStageId(String docStageId){
		this.docStageId = docStageId;
	}
	
	public String getStageId(){
		return StageId;
	}
	
	public void setStageId(String StageId){
		this.StageId = StageId;
	}
	
	public String getEin(){
		return ein;
	}
	
	public void setEin(String ein){
		this.ein = ein;
	}
    
}

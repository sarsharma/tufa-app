package gov.hhs.fda.ctp.common.beans;

public class AffectedAssessmentbean {
    
    private String assessmentFY;
    private String assessmentQTR;
    private String assessmentType;
    private String companyName;
    private String ein;
    private String permitNumber;
    
    public String getAssessmentFY() {
        return assessmentFY;
    }
    public void setAssessmentFY(String assessmentFY) {
        this.assessmentFY = assessmentFY;
    }
    public String getAssessmentQTR() {
		return assessmentQTR;
	}
	public void setAssessmentQTR(String assessmentQTR) {
		this.assessmentQTR = assessmentQTR;
	}
    public String getAssessmentType() {
        return assessmentType;
    }
    public void setAssessmentType(String assessmentType) {
        this.assessmentType = assessmentType;
    }
    public String getCompanyName() {
        return companyName;
    }
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public String getEin() {
        return ein;
    }
    public void setEin(String ein) {
        this.ein = ein;
    }
    public String getPermitNumber() {
        return permitNumber;
    }
    public void setPermitNumber(String permitNumber) {
        this.permitNumber = permitNumber;
    }
	

}

package gov.hhs.fda.ctp.common.beans;

import java.util.ArrayList;
import java.util.List;

public class AnnImporterGrid {

	private List<AnnImporterGridRecord> summaryMonths;
	private List<AnnImporterGridRecord> summaryQuarterTotals;
	private AnnImporterGridRecord summaryTotal;
	private String tobaccoClass;
	
	/**
	 * @return the summaryMonths
	 */
	public List<AnnImporterGridRecord> getSummaryMonths() {
		return summaryMonths;
	}
	/**
	 * @param summaryMonths the summaryMonths to set
	 */
	public void setSummaryMonths(List<AnnImporterGridRecord> summaryMonths) {
		this.summaryMonths = summaryMonths;
		if(summaryMonths.size() > 3) {
			this.setSummaryQuarterTotals();
		}
		this.setSummaryTotal();
	}
	/**
	 * @return the tobaccoClass
	 */
	public String getTobaccoClass() {
		return tobaccoClass;
	}
	/**
	 * @param tobaccoClass the tobaccoClass to set
	 */
	public void setTobaccoClass(String tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}

	/**
	 * @return the summaryQuarterTotals
	 */
	public List<AnnImporterGridRecord> getSummaryQuarterTotals() {
		return summaryQuarterTotals;
	}
	
	private void setSummaryQuarterTotals() {
		for(int i = 0; i < 4; i++) {
			AnnImporterGridRecord quarterTotal = new AnnImporterGridRecord();
			quarterTotal.setMonth("Quarter " + (i + 1));
			
			for(int j = i*3; j < (i+1)*3; j++) {
				quarterTotal.addRecord(this.summaryMonths.get(j));
			}

			
			if(this.summaryQuarterTotals == null) {
				this.summaryQuarterTotals = new ArrayList<>();
			}
			this.summaryQuarterTotals.add(quarterTotal);
		}
	}

	/**
	 * @return the summaryTotal
	 */
	public AnnImporterGridRecord getSummaryTotal() {
		return summaryTotal;
	}
	
	private void setSummaryTotal() {
		summaryTotal = new AnnImporterGridRecord();
		
		for (AnnImporterGridRecord month : this.summaryMonths){
			summaryTotal.addRecord(month);
		};
	}
}

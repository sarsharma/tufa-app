package gov.hhs.fda.ctp.common.beans;

public class AnnImporterGridRecord {
	private String month;
	private Double summaryValue;
	private Double detailValue;
	private Double tufaValue;

	private Double summaryAndDetailDiff;
	private Double tufaAndDetailDiff;
	private Double tufaAndSummaryDiff;
	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}
	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}
	/**
	 * @return the summaryValue
	 */
	public Double getSummaryValue() {
		return summaryValue;
	}
	/**
	 * @param summaryValue the summaryValue to set
	 */
	public void setSummaryValue(Double summaryValue) {
		this.summaryValue = summaryValue;
	}
	/**
	 * @return the detailValue
	 */
	public Double getDetailValue() {
		return detailValue;
	}
	/**
	 * @param detailValue the detailValue to set
	 */
	public void setDetailValue(Double detailValue) {
		this.detailValue = detailValue;
	}
	/**
	 * @return the tufaValue
	 */
	public Double getTufaValue() {
		return tufaValue;
	}
	/**
	 * @param tufaValue the tufaValue to set
	 */
	public void setTufaValue(Double tufaValue) {
		this.tufaValue = tufaValue;
	}
	
	public void addTufaValue(Double tufaValue) {
		this.tufaValue = this.add(this.tufaValue, tufaValue);
	}
	
	/**
	 * @return the summaryAndDetailDiff
	 */
	public Double getSummaryAndDetailDiff() {
		this.summaryAndDetailDiff = this.subtract(this.summaryValue, this.detailValue);
		return summaryAndDetailDiff;
	}
	/**
	 * @return the tufaAndDetailDiff
	 */
	public Double getTufaAndDetailDiff() {
		this.tufaAndDetailDiff = this.subtract(this.tufaValue, this.detailValue);
		return tufaAndDetailDiff;
	}
	/**
	 * @return the tufaAndSummaryDiff
	 */
	public Double getTufaAndSummaryDiff() {
		this.tufaAndSummaryDiff = this.subtract(this.tufaValue, this.summaryValue);
		return tufaAndSummaryDiff;
	}
	
	public void addRecord(AnnImporterGridRecord record) {
		this.summaryValue = this.add(summaryValue, record.summaryValue);
		this.detailValue = this.add(detailValue, record.detailValue);
		this.tufaValue = this.add(tufaValue, record.tufaValue);
	}
	
	/**
	 * @return the difference of val1 and val2. If only one value is null it is treated as 0. If both values are null null is returned.
	 * */
	private Double subtract(Double val1, Double val2) {
		if(val1 == null && val2 == null) {
			return null;
		} else if (val1 == null) {
			return 0 - val2;
		} else if (val2 == null) {
			return val1;
		} else {
			return val1 - val2;
		}
	}
	
	/**
	 * @return the sum of both values. If both values are null the null is returned
	 * */
	private Double add (Double val1, Double val2) {
		if(val2 != null) {
			if(val1 == null) {
				return val2;
			} else {
				return val1 + val2;
			}
		} else {
			return val1;
		}
	}
}

/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import gov.hhs.fda.ctp.common.util.Constants;

/**
 * The Class Assessment.
 *
 * @author akari
 */
public class Assessment extends BaseTufaBean {

	/** The assessment id. */
	private long assessmentId;
	
	/** The assessment yr. */
	private int assessmentYr;
	
	/** The assessment qtr. */
	private int assessmentQtr;
	
	/** The display qtr. */
	private String displayQtr;
		
	/** The submitted ind. */
	private String submittedInd;
	
	private String cigarsubmittedInd;
	
	/** The assessment type. */
	private String assessmentType;
	
	/** Assessment approval message */
	private String approvalMessage;
	
	/** The versions */
	private List<AssessmentVersion> versions = new ArrayList<>();
	
	/** The quarter months. */
	private List<String> quarterMonths = new ArrayList<>();
	
	/** Annual True-Up Submitted Message 	 **/
	private String annualTrueUpSubmittedMsg;

	/** The Constant SUBMITTED. */
	public static final String SUBMITTED = "Submitted";
	
	/** The Constant NOT_SUBMITTED. */
	public static final String NOT_SUBMITTED = "Not Submitted";
	
	/** The Constant Q1. */
	public static final String Q1 = "First Quarter-Q1";
	
	/** The Constant Q2. */
	public static final String Q2 = "Second Quarter-Q2";
	
	/** The Constant Q3. */
	public static final String Q3 = "Third Quarter-Q3";
	
	/** The Constant Q4. */
	public static final String Q4 = "Fourth Quarter-Q4";
	

	/**
	 * Gets the assessment id.
	 *
	 * @return the assessmentId
	 */
	public long getAssessmentId() {
		return assessmentId;
	}

	/**
	 * Sets the assessment id.
	 *
	 * @param assessmentId            the assessmentId to set
	 */
	public void setAssessmentId(long assessmentId) {
		this.assessmentId = assessmentId;
	}

	/**
	 * Gets the assessment yr.
	 *
	 * @return the assessmentYr
	 */
	public int getAssessmentYr() {
		return assessmentYr;
	}

	/**
	 * Sets the assessment yr.
	 *
	 * @param assessmentYr            the assessmentYr to set
	 */
	public void setAssessmentYr(int assessmentYr) {
		this.assessmentYr = assessmentYr;
	}

	/**
	 * Gets the assessment qtr.
	 *
	 * @return the assessmentQtr
	 */
	public int getAssessmentQtr() {
		return assessmentQtr;
	}

	/**
	 * Sets the assessment qtr.
	 *
	 * @param assessmentQtr            the assessmentQtr to set
	 */
	public void setAssessmentQtr(int assessmentQtr) {
		this.assessmentQtr = assessmentQtr;
	}

	/**
	 * Gets the submitted ind.
	 *
	 * @return the submittedInd
	 */
	public String getSubmittedInd() {
		
		return "Y".equalsIgnoreCase(submittedInd) ? SUBMITTED : NOT_SUBMITTED;
	
	}
	
	/**
	 * Sets the submitted ind.
	 *
	 * @param submittedInd            the submittedInd to set
	 */
	public void setSubmittedInd(String submittedInd) {
		this.submittedInd = submittedInd;
	}
	
	/**
	 * Gets the Cigarsubmitted ind.
	 *
	 * @return the submittedInd
	 */

     public String getCigarSubmittedInd() {
		
    	 if(cigarsubmittedInd == null)
    	 {
    		 return NOT_SUBMITTED;
    	 }
    	 else 
    		 return cigarsubmittedInd;
    	 
    	
	
	}
	
     /**
 	 * Sets the  Cigar submitted ind.
 	 *
 	 * @param  Cigar submittedInd            the Cigar submittedInd to set
 	 */
 	public void setCigarSubmittedInd(String cigarsubmittedInd) {
 		this.cigarsubmittedInd = cigarsubmittedInd;
 	}

	/**
	 * Gets the assessment type.
	 *
	 * @return the assessmentType
	 */
	public String getAssessmentType() {
		return assessmentType;
	}

	/**
	 * Sets the assessment type.
	 *
	 * @param assessmentType            the assessmentType to set
	 */
	public void setAssessmentType(String assessmentType) {
		this.assessmentType = assessmentType;
	}

	/**
	 * Gets the quarter months.
	 *
	 * @return the quarterMonths
	 */
	public List<String> getQuarterMonths() {
		switch (this.assessmentQtr) {
		case 1:
			this.quarterMonths.add(Constants.MMMToMMMM.get("OCT"));
			this.quarterMonths.add(Constants.MMMToMMMM.get("NOV"));
			this.quarterMonths.add(Constants.MMMToMMMM.get("DEC"));
			break;
		case 2:
			this.quarterMonths.add(Constants.MMMToMMMM.get("JAN"));
			this.quarterMonths.add(Constants.MMMToMMMM.get("FEB"));
			this.quarterMonths.add(Constants.MMMToMMMM.get("MAR"));
			break;
		case 3:
			this.quarterMonths.add(Constants.MMMToMMMM.get("APR"));
			this.quarterMonths.add(Constants.MMMToMMMM.get("MAY"));
			this.quarterMonths.add(Constants.MMMToMMMM.get("JUN"));
			break;

		default:
			this.quarterMonths.add(Constants.MMMToMMMM.get("JUL"));
			this.quarterMonths.add(Constants.MMMToMMMM.get("AUG"));
			this.quarterMonths.add(Constants.MMMToMMMM.get("SEP"));
			break;
		}
		return quarterMonths;
	}

	/**
	 * Sets the quarter months.
	 *
	 * @param quarterMonths            the quarterMonths to set
	 */
	public void setQuarterMonths(List<String> quarterMonths) {
		this.quarterMonths = quarterMonths;
	}

	/**
	 * Gets the display qtr.
	 *
	 * @return the displayQtr
	 */
	public String getDisplayQtr() {

		switch (this.assessmentQtr) {
		case 1:
			this.displayQtr = Q1;
			break;
		case 2:
			this.displayQtr = Q2;
			break;
		case 3:
			this.displayQtr = Q3;
			break;
		default:
			this.displayQtr = Q4;
			break;
		}
		return displayQtr;
	}

	/**
	 * Sets the display qtr.
	 *
	 * @param displayQtr            the displayQtr to set
	 */
	public void setDisplayQtr(String displayQtr) {
		this.displayQtr = displayQtr;
	}

	/**
	 * Gets the approval message.
	 * @return
	 */
	public String getApprovalMessage() {
		return approvalMessage;
	}

	/**
	 * Sets the approval message.
	 * @param approvalMessage
	 */
	public void setApprovalMessage(String approvalMessage) {
		this.approvalMessage = approvalMessage;
	}

	/**
	 * @return the versions
	 */
	public List<AssessmentVersion> getVersions() {
		return versions;
	}

	/**
	 * @param versions the versions to set
	 */
	public void setVersions(List<AssessmentVersion> versions) {
		this.versions = versions;
	}
	
	public String getAnnualTrueUpSubmittedMsg() {
		return annualTrueUpSubmittedMsg;
	}

	public void setAnnualTrueUpSubmittedMsg(String annualTrueUpSubmittedMsg) {
		this.annualTrueUpSubmittedMsg = annualTrueUpSubmittedMsg;
	}

}

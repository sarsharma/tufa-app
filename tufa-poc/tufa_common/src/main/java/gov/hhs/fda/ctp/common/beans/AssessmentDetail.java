/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

/**
 * @author tgunter
 *
 */
public class AssessmentDetail {
	private int rank;
	private int year;
	private int quarter;
	private String company;
	private String permit;
	private String EIN;
	private double volume;
	private double tax;
	private double share;
	private String streetAddress;
	private String city;
	private String state;
	private String tobaccoClass;
	/**
	 * @return the rank
	 */
	public int getRank() {
		return rank;
	}
	/**
	 * @param rank the rank to set
	 */
	public void setRank(int rank) {
		this.rank = rank;
	}
	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}
	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}
	/**
	 * @return the quarter
	 */
	public int getQuarter() {
		return quarter;
	}
	/**
	 * @param quarter the quarter to set
	 */
	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}
	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}
	/**
	 * @return the permit
	 */
	public String getPermit() {
		return permit;
	}
	/**
	 * @param permit the permit to set
	 */
	public void setPermit(String permit) {
		this.permit = permit;
	}
	/**
	 * @return the eIN
	 */
	public String getEIN() {
		return EIN;
	}
	/**
	 * @param eIN the eIN to set
	 */
	public void setEIN(String eIN) {
		EIN = eIN;
	}
	/**
	 * @return the volume
	 */
	public double getVolume() {
		return volume;
	}
	/**
	 * @param volume the volume to set
	 */
	public void setVolume(double volume) {
		this.volume = volume;
	}
	/**
	 * @return the tax
	 */
	public double getTax() {
		return tax;
	}
	/**
	 * @param tax the tax to set
	 */
	public void setTax(double tax) {
		this.tax = tax;
	}
	/**
	 * @return the share
	 */
	public double getShare() {
		return share;
	}
	/**
	 * @param share the share to set
	 */
	public void setShare(double share) {
		this.share = share;
	}
	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}
	/**
	 * @param streetAddress the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the tobaccoClass
	 */
	public String getTobaccoClass() {
		return tobaccoClass;
	}
	/**
	 * @param tobaccoClass the tobaccoClass to set
	 */
	public void setTobaccoClass(String tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	private String type;
}

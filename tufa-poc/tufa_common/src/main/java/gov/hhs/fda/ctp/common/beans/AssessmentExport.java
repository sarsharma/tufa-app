/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

/**
 * @author tgunter
 *
 */
public class AssessmentExport {
	private String title;
	private String tobaccoType;
	private String csv;
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the tobaccoType
	 */
	public String getTobaccoType() {
		return tobaccoType;
	}
	/**
	 * @param tobaccoType the tobaccoType to set
	 */
	public void setTobaccoType(String tobaccoType) {
		this.tobaccoType = tobaccoType;
	}
	/**
	 * @return the csv
	 */
	public String getCsv() {
		return csv;
	}
	/**
	 * @param csv the csv to set
	 */
	public void setCsv(String csv) {
		this.csv = csv;
	}
}
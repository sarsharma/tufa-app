/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.List;
import java.util.Set;

/**
 * The Class AssessmentFilter.
 *
 * @author akari
 */
public class AssessmentFilter extends BaseTufaBean {

	/** The quarter filters. */
	private Set<Integer> quarterFilters;
	
	/** The status filters. */
	private Set<String> statusFilters;
	
	/** The fiscal yr filters. */
	private Set<Integer> fiscalYrFilters;
	
	/** The results. */
	private List<Assessment> results;

	/**
	 * Gets the results.
	 *
	 * @return the results
	 */
	public List<Assessment> getResults() {
		return results;
	}

	/**
	 * Sets the results.
	 *
	 * @param results            the results to set
	 */
	public void setResults(List<Assessment> results) {
		this.results = results;
	}

	/**
	 * Gets the status filters.
	 *
	 * @return the statusFilters
	 */
	public Set<String> getStatusFilters() {
		return statusFilters;
	}

	/**
	 * Sets the status filters.
	 *
	 * @param statusFilters            the statusFilters to set
	 */
	public void setStatusFilters(Set<String> statusFilters) {
		this.statusFilters = statusFilters;
	}

	/**
	 * Gets the quarter filters.
	 *
	 * @return the quarterFilters
	 */
	public Set<Integer> getQuarterFilters() {
		return quarterFilters;
	}

	/**
	 * Sets the quarter filters.
	 *
	 * @param quarterFilters            the quarterFilters to set
	 */
	public void setQuarterFilters(Set<Integer> quarterFilters) {
		this.quarterFilters = quarterFilters;
	}

	/**
	 * Gets the fiscal yr filters.
	 *
	 * @return the fiscalYrFilters
	 */
	public Set<Integer> getFiscalYrFilters() {
		return fiscalYrFilters;
	}

	/**
	 * Sets the fiscal yr filters.
	 *
	 * @param fiscalYrFilters            the fiscalYrFilters to set
	 */
	public void setFiscalYrFilters(Set<Integer> fiscalYrFilters) {
		this.fiscalYrFilters = fiscalYrFilters;
	}

}

/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * the class to hold Quarterly Assessment report status data
 * 
 * @author akari
 *
 */
public class AssessmentReportStatus extends BaseTufaBean {

	int year;
	int fiscalYr;
	String legalNm;
	long companyId;
	long permitId;
	String permitNum;
	String permitTypeCd;
	String permitStatusTypeCd;
	List<AssessmentReportStatusMonth> quarterReports = new ArrayList<>();

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the fiscalYr
	 */
	public int getFiscalYr() {
		return fiscalYr;
	}

	/**
	 * @param fiscalYr
	 *            the fiscalYr to set
	 */
	public void setFiscalYr(int fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	/**
	 * @return the legalNm
	 */
	public String getLegalNm() {
		return legalNm;
	}

	/**
	 * @param legalNm
	 *            the legalNm to set
	 */
	public void setLegalNm(String legalNm) {
		this.legalNm = legalNm;
	}

	/**
	 * @return the companyId
	 */
	public long getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId
	 *            the companyId to set
	 */
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the permitId
	 */
	public long getPermitId() {
		return permitId;
	}

	/**
	 * @param permitId
	 *            the permitId to set
	 */
	public void setPermitId(long permitId) {
		this.permitId = permitId;
	}

	/**
	 * @return the permitNum
	 */
	public String getPermitNum() {
		return permitNum;
	}

	/**
	 * @param permitNum
	 *            the permitNum to set
	 */
	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}

	/**
	 * @return the permitTypeCd
	 */
	public String getPermitTypeCd() {
		return permitTypeCd;
	}

	/**
	 * @param permitTypeCd
	 *            the permitTypeCd to set
	 */
	public void setPermitTypeCd(String permitTypeCd) {
		this.permitTypeCd = permitTypeCd;
	}

	/**
	 * @return the permitStatusTypeCd
	 */
	public String getPermitStatusTypeCd() {
		return permitStatusTypeCd;
	}

	/**
	 * @param permitStatusTypeCd
	 *            the permitStatusTypeCd to set
	 */
	public void setPermitStatusTypeCd(String permitStatusTypeCd) {
		this.permitStatusTypeCd = permitStatusTypeCd;
	}

	/**
	 * @return the quarterReports
	 */
	public List<AssessmentReportStatusMonth> getQuarterReports() {
		return quarterReports;
	}

	/**
	 * @param quarterReports the quarterReports to set
	 */
	public void setQuarterReports(List<AssessmentReportStatusMonth> quarterReports) {
		this.quarterReports = quarterReports;
	}

}

/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

/**
 * @author akari
 *
 */
public class AssessmentReportStatusMonth {
	String month;
	String periodStatusTypeCd;
	String cigarOnly;
	long periodId;
	String zeroFlag;

	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return the periodStatusTypeCd
	 */
	public String getPeriodStatusTypeCd() {
		return periodStatusTypeCd;
	}

	/**
	 * @param periodStatusTypeCd
	 *            the periodStatusTypeCd to set
	 */
	public void setPeriodStatusTypeCd(String periodStatusTypeCd) {
		this.periodStatusTypeCd = periodStatusTypeCd;
	}

	/**
	 * @return the periodId
	 */
	public long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId
	 *            the periodId to set
	 */
	public void setPeriodId(long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the cigarOnly
	 */
	public String getCigarOnly() {
		return cigarOnly;
	}

	/**
	 * @param cigarOnly the cigarOnly to set
	 */
	public void setCigarOnly(String cigarOnly) {
		this.cigarOnly = cigarOnly;
	}

	/**
	 * @return the zeroFlag
	 */
	public String getZeroFlag() {
		return zeroFlag;
	}

	/**
	 * @param zeroFlag the zeroFlag to set
	 */
	public void setZeroFlag(String zeroFlag) {
		this.zeroFlag = zeroFlag;
	}

}

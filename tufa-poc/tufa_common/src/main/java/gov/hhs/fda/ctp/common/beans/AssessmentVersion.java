/**
 *
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author tgunter
 *
 */
public class AssessmentVersion implements Comparable<AssessmentVersion> {
	private long versionNum;
	private String createdBy;
	private long cigarQtr = 0L;
	private String createdDt;
	private String marketshareType;
	@JsonIgnore
	private List<MarketShare> marketShares;
	@JsonIgnore
	private boolean includeDeltas = false;
	private Map<String, List<AssessmentExport>> exports;
	
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @param 
	 */
	public void setCigarQtr(long cigarQtr) {
		this.cigarQtr = cigarQtr;
	}

	/**
	 * @return the author
	 */
	public long getCigarQtr() {
		return cigarQtr;
	}
	
	/**
	 * @return the versionNum
	 */
	public long getVersionNum() {
		return versionNum;
	}
	/**
	 * @param versionNum the versionNum to set
	 */
	public void setVersionNum(long versionNum) {
		this.versionNum = versionNum;
	}
	public String getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the marketShares
	 */
	public List<MarketShare> getMarketShares() {
		return marketShares;
	}
	/**
	 * @param marketShares the marketShares to set
	 */
	public void setMarketShares(List<MarketShare> marketShares) {
		this.marketShares = marketShares;
	}
	/**
	 * @return the exports
	 */
	public Map<String, List<AssessmentExport>> getExports() {
		return exports;
	}
	/**
	 * @param exports the exports to set
	 */
	public void setExports(Map<String, List<AssessmentExport>> exports) {
		this.exports = exports;
	}

	@Override
	public int compareTo(AssessmentVersion version) {
		// version zero is always the latest version
		long thisVersion = versionNum!=0?versionNum:Long.MAX_VALUE;
		long thatVersion = version.getVersionNum()!=0L?version.getVersionNum():Long.MAX_VALUE;
		// sort descending
		return Long.compare(thatVersion, thisVersion);
	}

	/**
	 * @return the marketshareType
	 */
	public String getMarketshareType() {
		return marketshareType;
	}

	/**
	 * @param marketshareType the marketshareType to set
	 */
	public void setMarketshareType(String marketshareType) {
		this.marketshareType = marketshareType;
	}

	/**
	 * @return the includeDeltas
	 */
	public boolean isIncludeDeltas() {
		return includeDeltas;
	}

	/**
	 * @param includeDeltas the includeDeltas to set
	 */
	public void setIncludeDeltas(boolean includeDeltas) {
		this.includeDeltas = includeDeltas;
	}
}

/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.Comparator;

/**
 * @author tgunter
 *
 */
public class AssessmentVersionComparator implements Comparator<AssessmentVersion> {
	public int compare(AssessmentVersion version1, AssessmentVersion version2) {
		Long v1 = version1.getVersionNum();
		Long v2 = version2.getVersionNum();
		// version 0 always gets displayed first
		if(v1 == 0) {
			v1 = 99L;
		}
		if(v2 == 0) {
			v2 = 99L;
		}
		// reverse order comparison
		return v2.compareTo(v1);
	}
}

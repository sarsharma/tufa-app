/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

/**
 *
 * @author Ragh.Balasubramanian
 */
public class Attachment {
    private long documentId; 
    private String docDesc;   
    private String formTypes;    
    private byte[] reportPdf;
    private Date createdDt;

    public long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(long documentId) {
        this.documentId = documentId;
    }

    public String getDocDesc() {
        return docDesc;
    }

    public void setDocDesc(String docDesc) {
        this.docDesc = docDesc;
    }

    public String getFormTypes() {
        return formTypes;
    }

    public void setFormTypes(String formTypes) {
        this.formTypes = formTypes;
    }

    public byte[] getReportPdf() {
		return reportPdf;
	}

	public void setReportPdf(byte[] reportPdf) {
		this.reportPdf = reportPdf;
	}

	public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }
    
}

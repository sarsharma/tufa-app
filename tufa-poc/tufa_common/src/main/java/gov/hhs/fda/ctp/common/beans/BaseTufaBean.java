/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

/**
 * Fields common to all beans.
 * 
 * @author tgunter
 *
 */
public class BaseTufaBean {
    
    /** The created by. */
    private String createdBy;
    
    /** The created dt. */
    private String createdDt;
    
    /** The modified by. */
    private String modifiedBy;
    
    /** The modified dt. */
    private String modifiedDt;

    /**
     * Gets the created by.
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }
    
    /**
     * Sets the created by.
     *
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
    /**
     * Gets the created dt.
     *
     * @return the createdDt
     */
    public String getCreatedDt() {
        return createdDt;
    }
    
    /**
     * Sets the created dt.
     *
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }
    
    /**
     * Gets the modified by.
     *
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }
    
    /**
     * Sets the modified by.
     *
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    
    /**
     * Gets the modified dt.
     *
     * @return the modifiedDt
     */
    public String getModifiedDt() {
        return modifiedDt;
    }
    
    /**
     * Sets the modified dt.
     *
     * @param modifiedDt the modifiedDt to set
     */
    public void setModifiedDt(String modifiedDt) {
        this.modifiedDt = modifiedDt;
    }

}

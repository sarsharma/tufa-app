package gov.hhs.fda.ctp.common.beans;

import java.util.List;
import java.util.Map;

public class CBPAmendment {
	
    private Long cbpAmendmentId;
	
    private Long cbpCompanyId;
	
    private Long tobaccoClassId;
	
    private String fiscalYr;
	
	private String qtr;
	
	private Double amendedTotalTax;
	
	private Double amendedTotalVolume;
		
	private String acceptanceFlag;
	
	private String inProgressFlag;
	
	private String reviewFlag;
	
	private String comments;
	
	private String tobaccoType;
	
	private Boolean inclAllQtrs;
	
	private Double fdaDelta;
	
	private Map<String,String> mixedActionQuarterDetails;
	
	
	private List<CBPQtrComment> userComments;
	
	private List<Integer> excludedPermits;
	
	private boolean isAmendmentPresent=true;
	
	private Double mixedActionVolume;
    
    private Double mixedActionTax;
    

	public Long getCbpAmendmentId() {
		return cbpAmendmentId;
	}

	public void setCbpAmendmentId(Long cbpAmendmentId) {
		this.cbpAmendmentId = cbpAmendmentId;
	}

	public Long getCbpCompanyId() {
		return cbpCompanyId;
	}

	public void setCbpCompanyId(Long cbpCompanyId) {
		this.cbpCompanyId = cbpCompanyId;
	}

	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}

	public void setTobaccoClassId(Long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}

	public String getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getQtr() {
		return qtr;
	}

	public void setQtr(String qtr) {
		this.qtr = qtr;
	}

	public Double getAmendedTotalTax() {
		return amendedTotalTax;
	}

	public void setAmendedTotalTax(Double amendedTotalTax) {
		this.amendedTotalTax = amendedTotalTax;
	}

	public Double getAmendedTotalVolume() {
		return amendedTotalVolume;
	}

	public void setAmendedTotalVolume(Double amendedTotalVolume) {
		this.amendedTotalVolume = amendedTotalVolume;
	}

	public String getAcceptanceFlag() {
		return acceptanceFlag;
	}

	public void setAcceptanceFlag(String acceptanceFlag) {
		this.acceptanceFlag = acceptanceFlag;
	}

	
	public String getInProgressFlag() {
		return inProgressFlag;
	}

	public void setInProgressFlag(String inProgressFlag) {
		this.inProgressFlag = inProgressFlag;
	}
	
	public String getreviewFlag() {
		return reviewFlag;
	}

	public void setreviewFlag(String reviewFlag) {
		this.reviewFlag = reviewFlag;
	}
	
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTobaccoType() {
		return tobaccoType;
	}

	public void setTobaccoType(String tobaccoType) {
		this.tobaccoType = tobaccoType;
	}

	public List<CBPQtrComment> getUserComments() {
		return userComments;
	}

	public void setUserComments(List<CBPQtrComment> userComments) {
		this.userComments = userComments;
	}

	public Boolean getInclAllQtrs() {
		return inclAllQtrs;
	}

	public void setInclAllQtrs(Boolean inclAllQtrs) {
		this.inclAllQtrs = inclAllQtrs;
	}

	public Double getFdaDelta() {
		return fdaDelta;
	}

	public void setFdaDelta(Double fdaDelta) {
		this.fdaDelta = fdaDelta;
	}

	public List<Integer> getExcludedPermits() {
		return excludedPermits;
	}

	public void setExcludedPermits(List<Integer> excludedPermits) {
		this.excludedPermits = excludedPermits;
	}

	public boolean isAmendmentPresent() {
		return isAmendmentPresent;
	}

	public void setAmendmentPresent(boolean isAmendmentPresent) {
		this.isAmendmentPresent = isAmendmentPresent;
	}

	public Map<String, String> getMixedActionQuarterDetails() {
		return mixedActionQuarterDetails;
	}

	public void setMixedActionQuarterDetails(Map<String, String> mixedActionQuarterDetails) {
		this.mixedActionQuarterDetails = mixedActionQuarterDetails;
	}

	public Double getMixedActionVolume() {
		return mixedActionVolume;
	}

	public void setMixedActionVolume(Double mixedActionVolume) {
		this.mixedActionVolume = mixedActionVolume;
	}

	public Double getMixedActionTax() {
		return mixedActionTax;
	}

	public void setMixedActionTax(Double mixedActionTax) {
		this.mixedActionTax = mixedActionTax;
	}
	
	
	
	
}

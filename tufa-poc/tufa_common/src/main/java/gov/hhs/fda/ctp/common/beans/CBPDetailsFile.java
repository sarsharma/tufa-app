package gov.hhs.fda.ctp.common.beans;

public class CBPDetailsFile extends BaseTufaBean{
    private Long lineItemId;
	private String entrySummaryNumber;
	private String entrySummaryDate;
    private String entryDate;
    private long htsNumber;
    private Double  lineTarrifQty1;
	private String importerNumber;
	private String importerName;
	private String ultimateConsigneeNumber;
	private String ultimateConsigneeName;
    private String htsDescription;
    private String lineTarrifUOM1;
    private long lineNumber;
    private String entryTypeCode;
	private Double lineTariffGoodsValueAmount;
	private Double irTaxAmount;
	private String importDate;
	/**
	 * @return the lineItemId
	 */
	public Long getLineItemId() {
		return lineItemId;
	}
	public String getImportDate() {
        return importDate;
    }
    public void setImportDate(String importDate) {
        this.importDate = importDate;
    }
    /**
	 * @param lineItemId the lineItemId to set
	 */
	public void setLineItemId(Long lineItemId) {
		this.lineItemId = lineItemId;
	}
	/**
	 * @return the entrySummaryNumber
	 */
	public String getEntrySummaryNumber() {
		return entrySummaryNumber;
	}
	/**
	 * @param entrySummaryNumber the entrySummaryNumber to set
	 */
	public void setEntrySummaryNumber(String entrySummaryNumber) {
		this.entrySummaryNumber = entrySummaryNumber;
	}
	/**
	 * @return the entrySummaryDate
	 */
	public String getEntrySummaryDate() {
		return entrySummaryDate;
	}
	/**
	 * @param entrySummaryDate the entrySummaryDate to set
	 */
	public void setEntrySummaryDate(String entrySummaryDate) {
		this.entrySummaryDate = entrySummaryDate;
	}
	/**
	 * @return the entryDate
	 */
	public String getEntryDate() {
		return entryDate;
	}
	/**
	 * @param entryDate the entryDate to set
	 */
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	/**
	 * @return the htsNumber
	 */
	public long getHtsNumber() {
		return htsNumber;
	}
	/**
	 * @param htsNumber the htsNumber to set
	 */
	public void setHtsNumber(long htsNumber) {
		this.htsNumber = htsNumber;
	}
	/**
	 * @return the lineTarrifQty1
	 */
	public Double getLineTarrifQty1() {
		return lineTarrifQty1;
	}
	/**
	 * @param lineTarrifQty1 the lineTarrifQty1 to set
	 */
	public void setLineTarrifQty1(Double lineTarrifQty1) {
		this.lineTarrifQty1 = lineTarrifQty1;
	}
	/**
	 * @return the importerNumber
	 */
	public String getImporterNumber() {
		return importerNumber;
	}
	/**
	 * @param importerNumber the importerNumber to set
	 */
	public void setImporterNumber(String importerNumber) {
		this.importerNumber = importerNumber;
	}
	/**
	 * @return the importerName
	 */
	public String getImporterName() {
		return importerName;
	}
	/**
	 * @param importerName the importerName to set
	 */
	public void setImporterName(String importerName) {
		this.importerName = importerName;
	}
	/**
	 * @return the ultimateConsigneeNumber
	 */
	public String getUltimateConsigneeNumber() {
		return ultimateConsigneeNumber;
	}
	/**
	 * @param ultimateConsigneeNumber the ultimateConsigneeNumber to set
	 */
	public void setUltimateConsigneeNumber(String ultimateConsigneeNumber) {
		this.ultimateConsigneeNumber = ultimateConsigneeNumber;
	}
	/**
	 * @return the ultimateConsigneeName
	 */
	public String getUltimateConsigneeName() {
		return ultimateConsigneeName;
	}
	/**
	 * @param ultimateConsigneeName the ultimateConsigneeName to set
	 */
	public void setUltimateConsigneeName(String ultimateConsigneeName) {
		this.ultimateConsigneeName = ultimateConsigneeName;
	}
	/**
	 * @return the htsDescription
	 */
	public String getHtsDescription() {
		return htsDescription;
	}
	/**
	 * @param htsDescription the htsDescription to set
	 */
	public void setHtsDescription(String htsDescription) {
		this.htsDescription = htsDescription;
	}
	/**
	 * @return the lineTarrifUOM1
	 */
	public String getLineTarrifUOM1() {
		return lineTarrifUOM1;
	}
	/**
	 * @param lineTarrifUOM1 the lineTarrifUOM1 to set
	 */
	public void setLineTarrifUOM1(String lineTarrifUOM1) {
		this.lineTarrifUOM1 = lineTarrifUOM1;
	}
	/**
	 * @return the lineNumber
	 */
	public long getLineNumber() {
		return lineNumber;
	}
	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(long lineNumber) {
		this.lineNumber = lineNumber;
	}
	/**
	 * @return the entryTypeCode
	 */
	public String getEntryTypeCode() {
		return entryTypeCode;
	}
	/**
	 * @param entryTypeCode the entryTypeCode to set
	 */
	public void setEntryTypeCode(String entryTypeCode) {
		this.entryTypeCode = entryTypeCode;
	}
	/**
	 * @return the lineTariffGoodsValueAmount
	 */
	public Double getLineTariffGoodsValueAmount() {
		return lineTariffGoodsValueAmount;
	}
	/**
	 * @param lineTariffGoodsValueAmount the lineTariffGoodsValueAmount to set
	 */
	public void setLineTariffGoodsValueAmount(Double lineTariffGoodsValueAmount) {
		this.lineTariffGoodsValueAmount = lineTariffGoodsValueAmount;
	}
	/**
	 * @return the irTaxAmount
	 */
	public Double getIrTaxAmount() {
		return irTaxAmount;
	}
	/**
	 * @param irTaxAmount the irTaxAmount to set
	 */
	public void setIrTaxAmount(Double irTaxAmount) {
		this.irTaxAmount = irTaxAmount;
	}
}

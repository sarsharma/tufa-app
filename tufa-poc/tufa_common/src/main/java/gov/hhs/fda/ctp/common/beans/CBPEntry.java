package gov.hhs.fda.ctp.common.beans;

import java.util.Date;
import java.util.Set;


public class CBPEntry {

    private Long cbpEntryId;

    private String entryNum;
    
    private Long lineNum;
    
    private String entryTypeCd;
    
    private String entrySummDt;
    
    private String entryDt;

    private String importDt;

    private String uomCd;
    
    private Double taxRate;
    
    private String htsCd;
    
    private Double qtyRemoved;
    
    private Double estimatedTax;
    
    private Double estimatedDuty;

    private Long tobaccoClassId;
    
    private String defaultHtsFlag;

    private String fiscalYear;
    
    private String assignedMonth;
    
    private String fiscalQtr;
    
    private String includeFlag;
    
    private String createdBy;
   
    private Date createdDt;

    private String modifiedBy;
    
    private String tobaccoType;
    
	private Date modifiedDt;
    
    private CBPImporter cbpImporter;
    
    private TobaccoClass tobaccoClass;
    
    private boolean providedAnswer;
    // to be used for the associate HTS code bucket only.
    private Set<String> companies;
    
    private String excludedFlag;
    
    private String reallocatedFlag;
    
    public boolean isProvidedAnswer() {
		return providedAnswer;
	}

	public void setProvidedAnswer(boolean providedAnswer) {
		this.providedAnswer = providedAnswer;
	}

	public Long getCbpEntryId() {
		return cbpEntryId;
	}

	public void setCbpEntryId(Long cbpEntryId) {
		this.cbpEntryId = cbpEntryId;
	}

	public String getEntryNum() {
		return entryNum;
	}

	public void setEntryNum(String entryNum) {
		this.entryNum = entryNum;
	}

	public Long getLineNum() {
		return lineNum;
	}

	public void setLineNum(Long lineNum) {
		this.lineNum = lineNum;
	}

	public String getEntryTypeCd() {
		return entryTypeCd;
	}

	public void setEntryTypeCd(String entryTypeCd) {
		this.entryTypeCd = entryTypeCd;
	}

	public String getEntryDt() {
		return entryDt;
	}

	public void setEntryDt(String entryDt) {
		this.entryDt = entryDt;
	}

	public String getEntrySummDt() {
		return entrySummDt;
	}

	public void setEntrySummDt(String entrySummDt) {
		this.entrySummDt = entrySummDt;
	}

	public String getImportDt() {
		return importDt;
	}

	public void setImportDt(String importDt) {
		this.importDt = importDt;
	}

	public String getUomCd() {
		return uomCd;
	}

	public void setUomCd(String uomCd) {
		this.uomCd = uomCd;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public String getHtsCd() {
		return htsCd;
	}

	public void setHtsCd(String htsCd) {
		this.htsCd = htsCd;
	}

	public Double getQtyRemoved() {
		return qtyRemoved;
	}

	public void setQtyRemoved(Double qtyRemoved) {
		this.qtyRemoved = qtyRemoved;
	}

	public Double getEstimatedTax() {
		return estimatedTax;
	}

	public void setEstimatedTax(Double estimatedTax) {
		this.estimatedTax = estimatedTax;
	}

	public Double getEstimatedDuty() {
		return estimatedDuty;
	}

	public void setEstimatedDuty(Double estimatedDuty) {
		this.estimatedDuty = estimatedDuty;
	}

	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}

	public void setTobaccoClassId(Long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}

	/**
	 * @return the defaultHtsFlag
	 */
	public String getDefaultHtsFlag() {
		return defaultHtsFlag;
	}

	/**
	 * @param defaultHtsFlag the defaultHtsFlag to set
	 */
	public void setDefaultHtsFlag(String defaultHtsFlag) {
		this.defaultHtsFlag = defaultHtsFlag;
	}

	public String getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public String getAssignedMonth() {
		return assignedMonth;
	}

	public void setAssignedMonth(String assignedMonth) {
		this.assignedMonth = assignedMonth;
	}

	public String getFiscalQtr() {
		return fiscalQtr;
	}

	public void setFiscalQtr(String fiscalQtr) {
		this.fiscalQtr = fiscalQtr;
	}

	public String getIncludeFlag() {
		return includeFlag;
	}

	public void setIncludeFlag(String includeFlag) {
		this.includeFlag = includeFlag;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public CBPImporter getCbpImporter() {
		return cbpImporter;
	}

	public void setCbpImporter(CBPImporter cbpImporter) {
		this.cbpImporter = cbpImporter;
	}

	public String getTobaccoType() {
		return tobaccoType;
	}

	public void setTobaccoType(String tobaccoType) {
		this.tobaccoType = tobaccoType;
	}

	public TobaccoClass getTobaccoClass() {
		return tobaccoClass;
	}

	public void setTobaccoClass(TobaccoClass tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}

	/**
	 * @return the companies
	 */
	public Set<String> getCompanies() {
		return companies;
	}

	/**
	 * @param companies the companies to set
	 */
	public void setCompanies(Set<String> companies) {
		this.companies = companies;
	}

	public String getExcludedFlag() {
		return excludedFlag;
	}

	public void setExcludedFlag(String excludedFlag) {
		this.excludedFlag = excludedFlag;
	}

	public String getReallocatedFlag() {
		return reallocatedFlag;
	}

	public void setReallocatedFlag(String reallocatedFlag) {
		this.reallocatedFlag = reallocatedFlag;
	}


}

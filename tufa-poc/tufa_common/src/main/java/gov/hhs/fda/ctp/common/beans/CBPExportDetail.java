package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

public class CBPExportDetail
{
	
	private String consigneeEIN;
	private String consigneeName;
	private String importerName;
	private String importerEIN;
	private String htsCd;
	private Date entrySummDate;
	private Date entryDate;
	private String secondarydateflag;
	private String entryNo;
	private String uom1;
	private Double qty1;
	private int lineNo;
	private Double estimatedTax;
	private String excludedFlag;
    private String reallocatedFlag;
    private Long fiscalYr;
    private String fiscalQtr;
    private String tobaccoClass;
    private int entryTypeCd;
    private String missingFlag;
    
    public int getEntryTypeCd() {
		return entryTypeCd;
	}
	public void setEntryTypeCd(int entryTypeCd) {
		this.entryTypeCd = entryTypeCd;
	}
	public String getTobaccoClass() {
		return tobaccoClass;
	}
	/**
	 * @param tobaccoClass the tobaccoClass to set
	 */
	public void setTobaccoClass(String tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}
	
    public String getConsigneeEIN() {
		return consigneeEIN;
	}

	public void setConsigneeEIN(String consigneeEIN) {
		this.consigneeEIN = consigneeEIN;
	}

	public String getImporterEIN() {
		return importerEIN;
	}

	public void setImporterEIN(String importerEIN) {
		this.importerEIN = importerEIN;
	}

	public String getConsigneeName() {
		return consigneeName;
	}
	
	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}
	
	public String getImporterName() {
		return importerName;
	}

	public void setImporterName(String importerName) {
		this.importerName = importerName;
	}

	public String getEntryNo() {
		return entryNo;
	}

	public void setEntryNo(String entryNo) {
		this.entryNo = entryNo;
	}

	public int getLineNo() {
		return lineNo;
	}

	public void setLineNo(int lineNo) {
		this.lineNo = lineNo;
	}

	public Date getEntrySummDate() {
		return entrySummDate;
	}

	public void setEntrySummDate(Date entrySummDate) {
		this.entrySummDate = entrySummDate;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public String getUom1() {
		return uom1;
	}

	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}

	public String getHtsCd() {
		return htsCd;
	}

	public void setHtsCd(String htsCd) {
		this.htsCd = htsCd;
	}

	public Double getQty1() {
		return qty1;
	}

	public void setQty1(Double qty1) {
		this.qty1 = qty1;
	}

	public Double getEstimatedTax() {
		return estimatedTax;
	}

	public void setEstimatedTax(Double estimatedTax) {
		this.estimatedTax = estimatedTax;
	}
   
	public Long getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getFiscalQtr() {
		return fiscalQtr;
	}

	public void setFiscalQtr(String fiscalQtr) {
		this.fiscalQtr = fiscalQtr;
	}
	
    
	public String getExcludedFlag() {
		return excludedFlag;
	}

	public void setExcludedFlag(String excludedFlag) {
		this.excludedFlag = excludedFlag;
	}

	public String getReallocatedFlag() {
		return reallocatedFlag;
	}

	public void setReallocatedFlag(String reallocatedFlag) {
		this.reallocatedFlag = reallocatedFlag;
	}
	public String getSecondarydate() {
		return secondarydateflag;
	}
	public void setSecondarydate(String secondarydate) {
		this.secondarydateflag = secondarydate;
	}
	public String getMissingFlag() {
		return missingFlag;
	}
	public void setMissingFlag(String missingFlag) {
		this.missingFlag = missingFlag;
	}
	
	
	
}
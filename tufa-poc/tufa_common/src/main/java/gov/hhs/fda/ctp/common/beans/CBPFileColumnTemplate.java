package gov.hhs.fda.ctp.common.beans;

import java.util.HashSet;
import java.util.Set;

public class CBPFileColumnTemplate {

	private Integer columnCount;
	private String entrySummaryNumber;
	private String entrySummaryDate;
	private String entryDate;
	private String htsNumber;
	private String lineTariffqty1;
	private String importerNumber;
	private String importerName;
	private String ultimateConsigneeNumber;
	private String ultimateConsigneeName;
	private String htsDesciption;
	private String lineTariffuom1;
	private String lineNumber;
	private String entryTypeCode;
	private String entryTypeCodeDesciption;
	private String estimatedtax;
	private String totalAscertainedTax;
	private String lineTariffGoodsValueAmount;
	private String irTaxAmount;
	private String fiscalYear;
	private String createdDate;
	private String createdBy;
	private String importDate;
			
	private TTBFileColumnFormat colFormat;
	
	
	public String getEntrySummaryNumber() {
		return entrySummaryNumber;
	}

	public void setEntrySummaryNumber(String entrySummaryNumber) {
		this.entrySummaryNumber = entrySummaryNumber;
	}

	public String getEntrySummaryDate() {
		return entrySummaryDate;
	}

	public void setEntrySummaryDate(String entrySummaryDate) {
		this.entrySummaryDate = entrySummaryDate;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getHtsNumber() {
		return htsNumber;
	}

	public void setHtsNumber(String htsNumber) {
		this.htsNumber = htsNumber;
	}

	public String getLineTariffqty1() {
		return lineTariffqty1;
	}

	public void setLineTariffqty1(String lineTariffqty1) {
		this.lineTariffqty1 = lineTariffqty1;
	}

	public String getImporterNumber() {
		return importerNumber;
	}

	public void setImporterNumber(String importerNumber) {
		this.importerNumber = importerNumber;
	}

	public String getImporterName() {
		return importerName;
	}

	public void setImporterName(String importerName) {
		this.importerName = importerName;
	}

	public String getUltimateConsigneeNumber() {
		return ultimateConsigneeNumber;
	}

	public void setUltimateConsigneeNumber(String ultimateConsigneeNumber) {
		this.ultimateConsigneeNumber = ultimateConsigneeNumber;
	}

	public String getUltimateConsigneeName() {
		return ultimateConsigneeName;
	}

	public void setUltimateConsigneeName(String ultimateConsigneeName) {
		this.ultimateConsigneeName = ultimateConsigneeName;
	}

	public String getHtsDesciption() {
		return htsDesciption;
	}

	public void setHtsDesciption(String htsDesciption) {
		this.htsDesciption = htsDesciption;
	}

	public String getLineTariffuom1() {
		return lineTariffuom1;
	}

	public void setLineTariffuom1(String lineTariffuom1) {
		this.lineTariffuom1 = lineTariffuom1;
	}

	public String getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getEntryTypeCode() {
		return entryTypeCode;
	}

	public void setEntryTypeCode(String entryTypeCode) {
		this.entryTypeCode = entryTypeCode;
	}

	public String getEntryTypeCodeDesciption() {
		return entryTypeCodeDesciption;
	}

	public void setEntryTypeCodeDesciption(String entryTypeCodeDesciption) {
		this.entryTypeCodeDesciption = entryTypeCodeDesciption;
	}

	public String getEstimatedtax() {
		return estimatedtax;
	}

	public void setEstimatedtax(String estimatedtax) {
		this.estimatedtax = estimatedtax;
	}

	
	
	
	public String getTotalAscertainedTax() {
		return totalAscertainedTax;
	}

	public void setTotalAscertainedTax(String totalAscertainedTax) {
		this.totalAscertainedTax = totalAscertainedTax;
	}

	public String getLineTariffGoodsValueAmount() {
		return lineTariffGoodsValueAmount;
	}

	public void setLineTariffGoodsValueAmount(String lineTariffGoodsValueAmount) {
		this.lineTariffGoodsValueAmount = lineTariffGoodsValueAmount;
	}

	public String getirTaxAmount() {
		return irTaxAmount;
	}

	public void setirTaxAmount(String irTaxAmount) {
		this.irTaxAmount = irTaxAmount;
	}

	public String getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
	


	private Set<String> colHeadings;

	public Integer getColumnCount() {
		return columnCount;
	}

	public void setColumnCount(Integer columnCount) {
		this.columnCount = columnCount;
	}



	public TTBFileColumnFormat getColFormat() {
		return colFormat;
	}

	public void setColFormat(TTBFileColumnFormat colFormat) {
		this.colFormat = colFormat;
	}


	public Set<String> getColHeadings() {
		return colHeadings;
	}

	public void setColHeadings(Set<String> colHeadings) {
		this.colHeadings = colHeadings;
	}


	@Override
	public boolean equals(Object o) {

		if (o == this) {
			return true;
		}

		if (!(o instanceof CBPFileColumnTemplate)) {
			return false;
		}

		CBPFileColumnTemplate other = (CBPFileColumnTemplate) o;

		return     other.ultimateConsigneeName.equalsIgnoreCase(this.ultimateConsigneeName)
				&& other.ultimateConsigneeNumber.equalsIgnoreCase(this.ultimateConsigneeNumber)
				&& other.importerName.equalsIgnoreCase(this.importerName)
				&& other.importerNumber.equalsIgnoreCase(this.importerNumber)
				&& other.entrySummaryNumber.equalsIgnoreCase(this.entrySummaryNumber)
				&& other.entrySummaryDate.equalsIgnoreCase(this.entrySummaryDate)
				&& other.entryDate.equalsIgnoreCase(this.entryDate)
				&& other.entryTypeCode.equalsIgnoreCase(this.entryTypeCode)
				&& other.entryTypeCodeDesciption.equalsIgnoreCase(this.entryTypeCodeDesciption)
				&& other.estimatedtax.equalsIgnoreCase(this.estimatedtax)
				&& other.createdBy.equalsIgnoreCase(this.createdBy)
				&& other.createdDate.equalsIgnoreCase(this.createdDate) 
				&& other.htsDesciption.equalsIgnoreCase(this.htsDesciption)
				&& other.htsNumber.equalsIgnoreCase(this.htsNumber)
				&& other.lineTariffqty1.equalsIgnoreCase(this.lineTariffqty1)
				&& other.lineTariffuom1.equalsIgnoreCase(this.lineTariffuom1)
				&& other.totalAscertainedTax.equalsIgnoreCase(this.totalAscertainedTax)
				&& other.lineNumber.equalsIgnoreCase(this.lineNumber)
				&& other.lineTariffGoodsValueAmount.equalsIgnoreCase(this.lineTariffGoodsValueAmount)
				&& other.irTaxAmount.equalsIgnoreCase(this.irTaxAmount)
		        && other.fiscalYear.equalsIgnoreCase(this.fiscalYear)
				&& other.fiscalYear.equalsIgnoreCase(this.importDate);
	}

	public Set<String> columnHeadings() {

		this.colHeadings = new HashSet<>();

		if ((ultimateConsigneeName != null))
			this.colHeadings.add(ultimateConsigneeName.trim().toUpperCase());
		if (ultimateConsigneeNumber != null)
			this.colHeadings.add(ultimateConsigneeNumber.trim().toUpperCase());
		if (importerName != null)
			this.colHeadings.add(importerName.trim().toUpperCase());
		if (importerNumber != null)
			this.colHeadings.add(importerNumber.trim().toUpperCase());
		if (entrySummaryNumber != null)
			this.colHeadings.add(entrySummaryNumber.trim().toUpperCase());
		if (entrySummaryDate != null)
			this.colHeadings.add(entrySummaryDate.trim().toUpperCase());
		if (entryDate != null)
			this.colHeadings.add(entryDate.trim().toUpperCase());
		if (entryTypeCode != null)
			this.colHeadings.add(entryTypeCode.trim().toUpperCase());
		if (entryTypeCodeDesciption != null)
			this.colHeadings.add(entryTypeCodeDesciption.trim().toUpperCase());
		if (estimatedtax != null)
			this.colHeadings.add(estimatedtax.trim().toUpperCase());
		if (createdBy != null)
			this.colHeadings.add(createdBy.trim().toUpperCase());
		if (htsDesciption != null)
			this.colHeadings.add(htsDesciption.trim().toUpperCase());
		if (htsNumber != null)
			this.colHeadings.add(htsNumber.trim().toUpperCase());
		if (lineTariffqty1 != null)
			this.colHeadings.add(lineTariffqty1.trim().toUpperCase());
		if (lineTariffuom1 != null)
			this.colHeadings.add(lineTariffuom1.trim().toUpperCase());
		if (totalAscertainedTax != null)
			this.colHeadings.add(totalAscertainedTax.trim().toUpperCase());
		if (lineNumber != null)
			this.colHeadings.add(lineNumber.trim().toUpperCase());
		if (lineTariffGoodsValueAmount != null)
			this.colHeadings.add(lineTariffGoodsValueAmount.trim().toUpperCase());
		if (irTaxAmount != null)
			this.colHeadings.add(irTaxAmount.trim().toUpperCase());
		if (createdDate != null)
			this.colHeadings.add(createdDate.trim().toUpperCase());
		if (fiscalYear != null)
			this.colHeadings.add(fiscalYear.trim().toUpperCase());
		if (importDate != null)
			this.colHeadings.add(importDate.trim().toUpperCase());

		return this.colHeadings;

	}

	public boolean hasColHeading(String heading) {
		if (this.colHeadings != null && this.colHeadings.size() > 0)
			return this.colHeadings.contains(heading);

		return false;
	}
	public String getImportDate() {
		return importDate;
	}

	public void setImportDate(String importDate) {
		this.importDate = importDate;
	}

	
}

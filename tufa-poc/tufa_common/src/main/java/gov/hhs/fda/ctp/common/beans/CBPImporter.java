/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author tgunter
 *
 */
public class CBPImporter extends BaseTufaBean {
	
	private String ein;
	private String tufaLegalNm;
	private String fiscalYr;
	private String associationTypeCd;
	private Long cbpImporterId;
	private String importerNm;
	private String impInTufa;
	private String consgInTufa;
	 
	private Map<String,CBPImporterTaxDeltas> consigneeDeltas;
	private Map<String,CBPImporterTaxDeltas> importerDeltas;


	boolean providedAnswer;
	
//	private Long   cbpImporterId;
//	
//	private Double totalTaxes;
//	private String importerNm;
	private String importerEIN;
//	private Long   fiscalYear;
//    private String associationTypeCd;
//    private String tobaccoType;
//    private String consigneeExistsFlag;
    private String consigneeEIN;
//    private String includeFlag;
    private String consigneeNm;
//	private String existsInTufaFlag;
	private Set<String> otherTobaccoTypesReported;

	
    
    
//    public String getExistsInTufaFlag() {
//		return existsInTufaFlag;
//	}
//	public void setExistsInTufaFlag(String existsInTufaFlag) {
//		this.existsInTufaFlag = existsInTufaFlag;
//	}
//	public String getConsigneeNm() {
//		return consigneeNm;
//	}
//	public void setConsigneeNm(String consigneeNm) {
//		this.consigneeNm = consigneeNm;
//	}
//    
//	//private List<CBPEntry> cbpEntries;
//	/**
//	 * @return the cbpImporterId
//	 */
//	public Long getCbpImporterId() {
//		return cbpImporterId;
//	}
//	/**
//	 * @param cbpImporterId the cbpImporterId to set
//	 */
//	public void setCbpImporterId(Long cbpImporterId) {
//		this.cbpImporterId = cbpImporterId;
//	}	
//	/**
//	 * @return the totalTaxes
//	 */
//	public Double getTotalTaxes() {
//		return totalTaxes;
//	}
//	/**
//	 * @param totalTaxes the totalTaxes to set
//	 */
//	public void setTotalTaxes(Double totalTaxes) {
//		this.totalTaxes = totalTaxes;
//	}
//	
//	/**
//	 * @return the fiscalYear
//	 */
//	public Long getFiscalYear() {
//		return fiscalYear;
//	}
//	/**
//	 * @param fiscalYear the fiscalYear to set
//	 */
//	public void setFiscalYear(Long fiscalYear) {
//		this.fiscalYear = fiscalYear;
//	}
//	/**
//	 * @return the associationTypeCd
//	 */
//	public String getAssociationTypeCd() {
//		return associationTypeCd;
//	}
//	/**
//	 * @param associationTypeCd the associationTypeCd to set
//	 */
//	public void setAssociationTypeCd(String associationTypeCd) {
//		this.associationTypeCd = associationTypeCd;
//	}
//	/**
//	 * @return the tobaccoType
//	 */
//	public String getTobaccoType() {
//		return tobaccoType;
//	}
//	/**
//	 * @param tobaccoType the tobaccoType to set
//	 */
//	public void setTobaccoType(String tobaccoType) {
//		this.tobaccoType = tobaccoType;
//	}
//	/**
//	 * @return the consigneeExistsFlag
//	 */
//	public String getConsigneeExistsFlag() {
//		return consigneeExistsFlag;
//	}
//	/**
//	 * @param consigneeExistsFlag the consigneeExistsFlag to set
//	 */
//	public void setConsigneeExistsFlag(String consigneeExistsFlag) {
//		this.consigneeExistsFlag = consigneeExistsFlag;
//	}
//	/*public List<CBPEntry> getCbpEntries() {
//		return cbpEntries;
//	}
//	public void setCbpEntries(List<CBPEntry> cbpEntries) {
//		this.cbpEntries = cbpEntries;
//	}*/
//	public String getImporterEIN() {
//		return importerEIN;
//	}
//	public void setImporterEIN(String importerEIN) {
//		this.importerEIN = importerEIN;
//	}
//	public String getConsigneeEIN() {
//		return consigneeEIN;
//	}
//	public void setConsigneeEIN(String consigneeEIN) {
//		this.consigneeEIN = consigneeEIN;
//	}
//	public String getImporterNm() {
//		return importerNm;
//	}	
//	public void setImporterNm(String importerNm) {
//		this.importerNm = importerNm;
//	}
//	public Set<String> getOtherTobaccoTypesReported() {
//		return otherTobaccoTypesReported;
//	}
//	public void setOtherTobaccoTypesReported(Set<String> otherTobaccoTypesReported) {
//		this.otherTobaccoTypesReported = otherTobaccoTypesReported;
//	}
//	public String getIncludeFlag() {
//		return includeFlag;
//	}
//	public void setIncludeFlag(String includeFlag) {
//		this.includeFlag = includeFlag;
//	}    
    public boolean isProvidedAnswer() {
		return providedAnswer;
	}
	
	public void setProvidedAnswer(boolean providedAnswer) {
		this.providedAnswer = providedAnswer;
	}
	public String getImporterNm() {
		return importerNm;
	}
	public void setImporterNm(String importerNm) {
		this.importerNm = importerNm;
	}
	public String getEin() {
		return ein;
	}
	public void setEin(String ein) {
		this.ein = ein;
	}
	public String getTufaLegalNm() {
		return tufaLegalNm;
	}
	public void setTufaLegalNm(String tufaLegalNm) {
		this.tufaLegalNm = tufaLegalNm;
	}
	public String getFiscalYr() {
		return fiscalYr;
	}
	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}
	public String getAssociationTypeCd() {
		return associationTypeCd;
	}
	public void setAssociationTypeCd(String associationTypeCd) {
		this.associationTypeCd = associationTypeCd;
	}
	public Long getCbpImporterId() {
		return cbpImporterId;
	}
	public void setCbpImporterId(Long cbpImporterId) {
		this.cbpImporterId = cbpImporterId;
	}
	public String getImpInTufa() {
		return impInTufa;
	}
	public void setImpInTufa(String impInTufa) {
		this.impInTufa = impInTufa;
	}
	public String getConsgInTufa() {
		return consgInTufa;
	}
	public void setConsgInTufa(String consgInTufa) {
		this.consgInTufa = consgInTufa;
	}

	public String getImporterEIN() {
		return importerEIN;
	}

	public void setImporterEIN(String importerEIN) {
		this.importerEIN = importerEIN;
	}

	public String getConsigneeEIN() {
		return consigneeEIN;
	}

	public void setConsigneeEIN(String consigneeEIN) {
		this.consigneeEIN = consigneeEIN;
	}

	public Set<String> getOtherTobaccoTypesReported() {
		return otherTobaccoTypesReported;
	}

	public void setOtherTobaccoTypesReported(Set<String> otherTobaccoTypesReported) {
		this.otherTobaccoTypesReported = otherTobaccoTypesReported;
	}

	public String getConsigneeNm() {
		return consigneeNm;
	}

	public void setConsigneeNm(String consigneeNm) {
		this.consigneeNm = consigneeNm;
	}

	public Map<String,CBPImporterTaxDeltas> getConsigneeDeltas() {
		return consigneeDeltas;
	}

	public void setConsigneeDeltas(Map<String,CBPImporterTaxDeltas> consigneeDeltas) {
		this.consigneeDeltas = consigneeDeltas;
	}

	public Map<String,CBPImporterTaxDeltas> getImporterDeltas() {
		return importerDeltas;
	}

	public void setImporterDeltas(Map<String,CBPImporterTaxDeltas> importerDeltas) {
		this.importerDeltas = importerDeltas;
	}

	
}

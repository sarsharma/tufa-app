package gov.hhs.fda.ctp.common.beans;

public class CBPImporterTaxDeltas {

	private String ein;
	private String tufaLegalNm;
	private String fiscalYr;
	private String associationTypeCd;
	private String importerNm;
	private String impInTufa;
	private String consgInTufa;

	/**** CURRENT DELTA ****/
	private String quarter;
	private Double taxesCigarsTufa;
	private Double taxesCigsTufa;
	private Double taxesChewTufa;
	private Double taxesSnuffTufa;
	private Double taxesPipeTufa;
	private Double taxesRYOTufa;
	private Double taxesUnknownTufa;

	/**** New Delta *****/
	private Double deltaCigars;
	private Double deltaCigarettes;
	private Double deltaChew;
	private Double deltaSnuff;
	private Double deltaPipe;
	private Double deltaRYO;
	private Double deltaUnknown;

	public Double getTaxesCigarsTufa() {
		return taxesCigarsTufa;
	}

	public void setTaxesCigarsTufa(Double taxesCigarsTufa) {
		this.taxesCigarsTufa = taxesCigarsTufa;
	}

	public Double getTaxesCigsTufa() {
		return taxesCigsTufa;
	}

	public void setTaxesCigsTufa(Double taxesCigsTufa) {
		this.taxesCigsTufa = taxesCigsTufa;
	}

	public Double getTaxesChewTufa() {
		return taxesChewTufa;
	}

	public void setTaxesChewTufa(Double taxesChewTufa) {
		this.taxesChewTufa = taxesChewTufa;
	}

	public String getTufaLegalNm() {
		return tufaLegalNm;
	}

	public void setTufaLegalNm(String tufaLegalNm) {
		this.tufaLegalNm = tufaLegalNm;
	}

	public String getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getAssociationTypeCd() {
		return associationTypeCd;
	}

	public void setAssociationTypeCd(String associationTypeCd) {
		this.associationTypeCd = associationTypeCd;
	}

	public String getImporterNm() {
		return importerNm;
	}

	public void setImporterNm(String importerNm) {
		this.importerNm = importerNm;
	}

	public String getImpInTufa() {
		return impInTufa;
	}

	public void setImpInTufa(String impInTufa) {
		this.impInTufa = impInTufa;
	}

	public String getConsgInTufa() {
		return consgInTufa;
	}

	public void setConsgInTufa(String consgInTufa) {
		this.consgInTufa = consgInTufa;
	}

	public Double getTaxesSnuffTufa() {
		return taxesSnuffTufa;
	}

	public void setTaxesSnuffTufa(Double taxesSnuffTufa) {
		this.taxesSnuffTufa = taxesSnuffTufa;
	}

	public Double getTaxesPipeTufa() {
		return taxesPipeTufa;
	}

	public void setTaxesPipeTufa(Double taxesPipeTufa) {
		this.taxesPipeTufa = taxesPipeTufa;
	}

	public Double getTaxesRYOTufa() {
		return taxesRYOTufa;
	}

	public void setTaxesRYOTufa(Double taxesRYOTufa) {
		this.taxesRYOTufa = taxesRYOTufa;
	}

	public Double getDeltaCigars() {
		return deltaCigars;
	}

	public void setDeltaCigars(Double deltaCigars) {
		this.deltaCigars = deltaCigars;
	}

	public Double getDeltaCigarettes() {
		return deltaCigarettes;
	}

	public void setDeltaCigarettes(Double deltaCigarettes) {
		this.deltaCigarettes = deltaCigarettes;
	}

	public Double getDeltaChew() {
		return deltaChew;
	}

	public void setDeltaChew(Double deltaChew) {
		this.deltaChew = deltaChew;
	}

	public Double getDeltaSnuff() {
		return deltaSnuff;
	}

	public void setDeltaSnuff(Double deltaSnuff) {
		this.deltaSnuff = deltaSnuff;
	}

	public Double getDeltaPipe() {
		return deltaPipe;
	}

	public void setDeltaPipe(Double deltaPipe) {
		this.deltaPipe = deltaPipe;
	}

	public Double getDeltaRYO() {
		return deltaRYO;
	}

	public void setDeltaRYO(Double deltaRYO) {
		this.deltaRYO = deltaRYO;
	}

	public String getEin() {
		return ein;
	}

	public void setEin(String ein) {
		this.ein = ein;
	}

	public Double getTaxesUnknownTufa() {
		return taxesUnknownTufa;
	}

	public void setTaxesUnknownTufa(Double taxesUnknownTufa) {
		this.taxesUnknownTufa = taxesUnknownTufa;
	}

	public Double getDeltaUnknown() {
		return deltaUnknown;
	}

	public void setDeltaUnknown(Double deltaUnknown) {
		this.deltaUnknown = deltaUnknown;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public boolean checkCurrentDeltaEmpty() {

		if (this.taxesCigarsTufa == null && this.taxesChewTufa == null && this.taxesCigsTufa == null
				&& this.taxesPipeTufa == null && this.taxesRYOTufa == null && this.taxesSnuffTufa == null
				&& this.taxesUnknownTufa == null)
			return true;
		return false;

	}

	public boolean checkNewDeltaEmpty() {
		if (this.deltaCigars == null && this.deltaChew == null && this.deltaCigarettes == null && this.deltaPipe == null
				&& this.deltaRYO == null && this.deltaSnuff == null && this.deltaUnknown == null)
			return true;

		return false;

	}

}

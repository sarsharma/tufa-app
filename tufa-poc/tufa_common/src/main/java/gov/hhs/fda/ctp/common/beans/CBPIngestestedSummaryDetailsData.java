package gov.hhs.fda.ctp.common.beans;

public class CBPIngestestedSummaryDetailsData {
	
	private String cbpSummaryTaxAmount;
	private String cbpDetailTaxAmount;
	private String cbpDetailVolume;
	private String tufaTaxRate;
	private String filetype;
	
	public String getTufaTaxRate() {
		return tufaTaxRate;
	}

	public void setTufaTaxRate(String tufaTaxRate) {
		this.tufaTaxRate = tufaTaxRate;
	}

	public String getCbpSummaryTaxAmount() {
		return cbpSummaryTaxAmount;
	}

	public void setCbpSummaryTaxAmount(String cbpSummaryTaxAmount) {
		this.cbpSummaryTaxAmount = cbpSummaryTaxAmount;
	}

	public String getCbpDetailTaxAmount() {
		return cbpDetailTaxAmount;
	}

	public void setCbpDetailTaxAmount(String cbpDetailTaxAmount) {
		this.cbpDetailTaxAmount = cbpDetailTaxAmount;
	}

	public String getCbpDetailVolume() {
		return cbpDetailVolume;
	}

	public void setCbpDetailVolume(String cbpDetailVolume) {
		this.cbpDetailVolume = cbpDetailVolume;
	}

	public String getFiletype() {
		return filetype;
	}

	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}


	
}
package gov.hhs.fda.ctp.common.beans;

public class CBPLineEntryUpdateInfo {
    private Long cbEntryId;
    private String reallocateFlag;
    private String excludeFlag;
    private String assignedMonth;
    private Long assignedQuarter; 
    private Long assignedYear;
    private String missingFlag;
    private String resetMissingFlag;
	public Long getCbEntryId() {
		return cbEntryId;
	}
	public void setCbEntryId(Long cbEntryId) {
		this.cbEntryId = cbEntryId;
	}
	public String getReallocateFlag() {
		return reallocateFlag;
	}
	public void setReallocateFlag(String reallocateFlag) {
		this.reallocateFlag = reallocateFlag;
	}
	public String getExcludeFlag() {
		return excludeFlag;
	}
	public void setExcludeFlag(String excludeFlag) {
		this.excludeFlag = excludeFlag;
	}
	public String getAssignedMonth() {
		return assignedMonth;
	}
	public void setAssignedMonth(String assignedMonth) {
		this.assignedMonth = assignedMonth;
	}
	public Long getAssignedYear() {
		return assignedYear;
	}
	public void setAssignedYear(Long assignedYear) {
		this.assignedYear = assignedYear;
	}
	public Long getAssignedQuarter() {
		return assignedQuarter;
	}
	public void setAssignedQuarter(Long assignedQuarter) {
		this.assignedQuarter = assignedQuarter;
	}
	public String getResetMissingFlag() {
		return resetMissingFlag;
	}
	public void setResetMissingFlag(String resetMissingFlag) {
		this.resetMissingFlag = resetMissingFlag;
	}
	public String getMissingFlag() {
		return missingFlag;
	}
	public void setMissingFlag(String missingFlag) {
		this.missingFlag = missingFlag;
	}  
	
	
}

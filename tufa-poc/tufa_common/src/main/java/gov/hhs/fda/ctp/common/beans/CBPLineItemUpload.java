package gov.hhs.fda.ctp.common.beans;

import gov.hhs.fda.ctp.common.validators.EntryDateMatch;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

//@EntryDateMatch
public class CBPLineItemUpload extends DocUpload{
	@NotNull(message = "Entry Summary Number is missing")
	@Size(max=12)
	private String entrySummaryNumber;
	
	@Size(max=8)
	private String entrySummaryDate;
	
	@Size(max=8)
	private String entryDate;
	
	@Digits(integer=10, fraction=0)
	private Long htsNumber;
	
	@Digits(integer=8, fraction=2, message="Line Tarrif Qty1")	
	private Double lineTarrifQty1;
	
	
	@NotNull(message = "Importer Number is missing.")
	@Size(min=1, message = "Importer Number is missing.")
	@Pattern(regexp="^([0-9]*-[0-9]*)+$", message="Invalid Importer Number")
	private String importerNumber;
		
	@NotNull(message="Importer name is missing.")
	@Size(min=1, max=80)
	private String importerName;
		
	@Pattern(regexp="^([0-9]*-[0-9]*)+$", message="Invalid ultimate Consignee Number")
	private String ultimateConsigneeNumber;

	@Size(min=1, max=80)
	private String ultimateConsigneeName;
	
	private String htsDescription;
		
	@Size(max=2)
	private String lineTarrifUOM1;
	
	@Digits(integer=3, fraction=0)
	private long lineNumber;
	
	@Size(max=100)
	private String entryTypeCode;

	@Digits(integer=8, fraction=2, message="Line Tariff Goods Value Amount")	
	private Double lineTariffGoodsValueAmount;
	
	@NotNull(message = "IR Tax Amount is missing")
	@Digits(integer=9, fraction=2, message="IR Tax Amount")	
	private Double irTaxAmount;
	
	@Size(max=8)
	private String importDate;
	
	
	// generated values, not to be validated.
	private String errors;
	private Long fiscalYr;
	
	public String getEntrySummaryNumber() {
		return entrySummaryNumber;
	}
	public void setEntrySummaryNumber(String entrySummaryNumber) {
		this.entrySummaryNumber = entrySummaryNumber;
	}
	public String getEntrySummaryDate() {
		return entrySummaryDate;
	}
	public void setEntrySummaryDate(String entrySummaryDate) {
		this.entrySummaryDate = entrySummaryDate;
	}
	public String getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	public Long getHtsNumber() {
		return htsNumber;
	}
	public void setHtsNumber(Long htsNumber) {
		this.htsNumber = htsNumber;
	}
	public String getImporterNumber() {
		return importerNumber;
	}
	public void setImporterNumber(String importerNumber) {
		this.importerNumber = importerNumber;
	}
	public String getImporterName() {
		return importerName;
	}
	public void setImporterName(String importerName) {
		this.importerName = importerName;
	}
	public String getUltimateConsigneeNumber() {
		return ultimateConsigneeNumber;
	}
	public void setUltimateConsigneeNumber(String ultimateConsigneeNumber) {
		this.ultimateConsigneeNumber = ultimateConsigneeNumber;
	}
	public String getUltimateConsigneeName() {
		return ultimateConsigneeName;
	}
	public void setUltimateConsigneeName(String ultimateConsigneeName) {
		this.ultimateConsigneeName = ultimateConsigneeName;
	}
	public String getHtsDescription() {
		return htsDescription;
	}
	public void setHtsDescription(String htsDescription) {
		this.htsDescription = htsDescription;
	}
	public String getLineTarrifUOM1() {
		return lineTarrifUOM1;
	}
	public void setLineTarrifUOM1(String lineTarrifUOM1) {
		this.lineTarrifUOM1 = lineTarrifUOM1;
	}
	public String getEntryTypeCode() {
		return entryTypeCode;
	}
	public void setEntryTypeCode(String entryTypeCode) {
		this.entryTypeCode = entryTypeCode;
	}
	public Double getLineTariffGoodsValueAmount() {
		return lineTariffGoodsValueAmount;
	}
	public void setLineTariffGoodsValueAmount(Double lineTariffGoodsValueAmount) {
		this.lineTariffGoodsValueAmount = lineTariffGoodsValueAmount;
	}
	public String getErrors() {
		return errors;
	}
	public void setErrors(String errors) {
		this.errors = errors;
	}
	public Long getFiscalYr() {
		return fiscalYr;
	}
	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}
	public long getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(long lineNumber) {
		this.lineNumber = lineNumber;
	}
	
	public String getImportDate() {
		return importDate;
	}
	public void setImportDate(String importDate) {
		this.importDate = importDate;
	}
	public Double getLineTarrifQty1() {
		return lineTarrifQty1;
	}
	public void setLineTarrifQty1(Double lineTarrifQty1) {
		this.lineTarrifQty1 = lineTarrifQty1;
	}
	public Double getIrTaxAmount() {
		return irTaxAmount;
	}
	public void setIrTaxAmount(Double irTaxAmount) {
		this.irTaxAmount = irTaxAmount;
	}
	
}

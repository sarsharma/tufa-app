package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

public class CBPMetrics {
	private String fiscalYr;
	private Integer summaryFileEntrySummaryNoCount;
	private Integer detailFileEntrySummaryNoCount;
	private Integer matchedEntrySummaryNoCount;
	private Integer unmatchedEntrySummayNoCount;
	private Integer uniqueSummaryFileEntrySummaryNo;
	private Integer uniqueDetailFileEntrySummaryNo;
	private Double detailFileTotalTaxAmount;
	private Integer mixedEntrySummaryNoCount;
	private String accepted;
	
	private Date createdDt;
	private String createdBy;
	private Date modifiedDt;
	private String modifiedBy;
	
	/**
	 * @return the fiscalYr
	 */
	public String getFiscalYr() {
		return fiscalYr;
	}
	/**
	 * @param fiscalYr the fiscalYr to set
	 */
	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}
	/**
	 * @return the summaryFileEntrySummaryNoCount
	 */
	public Integer getSummaryFileEntrySummaryNoCount() {
		return summaryFileEntrySummaryNoCount;
	}
	/**
	 * @param summaryFileEntrySummaryNoCount the summaryFileEntrySummaryNoCount to set
	 */
	public void setSummaryFileEntrySummaryNoCount(Integer summaryFileEntrySummaryNoCount) {
		this.summaryFileEntrySummaryNoCount = summaryFileEntrySummaryNoCount;
	}
	/**
	 * @return the detailFileEntrySummaryNoCount
	 */
	public Integer getDetailFileEntrySummaryNoCount() {
		return detailFileEntrySummaryNoCount;
	}
	/**
	 * @param detailFileEntrySummaryNoCount the detailFileEntrySummaryNoCount to set
	 */
	public void setDetailFileEntrySummaryNoCount(Integer detailFileEntrySummaryNoCount) {
		this.detailFileEntrySummaryNoCount = detailFileEntrySummaryNoCount;
	}
	/**
	 * @return the matchedEntrySummaryNoCount
	 */
	public Integer getMatchedEntrySummaryNoCount() {
		return matchedEntrySummaryNoCount;
	}
	/**
	 * @param matchedEntrySummaryNoCount the matchedEntrySummaryNoCount to set
	 */
	public void setMatchedEntrySummaryNoCount(Integer matchedEntrySummaryNoCount) {
		this.matchedEntrySummaryNoCount = matchedEntrySummaryNoCount;
	}
	/**
	 * @return the unmatchedEntrySummayNoCount
	 */
	public Integer getUnmatchedEntrySummayNoCount() {
		return unmatchedEntrySummayNoCount;
	}
	/**
	 * @param unmatchedEntrySummayNoCount the unmatchedEntrySummayNoCount to set
	 */
	public void setUnmatchedEntrySummayNoCount(Integer unmatchedEntrySummayNoCount) {
		this.unmatchedEntrySummayNoCount = unmatchedEntrySummayNoCount;
	}
	/**
	 * @return the uniqueSummaryFileEntrySummaryNo
	 */
	public Integer getUniqueSummaryFileEntrySummaryNo() {
		return uniqueSummaryFileEntrySummaryNo;
	}
	/**
	 * @param uniqueSummaryFileEntrySummaryNo the uniqueSummaryFileEntrySummaryNo to set
	 */
	public void setUniqueSummaryFileEntrySummaryNo(Integer uniqueSummaryFileEntrySummaryNo) {
		this.uniqueSummaryFileEntrySummaryNo = uniqueSummaryFileEntrySummaryNo;
	}
	/**
	 * @return the uniqueDetailFileEntrySummaryNo
	 */
	public Integer getUniqueDetailFileEntrySummaryNo() {
		return uniqueDetailFileEntrySummaryNo;
	}
	/**
	 * @param uniqueDetailFileEntrySummaryNo the uniqueDetailFileEntrySummaryNo to set
	 */
	public void setUniqueDetailFileEntrySummaryNo(Integer uniqueDetailFileEntrySummaryNo) {
		this.uniqueDetailFileEntrySummaryNo = uniqueDetailFileEntrySummaryNo;
	}
	/**
	 * @return the detailFileTotalTaxAmount
	 */
	public Double getDetailFileTotalTaxAmount() {
		return detailFileTotalTaxAmount;
	}
	/**
	 * @param detailFileTotalTaxAmount the detailFileTotalTaxAmount to set
	 */
	public void setDetailFileTotalTaxAmount(Double detailFileTotalTaxAmount) {
		this.detailFileTotalTaxAmount = detailFileTotalTaxAmount;
	}
	/**
	 * @return the mixedEntrySummaryNoCount
	 */
	public Integer getMixedEntrySummaryNoCount() {
		return mixedEntrySummaryNoCount;
	}
	/**
	 * @param mixedEntrySummaryNoCount the mixedEntrySummaryNoCount to set
	 */
	public void setMixedEntrySummaryNoCount(Integer mixedEntrySummaryNoCount) {
		this.mixedEntrySummaryNoCount = mixedEntrySummaryNoCount;
	}
	public String getAccepted() {
		return accepted;
	}
	public void setAccepted(String accepted) {
		this.accepted = accepted;
	}
	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}
	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	
}

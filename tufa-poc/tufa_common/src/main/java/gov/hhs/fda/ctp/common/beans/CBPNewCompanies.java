package gov.hhs.fda.ctp.common.beans;

import java.util.Date;
import java.util.Set;

public class CBPNewCompanies {

	
	private String importerName;
	private String importerEIN;
	private Set<String> tobaccoName;
	private String entrydate;
	private Double taxamount;
	
	
	public String getImporterEIN() {
		return importerEIN;
	}

	public void setImporterEIN(String importerEIN) {
		this.importerEIN = importerEIN;
	}

	public String getImporterName() {
		return importerName;
	}

	public void setImporterName(String  string) {
		this.importerName = string;
	}
	
	public String getentrysummddate() {
		return entrydate;
	}

	public void setentrysummdate(String entrydate) {
		this.entrydate = entrydate;
	}
	
	public Double gettaxamount() {
		return taxamount;
	}

	public void settaxamount(Double taxamount) {
		this.taxamount = taxamount;
	}
	
	
	public Set<String> getTobaccoName() {
		return tobaccoName;
	}

	public void setTobaccoName(Set<String> tobaccoName) {
		this.tobaccoName = tobaccoName;
	}
	
	
}

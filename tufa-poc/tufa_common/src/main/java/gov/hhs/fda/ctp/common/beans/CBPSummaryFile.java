/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

/**
 * @author Priti
 *
 */
public class CBPSummaryFile extends BaseTufaBean {

	private Long cbpSummaryId;
	private String ultimateConsigneeNumber;
	private String ultimateConsigneeName;
	private String importerNumber;
	private String importerName;
	private String EntryDate;
	private String entrySummaryDate;
	private String entrySummaryNumber;
	private Double estimatedTax;
	private Double totalAscertainedtax;
	private String entryTypeCodeDescription;
	private Long fiscalYr;
	
	/**
	 * @return the cbpSummaryId
	 */
	public Long getCbpSummaryId() {
		return cbpSummaryId;
	}
	/**
	 * @param cbpSummaryId the cbpSummaryId to set
	 */
	public void setCbpSummaryId(Long cbpSummaryId) {
		this.cbpSummaryId = cbpSummaryId;
	}
	/**
	 * @return the ultimateConsigneeNumber
	 */
	public String getUltimateConsigneeNumber() {
		return ultimateConsigneeNumber;
	}
	/**
	 * @param ultimateConsigneeNumber the ultimateConsigneeNumber to set
	 */
	public void setUltimateConsigneeNumber(String ultimateConsigneeNumber) {
		this.ultimateConsigneeNumber = ultimateConsigneeNumber;
	}
	/**
	 * @return the ultimateConsigneeName
	 */
	public String getUltimateConsigneeName() {
		return ultimateConsigneeName;
	}
	/**
	 * @param ultimateConsigneeName the ultimateConsigneeName to set
	 */
	public void setUltimateConsigneeName(String ultimateConsigneeName) {
		this.ultimateConsigneeName = ultimateConsigneeName;
	}
	/**
	 * @return the importerNumber
	 */
	public String getImporterNumber() {
		return importerNumber;
	}
	/**
	 * @param importerNumber the importerNumber to set
	 */
	public void setImporterNumber(String importerNumber) {
		this.importerNumber = importerNumber;
	}
	/**
	 * @return the importerName
	 */
	public String getImporterName() {
		return importerName;
	}
	/**
	 * @param importerName the importerName to set
	 */
	public void setImporterName(String importerName) {
		this.importerName = importerName;
	}
	/**
	 * @return the entryDate
	 */
	public String getEntryDate() {
		return EntryDate;
	}
	/**
	 * @param entryDate the entryDate to set
	 */
	public void setEntryDate(String entryDate) {
		EntryDate = entryDate;
	}
	/**
	 * @return the entrySummaryDate
	 */
	public String getEntrySummaryDate() {
		return entrySummaryDate;
	}
	/**
	 * @param entrySummaryDate the entrySummaryDate to set
	 */
	public void setEntrySummaryDate(String entrySummaryDate) {
		this.entrySummaryDate = entrySummaryDate;
	}
	/**
	 * @return the entrySummaryNumber
	 */
	public String getEntrySummaryNumber() {
		return entrySummaryNumber;
	}
	/**
	 * @param entrySummaryNumber the entrySummaryNumber to set
	 */
	public void setEntrySummaryNumber(String entrySummaryNumber) {
		this.entrySummaryNumber = entrySummaryNumber;
	}
	/**
	 * @return the estimatedTax
	 */
	public Double getEstimatedTax() {
		return estimatedTax;
	}
	/**
	 * @param estimatedTax the estimatedTax to set
	 */
	public void setEstimatedTax(Double estimatedTax) {
		this.estimatedTax = estimatedTax;
	}
	/**
	 * @return the totalAscertainedtax
	 */
	public Double getTotalAscertainedtax() {
		return totalAscertainedtax;
	}
	/**
	 * @param totalAscertainedtax the totalAscertainedtax to set
	 */
	public void setTotalAscertainedtax(Double totalAscertainedtax) {
		this.totalAscertainedtax = totalAscertainedtax;
	}
	/**
	 * @return the entryTypeCodeDescription
	 */
	public String getEntryTypeCodeDescription() {
		return entryTypeCodeDescription;
	}
	/**
	 * @param entryTypeCodeDescription the entryTypeCodeDescription to set
	 */
	public void setEntryTypeCodeDescription(String entryTypeCodeDescription) {
		this.entryTypeCodeDescription = entryTypeCodeDescription;
	}
	/**
	 * @return the fiscalYr
	 */
	public Long getFiscalYr() {
		return fiscalYr;
	}
	/**
	 * @param fiscalYr the fiscalYr to set
	 */
	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

}

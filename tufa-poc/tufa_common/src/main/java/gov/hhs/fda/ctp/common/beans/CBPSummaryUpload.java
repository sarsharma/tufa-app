package gov.hhs.fda.ctp.common.beans;

import gov.hhs.fda.ctp.common.validators.EntryDateMatch;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

//@EntryDateMatch
public class CBPSummaryUpload extends DocUpload{
	
	@Size(max=12)
	@NotNull(message = "Entry Summary Number is missing.")
	private String entrySummaryNumber;
	
	@NotNull(message = "Importer Number is missing.")
	@Size(min=1, message = "Importer Number is missing.")
	@Pattern(regexp="^([0-9]*-[0-9]*)+$", message="Invalid Importer Number")
	private String importerNumber;
		
	@NotNull(message="Importer name is missing.")
	@Size(min=1, max=80)
	private String importerName;
		
	@Size(max=8)
	private String entryDate;
	
	@Size(max=100)
	private String entryTypeCodeDescription;
	
	@Size(max=8)
	private String entrySummaryDate;
		
	@Pattern(regexp="^([0-9]*-[0-9]*)+$", message="Invalid Consignee Number")
	private String ultimateConsigneeNumber;

	
	@Size(min=1, max=80)
	private String ultimateConsigneeName;
	
	@NotNull(message = "Tax amount is missing.")
	@Digits(integer=9, fraction=2, message="Tax amount exceeds maximum.")	
	private Double estimatedTax;
	
	@NotNull(message = "Total Ascertained Tax Amount is missing.")
	@Digits(integer=9, fraction=2, message="Total Ascertained Tax Amount exceeds maximum.")	
	private Double totalAscertainedtax;
	
	
	public String getentrySummaryNumber() {
		return entrySummaryNumber;
	}

	public void setentrySummaryNumber(String entrySummaryNumber) {
		this.entrySummaryNumber = entrySummaryNumber;
	}
	
	
	public String getImporterName() {
		return importerName;
	}

	public void setImporterName(String importerName) {
		this.importerName = importerName;
	}

	public String getImporterNumber() {
		return importerNumber;
	}

	public void setImporterNumber(String importerNumber) {
		this.importerNumber = importerNumber;
	}
	
	public String getultimateConsigneeNumber() {
		return ultimateConsigneeNumber;
	}

	public void setultimateConsigneeNumber(String ultimateconsigneeNumber) {
		this.ultimateConsigneeNumber = ultimateconsigneeNumber;
	}	

	public String getultimateConsigneeName() {
		return ultimateConsigneeName;
	}

	public void setultimateConsigneeName(String ultimateconsigneeName) {
		this.ultimateConsigneeName = ultimateconsigneeName;
	}

	public String getentrySummaryDate() {
		return entrySummaryDate;
	}

	public void setentrySummaryDate(String entrySummaryDate) {
		this.entrySummaryDate = entrySummaryDate;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public Double getEstimatedTax() {
		return estimatedTax;
	}

	public void setEstimatedTax(Double estimatedTax) {
		this.estimatedTax = estimatedTax;
	}
	
	public Double getTotalAscertainedtax() {
		return totalAscertainedtax;
	}

	public void setTotalAscertainedtax(Double totalAscertainedtax) {
		this.totalAscertainedtax = totalAscertainedtax;
	}
	
	public String getentryTypeCodeDescription() {
		return entryTypeCodeDescription;
	}

	public void setentryTypeCodeDescription(String entryTypeCodeDescription) {
		this.entryTypeCodeDescription = entryTypeCodeDescription;
	}

	// the following methods and attributes are for 
	// generated values, not to be validated.
	private String error;
	private Long fiscalYr;

	/**
	 * Get fiscal year.
	 * @return
	 */
	public Long getFiscalYr() {
		return fiscalYr;
	}

	/**
	 * Set fiscal year.
	 * @param fiscalYr
	 */
	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	/**
	 * @return the errors
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setError(String error) {
		this.error = error;
	}
	
}

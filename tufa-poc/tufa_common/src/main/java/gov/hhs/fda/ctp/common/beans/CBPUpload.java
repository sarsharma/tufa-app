package gov.hhs.fda.ctp.common.beans;

import gov.hhs.fda.ctp.common.validators.EntryDateMatch;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

//@EntryDateMatch
public class CBPUpload extends DocUpload{
	@Pattern(regexp="^([0-9]*-[0-9]*)+$", message="Invalid EIN")
	private String consigneeEIN;

	@NotNull(message="Importer name is missing.")
	@Size(min=1, max=80)
	private String importerName;
	
	//@NotNull(message="Consignee name is missing.")
	@Size(min=1, max=80)
	private String consigneeName;

	
	@NotNull(message = "EIN is missing.")
	@Size(min=1, message = "EIN is missing.")
	@Pattern(regexp="^([0-9]*-[0-9]*)+$", message="Invalid EIN")
	private String importerEIN;
	
	@Size(max=12)
	private String entryNo;

	@Digits(integer=3, fraction=0)
	private Long lineNo;
	
	@Size(max=8)
	private String entrySummDate;

	@Size(max=8)
	private String entryDate;
    
	@Size(max=2)
	private String uom1;
	
	@Size(max=10)
	private String htsCd;
	
	@Digits(integer=8, fraction=2)
	private Double qty1;
	
	@NotNull(message = "Tax amount is missing.")
	@Digits(integer=8, fraction=2, message="Tax amount exceeds maximum.")	
	private Double estimatedTax;
	
	@Size(max=4)
	private String entryType;

	public String getConsigneeEIN() {
		return consigneeEIN;
	}

	public void setConsigneeEIN(String consigneeEIN) {
		this.consigneeEIN = consigneeEIN;
	}

	public String getImporterEIN() {
		return importerEIN;
	}

	public void setImporterEIN(String importerEIN) {
		this.importerEIN = importerEIN;
	}

	public String getImporterName() {
		return importerName;
	}

	public void setImporterName(String importerName) {
		this.importerName = importerName;
	}

	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public String getEntryNo() {
		return entryNo;
	}

	public void setEntryNo(String entryNo) {
		this.entryNo = entryNo;
	}

	public Long getLineNo() {
		return lineNo;
	}

	public void setLineNo(Long lineNo) {
		this.lineNo = lineNo;
	}

	public String getEntrySummDate() {
		return entrySummDate;
	}

	public void setEntrySummDate(String entrySummDate) {
		this.entrySummDate = entrySummDate;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getUom1() {
		return uom1;
	}

	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}

	public String getHtsCd() {
		return htsCd;
	}

	public void setHtsCd(String htsCd) {
		this.htsCd = htsCd;
	}

	public Double getQty1() {
		return qty1;
	}

	public void setQty1(Double qty1) {
		this.qty1 = qty1;
	}

	public Double getEstimatedTax() {
		return estimatedTax;
	}

	public void setEstimatedTax(Double estimatedTax) {
		this.estimatedTax = estimatedTax;
	}

	// the following methods and attributes are for 
	// generated values, not to be validated.
	private String errors;
	private Long fiscalYr;

	/**
	 * Get fiscal year.
	 * @return
	 */
	public Long getFiscalYr() {
		return fiscalYr;
	}

	/**
	 * Set fiscal year.
	 * @param fiscalYr
	 */
	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	/**
	 * @return the errors
	 */
	public String getErrors() {
		return errors;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(String errors) {
		this.errors = errors;
	}

	public String getEntryType() {
		return entryType;
	}

	public void setEntryType(String entryType) {
		this.entryType = entryType;
	}
}

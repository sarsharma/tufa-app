package gov.hhs.fda.ctp.common.beans;

public class CigarAssessment {
	
    private Long assessmentYr;
    
    private String createdDt;

    private String submittedInd;  
    
    private String CigarSubmittedInd;
    
    private Long submittedStatusSort;
    


	public String getSubmittedInd() {
		return submittedInd;
	}

	public void setSubmittedInd(String submittedInd) {
		this.submittedInd = submittedInd;
	}

	
	public String getCigarSubmittedInd() {
		return CigarSubmittedInd;
	}

	public void setCigarSubmittedInd(String cigarsubmittedInd) {
		this.CigarSubmittedInd = cigarsubmittedInd;
	}
	public Long getAssessmentYr() {
		return assessmentYr;
	}

	public void setAssessmentYr(Long assessmentYr) {
		this.assessmentYr = assessmentYr;
	}

	public String getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}

	public Long getSubmittedStatusSort() {
		return submittedStatusSort;
	}

	public void setSubmittedStatusSort(Long submittedStatusSort) {
		this.submittedStatusSort = submittedStatusSort;
	}

	

}

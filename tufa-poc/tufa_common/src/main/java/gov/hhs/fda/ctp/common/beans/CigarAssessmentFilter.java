package gov.hhs.fda.ctp.common.beans;

import java.util.List;
import java.util.Set;

public class CigarAssessmentFilter {
	
	/** The status filters. */
	private Set<String> statusFilters;
	
	/** The fiscal yr filters. */
	private Set<Integer> fiscalYrFilters;
	
	/** The results. */
	private List<CigarAssessment> results;

	/**
	 * Gets the results.
	 *
	 * @return the results
	 */
	public List<CigarAssessment> getResults() {
		return results;
	}

	/**
	 * Sets the results.
	 *
	 * @param results            the results to set
	 */
	public void setResults(List<CigarAssessment> results) {
		this.results = results;
	}

	/**
	 * Gets the status filters.
	 *
	 * @return the statusFilters
	 */
	public Set<String> getStatusFilters() {
		return statusFilters;
	}

	/**
	 * Sets the status filters.
	 *
	 * @param statusFilters            the statusFilters to set
	 */
	public void setStatusFilters(Set<String> statusFilters) {
		this.statusFilters = statusFilters;
	}


	/**
	 * Gets the fiscal yr filters.
	 *
	 * @return the fiscalYrFilters
	 */
	public Set<Integer> getFiscalYrFilters() {
		return fiscalYrFilters;
	}

	/**
	 * Sets the fiscal yr filters.
	 *
	 * @param fiscalYrFilters            the fiscalYrFilters to set
	 */
	public void setFiscalYrFilters(Set<Integer> fiscalYrFilters) {
		this.fiscalYrFilters = fiscalYrFilters;
	}

}

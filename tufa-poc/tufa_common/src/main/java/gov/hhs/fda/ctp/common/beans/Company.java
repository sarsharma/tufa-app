/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.Size;

import org.dozer.Mapping;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * The Class Company.
 *
 * @author tgunter
 */
public class Company extends BaseTufaBean {
    /* company fields */
    private long companyId;
    @NotEmpty(message="Must supply a company name.")
    @Size(min=1, max=100, message="Company name must be between 1 and 100 characters.")
    private String legalName;
    @NotEmpty(message="Must supply an EIN.")
    @Size(min=9, max=9, message="EIN must be 9 digits.")
    @Mapping("EIN")
    private String einNumber;
    
    private String companyCreateDt;
    /* permit fields */
    private Set<Permit> permitSet;
    /* create company sends in a single permit */
    private String permitNumber;
    private String activeDate;
    private String inactiveDate;
    private String permitTypeCd;
    private String permitStatusTypeCd;
    /* company address primary */
    Address primaryAddress;
    Address alternateAddress;
    /* permit history - this will only be populated if requested */
    private List<CompanyPermitHistory> permitHistoryList;
    /* contact list */
    private List<Contact> contactSet;
    /* address list */
    private Set<Address> addressSet;
    private String companyStatus;
    private String companyComments;
    
    private PermitAudit permitAudit;
    
    /* Company Tracking Flags */
    private String emailAddressFlag;
    private String physicalAddressFlag;
    private String welcomeLetterFlag;
    private String welcomeLetterSentDt;
    
    /**
	 * @return the companyComments
	 */
	public String getCompanyComments() {
		return companyComments;
	}

	/**
	 * @param companyComments the companyComments to set
	 */
	public void setCompanyComments(String companyComments) {
		this.companyComments = companyComments;
	}

	/**
     * Gets the company status.
     *
     * @return the company status
     */
    public String getCompanyStatus() {
        return this.companyStatus;
    }

    /**
     * Sets the company status.
     *
     * @param status the new company status
     */
    public void setCompanyStatus(String status) {
        this.companyStatus = status;
    }
    
    /**
     * Gets the company id.
     *
     * @return the company_id
     */
    public long getCompanyId() {
        return companyId;
    }
    
    /**
     * Sets the company id.
     *
     * @param compId the new company id
     */
    public void setCompanyId(long compId) {
        this.companyId = compId;
    }
    
    /**
     * Gets the legal name.
     *
     * @return the legalName
     */
    public String getLegalName() {
        return legalName;
    }
    
    /**
     * Sets the legal name.
     *
     * @param legalName the legalName to set
     */
    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }
        
    /**
     * @return the eIN
     */
    public String getEinNumber() {
        return einNumber;
    }
    
    /**
     * Sets the ein number.
     *
     * @param EIN the EIN to set
     */
    public void setEinNumber(String EIN) {
        einNumber = EIN;
    }

    /**
     * Gets the permit set.
     *
     * @return the permits
     */
    public Set<Permit> getPermitSet() {
        return permitSet;
    }

    /**
     * Sets the permit set.
     *
     * @param permits the permits to set
     */
    public void setPermitSet(Set<Permit> permits) {
        this.permitSet = permits;
    }

    /**
     * Gets the permit number.
     *
     * @return the permitNumber
     */
    public String getPermitNumber() {
        return permitNumber;
    }

    /**
     * Sets the permit number.
     *
     * @param permitNumber the permitNumber to set
     */
    public void setPermitNumber(String permitNumber) {
        this.permitNumber = permitNumber;
    }
    
    public String getPermitTypeCd() {
        return permitTypeCd;
    }

    /**
     * Sets the permit type cd.
     *
     * @param permitTypeCd the permitTypeCd to set
     */
    public void setPermitStatusTypeCd(String permitStatusTypeCd) {
        this.permitStatusTypeCd = permitStatusTypeCd;
    }
    
    
    public String getPermitStatusTypeCd() {
        return permitStatusTypeCd;
    }

    /**
     * Sets the permit type cd.
     *
     * @param permitTypeCd the permitTypeCd to set
     */
    public void setPermitTypeCd(String permitTypeCd) {
        this.permitTypeCd = permitTypeCd;
    }

    /**
     * Gets the active date.
     *
     * @return the activeDate
     */
    public String getActiveDate() {
        return activeDate;
    }

    /**
     * Sets the active date.
     *
     * @param activeDate the activeDate to set
     */
    public void setActiveDate(String activeDate) {
        this.activeDate = activeDate;
    }

    /**
     * Gets the closed date.
     *
     * @return the activeDate
     */
    public String getinactiveDate() {
        return inactiveDate;
    }

    /**
     * Sets the closed date.
     *
     * @param activeDate the activeDate to set
     */
    public void setinactiveDate(String inactiveDate) {
        this.inactiveDate = inactiveDate;
    }
    
    
    /**
     * Gets the permit history list.
     *
     * @return the permitHistoryList
     */
    public List<CompanyPermitHistory> getPermitHistoryList() {
        return permitHistoryList;
    }

    /**
     * Sets the permit history list.
     *
     * @param permitHistoryList the permitHistoryList to set
     */
    public void setPermitHistoryList(List<CompanyPermitHistory> permitHistoryList) {
        this.permitHistoryList = permitHistoryList;
    }

    /**
     * Gets the primary address.
     *
     * @return the primaryAddress
     */
    public Address getPrimaryAddress() {
        return primaryAddress;
    }

    /**
     * Sets the primary address.
     *
     * @param primaryAddress the primaryAddress to set
     */
    public void setPrimaryAddress(Address primaryAddress) {
        this.primaryAddress = primaryAddress;
    }

    /**
     * Gets the alternate address.
     *
     * @return the alternateAddress
     */
    public Address getAlternateAddress() {
        return alternateAddress;
    }

    /**
     * Sets the alternate address.
     *
     * @param alternateAddress the alternateAddress to set
     */
    public void setAlternateAddress(Address alternateAddress) {
        this.alternateAddress = alternateAddress;
    }

	/**
	 * Gets the contact set.
	 *
	 * @return the contact set
	 */
	public List<Contact> getContactSet() {
		return contactSet;
	}

	/**
	 * Sets the contact set.
	 *
	 * @param contactSet the new contact set
	 */
	public void setContactSet(List<Contact> contactSet) {
		this.contactSet = contactSet;
	}

	/**
	 * Gets the address set.
	 *
	 * @return the address set
	 */
	public Set<Address> getAddressSet() {
		return addressSet;
	}

	/**
	 * Sets the address set.
	 *
	 * @param addressSet the new address set
	 */
	public void setAddressSet(Set<Address> addressSet) {
		this.addressSet = addressSet;
	}

	public PermitAudit getPermitAudit() {
		return permitAudit;
	}

	public void setPermitAudit(PermitAudit permitAudit) {
		this.permitAudit = permitAudit;
	}

	/**
	 * @return the emailAddressFlag
	 */
	public String getEmailAddressFlag() {
		return emailAddressFlag;
	}

	/**
	 * @param emailAddressFlag the emailAddressFlag to set
	 */
	public void setEmailAddressFlag(String emailAddressFlag) {
		this.emailAddressFlag = emailAddressFlag;
	}

	/**
	 * @return the physicalAddressFlag
	 */
	public String getPhysicalAddressFlag() {
		return physicalAddressFlag;
	}

	/**
	 * @param physicalAddressFlag the physicalAddressFlag to set
	 */
	public void setPhysicalAddressFlag(String physicalAddressFlag) {
		this.physicalAddressFlag = physicalAddressFlag;
	}

	/**
	 * @return the welcomLetterFlag
	 */
	public String getWelcomeLetterFlag() {
		return welcomeLetterFlag;
	}

	/**
	 * @param welcomLetterFlag the welcomLetterFlag to set
	 */
	public void setWelcomeLetterFlag(String welcomLetterFlag) {
		this.welcomeLetterFlag = welcomLetterFlag;
	}

	/**
	 * @return the welcomeLetterSentDt
	 */
	public String getWelcomeLetterSentDt() {
		return welcomeLetterSentDt;
	}

	/**
	 * @param welcomeLetterSentDt the welcomeLetterSentDt to set
	 */
	public void setWelcomeLetterSentDt(String welcomeLetterSentDt) {
		this.welcomeLetterSentDt = welcomeLetterSentDt;
	}

	
	public String getcompanyCreateDt() {
		return companyCreateDt;
	}

	/**
	 * @param welcomeLetterSentDt the welcomeLetterSentDt to set
	 */
	public void setcompanyCreateDt(String companyCreateDt) {
		this.companyCreateDt = companyCreateDt;
	}

}

package gov.hhs.fda.ctp.common.beans;

import java.util.Date;
import java.util.List;

public class CompanyComment {
//    Common comment properties
	private String userComment;
    private Boolean resolveComment;
    private String author;
    private String commentDate;
    private String modifiedBy;
    private Date modifiedDt;
//  Company comment properties
	private List<Integer> attachmentIds;
	
	/**
	 * @return the userComment
	 */
	public String getUserComment() {
		return userComment;
	}
	/**
	 * @param userComment the userComment to set
	 */
	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}
	/**
	 * @return the resolveComment
	 */
	public Boolean getResolveComment() {
		return resolveComment;
	}
	/**
	 * @param resolveComment the resolveComment to set
	 */
	public void setResolveComment(Boolean resolveComment) {
		this.resolveComment = resolveComment;
	}
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * @return the commentDate
	 */
	public String getCommentDate() {
		return commentDate;
	}
	/**
	 * @param commentDate the commentDate to set
	 */
	public void setCommentDate(String commentDate) {
		this.commentDate = commentDate;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}
	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
	/**
	 * @return the attachmentIds
	 */
	public List<Integer> getAttachmentIds() {
		return attachmentIds;
	}
	/**
	 * @param attachmentIds the attachmentIds to set
	 */
	public void setAttachmentIds(List<Integer> attachmentIds) {
		this.attachmentIds = attachmentIds;
	}
}

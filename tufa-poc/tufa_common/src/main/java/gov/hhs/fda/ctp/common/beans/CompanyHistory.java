package gov.hhs.fda.ctp.common.beans;

import java.util.Objects;

/**
 * The Class CompanyHistory.
 */
public class CompanyHistory implements Comparable<CompanyHistory>{
    
    /** The company name. */
    private String companyName;
    
    /** The company id. */
    private long companyId;
    
    /** The permits. */
    private String permits;
    
    /** The company status. */
    private String companyStatus;
    
    /** The company EIN. */
    private String companyEIN;    
    
    /**
     * Instantiates a new company history.
     */
    public CompanyHistory() {
    	companyName = "";
    	companyId = -1L;
    	companyStatus = "";
    	setCompanyEIN("");    	
    }
    
    /**
     * Instantiates a new company history.
     *
     * @param companyHistory the company history
     */
    public CompanyHistory(CompanyHistory companyHistory) {
        if(companyHistory == null) 
            return;
        this.setCompanyName(companyHistory.getCompanyName());
        this.setCompanyId(companyHistory.getCompanyId());
        this.setPermits(companyHistory.getPermits());
        this.setCompanyStatus(companyHistory.getCompanyStatus());
        this.setCompanyEIN(companyHistory.getCompanyEIN());
    }

	/**
	 * Gets the company name.
	 *
	 * @return the company name
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the company name.
	 *
	 * @param companyName the new company name
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * Gets the company id.
	 *
	 * @return the company id
	 */
	public long getCompanyId() {
		return companyId;
	}

	/**
	 * Sets the company id.
	 *
	 * @param companyId the new company id
	 */
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	/**
	 * Gets the permits.
	 *
	 * @return the permits
	 */
	public String getPermits() {
		return permits;
	}

	/**
	 * Sets the permits.
	 *
	 * @param permits the new permits
	 */
	public void setPermits(String permits) {
		this.permits = permits;
	}
	
	/**
	 * Gets the company status.
	 *
	 * @return the company status
	 */
	public String getCompanyStatus() {
		return companyStatus;
	}

	/**
	 * Sets the company status.
	 *
	 * @param companyStatus the new company status
	 */
	public void setCompanyStatus(String companyStatus) {
		this.companyStatus = companyStatus;
	}		

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CompanyHistory o) {
		return 0;
	}
	
    /**
     * Implemented for code compliance.
     *
     * @param o the o
     * @return true, if successful
     */
    @Override
    public boolean equals(Object o) {
        boolean returnVal = false;
        if (o instanceof CompanyHistory) {
        	CompanyHistory companyHistory = (CompanyHistory)o;
            returnVal = Objects.equals(this.companyId, companyHistory.getCompanyId());
        }
        return returnVal;
    }	
	
    /**
     * Implemented for code compliance.
     *
     * @return the int
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.companyName);
    }

	public String getCompanyEIN() {
		return companyEIN;
	}

	public void setCompanyEIN(String companyEIN) {
		this.companyEIN = companyEIN;
	}
}

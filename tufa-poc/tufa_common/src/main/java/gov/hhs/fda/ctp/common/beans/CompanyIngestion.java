package gov.hhs.fda.ctp.common.beans;

import java.util.Set;

public class CompanyIngestion {

	private String companyName;
	private String einNo;
    private String includeFlag;
	private Double totalTaxes;
    private Set<String> otherTobaccoTypesReported;

    private Set<Permit> permits;
    private Set<CBPEntry> entry;
    
	/** importer specific properties **/
	private String importerNm;
	private String importerEIN;
	private Long   fiscalYear;
	boolean providedAnswer;
	
	/** TTB specific properties **/
	private String einNum;
	private String companyNm;
	private String fiscalYr;
	
	public String getAssociationType() {
		return associationType;
	}
	public void setAssociationType(String associationType) {
		this.associationType = associationType;
	}
	/** Association Type**/
	private String associationType;
    
	public String getCompanyName() {
		return companyName;
	}
	public String getEin() {
		return einNo;
	}
	public String getIncludeFlag() {
		return includeFlag;
	}
	public Double getTotalTaxes() {
		return totalTaxes;
	}
	public Set<String> getOtherTobaccoTypesReported() {
		return otherTobaccoTypesReported;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public void setEin(String ein) {
		this.einNo = ein;
	}
	public void setIncludeFlag(String includeFlag) {
		this.includeFlag = includeFlag;
	}
	public void setTotalTaxes(Double totalTaxes) {
		this.totalTaxes = totalTaxes;
	}
	public void setOtherTobaccoTypesReported(Set<String> otherTobaccoTypesReported) {
		this.otherTobaccoTypesReported = otherTobaccoTypesReported;
	}
	public String getImporterNm() {
		return importerNm;
	}
	public String getImporterEIN() {
		return importerEIN;
	}
	public Long getFiscalYear() {
		return fiscalYear;
	}
	public String getEinNum() {
		return einNum;
	}
	public String getCompanyNm() {
		return companyNm;
	}
	public String getFiscalYr() {
		return fiscalYr;
	}
	public boolean isProvidedAnswer() {
		return providedAnswer;
	}
	public void setProvidedAnswer(boolean providedAnswer) {
		this.providedAnswer = providedAnswer;
	}
	public void setImporterNm(String importerNm) {
		this.importerNm = importerNm;
		this.companyName = importerNm;
	}
	public void setImporterEIN(String importerEIN) {
		this.importerEIN = importerEIN;
		this.einNo = importerEIN;
	}
	public void setFiscalYear(Long fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	public void setEinNum(String einNum) {
		this.einNum = einNum;
		this.einNo = einNum;
	}
	public String getEinNo() {
		return einNo;
	}
	public void setEinNo(String einNo) {
		this.einNo = einNo;
	}
	public void setCompanyNm(String companyNm) {
		this.companyNm = companyNm;
		this.companyName = companyNm;
	}
	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}
	public Set<Permit> getPermits() {
		return permits;
	}
	public void setPermits(Set<Permit> permits) {
		this.permits = permits;
	}

	public Set<CBPEntry> getEntry() {
		return entry;
	}
	public void setEntry(Set<CBPEntry> entry) {
		this.entry = entry;
	}
}

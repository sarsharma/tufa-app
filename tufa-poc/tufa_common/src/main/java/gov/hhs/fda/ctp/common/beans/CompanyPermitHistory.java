/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.Objects;

/**
 * The Class CompanyPermitHistory.
 *
 * @author tgunter
 * Specialized permit history bean when looking up all permits for a single company.
 */
public class CompanyPermitHistory {
    
    /** The company name. */
    private String companyName;
    
    /** The company id. */
    private long companyId;
    
    /** The permit id. */
    private long permitId;
    
    /** The report period id. */
    private long reportPeriodId;
    
    /** The permit type cd. */
    private String permitTypeCd;
    
    /** The permit status cd. */
    private String permitStatusCd;
    
    /** The permit num. */
    private String permitNum;
    
    /** The month. */
    private String month;
    
    /** The year. */
    private String year;
    
    /** The fiscal year */
    private String fiscalYear;
    
    public String getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	/** The display month year. */
    private String displayMonthYear;
    
    /** The sort month year. */
    private String sortMonthYear;
    
    /** The report status cd. */
    private String reportStatusCd;
    
    /** The activity. */
    private String activity;
    
    /** EIN */
    private String companyEIN;   
    
    /** 3852 DOC ID */
    private long doc3852Id;   

    /**
     * Instantiates a new company permit history.
     */
    public CompanyPermitHistory() {
        /*
         * default constructor
         * 
         */
    }
    
    /**
     * Instantiates a new company permit history.
     *
     * @param permitHistory the permit history
     */
    public CompanyPermitHistory(PermitHistory permitHistory) {
        if(permitHistory == null) 
            return;
        this.reportPeriodId = permitHistory.getReportPeriodId();
        this.permitNum = permitHistory.getPermitNum();
        this.month = permitHistory.getMonth();
        this.year = permitHistory.getYear();
        this.displayMonthYear = permitHistory.getDiaplayMonthYear();
        this.sortMonthYear = permitHistory.getSortMonthYear();
        this.reportStatusCd = permitHistory.getStatusCd();
        this.activity = permitHistory.getActivity();
    }

    /**
     * Gets the permit num.
     *
     * @return the permitNum
     */
    public String getPermitNum() {
        return permitNum;
    }

    /**
     * Sets the permit num.
     *
     * @param permitNum the permitNum to set
     */
    public void setPermitNum(String permitNum) {
        this.permitNum = permitNum;
    }
    
    /**
     * Gets the company name.
     *
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }
    
    /**
     * Sets the company name.
     *
     * @param companyName the companyName to set
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    
    /**
     * Gets the permit id.
     *
     * @return the permitId
     */
    public long getPermitId() {
        return permitId;
    }
    
    /**
     * Sets the permit id.
     *
     * @param permitId the permitId to set
     */
    public void setPermitId(long permitId) {
        this.permitId = permitId;
    }
    
    /**
     * Gets the report period id.
     *
     * @return the reportPeriodId
     */
    public long getReportPeriodId() {
        return reportPeriodId;
    }
    
    /**
     * Sets the report period id.
     *
     * @param reportPeriodId the reportPeriodId to set
     */
    public void setReportPeriodId(long reportPeriodId) {
        this.reportPeriodId = reportPeriodId;
    }
    
    /**
     * Gets the month.
     *
     * @return the month
     */
    public String getMonth() {
        return month;
    }
    
    /**
     * Sets the month.
     *
     * @param month the month to set
     */
    public void setMonth(String month) {
        this.month = month;
    }
    
    /**
     * Gets the year.
     *
     * @return the year
     */
    public String getYear() {
        return year;
    }
    
    /**
     * Sets the year.
     *
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }
    
    /**
     * Gets the display month year.
     *
     * @return the diaplayMonthYear
     */
    public String getDisplayMonthYear() {
        return displayMonthYear;
    }
    
    /**
     * Sets the display month year.
     *
     * @param displayMonthYear the displayMonthYear to set
     */
    public void setDisplayMonthYear(String diaplayMonthYear) {
        this.displayMonthYear = diaplayMonthYear;
    }
    
    /**
     * Gets the sort month year.
     *
     * @return the sortMonthYear
     */
    public String getSortMonthYear() {
        return sortMonthYear;
    }
    
    /**
     * Sets the sort month year.
     *
     * @param sortMonthYear the sortMonthYear to set
     */
    public void setSortMonthYear(String sortMonthYear) {
        this.sortMonthYear = sortMonthYear;
    }
    
    /**
     * Gets the report status cd.
     *
     * @return the statusCd
     */
    public String getReportStatusCd() {
        return reportStatusCd;
    }
    
    /**
     * Sets the report status cd.
     *
     * @param statusCd the statusCd to set
     */
    public void setReportStatusCd(String statusCd) {
        this.reportStatusCd = statusCd;
    }
    
    /**
     * Gets the activity.
     *
     * @return the activity
     */
    public String getActivity() {
        return activity;
    }
    
    /**
     * Sets the activity.
     *
     * @param activity the activity to set
     */
    public void setActivity(String activity) {
        this.activity = activity;
    }

    /**
     * Gets the permit status cd.
     *
     * @return the permitStatusCd
     */
    public String getPermitStatusCd() {
        return permitStatusCd;
    }

    /**
     * Sets the permit status cd.
     *
     * @param permitStatusCd the permitStatusCd to set
     */
    public void setPermitStatusCd(String permitStatusCd) {
        this.permitStatusCd = permitStatusCd;
    }

    /**
     * Gets the permit type cd.
     *
     * @return the permitTypeCd
     */
    public String getPermitTypeCd() {
        return permitTypeCd;
    }

    /**
     * Sets the permit type cd.
     *
     * @param permitTypeCd the permitTypeCd to set
     */
    public void setPermitTypeCd(String permitTypeCd) {
        this.permitTypeCd = permitTypeCd;
    }

    /**
     * Gets the company id.
     *
     * @return the companyId
     */
    public long getCompanyId() {
        return companyId;
    }

    /**
     * Sets the company id.
     *
     * @param companyId the companyId to set
     */
    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }
    
	public String getCompanyEIN() {
		return companyEIN;
	}

	public void setCompanyEIN(String companyEIN) {
		this.companyEIN = companyEIN;
	}    
    
	/**
	 * @return the doc3852
	 */
	public long getDoc3852Id() {
		return doc3852Id;
	}

	/**
	 * @param doc3852 the doc3852 to set
	 */
	public void setDoc3852Id(long doc3852Id) {
		this.doc3852Id = doc3852Id;
	}

	/**
	 * Implemented for code compliance.
	 *
	 * @param o the o
	 * @return true, if successful
	 */
	@Override
	public boolean equals(Object o) {
	    boolean returnVal = false;
	    if (o instanceof CompanyPermitHistory) {
	        CompanyPermitHistory permitHistory = (CompanyPermitHistory)o;
	        returnVal = Objects.equals(this.permitNum, permitHistory.getPermitNum());
	    }
	    return returnVal;
	}
	
	/**
	 * Implemented for code compliance.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
	    return Objects.hash(this.permitNum);
	}


    
}

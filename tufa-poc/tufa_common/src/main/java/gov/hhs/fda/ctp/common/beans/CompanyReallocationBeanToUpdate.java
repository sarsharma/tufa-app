package gov.hhs.fda.ctp.common.beans;

import java.util.List;

public class CompanyReallocationBeanToUpdate {
    
    private long companyId;
    private long fiscalYear;
    private String secondaryDateFlag;
    private long tobaccoClassId;
    private String ein;
    
    
	public String getEin() {
		return ein;
	}
	public void setEin(String ein) {
		this.ein = ein;
	}
	public long getCompanyId() {
        return companyId;
    }
    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }
    public long getFiscalYear() {
        return fiscalYear;
    }
    public void setFiscalYear(long fiscalYear) {
        this.fiscalYear = fiscalYear;
    }
	public String getSecondaryDateFlag() {
		return secondaryDateFlag;
	}
	public void setSecondaryDateFlag(String secondaryDateFlag) {
		this.secondaryDateFlag = secondaryDateFlag;
	}
	public long getTobaccoClassId() {
		return tobaccoClassId;
	}
	public void setTobaccoClassId(long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}

    
}

package gov.hhs.fda.ctp.common.beans;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * The Class CompanySearchCriteria.
 */
public class CompanySearchCriteria {
    
    /** The company name filter. */
    private String companyNameFilter;
    
    /** The company id filter. */
    private Long   companyIdFilter;
    
    /** The company status filters. */
    private Set<String> companyStatusFilters;
    
    /** The results. */
    private List<CompanyHistory> results;
    
    /** Company EIN filter */
    private String companyEINFilter;
    
    /** Email address flag */
    private String emailAddressFlagFliter;
    
    /** Physical address flag */
    private String physicalAddressFlagFilter;
    
    /** Welcome letter flag */
    private String welcomeLetterFlagFilter;
    
    /** Date the company was added */
    private Date companyCreatedDtFilter;
    
    private Date welcomeLetterSentDtFilter;
    
    /**
     * Gets the results.
     *
     * @return the results
     */
    public List<CompanyHistory> getResults() {
        return results;
    }

	/**
	 * Gets the company name filter.
	 *
	 * @return the company name filter
	 */
	public String getCompanyNameFilter() {
		return companyNameFilter;
	}

	/**
	 * Sets the company name filter.
	 *
	 * @param companyNameFilter the new company name filter
	 */
	public void setCompanyNameFilter(String companyNameFilter) {
		this.companyNameFilter = companyNameFilter;
	}

	/**
	 * Gets the company id filter.
	 *
	 * @return the company id filter
	 */
	public Long getCompanyIdFilter() {
		return companyIdFilter;
	}

	/**
	 * Sets the company id filter.
	 *
	 * @param companyIdFilter the new company id filter
	 */
	public void setCompanyIdFilter(Long companyIdFilter) {
		this.companyIdFilter = companyIdFilter;
	}

	/**
	 * Gets the company status filters.
	 *
	 * @return the company status filters
	 */
	public Set<String> getCompanyStatusFilters() {
		return companyStatusFilters;
	}

	/**
	 * Sets the company status filters.
	 *
	 * @param companyStatusFilters the new company status filters
	 */
	public void setCompanyStatusFilters(Set<String> companyStatusFilters) {
		this.companyStatusFilters = companyStatusFilters;
	}

	/**
	 * Sets the results.
	 *
	 * @param results the new results
	 */
	public void setResults(List<CompanyHistory> results) {
		this.results = results;		
	}

	/**
	 * @return the companyEINFilter
	 */
	public String getCompanyEINFilter() {
		return companyEINFilter;
	}

	/**
	 * @param companyEINFilter the companyEINFilter to set
	 */
	public void setCompanyEINFilter(String companyEINFilter) {
		this.companyEINFilter = companyEINFilter;
	}

	/**
	 * @return the emailAddressFlag
	 */
	public String getEmailAddressFlagFilter() {
		return emailAddressFlagFliter;
	}

	/**
	 * @param emailAddressFlag the emailAddressFlag to set
	 */
	public void setEmailAddressFlagFilter(String emailAddressFlag) {
		this.emailAddressFlagFliter = emailAddressFlag;
	}

	/**
	 * @return the physicalAddressFlag
	 */
	public String getPhysicalAddressFlagFilter() {
		return physicalAddressFlagFilter;
	}

	/**
	 * @param physicalAddressFlag the physicalAddressFlag to set
	 */
	public void setPhysicalAddressFlagFilter(String physicalAddressFlag) {
		this.physicalAddressFlagFilter = physicalAddressFlag;
	}

	/**
	 * @return the welcomeLetterFlag
	 */
	public String getWelcomeLetterFlagFilter() {
		return welcomeLetterFlagFilter;
	}

	/**
	 * @param welcomeLetterFlag the welcomeLetterFlag to set
	 */
	public void setWelcomeLetterFlagFilter(String welcomeLetterFlag) {
		this.welcomeLetterFlagFilter = welcomeLetterFlag;
	}

	/**
	 * @return the dateAdded
	 */
	public Date getCompanyCreatedDtFilter() {
		return companyCreatedDtFilter;
	}

	/**
	 * @param dateAdded the dateAdded to set
	 */
	public void setCompanyCreatedDtFilter(Date createdDtFilter) {
		this.companyCreatedDtFilter = createdDtFilter;
	}

	public Date getWelcomeLetterSentDtFilter() {
		return welcomeLetterSentDtFilter;
	}

	public void setWelcomeLetterSentDtFilter(Date welcomeLetterSentDtFilter) {
		this.welcomeLetterSentDtFilter = welcomeLetterSentDtFilter;
	}
}

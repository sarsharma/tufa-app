
package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

public class ComparisonAllDeltaComments {
	 
	    public String userComment;
	    public String author;
	    public String commentDate;	 
	    public Long commentSeq;	
		public Boolean resolveComment;
	    public String modifiedBy;
		public String modifiedDt;
		public String commentQtrs;
		public Long matchCommSeq;	
		private Long attachmentsCount;
		 
		 

		public Long getAttachmentsCount() {
			return attachmentsCount;
		}

		public void setAttachmentsCount(Long attachmentsCount) {
			this.attachmentsCount = attachmentsCount;
		}
       
		public String getCommentQtrs() {
			return commentQtrs;
		}
		public void setCommentQtrs(String commentQtrs) {
			this.commentQtrs = commentQtrs;
		}
		/**
		 * @return the userComment
		 */
		public String getUserComment() {
			return userComment;
		}
		/**
		 * @param userComment the userComment to set
		 */
		public void setUserComment(String userComment) {
			this.userComment = userComment;
		}
		/**
		 * @return the author
		 */
		public String getAuthor() {
			return author;
		}
		/**
		 * @param author the author to set
		 */
		public void setAuthor(String author) {
			this.author = author;
		}
		
				
		public Long getCommentSeq() {
			return commentSeq;
		}

		public void setCommentSeq(Long commentSeq) {
			this.commentSeq = commentSeq;
		}
		public Boolean getResolveComment() {
			return resolveComment;
		}
		public void setResolveComment(Boolean resolveComment) {
			this.resolveComment = resolveComment;
		}
		public String getModifiedBy() {
			return modifiedBy;
		}
		public void setModifiedBy(String modifiedBy) {
			this.modifiedBy = modifiedBy;
		}
		public String getModifiedDt() {
			return modifiedDt;
		}
		public void setModifiedDt(String modifiedDt) {
			this.modifiedDt = modifiedDt;
		}
		 public String getCommentDate() {
				return commentDate;
			}
		public void setCommentDate(String commentDate) {
				this.commentDate = commentDate;
		}
		 public Long getMatchCommSeq() {
				return matchCommSeq;
		}
		public void setMatchCommSeq(Long matchCommSeq) {
				this.matchCommSeq = matchCommSeq;
		}
		
}

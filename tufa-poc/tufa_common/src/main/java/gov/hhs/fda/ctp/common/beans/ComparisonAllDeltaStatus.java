
package gov.hhs.fda.ctp.common.beans;

import java.util.Date;
import java.util.List;


public class ComparisonAllDeltaStatus
 {	
    private Long cmpAllDeltaId;	
    private String ein;	
    private String fiscalYr;	
    private String fiscalQtr;
   private String tobaccoClassName;
    private String permitType;
    private String deltaStatus;
    private Double delta;
    private String tobaccoSubType1;
    private Double tobaccoSubType1Amndtx;
	private Double tobaccoSubType1Pounds;
    private String tobaccoSubType2;
    private Double tobaccoSubType2Amndtx;
	private Double tobaccoSubType2Pounds;
    private String deltaType;
	private String fileType;
    public List<ComparisonAllDeltaComments> userComments;
   

	
	public Double getTobaccoSubType1Pounds() {
		return tobaccoSubType1Pounds;
	}

	public void setTobaccoSubType1Pounds(Double tobaccoSubType1Pounds) {
		this.tobaccoSubType1Pounds = tobaccoSubType1Pounds;
	}

	public Double getTobaccoSubType2Pounds() {
		return tobaccoSubType2Pounds;
	}

	public void setTobaccoSubType2Pounds(Double tobaccoSubType2Pounds) {
		this.tobaccoSubType2Pounds = tobaccoSubType2Pounds;
	}


	public Long getCmpAllDeltaId() {
		return cmpAllDeltaId;
	}

	public void setCmpAllDeltaId(Long cmpAllDeltaId) {
		this.cmpAllDeltaId = cmpAllDeltaId;
	}

	public String getEin() {
		return ein;
	}

	public void setEin(String ein) {
		this.ein = ein;
	}


	public String getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getFiscalQtr() {
		return fiscalQtr;
	}

	public void setFiscalQtr(String fiscalQtr) {
		this.fiscalQtr = fiscalQtr;
	}

	public String getTobaccoClassName() {
		return tobaccoClassName;
	}

	public void setTobaccoClassName(String tobaccoClassName) {
		this.tobaccoClassName = tobaccoClassName;
	}

	public String getPermitType() {
		return permitType;
	}

	public void setPermitType(String permitType) {
		this.permitType = permitType;
	}

	public String getDeltaStatus() {
		return deltaStatus;
	}

	public void setDeltaStatus(String deltaStatus) {
		this.deltaStatus = deltaStatus;
	}

	public Double getDelta() {
		return delta;
	}

	public void setDelta(Double delta) {
		this.delta = delta;
	}

	public String getTobaccoSubType1() {
		return tobaccoSubType1;
	}

	public void setTobaccoSubType1(String tobaccoSubType1) {
		this.tobaccoSubType1 = tobaccoSubType1;
	}

	public String getTobaccoSubType2() {
		return tobaccoSubType2;
	}

	public void setTobaccoSubType2(String tobaccoSubType2) {
		this.tobaccoSubType2 = tobaccoSubType2;
	}

	public Double getTobaccoSubType1Amndtx() {
		return tobaccoSubType1Amndtx;
	}

	public void setTobaccoSubType1Amndtx(Double tobaccoSubType1Amndtx) {
		this.tobaccoSubType1Amndtx = tobaccoSubType1Amndtx;
	}

	public Double getTobaccoSubType2Amndtx() {
		return tobaccoSubType2Amndtx;
	}

	public void setTobaccoSubType2Amndtx(Double tobaccoSubType2Amndtx) {
		this.tobaccoSubType2Amndtx = tobaccoSubType2Amndtx;
	}

	public String getDeltaType() {
		return deltaType;
	}

	public void setDeltaType(String deltaType) {
		this.deltaType = deltaType;
	}
	
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public List<ComparisonAllDeltaComments> getUserComments() {
		return userComments;
	}

	public void setUserComments(List<ComparisonAllDeltaComments> userComments) {
		this.userComments = userComments;
	}
	
}

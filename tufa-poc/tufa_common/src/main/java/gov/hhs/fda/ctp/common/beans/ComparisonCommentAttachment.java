package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

public class ComparisonCommentAttachment {
    private long documentId;
    private long cbpCommentSeq;
    private long ttbCommentSeq;
    private long allDeltaCommentSeq;
    private String docFileNm;
    private String docDesc;
    private String docStatusCd;
    private String createdBy;
    private Date createdDt;
    private String modifiedBy;
    private Date modifiedDt;
	public long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}
	public long getCbpCommentSeq() {
		return cbpCommentSeq;
	}
	public void setCbpCommentSeq(long cbpCommentSeq) {
		this.cbpCommentSeq = cbpCommentSeq;
	}
	public long getTtbCommentSeq() {
		return ttbCommentSeq;
	}
	public void setTtbCommentSeq(long ttbCommentSeq) {
		this.ttbCommentSeq = ttbCommentSeq;
	}
	public long getAllDeltaCommentSeq() {
		return allDeltaCommentSeq;
	}
	public void setAllDeltaCommentSeq(long allDeltaCommentSeq) {
		this.allDeltaCommentSeq = allDeltaCommentSeq;
	}
	public String getDocFileNm() {
		return docFileNm;
	}
	public void setDocFileNm(String docFileNm) {
		this.docFileNm = docFileNm;
	}
	public String getDocDesc() {
		return docDesc;
	}
	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}
	public String getDocStatusCd() {
		return docStatusCd;
	}
	public void setDocStatusCd(String docStatusCd) {
		this.docStatusCd = docStatusCd;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedDt() {
		return modifiedDt;
	}
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
    
}

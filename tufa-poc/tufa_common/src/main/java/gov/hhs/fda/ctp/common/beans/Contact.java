/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

/**
 * The Class Contact.
 *
 * @author tgunter
 */
public class Contact extends BaseTufaBean {
    
    /** The contact id. */
    private Long contactId;
    
    private String ein;
    
    /** The company id. */
    private Long companyId;
    
    private String docStageId;
	private String StageId;
    
    /** The last nm. */
    private String lastNm;    
    
    /** The first nm. */
    private String firstNm;
    
    /** The country dial code */
    private String cntryDialCd;
    
    /** The phone num. */
    private String phoneNum;
    
    /** The phone ext. */
    private String phoneExt;
    
    /** The email address. */
    private String emailAddress;
    
    /** The country cd. */
    private String countryCd;
    
    /** The fax num. */
    private String faxNum;
    
    /** The country fax dial code */
    private String cntryFaxDialCd;    
    
	/** The company. */
    private Company company;
    
    /** The sequnce number for primary contact */
    private Long primaryContactSequence;
    
    private Boolean setAsPrimary = false;
    
    /**
     * Gets the contact id.
     *
     * @return the contactId
     */
    public Long getContactId() {
        return contactId;
    }
    
    /**
     * Sets the contact id.
     *
     * @param contactId the contactId to set
     */
    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }
    
    /**
     * Gets the company id.
     *
     * @return the companyId
     */
    public Long getCompanyId() {
        return companyId;
    }
    
    /**
     * Sets the company id.
     *
     * @param companyId the companyId to set
     */
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
    
    /**
     * Gets the last nm.
     *
     * @return the lastNm
     */
    public String getLastNm() {
        return lastNm;
    }
    
    /**
     * Sets the last nm.
     *
     * @param lastNm the lastNm to set
     */
    public void setLastNm(String lastNm) {
        this.lastNm = lastNm;
    }
    
    /**
     * Gets the first nm.
     *
     * @return the firstNm
     */
    public String getFirstNm() {
        return firstNm;
    }
    
    /**
     * Sets the first nm.
     *
     * @param firstNm the firstNm to set
     */
    public void setFirstNm(String firstNm) {
        this.firstNm = firstNm;
    }
    
    /**
     * Gets the phone num.
     *
     * @return the phoneNum
     */
    public String getPhoneNum() {
        return phoneNum;
    }
    
    /**
     * Sets the phone num.
     *
     * @param phoneNum the phoneNum to set
     */
    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }
    
    public String getCntryDialCd() {
		return cntryDialCd;
	}

	public void setCntryDialCd(String cntryDialCd) {
		this.cntryDialCd = cntryDialCd;
	}

   /**
     * Gets the email address.
     *
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }
    
    /**
     * Gets the country cd.
     *
     * @return the country cd
     */
    public String getCountryCd() {
		return countryCd;
	}
	
	/**
	 * Sets the country cd.
	 *
	 * @param countryCd the new country cd
	 */
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}
	
	/**
	 * Sets the email address.
	 *
	 * @param emailAddress the emailAddress to set
	 */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
	/**
	 * Gets the company.
	 *
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}
	
	/**
	 * Sets the company.
	 *
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the faxNum
	 */
	public String getFaxNum() {
		return faxNum;
	}

	/**
	 * @param faxNum the faxNum to set
	 */
	public void setFaxNum(String faxNum) {
		this.faxNum = faxNum;
	}

    public String getCntryFaxDialCd() {
		return cntryFaxDialCd;
	}

	public void setCntryFaxDialCd(String cntryFaxDialCd) {
		this.cntryFaxDialCd = cntryFaxDialCd;
	}
	
	/**
	 * @return the phoneExt
	 */
	public String getPhoneExt() {
		return phoneExt;
	}

	/**
	 * @param phoneExt the phoneExt to set
	 */
	public void setPhoneExt(String phoneExt) {
		this.phoneExt = phoneExt;
	}

	
	public String getDocStageId(){
		return docStageId;
	}
	
	public void setDocStageId(String docStageId){
		this.docStageId = docStageId;
	}
	
	public String getStageId(){
		return StageId;
	}
	
	public void setStageId(String StageId){
		this.StageId = StageId;
	}
	
	public String getEin(){
		return ein;
	}
	
	public void setEin(String ein){
		this.ein = ein;
	}

	/**
	 * @return the primaryContactSequence
	 */
	public Long getPrimaryContactSequence() {
		return primaryContactSequence;
	}

	/**
	 * @param primaryContactSequence the primaryContactSequence to set
	 */
	public void setPrimaryContactSequence(Long primaryContactSequence) {
		this.primaryContactSequence = primaryContactSequence;
	}

	/**
	 * @return the setAsPrimary
	 */
	public Boolean getSetAsPrimary() {
		return setAsPrimary;
	}

	/**
	 * @param setAsPrimary the setAsPrimary to set
	 */
	public void setSetAsPrimary(Boolean setAsPrimary) {
		this.setAsPrimary = setAsPrimary;
	}
}

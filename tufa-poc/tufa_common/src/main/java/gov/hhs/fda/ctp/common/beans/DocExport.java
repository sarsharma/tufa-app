package gov.hhs.fda.ctp.common.beans;

/**
 * Base class for any and all exports. 
 * */

/*
 * If more fields are needed that are not generic 
 * enough to use across all extensions then create a new class.
 * */
public class DocExport {
	
	private String fileName;
	private String csv;
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the csv (Comma-Separated Value)
	 */
	public String getCsv() {
		return csv;
	}
	/**
	 * @param csv the (Comma-Separated Value) csv to set
	 */
	public void setCsv(String csv) {
		this.csv = csv;
	}
	
	
	
}

package gov.hhs.fda.ctp.common.beans;

public class DocUpload {

		private String fileName;
		private String createdDt;
		private String fileType;
		
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public String getCreatedDt() {
			return createdDt;
		}
		public void setCreatedDt(String createdDt) {
			this.createdDt = createdDt;
		}
		public String getFileType() {
			return fileType;
		}
		public void setFileType(String fileType) {
			this.fileType = fileType;
		}
		
}

package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

/**
 * @author Mohan.Bhutada
 *
 */
public class ExcludePermit {

    private long permit;
    private Date excludedDate;

    public long getPermit() {
        return permit;
    }

    public void setPermit(long permit) {
        this.permit = permit;
    }

    public Date getExcludedDate() {
        return excludedDate;
    }

    public void setExcludedDate(Date excludedDate) {
        this.excludedDate = excludedDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ExcludePermit) {
            return ((ExcludePermit) obj).permit == permit;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (int) this.permit;
    }
}

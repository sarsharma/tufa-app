package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

public class ExcludePermitBean {

    private String quarter;
    private String assessmentYear;
    private String datecreated;
    private String user;
    private String status;
    private String assessmentPeriod;
    private long permitId;
    private long excludeScopeId;
    public String getQuarter() {
        return quarter;
    }
    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }
    public String getAssessmentYear() {
        return assessmentYear;
    }
    public void setAssessmentYear(String assessmentYear) {
        this.assessmentYear = assessmentYear;
    }
    public String getDatecreated() {
        return datecreated;
    }
    public void setDatecreated(String datecreated) {
        this.datecreated = datecreated;
    }
    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getAssessmentPeriod() {
        return assessmentPeriod;
    }
    public void setAssessmentPeriod(String assessmentPeriod) {
        this.assessmentPeriod = assessmentPeriod;
    }
    public long getPermitId() {
        return permitId;
    }
    public void setPermitId(long permitId) {
        this.permitId = permitId;
    }
    public long getExcludeScopeId() {
        return excludeScopeId;
    }
    public void setExcludeScopeId(long excludeScopeId) {
        this.excludeScopeId = excludeScopeId;
    }
    
    
}

package gov.hhs.fda.ctp.common.beans;

import java.util.List;

public class ExcludePermitDTO {
	private List<ExcludePermitRequest> excludes;
	private PermitExcludeComment comment;
	
	/**
	 * @return the excludes
	 */
	public List<ExcludePermitRequest> getExcludes() {
		return excludes;
	}
	/**
	 * @param excludes the excludes to set
	 */
	public void setExcludes(List<ExcludePermitRequest> excludes) {
		this.excludes = excludes;
	}
	/**
	 * @return the comment
	 */
	public PermitExcludeComment getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(PermitExcludeComment comment) {
		this.comment = comment;
	}
}

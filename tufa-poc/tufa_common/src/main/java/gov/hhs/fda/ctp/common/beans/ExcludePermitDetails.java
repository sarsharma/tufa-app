package gov.hhs.fda.ctp.common.beans;

public class ExcludePermitDetails {
	
	private long year;
	private int quarter;
	private int exclusionScopeId;
	public long getYear() {
		return year;
	}
	public void setYear(long year) {
		this.year = year;
	}
	public int getQuarter() {
		return quarter;
	}
	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}
	public int getExclusionScopeId() {
		return exclusionScopeId;
	}
	public void setExclusionScopeId(int exclusionScopeId) {
		this.exclusionScopeId = exclusionScopeId;
	}
	

}

package gov.hhs.fda.ctp.common.beans;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


/**
 * @author Mohan.Bhutada
 *
 */
public class ExcludePermitRequest implements Serializable{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String fiscalYear;
    private List<String> quarterselected;
    
    public String getFiscalYear() {
        return fiscalYear;
    }
    
    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }
    
    public List<String> getQuarterselected() {
        return quarterselected;
    }
    
    public void setQuarterselected(List<String> quarterselected) {
        this.quarterselected = quarterselected;
    }

}

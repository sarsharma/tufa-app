/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

/**
 * @author tgunter
 *
 */
public class FormComment {
	    private long formId;
	    private long tobaccoClassId;
	    private String userComment;
	    private String author;
	    private String commentDate;
	    private String formTypeCd;
	    private long commentSeq;
	    private long id;
	    private Boolean resolveComment;
		private String modifiedBy;
		private String modifiedDt;

		/**
		 * @return the formId
		 */
		public long getFormId() {
			return formId;
		}
		/**
		 * @param formId the formId to set
		 */
		public void setFormId(long formId) {
			this.formId = formId;
		}
		/**
		 * @return the tobaccoClassId
		 */
		public long getTobaccoClassId() {
			return tobaccoClassId;
		}
		/**
		 * @param tobaccoClassId the tobaccoClassId to set
		 */
		public void setTobaccoClassId(long tobaccoClassId) {
			this.tobaccoClassId = tobaccoClassId;
		}
		
		/**
		 * @return the userComment
		 */
		public String getUserComment() {
			return userComment;
		}
		/**
		 * @param userComment the userComment to set
		 */
		public void setUserComment(String userComment) {
			this.userComment = userComment;
		}
		/**
		 * @return the author
		 */
		public String getAuthor() {
			return author;
		}
		/**
		 * @param author the author to set
		 */
		public void setAuthor(String author) {
			this.author = author;
		}
		/**
		 * @return the commentDate
		 */
		public String getCommentDate() {
			return commentDate;
		}
		/**
		 * @param commentDate the commentDate to set
		 */
		public void setCommentDate(String commentDate) {
			this.commentDate = commentDate;
		}
		/**
		 * @return the formTypeCd
		 */
		public String getFormTypeCd() {
			return formTypeCd;
		}
		/**
		 * @param formTypeCd the formTypeCd to set
		 */
		public void setFormTypeCd(String formTypeCd) {
			this.formTypeCd = formTypeCd;
		}
		public long getCommentSeq() {
			return commentSeq;
		}
		public void setCommentSeq(long commentSeq) {
			this.commentSeq = commentSeq;
		}

		public Boolean getResolveComment() {
			return resolveComment;
		}
		public void setResolveComment(Boolean resolveComment) {
			this.resolveComment = resolveComment;
		}
		public String getModifiedBy() {
			return modifiedBy;
		}
		public void setModifiedBy(String modifiedBy) {
			this.modifiedBy = modifiedBy;
		}
		public String getModifiedDt() {
			return modifiedDt;
		}
		public void setModifiedDt(String modifiedDt) {
			this.modifiedDt = modifiedDt;
		}
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		
}

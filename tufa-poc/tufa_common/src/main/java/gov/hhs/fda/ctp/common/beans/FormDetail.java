/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

/**
 * The Class FormDetail.
 *
 * @author tgunter
 */
public class FormDetail  extends BaseTufaBean implements Comparable<FormDetail> {
    
    /** The form id. */
    private Long formId;
    
    /** The tobacco class id. */
    private Long tobaccoClassId;
    
    /** The removal qty. */
    private Double removalQty;
    
    /** The removal uom. */
    private String removalUom;
    
    /** The taxes paid. */
    private Double taxesPaid;
    
    /** The dol difference. */
    private Double dolDifference;
    
    /** The activity cd. */
    private String activityCd;
    
    /** The line num. */
    private Long lineNum;
    
    /** The filer entry num. */
    private String filerEntryNum;

    /** The ad valorem dollar amount */
    private Double valoremDol;

    private Double calcTxVar7501;
    
    private String calcTxActCd7501;
    
    private Double calcTaxRate;
    
    private Double dolRemovalDifference;
    
    private String removalActivity;
    
    private String taxFormId;
    
    private Double adjATotal;
    
    private Double adjBTotal;
    
    private Double grandTotal;
    
    private Double adjustmentTotal;
    
    private String enableAdj;
    
    private Double adjustmentRemoval;
    
    private Double grandRemovalTotal;
    
    public Double getValoremDol() {
		return valoremDol;
	}

	public void setValoremDol(Double valoremDol) {
		this.valoremDol = valoremDol;
	}

	/**
     * Gets the form id.
     *
     * @return the formId
     */
    public Long getFormId() {
        return formId;
    }
    
    /**
     * Sets the form id.
     *
     * @param formId the formId to set
     */
    public void setFormId(Long formId) {
        this.formId = formId;
    }
    
    /**
     * Gets the tobacco class id.
     *
     * @return the tobaccoClassId
     */
    public Long getTobaccoClassId() {
        return tobaccoClassId;
    }
    
    /**
     * Sets the tobacco class id.
     *
     * @param tobaccoClassId the tobaccoClassId to set
     */
    public void setTobaccoClassId(Long tobaccoClassId) {
        this.tobaccoClassId = tobaccoClassId;
    }
    
    /**
     * Gets the removal qty.
     *
     * @return the removalQty
     */
    public Double getRemovalQty() {
        return removalQty;
    }
    
    /**
     * Sets the removal qty.
     *
     * @param removalQty the removalQty to set
     */
    public void setRemovalQty(Double removalQty) {
        this.removalQty = removalQty;
    }
    
    /**
     * Gets the removal uom.
     *
     * @return the removalUom
     */
    public String getRemovalUom() {
        return removalUom;
    }
    
    /**
     * Sets the removal uom.
     *
     * @param removalUom the removalUom to set
     */
    public void setRemovalUom(String removalUom) {
        this.removalUom = removalUom;
    }
    
    /**
     * Gets the taxes paid.
     *
     * @return the taxesPaid
     */
    public Double getTaxesPaid() {
        return taxesPaid;
    }
    
    /**
     * Sets the taxes paid.
     *
     * @param taxesPaid the taxesPaid to set
     */
    public void setTaxesPaid(Double taxesPaid) {
        this.taxesPaid = taxesPaid;
    }
    
    /**
     * Gets the dol difference.
     *
     * @return the pctDifference
     */
    public Double getDolDifference() {
        return dolDifference;
    }
    
    /**
     * Sets the dol difference.
     *
     * @param dolDifference the new dol difference
     */
    public void setDolDifference(Double dolDifference) {
        this.dolDifference = dolDifference;
    }
    
    /**
     * Gets the activity cd.
     *
     * @return the activityCd
     */
    public String getActivityCd() {
        return activityCd;
    }
    
    /**
     * Sets the activity cd.
     *
     * @param activityCd the activityCd to set
     */
    public void setActivityCd(String activityCd) {
        this.activityCd = activityCd;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FormDetail detail = (FormDetail)obj;
        return toString().equals(detail.toString());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("#"+this.formId+"#"+this.lineNum+"#"+this.tobaccoClassId+"#");
        return builder.toString();
    }
    
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(FormDetail detail) {
        return this.lineNum.compareTo(detail.getLineNum());
    }
    
    /**
     * Gets the line num.
     *
     * @return the lineNum
     */
    public Long getLineNum() {
        return lineNum;
    }
    
    /**
     * Sets the line num.
     *
     * @param lineNum the lineNum to set
     */
    public void setLineNum(Long lineNum) {
        this.lineNum = lineNum;
    }
    
    /**
     * Gets the filer entry num.
     *
     * @return the filerEntryNum
     */
    public String getFilerEntryNum() {
        return filerEntryNum;
    }
    
    /**
     * Sets the filer entry num.
     *
     * @param filerEntryNum the filerEntryNum to set
     */
    public void setFilerEntryNum(String filerEntryNum) {
        this.filerEntryNum = filerEntryNum;
    }

	public Double getCalcTxVar7501() {
		return calcTxVar7501;
	}

	public void setCalcTxVar7501(Double calcTxVar7501) {
		this.calcTxVar7501 = calcTxVar7501;
	}

	public String getCalcTxActCd7501() {
		return calcTxActCd7501;
	}

	public void setCalcTxActCd7501(String calcTxActCd7501) {
		this.calcTxActCd7501 = calcTxActCd7501;
	}

	/**
	 * @return the calcTaxRate
	 */
	public Double getCalcTaxRate() {
		return calcTaxRate;
	}

	/**
	 * @param calcTaxRate the calcTaxRate to set
	 */
	public void setCalcTaxRate(Double calcTaxRate) {
		this.calcTaxRate = calcTaxRate;
	}

	/**
	 * @return the dolRemovalDifference
	 */
	public Double getDolRemovalDifference() {
		return dolRemovalDifference;
	}

	/**
	 * @param dolRemovalDifference the dolRemovalDifference to set
	 */
	public void setDolRemovalDifference(Double dolRemovalDifference) {
		this.dolRemovalDifference = dolRemovalDifference;
	}

	/**
	 * @return the removalActivity
	 */
	public String getRemovalActivity() {
		return removalActivity;
	}

	/**
	 * @param removalActivity the removalActivity to set
	 */
	public void setRemovalActivity(String removalActivity) {
		this.removalActivity = removalActivity;
	}

	public String getTaxFormId() {
		return taxFormId;
	}

	public void setTaxFormId(String taxFormId) {
		this.taxFormId = taxFormId;
	}

	public Double getAdjATotal() {
		return adjATotal;
	}

	public void setAdjATotal(Double adjATotal) {
		this.adjATotal = adjATotal;
	}

	public Double getAdjBTotal() {
		return adjBTotal;
	}

	public void setAdjBTotal(Double adjBTotal) {
		this.adjBTotal = adjBTotal;
	}

	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public Double getAdjustmentTotal() {
		return adjustmentTotal;
	}

	public void setAdjustmentTotal(Double adjustmentTotal) {
		this.adjustmentTotal = adjustmentTotal;
	}

	public String getEnableAdj() {
		return enableAdj;
	}

	public void setEnableAdj(String enableAdj) {
		this.enableAdj = enableAdj;
	}

	public Double getAdjustmentRemoval() {
		return adjustmentRemoval;
	}

	public void setAdjustmentRemoval(Double adjustmentRemoval) {
		this.adjustmentRemoval = adjustmentRemoval;
	}

	public Double getGrandRemovalTotal() {
		return grandRemovalTotal;
	}

	public void setGrandRemovalTotal(Double grandRemovalTotal) {
		this.grandRemovalTotal = grandRemovalTotal;
	}

	
	
}

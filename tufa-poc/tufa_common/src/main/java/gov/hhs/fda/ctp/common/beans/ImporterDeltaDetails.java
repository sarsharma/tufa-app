package gov.hhs.fda.ctp.common.beans;

/**
 * @author Mohan.Bhutada
 *
 */
public class ImporterDeltaDetails {

    private String period;
    private String periodDelta;
    
    public String getPeriod() {
        return period;
    }
    public void setPeriod(String period) {
        this.period = period;
    }
    public String getPeriodDelta() {
        return periodDelta;
    }
    public void setPeriodDelta(String periodDelta) {
        this.periodDelta = periodDelta;
    }
   
}

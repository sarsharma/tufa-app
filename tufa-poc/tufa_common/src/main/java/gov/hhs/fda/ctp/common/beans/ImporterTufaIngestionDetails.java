package gov.hhs.fda.ctp.common.beans;

import java.util.List;
import java.util.Map;

/**
 * @author Mohan.Bhutada
 *
 */
public class ImporterTufaIngestionDetails {
    
    
    private List<TrueUpComparisonImporterDetail> tufaDetails;
    private List<TrueUpComparisonImporterIngestionDetail> ingestedDetails;
    private List<ImporterDeltaDetails> detailDeltas;
    private String tufaTotal;
    private String detailTotal;
    private String deltaTotal;
    private String dataType;
    private Map<String,String> mixedActionDetailsMap;
    private String tufaRemovalQunatity;
    private String ingestedRemovalQunatity;
    private String mixedActionTax;
    private String mixedActionQuantityRemoved;
    public List<TrueUpComparisonImporterDetail> getTufaDetails() {
        return tufaDetails;
    }
    public void setTufaDetails(List<TrueUpComparisonImporterDetail> tufaDetails) {
        this.tufaDetails = tufaDetails;
    }
    public List<TrueUpComparisonImporterIngestionDetail> getIngestedDetails() {
        return ingestedDetails;
    }
    public void setIngestedDetails(
            List<TrueUpComparisonImporterIngestionDetail> ingestedDetails) {
        this.ingestedDetails = ingestedDetails;
    }
    public List<ImporterDeltaDetails> getDetailDeltas() {
        return detailDeltas;
    }
    public void setDetailDeltas(List<ImporterDeltaDetails> detailDeltas) {
        this.detailDeltas = detailDeltas;
    }
    public String getTufaTotal() {
        return tufaTotal;
    }
    public void setTufaTotal(String tufaTotal) {
        this.tufaTotal = tufaTotal;
    }
    public String getDetailTotal() {
        return detailTotal;
    }
    public void setDetailTotal(String detailTotal) {
        this.detailTotal = detailTotal;
    }
    public String getDeltaTotal() {
        return deltaTotal;
    }
    public void setDeltaTotal(String deltaTotal) {
        this.deltaTotal = deltaTotal;
    }
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public Map<String, String> getMixedActionDetailsMap() {
		return mixedActionDetailsMap;
	}
	public void setMixedActionDetailsMap(Map<String, String> mixedActionDetailsMap) {
		this.mixedActionDetailsMap = mixedActionDetailsMap;
	}
	public String getTufaRemovalQunatity() {
		return tufaRemovalQunatity;
	}
	public void setTufaRemovalQunatity(String tufaRemovalQunatity) {
		this.tufaRemovalQunatity = tufaRemovalQunatity;
	}
	public String getIngestedRemovalQunatity() {
		return ingestedRemovalQunatity;
	}
	public void setIngestedRemovalQunatity(String ingestedRemovalQunatity) {
		this.ingestedRemovalQunatity = ingestedRemovalQunatity;
	}
	public String getMixedActionTax() {
		return mixedActionTax;
	}
	public void setMixedActionTax(String mixedActionTax) {
		this.mixedActionTax = mixedActionTax;
	}
	public String getMixedActionQuantityRemoved() {
		return mixedActionQuantityRemoved;
	}
	public void setMixedActionQuantityRemoved(String mixedActionQuantityRemoved) {
		this.mixedActionQuantityRemoved = mixedActionQuantityRemoved;
	}
   
    
    
    
}

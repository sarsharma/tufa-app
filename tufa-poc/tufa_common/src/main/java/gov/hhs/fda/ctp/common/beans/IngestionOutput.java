/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tgunter
 *
 */
public class IngestionOutput {
	private long fiscalYear;
	private long errorCount;
	private List<DocUpload> output;
	
	public IngestionOutput(){
		this.output = new ArrayList<DocUpload>();
		this.errorCount = 0;
	}
	
	public IngestionOutput(long fiscalYear) {
		super();
		this.fiscalYear = fiscalYear;
		this.output = new ArrayList<DocUpload>();
		this.errorCount = 0;
	}
	/**
	 * @return the fiscalYear
	 */
	public long getFiscalYear() {
		return fiscalYear;
	}
	/**
	 * @param fiscalYear the fiscalYear to set
	 */
	public void setFiscalYear(long fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	/**
	 * @return the output
	 */
	public List<DocUpload> getOutput() {
		return output;
	}
	/**
	 * @param output the output to set
	 */
	public void setOutput(List<DocUpload> output) {
		this.output = output;
	}
	/**
	 * Add a docupload object to the list of outputs.
	 * @param row
	 */
	public void add(DocUpload docUpload) {
		this.output.add(docUpload);
	}
	/**
	 * @return the errorCount
	 */
	public long getErrorCount() {
		return errorCount;
	}
	/**
	 * Increment the error count.
	 */
	public void incrementErrorCount() {
		this.errorCount++;
	}
}

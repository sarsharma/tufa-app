package gov.hhs.fda.ctp.common.beans;


/**
 * @author Priti Upadhyaya
 *
 */
public class IngestionWorkflowContext {

	private long ingestionWorkflowId;
	private long fiscalYr;
	private String context;
	
	public long getIngestionWorkflowId() {
		return ingestionWorkflowId;
	}
	public void setIngestionWorkflowId(long ingestionWorkflowId) {
		this.ingestionWorkflowId = ingestionWorkflowId;
	}
	public long getFiscalYr() {
		return fiscalYr;
	}
	public void setFiscalYr(long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	
	

}
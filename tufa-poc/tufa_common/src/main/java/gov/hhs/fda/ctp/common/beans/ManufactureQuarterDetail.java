package gov.hhs.fda.ctp.common.beans;

import java.util.List;
import java.util.Map;

public class ManufactureQuarterDetail {
	
	private int quarter;
	private String tufaTobaccoClass1Total;
	private String tufaTobaccoClass2Total;
	private String tufaTotal;
	private String ingestedTotal;
	private String delta;
	private List<TrueUpComparisonManufacturerDetail> permits;
	private String dataType;
	private Map<String,String> mixedActionDetailsMap;
	private List<TrueUpComparisonManuDetail> tufaDetails;
	private List<TrueUpComparisonManuIngestionDetail> ingestedDetails; 
	private double mixedActionQuantityRemoved;
	private double mixedActionTax;
	
	public double getMixedActionQuantityRemoved() {
		return mixedActionQuantityRemoved;
	}

	public void setMixedActionQuantityRemoved(double mixedActionQuantityRemoved) {
		this.mixedActionQuantityRemoved = mixedActionQuantityRemoved;
	}

	public double getMixedActionTax() {
		return mixedActionTax;
	}

	public void setMixedActionTax(double mixedActionTax) {
		this.mixedActionTax = mixedActionTax;
	}

	public Map<String, String> getMixedActionDetailsMap() {
		return mixedActionDetailsMap;
	}

	public void setMixedActionDetailsMap(Map<String, String> mixedActionDetailsMap) {
		this.mixedActionDetailsMap = mixedActionDetailsMap;
	}
	public List<TrueUpComparisonManuDetail> getTufaDetails() {
		return tufaDetails;
	}

	public void setTufaDetails(List<TrueUpComparisonManuDetail> tufaDetails) {
		this.tufaDetails = tufaDetails;
	}

	public List<TrueUpComparisonManuIngestionDetail> getIngestedDetails() {
		return ingestedDetails;
	}

	public void setIngestedDetails(List<TrueUpComparisonManuIngestionDetail> ingestedDetails) {
		this.ingestedDetails = ingestedDetails;
	}

	public ManufactureQuarterDetail() {}

	public int getQuarter() {
		return quarter;
	}

	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}

	public String getTufaTobaccoClass1Total() {
		return tufaTobaccoClass1Total;
	}

	public void setTufaTobaccoClass1Total(String tufaTobaccoClass1Total) {
		this.tufaTobaccoClass1Total = tufaTobaccoClass1Total;
	}

	public String getTufaTobaccoClass2Total() {
		return tufaTobaccoClass2Total;
	}

	public void setTufaTobaccoClass2Total(String tufaTobaccoClass2Total) {
		this.tufaTobaccoClass2Total = tufaTobaccoClass2Total;
	}

	public String getTufaTotal() {
		return tufaTotal;
	}

	public void setTufaTotal(String tufaTotal) {
		this.tufaTotal = tufaTotal;
	}

	public String getIngestedTotal() {
		return ingestedTotal;
	}

	public void setIngestedTotal(String ingestedTotal) {
		this.ingestedTotal = ingestedTotal;
	}
	
	public String getDelta() {
		return delta;
	}

	public void setDelta(String delta) {
		this.delta = delta;
	}

	public List<TrueUpComparisonManufacturerDetail> getPermits() {
		return permits;
	}

	public void setPermits(List<TrueUpComparisonManufacturerDetail> permits) {
		this.permits = permits;
		this.updateTotals();
	}
	
	private void  updateTotals() {
		this.ingestedTotal = "NA";
		this.tufaTobaccoClass1Total = "NA";
		this.tufaTobaccoClass2Total = "NA";
		this.tufaTotal = "NA";
		
		if(this.permits == null || this.permits.size() == 0) {
			return;
		} else {
			this.permits.forEach( permit -> {
				permit.setPeriodTotalCombined(this.sumStrings(permit.getPeriodTotal(), permit.getPeriodTotal2()));
				permit.setDelta(this.diffStrings(permit.getPeriodTotalCombined(), permit.getIngestedTax()));
				this.setTufaTobaccoClass1Total(this.sumStrings(this.tufaTobaccoClass1Total, permit.getPeriodTotal()));
				this.setTufaTobaccoClass2Total(this.sumStrings(this.tufaTobaccoClass2Total, permit.getPeriodTotal2()));
				this.setTufaTotal(this.sumStrings(this.tufaTotal, permit.getPeriodTotalCombined()));
				this.setIngestedTotal(this.sumStrings(this.ingestedTotal, permit.getIngestedTax()));
			});
			
			this.setDelta(this.diffStrings(this.tufaTotal, this.ingestedTotal));
			if(this.ingestedTotal.equalsIgnoreCase("NA") && this.tufaTotal.equalsIgnoreCase("NA")) {
				this.setDataType("NA");
			}else if(this.ingestedTotal.equalsIgnoreCase("NA") || this.tufaTotal.equalsIgnoreCase("NA")) {
				
				if(this.ingestedTotal.equalsIgnoreCase("NA")) {
					this.setDataType("TUFA");
				} else if(this.tufaTotal.equalsIgnoreCase("NA")) {
					this.setDataType("INGESTED");
				} else {
					this.setDataType("MATCHED");
				}
			} else {
				this.setDataType("MATCHED");
			}
			
			
		}
	}
	
	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	private String sumStrings(String var1, String var2) {
		if("NA".equals(var1) || var1 == null) {
			return var2 == null ? "NA" : var2;
		} else if ("NA".equals(var2) || var2 == null) {
			return var1 == null ? "NA" : var1;
		} else {
			return String.format("%.2f",Double.valueOf(var1) + Double.valueOf(var2));
		}
	}
	
	private String diffStrings(String var1, String var2) {
		if("NA".equals(var1) && "NA".equals(var2)) {
			return var1;
		} else if ("NA".equals(var1)) {
			return Double.toString(-1 * Double.valueOf(var2));
		} else if ("NA".equals(var2)) {
			return var1;
		} else {
			return String.format("%.2f",Double.valueOf(var1) - Double.valueOf(var2));
		}
	}
}

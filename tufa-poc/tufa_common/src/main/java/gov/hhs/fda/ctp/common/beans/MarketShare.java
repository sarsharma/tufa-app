/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;


/**
 * @author tgunter
 *
 */
public class MarketShare {
	private long assessmentId;
	private long versionNum;
	private long msRank;
	private long assessmentYr;
	private long assessmentQtr;
	private String legalName;
	private String author;
	private String EIN;
	private String permitNum;
	private double totVolRemoved;
	private double totTaxesPaid;
	private double shareTotTaxes;
	private Double previousShare;
	private Double deltaShare;
	private String streetAddress;
	private String suite;
	private String city;
	private String state;
	private String province;
	private String postalCd;
	private String cntryDialCd;
	private String phoneNum;
	private String emailAddress;
	private String tobaccoClassNm;
//	private String type;
	private String addressChangedFlag;
	private String contactChangedFlag;
	private String originalCorrected;
	private String createdDt;
	
	
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param versionNum the versionNum to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	
	
	/**
	 * @return the assessmentId
	 */
	public long getAssessmentId() {
		return assessmentId;
	}
	/**
	 * @param assessmentId the assessmentId to set
	 */
	public void setAssessmentId(long assessmentId) {
		this.assessmentId = assessmentId;
	}
	/**
	 * @return the versionNum
	 */
	public long getVersionNum() {
		return versionNum;
	}
	/**
	 * @param versionNum the versionNum to set
	 */
	public void setVersionNum(long versionNum) {
		this.versionNum = versionNum;
	}
	
	
	
	/**
	 * @return the msRank
	 */
	public long getMsRank() {
		return msRank;
	}
	/**
	 * @param msRank the msRank to set
	 */
	public void setMsRank(long msRank) {
		this.msRank = msRank;
	}
	/**
	 * @return the assessmentYr
	 */
	public long getAssessmentYr() {
		return assessmentYr;
	}
	/**
	 * @param assessmentYr the assessmentYr to set
	 */
	public void setAssessmentYr(long assessmentYr) {
		this.assessmentYr = assessmentYr;
	}
	/**
	 * @return the assessmentQtr
	 */
	public long getAssessmentQtr() {
		return assessmentQtr;
	}
	/**
	 * @param assessmentQtr the assessmentQtr to set
	 */
	public void setAssessmentQtr(long assessmentQtr) {
		this.assessmentQtr = assessmentQtr;
	}
	/**
	 * @return the legalName
	 */
	public String getLegalName() {
		return legalName;
	}
	/**
	 * @param legalName the legalName to set
	 */
	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}
	/**
	 * @return the eIN
	 */
	public String getEIN() {
		return EIN;
	}
	/**
	 * @param eIN the eIN to set
	 */
	public void setEIN(String eIN) {
		EIN = eIN;
	}
	/**
	 * @return the permitNum
	 */
	public String getPermitNum() {
		return permitNum;
	}
	/**
	 * @param permitNum the permitNum to set
	 */
	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}
	/**
	 * @return the totVolRemoved
	 */
	public double getTotVolRemoved() {
		return totVolRemoved;
	}
	/**
	 * @param totVolRemoved the totVolRemoved to set
	 */
	public void setTotVolRemoved(double totVolRemoved) {
		this.totVolRemoved = totVolRemoved;
	}
	/**
	 * @return the totTaxesPaid
	 */
	public double getTotTaxesPaid() {
		return totTaxesPaid;
	}
	/**
	 * @param totTaxesPaid the totTaxesPaid to set
	 */
	public void setTotTaxesPaid(double totTaxesPaid) {
		this.totTaxesPaid = totTaxesPaid;
	}
	/**
	 * @return the shareTotTaxes
	 */
	public double getShareTotTaxes() {
		return shareTotTaxes;
	}
	/**
	 * @param shareTotTaxes the shareTotTaxes to set
	 */
	public void setShareTotTaxes(double shareTotTaxes) {
		this.shareTotTaxes = shareTotTaxes;
	}
	public Double getPreviousShare() {
		return previousShare;
	}
	public void setPreviousShare(Double previousShare) {
		this.previousShare = previousShare;
	}
	public Double getDeltaShare() {
		return deltaShare;
	}
	public void setDeltaShare(Double deltaShare) {
		this.deltaShare = deltaShare;
	}
	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}
	/**
	 * @param streetAddress the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	/**
	 * @return the suite
	 */
	public String getSuite() {
		return suite;
	}
	/**
	 * @param suite the suite to set
	 */
	public void setSuite(String suite) {
		this.suite = suite;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}
	/**
	 * @param province the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}
	/**
	 * @return the postalCd
	 */
	public String getPostalCd() {
		return postalCd;
	}
	/**
	 * @param postalCd the postalCd to set
	 */
	public void setPostalCd(String postalCd) {
		this.postalCd = postalCd;
	}
	/**
	 * @return the cntryDialCd
	 */
	public String getCntryDialCd() {
		return cntryDialCd;
	}
	/**
	 * @param cntryDialCd the cntryDialCd to set
	 */
	public void setCntryDialCd(String cntryDialCd) {
		this.cntryDialCd = cntryDialCd;
	}
	/**
	 * @return the phoneNum
	 */
	public String getPhoneNum() {
		return phoneNum;
	}
	/**
	 * @param phoneNum the phoneNum to set
	 */
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}
	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	/**
	 * @return the tobaccoClassNm
	 */
	public String getTobaccoClassNm() {
		return tobaccoClassNm;
	}
	/**
	 * @param tobaccoClassNm the tobaccoClassNm to set
	 */
	public void setTobaccoClassNm(String tobaccoClassNm) {
		this.tobaccoClassNm = tobaccoClassNm;
	}
	/**
	 * @return the addressChangedFlag
	 */
	public String getAddressChangedFlag() {
		return addressChangedFlag;
	}
	/**
	 * @param addressChangedFlag the addressChangedFlag to set
	 */
	public void setAddressChangedFlag(String addressChangedFlag) {
		this.addressChangedFlag = addressChangedFlag;
	}
	/**
	 * @return the contactChangedFlag
	 */
	public String getContactChangedFlag() {
		return contactChangedFlag;
	}
	/**
	 * @param contactChangedFlag the contactChangedFlag to set
	 */
	public void setContactChangedFlag(String contactChangedFlag) {
		this.contactChangedFlag = contactChangedFlag;
	}
	/**
	 * @return the originalCorrected
	 */
	public String getOriginalCorrected() {
		return originalCorrected;
	}
	/**
	 * @param originalCorrected the originalCorrected to set
	 */
	public void setOriginalCorrected(String originalCorrected) {
		this.originalCorrected = originalCorrected;
	}
	public String getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(String createDt) {
		this.createdDt = createDt;
	}
}

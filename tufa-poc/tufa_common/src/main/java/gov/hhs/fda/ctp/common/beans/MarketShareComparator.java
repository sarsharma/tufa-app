/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.Comparator;

/**
 * @author tgunter
 *
 */
public class MarketShareComparator implements Comparator<MarketShare> {
	public int compare(MarketShare share1, MarketShare share2) {
		Double tax1 = share1.getShareTotTaxes();
		Double tax2 = share2.getShareTotTaxes();
		return tax2.compareTo(tax1);
	}
}

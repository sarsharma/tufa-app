package gov.hhs.fda.ctp.common.beans;

public class NewCompaniesExport {
	
	private String title;
	private String csv;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCsv() {
		return csv;
	}
	public void setCsv(String csv) {
		this.csv = csv;
	}
	
}
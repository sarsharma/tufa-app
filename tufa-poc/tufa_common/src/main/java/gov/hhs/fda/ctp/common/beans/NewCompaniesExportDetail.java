package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

public class NewCompaniesExportDetail
{
	 /** The company name. */
	private String companyName;
	
	/** The company EIN. */
	private String companyEIN;
	
	/** Company added date **/
	private Date createdDate;
	
	/** Welcome Letter sent Date **/
	private Date welcomeLetterSentDate;
	
		
	/** Primary address of the company **/
	private String companyPrimAddress;
	
	/** List of email ids **/
	private String emailId;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyEIN() {
		return companyEIN;
	}

	public void setCompanyEIN(String companyEIN) {
		this.companyEIN = companyEIN;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getWelcomeLetterSentDate() {
		return welcomeLetterSentDate;
	}

	public void setWelcomeLetterSentDate(Date welcomeLetterSentDate) {
		this.welcomeLetterSentDate = welcomeLetterSentDate;
	}



	public String getCompanyPrimAddress() {
		return companyPrimAddress;
	}

	public void setCompanyPrimAddress(String companyPrimAddress) {
		this.companyPrimAddress = companyPrimAddress;
	}
	
    
   	
}
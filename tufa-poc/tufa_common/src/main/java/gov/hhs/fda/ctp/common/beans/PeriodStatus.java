package gov.hhs.fda.ctp.common.beans;

public class PeriodStatus {

	private Long permitStatusId;
    
    private Long permitId;
    
    private Long periodId;
    
    private String periodStatusTypeCd;
    
    private String reconStatusTypeCd;
    
    private String statusTypeTypeCd;
        
    private String createdDt;
    
    private String createdBy;
    
    private String modifiedDt;
    
    private String modifiedBy;
    
    private String reconStatusModifiedBy;
    
    private String reconStatusModifiedDt;

	public Long getPermitStatusId() {
		return permitStatusId;
	}

	public void setPermitStatusId(Long permitStatusId) {
		this.permitStatusId = permitStatusId;
	}

	public Long getPermitId() {
		return permitId;
	}

	public void setPermitId(Long permitId) {
		this.permitId = permitId;
	}

	public Long getPeriodId() {
		return periodId;
	}

	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	public String getPeriodStatusTypeCd() {
		return periodStatusTypeCd;
	}

	public void setPeriodStatusTypeCd(String periodStatusTypeCd) {
		this.periodStatusTypeCd = periodStatusTypeCd;
	}

	public String getStatusTypeTypeCd() {
		return statusTypeTypeCd;
	}

	public void setStatusTypeTypeCd(String statusTypeTypeCd) {
		this.statusTypeTypeCd = statusTypeTypeCd;
	}

	public String getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}

	public String getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(String modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getReconStatusTypeCd() {
		return reconStatusTypeCd;
	}

	public void setReconStatusTypeCd(String reconStatusTypeCd) {
		this.reconStatusTypeCd = reconStatusTypeCd;
	}

	public String getReconStatusModifiedBy() {
		return reconStatusModifiedBy;
	}

	public void setReconStatusModifiedBy(String reconStatusModifiedBy) {
		this.reconStatusModifiedBy = reconStatusModifiedBy;
	}

	public String getReconStatusModifiedDt() {
		return reconStatusModifiedDt;
	}

	public void setReconStatusModifiedDt(String reconStatusModifiedDt) {
		this.reconStatusModifiedDt = reconStatusModifiedDt;
	}
    
}

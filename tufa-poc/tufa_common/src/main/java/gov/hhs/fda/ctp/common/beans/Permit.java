package gov.hhs.fda.ctp.common.beans;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import gov.hhs.fda.ctp.common.validators.NoFutureDates;


/**
 * The Class Permit.
 *
 * @author tgunter
 */
public class Permit extends BaseTufaBean {
    
    /** The permit id. */
    private long permitId;
    
    /** The permit num. */
    @NotEmpty(message="Missing permit number.")
    @Size(min=1, max=11, message="Permit number must be between 1 and 11 characters.")
    private String permitNum;
    
    /** The issue dt. */
    @NoFutureDates(message="Active date cannot be future date.")
    private String issueDt;
    
    /** The close dt. */
    private String closeDt;
    
    /** The TTB START dt. */   
    private String ttbstartDt;
    
    /** The TTB  END  dt. */   
    private String ttbendDt;
    
    /** The permit type cd. */
    private String permitTypeCd;
    
    /** The permit status type cd. */
    private String permitStatusTypeCd;
    
    /** Permit comments */
    private List<PermitHistCommentsBean> permitComments;
    
    /** The company id. */
    @NotNull(message="Permit requires a company.")
    private long companyId;
    
    /** The company. */
    private Company company;
    
    /** Total permit history records for pagination */
    private long permitHistoryCount;
    
    private boolean duplicateFlag;
    
    /** The contacts. */
    /* points of contact */
    private List<Contact> contacts;
    
    /** The permit history list. */
    /* permit history - this will only be populated if requested */
    private List<CompanyPermitHistory> permitHistoryList;
    
    /** The tobacco classes. */
    /* tobacco classes (attach for manufacturer / importer forms */
    private List<TobaccoClass> tobaccoClasses;
    
    /** The permit periods. */
    // not mapped on entity side, currently for unit testing purposes only
    private List<PermitPeriod> permitPeriods;
    
    // private long companyContactId; SKIP FOR NOW
    
    private String tpbdDt;
    
    
    private PermitAudit permitAudit;
    
    private String permitAction;
    
    
    /**
     * CTPTUFA-5256- Company Details Page: Add a flag/indicator to the Permits Panel to indicate if a Permit is Excluded
     */
    private boolean permitExcluded;
    
    private String dateExcluded;
    

    /**
     * Gets the permit id.
     *
     * @return the permitId
     */
    public long getPermitId() {
        return permitId;
    }
    
    /**
     * Sets the permit id.
     *
     * @param permitId the permitId to set
     */
    public void setPermitId(long permitId) {
        this.permitId = permitId;
    }
    
    /**
     * Gets the permit num.
     *
     * @return the permitNum
     */
    public String getPermitNum() {
        return permitNum;
    }
    
    /**
     * Sets the permit num.
     *
     * @param permitNum the permitNum to set
     */
    public void setPermitNum(String permitNum) {
        this.permitNum = permitNum;
    }
    
    /**
     * Gets the issue dt.
     *
     * @return the issueDt
     */
    public String getIssueDt() {
        return issueDt;
    }
    
    /**
     * Sets the issue dt.
     *
     * @param issueDt the issueDt to set
     */
    public void setIssueDt(String issueDt) {
        this.issueDt = issueDt;
    }
    
    /**
     * Gets the close dt.
     *
     * @return the closeDt
     */
    public String getCloseDt() {
        return closeDt;
    }
    
    /**
     * Sets the close dt.
     *
     * @param closeDt the closeDt to set
     */
    public void setCloseDt(String closeDt) {
        this.closeDt = closeDt;
    }
    
    
    /**
     * Gets the ttb start dt.
     *
     * @return the ttb start dt.
     */
    public String getttbstartDt() {
        return ttbstartDt;
    }
    
    /**
     * Sets the ttbstart dt.
     *
     * @param ttbstart the new ttbstart dt
     */
    public void setttbstartDt(String ttbstartDt) {
        this.ttbstartDt = ttbstartDt;
    }
    
    /**
     * Gets the close dt.
     *
     * @return the close dt
     */
    public String getttbendDt() {
        return ttbendDt;
    }
    
    /**
     * Sets the close dt.
     *
     * @param closeDt the new close dt
     */
    public void setttbendDt(String ttbendDt) {
        this.ttbendDt = ttbendDt;
    }
    
    
    /**
     * Gets the permit type cd.
     *
     * @return the permitTypeCd
     */
    public String getPermitTypeCd() {
        return permitTypeCd;
    }
    
    /**
     * Sets the permit type cd.
     *
     * @param permitTypeCd the permitTypeCd to set
     */
    public void setPermitTypeCd(String permitTypeCd) {
        this.permitTypeCd = permitTypeCd;
    }
    
    /**
     * Gets the permit status type cd.
     *
     * @return the permitStatusTypeCd
     */
    public String getPermitStatusTypeCd() {
        return permitStatusTypeCd;
    }
    
    /**
     * Sets the permit status type cd.
     *
     * @param permitStatusTypeCd the permitStatusTypeCd to set
     */
    public void setPermitStatusTypeCd(String permitStatusTypeCd) {
        this.permitStatusTypeCd = permitStatusTypeCd;
    }
    
    /**
	 * @return the permitComments
	 */
	public List<PermitHistCommentsBean> getPermitComments() {
		return permitComments;
	}

	/**
	 * @param permitComments the permitComments to set
	 */
	public void setPermitComments(List<PermitHistCommentsBean> permitComments) {
		this.permitComments = permitComments;
	}

	/**
     * Gets the company id.
     *
     * @return the companyId
     */
    public long getCompanyId() {
        return companyId;
    }
    
    /**
     * Sets the company id.
     *
     * @param companyId the companyId to set
     */
    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }
    
    /**
     * Get the company for this permit.
     *
     * @return the company
     */
	public Company getCompany() {
		return company;
	}
	
	/**
	 * Set the company on this permit.
	 *
	 * @param company the new company
	 */
	public void setCompany(Company company) {
		this.company = company;
	}
	
    public long getPermitHistoryCount() {
		return permitHistoryCount;
	}

	public void setPermitHistoryCount(long permitHistoryCount) {
		this.permitHistoryCount = permitHistoryCount;
	}

	/**
     * Gets the permit history list.
     *
     * @return the permitHistoryList
     */
    public List<CompanyPermitHistory> getPermitHistoryList() {
        return permitHistoryList;
    }

    /**
     * Sets the permit history list.
     *
     * @param permitHistoryList the permitHistoryList to set
     */
    public void setPermitHistoryList(List<CompanyPermitHistory> permitHistoryList) {
        this.permitHistoryList = permitHistoryList;
    }
    
    /**
     * Gets the contacts.
     *
     * @return the contacts
     */
    public List<Contact> getContacts() {
        return contacts;
    }
    
    /**
     * Sets the contacts.
     *
     * @param contacts the contacts to set
     */
    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }
    
    /**
     * Gets the tobacco classes.
     *
     * @return the tobaccoClasses
     */
    public List<TobaccoClass> getTobaccoClasses() {
        return tobaccoClasses;
    }
    
    /**
     * Sets the tobacco classes.
     *
     * @param tobaccoClasses the tobaccoClasses to set
     */
    public void setTobaccoClasses(List<TobaccoClass> tobaccoClasses) {
        this.tobaccoClasses = tobaccoClasses;
    }
    
    /**
     * Gets the permit periods.
     *
     * @return the permitPeriods
     */
    public List<PermitPeriod> getPermitPeriods() {
        return permitPeriods;
    }
    
    /**
     * Sets the permit periods.
     *
     * @param permitPeriods the permitPeriods to set
     */
    public void setPermitPeriods(List<PermitPeriod> permitPeriods) {
        this.permitPeriods = permitPeriods;
    }

	public String getTpbdDt() {
		return tpbdDt;
	}

	public void setTpbdDt(String tpbdDt) {
		this.tpbdDt = tpbdDt;
	}

	public boolean getDuplicateFlag() {
		return this.duplicateFlag;
	}
	
	public void setDuplicateFlag(boolean flag) {
		this.duplicateFlag = flag;
	}

	public PermitAudit getPermitAudit() {
		return permitAudit;
	}

	public void setPermitAudit(PermitAudit permitAudit) {
		this.permitAudit = permitAudit;
	}

	public String getPermitAction() {
		return permitAction;
	}

	public void setPermitAction(String permitAction) {
		this.permitAction = permitAction;
	}

    public boolean isPermitExcluded() {
        return permitExcluded;
    }

    public void setPermitExcluded(boolean permitExcluded) {
        this.permitExcluded = permitExcluded;
    }

    public String getDateExcluded() {
        return dateExcluded;
    }

    public void setDateExcluded(String dateExcluded) {
        this.dateExcluded = dateExcluded;
    }

	

}

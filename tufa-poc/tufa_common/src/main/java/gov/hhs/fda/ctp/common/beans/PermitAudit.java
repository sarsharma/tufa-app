package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

public class PermitAudit {
	
	

	public PermitAudit() {
	}

	public PermitAudit(Long auditId, String companyName,Long companyId, String einNum, String permitId,
			String permitAction, String permitStatus, Long docStageId, String issueDt, String closeDt, String actionMsg,
			String action, String permitExclResolved, String actionContext ) {

		this.permitAuditId = auditId;
		this.companyName = companyName;
		this.companyId = companyId;
		this.docId = docStageId;
		this.einNum = einNum;
		this.permitNum = permitId;
		this.permitStatus = permitStatus;
		this.permitAction = permitAction;
		this.closeDt = closeDt;
		this.issueDt = issueDt;
		this.action = action;
		this.actionMsg = actionMsg;
		this.actionContext = actionContext;
		this.permitExclResolved = "Y".equalsIgnoreCase(permitExclResolved)?true:false;
	}

	public PermitAudit(String auditId, String companyName,String companyCreateDt, String companyId,String docStageId, String einNum,
			String permitId, String permitStatus, String permitAction, String companyStatus, String companyAction,
			String closeDt, String issueDt, String actionDt, String createdDt, String errors,
			String permitStatusChangeAction, String permitIdTufa, String companyStatusChangeAction, String permitTypeCd,
			String permitStatusTufa, String pPermitId, String pPermitNum, String pPermitStatusTypeCd, String pIssueDt,
			String pCloseDt, String cLegalNm, String cCompanyStatus, String rpLastRptDt, String pTTBStartDt,
			String pTTBEndDt) {

		this.permitAuditId = Long.valueOf(auditId);
		this.companyName = companyName;
		this.companyCreateDt = companyCreateDt;
		this.companyId = Long.valueOf(companyId);
		this.docId = Long.valueOf(docStageId);
		this.einNum = einNum;
		this.permitNum = permitId;
		this.permitStatus = permitStatus;
		this.permitAction = permitAction;
		this.companyStatus = companyStatus;
		this.companyAction = companyAction;
		this.closeDt = closeDt;
		this.issueDt = issueDt;
		this.actionDt = actionDt;
		this.createdDt = createdDt;
		this.errors = errors;
		this.permitStatusChangeAction = permitStatusChangeAction;
		this.setLastRptDt(rpLastRptDt);
		this.setCompanyCreateDt(companyCreateDt);
		
		if (permitIdTufa != null) {
			this.permitIdTufa = Long.valueOf(permitIdTufa);
			this.permitTufa = new Permit();
			this.permitTufa.setPermitId(Long.valueOf(pPermitId));
			this.permitTufa.setPermitNum(pPermitNum);
			this.permitTufa.setPermitStatusTypeCd(pPermitStatusTypeCd);
			this.permitTufa.setIssueDt(pIssueDt);
			this.permitTufa.setCloseDt(pCloseDt);
			this.permitTufa.setttbstartDt(pTTBStartDt);
			this.permitTufa.setttbendDt(pTTBEndDt);

		}

		this.companyStatusChangeAction = companyStatusChangeAction;
		this.permitTypeCd = permitTypeCd;
		this.permitStatusTufa = permitStatusTufa;

		if (this.companyId != null) {
			this.companyTufa = new Company();
			this.companyTufa.setCompanyId(this.companyId);
			this.companyTufa.setLegalName(cLegalNm);
			this.companyTufa.setCompanyStatus(cCompanyStatus);
			this.companyTufa.setcompanyCreateDt(companyCreateDt);
			
			
		}
	}


	private Long permitAuditId;

	private String companyName;
	
	private String companyCreateDt;

	private Long companyId;

	private String einNum;

	private String permitNum;

	private String permitAction;

	private String permitStatus;

	private String permitStatusTufa;

	private Long docId;

	private String issueDt;

	private String closeDt;

	private String createdDt;

	private String actionDt;

	private String errors;

	private String companyStatus;

	private String companyAction;

	private String permitTypeCd;

	private Long permitIdTufa;

	private Permit permitTufa;

	private Company companyTufa;

	private String permitStatusChangeAction;

	private String companyStatusChangeAction;

	private String lastRptDt;

	// fields from TTB_PERMIT_LIST_VW view
	private String actionMsg;

	private String action;

	private String actionContext;

	private boolean permitExclResolved;

	
	public Long getPermitAuditId() {
		return permitAuditId;
	}

	public void setPermitAuditId(Long permitAuditId) {
		this.permitAuditId = permitAuditId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getEinNum() {
		return einNum;
	}

	public void setEinNum(String einNum) {
		this.einNum = einNum;
	}

	public String getPermitNum() {
		return permitNum;
	}

	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}

	public String getPermitAction() {
		return permitAction;
	}

	public void setPermitAction(String permitAction) {
		this.permitAction = permitAction;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getPermitStatus() {
		return permitStatus;
	}

	public void setPermitStatus(String permitStatus) {
		this.permitStatus = permitStatus;
	}

	public String getPermitStatusTufa() {
		return permitStatusTufa;
	}

	public void setPermitStatusTufa(String permitStatusTufa) {
		this.permitStatusTufa = permitStatusTufa;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public String getCompanyStatus() {
		return companyStatus;
	}

	public void setCompanyStatus(String companyStatus) {
		this.companyStatus = companyStatus;
	}

	public String getCompanyAction() {
		return companyAction;
	}

	public void setCompanyAction(String companyAction) {
		this.companyAction = companyAction;
	}

	public Permit getPermitTufa() {
		return permitTufa;
	}

	public void setPermitTufa(Permit permitTufa) {
		this.permitTufa = permitTufa;
	}

	public Company getCompanyTufa() {
		return companyTufa;
	}

	public String getPermitStatusChangeAction() {
		return permitStatusChangeAction;
	}

	public void setPermitStatusChangeAction(String permitStatusChangeAction) {
		this.permitStatusChangeAction = permitStatusChangeAction;
	}

	public String getCompanyStatusChangeAction() {
		return companyStatusChangeAction;
	}

	public void setCompanyStatusChangeAction(String companyStatusChangeAction) {
		this.companyStatusChangeAction = companyStatusChangeAction;
	}

	public void setCompanyTufa(Company companyTufa) {
		this.companyTufa = companyTufa;
	}

	public String getIssueDt() {
		return issueDt;
	}

	public void setIssueDt(String issueDt) {
		this.issueDt = issueDt;
	}

	public String getCloseDt() {
		return closeDt;
	}

	public void setCloseDt(String closeDt) {
		this.closeDt = closeDt;
	}

	public String getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}

	public String getActionDt() {
		return actionDt;
	}

	public void setActionDt(String actionDt) {
		this.actionDt = actionDt;
	}

	public Long getPermitIdTufa() {
		return permitIdTufa;
	}

	public void setPermitIdTufa(Long permitIdTufa) {
		this.permitIdTufa = permitIdTufa;
	}

	public String getPermitTypeCd() {
		return permitTypeCd;
	}

	public void setPermitTypeCd(String permitTypeCd) {
		this.permitTypeCd = permitTypeCd;
	}

	public String getLastRptDt() {
		return lastRptDt;
	}

	public void setLastRptDt(String lastRptDt) {
		this.lastRptDt = lastRptDt;
	}

	public String getActionMsg() {
		return actionMsg;
	}

	public void setActionMsg(String actionMsg) {
		this.actionMsg = actionMsg;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getActionContext() {
		return actionContext;
	}

	public void setActionContext(String actionContext) {
		this.actionContext = actionContext;
	}

	public boolean getPermitExclResolved() {
		return permitExclResolved;
	}

	public void setPermitExclResolved(boolean permitExclResolved) {
		 this.permitExclResolved = permitExclResolved;
	}

	public String companyCreateDt() {
		return companyCreateDt;
	}

	public void setCompanyCreateDt(String companyCreateDt) {
		this.companyCreateDt = companyCreateDt;
	}

	

}

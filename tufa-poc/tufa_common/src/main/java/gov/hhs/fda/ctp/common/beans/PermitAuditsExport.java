package gov.hhs.fda.ctp.common.beans;

public class PermitAuditsExport {
	
	String filename;
	String csv;
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getCsv() {
		return csv;
	}
	public void setCsv(String csv) {
		this.csv = csv;
	}
	
	

}

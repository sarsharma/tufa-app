package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

public class PermitCommentAttachments {
    private long documentId;
    private long commentId;
    private String docFileNm;
    private String docStatusCd;
//    private Blob reportPdf;
    private String docDesc;
    private String formTypes;    
    private String createdBy;
    private Date createdDt;
    private String modifiedBy;
    private Date modifiedDt;
    private PermitExcludeComment comment;
	/**
	 * @return the documentId
	 */
	public long getDocumentId() {
		return documentId;
	}
	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}
	/**
	 * @return the commentId
	 */
	public long getCommentId() {
		return commentId;
	}
	/**
	 * @param commentId the commentId to set
	 */
	public void setCommentId(long commentId) {
		this.commentId = commentId;
	}
	/**
	 * @return the docFileNm
	 */
	public String getDocFileNm() {
		return docFileNm;
	}
	/**
	 * @param docFileNm the docFileNm to set
	 */
	public void setDocFileNm(String docFileNm) {
		this.docFileNm = docFileNm;
	}
	/**
	 * @return the docStatusCd
	 */
	public String getDocStatusCd() {
		return docStatusCd;
	}
	/**
	 * @param docStatusCd the docStatusCd to set
	 */
	public void setDocStatusCd(String docStatusCd) {
		this.docStatusCd = docStatusCd;
	}
	/**
	 * @return the docDesc
	 */
	public String getDocDesc() {
		return docDesc;
	}
	/**
	 * @param docDesc the docDesc to set
	 */
	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}
	/**
	 * @return the formTypes
	 */
	public String getFormTypes() {
		return formTypes;
	}
	/**
	 * @param formTypes the formTypes to set
	 */
	public void setFormTypes(String formTypes) {
		this.formTypes = formTypes;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}
	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
	/**
	 * @return the comment
	 */
	public PermitExcludeComment getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(PermitExcludeComment comment) {
		this.comment = comment;
	}
}

package gov.hhs.fda.ctp.common.beans;

import java.util.Date;
import java.util.List;

public class PermitExcludeComment {
//    Common comment properties
	private long commentSeq;
	private String userComment;
    private Boolean resolveComment;
    private String author;
    private Date commentDate;
    private String modifiedBy;
    private Date modifiedDt;
//  Company comment properties
	private Integer attachmentsCount;
	
	/**
	 * @return the commentSeq
	 */
	public long getCommentSeq() {
		return commentSeq;
	}
	/**
	 * @param commentSeq the commentSeq to set
	 */
	public void setCommentSeq(long commentSeq) {
		this.commentSeq = commentSeq;
	}
	/**
	 * @return the userComment
	 */
	public String getUserComment() {
		return userComment;
	}
	/**
	 * @param userComment the userComment to set
	 */
	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}
	/**
	 * @return the resolveComment
	 */
	public Boolean getResolveComment() {
		return resolveComment;
	}
	/**
	 * @param resolveComment the resolveComment to set
	 */
	public void setResolveComment(Boolean resolveComment) {
		this.resolveComment = resolveComment;
	}
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * @return the commentDate
	 */
	public Date getCommentDate() {
		return commentDate;
	}
	/**
	 * @param commentDate the commentDate to set
	 */
	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}
	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
	
	public Integer getAttachmentsCount() {
		return attachmentsCount;
	}
	public void setAttachmentsCount(Integer attachmentsCount) {
		this.attachmentsCount = attachmentsCount;
	}

}

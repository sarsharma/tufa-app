package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

/**
 * @author Mohan.Bhutada
 *
 */
/**
 * @author Mohan.Bhutada
 *
 */
public class PermitHistCommentsBean  {
    
    private String userComment;
    private String author;
    private String commentDate;
    private Boolean resolveComment;
    private String modifiedBy;
    private Date modifiedDt;
    private long permitCommentId;
    public String getUserComment() {
        return userComment;
    }
    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public String getCommentDate() {
        return commentDate;
    }
    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }
    public Boolean getResolveComment() {
        return resolveComment;
    }
    public void setResolveComment(Boolean resolveComment) {
        this.resolveComment = resolveComment;
    }
    public String getModifiedBy() {
        return modifiedBy;
    }
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    public Date getModifiedDt() {
        return modifiedDt;
    }
    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }
    public long getPermitCommentId() {
        return permitCommentId;
    }
    public void setPermitCommentId(long permitCommentId) {
        this.permitCommentId = permitCommentId;
    }
  
    
    

}

/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.Objects;

/**
 * The Class PermitHistory.
 *
 * @author tgunter
 */
public class PermitHistory implements Comparable<PermitHistory> {
    
    /** The report period id. */
    private long reportPeriodId;
    
    /** The permit num. */
    private String permitNum;
    
    /** The month. */
    private String month;
    
    /** The year. */
    private String year;
    
    /** Fiscal year */
    private String fiscalYear;
    
    public String getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	/** The diaplay month year. */
    private String diaplayMonthYear;
    
    /** The sort month year. */
    private String sortMonthYear;
    
    /** The status cd. */
    private String statusCd;
    
    /** The activity. */
    private String activity;
    
    /** The attachments. */
    private String attachments;
    
    /** The attachments id. */
    private long attachmentsId;
    
    /**
     * Gets the report period id.
     *
     * @return the reportPeriodId
     */
    public long getReportPeriodId() {
        return reportPeriodId;
    }
    
    /**
     * Sets the report period id.
     *
     * @param reportPeriodId the reportPeriodId to set
     */
    public void setReportPeriodId(long reportPeriodId) {
        this.reportPeriodId = reportPeriodId;
    }
    
    /**
     * Gets the permit num.
     *
     * @return the permitNum
     */
    public String getPermitNum() {
        return permitNum;
    }
    
    /**
     * Sets the permit num.
     *
     * @param permitNum the permitNum to set
     */
    public void setPermitNum(String permitNum) {
        this.permitNum = permitNum;
    }
    
    /**
     * Gets the month.
     *
     * @return the month
     */
    public String getMonth() {
        return month;
    }
    
    /**
     * Sets the month.
     *
     * @param month the month to set
     */
    public void setMonth(String month) {
        this.month = month;
    }
    
    /**
     * Gets the year.
     *
     * @return the year
     */
    public String getYear() {
        return year;
    }
    
    /**
     * Sets the year.
     *
     * @param string the year to set
     */
    public void setYear(String string) {
        this.year = string;
    }
    
    /**
     * Gets the status cd.
     *
     * @return the statusCd
     */
    public String getStatusCd() {
        return statusCd;
    }
    
    /**
     * Sets the status cd.
     *
     * @param statusCd the statusCd to set
     */
    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
    
    /**
     * Gets the activity.
     *
     * @return the activity
     */
    public String getActivity() {
        return activity;
    }
    
    /**
     * Sets the activity.
     *
     * @param activity the activity to set
     */
    public void setActivity(String activity) {
        this.activity = activity;
    }
    
    /**
     * Gets the attachments.
     *
     * @return the attachments
     */
    public String getAttachments() {
        return attachments;
    }
    
    /**
     * Sets the attachments.
     *
     * @param attachments the attachments to set
     */
    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }
    
    /**
     * Gets the attachments id.
     *
     * @return the attachmentsId
     */
    public long getAttachmentsId() {
        return attachmentsId;
    }
    
    /**
     * Sets the attachments id.
     *
     * @param attachmentsId the attachmentsId to set
     */
    public void setAttachmentsId(long attachmentsId) {
        this.attachmentsId = attachmentsId;
    }
    
    /**
     * Gets the diaplay month year.
     *
     * @return the diaplayMonthYear
     */
    public String getDiaplayMonthYear() {
        return diaplayMonthYear;
    }
    
    /**
     * Sets the diaplay month year.
     *
     * @param diaplayMonthYear the diaplayMonthYear to set
     */
    public void setDiaplayMonthYear(String diaplayMonthYear) {
        this.diaplayMonthYear = diaplayMonthYear;
    }
    
    /**
     * Gets the sort month year.
     *
     * @return the sortMonthYear
     */
    public String getSortMonthYear() {
        return sortMonthYear;
    }
    
    /**
     * Sets the sort month year.
     *
     * @param sortMonthYear the sortMonthYear to set
     */
    public void setSortMonthYear(String sortMonthYear) {
        this.sortMonthYear = sortMonthYear;
    }

    /**
     * Sort month/year will be stored in format YYYYMM.
     *
     * @param permitHistory the permit history
     * @return int
     */
    @Override
    public int compareTo(PermitHistory permitHistory) {
        return this.sortMonthYear.compareTo(permitHistory.getSortMonthYear());
    }
    
    /**
     * Implemented for code compliance.
     *
     * @param o the o
     * @return true, if successful
     */
    @Override
    public boolean equals(Object o) {
        boolean returnVal = false;
        if (o instanceof PermitHistory) {
            PermitHistory permitHistory = (PermitHistory)o;
            returnVal = Objects.equals(this.permitNum, permitHistory.getPermitNum());
        }
        return returnVal;
    }
    
    /**
     * Implemented for code compliance.
     *
     * @return the int
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.permitNum);
    }

}

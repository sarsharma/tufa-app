package gov.hhs.fda.ctp.common.beans;

import java.io.Serializable;
import java.util.List;

/**
 * @author Mohan.Bhutada
 *
 */
public class PermitHistoryCommentResponse extends BaseTufaBean implements Comparable<SubmittedForm>{
    
    List<PermitHistCommentsBean> commentsList;

    public List<PermitHistCommentsBean> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<PermitHistCommentsBean> commentsList) {
        this.commentsList = commentsList;
    }

    @Override
    public int compareTo(SubmittedForm o) {
        // TODO Auto-generated method stub
        return 0;
    }

}

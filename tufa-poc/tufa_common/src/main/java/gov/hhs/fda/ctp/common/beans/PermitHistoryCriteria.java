/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.List;
import java.util.Set;

/**
 * The Class PermitHistoryCriteria.
 *
 * @author tgunter
 */
public class PermitHistoryCriteria {
    
    /** The company name filter. */
    private String companyNameFilter;
    
    /** The permit name filter. */
    private String permitNameFilter;
    
    /** The company id filter. */
    private Long   companyIdFilter;
    
    /** The month filters. */
    private Set<String> monthFilters;
    
    /** The year filters. */
    private Set<String> yearFilters;
    
    /** The permit type filters. */
    private Set<String> permitTypeFilters;
    
    /** The permit status filters. */
    private Set<String> permitStatusFilters;
    
    /** The report status filters. */
    private Set<String> reportStatusFilters;
    
    /** The results. */
    private List<CompanyPermitHistory> results;
    
    private Integer itemsTotal;
    
    private String fiscalYears;
    
    
    /**
     * Gets the results.
     *
     * @return the permitHistoryList
     */
    public List<CompanyPermitHistory> getResults() {
        return results;
    }
    
    /**
     * Sets the results.
     *
     * @param results the new results
     */
    public void setResults(List<CompanyPermitHistory> results) {
        this.results = results;
    }
    
    /**
     * Gets the company id filter.
     *
     * @return the companyIdFilter
     */
    public Long getCompanyIdFilter() {
        return companyIdFilter;
    }
    
    /**
     * Sets the company id filter.
     *
     * @param companyIdFilter the companyIdFilter to set
     */
    public void setCompanyIdFilter(Long companyIdFilter) {
        this.companyIdFilter = companyIdFilter;
    }
    
    /**
     * Gets the month filters.
     *
     * @return the monthFilters
     */
    public Set<String> getMonthFilters() {
        return monthFilters;
    }
    
    /**
     * Sets the month filters.
     *
     * @param monthFilters the monthFilters to set
     */
    public void setMonthFilters(Set<String> monthFilters) {
        this.monthFilters = monthFilters;
    }
    
    /**
     * Gets the year filters.
     *
     * @return the yearFilters
     */
    public Set<String> getYearFilters() {
        return yearFilters;
    }
    
    /**
     * Sets the year filters.
     *
     * @param yearFilters the new year filters
     */
    public void setYearFilters(Set<String> yearFilters) {
        this.yearFilters = yearFilters;
    }    
    
    /**
     * Gets the company name filter.
     *
     * @return the companyNameFilter
     */
    public String getCompanyNameFilter() {
        return companyNameFilter;
    }
    
    /**
     * Sets the company name filter.
     *
     * @param companyNameFilter the companyNameFilter to set
     */
    public void setCompanyNameFilter(String companyNameFilter) {
        this.companyNameFilter = companyNameFilter;
    }
    
    /**
     * Gets the permit name filter.
     *
     * @return the permitNameFilter
     */
    public String getPermitNameFilter() {
        return permitNameFilter;
    }
    
    /**
     * Sets the permit name filter.
     *
     * @param permitNameFilter the permitNameFilter to set
     */
    public void setPermitNameFilter(String permitNameFilter) {
        this.permitNameFilter = permitNameFilter;
    }
    
    /**
     * Gets the permit type filters.
     *
     * @return the permitTypeFilters
     */
    public Set<String> getPermitTypeFilters() {
        return permitTypeFilters;
    }
    
    /**
     * Sets the permit type filters.
     *
     * @param permitTypeFilters the permitTypeFilters to set
     */
    public void setPermitTypeFilters(Set<String> permitTypeFilters) {
        this.permitTypeFilters = permitTypeFilters;
    }
    
    /**
     * Gets the permit status filters.
     *
     * @return the permitStatusFilters
     */
    public Set<String> getPermitStatusFilters() {
        return permitStatusFilters;
    }
    
    /**
     * Sets the permit status filters.
     *
     * @param permitStatusFilters the permitStatusFilters to set
     */
    public void setPermitStatusFilters(Set<String> permitStatusFilters) {
        this.permitStatusFilters = permitStatusFilters;
    }
    
    /**
     * Gets the report status filters.
     *
     * @return the reportStatusFilters
     */
    public Set<String> getReportStatusFilters() {
        return reportStatusFilters;
    }
    
    /**
     * Sets the report status filters.
     *
     * @param reportStatusFilters the reportStatusFilters to set
     */
    public void setReportStatusFilters(Set<String> reportStatusFilters) {
        this.reportStatusFilters = reportStatusFilters;
    }
    
    public Integer getItemsTotal() {
		return itemsTotal;
	}

	public void setItemsTotal(Integer itemsTotal) {
		this.itemsTotal = itemsTotal;
	}

	public String getFiscalYears() {
		return fiscalYears;
	}

	public void setFiscalYears(String fiscalYears) {
		this.fiscalYears = fiscalYears;
	}

	@Override
    public String toString() {
    	StringBuilder builder = new StringBuilder("criteria: ");
    	/** The company name filter. */
    	builder.append("company name like ");
    	builder.append(companyNameFilter);
    	builder.append(", ");
        /** The permit name filter. */
    	builder.append("permit name like ");
    	builder.append(permitNameFilter);
    	builder.append(", ");
        /** The company id filter. */
    	builder.append("company id = ");
    	builder.append(companyIdFilter);
    	builder.append(", ");
        /** The month filters. */
    	if(monthFilters != null) {
	    	builder.append("months = ");
	    	for(String month: monthFilters) {
	    		builder.append(month);
	    		builder.append(" ");
	    	}
	    	builder.append(", ");
    	}
	    /** The year filters. */
    	if(yearFilters != null) {
    		builder.append("years = ");
    		for(String year: yearFilters) {
    			builder.append(year);
    			builder.append(" ");
    		}
    		builder.append(", ");
    	}
    	
        /** The permit type filters. */
    	if(permitTypeFilters != null) {
    		builder.append("permit types = ");
    		for(String permitType: permitTypeFilters) {
    			builder.append(permitType);
    			builder.append(" ");
    		}
    		builder.append(", ");
    	}
        
        /** The permit status filters. */
    	if(permitStatusFilters != null) {
    		builder.append("permit status types = ");
    		for(String permitStatus: permitStatusFilters) {
    			builder.append(permitStatus);
    			builder.append(" ");
    		}
    		builder.append(", ");
    	}
        
        /** The report status filters. */
    	if(reportStatusFilters != null) {
    		builder.append("report status types = ");
    		for(String reportStatus: reportStatusFilters) {
    			builder.append(reportStatus);
    			builder.append(" ");
    		}
    	}
    	builder.append(System.lineSeparator());
    	return builder.toString();
    }
}

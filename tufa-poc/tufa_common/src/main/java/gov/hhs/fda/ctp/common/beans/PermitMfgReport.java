/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.List;

/**
 * The Class PermitMfgReport.
 *
 * @author tgunter
 */
public class PermitMfgReport {
    
    /** The permit id. */
    private Long permitId;
    
    /** The period id. */
    private Long periodId;
    
    /** The status. */
    private String status;
    
    
    private boolean is3852modified;
    
    

	/** The submitted forms. */
    private List<SubmittedForm> submittedForms;
    
    /**
     * Gets the permit id.
     *
     * @return the permitId
     */
    public Long getPermitId() {
        return permitId;
    }
    
    /**
     * Sets the permit id.
     *
     * @param permitId the permitId to set
     */
    public void setPermitId(Long permitId) {
        this.permitId = permitId;
    }
    
    /**
     * Gets the period id.
     *
     * @return the periodId
     */
    public Long getPeriodId() {
        return periodId;
    }
    
    /**
     * Sets the period id.
     *
     * @param periodId the periodId to set
     */
    public void setPeriodId(Long periodId) {
        this.periodId = periodId;
    }
    
    /**
     * Gets the status.
     *
     * @return the statusCd
     */
    public String getStatus() {
        return status;
    }
    
    /**
     * Sets the status.
     *
     * @param status the new status
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
    /**
     * Gets the submitted forms.
     *
     * @return the submittedForms
     */
    public List<SubmittedForm> getSubmittedForms() {
        return submittedForms;
    }
    
    /**
     * Sets the submitted forms.
     *
     * @param submittedForms the submittedForms to set
     */
    public void setSubmittedForms(List<SubmittedForm> submittedForms) {
        this.submittedForms = submittedForms;
    }
    
    public boolean isIs3852modified() {
		return is3852modified;
	}

	public void setIs3852modified(boolean is3852modified) {
		this.is3852modified = is3852modified;
	}
}

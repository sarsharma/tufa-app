/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.List;

/**
 * The Class PermitPeriod.
 *
 * @author tgunter
 */
public class PermitPeriod {
    
    /** The permit id. */
    private Long permitId;
    
    /** The period id. */
    private Long periodId;
    
    /** The month. */
    private String month;
    
    /** The year. */
    private Long year;
    
    /** The quarter. */
    private Long quarter;
    
    /** The status. */
    private String status;
    
    /** The submitted forms. */
    private List<SubmittedForm> submittedForms;
    
    /** The permit. */
    private Permit permit;
    
    private String periodOneTaxId_500024;
    private String periodTwoTaxId_500024;
    private String periodThreeTaxId_500024;
    
    /*  forms */
    /**
     * Gets the permit id.
     *
     * @return the permitId
     */
    public Long getPermitId() {
        return permitId;
    }
    
    /**
     * Sets the permit id.
     *
     * @param permitId the permitId to set
     */
    public void setPermitId(Long permitId) {
        this.permitId = permitId;
    }
    
    /**
     * Gets the period id.
     *
     * @return the periodId
     */
    public Long getPeriodId() {
        return periodId;
    }
    
    /**
     * Sets the period id.
     *
     * @param periodId the periodId to set
     */
    public void setPeriodId(Long periodId) {
        this.periodId = periodId;
    }
    
    /**
     * Gets the month.
     *
     * @return the month
     */
    public String getMonth() {
        return month;
    }
    
    /**
     * Sets the month.
     *
     * @param month the month to set
     */
    public void setMonth(String month) {
        this.month = month;
    }
    
    /**
     * Gets the year.
     *
     * @return the year
     */
    public Long getYear() {
        return year;
    }
    
    /**
     * Sets the year.
     *
     * @param year the year to set
     */
    public void setYear(Long year) {
        this.year = year;
    }
    
    /**
     * Gets the status.
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }
    
    /**
     * Sets the status.
     *
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
    /**
     * Gets the quarter.
     *
     * @return the quarter
     */
    public Long getQuarter() {
        return quarter;
    }
    
    /**
     * Sets the quarter.
     *
     * @param quarter the quarter to set
     */
    public void setQuarter(Long quarter) {
        this.quarter = quarter;
    }
    
    /**
     * Gets the submitted forms.
     *
     * @return the forms
     */
    public List<SubmittedForm> getSubmittedForms() {
        return submittedForms;
    }
    
    /**
     * Sets the submitted forms.
     *
     * @param forms the forms to set
     */
    public void setSubmittedForms(List<SubmittedForm> forms) {
        this.submittedForms = forms;
    }
    
    /**
     * Gets the permit.
     *
     * @return the permit
     */
    public Permit getPermit() {
        return permit;
    }
    
    /**
     * Sets the permit.
     *
     * @param permit the permit to set
     */
    public void setPermit(Permit permit) {
        this.permit = permit;
    }

	public String getPeriodOneTaxId_500024() {
		return periodOneTaxId_500024;
	}

	public void setPeriodOneTaxId_500024(String periodOneTaxId_500024) {
		this.periodOneTaxId_500024 = periodOneTaxId_500024;
	}

	public String getPeriodTwoTaxId_500024() {
		return periodTwoTaxId_500024;
	}

	public void setPeriodTwoTaxId_500024(String periodTwoTaxId_500024) {
		this.periodTwoTaxId_500024 = periodTwoTaxId_500024;
	}

	public String getPeriodThreeTaxId_500024() {
		return periodThreeTaxId_500024;
	}

	public void setPeriodThreeTaxId_500024(String periodThreeTaxId_500024) {
		this.periodThreeTaxId_500024 = periodThreeTaxId_500024;
	}
    
    

}

package gov.hhs.fda.ctp.common.beans;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import gov.hhs.fda.ctp.common.validators.StateCodeMatch;

public class PermitUpdateUpload extends DocUpload{
	
    private Long permitUpdateId;
    
    private Long docStageId;
    	
	@Pattern(regexp="^([0-9]{2}-[0-9]{7})", message="Invalid EIN")
	private String einNum;
	
	@NotNull(message = "PERMIT_ID is missing.")
	//@Size(min=5, max=9, message = "Invalid permit id.")
	@Pattern(regexp="^([a-zA-Z]{4}[0-9]{1,5})", message="Invalid permitNum")
	private String permitNum;
	
	@NotNull(message="Issue Date is missing.")
	@Size(min=6, max=10, message="Invalid permit issue date.")
	private String issueDt;
	
	@Size(min=6, max=10, message="Invalid permit close date.")
	private String closeDt;
	
	private String errors;
	
	@Pattern(regexp="^(ACT|CLS)", message="Invalid Permit Status")
	private String permitStatus;
	
	@Pattern(regexp="^(MTP|TIM)", message="Invalid Permit Type")
    private String permitType;

	@NotNull(message = "TTB Company name is blank.")
	private String businessName;
	
	private String mailingAttention;
	
    private String mailingProvince;
	
	private String mailingStreet;
	
	private String mailingCity;
	
	@StateCodeMatch
	private String mailingState;
	
	private String mailingPostalCd;
	
    private String mailingText;
    
    private String email;
    
    private String faxnum;
    
    private long company_id;
   
    private String addressStatus;
    
  
    private String contactStatus;

    
	//@Size(min=10, max=10, message="Invalid premise phone number")
    private String premisePhone;
    
    private String premisePhone2;
    
    private long tufaDoc3852Id;
    
    private String mrMonth;
    
    private String mrFiscalYr;
    
    private String ingestedStateValid;
    
    public String getAddressStatus() {
		return addressStatus;
	}

	public void setAddressStatus(String addressStatus) {
		this.addressStatus = addressStatus;
	}

	public String getContactStatus() {
		return contactStatus;
	}

	public void setContactStatus(String contactStatus) {
		this.contactStatus = contactStatus;
	}
    
    public Long getPermitUpdateId() {
		return permitUpdateId;
	}

	public void setPermitUpdateId(Long permitUpdateId) {
		this.permitUpdateId = permitUpdateId;
	}
    
	public Long getDocStageId() {
		return docStageId;
	}

	public void setDocStageId(Long docStageId) {
		this.docStageId = docStageId;
	}
    
    public long getCompanyId() {
		return company_id;
	}

	public void setCompanyId(long company_id) {
		this.company_id = company_id;
	}
    
	public String getEinNum() {
		return einNum;
	}

	public void setEinNum(String einNum) {
		this.einNum = einNum;
	}

	public String getPermitNum() {
		return permitNum;
	}

	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}

	public String getIssueDt() {
		return issueDt;
	}

	public void setIssueDt(String issueDt) {
		this.issueDt = issueDt;
	}

	public String getCloseDt() {
		return closeDt;
	}

	public void setCloseDt(String closeDt) {
		this.closeDt = closeDt;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	
	
	public String getMailingAttention() {
		return mailingAttention;
	}

	public void setMailingAttention(String mailingAttention) {
		this.mailingAttention = mailingAttention;
	}
	
	
	public String getMailingProvince() {
		return mailingStreet;
	}

	public void setMailingProvince(String mailingProvince) {
		this.mailingProvince = mailingProvince;
	}
	
	

	public String getMailingStreet() {
		return mailingStreet;
	}

	public void setMailingStreet(String mailingStreet) {
		this.mailingStreet = mailingStreet;
	}

	public String getMailingCity() {
		return mailingCity;
	}

	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}

	public String getMailingState() {
		return mailingState;
	}

	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}

	public String getMailingPostalCd() {
		return mailingPostalCd;
	}

	public void setMailingPostalCd(String mailingPostalCd) {
		this.mailingPostalCd = mailingPostalCd;
	}

	public String getMailingText() {
		return mailingText;
	}

	public void setMailingText(String mailingText) {
		this.mailingText = mailingText;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return faxnum;
	}

	public void setFax(String fax) {
		this.faxnum = fax;
	}
	
	
	public String getPremisePhone() {
		return premisePhone;
	}

	public void setPremisePhone(String premisePhone) {
		this.premisePhone = premisePhone;
	}
	
	

	public String getPremisePhone2() {
		return premisePhone2;
	}

	public void setPremisePhone2(String premisePhone2) {
		this.premisePhone2 = premisePhone2;
	}

	public String getPermitType() {
		return permitType;
	}

	public void setPermitType(String permitType) {
		this.permitType = permitType;
	}

	public String getPermitStatus() {
		return permitStatus;
	}

	public void setPermitStatus(String permitStatus) {
		this.permitStatus = permitStatus;
	}

	public long getTufaDoc3852Id() {
		return tufaDoc3852Id;
	}

	public void setTufaDoc3852Id(long tufaDoc3852Id) {
		this.tufaDoc3852Id = tufaDoc3852Id;
	}

	public String getMrMonth() {
		return mrMonth;
	}

	public void setMrMonth(String mrMonth) {
		this.mrMonth = mrMonth;
	}

	public String getMrFiscalYr() {
		return mrFiscalYr;
	}

	public void setMrFiscalYr(String mrFiscalYr) {
		this.mrFiscalYr = mrFiscalYr;
	}

	public String getIngestedStateValid() {
		return ingestedStateValid;
	}

	public void setIngestedStateValid(String ingestedStateValid) {
		this.ingestedStateValid = ingestedStateValid;
	}

}

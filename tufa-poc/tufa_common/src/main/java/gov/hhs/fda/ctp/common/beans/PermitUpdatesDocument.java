package gov.hhs.fda.ctp.common.beans;

public class PermitUpdatesDocument extends BaseTufaBean {

	private Long docId;
	
	private String fileNm;

	private String docDesc;
			
	private Long errorCount;
	
	private String aliasFileNm;
	
	private byte[] document;

	public String getDocDesc() {
		return docDesc;
	}

	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}

	public Long getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(Long errorCount) {
		this.errorCount = errorCount;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getAliasFileNm() {
		return aliasFileNm;
	}

	public void setAliasFileNm(String aliasFileNm) {
		this.aliasFileNm = aliasFileNm;
	}

	public byte[] getDocument() {
		return document;
	}

	public void setDocument(byte[] document) {
		this.document = document;
	}

	public String getFileNm() {
		return fileNm;
	}

	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}
	
	
}

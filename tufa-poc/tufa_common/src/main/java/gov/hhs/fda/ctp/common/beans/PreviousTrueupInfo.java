package gov.hhs.fda.ctp.common.beans;

import java.util.List;

public class PreviousTrueupInfo {
	
	private String fiscalYear;
	private String quarter;
	private String tobaccoClassName;
	private String status;
	private List<String> comments;
	private long amendmentId;
	private String deltaType;
	private String ein;
	public String getFiscalYear() {
		return fiscalYear;
	}
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	public String getQuarter() {
		return quarter;
	}
	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}
	public String getTobaccoClassName() {
		return tobaccoClassName;
	}
	public void setTobaccoClassName(String tobaccoClassName) {
		this.tobaccoClassName = tobaccoClassName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<String> getComments() {
		return comments;
	}
	public void setComments(List<String> comments) {
		this.comments = comments;
	}
	public long getAmendmentId() {
		return amendmentId;
	}
	public void setAmendmentId(long amendmentId) {
		this.amendmentId = amendmentId;
	}
	public String getDeltaType() {
		return deltaType;
	}
	public void setDeltaType(String deltaType) {
		this.deltaType = deltaType;
	}
	public String getEin() {
		return ein;
	}
	public void setEin(String ein) {
		this.ein = ein;
	}
	
	

}

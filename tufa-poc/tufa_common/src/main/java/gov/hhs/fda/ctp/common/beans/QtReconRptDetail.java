package gov.hhs.fda.ctp.common.beans;

public class QtReconRptDetail {
	
	private String companyName;
	private long companyId;
	private String permitNum;
	private String month;
	private String fiscalYear;
	private String reconStatus;
	private String reportStatus;
	private long permitId;
	private long periodId;
	private String reportedTobacco;
	private String permitTypeCd;
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getPermitNum() {
		return permitNum;
	}
	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	
	public String getFiscalYear() {
		return fiscalYear;
	}
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	public String getReconStatus() {
		return reconStatus;
	}
	public void setReconStatus(String reconStatus) {
		this.reconStatus = reconStatus;
	}
	public long getPermitId() {
		return permitId;
	}
	public void setPermitId(long permitId) {
		this.permitId = permitId;
	}
	public long getPeriodId() {
		return periodId;
	}
	public void setPeriodId(long periodId) {
		this.periodId = periodId;
	}
	public String getReportedTobacco() {
		return reportedTobacco;
	}
	public void setReportedTobacco(String reportedTobacco) {
		this.reportedTobacco = reportedTobacco;
	}
	public String getPermitTypeCd() {
		return permitTypeCd;
	}
	public void setPermitTypeCd(String permitTypeCd) {
		this.permitTypeCd = permitTypeCd;
	}
	public String getReportStatus() {
		return reportStatus;
	}
	public void setReportStatus(String reportStatus) {
		this.reportStatus = reportStatus;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	

}

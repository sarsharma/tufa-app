/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author tgunter
 *
 */
public class ReconExportDetail {
	private int year;
	private int quarter;
	private String month;
	private String legalNm;
	private String ein;
	private String permitNum;
	private String tobaccoClass;
	private Double volumeRemoved;
	private Double tax;
	private String reconStatus;
	private String reconUser;
	private String reconDisplayDate;
	private String reconDisplayTime;
	@JsonIgnore
	private int tobaccoClassId;
	@JsonIgnore
	private Date reconDate;
	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}
	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}
	/**
	 * @return the quarter
	 */
	public int getQuarter() {
		return quarter;
	}
	/**
	 * @param quarter the quarter to set
	 */
	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}
	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}
	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}
	/**
	 * @return the legalNm
	 */
	public String getLegalNm() {
		return legalNm;
	}
	/**
	 * @param legalNm the legalNm to set
	 */
	public void setLegalNm(String legalNm) {
		this.legalNm = legalNm;
	}
	/**
	 * @return the ein
	 */
	public String getEin() {
		return ein;
	}
	/**
	 * @param ein the ein to set
	 */
	public void setEin(String ein) {
		this.ein = ein;
	}
	/**
	 * @return the permitNum
	 */
	public String getPermitNum() {
		return permitNum;
	}
	/**
	 * @param permitNum the permitNum to set
	 */
	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}
	/**
	 * @return the tobaccoClass
	 */
	public String getTobaccoClass() {
		return tobaccoClass;
	}
	/**
	 * @param tobaccoClass the tobaccoClass to set
	 */
	public void setTobaccoClass(String tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}
	/**
	 * @return the tobaccoClassId
	 */
	public int getTobaccoClassId() {
		return tobaccoClassId;
	}
	/**
	 * @param tobaccoClassId the tobaccoClassId to set
	 */
	public void setTobaccoClassId(int tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}
	/**
	 * @return the volumeRemoved
	 */
	public Double getVolumeRemoved() {
		return volumeRemoved;
	}
	/**
	 * @param volumeRemoved the volumeRemoved to set
	 */
	public void setVolumeRemoved(Double volumeRemoved) {
		this.volumeRemoved = volumeRemoved;
	}
	/**
	 * @return the tax
	 */
	public Double getTax() {
		return tax;
	}
	/**
	 * @param tax the tax to set
	 */
	public void setTax(Double tax) {
		this.tax = tax;
	}
	/**
	 * @return the reconStatus
	 */
	public String getReconStatus() {
		return reconStatus;
	}
	/**
	 * @param reconStatus the reconStatus to set
	 */
	public void setReconStatus(String reconStatus) {
		this.reconStatus = reconStatus;
	}
	/**
	 * @return the reconUser
	 */
	public String getReconUser() {
		return reconUser;
	}
	/**
	 * @param reconUser the reconUser to set
	 */
	public void setReconUser(String reconUser) {
		this.reconUser = reconUser;
	}
	/**
	 * @return the reconDisplayDate
	 */
	public String getReconDisplayDate() {
		return reconDisplayDate;
	}
	/**
	 * @param reconDisplayDate the reconDisplayDate to set
	 */
	public void setReconDisplayDate(String reconDisplayDate) {
		this.reconDisplayDate = reconDisplayDate;
	}
	/**
	 * @return the reconDisplayTime
	 */
	public String getReconDisplayTime() {
		return reconDisplayTime;
	}
	/**
	 * @param reconDisplayTime the reconDisplayTime to set
	 */
	public void setReconDisplayTime(String reconDisplayTime) {
		this.reconDisplayTime = reconDisplayTime;
	}
	/**
	 * @return the reconDate
	 */
	public Date getReconDate() {
		return reconDate;
	}
	/**
	 * @param reconDate the reconDate to set
	 */
	public void setReconDate(Date reconDate) {
		this.reconDate = reconDate;
	}
}

/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.List;

/**
 * @author akari
 *
 */
public class ReportStatus {
	List<AssessmentReportStatus> result;

	/**
	 * @return the result
	 */
	public List<AssessmentReportStatus> getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(List<AssessmentReportStatus> result) {
		this.result = result;
	}

}

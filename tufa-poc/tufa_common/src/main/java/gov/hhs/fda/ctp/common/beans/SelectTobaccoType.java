/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.hhs.fda.ctp.common.beans;

/**
 *
 * @author Ragh.Balasubramanian
 */
public class SelectTobaccoType {
    private boolean showCigarettes;
    private boolean showCigars;
    private boolean showSnuffs;
    private boolean showPipes;
    private boolean showChews;
    private boolean showRollYourOwn;

    public boolean isShowCigarettes() {
        return showCigarettes;
    }

    public void setShowCigarettes(boolean showCigarettes) {
        this.showCigarettes = showCigarettes;
    }

    public boolean isShowCigars() {
        return showCigars;
    }

    public void setShowCigars(boolean showCigars) {
        this.showCigars = showCigars;
    }

    public boolean isShowSnuffs() {
        return showSnuffs;
    }

    public void setShowSnuffs(boolean showSnuffs) {
        this.showSnuffs = showSnuffs;
    }

    public boolean isShowPipes() {
        return showPipes;
    }

    public void setShowPipes(boolean showPipes) {
        this.showPipes = showPipes;
    }

    public boolean isShowChews() {
        return showChews;
    }

    public void setShowChews(boolean showChews) {
        this.showChews = showChews;
    }

    public boolean isShowRollYourOwn() {
        return showRollYourOwn;
    }

    public void setShowRollYourOwn(boolean showRollYourOwn) {
        this.showRollYourOwn = showRollYourOwn;
    }
    
    
}

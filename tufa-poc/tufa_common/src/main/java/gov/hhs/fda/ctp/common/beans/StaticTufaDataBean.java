package gov.hhs.fda.ctp.common.beans;

import java.util.List;

public class StaticTufaDataBean {
	
	private List<String> stateCodes;

	public List<String> getStateCodes() {
		return stateCodes;
	}

	public void setStateCodes(List<String> stateCodes) {
		this.stateCodes = stateCodes;
	}
	
	

}

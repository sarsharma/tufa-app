/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.List;


/**
 * The Class SubmittedForm.
 *
 * @author tgunter
 */
public class SubmittedForm extends BaseTufaBean implements Comparable<SubmittedForm> {
    
    /** The form id. */
    private Long formId;
    
    /** The permit id. */
    private Long permitId;
    
    /** The period id. */
    private Long periodId;
    
    /** The form type cd. */
    private String formTypeCd;
    
    /** The form num. */
    private Long formNum;
    
    /** The form details. */
    private List<FormDetail> formDetails;
    
    /** The form comments */
    private List<FormComment> formComments;
    
    private List<TTBAdjustment> ttbAdjustments;
    
    /*
     * this will hold the selected tobacco types
     */
    private String selectedTobaccoTypes;
    
    /**
     * Gets the form id.
     *
     * @return the formId
     */
    public Long getFormId() {
        return formId;
    }
    
    /**
     * Sets the form id.
     *
     * @param formId the formId to set
     */
    public void setFormId(Long formId) {
        this.formId = formId;
    }
    
    /**
     * Gets the permit id.
     *
     * @return the permitId
     */
    public Long getPermitId() {
        return permitId;
    }
    
    /**
     * Sets the permit id.
     *
     * @param permitId the permitId to set
     */
    public void setPermitId(Long permitId) {
        this.permitId = permitId;
    }
    
    /**
     * Gets the period id.
     *
     * @return the periodId
     */
    public Long getPeriodId() {
        return periodId;
    }
    
    /**
     * Sets the period id.
     *
     * @param periodId the periodId to set
     */
    public void setPeriodId(Long periodId) {
        this.periodId = periodId;
    }
    
    /**
     * Gets the form type cd.
     *
     * @return the formTypeCd
     */
    public String getFormTypeCd() {
        return formTypeCd;
    }
    
    /**
     * Sets the form type cd.
     *
     * @param formTypeCd the formTypeCd to set
     */
    public void setFormTypeCd(String formTypeCd) {
        this.formTypeCd = formTypeCd;
    }
    
    /**
     * Gets the form num.
     *
     * @return the formNum
     */
    public Long getFormNum() {
        return formNum;
    }
    
    /**
     * Sets the form num.
     *
     * @param formNum the formNum to set
     */
    public void setFormNum(Long formNum) {
        this.formNum = formNum;
    }
    
    /**
     * Gets the form details.
     *
     * @return the details
     */
    public List<FormDetail> getFormDetails() {
        return formDetails;
    }
    
    /**
     * Sets the form details.
     *
     * @param details the details to set
     */
    public void setFormDetails(List<FormDetail> details) {
        this.formDetails = details;
    }
    
    /**
	 * @return the formComments
	 */
	public List<FormComment> getFormComments() {
		return formComments;
	}

	/**
	 * @param formComments the formComments to set
	 */
	public void setFormComments(List<FormComment> formComments) {
		this.formComments = formComments;
	}

	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SubmittedForm form = (SubmittedForm)obj;
        return(toString().equals(form.toString()));
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("#"+this.permitId+"#"+this.periodId+"#"+this.formTypeCd+ "#" + this.formNum + "#");
        return builder.toString();
    }
    

    /**
     * Compare string.
     *
     * @return the string
     */
    private String compareString() {
        return formTypeCd + formNum;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(SubmittedForm form) {
        return this.compareString().compareTo(form.compareString());
    }

	/**
	 * @return the selectedTobaccoTypes
	 */
	public String getSelectedTobaccoTypes() {
		return selectedTobaccoTypes;
	}

	/**
	 * @param selectedTobaccoTypes the selectedTobaccoTypes to set
	 */
	public void setSelectedTobaccoTypes(String selectedTobaccoTypes) {
		this.selectedTobaccoTypes = selectedTobaccoTypes;
	}

	public List<TTBAdjustment> getTtbAdjustments() {
		return ttbAdjustments;
	}

	public void setTtbAdjustments(List<TTBAdjustment> ttbAdjustments) {
		this.ttbAdjustments = ttbAdjustments;
	}
	
	

}

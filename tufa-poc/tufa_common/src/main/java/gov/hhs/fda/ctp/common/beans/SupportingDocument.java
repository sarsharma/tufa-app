package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

public class SupportingDocument extends BaseTufaBean {
	/** The document number */
    private long asmntDocId;

	/** The assessment id. */
    private long assessmentId;

	/** The document number */
    private long documentNumber;

	/** The filename. */
    private String filename;

	/** The description. */
    private String description;

    /** The version number. */
    private int versionNum;

    /** The dt uploaded. */
    private Date dateUploaded;

	/** The description. */
    private String author;

    /** The created by. */
    private String createdBy;

    /** The created dt. */
    private String createdDt;
    
    /** The cigar qtr. */
    private int cigarQtr;
    
    public long getAsmntDocId() {
		return asmntDocId;
	}

	public void setAsmntDocId(long asmntDocId) {
		this.asmntDocId = asmntDocId;
	}

	public long getAssessmentId() {
		return assessmentId;
	}

	public void setAssessmentId(long assessmentId) {
		this.assessmentId = assessmentId;
	}

	public long getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(long documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getVersionNum() {
		return versionNum;
	}

	public void setVersionNum(int versionNum) {
		this.versionNum = versionNum;
	}

	public Date getDateUploaded() {
		return dateUploaded;
	}

	public void setDateUploaded(Date dateUploaded) {
		this.dateUploaded = dateUploaded;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}

	public int getCigarQtr() {
		return cigarQtr;
	}

	public void setCigarQtr(int cigarQtr) {
		this.cigarQtr = cigarQtr;
	}
}

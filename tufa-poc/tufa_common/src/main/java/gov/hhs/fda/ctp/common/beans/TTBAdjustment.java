/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.List;

public class TTBAdjustment {
	
    private List<TTBAdjustmentDetail> ttbAdjustmentDetail;
    private Double taxAmountAdjusmentA;
    private Double taxAmountAdjusmentB;
    private Double taxAmountTotalAdjustment;
    private long tobaccoClassId;
    private long formId;
	public List<TTBAdjustmentDetail> getTtbAdjustmentDetail() {
		return ttbAdjustmentDetail;
	}
	public void setTtbAdjustmentDetail(List<TTBAdjustmentDetail> ttbAdjustmentDetail) {
		this.ttbAdjustmentDetail = ttbAdjustmentDetail;
	}
	public Double getTaxAmountAdjusmentA() {
		return taxAmountAdjusmentA;
	}
	public void setTaxAmountAdjusmentA(Double taxAmountAdjusmentA) {
		this.taxAmountAdjusmentA = taxAmountAdjusmentA;
	}
	public Double getTaxAmountAdjusmentB() {
		return taxAmountAdjusmentB;
	}
	public void setTaxAmountAdjusmentB(Double taxAmountAdjusmentB) {
		this.taxAmountAdjusmentB = taxAmountAdjusmentB;
	}
	public Double getTaxAmountTotalAdjustment() {
		return taxAmountTotalAdjustment;
	}
	public void setTaxAmountTotalAdjustment(Double taxAmountTotalAdjustment) {
		this.taxAmountTotalAdjustment = taxAmountTotalAdjustment;
	}
	public long getTobaccoClassId() {
		return tobaccoClassId;
	}
	public void setTobaccoClassId(long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}
	public long getFormId() {
		return formId;
	}
	public void setFormId(long formId) {
		this.formId = formId;
	}
    
    
    }

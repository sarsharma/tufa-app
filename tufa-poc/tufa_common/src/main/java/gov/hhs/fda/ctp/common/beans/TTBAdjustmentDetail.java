/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;


public class TTBAdjustmentDetail {
    
    /** The adj form id. */
    private Long adjFormId;
    
    /** The form id. */
    private Long formId;
    
    /** The tobacco class id. */
    private Long tobaccoClassId;
    
    /** The line num. */
    private Long adjLineNum;
    
    /** The taxes paid. */
    private Double taxAmount;
    
    /** The taxReturnId */
    private String taxReturnId;

    private String adjType;
    
    private String lineDesc;

	public Long getAdjFormId() {
		return adjFormId;
	}

	public void setAdjFormId(Long adjFormId) {
		this.adjFormId = adjFormId;
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}

	public void setTobaccoClassId(Long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}

	public Long getAdjLineNum() {
		return adjLineNum;
	}

	public void setAdjLineNum(Long adjLineNum) {
		this.adjLineNum = adjLineNum;
	}


	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getTaxReturnId() {
		return taxReturnId;
	}

	public void setTaxReturnId(String taxReturnId) {
		this.taxReturnId = taxReturnId;
	}

	public String getAdjType() {
		return adjType;
	}

	public void setAdjType(String adjType) {
		this.adjType = adjType;
	}

	public String getLineDesc() {
		return lineDesc;
	}

	public void setLineDesc(String lineDesc) {
		this.lineDesc = lineDesc;
	}

	
    
    
}

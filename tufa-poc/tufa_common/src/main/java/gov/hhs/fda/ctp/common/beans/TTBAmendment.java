package gov.hhs.fda.ctp.common.beans;

import java.util.List;
import java.util.Map;

public class TTBAmendment {
	
    private Long ttbAmendmentId;
	
    private Long ttbCompanyId;
	
    private Long tobaccoClassId;
	
    private String fiscalYr;
	
	private String qtr;
	
	private Double amendedTotalTax;
	
	private Double amendedTotalVolume;

	private Double acceptedIngestedTotalTax;
		
	private String acceptanceFlag;
	
	private String inProgressFlag;
	
	private String comments;
	
	private String tobaccoType;
	
	private Boolean inclAllQtrs;

	private List<TTBQtrComment> userComments;
	
	private List<Integer> excludedPermits;
	
	private boolean isAmendmentPresent=true;
	
	private String adjProcessFlag;
	
	private Map<String,String> mixedActionQuarterDetails;
	
	private Long mixedActionVolume;
    
    private Double mixedActionTax;
	
	public Map<String, String> getMixedActionQuarterDetails() {
		return mixedActionQuarterDetails;
	}

	public void setMixedActionQuarterDetails(Map<String, String> mixedActionQuarterDetails) {
		this.mixedActionQuarterDetails = mixedActionQuarterDetails;
	}

	public Long getMixedActionVolume() {
		return mixedActionVolume;
	}

	public void setMixedActionVolume(Long mixedActionVolume) {
		this.mixedActionVolume = mixedActionVolume;
	}

	public Double getMixedActionTax() {
		return mixedActionTax;
	}

	public void setMixedActionTax(Double mixedActionTax) {
		this.mixedActionTax = mixedActionTax;
	}

	public boolean isAmendmentPresent() {
		return isAmendmentPresent;
	}

	public void setAmendmentPresent(boolean isAmendmentPresent) {
		this.isAmendmentPresent = isAmendmentPresent;
	}

	public Long getTtbAmendmentId() {
		return ttbAmendmentId;
	}

	public void setTtbAmendmentId(Long ttbAmendmentId) {
		this.ttbAmendmentId = ttbAmendmentId;
	}

	public Long getTtbCompanyId() {
		return ttbCompanyId;
	}

	public void setTtbCompanyId(Long ttbCompanyId) {
		this.ttbCompanyId = ttbCompanyId;
	}

	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}

	public void setTobaccoClassId(Long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}

	public String getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getQtr() {
		return qtr;
	}

	public void setQtr(String qtr) {
		this.qtr = qtr;
	}

	public Double getAmendedTotalTax() {
		return amendedTotalTax;
	}

	public void setAmendedTotalTax(Double amendedTotalTax) {
		this.amendedTotalTax = amendedTotalTax;
	}

	public Double getAmendedTotalVolume() {
		return amendedTotalVolume;
	}

	public void setAmendedTotalVolume(Double amendedTotalVolume) {
		this.amendedTotalVolume = amendedTotalVolume;
	}

	public Double getAcceptedIngestedTotalTax() {
		return acceptedIngestedTotalTax;
	}

	public void setAcceptedIngestedTotalTax(Double acceptedIngestedTotalVolume) {
		this.acceptedIngestedTotalTax = acceptedIngestedTotalVolume;
	}

	public String getAcceptanceFlag() {
		return acceptanceFlag;
	}

	public void setAcceptanceFlag(String acceptanceFlag) {
		this.acceptanceFlag = acceptanceFlag;
	}
	
	public String getInProgressFlag() {
		return inProgressFlag;
	}

	public void setInProgressFlag(String inProgressFlag) {
		this.inProgressFlag = inProgressFlag;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTobaccoType() {
		return tobaccoType;
	}

	public void setTobaccoType(String tobaccoType) {
		this.tobaccoType = tobaccoType;
	}

	public Boolean getInclAllQtrs() {
		return inclAllQtrs;
	}

	public void setInclAllQtrs(Boolean inclAllQtrs) {
		this.inclAllQtrs = inclAllQtrs;
	}

	public List<TTBQtrComment> getUserComments() {
		return userComments;
	}

	public void setUserComments(List<TTBQtrComment> userComments) {
		this.userComments = userComments;
	}

	public List<Integer> getExcludedPermits() {
		return excludedPermits;
	}

	public void setExcludedPermits(List<Integer> excludedPermits) {
		this.excludedPermits = excludedPermits;
	}

	public String getAdjProcessFlag() {
		return adjProcessFlag;
	}

	public void setAdjProcessFlag(String adjProcessFlag) {
		this.adjProcessFlag = adjProcessFlag;
	}

	
	
	
}

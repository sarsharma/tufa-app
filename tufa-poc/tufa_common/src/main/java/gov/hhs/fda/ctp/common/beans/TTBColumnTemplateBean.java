package gov.hhs.fda.ctp.common.beans;

import java.util.List;
import java.util.Map;

public class TTBColumnTemplateBean {

	private List<TTBFileColumnTemplate> ttbColumnTemplates;

	private Map<String, Map<String, List<String>>> colAliasesMap;

	public String getPrimaryValueForCol(String columnName, String alias) {

		if (colAliasesMap != null && !colAliasesMap.isEmpty()) {
			Map<String, List<String>> primaryValueMap = null;
			primaryValueMap = colAliasesMap.get(columnName);
			
			if (primaryValueMap == null) return null;
			
			for (String primaryValue : primaryValueMap.keySet()) {
				if (primaryValue.equalsIgnoreCase(alias) || alias.toUpperCase().contains(primaryValue))
					return primaryValue;
				List<String> aliases = primaryValueMap.get(primaryValue);
				
				if (aliases.contains(alias.toUpperCase()))	
					return primaryValue;
			}

		}

		return null;
	}

	public List<TTBFileColumnTemplate> getTtbColumnTemplates() {
		return ttbColumnTemplates;
	}

	public void setTtbColumnTemplates(List<TTBFileColumnTemplate> ttbColumnTemplates) {
		this.ttbColumnTemplates = ttbColumnTemplates;
	}

	public Map<String, Map<String, List<String>>> getColAliasesMap() {
		return colAliasesMap;
	}

	public void setColAliasesMap(Map<String, Map<String, List<String>>> colAliasesMap) {
		this.colAliasesMap = colAliasesMap;
	}

}

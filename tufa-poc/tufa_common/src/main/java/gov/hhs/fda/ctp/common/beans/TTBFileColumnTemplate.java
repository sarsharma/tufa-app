package gov.hhs.fda.ctp.common.beans;

import java.util.HashSet;
import java.util.Set;

public class TTBFileColumnTemplate {

	private Integer columnCount;
	private String ein;
	private String permitId;
	private String businessName;
	private String mailingStreet;
	private String mailingCity;
	private String mailingState;
	private String mailingPostalCd;
	private String mailingText;
	private String premisePhone;
	private String premisePhone2;
	private String email;
	private String issueDate;
	private String closedDate;
	private String permitType;
	private String permitStatus;

	private TTBFileColumnFormat colFormat;

	private Set<String> colHeadings;

	public Integer getColumnCount() {
		return columnCount;
	}

	public void setColumnCount(Integer columnCount) {
		this.columnCount = columnCount;
	}

	public String getEin() {
		return ein;
	}

	public void setEin(String ein) {
		this.ein = ein;
	}

	public String getPermitId() {
		return permitId;
	}

	public void setPermitId(String permitId) {
		this.permitId = permitId;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getMailingStreet() {
		return mailingStreet;
	}

	public void setMailingStreet(String mailingstreet) {
		this.mailingStreet = mailingstreet;
	}

	public String getMailingCity() {
		return mailingCity;
	}

	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}

	public String getMailingState() {
		return mailingState;
	}

	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}

	public String getMailingPostalCd() {
		return mailingPostalCd;
	}

	public void setMailingPostalCd(String mailingPostalCd) {
		this.mailingPostalCd = mailingPostalCd;
	}

	public String getMailingText() {
		return mailingText;
	}

	public void setMailingText(String mailingText) {
		this.mailingText = mailingText;
	}

	public String getPremisePhone() {
		return premisePhone;
	}

	public void setPremisePhone(String premisePhone) {
		this.premisePhone = premisePhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}

	public String getPermitType() {
		return permitType;
	}

	public void setPermitType(String permitType) {
		this.permitType = permitType;
	}

	public String getPermitStatus() {
		return permitStatus;
	}

	public void setPermitStatus(String permitStatus) {
		this.permitStatus = permitStatus;
	}

	public String getPremisePhone2() {
		return premisePhone2;
	}

	public void setPremisePhone2(String premisePhone2) {
		this.premisePhone2 = premisePhone2;
	}

	public TTBFileColumnFormat getColFormat() {
		return colFormat;
	}

	public void setColFormat(TTBFileColumnFormat colFormat) {
		this.colFormat = colFormat;
	}

	public Set<String> getColHeadings() {
		return colHeadings;
	}

	public void setColHeadings(Set<String> colHeadings) {
		this.colHeadings = colHeadings;
	}


	@Override
	public boolean equals(Object o) {

		if (o == this) {
			return true;
		}

		if (!(o instanceof TTBFileColumnTemplate)) {
			return false;
		}

		TTBFileColumnTemplate other = (TTBFileColumnTemplate) o;

		return other.ein.equalsIgnoreCase(this.ein) && other.permitId.equalsIgnoreCase(this.permitId)
				&& other.businessName.equalsIgnoreCase(this.businessName)
				&& other.mailingStreet.equalsIgnoreCase(this.mailingStreet)
				&& other.mailingCity.equalsIgnoreCase(this.mailingCity)
				&& other.mailingState.equalsIgnoreCase(this.mailingState)
				&& other.mailingPostalCd.equalsIgnoreCase(this.mailingPostalCd)
				&& other.mailingText.equalsIgnoreCase(this.mailingText)
				&& other.premisePhone.equalsIgnoreCase(this.premisePhone) && other.email.equalsIgnoreCase(this.email)
				&& other.issueDate.equalsIgnoreCase(this.issueDate)
				&& other.closedDate.equalsIgnoreCase(this.closedDate)
				&& other.permitType.equalsIgnoreCase(this.permitType)
				&& other.permitStatus.equalsIgnoreCase(this.permitStatus);
	}

	public Set<String> columnHeadings() {

		this.colHeadings = new HashSet<>();

		if ((ein != null))
			this.colHeadings.add(ein.trim().toUpperCase());
		if (businessName != null)
			this.colHeadings.add(businessName.trim().toUpperCase());
		if (permitId != null)
			this.colHeadings.add(permitId.trim().toUpperCase());
		if (issueDate != null)
			this.colHeadings.add(issueDate.trim().toUpperCase());
		if (mailingStreet != null)
			this.colHeadings.add(mailingStreet.trim().toUpperCase());
		if (mailingCity != null)
			this.colHeadings.add(mailingCity.trim().toUpperCase());
		if (mailingState != null)
			this.colHeadings.add(mailingState.trim().toUpperCase());
		if (mailingPostalCd != null)
			this.colHeadings.add(mailingPostalCd.trim().toUpperCase());
		if (mailingText != null)
			this.colHeadings.add(mailingText.trim().toUpperCase());
		if (premisePhone != null)
			this.colHeadings.add(premisePhone.trim().toUpperCase());
		if (email != null)
			this.colHeadings.add(email.trim().toUpperCase());
		if (closedDate != null)
			this.colHeadings.add(closedDate.trim().toUpperCase());
		if (permitType != null)
			this.colHeadings.add(permitType.trim().toUpperCase());
		if (permitStatus != null)
			this.colHeadings.add(permitStatus.trim().toUpperCase());

		return this.colHeadings;

	}

	public boolean hasColHeading(String heading) {
		if (this.colHeadings != null && this.colHeadings.size() > 0)
			return this.colHeadings.contains(heading);

		return false;
	}
}

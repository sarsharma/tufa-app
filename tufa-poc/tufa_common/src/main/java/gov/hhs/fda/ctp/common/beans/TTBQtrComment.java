package gov.hhs.fda.ctp.common.beans;

public class TTBQtrComment {

    private Long commentSeq;
    
    private String qtr;
    
	private String commentQtrs;
    
    private String userComment;
    
    private String author;
        
    private String commentDate;
    
    private String modifiedBy;
        
    private String modifiedDt;
    
    private Long attachmentsCount;
	 
	 

	public Long getAttachmentsCount() {
		return attachmentsCount;
	}

	public void setAttachmentsCount(Long attachmentsCount) {
		this.attachmentsCount = attachmentsCount;
	}


	public String getUserComment() {
		return userComment;
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public Long getCommentSeq() {
		return commentSeq;
	}

	public void setCommentSeq(Long commentSeq) {
		this.commentSeq = commentSeq;
	}


	public String getQtr() {
		return qtr;
	}

	public void setQtr(String qtr) {
		this.qtr = qtr;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public String getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(String commentDate) {
		this.commentDate = commentDate;
	}

	public String getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(String modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getCommentQtrs() {
		return commentQtrs;
	}

	public void setCommentQtrs(String commentQtrs) {
		this.commentQtrs = commentQtrs;
	}
    
}


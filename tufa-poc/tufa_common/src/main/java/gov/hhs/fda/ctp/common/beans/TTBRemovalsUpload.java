package gov.hhs.fda.ctp.common.beans;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import gov.hhs.fda.ctp.common.validators.PeriodMatch;

//@PeriodMatch
public class TTBRemovalsUpload extends DocUpload{

	@NotNull(message="Period is missing.")
	private String period;
	
	@NotNull(message = "EIN is missing.")
	@Size(min=1, message = "EIN is missing.")
	private String ein;
	
	@NotNull(message = "TTB Permit is missing.")
	@Size(min=5, max=9, message = "Invalid  permit Id.")
	private String idPermit;
	
	@NotNull(message = "Tobacco Class is missing")
	@Size(min=1, message = "Tobacco Class is missing.")
	private String product;
	
	private Double pounds;

	private String nameOwner;
	
	// not on the spreadsheet, including for informational purposes
	@Digits(integer=4, fraction=0)
	private Long fiscalYr;
	
	private String errors;


	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}


	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public Double getPounds() {
		return pounds;
	}

	public void setPounds(Double pounds) {
		this.pounds = pounds;
	}

	public String getNameOwner() {
		return nameOwner;
	}

	public void setNameOwner(String nameOwner) {
		this.nameOwner = nameOwner;
	}

	public Long getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public String getEin() {
		return ein;
	}

	public void setEin(String ein) {
		this.ein = ein;
	}

	public String getIdPermit() {
		return idPermit;
	}

	public void setIdPermit(String idPermit) {
		this.idPermit = idPermit;
	}
	
}

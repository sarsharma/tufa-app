package gov.hhs.fda.ctp.common.beans;

import gov.hhs.fda.ctp.common.validators.TPBDMatch;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

//@TPBDMatch
public class TTBUpload extends DocUpload{
	// not on the spreadsheet, including for informational purposes
	@Digits(integer=4, fraction=0)
    private Long fiscalYr;

	@NotNull(message = "EIN is missing.")
	@Size(min=1, message = "EIN is missing.")
	private String einNum;
	
	@NotNull(message="Date is missing.")
	@Size(min=6, max=10, message="Invalid date.")
	private String tpbd;
	
	@NotNull(message = "Registry Number is missing.")
	@Size(min=5, max=9, message = "Invalid registry number.")
	private String permitNum;
	
	@NotNull(message = "Entity name is missing.")
	@Size(min=1, max=80, message = "Entity name is missing.")
	private String companyNm;

	@Digits(integer=9, fraction=3, message = "Tax amount exceeds maximum.")
	private Double chewSnuffTaxes;
	
	@Digits(integer=9, fraction=3, message = "Tax amount exceeds maximum.")
	private Double cigaretteTaxes;

	@Digits(integer=9, fraction=3, message = "Tax amount exceeds maximum.")
	private Double cigarTaxes;
	
	@Digits(integer=9, fraction=3, message = "Tax amount exceeds maximum.")
	private Double pipeRyoTaxes;

	@Digits(integer=9, fraction=3, message = "Increment adjusments exceeds maximum.")
	private Double incAdj;
	
	@Digits(integer=9, fraction=3, message = "Decrement adjusments exceeds maximum.")
	private Double decAdj;
	
	private String errors;
	
	public String getEinNum() {
		return einNum;
	}

	public void setEinNum(String einNum) {
		this.einNum = einNum;
	}

	/**
	 * @return the tpbd
	 */
	public String getTpbd() {
		return tpbd;
	}

	/**
	 * @param tpbd the tpbd to set
	 */
	public void setTpbd(String tpbd) {
		this.tpbd = tpbd;
	}

	public Long getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getPermitNum() {
		return permitNum;
	}

	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}

	public String getCompanyNm() {
		return companyNm;
	}

	public void setCompanyNm(String companyNm) {
		this.companyNm = companyNm;
	}

	public Double getChewSnuffTaxes() {
		return chewSnuffTaxes;
	}

	public void setChewSnuffTaxes(Double chewSnuffTaxes) {
		this.chewSnuffTaxes = chewSnuffTaxes;
	}

	public Double getCigaretteTaxes() {
		return cigaretteTaxes;
	}

	public void setCigaretteTaxes(Double cigaretteTaxes) {
		this.cigaretteTaxes = cigaretteTaxes;
	}

	public Double getCigarTaxes() {
		return cigarTaxes;
	}

	public void setCigarTaxes(Double cigarTaxes) {
		this.cigarTaxes = cigarTaxes;
	}

	public Double getPipeRyoTaxes() {
		return pipeRyoTaxes;
	}

	public void setPipeRyoTaxes(Double pipeRyoTaxes) {
		this.pipeRyoTaxes = pipeRyoTaxes;
	}

	/**
	 * @return the errors
	 */
	public String getErrors() {
		return errors;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(String errors) {
		this.errors = errors;
	}

	public Double getIncAdj() {
		return incAdj;
	}

	public void setIncAdj(Double incAdj) {
		this.incAdj = incAdj;
	}

	public Double getDecAdj() {
		return decAdj;
	}

	public void setDecAdj(Double decAdj) {
		this.decAdj = decAdj;
	}

	

}

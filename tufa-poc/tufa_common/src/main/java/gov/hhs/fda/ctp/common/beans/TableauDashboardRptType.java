package gov.hhs.fda.ctp.common.beans;

public class TableauDashboardRptType {

	private Long dashboardRptTypeId;
	
	private String dashboardType;

	private String displayValue;

	private Boolean enable;

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	public String getDashboardType() {
		return dashboardType;
	}

	public void setDashboardType(String dashboardType) {
		this.dashboardType = dashboardType;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}


	public Long getDashboardRptTypeId() {
		return dashboardRptTypeId;
	}

	public void setDashboardRptTypeId(Long dashboardRptTypeId) {
		this.dashboardRptTypeId = dashboardRptTypeId;
	}

}

/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

/**
 * The Class TobaccoClass.
 *
 * @author tgunter
 */
public class TobaccoClass {
    
    /** The tobacco class id. */
    private Long tobaccoClassId;
    
    /** The parent class id. */
    private Long parentClassId;
    
    /** The tobacco class nm. */
    private String tobaccoClassNm;
    
    /** The class type cd. */
    private String classTypeCd;
    
    /** The tax rate. */
    private Double taxRate;
    
    /** The tax rate. */
    private Double conversionRate;
    
    /** Begin date for tax rate */
    private Date taxRateEffectiveDate;
    
    /** End date for tax rate */
    private Date taxRateEndDate;
    
    /** Threshold value for tobacco class**/
    private Double thresholdValue;
    
    public Double getThresholdValue() {
		return thresholdValue;
	}

	public void setThresholdValue(Double thresholdValue) {
		this.thresholdValue = thresholdValue;
	}
    
    /**
	 * @return the taxRateEffectiveDate
	 */
	public Date getTaxRateEffectiveDate() {
		return taxRateEffectiveDate;
	}

	/**
	 * @param taxRateEffectiveDate the taxRateEffectiveDate to set
	 */
	public void setTaxRateEffectiveDate(Date taxRateEffectiveDate) {
		this.taxRateEffectiveDate = taxRateEffectiveDate;
	}

	/**
	 * @return the taxRateEndDate
	 */
	public Date getTaxRateEndDate() {
		return taxRateEndDate;
	}

	/**
	 * @param taxRateEndDate the taxRateEndDate to set
	 */
	public void setTaxRateEndDate(Date taxRateEndDate) {
		this.taxRateEndDate = taxRateEndDate;
	}

	/**
     * Gets the tobacco class nm.
     *
     * @return the tobaccoClassNm
     */
    public String getTobaccoClassNm() {
        return tobaccoClassNm;
    }
    
    /**
     * Sets the tobacco class nm.
     *
     * @param tobaccoClassNm the tobaccoClassNm to set
     */
    public void setTobaccoClassNm(String tobaccoClassNm) {
        this.tobaccoClassNm = tobaccoClassNm;
    }
    
    /**
     * Gets the class type cd.
     *
     * @return the classTypeCd
     */
    public String getClassTypeCd() {
        return classTypeCd;
    }
    
    /**
     * Sets the class type cd.
     *
     * @param classTypeCd the classTypeCd to set
     */
    public void setClassTypeCd(String classTypeCd) {
        this.classTypeCd = classTypeCd;
    }
    
    /**
     * Gets the tax rate.
     *
     * @return the taxRate
     */
    public Double getTaxRate() {
        return taxRate;
    }
    
    /**
     * Sets the tax rate.
     *
     * @param taxRate the taxRate to set
     */
    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }
    
    /**
     * Gets the conversion rate.
     *
     * @return the conversionRate
     */
    public Double getConversionRate() {
        return conversionRate;
    }
    
    /**
     * Sets the conversion rate.
     *
     * @param taxRate the conversionRate to set
     */
    public void setConversionRate(Double conversionRate) {
        this.conversionRate = conversionRate;
    }
    
    /**
     * Gets the tobacco class id.
     *
     * @return the tobaccoClassId
     */
    public Long getTobaccoClassId() {
        return tobaccoClassId;
    }
    
    /**
     * Sets the tobacco class id.
     *
     * @param tobaccoClassId the tobaccoClassId to set
     */
    public void setTobaccoClassId(Long tobaccoClassId) {
        this.tobaccoClassId = tobaccoClassId;
    }
    
    /**
     * Gets the parent class id.
     *
     * @return the parentClassId
     */
    public Long getParentClassId() {
        return parentClassId;
    }
    
    /**
     * Sets the parent class id.
     *
     * @param parentClassId the parentClassId to set
     */
    public void setParentClassId(Long parentClassId) {
        this.parentClassId = parentClassId;
    }
}

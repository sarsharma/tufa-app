package gov.hhs.fda.ctp.common.beans;

public class TobaccoTypeTTBVol {
	
	
	private Integer qtr;
	private String ein;
	private String tobaccoClass;
	private Double pounds;
	private Long fiscalYr;
	private Double estimatedTax;
	private Double amendedTax;
	
	public TobaccoTypeTTBVol(){}
	
	public TobaccoTypeTTBVol(Long fiscalYr,String ein,Integer qtr,String tobaccoClass){
		this.fiscalYr = fiscalYr;
		this.ein = ein;
		this.setQtr(qtr);
		this.tobaccoClass = tobaccoClass;
	}
	
	public Integer getQtr() {
		return qtr;
	}

	public void setQtr(Integer qtr) {
		this.qtr = qtr;
	}

	public String getEin() {
		return ein;
	}
	public void setEin(String ein) {
		this.ein = ein;
	}
	public String getTobaccoClass() {
		return tobaccoClass;
	}
	public void setTobaccoClass(String tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}
	
	
	public Double getPounds() {
		return pounds;
	}
	public void setPounds(Double pounds) {
		this.pounds = pounds;
	}
	public Long getFiscalYr() {
		return fiscalYr;
	}
	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}
	public Double getEstimatedTax() {
		return estimatedTax;
	}
	public void setEstimatedTax(Double estimatedTax) {
		this.estimatedTax = estimatedTax;
	}
	public Double getAmendedTax() {
		return amendedTax;
	}
	public void setAmendedTax(Double amendedTax) {
		this.amendedTax = amendedTax;
	}
	
}

/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The Class Assessment.
 *
 * @author rnasina
 */
public class TrueUp extends BaseTufaBean {
	/** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;

    private Long fiscalYear;
    
    private Date submittedDt;

    private String submittedInd;    
    
    private List<Assessment> assessments;
    
    private List<TrueUpVersion> versions;

	/**
	 * @return the fiscalYear
	 */
	public Long getFiscalYear() {
		return fiscalYear;
	}

	/**
	 * @param fiscalYear the fiscalYear to set
	 */
	public void setFiscalYear(Long fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	/**
	 * @return the submittedDt
	 */
	public Date getSubmittedDt() {
		return submittedDt;
	}

	/**
	 * @param submittedDt the submittedDt to set
	 */
	public void setSubmittedDt(Date submittedDt) {
		this.submittedDt = submittedDt;
	}

	public String getSubmittedInd() {
		return submittedInd;
	}

	public void setSubmittedInd(String submittedInd) {
		this.submittedInd = submittedInd;
	}

	/**
	 * @return the assessments
	 */
	public List<Assessment> getAssessments() {
		return assessments;
	}

	/**
	 * @param assessments the assessments to set
	 */
	public void setAssessments(List<Assessment> assessments) {
		this.assessments = assessments;
	}

	/**
	 * @return the versions
	 */
	public List<TrueUpVersion> getVersions() {
		return versions;
	}

	/**
	 * @param versions the versions to set
	 */
	public void setVersions(List<TrueUpVersion> versions) {
		this.versions = versions;
	}

	/**
	 * Initialize the true up versions from the embedded assessment versions.
	 */
	public void initialiazeVersions() {
		if(this.versions == null) {
			this.versions = new ArrayList<TrueUpVersion>();
		}
		if(assessments != null) {
			Assessment quarter = assessments.get(0);
			for(AssessmentVersion version:quarter.getVersions()) {
				TrueUpVersion trueUpVersion = new TrueUpVersion(version);
				this.versions.add(trueUpVersion);
			}
		}
	}

}

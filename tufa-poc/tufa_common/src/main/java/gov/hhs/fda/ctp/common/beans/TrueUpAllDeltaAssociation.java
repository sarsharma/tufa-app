package gov.hhs.fda.ctp.common.beans;

public class TrueUpAllDeltaAssociation extends BaseTufaBean {
	
	private String parentCmpyEIN;
	private String parentCmpyName;
	
	private String currentCmpyEIN;
	private String currentCmpyName;
	
	private String tobaccoClass;
	private long tobaccoClassId;
	private String fiscalQtr;
	private String fiscalYr;
	private String delta;
	private String permitType;
	private String deltaComment;
	private String deltaActionSpan;
	
	
	public String getDeltaComment() {
		return deltaComment;
	}
	public void setDeltaComment(String deltaComment) {
		this.deltaComment = deltaComment;
	}
	public String getParentCmpyEIN() {
		return parentCmpyEIN;
	}
	public void setParentCmpyEIN(String parentCmpyEIN) {
		this.parentCmpyEIN = parentCmpyEIN;
	}
	public String getParentCmpyName() {
		return parentCmpyName;
	}
	public void setParentCmpyName(String parentCmpyName) {
		this.parentCmpyName = parentCmpyName;
	}
	public String getCurrentCmpyEIN() {
		return currentCmpyEIN;
	}
	public void setCurrentCmpyEIN(String currentEIN) {
		this.currentCmpyEIN = currentEIN;
	}
	public String getCurrentCmpyName() {
		return currentCmpyName;
	}
	public void setCurrentCmpyName(String currentCmpyName) {
		this.currentCmpyName = currentCmpyName;
	}
	public String getTobaccoClass() {
		return tobaccoClass;
	}
	public void setTobaccoClass(String tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}
	public String getFiscalQtr() {
		return fiscalQtr;
	}
	public void setFiscalQtr(String fiscalQtr) {
		this.fiscalQtr = fiscalQtr;
	}
	public String getFiscalYr() {
		return fiscalYr;
	}
	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}
	public String getDelta() {
		return delta;
	}
	public void setDelta(String delta) {
		this.delta = delta;
	}
	public String getPermitType() {
		return permitType;
	}
	public void setPermitType(String permitType) {
		this.permitType = permitType;
	}
	public String getDeltaActionSpan() {
		return deltaActionSpan;
	}
	public void setDeltaActionSpan(String deltaActionSpan) {
		this.deltaActionSpan = deltaActionSpan;
	}
	
}

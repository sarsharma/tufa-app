package gov.hhs.fda.ctp.common.beans;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;


public class TrueUpCompareSearchCriteria {
    private String companyFilter;
    private String einFilter;
    private String permitNumFilter;
    private String tobaccoClassFilter;
    private String quarterFilter;
    private String deltaTaxFilter;
    private String permitTypeFilter;
    private Set<String>  statusTypeFilter;
    private Set<String> allDeltaStatusFilter;
    private String dataTypeFilter;
    private String originalNameFilter;

	private SelectTobaccoType selTobaccoTypes;
    
    public String getCompanyFilter() {
            return companyFilter;
    }
    public void setCompanyFilter(String companyFilter) {
            this.companyFilter = companyFilter;
    }
    
    public String getEinFilter() {
        return einFilter;
     }
    
    public void setEinFilter(String einFilter) {
         this.einFilter = einFilter;
     }
      
      public String getpermitNumFilter() {
          return permitNumFilter;
       }
      
     public void setpermitNumFilter(String permitNumFilter) {
          this.permitNumFilter = permitNumFilter;
     }
    
     public String getTobaccoClassFilter() {
            return tobaccoClassFilter;
    }
    public void setTobaccoClassFilter(String tobaccoClassFilter) {
            this.tobaccoClassFilter = tobaccoClassFilter;
    }
    public String getQuarterFilter() {
            return quarterFilter;
    }
    public void setQuarterFilter(String quarterFilter) {
            this.quarterFilter = quarterFilter;
    }
    public String getDeltaTaxFilter() {
            return deltaTaxFilter;
    }
    public void setDeltaTaxFilter(String deltaTaxFilter) {
            this.deltaTaxFilter = deltaTaxFilter;
    }
    public String getPermitTypeFilter() {
            return permitTypeFilter;
    }
    public void setPermitTypeFilter(String permitTypeFilter) {
            this.permitTypeFilter = permitTypeFilter;
    }
    public Set<String> getStatusTypeFilter() {
            return statusTypeFilter;
    }
    public void setStatusTypeFilter(Set<String> statusTypeFilter) {
            this.statusTypeFilter = statusTypeFilter;
    }
    
    public Set<String> getAllDeltaStatusTypeFilter() {
        return allDeltaStatusFilter;
     }
    
    public void setAllDeltaStatusTypeFilter(Set<String> allDeltaStatusFilter) {
        this.allDeltaStatusFilter = allDeltaStatusFilter;
     }
    
    public String getDataTypeFilter() {
        return dataTypeFilter;
    }

    public void setDataTypeFilter(String dataTypeFilter) {
        this.dataTypeFilter = dataTypeFilter;
    }

    public SelectTobaccoType getSelTobaccoTypes() {
        return selTobaccoTypes;
    }

    public void setSelTobaccoTypes(SelectTobaccoType selTobaccoTypes) {
        this.selTobaccoTypes = selTobaccoTypes;
    }
    
    /**
	 * @return the parentNameFilter
	 */
	public String getOriginalNameFilter() {
		return originalNameFilter;
	}
	/**
	 * @param parentNameFilter the parentNameFilter to set
	 */
	public void setOriginalNameFilter(String originalNameFilter) {
		this.originalNameFilter = originalNameFilter;
	}
    
	public boolean isAnyFilterSelected() {
	    return (StringUtils.isNoneEmpty(companyFilter)||StringUtils.isNoneEmpty(einFilter)||StringUtils.isNoneEmpty(permitNumFilter)||StringUtils.isNoneEmpty(tobaccoClassFilter)
	            ||StringUtils.isNoneEmpty(quarterFilter)||StringUtils.isNoneEmpty(deltaTaxFilter)||StringUtils.isNoneEmpty(permitTypeFilter)||StringUtils.isNoneEmpty(dataTypeFilter)
	            ||StringUtils.isNoneEmpty(originalNameFilter)
	            || (null != statusTypeFilter && statusTypeFilter.size()>0) || (null != allDeltaStatusFilter && allDeltaStatusFilter.size()>0));
	}
}

package gov.hhs.fda.ctp.common.beans;

import java.util.ArrayList;
import java.util.List;

public class TrueUpComparisonImporterDetail {
	
	private String period;
	private String periodTotal;
	private List<TrueUpComparisonImporterDetailPeriod> periodDetails;
	private List<TrueUpComparisonImporterDetail7501Period> period7501Details;
	private String removalQuantity;
	

	public List<TrueUpComparisonImporterDetail7501Period> getPeriod7501Details() {
		return period7501Details;
	}
	public void setPeriod7501Details(List<TrueUpComparisonImporterDetail7501Period> period7501Details) {
		this.period7501Details = period7501Details;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public List<TrueUpComparisonImporterDetailPeriod> getPeriodDetails() {
		return periodDetails;
	}
	public void setPeriodDetails(List<TrueUpComparisonImporterDetailPeriod> periodDetails) {
		this.periodDetails = periodDetails;
	}

	public String getPeriodTotal() {
		return periodTotal;
	}
	public void setPeriodTotal(String periodTotal) {
		this.periodTotal = periodTotal;
	}

	public TrueUpComparisonImporterDetail(){
		periodDetails = new ArrayList<>();
	}
	public String getRemovalQuantity() {
		return removalQuantity;
	}
	public void setRemovalQuantity(String removalQuantity) {
		this.removalQuantity = removalQuantity;
	}
}

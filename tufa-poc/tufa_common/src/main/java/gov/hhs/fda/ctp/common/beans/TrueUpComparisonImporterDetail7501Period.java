package gov.hhs.fda.ctp.common.beans;

import java.util.List;

public class TrueUpComparisonImporterDetail7501Period {
	private String period;
	private long lineNum;
	private String permitNum;
	private String qty;
	private String taxAmt;
	private String calcTaxRate;
	private String taxDiff;
	private String type;
	
	
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public long getLineNum() {
		return lineNum;
	}
	public void setLineNum(long lineNum) {
		this.lineNum = lineNum;
	}
	public String getPermitNum() {
		return permitNum;
	}
	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getTaxAmt() {
		return taxAmt;
	}
	public void setTaxAmt(String taxAmt) {
		this.taxAmt = taxAmt;
	}
	public String getCalcTaxRate() {
		return calcTaxRate;
	}
	public void setCalcTaxRate(String calcTaxRate) {
		this.calcTaxRate = calcTaxRate;
	}
	public String getTaxDiff() {
		return taxDiff;
	}
	public void setTaxDiff(String taxDiff) {
		this.taxDiff = taxDiff;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}

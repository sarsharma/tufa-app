package gov.hhs.fda.ctp.common.beans;

import java.util.List;

public class TrueUpComparisonImporterDetailPeriod {
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getPermit() {
		return permit;
	}
	public void setPermit(String permit) {
		this.permit = permit;
	}
	public String getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(String taxAmount) {
		this.taxAmount = taxAmount;
	}

	public long getPeriodId() {
		return periodId;
	}
	public void setPeriodId(long periodId) {
		this.periodId = periodId;
	}

	public long getPermitId() {
		return permitId;
	}
	public void setPermitId(long permitId) {
		this.permitId = permitId;
	}
	
	public long getQuarter() {
		return quarter;
	}
	public void setQuarter(long quarter) {
		this.quarter = quarter;
	}
	
	public List<Attachment> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}	

	/**
	 * @return the lineCount7501
	 */
	public long getLineCount7501() {
		return lineCount7501;
	}
	/**
	 * @param lineCount7501 the lineCount7501 to set
	 */
	public void setLineCount7501(long lineCount7501) {
		this.lineCount7501 = lineCount7501;
	}

	private String status;
	private String month;
	private long quarter;
	private String permit;
	private long periodId;
	private long permitId;
	private String taxAmount;
	private List<Attachment> attachments;
	private long lineCount7501;
	private long quantityRemoved;
	public long getQuantityRemoved() {
		return quantityRemoved;
	}
	public void setQuantityRemoved(long quantityRemoved) {
		this.quantityRemoved = quantityRemoved;
	}

}

package gov.hhs.fda.ctp.common.beans;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

//@JsonInclude(Include.NON_NULL)
public class TrueUpComparisonImporterIngestionDetail {
	
	private String period;
	private String periodTotal;
	private String periodEntryDateCategory;
	private long   periodEntryItemCount;
	private String removalQuantity;
	private List<TrueUpComparisonImporterIngestionDetailPeriod> periodDetails;

	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public List<TrueUpComparisonImporterIngestionDetailPeriod> getPeriodDetails() {
		return periodDetails;
	}
	public void setPeriodDetails(List<TrueUpComparisonImporterIngestionDetailPeriod> periodDetails) {
		this.periodDetails = periodDetails;
	}
	public String getPeriodTotal() {
		return periodTotal;
	}
	public void setPeriodTotal(String periodTotal) {
		this.periodTotal = periodTotal;
	}
	public TrueUpComparisonImporterIngestionDetail(){
		periodDetails = new ArrayList<>();
	}
	public String getPeriodEntryDateCategory() {
		return periodEntryDateCategory;
	}
	public void setPeriodEntryDateCategory(String periodEntryDateCategory) {
		this.periodEntryDateCategory = periodEntryDateCategory;
	}
	public long getPeriodEntryItemCount() {
		return periodEntryItemCount;
	}
	public void setPeriodEntryItemCount(long periodEntryItemCount) {
		this.periodEntryItemCount = periodEntryItemCount;
	}
	public String getRemovalQuantity() {
		return removalQuantity;
	}
	public void setRemovalQuantity(String removalQuantity) {
		this.removalQuantity = removalQuantity;
	}
	
	
	
	
}

package gov.hhs.fda.ctp.common.beans;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

//@JsonInclude(Include.NON_NULL)
public class TrueUpComparisonImporterIngestionDetailPeriod {
	public String getEntryNum() {
		return entryNum;
	}
	public void setEntryNum(String entryNum) {
		this.entryNum = entryNum;
	}
	public Date getEntrysummaryDate() {
		return entrysummaryDate;
	}
	public void setEntrysummaryDate(Date entrysummaryDate) {
		this.entrysummaryDate = entrysummaryDate;
	}
	public Date getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(Date date) {
		this.entryDate = date;
	}
	public long getLineNum() {
		return lineNum;
	}
	public void setLineNum(long lineNum) {
		this.lineNum = lineNum;
	}
	public String getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(String taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public long getQuarter() {
		return quarter;
	}
	public void setQuarter(long quarter) {
		this.quarter = quarter;
	}
	public double getQtyRemoved() {
		return qtyRemoved;
	}
	public void setQtyRemoved(double qtyRemoved) {
		this.qtyRemoved = qtyRemoved;
	}
	public long getEntryId() {
		return entryId;
	}
	public void setEntryId(long entryId) {
		this.entryId = entryId;
	}
	public String getExcludedFlag() {
		return excludedFlag;
	}
	public void setExcludedFlag(String excludedFlag) {
		this.excludedFlag = excludedFlag;
	}
	public String getReallocatedFlag() {
		return reallocatedFlag;
	}
	public void setReallocatedFlag(String reallocatedFlag) {
		this.reallocatedFlag = reallocatedFlag;
	}
	public long getEntryTypeCd() {
		return entryTypeCd;
	}
	public void setEntryTypeCd(long entryTypeCd) {
		this.entryTypeCd = entryTypeCd;
	}
	public String getInfosortOrder() {
		return infosortOrder;
	}
	public void setInfosortOrder(String infosortOrder) {
		this.infosortOrder = infosortOrder;
	}
	public String getSecondaryDateFlag() {
		return secondaryDateFlag;
	}
	public void setSecondaryDateFlag(String secondaryDateFlag) {
		this.secondaryDateFlag = secondaryDateFlag;
	}
	private String entryNum;
	private Date entrysummaryDate;
	private Date entryDate;
	private long lineNum;
	private String taxAmount;
	private double qtyRemoved;
	private String month;
	private long quarter;
	private long entryId;
	private String excludedFlag;
	private String reallocatedFlag;
	private long entryTypeCd;
	private String infosortOrder;
	private String secondaryDateFlag;
	private String missingFlag;
	public String getMissingFlag() {
		return missingFlag;
	}
	public void setMissingFlag(String missingFlag) {
		this.missingFlag = missingFlag;
	}

	
	
}

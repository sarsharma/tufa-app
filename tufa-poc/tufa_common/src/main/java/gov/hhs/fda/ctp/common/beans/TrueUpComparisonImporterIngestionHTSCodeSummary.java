package gov.hhs.fda.ctp.common.beans;

public class TrueUpComparisonImporterIngestionHTSCodeSummary {
	public String getHtsCode() {
		return htsCode;
	}
	public void setHtsCode(String htsCode) {
		this.htsCode = htsCode;
	}
	public long getLineCount() {
		return lineCount;
	}
	public void setLineCount(long lineCount) {
		this.lineCount = lineCount;
	}
	public Double getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getTobaccoClass() {
		return tobaccoClass;
	}
	public void setTobaccoClass(String tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}
	private String htsCode;
	private String tobaccoClass;
	private long lineCount;
	private Double taxAmount;
}

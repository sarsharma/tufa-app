package gov.hhs.fda.ctp.common.beans;

import java.util.List;

public class TrueUpComparisonManuDetail{
	
	private String periodId;
	private double periodTotal;
	private double removalQuantity;
	private String month;
	private double removalQtyTobaccoClass1;
	private double taxAmountTobaccoClass1;
	private double removalQtyTobaccoClass2;
	private double taxAmountTobaccoClass2;
	
	public String getPeriodId() {
		return periodId;
	}
	public void setPeriodId(String periodId) {
		this.periodId = periodId;
	}
	public double getPeriodTotal() {
		return periodTotal;
	}
	public void setPeriodTotal(double periodTotal) {
		this.periodTotal = periodTotal;
	}
	public double getRemovalQuantity() {
		return removalQuantity;
	}
	public void setRemovalQuantity(double removalQuantity) {
		this.removalQuantity = removalQuantity;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public double getRemovalQtyTobaccoClass1() {
		return removalQtyTobaccoClass1;
	}
	public void setRemovalQtyTobaccoClass1(double removalQtyTobaccoClass1) {
		this.removalQtyTobaccoClass1 = removalQtyTobaccoClass1;
	}
	public double getTaxAmountTobaccoClass1() {
		return taxAmountTobaccoClass1;
	}
	public void setTaxAmountTobaccoClass1(double taxAmountTobaccoClass1) {
		this.taxAmountTobaccoClass1 = taxAmountTobaccoClass1;
	}
	public double getRemovalQtyTobaccoClass2() {
		return removalQtyTobaccoClass2;
	}
	public void setRemovalQtyTobaccoClass2(double removalQtyTobaccoClass2) {
		this.removalQtyTobaccoClass2 = removalQtyTobaccoClass2;
	}
	public double getTaxAmountTobaccoClass2() {
		return taxAmountTobaccoClass2;
	}
	public void setTaxAmountTobaccoClass2(double taxAmountTobaccoClass2) {
		this.taxAmountTobaccoClass2 = taxAmountTobaccoClass2;
	}

}

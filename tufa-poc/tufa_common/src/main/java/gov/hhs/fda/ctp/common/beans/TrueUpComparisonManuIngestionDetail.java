package gov.hhs.fda.ctp.common.beans;

import java.util.HashMap;
import java.util.Map;

public class TrueUpComparisonManuIngestionDetail {
	private String periodId;
	private double periodTotalIngested;
	private double removalQuantityIngested;
	private String month;
	private double removalQtyTobaccoClass1Ingested;
	private double taxAmountTobaccoClass1Ingested;
	private double removalQtyTobaccoClass2Ingested;
	private double taxAmountTobaccoClass2Ingested;
	
	public String getPeriodId() {
		return periodId;
	}
	public void setPeriodId(String periodId) {
		this.periodId = periodId;
	}
	public double getPeriodTotalIngested() {
		return periodTotalIngested;
	}
	public void setPeriodTotalIngested(double periodTotalIngested) {
		this.periodTotalIngested = periodTotalIngested;
	}
	public double getRemovalQuantityIngested() {
		return removalQuantityIngested;
	}
	public void setRemovalQuantityIngested(double removalQuantityIngested) {
		this.removalQuantityIngested = removalQuantityIngested;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public double getRemovalQtyTobaccoClass1Ingested() {
		return removalQtyTobaccoClass1Ingested;
	}
	public void setRemovalQtyTobaccoClass1Ingested(double removalQtyTobaccoClass1Ingested) {
		this.removalQtyTobaccoClass1Ingested = removalQtyTobaccoClass1Ingested;
	}
	public double getTaxAmountTobaccoClass1Ingested() {
		return taxAmountTobaccoClass1Ingested;
	}
	public void setTaxAmountTobaccoClass1Ingested(double taxAmountTobaccoClass1Ingested) {
		this.taxAmountTobaccoClass1Ingested = taxAmountTobaccoClass1Ingested;
	}
	public double getRemovalQtyTobaccoClass2Ingested() {
		return removalQtyTobaccoClass2Ingested;
	}
	public void setRemovalQtyTobaccoClass2Ingested(double removalQtyTobaccoClass2Ingested) {
		this.removalQtyTobaccoClass2Ingested = removalQtyTobaccoClass2Ingested;
	}
	public double getTaxAmountTobaccoClass2Ingested() {
		return taxAmountTobaccoClass2Ingested;
	}
	public void setTaxAmountTobaccoClass2Ingested(double taxAmountTobaccoClass2Ingested) {
		this.taxAmountTobaccoClass2Ingested = taxAmountTobaccoClass2Ingested;
	}
	public void setTaxAmountTobaccoClassIngested(double ingestedTax, double tobaccoClass1TTBvolume, double tobaccoClass2TTBvolume, double taxAmountTobaccoClass1Ingested, double taxAmountTobaccoClass2Ingested, String tobaccoClass) {
		 HashMap<String, Double> taxRate = new HashMap<String, Double>();
		 taxRate.put("Chew" , 0.5033 );
		 taxRate.put("Snuff" , 1.51 );
		 taxRate.put("Pipe" , 2.8311 );
		 taxRate.put("RYO" , 24.78 );
		 taxRate.put("Cigarette" , 0.05033 );
		 taxRate.put("Cigar" , 2.13 );
		 double estimatedTax1 = 0;
		 double estimatedTax2 = 0;
		if(tobaccoClass.equalsIgnoreCase("Chew-Snuff")){
			estimatedTax1 = tobaccoClass1TTBvolume * taxRate.get("Chew");
			estimatedTax2 = tobaccoClass2TTBvolume * taxRate.get("Snuff");
			}
		else if(tobaccoClass.equalsIgnoreCase("Pipe-Roll Your Own")){
			estimatedTax1 = tobaccoClass1TTBvolume * taxRate.get("Pipe");
			estimatedTax2 = tobaccoClass2TTBvolume * taxRate.get("RYO");	
		}else {
			estimatedTax1 = tobaccoClass1TTBvolume * taxRate.get(tobaccoClass);
		}
		
		if(estimatedTax1 == 0 && estimatedTax2 == 0 && taxAmountTobaccoClass1Ingested >= 0 && taxAmountTobaccoClass2Ingested >= 0) {
			this.taxAmountTobaccoClass1Ingested = ingestedTax/2 + taxAmountTobaccoClass1Ingested;
			this.taxAmountTobaccoClass2Ingested = ingestedTax/2 + taxAmountTobaccoClass2Ingested;
		}
		else if(estimatedTax1 == 0 && estimatedTax2 == 0 && taxAmountTobaccoClass1Ingested > 0 ) {
			this.taxAmountTobaccoClass1Ingested = taxAmountTobaccoClass1Ingested + ingestedTax;
		}
		else if(estimatedTax1 == 0 && estimatedTax2 == 0 && taxAmountTobaccoClass2Ingested > 0) {
			this.taxAmountTobaccoClass2Ingested = taxAmountTobaccoClass2Ingested + ingestedTax;
		}
		else {
			this.taxAmountTobaccoClass1Ingested = ((estimatedTax1/(estimatedTax1+estimatedTax2))*ingestedTax) + taxAmountTobaccoClass1Ingested;
			this.taxAmountTobaccoClass2Ingested = ((estimatedTax2/(estimatedTax1+estimatedTax2))*ingestedTax) + taxAmountTobaccoClass2Ingested;		
		}
	}
	
	
}

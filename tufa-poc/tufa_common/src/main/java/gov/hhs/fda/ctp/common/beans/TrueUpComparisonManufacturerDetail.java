package gov.hhs.fda.ctp.common.beans;

import java.util.ArrayList;
import java.util.List;

public class TrueUpComparisonManufacturerDetail {

	
	private int quarter;
	private String permit;
	private long permitId;
	private String periodTotal;
	private String periodTotal2;
	private String periodTotalCombined;
	private List<TrueUpComparisonManufacturerDetailPermit> permitDetails;
	private String ingestedTax;
	private String delta;
	
	public TrueUpComparisonManufacturerDetail(){
		permitDetails = new ArrayList<>();
	}

	public String getPermit() {
		return permit;
	}
	public void setPermit(String permit) {
		this.permit = permit;
	}
	public String getPeriodTotal() {
		return periodTotal;
	}
	public void setPeriodTotal(String periodTotal) {
		this.periodTotal = periodTotal;
	}
	public List<TrueUpComparisonManufacturerDetailPermit> getPermitDetails() {
		return permitDetails;
	}
	public void setPermitDetails(List<TrueUpComparisonManufacturerDetailPermit> permitDetails) {
		this.permitDetails = permitDetails;
	}
	public int getQuarter() {
		return quarter;
	}
	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}
	public String getPeriodTotal2() {
		return periodTotal2;
	}
	public void setPeriodTotal2(String periodTotal2) {
		this.periodTotal2 = periodTotal2;
	}
	public String getPeriodTotalCombined() {
		return periodTotalCombined;
	}
	public void setPeriodTotalCombined(String periodTotalCombined) {
		this.periodTotalCombined = periodTotalCombined;
	}
	public long getPermitId() {
		return permitId;
	}
	public void setPermitId(long permitId) {
		this.permitId = permitId;
	}

	public String getIngestedTax() {
		return ingestedTax;
	}

	public void setIngestedTax(String ingestedTax) {
		this.ingestedTax = ingestedTax;
	}

	public String getDelta() {
		return delta;
	}

	public void setDelta(String delta) {
		this.delta = delta;
	}

}

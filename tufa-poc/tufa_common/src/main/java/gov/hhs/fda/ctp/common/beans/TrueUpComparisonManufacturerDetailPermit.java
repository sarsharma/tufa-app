package gov.hhs.fda.ctp.common.beans;

import java.util.List;

public class TrueUpComparisonManufacturerDetailPermit {
	
	
	private String status;
	private String month;
	private long periodId;
	private String taxAmount;
	private String taxAmount2; // Used for Pipe/RYO or Chew/Snuff
	private String taxTotalAmount; // Used for SubTotal of Pipe/RYO or Chew/Snuff
	private String ingestedTax;
	
	private String tobaccoclass1ttbvolume;// Used for Pipe/RYO or Chew/Snuff
    private String tobaccoclass2ttbvolume;// Used for Pipe/RYO or Chew/Snuff
    private String tobaccoclass1ttbtax;// Used for Pipe/RYO or Chew/Snuff
    private String tobaccoclass2ttbtax;// Used for Pipe/RYO or Chew/Snuff
    private String calculatedDelta1;// Used for Pipe/RYO or Chew/Snuff
    private String calculatedDelta2;// Used for Pipe/RYO or Chew/Snuff
    
    private String tobaccoclass1Tufattbvolume;// Used for Pipe/RYO or Chew/Snuff    
	private String tobaccoclass2Tufattbvolume;// Used for Pipe/RYO or Chew/Snuff
	private String tobaccoTotalTufattbvolume;// Used for Pipe/RYO or Chew/Snuff
	
	private String ttb5000Cnt;
    private String ingestedThreshCnt;
    
    private List<Attachment> attachments;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(String taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getTaxAmount2() {
		return taxAmount2;
	}
	public void setTaxAmount2(String taxAmount2) {
		this.taxAmount2 = taxAmount2;
	}

	public String getTaxTotalAmount() {
		return taxTotalAmount;
	}
	public void setTaxTotalAmount(String taxTotalAmount) {
		this.taxTotalAmount = taxTotalAmount;
	}
	public long getPeriodId() {
		return periodId;
	}
	public void setPeriodId(long periodId) {
		this.periodId = periodId;
	}
	public String getIngestedTax() {
		return ingestedTax;
	}
	public void setIngestedTax(String ingestedTax) {
		this.ingestedTax = ingestedTax;
	}
	//TTB volume data 
    public String getTobaccoClass1TTBvolume() {
          return tobaccoclass1ttbvolume;
    }
    public void setTobaccoClass1TTBvolume(String tobaccoclass1ttbvolume) {
          this.tobaccoclass1ttbvolume = tobaccoclass1ttbvolume;
    }
    public String getTobaccoClass2TTBvolume() {
          return tobaccoclass2ttbvolume;
    }
    public void setTobaccoClass2TTBvolume(String tobaccoclass2ttbvolume) {
          this.tobaccoclass2ttbvolume = tobaccoclass2ttbvolume;
    }
    
    //TTB Calculated Delta
    public String getCalculatedDelta1() {
          return calculatedDelta1;
    }
    public void setCalculatedDelta1(String calculatedDelta1) {
          this.calculatedDelta1 = calculatedDelta1;
    }
    public String getCalculatedDelta2() {
          return calculatedDelta2;
    }
    public void setCalculatedDelta2(String calculatedDelta2) {
          this.calculatedDelta2 = calculatedDelta2;
    }
       
    //TTB Calculated Tax 
    public String getTobaccoClass1TTBTAX() {
          return tobaccoclass1ttbtax;
    }
    public void setTobaccoClass1TTBTAX(String tobaccoclass1ttbtax) {
          this.tobaccoclass1ttbtax = tobaccoclass1ttbtax;
    }
    public String getTobaccoClass2TTBTAX() {
          return tobaccoclass2ttbtax;
    }
    public void setTobaccoClass2TTBTAX(String tobaccoclass2ttbtax) {
          this.tobaccoclass2ttbtax = tobaccoclass2ttbtax;
    }
	public String getTtb5000Cnt() {
		return ttb5000Cnt;
	}
	public void setTtb5000Cnt(String ttb5000Cnt) {
		this.ttb5000Cnt = ttb5000Cnt;
	}
	public String getIngestedThreshCnt() {
		return ingestedThreshCnt;
	}
	public void setIngestedThreshCnt(String ingestedThreshCnt) {
		this.ingestedThreshCnt = ingestedThreshCnt;
	}
	public List<Attachment> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}
	public String getTobaccoclass1Tufattbvolume() {
		return tobaccoclass1Tufattbvolume;
	}
	public void setTobaccoclass1Tufattbvolume(String tobaccoclass1Tufattbvolume) {
		this.tobaccoclass1Tufattbvolume = tobaccoclass1Tufattbvolume;
	}
    public String getTobaccoclass2Tufattbvolume() {
		return tobaccoclass2Tufattbvolume;
	}
	public void setTobaccoclass2Tufattbvolume(String tobaccoclass2Tufattbvolume) {
		this.tobaccoclass2Tufattbvolume = tobaccoclass2Tufattbvolume;
	}
	public String getTobaccoTotalTufattbvolume() {
		return tobaccoTotalTufattbvolume;
	}
	public void setTobaccoTotalTufattbvolume(String tobaccoTotalTufattbvolume) {
		this.tobaccoTotalTufattbvolume = tobaccoTotalTufattbvolume;
	}
}

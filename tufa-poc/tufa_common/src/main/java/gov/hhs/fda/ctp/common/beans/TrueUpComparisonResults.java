package gov.hhs.fda.ctp.common.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrueUpComparisonResults {
	
	private String legalName;
	private String ein;
	private String permitNum;
	private String tobaccoClass;
	private String quarter;
	private Double totalDeltaTax;
	private Double abstotalDeltaTax;
	private String permitType;
	private String fileType;
	private long companyId;
	private String status;
	private String allDeltaStatus;
	private String allDeltaCurrentStatus;
    private String dataType;
    private String originalName;
    private String originalEIN;
    
    private String fiscalYr;
    private String deltaActionSpan;
    
	private Integer ttlCntDeltaChng;
	private Integer ttlCntDeltaChngZero;
	private Integer ttlCntAccptApprv;
	private Integer ttlCntInProgress;
	private Integer ttlDeltaCntInReview;
	private Integer ttlDeltaCntInProgress;
	
	private String deltaComment;
	
	private List<ComparisonAllDeltaComments> userComments;
	private boolean affectDeltaFlag;
	
	private Map<String, Double[]> tobaccoSubTypeTxVol;
	
	// Accommodating fields for 3712 - Notify user of affected deltas
	// Taxes for this tobacco type across all four qtrs
	private Integer affectedDeltaCnt;
	
	private Double qtr1;
	private Double qtr2;
	private Double qtr3;
	private Double qtr4;
	private boolean keepOriginalComment;
	private String deltaexcisetaxFlag;
	
	public String getLegalName() {
		return legalName;
	}
	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}
	
	public String getEin() {
		return ein;
	}
	public void setEin(String ein) {
		this.ein = ein;
	}
	
	public String getPermitNum() {
		return permitNum;
	}
	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}
	public String getTobaccoClass() {
		return tobaccoClass;
	}
	public void setTobaccoClass(String tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}
	public String getQuarter() {
		return quarter;
	}
	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}
	public Double getTotalDeltaTax() {
		return totalDeltaTax;
	}
	public void setTotalDeltaTax(Double totalDeltaTax) {
		this.totalDeltaTax = totalDeltaTax;
	}
	public String getPermitType() {
		return permitType;
	}
	public void setPermitType(String permitType) {
		this.permitType = permitType;
	}
	public Double getAbstotalDeltaTax() {
		return abstotalDeltaTax;
	}
	public void setAbstotalDeltaTax(Double abstotalDeltaTax) {
		this.abstotalDeltaTax = abstotalDeltaTax;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getAllDeltaStatus() {
		return allDeltaStatus;
	}
	public void setAllDeltaStatus(String alldeltastatus) {
		this.allDeltaStatus = alldeltastatus;
	}
	
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	
	public Integer getTtlCntDeltaChng() {
		return ttlCntDeltaChng;
	}
	public void setTtlCntDeltaChng(Integer ttlCntDeltaChng) {
		this.ttlCntDeltaChng = ttlCntDeltaChng;
	}
	public Integer getTtlCntDeltaChngZero() {
		return ttlCntDeltaChngZero;
	}
	public void setTtlCntDeltaChngZero(Integer ttlCntDeltaChngZero) {
		this.ttlCntDeltaChngZero = ttlCntDeltaChngZero;
	}
	public Integer getttlDeltaCntInReview() {
		return ttlDeltaCntInReview;
	}
	public void setttlDeltaCntInReview(Integer ttlDeltaCntInReview) {
		this.ttlDeltaCntInReview = ttlDeltaCntInReview;
	}
	
	public Double getQtr1() {
		return qtr1;
	}
	public void setQtr1(Double qtr1) {
		this.qtr1 = qtr1;
	}
	public Double getQtr2() {
		return qtr2;
	}
	public void setQtr2(Double qtr2) {
		this.qtr2 = qtr2;
	}
	public Double getQtr3() {
		return qtr3;
	}
	public void setQtr3(Double qtr3) {
		this.qtr3 = qtr3;
	}
	public Double getQtr4() {
		return qtr4;
	}
	public void setQtr4(Double qtr4) {
		this.qtr4 = qtr4;
	}
	public Integer getttlDeltaCntInProgress() {
		return ttlDeltaCntInProgress;
	}
	public void setttlDeltaCntInProgress(Integer ttlDeltaCntInProgress) {
		this.ttlDeltaCntInProgress = ttlDeltaCntInProgress;
	}
	
	
	public Integer getTtlCntAccptApprv() {
		return ttlCntAccptApprv;
	}
	public void setTtlCntAccptApprv(Integer ttlCntAccptApprv) {
		this.ttlCntAccptApprv = ttlCntAccptApprv;
	}
	
	
	

	public Integer getTtlCntInProgress() {
		return ttlCntInProgress;
	}
	public void setTtlCntInProgress(Integer ttlCntInProgress) {
		this.ttlCntInProgress = ttlCntInProgress;
	}

        public String getDataType() {
            return dataType;
        }

        public void setDataType(String dataType) {
            this.dataType = dataType;
        }
		/**
		 * @return the originalName
		 */
		public String getOriginalName() {
			return originalName;
		}
		/**
		 * @param originalName the originalName to set
		 */
		public void setOriginalName(String parentName) {
			this.originalName = parentName;
		}
		/**
		 * @return the originalEIN
		 */
		public String getOriginalEIN() {
			return originalEIN;
		}
		/**
		 * @param originalEIN the originalEIN to set
		 */
		public void setOriginalEIN(String originalEIN) {
			this.originalEIN = originalEIN;
		}
		public String getFiscalYr() {
			return fiscalYr;
		}
		public void setFiscalYr(String fiscalYr) {
			this.fiscalYr = fiscalYr;
		}
		public String getDeltaActionSpan() {
			return deltaActionSpan;
		}
		public void setDeltaActionSpan(String deltaActionSpan) {
			this.deltaActionSpan = deltaActionSpan;
		}
		public String getAllDeltaCurrentStatus() {
			return allDeltaCurrentStatus;
		}
		public void setAllDeltaCurrentStatus(String allDeltaCurrentStatus) {
			this.allDeltaCurrentStatus = allDeltaCurrentStatus;
		}

		public Integer getAffectedDeltaCnt() {
			return affectedDeltaCnt;
		}
		public void setAffectedDeltaCnt(Integer affectedDeltaCnt) {
			this.affectedDeltaCnt = affectedDeltaCnt;
		}

		public String getDeltaComment() {
			return deltaComment;
		}
		public void setDeltaComment(String deltaComment) {
			this.deltaComment = deltaComment;
		}
		
		public Map<String, Double[]> getTobaccoSubTypeTxVol() {
			return tobaccoSubTypeTxVol;
		}
		public void setTobaccoSubTypeTxVol(Map<String, Double[]> tobaccoSubTypeTxVol) {
			this.tobaccoSubTypeTxVol = tobaccoSubTypeTxVol;
		}
		
		public boolean getKeepOriginalComment() {
			return this.keepOriginalComment;
		}
		public void setKeepOriginalComment(boolean keep) {
			this.keepOriginalComment = keep;
		}
		
		public boolean getaffectDeltaFlag() {
			return this.affectDeltaFlag;
		}
		public void setaffectDeltaFlag(boolean affectDeltaFlag) {
			this.affectDeltaFlag = affectDeltaFlag;
		}
		public String getDeltaexcisetaxFlag() {
			return deltaexcisetaxFlag;
		}
		public void setDeltaexcisetaxFlag(String deltaexcisetaxFlag) {
			this.deltaexcisetaxFlag = deltaexcisetaxFlag;
		}
		public String getFileType() {
			return fileType;
		}
		public void setFileType(String fileType) {
			this.fileType = fileType;
		}
		public List<ComparisonAllDeltaComments> getUserComments() {
			return userComments;
		}
		public void setUserComments(List<ComparisonAllDeltaComments> userComments) {
			this.userComments = userComments;
		}
	
}

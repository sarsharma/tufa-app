package gov.hhs.fda.ctp.common.beans;

import java.util.List;

public class TrueUpComparisonSummary {
	
	
	
	
	private int numColumns;
	private List<String> cellValues;
	
	
	public List<String> getCellValues() {
		return cellValues;
	}

	public void setCellValues(List<String> cellValues) {
		this.cellValues = cellValues;
	}
	public int getNumColumns() {
		return numColumns;
	}

	public void setNumColumns(int numColumns) {
		this.numColumns = numColumns;
	}
	
	
}

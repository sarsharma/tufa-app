package gov.hhs.fda.ctp.common.beans;

public class TrueUpDocument extends BaseTufaBean {

	private Long fiscalYr;

	private Long versionNum;

	private String trueUpFilename;

	private String docDesc;
	
	private Long errorCount;

	private byte[] trueupDocument;

	public Long getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public Long getVersionNum() {
		return versionNum;
	}

	public void setVersionNum(Long versionNum) {
		this.versionNum = versionNum;
	}

	public String getTrueUpFilename() {
		return trueUpFilename;
	}

	public void setTrueUpFilename(String trueUpFilename) {
		this.trueUpFilename = trueUpFilename;
	}

	public String getDocDesc() {
		return docDesc;
	}

	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}

	/**
	 * @return the errorCount
	 */
	public Long getErrorCount() {
		return errorCount;
	}

	/**
	 * @param errorCount the errorCount to set
	 */
	public void setErrorCount(Long errorCount) {
		this.errorCount = errorCount;
	}

	public byte[] getTrueupDocument() {
		return trueupDocument;
	}

	public void setTrueupDocument(byte[] trueupDocument) {
		this.trueupDocument = trueupDocument;
	}

}

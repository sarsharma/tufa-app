/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

/**
 * @author tgunter
 *
 */
public class TrueUpErrorsExport {
	String filename;
	String csv;
	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}
	/**
	 * @param filename the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}
	/**
	 * @return the csv
	 */
	public String getCsv() {
		return csv;
	}
	/**
	 * @param csv the csv to set
	 */
	public void setCsv(String csv) {
		this.csv = csv;
	}
	
}

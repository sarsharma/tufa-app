/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

import java.util.List;
import java.util.Set;

/**
 * The Class AssessmentFilter.
 *
 * @author rnasina
 */
public class TrueUpFilter extends BaseTufaBean {
	
	/** The status filters. */
	private Set<String> statusFilters;
	
	/** The fiscal yr filters. */
	private Set<Long> fiscalYrFilters;
	
	/** The results. */
	private List<TrueUp> results;

	/**
	 * Gets the results.
	 *
	 * @return the results
	 */
	public List<TrueUp> getResults() {
		return results;
	}

	/**
	 * Sets the results.
	 *
	 * @param results            the results to set
	 */
	public void setResults(List<TrueUp> results) {
		this.results = results;
	}

	/**
	 * Gets the status filters.
	 *
	 * @return the statusFilters
	 */
	public Set<String> getStatusFilters() {
		return statusFilters;
	}

	/**
	 * Sets the status filters.
	 *
	 * @param statusFilters            the statusFilters to set
	 */
	public void setStatusFilters(Set<String> statusFilters) {
		this.statusFilters = statusFilters;
	}


	/**
	 * Gets the fiscal yr filters.
	 *
	 * @return the fiscalYrFilters
	 */
	public Set<Long> getFiscalYrFilters() {
		return fiscalYrFilters;
	}

	/**
	 * Sets the fiscal yr filters.
	 *
	 * @param fiscalYrFilters            the fiscalYrFilters to set
	 */
	public void setFiscalYrFilters(Set<Long> fiscalYrFilters) {
		this.fiscalYrFilters = fiscalYrFilters;
	}

}

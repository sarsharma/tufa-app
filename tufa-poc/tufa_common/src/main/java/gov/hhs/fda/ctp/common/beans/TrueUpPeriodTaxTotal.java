package gov.hhs.fda.ctp.common.beans;

public class TrueUpPeriodTaxTotal {
	public String getTobaccoClass() {
		return tobaccoClass;
	}
	public void setTobaccoClass(String tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}
	public Double getTotalTaxes() {
		return totalTaxes;
	}
	public void setTotalTaxes(Double totalTaxes) {
		this.totalTaxes = totalTaxes;
	}

	private String tobaccoClass;
	private Double totalTaxes;

}

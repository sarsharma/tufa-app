/**
 * 
 */
package gov.hhs.fda.ctp.common.beans;

/**
 * @author Anthony.Gunter
 *
 */
public class TrueUpVersion extends BaseTufaBean {
	private int versionNum;
	private String createdDt;
	private String createdBy;
	private int cigarQtr;
	
	/**
	 * Create an empty trueupversion.
	 */
	public TrueUpVersion() {
	}
	
	/**
	 * Create a TrueUpVersion from the corresponding AssessmentVersion.
	 * @param version
	 */
	public TrueUpVersion(AssessmentVersion version) {
		this.versionNum = (int)version.getVersionNum();
		this.createdBy = version.getCreatedBy();
		this.createdDt = version.getCreatedDt();
		this.cigarQtr = (int)version.getCigarQtr();
	}

	/**
	 * @return the versionNum
	 */
	public int getVersionNum() {
		return versionNum;
	}
	/**
	 * @param versionNum the versionNum to set
	 */
	public void setVersionNum(int versionNum) {
		this.versionNum = versionNum;
	}
	/**
	 * @return the createdDt
	 */
	public String getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(String createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the cigarQtr
	 */
	public int getCigarQtr() {
		return cigarQtr;
	}

	/**
	 * @param cigarQtr the cigarQtr to set
	 */
	public void setCigarQtr(int cigarQtr) {
		this.cigarQtr = cigarQtr;
	}
	
}

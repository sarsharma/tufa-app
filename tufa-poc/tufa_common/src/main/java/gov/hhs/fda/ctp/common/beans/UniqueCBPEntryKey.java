package gov.hhs.fda.ctp.common.beans;

import java.util.Objects;

public class UniqueCBPEntryKey {

	private Long cbpImporterId;
	private Long cbpEntryId;

	/**
	 * Constructor
	 * @param id
	 * @param htsCd
	 */
	public UniqueCBPEntryKey(Long cbpImporterId, Long cbpEntryId ) {
		this.cbpImporterId = cbpImporterId;
		this.cbpEntryId = cbpEntryId;
	}


	/**
	 * @return the cbpImporterId
	 */
	public Long getCbpImporterId() {
		return cbpImporterId;
	}
	/**
	 * @param cbpImporterId the cbpImporterId to set
	 */
	public void setCbpImporterId(Long cbpImporterId) {
		this.cbpImporterId = cbpImporterId;
	}


	@Override
	public boolean equals(Object obj) {
	    if (obj == null) {
	        return false;
	    }
	    if (this.getClass() != obj.getClass()) {
	        return false;
	    }
	    if (!UniqueCBPEntryKey.class.isAssignableFrom(obj.getClass())) {
	        return false;
	    }

	    final UniqueCBPEntryKey key = (UniqueCBPEntryKey) obj;
	    if(key.getCbpImporterId() == null || key.getCbpEntryId() == null) {
	    	return false;
	    }
	    if(this.cbpImporterId == null || this.cbpEntryId == null) {
	    	return false;
	    }
	    return key.getCbpImporterId().equals(cbpImporterId) && key.getCbpEntryId().equals(cbpEntryId);
	}

	@Override
    public int hashCode() {
        return Objects.hash(cbpImporterId, cbpEntryId);
    }


	public Long getCbpEntryId() {
		return cbpEntryId;
	}


	public void setCbpEntryId(Long cbpEntryId) {
		this.cbpEntryId = cbpEntryId;
	}


}

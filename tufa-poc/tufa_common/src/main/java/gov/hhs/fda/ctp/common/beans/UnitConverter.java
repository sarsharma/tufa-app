package gov.hhs.fda.ctp.common.beans;

public class UnitConverter {
	
	private Long referenceTypeValueId;
	private String code;
	private String value;
	private Double kgstopoundConversion;
	

	//	ID
	public Long getReferenceTypeValueId() {
		return referenceTypeValueId;
	}
	public void setReferenceTypeValueId(Long referenceTypeValueId) {
		this.referenceTypeValueId = referenceTypeValueId;
	}
	
//	Code
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
//	Value
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public final Double getKgstopoundConversion() {
		return kgstopoundConversion;
	}
	public final void setKgstopoundConversion(Double kgstopoundConversion) {
		this.kgstopoundConversion = kgstopoundConversion;
	}
}
	
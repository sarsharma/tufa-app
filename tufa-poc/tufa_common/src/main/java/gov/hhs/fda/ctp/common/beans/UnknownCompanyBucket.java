package gov.hhs.fda.ctp.common.beans;

public class UnknownCompanyBucket extends BaseTufaBean {
	private String tobaccoClasses;
	private Double totalTaxes;
	private String importerEin;
	private String importerName;
	private String consigneeEin;
	private Long   fiscalYear;
	private Long   cbpImporterId;
	private boolean existsInTufaFlag;
	private boolean consignSSNFlag;
	private String useEntityInd;
	
	/**
	 * @return the tobaccoType
	 */
	public String getTobaccoClasses() {
		return tobaccoClasses;
	}
	/**
	 * @param tobaccoType the tobaccoType to set
	 */
	public void setTobaccoClasses(String tobaccoTypes) {
		this.tobaccoClasses = tobaccoTypes;
	}
	/**
	 * @return the totalTaxes
	 */
	public Double getTotalTaxes() {
		return totalTaxes;
	}
	/**
	 * @param totalTaxes the totalTaxes to set
	 */
	public void setTotalTaxes(Double totalTaxes) {
		this.totalTaxes = totalTaxes;
	}
	/**
	 * @return the importerName
	 */
	public String getImporterName() {
		return importerName;
	}
	/**
	 * @param importerName the importerName to set
	 */
	public void setImporterName(String importerName) {
		this.importerName = importerName;
	}
	/**
	 * @return the consigneeName
	 */
	public String getConsigneeEin() {
		return consigneeEin;
	}
	/**
	 * @param consigneeName the consigneeName to set
	 */
	public void setConsigneeEin(String consigneeEin) {
		this.consigneeEin = consigneeEin;
	}
	public String getImporterEin() {
		return importerEin;
	}
	public void setImporterEin(String importerEin) {
		this.importerEin = importerEin;
	}
	/**
	 * @return the fiscalYear
	 */
	public Long getFiscalYear() {
		return fiscalYear;
	}
	/**
	 * @param fiscalYear the fiscalYear to set
	 */
	public void setFiscalYear(Long fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	/**
	 * @return the cbpImporterId
	 */
	public Long getCbpImporterId() {
		return cbpImporterId;
	}
	/**
	 * @param cbpImporterId the cbpImporterId to set
	 */
	public void setCbpImporterId(Long cbpImporterId) {
		this.cbpImporterId = cbpImporterId;
	}
	/**
	 * @return the existsInTufaFlag
	 */
	public boolean isExistsInTufaFlag() {
		return existsInTufaFlag;
	}
	/**
	 * @param existsInTufaFlag the existsInTufaFlag to set
	 */
	public void setExistsInTufaFlag(boolean existsInTufaFlag) {
		this.existsInTufaFlag = existsInTufaFlag;
	}
	/**
	 * @return the consignSSNFlag
	 */
	public boolean isConsignSSNFlag() {
		return consignSSNFlag;
	}
	/**
	 * @param consignSSNFlag the consignSSNFlag to set
	 */
	public void setConsignSSNFlag(boolean consignSSNFlag) {
		this.consignSSNFlag = consignSSNFlag;
	}
	/**
	 * @return the useEntityInd
	 */
	public String getUseEntityInd() {
		return useEntityInd;
	}
	/**
	 * @param useEntityInd the useEntityInd to set
	 */
	public void setUseEntityInd(String useEntityInd) {
		this.useEntityInd = useEntityInd;
	}
}

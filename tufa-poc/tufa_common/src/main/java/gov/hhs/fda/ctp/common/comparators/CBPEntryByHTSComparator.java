/**
 * 
 */
package gov.hhs.fda.ctp.common.comparators;
import java.util.Comparator;
import gov.hhs.fda.ctp.common.beans.CBPEntry;

/**
 * @author tgunter
 *
 */
public class CBPEntryByHTSComparator implements Comparator<CBPEntry> {
		@Override
	    public int compare(CBPEntry entry1, CBPEntry entry2) {
	        return entry1.getHtsCd().compareTo(entry2.getHtsCd());
	    }
}

package gov.hhs.fda.ctp.common.comparators;

import java.util.Comparator;

import gov.hhs.fda.ctp.common.beans.CBPEntry;

public class CBPEntryByIdComparator implements Comparator<CBPEntry> {

	@Override
    public int compare(CBPEntry entry1, CBPEntry entry2) {
        return entry2.getCbpEntryId().compareTo(entry1.getCbpEntryId());
    }
}

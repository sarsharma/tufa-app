/**
 * 
 */
package gov.hhs.fda.ctp.common.comparators;

import java.util.Comparator;

import gov.hhs.fda.ctp.common.beans.CBPImporter;

/**
 * @author tgunter
 *
 */
public class CBPImporterByIdComparator implements Comparator<CBPImporter> {
    @Override
    public int compare(CBPImporter importer1, CBPImporter importer2) {
        return importer2.getCbpImporterId().compareTo(importer1.getCbpImporterId());
    }
}

/**
 * 
 */
package gov.hhs.fda.ctp.common.comparators;

import java.util.Comparator;

/**
 * The Class DescendingStringComparator.
 *
 * @author tgunter
 */
/**
 * Sort strings in descending order.
 * @author tgunter
 *
 */
public class DescendingStringComparator implements Comparator<String> {
    
    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    public int compare(String obj1, String obj2) {
        return obj2.compareTo(obj1);
    }
}
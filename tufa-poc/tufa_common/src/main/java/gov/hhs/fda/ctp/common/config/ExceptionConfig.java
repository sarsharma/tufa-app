/**
 * 
 */
package gov.hhs.fda.ctp.common.config;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tgunter
 *
 */
public class ExceptionConfig {

    Map<String, String> uriClassMap = new HashMap<String, String>();

    public ExceptionConfig(Map<String,String> exceptionMap) {
    	  uriClassMap = exceptionMap;
    }

    /**
     * Get the exception class name for a given uri.
     * @param uri
     * @return
     */
    public String getClassNameForURI(String uri) {
    	String path = uri!=null?uri.toUpperCase():uri;
        String returnVal = uriClassMap.get(path);
        if(returnVal == null) {
            returnVal = "gov.hhs.fda.ctp.common.exception.TufaException";
        }
        return returnVal;
    }
	
}

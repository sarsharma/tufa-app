package gov.hhs.fda.ctp.common.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource({ "classpath:exception/exceptionmap.properties" })
@ComponentScan({ "gov.hhs.fda.ctp" })
public class ExceptionConfigMapping {

	@Autowired
	private Environment env;

	@Bean
	public ExceptionConfig exceptionConfig() {
		Map<String,String> exceptionMap= getHeadingsFromProperties("exception.map");
		return new ExceptionConfig(exceptionMap);
	}

	private Map<String, String> getHeadingsFromProperties(String propertyName) {

		Map<String, String> HeadingsMap = new HashMap<>();
		String mapString = env.getProperty(propertyName);
		String[] keyvalues = mapString.split(",");
		for (String kv : keyvalues) {
			String[] kvpair = kv.split(":");
			HeadingsMap.put(kvpair[0].trim().toUpperCase(), kvpair[1]);
		}

		return HeadingsMap;
	}
}

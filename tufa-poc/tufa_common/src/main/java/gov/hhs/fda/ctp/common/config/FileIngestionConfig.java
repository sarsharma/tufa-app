package gov.hhs.fda.ctp.common.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

import gov.hhs.fda.ctp.common.ingestion.FileIngestorFactory;
import gov.hhs.fda.ctp.common.ingestion.FileIngestorNameMapping;
import gov.hhs.fda.ctp.common.ingestion.PermitMgmt.PermitUpdateHeadingMapping;
import gov.hhs.fda.ctp.common.ingestion.TTB.TTBHeadingMapping;
import gov.hhs.fda.ctp.common.ingestion.TTBRemovals.TTBRemovalsHeadingMapping;

@Configuration
@PropertySource({"classpath:ingestion/headings.properties" })
@ComponentScan({ "gov.hhs.fda.ctp"})
public class FileIngestionConfig {
	
	@Autowired
	private Environment env;

	
	@Bean
    public ServiceLocatorFactoryBean fileIngestorServiceLocatorFactoryBean() {
        ServiceLocatorFactoryBean bean = new ServiceLocatorFactoryBean();
        bean.setServiceLocatorInterface(FileIngestorFactory.class);
        return bean;
    }

    @Bean
    public FileIngestorFactory fileIngestorFactory() {
        return (FileIngestorFactory) fileIngestorServiceLocatorFactoryBean().getObject();
    }
    
    @Bean  
    FileIngestorNameMapping fileIngestorNamingMap(){
    	return new FileIngestorNameMapping();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigIn() {
    		return new PropertySourcesPlaceholderConfigurer();
    }
    
    
    
    @Bean(name="TTBHeadingMapping")
    public TTBHeadingMapping ttbHeadingMapping(){
    	return  new TTBHeadingMapping(getHeadingsFromProperties("ttbheadings.map"));
    }
    
    @Bean(name="TTBRemovalsHeadingMapping")
    public TTBRemovalsHeadingMapping ttbRemovalsHeadingMapping(){
    	return  new TTBRemovalsHeadingMapping(getHeadingsFromProperties("ttbvheadings.map"));
    }
    
    @Bean(name="PermitUpdateHeadingMapping")
    public PermitUpdateHeadingMapping permitUpdateHeadingMapping(){
    	return  new PermitUpdateHeadingMapping(getHeadingsFromProperties("permitupdateheadings.map"));
    }
    
    private  Map<String,String> getHeadingsFromProperties(String propertyName) {

		Map<String, String> HeadingsMap = new HashMap<>();
		String mapString = env.getProperty(propertyName);
		String[] keyvalues = mapString.split(",");
		for (String kv : keyvalues) {
			String[] kvpair = kv.split(":");
			HeadingsMap.put(kvpair[0].trim().toUpperCase(), kvpair[1]);
		}

		return HeadingsMap;
	}
}

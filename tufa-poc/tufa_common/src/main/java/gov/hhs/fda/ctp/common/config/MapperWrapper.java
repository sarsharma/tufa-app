/**
 * 
 */
package gov.hhs.fda.ctp.common.config;

import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import gov.hhs.fda.ctp.common.exception.TufaException;

/**
 * @author tgunter
 *
 */
@Component
@ComponentScan({ "gov.hhs.fda.ctp" })
public class MapperWrapper {
    private static final Logger logger = LoggerFactory.getLogger(MapperWrapper.class);	
    
    @Autowired
    DozerBeanMapperFactoryBean factory;
    
    /**
     * Dozer doesn't play well with Spring.  Spring returns a factory singleton 
     * instead of an actual mapper.  This method will return the mapper and hide 
     * the exception clutter that Dozer requires us to catch. 
     * @param factory
     * @return dozer.Mapper
     */
	
//    public static final Mapper getMapper(DozerBeanMapperFactoryBean factory) {
//        Mapper mapper = null;
//        try {
//            mapper = factory.getObject();
//        }
//        catch (Exception e) {
//        	logger.error("error", e);
//            throw new TufaException("Could not create mapper from DozerBeanMapperFactoryBean.");
//        }
//        return mapper;
//    }
    
    public Mapper getMapper() {
        Mapper mapper = null;
        try {
            mapper = this.factory.getObject();
        }
        catch (Exception e) {
        	logger.error("error", e);
            throw new TufaException("Could not create mapper from DozerBeanMapperFactoryBean.");
        }
        return mapper;
    }
}

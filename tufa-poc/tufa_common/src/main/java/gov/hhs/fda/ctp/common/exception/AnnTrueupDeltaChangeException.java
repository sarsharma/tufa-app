package gov.hhs.fda.ctp.common.exception;

public class AnnTrueupDeltaChangeException extends TufaException{
	
	private static final long serialVersionUID = 1L;
	
	private String data;
	
	public  AnnTrueupDeltaChangeException(String exMssg, String data){
		super(exMssg);
		this.setData(data);
	}
	
    public  AnnTrueupDeltaChangeException(Throwable t) {
        super(t);
    }
    
    @Override
    public String getDisplayMessage() {
            return "There are Delta Changes ";
    }

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}


}

/**
 * 
 */
package gov.hhs.fda.ctp.common.exception;

/**
 * @author tgunter
 *
 */
public class CreateCompanyException extends TufaException {
    private static final long serialVersionUID = 1L;
    

    public CreateCompanyException(){
    	
    }
    
    public CreateCompanyException(Throwable t) {
        super(t);
    }
    
    @Override
    public String getDisplayMessage() {
        if(this.getCause().getMessage().toUpperCase().contains("EIN")) {
            return "EIN already exists.";
        }
        else {
            return "Error saving company.";
        }
    }
}
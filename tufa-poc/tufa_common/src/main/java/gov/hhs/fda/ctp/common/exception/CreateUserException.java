package gov.hhs.fda.ctp.common.exception;

public class CreateUserException extends TufaException{
	private static final long serialVersionUID = 1L;

    public CreateUserException(Throwable t) {
        super(t);
    }
    
    @Override
    public String getDisplayMessage() {
        if(this.getCause().getMessage().toUpperCase().contains("USER_EMAIL")) {
            return "User already exists.";
        }
        else {
            return "Error saving user.";
        }
    }
}

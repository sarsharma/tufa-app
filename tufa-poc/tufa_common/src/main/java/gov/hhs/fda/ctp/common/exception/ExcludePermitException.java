package gov.hhs.fda.ctp.common.exception;

/**
 * @author Mohan.Bhutada
 *
 *This class will handle all kind of excpetion for Exclude permit
 */
public class ExcludePermitException extends TufaException{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public ExcludePermitException(String message) {
        super(message);
    }

}

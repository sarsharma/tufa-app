/**
 * 
 */
package gov.hhs.fda.ctp.common.exception;

/**
 * @author tgunter
 *
 */
public class FileException extends TufaException {
	private static final long serialVersionUID = 1L;


    public FileException(Throwable t) {
        super(t);
    }
    
    @Override
    public String getDisplayMessage() {
        if(this.getCause().getMessage().toUpperCase().contains("FILENAME")) {
            return "File name exceeds 200 characters.";
        }
        else {
            return "Error saving file.";
        }
    }
}

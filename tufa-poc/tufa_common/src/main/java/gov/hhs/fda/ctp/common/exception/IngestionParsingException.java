package gov.hhs.fda.ctp.common.exception;

public class IngestionParsingException extends TufaException {

	private static final long serialVersionUID = 1L;

	public IngestionParsingException(String exMssg){
		super(exMssg);
	}
	
    public IngestionParsingException(Throwable t) {
        super(t);
    }
    
    @Override
    public String getDisplayMessage() {
            return "Error parsing file.";
    }
	
}

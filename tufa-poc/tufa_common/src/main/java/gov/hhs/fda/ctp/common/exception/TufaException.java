/**
 * 
 */
package gov.hhs.fda.ctp.common.exception;

/**
 * @author tgunter
 *
 */
public class TufaException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static final String ERROR_PERFORMING = "Error performing operation";
        /**
         * Default constructor.
         */
        public TufaException()
        {
            /*
             * Default constructor.
             */
        }
 
        /**
         * Constructor with text message.
         * @param message
         */
        public TufaException(String message)
        {
            super(message);
        }

        /**
         * Constructor wrapping original exception.
         * @param cause
         */
        public TufaException(Throwable cause)
        {
            super(cause);
        }
 
        /**
         * Constructor with message and original exception.
         * @param message
         * @param cause
         */
        public TufaException(String message, Throwable cause)
        {
            super(message, cause);
        }
 
        /**
         * Constructor with message, original exception, enableSuppression flag, and writableStackTrace falg.
         * @param message
         * @param cause
         * @param enableSuppression
         * @param writableStackTrace
         */
        public TufaException(String message, Throwable cause,
                                           boolean enableSuppression, boolean writableStackTrace)
        {
            super(message, cause, enableSuppression, writableStackTrace);
        }
        
        public String getDisplayMessage() {
            return ERROR_PERFORMING;
        }

}

package gov.hhs.fda.ctp.common.ingestion.CBP;

import java.util.Map;

public class CBPHeadingMapping {

	public static enum Headings {
		ENTRY_NO, LINE_NO, HTS, IMPORTER, IMPORTER_NAME, CONSIGNEE_EIN, ENTRY_DATE, ENTRY_SUMM_DATE, ESTIMATED_TAX, QTY1, UOM1,CONSIGNEE_NAME, ENTRY_TYPE_DESC;
	}

	private Map<String, String> cbpheadings;

	public CBPHeadingMapping(Map<String, String> cbpheadings) {
		this.cbpheadings = cbpheadings;
	}

	public String getDocHeading(String excelHeading) {
		if (cbpheadings != null){
			return cbpheadings.get(excelHeading);
		}
		return null;
	}
}

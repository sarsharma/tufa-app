package gov.hhs.fda.ctp.common.ingestion.CBP;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.hhs.fda.ctp.common.beans.CBPUpload;
import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.exception.IngestionParsingException;
import gov.hhs.fda.ctp.common.util.ApplicationContextUtil;
import gov.hhs.fda.ctp.common.util.EinBuilder;

public class CBPSheetHandler implements SheetContentsHandler {
	Logger logger = LoggerFactory.getLogger(CBPSheetHandler.class);
	
	private CBPHeadingMapping cbpHeadingMapping;

	private IngestionOutput output;

	private CBPUpload currentCBPUpload;

	private Map<String, String> cpbHeaderIndexMapping = new HashMap<>();

	private boolean firstRow = false;

	private Validator validator;

	private boolean isEmptyRow = true;

	private final static Integer CBP_NO_COLS = 13;

	public CBPSheetHandler(IngestionOutput output) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		this.validator = factory.getValidator();
		this.output = output;
		this.cbpHeadingMapping = (CBPHeadingMapping) ApplicationContextUtil.getApplicationContext()
				.getBean("CBPHeadingMapping");
	}

	@Override
	public void startRow(int rowNum) {

		this.isEmptyRow = true;
		this.firstRow = (rowNum == 0) ? true : false;

		if (!firstRow)
			this.currentCBPUpload = new CBPUpload();
	}

	@Override
	public void endRow(int rowNum) {
		if(firstRow){
			if(cpbHeaderIndexMapping.size()!= CBP_NO_COLS)
				throw new IngestionParsingException("Invalid column headings in CBP file");
		}

		if (!firstRow && !isEmptyRow) {
				this.validateDocUploadBean(rowNum);
				this.output.add(currentCBPUpload);
		}

	}

	@Override
	public void cell(String cellReference, String formattedValue, XSSFComment comment) {

		if (this.firstRow) {
			String colHeading = formattedValue;
			if(cbpHeadingMapping.getDocHeading(colHeading.trim().toUpperCase())!= null)
				cpbHeaderIndexMapping.put(cellReference.replaceAll("\\d", ""), colHeading.trim().toUpperCase());
		} else {
			String columnIndex = cellReference.replaceAll("\\d", "");
			String columnHeading = cpbHeaderIndexMapping.get(columnIndex);
			String colHeadingMapped = cbpHeadingMapping.getDocHeading(columnHeading);
			if (colHeadingMapped != null)
				this.populateDocUploadModel(colHeadingMapped, formattedValue);
		}

	}

	@Override
	public void headerFooter(String text, boolean isHeader, String tagName) {

	}

	public void populateDocUploadModel(String heading, String value) {
		this.isEmptyRow = false;
		value = value.trim();
		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.ENTRY_NO.toString())) {
			this.currentCBPUpload.setEntryNo(value);
			return;
		}

		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.LINE_NO.toString())) {
			String lineNum = value;
			if (lineNum != null)
				this.currentCBPUpload.setLineNo(Long.parseLong(lineNum));
			else
				this.currentCBPUpload.setLineNo(null);
			return;
		}

		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.HTS.toString())) {
			this.currentCBPUpload.setHtsCd(value);
			return;
		}
		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.IMPORTER.toString())) {
			// truncate ein at 10 digits
			if(value != null) {
				EinBuilder importerein = new EinBuilder(value);
				value = importerein.getRawData();				
			}			
			this.currentCBPUpload.setImporterEIN(value);
			return;
		}
		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.IMPORTER_NAME.toString())) {
			this.currentCBPUpload.setImporterName(value);
			return;
		}
		
		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.CONSIGNEE_NAME.toString())) {
			this.currentCBPUpload.setConsigneeName(value);
			return;
		}

		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.CONSIGNEE_EIN.toString())) {
			// truncate ein at 10 digits
			if(value != null) {
				EinBuilder consignee = new EinBuilder(value);
				value = consignee.getRawData();				
			}
			this.currentCBPUpload.setConsigneeEIN(value);
			return;
		}

		DateTimeFormatter dtfInput = DateTimeFormat.forPattern("MM/dd/yyyy");
		DateTimeFormatter dtfOutput = DateTimeFormat.forPattern("yyyyMMdd");

		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.ENTRY_DATE.toString())) {
			if(value!=null){
				DateTime inputDt= dtfInput.parseDateTime(value);
				String outputDt = dtfOutput.print(inputDt);
				this.currentCBPUpload.setEntryDate(outputDt.toString());
			}
			return;
		}

		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.ENTRY_SUMM_DATE.toString())) {
			if(value != null){
				DateTime inputDt= dtfInput.parseDateTime(value);
				String outputDt = dtfOutput.print(inputDt);
				this.currentCBPUpload.setEntrySummDate(outputDt.toString());
			}
			return;
		}

		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.ESTIMATED_TAX.toString())) {
			String tax = value;

			if (tax != null)
				this.currentCBPUpload.setEstimatedTax(Double.parseDouble(tax.replaceAll("[$,]", "")));
			else
				this.currentCBPUpload.setEstimatedTax(null);
			return;
		}

		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.QTY1.toString())) {
			String qty = value;
			if (qty != null)
				this.currentCBPUpload.setQty1(Double.parseDouble(qty.replaceAll(",", "")));
			else
				this.currentCBPUpload.setQty1(null);
			return;
		}

		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.UOM1.toString())) {
			this.currentCBPUpload.setUom1(value);
			return;
		}
		if (heading.equalsIgnoreCase(CBPHeadingMapping.Headings.ENTRY_TYPE_DESC.toString())) {
			if(value != null) {
				String entryType = value.substring(0,2);
				this.currentCBPUpload.setEntryType(entryType);
			}
			return;
		}
	}

	/**
	 * Call validation, attach error messages if they exist.
	 * @param rowNum
	 */
	private void validateDocUploadBean(int rowNum) {

		Set<ConstraintViolation<CBPUpload>> violations = validator.validate(this.currentCBPUpload);
		StringBuilder errorMsgs = new StringBuilder(2000);
		Iterator<ConstraintViolation<CBPUpload>> iter = violations.iterator();
		if(iter.hasNext()) {
			errorMsgs.append("Row #");
			errorMsgs.append(rowNum+1);
			errorMsgs.append(": ");
			while (iter.hasNext()) {
				ConstraintViolation<CBPUpload> violation = iter.next();
				errorMsgs.append("[");
				errorMsgs.append(violation.getPropertyPath());
				errorMsgs.append(":");
				errorMsgs.append(violation.getMessage());
				errorMsgs.append("] ");
				output.incrementErrorCount();
			}
		}
		this.currentCBPUpload.setErrors(errorMsgs.toString());

	}

}

package gov.hhs.fda.ctp.common.ingestion.CBPLineItem;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import gov.hhs.fda.ctp.common.beans.CBPColumnTemplateBean;
import gov.hhs.fda.ctp.common.beans.CBPFileColumnTemplate;
import gov.hhs.fda.ctp.common.beans.CBPLineItemUpload;
import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.beans.PermitUpdateUpload;
import gov.hhs.fda.ctp.common.beans.TTBColumnTemplateBean;
import gov.hhs.fda.ctp.common.beans.TTBFileColumnFormat;
import gov.hhs.fda.ctp.common.beans.TTBFileColumnTemplate;
import gov.hhs.fda.ctp.common.exception.IngestionParsingException;
import gov.hhs.fda.ctp.common.util.ApplicationContextUtil;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.EinBuilder;
import gov.hhs.fda.ctp.common.util.PermitBuilder;

public class CBPLineItemSheetHandler implements SheetContentsHandler {
	Logger logger = LoggerFactory.getLogger(CBPLineItemSheetHandler.class);

	private CBPColumnTemplateBean cbpTemplateBean;
	
	private List<CBPFileColumnTemplate> cbpColumnTemplates;

	private CBPFileColumnTemplate cbpMatchedTemplate;

	private IngestionOutput output;

	private CBPLineItemUpload currentCBPLineItemUpload;

	private Map<String, String> cbpLineItemHeaderIndexMapping = new HashMap<>();

	private boolean firstRow = false;

	private Validator validator;

	private boolean isEmptyRow = true;


	@Autowired
	private DozerBeanMapperFactoryBean mapperFactory;

	public CBPLineItemSheetHandler(IngestionOutput output) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		this.validator = factory.getValidator();
		this.output = output;

		cbpTemplateBean = (CBPColumnTemplateBean) ApplicationContextUtil.getApplicationContext().getBean("cbpColumnTemplates");
		this.cbpColumnTemplates = cbpTemplateBean.getcbpColumnTemplates();
	}

	@Override
	public void startRow(int rowNum) {

		this.isEmptyRow = true;
		this.firstRow = (rowNum == 0) ? true : false;

		if (!firstRow)
			this.currentCBPLineItemUpload = new CBPLineItemUpload();
	}

	@Override
	public void endRow(int rowNum) {
		if (firstRow) {

			// get matched template
			this.cbpMatchedTemplate = this.getTTBMatchedTemplate(cbpLineItemHeaderIndexMapping);
		}
		if (!firstRow && !isEmptyRow) {
			
			//Filter out row if Entry Summary Date/Entry Summary Number outside fiscal Year.
			if(CTPUtil.includeCBPDates(currentCBPLineItemUpload.getEntrySummaryDate(),output.getFiscalYear())){
				this.validateDocUploadBean(rowNum);
				this.output.add(currentCBPLineItemUpload);
			}
		}

	}

	@Override
	public void cell(String cellReference, String formattedValue, XSSFComment comment) {

		if (this.firstRow) {
			String colHeading = formattedValue;
			cbpLineItemHeaderIndexMapping.put(cellReference.replaceAll("\\d", ""), colHeading.trim().toUpperCase());
		} else {
			String columnIndex = cellReference.replaceAll("\\d", "");
			String fileColHeading = cbpLineItemHeaderIndexMapping.get(columnIndex);
			this.populateDocUploadModel(fileColHeading, formattedValue);
		}

	}

	private CBPFileColumnTemplate getTTBMatchedTemplate(Map<String, String> permitUpdateHeaderIndexMapping) {

		Set<String> colHeadings = new HashSet<>();
		for (String key : permitUpdateHeaderIndexMapping.keySet())
			colHeadings.add(permitUpdateHeaderIndexMapping.get(key));

		// Ascertain template to be used
		CBPFileColumnTemplate mtchTemplate = null;
		List<CBPFileColumnTemplate> templates = this.cbpColumnTemplates;
		for (CBPFileColumnTemplate template : templates) {
			// Get template headings
			if (colHeadings.containsAll(template.columnHeadings())) {
				mtchTemplate = template;
				break;
			}
		}

		// Throw error if no template match
		if (mtchTemplate == null)
			throw new IngestionParsingException(" invalid column headings in Permit List Response file ");

		return mtchTemplate;
	}

	@Override
	public void headerFooter(String text, boolean isHeader, String tagName) {

	}

	public void populateDocUploadModel(String heading, String value) {

		this.isEmptyRow = false;
		value = value.trim();
		DateTimeFormatter dtfInput = DateTimeFormat.forPattern("dd-MMM-yy");
		DateTimeFormatter dtfOutput = DateTimeFormat.forPattern("yyyyMMdd");

		//Entry Summary Number
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getEntrySummaryNumber())) {
			this.currentCBPLineItemUpload.setEntrySummaryNumber(value);
			return;
		}
		//Entry Summary Date
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getEntrySummaryDate())) {
			if(value!=null){
				DateTime inputDt= dtfInput.parseDateTime(value);
				String outputDt = dtfOutput.print(inputDt);
				this.currentCBPLineItemUpload.setEntrySummaryDate(outputDt.toString());
			}
			return;
		}
		//Entry Date
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getEntryDate())) {
			if(value!=null){
				DateTime inputDt= dtfInput.parseDateTime(value);
				String outputDt = dtfOutput.print(inputDt);
				this.currentCBPLineItemUpload.setEntryDate(outputDt.toString());
			}
			return;
			
		}
		//HTS Number
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getHtsNumber())) {
			if(value!=null){
				this.currentCBPLineItemUpload.setHtsNumber(Long.parseLong(value));
			}
			return;
		}
		//Line Tariff Qty 1
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getLineTariffqty1())) {
			if(value!=null){
				this.currentCBPLineItemUpload.setLineTarrifQty1(Double.parseDouble(value.replaceAll("[$,]", "")));
			}
			return;
		}
		//Importer Number
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getImporterNumber())) {
			if(value != null) {
				EinBuilder einFomatted = new EinBuilder(value);
				value = einFomatted.getRawData();				
			}	
			this.currentCBPLineItemUpload.setImporterNumber(value);
			return;
		}
		//Importer Name
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getImporterName())) {
			this.currentCBPLineItemUpload.setImporterName(value);
			return;
		}
		//Ultimate Consignee Number
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getUltimateConsigneeNumber())) {
			if(value != null) {
				EinBuilder einFomatted = new EinBuilder(value);
				value = einFomatted.getRawData();				
			}	
			this.currentCBPLineItemUpload.setUltimateConsigneeNumber(value);
			return;
		}
		//Ultimate Consignee Name
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getUltimateConsigneeName())) {
			currentCBPLineItemUpload.setUltimateConsigneeName(value);
			return;
		}
		//HTS Description
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getHtsDesciption())) {
			this.currentCBPLineItemUpload.setHtsDescription(value);
			return;
		}

		//Line Tariff UOM1
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getLineTariffuom1())) {
		
			this.currentCBPLineItemUpload.setLineTarrifUOM1(value);
			return;
		}

		//Line Number
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getLineNumber())) {
			this.currentCBPLineItemUpload.setLineNumber(Long.parseLong(value));
			return;
		}
		//Entry Type Code
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getEntryTypeCode())) {
			this.currentCBPLineItemUpload.setEntryTypeCode(value);
			return;
		}
		//Line Tariff Goods Value Amount
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getLineTariffGoodsValueAmount())) {
			if (value != null){
				this.currentCBPLineItemUpload.setLineTariffGoodsValueAmount(Double.parseDouble(value.replaceAll("[$,]", "")));
			}else{
				this.currentCBPLineItemUpload.setLineTariffGoodsValueAmount(null);
			}
			return;
		}
		//IR TAX AMOUNT 
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getirTaxAmount())) {
			if (value != null){
				this.currentCBPLineItemUpload.setIrTaxAmount(Double.parseDouble(value.replaceAll("[$,]", "")));
			}else{
				this.currentCBPLineItemUpload.setIrTaxAmount(null);
			}
			return;
		}
		//IMPORT DATE 
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getImportDate())) {
			if(value!=null){
				DateTime inputDt= dtfInput.parseDateTime(value);
				String outputDt = dtfOutput.print(inputDt);
				this.currentCBPLineItemUpload.setImportDate(outputDt.toString());
			}
			return;
		}
		
		}

	/**
	 * Call validation, attach error messages if they exist.
	 * 
	 * @param rowNum
	 */
	private void validateDocUploadBean(int rowNum) {

		Set<ConstraintViolation<CBPLineItemUpload>> violations = validator.validate(this.currentCBPLineItemUpload);
		StringBuilder errorMsgs = new StringBuilder(2000);
		errorMsgs.append("Row #");
		errorMsgs.append(":");
		errorMsgs.append(rowNum + 1);
		errorMsgs.append(";");

		Iterator<ConstraintViolation<CBPLineItemUpload>> iter = violations.iterator();
		if (iter.hasNext()) {
			errorMsgs.append("VALIDATION_ERRORS");
			errorMsgs.append(":");
			while (iter.hasNext()) {
				ConstraintViolation<CBPLineItemUpload> violation = iter.next();
				errorMsgs.append("[");
				errorMsgs.append(violation.getPropertyPath());
				errorMsgs.append(":");
				errorMsgs.append(violation.getMessage());
				errorMsgs.append("] ");
				output.incrementErrorCount();
			}
			this.currentCBPLineItemUpload.setErrors(errorMsgs.toString());
		}
		

	}

	public DozerBeanMapperFactoryBean getMapperFactory() {
		return mapperFactory;
	}

	public void setMapperFactory(DozerBeanMapperFactoryBean mapperFactory) {
		this.mapperFactory = mapperFactory;
	}

}

package gov.hhs.fda.ctp.common.ingestion.CBPLineItem;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.ingestion.ExcelFileIngestor;

@Component("CBPLineFileIngestor")
public class ExcelCBPLineItemFileIngestor extends ExcelFileIngestor {

	//Need to check on the count
	private int minColumns = 16;

	public void parseCBPLineDetailsFileSAX(String fileName, IngestionOutput output)
			throws InvalidFormatException, IOException, OpenXML4JException, SAXException {
		
		File xlsxFile = new File(fileName);
		OPCPackage pkg  = OPCPackage.open(xlsxFile.getPath(),PackageAccess.READ);
		CBPLineItemExcel2DocUpload excel2DocUpload = new CBPLineItemExcel2DocUpload(pkg, output, minColumns);
		excel2DocUpload.process();
		pkg.revert();

	}
	
	@Override
	public void parseFileSAX(String fileName, IngestionOutput output)
			throws InvalidFormatException, IOException, OpenXML4JException, SAXException {
		this.parseCBPLineDetailsFileSAX(fileName, output);
	}

	

}

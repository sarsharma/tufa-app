package gov.hhs.fda.ctp.common.ingestion.CBPSummary;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import gov.hhs.fda.ctp.common.beans.CBPColumnTemplateBean;
import gov.hhs.fda.ctp.common.beans.CBPFileColumnTemplate;
import gov.hhs.fda.ctp.common.beans.CBPSummaryUpload;
import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.exception.IngestionParsingException;
import gov.hhs.fda.ctp.common.util.ApplicationContextUtil;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.EinBuilder;

public class CBPSummarySheetHandler implements SheetContentsHandler {
	Logger logger = LoggerFactory.getLogger(CBPSummarySheetHandler.class);

	private CBPColumnTemplateBean cbpTemplateBean;
	
	private List<CBPFileColumnTemplate> cbpColumnTemplates;

	private CBPFileColumnTemplate cbpMatchedTemplate;

	private IngestionOutput output;

	private CBPSummaryUpload cbpsummfile;

	private Map<String, String> cbpUpdateHeaderIndexMapping = new HashMap<>();

	private boolean firstRow = false;

	private Validator validator;

	private boolean isEmptyRow = true;


	@Autowired
	private DozerBeanMapperFactoryBean mapperFactory;

	public CBPSummarySheetHandler(IngestionOutput output) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		this.validator = factory.getValidator();
		this.output = output;

		cbpTemplateBean = (CBPColumnTemplateBean) ApplicationContextUtil.getApplicationContext()
				.getBean("cbpColumnTemplates");
		this.cbpColumnTemplates = cbpTemplateBean.getcbpColumnTemplates();
	}

	@Override
	public void startRow(int rowNum) {

		this.isEmptyRow = true;
		this.firstRow = (rowNum == 0) ? true : false;

		if (!firstRow)
			this.cbpsummfile = new CBPSummaryUpload();
	}

	@Override
	public void endRow(int rowNum) {
		if (firstRow) {

			// get matched template
			this.cbpMatchedTemplate = this.getCBPMatchedTemplate(cbpUpdateHeaderIndexMapping);
		}

		if (!firstRow && !isEmptyRow) {
			
			//Filter out row if Entry Summary Date/Entry Summary Number outside fiscal Year.
			if(CTPUtil.includeCBPDates(cbpsummfile.getentrySummaryDate(),output.getFiscalYear())){
				this.validateDocUploadBean(rowNum);
				this.output.add(cbpsummfile);
			}
		}

	}

	@Override
	public void cell(String cellReference, String formattedValue, XSSFComment comment) {

		if (this.firstRow) {
			String colHeading = formattedValue;
			cbpUpdateHeaderIndexMapping.put(cellReference.replaceAll("\\d", ""), colHeading.trim().toUpperCase());
		} else {
			String columnIndex = cellReference.replaceAll("\\d", "");
			String fileColHeading = cbpUpdateHeaderIndexMapping.get(columnIndex);
			this.populateDocUploadModel(fileColHeading, formattedValue);
		}

	}

	private CBPFileColumnTemplate getCBPMatchedTemplate(Map<String, String> cbpUpdateHeaderIndexMapping) {

		Set<String> colHeadings = new HashSet<>();
		for (String key : cbpUpdateHeaderIndexMapping.keySet())
			colHeadings.add(cbpUpdateHeaderIndexMapping.get(key));

		// Ascertain template to be used
		CBPFileColumnTemplate mtchTemplate = null;
		List<CBPFileColumnTemplate> templates = this.cbpColumnTemplates;
		for (CBPFileColumnTemplate template : templates) {
			// Get template headings
			if (colHeadings.containsAll(template.columnHeadings())) {
				mtchTemplate = template;
				break;
			}
		}

		// Throw error if no template match
		if (mtchTemplate == null)
			throw new IngestionParsingException("invalid column headings in CBP Summary Response file ");

		return mtchTemplate;
	}

	@Override
	public void headerFooter(String text, boolean isHeader, String tagName) {

	}

	public void populateDocUploadModel(String heading, String value) {

		this.isEmptyRow = false;
		value = value.trim();

		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getEntrySummaryNumber())) {
			this.cbpsummfile.setentrySummaryNumber(value);
			return;
		}

		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getImporterName())) {
			this.cbpsummfile.setImporterName(value);
			return;
		}

		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getImporterNumber())) {
			if(value != null) {
				EinBuilder einFomatted = new EinBuilder(value);
				value = einFomatted.getRawData();				
			}	
			this.cbpsummfile.setImporterNumber(value);
			return;
		}

		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getUltimateConsigneeName())) {
			this.cbpsummfile.setultimateConsigneeName(value);
			return;
		}

		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getUltimateConsigneeNumber())) {
			if(value != null) {
				EinBuilder einFomatted = new EinBuilder(value);
				value = einFomatted.getRawData();				
			}	
			this.cbpsummfile.setultimateConsigneeNumber(value);
			return;
		}

		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getEntryTypeCodeDesciption())) {
			this.cbpsummfile.setentryTypeCodeDescription(value);
			return;
		}

		
		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getEstimatedtax())) {
		
			String tax = value;

			if (tax != null)
				this.cbpsummfile.setEstimatedTax(Double.parseDouble(tax.replaceAll("[$,]", "")));
			else
				this.cbpsummfile.setEstimatedTax(null);
			return;
			
		}

		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getTotalAscertainedTax())) {
							
			String tax = value;
			if (tax != null)
				this.cbpsummfile.setTotalAscertainedtax(Double.parseDouble(tax.replaceAll("[$,]", "")));
			else
				this.cbpsummfile.setTotalAscertainedtax(null);
			return;
			
		}
		
		DateTimeFormatter dtfInput = DateTimeFormat.forPattern("MM/dd/yyyy");
		DateTimeFormatter dtfOutput = DateTimeFormat.forPattern("yyyyMMdd");

		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getEntrySummaryDate())) {
		
			if(value!=null){
				DateTime inputDt= dtfInput.parseDateTime(value);
				String outputDt = dtfOutput.print(inputDt);
				this.cbpsummfile.setentrySummaryDate(outputDt.toString());
			}
			return;
		}

		if (heading.equalsIgnoreCase(cbpMatchedTemplate.getEntryDate())) {
			
			
			if(value!=null){
				DateTime inputDt= dtfInput.parseDateTime(value);
				String outputDt = dtfOutput.print(inputDt);
				this.cbpsummfile.setEntryDate(outputDt.toString());
			}
			return;
			
		}

		
	}

	/**
	 * Call validation, attach error messages if they exist.
	 * 
	 * @param rowNum
	 */
	private void validateDocUploadBean(int rowNum) {
		
		Set<ConstraintViolation<CBPSummaryUpload>> violations = validator.validate(this.cbpsummfile);
		StringBuilder errorMsgs = new StringBuilder(2000);
		errorMsgs.append("Row #");
		errorMsgs.append(":");
		errorMsgs.append(rowNum + 1);
		errorMsgs.append(";");

		Iterator<ConstraintViolation<CBPSummaryUpload>> iter = violations.iterator();
		if (iter.hasNext()) {
			errorMsgs.append("VALIDATION_ERRORS");
			errorMsgs.append(":");
			while (iter.hasNext()) {
				ConstraintViolation<CBPSummaryUpload> violation = iter.next();
				errorMsgs.append("[");
				errorMsgs.append(violation.getPropertyPath());
				errorMsgs.append(":");
				errorMsgs.append(violation.getMessage());
				errorMsgs.append("] ");
				output.incrementErrorCount();
			}
			this.cbpsummfile.setError(errorMsgs.toString());
		}
		

	}

	public DozerBeanMapperFactoryBean getMapperFactory() {
		return mapperFactory;
	}

	public void setMapperFactory(DozerBeanMapperFactoryBean mapperFactory) {
		this.mapperFactory = mapperFactory;
	}

}
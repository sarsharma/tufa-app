package gov.hhs.fda.ctp.common.ingestion.CBPSummary;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.ingestion.ExcelFileIngestor;

@Component("CBPSummFileIngestor")
public class ExcelCBPSummFileIngestor extends ExcelFileIngestor {

	private int minColumns = 10;

	public void parseCBPFileSAX(String fileName,IngestionOutput output) throws InvalidFormatException, IOException, OpenXML4JException, SAXException{

		File xlsxFile = new File(fileName);
		OPCPackage pkg  = OPCPackage.open(xlsxFile.getPath(),PackageAccess.READ);
		CBPSummExcel2DocUpload  cbpExcelToDocUpload = new CBPSummExcel2DocUpload(pkg,output ,minColumns);
		cbpExcelToDocUpload.process();
		pkg.revert();
	}
	
	@Override
	public void parseFileSAX(String fileName, IngestionOutput output)
			throws InvalidFormatException, IOException, OpenXML4JException, SAXException {
		this.parseCBPFileSAX(fileName, output);
	}


}

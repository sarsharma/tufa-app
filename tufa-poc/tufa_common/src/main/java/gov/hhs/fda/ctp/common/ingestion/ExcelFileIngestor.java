package gov.hhs.fda.ctp.common.ingestion;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.xml.sax.SAXException;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;

public abstract class ExcelFileIngestor implements FileIngestor {

	@Override
	public void parseTTBFileSAX(String fileName, IngestionOutput output)
			throws InvalidFormatException, IOException, OpenXML4JException, SAXException {

	}

	@Override
	public void parseCBPFileSAX(String fileName, IngestionOutput output)
			throws InvalidFormatException, IOException, OpenXML4JException, SAXException {

	}

	@Override
	public void parseTTBRemovalsFileSAX(String fileName, IngestionOutput output)
			throws InvalidFormatException, IOException, OpenXML4JException, SAXException {
	}

	public void parsePermitListFileSAX(String fileName, IngestionOutput output)
			throws InvalidFormatException, IOException, OpenXML4JException, SAXException {
	}

	@Override
	public void parseCBPLineDetailsFileSAX(String fileName, IngestionOutput output)
			throws InvalidFormatException, IOException, OpenXML4JException, SAXException {

	}
	
	@Override
	public void parseFileSAX(String fileName, IngestionOutput output)
			throws InvalidFormatException, IOException, OpenXML4JException, SAXException {
		// TODO Auto-generated method stub
		
	}

}

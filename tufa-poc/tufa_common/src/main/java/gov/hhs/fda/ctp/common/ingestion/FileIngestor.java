package gov.hhs.fda.ctp.common.ingestion;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.xml.sax.SAXException;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;

public interface FileIngestor {

		public void parseCBPFileSAX(String fileName,IngestionOutput output) throws InvalidFormatException, IOException, OpenXML4JException, SAXException;
		public void parseTTBFileSAX(String fileName,IngestionOutput output) throws InvalidFormatException, IOException, OpenXML4JException, SAXException;
		public void parseTTBRemovalsFileSAX(String fileName, IngestionOutput output)throws InvalidFormatException, IOException, OpenXML4JException, SAXException;
		public void parsePermitListFileSAX(String fileName,IngestionOutput output) throws InvalidFormatException, IOException, OpenXML4JException, SAXException;
		public void parseCBPLineDetailsFileSAX(String fileName, IngestionOutput output) throws InvalidFormatException, IOException, OpenXML4JException, SAXException;

		public void parseFileSAX(String fileName, IngestionOutput output) throws InvalidFormatException, IOException, OpenXML4JException, SAXException;
}

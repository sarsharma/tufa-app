package gov.hhs.fda.ctp.common.ingestion;

public interface FileIngestorFactory {
	
	 public FileIngestor getFileIngestor(String ingestorName);

}

package gov.hhs.fda.ctp.common.ingestion;

import java.util.HashMap;
import java.util.Map;

import gov.hhs.fda.ctp.common.util.Constants;

public class FileIngestorNameMapping {

	private Map<String, String> ingestors = new HashMap<>();

	public FileIngestorNameMapping() {
		ingestors.put(Constants.CBP, "CBPFileIngestor");
		ingestors.put(Constants.CBPSumm, "CBPSummFileIngestor");
		ingestors.put(Constants.CBPLine, "CBPLineFileIngestor");
		ingestors.put(Constants.TTB, "TTBFileIngestor");
		ingestors.put(Constants.TTB_VOLUME, "TTBRemovalsFileIngestor");
		ingestors.put(Constants.PERMIT_UPDATE, "PermitUpdateFileIngestor");
	}

	public String getFileIngestorName(String fileType) {
		return this.ingestors.get(fileType);
	}

}

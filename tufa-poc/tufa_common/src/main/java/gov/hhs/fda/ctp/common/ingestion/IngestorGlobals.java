/**
 * 
 */
package gov.hhs.fda.ctp.common.ingestion;

import org.springframework.stereotype.Component;

/**
 * @author tgunter
 *
 */
@Component("IngestorGlobals")
public class IngestorGlobals {
	private long errorCount;
	private long fiscalYear;
	public void initialize() {
		errorCount = 0L;
		fiscalYear = 0L;
	}
	public void incrementErrorCount() {
		errorCount++;
	}
	/**
	 * @return the errorCount
	 */
	public long getErrorCount() {
		return errorCount;
	}
	/**
	 * @param errorCount the errorCount to set
	 */
	public void setErrorCount(long errorCount) {
		this.errorCount = errorCount;
	}
	/**
	 * @return the fiscalYear
	 */
	public long getFiscalYear() {
		return fiscalYear;
	}
	/**
	 * @param fiscalYear the fiscalYear to set
	 */
	public void setFiscalYear(long fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
}

package gov.hhs.fda.ctp.common.ingestion.PermitMgmt;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.ingestion.ExcelFileIngestor;

@Component("PermitUpdateFileIngestor")
public class ExcelPermitUpdateFileIngestor extends ExcelFileIngestor {

	private int minColumns = 4;

	@Override
	public void parsePermitListFileSAX(String fileName, IngestionOutput output)
			throws InvalidFormatException, IOException, OpenXML4JException, SAXException {
		
		File xlsxFile = new File(fileName);
		OPCPackage pkg  = OPCPackage.open(xlsxFile.getPath(),PackageAccess.READ);
		PermitUpdateExcel2DocUpload  permitUpdateExcelToDocUpload = new PermitUpdateExcel2DocUpload(pkg, output, minColumns);
		permitUpdateExcelToDocUpload.process();
		pkg.revert();

	}

}

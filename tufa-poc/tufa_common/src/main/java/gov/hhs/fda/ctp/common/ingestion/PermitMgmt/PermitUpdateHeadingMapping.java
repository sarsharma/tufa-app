package gov.hhs.fda.ctp.common.ingestion.PermitMgmt;

import java.util.Map;

public class PermitUpdateHeadingMapping {

	
	public static enum Headings {
		EIN,PERMIT_ID,BUSINESS_NAME,MAILING_STREET,MAILING_CITY,MAILING_STATE,
		MAILING_POSTAL_CD,MAILING_TEXT,PREMISE_PHONE,EMAIL,ISSUE_DATE,CLOSED_DATE,PERMIT_TYPE,PERMIT_STATUS;
	}

	private Map<String, String> permitUpdateHeadings;

	public  PermitUpdateHeadingMapping(Map<String, String> permitUpdateHeadings) {
		this.permitUpdateHeadings = permitUpdateHeadings;
	}

	public String getDocHeading(String excelHeading) {
		if (permitUpdateHeadings != null){
			return permitUpdateHeadings.get(excelHeading);
		}
		return null;
	}
}

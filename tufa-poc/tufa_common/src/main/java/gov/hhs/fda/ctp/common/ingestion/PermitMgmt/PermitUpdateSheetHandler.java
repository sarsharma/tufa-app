package gov.hhs.fda.ctp.common.ingestion.PermitMgmt;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.beans.PermitUpdateUpload;
import gov.hhs.fda.ctp.common.beans.TTBColumnTemplateBean;
import gov.hhs.fda.ctp.common.beans.TTBFileColumnFormat;
import gov.hhs.fda.ctp.common.beans.TTBFileColumnTemplate;
import gov.hhs.fda.ctp.common.exception.IngestionParsingException;
import gov.hhs.fda.ctp.common.util.ApplicationContextUtil;
import gov.hhs.fda.ctp.common.util.PermitBuilder;

public class PermitUpdateSheetHandler implements SheetContentsHandler {
	Logger logger = LoggerFactory.getLogger(PermitUpdateSheetHandler.class);

	private TTBColumnTemplateBean ttbTemplateBean;
	
	private List<TTBFileColumnTemplate> ttbColumnTemplates;

	private TTBFileColumnTemplate ttbMatchedTemplate;

	private IngestionOutput output;

	private PermitUpdateUpload currentPermitUpdateUpload;

	private Map<String, String> permitUpdateHeaderIndexMapping = new HashMap<>();

	private boolean firstRow = false;

	private Validator validator;

	private boolean isEmptyRow = true;


	@Autowired
	private DozerBeanMapperFactoryBean mapperFactory;

	public PermitUpdateSheetHandler(IngestionOutput output) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		this.validator = factory.getValidator();
		this.output = output;

		ttbTemplateBean = (TTBColumnTemplateBean) ApplicationContextUtil.getApplicationContext()
				.getBean("ttbColumnTemplates");
		this.ttbColumnTemplates = ttbTemplateBean.getTtbColumnTemplates();
	}

	@Override
	public void startRow(int rowNum) {

		this.isEmptyRow = true;
		this.firstRow = (rowNum == 0) ? true : false;

		if (!firstRow)
			this.currentPermitUpdateUpload = new PermitUpdateUpload();
	}

	@Override
	public void endRow(int rowNum) {
		if (firstRow) {

			// get matched template
			this.ttbMatchedTemplate = this.getTTBMatchedTemplate(permitUpdateHeaderIndexMapping);
		}

		if (!firstRow && !isEmptyRow) {
			this.validateDocUploadBean(rowNum);
			this.output.add(currentPermitUpdateUpload);
		}

	}

	@Override
	public void cell(String cellReference, String formattedValue, XSSFComment comment) {

		if (this.firstRow) {
			String colHeading = formattedValue;
			permitUpdateHeaderIndexMapping.put(cellReference.replaceAll("\\d", ""), colHeading.trim().toUpperCase());
		} else {
			String columnIndex = cellReference.replaceAll("\\d", "");
			String fileColHeading = permitUpdateHeaderIndexMapping.get(columnIndex);
			this.populateDocUploadModel(fileColHeading, formattedValue);
		}

	}

	private TTBFileColumnTemplate getTTBMatchedTemplate(Map<String, String> permitUpdateHeaderIndexMapping) {

		Set<String> colHeadings = new HashSet<>();
		for (String key : permitUpdateHeaderIndexMapping.keySet())
			colHeadings.add(permitUpdateHeaderIndexMapping.get(key));

		// Ascertain template to be used
		TTBFileColumnTemplate mtchTemplate = null;
		List<TTBFileColumnTemplate> templates = this.ttbColumnTemplates;
		for (TTBFileColumnTemplate template : templates) {
			// Get template headings
			if (colHeadings.containsAll(template.columnHeadings())) {
				mtchTemplate = template;
				break;
			}
		}

		// Throw error if no template match
		if (mtchTemplate == null)
			throw new IngestionParsingException(" invalid column headings in Permit List Response file ");

		return mtchTemplate;
	}

	@Override
	public void headerFooter(String text, boolean isHeader, String tagName) {

	}

	public void populateDocUploadModel(String heading, String value) {

		this.isEmptyRow = false;
		value = value.trim();

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getEin())) {
			this.currentPermitUpdateUpload.setEinNum(value);
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getBusinessName())) {
			this.currentPermitUpdateUpload.setBusinessName(value);
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getPermitId())) {
			PermitBuilder permitBuilder = new PermitBuilder(value);
			this.currentPermitUpdateUpload.setPermitNum(permitBuilder.unformatPermit());
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getMailingStreet())) {
			if(StringUtils.isNotBlank(value)){
				value =  value.toUpperCase();
			}
			this.currentPermitUpdateUpload.setMailingStreet(value);
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getMailingCity())) {
			if(StringUtils.isNotBlank(value)){
				value =  value.toUpperCase();
			}
			this.currentPermitUpdateUpload.setMailingCity(value);
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getMailingPostalCd())) {
			this.currentPermitUpdateUpload.setMailingPostalCd(value);
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getMailingState())) {
			if(StringUtils.isNotBlank(value)){
				value =  value.toUpperCase();
			}
			this.currentPermitUpdateUpload.setMailingState(value);
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getMailingText())) {
			if(StringUtils.isNotBlank(value)){
				value =  value.toUpperCase();
			}
			this.currentPermitUpdateUpload.setMailingText(value);
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getEmail())) {
			if(StringUtils.isNotBlank(value)){
				value =  value.toUpperCase();
			}
			this.currentPermitUpdateUpload.setEmail(value);
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getPremisePhone())) {
			this.currentPermitUpdateUpload.setPremisePhone(value);
			return;
		}
		
		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getPremisePhone2())) {
			this.currentPermitUpdateUpload.setPremisePhone2(value);
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getIssueDate())) {
			TTBFileColumnFormat formats = ttbMatchedTemplate.getColFormat();
			String dtFmt = formats.getDateFormat1();
			DateTimeFormatter dtf = DateTimeFormat.forPattern(dtFmt);
			String dtValue = value.substring(0, dtFmt.length());
			DateTime issueDt = dtf.parseDateTime(dtValue);
			DateTimeFormatter dtf1 = DateTimeFormat.forPattern("MM/dd/yyyy");
			this.currentPermitUpdateUpload.setIssueDt(dtf1.print(issueDt));
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getClosedDate())) {
			TTBFileColumnFormat formats = ttbMatchedTemplate.getColFormat();
			String dtFmt = formats.getDateFormat1();
			DateTimeFormatter dtf = DateTimeFormat.forPattern(dtFmt);
			String dtValue = value.substring(0, dtFmt.length());
			DateTime closeDt = dtf.parseDateTime(dtValue);
			DateTimeFormatter dtf1 = DateTimeFormat.forPattern("MM/dd/yyyy");
			this.currentPermitUpdateUpload.setCloseDt(dtf1.print(closeDt));
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getPermitType())) {
			String primValue  = this.ttbTemplateBean.getPrimaryValueForCol("PERMIT_TYPE", value);
			this.currentPermitUpdateUpload.setPermitType(primValue!=null?primValue:value);
			return;
		}

		if (heading.equalsIgnoreCase(ttbMatchedTemplate.getPermitStatus())) {
			String primValue  = this.ttbTemplateBean.getPrimaryValueForCol("PERMIT_STATUS", value);
			this.currentPermitUpdateUpload.setPermitStatus(primValue!=null?primValue:value);
		}
	}

	/**
	 * Call validation, attach error messages if they exist.
	 * 
	 * @param rowNum
	 */
	private void validateDocUploadBean(int rowNum) {

		Set<ConstraintViolation<PermitUpdateUpload>> violations = validator.validate(this.currentPermitUpdateUpload);
		StringBuilder errorMsgs = new StringBuilder(2000);
		errorMsgs.append("Row #");
		errorMsgs.append(":");
		errorMsgs.append(rowNum + 1);
		errorMsgs.append(";");

		Iterator<ConstraintViolation<PermitUpdateUpload>> iter = violations.iterator();
		if (iter.hasNext()) {
			errorMsgs.append("VALIDATION_ERRORS");
			errorMsgs.append(":");
			while (iter.hasNext()) {
				ConstraintViolation<PermitUpdateUpload> violation = iter.next();
				errorMsgs.append("[");
				errorMsgs.append(violation.getPropertyPath());
				errorMsgs.append(":");
				errorMsgs.append(violation.getMessage());
				errorMsgs.append("] ");
				output.incrementErrorCount();
			}
		}
		this.currentPermitUpdateUpload.setErrors(errorMsgs.toString());

	}

	public DozerBeanMapperFactoryBean getMapperFactory() {
		return mapperFactory;
	}

	public void setMapperFactory(DozerBeanMapperFactoryBean mapperFactory) {
		this.mapperFactory = mapperFactory;
	}

}

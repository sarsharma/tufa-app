package gov.hhs.fda.ctp.common.ingestion.TTB;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.util.SAXHelper;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.CommentsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;

public class TTBExcel2DocUpload {

	OPCPackage xlsxPackage;
	IngestionOutput output;
	Integer minColumns;
	TTBSheetHandler sheetHandler;

	public TTBExcel2DocUpload(OPCPackage pkg, IngestionOutput output, int minColumns) {
		this.xlsxPackage = pkg;
		this.output = output;
		this.minColumns = minColumns;
		sheetHandler = new TTBSheetHandler(output);
	}

	public void processSheet(StylesTable styles, ReadOnlySharedStringsTable strings, gov.hhs.fda.ctp.common.ingestion.TTB.TufaXSSFSheetXMLHandler.SheetContentsHandler sheetHandler,
			InputStream sheetInputStream) throws IOException, SAXException {
		DataFormatter formatter = new DataFormatter();
		CommentsTable comments = new CommentsTable();
		InputSource sheetSource = new InputSource(sheetInputStream);
		try {
			XMLReader sheetParser = SAXHelper.newXMLReader();
			ContentHandler handler = new TufaXSSFSheetXMLHandler(styles, comments, strings, sheetHandler, formatter, false);
			sheetParser.setContentHandler(handler);
			sheetParser.setErrorHandler(new TTBErrorHandler());
			sheetParser.parse(sheetSource);
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("SAX parser appears to be broken - " + e.getMessage());
		}
	}

	 public void process() throws IOException, OpenXML4JException, SAXException {
	        ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(this.xlsxPackage);
	        XSSFReader xssfReader = new XSSFReader(this.xlsxPackage);
	        StylesTable styles = xssfReader.getStylesTable();
	        XSSFReader.SheetIterator iter = (XSSFReader.SheetIterator) xssfReader.getSheetsData();
	        while (iter.hasNext()) {
	        	InputStream stream = iter.next();
	        	//TTBSheetHandler sheetHandler = new TTBSheetHandler(output,errorCount);
	            processSheet(styles, strings,sheetHandler, stream);
	            stream.close();
	            //Only first sheet has to be read, for now
	            break;
	        }
	 }

}

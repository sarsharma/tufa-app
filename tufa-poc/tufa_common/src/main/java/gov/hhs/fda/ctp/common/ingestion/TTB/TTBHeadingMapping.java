package gov.hhs.fda.ctp.common.ingestion.TTB;

import java.util.Map;

public class TTBHeadingMapping {

	public static enum Headings {
		EIN_NUM, COMPANY_NM, PERMIT_NUM, TPBD, CHEWSNUFF_TAXES, CIGARETTE_TAXES, CIGAR_TAXES, PIPERYO_TAXES, INC_ADJ, DEC_ADJ;
	}

	private Map<String, String> ttbheadings;

	public TTBHeadingMapping(Map<String, String> ttbheadings) {
		this.ttbheadings = ttbheadings;
	}

	public String getDocHeading(String excelHeading) {
		if (ttbheadings != null)
			return ttbheadings.get(excelHeading);
		return null;
	}
}

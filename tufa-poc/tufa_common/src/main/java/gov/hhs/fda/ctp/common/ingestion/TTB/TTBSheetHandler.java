package gov.hhs.fda.ctp.common.ingestion.TTB;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.poi.xssf.usermodel.XSSFComment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.beans.TTBUpload;
import gov.hhs.fda.ctp.common.exception.IngestionParsingException;
import gov.hhs.fda.ctp.common.util.ApplicationContextUtil;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.PermitBuilder;

public class TTBSheetHandler implements gov.hhs.fda.ctp.common.ingestion.TTB.TufaXSSFSheetXMLHandler.SheetContentsHandler {
	Logger logger = LoggerFactory.getLogger(TTBSheetHandler.class);
	
	private TTBHeadingMapping ttbHeadingMapping;

	private IngestionOutput output;

	private String millenniumCentury;

	private TTBUpload currentTTBUpload;

	private Map<String, String> ttbHeaderIndexMapping = new HashMap<>();

	private boolean firstRow = false;

	private Validator validator;

	private boolean isEmptyRow = true;

	private final static Integer TTB_COLS = 10;

	public TTBSheetHandler(IngestionOutput output) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		this.validator = factory.getValidator();
		this.output = output;
		String year = Long.toString(output.getFiscalYear());
		millenniumCentury = year.substring(0, 2);
		this.ttbHeadingMapping = (TTBHeadingMapping) ApplicationContextUtil.getApplicationContext()
				.getBean("TTBHeadingMapping");
	}

	@Override
	public void startRow(int rowNum) {
		isEmptyRow = true;
		this.firstRow = (rowNum == 0) ? true : false;
		if (!firstRow)
			this.currentTTBUpload = new TTBUpload();
	}

	@Override
	public void endRow(int rowNum) {

		if (firstRow) {
			if (ttbHeaderIndexMapping.size() != TTB_COLS)
				throw new IngestionParsingException("Invalid column headings in TTB file");
		}

		if (!firstRow && !isEmptyRow) {
			this.validateDocUploadBean(rowNum);
			this.output.add(currentTTBUpload);
		}
	}

	@Override
	public void cell(String cellReference, String formattedValue, XSSFComment comment) {

		if (this.firstRow) {
			String colHeading = formattedValue;
			if (ttbHeadingMapping.getDocHeading(colHeading.trim().toUpperCase()) != null)
				ttbHeaderIndexMapping.put(cellReference.replaceAll("\\d", ""), formattedValue.trim().toUpperCase());

		} else {
			String columnIndex = cellReference.replaceAll("\\d", "");
			String columnHeading = ttbHeaderIndexMapping.get(columnIndex);
			String colHeadingMapped = ttbHeadingMapping.getDocHeading(columnHeading);
			if (colHeadingMapped != null)
				this.populateDocUploadModel(colHeadingMapped, formattedValue);
		}

	}

	@Override
	public void headerFooter(String text, boolean isHeader, String tagName) {

	}

	public void populateDocUploadModel(String heading, String value) {
		isEmptyRow = false;
		value = value.trim();
		if (heading.equalsIgnoreCase(TTBHeadingMapping.Headings.EIN_NUM.toString())) {
			currentTTBUpload.setEinNum(value);
			return;
		}

		if (heading.equalsIgnoreCase(TTBHeadingMapping.Headings.COMPANY_NM.toString())) {
			currentTTBUpload.setCompanyNm(value);
			return;
		}

		if (heading.equalsIgnoreCase(TTBHeadingMapping.Headings.TPBD.toString())) {
			currentTTBUpload.setTpbd(value);
			if(value != null) {
				Long fiscalYear = CTPUtil.getFiscalYearMMDDYYYY(value);
				if(fiscalYear != null && fiscalYear.longValue() < 100L) {
					fiscalYear = Long.parseLong(millenniumCentury) * 100L + fiscalYear.longValue();
				}
				if(fiscalYear == null) fiscalYear = -1L;
				if(fiscalYear.longValue() == output.getFiscalYear()) {
					currentTTBUpload.setFiscalYr(fiscalYear);
				}
				else currentTTBUpload.setFiscalYr(new Long(-1L));
			}
			return;
		}

		if (heading.equalsIgnoreCase(TTBHeadingMapping.Headings.PERMIT_NUM.toString())) {
			PermitBuilder permitBuilder = new PermitBuilder(value);
			currentTTBUpload.setPermitNum(permitBuilder.unformatPermit());
			return;
		}

		if (heading.equalsIgnoreCase(TTBHeadingMapping.Headings.CHEWSNUFF_TAXES.toString())) {
			String chewsnuffTx = value;
			if (chewsnuffTx != null)
				currentTTBUpload.setChewSnuffTaxes(Double.parseDouble(chewsnuffTx));
			else
				currentTTBUpload.setChewSnuffTaxes(null);
			return;
		}

		if (heading.equalsIgnoreCase(TTBHeadingMapping.Headings.CIGAR_TAXES.toString())) {
			String cigarsTx = value;
			if (cigarsTx != null)
				currentTTBUpload.setCigarTaxes(Double.parseDouble(cigarsTx));
			else
				currentTTBUpload.setCigarTaxes(null);
			return;
		}

		if (heading.equalsIgnoreCase(TTBHeadingMapping.Headings.CIGARETTE_TAXES.toString())) {
			String cigarettesTx = value;
			if (cigarettesTx != null)
				currentTTBUpload.setCigaretteTaxes(Double.parseDouble(cigarettesTx));
			else
				currentTTBUpload.setCigaretteTaxes(null);
			return;
		}

		if (heading.equalsIgnoreCase(TTBHeadingMapping.Headings.PIPERYO_TAXES.toString())) {
			String pipeRYOTx = value;
			if (pipeRYOTx != null)
				currentTTBUpload.setPipeRyoTaxes(Double.parseDouble(pipeRYOTx));
			else
				currentTTBUpload.setPipeRyoTaxes(null);
		}
		
		if (heading.equalsIgnoreCase(TTBHeadingMapping.Headings.INC_ADJ.toString())) {
			String incAdj = value;
			if (incAdj != null)
				currentTTBUpload.setIncAdj(Double.parseDouble(incAdj));
			else
				currentTTBUpload.setIncAdj(null);
		}
		
		if (heading.equalsIgnoreCase(TTBHeadingMapping.Headings.DEC_ADJ.toString())) {
			String decAdj = value;
			if (decAdj != null)
				currentTTBUpload.setDecAdj(Double.parseDouble(decAdj));
			else
				currentTTBUpload.setDecAdj(null);
		}
	}

//	private String getDateCellValue(String value, String format) {
//		SimpleDateFormat sdf = new SimpleDateFormat(format);
//		return sdf.format(value);
//	}

	/**
	 * Call validation, attach error messages if they exist.
	 * @param rowNum
	 */
	private void validateDocUploadBean(int rowNum) {
		// validate, save errors in bean
		Set<ConstraintViolation<TTBUpload>> violations = validator.validate(this.currentTTBUpload);
		StringBuilder errorMsgs = new StringBuilder(2000);
		Iterator<ConstraintViolation<TTBUpload>> iter = violations.iterator();
		if(iter.hasNext()) {
			errorMsgs.append("Row #");
			errorMsgs.append(rowNum+1);
			errorMsgs.append(": ");
			while (iter.hasNext()) {
				ConstraintViolation<TTBUpload> violation = iter.next();
				errorMsgs.append("[");
				errorMsgs.append(violation.getPropertyPath());
				errorMsgs.append(":");
				errorMsgs.append(violation.getMessage());
				errorMsgs.append("] ");
				output.incrementErrorCount();;
			}
		}
		this.currentTTBUpload.setErrors(errorMsgs.toString());
	}

}

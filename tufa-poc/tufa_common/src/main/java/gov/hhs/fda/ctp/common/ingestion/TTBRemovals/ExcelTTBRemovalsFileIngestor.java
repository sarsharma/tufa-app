package gov.hhs.fda.ctp.common.ingestion.TTBRemovals;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.ingestion.ExcelFileIngestor;

@Component("TTBRemovalsFileIngestor")
public class ExcelTTBRemovalsFileIngestor extends ExcelFileIngestor{
	private int minColumns = 10;

	
	public void parseTTBRemovalsFileSAX(String fileName,IngestionOutput output) throws InvalidFormatException, IOException, OpenXML4JException, SAXException{
		File xlsxFile = new File(fileName);
		OPCPackage pkg  = OPCPackage.open(xlsxFile.getPath(),PackageAccess.READ);
		TTBRemovalsExcel2DocUpload  ttbRemovalsExcelToDocUpload = new TTBRemovalsExcel2DocUpload(pkg,output ,minColumns);
		ttbRemovalsExcelToDocUpload.process();
		pkg.revert();
	}
	
	@Override
	public void parseFileSAX(String fileName, IngestionOutput output)
			throws InvalidFormatException, IOException, OpenXML4JException, SAXException {
		this.parseTTBRemovalsFileSAX(fileName, output);
	}
}

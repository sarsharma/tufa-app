package gov.hhs.fda.ctp.common.ingestion.TTBRemovals;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class TTBRemovalsErrorHandler implements ErrorHandler{
	Logger logger = LoggerFactory.getLogger(TTBRemovalsErrorHandler.class);
	
	private String getParseExceptionInfo(SAXParseException spe) {
        String systemId = spe.getSystemId();

        if (systemId == null) {
            systemId = "null";
        }

        String info = "URI=" + systemId + " Line=" 
            + spe.getLineNumber() + ": " + spe.getMessage();

        return info;
    }
	
	@Override
	public void warning(SAXParseException exception) throws SAXException {
	}

	@Override
	public void error(SAXParseException exception) throws SAXException {
	}

	@Override
	public void fatalError(SAXParseException spe) throws SAXException {
		String message = "Fatal Error: " + getParseExceptionInfo(spe);
		logger.error(message);
        throw new SAXException(message);
	}
}

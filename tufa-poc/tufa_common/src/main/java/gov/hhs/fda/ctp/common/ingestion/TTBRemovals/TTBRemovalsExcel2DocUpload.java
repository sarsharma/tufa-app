package gov.hhs.fda.ctp.common.ingestion.TTBRemovals;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.util.SAXHelper;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.model.StylesTable;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;

public class TTBRemovalsExcel2DocUpload {

	OPCPackage xlsxPackage;
	IngestionOutput output;
	Integer minColumns;
	TTBRemovalsSheetHandler sheetHandler;

	public TTBRemovalsExcel2DocUpload(OPCPackage pkg, IngestionOutput output, int minColumns) {
		this.xlsxPackage = pkg;
		this.output = output;
		this.minColumns = minColumns;
		sheetHandler = new TTBRemovalsSheetHandler(output);
	}

	public void processSheet(StylesTable styles, ReadOnlySharedStringsTable strings, SheetContentsHandler sheetHandler,
			InputStream sheetInputStream) throws IOException, SAXException {
		DataFormatter formatter = new DataFormatter();
		InputSource sheetSource = new InputSource(sheetInputStream);
		try {
			XMLReader sheetParser = SAXHelper.newXMLReader();
			ContentHandler handler = new XSSFSheetXMLHandler(styles, null, strings, sheetHandler, formatter, false);
			sheetParser.setContentHandler(handler);
			sheetParser.setErrorHandler(new TTBRemovalsErrorHandler());
			sheetParser.parse(sheetSource);
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("SAX parser appears to be broken - " + e.getMessage());
		}
	}

	 public void process() throws IOException, OpenXML4JException, SAXException {
	        ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(this.xlsxPackage);
	        XSSFReader xssfReader = new XSSFReader(this.xlsxPackage);
	        StylesTable styles = xssfReader.getStylesTable();
	        XSSFReader.SheetIterator iter = (XSSFReader.SheetIterator) xssfReader.getSheetsData();
	        while (iter.hasNext()) {
	        	InputStream stream = iter.next();
	            processSheet(styles, strings,sheetHandler, stream);
	            stream.close();
	            //Only first sheet has to be read, for now
	            break;
	        }
	 }

}

package gov.hhs.fda.ctp.common.ingestion.TTBRemovals;

import java.util.Map;

public class TTBRemovalsHeadingMapping {
	
	public static enum Headings {
		PERIOD, NAME_OWNER, ID_PERMIT, EIN, PRODUCT, POUNDS;
	}

	private Map<String, String> ttbRemovalsHeadings;

	public TTBRemovalsHeadingMapping(Map<String, String> ttbheadings) {
		this.ttbRemovalsHeadings = ttbheadings;
	}

	public String getDocHeading(String excelHeading) {
		if (ttbRemovalsHeadings != null)
			return ttbRemovalsHeadings.get(excelHeading);
		return null;
	}

}

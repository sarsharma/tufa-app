package gov.hhs.fda.ctp.common.ingestion.TTBRemovals;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.beans.TTBRemovalsUpload;
import gov.hhs.fda.ctp.common.exception.IngestionParsingException;
import gov.hhs.fda.ctp.common.util.ApplicationContextUtil;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.PermitBuilder;

public class TTBRemovalsSheetHandler implements SheetContentsHandler {
	Logger logger = LoggerFactory.getLogger(TTBRemovalsSheetHandler.class);

	private TTBRemovalsHeadingMapping ttbRemovalsHeadingMapping;

	private IngestionOutput output;

	private TTBRemovalsUpload currentTTBRemovalsUpload;

	private String millenniumCentury;

	private Map<String, String> ttbRemovalsHeaderIndexMapping = new HashMap<>();

	private boolean firstRow = false;

	private Validator validator;

	private boolean isEmptyRow = true;

	private final static Integer TTB_REMOVALS_COLS = 6;

	public TTBRemovalsSheetHandler(IngestionOutput output) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		this.validator = factory.getValidator();
		this.output = output;
		String year = Long.toString(output.getFiscalYear());
		millenniumCentury = year.substring(0, 2);
		this.ttbRemovalsHeadingMapping = (TTBRemovalsHeadingMapping) ApplicationContextUtil.getApplicationContext()
				.getBean("TTBRemovalsHeadingMapping");
	}

	@Override
	public void startRow(int rowNum) {
		isEmptyRow = true;
		this.firstRow = (rowNum == 0) ? true : false;
		if (!firstRow)
			this.currentTTBRemovalsUpload = new TTBRemovalsUpload();
	}

	@Override
	public void endRow(int rowNum) {

		if (firstRow) {
			if (ttbRemovalsHeaderIndexMapping.size() != TTB_REMOVALS_COLS)
				throw new IngestionParsingException("Invalid column headings in TTB Removals file");
		}

		if (!firstRow && !isEmptyRow) {
			if (this.includeDates(currentTTBRemovalsUpload.getPeriod(), output.getFiscalYear())) {
				this.validateDocUploadBean(rowNum);
				this.output.add(currentTTBRemovalsUpload);
			}
		}
	}
	
	private boolean includeDates(String period, Long fiscalYr) {
		boolean returnVal = true;
		Long entryFiscalYear = CTPUtil.getFiscalYearMMMyy(period);
		if (entryFiscalYear != null ) {
			if (!fiscalYr.equals(Long.parseLong(millenniumCentury) * 100L + entryFiscalYear)) {
				returnVal = false;
			}
		} else{
			returnVal = false;
		}
		return returnVal;
	}

	@Override
	public void cell(String cellReference, String formattedValue, XSSFComment comment) {

		if (this.firstRow) {
			String colHeading = formattedValue;
			if (this.ttbRemovalsHeadingMapping.getDocHeading(colHeading.trim().toUpperCase()) != null)
				ttbRemovalsHeaderIndexMapping.put(cellReference.replaceAll("\\d", ""),
						formattedValue.trim().toUpperCase());
		} else {
			String columnIndex = cellReference.replaceAll("\\d", "");
			String columnHeading = ttbRemovalsHeaderIndexMapping.get(columnIndex);
			String colHeadingMapped = ttbRemovalsHeadingMapping.getDocHeading(columnHeading);
			if (colHeadingMapped != null)
				this.populateDocUploadModel(colHeadingMapped, formattedValue);
		}

	}

	@Override
	public void headerFooter(String text, boolean isHeader, String tagName) {

	}

	public void populateDocUploadModel(String heading, String value) {

		isEmptyRow = false;

		if (heading.equalsIgnoreCase(TTBRemovalsHeadingMapping.Headings.PERIOD.toString())) {
			currentTTBRemovalsUpload.setPeriod(value);
			if (value != null) {
				Long fiscalYear = CTPUtil.getFiscalYearMMMyy(value);
				if (fiscalYear != null && fiscalYear.longValue() < 100L) {
					fiscalYear = Long.parseLong(millenniumCentury) * 100L + fiscalYear.longValue();
				}
				if (fiscalYear == null)
					fiscalYear = -1L;

				if (fiscalYear.longValue() == output.getFiscalYear()) {
					currentTTBRemovalsUpload.setFiscalYr(fiscalYear);
				} else
					currentTTBRemovalsUpload.setFiscalYr(new Long(-1L));
			}
			return;
		}

		if (heading.equalsIgnoreCase(TTBRemovalsHeadingMapping.Headings.EIN.toString())) {
			currentTTBRemovalsUpload.setEin(value);
			return;
		}

		if (heading.equalsIgnoreCase(TTBRemovalsHeadingMapping.Headings.ID_PERMIT.toString())) {
			PermitBuilder permitBuilder = new PermitBuilder(value);
			currentTTBRemovalsUpload.setIdPermit(permitBuilder.unformatPermit());
			return;
		}

		if (heading.equalsIgnoreCase(TTBRemovalsHeadingMapping.Headings.PRODUCT.toString())) {
			currentTTBRemovalsUpload.setProduct(value);
			return;
		}

		if (heading.equalsIgnoreCase(TTBRemovalsHeadingMapping.Headings.NAME_OWNER.toString())) {
			currentTTBRemovalsUpload.setNameOwner(value);
			return;
		}

		if (heading.equalsIgnoreCase(TTBRemovalsHeadingMapping.Headings.POUNDS.toString())) {
			if (value != null) {
				currentTTBRemovalsUpload.setPounds(Double.parseDouble(value));
			} else
				currentTTBRemovalsUpload.setPounds(null);
			return;
		}

	}

	// private String getDateCellValue(String value, String format) {
	// SimpleDateFormat sdf = new SimpleDateFormat(format);
	// return sdf.format(value);
	// }

	/**
	 * Call validation, attach error messages if they exist.
	 * 
	 * @param rowNum
	 */
	private void validateDocUploadBean(int rowNum) {
		// validate, save errors in bean
		Set<ConstraintViolation<TTBRemovalsUpload>> violations = validator.validate(this.currentTTBRemovalsUpload);
		StringBuilder errorMsgs = new StringBuilder(2000);
		Iterator<ConstraintViolation<TTBRemovalsUpload>> iter = violations.iterator();
		if (iter.hasNext()) {
			errorMsgs.append("Row #");
			errorMsgs.append(rowNum + 1);
			errorMsgs.append(": ");
			while (iter.hasNext()) {
				ConstraintViolation<TTBRemovalsUpload> violation = iter.next();
				errorMsgs.append("[");
				errorMsgs.append(violation.getMessage());
				errorMsgs.append("] ");
				output.incrementErrorCount();
			}
		}
		this.currentTTBRemovalsUpload.setErrors(errorMsgs.toString());
	}

}

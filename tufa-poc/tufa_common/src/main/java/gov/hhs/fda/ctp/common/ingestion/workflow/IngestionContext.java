package gov.hhs.fda.ctp.common.ingestion.workflow;

import java.util.List;

import gov.hhs.fda.ctp.common.beans.IngestionWorkflowContext;

public class IngestionContext {
	
	private List<IngestionWorkflowContext> contexts;
	

	public List<IngestionWorkflowContext> getContexts() {
		return contexts;
	}

	public void setContexts(List<IngestionWorkflowContext> contexts) {
		this.contexts = contexts;
	}
	

}

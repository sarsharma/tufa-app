package gov.hhs.fda.ctp.common.ingestion.workflow;

import java.util.HashMap;
import java.util.Map;

import gov.hhs.fda.ctp.common.beans.IngestionWorkflowContext;

public class IngestionWFUserContext {
	
	private Map<String,IngestionWorkflowContext> currentUserContexts = new HashMap<>();
	

	public Map<String,IngestionWorkflowContext> getCurrentUserContexts() {
		return currentUserContexts;
	}

	public void setCurrentUserContexts(Map<String,IngestionWorkflowContext> currentUserContexts) {
		this.currentUserContexts = currentUserContexts;
	}

	/** 
	 * @param usernameFiscalYear combination of username and the requested fiscal year. Ex: test@test.com2014
	 * @param userContext detailOnly or summDetail
	 * */
	public void updateUserContext(String usernameFiscalYear, IngestionWorkflowContext userContext){
		this.currentUserContexts.put(usernameFiscalYear, userContext);
	}
	
	/** 
	 * @param usernameFiscalYear combination of username and the requested fiscal year. Ex: test@test.com2014
	 * */
	public IngestionWorkflowContext getUserContext(String usernameFiscalYear){
		return this.currentUserContexts.get(usernameFiscalYear);
	}
		
}

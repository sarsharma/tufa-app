package gov.hhs.fda.ctp.common.pagination;

public class PaginationAttributes {
	
	int activePage;
	int pageSize;
	int itemsTotal;
	boolean calculateTotalRows;
	String [] sortBy;
	String sortOrder;
	int filteredRows;

	public PaginationAttributes(){
		
	}
	
	public PaginationAttributes(int activePage, int pageSize, boolean calculateTotalRows,String [] sortBy, String sortOrder) {
		this.activePage = activePage;
		this.pageSize = pageSize;
		this.calculateTotalRows = calculateTotalRows;
		this.sortBy = sortBy;
		this.sortOrder = sortOrder;
	}

	public int getActivePage() {
		return activePage;
	}

	public void setActivePage(int activePage) {
		this.activePage = activePage;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public boolean isCalculateTotalRows() {
		return calculateTotalRows;
	}

	public void setCalculateTotalRows(boolean calculateTotalRows) {
		this.calculateTotalRows = calculateTotalRows;
	}

	public int getItemsTotal() {
		return itemsTotal;
	}

	public void setItemsTotal(int itemsTotal) {
		this.itemsTotal = itemsTotal;
	}

	public String[] getSortBy() {
		return sortBy;
	}

	public void setSortBy(String[] sortBy) {
		this.sortBy = sortBy;
	}

	public int getFilteredRows() {
		return filteredRows;
	}

	public void setFilteredRows(int filteredRows) {
		this.filteredRows = filteredRows;
	}
}

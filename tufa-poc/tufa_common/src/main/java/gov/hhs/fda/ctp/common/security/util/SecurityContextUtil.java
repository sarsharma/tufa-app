package gov.hhs.fda.ctp.common.security.util;

import gov.hhs.fda.ctp.common.security.UserContext;

public interface SecurityContextUtil {
	public UserContext getPrincipal();
	public String getSessionUser();
}

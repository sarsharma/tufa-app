package gov.hhs.fda.ctp.common.security.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import gov.hhs.fda.ctp.common.security.UserContext;

@Component()
public class SecurityContextUtilImpl implements SecurityContextUtil{

	private SecurityContextUtilImpl() {}
	
	public UserContext getPrincipal(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserContext user = null;
		
		if( auth!=null && auth.isAuthenticated()  )
			user =(UserContext)auth.getPrincipal();
		return user;
	}
	
	public String getSessionUser(){
		return getPrincipal() != null
				? getPrincipal().getUsername() : null;
	}
}

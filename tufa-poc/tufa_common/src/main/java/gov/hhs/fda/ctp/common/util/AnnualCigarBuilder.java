/**
 * 
 */
package gov.hhs.fda.ctp.common.util;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gov.hhs.fda.ctp.common.beans.Assessment;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.AssessmentVersion;

/**
 * @author Anthony.Gunter
 *
 */
public class AnnualCigarBuilder {
	Assessment finalAssessment;
	List<AssessmentVersion> cigarVersions;
	List<AssessmentVersion> fourthQuarterVersions;

	/**
	 * Create AnnualCigarBuilder
	 * @param cigarAssessment
	 * @param fourthQuarterAssessment
	 */
	public AnnualCigarBuilder(Assessment cigarAssessment, Assessment fourthQuarterAssessment) {
		this.cigarVersions = cigarAssessment.getVersions();
		this.fourthQuarterVersions = fourthQuarterAssessment.getVersions();
		finalAssessment = fourthQuarterAssessment;
	}
	
	/**
	 * Build the fourth quarter assessment.
	 * @return
	 */
	public Assessment buildAssessment() {
		// create a map of the versions in final assessment
		Map<Long, AssessmentVersion> versionMap = new HashMap<Long, AssessmentVersion>();
		for(AssessmentVersion version : fourthQuarterVersions) {
			Long key = version.getVersionNum();
			versionMap.put(key, version);
		}
		// loop through cigar versions and add marketshare to final version
		for(AssessmentVersion version : cigarVersions) {
			AssessmentVersion finalVersion = versionMap.get(version.getVersionNum());
			// just in case this version doesn't exist in final
			if(finalVersion == null) {
				finalVersion = new AssessmentVersion();
				finalVersion.setVersionNum(version.getVersionNum());
				finalVersion.setCreatedBy(version.getCreatedBy());
				finalVersion.setCreatedDt(version.getCreatedDt());
				fourthQuarterVersions.add(finalVersion);
				versionMap.put(version.getVersionNum(), finalVersion);
			}
			// pull the exports out of version and add them to final version
			Map<String, List<AssessmentExport>> cigarExports = version.getExports();
			List<AssessmentExport> cigarExportList = cigarExports.get("Cigars");
			Map<String, List<AssessmentExport>> exports = finalVersion.getExports();
			// add cigar exports to other tobacco exports if not empty
			if(cigarExports != null && !cigarExports.isEmpty()) {
				if(exports == null) {
					finalVersion.setExports(cigarExports);
				}
				else {
					exports.put("Cigars", cigarExportList);
				}
			}
		}
		return finalAssessment;
	}
}

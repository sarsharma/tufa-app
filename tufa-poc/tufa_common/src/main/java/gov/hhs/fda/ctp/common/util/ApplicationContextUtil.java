package gov.hhs.fda.ctp.common.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ApplicationContextUtil implements ApplicationContextAware{

	private static ApplicationContext ctx;

	@Override
	public void setApplicationContext(ApplicationContext appCtx) throws BeansException {
		ctx = appCtx;
	}
	
	public static ApplicationContext getApplicationContext(){
			return ctx;
	}
}

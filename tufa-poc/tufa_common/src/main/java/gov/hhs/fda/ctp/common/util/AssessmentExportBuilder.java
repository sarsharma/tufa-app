/**
 * 
 */
package gov.hhs.fda.ctp.common.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import gov.hhs.fda.ctp.common.beans.AnnualExportComparator;
import gov.hhs.fda.ctp.common.beans.Assessment;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.AssessmentVersion;
import gov.hhs.fda.ctp.common.beans.MarketShare;
import gov.hhs.fda.ctp.common.beans.MarketShareComparator;
/**
 * @author Anthony.Gunter
 *
 */
public class AssessmentExportBuilder {
	enum OriginalBehavior {ORIGINAL, CORRECTED, SPECIAL_ORIGINAL};
	private Assessment assessment;
	private Exportable exportBehavior;
	
	public AssessmentExportBuilder(Assessment assessment) {
		this.assessment = assessment;
		exportBehavior = ExportableFactory.getExportBehavior(assessment);
	}

	/**
	 * Utility method to build the export.
	 * @param version
	 * @param marketShares
	 * @return export csv
	 */
	private AssessmentExport getExport(AssessmentVersion version, List<MarketShare> marketShares) {
		boolean addressOnly = "ACONLY".equals(version.getMarketshareType());
		// create title
		AssessmentExport export = new AssessmentExport();
		if(marketShares == null || marketShares.size() < 1){
			return export;
		}
		MarketShare firstMarketShare = marketShares.get(0);
		OriginalBehavior originalCorrectedOrCombined = originalCorrectedOrCombined(firstMarketShare, exportBehavior.isOriginal(version));
		// build title
		StringBuilder title = new StringBuilder("FY_");
		int titleYear = exportBehavior.getDisplayYear(version);
		int titleQuarter = exportBehavior.getDisplayQuarter(version);
		if("CIGARS".equalsIgnoreCase(firstMarketShare.getTobaccoClassNm()) && ("_Annual_".equalsIgnoreCase(exportBehavior.prependedVersionTitle()))){
			if(titleQuarter == 1){
				titleQuarter = 4;
			}else{
				titleQuarter = titleQuarter - 1;
				titleYear = titleYear + 1;
			}
		}
		title.append(titleYear);
		title.append("_Q");
		
		
		title.append(titleQuarter);
		title.append("_");
		title.append(firstMarketShare.getTobaccoClassNm());
		title.append(exportBehavior.prependedVersionTitle());
		title.append("v");
		title.append(firstMarketShare.getVersionNum());
		if(originalCorrectedOrCombined == OriginalBehavior.CORRECTED) {
			title.append("_Corrected");
		}
		title.append(".csv");
		// create csv data
		// Add data to csv.  Currently not fleshed out, but this is what we know so far:
		//				•	Rank = I don’t know where this comes from, there is no shown value
		//				•	Year = Unsure if this is fiscal or calendar
		//				•	Q = Quarter
		//				•	Company Name
		//				•	TTB No. (no example)
		//				•	Tax ID (9 digits so this may be the EIN)
		//				•	“Total Volume Removed (number)” - For Cigars - it will be divided by 4
		//				•	“Total Taxes Paid ($)” - For Cigars - it will be divided 4
		//				•	Share of Total Taxes (this is a decimal value)
		//				•	Company Street Address
		//				•	City
		//				•	State -> State/Province
		//				•	Class -> Tobacco Class (I find this redundant if the entire file is related to one tobacco class)
		//				•	Type ("O" for original version 1, "C" for changed subsequent versions)
		CsvStringBuilder csv = new CsvStringBuilder();
		// header row
		csv.csvAppendRaw("RANK,YEAR,Q,Company Name,TTB No.,Tax ID,Total Volume Removed (number),Total Taxes Paid ($),");
		if(originalCorrectedOrCombined != OriginalBehavior.ORIGINAL && !addressOnly) {
			// never include deltas for addressOnly assessment
			csv.csvAppendRaw("Revised Share of Total Taxes,Previous Share of Total Taxes,Delta (Revised - Previous),");
		}
		else {
			csv.csvAppendRaw("Share of Total Taxes,");
		}
		csv.csvAppendRaw("Company Street Address,City,State,Zip Code,Phone Number,Class,Type,Email Address");
		if(exportBehavior.displayAddressChange()) {
			csv.csvAppendRaw(",Address Changed,Contact Changed");
		}
		csv.csvAppendNewLine("");
		int rank = 1;
		for(MarketShare detail : marketShares) {
			String stateOrProvince = detail.getState();
			if(stateOrProvince == null || stateOrProvince.isEmpty()) {
				stateOrProvince = detail.getProvince();
			}
			csv.csvAppend(rank++);
			csv.csvAppend(titleYear);
			csv.csvAppendRaw("Q");
			csv.csvAppend(titleQuarter);
			csv.csvAppendInQuotes(detail.getLegalName());
			csv.csvAppend((detail.getPermitNum() != null) ? detail.getPermitNum() : " ");
			csv.csvAppendRawQuotes(detail.getEIN());
			csv.csvAppendFormat("###,###,###.##", exportBehavior.getRemovalVolume(detail));
			csv.csvAppendFormat("$###,###,###.00", exportBehavior.getTotalTaxes(detail));
			csv.csvAppendFormat("0.######", detail.getShareTotTaxes());

			if(originalCorrectedOrCombined != OriginalBehavior.ORIGINAL && !addressOnly) {
				csv.csvAppendFormat("0.######", detail.getPreviousShare() != null ? detail.getPreviousShare(): 0.0);
				csv.csvAppendFormat("0.######", detail.getDeltaShare() != null ? detail.getDeltaShare() : 0.0);
			}

			// append the suite to street address if it exists
			/*if(detail.getSuite() != null && detail.getStreetAddress() != null && detail.getStreetAddress().trim().length()>0) {
				csv.csvAppendInQuotes(detail.getStreetAddress() + " Suite " + detail.getSuite());
			}
			else {*/
				//suite has been added to view so removing the logic in java
				csv.csvAppendInQuotes(detail.getStreetAddress());
			//}
			
			csv.csvAppendInQuotes(detail.getCity());
			csv.csvAppendInQuotes(stateOrProvince);
			csv.csvAppendInQuotes(detail.getPostalCd());
			csv.csvAppendInQuotes(CTPUtil.formatRawPhoneNumber(detail.getPhoneNum(), detail.getCntryDialCd()));
			csv.csvAppend(getTobaccoDisplayName(detail.getTobaccoClassNm()));
			if(originalCorrectedOrCombined != OriginalBehavior.CORRECTED) {
				csv.csvAppend("O");
			}
			else {
				csv.csvAppend("C");
			}
			if(exportBehavior.displayAddressChange()) {
				csv.csvAppend(detail.getEmailAddress());
				csv.csvAppend(detail.getAddressChangedFlag());
				csv.csvAppendNewLine(detail.getContactChangedFlag());
			}
			else {
				csv.csvAppendNewLine(detail.getEmailAddress());
			}
		}
		export.setTitle(title.toString());
		export.setCsv(csv.toString());
		return export;
	}

	/**
	 * Decide if record is original or corrected.  
	 * @param record
	 * @param isOriginalFromExportable
	 * @return
	 */
	private OriginalBehavior originalCorrectedOrCombined(MarketShare record, boolean isOriginalFromExportable) {
		OriginalBehavior decision = OriginalBehavior.CORRECTED;
		String originalCorrected = record.getOriginalCorrected();
		if(originalCorrected == null) {
			originalCorrected = "";
		}
		switch(originalCorrected) {
			case "O":
				// has been marked as original
				decision = OriginalBehavior.ORIGINAL;
				break;
			case "B":
				// original, but needs to show the delta columns.
				decision = OriginalBehavior.SPECIAL_ORIGINAL;
				break;
			default:
				// not determined in marketshare record, check the behavior from exportable
				if(isOriginalFromExportable) {
					decision = OriginalBehavior.ORIGINAL;
				}
				break;
		}
		return decision;
	}
	

	/**
	 * Return the value we should display on the report.
	 * @param tobaccoType
	 * @return displayTobaccoType
	 */
	private String getTobaccoDisplayName(String tobaccoType) {
		String returnVal = tobaccoType;
		if("Roll-Your-Own".equals(tobaccoType)) {
			returnVal = "Roll-Your-Own Tobacco";
		}
		if("Pipe".equals(tobaccoType)) {
			returnVal = "Pipe Tobacco";
		}
		if("Chew".equals(tobaccoType)) {
			returnVal = "Chewing Tobacco";
		}
		return returnVal;
	}
	
	/**
	 * Build the market share exports and attach them to the assessment.
	 * @return
	 */
	public Assessment buildAssessmentExports() {
		MarketShareMapFactory mapFactory = new MarketShareMapFactory();

		// loop through each version creating a map of exports by tobacco type
		List<AssessmentVersion> versions = assessment.getVersions();
		for(AssessmentVersion version : versions) {			
			// determine if this version was the original submitted version.

			// get the map of market share records per tobacco class for this version.
			Map<String, List<MarketShare>> marketShareMap = mapFactory.getMarketShareMap(version);
			// create a map to store the exports per tobacco class
			Map<String, List<AssessmentExport>> exportMap = new HashMap<>();
			// loop through each tobacco class and create export
			for(String tobaccoType : marketShareMap.keySet()) {
				// get the list of market shares for this tobacco type
				List<MarketShare> marketShares = marketShareMap.get(tobaccoType);
				Collections.sort(marketShares, new MarketShareComparator());
				// this should be a single object instead of a list.
				// re-factor please TODO
				List<AssessmentExport> exports = new ArrayList<>();
				
				// get the export
				AssessmentExport export = getExport(version, marketShares);
				export.setTobaccoType(tobaccoType);
				exports.add(export);
				// add this tobacco type to the map
				exportMap.put(tobaccoType, exports);
			}
			Map<String, List<AssessmentExport>> sortMap = new TreeMap<String, List<AssessmentExport>>(new AnnualExportComparator());
			sortMap.putAll(exportMap);
			version.setExports(sortMap);
		}
		return assessment;
	}

}

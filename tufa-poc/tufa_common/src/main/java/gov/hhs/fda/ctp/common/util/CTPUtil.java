/**
 *
 */
package gov.hhs.fda.ctp.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.hhs.fda.ctp.common.exception.TufaException;

/**
 * @author tgunter
 *
 */
public class CTPUtil {
	private static final Logger logger = LoggerFactory.getLogger(CTPUtil.class);
	
	private static final Integer FY_OCT_MNTH = 10;
	private static final Integer FY_SEPT_MNTH = 9;
	private static final String DATE_PATTERN = "yyyy-mm";
	private static final String DATE_YYY_MM_DD_PATTERN = "yyyy-MM-dd";


	/**
	 * Hidden default constructor to prevent instantiation.
	 */
	private CTPUtil() {
		/*
		 * Don't want user to instantiate. All members should be static.
		 */
	}

	/**
	 * Parse a string date into a java date.
	 *
	 * @param dateString
	 * @return Date
	 *
	 */
	public static Date parseStringToDate(String dateString) {
		/* if date does not exist, return null */
		if (dateString == null || dateString.isEmpty()) {
			return null;
		}
		/*
		 * test to see if it's MM/dd/yyyy (database) or yyyy-MM-dd (date picker
		 * format)
		 */
		SimpleDateFormat simpleDateFormat;
		String stringToParse;
		if (dateString.matches("\\d{2}/\\d{2}/\\d{4}")) {
			simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
			stringToParse = dateString;
		} else if (dateString.matches("[a-zA-Z]{3} [a-zA-Z]{3} [0-9]{2} [0-9]{4}.+")) {
			simpleDateFormat = new SimpleDateFormat("EEE MMM dd yyyy", Locale.US);
			stringToParse = dateString.substring(0, 15);
		} else if (dateString.matches("[0-9]{8}")) {
			simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
			stringToParse = dateString;
		} else {
			simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
			stringToParse = dateString;
		}
		simpleDateFormat.setLenient(false);

		/* parse, throw exception with error message on failure */
		try {
			logger.debug("parsing date {}.", stringToParse);
			return simpleDateFormat.parse(stringToParse);
		} catch (Exception e) {
			logger.warn("Parsing failed on {}. \n {}", dateString, e);
			throw new TufaException("Invalid date format.", e);
		}
	}

	/**
	 * Parse a date into a string.
	 * 
	 * @param date
	 * @return String
	 */
	public static String parseDateToString(Date date) {
		/* return empty string if date is null */
		if (date == null) {
			return Constants.EMPTY;
		}
		DateFormat simpleDate = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
		return simpleDate.format(date);
	}

	/**
	 * Parse a date into military time.
	 * 
	 * @param date
	 * @return
	 */
	public static String parseTimeToString(Date date) {
		if (date == null) {
			return Constants.EMPTY;
		}
		DateFormat simpleDate = new SimpleDateFormat("HH:mm");
		return simpleDate.format(date);
	}

	public static String returnSetAsCSV(Set<String> set) {
		int count = 0;
		StringBuilder text = new StringBuilder(Constants.EMPTY);
		if (set != null && !set.isEmpty()) {
			for (String member : set) {
				text.append("'");
				text.append(member);
				text.append("'");
				if (++count != set.size()) {
					text.append(",");
				}
			}
		}
		return text.toString();
	}

	public static boolean is7501Form(String formTypeCd) {
		return "7501".equals(formTypeCd);
	}

	public static boolean is5000Form(String formTypeCd) {
		return "5024".equals(formTypeCd) || "5000.24".equals(formTypeCd);
	}

	public static String formatRawPhoneNumber(String phoneNum, String countryDialCd) {
		if (phoneNum == null)
			return "";
		if (phoneNum.contains("-"))
			phoneNum = phoneNum.replaceAll("-", "");
		StringBuilder phoneBuilder = new StringBuilder(phoneNum);
		if (!phoneNum.contains("(")) {
			int length = phoneNum.length();
			if (length > 3) {
				phoneBuilder.insert(0, "(");
				phoneBuilder.insert(4, ") ");
				length += 3;
			}
			if (length > 9) {
				phoneBuilder.insert(9, "-");
				length++;
			}
			if (length > 14) {
				phoneBuilder.insert(14, " ");
			}
		}
		if (countryDialCd != null) {
			if (!(countryDialCd.equals("1") || countryDialCd.equals("+1"))) {
				phoneBuilder.insert(0, countryDialCd + " ");
			}
		}
		return phoneBuilder.toString();
	}

	/**
	 * Get fiscal year from a mmddyyyy date string.
	 * 
	 * @param mmddyyyy
	 * @return
	 */
	public static Long getFiscalYearMMDDYYYY(String mmddyyyy) {
		Long returnVal = null;
		if (mmddyyyy != null && mmddyyyy.length() > 5) {
			int yearVal = 0;
			int monthVal = 0;
			int firstSlash = mmddyyyy.indexOf('/');
			int lastSlash = mmddyyyy.lastIndexOf('/');
			if (firstSlash == lastSlash) {
				return null;
			}
			String month = mmddyyyy.substring(0, firstSlash);
			String year = mmddyyyy.substring(lastSlash + 1, mmddyyyy.length());
			try {
				yearVal = Integer.parseInt(year);
				monthVal = Integer.parseInt(month);
			} catch (Exception ex) {
				return null;
			}
			if (monthVal > 9) {
				yearVal++;
			}
			returnVal = new Long(yearVal);
		}
		return returnVal;
	}

	/**
	 * Get fiscal year from a yyyymmdd format string.
	 * 
	 * @param yyyymmdd
	 * @return
	 */
	public static Long getFiscalYearYYYYMMDD(String yyyymmdd) {
		Long returnVal = null;
		if (yyyymmdd != null && yyyymmdd.length() == 8) {
			int yearVal = 0;
			int monthVal = 0;
			String month = yyyymmdd.substring(4, 6);
			String year = yyyymmdd.substring(0, 4);
			try {
				yearVal = Integer.parseInt(year);
				monthVal = Integer.parseInt(month);
			} catch (Exception ex) {
				return null;
			}
			if (monthVal > 9)
				yearVal++;
			returnVal = new Long(yearVal);
		}
		return returnVal;
	}

	/**
	 * Get fiscal year from a yyyymmdd format string.
	 * 
	 * @param yyyymmdd
	 * @return
	 */
	public static Long getFiscalYearYYYYMMDD(String yyyymmdd, Integer monthPadding, Long fiscalYr) {
		Long returnVal = null;
		if (yyyymmdd != null && yyyymmdd.length() == 8) {
			int yearVal = 0;
			int monthVal = 0;
			String month = yyyymmdd.substring(4, 6);
			String year = yyyymmdd.substring(0, 4);
			try {
				yearVal = Integer.parseInt(year);
				monthVal = Integer.parseInt(month);
			} catch (Exception ex) {
				return null;
			}
			
			if (fiscalYr.intValue() == yearVal && monthVal<(FY_OCT_MNTH + monthPadding)) {
				returnVal = new Long(yearVal);
			} else if (fiscalYr.intValue() == yearVal && monthVal > FY_OCT_MNTH) {
				returnVal = new Long(++yearVal);
			} else if (fiscalYr.intValue() != yearVal && monthVal > FY_SEPT_MNTH - monthPadding)
				returnVal = new Long(++yearVal);
			else
				returnVal = new Long(yearVal);
		}
		return returnVal;
	}

	/**
	 * Derive a date from month and year.
	 * 
	 * @param month
	 * @param year
	 * @return
	 */
	public static Date getDateFromMonthAndYear(int month, int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.YEAR, year);
		Date date = calendar.getTime();
		return date;
	}

	/**
	 * Strip all the non-digit characters, return just numbers.
	 * 
	 * @param input
	 * @return
	 */
	public static String stripNonDigits(final CharSequence input) {
		final StringBuilder sb = new StringBuilder(input.length());
		for (int i = 0; i < input.length(); i++) {
			final char c = input.charAt(i);
			if (c > 47 && c < 58) {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	/**
	 * Test to see if string is null or empty.
	 * 
	 * @param test
	 * @return
	 */
	public static boolean stringNullOrEmpty(String test) {
		boolean returnVal = true;
		if (test != null) {
			if (test.trim().length() > 0) {
				returnVal = false;
			}
		}
		return returnVal;
	}

	/**
	 * Return two digit year from long value. I.E. 2017 returns "17".
	 * 
	 * @param fiscalYear
	 * @return
	 */
	public static String twoDigitYear(long fiscalYear) {
		String yearString = Long.toString(fiscalYear);
		int yearLength = yearString.length();
		if (yearLength < 4) {
			throw new TufaException("Invalid fiscal year.");
		}
		return yearString.substring(yearLength - 2);
	}

	/**
	 * REQ: Stands for roughly equal to based on the precision set (float)
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 * @param precision
	 *            the precision
	 * @return true, if successful
	 */
	public static boolean REQ(float v1, float v2, float precision) {
		if (Math.abs(v1 - v2) < precision) {
			return true;
		}
		return false;
	}

	public static Long getFiscalYearMMMyy(String value) {

		String[] dtprts = value.split("-");
		DateTimeFormatter formatter = DateTimeFormat.forPattern("MMM");
		Long yr = null;
		try {
			DateTime dt = formatter.parseDateTime(dtprts[0]);

			int mnth = dt.getMonthOfYear();
			yr = Long.parseLong(dtprts[1]);
			if (mnth > 9) {
				yr = Long.parseLong(dtprts[1]);
				yr = ++yr % 100;
			}
		} catch (Exception ex) {
			return null;
		}

		return yr;
	}

	/**
	 * REQD: Stands for roughly equal to based on the precision set (double)
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 * @param precision
	 *            the precision
	 * @return true, if successful
	 */
	public static boolean REQD(double v1, double v2, double precision) {
		if (Math.abs(v1 - v2) < precision) {
			return true;
		}
		return false;
	}

	public static String removecharachters(String importername) {
		return importername.replace("&apos;", "'").replace("&amp;", "&");

	}

	public static boolean isNotNullorNotEmpty(Object obj, String replaceSymbol) {
		return (obj == null) ? false : obj.toString().contains(replaceSymbol) ? StringUtils.isNotBlank(obj.toString().replace(replaceSymbol, "")) :StringUtils.isNotBlank(obj.toString());
	}
	
	public static String getNextMonthDate(String date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(CTPUtil.parseStringToDate(date));
		cal.set(Calendar.MONTH, cal.get(cal.MONTH) + 1);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return CTPUtil.parseDateToString(cal.getTime());
	}
	
	public static String getPrevMonthDate(String date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(CTPUtil.parseStringToDate(date));
		cal.set(Calendar.MONTH, cal.get(cal.MONTH) - 1);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return CTPUtil.parseDateToString(cal.getTime());
	}
	
	
	public static String addDaysToDate(String date, int daysToAdd){
		Calendar cal = Calendar.getInstance();
		cal.setTime(CTPUtil.parseStringToDate(date));
		cal.add(Calendar.DAY_OF_MONTH, daysToAdd);
		return CTPUtil.parseDateToString(cal.getTime());
		
	}
	public static int compareDateStrings(String date1, String date2){
		Date dateObj1 = CTPUtil.parseStringToDate(date1);
		Date dateObj2 = CTPUtil.parseStringToDate(date2);
	
		return dateObj1.compareTo(dateObj2);
	}
	
	public static String formatPermitNumWithDashes(String permitNum) {
		if(permitNum == null) 
			return null;
		return permitNum.substring(0, 2) + "-" + permitNum.substring(2, 4) + "-" + permitNum.substring(4);
	}

	
	public static boolean includeCBPDates(String entrySumDate, Long fiscalYr) {
		boolean returnVal = true;
		Long entryFiscalYear = CTPUtil.getFiscalYearYYYYMMDD(entrySumDate);
		// Long importFiscalYear = CTPUtil.getFiscalYearYYYYMMDD(importDate);
		//Long importFiscalYear = CTPUtil.getFiscalYearYYYYMMDD(entryDate, 1, fiscalYr);

		if (entryFiscalYear != null ) {
			if (!fiscalYr.equals(entryFiscalYear) ) {
				returnVal = false;
			}
		} else{
			returnVal = false;
		}
		return returnVal;
	}

	
	public static String getDateStringMMDDYY(String dateString){
		String finalString = "";
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd"); 
		SimpleDateFormat newFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
		Date date;
		if(dateString== null || dateString == ""){
			return finalString;
		}
		try {
			date = (Date)formatter.parse(dateString);
			finalString = newFormat.format(date);
		} catch (ParseException e) {
			logger.warn("Parsing failed on {}. \n {}", dateString, e);
		}
		return finalString;
	}
	
	public static boolean isOnlyNumbers(String text) {
	    try {
	        Double.parseDouble(text);
	        return true;
	    } catch (NumberFormatException ex) {
	        return false;
	    }
	} 
	public static String getDateWithLastDayOfMonth(String dateString) throws ParseException {
	    DateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(dateFormat.parse(dateString));
	    dateString = dateString +"-"+calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	    return dateString;
	}
	public static String getFiscalYrQtr(Date date) throws ParseException {
	    DateFormat dateFormat = new SimpleDateFormat(DATE_YYY_MM_DD_PATTERN);
	    String dateString = dateFormat.format(date);
	    String[] dtprts = dateString.split("-");
	   // String qtr = Constants.MMToQtr.get(dtprts[1]);
	    long exQtr = Long.parseLong(Constants.MMToQtr.get(dtprts[1]));
	    long calYr = Long.parseLong(dtprts[0]);
	    if(exQtr == 1) {
	    	calYr = calYr +1;
	    }
	    String fiscalYrQtr = calYr + "-" + exQtr;
	    return fiscalYrQtr;
	}
}

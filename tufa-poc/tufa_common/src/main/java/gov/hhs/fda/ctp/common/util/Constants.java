/**
 * 
 */
package gov.hhs.fda.ctp.common.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * @author tgunter
 *
 */
public class Constants {
    /* INGESTION TYPE */
	public static final String CBP = "CBP";
	public static final String CBPSumm = "CBP Summary";
	public static final String CBPLine = "CBP Details";
	public static final String TTB = "TTB Tax";
	public static final String TTB_VOLUME = "TTB Volume";
	public static final String PERMIT_UPDATE = "PERMIT Update";

	
    /* PERMIT_TYPE */
    public static final String MANUFACTURER_CODE = "MANU";
    public static final String IMPORTER_CODE = "IMPT";

    /* PERMIT STATUS */
    public static final String ACTIVE_CODE = "ACTV";
    public static final String CLOSED_CODE = "CLSD";
    
    /* generic constants */
    public static final String EMPTY = "";
    
    /* cigar assessment */
    public static final int CIGAR_ASSESSMENT_QTR = 0;
    
    /* assessment types */
    public static final String QUARTERLY = "QTRY";
    public static final String CIGAR = "CIQT";
    public static final String CIGR_QTRLY = "CIQT";
    public static final String ANNUAL = "ANNU";
    /** The Constant ADDRESS_DIFFERENTIATOR*/
    public static final String DIFFERENTIATOR = "-DIFFERENTIATOR-";
    /** The Constant COMMA_SEPERATOR*/
    public static final String COMMA_SEPERATOR = ",";
    /** The Constant SEMICOLON_SEPERATOR*/
    public static final String SEMICOLON_SEPERATOR = ";";
    
    /* MONTHS */
    public static final Map<String, String> MMMToNumber;
        /* Convert MMM to sortable string:  JUN = 06 */
        static {
            Map<String, String> staticMap = new HashMap<>();
            staticMap.put("JAN", "01");
            staticMap.put("FEB", "02");
            staticMap.put("MAR", "03");
            staticMap.put("APR", "04");
            staticMap.put("MAY", "05");
            staticMap.put("JUN", "06");
            staticMap.put("JUL", "07");
            staticMap.put("AUG", "08");
            staticMap.put("SEP", "09");
            staticMap.put("OCT", "10");
            staticMap.put("NOV", "11");
            staticMap.put("DEC", "12");
            MMMToNumber = Collections.unmodifiableMap(staticMap);
        };
        
        /* MONTHS */
        public static final Map<String, Integer> MMMToMMNumber;
            /* Convert MMM to sortable string:  JUN = 06 */
            static {
                Map<String, Integer> staticMap = new HashMap<>();
                staticMap.put("JAN", 4);
                staticMap.put("FEB", 5);
                staticMap.put("MAR", 6);
                staticMap.put("APR", 7);
                staticMap.put("MAY", 8);
                staticMap.put("JUN", 9);
                staticMap.put("JUL", 10);
                staticMap.put("AUG", 11);
                staticMap.put("SEP", 12);
                staticMap.put("OCT", 1);
                staticMap.put("NOV", 2);
                staticMap.put("DEC", 3);
                MMMToMMNumber = Collections.unmodifiableMap(staticMap);
            };
 
    public static final Map<String, String> MMMToMMMM;
        /* Convert MMM to MMMM:  JUN = June */
        static {
            Map<String, String> staticMap = new HashMap<>();
            staticMap.put("JAN", "January");
            staticMap.put("FEB", "February");
            staticMap.put("MAR", "March");
            staticMap.put("APR", "April");
            staticMap.put("MAY", "May");
            staticMap.put("JUN", "June");
            staticMap.put("JUL", "July");
            staticMap.put("AUG", "August");
            staticMap.put("SEP", "September");
            staticMap.put("OCT", "October");
            staticMap.put("NOV", "November");
            staticMap.put("DEC", "December");
            MMMToMMMM = Collections.unmodifiableMap(staticMap);
        };

        /* MONTHS */
        public static final Map<String, String> MMMMToNumber;
        static {
            Map<String, String> staticMap = new HashMap<>();
            staticMap.put("January", "04");
            staticMap.put("February", "05");
            staticMap.put("March", "06");
            staticMap.put("April", "07");
            staticMap.put("May", "08");
            staticMap.put("June", "09");
            staticMap.put("July", "10");
            staticMap.put("August", "11");
            staticMap.put("September", "12");
            staticMap.put("October", "01");
            staticMap.put("November", "02");
            staticMap.put("December", "03");
            MMMMToNumber = Collections.unmodifiableMap(staticMap);
        };
        
        public static final Map<Integer, ArrayList<String>> MMMByQuarter;
        static {
            Map<Integer, ArrayList<String>> staticMap = new HashMap<>();
        	ArrayList<String> q1 = new ArrayList<>();
        	q1.add("OCT");
        	q1.add("NOV");
        	q1.add("DEC");
        	staticMap.put(1, q1);
        	
        	ArrayList<String> q2 = new ArrayList<>();
        	q2.add("JAN");
        	q2.add("FEB");
        	q2.add("MAR");
        	staticMap.put(2, q2);
        	
        	ArrayList<String> q3 = new ArrayList<>();
        	q3.add("APR");
        	q3.add("MAY");
        	q3.add("JUN");
        	staticMap.put(3, q3);
        	
        	ArrayList<String> q4 = new ArrayList<>();
        	q4.add("JUL");
        	q4.add("AUG");
        	q4.add("SEP");
        	staticMap.put(4, q4);
        	
        	ArrayList<String> q5 = new ArrayList<>();
        	q5.addAll(q1);
        	q5.addAll(q2);
        	q5.addAll(q3);
        	q5.addAll(q4);
        	staticMap.put(5, q5);
        	
        	MMMByQuarter = Collections.unmodifiableMap(staticMap);
        }
        
        public static final Map<String, String> QtrToMM;
        static {
            Map<String, String> staticMap = new HashMap<>();
            staticMap.put("1", "12");
            staticMap.put("2", "03");
            staticMap.put("3", "06");
            staticMap.put("4", "09");
            
           
            QtrToMM = Collections.unmodifiableMap(staticMap);
        };
        
        public static final Map<String, String> MMToQtr;
        static {
            Map<String, String> staticMap = new HashMap<>();
            staticMap.put("10", "1");
            staticMap.put("11", "1");
            staticMap.put("12", "1");
            staticMap.put("01", "2");
            staticMap.put("02", "2");
            staticMap.put("03", "2");
            staticMap.put("04", "3");
            staticMap.put("05", "3");
            staticMap.put("06", "3");
            staticMap.put("07", "4");
            staticMap.put("08", "4");
            staticMap.put("09", "4");
            MMToQtr = Collections.unmodifiableMap(staticMap);
        };
        
        public static final Map<String, String> NNToMMM;
        static {
            Map<String, String> staticMap = new HashMap<>();
            staticMap.put("10","OCT");
            staticMap.put("11","NOV");
            staticMap.put("12","DEC");
            staticMap.put("01","JAN");
            staticMap.put("02","FEB");
            staticMap.put("03","MAR");
            staticMap.put("04","APR");
            staticMap.put("05","MAY");
            staticMap.put("06","JUN");
            staticMap.put("07","JUL");
            staticMap.put("08","AUG");
            staticMap.put("09","SEP");
            NNToMMM = Collections.unmodifiableMap(staticMap);
        };
        
        public static final String  IN_PROGRESS="InProgress";
        public static final String TTB_COLOUMN_TEMPLATES = "ttbColumnTemplates";
        public static final String CBP_COLOUMN_TEMPLATES = "cbpColumnTemplates";
        public static final String STATIC_TUFA_DATA_BEAN= "staticTufaDataBean";
        public static final String INGESTION_CONTEXT = "ingestionContext";
        public static final String INGESTION_WF_USER_CONTEXT = "ingestionWFUserContext";
        
        /**
         * Private constructor prevents instantiation. 
         */
        private Constants() {
            /*
             * Added private constructor to
             * prevent instantiation.
             * 
             */
        }          
}

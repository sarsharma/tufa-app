/**
 * 
 */
package gov.hhs.fda.ctp.common.util;

import java.text.DecimalFormat;

/**
 * @author tgunter
 *
 */
public class CsvStringBuilder {
	private StringBuilder csv = new StringBuilder();
	
	/**
	 * Append the value with no comma separator.
	 * @param rawText
	 */
	public void csvAppendRaw(String rawText) {
		if(rawText == null) csv.append("");
		else csv.append(rawText);
	}
	
	/**
	 * Append the formated value with no comma separator.
	 * @param rawText
	 */
	public <T> void csvAppendFormatRaw(String format, T rawValue) {
		if(rawValue == null) csv.append("");
		else {
			DecimalFormat formatter = new DecimalFormat(format);
			csv.append(formatter.format(rawValue));
		}
	}
	
	/**
	 * Append the value with comma separator.
	 * @param value
	 */
	public <T> void csvAppend(T value) {
		if(value == null) {
			csv.append(","); 
		}else {
			csv.append(value);
			csv.append(",");
		}
	}

	/**
	 * Append the value with a numeric format.
	 * @param format
	 * @param value
	 */
	public <T> void csvAppendFormat(String format, T value) {
		boolean inQuotes = false;
		if(format.contains(",") || format.contains("0")) {
			inQuotes = true;
		}
		if(value == null) csv.append(",");
		else {
			// "=""Data Here"""
			if(inQuotes) csv.append("\"=\"\"");
			DecimalFormat formatter = new DecimalFormat(format);
			csv.append(formatter.format(value));
			if(inQuotes) {
				csv.append("\"\"\",");
			}
			else {
				csv.append(",");
			}
		}
	}

	/**
	 * Append the value exactly as it appears, in quotes.
	 * Protects the value from excel auto-formatting.
	 * @param value
	 */
	public <T> void csvAppendRawQuotes(T value) {
		if(value == null){
			csv.append(",");
		}else{
			csv.append("\"=\"\"");
			csv.append(value);
			csv.append("\"\"\",");
		}
	}

	/**
	 * Append the value in quotes.
	 * Does not protect from excel auto-formatting.
	 * @param value
	 */
	public <T> void csvAppendInQuotes(T value) {
		csv.append("\"");
		if(value == null){
			csv.append("");
		}
		else {
			csv.append(value);
		}
		csv.append("\",");
	}

	/**
	 * Append value and move to the next line.
	 * @param value
	 */
	public <T> void csvAppendNewLine(T value) {
		if(value == null) csv.append("");
		else {
			csv.append(value);
		}
		csv.append(String.format("%n"));
	}

	/**
	 * Return the final result.
	 */
	public String toString() {
		return csv.toString();
	}

}

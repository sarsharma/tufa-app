/**
 * 
 */
package gov.hhs.fda.ctp.common.util;

/**
 * @author tgunter
 *
 */
public class EinBuilder {
	public static final int EIN_MAX_LENGTH = 10;
	StringBuilder einBuilder = null;

	public EinBuilder(String ein) {
		if(ein == null) ein = "";
		// truncate to max length
		int max = ein.length() > EIN_MAX_LENGTH ? EIN_MAX_LENGTH:ein.length();
		ein = ein.substring(0,max);
		// set string builder to ein
		einBuilder = new StringBuilder(ein);
	}
	
	public String getFormattedEin() {
		if(einBuilder == null) einBuilder = new StringBuilder("");
		if(einBuilder.length() > 2) {
				einBuilder.insert(2, "-");
		}
		return einBuilder.toString();
	}
	
	public String unformatEin() {
		if(einBuilder == null) einBuilder = new StringBuilder("");
		einBuilder = new StringBuilder(einBuilder.toString().replaceAll("-", ""));
		if(einBuilder.length() > 9) {
			einBuilder.delete(9, einBuilder.length());
		}
		return einBuilder.toString();
	}
	
	public String getRawData() {
		return einBuilder.toString();
	}

}
/**
 * 
 */
package gov.hhs.fda.ctp.common.util;

import gov.hhs.fda.ctp.common.beans.Assessment;
import gov.hhs.fda.ctp.common.beans.AssessmentVersion;
import gov.hhs.fda.ctp.common.beans.MarketShare;

/**
 * @author Anthony.Gunter
 *
 */
public class ExportAnnualBehavior implements Exportable {
	int year;
	int quarter;
	
	/**
	 * Constructor takes assessment.
	 * @param assessment
	 */
	public ExportAnnualBehavior(Assessment assessment) {
		year = assessment.getAssessmentYr();
		quarter = assessment.getAssessmentQtr();
	}

	@Override
	public String prependedVersionTitle() {
		return "_Annual_";
	}

	@Override
	public boolean isOriginal(AssessmentVersion version) {
		return false;
	}

	@Override
	public int getDisplayYear(AssessmentVersion version) {
		int retYear = year;
		// if cigar assessment, or last quarter
		// of other assessment, increment year
		if(quarter == 4 || quarter == 0){
				retYear = retYear + 1;
		}
		return retYear;
	}

	@Override
	public int getDisplayQuarter(AssessmentVersion version) {
		// assessment is always published for the quarter 
		// following this one.
		int retQuarter = quarter;
		switch(retQuarter){
		case 0:
			// cigar assessment
			retQuarter = 4;
			break;
		case 1:
		case 2:
		case 3:
			retQuarter = retQuarter + 1;
			break;
			
		case 4:
			retQuarter = 1;
			break;
			
			default:
				break;
		}
		return retQuarter;
	}

	@Override
	public Double getRemovalVolume(MarketShare marketShare) {
		if(Double.compare(0,marketShare.getTotVolRemoved()) == 0) return null;
		return "Cigars".equals(marketShare.getTobaccoClassNm())?marketShare.getTotVolRemoved()/4.0 : marketShare.getTotVolRemoved();
	}
	
	@Override
	public Double getTotalTaxes(MarketShare marketShare) {
		if(Double.compare(0, marketShare.getTotTaxesPaid())==0) return null;
		return "Cigars".equals(marketShare.getTobaccoClassNm())?marketShare.getTotTaxesPaid()/4.0 : marketShare.getTotTaxesPaid();
	}
	
	@Override
	public boolean displayAddressChange() {
		return true;
	}

}

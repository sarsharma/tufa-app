/**
 * 
 */
package gov.hhs.fda.ctp.common.util;

import gov.hhs.fda.ctp.common.beans.Assessment;
import gov.hhs.fda.ctp.common.beans.AssessmentVersion;
import gov.hhs.fda.ctp.common.beans.MarketShare;

/**
 * @author Anthony.Gunter
 *
 */
public class ExportAssessmentBehavior implements Exportable {
	int year;
	int quarter;
	
	/**
	 * Constructor takes assessment.
	 * @param assessment
	 */
	public ExportAssessmentBehavior(Assessment assessment) {
		year = assessment.getAssessmentYr();
		quarter = assessment.getAssessmentQtr();
	}
	
	@Override
	public String prependedVersionTitle() {
		return "_";
	}

	
	@Override
	public boolean isOriginal(AssessmentVersion version) {
		// original flag will normally come from the marketshare records
		// but in the case of migrated data, check for version 1
		// because Original_Corrected could be null instead of "O" or "C".
		return version.getVersionNum() == 1L;
	}

	@Override
	public int getDisplayYear(AssessmentVersion version) {
		// assessment is always published for the quarter 
		// following this one.
		int retYear = year;
		if(quarter == 4){
				retYear = retYear + 1;
		}
		return retYear;
	}

	@Override
	public int getDisplayQuarter(AssessmentVersion version) {
		// assessment is always published for the quarter 
		// following this one.
		int retQuarter = quarter;
		switch(retQuarter){
		case 1:
		case 2:
		case 3:
			retQuarter = retQuarter + 1;
			break;
			
		case 4:
			retQuarter = 1;
			break;
			
			default:
				break;
		}
		return retQuarter;
	}

	@Override
	public Double getRemovalVolume(MarketShare marketShare) {
		if(Double.compare(0,marketShare.getTotVolRemoved()) == 0) return null;
		return (marketShare.getTotVolRemoved()==0)?null:marketShare.getTotVolRemoved();
	}
	
	@Override
	public Double getTotalTaxes(MarketShare marketShare) {
		if(Double.compare(0,marketShare.getTotTaxesPaid()) == 0) return null;
		return (marketShare.getTotTaxesPaid()==0)?null:marketShare.getTotTaxesPaid();
	}
	
	@Override
	public boolean displayAddressChange() {
		return false;
	}

}

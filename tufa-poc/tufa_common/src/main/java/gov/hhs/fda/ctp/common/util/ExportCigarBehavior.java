/**
 * 
 */
package gov.hhs.fda.ctp.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import gov.hhs.fda.ctp.common.beans.Assessment;
import gov.hhs.fda.ctp.common.beans.AssessmentVersion;
import gov.hhs.fda.ctp.common.beans.MarketShare;

/**
 * @author Anthony.Gunter
 *
 */
public class ExportCigarBehavior implements Exportable {
	int year;
	List<TreeSet<AssessmentVersion>> versionSets = new ArrayList<TreeSet<AssessmentVersion>>();
	
	public ExportCigarBehavior(Assessment assessment) {
		year = assessment.getAssessmentYr();	
		List<AssessmentVersion> versions = assessment.getVersions();
		// create the sorted sets to hold the versions by quarter
		for(int i=0;i<5;i++) {
			versionSets.add(new TreeSet<AssessmentVersion>());
		}
		// loop through the versions and place them in the sorted sets
		for(AssessmentVersion version: versions) {
			TreeSet<AssessmentVersion> quarterSet = versionSets.get((int)version.getCigarQtr());
			if(quarterSet != null) {
				quarterSet.add(version);
			}
		}	
	}

	@Override
	public String prependedVersionTitle() {
		return "_";
	}

	@Override
	public boolean isOriginal(AssessmentVersion version) {
		// retrieve the sorted set by cigar quarter
		TreeSet<AssessmentVersion> versions = versionSets.get((int)version.getCigarQtr());
		// find the original version (should be last in the list)
		List<AssessmentVersion> versionList = new ArrayList<>(versions);
		AssessmentVersion firstVersion = versionList.get(versionList.size()-1);
		// return this version equals first version
		return version.getVersionNum() == firstVersion.getVersionNum();
	}
	
	@Override
	public int getDisplayYear(AssessmentVersion version) {
		return year+1;
	}

	@Override
	public int getDisplayQuarter(AssessmentVersion version) {
		return (int)version.getCigarQtr();
	}

	@Override
	public Double getRemovalVolume(MarketShare marketShare) {
		if(Double.compare(0,marketShare.getTotVolRemoved()) == 0) return null;

		// total is calculated for an entire year, but
		// displayed for just one quarter.
		return marketShare.getTotVolRemoved()/4.0;
	}
	
	@Override
	public Double getTotalTaxes(MarketShare marketShare) {
		if(Double.compare(0,marketShare.getTotTaxesPaid()) == 0) return null;

		// total is calculated for an entire year, but
		// displayed for just one quarter.
		return marketShare.getTotTaxesPaid()/4.0;
	}
	
	@Override
	public boolean displayAddressChange() {
		return true;
	}

	
}

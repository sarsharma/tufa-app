/**
 * 
 */
package gov.hhs.fda.ctp.common.util;

import gov.hhs.fda.ctp.common.beans.AssessmentVersion;
import gov.hhs.fda.ctp.common.beans.MarketShare;

/**
 * @author Anthony.Gunter
 *
 */
public interface Exportable {
	// behavior that differs across different assessment types.
	public boolean isOriginal(AssessmentVersion version);
	public int getDisplayYear(AssessmentVersion version);
	public int getDisplayQuarter(AssessmentVersion version);
	public Double getRemovalVolume(MarketShare marketShare);
	public Double getTotalTaxes(MarketShare marketShare);
	public String prependedVersionTitle();
	public boolean displayAddressChange();
}

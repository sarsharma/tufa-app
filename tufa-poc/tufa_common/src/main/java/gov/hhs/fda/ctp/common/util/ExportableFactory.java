/**
 * 
 */
package gov.hhs.fda.ctp.common.util;
import gov.hhs.fda.ctp.common.beans.Assessment;
/**
 * @author Anthony.Gunter
 *
 */
public class ExportableFactory {
	public static Exportable getExportBehavior(Assessment assessment) {
		if("CIQT".equals(assessment.getAssessmentType())) {
			return new ExportCigarBehavior(assessment);
		}
		if("ANNU".equals(assessment.getAssessmentType())) {
			return new ExportAnnualBehavior(assessment);
		}
		return new ExportAssessmentBehavior(assessment);
	}
}
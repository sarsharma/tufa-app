/**
 * 
 */
package gov.hhs.fda.ctp.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gov.hhs.fda.ctp.common.beans.AssessmentVersion;
import gov.hhs.fda.ctp.common.beans.MarketShare;

/**
 * @author Anthony.Gunter
 *
 */
public class MarketShareMapFactory {
	/**
	 * Aggregate the marketshares into separate tobacco types.
	 * @param version
	 */
	public Map<String, List<MarketShare>> getMarketShareMap(AssessmentVersion version) {
		Map<String, List<MarketShare>> marketShareMap = new HashMap<>();

		List<MarketShare> marketShares = version.getMarketShares();
		for(MarketShare marketShare : marketShares) {
			if(marketShare.getDeltaShare() != null) {
				
				// this version includes deltas
				version.setIncludeDeltas(true);
			}
			List<MarketShare> mapEntry = marketShareMap.get(marketShare.getTobaccoClassNm());
			if(mapEntry == null) {
				mapEntry = new ArrayList<>();
				marketShareMap.put(marketShare.getTobaccoClassNm(), mapEntry);
			}
			mapEntry.add(marketShare);
		}
		return marketShareMap;
	}
		
}


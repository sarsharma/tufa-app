/**
 * 
 */
package gov.hhs.fda.ctp.common.util;

/**
 * @author tgunter
 *
 */
public class PermitBuilder {
	StringBuilder permitBuilder = null;

	public PermitBuilder(String permitNum) {
		if(permitNum == null) permitNum = "";
		permitBuilder = new StringBuilder(permitNum);
	}
	
	public String formatPermit() {
		if(permitBuilder == null) permitBuilder = new StringBuilder("");
		permitBuilder = new StringBuilder(unformatPermit());
		if(permitBuilder.length() > 2) {
			permitBuilder.insert(2, "-");
		}
		if(permitBuilder.length() > 5) {
			permitBuilder.insert(5, "-");
		}
		return permitBuilder.toString();
	}

	public String unformatPermit() {
		if(permitBuilder == null) permitBuilder = new StringBuilder("");
		permitBuilder = new StringBuilder(permitBuilder.toString().replaceAll("-", "").toUpperCase());
		return permitBuilder.toString();
	}

}

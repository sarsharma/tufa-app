/**
 * 
 */
package gov.hhs.fda.ctp.common.util;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import gov.hhs.fda.ctp.common.beans.TobaccoClass;

/**
 * @author Anthony.Gunter
 *
 */
public class TobaccoClassFacade {
	
	private NavigableMap<Date, Map<String, TobaccoClass>> classesByDateRange = new TreeMap<Date, Map<String, TobaccoClass>>();
	
	public TobaccoClassFacade(List<TobaccoClass> allClasses) {
		for(TobaccoClass tobacco : allClasses) {
			Map<String, TobaccoClass> classesForRange = classesByDateRange.get(tobacco.getTaxRateEndDate());
			if(classesForRange == null) {
				classesForRange = new TreeMap<String, TobaccoClass>();
			}
			classesForRange.put(tobacco.getTobaccoClassNm(), tobacco);
			classesByDateRange.put(tobacco.getTaxRateEndDate(), classesForRange);
		}
	}
	
	public TobaccoClass getClass(Date date, String className) {
		TobaccoClass returnClass = new TobaccoClass();
		Map<String, TobaccoClass> classMap = null;
		Map.Entry<Date, Map<String, TobaccoClass>>ceilingEntry = classesByDateRange.ceilingEntry(date);
		if(ceilingEntry!=null) {
			classMap = ceilingEntry.getValue();
			returnClass = classMap.get(className);
		}
		return returnClass;
	}

}

/**
* 
 */
package gov.hhs.fda.ctp.common.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import gov.hhs.fda.ctp.common.beans.CBPUpload;
import gov.hhs.fda.ctp.common.util.CTPUtil;

/**
* @author rnasina
*
*/
public class EntryDateMatchValidator implements ConstraintValidator<EntryDateMatch, CBPUpload> {
    @Override
    public void initialize (EntryDateMatch constraintAnnotation) {
    }

    @Override
    public boolean isValid (CBPUpload cbp,
                                   ConstraintValidatorContext context) {
    	try {
			String periodString = cbp.getEntrySummDate().isEmpty() ? cbp.getEntrySummDate() : cbp.getEntryDate();
			if (periodString == null) return true; // let the @NotNull validator catch this one later.

			//DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
			//DateTime dttime = formatter.parseDateTime(periodString);

			String yearString = CTPUtil.stripNonDigits(cbp.getFiscalYr().toString());
			int yearLen = yearString.length();
			if (yearLen < 2) return false;

			int twoDigitPeriod = Integer.parseInt(periodString.substring(4,6));
			int twoDigitYear = Integer.parseInt(periodString.substring(2,4));
			if (twoDigitPeriod > 9)
				++twoDigitYear;

			String twoDigitYrStr = new Integer(twoDigitYear).toString();
			return twoDigitYrStr.equalsIgnoreCase(yearString.substring(2));

		} catch (Exception e) {
			// something is wrong with the format,
			return false;
		}
    }
}

/**
 * 
 */
package gov.hhs.fda.ctp.common.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.joda.time.DateTime;

import gov.hhs.fda.ctp.common.util.CTPUtil;

/**
 * @author tgunter
 *
 */
public class NoFutureDatesValidator implements ConstraintValidator<NoFutureDates, String> {

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
     */
    @Override
    public void initialize(NoFutureDates constraintAnnotation) {
        // empty initializer
    }

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(String datetime, ConstraintValidatorContext context) {
    	boolean result = false;
        if (datetime == null || datetime.isEmpty()) {
        	// null date, valid = true
        	result = true;
        }
        else {
	        try {
		        DateTime incomingDate = new DateTime(CTPUtil.parseStringToDate(datetime));
		        DateTime tomorrow = new DateTime().plusDays(1);
		        if (incomingDate.isBefore(tomorrow)) {
		            result = true;
		        }
	        }
	        catch(Exception e) {
	        	// date fails parsing, fail validation
	        	result = false;
	        }
        }
        return result;
    }
}

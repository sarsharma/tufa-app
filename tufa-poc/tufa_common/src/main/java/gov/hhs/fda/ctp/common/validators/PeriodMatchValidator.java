/**
* 
 */
package gov.hhs.fda.ctp.common.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import gov.hhs.fda.ctp.common.beans.TTBRemovalsUpload;
import gov.hhs.fda.ctp.common.util.CTPUtil;

/**
 * @author tgunter
 *
 */
public class PeriodMatchValidator implements ConstraintValidator<PeriodMatch, TTBRemovalsUpload> {
	@Override
	public void initialize(PeriodMatch constraintAnnotation) {
	}

	@Override
	public boolean isValid(TTBRemovalsUpload removal, ConstraintValidatorContext context) {

		try {
			String periodString = removal.getPeriod();
			if (periodString == null) return true; // let the @NotNull validator catch this one later.

			DateTimeFormatter formatter = DateTimeFormat.forPattern("MMM-yy");
			DateTime dttime = formatter.parseDateTime(removal.getPeriod());

			String yearString = CTPUtil.stripNonDigits(removal.getFiscalYr().toString());
			int yearLen = yearString.length();
			if (yearLen < 2) return false;

			int twoDigitPeriod = dttime.getMonthOfYear();
			int fourDigitYear = dttime.getYear();
			if (twoDigitPeriod > 9)
				++fourDigitYear;

			String fourDigitYrStr = new Integer(fourDigitYear).toString();
			
			return fourDigitYrStr.equalsIgnoreCase(yearString);

		} catch (Exception e) {
			// something is wrong with the format,
			return false;
		}

	}
}

package gov.hhs.fda.ctp.common.validators;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import gov.hhs.fda.ctp.common.beans.StaticTufaDataBean;
import gov.hhs.fda.ctp.common.util.ApplicationContextUtil;

public class StateCodeMatchValidator implements ConstraintValidator<StateCodeMatch, String> {

	@Override
	public void initialize(StateCodeMatch constraintAnnotation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		StaticTufaDataBean staticTufaDataBean = (StaticTufaDataBean) ApplicationContextUtil.getApplicationContext()
		.getBean("staticTufaDataBean");
		List<String> stateCds = staticTufaDataBean.getStateCodes();
		boolean retBolean = false;
		if(StringUtils.isBlank(value) || stateCds.contains(value.toUpperCase())){
			retBolean = true;
		}
		return retBolean;
	}

}

/**
* 
 */
package gov.hhs.fda.ctp.common.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import gov.hhs.fda.ctp.common.beans.TTBUpload;
import gov.hhs.fda.ctp.common.util.CTPUtil;

/**
* @author rnasina
*
*/
public class TPBDMatchValidator implements ConstraintValidator<TPBDMatch, TTBUpload> {
    @Override
    public void initialize (TPBDMatch constraintAnnotation) {
    }

    @Override
    public boolean isValid (TTBUpload tax,
                                   ConstraintValidatorContext context) {
    	try {
			String periodString = tax.getTpbd();
			if (periodString == null) return true; // let the @NotNull validator catch this one later.

			//DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
			//DateTime dttime = formatter.parseDateTime(tax.getTpbd());

			String yearString = CTPUtil.stripNonDigits(tax.getFiscalYr().toString());
			int yearLen = yearString.length();
			if (yearLen < 2) return false;

			int twoDigitPeriod = Integer.parseInt(periodString.substring(0, 2));
			int twoDigitYear = Integer.parseInt(periodString.substring(6, 8));
			if (twoDigitPeriod > 9)
				++twoDigitYear;

			String twoDigitYrStr = new Integer(twoDigitYear).toString();
			return twoDigitYrStr.equalsIgnoreCase(yearString.substring(2));

		} catch (Exception e) {
			// something is wrong with the format,
			return false;
		}
    }
}

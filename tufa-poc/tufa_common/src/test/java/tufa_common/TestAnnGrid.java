package tufa_common;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import gov.hhs.fda.ctp.common.beans.AnnImporterGrid;
import gov.hhs.fda.ctp.common.beans.AnnImporterGridRecord;

@ActiveProfiles("commontest")
public class TestAnnGrid {

	private AnnImporterGrid grid;
	
	@Test
	public void testGridTotals() {
		grid = new AnnImporterGrid();
		
		List<AnnImporterGridRecord> months = new ArrayList<>();
		for(int i = 0; i < 3; i++) {
			AnnImporterGridRecord record = new AnnImporterGridRecord();
			
			record.setTufaValue(new Double(1));
			record.setDetailValue(new Double(2));
			record.setSummaryValue(new Double(3));
			
			months.add(record);
		}
		grid.setSummaryMonths(months);
		
		assert(grid.getSummaryTotal() != null);
		assert((new Double(9)).equals(grid.getSummaryTotal().getSummaryValue()));
		assert((new Double(6)).equals(grid.getSummaryTotal().getDetailValue()));
		assert((new Double(3)).equals(grid.getSummaryTotal().getTufaValue()));
	}
	
	@Test
	public void testGridQuarterTotals() {
		grid = new AnnImporterGrid();
		
		List<AnnImporterGridRecord> months = new ArrayList<>();
		for(int i = 0; i < 12; i++) {
			AnnImporterGridRecord record = new AnnImporterGridRecord();
			
			record.setTufaValue(new Double(1));
			record.setDetailValue(new Double(2));
			record.setSummaryValue(new Double(3));
			
			months.add(record);
		}
		grid.setSummaryMonths(months);
		
		assert(grid.getSummaryTotal() != null);
		assert(grid.getSummaryQuarterTotals() != null);
		assert(grid.getSummaryQuarterTotals().size() == 4);
//		assert((new Double(9)).equals(grid.getSummaryTotal().getSummaryValue()));
//		assert((new Double(6)).equals(grid.getSummaryTotal().getDetailValue()));
//		assert((new Double(3)).equals(grid.getSummaryTotal().getTufaValue()));
	}
}

package tufa_common;

import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import gov.hhs.fda.ctp.common.beans.AnnImporterGridRecord;

@ActiveProfiles("commontest")
public class TestAnnGridRecord {
	
	private AnnImporterGridRecord record;
	
	@Test
	public void testDifference() {
		record = new AnnImporterGridRecord();
		
		int tufa = 1;
		int summary = 1;
		int detail = 1;
		
		record.setTufaValue(new Double(tufa));
		record.setDetailValue(new Double(detail));
		record.setSummaryValue(new Double(summary));
		
		assert((new Double(summary - detail)).equals(record.getSummaryAndDetailDiff()));
		assert((new Double(tufa - detail)).equals(record.getTufaAndDetailDiff()));
		assert((new Double(tufa - summary)).equals(record.getTufaAndSummaryDiff()));
	}
	
	@Test
	public void testDifferenceWithNulls() {
		record = new AnnImporterGridRecord();
		
		int tufa = 1;
		int summary = 1;
		int detail = 1;
		
		record.setTufaValue(null);
		record.setDetailValue(new Double(detail));
		record.setSummaryValue(new Double(summary));
		
		assert((new Double(summary - detail)).equals(record.getSummaryAndDetailDiff()));
		assert((new Double(0 - detail)).equals(record.getTufaAndDetailDiff()));
		assert((new Double(0 - summary)).equals(record.getTufaAndSummaryDiff()));
		

		record.setTufaValue(new Double(tufa));
		record.setDetailValue(null);
		record.setSummaryValue(new Double(summary));
		
		assert((new Double(summary - 0)).equals(record.getSummaryAndDetailDiff()));
		assert((new Double(tufa - 0)).equals(record.getTufaAndDetailDiff()));
		assert((new Double(tufa - summary)).equals(record.getTufaAndSummaryDiff()));
		

		record.setTufaValue(new Double(tufa));
		record.setDetailValue(null);
		record.setSummaryValue(null);
		
		assert(null == record.getSummaryAndDetailDiff());
		assert((new Double(tufa - 0)).equals(record.getTufaAndDetailDiff()));
		assert((new Double(tufa - 0)).equals(record.getTufaAndSummaryDiff()));
	}
	
	@Test
	public void testAddition() {
		record = new AnnImporterGridRecord();
		AnnImporterGridRecord secondRecord = new AnnImporterGridRecord();

		int tufa = 1;
		int summary = 1;
		int detail = 1;
		
		record.setTufaValue(new Double(tufa));
		record.setDetailValue(new Double(detail));
		record.setSummaryValue(new Double(summary));

		secondRecord.setTufaValue(new Double(tufa));
		secondRecord.setDetailValue(new Double(detail));
		secondRecord.setSummaryValue(new Double(summary));
		
		record.addRecord(secondRecord);
		
		assert((new Double(summary + summary)).equals(record.getSummaryValue()));
		assert((new Double(detail + detail)).equals(record.getDetailValue()));
		assert((new Double(tufa + tufa)).equals(record.getDetailValue()));
	}

	@Test
	public void testAdditionWithNulls() {
		record = new AnnImporterGridRecord();
		AnnImporterGridRecord secondRecord = new AnnImporterGridRecord();

		int tufa = 1;
		int summary = 1;
		int detail = 1;
		
		record.setTufaValue(null);
		record.setDetailValue(new Double(detail));
		record.setSummaryValue(null);

		secondRecord.setTufaValue(new Double(tufa));
		secondRecord.setDetailValue(null);
		secondRecord.setSummaryValue(null);
		
		record.addRecord(secondRecord);
		
		assert(null == record.getSummaryValue());
		assert((new Double(detail)).equals(record.getDetailValue()));
		assert((new Double(tufa)).equals(record.getDetailValue()));
	}
}

/**
 *
 */
package tufa_common;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import gov.hhs.fda.ctp.common.beans.BaseTufaBean;
import gov.hhs.fda.ctp.common.beans.CBPUpload;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.CompanyPermitHistory;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.TTBUpload;
import gov.hhs.fda.ctp.common.exception.CreateCompanyException;
import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.common.util.EinBuilder;
import gov.hhs.fda.ctp.common.util.PermitBuilder;


/**
 * @author tgunter
 *
 */
@ActiveProfiles("commontest")
public class TestCommon {

    @Test
    public void testDate() {
        /* test for empty dates */
        Date date = CTPUtil.parseStringToDate(Constants.EMPTY);
        Assert.assertNull(date);
        String emptyDate = CTPUtil.parseDateToString(date);
        Assert.assertTrue(emptyDate.equals(Constants.EMPTY));

        String incomingDate = "Mon Oct 17 2016 12:02:27 GMT-0400 (Eastern Daylight Time)";
        String otherIncomingDate = "2016-10-17T04:00:00.000Z";
        String outgoingDate = "10/17/2016";
        /* parse first format */
        date = CTPUtil.parseStringToDate(incomingDate);
        String dateString = CTPUtil.parseDateToString(date);
        Assert.assertTrue(dateString.equals(outgoingDate));
        /* parse second format */
        date = CTPUtil.parseStringToDate(otherIncomingDate);
        dateString = CTPUtil.parseDateToString(date);
        Assert.assertTrue(dateString.equals(outgoingDate));
        /* parse third format */
        date = CTPUtil.parseStringToDate(outgoingDate);
        dateString = CTPUtil.parseDateToString(date);
        Assert.assertTrue(dateString.equals(outgoingDate));

        /* try a bad date 
        try {
            date = CTPUtil.parseStringToDate("26/10/2016");
            Assert.fail();
        }
        catch(Exception e) {
             
        }*/
    }

    @Test
    public void testFiscalYear() {
    	String mmddyyyy = "10/05/2016";
    	long fiscalYr = CTPUtil.getFiscalYearMMDDYYYY(mmddyyyy);
    	assert(fiscalYr == 2017);

    	String mmddyy = "10/05/16";
    	fiscalYr = CTPUtil.getFiscalYearMMDDYYYY(mmddyy);
    	assert(fiscalYr == 17);

    	String yyyymmdd = "20161005";
    	fiscalYr = CTPUtil.getFiscalYearYYYYMMDD(yyyymmdd);
    	assert(fiscalYr == 2017);
    	
    	long fiscalYear = 2017;
    	assert("17".equals(CTPUtil.twoDigitYear(fiscalYear)));

    }
/*
    @Test
    public void testEin() {
    	EinBuilder firstBuilder = new EinBuilder("123456789");
    	String formatted = firstBuilder.getFormattedEin();
    	assert("12-3456789".equals(formatted));
    	EinBuilder secondBuilder = new EinBuilder(formatted);
    	String unformatted = secondBuilder.unformatEin();
    	assert("123456789".equals(unformatted));
    	// verify that unformatting an already unformatted ein will do nothing
    	EinBuilder thirdBuilder = new EinBuilder(unformatted);
    	String stillUnformatted = thirdBuilder.unformatEin();
    	assert("123456789".equals(stillUnformatted));
    	// test to see if einbuilder will remove dashes from SSN
    	EinBuilder fourthBuilder = new EinBuilder("428-95-7234");
    	String ssn = fourthBuilder.unformatEin();
    	assert("428957234".equals(ssn));
    	EinBuilder fifthBuilder = new EinBuilder("12-345678900000");
    	unformatted = fifthBuilder.unformatEin();
    	assert("123456789".equals(unformatted));
    	EinBuilder sixthBuilder = new EinBuilder("12-3456789UPS");
    	unformatted = sixthBuilder.getRawData();
    	assert("12-3456789".equals(unformatted));
    }*/

    @Test
    public void testPermitBuilder() {
    	PermitBuilder permitBuilder = new PermitBuilder("TT-BB-12345");
    	String permitNum = permitBuilder.unformatPermit();
    	assert("TTBB12345".equals(permitNum));
    	permitBuilder = new PermitBuilder(permitNum);
    	permitNum = permitBuilder.formatPermit();
    	assert("TT-BB-12345".equals(permitNum));
    }

    @Test
    public void testValidate() {
    	ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    	Validator validator = factory.getValidator();
    	CBPUpload cbp = new CBPUpload();
    	cbp.setConsigneeEIN("12-34-56-78-90");
    	Set<ConstraintViolation<CBPUpload>> violations = validator.validate(cbp);
    	assert(violations.size() > 0);

    	TTBUpload ttb = new TTBUpload();
    	ttb.setEinNum("12-34-56-78-90");
    	Set<ConstraintViolation<TTBUpload>> ttbViolations = validator.validate(ttb);
    	assert(ttbViolations.size()>0);
    }

    @Test
    public void testBeans() {
        final String TESTDATE = new String("12/31/2015");
        final String TESTNAME = new String("TEST");
        final String TESTEIN = new String("061234567");
        final long TESTLONG = 1;

        /* base tufa bean */
        BaseTufaBean base = new BaseTufaBean();
        base.setCreatedBy(TESTNAME);
        base.setCreatedDt(TESTDATE);
        base.setModifiedBy(TESTNAME);
        base.setModifiedDt(TESTDATE);
        /* test the values */
        Assert.assertTrue(TESTNAME.equals(base.getCreatedBy()));
        Assert.assertTrue(TESTNAME.equals(base.getModifiedBy()));
        Assert.assertTrue(TESTDATE.equals(base.getCreatedDt()));
        Assert.assertTrue(TESTDATE.equals(base.getModifiedDt()));

        /* Company bean */
        Company company = new Company();
        company.setActiveDate(TESTDATE);
        company.setCompanyId(TESTLONG);
        company.setEinNumber(TESTEIN);
        company.setLegalName(TESTNAME);
        /* test the values */
        Assert.assertTrue(TESTDATE.equals(company.getActiveDate()));
        Assert.assertTrue(TESTLONG == company.getCompanyId());
        Assert.assertTrue(TESTEIN.equals(company.getEinNumber()));
        Assert.assertTrue(TESTNAME.equals(company.getLegalName()));

        /* Permit bean */
        Permit permit = new Permit();
        permit.setCloseDt(TESTDATE);
        permit.setCompany(company);
        permit.setCompanyId(TESTLONG);
        permit.setIssueDt(TESTDATE);
        permit.setPermitId(TESTLONG);
        permit.setPermitNum(TESTNAME);
        permit.setPermitStatusTypeCd(TESTNAME);
        permit.setPermitTypeCd(TESTNAME);
        /* test the values */
        Assert.assertTrue(TESTDATE.equals(permit.getCloseDt()));
        Assert.assertEquals(company, permit.getCompany());
        Assert.assertTrue(TESTLONG == permit.getCompanyId());
        Assert.assertTrue(TESTDATE.equals(permit.getIssueDt()));
        Assert.assertTrue(TESTLONG == permit.getPermitId());
        Assert.assertTrue(TESTNAME.equals(permit.getPermitNum()));
        Assert.assertTrue(TESTNAME.equals(permit.getPermitStatusTypeCd()));
        Assert.assertTrue(TESTNAME.equals(permit.getPermitTypeCd()));

        /* Permit History bean */
        CompanyPermitHistory permitHistory = new CompanyPermitHistory();
        permitHistory.setActivity(TESTNAME);
//        permitHistory.setAttachments(TESTNAME);
//        permitHistory.setAttachmentsId(TESTLONG);
        permitHistory.setDisplayMonthYear(TESTNAME);
        permitHistory.setMonth(TESTNAME);
        permitHistory.setPermitNum(TESTNAME);
        permitHistory.setReportPeriodId(TESTLONG);
        permitHistory.setSortMonthYear("20151231");
        permitHistory.setReportStatusCd(TESTNAME);
        permitHistory.setYear(TESTNAME);
        /* create history for compareTo test */
        CompanyPermitHistory earlierHistory = new CompanyPermitHistory();
        earlierHistory.setSortMonthYear("20150615");
        earlierHistory.setReportStatusCd(TESTNAME);
        /* test values */
        Assert.assertTrue(TESTNAME.equals(permitHistory.getActivity()));
//        Assert.assertTrue(TESTNAME.equals(permitHistory.getAttachments()));
        Assert.assertTrue(TESTNAME.equals(permitHistory.getDisplayMonthYear()));
        Assert.assertTrue(TESTNAME.equals(permitHistory.getMonth()));
        Assert.assertTrue(TESTNAME.equals(permitHistory.getPermitNum()));
        Assert.assertTrue("20151231".equals(permitHistory.getSortMonthYear()));
        Assert.assertTrue(TESTNAME.equals(permitHistory.getReportStatusCd()));
        Assert.assertTrue(TESTNAME.equals(permitHistory.getYear()));
//        Assert.assertTrue(TESTLONG == permitHistory.getAttachmentsId());
        Assert.assertTrue(TESTLONG == permitHistory.getReportPeriodId());
        Assert.assertTrue(permitHistory.hashCode() == Objects.hash(permitHistory.getPermitNum()));
        //Assert.assertTrue(permitHistory.compareTo(earlierHistory) == 0);
        Assert.assertTrue(permitHistory.equals(permitHistory));

//        /* Company Permit History bean */
//        CompanyPermitHistory permitHistory = new CompanyPermitHistory(permitHistory);
        permitHistory.setCompanyName(TESTNAME);
        /* create history for compareTo test */
        CompanyPermitHistory earlierCompany = new CompanyPermitHistory();
        earlierCompany.setSortMonthYear("20150615");
        earlierCompany.setReportStatusCd(TESTNAME);

        /* test values */
        Assert.assertTrue(TESTNAME.equals(permitHistory.getCompanyName()));
        Assert.assertTrue(TESTNAME.equals(permitHistory.getActivity()));
        Assert.assertTrue(TESTNAME.equals(permitHistory.getDisplayMonthYear()));
        Assert.assertTrue(TESTNAME.equals(permitHistory.getMonth()));
        Assert.assertTrue(TESTNAME.equals(permitHistory.getPermitNum()));
        Assert.assertTrue("20151231".equals(permitHistory.getSortMonthYear()));
        Assert.assertTrue(TESTNAME.equals(permitHistory.getReportStatusCd()));
        Assert.assertTrue(TESTNAME.equals(permitHistory.getYear()));
        Assert.assertTrue(TESTLONG == permitHistory.getReportPeriodId());
        Assert.assertTrue(permitHistory.hashCode() == Objects.hash(permitHistory.getPermitNum()));
        //Assert.assertTrue(permitHistory.compareTo(earlierCompany) == 0);
        Assert.assertTrue(permitHistory.equals(permitHistory));

        /* lists / sets */
        List<CompanyPermitHistory> companyHistoryList = new ArrayList<>();
        companyHistoryList.add(permitHistory);
        List<CompanyPermitHistory> permitHistoryList = new ArrayList<>();
        permitHistoryList.add(permitHistory);
        permitHistoryList.add(earlierHistory);
        permit.setPermitHistoryList(permitHistoryList);
        Set<Permit> permitSet = new HashSet<>();
        permitSet.add(permit);
        company.setPermitSet(permitSet);
        company.setPermitHistoryList(companyHistoryList);

        Assert.assertEquals(company.getPermitSet().size(), 1);
        Assert.assertEquals(company.getPermitHistoryList().size(), 1);
        Assert.assertEquals(permit.getPermitHistoryList().size(), 2);

        /**************************
         * Test exception methods
         **************************/
        // empty constructor
        try {
            throw new TufaException();
        }
        catch(Exception e) {
            Assert.assertTrue(e instanceof TufaException);
        }

        // message constructor
        try {
            throw new TufaException("TESTING");
        }
        catch (Exception e) {
            Assert.assertTrue(e instanceof TufaException);
        }

        // throwable constructor
        try {
            throw new TufaException(new RuntimeException("TESTING"));
        }
        catch (Exception e) {
            Assert.assertTrue(e instanceof TufaException);
        }

        // message and throwable constructor
        try {
            throw new TufaException("TESTING", new RuntimeException("TESTING"));
        }
        catch(Exception e) {
            Assert.assertTrue(e instanceof TufaException);
        }

        // message and throwable and boolean enableSuppression and boolean writableStackTrace
        try {
            throw new TufaException("TESTING", new RuntimeException("TESTING"), false, true);
        }
        catch(Exception e) {
            Assert.assertTrue(e instanceof TufaException);
        }

        // Displays generic error message back to user.
        TufaException ex = new TufaException("GENERIC ERROR");
        Assert.assertTrue(ex.getDisplayMessage().contains("Error performing operation"));

        // CreateCompanyException displays custom message back to user when EIN is in the original message.
        Throwable t = new TufaException(new RuntimeException("EIN"));
        ex =  new CreateCompanyException(t);
        Assert.assertTrue(ex.getDisplayMessage().contains("EIN"));


        /**********************
         * Test the Constants
         **********************/
        Assert.assertTrue(Constants.MMMToMMMM.get("JUL").equals("July"));
        Assert.assertTrue(Constants.MMMToNumber.get("JUL").equals("07"));


    }
}

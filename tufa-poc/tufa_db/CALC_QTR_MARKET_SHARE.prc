create or replace
PROCEDURE calc_qtr_market_share (assess_yr NUMBER, assess_qtr NUMBER) AS
BEGIN


DECLARE

  v_period_tot_pipe_taxes NUMBER(14,2) := 0;
  v_period_tot_chew_taxes NUMBER(14,2) := 0;
  v_period_tot_snuff_taxes NUMBER(14,2) := 0;
  v_period_tot_cigarette_taxes NUMBER(14,2) := 0;
  v_period_tot_ryo_taxes NUMBER(14,2) := 0;
  v_total_volume NUMBER(20,6) := 0;
  v_assessment_id NUMBER := 0;
  v_year NUMBER(4) := assess_yr;
  v_qtr NUMBER(1) := assess_qtr;
  v_submitind CHAR(1);
  v_version_num NUMBER(2) := '1';
  v_cigarette_rank NUMBER(4) := 0;
  v_chew_rank NUMBER(4) := 0;
  v_ryo_rank NUMBER(4) := 0;
  v_snuff_rank NUMBER(4) := 0;
  v_pipe_rank NUMBER(4) := 0;

  CURSOR calc_marketshare IS

    SELECT company_id, tobacco_class_id,
                 tobacco_class_nm, SUM(TOTTAX) as TOTTAX
    FROM (
        select a.company_id, h.tobacco_class_id,
               tobacco_class_nm, SUM(taxes_paid) as TOTTAX
        from tu_company a,
                 tu_permit b,
                 tu_permit_period c,
                 tu_rpt_period d,
                 tu_submitted_form f,
                 tu_form_detail g,
                 tu_tobacco_class h
         where a.company_id=b.company_id
           and B.PERMIT_ID=C.PERMIT_ID
           and D.PERIOD_ID=C.PERIOD_ID
           and F.PERMIT_ID=C.PERMIT_ID
           and F.PERIOD_ID=C.PERIOD_ID
           and G.FORM_ID=F.FORM_ID
           and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID
           and h.class_type_cd <> 'SPTP'
           and h.tobacco_class_nm <> 'Cigars'
           and a.company_status='ACTV'
           and b.permit_status_type_cd='ACTV'
           and f.form_type_cd='3852'
           and D.fiscal_yr=v_year
           and D.QUARTER=v_qtr
          group by a.company_id, h.tobacco_class_id,
                 tobacco_class_nm
        UNION
        select a.company_id, f.tobacco_class_id,
               f.tobacco_class_nm, 0 as TOTTAX
        from tu_company a,
                 tu_permit b,
                 tu_permit_period c,
                 tu_rpt_period d,
                 tu_assessment e,
                 tu_tobacco_class f
        where a.company_id=b.company_id
           and B.PERMIT_ID=C.PERMIT_ID
           and c.period_id=d.period_id
           and (d.fiscal_yr = e.assessment_yr AND d.quarter=e.assessment_qtr)
           and c.zero_flag='Y'
           and f.class_type_cd='TYPE'
           and d.fiscal_yr = v_year
           and d.quarter = v_qtr
           and (e.assessment_id || ' ' || a.company_id)   NOT IN
               (SELECT (e.assessment_id || ' ' || a.company_id) as nonzero
                from tu_company a,
                 tu_permit b,
                 tu_permit_period c,
                 tu_rpt_period d,
                 tu_assessment e
               where a.company_id=b.company_id
                 and B.PERMIT_ID=C.PERMIT_ID
                 and c.period_id=d.period_id
                 and (d.year = e.assessment_yr AND d.quarter=e.assessment_qtr)
                 and c.zero_flag='N'
                 GROUP BY e.assessment_id, a.company_id)
        group by a.company_id, f.tobacco_class_id, f.tobacco_class_nm
      ) allrecords
      group by company_id, tobacco_class_id,
                 tobacco_class_nm
      order by TOTTAX DESC;

BEGIN
--
  BEGIN
    SELECT assessment_id, assessment_yr, assessment_qtr,
           submitted_ind
      INTO v_assessment_id, v_year, v_qtr, v_submitind
    FROM tu_assessment
    WHERE assessment_yr = v_year
      and assessment_qtr = v_qtr;
    exception when no_data_found then v_assessment_id := '0';
  END;
--
  BEGIN
    IF v_submitind = 'N' then
      DELETE from tu_market_share
      WHERE assessment_id = v_assessment_id
        and version_num = '0';
      v_version_num := 0;
    ELSIF v_submitind = 'Y' then
      BEGIN
      SELECT max(version_num)+1 into v_version_num
        FROM tu_market_share
      WHERE assessment_id = v_assessment_id;
      EXCEPTION when no_data_found then v_version_num := 1;
      END;
      BEGIN
      DELETE from tu_market_share
      WHERE assessment_id = v_assessment_id
        and version_num = '0';
      EXCEPTION when no_data_found then NULL;
      END;
    END IF;
  END;
--
  BEGIN
    select sum(taxes_paid)
      into v_period_tot_pipe_taxes
    from  tu_company a,
             tu_permit b,
             tu_permit_period c,
             tu_rpt_period d,
             tu_submitted_form f,
             tu_form_detail g,
             tu_tobacco_class h
     where a.company_id=b.company_id
       and B.PERMIT_ID=C.PERMIT_ID
       and D.PERIOD_ID=C.PERIOD_ID
       and F.PERMIT_ID=C.PERMIT_ID
         and F.PERIOD_ID=C.PERIOD_ID
       and G.FORM_ID=F.FORM_ID
       and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID
       and F.FORM_TYPE_CD = '3852'
       and h.class_type_cd <> 'SPTP'
       and h.tobacco_class_nm = 'Pipe'
       and D.fiscal_yr=v_year
       and D.QUARTER=v_qtr;
  END;
--
  BEGIN
    select sum(taxes_paid)
      into v_period_tot_chew_taxes
    from  tu_company a,
             tu_permit b,
             tu_permit_period c,
             tu_rpt_period d,
             tu_submitted_form f,
             tu_form_detail g,
             tu_tobacco_class h
     where a.company_id=b.company_id
       and B.PERMIT_ID=C.PERMIT_ID
       and D.PERIOD_ID=C.PERIOD_ID
       and F.PERMIT_ID=C.PERMIT_ID
         and F.PERIOD_ID=C.PERIOD_ID
       and G.FORM_ID=F.FORM_ID
       and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID
       and F.FORM_TYPE_CD = '3852'
       and h.class_type_cd <> 'SPTP'
       and h.tobacco_class_nm = 'Chew'
       and D.fiscal_yr=v_year
       and D.QUARTER=v_qtr;
  END;
--
  BEGIN
    select sum(taxes_paid)
      into v_period_tot_snuff_taxes
    from  tu_company a,
             tu_permit b,
             tu_permit_period c,
             tu_rpt_period d,
             tu_submitted_form f,
             tu_form_detail g,
             tu_tobacco_class h
     where a.company_id=b.company_id
       and B.PERMIT_ID=C.PERMIT_ID
       and D.PERIOD_ID=C.PERIOD_ID
       and F.PERMIT_ID=C.PERMIT_ID
         and F.PERIOD_ID=C.PERIOD_ID
       and G.FORM_ID=F.FORM_ID
       and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID
       and F.FORM_TYPE_CD = '3852'
       and h.class_type_cd <> 'SPTP'
       and h.tobacco_class_nm = 'Snuff'
       and D.fiscal_yr=v_year
       and D.QUARTER=v_qtr;
  END;
--
  BEGIN
    select sum(taxes_paid)
      into v_period_tot_cigarette_taxes
    from  tu_company a,
             tu_permit b,
             tu_permit_period c,
             tu_rpt_period d,
             tu_submitted_form f,
             tu_form_detail g,
             tu_tobacco_class h
     where a.company_id=b.company_id
       and B.PERMIT_ID=C.PERMIT_ID
       and D.PERIOD_ID=C.PERIOD_ID
       and F.PERMIT_ID=C.PERMIT_ID
         and F.PERIOD_ID=C.PERIOD_ID
       and G.FORM_ID=F.FORM_ID
       and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID
       and F.FORM_TYPE_CD = '3852'
       and h.class_type_cd <> 'SPTP'
       and h.tobacco_class_nm = 'Cigarettes'
       and D.fiscal_yr=v_year
       and D.QUARTER=v_qtr;
  END;
--
  BEGIN
    select sum(taxes_paid)
      into v_period_tot_ryo_taxes
    from  tu_company a,
             tu_permit b,
             tu_permit_period c,
             tu_rpt_period d,
             tu_submitted_form f,
             tu_form_detail g,
             tu_tobacco_class h
     where a.company_id=b.company_id
       and B.PERMIT_ID=C.PERMIT_ID
       and D.PERIOD_ID=C.PERIOD_ID
       and F.PERMIT_ID=C.PERMIT_ID
         and F.PERIOD_ID=C.PERIOD_ID
       and G.FORM_ID=F.FORM_ID
       and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID
       and F.FORM_TYPE_CD = '3852'
       and h.class_type_cd <> 'SPTP'
       and h.tobacco_class_nm = 'Roll-Your-Own'
       and D.fiscal_yr=v_year
       and D.QUARTER=v_qtr;
  END;


--
  BEGIN
    For rec in calc_marketshare LOOP
--
    BEGIN

   select sum(removal_qty)
     into v_total_volume
    from  tu_company a,
             tu_permit b,
             tu_permit_period c,
             tu_rpt_period d,
             tu_submitted_form f,
             tu_form_detail g,
             tu_tobacco_class h
     where a.company_id=b.company_id
       and B.PERMIT_ID=C.PERMIT_ID
       and D.PERIOD_ID=C.PERIOD_ID
       and F.PERMIT_ID=C.PERMIT_ID
       and F.PERIOD_ID=C.PERIOD_ID
       and G.FORM_ID=F.FORM_ID
       and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID
       and F.FORM_TYPE_CD = '3852'
       and h.class_type_cd <> 'SPTP'
       and D.fiscal_yr=v_year
       and D.QUARTER=v_qtr
       and a.company_id=rec.company_id
      and h.tobacco_class_id=rec.tobacco_class_id;
    END;
    
    BEGIN
        -- insert the existing company addresses
      merge into tu_assessment_address rp
         using (select a.company_id, p.assessment_id, p.assessment_version, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd
                  from tu_address a, tu_assessment p
                  where a.company_id = rec.company_id
                  and p.assessment_id = v_assessment_id
                  and a.address_type_cd='PRIM') ad
            on (rp.company_id = ad.company_id
              and rp.assessment_id = ad.assessment_id
              and rp.address_type_cd = ad.address_type_cd
              and rp.version_num = v_version_num)
        when matched then
         update set
            rp.street_address = ad.street_address, rp.suite = ad.suite, rp.city = ad.city, rp.state = ad.state, rp.postal_cd = ad.postal_cd, rp.fax_num = ad.fax_num, rp.cntry_dial_cd = ad.cntry_dial_cd, rp.country_cd = ad.country_cd
        when not matched then insert
         (company_id, assessment_id, version_num, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd)
          values (rec.company_id, v_assessment_id, v_version_num, ad.address_type_cd, ad.street_address, ad.suite, ad.attention, ad.city, ad.state, ad.province, ad.postal_cd, ad.fax_num, ad.cntry_dial_cd, ad.country_cd);

    -- insert the existing company contacts
       merge into tu_assessment_contact rp
         using (select c.contact_id, c.company_id, p.assessment_id, p.assessment_version, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num
                  from primary_contact_vw c, tu_assessment p
                  where c.company_id = rec.company_id
                  and p.assessment_id = v_assessment_id) ct
            on (rp.company_id = ct.company_id
              and rp.assessment_id = ct.assessment_id
              and rp.version_num = v_version_num)
        when matched then
         update set
            rp.contact_id = ct.contact_id, rp.last_nm = ct.last_nm, rp.first_nm = ct.first_nm, rp.phone_num = ct.phone_num, rp.phone_ext = ct.phone_ext, rp.email_address = ct.email_address, rp.country_cd = ct.country_cd, rp.fax_num = ct.fax_num
        when not matched then insert
         (company_id, contact_id, assessment_id, version_num, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num)
          values (rec.company_id, ct.contact_id, v_assessment_id, v_version_num, ct.last_nm, ct.first_nm, ct.phone_num, ct.phone_ext, ct.cntry_dial_cd, ct.email_address, ct.country_cd, ct.fax_num);
    
    END;
--
    BEGIN
    IF rec.tobacco_class_nm = 'Chew' then

      v_chew_rank := v_chew_rank+1;

      if v_period_tot_chew_taxes <> '0' then
        INSERT into tu_market_share
          (assessment_id, company_id, tobacco_class_id,
           version_num, ms_rank,
           tot_vol_removed, tot_taxes_paid, share_tot_taxes,
           created_dt)
        VALUES
          (v_assessment_id, rec.company_id, rec.tobacco_class_id,
           v_version_num, v_chew_rank,
           v_total_volume, rec.TOTTAX,
           round(rec.TOTTAX/v_period_tot_chew_taxes,6), sysdate);
      else
        INSERT into tu_market_share
          (assessment_id, company_id, tobacco_class_id,
           version_num, ms_rank,
           tot_vol_removed, tot_taxes_paid, share_tot_taxes,
           created_dt)
        VALUES
          (v_assessment_id, rec.company_id, rec.tobacco_class_id,
           v_version_num, v_chew_rank,
           v_total_volume, rec.TOTTAX,
           0, sysdate);      
      end if;

    ELSIF rec.tobacco_class_nm = 'Snuff' then
      v_snuff_rank := v_snuff_rank+1;

      if v_period_tot_snuff_taxes <> '0' then
        INSERT into tu_market_share
          (assessment_id, company_id, tobacco_class_id,
           version_num, ms_rank,
           tot_vol_removed, tot_taxes_paid, share_tot_taxes,
           created_dt)
        VALUES
          (v_assessment_id, rec.company_id, rec.tobacco_class_id,
           v_version_num, v_snuff_rank,
           v_total_volume, rec.TOTTAX,
           round(rec.TOTTAX/v_period_tot_snuff_taxes,6),sysdate);
      else
        INSERT into tu_market_share
          (assessment_id, company_id, tobacco_class_id,
           version_num, ms_rank,
           tot_vol_removed, tot_taxes_paid, share_tot_taxes,
           created_dt)
        VALUES
          (v_assessment_id, rec.company_id, rec.tobacco_class_id,
           v_version_num, v_chew_rank,
           v_total_volume, rec.TOTTAX,
           0, sysdate);      
      end if;

    ELSIF rec.tobacco_class_nm = 'Cigarettes' then

      v_cigarette_rank := v_cigarette_rank+1;

      if v_period_tot_cigarette_taxes <> '0' then
        INSERT into tu_market_share
          (assessment_id, company_id, tobacco_class_id,
           version_num, ms_rank,
           tot_vol_removed, tot_taxes_paid, share_tot_taxes,
           created_dt)
        VALUES
          (v_assessment_id, rec.company_id, rec.tobacco_class_id,
           v_version_num, v_cigarette_rank,
           v_total_volume, rec.TOTTAX,
           round(rec.TOTTAX/v_period_tot_cigarette_taxes,6),
           sysdate);
      else
        INSERT into tu_market_share
          (assessment_id, company_id, tobacco_class_id,
           version_num, ms_rank,
           tot_vol_removed, tot_taxes_paid, share_tot_taxes,
           created_dt)
        VALUES
          (v_assessment_id, rec.company_id, rec.tobacco_class_id,
           v_version_num, v_chew_rank,
           v_total_volume, rec.TOTTAX,
           0, sysdate);      
      end if;

    ELSIF rec.tobacco_class_nm = 'Pipe' then

      v_pipe_rank := v_pipe_rank+1;

      if v_period_tot_pipe_taxes <> '0' then
        INSERT into tu_market_share
          (assessment_id, company_id, tobacco_class_id,
           version_num, ms_rank,
           tot_vol_removed, tot_taxes_paid, share_tot_taxes,
           created_dt)
        VALUES
          (v_assessment_id, rec.company_id, rec.tobacco_class_id,
           v_version_num, v_pipe_rank,
           v_total_volume, rec.TOTTAX,
           round(rec.TOTTAX/v_period_tot_pipe_taxes,6), sysdate);
      else
        INSERT into tu_market_share
          (assessment_id, company_id, tobacco_class_id,
           version_num, ms_rank,
           tot_vol_removed, tot_taxes_paid, share_tot_taxes,
           created_dt)
        VALUES
          (v_assessment_id, rec.company_id, rec.tobacco_class_id,
           v_version_num, v_chew_rank,
           v_total_volume, rec.TOTTAX,
           0, sysdate);      
      end if;

    ELSIF rec.tobacco_class_nm = 'Roll-Your-Own' then

      v_ryo_rank := v_ryo_rank+1;

      if v_period_tot_ryo_taxes <> '0' then
        INSERT into tu_market_share
          (assessment_id, company_id, tobacco_class_id,
           version_num, ms_rank,
           tot_vol_removed, tot_taxes_paid, share_tot_taxes,
           created_dt)
        VALUES
          (v_assessment_id, rec.company_id, rec.tobacco_class_id,
           v_version_num, v_ryo_rank,
           v_total_volume, rec.TOTTAX,
           round(rec.TOTTAX/v_period_tot_ryo_taxes,6), sysdate);
      else
        INSERT into tu_market_share
          (assessment_id, company_id, tobacco_class_id,
           version_num, ms_rank,
           tot_vol_removed, tot_taxes_paid, share_tot_taxes,
           created_dt)
        VALUES
          (v_assessment_id, rec.company_id, rec.tobacco_class_id,
           v_version_num, v_chew_rank,
           v_total_volume, rec.TOTTAX,
           0, sysdate);      
      end if;

    END IF;
    END;
    COMMIT;
    END LOOP;
  END;
END;
END;
/

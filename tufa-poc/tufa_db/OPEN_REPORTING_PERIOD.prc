CREATE OR REPLACE PROCEDURE OPEN_REPORTING_PERIOD(p_period_dt_i in varchar2,
                                                          p_ret_code_o out number)
AS

-- pass in the period and year to be opened in the form MM/YYYY (2 digit month and four digit year)
-- DECLARE
--    p_period_dt_i varchar2(8) := '12/2017';
--    p_ret_code_o number;
    v_period_dt date := last_day(to_date(p_period_dt_i,'MM/YYYY'));

    v_period_yr  number      := 0;
    v_period_mn  varchar2(3) := null;
    v_period_qtr number      := 0;
    v_period_fy  number      := null;

    v_period_id  number      := 0;

    v_rows_inserted number   := 0;

    v_sql1 varchar2(4000)    := null;
    v_sql2 varchar2(4000)    := null;
    v_sql3 varchar2(4000)    := null;

  cursor c_rec (cp_prd_dt date, cp_prd_id number)
  is (select permit_id,
             trunc(issue_dt,'MM') as issue_dt,
             last_day(close_dt) as close_dt
        from tu_permit prmt
	   where not exists (select 1
	                       from tu_period_status stat
						       where stat.permit_id = prmt.permit_id
						        and stat.period_id = cp_prd_id)
		  and cp_prd_dt between trunc(issue_dt,'MM') and NVL(last_day(close_dt),cp_prd_dt));

   v_rec   c_rec%rowtype;

begin
   --open the period
   v_period_fy   := extract(year from v_period_dt);
   v_period_qtr  := mod(to_char(v_period_dt,'Q'),4)+1;
   v_period_mn   := to_char(to_date(extract(month from v_period_dt),'MM'),'MON');

   case
      when v_period_mn in ('OCT', 'NOV', 'DEC')
      then v_period_yr := v_period_fy - 1;
      else v_period_yr := v_period_fy;
   end case;

   begin

      v_sql1 := 'insert into tu_rpt_period (year, month, quarter, fiscal_yr) '||
                'values (:1,:2,:3,:4) returning period_id into :5';
      execute immediate v_sql1
      using v_period_yr, v_period_mn, v_period_qtr, v_period_fy
      returning into v_period_id;

   exception
      when dup_val_on_index
	   then
	      select period_id
		     into v_period_id
		     from tu_rpt_period
		    where year = v_period_yr
		      and month = v_period_mn
			   and quarter = v_period_qtr
			   and fiscal_yr = v_period_fy;
	   when others
	   then
	      p_ret_code_o := -1;

   end;

   open c_rec (v_period_dt, v_period_id);
   loop
      fetch c_rec into v_rec;
      exit when c_rec%notfound;

      v_sql3 := 'insert into tu_permit_period (permit_id, period_id, source_cd) '||
                'values (:1,:2,:3)';

      execute immediate v_sql3
         using v_rec.permit_id, v_period_id, 'EMAI';

      v_sql2 := 'insert into tu_period_status (permit_id, period_id, period_status_type_cd, recon_status_type_cd) '||
                'values (:1,:2,:3, :4)';

      execute immediate v_sql2
         using v_rec.permit_id, v_period_id, 'NSTD', 'UNAP';

      v_rows_inserted := v_rows_inserted + 1;
   end loop;

   p_ret_code_o := v_rows_inserted;
   commit;
   close c_rec;

-- insert the existing company addresses
   merge into tu_report_address rp
     using (select a.company_id, p.permit_id, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd
              from tu_address a, tu_permit p
              where a.company_id = p.company_id) ad
        on (rp.company_id = ad.company_id
          and rp.permit_id = ad.permit_id
          and rp.period_id = v_period_id
          and rp.address_type_cd = ad.address_type_cd)
--    when matched then
--     update set
--        rp.street_address = ad.street_address, rp.suite = ad.suite, rp.city = ad.city, rp.state = ad.state, rp.postal_cd = ad.postal_cd, rp.fax_num = ad.fax_num, rp.country_cd = ad.country_cd
    when not matched then insert
     (company_id, permit_id, period_id, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd)
      values (ad.company_id, ad.permit_id, v_period_id, ad.address_type_cd, ad.street_address, ad.suite, ad.attention, ad.city, ad.state, ad.province, ad.postal_cd, ad.fax_num, ad.cntry_dial_cd, ad.country_cd);


-- insert the existing company contacts
   merge into tu_report_contact rp
     using (select c.contact_id, c.company_id, p.permit_id, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, fax_cntry_dial_cd
              from tu_contact c, tu_permit p
              where c.company_id = p.company_id) ct
        on (rp.contact_id = ct.contact_id
          and rp.company_id = ct.company_id
          and rp.permit_id = ct.permit_id
          and rp.period_id = v_period_id)
--    when matched then
--     update set
--        rp.last_nm = ct.last_nm, rp.first_nm = ct.first_nm, rp.phone_num = ct.phone_num, rp.phone_ext = ct.phone_ext, rp.email_address = ct.email_address, rp.country_cd = ct.country_cd, rp.fax_num = ct.fax_num
    when not matched then insert
     (contact_id, company_id, permit_id, period_id, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, fax_cntry_dial_cd)
      values (ct.contact_id, ct.company_id, ct.permit_id, v_period_id, ct.last_nm, ct.first_nm, ct.phone_num, ct.phone_ext, ct.cntry_dial_cd, ct.email_address, ct.country_cd, ct.fax_num, ct.fax_cntry_dial_cd);

exception
   when others
   then
      p_ret_code_o := -1;
      rollback;
end;
/
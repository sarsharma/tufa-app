--------------------------------------------------------
--  File created - Monday-January-20-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View ALL_TUFA_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP12"."ALL_TUFA_VW" ("EIN", "LEGAL_NM", "TAXES_PAID", "TOBACCO_CLASS_NM", "QUARTER", "FISCAL_YEAR", "COMPANY_TYPE", "ZERO_FLAG", "PERMIT_NUM") AS 
  (  SELECT /*+ USE_NL(TC1,FD,SF,PS,A2,RP) ORDERED */
            c2.ein,
             c2.legal_nm,
             SUM (NVL (fd.taxes_paid, 0)) AS taxes_paid,
             tc1.tobacco_class_nm,
             CASE
                WHEN tc1.tobacco_class_nm IS NULL THEN TO_CHAR (rp.quarter)
                WHEN tc1.tobacco_class_nm <> 'Cigars' THEN TO_CHAR (rp.quarter)
                ELSE '1-4'
             END
                AS quarter,
             rp.fiscal_yr AS fiscal_year,
             'IMPT' AS company_type,
             NVL (pp.zero_flag, 'N') AS zero_flag,
             p.permit_num
        FROM tu_company c2
             INNER JOIN tu_permit p ON c2.company_id = p.company_id
             INNER JOIN tu_permit_period pp ON p.permit_id = pp.permit_id
             INNER JOIN tu_rpt_period rp ON rp.period_id = pp.period_id
             INNER JOIN tu_annual_trueup a2 ON a2.fiscal_yr = rp.fiscal_yr
             INNER JOIN tu_period_status ps
                ON pp.period_id = ps.period_id AND pp.permit_id = ps.permit_id
             LEFT JOIN tu_submitted_form sf
                ON sf.permit_id = pp.permit_id AND sf.period_id = pp.period_id
             LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id
             LEFT JOIN tu_tobacco_class tc1
                ON fd.tobacco_class_id = tc1.tobacco_class_id
       WHERE permit_type_cd = 'IMPT' AND NVL (form_type_cd, '3852') = '3852'
    --     AND tobacco_class_nm IS NOT NULL
    --    AND (pp.zero_flag is null or pp.zero_flag ='N'  --and fd.taxes_paid<>0
    --    )
    GROUP BY tc1.tobacco_class_nm,
             NVL (sf.form_type_cd, '3852'),
             rp.fiscal_yr,
             CASE
                WHEN tc1.tobacco_class_nm IS NULL
                THEN
                   TO_CHAR (rp.quarter)
                WHEN tc1.tobacco_class_nm <> 'Cigars'
                THEN
                   TO_CHAR (rp.quarter)
                ELSE
                   '1-4'
             END,
             p.permit_type_cd,
             c2.ein,
             c2.legal_nm,
             NVL (pp.zero_flag, 'N'),
             p.permit_num)
   UNION ALL
   --Manufacuter
   (  SELECT c2.ein,
             c2.legal_nm,
             SUM (NVL (fd.taxes_paid, 0)) AS taxes_paid,
             CASE
                WHEN tc1.tobacco_class_nm IN ('Chew', 'Snuff')
                THEN
                   'Chew/Snuff'
                WHEN tc1.tobacco_class_nm IN ('Roll-Your-Own', 'Pipe')
                THEN
                   'Pipe/Roll-Your-Own'
                ELSE
                   tc1.tobacco_class_nm
             END
                AS tobacco_class_nm,
             CASE
                WHEN tc1.tobacco_class_nm IS NULL THEN TO_CHAR (rp.quarter)
                WHEN tc1.tobacco_class_nm <> 'Cigars' THEN TO_CHAR (rp.quarter)
                ELSE '1-4'
             END
                AS quarter,
             rp.fiscal_yr AS fiscal_year,
             'MANU' AS company_type,
             NVL (pp.zero_flag, 'N') AS zero_flag,
             p.permit_num
        FROM tu_company c2
             INNER JOIN tu_permit p ON c2.company_id = p.company_id
             INNER JOIN tu_permit_period pp ON p.permit_id = pp.permit_id
             INNER JOIN tu_rpt_period rp ON rp.period_id = pp.period_id
             INNER JOIN tu_annual_trueup a2 ON a2.fiscal_yr = rp.fiscal_yr
             INNER JOIN tu_period_status ps
                ON pp.period_id = ps.period_id AND pp.permit_id = ps.permit_id
             LEFT JOIN tu_submitted_form sf
                ON sf.permit_id = pp.permit_id AND sf.period_id = pp.period_id
             LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id
             LEFT JOIN tu_tobacco_class tc1
                ON fd.tobacco_class_id = tc1.tobacco_class_id
       WHERE permit_type_cd = 'MANU' AND NVL (form_type_cd, '3852') = '3852'
    --     AND TOBACCO_CLASS_NM IS NOT NULL
    --    AND (pp.zero_flag is null or pp.zero_flag ='N'  --and fd.taxes_paid<>0
    --    )
    GROUP BY CASE
                WHEN tc1.tobacco_class_nm IN ('Chew', 'Snuff')
                THEN
                   'Chew/Snuff'
                WHEN tc1.tobacco_class_nm IN ('Roll-Your-Own', 'Pipe')
                THEN
                   'Pipe/Roll-Your-Own'
                ELSE
                   tc1.tobacco_class_nm
             END,
             sf.form_type_cd,
             rp.fiscal_yr,
             CASE
                WHEN tc1.tobacco_class_nm IS NULL
                THEN
                   TO_CHAR (rp.quarter)
                WHEN tc1.tobacco_class_nm <> 'Cigars'
                THEN
                   TO_CHAR (rp.quarter)
                ELSE
                   '1-4'
             END,
             p.permit_type_cd,
             c2.ein,
             c2.legal_nm,
             NVL (pp.zero_flag, 'N'),
             p.permit_num
             )
;

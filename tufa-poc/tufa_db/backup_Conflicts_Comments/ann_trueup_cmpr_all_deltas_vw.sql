
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP12"."ANN_TRUEUP_CMPR_ALL_DELTAS_VW" ("EIN", "COMPANY_NM", "ORIGINAL_COMPANY_NM", "ORIGINAL_EIN", "FISCAL_YR", "TOBACCO_CLASS_NM", "FISCAL_QTR", "DELTA", "PERMIT_TYPE", "SOURCE", "STATUS", "DELTA_COMMENT", "PERMIT_NUM", "COMPANY_ID", "DELTAEXCISETAX_FLAG", "PERMIT_EXCLUDED_ID") AS 
  SELECT
        ein,
        company_nm,
        original_company_nm,
        original_ein,
        fiscal_yr,
        tobacco_class_nm,
        fiscal_qtr,
        DECODE(deltaexcisetax_flag,'Review',0.0,delta) AS delta,
        permit_type,
        source,
        status,
        DECODE(status,'Conflicted','Ingested data falls in an excluded date range for this company',delta_comment) AS delta_comment,        
        permit_num,
        company_id,
        deltaexcisetax_flag,
        permit_excluded_id
    FROM
        (
            SELECT
                ein,
                company_nm,
                original_company_nm,
                original_ein,
                fiscal_yr,
                tobacco_class_nm,
                fiscal_qtr,
                tufa_detail_delta   AS delta,
                permit_type,
                source,
                status,
                delta_comment,
                permit_num,
                company_id,
                CASE
                    WHEN ( ( permit_type = 'MANU' )
                           OR ( permit_type = 'IMPT'
                                AND ( source = 'TUFA'
                                      OR ( source = 'INGESTED'
                                           AND abs(summary_detail_delta) < 1 ) ) ) ) THEN 'Value'
                    WHEN permit_type = 'IMPT'
                         AND source = 'INGESTED'
                         AND ( abs(summary_detail_delta) >= 1 ) THEN 'Review'
           -- All the deltas match  or TUFA matches summary
                    WHEN permit_type = 'IMPT'
                         AND source = 'MATCHED'
                         AND ( ( ( abs(tufa_detail_delta) < 1 )
                                 AND ( abs(tufa_summary_delta) < 1 )
                                 AND ( abs(summary_detail_delta) < 1 ) )
                               OR ( ( abs(tufa_detail_delta) >= 1 )
                                    AND ( abs(tufa_summary_delta) < 1 )
                                    AND ( abs(summary_detail_delta) >= 1 ) ) ) THEN 'Match'
             -- summary and detail match then display delta of tufa and details
                    WHEN permit_type = 'IMPT'
                         AND source = 'MATCHED'
                         AND ( abs(tufa_detail_delta) >= 1 )
                         AND ( abs(tufa_summary_delta) >= 1 )
                         AND ( abs(summary_detail_delta) < 1 ) THEN 'Value'
             -- none of the delta match and tufa matches with detail but not summary
                    WHEN permit_type = 'IMPT'
                         AND source = 'MATCHED'
                         AND ( ( ( abs(tufa_detail_delta) >= 1 )
                                 AND ( abs(tufa_summary_delta) >= 1 )
                                 AND ( abs(summary_detail_delta) >= 1 ) )
                               OR ( ( abs(tufa_detail_delta) < 1 )
                                    AND ( abs(tufa_summary_delta) >= 1 )
                                    AND ( abs(summary_detail_delta) >= 1 ) ) ) THEN 'Review'
                END AS deltaexcisetax_flag,
                permit_excluded_id
            FROM
                (
                    SELECT /*+ NO_CPU_COSTING */
                        ein,
                        company_nm,
                        original_company_nm,
                        original_ein,
                        fiscal_yr,
                        tobacco_class_nm,
                        fiscal_qtr,
                        SUM(nvl(tufa_taxes,0) ) - nvl(ingested_taxes,0) AS tufa_detail_delta,
                        SUM(nvl(tufa_taxes,0) ) - nvl(ingested_summary_taxes,0) AS tufa_summary_delta,
                        nvl(ingested_summary_taxes,0) - nvl(ingested_taxes,0) AS summary_detail_delta,
                        permit_type,
                        source,
                        DECODE(v2.permit_excluded_id,null,DECODE(source,'MATCHED','NA', (nvl( (
                            SELECT
                                tcads.status
                            FROM
                                tu_comparison_all_delta_sts tcads
                            WHERE
                                (tcads.ein = nvl(v2.original_ein,v2.ein)
                                AND tcads.fiscal_yr = v2.fiscal_yr
                                AND tcads.fiscal_qtr = v2.fiscal_qtr
                                AND tcads.tobacco_class_nm = v2.tobacco_class_nm
                                AND tcads.permit_type = v2.permit_type) 
                        ),'Review') ) ),nvl( (
                            SELECT
                                tcads.status
                            FROM
                                tu_comparison_all_delta_sts tcads
                            WHERE
                                (tcads.ein = nvl(v2.original_ein,v2.ein)
                                AND tcads.fiscal_yr = v2.fiscal_yr
                                AND tcads.fiscal_qtr = v2.fiscal_qtr
                                AND tcads.tobacco_class_nm = v2.tobacco_class_nm
                                AND tcads.permit_type = v2.permit_type) 
                        ),'Conflicted')) AS status,
                        (
                            SELECT
                                tadc.user_comment
                            FROM
                                tu_comparison_all_delta_sts tcads,
                                tu_all_delta_join_comment tasjc,
                                tu_all_delta_comment tadc
                            WHERE
                                tcads.cmp_all_delta_id = tasjc.cmp_all_delta_id
                                AND tasjc.comment_seq = tadc.comment_seq
                                AND tcads.ein = nvl(v2.original_ein,v2.ein)
                                AND tcads.fiscal_yr = v2.fiscal_yr
                                AND tcads.fiscal_qtr = v2.fiscal_qtr
                                AND tcads.tobacco_class_nm = v2.tobacco_class_nm
                                AND tcads.permit_type = v2.permit_type
                                AND TO_CHAR(tadc.comment_date,'dd-MON-YY hh24:mi:ss') IN (
                                    SELECT
                                        MAX(TO_CHAR(tadc1.comment_date,'dd-MON-YY hh24:mi:ss') )
                                    FROM
                                        tu_all_delta_comment tadc1,
                                        tu_all_delta_join_comment tasjc1
                                    WHERE
                                        tadc1.comment_seq = tasjc1.comment_seq
                                        AND tasjc1.cmp_all_delta_id = tcads.cmp_all_delta_id
                                )
                        ) AS delta_comment,
                        rtrim(permit_num,';') AS permit_num,
                        company_id,
                        permit_excluded_id
                    FROM
                        (
-- Ingested outer join tufa
                            SELECT
                                nvl(ingested.company_ein,tufa.ein) AS ein,
                                nvl(ingested.company_nm,tufa.legal_nm) AS company_nm,
                                (
                                    SELECT
                                        tc.company_id
                                    FROM
                                        tu_company tc
                                    WHERE
                                        tc.ein = nvl(ingested.company_ein,tufa.ein)
                                ) AS company_id,
                                ingested.original_company_nm   AS original_company_nm,
                                ingested.original_ein          AS original_ein,
                                nvl(ingested.tobacco_class_nm,tufa.tobacco_class_nm) AS tobacco_class_nm,
                                nvl(ingested.fiscal_qtr,tufa.quarter) AS fiscal_qtr,
                                nvl(tufa.taxes_paid,0) AS tufa_taxes,
                                nvl(ingested.taxes_paid,0) AS ingested_taxes,
                                nvl(ingested.summary_taxes_paid,0) AS ingested_summary_taxes,
                                nvl(ingested.company_type,tufa.company_type) AS permit_type,
                                nvl(ingested.fiscal_year,tufa.fiscal_year) AS fiscal_yr,
                                CASE
                                    WHEN ingested.company_ein IS NULL THEN 'TUFA'
                                    WHEN tufa.ein IS NULL THEN 'INGESTED'
                                    ELSE 'MATCHED'
                                END AS source,
                                ( DECODE(ingested.permit_num,NULL,'',ingested.permit_num || ';')
                                  || (
                                    SELECT DISTINCT
                                        LISTAGG(a.permit_num,';') WITHIN GROUP(
                                            ORDER BY
                                                a.created_dt
                                        ) OVER(
                                            PARTITION BY a.company_id
                                        )
                                    FROM
                                        tu_permit a
                                    WHERE
                                        a.company_id = (
                                            SELECT
                                                tc.company_id
                                            FROM
                                                tu_company tc
                                            WHERE
                                                tc.ein = tufa.ein
                                        )
                                ) ) AS permit_num,
                                permit_excluded_id
                            FROM
----------------------------REPLACED ALL_INGESTED_VW------------
                                (
                                    ( SELECT
                                        company_nm,
                                        company_ein,
                                        original_company_nm,
                                        original_ein,
                                        SUM(nvl(taxes_paid,0) ) AS taxes_paid,
                                        SUM(nvl(cbp_summary_taxes_paid,0) ) AS summary_taxes_paid,
                                        fiscal_year,
                                        fiscal_qtr,
                                        tobacco_class_nm,
                                        tobacco_class_id,
                                        company_type,
                                        include_flag,
                                        permit_num,
                                        max(permit_excluded_id) as permit_excluded_id
                                    FROM
                                        (
                                            SELECT
                                                company_nm,
                                                company_ein,
                                                NULL AS original_company_nm,
                                                NULL AS original_ein,
                                                SUM(nvl(estimated_tax,0) ) AS taxes_paid,
                                                cbp_summary_taxes_paid,
                                                cbp_summary_number,
                                                fiscal_year,
                                                fiscal_qtr,
                                                tobacco_class_nm,
                                                tobacco_class_id,
                                                'IMPT' AS company_type,
                                                nvl(include_flag,'Y') AS include_flag,
                                                NULL AS permit_num,
                                                permit_excluded_id
                                            FROM
                                                (
                                                    SELECT
                                                        CASE
                                                            WHEN c.legal_nm IS NOT NULL THEN c.legal_nm
                                                            ELSE cbpdetail1.importer_nm
                                                        END AS company_nm,
                                                        CASE
                                                            WHEN c.ein IS NOT NULL THEN c.ein
                                                            ELSE cbpdetail1.importer_ein
                                                        END AS company_ein,
                                                        cbpentry1.fiscal_year,
                                                        DECODE(ttc1.tobacco_class_nm,'Cigars','1-4',cbpentry1.fiscal_qtr) AS fiscal_qtr
                                                       ,
                                                        ttc1.tobacco_class_nm,
                                                        ttc1.tobacco_class_id,
                                                        cbpentry1.estimated_tax,
                                                        nvl(cbpentry1.summarized_file_tax,0) AS cbp_summary_taxes_paid,
                                                        cbpentry1.summary_entry_summary_number   AS cbp_summary_number,
                                                        cbpdetail1.include_flag                  AS include_flag,
                                                         NVL((select excl.PERM_EXCL_AUDIT_ID from tu_CBP_ingested_excl_vw excl 
                                                         where excl.CBP_ENTRY_ID=cbpentry1.CBP_ENTRY_ID ),null) as permit_excluded_id 
                                                    FROM
                                                        tu_cbp_importer cbpdetail1
                                                        JOIN tu_cbp_entry cbpentry1 ON cbpdetail1.cbp_importer_id = cbpentry1.cbp_importer_id
                                                        JOIN tu_tobacco_class ttc1 ON cbpentry1.tobacco_class_id = ttc1.tobacco_class_id
                                                        LEFT JOIN tu_company c ON CASE
                                                            WHEN cbpdetail1.importer_ein = c.ein
                                                                 AND cbpdetail1.association_type_cd = 'IMPT' THEN 1
                                                            WHEN cbpdetail1.consignee_ein = c.ein
                                                                 AND cbpdetail1.association_type_cd = 'CONS' THEN 1
                                                            ELSE 0
                                                        END = 1
                                                    WHERE
                                                        nvl(cbpdetail1.include_flag,'Y') != 'N'
                                                        AND ( cbpentry1.excluded_flag IS NULL
                                                              OR cbpentry1.excluded_flag <> 'Y' )
                                                        AND ( ttc1.tobacco_class_id <> 14
                                                              OR ttc1.tobacco_class_nm <> 'Non-Taxable' )
                                                        AND ( cbpdetail1.association_type_cd IS NULL
                                                              OR cbpdetail1.association_type_cd <> 'EXCL'
                                                              AND cbpdetail1.association_type_cd <> 'UNKN' )
                                                        AND NOT EXISTS (
                                                            SELECT
                                                                'X'
                                                            FROM
                                                                (
                                                                    SELECT
                                                                        cbp_importer_id
                                                                    FROM
                                                                        (
                                                                            SELECT
                                                                                c1.company_id,
                                                                                c1.legal_nm,
                                                                                SUM(nvl(cbpingested.cbp_taxes_paid,0) ) AS taxes_paid
                                                                               ,
                                                                                cbpingested.fiscal_qtr   AS quarter,
                                                                                cbpingested.fiscal_yr,
                                                                                DECODE(cbpingested.tobacco_class_nm,NULL,'UNKNOWN'
                                                                               ,cbpingested.tobacco_class_nm) AS tobacco_class_nm
                                                                               ,
                                                                                cbpingested.association_type_cd,
                                                                                cbpingested.cbp_importer_id,
                                                                                cbpingested.importer_nm,
                                                                                cbpingested.consignee_nm,
                                                                                CASE
                                                                                    WHEN cbpingested.importer_ein IN (
                                                                                        SELECT
                                                                                            ein
                                                                                        FROM
                                                                                            tu_company
                                                                                    ) THEN 'Y'
                                                                                    ELSE 'N'
                                                                                END AS imp_in_tufa,
                                                                                CASE
                                                                                    WHEN cbpingested.consignee_ein IN (
                                                                                        SELECT
                                                                                            ein
                                                                                        FROM
                                                                                            tu_company
                                                                                    ) THEN 'Y'
                                                                                    ELSE 'N'
                                                                                END AS consg_in_tufa,
                                                                                DECODE(cbpingested.importer_ein,c1.ein,'Y','N') AS
                                                                                imp_match,
                                                                                DECODE(cbpingested.consignee_ein,c1.ein,'Y','N') AS
                                                                                consg_match,
                                                                                cbpingested.importer_ein,
                                                                                cbpingested.consignee_ein
                                                                            FROM
                                                                                tu_annual_trueup a1,
                                                                                (
                                                                                    SELECT /*+ INDEX_DESC(CBPENTRY2 TU_CBP_ENTRY_UK) */
                                                                                        cbpdetail2.cbp_importer_id,
                                                                                        cbpdetail2.importer_nm,
                                                                                        cbpdetail2.consignee_nm,
                                                                                        cbpdetail2.importer_ein,
                                                                                        cbpdetail2.consignee_ein,
                                                                                        cbpdetail2.fiscal_yr,
                                                                                        SUM(nvl(cbpentry2.estimated_tax,0) ) AS cbp_taxes_paid
                                                                                       ,
                                                                                        ttc2.tobacco_class_nm,
                                                                                        ttc2.tobacco_class_id   AS tobacco_class_id,
                                                                                        cbpentry2.fiscal_qtr    AS fiscal_qtr,
                                                                                        cbpdetail2.association_type_cd,
                                                                                        cbpdetail2.include_flag
                                                                                    FROM
                                                                                        tu_cbp_importer cbpdetail2
                                                                                        INNER JOIN tu_cbp_entry cbpentry2 ON cbpdetail2
                                                                                        .cbp_importer_id = cbpentry2.cbp_importer_id
                                                                                        LEFT JOIN tu_tobacco_class ttc2 ON cbpentry2
                                                                                        .tobacco_class_id = ttc2.tobacco_class_id
                                                                                    WHERE
                                                                                        cbpdetail2.importer_ein <> cbpdetail2.consignee_ein
                                                                                        AND cbpdetail2.default_flag = 'N'
                                                                                    GROUP BY
                                                                                        cbpdetail2.cbp_importer_id,
                                                                                        cbpdetail2.importer_nm,
                                                                                        cbpdetail2.consignee_nm,
                                                                                        cbpdetail2.importer_ein,
                                                                                        cbpdetail2.consignee_ein,
                                                                                        cbpdetail2.fiscal_yr,
                                                                                        ttc2.tobacco_class_nm,
                                                                                        ttc2.tobacco_class_id,
                                                                                        cbpentry2.fiscal_qtr,
                                                                                        cbpdetail2.association_type_cd,
                                                                                        cbpdetail2.include_flag
                                                                                ) cbpingested,
                                                                                tu_company c1
                                                                            WHERE
                                                                                ( cbpingested.consignee_ein = c1.ein
                                                                                  OR cbpingested.importer_ein = c1.ein )
                                                                                AND cbpingested.importer_ein <> cbpingested.consignee_ein
                                                                                AND cbpingested.fiscal_yr = a1.fiscal_yr
                                                                            GROUP BY
                                                                                c1.company_id,
                                                                                c1.legal_nm,
                                                                                cbpingested.fiscal_qtr,
                                                                                cbpingested.fiscal_yr,
                                                                                cbpingested.tobacco_class_nm,
                                                                                cbpingested.association_type_cd,
                                                                                cbpingested.include_flag,
                                                                                cbpingested.cbp_importer_id,
                                                                                cbpingested.importer_nm,
                                                                                cbpingested.consignee_nm,
                                                                                DECODE(cbpingested.importer_ein,c1.ein,'Y','N'),
                                                                                DECODE(cbpingested.consignee_ein,c1.ein,'Y','N'),
                                                                                cbpingested.importer_ein,
                                                                                cbpingested.consignee_ein
  --, NVL(cbpentry.summarized_file_tax, 0), cbpentry.summary_entry_summary_number
                                                                            ORDER BY
                                                                                c1.company_id
                                                                        ) PIVOT (
                                                                            MAX ( taxes_paid )
                                                                            FOR tobacco_class_nm
                                                                            IN ( 'Cigars' AS taxes_cigars,'Pipe' AS taxes_pipe,'Snuff'
                                                                            AS taxes_snuff,'Chew' AS taxes_chew,'Cigarettes' AS taxes_cigs
                                                                           ,'Roll-Your-Own' AS taxes_ryo,'UNKNOWN' AS taxes_unknown
                                                                            )
                                                                        )
                                                                        v3
                                                                    WHERE
                                                                        association_type_cd IS NULL
                                                                        AND nvl(include_flag,'Y') != 'N'
                                                                ) acadv
                                                            WHERE
                                                                acadv.cbp_importer_id = cbpdetail1.cbp_importer_id
                                                        )
                                                ) v2
                                            GROUP BY
                                                company_nm,
                                                company_ein,
                                                fiscal_year,
                                                fiscal_qtr,
                                                tobacco_class_nm,
                                                tobacco_class_id,
                                                nvl(include_flag,'Y'),
                                                cbp_summary_taxes_paid,
                                                cbp_summary_number,
                                                permit_excluded_id
                                        ) v1
                                    GROUP BY
                                        company_nm,
                                        company_ein,
                                        original_company_nm,
                                        original_ein,
                                        fiscal_year,
                                        fiscal_qtr,
                                        tobacco_class_nm,
                                        tobacco_class_id,
                                        company_type,
                                        include_flag,
                                        permit_num
                                       -- permit_excluded_id
                                    )
                                    UNION ALL
--  Importer ASSOCIATED_DELTAS
                                    ( SELECT
                                        company_nm,
                                        company_ein,
                                        original_company_nm,
                                        original_ein,
                                        SUM(nvl(taxes_paid,0) ) AS taxes_paid,
                                        SUM(nvl(cbp_summary_taxes_paid,0) ) AS summary_taxes_paid,
                                        fiscal_year,
                                        fiscal_qtr,
                                        tobacco_class_nm,
                                        tobacco_class_id,
                                        company_type,
                                        include_flag,
                                        permit_num,
                                        NULL as permit_excluded_id
                                    FROM
                                        (
                                            SELECT
                                                company_nm,
                                                company_ein,
                                                original_company_nm,
                                                original_ein,
                                                SUM(nvl(estimated_tax,0) ) AS taxes_paid,
                                                cbp_summary_taxes_paid,
                                                cbp_summary_number,
                                                fiscal_year,
                                                fiscal_qtr,
                                                tobacco_class_nm,
                                                tobacco_class_id,
                                                'IMPT' AS company_type,
                                                nvl(include_flag,'Y') AS include_flag,
                                                NULL AS permit_num
                                            FROM
                                                (
                                                    SELECT
--Need to some how get the Consignee NM if associtaion type is CONS and the entry as been associated to a parent
                                                        nvl(c2.legal_nm,cbpdetailparent.importer_nm) AS company_nm,
                                                        nvl(c2.ein,cbpdetail.importer_ein) AS company_ein,
                                                        nvl(c.legal_nm,cbpdetail.importer_nm) AS original_company_nm,
                                                        nvl(c.ein,cbpdetail.importer_ein) AS original_ein,
                                                        cbpentry.fiscal_year,
                                                        DECODE(ttc.tobacco_class_nm,'Cigars','1-4',cbpentry.fiscal_qtr) AS fiscal_qtr
                                                       ,
                                                        ttc.tobacco_class_nm,
                                                        ttc.tobacco_class_id,
                                                        cbpentry.estimated_tax,
                                                        cbpdetail.include_flag                  AS include_flag,
                                                        nvl(cbpentry.summarized_file_tax,0) AS cbp_summary_taxes_paid,
                                                        cbpentry.summary_entry_summary_number   AS cbp_summary_number
                                                    FROM
                                                        tu_cbp_importer cbpdetail
                                                        JOIN tu_cbp_entry cbpentry ON cbpdetail.cbp_importer_id = cbpentry.original_cbp_importer_id
                                                        JOIN tu_cbp_importer cbpdetailparent ON cbpentry.cbp_importer_id = cbpdetailparent
                                                        .cbp_importer_id
                                                        JOIN tu_tobacco_class ttc ON cbpentry.tobacco_class_id = ttc.tobacco_class_id
                                                        LEFT JOIN tu_company c ON CASE
                                                            WHEN cbpdetail.importer_ein = c.ein
                                                                 AND cbpdetail.association_type_cd = 'IMPT' THEN 1
                                                            WHEN cbpdetail.consignee_ein = c.ein
                                                                 AND cbpdetail.association_type_cd = 'CONS' THEN 1
                                                            ELSE 0
                                                        END = 1
                                                        LEFT JOIN tu_company c2 ON CASE
                                                            WHEN cbpdetailparent.importer_ein = c2.ein
                                                                 AND cbpdetailparent.association_type_cd = 'IMPT' THEN 1
                                                            WHEN cbpdetailparent.consignee_ein = c2.ein
                                                                 AND cbpdetailparent.association_type_cd = 'CONS' THEN 1
                                                            ELSE 0
                                                        END = 1
                                                    WHERE
                                                        nvl(cbpdetail.include_flag,'Y') != 'N'
                                                        AND ( cbpentry.excluded_flag IS NULL
                                                              OR cbpentry.excluded_flag <> 'Y' )
                                                        AND ( ttc.tobacco_class_id <> 14
                                                              OR ttc.tobacco_class_nm <> 'Non-Taxable' )
                            -- Excluding any UNKN associations
                                                        AND ( cbpdetail.association_type_cd IS NULL
                                                              OR ( cbpdetail.association_type_cd <> 'EXCL'
                                                                   AND cbpdetail.association_type_cd <> 'UNKN' ) )
                                                        AND NOT EXISTS (
                                                            SELECT
                                                                'X'
                                                            FROM
                                                                (
                       ---------------------------------------
                                                                 (
                                                                    SELECT
                                                                        cbp_importer_id
                                                                       FROM
                                                                        (
                                                                            SELECT
                                                                                c1.company_id,
                                                                                c1.legal_nm,
                                                                                SUM(nvl(cbpingested.cbp_taxes_paid,0) ) AS taxes_paid
                                                                               ,
                                                                                cbpingested.fiscal_qtr   AS quarter,
                                                                                cbpingested.fiscal_yr,
                                                                                DECODE(cbpingested.tobacco_class_nm,NULL,'UNKNOWN'
                                                                               ,cbpingested.tobacco_class_nm) AS tobacco_class_nm
                                                                               ,
                                                                                cbpingested.association_type_cd,
                                                                                cbpingested.cbp_importer_id,
                                                                                cbpingested.importer_nm,
                                                                                cbpingested.consignee_nm,
                                                                                CASE
                                                                                    WHEN cbpingested.importer_ein IN (
                                                                                        SELECT
                                                                                            ein
                                                                                        FROM
                                                                                            tu_company
                                                                                    ) THEN 'Y'
                                                                                    ELSE 'N'
                                                                                END AS imp_in_tufa,
                                                                                CASE
                                                                                    WHEN cbpingested.consignee_ein IN (
                                                                                        SELECT
                                                                                            ein
                                                                                        FROM
                                                                                            tu_company
                                                                                    ) THEN 'Y'
                                                                                    ELSE 'N'
                                                                                END AS consg_in_tufa,
                                                                                DECODE(cbpingested.importer_ein,c1.ein,'Y','N') AS
                                                                                imp_match,
                                                                                DECODE(cbpingested.consignee_ein,c1.ein,'Y','N') AS
                                                                                consg_match,
                                                                                cbpingested.importer_ein,
                                                                                cbpingested.consignee_ein
                                                                            FROM
                                                                                tu_annual_trueup a1
                                                                                INNER JOIN (
                                                                                    SELECT
                                                                                        cbpdetail.cbp_importer_id,
                                                                                        cbpdetail.importer_nm,
                                                                                        cbpdetail.consignee_nm,
                                                                                        cbpdetail.importer_ein,
                                                                                        cbpdetail.consignee_ein,
                                                                                        cbpdetail.fiscal_yr,
                                                                                        SUM(nvl(cbpentry.estimated_tax,0) ) AS cbp_taxes_paid
                                                                                       ,
                                                                                        ttc.tobacco_class_nm,
                                                                                        ttc.tobacco_class_id   AS tobacco_class_id,
                                                                                        cbpentry.fiscal_qtr    AS fiscal_qtr,
                                                                                        cbpdetail.association_type_cd
                                                                                    FROM
                                                                                        tu_cbp_importer cbpdetail
                                                                                        INNER JOIN tu_cbp_entry cbpentry ON cbpdetail
                                                                                        .cbp_importer_id = cbpentry.cbp_importer_id
                                                                                        LEFT JOIN tu_tobacco_class ttc ON cbpentry
                                                                                        .tobacco_class_id = ttc.tobacco_class_id
                                                                                    WHERE --  (CBPENTRY.EXCLUDED_FLAG IS NULL OR CBPENTRY.EXCLUDED_FLAG     <> 'Y')
--   CBPENTRY.TOBACCO_CLASS_ID  = TTC.TOBACCO_CLASS_ID
--  AND (TTC.TOBACCO_CLASS_ID     <> 14
--  OR TTC.TOBACCO_CLASS_NM       <> 'Non-Taxable')
--  AND CBPDETAIL.CBP_IMPORTER_ID  = CBPENTRY.CBP_IMPORTER_ID
                                                                                        cbpdetail.importer_ein <> cbpdetail.consignee_ein
                                                                                        AND cbpdetail.default_flag = 'N'
                                                                                    GROUP BY
                                                                                        cbpdetail.cbp_importer_id,
                                                                                        cbpdetail.importer_nm,
                                                                                        cbpdetail.consignee_nm,
                                                                                        cbpdetail.importer_ein,
                                                                                        cbpdetail.consignee_ein,
                                                                                        cbpdetail.fiscal_yr,
                                                                                        ttc.tobacco_class_nm,
                                                                                        ttc.tobacco_class_id,
                                                                                        cbpentry.fiscal_qtr,
                                                                                        cbpdetail.association_type_cd
                                                                                ) cbpingested ON cbpingested.fiscal_yr = a1.fiscal_yr
                                                                                INNER JOIN tu_company c1 ON ( cbpingested.consignee_ein
                                                                                = c1.ein
                                                                                                              OR cbpingested.importer_ein
                                                                                                              = c1.ein )
                                                                                                            AND ( cbpingested.importer_ein
                                                                                                            <> cbpingested.consignee_ein
                                                                                                            )
                                                                            GROUP BY
                                                                                c1.company_id,
                                                                                c1.legal_nm,
                                                                                cbpingested.fiscal_qtr,
                                                                                cbpingested.fiscal_yr,
                                                                                cbpingested.tobacco_class_nm,
                                                                                cbpingested.association_type_cd,
                                                                                cbpingested.cbp_importer_id,
                                                                                cbpingested.importer_nm,
                                                                                cbpingested.consignee_nm,
                                                                                DECODE(cbpingested.importer_ein,c1.ein,'Y','N'),
                                                                                DECODE(cbpingested.consignee_ein,c1.ein,'Y','N'),
                                                                                cbpingested.importer_ein,
                                                                                cbpingested.consignee_ein,
                                                                                nvl(cbpentry.summarized_file_tax,0),
                                                                                cbpentry.summary_entry_summary_number
                                                                            ORDER BY
                                                                                c1.company_id
                                                                        ) PIVOT (
                                                                            MAX ( taxes_paid )
                                                                            FOR tobacco_class_nm
                                                                            IN ( 'Cigars' AS taxes_cigars,'Pipe' AS taxes_pipe,'Snuff'
                                                                            AS taxes_snuff,'Chew' AS taxes_chew,'Cigarettes' AS taxes_cigs
                                                                           ,'Roll-Your-Own' AS taxes_ryo,'UNKNOWN' AS taxes_unknown
                                                                            )
                                                                        )
                                                                    WHERE
                                                                        association_type_cd IS NULL
                                                                )
                       ---------------------------------------
                                                                 ) acadv
                                                            WHERE
                                                                acadv.cbp_importer_id = cbpdetail.cbp_importer_id
                        --AND acadv.association_type_cd IS NULL
                                                        )
                                                )
                                            GROUP BY
                                                company_nm,
                                                company_ein,
                                                original_company_nm,
                                                original_ein,
                                                fiscal_year,
                                                fiscal_qtr,
                                                tobacco_class_nm,
                                                tobacco_class_id,
                                                nvl(include_flag,'Y'),
                                                cbp_summary_taxes_paid,
                                                cbp_summary_number
                                        )
                                    GROUP BY
                                        company_nm,
                                        company_ein,
                                        original_company_nm,
                                        original_ein,
                  --  taxes_paid,
                                        fiscal_year,
                                        fiscal_qtr,
                                        tobacco_class_nm,
                                        tobacco_class_id,
                                        company_type,
                                        include_flag,
                                        permit_num
                                    )
                                    UNION ALL
                                    SELECT
                                        company_nm,
                                        company_ein,
                                        NULL AS original_company_nm,
                                        NULL AS original_ein,
                                        SUM(taxes_paid) AS taxes_paid,
                                        0 AS cbp_summary_taxes_paid,
                                        fiscal_yr,
                                        fiscal_qtr,
                                        tobacco_class_nm,
                                        tobacco_class_id,
                                        company_type,
                                        nvl(include_flag,'Y'),
                                        permit_num,
                                        max(permit_excluded_id)
                                    FROM
                                        ( (
                                            SELECT /*+ NO_INDEX(TTC) */
                                                NVL((select ttbtc.legal_nm from tu_company ttbtc where ttbtc.ein = ttbcmpy.ein_num), ttbcmpy.company_nm) as company_nm,
                                                ttbcmpy.ein_num      AS company_ein,
                                                ( nvl(ttbannualtx.ttb_taxes_paid,0) ) AS taxes_paid,
                                                ttbcmpy.fiscal_yr,
                                                CASE
                                                    WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(ttbannualtx.ttb_calendar_qtr
                                                    )
                                                    ELSE '1-4'
                                                END AS fiscal_qtr,
                                                CASE
                                                    WHEN ttc.tobacco_class_nm = 'Chew-and-Snuff' THEN 'Chew/Snuff'
                                                    WHEN ttc.tobacco_class_nm = 'Pipe-RYO'       THEN 'Pipe/Roll-Your-Own'
                                                    ELSE ttc.tobacco_class_nm
                                                END AS tobacco_class_nm,
                                                ttc.tobacco_class_id,
                                                'MANU' AS company_type,
                                                nvl(ttbcmpy.include_flag,'Y') AS include_flag,
                                                (
                                                    SELECT DISTINCT
                                                        LISTAGG(ab.permit_num,';') WITHIN GROUP(
                                                            ORDER BY
                                                                ab.created_dt
                                                        ) OVER(
                                                            PARTITION BY ab.ttb_company_id
                                                        )
                                                    FROM
                                                        tu_ttb_permit ab
                                                    WHERE
                                                        ab.ttb_company_id = ttbcmpy.ttb_company_id
                                                ) AS permit_num,
                                                NVL((select excl.PERM_EXCL_AUDIT_ID from tu_ttb_ingested_excl_vw excl where ttbannualtx.TTB_ANNUAL_TAX_ID = (excl.TTB_ANNUAL_TAX_ID)),null) as permit_excluded_id 
                                            FROM
                                                tu_ttb_company ttbcmpy,
                                                tu_ttb_permit ttbpermit,
                                                tu_ttb_annual_tax ttbannualtx,
                                                tu_tobacco_class ttc
                                            WHERE
                                                nvl(ttbcmpy.include_flag,'Y') != 'N'
                                                AND ttbannualtx.tobacco_class_id = ttc.tobacco_class_id
                                                AND ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id
                                                AND ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id
                                        ) )
                                    GROUP BY
                                        company_nm,
                                        company_ein,
                                        fiscal_yr,
                                        fiscal_qtr,
                                        tobacco_class_nm,
                                        tobacco_class_id,
                                        company_type,
                                        nvl(include_flag,'Y'),
                                        permit_num
                                        --permit_excluded_id
                                ) ingested
        ----------------------end---------------------------------------------
                                FULL OUTER JOIN all_tufa_vw tufa ON tufa.ein = nvl(ingested.original_company_nm,ingested.company_ein
                                )
                                                                    AND ingested.fiscal_year = tufa.fiscal_year
                                                                    AND ingested.fiscal_qtr = DECODE(ingested.tobacco_class_nm ||
                                                                    tufa.zero_flag,'CigarsY','1-4',tufa.quarter)
                                                                    AND ingested.tobacco_class_nm = DECODE(tufa.zero_flag,'Y',ingested
                                                                    .tobacco_class_nm,tufa.tobacco_class_nm)
                                                                    AND ingested.company_type = tufa.company_type
       -- WHERE  abs(nvl(ingested.taxes_paid, tufa.taxes_paid)) >= 1 -- as we have check in the outer select
                        ) v2
                    GROUP BY
                        ein,
                        company_nm,
                        company_id,
                        original_company_nm,
                        original_ein,
                        ingested_taxes,
                        tobacco_class_nm,
                        fiscal_qtr,
                        permit_type,
                        source,
                        fiscal_yr,
                        permit_num,
                        ingested_summary_taxes,
                        permit_excluded_id
                )
        )
    WHERE
        ( deltaexcisetax_flag = 'Review'
          OR ( deltaexcisetax_flag = 'Value'
               AND abs(delta) > 0.99 ) );

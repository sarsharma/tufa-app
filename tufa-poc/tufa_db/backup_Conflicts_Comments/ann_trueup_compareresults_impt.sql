--------------------------------------------------------
--  File created - Thursday-January-16-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View ANN_TRUEUP_COMPARERESULTS_IMP
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP12"."ANN_TRUEUP_COMPARERESULTS_IMP" ("LEGAL_NM", "CLASSNAME", "QUARTERX", "DELTA", "PERMIT_TYPE_CD", "COMPANY_ID", "FISCAL_YR", "ACCEPTANCE_FLAG", "DELTA_CHANGE", "FDA_DELTA", "IN_PROGRESS_FLAG", "EIN", "STATUS", "PERMIT_NUM", "DELTAEXCISETAX_FLAG") AS 
  SELECT
    legal_nm,
    classname,
    quarterx,
    delta,
    permit_type_cd,
    company_id,
    fiscal_yr,
    acceptance_flag,
    delta_change,
    fda_delta,
    in_progress_flag,
    ein,
    status,
    permit_num,
    deltaexcisetax_flag
FROM
    (
        SELECT
            legal_nm,
            classname,
            quarterx,
            tufa_detail_delta AS delta,
            permit_type_cd,
            company_id,
            fiscal_yr,
            acceptance_flag,
            delta_change,
            fda_delta,
            CASE
                WHEN in_progress_flag = 'Y' THEN
                    'InProgress'
                WHEN in_progress_flag = 'N' THEN
                    'NotInProgress'
                ELSE
                    NULL
            END AS in_progress_flag,
            ein,
            'NA' AS status,
            permit_num,
            CASE
        -- All the deltas match  or TUFA matches summary
                WHEN ( ( ( abs(tufa_detail_delta) < 1 )
                         AND ( abs(tufa_summary_delta) < 1 )
                         AND ( abs(summary_detail_delta) < 1 ) )
                       OR ( ( abs(tufa_detail_delta) >= 1 )
                            AND ( abs(tufa_summary_delta) < 1 )
                           AND ( abs(summary_detail_delta) >= 1 ) ) ) THEN
                    'Match'
        -- summary and detail match then display delta of tufa and details
                WHEN ( abs(tufa_detail_delta) >= 1 )
                     AND ( abs(tufa_summary_delta) >= 1 )
                     AND ( abs(summary_detail_delta) < 1 ) THEN
                    'Value'
        -- none of the delta match and tufa matches with detail but not summary
                WHEN ( ( ( abs(tufa_detail_delta) >= 1 )
                         AND ( abs(tufa_summary_delta) >= 1 )
                         AND ( abs(summary_detail_delta) >= 1 ) )
                       OR ( ( abs(tufa_detail_delta) < 1 )
                            AND ( abs(tufa_summary_delta) >= 1 )
                            AND ( abs(summary_detail_delta) >= 1 ) ) ) THEN
                    'Review'
            END AS deltaexcisetax_flag
        FROM
            (
                SELECT
                    ingested.legal_nm           AS legal_nm,
                    ingested.ein                AS ein,
                    ingested.tobacco_class_nm   AS classname,
                    TO_CHAR(ingested.fiscal_qtr) AS quarterx,
                    SUM(nvl(reports.taxes_paid, 0)) - nvl(ingested.cbp_taxes_paid, 0) AS tufa_detail_delta,
                    SUM(nvl(reports.taxes_paid, 0)) - nvl(ingested.cbp_summary_taxes_paid, 0) AS tufa_summary_delta,
                    nvl(ingested.cbp_summary_taxes_paid, 0) - nvl(ingested.cbp_taxes_paid, 0) AS summary_detail_delta,
                    nvl(reports.permit_type_cd, 'IMPT') AS permit_type_cd,
                    ingested.company_id         AS company_id,
                    ingested.fiscal_year        AS fiscal_yr,
                    cbpamnd.acceptance_flag     AS acceptance_flag,
                    cbpamnd.in_progress_flag    AS in_progress_flag,
                    CASE
                        WHEN cbpamnd.fda_delta IS NOT NULL
                             AND ( abs(cbpamnd.fda_delta - SUM(nvl(reports.taxes_paid, 0)) + nvl(ingested.cbp_taxes_paid, 0)) > 0
                                   OR SUM(nvl(reports.taxes_paid, 0)) - nvl(ingested.cbp_taxes_paid, 0) = 0 AND cbpamnd.fda_delta != 0) THEN
                            'Y'
                        WHEN cbpamnd.fda_delta IS NULL
                             AND cbpamnd.acceptance_flag IN
                            --4046 Kyle - Ingestedaccepted
                              (
                            'FDAaccepted',
                            'Ingestedaccepted',
                            'Amended'
                        ) THEN
                            'I'
                        ELSE
                            'N'
                    END AS delta_change,
                    cbpamnd.fda_delta           AS fda_delta,
                    reports.permit_num
                FROM
                    (
                        SELECT
                            cbpingestedfinal.legal_nm,
                            cbpingestedfinal.ein,
                            SUM(nvl(cbpingestedfinal.cbp_taxes_paid, 0)) AS cbp_taxes_paid,
                            SUM(nvl(cbpingestedfinal.cbp_summary_taxes_paid, 0)) AS cbp_summary_taxes_paid,
                            cbpingestedfinal.fiscal_qtr,
                            cbpingestedfinal.fiscal_year,
                            cbpingestedfinal.tobacco_class_nm,
                            cbpingestedfinal.tobacco_class_id,
                            cbpingestedfinal.company_id
                        FROM
                            (
                                SELECT
                                    c1.legal_nm,
                                    c1.ein,
                                    SUM(nvl(cbpingested.cbp_taxes_paid, 0)) AS cbp_taxes_paid,
                                    cbpingested.cbp_summary_taxes_paid,
                                    DECODE(cbpingested.tobacco_class_nm, 'Cigars', '1-4', cbpingested.fiscal_qtr) AS fiscal_qtr,
                                    cbpingested.fiscal_year,
                                    cbpingested.tobacco_class_nm,
                                    cbpingested.tobacco_class_id,
                                    c1.company_id,
                                    cbpingested.summary_entry_summary_number
                                FROM
                                    tu_annual_trueup   a1,
                                    (
                                        SELECT
                                            cbpdetail.importer_ein,
                                            cbpdetail.consignee_ein,
                                            SUM(nvl(cbpentry.estimated_tax, 0)) AS cbp_taxes_paid,
                                            ttc.tobacco_class_nm,
                                            CASE
                                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN
                                                    TO_CHAR(cbpentry.fiscal_qtr)
                                                ELSE
                                                    '1-4'
                                            END AS fiscal_qtr,
                                            cbpentry.fiscal_year,
                                            cbpdetail.association_type_cd,
                                            ttc.tobacco_class_id AS tobacco_class_id,
                                            cbpdetail.consignee_exists_flag,
                                            nvl(cbpentry.summarized_file_tax, 0) AS cbp_summary_taxes_paid,
                                            cbpentry.summary_entry_summary_number
                                        FROM
                                            tu_cbp_importer    cbpdetail,
                                            tu_cbp_entry       cbpentry,
                                            tu_tobacco_class   ttc
                                            
                                        WHERE
                                            ( cbpentry.excluded_flag IS NULL
                                              OR cbpentry.excluded_flag <> 'Y' )
                                            AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
                                            AND ( ttc.tobacco_class_id <> 14
                                                  OR ttc.tobacco_class_nm <> 'Non-Taxable' )
                                            AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
--                                            and  cbpentry.cbp_entry_id  not in (select excl.cbp_entry_id from tu_cbp_ingested_excl_vw excl 
--                                                 where excl.PERM_EXCL_AUDIT_ID is not null and excl.PERM_EXCL_AUDIT_ID not in 
--                                                      (select vw.perm_excl_audit_id from permit_excl_status_vw vw,
--                                                            (select  c.ein,ttam.qtr,ttam.fiscal_yr from tu_cbp_amendment ttam 
--                                                                inner join  tu_company c on c.company_id = ttam.cbp_company_id)a 
--                                                            ))
                                                    
                                        GROUP BY
                                            cbpdetail.importer_ein,
                                            cbpdetail.consignee_ein,
                                            ttc.tobacco_class_nm,
                                            cbpdetail.consignee_exists_flag,
                                            CASE
                                                    WHEN ttc.tobacco_class_nm <> 'Cigars' THEN
                                                        TO_CHAR(cbpentry.fiscal_qtr)
                                                    ELSE
                                                        '1-4'
                                                END,
                                            cbpentry.fiscal_year,
                                            cbpdetail.association_type_cd,
                                            ttc.tobacco_class_id,
                                            nvl(cbpentry.summarized_file_tax, 0),
                                            cbpentry.summary_entry_summary_number
                                    ) cbpingested,
                                    tu_company         c1
                                WHERE
                                    ( ( cbpingested.importer_ein = c1.ein
                                        AND cbpingested.association_type_cd = 'IMPT' )
                                      OR ( cbpingested.consignee_ein = c1.ein
                                           AND cbpingested.association_type_cd = 'CONS' )
                                      OR ( cbpingested.consignee_ein = c1.ein
                                           AND cbpingested.association_type_cd IS NULL
                                           AND cbpingested.consignee_exists_flag = 'N' ) )
                                    AND cbpingested.cbp_taxes_paid != 0
                                    AND cbpingested.fiscal_year = a1.fiscal_yr
                                GROUP BY
                                    c1.legal_nm,
                                    c1.ein,
                                    cbpingested.cbp_summary_taxes_paid,
                                    DECODE(cbpingested.tobacco_class_nm, 'Cigars', '1-4', cbpingested.fiscal_qtr),
                                    cbpingested.fiscal_year,
                                    cbpingested.tobacco_class_nm,
                                    cbpingested.tobacco_class_id,
                                    c1.company_id,
                                    nvl(cbpingested.cbp_summary_taxes_paid, 0),
                                    cbpingested.summary_entry_summary_number
                            ) cbpingestedfinal
                        GROUP BY
                            cbpingestedfinal.legal_nm,
                            cbpingestedfinal.ein,
                            cbpingestedfinal.fiscal_qtr,
                            cbpingestedfinal.fiscal_year,
                            cbpingestedfinal.tobacco_class_nm,
                            cbpingestedfinal.tobacco_class_id,
                            cbpingestedfinal.company_id
                    ) ingested
                    INNER JOIN (
                        SELECT /*+ USE_NL(TC1,FD,SF,PS,A2,RP) ORDERED */
                            SUM(nvl(fd.taxes_paid, 0)) AS taxes_paid,
                            tc1.tobacco_class_nm,
                            nvl(sf.form_type_cd, '3852') AS form_type_cd,
                            rp.fiscal_yr,
                            CASE
                                WHEN tc1.tobacco_class_nm IS NULL THEN
                                    TO_CHAR(rp.quarter)
                                WHEN tc1.tobacco_class_nm <> 'Cigars' THEN
                                    TO_CHAR(rp.quarter)
                                ELSE
                                    '1-4'
                            END AS quarter,
                            p.permit_type_cd,
                            c2.ein,
                            c2.company_id,
                            pp.zero_flag,
                            (
                                SELECT DISTINCT
                                    LISTAGG(a.permit_num, ';') WITHIN GROUP(
                                        ORDER BY
                                            a.created_dt
                                    ) OVER(
                                        PARTITION BY a.company_id
                                    )
                                FROM
                                    tu_permit a
                                WHERE
                                    a.company_id = c2.company_id
                            ) permit_num
                        FROM
                            tu_company          c2
                            INNER JOIN tu_permit           p ON c2.company_id = p.company_id
                            INNER JOIN tu_permit_period    pp ON p.permit_id = pp.permit_id
                            INNER JOIN tu_rpt_period       rp ON rp.period_id = pp.period_id
                            INNER JOIN tu_annual_trueup    a2 ON a2.fiscal_yr = rp.fiscal_yr
                            INNER JOIN tu_period_status    ps ON pp.period_id = ps.period_id
                                                              AND pp.permit_id = ps.permit_id
                            LEFT JOIN tu_submitted_form   sf ON sf.permit_id = pp.permit_id
                                                              AND sf.period_id = pp.period_id
                            LEFT JOIN tu_form_detail      fd ON sf.form_id = fd.form_id
                            LEFT JOIN tu_tobacco_class    tc1 ON fd.tobacco_class_id = tc1.tobacco_class_id
                            where
                                   --------------------------Changes for Non Cigar Classes----------------
                             ((fd.tobacco_class_id <>8 and (c2.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(rp.quarter,4,rp.fiscal_yr+1,rp.fiscal_yr)
                                        and   ASSMNT_QTR=Decode(rp.quarter,4,1,rp.quarter+1)
                                        and (EXCL_SCOPE_ID = 2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(rp.quarter,4,rp.fiscal_yr+1,rp.fiscal_yr)
                                            or 
                                           (ASSMNT_QTR <=(Decode(rp.quarter,4,1,rp.quarter+1)) and ASSMNT_YR=Decode(rp.quarter,4,rp.fiscal_yr+1,rp.fiscal_yr))
                                          )
                                        )
                                   ) OR (c2.ein in (select vw.ein from permit_excl_status_vw vw,
                                        (select  c.ein,ttam.qtr,ttam.fiscal_yr from tu_ttb_amendment ttam 
                                            inner join  tu_company c on c.company_id = ttam.ttb_company_id)a 
                                             where vw.ein = a.ein and 
                                             ((vw.ASSMNT_YR= Decode(a.QTR,4,a.fiscal_yr+1,a.fiscal_yr)
                                        and   vw.ASSMNT_QTR=Decode(a.QTR,4,1,a.QTR+1)
                                        and (vw.EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (vw.EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           vw.ASSMNT_YR<Decode(a.QTR,4,a.fiscal_yr+1,a.fiscal_yr)
                                            or 
                                           (vw.ASSMNT_QTR <=(Decode(a.QTR,4,1,a.QTR+1)) and vw.ASSMNT_YR=Decode(a.QTR,4,a.fiscal_yr+1,a.fiscal_yr))
                                          )
                                        ))
                                    ))

                                )
                            or 
                 --------------------------Chnages for Cigar Class----------------------
                           (fd.tobacco_class_id=8
                                and (c2.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                  from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= rp.fiscal_yr+1
                                        and   ASSMNT_QTR=rp.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR <rp.fiscal_yr+1
                                            or 
                                           (ASSMNT_QTR <=rp.quarter and ASSMNT_YR=rp.fiscal_yr+1)
                                          )
                                        )
                                   ) OR(c2.ein in (select vw.ein from permit_excl_status_vw vw,
                            (select  c.ein,ttam.qtr,ttam.fiscal_yr from tu_ttb_amendment ttam 
                                inner join  tu_company c on c.company_id = ttam.ttb_company_id)a 
                                where vw.ein = a.ein and 
                                ((vw.ASSMNT_YR= a.fiscal_yr+1
                                        and   vw.ASSMNT_QTR=a.QTR
                                        and (vw.EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (vw.EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           vw.ASSMNT_YR <a.fiscal_yr+1
                                            or 
                                           (vw.ASSMNT_QTR <=a.QTR and vw.ASSMNT_YR=a.fiscal_yr+1)
                                          )
                                        ))
                            ) )                                                                 
                        ))           

                        GROUP BY
                            tc1.tobacco_class_nm,
                            nvl(sf.form_type_cd, '3852'),
                            rp.fiscal_yr,
                            CASE
                                    WHEN tc1.tobacco_class_nm IS NULL THEN
                                        TO_CHAR(rp.quarter)
                                    WHEN tc1.tobacco_class_nm <> 'Cigars' THEN
                                        TO_CHAR(rp.quarter)
                                    ELSE
                                        '1-4'
                                END,
                            p.permit_type_cd,
                            c2.ein,
                            c2.company_id,
                            pp.zero_flag
                    ) reports ON ( reports.quarter = ingested.fiscal_qtr
                                   OR ( ingested.tobacco_class_nm = 'Cigars'
                                        AND reports.zero_flag = 'Y' ) )
                                 AND reports.fiscal_yr = ingested.fiscal_year
                                 AND ingested.company_id = reports.company_id
                                 AND ( reports.tobacco_class_nm = ingested.tobacco_class_nm
                                       OR reports.zero_flag = 'Y' )
                                 AND reports.permit_type_cd = 'IMPT'
                                 AND reports.form_type_cd = '3852'
                    LEFT OUTER JOIN (
                        SELECT UNIQUE
                            camnd.cbp_company_id   AS cmpy_id,
                            DECODE(camnd.qtr, 5, '1-4', camnd.qtr) AS qtr,
                            DECODE(camnd.acceptance_flag, 'Y', 'FDAaccepted',
                            --4046 Kyle - Ingestedaccepted
                             'I', 'Ingestedaccepted', 'N', 'Amended', 'X', 'DeltaChange', 'E', 'ExcludeChange', 'noAction') acceptance_flag,
                            camnd.in_progress_flag,
                            camnd.fiscal_yr,
                            tc2.tobacco_class_id   AS tobacco_class_id,
                            camnd.fda_delta
                        FROM
                            tu_cbp_amendment   camnd,
                            tu_tobacco_class   tc2
                        WHERE
                            tc2.tobacco_class_id = camnd.tobacco_class_id
                    ) cbpamnd ON cbpamnd.cmpy_id = ingested.company_id
                                 AND cbpamnd.fiscal_yr = ingested.fiscal_year
                                 AND TO_CHAR(cbpamnd.qtr) = ingested.fiscal_qtr
                                 AND cbpamnd.tobacco_class_id = ingested.tobacco_class_id
                GROUP BY
                    ingested.legal_nm,
                    ingested.tobacco_class_nm,
                    TO_CHAR(ingested.fiscal_qtr),
                    nvl(reports.permit_type_cd, 'IMPT'),
                    ingested.company_id,
                    ingested.fiscal_year,
                    cbpamnd.acceptance_flag,
                    cbpamnd.in_progress_flag,
                    ingested.cbp_taxes_paid,
                    ingested.ein,
                    cbpamnd.fda_delta,
                    reports.permit_num,
                    ingested.cbp_summary_taxes_paid
            )
    )
WHERE
    ( deltaexcisetax_flag = 'Review'
      OR ( deltaexcisetax_flag = 'Value'
           AND abs(delta) > 0.99 ) )
    OR delta_change = 'Y'
    OR acceptance_flag = 'DeltaChange'
;
REM INSERTING into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP
SET DEFINE OFF;
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('American Cigar Import Corp','Cigars','1-4',-50170.29,'IMPT',198,2016,null,'N',null,null,'474710295','NA','FLTI40057','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('James Norman Ltd','Pipe','4',-667.84,'IMPT',273,2016,null,'N',null,null,'133781604','NA','NJTI40005;NJTI39','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Jja Distributors Llc','Cigars','1-4',190372.92,'IMPT',125,2016,null,'N',null,null,'010616537','NA','VATI30013;VATI15029','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Neptune Cigars Inc','Cigars','1-4',409.04,'IMPT',311,2016,null,'N',null,null,'450486691','NA','FLTI40013','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Kings Distribution Llc','Cigars','1-4',-7927.93,'IMPT',127,2016,null,'N',null,null,'371651135','NA','FLTI40052;FLTI20006','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Blunt Wrap Usa Inc','Cigars','1-4',54.82,'IMPT',412,2016,null,'N',null,null,'721450488','NA','LATI15003;LATI40001','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Blunt Wrap Usa Inc','Pipe','4',72.26,'IMPT',412,2016,null,'N',null,null,'721450488','NA','LATI15003;LATI40001','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Blunt Wrap Usa Inc','Roll-Your-Own','2',-102280.45,'IMPT',412,2016,null,'N',null,null,'721450488','NA','LATI15003;LATI40001','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Swi-De Llc','Cigars','1-4',-1437621.36,'IMPT',195,2016,null,'N',null,null,'352517016','NA','FLTI40035','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Classic Cigars Llc','Cigars','1-4',837.41,'IMPT',194,2016,null,'N',null,null,'461062736','NA','FLTI40042','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Lbd Coffee Llc','Cigars','1-4',2096.7,'IMPT',259,2016,null,'N',null,null,'710921830','NA','HITI20001;HITI40001;TPHI15001;HITI15001;TPHI15124','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Jrny Trading Company Inc','Roll-Your-Own','4',24041.57,'IMPT',183,2016,null,'N',null,null,'462837958','NA','FLTI40012','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Commonwealth Brands, Inc.','Roll-Your-Own','2',57219.45,'IMPT',354,2016,null,'N',null,null,'611208598','NA','NCTI30013;TPNC630;NCTI15003','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Jja Distributors Llc','Roll-Your-Own','1',-34316.38,'IMPT',125,2016,null,'N',null,null,'010616537','NA','VATI30013;VATI15029','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Tobacco Technology Inc','Cigars','1-4',2.5,'IMPT',382,2016,null,'N',null,null,'521043101','NA','MDTI15000;MDTI40004','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Ciguayo Imports Corporation','Cigars','1-4',24913.02,'IMPT',203,2016,null,'N',null,null,'471138098','NA','FLTI40030','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Aremar Import Inc','Cigars','1-4',1196.33,'IMPT',329,2016,null,'N',null,null,'900113057','NA','FLTI309;FLTI30022','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Family Tobacco Traders Llc','Cigars','1-4',1753611.54,'IMPT',22,2016,null,'N',null,null,'300534590','NA','FLTI40045','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Thompson And Company Of Tampa Inc','Cigars','1-4',-10873.22,'IMPT',381,2016,null,'N',null,null,'590999777','NA','FLTI40055','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Nicks Cigar Company','Cigars','1-4',-2486269.57,'IMPT',313,2016,null,'N',null,null,'650630403','NA','FLTI30056','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Siboney Corporation Dba Blue Mountain Cigars','Cigars','1-4',6399.72,'IMPT',64,2016,null,'N',null,null,'800485980','NA','FLTI15227;FLTI40067','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Carisam Samuel Meisel Fl Inc','Cigars','1-4',705838.25,'IMPT',97,2016,null,'N',null,null,'742478978','NA','FLTI138;FLTI40099','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Davidoff Of Geneva Distribution Incorporated','Pipe','4',755.33,'IMPT',359,2016,null,'N',null,null,'061257625','NA','FLTI40061','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Sky Speed Distributors Inc','Roll-Your-Own','2',77473.74,'IMPT',199,2016,null,'N',null,null,'203733610','NA','GATI40009','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Amvatrade Corp','Cigars','1-4',-110.73,'IMPT',25,2016,null,'N',null,null,'262473417','NA','NYTI15052;NYTI40004','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Piloto Cigars Inc Dba Padron Cigars','Cigarettes','3',-599.4,'IMPT',320,2014,null,'N',null,null,'591399281','NA','FLTI15298;FLTI40056','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Cigar Importers Inc','Cigars','1-4',594634.01,'IMPT',46,2016,null,'N',null,null,'141961316','NA','FLTI15245','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Cigar Importer Of Miami Inc','Cigars','1-4',-5449.08,'IMPT',156,2016,null,'N',null,null,'461985552','NA','FLTI30009','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Jja Distributors Llc','Roll-Your-Own','4',-39075.06,'IMPT',125,2016,null,'N',null,null,'010616537','NA','VATI30013;VATI15029','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Premium Cigars By Don Rigo Inc','Cigars','1-4',-3128.01,'IMPT',136,2016,null,'N',null,null,'453072250','NA','NYTI20002;NYTI30025','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('J C Newman Cigar Company','Cigars','1-4',-1571915.21,'IMPT',276,2016,null,'N',null,null,'590884171','NA','FLTI40050;TPFL611;FLTI5','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Piloto Cigars Inc Dba Padron Cigars','Cigars','1-4',-86281.21,'IMPT',320,2016,null,'N',null,null,'591399281','NA','FLTI15298;FLTI40056','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Scandinavian Tobacco Group Lane Ltd','Roll-Your-Own','4',-29680.66,'IMPT',290,2016,null,'N',null,null,'132855575','NA','GATI3;TPGA19;GATI30004','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Hector Eduardo Suazo Familia','Cigars','1-4',-92021.88,'IMPT',41,2016,null,'N',null,null,'800521210','NA','FLTI15233','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('My Father Cigars Inc','Cigars','1-4',-161154.99,'IMPT',61,2016,null,'N',null,null,'263007638','NA','FLTI40006','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Habaneros Cigars Inc','Cigars','1-4',-8864.36,'IMPT',116,2016,null,'N',null,null,'204727374','NA','FLTI40054','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Ashton Distributors Inc','Pipe','4',-6.24,'IMPT',281,2016,null,'N',null,null,'232728518','NA','PATI9;PATI30010','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Treezwrap Marketing And Dist','Cigars','1-4',0,'IMPT',48,2016,null,'N',null,null,'263847285','NA','FLTI15210','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Scandinavian Tobacco Group Lane Ltd','Roll-Your-Own','3',0,'IMPT',290,2016,null,'N',null,null,'132855575','NA','GATI3;TPGA19;GATI30004','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Oliva Cigar Company','Cigars','1-4',-15101.53,'IMPT',317,2016,null,'N',null,null,'582268398','NA','FLTI15291;FLTI30035','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Anexim Marketing Group Inc','Cigars','1-4',23559.06,'IMPT',99,2016,null,'N',null,null,'451729069','NA','FLTI30033','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Oliva Tobacco Co','Cigars','1-4',402476.04,'IMPT',254,2016,null,'N',null,null,'590383380','NA','FLTI40058','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Humicon Llc Dba Alliance Cigar','Cigars','1-4',-1160.5,'IMPT',269,2016,null,'N',null,null,'113393999','NA','NYTI40003','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Crossfire Cigars Llc','Cigars','1-4',-970.6,'IMPT',73,2016,null,'N',null,null,'273090786','NA','KYTI40001','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Jja Distributors Llc','Roll-Your-Own','3',111719.97,'IMPT',125,2016,null,'N',null,null,'010616537','NA','VATI30013;VATI15029','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Commonwealth Brands, Inc.','Roll-Your-Own','4',72570.48,'IMPT',354,2016,null,'N',null,null,'611208598','NA','NCTI30013;TPNC630;NCTI15003','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Manoa Inc','Roll-Your-Own','3',81018.47,'IMPT',107,2016,null,'N',null,null,'452741155','NA','FLTI30031;FLTI15293','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Bid 1800 Inc','Roll-Your-Own','1',15306.23,'IMPT',134,2016,null,'N',null,null,'460821782','NA','FLTI15327','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Squaresmoke International Llc','Pipe','4',-102270.49,'IMPT',223,2016,null,'N',null,null,'464028052','NA','CATI40019','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Rc Cutler And Company','Cigars','1-4',-62709.98,'IMPT',69,2016,null,'N',null,null,'061141262','NA','FLTI30027','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Manoa Inc','Cigars','1-4',-4925406.55,'IMPT',107,2016,null,'N',null,null,'452741155','NA','FLTI30031;FLTI15293','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Jrny Trading Company Inc','Cigars','1-4',72032.58,'IMPT',183,2016,null,'N',null,null,'462837958','NA','FLTI40012','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Dufry Free Seattle Jv','Roll-Your-Own','2',-4533.29,'IMPT',140,2016,null,'N',null,null,'800864816','NA','WATI00000','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('General Cigar Co Inc','Pipe','4',31.13,'IMPT',252,2016,null,'N',null,null,'133370406','NA','VATI15025;VATI40010','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('American Western Inc','Cigars','1-4',-5949.29,'IMPT',267,2016,null,'N',null,null,'311000505','NA','OHTI40003','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Jja Distributors Llc','Pipe','4',0,'IMPT',125,2016,null,'N',null,null,'010616537','NA','VATI30013;VATI15029','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('A Fuente & Co Inc','Cigars','1-4',91218.95,'IMPT',266,2016,null,'N',null,null,'205352094','NA','FLTI30061','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Commonwealth Brands, Inc.','Roll-Your-Own','3',-57219.45,'IMPT',354,2016,null,'N',null,null,'611208598','NA','NCTI30013;TPNC630;NCTI15003','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Miami Tobacco Import & Export','Cigars','1-4',-68.75,'IMPT',88,2016,null,'N',null,null,'650693434','NA','FLTI30013','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Jja Distributors Llc','Roll-Your-Own','2',-43955.84,'IMPT',125,2016,null,'N',null,null,'010616537','NA','VATI30013;VATI15029','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Epc Cigar Llc','Cigars','1-4',-23604.07,'IMPT',5,2016,null,'N',null,null,'900454996','NA','FLTI15205;FLTI40092','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Sky Speed Distributors Inc','Roll-Your-Own','4',-92907.04,'IMPT',199,2016,null,'N',null,null,'203733610','NA','GATI40009','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Premium Imports Inc Dba La Flor Dominicana','Cigars','1-4',-22505.35,'IMPT',322,2016,null,'N',null,null,'650622977','NA','FLTI40041','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Levy Cafe Inc','Cigars','1-4',9006.01,'IMPT',294,2016,null,'N',null,null,'232837093','NA','PATI30002','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Crowned Heads Llc','Cigars','1-4',-31845.25,'IMPT',105,2016,null,'N',null,null,'274472305','NA','TNTI40002','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Piloto Cigars Inc Dba Padron Cigars','Pipe','3',-1132.05,'IMPT',320,2014,null,'N',null,null,'591399281','NA','FLTI15298;FLTI40056','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Scandinavian Tobacco Group Lane Ltd','Roll-Your-Own','1',-143681.24,'IMPT',290,2016,null,'N',null,null,'132855575','NA','GATI3;TPGA19;GATI30004','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Flor De Gonzalez Inc','Cigars','1-4',392.54,'IMPT',265,2016,null,'N',null,null,'650572130','NA','TPFL828;FLTI40033','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Specialty Tobacco Inc','Roll-Your-Own','2',0,'IMPT',12,2016,null,'N',null,null,'208427179','NA','CATI15095;CATI40017','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('L A Tobacco Inc','Cigars','1-4',-4532219.89,'IMPT',154,2016,null,'N',null,null,'453549478','NA','FLTI20012','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Mj Frias Import Llc','Cigars','1-4',2313.18,'IMPT',186,2016,null,'N',null,null,'461367316','NA','FLTI40020','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Regal Inc','Cigars','1-4',64251.62,'IMPT',187,2016,null,'N',null,null,'461945377','NA','FLTI30015','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Cigar Art Llc','Cigars','1-4',33.7,'IMPT',205,2016,null,'N',null,null,'455365371','NA','TXTI40008;TPTX20002','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Finck Cigar Company Lp','Cigars','1-4',-50.33,'IMPT',348,2016,null,'N',null,null,'740623580','NA','TXTI2;TPTX182','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Santa Clara Cigar Maufacturer Inc','Cigars','1-4',-10594.74,'IMPT',338,2016,null,'N',null,null,'650816355','NA','FLTI30039','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Jre Cigars Inc','Cigars','1-4',817.9,'IMPT',193,2016,null,'N',null,null,'473680596','NA','FLTI30049','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Hail & Cotton Inc','Roll-Your-Own','3',-394.98,'IMPT',242,2016,null,'N',null,null,'610214690','NA','TNTI30004','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Sky Speed Distributors Inc','Roll-Your-Own','3',107131.13,'IMPT',199,2016,null,'N',null,null,'203733610','NA','GATI40009','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Miami Cigar And Company','Cigars','1-4',2749.05,'IMPT',301,2016,null,'N',null,null,'650169907','NA','FLTI30037','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Ashton Distributors Inc','Cigars','1-4',-885.72,'IMPT',281,2016,null,'N',null,null,'232728518','NA','PATI9;PATI30010','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Blunt Wrap Usa Inc','Roll-Your-Own','4',0,'IMPT',412,2016,null,'N',null,null,'721450488','NA','LATI15003;LATI40001','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Las Vegas Cigar Company','Cigars','1-4',-1741.28,'IMPT',292,2016,null,'N',null,null,'880338034','NA','NVTI40003','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Pierre Perales','Cigars','1-4',844.69,'IMPT',9,2016,null,'N',null,null,'204883273','NA','CATI15137','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Manoa Inc','Roll-Your-Own','2',25070.91,'IMPT',107,2016,null,'N',null,null,'452741155','NA','FLTI30031;FLTI15293','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Caribe Trading Corp','Cigars','1-4',-8696.75,'IMPT',145,2016,null,'N',null,null,'454500524','NA','FLTI40075;FLTI20008','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Inter Continental Cigar Corporation','Cigars','1-4',-122873.64,'IMPT',271,2016,null,'N',null,null,'650704561','NA','FLTI40068;FLTI75','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Specialty Tobacco Inc','Cigars','1-4',107119.09,'IMPT',12,2016,null,'N',null,null,'208427179','NA','CATI15095;CATI40017','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Manoa Inc','Roll-Your-Own','4',-58322.4,'IMPT',107,2016,null,'N',null,null,'452741155','NA','FLTI30031;FLTI15293','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Paul J Bush Dba Flatbed Cigar Co','Cigars','1-4',2784.26,'IMPT',394,2016,null,'N',null,null,'113823766','NA','PATI40006','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Gateway Import Management Inc','Roll-Your-Own','1',-28765.43,'IMPT',111,2016,null,'N',null,null,'300694225','NA','FLTI15292','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Altadis Usa Inc','Cigars','1-4',-389669.94,'IMPT',279,2016,null,'N',null,null,'593472656','NA','FLTI30043;TPFL773;TPIL56474;TPGA11223','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Cuba Rica Inc','Cigars','1-4',6441.6,'IMPT',176,2016,null,'N',null,null,'451498232','NA','CATI30006','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Blunt Wrap Usa Inc','Roll-Your-Own','1',129417.35,'IMPT',412,2016,null,'N',null,null,'721450488','NA','LATI15003;LATI40001','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Scandinavian Tobacco Group Lane Ltd','Pipe','4',-9550.73,'IMPT',290,2016,null,'N',null,null,'132855575','NA','GATI3;TPGA19;GATI30004','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Premium Cigar Services Inc','Cigars','1-4',1157.06,'IMPT',211,2016,null,'N',null,null,'743146405','NA','TPNY15061;NYTI30010','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Piloto Cigars Inc Dba Padron Cigars','Cigars','1-4',-2487,'IMPT',320,2014,null,'N',null,null,'591399281','NA','FLTI15298;FLTI40056','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Anexim Marketing Group Inc','Pipe','4',1.13,'IMPT',99,2016,null,'N',null,null,'451729069','NA','FLTI30033','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Kretek Distributors Incorporated','Cigars','1-4',8599.52,'IMPT',288,2016,null,'N',null,null,'770013041','NA','CATI42;CATI30026','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Roberto Duran Llc','Cigars','1-4',-74212.48,'IMPT',144,2016,null,'N',null,null,'460907977','NA','FLTI15325;FLTI40072','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Cano Tobacco Inc','Roll-Your-Own','4',463539.8,'IMPT',113,2016,null,'N',null,null,'453454570','NA','FLTI40032','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Arcon Trading Inc','Cigars','1-4',-5196.09,'IMPT',204,2016,null,'N',null,null,'454974086','NA','FLTI30028','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Daughters & Ryan, Inc. Dba D&R Tobacco & 2 Daughters','Chew','2',-34842.4,'IMPT',358,2007,null,'N',null,null,'561928251','NA','NCTI40011;NCTI20002;TPNC637;TKJK55674','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Sayegh Llc','Cigars','1-4',-1127.31,'IMPT',404,2016,null,'N',null,null,'202274318','NA','TPNV15010;NVTI40004','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Kretek Distributors Incorporated','Pipe','4',0.01,'IMPT',288,2016,null,'N',null,null,'770013041','NA','CATI42;CATI30026','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('General Cigar Co Inc','Cigars','1-4',856649.85,'IMPT',252,2016,null,'N',null,null,'133370406','NA','VATI15025;VATI40010','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Scandinavian Tobacco Group Lane Ltd','Roll-Your-Own','2',-133872.86,'IMPT',290,2016,null,'N',null,null,'132855575','NA','GATI3;TPGA19;GATI30004','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Cano Tobacco Inc','Cigars','1-4',2533152.98,'IMPT',113,2016,null,'N',null,null,'453454570','NA','FLTI40032','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Transphere Inc','Cigars','1-4',80335.95,'IMPT',174,2016,null,'N',null,null,'463084556','NA','NYTI40002','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Gateway Import Management Inc','Roll-Your-Own','2',-10556.16,'IMPT',111,2016,null,'N',null,null,'300694225','NA','FLTI15292','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Dufry Free Seattle Jv','Roll-Your-Own','1',-4315.77,'IMPT',140,2016,null,'N',null,null,'800864816','NA','WATI00000','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('All American Tobacco Llc','Cigars','1-4',841898.7,'IMPT',28,2016,null,'N',null,null,'264250398','NA','FLTI40046','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('The Pipe Guys Llc','Pipe','4',-39.58,'IMPT',171,2016,null,'N',null,null,'461209855','NA','NJTI40003','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Francisco Pla','Cigars','1-4',21620.76,'IMPT',369,2016,null,'N',null,null,'591676887','NA','FLTI30021','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Sayegh Llc','Pipe','4',-92498.13,'IMPT',404,2016,null,'N',null,null,'202274318','NA','TPNV15010;NVTI40004','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Oakbrook Tobaco And Gifts, Inc.','Cigars','1-4',50.33,'IMPT',316,2016,null,'N',null,null,'262733942','NA','ILTI40002','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Alec Bradley Cigar Distributors Inc','Cigars','1-4',-696342.59,'IMPT',278,2016,null,'N',null,null,'510542921','NA','FLTI40048','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Jt Import Inc','Cigars','1-4',40636.26,'IMPT',164,2016,null,'N',null,null,'462818156','NA','FLTI40003','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Sky Speed Distributors Inc','Roll-Your-Own','1',-58508.72,'IMPT',199,2016,null,'N',null,null,'203733610','NA','GATI40009','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Seacoi Investments Inc','Cigars','1-4',-50292.4,'IMPT',339,2016,null,'N',null,null,'300151487','NA','FLTI40039','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Connshade Cigar Corp','Cigars','1-4',1835.06,'IMPT',356,2016,null,'N',null,null,'550799566','NA','FLTI30005','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('S A G Imports Inc','Cigars','1-4',-100105.88,'IMPT',337,2016,null,'N',null,null,'650419750','NA','FLTI30006','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Dominican Republic Cigar Shop','Cigars','1-4',-903.88,'IMPT',299,2016,null,'N',null,null,'043831015','NA','FLTI20001;FLTI40037;FLTI15056','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('A & T Cigarettes Imports Inc','Cigars','1-4',-99047.15,'IMPT',260,2016,null,'N',null,null,'364397421','NA','ILTI15022','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Jose Del Valle Import Inc','Cigars','1-4',9791.07,'IMPT',190,2016,null,'N',null,null,'474895298','NA','FLTI30068','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Nino Vasquez Inc','Cigars','1-4',-9.03,'IMPT',314,2016,null,'N',null,null,'650796808','NA','FLTI33;FLTI40036','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Swedish Match Cigars Inc','Cigars','1-4',-20200.37,'IMPT',332,2016,null,'N',null,null,'541925318','NA','TPAL631;ALTI40001','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Manoa Inc','Roll-Your-Own','1',-63608.67,'IMPT',107,2016,null,'N',null,null,'452741155','NA','FLTI30031;FLTI15293','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Famous Smoke Shop Pa Inc','Cigars','1-4',-13565.19,'IMPT',346,2016,null,'N',null,null,'522065566','NA','PATI22;PATI30015','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Tabacaleras Unidas','Cigars','1-4',-48738.53,'IMPT',124,2016,null,'N',null,null,'455110380','NA','FLTI15319','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Davidoff Of Geneva Distribution Incorporated','Cigars','1-4',6774.93,'IMPT',359,2016,null,'N',null,null,'061257625','NA','FLTI40061','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Tobacco Imports Llc','Cigars','1-4',-163662.68,'IMPT',72,2016,null,'N',null,null,'900606570','NA','FLTI15266;FLTI40074','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Zander-Greg Imports Inc','Cigars','1-4',-8809.25,'IMPT',126,2016,null,'N',null,null,'452574426','NA','CATI15151','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Domestic Tobacco Company','Cigars','1-4',-32453.64,'IMPT',366,2016,null,'N',null,null,'232315667','NA','PATI14;TPPA2444','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Hail & Cotton Inc','Roll-Your-Own','1',-24.78,'IMPT',242,2016,null,'N',null,null,'610214690','NA','TNTI30004','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Los Blancos Cigar Company Inc','Cigars','1-4',-62279.47,'IMPT',117,2016,null,'N',null,null,'455504042','NA','FLTI30017','Review');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Rocky Patel Premium Cigars','Cigars','1-4',-30422.36,'IMPT',270,2016,null,'N',null,null,'561948219','NA','FLTI40043','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Cigars International Inc Dba M&D Wholesale Distributors Inc','Cigars','1-4',-42659.11,'IMPT',353,2016,null,'N',null,null,'342002143','NA','PATI40003;PATI15001','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_IMP (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Commonwealth Brands, Inc.','Roll-Your-Own','1',0,'IMPT',354,2016,null,'N',null,null,'611208598','NA','NCTI30013;TPNC630;NCTI15003','Review');

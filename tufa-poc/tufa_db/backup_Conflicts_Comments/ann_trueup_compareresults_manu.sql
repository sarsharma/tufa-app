--------------------------------------------------------
--  File created - Thursday-January-16-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View ANN_TRUEUP_COMPARERESULTS_MANU
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP12"."ANN_TRUEUP_COMPARERESULTS_MANU" ("LEGAL_NM", "CLASSNAME", "QUARTERX", "DELTA", "PERMIT_TYPE_CD", "COMPANY_ID", "FISCAL_YR", "ACCEPTANCE_FLAG", "DELTA_CHANGE", "FDA_DELTA", "IN_PROGRESS_FLAG", "EIN", "STATUS", "PERMIT_NUM", "DELTAEXCISETAX_FLAG") AS 
  SELECT legal_nm,
           classname,
           quarterx,
           delta,
           permit_type_cd,
           company_id,
           fiscal_yr,
           acceptance_flag,
           delta_change,
           fda_delta,
           CASE
               WHEN in_progress_flag = 'Y' THEN 'InProgress'
               WHEN in_progress_flag = 'N' THEN 'NotInProgress'
               ELSE NULL
           END
               AS in_progress_flag,
           ein,
           'NA'    AS Status,
           permit_num,
           'Value' AS DELTAEXCISETAX_FLAG
      FROM (  SELECT ingested.legal_nm                  AS legal_nm,
                     ingested.ein_num                   AS ein,
                     ingested.tobacco_class_nm          AS classname,
                     TO_CHAR (ingested.fiscal_qtr)      AS quarterx,
                       SUM (NVL (reports.taxes_paid, 0))
                     - NVL (ingested.ttb_taxes_paid, 0)
                         AS delta,
                     NVL (reports.permit_type_cd, 'MANU') AS permit_type_cd,
                     ingested.company_id                AS company_id,
                     ingested.fiscal_yr                 AS fiscal_yr,
                     ttbamnd.acceptance_flag            AS acceptance_flag,
                     ttbamnd.in_progress_flag           AS in_progress_flag,
                     CASE
                         WHEN     ttbamnd.fda_delta IS NOT NULL
                              AND (   ABS (
                                            ttbamnd.fda_delta
                                          - SUM (NVL (reports.taxes_paid, 0))
                                          + NVL (ingested.ttb_taxes_paid, 0)) >
                                          0
                                   OR   SUM (NVL (reports.taxes_paid, 0))
                                      - NVL (ingested.ttb_taxes_paid, 0) = 0)
                         THEN
                             'Y'
                         WHEN     ttbamnd.fda_delta IS NULL
                              AND ttbamnd.acceptance_flag IN
                                      --4046 Kyle - Ingestedaccepted
                                       ('FDAaccepted',
                                        'Ingestedaccepted',
                                        'Amended')
                         THEN
                             'I'
                         ELSE
                             'N'
                     END
                         AS delta_change,
                     ttbamnd.fda_delta                  AS fda_delta,
                     (reports.permit_num || ';' || ingested.permit_num)
                         AS permit_num
                FROM (SELECT c1.legal_nm,
                             ttbingested.ein_num,
                             TO_CHAR (ttbingested.tobacco_class_nm)
                                 AS tobacco_class_nm,
                             ttbingested.fiscal_qtr,
                             NVL (ttbingested.ttb_taxes_paid, 0)
                                 AS ttb_taxes_paid,
                             c1.company_id,
                             a1.fiscal_yr AS fiscal_yr,
                             ttbingested.tobacco_class_id,
                             ttbingested.permit_num
                        FROM tu_annual_trueup a1,
                             (  SELECT abc.ein_num,
                                       abc.tobacco_class_nm,
                                       abc.tobacco_class_id,
                                       abc.fiscal_yr,
                                       abc.fiscal_qtr,
                                       abc.permit_num,
                                       SUM (NVL (abc.ttb_taxes_paid, 0))
                                           AS ttb_taxes_paid
                                  FROM (SELECT /*+ NO_INDEX(TTC) */
                                              ttbcmpy.ein_num,
                                               ttbannualtx.ttb_taxes_paid,
                                               CASE
                                                   WHEN ttc.tobacco_class_nm =
                                                            'Chew-and-Snuff'
                                                   THEN
                                                       'Chew/Snuff'
                                                   WHEN ttc.tobacco_class_nm =
                                                            'Pipe-RYO'
                                                   THEN
                                                       'Pipe/Roll Your Own'
                                                   ELSE
                                                       ttc.tobacco_class_nm
                                               END
                                                   AS tobacco_class_nm,
                                               ttc.tobacco_class_id,
                                               ttbcmpy.fiscal_yr,
                                               CASE
                                                   WHEN ttc.tobacco_class_nm <>
                                                            'Cigars'
                                                   THEN
                                                       TO_CHAR (
                                                           ttbannualtx.ttb_calendar_qtr)
                                                   ELSE
                                                       '1-4'
                                               END
                                                   AS fiscal_qtr,
                                               (SELECT DISTINCT
                                                       LISTAGG (
                                                           ab.permit_num,
                                                           ';')
                                                       WITHIN GROUP (ORDER BY
                                                                         ab.created_dt)
                                                       OVER (
                                                           PARTITION BY ab.TTB_COMPANY_ID)
                                                  FROM tu_ttb_permit ab
                                                 WHERE ab.TTB_COMPANY_ID =
                                                           ttbcmpy.TTB_COMPANY_ID)
                                                   AS permit_num
                                          FROM tu_ttb_company ttbcmpy,
                                               tu_ttb_permit  ttbpermit,
                                               tu_ttb_annual_tax ttbannualtx,
                                               tu_tobacco_class ttc

                                         WHERE     ttbpermit.permit_num IN
                                                       (SELECT permit_num
                                                          FROM tu_permit)
                                               AND ttbannualtx.tobacco_class_id =
                                                       ttc.tobacco_class_id
                                               AND ttbannualtx.ttb_permit_id =
                                                       ttbpermit.ttb_permit_id
                                               AND ttbpermit.ttb_company_id =
                                                       ttbcmpy.ttb_company_id

                                                 AND ttbannualtx.ttb_annual_tax_id  not in (select excl.ttb_annual_tax_id from tu_ttb_ingested_excl_vw excl 
                                                 where excl.PERM_EXCL_AUDIT_ID is not null and excl.PERM_EXCL_AUDIT_ID not in 
                                                 (select vw.perm_excl_audit_id from permit_excl_status_vw vw,
                                                            (select  c.ein,ttam.qtr,ttam.fiscal_yr from tu_ttb_amendment ttam 
                                                                inner join  tu_company c on c.company_id = ttam.ttb_company_id)a 
                                                                    --where a.fiscal_yr = vw.ASSMNT_yr and vw.ASSMNT_QTR = QTR and vw.ein = a.ein
                                                            ))
                                                     )
                                       abc
                              GROUP BY                           --1,3,4,5,6,7
                                      abc.ein_num,
                                       abc.tobacco_class_nm,
                                       abc.tobacco_class_id,
                                       abc.fiscal_yr,
                                       abc.fiscal_qtr,
                                       abc.permit_num) ttbingested,
                             tu_company      c1
                       WHERE     ttbingested.ein_num = c1.ein
                             AND ttbingested.fiscal_yr = a1.fiscal_yr) ingested
                     INNER JOIN
                     (  SELECT SUM (NVL (fd.taxes_paid, 0)) AS taxes_paid,
                               CASE
                                   WHEN tc1.tobacco_class_nm IN ('Chew', 'Snuff')
                                   THEN
                                       'Chew/Snuff'
                                   WHEN tc1.tobacco_class_nm IN
                                            ('Roll-Your-Own', 'Pipe')
                                   THEN
                                       'Pipe/Roll Your Own'
                                   ELSE
                                       tc1.tobacco_class_nm
                               END
                                   AS tobacco_class_nm,
                               NVL (sf.form_type_cd, '3852') AS form_type_cd,
                               rp.fiscal_yr,
                               CASE
                                   WHEN tc1.tobacco_class_nm IS NULL
                                   THEN
                                       TO_CHAR (rp.quarter)
                                   WHEN tc1.tobacco_class_nm <> 'Cigars'
                                   THEN
                                       TO_CHAR (rp.quarter)
                                   ELSE
                                       '1-4'
                               END
                                   AS quarter,
                               p.permit_type_cd,
                               c2.ein,
                               c2.company_id,
                               pp.zero_flag,
                               (SELECT DISTINCT
                                       LISTAGG (a.permit_num, ';')
                                           WITHIN GROUP (ORDER BY a.created_dt)
                                           OVER (PARTITION BY a.COMPANY_ID)
                                  FROM tu_permit a
                                 WHERE a.company_id = c2.company_id)
                                   AS permit_num
                          FROM tu_company c2
                               INNER JOIN tu_permit p
                                   ON c2.company_id = p.company_id
                               INNER JOIN tu_permit_period pp
                                   ON p.permit_id = pp.permit_id
                               INNER JOIN tu_rpt_period rp
                                   ON rp.period_id = pp.period_id
                               INNER JOIN tu_annual_trueup a2
                                   ON a2.fiscal_yr = rp.fiscal_yr
                               INNER JOIN tu_period_status ps
                                   ON     pp.period_id = ps.period_id
                                      AND pp.permit_id = ps.permit_id
                               LEFT JOIN tu_submitted_form sf
                                   ON     sf.permit_id = pp.permit_id
                                      AND sf.period_id = pp.period_id
                               LEFT JOIN tu_form_detail fd
                                   ON sf.form_id = fd.form_id
                               LEFT JOIN tu_tobacco_class tc1
                                   ON fd.tobacco_class_id = tc1.tobacco_class_id
------------------------------------Changes for Story#4964-----------------------------
                             Where       
                             --------------------------Changes for Non Cigar Classes----------------
                             ((fd.tobacco_class_id<>8 and (c2.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(rp.quarter,4,rp.fiscal_yr+1,rp.fiscal_yr)
                                        and   ASSMNT_QTR=Decode(rp.quarter,4,1,rp.quarter+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(rp.quarter,4,rp.fiscal_yr+1,rp.fiscal_yr)
                                            or 
                                           (ASSMNT_QTR <=(Decode(rp.quarter,4,1,rp.quarter+1)) and ASSMNT_YR=Decode(rp.quarter,4,rp.fiscal_yr+1,rp.fiscal_yr))
                                          )
                                        )
                                   ) OR (c2.ein in (select vw.ein from permit_excl_status_vw vw,
                                        (select  c.ein,ttam.qtr,ttam.fiscal_yr from tu_ttb_amendment ttam 
                                            inner join  tu_company c on c.company_id = ttam.ttb_company_id)a 
                                             where vw.ein = a.ein and 
                                             ((vw.ASSMNT_YR= Decode(a.QTR,4,a.fiscal_yr+1,a.fiscal_yr)
                                        and   vw.ASSMNT_QTR=Decode(a.QTR,4,1,a.QTR+1)
                                        and (vw.EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (vw.EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           vw.ASSMNT_YR<Decode(a.QTR,4,a.fiscal_yr+1,a.fiscal_yr)
                                            or 
                                           (vw.ASSMNT_QTR <=(Decode(a.QTR,4,1,a.QTR+1)) and vw.ASSMNT_YR=Decode(a.QTR,4,a.fiscal_yr+1,a.fiscal_yr))
                                          )
                                        ))
                                    ))

                                )
                             or 
                 --------------------------Chnages for Cigar Class----------------------
                           (fd.tobacco_class_id=8
                                and (c2.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                  from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= rp.fiscal_yr+1
                                        and   ASSMNT_QTR=rp.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR <rp.fiscal_yr+1
                                            or 
                                           (ASSMNT_QTR <=rp.quarter and ASSMNT_YR=rp.fiscal_yr+1)
                                          )
                                        )
                                   ) OR(c2.ein in (select vw.ein from permit_excl_status_vw vw,
                            (select  c.ein,ttam.qtr,ttam.fiscal_yr from tu_ttb_amendment ttam 
                                inner join  tu_company c on c.company_id = ttam.ttb_company_id)a 
                                where vw.ein = a.ein and 
                                ((vw.ASSMNT_YR= a.fiscal_yr+1
                                        and   vw.ASSMNT_QTR=a.QTR
                                        and (vw.EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (vw.EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           vw.ASSMNT_YR <a.fiscal_yr+1
                                            or 
                                           (vw.ASSMNT_QTR <=a.QTR and vw.ASSMNT_YR=a.fiscal_yr+1)
                                          )
                                        ))
                            ) )                                                                 
                        ))                   


                      GROUP BY CASE
                                   WHEN tc1.tobacco_class_nm IN
                                            ('Chew', 'Snuff')
                                   THEN
                                       'Chew/Snuff'
                                   WHEN tc1.tobacco_class_nm IN
                                            ('Roll-Your-Own', 'Pipe')
                                   THEN
                                       'Pipe/Roll Your Own'
                                   ELSE
                                       tc1.tobacco_class_nm
                               END,
                               sf.form_type_cd,
                               rp.fiscal_yr,
                               CASE
                                   WHEN tc1.tobacco_class_nm IS NULL
                                   THEN
                                       TO_CHAR (rp.quarter)
                                   WHEN tc1.tobacco_class_nm <> 'Cigars'
                                   THEN
                                       TO_CHAR (rp.quarter)
                                   ELSE
                                       '1-4'
                               END,
                               p.permit_type_cd,
                               c2.ein,
                               c2.company_id,
                               pp.zero_flag) reports
                         ON     (   reports.quarter = ingested.fiscal_qtr
                                 OR     ingested.tobacco_class_nm = 'Cigars'
                                    AND reports.zero_flag = 'Y')
                            AND reports.fiscal_yr = ingested.fiscal_yr
                            AND ingested.company_id = reports.company_id
                            AND (   reports.tobacco_class_nm =
                                        ingested.tobacco_class_nm
                                 OR reports.zero_flag = 'Y')
                            AND reports.permit_type_cd = 'MANU'
                            AND reports.form_type_cd = '3852'
                     LEFT OUTER JOIN
                     (  SELECT UNIQUE
                               tamnd.ttb_company_id                AS cmpy_id,
                               DECODE (tamnd.qtr, 5, '1-4', tamnd.qtr) AS qtr,
                               DECODE (tamnd.acceptance_flag,
                                       'Y', 'FDAaccepted',
                                       --4046 Kyle - Ingestedaccepted
                                       'I', 'Ingestedaccepted',
                                       'N', 'Amended',
                                       'X', 'DeltaChange',
                                       'E', 'ExcludeChange',
                                       'noAction')
                                   acceptance_flag,
                               tamnd.in_progress_flag,
                               tamnd.fiscal_yr,
                               CASE
                                   WHEN tc2.tobacco_class_nm IN ('Chew',
                                                                 'Snuff',
                                                                 'Pipe',
                                                                 'Roll-Your-Own')
                                   THEN
                                       tc2.parent_class_id
                                   ELSE
                                       tc2.tobacco_class_id
                               END
                                   AS tobacco_class_id,
                               tamnd.fda_delta
                          FROM tu_ttb_amendment tamnd, tu_tobacco_class tc2
                         WHERE tc2.tobacco_class_id = tamnd.tobacco_class_id
                      GROUP BY tamnd.ttb_company_id,
                               DECODE (tamnd.qtr, 5, '1-4', tamnd.qtr),
                               DECODE (tamnd.acceptance_flag,
                                       'Y', 'FDAaccepted',
                                       --4046 Kyle - Ingestedaccepted
                                       'I', 'Ingestedaccepted',
                                       'N', 'Amended',
                                       'X', 'DeltaChange',
                                       'E', 'ExcludeChange',
                                       'noAction'),
                               tamnd.in_progress_flag,
                               tamnd.fiscal_yr,
                               CASE
                                   WHEN tc2.tobacco_class_nm IN
                                            ('Chew',
                                             'Snuff',
                                             'Pipe',
                                             'Roll-Your-Own')
                                   THEN
                                       tc2.parent_class_id
                                   ELSE
                                       tc2.tobacco_class_id
                               END,
                               tamnd.fda_delta) ttbamnd
                         ON     ttbamnd.cmpy_id = ingested.company_id
                            AND ttbamnd.fiscal_yr = ingested.fiscal_yr
                            AND TO_CHAR (ttbamnd.qtr) = ingested.fiscal_qtr
                            AND ttbamnd.tobacco_class_id =
                                    ingested.tobacco_class_id
            GROUP BY ingested.legal_nm,
                     ingested.tobacco_class_nm,
                     TO_CHAR (ingested.fiscal_qtr),
                     NVL (reports.permit_type_cd, 'MANU'),
                     ingested.company_id,
                     ingested.fiscal_yr,
                     ttbamnd.acceptance_flag,
                     ttbamnd.in_progress_flag,
                     ingested.ttb_taxes_paid,
                     INGESTED.ein_num,
                     ttbamnd.fda_delta,
                     (reports.permit_num || ';' || ingested.permit_num))
     WHERE    ABS (delta) > 0.99
           OR delta_change = 'Y'
           OR acceptance_flag = 'DeltaChange'
;
REM INSERTING into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU
SET DEFINE OFF;
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Altadis Usa Inc','Chew/Snuff','2',526126.5,'MANU',279,2007,'FDAaccepted','N',526126.5,'NotInProgress','593472656','NA','FLTI30043;TPFL773;TPIL56474;TPGA11223;TPGA11223;TPFL773','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Scandinavian Tobacco Group Lane Ltd','Pipe/Roll Your Own','4',-972183.12,'MANU',290,2015,null,'N',null,null,'132855575','NA','GATI3;TPGA19;GATI30004;TPGA19','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Great Swamp Enterprises Inc','Cigarettes','4',57678.18,'MANU',79,2015,null,'N',null,null,'273504615','NA','TPNY15046;TPNY15046','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Cross Tobacco Company Inc','Pipe/Roll Your Own','1',536232.68,'MANU',342,2014,null,'N',null,null,'010670756','NA','TPPA2448;TPPA15000;TPPA2448;TPPA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Philip Morris Usa Inc','Chew/Snuff','4',16.03,'MANU',258,2015,null,'N',null,null,'131607658','NA','TPVA7;VATI6;VATI30006;TPVA7','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('U S Flue Cured Tobacco Growers Inc','Cigarettes','1',1186257.97,'MANU',385,2015,null,'N',null,null,'201259823','NA','TPNC15000;TPNC15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('King Mountain Tobacco Company Inc','Cigarettes','4',482906.28,'MANU',286,2015,null,'N',null,null,'203874581','NA','TPWA15000;TPWA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('J Conrad Seneca Dba Six Nations Manufacturing','Cigarettes','4',219841.44,'MANU',400,2015,null,'N',null,null,'161531252','NA','TPNY15033;TPNY15033','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Commonwealth Brands, Inc.','Cigarettes','4',-1005291.42,'MANU',354,2015,null,'N',null,null,'611208598','NA','NCTI30013;TPNC630;NCTI15003;TPNC630','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('National Tobacco Company Lp','Pipe/Roll Your Own','4',-174883.41,'MANU',307,2015,null,'N',null,null,'611133037','NA','KYTI30001;TPKY41;TPKY41','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Xcaliber International Ltd Llc','Cigarettes','3',603.96,'MANU',387,2015,null,'N',null,null,'731613028','NA','TPOK3;TPOK3','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('S & M Brands Inc','Pipe/Roll Your Own','2',-139029.31,'MANU',335,2015,null,'N',null,null,'541701410','NA','TPVA33;TPVA33','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('New Manu 12','Pipe/Roll Your Own','1',12122432.5,'MANU',1090,2007,'ExcludeChange','N',12122432.5,'NotInProgress','623332057','NA','TPNC960;TPNC960','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Cross Tobacco Company Inc','Pipe/Roll Your Own','3',-1615294.55,'MANU',342,2015,null,'N',null,null,'010670756','NA','TPPA2448;TPPA15000;TPPA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Domestic Tobacco Company','Chew/Snuff','2',1548.34,'MANU',366,2015,null,'N',null,null,'232315667','NA','PATI14;TPPA2444;TPPA2444','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Cross Tobacco Company Inc','Cigarettes','2',2888.03,'MANU',342,2014,null,'N',null,null,'010670756','NA','TPPA2448;TPPA15000;TPPA2448;TPPA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('U.S. SMOKELESS TOBACCO COMPANY LLC','Chew/Snuff','4',414645.6,'MANU',255,2015,null,'N',null,null,'061532188','NA','TPIL39;TPTN32;TPVA15011;TPTN32;TPIL39','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Farmers Tobacco Company Of Cynthiana Inc','Pipe/Roll Your Own','2',-404213.13,'MANU',347,2015,null,'N',null,null,'611295592','NA','TPKY45;TPKY45','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Lake Erie Tobacco Company','Chew/Snuff','2',835.78,'MANU',289,2015,null,'N',null,null,'760815741','NA','TPNY15011;NYTI15088;NYTI30008;TPNY12345;TPNY54321;TPNY15011','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Susan M Jesmer Dba Native Trading Associates','Cigarettes','1',3297017.64,'MANU',344,2015,null,'N',null,null,'202137441','NA','TPNY15002;TPNY15002','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Truth And Liberty Manufacturing Company','Pipe/Roll Your Own','2',-20209.4,'MANU',384,2015,null,'N',null,null,'680444745','NA','TPCA78;TPCA78','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Nasco Products Llc','Cigarettes','2',-2013.2,'MANU',45,2015,null,'N',null,null,'264788667','NA','TPNC15033;NCTI40008;TPNC15033','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Cross Tobacco Company Inc','Chew/Snuff','2',-17794.82,'MANU',342,2014,null,'N',null,null,'010670756','NA','TPPA2448;TPPA15000;TPPA2448;TPPA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Truth And Liberty Manufacturing Company','Pipe/Roll Your Own','3',-14967.71,'MANU',384,2015,null,'N',null,null,'680444745','NA','TPCA78;TPCA78','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Bailey Tobacco Corporation','Chew/Snuff','1',-6697.29,'MANU',261,2002,null,'N',null,null,'541921362','NA','TPVA37;TPVA37','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Commonwealth Brands, Inc.','Cigarettes','2',-1807048.32,'MANU',354,2015,null,'N',null,null,'611208598','NA','NCTI30013;TPNC630;NCTI15003;TPNC630','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Rock River Mfg','Cigarettes','2',281.85,'MANU',65,2015,null,'N',null,null,'352368880','NA','NETI15002;TPNE15000;NETI40001;TPNE15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Truth And Liberty Manufacturing Company','Pipe/Roll Your Own','1',-19666.61,'MANU',384,2015,null,'N',null,null,'680444745','NA','TPCA78;TPCA78','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Wind River Tobacco Company Llc','Pipe/Roll Your Own','2',-18365.84,'MANU',361,2015,null,'N',null,null,'830335018','NA','TPTN15001;TNTI15013;TNTI30005;NCTI30020;TPNC15038;TPTN20010;TPTN15022;TPTN20016;TPTN15001','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Itg Brands Llc','Cigarettes','3',-701197.56,'MANU',296,2015,'noAction','N',null,'InProgress','942994213','NA','TPNC15036;TPNC19999;TPNC15036','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('U S Flue Cured Tobacco Growers Inc','Pipe/Roll Your Own','1',-423719.47,'MANU',385,2015,null,'N',null,null,'201259823','NA','TPNC15000;TPNC15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Geoffrey L Griffin  The Bully Boy','Pipe/Roll Your Own','2',11.79,'MANU',118,2015,null,'N',null,null,'149464699','NA','TPSC15002;TPSC15002','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Commonwealth Brands, Inc.','Cigarettes','1',-1710112.74,'MANU',354,2015,null,'N',null,null,'611208598','NA','NCTI30013;TPNC630;NCTI15003;TPNC630','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('King Mountain Tobacco Company Inc','Pipe/Roll Your Own','4',4571.91,'MANU',286,2015,null,'N',null,null,'203874581','NA','TPWA15000;TPWA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Pierce National Enterprises Llc','Cigarettes','1',28912.1,'MANU',104,2015,null,'N',null,null,'273638963','NA','TPNY15049;TPNY15049','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('U.S. SMOKELESS TOBACCO COMPANY LLC','Chew/Snuff','2',1533.22,'MANU',255,2015,null,'N',null,null,'061532188','NA','TPIL39;TPTN32;TPVA15011;TPTN32;TPIL39','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Philip Morris Usa Inc','Cigarettes','4',8296864.99,'MANU',258,2015,null,'N',null,null,'131607658','NA','TPVA7;VATI6;VATI30006;TPVA7','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('S & M Brands Inc','Pipe/Roll Your Own','1',-110696.68,'MANU',335,2015,null,'N',null,null,'541701410','NA','TPVA33;TPVA33','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Rock River Mfg','Cigarettes','4',53873.23,'MANU',65,2015,null,'N',null,null,'352368880','NA','NETI15002;TPNE15000;NETI40001;TPNE15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Farmers Tobacco Company Of Cynthiana Inc','Pipe/Roll Your Own','4',-365908.36,'MANU',347,2015,null,'N',null,null,'611295592','NA','TPKY45;TPKY45','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Great Swamp Enterprises Inc','Cigarettes','1',-10,'MANU',79,2015,null,'N',null,null,'273504615','NA','TPNY15046;TPNY15046','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Pierce National Enterprises Llc','Cigarettes','2',10267.32,'MANU',104,2015,null,'N',null,null,'273638963','NA','TPNY15049;TPNY15049','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Farmers Tobacco Company Of Cynthiana Inc','Pipe/Roll Your Own','1',-398556.6,'MANU',347,2015,null,'N',null,null,'611295592','NA','TPKY45;TPKY45','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('U S Flue Cured Tobacco Growers Inc','Pipe/Roll Your Own','4',-487051.13,'MANU',385,2015,null,'N',null,null,'201259823','NA','TPNC15000;TPNC15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Cross Tobacco Company Inc','Cigarettes','1',-121714.97,'MANU',342,2014,null,'N',null,null,'010670756','NA','TPPA2448;TPPA15000;TPPA2448;TPPA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Cross Tobacco Company Inc','Pipe/Roll Your Own','4',4062487.66,'MANU',342,2014,null,'N',null,null,'010670756','NA','TPPA2448;TPPA15000;TPPA2448;TPPA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('National Tobacco Company Lp','Pipe/Roll Your Own','2',-176588.45,'MANU',307,2015,null,'N',null,null,'611133037','NA','KYTI30001;TPKY41;TPKY41','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('S & M Brands Inc','Cigarettes','1',923203.19,'MANU',335,2015,null,'N',null,null,'541701410','NA','TPVA33;TPVA33','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Scandinavian Tobacco Group Lane Ltd','Pipe/Roll Your Own','2',-850545.87,'MANU',290,2015,null,'N',null,null,'132855575','NA','GATI3;TPGA19;GATI30004;TPGA19','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Truth And Liberty Manufacturing Company','Pipe/Roll Your Own','4',-17748.53,'MANU',384,2015,null,'N',null,null,'680444745','NA','TPCA78;TPCA78','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Scandinavian Tobacco Group Lane Ltd','Pipe/Roll Your Own','1',-319824.44,'MANU',290,2015,null,'N',null,null,'132855575','NA','GATI3;TPGA19;GATI30004;TPGA19','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Wind River Tobacco Company Llc','Pipe/Roll Your Own','1',-16141.71,'MANU',361,2015,null,'N',null,null,'830335018','NA','TPTN15001;TNTI15013;TNTI30005;NCTI30020;TPNC15038;TPTN20010;TPTN15022;TPTN20016;TPTN15001','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Cross Tobacco Company Inc','Pipe/Roll Your Own','4',-1794978.11,'MANU',342,2015,null,'N',null,null,'010670756','NA','TPPA2448;TPPA15000;TPPA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Cheyenne International Llc','Pipe/Roll Your Own','1',-122776.44,'MANU',351,2015,null,'N',null,null,'010731357','NA','TPNC645;NCTI40001;TPNC645','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Philip Morris Usa Inc','Chew/Snuff','3',4.43,'MANU',258,2015,null,'N',null,null,'131607658','NA','TPVA7;VATI6;VATI30006;TPVA7','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Top Tobacco L.P.','Pipe/Roll Your Own','3',-4206848.07,'MANU',256,2015,null,'N',null,null,'363724117','NA','NCTI40004;TPNC632;NCTI9;TPNC632','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Farmers Tobacco Company Of Cynthiana Inc','Pipe/Roll Your Own','3',-420223,'MANU',347,2015,null,'N',null,null,'611295592','NA','TPKY45;TPKY45','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('J Conrad Seneca Dba Six Nations Manufacturing','Pipe/Roll Your Own','4',-21606.95,'MANU',400,2015,null,'N',null,null,'161531252','NA','TPNY15033;TPNY15033','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Cross Tobacco Company Inc','Chew/Snuff','1',-41718.25,'MANU',342,2014,null,'N',null,null,'010670756','NA','TPPA2448;TPPA15000;TPPA2448;TPPA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Top Tobacco L.P.','Pipe/Roll Your Own','2',-5124563.08,'MANU',256,2015,null,'N',null,null,'363724117','NA','NCTI40004;TPNC632;NCTI9;TPNC632','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('S & M Brands Inc','Pipe/Roll Your Own','4',-122719.22,'MANU',335,2015,null,'N',null,null,'541701410','NA','TPVA33;TPVA33','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Domestic Tobacco Company','Chew/Snuff','3',-2.43,'MANU',366,2015,null,'N',null,null,'232315667','NA','PATI14;TPPA2444;TPPA2444','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Domestic Tobacco Company','Chew/Snuff','4',1014.78,'MANU',366,2015,null,'N',null,null,'232315667','NA','PATI14;TPPA2444;TPPA2444','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('U S Flue Cured Tobacco Growers Inc','Pipe/Roll Your Own','3',-622733.72,'MANU',385,2015,null,'N',null,null,'201259823','NA','TPNC15000;TPNC15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('U S Flue Cured Tobacco Growers Inc','Pipe/Roll Your Own','2',-559801.91,'MANU',385,2015,null,'N',null,null,'201259823','NA','TPNC15000;TPNC15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Philip Morris Usa Inc','Cigarettes','3',301.9,'MANU',258,2015,null,'N',null,null,'131607658','NA','TPVA7;VATI6;VATI30006;TPVA7','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('National Tobacco Company Lp','Pipe/Roll Your Own','3',-175641.44,'MANU',307,2015,null,'N',null,null,'611133037','NA','KYTI30001;TPKY41;TPKY41','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('S & M Brands Inc','Cigarettes','4',3.52,'MANU',335,2015,null,'N',null,null,'541701410','NA','TPVA33;TPVA33','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('National Tobacco Company Lp','Pipe/Roll Your Own','1',-165324.2,'MANU',307,2015,null,'N',null,null,'611133037','NA','KYTI30001;TPKY41;TPKY41','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('U.S. SMOKELESS TOBACCO COMPANY LLC','Chew/Snuff','1',32.81,'MANU',255,2015,null,'N',null,null,'061532188','NA','TPIL39;TPTN32;TPVA15011;TPTN32;TPIL39','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('S & M Brands Inc','Pipe/Roll Your Own','3',-132141.94,'MANU',335,2015,null,'N',null,null,'541701410','NA','TPVA33;TPVA33','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Rock River Mfg','Cigarettes','1',162250.84,'MANU',65,2015,null,'N',null,null,'352368880','NA','NETI15002;TPNE15000;NETI40001;TPNE15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Top Tobacco L.P.','Pipe/Roll Your Own','1',-3943174.43,'MANU',256,2015,null,'N',null,null,'363724117','NA','NCTI40004;TPNC632;NCTI9;TPNC632','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Commonwealth Brands, Inc.','Cigarettes','3',-822291.54,'MANU',354,2015,null,'N',null,null,'611208598','NA','NCTI30013;TPNC630;NCTI15003;TPNC630','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Wind River Tobacco Company Llc','Pipe/Roll Your Own','3',-16471.33,'MANU',361,2015,null,'N',null,null,'830335018','NA','TPTN15001;TNTI15013;TNTI30005;NCTI30020;TPNC15038;TPTN20010;TPTN15022;TPTN20016;TPTN15001','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Q3 AND Q4 DATA IN TUFA','Cigarettes','2',3973.73,'MANU',2170,2014,null,'N',null,null,'987456321','NA','HFEX4946;HFEX4946','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Scandinavian Tobacco Group Lane Ltd','Pipe/Roll Your Own','3',-939443.82,'MANU',290,2015,null,'N',null,null,'132855575','NA','GATI3;TPGA19;GATI30004;TPGA19','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Rock River Mfg','Cigarettes','3',90020.24,'MANU',65,2015,null,'N',null,null,'352368880','NA','NETI15002;TPNE15000;NETI40001;TPNE15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Cheyenne International Llc','Pipe/Roll Your Own','2',-119422.93,'MANU',351,2015,null,'N',null,null,'010731357','NA','TPNC645;NCTI40001;TPNC645','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Top Tobacco L.P.','Pipe/Roll Your Own','4',-4373243.15,'MANU',256,2015,null,'N',null,null,'363724117','NA','NCTI40004;TPNC632;NCTI9;TPNC632','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Tantus Tobacco Llc','Cigarettes','3',906654.69,'MANU',380,2015,null,'N',null,null,'364523883','NA','TPKY15000;TPKY15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Angel Maria Hernandez','Chew/Snuff','1',253577,'MANU',630,2007,'Ingestedaccepted','N',253577,'NotInProgress','133710237','NA','TPNY166;TPFL2001;TPFL2001','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Cross Tobacco Company Inc','Cigars','1-4',-1012557.59,'MANU',342,2014,null,'N',null,null,'010670756','NA','TPPA2448;TPPA15000;TPPA2448;TPPA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Itg Brands Llc','Cigarettes','4',-622825.54,'MANU',296,2015,null,'N',null,null,'942994213','NA','TPNC15036;TPNC19999;TPNC15036','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Philip Morris Usa Inc','Pipe/Roll Your Own','4',-363.28,'MANU',258,2015,null,'N',null,null,'131607658','NA','TPVA7;VATI6;VATI30006;TPVA7','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Wind River Tobacco Company Llc','Pipe/Roll Your Own','4',-18893.03,'MANU',361,2015,null,'N',null,null,'830335018','NA','TPTN15001;TNTI15013;TNTI30005;NCTI30020;TPNC15038;TPTN20010;TPTN15022;TPTN20016;TPTN15001','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Swisher International Inc','Chew/Snuff','2',219362.51,'MANU',345,2015,null,'N',null,null,'591150320','NA','FLTI11;TPWV42;TPFL110;FLTI30065;TPWV42;TPFL110','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Cross Tobacco Company Inc','Pipe/Roll Your Own','2',-1816678.5,'MANU',342,2015,null,'N',null,null,'010670756','NA','TPPA2448;TPPA15000;TPPA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('U.S. SMOKELESS TOBACCO COMPANY LLC','Chew/Snuff','3',68.89,'MANU',255,2015,null,'N',null,null,'061532188','NA','TPIL39;TPTN32;TPVA15011;TPTN32;TPIL39','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Us Smokeless Tobacco Products Llc','Chew/Snuff','4',523.33,'MANU',51,2015,null,'N',null,null,'272331972','NA','TPVA15007;TPVA15007','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Cheyenne International Llc','Pipe/Roll Your Own','4',-125089.33,'MANU',351,2015,null,'N',null,null,'010731357','NA','TPNC645;NCTI40001;TPNC645','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Cross Tobacco Company Inc','Pipe/Roll Your Own','1',-1808199.26,'MANU',342,2015,null,'N',null,null,'010670756','NA','TPPA2448;TPPA15000;TPPA15000','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Cheyenne International Llc','Pipe/Roll Your Own','3',-137090.35,'MANU',351,2015,null,'N',null,null,'010731357','NA','TPNC645;NCTI40001;TPNC645','Value');
Insert into CTP_TUFA_MVP12.ANN_TRUEUP_COMPARERESULTS_MANU (LEGAL_NM,CLASSNAME,QUARTERX,DELTA,PERMIT_TYPE_CD,COMPANY_ID,FISCAL_YR,ACCEPTANCE_FLAG,DELTA_CHANGE,FDA_DELTA,IN_PROGRESS_FLAG,EIN,STATUS,PERMIT_NUM,DELTAEXCISETAX_FLAG) values ('Southern Cross Tobacco Company Inc','Pipe/Roll Your Own','2',17726.62,'MANU',342,2014,null,'N',null,null,'010670756','NA','TPPA2448;TPPA15000;TPPA2448;TPPA15000','Value');

--------------------------------------------------------
--  File created - Thursday-January-16-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure LOAD_CBP_ENTRY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP12"."LOAD_CBP_ENTRY" (p_fiscal_yr in NUMBER) AS
BEGIN
DECLARE
  v_fiscal_yr NUMBER(4) := p_fiscal_yr;
  CURSOR load_entry IS
  SELECT entry_no, line_no,
        CASE
             WHEN
                  TO_CHAR( protected_to_date(
                                entry_summ_date,
                                'YYYYMMDD'
                            ),'YYYY') = fiscal_yr

                            AND (
                                    TO_CHAR(protected_to_date(entry_summ_date,'YYYYMMDD'),'MM') IN (
                                        '10','11','12'
                                    )
                              )
                        THEN NULL
                        ELSE  protected_to_date(
                            entry_summ_date,
                            'YYYYMMDD'
                        )
                    END
                AS entry_summ_date,
         protected_to_date(entry_date, 'YYYYMMDD') entry_date,
         protected_to_date(summary_entry_summary_date, 'YYYYMMDD') summary_entry_summary_date,
         hts_cd,
         uom1,
         qty1,
         estimated_tax,
         decode(instr(consignee_ein,'-'), 3, substr(consignee_ein, 1,2)||substr(consignee_ein, 4,7),
                                          4, substr(consignee_ein, 1,3)||substr(consignee_ein, 5,2)||substr(consignee_ein, 8,4)) consignee_ein,
         decode(instr(importer_ein,'-'), 3, substr(importer_ein, 1,2)||substr(importer_ein, 4,7),
                                         4, substr(importer_ein, 1,3)||substr(importer_ein, 5,2)||substr(importer_ein, 8,4)) importer_ein,
         importer_name,
         cbp_upload_id,
         entry_type,
-------------------Adding additional Columns for Story# CTPTUFA-4315-----------------------------------------------
         SUMMARIZED_FILE_TAX,
         DETAILS_ENTRY_SUMMARY_DATE,
         SUMMARY_ENTRY_SUMMARY_NUMBER,
         MIXED_TOBACCO_TYPE_FLAG
        from tu_cbp_upload
    where fiscal_yr = v_fiscal_yr
    order by cbp_upload_id asc;
  v_importerid NUMBER;
  v_assignedmonth CHAR(3);
  v_assigneddate DATE;
  v_fiscal_qtr NUMBER(1);
  v_tobaccoclass_id NUMBER;
  v_default_hts_flag CHAR(1) := 'N';
  v_esd_month CHAR(3);
  v_esd_year  NUMBER;
  v_ed_month CHAR(3);
  v_ed_year  NUMBER;
  v_cbp_entry_id NUMBER;
  v_perm_excl_audit_id number;
  v_cmp_all_delta_id   number;
  v_comment_seq number;
  v_exists varchar2(1);
  v_tobaccoclass_nm varchar2(30);
  v_cigar_qtr varchar2(5);
  v_ein varchar2(15);
  
  BEGIN
    -- loop through the cursor
--    DBMS_OUTPUT.PUT_LINE('starting procedure');
    FOR rec IN load_entry LOOP
    -- get the importer id
--    DBMS_OUTPUT.PUT_LINE('importer ein=' || rec.importer_ein || ' consignee_ein=' || rec.consignee_ein);
    BEGIN
      SELECT cbp_importer_id
        INTO v_importerid
      FROM
      (SELECT cbp_importer_id from tu_cbp_importer
      WHERE NVL(consignee_ein, 'NULL')=NVL(rec.consignee_ein, 'NULL')
        and importer_ein=rec.importer_ein
        and fiscal_yr=v_fiscal_yr)
      WHERE rownum=1;
    EXCEPTION
      when no_data_found then v_importerid := '1';
    END;
    -- we may have wiped out the importer SSN
    -- to prevent storing PII
    if v_importerid = '1' THEN
      -- test for ssn / ein
      BEGIN
        --DBMS_OUTPUT.PUT_LINE('TRYING AGAIN');
        SELECT cbp_importer_id
          INTO v_importerid
        FROM
        (SELECT cbp_importer_id from tu_cbp_importer
        WHERE NVL(consignee_ein, 'NULL')=NVL(rec.consignee_ein, 'NULL')
          AND importer_ein='SSN'
          AND fiscal_yr=v_fiscal_yr)
        WHERE rownum=1;
      EXCEPTION
        when no_data_found then v_importerid:='1';
      END;
    END IF;
    if v_importerid = '1' THEN
      -- test form ein / ssn
      BEGIN
        --DBMS_OUTPUT.PUT_LINE('TRYING AGAIN AGAIN');
        SELECT cbp_importer_id
          INTO v_importerid
        FROM
        (SELECT cbp_importer_id from tu_cbp_importer
        WHERE importer_ein=rec.importer_ein
          AND consignee_ein='SSN'
          AND fiscal_yr=v_fiscal_yr)
        WHERE rownum=1;
      EXCEPTION
        when no_data_found then v_importerid:='1';
      END;
    END IF;
    if v_importerid = '1' THEN
      -- test for SSN / SSN
      BEGIN
        --DBMS_OUTPUT.PUT_LINE('TRYING AGAIN AGAIN AGAIN');
        SELECT cbp_importer_id
          INTO v_importerid
        FROM
        (SELECT cbp_importer_id from tu_cbp_importer
        WHERE importer_ein='SSN'
          AND consignee_ein='SSN'
          AND fiscal_yr=v_fiscal_yr)
        WHERE rownum=1;
      EXCEPTION
        when no_data_found then v_importerid:='1';
      END;
    END IF;
    v_esd_year := to_char(rec.entry_summ_date, 'YYYY');
    v_esd_month := to_char(rec.entry_summ_date, 'MON');
    v_ed_year := to_char(rec.entry_date, 'YYYY');
    v_ed_month := to_char(rec.entry_date, 'MON');
    -- get the assigned date
    IF rec.entry_summ_date is not null AND (v_esd_year = v_fiscal_yr OR  v_esd_year + 1 = v_fiscal_yr) THEN
          v_assigneddate := rec.entry_summ_date;
          v_assignedmonth := to_char(v_assigneddate,'MON');
    ELSIF rec.entry_date is not null AND  (v_ed_year = v_fiscal_yr OR  v_ed_year + 1 = v_fiscal_yr) THEN
          v_assigneddate := rec.entry_date;
          v_assignedmonth := to_char(v_assigneddate,'MON');
          IF  v_assignedmonth in ('SEP') AND v_ed_year + 1 = v_fiscal_yr THEN
              v_assignedmonth:='OCT';
          ELSIF  v_assignedmonth in ('OCT') AND v_ed_year =  v_fiscal_yr THEN
              v_assignedmonth:='SEP';
          END IF;
    ELSE
        v_assigneddate := NULL;
        v_assignedmonth := NULL;
    END IF;

    -- get the tobacco class id
    BEGIN
      SELECT DISTINCT(hc.tobacco_class_id)
        INTO v_tobaccoclass_id
        FROM tu_hts_code hc, tu_tobacco_class tc
      WHERE hc.tobacco_class_id = tc.tobacco_class_id
        AND hts_code = rec.hts_cd;
    EXCEPTION when no_data_found then v_tobaccoclass_id := NULL;
    END;
    -- did we find the tobacco class?  If so,
    -- mark it as default to prevent the user from
    -- being able to change it.
    if v_tobaccoclass_id IS NOT NULL THEN
      v_default_hts_flag := 'Y';
    else
      v_default_hts_flag := 'N';
    END IF;
--    IF v_assigneddate is not null then
--      v_assignedmonth := to_char(v_assigneddate,'MON');
--    ELSE v_assignedmonth := NULL;
--    END IF;
--    DBMS_OUTPUT.PUT_LINE('rec.entry_summ_date '||rec.entry_summ_date||' rec.entry_date '||rec.entry_date||' v_assignedmonth '||v_assignedmonth ||' v_ed_year '||v_ed_year||' v_fiscal_yr '||v_fiscal_yr);
 --   DBMS_OUTPUT.PUT_LINE(' cbp_importer_id ' || v_importerid || ' cbp_upload_id '||rec.cbp_upload_id);

    IF v_assignedmonth in ('OCT','NOV','DEC') then
      v_fiscal_qtr := '1';
    ELSIF v_assignedmonth in ('JAN','FEB','MAR') then
      v_fiscal_qtr := '2';
    ELSIF v_assignedmonth in ('APR','MAY','JUN') then
      v_fiscal_qtr := '3';
    ELSE v_fiscal_qtr := '4';
    END IF;
    BEGIN
  -- DBMS_OUTPUT.PUT_LINE('CBP_IMPORTER_ID--'||v_importerid);
    --DBMS_OUTPUT.PUT_LINE('ENTRY_NUM --'||rec.entry_no);
   --DBMS_OUTPUT.PUT_LINE('LINE_NUM --'||rec.line_no);

    insert into tu_cbp_entry
      (cbp_importer_id, entry_num, line_num, entry_summ_dt,
       entry_dt, hts_cd, qty_removed, estimated_tax,
       fiscal_year, assigned_dt, fiscal_qtr, tobacco_class_id, default_hts_flag, uom_cd,entry_type_cd,
       SUMMARIZED_FILE_TAX,  DETAILS_ENTRY_SUMMARY_DATE,SUMMARY_ENTRY_SUMMARY_NUMBER,MIXED_TOBACCO_TYPE_FLAG, summary_entry_summary_date)
    values
      (v_importerid, rec.entry_no, rec.line_no,
       rec.entry_summ_date,
       rec.entry_date, rec.hts_cd, rec.qty1, rec.estimated_tax,
       v_fiscal_yr, v_assignedmonth,
       v_fiscal_qtr, v_tobaccoclass_id, v_default_hts_flag,rec.uom1,rec.entry_type,
       REC.SUMMARIZED_FILE_TAX,REC.DETAILS_ENTRY_SUMMARY_DATE,REC.SUMMARY_ENTRY_SUMMARY_NUMBER,
       REC.MIXED_TOBACCO_TYPE_FLAG, rec.summary_entry_summary_date)
        RETURNING CBP_ENTRY_ID into v_cbp_entry_id; 
     
     
       exception
       when others then
      -- DBMS_OUTPUT.PUT_LINE(SQLERRM||'--'||sqlcode||' cbp_upload_id  ' || rec.cbp_upload_id );
       RAISE_APPLICATION_ERROR(-20000, ' cbp_upload_id  ' || rec.cbp_upload_id);     
    END;
    
    v_perm_excl_audit_id := null;
    v_ein:= null;
    v_comment_seq := null;
    v_cmp_all_delta_id := null;
    v_exists := null;
      
      
      /*
 -- Non-Cigars
 if(v_tobaccoclass_id <>  8)then
    BEGIN

        select vw.PERM_EXCL_AUDIT_ID, ingested.ein  
                 into v_perm_excl_audit_id ,v_ein
                from permit_excl_status_vw vw  
                inner join 
                ( select decode(cbpimp.association_type_cd,'IMPT',cbpimp.IMPORTER_EIN,'CONS',cbpimp.CONSIGNEE_EIN,'0') as  ein from  
                tu_cbp_importer cbpimp  inner join  tu_cbp_entry cbpent on 
                 cbpimp.cbp_importer_id = cbpent.cbp_importer_id 
                 WHERE cbpimp.CBP_IMPORTER_ID = v_importerid   AND  cbpent.cbp_entry_id = v_cbp_entry_id ) ingested 
                 on vw.ein  = ingested.ein   and                     
                 ((vw.excl_scope_id=1 and  ((decode(vw.assmnt_qtr,1,vw.assmnt_yr-1,vw.assmnt_yr) = v_fiscal_yr 
                and decode(vw.assmnt_qtr,1,vw.assmnt_qtr,vw.assmnt_qtr-1) <= v_fiscal_qtr) or((decode(vw.assmnt_qtr,1,vw.assmnt_yr-1,vw.assmnt_yr) < v_fiscal_yr)))) 
                or (vw.excl_scope_id=2 and decode(vw.assmnt_qtr,1,vw.assmnt_yr-1,vw.assmnt_yr) = v_fiscal_yr
                and decode(vw.assmnt_qtr,1,4,vw.assmnt_qtr-1)= v_fiscal_qtr));
      
              
    exception    
      when others then  
      DBMS_OUTPUT.PUT_LINE('others ' || v_cbp_entry_id);
         
  END;
 end if; 
 
 -- Cigars
 if(v_tobaccoclass_id =  8)then
    BEGIN
  
                select vw.PERM_EXCL_AUDIT_ID, ingested.ein  
                 into v_perm_excl_audit_id ,v_ein
                from permit_excl_status_vw vw  
                inner join 
                ( select decode(cbpimp.association_type_cd,'IMPT',cbpimp.IMPORTER_EIN,'CONS',cbpimp.CONSIGNEE_EIN,'0') as  ein from  
                tu_cbp_importer cbpimp  inner join  tu_cbp_entry cbpent on 
                 cbpimp.cbp_importer_id = cbpent.cbp_importer_id 
                 WHERE cbpimp.CBP_IMPORTER_ID = v_importerid   AND  cbpent.cbp_entry_id = v_cbp_entry_id ) ingested 
                 on vw.ein  = ingested.ein                
                 and   
                 ((vw.excl_scope_id = 1 and  ((vw.assmnt_yr = v_fiscal_yr 
                and vw.assmnt_qtr >= v_fiscal_qtr) or(vw.assmnt_yr >v_fiscal_yr))) 
                or (vw.excl_scope_id=2 and vw.assmnt_yr = v_fiscal_yr
                and vw.assmnt_qtr = v_fiscal_qtr));


                  
                  
    DBMS_OUTPUT.PUT_LINE('Cigars ' || v_cbp_entry_id);

    exception    
      when others then  
      DBMS_OUTPUT.PUT_LINE('Error---Cigars ' || v_cbp_entry_id ||'   '|| v_ein);
         
  END;
 end if; 
      
 --if (v_perm_excl_audit_id is not null) then 
 Begin  
 
        insert into TU_CBP_ingested_EXCL(CBP_ENTRY_ID,PERM_EXCL_AUDIT_ID) values(v_cbp_entry_id,v_perm_excl_audit_id);        --commit;
        exception
        when others then
        dbms_output.put_line('Cant insert data into TU_CBP_INGESTED_EXCL Table');
      
 end;
  
  -- Add Conflicted status into the compare delta sts table and then add Comment for the same 
BEGIN
   select tc.TOBACCO_CLASS_NM into v_tobaccoclass_nm from TU_TOBACCO_CLASS  tc where tobacco_class_id = v_tobaccoclass_id;
   v_exists := null;
   
   select 'X' 
   into v_exists
   from TU_COMPARISON_ALL_DELTA_STS sts 
   where status <> 'Conflicted' and
   EIN <> v_ein and
   TOBACCO_CLASS_NM <> v_tobaccoclass_nm and
   FISCAL_YR <> v_fiscal_yr  and 
   FISCAL_QTR =
     ( CASE v_tobaccoclass_nm WHEN 'Cigars' THEN  '1-4' 
     ELSE to_char(v_fiscal_qtr) END )
   and 
   PERMIT_TYPE =  'IMPT' ;
   --and 
  -- DELTA_TYPE <> 'Ingested';
 
   exception
        when NO_DATA_FOUND then
       dbms_output.put_line(' Data Not Present in STS Table ' || v_ein || v_fiscal_yr || v_tobaccoclass_nm || v_fiscal_qtr );
        v_exists:='A';
    end;
 
 if (NVL(v_exists,'A') <> 'X')
 
 then 
   -- INSERT INTO STS TABLE CONFLICT STATUS
   Begin    
   
    insert into TU_COMPARISON_ALL_DELTA_STS(EIN,FISCAL_YR,FISCAL_QTR,TOBACCO_CLASS_NM,PERMIT_TYPE,STATUS) 
         values
        (v_ein,v_fiscal_yr,
        CASE v_tobaccoclass_nm WHEN 'Cigars' THEN '1-4' else to_char(v_fiscal_qtr) end,
        v_tobaccoclass_nm,'IMPT','Conflicted') 
         RETURNING CMP_ALL_DELTA_ID into v_cmp_all_delta_id;   
         
     --  dbms_output.put_line('inserted data into sts table' || '    ' ||  v_ein || v_fiscal_yr || v_tobaccoclass_nm || v_fiscal_qtr);     
       
       insert into TU_ALL_DELTA_COMMENT(USER_COMMENT,COMMENT_DATE) values ('Ingested data falls in an excluded date range for this company.', SYSDATE)
        RETURNING COMMENT_SEQ into v_comment_seq;
      
       insert into TU_ALL_DELTA_JOIN_COMMENT(CMP_ALL_DELTA_ID, COMMENT_SEQ) values (v_cmp_all_delta_id, v_comment_seq);
       
        exception
        when others then
         dbms_output.put_line('cant insert data into comments data' || '  '  || v_ein || v_fiscal_yr || v_tobaccoclass_nm || v_fiscal_qtr ); 
      
   End;
end if;
 */

    END LOOP;
  END;
END;

/

--------------------------------------------------------
--  File created - Thursday-January-16-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure LOAD_TTB_DETAILS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP12"."LOAD_TTB_DETAILS" (p_fiscal_yr in NUMBER) AS
BEGIN
DECLARE
  v_fiscal_yr NUMBER(4) := p_fiscal_yr;
  CURSOR load_ttbdetail IS
  select permit_num,  TO_NUMBER(SUBSTR(ein_num,1,2)
        ||SUBSTR(ein_num,4,7)) EIN, chewsnuff_taxes, cigarette_taxes, cigar_taxes, piperyo_taxes, fiscal_yr ,
      CASE  WHEN CEIL(to_char(TO_DATE(tpbd,'MM/DD/YY'),'mm')/3)+1 <= 4 THEN CEIL(to_char(TO_DATE(tpbd,'MM/DD/YY'),'mm')/3)+1
      ELSE 1
      END as qtr , to_char(TO_DATE(tpbd,'MM/DD/YY'),'MON') as mnth,

      TO_DATE(tpbd, 'MM/DD/YY') as tpbd

    from tu_ttb_upload
    --where permit_num='TPFL123'
    ;


  v_permit_id NUMBER;

  v_chewsnuff_id NUMBER;

  v_cigarette_id NUMBER;

  v_cigar_id NUMBER;

  v_piperyo_id NUMBER;

  ex_TTB_ANNUAL_TAX_ID number;

ex_PERM_EXCL_AUDIT_ID number;

   v_comment_seq number;
   v_cmp_all_delta_id number;
   v_exists varchar2(1);
   v_ein varchar2(15);

  BEGIN

    FOR rec IN load_ttbdetail LOOP
    
     v_comment_seq := null;
     v_cmp_all_delta_id := null;

    --DBMS_OUTPUT.PUT_LINE('  rec.permit_num  ' ||  rec.permit_num ||' v_fiscal_yr '|| v_fiscal_yr );


    BEGIN

      SELECT ttb_permit_id

        INTO v_permit_id

      FROM tu_ttb_permit a, tu_ttb_company b

      WHERE a.ttb_company_id = b.ttb_company_id

        and b.fiscal_yr = v_fiscal_yr

        and a.permit_num = rec.permit_num and b.ein_num=rec.ein;

    EXCEPTION when no_data_found then v_permit_id := '1';

    END;



    SELECT tobacco_class_id

      INTO v_chewsnuff_id

      FROM tu_tobacco_class

    WHERE tobacco_class_nm='Chew-and-Snuff';



    SELECT tobacco_class_id

      INTO v_cigarette_id

      FROM tu_tobacco_class

    WHERE tobacco_class_nm='Cigarettes';





    SELECT tobacco_class_id

      INTO v_cigar_id

      FROM tu_tobacco_class

    WHERE tobacco_class_nm='Cigars';



    SELECT tobacco_class_id

      INTO v_piperyo_id

      FROM tu_tobacco_class

    WHERE tobacco_class_nm='Pipe-RYO';



    IF rec.chewsnuff_taxes <> '0' then

      insert into tu_ttb_annual_tax

        (ttb_permit_id, ttb_fiscal_yr, tobacco_class_id, ttb_taxes_paid,ttb_calendar_qtr,month,TPBD)

      values

        (v_permit_id, rec.fiscal_yr, v_chewsnuff_id, rec.chewsnuff_taxes, rec.qtr, rec.mnth,rec.tpbd)
      RETURNING TTB_ANNUAL_TAX_ID into ex_TTB_ANNUAL_TAX_ID;

       Begin
       select vw.PERM_EXCL_AUDIT_ID 
        into ex_PERM_EXCL_AUDIT_ID 
        from permit_excl_status_vw vw

        where vw.permit_num = rec.permit_num and vw.ein = rec.ein and
            ((vw.excl_scope_id=1 and  ((decode(vw.assmnt_qtr,1,vw.assmnt_yr-1,vw.assmnt_yr) = rec.fiscal_yr 
                and decode(vw.assmnt_qtr,1,vw.assmnt_qtr,vw.assmnt_qtr-1) <= rec.qtr) or((decode(vw.assmnt_qtr,1,vw.assmnt_yr-1,vw.assmnt_yr) < rec.fiscal_yr)))) 
                or (vw.excl_scope_id=2 and decode(vw.assmnt_qtr,1,vw.assmnt_yr-1,vw.assmnt_yr) = rec.fiscal_yr
                --need to confirm the partial exclude, data in 2016 file will have 2015 data
                and decode(vw.assmnt_qtr,1,4,vw.assmnt_qtr-1)=rec.qtr));
       exception
       when others then
        --dbms_output.put_line('Audit is not available');
        ex_PERM_EXCL_AUDIT_ID:=null;
       end; 
        --dbms_output.put_line('test3');
        Begin
            insert into tu_ttb_ingested_excl(TTB_ANNUAL_TAX_ID,PERM_EXCL_AUDIT_ID) values(ex_TTB_ANNUAL_TAX_ID,ex_PERM_EXCL_AUDIT_ID);
        exception
        when others then
        null;
         --dbms_output.put_line('cant insert null data');
        end; 

  -- Add Conflicted status into the compare delta sts table and then add Comment for the same 
   BEGIN

   v_exists := null;
   
   select 'X' 
   into v_exists
   from TU_COMPARISON_ALL_DELTA_STS sts 
   where status <> 'Conflicted' and
   EIN <> rec.ein and
   TOBACCO_CLASS_NM <> 'Chew/Snuff' and
   FISCAL_YR <> v_fiscal_yr  and 
   FISCAL_QTR = to_char(rec.qtr) and 
   PERMIT_TYPE = 'MANU' ;
   --and 
  -- DELTA_TYPE <> 'Ingested';
   exception
        when NO_DATA_FOUND then
       --dbms_output.put_line(' Data Not Present in STS Table ' || rec.ein || v_fiscal_yr || 'Chew/Snuff' || rec.qtr );
        v_exists:='A';
    end;
      --dbms_output.put_line(v_exists);
      
   if (NVL(v_exists,'A') <> 'X') then 

  
    
   Begin    
   
  
    insert into TU_COMPARISON_ALL_DELTA_STS(EIN,FISCAL_YR,FISCAL_QTR,TOBACCO_CLASS_NM,PERMIT_TYPE,STATUS) 
         values
        (rec.ein,v_fiscal_yr, to_char(rec.qtr), 'Chew/Snuff','MANU','Conflicted') 
         RETURNING CMP_ALL_DELTA_ID into v_cmp_all_delta_id;   
         
       --dbms_output.put_line('inserted data into sts table' || '    ' ||  rec.ein || v_fiscal_yr || 'Chew/Snuff' || rec.qtr);     
       
       insert into TU_ALL_DELTA_COMMENT(USER_COMMENT,COMMENT_DATE) values ('Ingested data falls in an excluded date range for this company.', SYSDATE)
        RETURNING COMMENT_SEQ into v_comment_seq;
      
       insert into TU_ALL_DELTA_JOIN_COMMENT(CMP_ALL_DELTA_ID, COMMENT_SEQ) values (v_cmp_all_delta_id, v_comment_seq);
       
        exception
        when others then
         dbms_output.put_line('cant insert data into comments data' || '  '  || rec.ein || v_fiscal_yr || 'Chew-Snuff' || rec.qtr ); 
      
end;
end if;



END IF;



    IF rec.cigarette_taxes <> '0' then

       insert into tu_ttb_annual_tax

        (ttb_permit_id, ttb_fiscal_yr, tobacco_class_id, ttb_taxes_paid,ttb_calendar_qtr,month,TPBD)

      values

        (v_permit_id, rec.fiscal_yr, v_cigarette_id, rec.cigarette_taxes,rec.qtr,rec.mnth,rec.tpbd)
       RETURNING TTB_ANNUAL_TAX_ID into ex_TTB_ANNUAL_TAX_ID;
       Begin
       select vw.PERM_EXCL_AUDIT_ID 
        into ex_PERM_EXCL_AUDIT_ID 
        from permit_excl_status_vw vw 
        where vw.permit_num = rec.permit_num and vw.EIN = rec.ein and
            ((vw.excl_scope_id=1 and  ((decode(vw.assmnt_qtr,1,vw.assmnt_yr-1,vw.assmnt_yr) = rec.fiscal_yr 
                and decode(vw.assmnt_qtr,1,vw.assmnt_qtr,vw.assmnt_qtr-1) <= rec.qtr) or((decode(vw.assmnt_qtr,1,vw.assmnt_yr-1,vw.assmnt_yr) < rec.fiscal_yr)))) 
                or (vw.excl_scope_id=2 and decode(vw.assmnt_qtr,1,vw.assmnt_yr-1,vw.assmnt_yr) = rec.fiscal_yr
                --need to confirm the partial exclude, data in 2016 file will have 2015 data
                and decode(vw.assmnt_qtr,1,4,vw.assmnt_qtr-1)=rec.qtr));
       exception
       when others then
            --dbms_output.put_line('Audit is not available');
            ex_PERM_EXCL_AUDIT_ID:=null;
       end; 
        --dbms_output.put_line('test3');
        Begin
        insert into tu_ttb_ingested_excl(TTB_ANNUAL_TAX_ID,PERM_EXCL_AUDIT_ID) values(ex_TTB_ANNUAL_TAX_ID,ex_PERM_EXCL_AUDIT_ID);
        exception
        when others then
        null;
            --dbms_output.put_line('cant insert null data');
        end; 
        
         -- Add Conflicted status into the compare delta sts table and then add Comment for the same 
   BEGIN

   v_exists := null;
   
   select 'X' 
   into v_exists
   from TU_COMPARISON_ALL_DELTA_STS sts 
   where status <> 'Conflicted' and
   EIN <> rec.ein and
   TOBACCO_CLASS_NM <> 'Cigarettes' and
   FISCAL_YR <> v_fiscal_yr  and 
   FISCAL_QTR = to_char(rec.qtr) and 
   PERMIT_TYPE = 'MANU' ;
   --and 
  -- DELTA_TYPE <> 'Ingested';
   exception
        when NO_DATA_FOUND then
       --dbms_output.put_line(' Data Not Present in STS Table ' || rec.ein || v_fiscal_yr || 'Chew/Snuff' || rec.qtr );
        v_exists:='A';
    end;
      --dbms_output.put_line(v_exists);
      
   if (NVL(v_exists,'A') <> 'X') then   
   Begin    
 
    insert into TU_COMPARISON_ALL_DELTA_STS(EIN,FISCAL_YR,FISCAL_QTR,TOBACCO_CLASS_NM,PERMIT_TYPE,STATUS) 
         values
        (rec.ein,v_fiscal_yr, to_char(rec.qtr), 'Cigarettes','MANU','Conflicted') 
         RETURNING CMP_ALL_DELTA_ID into v_cmp_all_delta_id;   
         
       --dbms_output.put_line('inserted data into sts table' || '    ' ||  rec.ein || v_fiscal_yr || 'Cigarettes' || rec.qtr);     
       
       insert into TU_ALL_DELTA_COMMENT(USER_COMMENT,COMMENT_DATE) values ('Ingested data falls in an excluded date range for this company.', SYSDATE)
        RETURNING COMMENT_SEQ into v_comment_seq;
      
       insert into TU_ALL_DELTA_JOIN_COMMENT(CMP_ALL_DELTA_ID, COMMENT_SEQ) values (v_cmp_all_delta_id, v_comment_seq);
       
        exception
        when others then
         null;
         --dbms_output.put_line('cant insert data into comments data' || '  '  || rec.ein || v_fiscal_yr || 'Cigarettes' || rec.qtr ); 
      
end;
end if;
             

END IF;



    IF rec.cigar_taxes <> '0' then

      insert into tu_ttb_annual_tax

        (ttb_permit_id, ttb_fiscal_yr, tobacco_class_id, ttb_taxes_paid,ttb_calendar_qtr,month,TPBD)

      values

        (v_permit_id, rec.fiscal_yr, v_cigar_id, rec.cigar_taxes,rec.qtr,rec.mnth,rec.tpbd)
        RETURNING TTB_ANNUAL_TAX_ID into ex_TTB_ANNUAL_TAX_ID;
       Begin
       select vw.PERM_EXCL_AUDIT_ID 
        into ex_PERM_EXCL_AUDIT_ID 
        from permit_excl_status_vw vw 
        where vw.permit_num = rec.permit_num and vw.EIN = rec.ein and
            ((vw.excl_scope_id=1 and  ((vw.assmnt_yr-1 = rec.fiscal_yr 
                and vw.assmnt_qtr <= rec.qtr) or(vw.assmnt_yr-1< rec.fiscal_yr))) 
                or (vw.excl_scope_id=2 and vw.assmnt_yr-1 = rec.fiscal_yr
                --need to confirm the partial exclude, data in 2016 file will have 2015 data
                and vw.assmnt_qtr = rec.qtr));
       exception
       when others then
            ex_PERM_EXCL_AUDIT_ID:=null;
       end; 
        Begin
        insert into tu_ttb_ingested_excl(TTB_ANNUAL_TAX_ID,PERM_EXCL_AUDIT_ID) values(ex_TTB_ANNUAL_TAX_ID,ex_PERM_EXCL_AUDIT_ID);
        exception
        when others then
            null;
            --dbms_output.put_line('cant insert null data');
        end;
        
        
BEGIN

   v_exists := null;
   
   select 'X' 
   into v_exists
   from TU_COMPARISON_ALL_DELTA_STS sts 
   where status <> 'Conflicted' and
   EIN <> rec.ein and
   TOBACCO_CLASS_NM <> 'Cigars' and
   FISCAL_YR <> v_fiscal_yr  and 
   FISCAL_QTR = '1-4' and 
   PERMIT_TYPE = 'MANU' ;
   --and 
  -- DELTA_TYPE <> 'Ingested';
   exception
        when NO_DATA_FOUND then
       --dbms_output.put_line(' Data Not Present in STS Table ' || rec.ein || v_fiscal_yr || 'Cigars' || rec.qtr );
        v_exists:='A';
    end;
      --dbms_output.put_line(v_exists);
      
   if (NVL(v_exists,'A') <> 'X') then    
   Begin    
     
    insert into TU_COMPARISON_ALL_DELTA_STS(EIN,FISCAL_YR,FISCAL_QTR,TOBACCO_CLASS_NM,PERMIT_TYPE,STATUS) 
         values
        (rec.ein,v_fiscal_yr, '1-4', 'Cigars','MANU','Conflicted') 
         RETURNING CMP_ALL_DELTA_ID into v_cmp_all_delta_id;   
         
       --dbms_output.put_line('inserted data into sts table' || '    ' ||  rec.ein || v_fiscal_yr || 'Cigars' || rec.qtr);     
       
       insert into TU_ALL_DELTA_COMMENT(USER_COMMENT,COMMENT_DATE) values ('Ingested data falls in an excluded date range for this company.', SYSDATE)
        RETURNING COMMENT_SEQ into v_comment_seq;
      
       insert into TU_ALL_DELTA_JOIN_COMMENT(CMP_ALL_DELTA_ID, COMMENT_SEQ) values (v_cmp_all_delta_id, v_comment_seq);
       
        exception
        when others then
         null;
         --dbms_output.put_line('cant insert data into comments data' || '  '  || rec.ein || v_fiscal_yr || 'Cigars' || rec.qtr ); 
      
end;
end if;  
        
      
 END IF;

    IF rec.piperyo_taxes <> '0' then

      insert into tu_ttb_annual_tax

        (ttb_permit_id, ttb_fiscal_yr,tobacco_class_id, ttb_taxes_paid,ttb_calendar_qtr,month,TPBD)

      values

        (v_permit_id, rec.fiscal_yr, v_piperyo_id, rec.piperyo_taxes,rec.qtr,rec.mnth,rec.tpbd)
        RETURNING TTB_ANNUAL_TAX_ID into ex_TTB_ANNUAL_TAX_ID;
       Begin
       select vw.PERM_EXCL_AUDIT_ID 
        into ex_PERM_EXCL_AUDIT_ID 
        from permit_excl_status_vw vw 
        where vw.permit_num = rec.permit_num and vw.ein = rec.ein and
            ((vw.excl_scope_id=1 and  ((decode(vw.assmnt_qtr,1,vw.assmnt_yr-1,vw.assmnt_yr) = rec.fiscal_yr 
                and decode(vw.assmnt_qtr,1,vw.assmnt_qtr,vw.assmnt_qtr-1) <= rec.qtr) or((decode(vw.assmnt_qtr,1,vw.assmnt_yr-1,vw.assmnt_yr) < rec.fiscal_yr)))) 
                or (vw.excl_scope_id=2 and decode(vw.assmnt_qtr,1,vw.assmnt_yr-1,vw.assmnt_yr) = rec.fiscal_yr
                --need to confirm the partial exclude, data in 2016 file will have 2015 data
                and decode(vw.assmnt_qtr,1,4,vw.assmnt_qtr-1)=rec.qtr));
       exception
       when others then
        ex_PERM_EXCL_AUDIT_ID:=null;
       end; 
       Begin
        insert into tu_ttb_ingested_excl(TTB_ANNUAL_TAX_ID,PERM_EXCL_AUDIT_ID) values(ex_TTB_ANNUAL_TAX_ID,ex_PERM_EXCL_AUDIT_ID);
        exception
        when others then
            null;
            --dbms_output.put_line('cant insert null data');
            --ex_PERM_EXCL_AUDIT_ID:=null;
        end; 

        --dbms_output.put_line('test4');
        
 BEGIN

   v_exists := null;
   
   select 'X' 
   into v_exists
   from TU_COMPARISON_ALL_DELTA_STS sts 
   where status <> 'Conflicted' and
   EIN <> rec.ein and
   TOBACCO_CLASS_NM <> 'Pipe/Roll-Your-Own' and
   FISCAL_YR <> v_fiscal_yr  and 
   FISCAL_QTR = to_char(rec.qtr) and 
   PERMIT_TYPE = 'MANU' ;
   --and 
  -- DELTA_TYPE <> 'Ingested';
   exception
        when NO_DATA_FOUND then
       --dbms_output.put_line(' Data Not Present in STS Table ' || rec.ein || v_fiscal_yr || 'Chew/Snuff' || rec.qtr );
        v_exists:='A';
    end;
      --dbms_output.put_line(v_exists);
      
   if (NVL(v_exists,'A') <> 'X') then 
    
   Begin    
   
  
    insert into TU_COMPARISON_ALL_DELTA_STS(EIN,FISCAL_YR,FISCAL_QTR,TOBACCO_CLASS_NM,PERMIT_TYPE,STATUS) 
         values
        (rec.ein,v_fiscal_yr, to_char(rec.qtr), 'Pipe/Roll-Your-Own','MANU','Conflicted') 
         RETURNING CMP_ALL_DELTA_ID into v_cmp_all_delta_id;   
         
       --dbms_output.put_line('inserted data into sts table' || '    ' ||  rec.ein || v_fiscal_yr || 'Pipe/Roll-Your-Own' || rec.qtr);     
       
       insert into TU_ALL_DELTA_COMMENT(USER_COMMENT,COMMENT_DATE) values ('Ingested data falls in an excluded date range for this company.', SYSDATE)
        RETURNING COMMENT_SEQ into v_comment_seq;
      
       insert into TU_ALL_DELTA_JOIN_COMMENT(CMP_ALL_DELTA_ID, COMMENT_SEQ) values (v_cmp_all_delta_id, v_comment_seq);
       
        exception
        when others then
         null;
         --dbms_output.put_line('cant insert data into comments data' || '  '  || rec.ein || v_fiscal_yr || 'Pipe/Roll-Your-Own' || rec.qtr ); 
       
end;
end if;

    END IF;
    END LOOP;
    COMMIT;

  END;

END;

/

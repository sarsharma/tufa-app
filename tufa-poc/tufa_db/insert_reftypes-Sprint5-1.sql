PROMPT
PROMPT===========================================================
PROMPT insert_reftypes-Sprint5-1.sql - BEGIN
PROMPT===========================================================
PROMPT
/*-------------------------------------------------------------*/
/*
/*Purpose: Insert reference table types, and populate tobacco classes.
/*
/*Created: 10/24/1016 by Tony Ireland
/*
/*-------------------------------------------------------------*/
--
DELETE from tu_ref_type;
commit;
--
insert into tu_ref_type
    (tu_ref_type_nm)
values
    ('ADDRESS_TYPE');

insert into tu_ref_type
    (tu_ref_type_nm)
values
    ('PERMIT_TYPE');

insert into tu_ref_type
    (tu_ref_type_nm)
values
    ('PERMIT_STATUS_TYPE');

insert into tu_ref_type
    (tu_ref_type_nm)
values
    ('RECON_STATUS_TYPE');
	
insert into tu_ref_type
    (tu_ref_type_nm)
values
    ('PERIOD_STATUS_TYPE');

insert into tu_ref_type
    (tu_ref_type_nm)
values
    ('DOC_STATUS_TYPE');

insert into tu_ref_type
    (tu_ref_type_nm)
values
    ('DOCUMENT_TYPE');

insert into tu_ref_type
    (tu_ref_type_nm)
values
    ('SOURCE_TYPE');

insert into tu_ref_type
    (tu_ref_type_nm)
values
    ('FORM_TYPE');

insert into tu_ref_type
    (tu_ref_type_nm)
values
    ('REMOVAL_UOM');

insert into tu_ref_type
    (tu_ref_type_nm)
values
    ('ACTIVITY_TYPE');

insert into tu_ref_type
    (tu_ref_type_nm)
values
    ('CLASS_TYPE');


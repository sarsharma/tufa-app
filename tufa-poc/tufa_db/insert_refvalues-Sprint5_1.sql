DECLARE
     v_address_type_id NUMBER := 0;
     v_permit_type_id NUMBER := 0;
     v_permitstatus_type_id NUMBER := 0;
     v_periodstatus_type_id NUMBER := 0;
     v_docstatus_type_id NUMBER := 0;
     v_doc_type_id NUMBER := 0;
     v_source_type_id NUMBER := 0;
     v_form_type_id NUMBER := 0;
     v_removal_uom_id NUMBER := 0;
     v_activity_type_id NUMBER := 0;
     v_class_type_id NUMBER := 0;
	 v_recon_status_type_id NUMBER := 0;
BEGIN
  BEGIN
    select tu_ref_type_id into v_address_type_id
      from tu_ref_type
    where tu_ref_type_nm = 'ADDRESS_TYPE';
    select tu_ref_type_id into v_permit_type_id
      from tu_ref_type
    where tu_ref_type_nm = 'PERMIT_TYPE';
    select tu_ref_type_id into v_permitstatus_type_id
      from tu_ref_type
    where tu_ref_type_nm = 'PERMIT_STATUS_TYPE';
    select tu_ref_type_id into v_periodstatus_type_id
      from tu_ref_type
    where tu_ref_type_nm = 'PERIOD_STATUS_TYPE';
	select tu_ref_type_id into v_recon_status_type_id
      from tu_ref_type
    where tu_ref_type_nm = 'RECON_STATUS_TYPE';
    select tu_ref_type_id into v_docstatus_type_id
      from tu_ref_type
    where tu_ref_type_nm = 'DOC_STATUS_TYPE';
    select tu_ref_type_id into v_doc_type_id
      from tu_ref_type
    where tu_ref_type_nm = 'DOCUMENT_TYPE';
    select tu_ref_type_id into v_source_type_id
      from tu_ref_type
    where tu_ref_type_nm = 'SOURCE_TYPE';
    select tu_ref_type_id into v_form_type_id
      from tu_ref_type
    where tu_ref_type_nm = 'FORM_TYPE';
    select tu_ref_type_id into v_removal_uom_id
      from tu_ref_type
    where tu_ref_type_nm = 'REMOVAL_UOM';
    select tu_ref_type_id into v_activity_type_id
      from tu_ref_type
    where tu_ref_type_nm = 'ACTIVITY_TYPE';
    select tu_ref_type_id into v_class_type_id
      from tu_ref_type
    where tu_ref_type_nm = 'CLASS_TYPE';
  END;
  BEGIN

/* Address Types */
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_address_type_id, 'Primary', 'PRIM');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_address_type_id, 'Alternate', 'ALTN');

/* Permit Types */
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_permit_type_id, 'Manufacturer', 'MANU');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_permit_type_id, 'Importer', 'IMPT');

/* Permit Status Types */
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_permitstatus_type_id, 'Active', 'ACTV');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_permitstatus_type_id, 'Closed', 'CLSD');

/* Period Status Types */
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_periodstatus_type_id, 'Not Started', 'NSTD');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_periodstatus_type_id, 'Complete', 'COMP');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_periodstatus_type_id, 'Error', 'ERRO');
	 
/* Recon Status Types */	 
	 insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_recon_status_type_id, 'Recon Approved', 'APRV');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_recon_status_type_id, 'Recon Not Approved', 'UNAP');

/* Period Status Type Types 
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_status_type_type, 'Report Status', 'RPST');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_status_type_type, 'Recon Status', 'REST');
*/

/* Document Status Types */
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_docstatus_type_id, 'Complete', 'COMP');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_docstatus_type_id, 'Incomplete', 'INCP');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_docstatus_type_id, 'Error', 'ERRO');

/* Document Types */
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_doc_type_id, '3852', '3852');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_doc_type_id, '7501', '7501');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_doc_type_id, '5210.5', '5210');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_doc_type_id, '5220.6', '5220');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_doc_type_id, '5000.24', '5024');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_doc_type_id, 'Other', 'OTHR');

/* Source Types */
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_source_type_id, 'Email', 'EMAI');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_source_type_id, 'Fax', 'FAXX');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_source_type_id, 'Mail', 'MAIL');

/* Form Types */
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_form_type_id, '3852', '3852');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_form_type_id, '7501', '7501');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_form_type_id, '5210.5', '2105');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_form_type_id, '5220.6', '2206');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_form_type_id, '5000.24', '5024');

/* Removal Units of Measure */
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_removal_uom_id, 'Pounds', 'LBSS');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_removal_uom_id, 'Kilograms', 'KILO');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_removal_uom_id, 'Sticks', 'STIK');

/* Activity Types */
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_activity_type_id, 'Valid', 'VALD');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_activity_type_id, 'Error', 'ERRO');

/* Tobacco Class Types */
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_class_type_id, 'Type', 'TYPE');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_class_type_id, 'Super-Type', 'SPTP');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_class_type_id, 'Sub-Type', 'SUBT');
     insert into tu_ref_value
       (tu_ref_type_id, tu_ref_value, tu_ref_code)
     values
       (v_class_type_id, 'Ad-Valorem', 'ADVL');
  END;
END;
/
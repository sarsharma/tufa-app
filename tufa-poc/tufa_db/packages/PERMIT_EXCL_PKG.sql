--------------------------------------------------------
--  DDL for Package PERMIT_EXCL_PKG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "CTP_TUFA_MVP13"."PERMIT_EXCL_PKG" AS

    -- constants
    asmnt_rerun_st     CONSTANT VARCHAR(15) := 'RE-RUN';
    asmnt_noaction_st  CONSTANT VARCHAR(15) := 'NO-ACTION';
    asmnt_complete_st  CONSTANT VARCHAR(15) := 'COMPLETE';

    exl_scope_full_id CONSTANT NUMBER := 1;
    exl_scope_partial_id CONSTANT NUMBER := 2;
    exl_scope_incl_id CONSTANT NUMBER := 3;

    permit_type_cd_imp CONSTANT VARCHAR(5) := 'IMPT';
    permit_type_cd_manu CONSTANT VARCHAR(5):= 'MANU';

   -- create new array type to store number datatype
   TYPE number_arry_type IS VARRAY(4000) OF NUMBER;
   -----------------------------------------------------------------------
    -- TODO: change to user RowType
    TYPE PermitExclType IS RECORD (
      company_id  TU_PERMIT_EXCL_AUDIT.COMPANY_ID%TYPE,
      permit_id   TU_PERMIT_EXCL_AUDIT.PERMIT_ID%TYPE,
      ASSMNT_YR   TU_PERMIT_EXCL_AUDIT.ASSMNT_YR%TYPE,
      ASSMNT_QTR   TU_PERMIT_EXCL_AUDIT.ASSMNT_QTR%TYPE,
      EXCLUSION_SCOPE_ID  TU_PERMIT_EXCL_AUDIT.EXCLUSION_SCOPE_ID%TYPE,
      CREATED_DT  TU_PERMIT_EXCL_AUDIT.CREATED_DT%TYPE,
      CREATED_BY  TU_PERMIT_EXCL_AUDIT.CREATED_BY%TYPE,
      PERM_EXCL_AUDIT_ID TU_PERMIT_EXCL_AUDIT.PERM_EXCL_AUDIT_ID%TYPE,
      FULL_PERM_EXCL_AUDIT_ID   TU_PERMIT_EXCL_AUDIT.FULL_PERM_EXCL_AUDIT_ID%TYPE,
      COMMENT_ID TU_PERMIT_EXCL_AUDIT.COMMENT_ID%TYPE
    );

    TYPE permit_excl_type_arry IS VARRAY(4000) OF PermitExclType;

    ----------------------------------------------------------------------
    TYPE AffectAssmntType IS RECORD (
--      ASSESSMENT_ID  TU_PERMIT_EXCL_AFFECT_ASSMNTS.ASSESSMENT_ID%TYPE,
--      ASSESSMENT_VERSION   TU_PERMIT_EXCL_AFFECT_ASSMNTS.ASSESSMENT_VERSION%TYPE,
      PERMIT_EXCL_AUDIT_ID   TU_PERMIT_EXCL_AFFECT_ASSMNTS.PERM_EXCL_AUDIT_ID%TYPE,
      AFFECTED_ASSMNT_ID  TU_PERMIT_EXCL_AFFECT_ASSMNTS.AFFECTED_ASSMNT_ID%TYPE,
      CREATED_DT TU_PERMIT_EXCL_AFFECT_ASSMNTS.CREATED_DT%TYPE,
      CREATED_BY TU_PERMIT_EXCL_AFFECT_ASSMNTS.CREATED_BY%TYPE,
      ASSESSMENT_STATUS  TU_PERMIT_EXCL_AFFECT_ASSMNTS.ASSESSMENT_STATUS%TYPE,
      ASSMNT_RPT_DATA_YR TU_PERMIT_EXCL_AFFECT_ASSMNTS.ASSMNT_RPT_DATA_YR%TYPE,
      ASSMNT_RPT_DATA_QTR TU_PERMIT_EXCL_AFFECT_ASSMNTS.ASSMNT_RPT_DATA_QTR%TYPE,
      ASSMNT_TYPE TU_PERMIT_EXCL_AFFECT_ASSMNTS.ASSMNT_TYPE%TYPE,
      ASSMNT_YR TU_PERMIT_EXCL_AFFECT_ASSMNTS.ASSMNT_YR%TYPE,
      ASSMNT_QTR TU_PERMIT_EXCL_AFFECT_ASSMNTS.ASSMNT_QTR%TYPE
    );

    TYPE aff_assmnt_type_arry IS VARRAY(4000) OF AffectAssmntType;

    ----------------------------------------------------------------------

    TYPE AssessmentType IS RECORD (
--      ASSESSMENT_ID  TU_ASSESSMENT.ASSESSMENT_ID%TYPE,
--      ASSESSMENT_VERSION   TU_ASSESSMENT.ASSESSMENT_VERSION%TYPE,
      ASSMNT_RPT_DATA_YR TU_ASSESSMENT.ASSESSMENT_YR%TYPE,
      ASSMNT_RPT_DATA_QTR TU_ASSESSMENT.ASSESSMENT_QTR%TYPE,
      ASSMNT_TYPE TU_ASSESSMENT.ASSESSMENT_TYPE%TYPE
    );

    TYPE asmnt_type_arry IS VARRAY(4000) OF AssessmentType;

    -----------------------------------------------------------------------

    TYPE AffectAssmntVWType IS RECORD (
      ASSESSMENT_TYPE   PERMIT_EXCL_AFFCTED_ASSMNTS_VW.ASSESSMENT_TYPE%TYPE,
      ASSESSMENT_QTR PERMIT_EXCL_AFFCTED_ASSMNTS_VW.ASSESSMENT_QTR%TYPE,
      ASSESSMENT_YR PERMIT_EXCL_AFFCTED_ASSMNTS_VW.ASSESSMENT_YR%TYPE,
      ASSESSMENT_STATUS  PERMIT_EXCL_AFFCTED_ASSMNTS_VW.ASSESSMENT_STATUS%TYPE,
--      PERM_EXCL_AUDIT_ID  PERMIT_EXCL_AFFCTED_ASSMNTS_VW.PERM_EXCL_AUDIT_ID%TYPE,
      CREATED_DT PERMIT_EXCL_AFFCTED_ASSMNTS_VW.CREATED_DT%TYPE,
      CREATED_BY PERMIT_EXCL_AFFCTED_ASSMNTS_VW.CREATED_BY%TYPE
    );

    TYPE aff_assmnt_vw_type_arry IS VARRAY(4000) OF AffectAssmntVWType;

    -------------------------------------------------------------------------
      TYPE PermitExclStatusVWType IS RECORD (
      company_id  PERMIT_EXCL_STATUS_VW.COMPANY_ID%TYPE,
      legal_nm    PERMIT_EXCL_STATUS_VW.legal_nm%TYPE,
      ein         PERMIT_EXCL_STATUS_VW.ein%TYPE,
      permit_id   PERMIT_EXCL_STATUS_VW.PERMIT_ID%TYPE,
      permit_num  PERMIT_EXCL_STATUS_VW.PERMIT_num%TYPE,
      PERM_EXCL_AUDIT_ID PERMIT_EXCL_STATUS_VW.PERM_EXCL_AUDIT_ID%TYPE,
      CREATED_DT  PERMIT_EXCL_STATUS_VW.CREATED_DT%TYPE,
      CREATED_BY  PERMIT_EXCL_STATUS_VW.CREATED_BY%TYPE,
      ASSMNT_YR   PERMIT_EXCL_STATUS_VW.ASSMNT_YR%TYPE,
      ASSMNT_QTR   PERMIT_EXCL_STATUS_VW.ASSMNT_QTR%TYPE,
      FULL_PERM_EXCL_AUDIT_ID   PERMIT_EXCL_STATUS_VW.FULL_PERM_EXCL_AUDIT_ID%TYPE,
      COMMENT_ID  PERMIT_EXCL_STATUS_VW.COMMENT_ID%TYPE,
      EXCL_SCOPE_DESC PERMIT_EXCL_STATUS_VW.EXCL_SCOPE_DESC%TYPE,
      EXCL_SCOPE_ID  PERMIT_EXCL_STATUS_VW.EXCL_SCOPE_ID%TYPE
    );

    TYPE permit_excl_vw_type_arry IS VARRAY(4000) OF PermitExclStatusVWType;

    -------------------------------------------------------------------------
   -- Function to get affected assessments
   FUNCTION get_perm_excl_affected_assmnt (
        assmnt_yr      IN             NUMBER,
        asmnt_qtr      IN             NUMBER,
        company_id     IN             NUMBER,
        exclusion_scope_id   IN       NUMBER
    ) RETURN asmnt_type_arry;

  -- Function to check if exclusion is affecting assessments
 PROCEDURE affected_assmnt_exist (
        assmnt_yr      IN             NUMBER,
        asmnt_qtr      IN             NUMBER,
        company_id     IN             NUMBER,
        exclusion_scope_id   IN       NUMBER,
        permit_id      IN NUMBER,
        affected_assmnt_size OUT  NUMBER
    );

 -- PROCEDURE to audit exclude/include actions on permit-period
 PROCEDURE exclude_permit_from_mktsh (
    company_id        IN                NUMBER DEFAULT NULL,
    permit_id         IN                NUMBER DEFAULT NULL,
    excl_asmt_fiscal_yr    IN           NUMBER DEFAULT NULL,
    excl_qtr_1          IN              NUMBER DEFAULT NULL,
    excl_qtr_2          IN              NUMBER DEFAULT NULL,
    excl_qtr_3          IN              NUMBER DEFAULT NULL,
    excl_qtr_4          IN              NUMBER DEFAULT NULL,
    exclusion_flag    IN                VARCHAR DEFAULT NULL,
    user_logged       IN                VARCHAR,
    new_comment_id    IN               NUMBER DEFAULT NULL
    );

   --  Procedure to insert affected assessments
    PROCEDURE insert_affected_assessments (
        company_id_in           IN                      NUMBER,
        permit_id_in            IN                      NUMBER,
        excl_assmnt_yr_in       IN                      NUMBER,
        excl_assmnt_qtr_in      IN                      NUMBER,
        exclusion_scope_id_in   IN                      NUMBER,
        new_exclusion_id_in     IN                      NUMBER,
        last_exclusion_id_in    IN                      NUMBER,
        created_dt_in           IN                      DATE,
        created_by_in           IN                      VARCHAR
    ) ;

    -- Function to insert exclusion entries in audit table
    PROCEDURE insert_exclude_entries(COMPANY_ID_IN IN  NUMBER, PERMIT_ID_in IN NUMBER,EXCL_ASSMNT_YR_in IN NUMBER ,ASSMNT_QTR_in IN NUMBER ,EXCLUSION_SCOPE_ID_in IN NUMBER ,
    CREATED_DT_in IN DATE,CREATED_BY_in IN VARCHAR,new_comment_id_in  IN NUMBER);

 -- Function to update the delta statuses based on exclusion/inclusion
 PROCEDURE EXCLUSION_DELTA_STATUS_CAHNGE(
        company_id_in           IN NUMBER,
        permit_id_in            IN NUMBER,
        excl_assmnt_yr_in       IN NUMBER,
        excl_assmnt_qtr_in      IN NUMBER,
        exclusion_scope_id_in   IN NUMBER,
        current_scope_id_in     IN NUMBER
 );

 -- Procedure to exclude/include ttb ingested entries
  PROCEDURE ttb_ingest_entries_exclude (
        company_id_in             IN                        NUMBER,
        permit_id_in              IN                        NUMBER,
        assmnt_yr_in              IN                        NUMBER,
        assmnt_qtr_in             IN                        NUMBER,
        exclusion_scope_id_in     IN                        NUMBER,
        perm_excl_audit_id_in     IN                        NUMBER,
        last_permit_excl_record   IN                        permitexclstatusvwtype,
        created_dt_in             IN                        DATE,
        created_by_in             IN                        VARCHAR
    );

-- Procedure to exclude/include cbp ingested entries
    PROCEDURE cbp_ingest_entries_exclude (
        company_id_in             IN                        NUMBER,
        permit_id_in              IN                        NUMBER,
        assmnt_yr_in              IN                        NUMBER,
        assmnt_qtr_in             IN                        NUMBER,
        exclusion_scope_id_in     IN                        NUMBER,
        perm_excl_audit_id_in     IN                        NUMBER,
        last_permit_excl_record   IN                        permitexclstatusvwtype,
        created_dt_in             IN                        DATE,
        created_by_in             IN                        VARCHAR
    );
END;

/

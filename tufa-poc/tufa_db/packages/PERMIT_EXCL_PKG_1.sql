--------------------------------------------------------
--  DDL for Package Body PERMIT_EXCL_PKG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "CTP_TUFA_MVP13"."PERMIT_EXCL_PKG" AS

   -- FUNCTION to return affected assessment for FULL or PARTIAL permit exclusions

    FUNCTION get_perm_excl_affected_assmnt (
        assmnt_yr            IN NUMBER,
        asmnt_qtr            IN NUMBER,
        company_id           IN NUMBER,
        exclusion_scope_id   IN NUMBER
    ) RETURN asmnt_type_arry IS

        v_assessment_yr         NUMBER;
        v_assessment_qtr        NUMBER;
        v_company_id            NUMBER;
        v_affected_asmnts       asmnt_type_arry;
        v_ann_affected_asmnts   asmnt_type_arry;
        v_qtrly_data_yr         NUMBER;
        v_qtrly_data_qtr        NUMBER;
        v_cig_data_yr           NUMBER;
        v_cig_data_qtr          NUMBER;

        v_index NUMBER;
    BEGIN

        -- Initializations
        v_assessment_yr := assmnt_yr;
        v_assessment_qtr := asmnt_qtr;
        v_company_id := company_id;

       -- calculate year and qtr when data was reported
        IF ( v_assessment_qtr = 1 ) THEN
            v_qtrly_data_yr := v_assessment_yr - 1;
            v_cig_data_yr := v_assessment_yr - 1;
            v_qtrly_data_qtr := 4;
            v_cig_data_qtr := v_assessment_qtr;
        ELSE
            v_qtrly_data_yr := v_assessment_yr;
            v_cig_data_yr := v_assessment_yr - 1;
            v_qtrly_data_qtr := v_assessment_qtr - 1;
            v_cig_data_qtr := v_assessment_qtr;
        END IF;

      --- check annual affected assessments

        SELECT DISTINCT
            ann_submitted.assessment_yr,
            -1 AS assessment_qtr,
            ann_submitted.assessment_type
        BULK COLLECT
        INTO v_ann_affected_asmnts
        FROM
            (
                SELECT
                    t_asmnt.assessment_id,
                    t_asmnt.assessment_yr,
                    t_asmnt.assessment_type,
                    MAX(t_version.version_num) AS last_version
                FROM
                    tu_assessment t_asmnt
                    INNER JOIN tu_annual_trueup t_ann_trueup ON t_ann_trueup.trueup_id = t_asmnt.trueup_id
                    INNER JOIN tu_assessment_version t_version ON t_version.assessment_id = t_asmnt.assessment_id
                WHERE
                    ( ( exclusion_scope_id = exl_scope_full_id
                        AND ( t_ann_trueup.fiscal_yr >= v_qtrly_data_yr
                              OR t_ann_trueup.fiscal_yr >= v_cig_data_yr ) )
                      OR ( exclusion_scope_id IN (
                        exl_scope_partial_id,
                        exl_scope_incl_id
                    )
                           AND ( t_ann_trueup.fiscal_yr = v_qtrly_data_yr
                                 OR t_ann_trueup.fiscal_yr = v_cig_data_yr ) ) )
                    AND T_ANN_TRUEUP.SUBMITTED_DT IS NOT NULL
                    --AND t_ann_trueup.submitted_ind = 'Y'
                    AND t_version.version_num > 0
                GROUP BY
                    t_asmnt.assessment_id,
                    t_asmnt.assessment_yr,
                    t_asmnt.assessment_type
            ) ann_submitted
            INNER JOIN tu_market_share t_mktsh ON t_mktsh.assessment_id = ann_submitted.assessment_id
                                                  AND ann_submitted.last_version = t_mktsh.version_num
                                                  AND t_mktsh.company_id = v_company_id;


-- check Qtrly/Cigar affected assessments

        SELECT DISTINCT
            asmnt_submitted.assessment_yr,
            asmnt_submitted.assessment_qtr,
            asmnt_submitted.assessment_type
        BULK COLLECT
        INTO v_affected_asmnts
        FROM
            (
                SELECT
                    t_asmnt_outer.assessment_id,
                    t_asmnt_outer.assessment_yr,
                    t_asmnt_outer.assessment_qtr,
                    t_asmnt_outer.assessment_type,
                    MAX(t_version.version_num) AS last_version
                FROM
                    tu_assessment t_asmnt_outer
                    INNER JOIN tu_assessment_version t_version ON t_version.assessment_id = t_asmnt_outer.assessment_id
                WHERE
                -- check for Qrtly/Cigar assessments when Partial exclusion or Inclusion
                    ( ( ( t_asmnt_outer.assessment_yr = v_qtrly_data_yr
                          AND t_asmnt_outer.assessment_qtr = v_qtrly_data_qtr
                          AND t_asmnt_outer.assessment_type = 'QTRY' )
                        OR ( t_asmnt_outer.assessment_yr = v_cig_data_yr
                             AND t_asmnt_outer.assessment_qtr = v_cig_data_qtr
                             AND t_asmnt_outer.assessment_type = 'CIQT' ) )
                      AND exclusion_scope_id IN (
                        exl_scope_partial_id,
                        exl_scope_incl_id
                    )
                      AND t_asmnt_outer.submitted_ind = 'Y' )
                    OR
                -- check for Qrtly/Cigar assessments when Full exclusion
                     ( ( ( ( ( t_asmnt_outer.assessment_yr = v_qtrly_data_yr
                                 AND t_asmnt_outer.assessment_qtr >= v_qtrly_data_qtr )
                               OR t_asmnt_outer.assessment_yr > v_qtrly_data_yr )
                             AND t_asmnt_outer.assessment_type = 'QTRY' )
                           OR ( ( ( t_asmnt_outer.assessment_yr = v_cig_data_yr
                                    AND t_asmnt_outer.assessment_qtr >= v_cig_data_qtr )
                                  OR t_asmnt_outer.assessment_yr > v_cig_data_yr )
                                AND t_asmnt_outer.assessment_type = 'CIQT' ) )
                         AND exclusion_scope_id = exl_scope_full_id
                         AND t_asmnt_outer.submitted_ind = 'Y' )
                    AND t_version.version_num > 0
                GROUP BY
                    t_asmnt_outer.assessment_id,
                    t_asmnt_outer.assessment_yr,
                    t_asmnt_outer.assessment_qtr,
                    t_asmnt_outer.assessment_type
            ) asmnt_submitted
            INNER JOIN tu_market_share t_mktsh ON t_mktsh.assessment_id = asmnt_submitted.assessment_id
                                                  AND asmnt_submitted.last_version = t_mktsh.version_num
        WHERE
            t_mktsh.company_id = v_company_id
            AND
        --   condition to filter out Qtrly/Cigar affected assessment if annual assessment already submitted for the years (v_qtrly_data_yr and v_cig_data_yr)
             NOT EXISTS (
       -- check annual assessment submitted
                SELECT
                    *
                FROM
                    (
                        SELECT
                            t_asmnt.assessment_id,
                            t_asmnt.assessment_yr,
                            t_asmnt.assessment_type,
                            MAX(t_version.version_num) AS last_version
                        FROM
                            tu_assessment t_asmnt
                            INNER JOIN tu_annual_trueup t_ann_trueup ON t_ann_trueup.trueup_id = t_asmnt.trueup_id
                            INNER JOIN tu_assessment_version t_version ON t_version.assessment_id = t_asmnt.assessment_id
                        WHERE
                            ( ( exclusion_scope_id = exl_scope_full_id
                                AND ( t_ann_trueup.fiscal_yr >= v_qtrly_data_yr
                                      OR t_ann_trueup.fiscal_yr >= v_cig_data_yr ) )
                              OR ( exclusion_scope_id IN (
                                exl_scope_partial_id,
                                exl_scope_incl_id
                            )
                                   AND ( t_ann_trueup.fiscal_yr = v_qtrly_data_yr
                                         OR t_ann_trueup.fiscal_yr = v_cig_data_yr ) ) )
                            --AND t_ann_trueup.submitted_ind = 'Y'
                            AND t_ann_trueup.SUBMITTED_DT IS NOT NULL
                            AND t_version.version_num > 0
                        GROUP BY
                            t_asmnt.assessment_id,
                            t_asmnt.assessment_yr,
                            t_asmnt.assessment_type
                    ) ann_submitted
                    INNER JOIN tu_market_share t_mktsh ON t_mktsh.assessment_id = ann_submitted.assessment_id
                                                          AND ann_submitted.last_version = t_mktsh.version_num
                                                          AND t_mktsh.company_id = v_company_id
                                                          AND ann_submitted.assessment_yr = asmnt_submitted.assessment_yr
            )
        ORDER BY
            asmnt_submitted.assessment_yr,
            asmnt_submitted.assessment_qtr;

        -- append affected annual assessments to cigar/quarterly affected assessments if any
        v_index :=  v_affected_asmnts.count;
        IF ( v_ann_affected_asmnts.count > 0 ) THEN
            v_affected_asmnts.extend(v_ann_affected_asmnts.count);
            FOR i IN 1..v_ann_affected_asmnts.count LOOP
                v_affected_asmnts(v_index + i) := v_ann_affected_asmnts(i);
            END LOOP;
        END IF;

         --for debugging purposes uncomment

        FOR i IN 1..v_affected_asmnts.count LOOP
            dbms_output.put_line('  affected assessments '
                                   || v_affected_asmnts(i).assmnt_rpt_data_yr
                                   || '  '
                                   || v_affected_asmnts(i).assmnt_rpt_data_qtr
                                   || '  '
                                   || v_affected_asmnts(i).assmnt_type);
        END LOOP;

        RETURN v_affected_asmnts;
    END get_perm_excl_affected_assmnt;

-- End Of :: get_perm_excl_affected_assmnt()

-- Function to check if permit exclusion is affecting assessments

    PROCEDURE affected_assmnt_exist (
        assmnt_yr              IN NUMBER,
        asmnt_qtr              IN NUMBER,
        company_id             IN NUMBER,
        exclusion_scope_id     IN NUMBER,
        permit_id              IN NUMBER,
        affected_assmnt_size   OUT NUMBER
    ) AS

        affected_assessments      asmnt_type_arry;
        current_aff_asmnts        aff_assmnt_type_arry;
        v_affected_assmnt_exist   BOOLEAN;
        v_assmnt_yr               VARCHAR(4);
        v_asmnt_qtr               VARCHAR(1);
        v_company_id              NUMBER;
        v_exclusion_scope_id      NUMBER;
        v_permit_id               NUMBER;

    BEGIN

    -- initializations
    v_assmnt_yr := assmnt_yr;
    v_asmnt_qtr := asmnt_qtr;
    v_company_id := company_id;
    v_exclusion_scope_id := exclusion_scope_id;
    v_permit_id := permit_id;
    v_affected_assmnt_exist := false;

    -- check affected assessments for  yr-qtr period exists;
    affected_assessments := permit_excl_pkg.get_perm_excl_affected_assmnt(v_assmnt_yr, v_asmnt_qtr, v_company_id, v_exclusion_scope_id);
    dbms_output.put_line(' getting current assessments ' || affected_assessments.count);
    BEGIN
        -- get current affected assessments
        SELECT
            affasmnt.perm_excl_audit_id,
            affasmnt.affected_assmnt_id,
            affasmnt.created_dt,
            affasmnt.created_by,
            affasmnt.assessment_status,
            affasmnt.assmnt_rpt_data_yr,
            affasmnt.assmnt_rpt_data_qtr,
            affasmnt.assmnt_type,
            affasmnt.assmnt_yr,
            affasmnt.assmnt_qtr
        BULK COLLECT
        INTO current_aff_asmnts
        FROM
            permit_excl_status_vw           pex
            INNER JOIN tu_permit_excl_affect_assmnts   affasmnt ON affasmnt.perm_excl_audit_id = pex.perm_excl_audit_id
        WHERE
            pex.company_id = v_company_id
            AND pex.permit_id = v_permit_id
            AND pex.assmnt_yr = v_assmnt_yr
            AND pex.assmnt_qtr = v_asmnt_qtr;

    EXCEPTION
        WHEN no_data_found THEN
            current_aff_asmnts := NULL;
    END;

    dbms_output.put_line(' affected_assessments.count ' || affected_assessments.count);
    dbms_output.put_line(' current_aff_asmnts.count ' || current_aff_asmnts.count);

     -- if affected assmnt not found in current affected assmnts then return indicator to display affected assessment warning
    FOR i IN 1..affected_assessments.count LOOP
        v_affected_assmnt_exist := false;
        FOR j IN 1..current_aff_asmnts.count LOOP
            IF current_aff_asmnts(j).assmnt_rpt_data_yr = affected_assessments(i).assmnt_rpt_data_yr
                AND current_aff_asmnts(j).assmnt_rpt_data_qtr = affected_assessments(i).assmnt_rpt_data_qtr
                AND current_aff_asmnts(j).assmnt_type = affected_assessments(i).assmnt_type
                AND current_aff_asmnts(j).assessment_status = permit_excl_pkg.asmnt_rerun_st THEN
                    v_affected_assmnt_exist := true;
            END IF;
        END LOOP;

        IF v_affected_assmnt_exist = false THEN
            EXIT;
        END IF;
    END LOOP;

    IF v_affected_assmnt_exist = false THEN
        affected_assmnt_size := affected_assessments.count;
    ELSE
        affected_assmnt_size := -1;
    END IF;
    dbms_output.put_line(' Assmnt Aff Warning ' || affected_assmnt_size);
    END;


-- End Of :: function affected_assmnt_exist()

-- Procedure to insert affected assessments.Calls get_perm_excl_affected_assmnt() function to ascertain affected assessments.

    PROCEDURE insert_affected_assessments (
        company_id_in           IN NUMBER,
        permit_id_in            IN NUMBER,
        excl_assmnt_yr_in       IN NUMBER,
        excl_assmnt_qtr_in      IN NUMBER,
        exclusion_scope_id_in   IN NUMBER,
        new_exclusion_id_in     IN NUMBER,
        last_exclusion_id_in    IN NUMBER,
        created_dt_in           IN DATE,
        created_by_in           IN VARCHAR
    ) AS

        old_aff_assmnt_arry    aff_assmnt_type_arry;
        new_aff_assmnt_arry    asmnt_type_arry;
        v_new_aff_asmnt        affectassmnttype;
        v_new_asmnt            assessmenttype;
        v_new_asmnt_exists     BOOLEAN;
        v_old_aff_asmnt        affectassmnttype;
        v_last_excl_scope_id   NUMBER;
        v_aff_asmnt_yr         NUMBER;
        v_aff_asmnt_qtr        NUMBER;
    BEGIN

        -- get last excluded  affected assessments
        BEGIN
            SELECT
                perm_excl_audit_id,
                affected_assmnt_id,
                created_dt,
                created_by,
                assessment_status,
                assmnt_rpt_data_yr,
                assmnt_rpt_data_qtr,
                assmnt_type,
                assmnt_yr,
                assmnt_qtr
            BULK COLLECT
            INTO old_aff_assmnt_arry
            FROM
                tu_permit_excl_affect_assmnts aff_assmnt
            WHERE
                perm_excl_audit_id = last_exclusion_id_in;

        EXCEPTION
            WHEN no_data_found THEN
                old_aff_assmnt_arry := NULL;
        END;

        BEGIN
            -- get last exclusion scope id
            SELECT
                pexl.exclusion_scope_id
            INTO v_last_excl_scope_id
            FROM
                tu_permit_excl_audit pexl
            WHERE
                pexl.perm_excl_audit_id = last_exclusion_id_in;

        EXCEPTION
            WHEN no_data_found THEN
                v_last_excl_scope_id :=-1;
        END;


            -- if last exclusion was a full and current is an Include
            -- get all assessments afected by the full exclusion and reset their statuses (no-action/re-run)

        IF ( v_last_excl_scope_id = exl_scope_full_id AND exclusion_scope_id_in = exl_scope_incl_id ) THEN
            new_aff_assmnt_arry := get_perm_excl_affected_assmnt(excl_assmnt_yr_in,excl_assmnt_qtr_in,company_id_in,v_last_excl_scope_id
            );
        ELSE
            new_aff_assmnt_arry := get_perm_excl_affected_assmnt(excl_assmnt_yr_in,excl_assmnt_qtr_in,company_id_in,exclusion_scope_id_in
            );
        END IF;

        FOR i IN 1..new_aff_assmnt_arry.count LOOP
            v_new_asmnt := new_aff_assmnt_arry(i);
            v_new_asmnt_exists := false;
            FOR j IN 1..old_aff_assmnt_arry.count LOOP
                v_old_aff_asmnt := old_aff_assmnt_arry(j);

                 -- If v_new_aff_asmnt exists in old_aff_assmnt_arry (assessment_yr,assessment_qtr,assessment_type check) and has status 'Complete'/'Re-Run'
                 -- Insert entry into TU_PERMIT_EXCL_AFFFECT_ASSMNTS as 'Complete'/'Re-Run' with new exclusion id
                IF ( v_new_asmnt.assmnt_rpt_data_yr = v_old_aff_asmnt.assmnt_rpt_data_yr AND v_new_asmnt.assmnt_rpt_data_qtr = v_old_aff_asmnt
                .assmnt_rpt_data_qtr AND v_new_asmnt.assmnt_type = v_old_aff_asmnt.assmnt_type ) THEN

                    -- If exclusion changing from Partial/Full to Include
                        -- If status 'Re-Run' change to null
                        -- If status 'Complete' change to 'Re-Run'
                    IF ( exclusion_scope_id_in = exl_scope_incl_id AND ( v_last_excl_scope_id = exl_scope_partial_id OR v_last_excl_scope_id
                    = exl_scope_full_id ) ) THEN
                        IF ( v_old_aff_asmnt.assessment_status = asmnt_rerun_st ) THEN
                            v_old_aff_asmnt.assessment_status := asmnt_noaction_st;
                        ELSIF ( v_old_aff_asmnt.assessment_status = asmnt_complete_st ) THEN
                            v_old_aff_asmnt.assessment_status := asmnt_rerun_st;
                        END IF;
                    END IF;

                     -- If exclusion changing from Include  to Partial/Full exclusion
                        -- If status null change to 'Re-Run'
                        -- If status 'Re-Run' change to null
                        -- If status 'Complete' change to 'Re-Run'

                    IF ( ( exclusion_scope_id_in = exl_scope_full_id OR exclusion_scope_id_in = exl_scope_partial_id ) AND v_last_excl_scope_id

                    = exl_scope_incl_id ) THEN
                        IF ( v_old_aff_asmnt.assessment_status = asmnt_noaction_st ) THEN
                            v_old_aff_asmnt.assessment_status := asmnt_rerun_st;
                        ELSIF ( v_old_aff_asmnt.assessment_status = asmnt_rerun_st ) THEN
                            v_old_aff_asmnt.assessment_status := asmnt_noaction_st;
                        ELSIF ( v_old_aff_asmnt.assessment_status = asmnt_complete_st ) THEN
                            v_old_aff_asmnt.assessment_status := asmnt_rerun_st;
                        END IF;

                    END IF;

                     -- If exclusion changing from Partial to Full exclusion carry pervious status in v_old_aff_asmnt.ASSESSMENT_STATUS
                            -- do nothing

                    v_old_aff_asmnt.permit_excl_audit_id := new_exclusion_id_in;
                    v_old_aff_asmnt.created_dt := created_dt_in;
                    v_old_aff_asmnt.created_by := created_by_in;
                    INSERT INTO tu_permit_excl_affect_assmnts (
                        perm_excl_audit_id,
                        created_dt,
                        created_by,
                        assessment_status,
                        assmnt_rpt_data_yr,
                        assmnt_rpt_data_qtr,
                        assmnt_type,
                        assmnt_yr,
                        assmnt_qtr
                    ) VALUES (
                        v_old_aff_asmnt.permit_excl_audit_id,
                        v_old_aff_asmnt.created_dt,
                        v_old_aff_asmnt.created_by,
                        v_old_aff_asmnt.assessment_status,
                        v_old_aff_asmnt.assmnt_rpt_data_yr,
                        v_old_aff_asmnt.assmnt_rpt_data_qtr,
                        v_old_aff_asmnt.assmnt_type,
                        v_old_aff_asmnt.assmnt_yr,
                        v_old_aff_asmnt.assmnt_qtr
                    );

                    v_new_asmnt_exists := true;
                    EXIT;
                END IF;

            END LOOP;

            -- If v_new_aff_asmnt does not exist in old_aff_assmnt_arry (assessment_id check)
                 -- insert entry as 'Re-Run' with new exclusion id

            IF ( v_new_asmnt_exists = false ) THEN
                v_new_aff_asmnt.permit_excl_audit_id := new_exclusion_id_in;
                v_new_aff_asmnt.created_dt := created_dt_in;
                v_new_aff_asmnt.created_by := created_by_in;
                v_new_aff_asmnt.assessment_status := asmnt_rerun_st;


            --  insert into tu_permit_excl_affect_assmnts new affected assessments ;
            --  Before inserting  calculate Assmnt_Yr and Assmnt_Qtr using period activity (v_new_asmnt.assmnt_rpt_data_yr, v_new_asmnt.assmnt_rpt_data_qtr, v_new_asmnt.assmnt_type)
                IF ( v_new_asmnt.assmnt_type = 'CIQT' ) THEN
                    v_aff_asmnt_yr := v_new_asmnt.assmnt_rpt_data_yr + 1;
                    v_aff_asmnt_qtr := v_new_asmnt.assmnt_rpt_data_qtr;
                ELSIF ( v_new_asmnt.assmnt_type = 'QTRY' ) THEN
                    IF ( v_new_asmnt.assmnt_rpt_data_qtr = 4 ) THEN
                        v_aff_asmnt_qtr := 1;
                        v_aff_asmnt_yr := v_new_asmnt.assmnt_rpt_data_yr + 1;
                    ELSE
                        v_aff_asmnt_qtr := v_new_asmnt.assmnt_rpt_data_qtr + 1;
                        v_aff_asmnt_yr := v_new_asmnt.assmnt_rpt_data_yr;
                    END IF;
                ELSIF ( v_new_asmnt.assmnt_type = 'ANNU' ) THEN
                    v_aff_asmnt_yr := v_new_asmnt.assmnt_rpt_data_yr;
                    v_aff_asmnt_qtr := v_new_asmnt.assmnt_rpt_data_qtr;
                END IF;

                -- TODO: change to use record type

                INSERT INTO tu_permit_excl_affect_assmnts (
                    perm_excl_audit_id,
                    created_dt,
                    created_by,
                    assessment_status,
                    assmnt_rpt_data_yr,
                    assmnt_rpt_data_qtr,
                    assmnt_type,
                    assmnt_yr,
                    assmnt_qtr
                ) VALUES (
                    new_exclusion_id_in,
                    created_dt_in,
                    created_by_in,
                    asmnt_rerun_st,
                    v_new_asmnt.assmnt_rpt_data_yr,
                    v_new_asmnt.assmnt_rpt_data_qtr,
                    v_new_asmnt.assmnt_type,
                    v_aff_asmnt_yr,
                    v_aff_asmnt_qtr
                );

            END IF;

        END LOOP;

    END insert_affected_assessments;

-- End Of::  insert_affected_assessments()



-- Procedure to evaluate and insert excluded permit-period details and affected assessments

    PROCEDURE insert_exclude_entries (
        company_id_in           IN NUMBER,
        permit_id_in            IN NUMBER,
        excl_assmnt_yr_in       IN NUMBER,
        assmnt_qtr_in           IN NUMBER,
        exclusion_scope_id_in   IN NUMBER,
        created_dt_in           IN DATE,
        created_by_in           IN VARCHAR,
        new_comment_id_in       IN NUMBER
    ) AS

    -- declarations

        v_new_excl_audit_id         NUMBER;
        v_last_excl_audit_id        NUMBER;
        v_permit_excl_vw_record     permitexclstatusvwtype;
        v_permit_excl_table_arry    permit_excl_type_arry;
        v_permit_excl_arry          permit_excl_vw_type_arry;
        v_permit_excl_record        permitexcltype;
        v_aff_asmnt_id              NUMBER;
        v_full_exclusion_audit_id   NUMBER;
        v_last_exclusion_scope_id   NUMBER;
        v_new_comment_seq           VARCHAR(4000);
        v_permit_type_cd            VARCHAR(5);
    BEGIN
        -- Full exclusion:
        IF ( exclusion_scope_id_in = exl_scope_full_id ) THEN
        -- Mark as 'Ex+'
            BEGIN
--                SELECT
--                    perm_excl_audit_id
--                INTO v_last_excl_audit_id
--                FROM
--                    permit_excl_status_vw pex
--                WHERE
--                    pex.company_id = company_id_in
--                    AND pex.permit_id = permit_id_in
--                    AND pex.assmnt_yr = excl_assmnt_yr_in
--                    AND pex.assmnt_qtr = assmnt_qtr_in;
                    SELECT perm_excl_audit_id INTO v_last_excl_audit_id FROM
                        (
                            SELECT perm_excl_audit_id, max(perm_excl_audit_id) OVER (PARTITION BY company_id,permit_id, assmnt_yr,assmnt_qtr) max_excl_id
                            FROM
                                permit_excl_status_vw pex
                            WHERE
                                pex.company_id = company_id_in
                                AND pex.permit_id = permit_id_in
                                AND pex.assmnt_yr = excl_assmnt_yr_in
                                AND pex.assmnt_qtr = assmnt_qtr_in
                        ) where perm_excl_audit_id = max_excl_id;

            EXCEPTION
                WHEN no_data_found THEN
                    v_last_excl_audit_id :=-1;
            END;

             -- If full exclusion get entries for FY-qtrs >= Current FY-qtr exclusion periods

            SELECT
                *
            BULK COLLECT
            INTO v_permit_excl_arry
            FROM
                permit_excl_status_vw perm_excl_status_vw
            WHERE
                perm_excl_status_vw.company_id = company_id_in
                AND perm_excl_status_vw.permit_id = permit_id_in
                AND ( ( perm_excl_status_vw.assmnt_yr = excl_assmnt_yr_in
                        AND perm_excl_status_vw.assmnt_qtr >= assmnt_qtr_in )
                      OR ( perm_excl_status_vw.assmnt_yr > excl_assmnt_yr_in ) )
                AND perm_excl_status_vw.excl_scope_id IN (
                    exl_scope_full_id,
                    exl_scope_partial_id
                )
            ORDER BY
                perm_excl_status_vw.assmnt_yr,
                perm_excl_status_vw.assmnt_qtr ASC;


            -- For each partial/full exclusion retrieved  set exclusion status as 'Included' and to  make the entry  a part of full exclusion
            -- insert new exclusion id for Current FY-qtr into FULL_PERM_EXCL_AUDIT_ID column in tu_permit_excl_audit table.

            FOR i IN 1..v_permit_excl_arry.count LOOP
                v_permit_excl_vw_record := v_permit_excl_arry(i);
                -- Mark as 'Included' and
                 -- insert into tu_permit_excl_audit
                INSERT INTO tu_permit_excl_audit (
                    company_id,
                    permit_id,
                    assmnt_yr,
                    assmnt_qtr,
                    comment_id,
                    exclusion_scope_id,
                    created_dt,
                    created_by
--                    full_perm_excl_audit_id
                ) VALUES (
                    company_id_in,
                    permit_id_in,
                    v_permit_excl_vw_record.assmnt_yr,
                    v_permit_excl_vw_record.assmnt_qtr,
                    v_permit_excl_vw_record.comment_id,
                    exl_scope_incl_id,
                    created_dt_in,
                    created_by_in
--                    v_full_exclusion_audit_id
                ) ;--RETURNING perm_excl_audit_id INTO v_new_excl_audit_id;

            END LOOP;

            INSERT INTO tu_permit_excl_audit (
                company_id,
                permit_id,
                assmnt_yr,
                assmnt_qtr,
                exclusion_scope_id,
                created_dt,
                created_by,
                comment_id
            ) VALUES (
                company_id_in,
                permit_id_in,
                excl_assmnt_yr_in,
                assmnt_qtr_in,
                exclusion_scope_id_in,
                created_dt_in,
                created_by_in,
                new_comment_id_in
            ) RETURNING perm_excl_audit_id INTO v_new_excl_audit_id;

--            v_full_exclusion_audit_id := v_new_excl_audit_id;


            -- insert affected assessments into tu_permit_excl_affect_assmnts
            insert_affected_assessments(company_id_in,permit_id_in,excl_assmnt_yr_in,assmnt_qtr_in,exclusion_scope_id_in,v_new_excl_audit_id
           ,v_last_excl_audit_id,created_dt_in,created_by_in);

--            -- If full exclusion get entries for FY-qtrs > Current FY-qtr exclusion periods
--
--            SELECT
--                *
--            BULK COLLECT
--            INTO v_permit_excl_arry
--            FROM
--                permit_excl_status_vw perm_excl_status_vw
--            WHERE
--                perm_excl_status_vw.company_id = company_id_in
--                AND perm_excl_status_vw.permit_id = permit_id_in
--                AND ( ( perm_excl_status_vw.assmnt_yr = excl_assmnt_yr_in
--                        AND perm_excl_status_vw.assmnt_qtr >= assmnt_qtr_in )
--                      OR ( perm_excl_status_vw.assmnt_yr > excl_assmnt_yr_in ) )
--                AND perm_excl_status_vw.excl_scope_id IN (
--                    exl_scope_full_id,
--                    exl_scope_partial_id
--                )
--            ORDER BY
--                perm_excl_status_vw.assmnt_yr,
--                perm_excl_status_vw.assmnt_qtr ASC;
--
--
--            -- For each partial/full exclusion retrieved  set exclusion status as 'Included' and to  make the entry  a part of full exclusion
--            -- insert new exclusion id for Current FY-qtr into FULL_PERM_EXCL_AUDIT_ID column in tu_permit_excl_audit table.
--
--            FOR i IN 1..v_permit_excl_arry.count LOOP
--                v_permit_excl_vw_record := v_permit_excl_arry(i);
--                -- Mark as 'Included' and
--                 -- insert into tu_permit_excl_audit
--                INSERT INTO tu_permit_excl_audit (
--                    company_id,
--                    permit_id,
--                    assmnt_yr,
--                    assmnt_qtr,
--                    comment_id,
--                    exclusion_scope_id,
--                    created_dt,
--                    created_by,
--                    full_perm_excl_audit_id
--                ) VALUES (
--                    company_id_in,
--                    permit_id_in,
--                    v_permit_excl_vw_record.assmnt_yr,
--                    v_permit_excl_vw_record.assmnt_qtr,
--                    v_permit_excl_vw_record.comment_id,
--                    exl_scope_incl_id,
--                    created_dt_in,
--                    created_by_in,
--                    v_full_exclusion_audit_id
--                ) RETURNING perm_excl_audit_id INTO v_new_excl_audit_id;
--
--            END LOOP;
exclusion_delta_status_cahnge(company_id_in,permit_id_in,excl_assmnt_yr_in,assmnt_qtr_in,exclusion_scope_id_in,NULL)

            ;
        END IF;
        -- End Full Exclusion Check

        -- Inclusion :

        IF ( exclusion_scope_id_in = exl_scope_incl_id ) THEN

            -- select the last exclusion
            BEGIN
--                SELECT
--                    *
--                INTO v_permit_excl_vw_record
--                FROM
--                    permit_excl_status_vw pex
--                WHERE
--                    pex.company_id = company_id_in
--                    AND pex.permit_id = permit_id_in
--                    AND pex.assmnt_yr = excl_assmnt_yr_in
--                    AND pex.assmnt_qtr = assmnt_qtr_in;
             select * INTO v_permit_excl_vw_record from permit_excl_status_vw where perm_excl_audit_id = (SELECT perm_excl_audit_id  FROM
                        (
                            SELECT pex.*, max(perm_excl_audit_id) OVER (PARTITION BY company_id,permit_id, assmnt_yr,assmnt_qtr) max_excl_id
                            FROM
                                permit_excl_status_vw pex
                            WHERE
                               pex.company_id = company_id_in
                               AND pex.permit_id = permit_id_in
                               AND pex.assmnt_yr = excl_assmnt_yr_in
                               AND pex.assmnt_qtr = assmnt_qtr_in
                        ) where perm_excl_audit_id = max_excl_id);
            EXCEPTION
                WHEN no_data_found THEN
                    v_permit_excl_vw_record := NULL;
            END;

            -- get last exclusion audit id

            IF ( v_permit_excl_vw_record.perm_excl_audit_id IS NOT NULL ) THEN
                v_last_excl_audit_id := v_permit_excl_vw_record.perm_excl_audit_id;
            ELSE
                v_last_excl_audit_id :=-1;
            END IF;

            IF ( v_last_excl_audit_id =-1 ) THEN
                return;
            END IF;

            -- If last exclusion was a 'FULL'
            -- get all other entries that were excluded as part of full exclusion
--            IF ( v_permit_excl_vw_record.excl_scope_id IS NOT NULL AND v_permit_excl_vw_record.excl_scope_id = exl_scope_full_id )
--            THEN
--                SELECT
--                    *
--                BULK COLLECT
--                INTO v_permit_excl_table_arry
--                FROM
--                    tu_permit_excl_audit pex
--                WHERE
--                    pex.full_perm_excl_audit_id = v_permit_excl_vw_record.perm_excl_audit_id;
--
--                -- For each excluded entry insert as 'Included' setting FULL_PERM_EXCL_AUDIT_ID as 'null'
--
--                FOR i IN 1..v_permit_excl_table_arry.count LOOP
--                    v_permit_excl_record := v_permit_excl_table_arry(i);
--                    v_permit_excl_record.full_perm_excl_audit_id := NULL;
--                    INSERT INTO tu_permit_excl_audit (
--                        company_id,
--                        permit_id,
--                        assmnt_yr,
--                        assmnt_qtr,
--                        exclusion_scope_id,
--                        created_dt,
--                        created_by,
--                        full_perm_excl_audit_id,
--                        comment_id
--                    ) VALUES (
--                        v_permit_excl_record.company_id,
--                        v_permit_excl_record.permit_id,
--                        v_permit_excl_record.assmnt_yr,
--                        v_permit_excl_record.assmnt_qtr,
--                        v_permit_excl_record.exclusion_scope_id,
--                        v_permit_excl_record.created_dt,
--                        v_permit_excl_record.created_by,
--                        v_permit_excl_record.full_perm_excl_audit_id,
--                        v_permit_excl_record.comment_id
--                    );
--
--                END LOOP;
--
--            END IF;


            -- Insert current FY-qtr as 'Included' and check for affected assessments
            -- To be changed to use record type

            INSERT INTO tu_permit_excl_audit (
                company_id,
                permit_id,
                assmnt_yr,
                assmnt_qtr,
                exclusion_scope_id,
                created_dt,
                created_by,
                comment_id
            ) VALUES (
                company_id_in,
                permit_id_in,
                excl_assmnt_yr_in,
                assmnt_qtr_in,
                exclusion_scope_id_in,
                created_dt_in,
                created_by_in,
                new_comment_id_in
            ) RETURNING perm_excl_audit_id INTO v_new_excl_audit_id;


            -- insert into tu_permit_excl_affect_assmnts

            insert_affected_assessments(company_id_in,permit_id_in,excl_assmnt_yr_in,assmnt_qtr_in,exclusion_scope_id_in,v_new_excl_audit_id

           ,v_last_excl_audit_id,created_dt_in,created_by_in);

            exclusion_delta_status_cahnge(company_id_in,permit_id_in,excl_assmnt_yr_in,assmnt_qtr_in,exclusion_scope_id_in,v_permit_excl_vw_record

            .excl_scope_id);

        END IF;

        -- Partial Exclusion :

        IF ( exclusion_scope_id_in = exl_scope_partial_id ) THEN
            BEGIN
                -- select the last exclusion audit id
                BEGIN
--                    SELECT
--                        perm_excl_audit_id
--                    INTO v_last_excl_audit_id
--                    FROM
--                        permit_excl_status_vw pex
--                    WHERE
--                        pex.company_id = company_id_in
--                        AND pex.permit_id = permit_id_in
--                        AND pex.assmnt_yr = excl_assmnt_yr_in
--                        AND pex.assmnt_qtr = assmnt_qtr_in;
                        SELECT perm_excl_audit_id INTO v_last_excl_audit_id FROM
                        (
                            SELECT perm_excl_audit_id, max(perm_excl_audit_id) OVER (PARTITION BY company_id,permit_id, assmnt_yr,assmnt_qtr) max_excl_id
                            FROM
                                permit_excl_status_vw pex
                            WHERE
                                pex.company_id = company_id_in
                                AND pex.permit_id = permit_id_in
                                AND pex.assmnt_yr = excl_assmnt_yr_in
                                AND pex.assmnt_qtr = assmnt_qtr_in
                        ) where perm_excl_audit_id = max_excl_id;

                EXCEPTION
                    WHEN no_data_found THEN
                        v_last_excl_audit_id :=-1;
                END;

                -- To be changed to use record type

                INSERT INTO tu_permit_excl_audit (
                    company_id,
                    permit_id,
                    assmnt_yr,
                    assmnt_qtr,
                    exclusion_scope_id,
                    created_dt,
                    created_by,
                    comment_id
                ) VALUES (
                    company_id_in,
                    permit_id_in,
                    excl_assmnt_yr_in,
                    assmnt_qtr_in,
                    exclusion_scope_id_in,
                    created_dt_in,
                    created_by_in,
                    new_comment_id_in
                ) RETURNING perm_excl_audit_id INTO v_new_excl_audit_id;


                -- insert into tu_permit_excl_affect_assmnts

                insert_affected_assessments(company_id_in,permit_id_in,excl_assmnt_yr_in,assmnt_qtr_in,exclusion_scope_id_in,v_new_excl_audit_id

               ,v_last_excl_audit_id,created_dt_in,created_by_in);

                exclusion_delta_status_cahnge(company_id_in,permit_id_in,excl_assmnt_yr_in,assmnt_qtr_in,exclusion_scope_id_in,NULL

                );
            END;
        END IF;

        -- Determine type of permit

        SELECT
            permit_type_cd
        INTO v_permit_type_cd
        FROM
            tu_permit
        WHERE
            tu_permit.permit_id = permit_id_in;

        -- Mark ingested TTB ingested entries as excluded/included
            -- last_permit_excl_record only used in case of inclusion

        IF ( v_permit_type_cd = permit_type_cd_manu ) THEN
            ttb_ingest_entries_exclude(company_id_in,permit_id_in,excl_assmnt_yr_in,assmnt_qtr_in,exclusion_scope_id_in,v_new_excl_audit_id
           ,v_permit_excl_vw_record,created_dt_in,created_by_in);
        ELSIF ( v_permit_type_cd = permit_type_cd_imp ) THEN
--         Mark ingested CBP ingested entries as excluded/included
--              last_permit_excl_record only used in case of inclusion
            cbp_ingest_entries_exclude(company_id_in,permit_id_in,excl_assmnt_yr_in,assmnt_qtr_in,exclusion_scope_id_in,v_new_excl_audit_id
           ,v_permit_excl_vw_record,created_dt_in,created_by_in);
        END IF;

    END insert_exclude_entries;

-- End Of :: insert_exclude_entries()

-- Include/Exclude TTB Ingested data

    PROCEDURE ttb_ingest_entries_exclude (
        company_id_in             IN NUMBER,
        permit_id_in              IN NUMBER,
        assmnt_yr_in              IN NUMBER,
        assmnt_qtr_in             IN NUMBER,
        exclusion_scope_id_in     IN NUMBER,
        perm_excl_audit_id_in     IN NUMBER,
        last_permit_excl_record   IN permitexclstatusvwtype,
        created_dt_in             IN DATE,
        created_by_in             IN VARCHAR
    ) AS
    BEGIN
   -- for Partial/Full exclusion - TTB
        IF ( exclusion_scope_id_in = exl_scope_full_id OR exclusion_scope_id_in = exl_scope_partial_id ) THEN
            INSERT INTO tu_ttb_ingested_excl (
                ttb_annual_tax_id,
                perm_excl_audit_id,
                modified_by,
                modified_dt
            )
                SELECT
                    ttb_annual_tax_id,
                    perm_excl_audit_id_in,
                    created_by_in,
                    created_dt_in
                FROM
                    tu_ttb_ingested_excl_vw ingestvw
                WHERE
                    ingestvw.ttb_company_id = company_id_in
                    AND ingestvw.ttb_permit_id = permit_id_in
                    AND ( ( ingestvw.asmt_qtr = assmnt_qtr_in
                            AND ingestvw.asmt_yr = assmnt_yr_in
                            AND exclusion_scope_id_in = exl_scope_partial_id )
                          OR ( ( ( ingestvw.asmt_qtr >= assmnt_qtr_in
                                   AND ingestvw.asmt_yr = assmnt_yr_in )
                                 OR ingestvw.asmt_yr > assmnt_yr_in )
                               AND exclusion_scope_id_in = exl_scope_full_id ) );

   -- for Inclusion - TTB

        ELSIF ( exclusion_scope_id_in = exl_scope_incl_id ) THEN
            INSERT INTO tu_ttb_ingested_excl (
                ttb_annual_tax_id,
                perm_excl_audit_id,
                modified_by,
                modified_dt
            )
                SELECT
                    ttb_annual_tax_id,
                    NULL,
                    created_by_in,
                    created_dt_in
                FROM
                    tu_ttb_ingested_excl_vw ingestvw
                WHERE
                    ingestvw.ttb_company_id = company_id_in
                    AND ingestvw.ttb_permit_id = permit_id_in
                    AND ingestvw.perm_excl_audit_id IS NOT NULL
                    AND ( ( ingestvw.asmt_qtr = assmnt_qtr_in
                            AND ingestvw.asmt_yr = assmnt_yr_in
                            AND last_permit_excl_record.excl_scope_id = exl_scope_partial_id )
                          OR ( ( ( ingestvw.asmt_qtr >= assmnt_qtr_in
                                   AND ingestvw.asmt_yr = assmnt_yr_in )
                                 OR ingestvw.asmt_yr > assmnt_yr_in )
                               AND last_permit_excl_record.excl_scope_id = exl_scope_full_id ) );

        END IF;
    END;

-- End Of :: ttb_ingest_entries_exclude()

-- Include/Exclude CBP Ingested data

    PROCEDURE cbp_ingest_entries_exclude (
        company_id_in             IN NUMBER,
        permit_id_in              IN NUMBER,
        assmnt_yr_in              IN NUMBER,
        assmnt_qtr_in             IN NUMBER,
        exclusion_scope_id_in     IN NUMBER,
        perm_excl_audit_id_in     IN NUMBER,
        last_permit_excl_record   IN permitexclstatusvwtype,
        created_dt_in             IN DATE,
        created_by_in             IN VARCHAR
    ) AS
    BEGIN
   -- for Partial/Full exclusion - CBP
        IF ( exclusion_scope_id_in = exl_scope_full_id OR exclusion_scope_id_in = exl_scope_partial_id ) THEN
            INSERT INTO tu_cbp_ingested_excl (
                cbp_entry_id,
                perm_excl_audit_id,
                modified_by,
                modified_dt
            )
                SELECT
                    cbp_entry_id,
                    perm_excl_audit_id_in,
                    created_by_in,
                    created_dt_in
                FROM
                    tu_cbp_ingested_excl_vw ingestvw
                WHERE
                    ingestvw.company_id = company_id_in
                    AND ( ( ingestvw.asmt_qtr = assmnt_qtr_in
                            AND ingestvw.asmt_yr = assmnt_yr_in
                            AND exclusion_scope_id_in = exl_scope_partial_id )
                          OR ( ( ( ingestvw.asmt_qtr >= assmnt_qtr_in
                                   AND ingestvw.asmt_yr = assmnt_yr_in )
                                 OR ingestvw.asmt_yr > assmnt_yr_in )
                               AND exclusion_scope_id_in = exl_scope_full_id ) );
   -- for Inclusion - CBP

        ELSIF ( exclusion_scope_id_in = exl_scope_incl_id ) THEN
            INSERT INTO tu_cbp_ingested_excl (
                cbp_entry_id,
                perm_excl_audit_id,
                modified_by,
                modified_dt
            )
                SELECT
                    cbp_entry_id,
                    NULL,
                    created_by_in,
                    created_dt_in
                FROM
                    tu_cbp_ingested_excl_vw ingestvw
                WHERE
                    ingestvw.company_id = company_id_in
                    AND ingestvw.perm_excl_audit_id IS NOT NULL
                    AND ( ( ingestvw.asmt_qtr = assmnt_qtr_in
                            AND ingestvw.asmt_yr = assmnt_yr_in
                            AND last_permit_excl_record.excl_scope_id = exl_scope_partial_id )
                          OR ( ( ( ingestvw.asmt_qtr >= assmnt_qtr_in
                                   AND ingestvw.asmt_yr = assmnt_yr_in )
                                 OR ingestvw.asmt_yr > assmnt_yr_in )
                               AND last_permit_excl_record.excl_scope_id = exl_scope_full_id ) );

        END IF;
    END;

-- End Of :: cbp_ingest_entries_exclude()

--  Entry point procedure for exclude permit functionality

    PROCEDURE exclude_permit_from_mktsh (
        company_id            IN NUMBER DEFAULT NULL,
        permit_id             IN NUMBER DEFAULT NULL,
        excl_asmt_fiscal_yr   IN NUMBER DEFAULT NULL,
        excl_qtr_1            IN NUMBER DEFAULT NULL,
        excl_qtr_2            IN NUMBER DEFAULT NULL,
        excl_qtr_3            IN NUMBER DEFAULT NULL,
        excl_qtr_4            IN NUMBER DEFAULT NULL,
        exclusion_flag        IN VARCHAR DEFAULT NULL,
        user_logged           IN VARCHAR,
        new_comment_id        IN NUMBER DEFAULT NULL
    ) AS

        v_company_id             NUMBER;
        v_permit_id              NUMBER;
        v_user                   VARCHAR(50);
        v_excl_qtr               NUMBER;
        v_excl_dt                DATE;
        v_permit_excl_scope_id   NUMBER;
        v_excl_asmt_fiscal_yr    NUMBER;
        v_excl_asmt_qtrs         number_arry_type;
        v_qtr_cnt                NUMBER;
        v_new_comment_id         NUMBER;
    BEGIN

      --  initializations
        v_excl_asmt_qtrs := number_arry_type ();
        v_excl_asmt_fiscal_yr := excl_asmt_fiscal_yr;
        v_company_id := company_id;
        v_permit_id := permit_id;
        v_user := user_logged;
        v_new_comment_id := new_comment_id;

        -- set exclusion date as sysdate. Could potentially be utilized in retrieving last excluded period statuses
        v_excl_dt := SYSDATE;

        -- need to change and pass in array of qtrs to the procedure
        -- add qtr parameters to v_excl_asmt_qtrs array
        v_excl_asmt_qtrs := number_arry_type ();
        v_qtr_cnt := 0;
        IF ( excl_qtr_1 IS NOT NULL ) THEN
            v_excl_asmt_qtrs.extend ();
            v_qtr_cnt := v_qtr_cnt + 1;
            v_excl_asmt_qtrs(v_qtr_cnt) := excl_qtr_1;
        END IF;

        IF ( excl_qtr_2 IS NOT NULL ) THEN
            v_excl_asmt_qtrs.extend ();
            v_qtr_cnt := v_qtr_cnt + 1;
            v_excl_asmt_qtrs(v_qtr_cnt) := excl_qtr_2;
        END IF;

        IF ( excl_qtr_3 IS NOT NULL ) THEN
            v_excl_asmt_qtrs.extend ();
            v_qtr_cnt := v_qtr_cnt + 1;
            v_excl_asmt_qtrs(v_qtr_cnt) := excl_qtr_3;
        END IF;

        IF ( excl_qtr_4 IS NOT NULL ) THEN
            v_excl_asmt_qtrs.extend ();
            v_qtr_cnt := v_qtr_cnt + 1;
            v_excl_asmt_qtrs(v_qtr_cnt) := excl_qtr_4;
        END IF;


        -- based on exclusion flag set permit_exclusion_scope_id from TU_PERMIT_EXCLUDE_SCOPE

        IF exclusion_flag IS NOT NULL AND exclusion_flag = 'FULL' THEN
            v_permit_excl_scope_id := exl_scope_full_id;
        ELSIF exclusion_flag IS NOT NULL AND exclusion_flag = 'PARTIAL' THEN
            v_permit_excl_scope_id := exl_scope_partial_id;
        ELSIF exclusion_flag IS NOT NULL AND exclusion_flag = 'INCLUDE' THEN
            v_permit_excl_scope_id := exl_scope_incl_id;
        END IF;


        -- TODO: For each excluded year loop. In case of Partial exclusion multiple exclusion years can be passed in as input parameters
        -- Context : year

        -- For each quarter do

        FOR i IN 1..v_excl_asmt_qtrs.count LOOP

             -- Context QTR
             -- set asmnt quarter to be excluded
            v_excl_qtr := v_excl_asmt_qtrs(i);
            IF ( v_excl_qtr IS NOT NULL ) THEN
                insert_exclude_entries(v_company_id,v_permit_id,v_excl_asmt_fiscal_yr,v_excl_qtr,v_permit_excl_scope_id,v_excl_dt
               ,v_user,v_new_comment_id);

            END IF;

        END LOOP;
        -- quarter loop end

    END;  -- End Of: exclude_permit_from_mktsh()

 -- KYLE
 -- Entry point procedure for updating the delta status

    PROCEDURE exclusion_delta_status_cahnge (
        company_id_in           IN NUMBER,
        permit_id_in            IN NUMBER,
        excl_assmnt_yr_in       IN NUMBER,
        excl_assmnt_qtr_in      IN NUMBER,
        exclusion_scope_id_in   IN NUMBER,
        current_scope_id_in     IN NUMBER
    ) AS

        v_permit_type           VARCHAR(4);
        v_prev_yr               NUMBER;
        v_prev_qtr              NUMBER;
        v_next_yr               NUMBER;
        v_next_qtr              NUMBER;
        v_cigar_yr              NUMBER;
        v_is_cigar_excluded     VARCHAR2(1);
        v_is_cigar_conflicted   VARCHAR2(1);
    BEGIN

    -- Get the year and quarter before the current exclusion selection
        IF excl_assmnt_qtr_in = 1 THEN
            v_prev_yr := excl_assmnt_yr_in - 1;
            v_prev_qtr := 4;
        ELSE
            v_prev_yr := excl_assmnt_yr_in;
            v_prev_qtr := excl_assmnt_qtr_in - 1;
        END IF;

--        dbms_output.put_line('Previous :: '
--                               || v_prev_yr
--                               || ' Q'
--                               || v_prev_qtr);
--        dbms_output.put_line('Current :: '
--                               || excl_assmnt_yr_in
--                               || ' Q'
--                               || excl_assmnt_qtr_in);

    -- Get the year and quarter after the current exclusion selection
        IF excl_assmnt_qtr_in = 4 THEN
            v_next_yr := excl_assmnt_yr_in + 1;
            v_next_qtr := 1;
        ELSE
            v_next_yr := excl_assmnt_yr_in;
            v_next_qtr := excl_assmnt_qtr_in + 1;
        END IF;

--        dbms_output.put_line('Next :: '
--                               || v_next_yr
--                               || ' Q'
--                               || v_next_qtr);
--
--    -- Get the cigar year (quarter is the same for cigar)
        v_cigar_yr := excl_assmnt_yr_in - 1;
--        dbms_output.put_line('Cigar :: '
--                               || v_cigar_yr
--                               || ' Q'
--                               || excl_assmnt_qtr_in);

        -- Determine if the full year is excluded for cigars
        BEGIN
            SELECT
                DECODE(COUNT(*),1,'Y','N')
            INTO v_is_cigar_excluded
            FROM
                (
                    SELECT
                        MIN(assmnt_qtr) AS range_start,
                        MAX(assmnt_qtr) AS range_end,
                        MIN(excl_scope_id) exclusion,
                        COUNT(*) AS range_count
                    FROM
                        (
                            SELECT
                                assmnt_qtr,
                                excl_scope_id,
                                ROW_NUMBER() OVER(
                                    ORDER BY
                                        assmnt_qtr,excl_scope_id
                                ) AS row_no,
                                assmnt_qtr - ROW_NUMBER() OVER(
                                    ORDER BY
                                        assmnt_qtr,excl_scope_id
                                ) AS grp
                            FROM
                                (
                                    SELECT DISTINCT
                                        assmnt_qtr,
                                        excl_scope_id
                                    FROM
                                        permit_excl_status_vw
                                    WHERE
                                        assmnt_yr = excl_assmnt_yr_in
                                        AND company_id = company_id_in
                                        AND excl_scope_id <> 3
                                )
                        )
                    GROUP BY
                        grp
                    ORDER BY
                        1
                )
            WHERE
                ( range_start = 1
                  AND exclusion = 1 )
                OR ( range_start = 1
                     AND range_count = 4 );

            SELECT
                DECODE(COUNT(*),0,'N','Y')
            INTO v_is_cigar_conflicted
            FROM
                permit_excl_status_vw
            WHERE
                assmnt_yr = excl_assmnt_yr_in
                AND company_id = company_id_in
                AND excl_scope_id <> 3;

        END;

--        dbms_output.put_line('Cigar Excluded :: ' || v_is_cigar_excluded);
--        dbms_output.put_line('Cigar Conflicted :: ' || v_is_cigar_conflicted);

    -- GET PERMIT TYPE
        SELECT
            permit_type_cd
        INTO v_permit_type
        FROM
            tu_permit
        WHERE
            permit_id = permit_id_in;

--        dbms_output.put_line('Permit Type :: ' || v_permit_type);
     -- END OF INITALIZATIONS -----------------------------------------------------------------------
        BEGIN
            IF ( exclusion_scope_id_in IN (
                exl_scope_full_id,
                exl_scope_partial_id
            ) ) THEN
                IF v_permit_type = 'IMPT' THEN
--                    dbms_output.put_line('Importer exclude');
                    UPDATE tu_cbp_amendment
                    SET
                        prev_accept_flag = nvl(prev_accept_flag,decode(acceptance_flag,'E',null,acceptance_flag)),
                        in_progress_flag = 'N',
                        acceptance_flag = 'E'
                    WHERE
                        cbp_company_id = company_id_in
                        AND ( ( tobacco_class_id <> 8
                                AND fiscal_yr = v_prev_yr
                                AND qtr = v_prev_qtr )
                              OR ( tobacco_class_id = 8
                                   AND v_is_cigar_excluded = 'Y'
                                   AND fiscal_yr = v_cigar_yr ) );

                ELSIF v_permit_type = 'MANU' THEN
                    UPDATE tu_ttb_amendment
                    SET
                        prev_accept_flag = nvl(prev_accept_flag,decode(acceptance_flag,'E',null,acceptance_flag)),
                        in_progress_flag = 'N',
                        acceptance_flag = 'E'
                    WHERE
                        ttb_company_id = company_id_in
                        AND ( ( tobacco_class_id <> 8
                                AND fiscal_yr = v_prev_yr
                                AND qtr = v_prev_qtr )
                              OR ( tobacco_class_id = 8
                                   AND v_is_cigar_excluded = 'Y'
                                   AND fiscal_yr = v_cigar_yr ) );

                END IF;

                UPDATE tu_comparison_all_delta_sts
                SET
                    prev_status = nvl(prev_status,decode(status,'Excluded X', null, status)),
                    status = 'Excluded X'
                WHERE
                    cmp_all_delta_id IN (
                        SELECT
                            allsts.cmp_all_delta_id
                        FROM
                            tu_comparison_all_delta_sts allsts
                            JOIN tu_company company ON allsts.ein = company.ein
                        WHERE
                            company.company_id = company_id_in
                            AND allsts.fiscal_qtr <> '1-4'
                            AND allsts.permit_type = v_permit_type
                            AND ( ( allsts.fiscal_yr = v_prev_yr
                                    AND allsts.fiscal_qtr = v_prev_qtr )
                                  OR ( allsts.delta_type = 'TUFA'
                                       AND exclusion_scope_id_in = exl_scope_full_id
                                       AND ( allsts.fiscal_yr > v_prev_yr
                                             OR ( allsts.fiscal_yr = v_prev_yr
                                                  AND allsts.fiscal_qtr > v_prev_qtr ) ) ) )
                    );

--dbms_output.put_line('UPDATE EXCLUDE CIGAR : ' || v_cigar_yr || v_is_cigar_excluded || exclusion_scope_id_in);
                UPDATE tu_comparison_all_delta_sts
                SET
                    prev_status = nvl(prev_status,decode(status,'Excluded X', null, status)),
                    status = 'Excluded X'
                WHERE
                    cmp_all_delta_id IN (
                        SELECT
                            allsts.cmp_all_delta_id
                        FROM
                            tu_comparison_all_delta_sts allsts
                            JOIN tu_company company ON allsts.ein = company.ein
                        WHERE
                            company.company_id = company_id_in
                            AND allsts.fiscal_qtr = '1-4'
                            AND allsts.permit_type = v_permit_type
                            AND v_is_cigar_excluded = 'Y'
                            AND ( ( exclusion_scope_id_in = exl_scope_full_id
                                    AND ( fiscal_yr = v_cigar_yr
                                          OR ( delta_type = 'TUFA'
                                               AND fiscal_yr > v_cigar_yr ) ) )
                                  OR ( exclusion_scope_id_in = exl_scope_partial_id
                                       AND ( fiscal_yr = v_cigar_yr
                                             OR ( delta_type = 'TUFA'
                                                  AND fiscal_yr = excl_assmnt_yr_in ) ) ) )
                    );

            END IF;

-- Set Conflicts

            IF exclusion_scope_id_in = exl_scope_full_id THEN
                IF v_permit_type = 'IMPT' THEN
                    UPDATE tu_cbp_amendment
                    SET
                        prev_accept_flag = nvl(prev_accept_flag,decode(acceptance_flag,'E',null,acceptance_flag)),
                        in_progress_flag = 'Y',
                        acceptance_flag = NULL
                    WHERE
                        cbp_company_id = company_id_in
                        AND ( ( tobacco_class_id <> 8
                                AND ( ( fiscal_yr = excl_assmnt_yr_in
                                        AND qtr >= excl_assmnt_qtr_in )
                                      OR fiscal_yr > excl_assmnt_yr_in ) )
                              OR ( tobacco_class_id = 8
                                   AND fiscal_yr >= excl_assmnt_yr_in ) );

                ELSIF v_permit_type = 'MANU' THEN
                    UPDATE tu_ttb_amendment
                    SET
                        prev_accept_flag = nvl(prev_accept_flag,decode(acceptance_flag,'E',null,acceptance_flag)),
                        in_progress_flag = 'Y',
                        acceptance_flag = NULL
                    WHERE
                        ttb_company_id = company_id_in
                        AND ( ( tobacco_class_id <> 8
                                AND ( ( fiscal_yr = excl_assmnt_yr_in
                                        AND qtr >= excl_assmnt_qtr_in )
                                      OR fiscal_yr > excl_assmnt_yr_in ) )
                              OR ( tobacco_class_id = 8
                                   AND fiscal_yr >= excl_assmnt_yr_in ) );

                END IF;

                UPDATE tu_comparison_all_delta_sts
                SET
                    prev_status = nvl(prev_status,decode(status,'Excluded X', null, status)),
                    status = 'In Progress'
                WHERE
                    cmp_all_delta_id IN (
                        SELECT
                            allsts.cmp_all_delta_id
                        FROM
                            tu_comparison_all_delta_sts allsts
                            JOIN tu_company company ON allsts.ein = company.ein
                        WHERE
                            company.company_id = company_id_in
                            AND allsts.permit_type = v_permit_type
                            AND allsts.delta_type = 'INGESTED'
                            AND ( allsts.fiscal_yr > excl_assmnt_yr_in
                                  OR ( allsts.fiscal_yr = excl_assmnt_yr_in
                                       AND DECODE(allsts.fiscal_qtr,'1-4',5,allsts.fiscal_qtr) >= excl_assmnt_qtr_in ) )
                    );

            ELSIF exclusion_scope_id_in = exl_scope_partial_id THEN
--                dbms_output.put_line('Partial');
                IF v_permit_type = 'IMPT' THEN
                    UPDATE tu_cbp_amendment
                    SET
                        prev_accept_flag = nvl(prev_accept_flag,decode(acceptance_flag,'E',null,acceptance_flag)),
                        in_progress_flag = 'Y',
                        acceptance_flag = NULL
                    WHERE
                        cbp_company_id = company_id_in
                        AND ( ( tobacco_class_id <> 8
                                AND fiscal_yr = excl_assmnt_yr_in
                                AND qtr = excl_assmnt_qtr_in
                                AND NOT EXISTS (
                            SELECT
                                'X'
                            FROM
                                permit_excl_status_vw
                            WHERE
                                company_id = company_id_in
                                AND assmnt_yr = v_next_yr
                                AND assmnt_qtr = v_next_qtr
                                AND excl_scope_id IN (
                                    exl_scope_full_id,
                                    exl_scope_partial_id
                                )
                        ) )
                              OR ( tobacco_class_id = 8
                                   AND fiscal_yr = excl_assmnt_yr_in ) );

                ELSIF v_permit_type = 'MANU' THEN
                    UPDATE tu_ttb_amendment
                    SET
                        prev_accept_flag = nvl(prev_accept_flag,decode(acceptance_flag,'E',null,acceptance_flag)),
                        in_progress_flag = 'Y',
                        acceptance_flag = NULL
                    WHERE
                        ttb_company_id = company_id_in
                        AND ( ( tobacco_class_id <> 8
                                AND fiscal_yr = excl_assmnt_yr_in
                                AND qtr = excl_assmnt_qtr_in
                                AND NOT EXISTS (
                            SELECT
                                'X'
                            FROM
                                permit_excl_status_vw
                            WHERE
                                company_id = company_id_in
                                AND assmnt_yr = v_next_yr
                                AND assmnt_qtr = v_next_qtr
                                AND excl_scope_id IN (
                                    exl_scope_full_id,
                                    exl_scope_partial_id
                                )
                        ) )
                              OR ( tobacco_class_id = 8
                                   AND fiscal_yr = excl_assmnt_yr_in ) );

                END IF;

                UPDATE tu_comparison_all_delta_sts
                SET
                    prev_status = nvl(prev_status,decode(status,'Excluded X', null, status)),
                    status = 'In Progress'
                WHERE
                    cmp_all_delta_id IN (
                        SELECT
                            allsts.cmp_all_delta_id
                        FROM
                            tu_comparison_all_delta_sts allsts
                            JOIN tu_company company ON allsts.ein = company.ein
                        WHERE
                            company.company_id = company_id_in
                            AND allsts.permit_type = v_permit_type
                            AND allsts.delta_type = 'INGESTED'
                            AND allsts.fiscal_yr = excl_assmnt_yr_in
                            AND DECODE(allsts.fiscal_qtr,'1-4',5,allsts.fiscal_qtr) IN (
                                excl_assmnt_qtr_in,
                                5
                            )
                    );

            ELSIF exclusion_scope_id_in = exl_scope_incl_id THEN
                IF v_permit_type = 'IMPT' THEN
--                    dbms_output.put_line(current_scope_id_in
--                                           || ' '
--                                           || exl_scope_partial_id);
                    UPDATE tu_cbp_amendment amend
                    SET
                        acceptance_flag = prev_accept_flag,
                        in_progress_flag = decode(prev_accept_flag, NULL, 'Y','N'),
                        prev_accept_flag = NULL
                    WHERE
                        cbp_company_id = company_id_in
                        AND (prev_accept_flag IS NOT NULL OR acceptance_flag = 'E')
                        AND ( ( current_scope_id_in = exl_scope_partial_id
                                AND ( ( amend.tobacco_class_id <> 8
                                        AND ( ( amend.fiscal_yr = v_prev_yr
                                                AND amend.qtr = v_prev_qtr )
                                              OR ( amend.fiscal_yr = excl_assmnt_yr_in
                                                   AND amend.qtr = excl_assmnt_qtr_in ) ) )
                                      OR ( amend.tobacco_class_id = 8
                                           AND ( amend.fiscal_yr IN (
                            v_cigar_yr,
                            excl_assmnt_yr_in
                        ) ) ) ) )
                              OR ( current_scope_id_in = exl_scope_full_id
                                   AND ( ( amend.tobacco_class_id <> 8
                                           AND ( ( amend.fiscal_yr = v_prev_yr
                                                   AND amend.qtr >= v_prev_qtr )
                                                 OR amend.fiscal_yr > v_prev_yr ) )
                                         OR ( amend.tobacco_class_id = 8
                                              AND amend.fiscal_yr >= v_prev_yr ) ) ) );

                    UPDATE tu_cbp_amendment amend
                    SET
                        prev_accept_flag = nvl(prev_accept_flag,decode(acceptance_flag,'E',null,acceptance_flag)),
                        acceptance_flag = NULL,
                        in_progress_flag = 'Y'
                    WHERE
                        cbp_company_id = company_id_in
                        AND ( ( tobacco_class_id <> 8
                                AND fiscal_yr = v_prev_yr
                                AND qtr = v_prev_qtr
                                AND EXISTS (
                            SELECT
                                'X'
                            FROM
                                permit_excl_status_vw
                            WHERE
                                company_id = amend.cbp_company_id
                                AND assmnt_yr = amend.fiscal_yr
                                AND assmnt_qtr = amend.qtr
                                AND excl_scope_id = exl_scope_partial_id
                        ) )
                              OR ( tobacco_class_id = 8
                                   AND fiscal_yr = excl_assmnt_yr_in
                                   AND v_is_cigar_conflicted = 'Y' ) );

                    UPDATE tu_cbp_amendment amend
                    SET
                        prev_accept_flag = nvl(prev_accept_flag,decode(acceptance_flag,'E',null,acceptance_flag)),
                        acceptance_flag = 'E',
                        in_progress_flag = 'N'
                    WHERE
                        cbp_company_id = company_id_in
                        AND fiscal_yr = excl_assmnt_yr_in
                        AND qtr = excl_assmnt_qtr_in
                        AND EXISTS (
                            SELECT
                                'X'
                            FROM
                                permit_excl_status_vw
                            WHERE
                                company_id = amend.cbp_company_id
                                AND assmnt_yr = v_next_yr
                                AND assmnt_qtr = v_next_qtr
                                AND excl_scope_id IN (
                                    exl_scope_full_id,
                                    exl_scope_partial_id
                                )
                        );

                ELSIF v_permit_type = 'MANU' THEN
                    UPDATE tu_ttb_amendment amend
                    SET
                        acceptance_flag = prev_accept_flag,
                        in_progress_flag = decode(prev_accept_flag, NULL, 'Y','N'),
                        prev_accept_flag = NULL
                    WHERE
                        ttb_company_id = company_id_in
                        AND (prev_accept_flag IS NOT NULL OR acceptance_flag = 'E')
                        AND ( ( current_scope_id_in = exl_scope_partial_id
                                AND ( ( amend.tobacco_class_id <> 8
                                        AND ( ( amend.fiscal_yr = v_prev_yr
                                                AND amend.qtr = v_prev_qtr )
                                              OR ( amend.fiscal_yr = excl_assmnt_yr_in
                                                   AND amend.qtr = excl_assmnt_qtr_in ) ) )
                                      OR ( amend.tobacco_class_id = 8
                                           AND ( amend.fiscal_yr IN (
                            v_cigar_yr,
                            excl_assmnt_yr_in
                        ) ) ) ) )
                              OR ( current_scope_id_in = exl_scope_full_id
                                   AND ( ( amend.tobacco_class_id <> 8
                                           AND ( ( amend.fiscal_yr = v_prev_yr
                                                   AND amend.qtr > v_prev_qtr )
                                                 OR amend.fiscal_yr >= v_prev_yr ) )
                                         OR ( amend.tobacco_class_id = 8
                                              AND amend.fiscal_yr >= v_prev_yr ) ) ) );

                    UPDATE tu_ttb_amendment amend
                    SET
                        prev_accept_flag = nvl(prev_accept_flag,decode(acceptance_flag,'E',null,acceptance_flag)),
                        acceptance_flag = NULL,
                        in_progress_flag = 'Y'
                    WHERE
                        ttb_company_id = company_id_in
                        AND ( ( tobacco_class_id <> 8
                                AND fiscal_yr = v_prev_yr
                                AND qtr = v_prev_qtr
                                AND EXISTS (
                            SELECT
                                'X'
                            FROM
                                permit_excl_status_vw
                            WHERE
                                company_id = amend.ttb_company_id
                                AND assmnt_yr = amend.fiscal_yr
                                AND assmnt_qtr = amend.qtr
                                AND excl_scope_id = exl_scope_partial_id
                        ) )
                              OR ( tobacco_class_id = 8
                                   AND fiscal_yr = excl_assmnt_yr_in
                                   AND v_is_cigar_conflicted = 'Y' ) );

                    UPDATE tu_ttb_amendment amend
                    SET
                        prev_accept_flag = nvl(prev_accept_flag,decode(acceptance_flag,'E',null,acceptance_flag)),
                        acceptance_flag = 'E',
                        in_progress_flag = 'N'
                    WHERE
                        ttb_company_id = company_id_in
                        AND fiscal_yr = excl_assmnt_yr_in
                        AND qtr = excl_assmnt_qtr_in
                        AND EXISTS (
                            SELECT
                                'X'
                            FROM
                                permit_excl_status_vw
                            WHERE
                                company_id = amend.ttb_company_id
                                AND assmnt_yr = v_next_yr
                                AND assmnt_qtr = v_next_qtr
                                AND excl_scope_id IN (
                                    exl_scope_full_id,
                                    exl_scope_partial_id
                                )
                        );

                END IF;

                UPDATE tu_comparison_all_delta_sts allsts
                SET
                    status = prev_status,
                    prev_status = NULL
                WHERE
                    (prev_status IS NOT NULL OR status = 'Excluded X')
                    AND EXISTS (
                        SELECT
                            'X'
                        FROM
                            tu_comparison_all_delta_sts allsts2
                            LEFT JOIN tu_company company ON allsts2.ein = company.ein
                        WHERE
                            company.company_id = company_id_in
                            AND allsts.cmp_all_delta_id = allsts2.cmp_all_delta_id
                            AND ( current_scope_id_in = exl_scope_partial_id
                                  AND ( ( DECODE(allsts2.fiscal_qtr,'1-4',5,allsts2.fiscal_qtr) <> 5
                                          AND ( ( allsts2.fiscal_yr = v_prev_yr
                                                  AND DECODE(allsts2.fiscal_qtr,'1-4',5,allsts2.fiscal_qtr) = v_prev_qtr )
                                                OR ( allsts2.fiscal_yr = excl_assmnt_yr_in
                                                     AND DECODE(allsts2.fiscal_qtr,'1-4',5,allsts2.fiscal_qtr) = excl_assmnt_qtr_in
                                                     ) ) )
                                        OR ( DECODE(allsts2.fiscal_qtr,'1-4',5,allsts2.fiscal_qtr) = 5
                                             AND ( allsts2.fiscal_yr IN (
                                v_cigar_yr,
                                excl_assmnt_yr_in
                            ) ) ) ) )
                            OR ( current_scope_id_in = exl_scope_full_id
                                 AND ( ( DECODE(allsts2.fiscal_qtr,'1-4',5,allsts2.fiscal_qtr) <> 5
                                         AND ( ( allsts2.fiscal_yr = v_prev_yr
                                                 AND DECODE(allsts2.fiscal_qtr,'1-4',5,allsts2.fiscal_qtr) > v_prev_qtr )
                                               OR allsts2.fiscal_yr > v_prev_yr ) )
                                       OR ( DECODE(allsts2.fiscal_qtr,'1-4',5,allsts2.fiscal_qtr) = 5
                                            AND allsts2.fiscal_yr >= v_prev_yr ) ) )
                    );

                UPDATE tu_comparison_all_delta_sts allsts
                SET
                    prev_status = nvl(prev_status,decode(status,'Excluded X', null, status)),
                    status = 'In Progress'
                WHERE
                    EXISTS (
                        SELECT
                            'X'
                        FROM
                            tu_comparison_all_delta_sts allsts2
                            LEFT JOIN tu_company company ON allsts2.ein = company.ein
                        WHERE
                            company.company_id = company_id_in
                            AND allsts.cmp_all_delta_id = allsts2.cmp_all_delta_id
                            AND allsts2.delta_type = 'INGESTED'
                            AND ( ( DECODE(allsts2.fiscal_qtr,'1-4',5,allsts2.fiscal_qtr) <> 5
                                    AND allsts2.fiscal_yr = v_prev_yr
                                    AND DECODE(allsts2.fiscal_qtr,'1-4',5,allsts2.fiscal_qtr) = v_prev_qtr
                                    AND EXISTS (
                                SELECT
                                    'X'
                                FROM
                                    permit_excl_status_vw
                                WHERE
                                    company_id = company.company_id
                                    AND assmnt_yr = allsts2.fiscal_yr
                                    AND assmnt_qtr = DECODE(allsts2.fiscal_qtr,'1-4',5,allsts2.fiscal_qtr)
                                    AND excl_scope_id IN (
                                        exl_scope_full_id,
                                        exl_scope_partial_id
                                    )
                            ) )
                                  OR ( DECODE(allsts2.fiscal_qtr,'1-4',5,allsts2.fiscal_qtr) <> 5
                                       AND allsts2.fiscal_yr = excl_assmnt_yr_in
                                       AND v_is_cigar_conflicted = 'Y' ) )
                    );

                UPDATE tu_comparison_all_delta_sts allsts
                SET
                    prev_status = nvl(prev_status,decode(status,'Excluded X', null, status)),
                    status = 'Excluded X'
                WHERE
                    EXISTS (
                        SELECT
                            'X'
                        FROM
                            tu_comparison_all_delta_sts allsts2
                            LEFT JOIN tu_company company ON allsts2.ein = company.ein
                        WHERE
                            company.company_id = company_id_in
                            AND allsts.cmp_all_delta_id = allsts2.cmp_all_delta_id
                            AND allsts2.fiscal_yr = excl_assmnt_yr_in
                            AND DECODE(allsts2.fiscal_qtr,'1-4',5,allsts2.fiscal_qtr) = excl_assmnt_qtr_in
                            AND EXISTS (
                                SELECT
                                    'X'
                                FROM
                                    permit_excl_status_vw
                                WHERE
                                    company_id = company.company_id
                                    AND assmnt_yr = v_next_yr
                                    AND assmnt_qtr = v_next_qtr
                                    AND excl_scope_id IN (
                                        exl_scope_full_id,
                                        exl_scope_partial_id
                                    )
                            )
                    );

            END IF;

        END;

    END;-- End Of: EXCLUSION_DELTA_STATUS_CAHNGE

END; -- End Of: permit_excl_pkg()

/

/* Revised 11/18/16 added tax rates to cigars and Valorem */

DELETE from tu_tobacco_class
WHERE parent_class_id in 
  (select parent_class_id
   from tu_tobacco_class
   where tobacco_class_nm in ('Cigarettes', 'Cigars'));

commit;

DELETE from tu_tobacco_class
WHERE parent_class_id is not null; 

commit;

DELETE from tu_tobacco_class;

commit;

INSERT into tu_tobacco_class
  (TOBACCO_CLASS_NM,CLASS_TYPE_CD)
VALUES
   ('Chew-and-Snuff','SPTP')
;

INSERT into tu_tobacco_class
  (TOBACCO_CLASS_NM,CLASS_TYPE_CD)
VALUES
   ('Pipe-RYO','SPTP')
;
commit;

DECLARE
     v_chewsnuff_id NUMBER := 0;
     v_piperyo_id NUMBER := 0;
BEGIN
  BEGIN
    select tobacco_class_id into v_chewsnuff_id
      from tu_tobacco_class
    where tobacco_class_nm = 'Chew-and-Snuff';
    select tobacco_class_id into v_piperyo_id
      from tu_tobacco_class
    where tobacco_class_nm = 'Pipe-RYO';
  END;
  BEGIN
    INSERT into tu_tobacco_class
      (PARENT_CLASS_ID, 
       TOBACCO_CLASS_NM,CLASS_TYPE_CD,
       TAX_RATE)    
    VALUES
      (v_chewsnuff_id,
       'Chew','TYPE','.5033');
    INSERT into tu_tobacco_class
      (PARENT_CLASS_ID, 
       TOBACCO_CLASS_NM,CLASS_TYPE_CD,
       TAX_RATE)    
    VALUES
      (v_chewsnuff_id,
       'Snuff','TYPE','1.51');
    INSERT into tu_tobacco_class
      (PARENT_CLASS_ID, 
       TOBACCO_CLASS_NM,CLASS_TYPE_CD,
       TAX_RATE)    
    VALUES
      (v_piperyo_id,
       'Pipe','TYPE','2.8311');
    INSERT into tu_tobacco_class
      (PARENT_CLASS_ID, 
       TOBACCO_CLASS_NM,CLASS_TYPE_CD,
       TAX_RATE)    
    VALUES
      (v_piperyo_id,
       'Roll-Your-Own','TYPE','24.78');
    INSERT into tu_tobacco_class
      (TOBACCO_CLASS_NM,CLASS_TYPE_CD,
       TAX_RATE)    
    VALUES
      ('Cigarettes','TYPE','50.33');
    INSERT into tu_tobacco_class
      (TOBACCO_CLASS_NM,CLASS_TYPE_CD)    
    VALUES
      ('Cigars','TYPE');
  END;
END;
/

DECLARE
     v_cigarette_id NUMBER := 0;
     v_cigar_id NUMBER := 0;
BEGIN
  BEGIN
    select tobacco_class_id into v_cigarette_id
      from tu_tobacco_class
    where tobacco_class_nm = 'Cigarettes';
    select tobacco_class_id into v_cigar_id
      from tu_tobacco_class
    where tobacco_class_nm = 'Cigars';
  END;
  BEGIN
    INSERT into tu_tobacco_class
      (PARENT_CLASS_ID, 
       TOBACCO_CLASS_NM,CLASS_TYPE_CD)    
    VALUES
      (v_cigarette_id,
       'Large','SUBT');
    INSERT into tu_tobacco_class
      (PARENT_CLASS_ID, 
       TOBACCO_CLASS_NM,CLASS_TYPE_CD)    
    VALUES
      (v_cigarette_id,
       'Small','SUBT');
    INSERT into tu_tobacco_class
      (PARENT_CLASS_ID, 
       TOBACCO_CLASS_NM,CLASS_TYPE_CD,TAX_RATE)    
    VALUES
      (v_cigar_id,
       'Large','SUBT','402.60');
    INSERT into tu_tobacco_class
      (PARENT_CLASS_ID, 
       TOBACCO_CLASS_NM,CLASS_TYPE_CD,TAX_RATE)    
    VALUES
      (v_cigar_id,
       'Small','SUBT','50.33');
    INSERT into tu_tobacco_class
      (PARENT_CLASS_ID, 
       TOBACCO_CLASS_NM,CLASS_TYPE_CD,TAX_RATE)    
    VALUES
      (v_cigar_id,
       'Ad Valorem','ADVL','.5275');
  END;
END;
/
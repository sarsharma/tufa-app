PROMPT
PROMPT===================================================================
PROMPT populate_tu_iso_cntry.sql - BEGIN
PROMPT===================================================================
PROMPT Created:  09/16/2016 by Tony Ireland
PROMPT===================================================================

INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AF', 'AFGHANISTAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AX', '�LAND ISLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AL', 'ALBANIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('DZ', 'ALGERIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AS', 'AMERICAN SAMOA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AD', 'ANDORRA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AO', 'ANGOLA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AI', 'ANGUILLA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AQ', 'ANTARCTICA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AG', 'ANTIGUA AND BARBUDA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AR', 'ARGENTINA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AM', 'ARMENIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AW', 'ARUBA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AU', 'AUSTRALIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AT', 'AUSTRIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AZ', 'AZERBAIJAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BS', 'BAHAMAS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BH', 'BAHRAIN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BD', 'BANGLADESH');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BB', 'BARBADOS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BY', 'BELARUS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BE', 'BELGIUM');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BZ', 'BELIZE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BJ', 'BENIN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BM', 'BERMUDA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BT', 'BHUTAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BO', 'BOLIVIA, PLURINATIONAL STATE OF');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BA', 'BOSNIA AND HERZEGOVINA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BW', 'BOTSWANA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BV', 'BOUVET ISLAND');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BR', 'BRAZIL');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('IO', 'BRITISH INDIAN OCEAN TERRITORY');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BN', 'BRUNEI DARUSSALAM');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BG', 'BULGARIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BF', 'BURKINA FASO');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BI', 'BURUNDI');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('KH', 'CAMBODIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CM', 'CAMEROON');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CA', 'CANADA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CV', 'CAPE VERDE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('KY', 'CAYMAN ISLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CF', 'CENTRAL AFRICAN REPUBLIC');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CL', 'CHILE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CX', 'CHRISTMAS ISLAND');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CO', 'COLOMBIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CG', 'CONGO');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CK', 'COOK ISLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CR', 'COSTA RICA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CI', 'C�TE D''IVOIRE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CU', 'CUBA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('DK', 'DENMARK');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('DJ', 'DJIBOUTI');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('EC', 'ECUADOR');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('EG', 'EGYPT');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GQ', 'EQUATORIAL GUINEA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('EE', 'ESTONIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('FK', 'FALKLAND ISLANDS (MALVINAS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('FJ', 'FIJI');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('FR', 'FRANCE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PF', 'FRENCH POLYNESIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GA', 'GABON');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GM', 'GAMBIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('DE', 'GERMANY');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GR', 'GREECE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GD', 'GRENADA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GU', 'GUAM');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GG', 'GUERNSEY');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GW', 'GUINEA-BISSAU');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('HT', 'HAITI');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('VA', 'HOLY SEE (VATICAN CITY STATE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('HN', 'HONDURAS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('HU', 'HUNGARY');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('IN', 'INDIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('IR', 'IRAN, ISLAMIC REPUBLIC OF');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('IM', 'ISLE OF MAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('IT', 'ITALY');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('JP', 'JAPAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('JO', 'JORDAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('KE', 'KENYA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('KR', 'KOREA, REPUBLIC OF');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('KW', 'KUWAIT');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('KG', 'KYRGYZSTAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('LV', 'LATVIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('LB', 'LEBANON');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('LR', 'LIBERIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('LI', 'LIECHTENSTEIN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('LT', 'LITHUANIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MO', 'MACAO');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MG', 'MADAGASCAR');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MW', 'MALAWI');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MV', 'MALDIVES');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MT', 'MALTA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MQ', 'MARTINIQUE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MU', 'MAURITIUS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MX', 'MEXICO');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MD', 'MOLDOVA, REPUBLIC OF');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MC', 'MONACO');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('ME', 'MONTENEGRO');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MA', 'MOROCCO');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MM', 'MYANMAR');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('NR', 'NAURU');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('NL', 'NETHERLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('NC', 'NEW CALEDONIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('NI', 'NICARAGUA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('NG', 'NIGERIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('NF', 'NORFOLK ISLAND');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('NO', 'NORWAY');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PK', 'PAKISTAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PS', 'PALESTINIAN TERRITORY, OCCUPIED');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PG', 'PAPUA NEW GUINEA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PH', 'PHILIPPINES');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PL', 'POLAND');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PR', 'PUERTO RICO');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('RE', 'R�UNION');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('RW', 'RWANDA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('BL', 'SAINT BARTH�LEMY');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('KN', 'SAINT KITTS AND NEVIS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('LC', 'SAINT LUCIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MF', 'SAINT MARTIN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('VC', 'SAINT VINCENT AND THE GRENADINES');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SM', 'SAN MARINO');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SA', 'SAUDI ARABIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SN', 'SENEGAL');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SC', 'SEYCHELLES');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SG', 'SINGAPORE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SI', 'SLOVENIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SO', 'SOMALIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('LK', 'SRI LANKA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SJ', 'SVALBARD AND JAN MAYEN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SE', 'SWEDEN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SY', 'SYRIAN ARAB REPUBLIC');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TJ', 'TAJIKISTAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TZ', 'TANZANIA, UNITED REPUBLIC OF');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TL', 'TIMOR-LESTE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TK', 'TOKELAU');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TN', 'TUNISIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TR', 'TURKEY');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TC', 'TURKS AND CAICOS ISLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('UG', 'UGANDA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AE', 'UNITED ARAB EMIRATES');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('US', 'UNITED STATES');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('UM', 'UNITED STATES MINOR OUTLYING ISLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('UY', 'URUGUAY');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('UZ', 'UZBEKISTAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('VU', 'VANUATU');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('VE', 'VENEZUELA, BOLIVARIAN REPUBLIC OF');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('VN', 'VIET NAM');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('VG', 'VIRGIN ISLANDS, BRITISH');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('VI', 'VIRGIN ISLANDS, U.S.');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('WF', 'WALLIS AND FUTUNA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('EH', 'WESTERN SAHARA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('YE', 'YEMEN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('ZM', 'ZAMBIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('ZW', 'ZIMBABWE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TD', 'CHAD');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CN', 'CHINA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CC', 'COCOS (KEELING) ISLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('KM', 'COMOROS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('HR', 'CROATIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CY', 'CYPRUS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CZ', 'CZECH REPUBLIC');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('DM', 'DOMINICA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('DO', 'DOMINICAN REPUBLIC');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SV', 'EL SALVADOR');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('ER', 'ERITREA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('ET', 'ETHIOPIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('FO', 'FAROE ISLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('FI', 'FINLAND');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GF', 'FRENCH GUIANA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TF', 'FRENCH SOUTHERN TERRITORIES');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GE', 'GEORGIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GH', 'GHANA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GI', 'GIBRALTAR');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GL', 'GREENLAND');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GP', 'GUADELOUPE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GT', 'GUATEMALA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GN', 'GUINEA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GY', 'GUYANA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('HM', 'HEARD ISLAND AND MCDONALD ISLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('HK', 'HONG KONG');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('IS', 'ICELAND');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('ID', 'INDONESIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('IQ', 'IRAQ');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('IE', 'IRELAND');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('IL', 'ISRAEL');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('JM', 'JAMAICA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('JE', 'JERSEY');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('KZ', 'KAZAKHSTAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('KI', 'KIRIBATI');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('KP', 'KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('LA', 'LAO PEOPLE''S DEMOCRATIC REPUBLIC');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('LS', 'LESOTHO');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('LY', 'LIBYAN ARAB JAMAHIRIYA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('LU', 'LUXEMBOURG');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MY', 'MALAYSIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('ML', 'MALI');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MH', 'MARSHALL ISLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MR', 'MAURITANIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('YT', 'MAYOTTE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('FM', 'MICRONESIA, FEDERATED STATES OF');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MN', 'MONGOLIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MS', 'MONTSERRAT');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MZ', 'MOZAMBIQUE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('NA', 'NAMIBIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('NP', 'NEPAL');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('AN', 'NETHERLANDS ANTILLES');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('NZ', 'NEW ZEALAND');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('NE', 'NIGER');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('NU', 'NIUE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('MP', 'NORTHERN MARIANA ISLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('OM', 'OMAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PW', 'PALAU');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PA', 'PANAMA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PY', 'PARAGUAY');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PE', 'PERU');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PN', 'PITCAIRN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PT', 'PORTUGAL');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('QA', 'QATAR');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('RO', 'ROMANIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('RU', 'RUSSIAN FEDERATION');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SH', 'SAINT HELENA, ASCENSION AND TRISTAN DA CUNHA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('PM', 'SAINT PIERRE AND MIQUELON');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('WS', 'SAMOA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('ST', 'SAO TOME AND PRINCIPE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('RS', 'SERBIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SL', 'SIERRA LEONE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SK', 'SLOVAKIA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SB', 'SOLOMON ISLANDS');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('ZA', 'SOUTH AFRICA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('ES', 'SPAIN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SD', 'SUDAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SR', 'SURINAME');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('SZ', 'SWAZILAND');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('CH', 'SWITZERLAND');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TW', 'TAIWAN, PROVINCE OF CHINA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TH', 'THAILAND');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TG', 'TOGO');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TO', 'TONGA');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TT', 'TRINIDAD AND TOBAGO');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TM', 'TURKMENISTAN');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('TV', 'TUVALU');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('UA', 'UKRAINE');
INSERT INTO tu_iso_cntry
   (iso_cntry_cd, iso_country_nm)
 VALUES
   ('GB', 'UNITED KINGDOM');
COMMIT;

PROMPT
PROMPT===================================================================
PROMPT populate_tu_iso_cntry.sql - END
PROMPT===================================================================
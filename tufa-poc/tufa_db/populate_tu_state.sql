PROMPT
PROMPT===================================================================
PROMPT populate_tu_state.sql - BEGIN
PROMPT===================================================================
PROMPT Created:  09/16/2016 by Tony Ireland
PROMPT===================================================================

INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('AL', 'Alabama', 'US', '01');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('AK', 'Alaska', 'US', '02');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('AZ', 'Arizona', 'US', '04');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('AR', 'Arkansas', 'US', '05');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('CA', 'California', 'US', '06');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('CO', 'Colorado', 'US', '08');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('CT', 'Connecticut', 'US', '09');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('DE', 'Delaware', 'US', '10');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('DC', 'District of Columbia', 'US', '11');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('FL', 'Florida', 'US', '12');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('GA', 'Georgia', 'US', '13');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('HI', 'Hawaii', 'US', '15');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('ID', 'Idaho', 'US', '16');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('IL', 'Illinois', 'US', '17');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('IN', 'Indiana', 'US', '18');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('IA', 'Iowa', 'US', '19');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('KS', 'Kansas', 'US', '20');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('KY', 'Kentucky', 'US', '21');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('LA', 'Louisiana', 'US', '22');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('ME', 'Maine', 'US', '23');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('MD', 'Maryland', 'US', '24');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('MA', 'Massachusetts', 'US', '25');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('MI', 'Michigan', 'US', '26');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('MN', 'Minnesota', 'US', '27');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('MS', 'Mississippi', 'US', '28');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('MO', 'Missouri', 'US', '29');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('MT', 'Montana', 'US', '30');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('NE', 'Nebraska', 'US', '31');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('NV', 'Nevada', 'US', '32');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('NH', 'New Hampshire', 'US', '33');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('NJ', 'New Jersey', 'US', '34');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('NM', 'New Mexico', 'US', '35');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('NY', 'New York', 'US', '36');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('NC', 'North Carolina', 'US', '37');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('ND', 'North Dakota', 'US', '38');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('OH', 'Ohio', 'US', '39');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('OK', 'Oklahoma', 'US', '40');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('OR', 'Oregon', 'US', '41');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('PA', 'Pennsylvania', 'US', '42');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('RI', 'Rhode Island', 'US', '44');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('SC', 'South Carolina', 'US', '45');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('SD', 'South Dakota', 'US', '46');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('TN', 'Tennessee', 'US', '47');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('TX', 'Texas', 'US', '48');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('UT', 'Utah', 'US', '49');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('VT', 'Vermont', 'US', '50');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('VA', 'Virginia', 'US', '51');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('WA', 'Washington', 'US', '53');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('WV', 'West Virginia', 'US', '54');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('WI', 'Wisconsin', 'US', '55');
INSERT INTO tu_state
   (state_cd, state_nm, iso_cntry_cd, fips_state_cd)
 VALUES
   ('WY', 'Wyoming', 'US', '56');
COMMIT;

PROMPT
PROMPT===================================================================
PROMPT populate_tu_state.sql - END
PROMPT===================================================================
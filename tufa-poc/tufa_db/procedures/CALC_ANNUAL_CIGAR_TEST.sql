--------------------------------------------------------
--  DDL for Procedure CALC_ANNUAL_CIGAR_TEST
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."CALC_ANNUAL_CIGAR_TEST" (assess_yr NUMBER,assess_qtr NUMBER, author VARCHAR2) AS

BEGIN

DECLARE

--  type tot_taxes IS VARRAY(5) OF NUMBER(14,2);

--  v_tot_pipe_taxes tot_taxes := tot_taxes(0.0,0.0,0.0,0.0,0.0);

  v_address_changed CHAR(1) := 'N';
  v_contact_changed CHAR(1) := 'N';
  v_period_tot_taxes NUMBER(14,2) := 0;
  v_previous_share NUMBER(8,6) := 0;
  v_previous_assessment NUMBER := null;
  v_original_corrected CHAR(1) := 'O';
  v_previous_version NUMBER(2) := 0;
  v_max_version NUMBER(2) := 0;
  v_current_share NUMBER(8,6) := 0;
  v_delta_share NUMBER(8,6) := 0;
  v_assessment_id NUMBER := 0;
  v_year NUMBER(4) := assess_yr;
  v_qtr NUMBER(1) := assess_qtr;
  v_author VARCHAR2(50) :=author;
  v_marketshareType VARCHAR2(6) := 'FULLMS';
  v_submitind CHAR(1);
  v_version_num NUMBER(2) := '1';
  v_rank NUMBER(4) := 0;
  V_pre_ver number;
  v_exist  varchar2(5);
  v_data_exist varchar2(5);

 CURSOR CUR_PV (p_assignment_id number, p_version_num number)
  IS
  SELECT * from tu_market_share
  where ASSESSMENT_ID=p_assignment_id
  and version_num=p_version_num
  and tobacco_class_id=8;

  CURSOR calc_marketshare IS

  SELECT impt_manu.company_id, 
       impt_manu.ein, 
       impt_manu.legal_nm, 
       TC5.tobacco_class_id, 
       SUM(impt_manu.TOTTAX) AS TOTTAX, 
       SUM(impt_manu.TOTVOL) AS TOTVOL 
  FROM (SELECT * 
          FROM (SELECT company_id, 
                       ein, 
                       legal_nm, 
                       'IMPT' AS permit_type, 
                       CASE acceptance_flag WHEN 'I' THEN ingesttax 
                                            WHEN 'N' THEN amendtax 
                                            ELSE tufatax END AS tottax, 
                       CASE acceptance_flag WHEN 'I' THEN ingestvol 
                                            WHEN 'N' THEN amendvol 
                                            ELSE TUFAVOL END AS totvol 
                  FROM (SELECT TUFA1.company_id, 
                               TUFA1.ein, 
                               TUFA1.legal_nm, 
                               TUFATAX, 
                               TUFAVOL, 
                               INGESTTAX, 
                               INGESTVOL, 
                               AMENDTAX, 
                               AMENDVOL, 
                               acceptance_flag 
                          FROM (SELECT C1.company_id, 
                                       C1.ein, 
                                       C1.legal_nm, 
                                       SUM(qty) AS TUFAVOL, 
                                       SUM(tax) AS TUFATAX 
                                  FROM tu_company C1, 
                                       tu_permit P1, 
                                       (SELECT PP1.permit_id, 
                                               PP1.period_id, 
                                               NVL(DETAILS1.removal_qty, 0) AS qty, 
                                               NVL(DETAILS1.taxes_paid, 0) AS tax 
                                          FROM tu_permit P2 
                                               INNER JOIN tu_permit_period PP1 
                                                  ON P2.permit_id = PP1.permit_id 
                                               INNER JOIN tu_rpt_period RP1 
                                                  ON PP1.period_id = RP1.period_id 
                                               LEFT OUTER JOIN (SELECT PP2.permit_id, 
                                                                       PP2.period_id, 
                                                                       FD1.removal_qty, 
                                                                       FD1.taxes_paid 
                                                                  FROM tu_permit P3, 
                                                                       tu_permit_period PP2, 
                                                                       tu_rpt_period RP2, 
                                                                       tu_submitted_form SF1, 
                                                                       tu_form_detail FD1, 
                                                                       tu_tobacco_class TC1 
                                                                 WHERE TC1.tobacco_class_nm = 'Cigars' 
                                                                   AND SF1.form_type_cd = '3852' 
                                                                   AND TC1.tobacco_class_id = FD1.tobacco_class_id 
                                                                   AND SF1.form_id = FD1.form_id 
                                                                   AND SF1.permit_id = PP2.permit_id 
                                                                   AND SF1.period_id = PP2.period_id 
                                                                   AND PP2.period_id = RP2.period_id 
                                                                   AND P3.permit_id = PP2.permit_id) DETAILS1 
                                                 ON DETAILS1.permit_id = PP1.permit_id 
                                                    AND DETAILS1.period_id = PP1.period_id 
                                         WHERE P2.permit_type_cd = 'IMPT' 
                                           AND RP1.fiscal_yr = v_year) PERMIT_TAX1 
                                 WHERE (C1.company_id, P1.permit_num) NOT IN 
          -------------------------Added below Code as part of Story CTPTUFA-4834-------------
                                  (    Select distinct vw1.company_id,vw1.permit_num
                                  from PERMIT_EXCL_STATUS_VW vw1
                                        where
                                        (vw1.ASSMNT_YR= v_year+1
                                        and   vw1.ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (vw1.EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (vw1.EXCL_SCOPE_ID=1
                                          and
                                          (
                                           vw1.ASSMNT_YR <v_year+1
                                            or
                                           (vw1.ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and vw1.ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )

                                   AND PERMIT_TAX1.permit_id = P1.permit_id 
                                   AND C1.company_id = P1.company_id 
                                 GROUP BY C1.company_id, C1.ein, C1.legal_nm) TUFA1 
                               LEFT OUTER JOIN (SELECT /*+ INDEX(C11 EIN_UK) */ C11.ein, 
                                                       C11.company_id, 
                                                       SUM(NVL(cbpingested.ingest_vol, 0)) AS ingestvol, 
                                                       SUM(NVL(cbpingested.cbp_taxes_paid, 0)) AS ingesttax, 
                                                       cbpingested.tobacco_class_id 
                                                  FROM (SELECT cbpdetail.importer_ein, 
                                                               cbpdetail.consignee_ein, 
                                                               SUM(NVL(cbpentry.estimated_tax, 0)) AS cbp_taxes_paid, 
                                                               SUM(cbpentry.qty_removed * 1000) AS ingest_vol, 
                                                               TTC1.tobacco_class_nm, 
                                                               cbpentry.fiscal_year, 
                                                               cbpdetail.association_type_cd, 
                                                               TTC1.tobacco_class_id AS tobacco_class_id 
                                                          FROM tu_cbp_importer cbpdetail, 
                                                               tu_cbp_entry cbpentry, 
                                                               tu_tobacco_class TTC1 
                                                         WHERE (cbpentry.excluded_flag IS NULL 
                                                                 OR cbpentry.excluded_flag <> 'Y') 
                                                           AND cbpentry.tobacco_class_id = TTC1.tobacco_class_id 
                                    AND (TTC1.tobacco_class_id = 8 
                                     OR TTC1.tobacco_class_nm = 'Cigars') 
                                    AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id 
                                    AND cbpdetail.association_type_cd IS NOT NULL 
                                    AND cbpdetail.fiscal_yr = v_year GROUP BY cbpdetail.importer_ein, cbpdetail.consignee_ein, TTC1.tobacco_class_nm, cbpentry.fiscal_year, cbpdetail.association_type_cd, TTC1.tobacco_class_id, cbpentry.uom_cd) cbpingested, tu_company C11 WHERE (cbpingested.importer_ein = C11.ein 
                                    AND cbpingested.association_type_cd = 'IMPT' 
                                     OR cbpingested.consignee_ein = C11.ein 
                                    AND cbpingested.association_type_cd = 'CONS') 
                                    AND cbpingested.cbp_taxes_paid != 0 
                                    AND C11.company_id NOT IN 
       ------------------------------------------------------------
                                (    Select distinct company_id
                                     from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )
                    ------------------------------------------------------------
                                                 GROUP BY C11.company_id, C11.ein, cbpingested.tobacco_class_id) INGEST1 
                                 ON TUFA1.company_id = INGEST1.company_id 
                               LEFT OUTER JOIN (SELECT CBP_COMPANY_ID AS company_id, 
                                                       AMENDED_TOTAL_TAX AS AMENDTAX, 
                                                       AMENDED_TOTAL_VOLUME * 1000 AS AMENDVOL, 
                                                       CA1.acceptance_flag 
                                                  FROM tu_cbp_amendment CA1, 
                                                       tu_tobacco_class TC2 
                                                 WHERE CA1.tobacco_class_id = TC2.tobacco_class_id 
                                                   AND CA1.fiscal_yr = v_year 
                                                   AND TC2.tobacco_class_nm = 'Cigars' 
                                                   AND CBP_COMPANY_ID NOT IN 
                                                                  ------------------------------------------------------------
                                (    Select distinct company_id
                                     from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   ) ) AMEND1 
                                 ON TUFA1.company_id = AMEND1.company_id) V2 
                 WHERE ein NOT IN (SELECT TCADS1.EIN 
                                     FROM tu_comparison_all_delta_sts TCADS1 
                                    WHERE TCADS1.STATUS = 'Excluded' 
                                      AND Delta_type = 'TUFA' 
                                      AND TCADS1.FISCAL_YR = v_year 
                                      AND TCADS1.PERMIT_TYPE = 'IMPT' 
                                      AND TCADS1.TOBACCO_CLASS_NM = 'Cigars')) V1 
        UNION ALL 
        SELECT INGESTED_ONLY.Company_id, 
               INGESTED_ONLY.ein, 
               INGESTED_ONLY.legal_nm, 
               INGESTED_ONLY.permit_type, 
               INGESTED_ONLY.delta, 
               DECODE(DECODE(INGESTED_ONLY.permit_type, 'IMPT', 
               (SELECT DISTINCT uom_cd 
                  FROM TU_CBP_IMPORTER TCI, 
                       TU_CBP_ENTRY CE 
                 WHERE tci.IMPORTER_EIN = INGESTED_ONLY.ein 
                   AND TCI.CBP_IMPORTER_ID = CE.CBP_IMPORTER_ID 
                   AND TCI.FISCAL_YR = CE.FISCAL_YEAR 
                   AND CE.FISCAL_YEAR = INGESTED_ONLY.fiscal_yr 
                   AND ce.tobacco_class_id = INGESTED_ONLY.tobacco_class_id), 0), 'K', DECODE(INGESTED_ONLY.permit_type, 'IMPT', 
               (SELECT SUM(CE.QTY_REMOVED) 
                  FROM TU_CBP_IMPORTER TCI, 
                       TU_CBP_ENTRY CE 
                 WHERE tci.IMPORTER_EIN = INGESTED_ONLY.ein 
                   AND TCI.CBP_IMPORTER_ID = CE.CBP_IMPORTER_ID 
                   AND TCI.FISCAL_YR = CE.FISCAL_YEAR 
                   AND CE.FISCAL_YEAR = INGESTED_ONLY.fiscal_yr 
                   AND ce.tobacco_class_id = INGESTED_ONLY.tobacco_class_id), 0) * 1000, 
               'KG', 
               DECODE(INGESTED_ONLY.permit_type, 'IMPT', 
               (SELECT SUM(CE.QTY_REMOVED) 
                  FROM TU_CBP_IMPORTER TCI, 
                       TU_CBP_ENTRY CE 
                 WHERE tci.IMPORTER_EIN = INGESTED_ONLY.ein 
                   AND TCI.CBP_IMPORTER_ID = CE.CBP_IMPORTER_ID 
                   AND TCI.FISCAL_YR = CE.FISCAL_YEAR 
                   AND CE.FISCAL_YEAR = INGESTED_ONLY.fiscal_yr 
                   AND ce.tobacco_class_id = INGESTED_ONLY.tobacco_class_id), 
               0) * 2.204, 
               0) TOTVOL 
          FROM (SELECT (SELECT c.company_id 
                          FROM tu_company c 
                         WHERE c.ein = TCADS2.ein) AS company_id, 
                       TCADS2.EIN, 
                       (SELECT c.legal_nm 
                          FROM tu_company c 
                         WHERE c.ein = TCADS2.ein) AS legal_nm, 
                       (SELECT ttc1.tobacco_class_id 
                          FROM tu_tobacco_class ttc1 
                         WHERE ttc1.TOBACCO_CLASS_NM = TCADS2.TOBACCO_CLASS_NM) tobacco_class_id, 
                       TCADS2.PERMIT_TYPE, 
                       ABS(TCADS2.Delta) delta, 
                       NULL AS TOTVOL, 
                       TCADS2.FISCAL_YR 
                  FROM tu_comparison_all_delta_sts TCADS2 
                 WHERE TCADS2.STATUS = 'MSReady' 
                   AND Delta_type = 'INGESTED' 
                   AND TCADS2.FISCAL_YR = v_year 
                   AND TCADS2.TOBACCO_CLASS_NM = 'Cigars' 
                   AND TCADS2.ein NOT IN 
                                                     (    Select distinct ein
                                  from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )
------------------------------------------------------------------------------------

                                                  ) INGESTED_ONLY 
        UNION 
        SELECT * 
          FROM (SELECT company_id, 
                       ein, 
                       legal_nm, 
                       'MANU' AS permit_type, 
                       CASE acceptance_flag WHEN 'I' THEN ingesttax 
                                            WHEN 'N' THEN amendtax 
                                            ELSE tufatax END AS tottax, 
                       CASE acceptance_flag WHEN 'I' THEN ingestvol 
                                            WHEN 'N' THEN amendvol 
                                            ELSE tufavol END AS totvol 
                  FROM (SELECT TUFA2.company_id, 
                               TUFA2.ein, 
                               TUFA2.legal_nm, 
                               TUFATAX, 
                               TUFAVOL, 
                               INGESTTAX, 
                               INGESTVOL, 
                               AMENDTAX, 
                               AMENDVOL, 
                               acceptance_flag 
                          FROM (SELECT C2.company_id, 
                                       C2.ein, 
                                       C2.legal_nm, 
                                       SUM(qty) AS TUFAVOL, 
                                       SUM(tax) AS TUFATAX 
                                  FROM tu_company C2, 
                                       tu_permit P4, 
                                       (SELECT PP3.permit_id, 
                                               PP3.period_id, 
                                               NVL(DETAILS2.removal_qty, 0) AS qty, 
                                               NVL(DETAILS2.taxes_paid, 0) AS tax 
                                          FROM tu_permit P5 
                                               INNER JOIN tu_permit_period PP3 
                                                  ON P5.permit_id = PP3.permit_id 
                                               INNER JOIN tu_rpt_period RP3 
                                                  ON PP3.period_id = RP3.period_id 
                                               LEFT OUTER JOIN (SELECT PP4.permit_id, 
                                                                       PP4.period_id, 
                                                                       FD2.removal_qty, 
                                                                       FD2.taxes_paid 
                                                                  FROM tu_permit P6, 
                                                                       tu_permit_period PP4, 
                                                                       tu_rpt_period RP4, 
                                                                       tu_submitted_form SF2, 
                                                                       tu_form_detail FD2, 
                                                                       tu_tobacco_class TC3 
                                                                 WHERE TC3.tobacco_class_nm = 'Cigars' 
                                                                   AND SF2.form_type_cd = '3852' 
                                                                   AND TC3.tobacco_class_id = FD2.tobacco_class_id 
                                                                   AND SF2.form_id = FD2.form_id 
                                                                   AND SF2.permit_id = PP4.permit_id 
                                                                   AND SF2.period_id = PP4.period_id 
                                                                   AND PP4.period_id = RP4.period_id 
                                                                   AND P6.permit_id = PP4.permit_id) DETAILS2 
                                                 ON DETAILS2.permit_id = PP3.permit_id 
                                                    AND DETAILS2.period_id = PP3.period_id 
                                         WHERE P5.permit_type_cd = 'MANU' 
                                           AND RP3.fiscal_yr = v_year) PERMIT_TAX2 
                                 WHERE (C2.company_id, P4.permit_num) NOT IN 
          (    Select distinct company_id,permit_num
                                  from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )                         AND PERMIT_TAX2.permit_id = P4.permit_id 
                                   AND C2.company_id = P4.company_id 
                                 GROUP BY C2.company_id, C2.ein, C2.legal_nm) TUFA2 
                               LEFT OUTER JOIN (SELECT C4.company_id, 
                                                       C4.ein, 
                                                       ingestvol, 
                                                       ingesttax 
                                                  FROM (SELECT ein_num, 
                                                               tobacco_class_id, 
                                                               ingestvol, 
                                                               ingesttax 
                                                          FROM (SELECT ttbcmpy.ein_num, 
                                                                       TTC2.tobacco_class_id, 
                                                                       0 AS ingestvol, 
                                                                       SUM(NVL(ttb_taxes_paid, 0)) AS ingesttax 
                                                                  FROM tu_ttb_company ttbcmpy, 
                                                                       tu_ttb_permit ttbpermit, 
                                                                       tu_ttb_annual_tax ttbannualtx, 
                                                                       tu_tobacco_class TTC2 
                                                                 WHERE TTC2.tobacco_class_id = 8 
                                                                   AND ttbcmpy.fiscal_yr = v_year 
                                                                   AND ttbpermit.permit_num IN (SELECT P7.permit_num 
                                                                                                  FROM tu_permit P7, 
                                                                                                       tu_company C3 
                                                                                                 WHERE C3.ein = ttbcmpy.ein_num 
                                                                                                   AND P7.company_id = C3.company_id) 
                                                                   AND ttbannualtx.tobacco_class_id = TTC2.tobacco_class_id 
                                    AND ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id 
                                    AND ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id GROUP BY ttbcmpy.ein_num, TTC2.tobacco_class_id) V5)
                                    ttbingested, tu_company C4 WHERE C4.company_id NOT IN 
                                    (    Select distinct company_id
                                     from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )
                                                   AND ttbingested.ein_num = C4.ein) INGEST2 
                                 ON TUFA2.company_id = INGEST2.company_id 
                               LEFT OUTER JOIN (SELECT TTB_COMPANY_ID AS company_id, 
                                                       AMENDED_TOTAL_TAX AS AMENDTAX, 
                                                       AMENDED_TOTAL_VOLUME * 1000 AS AMENDVOL, 
                                                       CA2.acceptance_flag 
                                                  FROM tu_ttb_amendment CA2, 
                                                       tu_tobacco_class TC4 
                                                 WHERE CA2.tobacco_class_id = TC4.tobacco_class_id 
                                                   AND CA2.fiscal_yr = v_year 
                                                   AND TC4.tobacco_class_nm = 'Cigars' 
                                                   AND TTB_COMPANY_ID NOT IN 
                                                      (    Select distinct company_id
                                     from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )
                                                                                      ) AMEND2 
                                 ON TUFA2.company_id = AMEND2.company_id) V4 
                 WHERE ein NOT IN (SELECT TCADS3.EIN 
                                     FROM tu_comparison_all_delta_sts TCADS3 
                                    WHERE TCADS3.STATUS = 'Excluded' 
                                      AND Delta_type = 'TUFA' 
                                      AND TCADS3.FISCAL_YR = v_year 
                                      AND TCADS3.PERMIT_TYPE = 'MANU' 
                                      AND TCADS3.TOBACCO_CLASS_NM = 'Cigars')) V3) impt_manu, 
       tu_tobacco_class TC5 
 WHERE TC5.tobacco_class_nm = 'Cigars' 
 GROUP BY impt_manu.company_id, impt_manu.ein, impt_manu.legal_nm, TC5.tobacco_class_id 
 ORDER BY TOTTAX DESC;


BEGIN
-- determine submit status from true-up
  SELECT submitted_ind
    INTO v_submitind
  FROM tu_annual_trueup
  WHERE fiscal_yr = v_year;

-- get assessment information
  BEGIN

    SELECT assessment_id, assessment_yr
    INTO v_assessment_id, v_year
    FROM tu_assessment
    WHERE assessment_yr = v_year
    AND assessment_qtr = v_qtr
    AND assessment_type = 'ANNU';

  EXCEPTION
  WHEN no_data_found then v_assessment_id := '0';
  END;

-- get existing version information

  BEGIN

    SELECT max(version_num)
    INTO v_max_version
    FROM tu_assessment_version
    WHERE assessment_id = v_assessment_id;

  EXCEPTION

    WHEN no_data_found then
        v_max_version := -1;
  END;

----------Changes as per Story#5154 for including cigar assess,emt against same assessment ID's----------
if (v_submitind='Y')
then
---- we are decreasing it by 1 so that we can report it against the Same Version
-----number which is already used during non cigar assessment-----
v_max_version:=v_max_version-1;
end if;
----------Changes as per Story#5154----------

  if v_max_version IS NULL then
    v_max_version := -1;
  end if;

   -- DBMS_OUTPUT.PUT_LINE('MAX VERSION:' || v_max_version);

-- calculate current version and previous version

  BEGIN

    -- highest version will always be the previous

    v_previous_version := v_max_version;

    IF v_submitind = 'N' then
      -- we are generating version 0 (temporary version)
      v_version_num := 0;
    ELSIF v_submitind = 'Y' then
      -- we are generating the next version
      v_version_num := v_previous_version + 1;
    END IF;

    if v_previous_version < 1 then
      BEGIN
        SELECT assessment_id
          INTO v_previous_assessment
        FROM tu_assessment
        WHERE assessment_yr = v_year
          and assessment_qtr = v_qtr
          and assessment_type = 'CIQT';
        exception when no_data_found then v_previous_assessment := null;
      END;

      if v_previous_assessment IS NOT NULL then
        BEGIN
            SELECT max(version_num)
            INTO v_previous_version
            from tu_market_share
            where assessment_id = v_previous_assessment;
            EXCEPTION
            when no_data_found then
              v_previous_version := 0;
        END;
      else
        -- couldn't find the quarterly assessment
        -- set previous assessment back to current
        v_previous_assessment := v_assessment_id;
      end if;
    else
      -- we have already generated a true-up market share
      -- compare this version to previously generated trueup
      v_previous_assessment := v_assessment_id;
    END IF;

    -- upsert assessment version
    MERGE INTO TU_ASSESSMENT_VERSION av using dual on (assessment_id = v_assessment_id and version_num=v_version_num)
    WHEN MATCHED THEN
      UPDATE SET created_by=v_author, created_dt=SYSDATE
    WHEN NOT MATCHED THEN
      INSERT (assessment_id, version_num, created_by, created_dt)
      VALUES (v_assessment_id, v_version_num, v_author, SYSDATE);

  END;
 --DBMS_OUTPUT.PUT_LINE('CURRENT ASSESSMENT: ' || v_assessment_id);
   -- DBMS_OUTPUT.PUT_LINE('CURRENT VERSION:' || v_version_num);
    --DBMS_OUTPUT.PUT_LINE('PREVIOUS ASSESSMENT: ' || v_previous_assessment);
    --DBMS_OUTPUT.PUT_LINE('PREVIOUS VERSION: ' || v_previous_version);

-- determine if original or corrected.  always corrected if
-- cigar assessment has been fully submitted.
--Commenting this Piece as we will be calculating this field
---based upon Previous Share value while inserting data
 /* BEGIN
    SELECT 'C'
      INTO v_original_corrected
    FROM tu_assessment a
    WHERE assessment_yr = v_year
      and assessment_qtr = 4
      and assessment_type = 'CIQT'
    and assessment_id in (Select assessment_id from tu_assessment_version tav
                            where tav.assessment_id=a.assessment_id and version_num>0);
    EXCEPTION
      WHEN OTHERS THEN
        BEGIN
          if v_previous_assessment = v_assessment_id and v_previous_version > 0 then
            -- fourth quarter cigar assessment detected.  mark as corrected.
            v_original_corrected := 'C';
          else
            -- no fourth quarter cigar assessment detected.
            -- test for other quarter submissions (Q1-Q3).
            BEGIN
              SELECT DISTINCT 'B'
              INTO v_original_corrected
              FROM tu_assessment a
              WHERE assessment_yr = v_year
                AND assessment_type='CIQT'
                AND SUBMITTED_IND='Y';
             EXCEPTION
                WHEN OTHERS THEN
                  v_original_corrected := 'O';
            END;
          end if;
        END;
  END;
*/
-- including marketshare, addresses, and contacts

  BEGIN

    DELETE from tu_market_share

    WHERE assessment_id = v_assessment_id

      and version_num = '0'

      and tobacco_class_id = 8;

    EXCEPTION when no_data_found then NULL;

  END;

--  BEGIN
--
--    DELETE from tu_assessment_address
--
--    WHERE assessment_id = v_assessment_id
--
--      and version_num = '0';
--
--    EXCEPTION when no_data_found then NULL;
--
--  END;
--
--  BEGIN
--
--    DELETE from tu_assessment_contact
--
--    WHERE assessment_id = v_assessment_id
--
--      and version_num = '0';
--
--    EXCEPTION when no_data_found then NULL;
--
--  END;

-- GET THE TOTAL TAXES COLLECTED

    BEGIN

      SELECT SUM(TOTTAX)
        into v_period_tot_taxes
      FROM
      (SELECT impt_manu.company_id, 
       impt_manu.ein, 
       impt_manu.legal_nm, 
       TC5.tobacco_class_id, 
       SUM(impt_manu.TOTTAX) AS TOTTAX, 
       SUM(impt_manu.TOTVOL) AS TOTVOL 
  FROM (SELECT * 
          FROM (SELECT company_id, 
                       ein, 
                       legal_nm, 
                       'IMPT' AS permit_type, 
                       CASE acceptance_flag WHEN 'I' THEN ingesttax 
                                            WHEN 'N' THEN amendtax 
                                            ELSE tufatax END AS tottax, 
                       CASE acceptance_flag WHEN 'I' THEN ingestvol 
                                            WHEN 'N' THEN amendvol 
                                            ELSE TUFAVOL END AS totvol 
                  FROM (SELECT TUFA1.company_id, 
                               TUFA1.ein, 
                               TUFA1.legal_nm, 
                               TUFATAX, 
                               TUFAVOL, 
                               INGESTTAX, 
                               INGESTVOL, 
                               AMENDTAX, 
                               AMENDVOL, 
                               acceptance_flag 
                          FROM (SELECT C1.company_id, 
                                       C1.ein, 
                                       C1.legal_nm, 
                                       SUM(qty) AS TUFAVOL, 
                                       SUM(tax) AS TUFATAX 
                                  FROM tu_company C1, 
                                       tu_permit P1, 
                                       (SELECT PP1.permit_id, 
                                               PP1.period_id, 
                                               NVL(DETAILS1.removal_qty, 0) AS qty, 
                                               NVL(DETAILS1.taxes_paid, 0) AS tax 
                                          FROM tu_permit P2 
                                               INNER JOIN tu_permit_period PP1 
                                                  ON P2.permit_id = PP1.permit_id 
                                               INNER JOIN tu_rpt_period RP1 
                                                  ON PP1.period_id = RP1.period_id 
                                               LEFT OUTER JOIN (SELECT PP2.permit_id, 
                                                                       PP2.period_id, 
                                                                       FD1.removal_qty, 
                                                                       FD1.taxes_paid 
                                                                  FROM tu_permit P3, 
                                                                       tu_permit_period PP2, 
                                                                       tu_rpt_period RP2, 
                                                                       tu_submitted_form SF1, 
                                                                       tu_form_detail FD1, 
                                                                       tu_tobacco_class TC1 
                                                                 WHERE TC1.tobacco_class_nm = 'Cigars' 
                                                                   AND SF1.form_type_cd = '3852' 
                                                                   AND TC1.tobacco_class_id = FD1.tobacco_class_id 
                                                                   AND SF1.form_id = FD1.form_id 
                                                                   AND SF1.permit_id = PP2.permit_id 
                                                                   AND SF1.period_id = PP2.period_id 
                                                                   AND PP2.period_id = RP2.period_id 
                                                                   AND P3.permit_id = PP2.permit_id) DETAILS1 
                                                 ON DETAILS1.permit_id = PP1.permit_id 
                                                    AND DETAILS1.period_id = PP1.period_id 
                                         WHERE P2.permit_type_cd = 'IMPT' 
                                           AND RP1.fiscal_yr = v_year) PERMIT_TAX1 
                                 WHERE (C1.company_id, P1.permit_num) NOT IN 
          -------------------------Added below Code as part of Story CTPTUFA-4834-------------
                                  (    Select distinct vw1.company_id,vw1.permit_num
                                  from PERMIT_EXCL_STATUS_VW vw1
                                        where
                                        (vw1.ASSMNT_YR= v_year+1
                                        and   vw1.ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (vw1.EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (vw1.EXCL_SCOPE_ID=1
                                          and
                                          (
                                           vw1.ASSMNT_YR <v_year+1
                                            or
                                           (vw1.ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and vw1.ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )

                                   AND PERMIT_TAX1.permit_id = P1.permit_id 
                                   AND C1.company_id = P1.company_id 
                                 GROUP BY C1.company_id, C1.ein, C1.legal_nm) TUFA1 
                               LEFT OUTER JOIN (SELECT /*+ INDEX(C11 EIN_UK) */ C11.ein, 
                                                       C11.company_id, 
                                                       SUM(NVL(cbpingested.ingest_vol, 0)) AS ingestvol, 
                                                       SUM(NVL(cbpingested.cbp_taxes_paid, 0)) AS ingesttax, 
                                                       cbpingested.tobacco_class_id 
                                                  FROM (SELECT cbpdetail.importer_ein, 
                                                               cbpdetail.consignee_ein, 
                                                               SUM(NVL(cbpentry.estimated_tax, 0)) AS cbp_taxes_paid, 
                                                               SUM(cbpentry.qty_removed * 1000) AS ingest_vol, 
                                                               TTC1.tobacco_class_nm, 
                                                               cbpentry.fiscal_year, 
                                                               cbpdetail.association_type_cd, 
                                                               TTC1.tobacco_class_id AS tobacco_class_id 
                                                          FROM tu_cbp_importer cbpdetail, 
                                                               tu_cbp_entry cbpentry, 
                                                               tu_tobacco_class TTC1 
                                                         WHERE (cbpentry.excluded_flag IS NULL 
                                                                 OR cbpentry.excluded_flag <> 'Y') 
                                                           AND cbpentry.tobacco_class_id = TTC1.tobacco_class_id 
                                    AND (TTC1.tobacco_class_id = 8 
                                     OR TTC1.tobacco_class_nm = 'Cigars') 
                                    AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id 
                                    AND cbpdetail.association_type_cd IS NOT NULL 
                                    AND cbpdetail.fiscal_yr = v_year GROUP BY cbpdetail.importer_ein, cbpdetail.consignee_ein, TTC1.tobacco_class_nm, cbpentry.fiscal_year, cbpdetail.association_type_cd, TTC1.tobacco_class_id, cbpentry.uom_cd) cbpingested, tu_company C11 WHERE (cbpingested.importer_ein = C11.ein 
                                    AND cbpingested.association_type_cd = 'IMPT' 
                                     OR cbpingested.consignee_ein = C11.ein 
                                    AND cbpingested.association_type_cd = 'CONS') 
                                    AND cbpingested.cbp_taxes_paid != 0 
                                    AND C11.company_id NOT IN 
       ------------------------------------------------------------
                                (    Select distinct company_id
                                     from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )
                    ------------------------------------------------------------
                                                 GROUP BY C11.company_id, C11.ein, cbpingested.tobacco_class_id) INGEST1 
                                 ON TUFA1.company_id = INGEST1.company_id 
                               LEFT OUTER JOIN (SELECT CBP_COMPANY_ID AS company_id, 
                                                       AMENDED_TOTAL_TAX AS AMENDTAX, 
                                                       AMENDED_TOTAL_VOLUME * 1000 AS AMENDVOL, 
                                                       CA1.acceptance_flag 
                                                  FROM tu_cbp_amendment CA1, 
                                                       tu_tobacco_class TC2 
                                                 WHERE CA1.tobacco_class_id = TC2.tobacco_class_id 
                                                   AND CA1.fiscal_yr = v_year 
                                                   AND TC2.tobacco_class_nm = 'Cigars' 
                                                   AND CBP_COMPANY_ID NOT IN 
                                                                  ------------------------------------------------------------
                                (    Select distinct company_id
                                     from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   ) ) AMEND1 
                                 ON TUFA1.company_id = AMEND1.company_id) V2 
                 WHERE ein NOT IN (SELECT TCADS1.EIN 
                                     FROM tu_comparison_all_delta_sts TCADS1 
                                    WHERE TCADS1.STATUS = 'Excluded' 
                                      AND Delta_type = 'TUFA' 
                                      AND TCADS1.FISCAL_YR = v_year 
                                      AND TCADS1.PERMIT_TYPE = 'IMPT' 
                                      AND TCADS1.TOBACCO_CLASS_NM = 'Cigars')) V1 
        UNION ALL 
        SELECT INGESTED_ONLY.Company_id, 
               INGESTED_ONLY.ein, 
               INGESTED_ONLY.legal_nm, 
               INGESTED_ONLY.permit_type, 
               INGESTED_ONLY.delta, 
               DECODE(DECODE(INGESTED_ONLY.permit_type, 'IMPT', 
               (SELECT DISTINCT uom_cd 
                  FROM TU_CBP_IMPORTER TCI, 
                       TU_CBP_ENTRY CE 
                 WHERE tci.IMPORTER_EIN = INGESTED_ONLY.ein 
                   AND TCI.CBP_IMPORTER_ID = CE.CBP_IMPORTER_ID 
                   AND TCI.FISCAL_YR = CE.FISCAL_YEAR 
                   AND CE.FISCAL_YEAR = INGESTED_ONLY.fiscal_yr 
                   AND ce.tobacco_class_id = INGESTED_ONLY.tobacco_class_id), 0), 'K', DECODE(INGESTED_ONLY.permit_type, 'IMPT', 
               (SELECT SUM(CE.QTY_REMOVED) 
                  FROM TU_CBP_IMPORTER TCI, 
                       TU_CBP_ENTRY CE 
                 WHERE tci.IMPORTER_EIN = INGESTED_ONLY.ein 
                   AND TCI.CBP_IMPORTER_ID = CE.CBP_IMPORTER_ID 
                   AND TCI.FISCAL_YR = CE.FISCAL_YEAR 
                   AND CE.FISCAL_YEAR = INGESTED_ONLY.fiscal_yr 
                   AND ce.tobacco_class_id = INGESTED_ONLY.tobacco_class_id), 0) * 1000, 
               'KG', 
               DECODE(INGESTED_ONLY.permit_type, 'IMPT', 
               (SELECT SUM(CE.QTY_REMOVED) 
                  FROM TU_CBP_IMPORTER TCI, 
                       TU_CBP_ENTRY CE 
                 WHERE tci.IMPORTER_EIN = INGESTED_ONLY.ein 
                   AND TCI.CBP_IMPORTER_ID = CE.CBP_IMPORTER_ID 
                   AND TCI.FISCAL_YR = CE.FISCAL_YEAR 
                   AND CE.FISCAL_YEAR = INGESTED_ONLY.fiscal_yr 
                   AND ce.tobacco_class_id = INGESTED_ONLY.tobacco_class_id), 
               0) * 2.204, 
               0) TOTVOL 
          FROM (SELECT (SELECT c.company_id 
                          FROM tu_company c 
                         WHERE c.ein = TCADS2.ein) AS company_id, 
                       TCADS2.EIN, 
                       (SELECT c.legal_nm 
                          FROM tu_company c 
                         WHERE c.ein = TCADS2.ein) AS legal_nm, 
                       (SELECT ttc1.tobacco_class_id 
                          FROM tu_tobacco_class ttc1 
                         WHERE ttc1.TOBACCO_CLASS_NM = TCADS2.TOBACCO_CLASS_NM) tobacco_class_id, 
                       TCADS2.PERMIT_TYPE, 
                       ABS(TCADS2.Delta) delta, 
                       NULL AS TOTVOL, 
                       TCADS2.FISCAL_YR 
                  FROM tu_comparison_all_delta_sts TCADS2 
                 WHERE TCADS2.STATUS = 'MSReady' 
                   AND Delta_type = 'INGESTED' 
                   AND TCADS2.FISCAL_YR = v_year 
                   AND TCADS2.TOBACCO_CLASS_NM = 'Cigars' 
                   AND TCADS2.ein NOT IN 
                                                     (    Select distinct ein
                                  from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )
------------------------------------------------------------------------------------

                                                  ) INGESTED_ONLY 
        UNION 
        SELECT * 
          FROM (SELECT company_id, 
                       ein, 
                       legal_nm, 
                       'MANU' AS permit_type, 
                       CASE acceptance_flag WHEN 'I' THEN ingesttax 
                                            WHEN 'N' THEN amendtax 
                                            ELSE tufatax END AS tottax, 
                       CASE acceptance_flag WHEN 'I' THEN ingestvol 
                                            WHEN 'N' THEN amendvol 
                                            ELSE tufavol END AS totvol 
                  FROM (SELECT TUFA2.company_id, 
                               TUFA2.ein, 
                               TUFA2.legal_nm, 
                               TUFATAX, 
                               TUFAVOL, 
                               INGESTTAX, 
                               INGESTVOL, 
                               AMENDTAX, 
                               AMENDVOL, 
                               acceptance_flag 
                          FROM (SELECT C2.company_id, 
                                       C2.ein, 
                                       C2.legal_nm, 
                                       SUM(qty) AS TUFAVOL, 
                                       SUM(tax) AS TUFATAX 
                                  FROM tu_company C2, 
                                       tu_permit P4, 
                                       (SELECT PP3.permit_id, 
                                               PP3.period_id, 
                                               NVL(DETAILS2.removal_qty, 0) AS qty, 
                                               NVL(DETAILS2.taxes_paid, 0) AS tax 
                                          FROM tu_permit P5 
                                               INNER JOIN tu_permit_period PP3 
                                                  ON P5.permit_id = PP3.permit_id 
                                               INNER JOIN tu_rpt_period RP3 
                                                  ON PP3.period_id = RP3.period_id 
                                               LEFT OUTER JOIN (SELECT PP4.permit_id, 
                                                                       PP4.period_id, 
                                                                       FD2.removal_qty, 
                                                                       FD2.taxes_paid 
                                                                  FROM tu_permit P6, 
                                                                       tu_permit_period PP4, 
                                                                       tu_rpt_period RP4, 
                                                                       tu_submitted_form SF2, 
                                                                       tu_form_detail FD2, 
                                                                       tu_tobacco_class TC3 
                                                                 WHERE TC3.tobacco_class_nm = 'Cigars' 
                                                                   AND SF2.form_type_cd = '3852' 
                                                                   AND TC3.tobacco_class_id = FD2.tobacco_class_id 
                                                                   AND SF2.form_id = FD2.form_id 
                                                                   AND SF2.permit_id = PP4.permit_id 
                                                                   AND SF2.period_id = PP4.period_id 
                                                                   AND PP4.period_id = RP4.period_id 
                                                                   AND P6.permit_id = PP4.permit_id) DETAILS2 
                                                 ON DETAILS2.permit_id = PP3.permit_id 
                                                    AND DETAILS2.period_id = PP3.period_id 
                                         WHERE P5.permit_type_cd = 'MANU' 
                                           AND RP3.fiscal_yr = v_year) PERMIT_TAX2 
                                 WHERE (C2.company_id, P4.permit_num) NOT IN 
          (    Select distinct company_id,permit_num
                                  from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )                         AND PERMIT_TAX2.permit_id = P4.permit_id 
                                   AND C2.company_id = P4.company_id 
                                 GROUP BY C2.company_id, C2.ein, C2.legal_nm) TUFA2 
                               LEFT OUTER JOIN (SELECT C4.company_id, 
                                                       C4.ein, 
                                                       ingestvol, 
                                                       ingesttax 
                                                  FROM (SELECT ein_num, 
                                                               tobacco_class_id, 
                                                               ingestvol, 
                                                               ingesttax 
                                                          FROM (SELECT ttbcmpy.ein_num, 
                                                                       TTC2.tobacco_class_id, 
                                                                       0 AS ingestvol, 
                                                                       SUM(NVL(ttb_taxes_paid, 0)) AS ingesttax 
                                                                  FROM tu_ttb_company ttbcmpy, 
                                                                       tu_ttb_permit ttbpermit, 
                                                                       tu_ttb_annual_tax ttbannualtx, 
                                                                       tu_tobacco_class TTC2 
                                                                 WHERE TTC2.tobacco_class_id = 8 
                                                                   AND ttbcmpy.fiscal_yr = v_year 
                                                                   AND ttbpermit.permit_num IN (SELECT P7.permit_num 
                                                                                                  FROM tu_permit P7, 
                                                                                                       tu_company C3 
                                                                                                 WHERE C3.ein = ttbcmpy.ein_num 
                                                                                                   AND P7.company_id = C3.company_id) 
                                                                   AND ttbannualtx.tobacco_class_id = TTC2.tobacco_class_id 
                                    AND ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id 
                                    AND ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id GROUP BY ttbcmpy.ein_num, TTC2.tobacco_class_id) V5)
                                    ttbingested, tu_company C4 WHERE C4.company_id NOT IN 
                                    (    Select distinct company_id
                                     from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )
                                                   AND ttbingested.ein_num = C4.ein) INGEST2 
                                 ON TUFA2.company_id = INGEST2.company_id 
                               LEFT OUTER JOIN (SELECT TTB_COMPANY_ID AS company_id, 
                                                       AMENDED_TOTAL_TAX AS AMENDTAX, 
                                                       AMENDED_TOTAL_VOLUME * 1000 AS AMENDVOL, 
                                                       CA2.acceptance_flag 
                                                  FROM tu_ttb_amendment CA2, 
                                                       tu_tobacco_class TC4 
                                                 WHERE CA2.tobacco_class_id = TC4.tobacco_class_id 
                                                   AND CA2.fiscal_yr = v_year 
                                                   AND TC4.tobacco_class_nm = 'Cigars' 
                                                   AND TTB_COMPANY_ID NOT IN 
                                                      (    Select distinct company_id
                                     from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )
                                                                                      ) AMEND2 
                                 ON TUFA2.company_id = AMEND2.company_id) V4 
                 WHERE ein NOT IN (SELECT TCADS3.EIN 
                                     FROM tu_comparison_all_delta_sts TCADS3 
                                    WHERE TCADS3.STATUS = 'Excluded' 
                                      AND Delta_type = 'TUFA' 
                                      AND TCADS3.FISCAL_YR = v_year 
                                      AND TCADS3.PERMIT_TYPE = 'MANU' 
                                      AND TCADS3.TOBACCO_CLASS_NM = 'Cigars')) V3) impt_manu, 
       tu_tobacco_class TC5 
 WHERE TC5.tobacco_class_nm = 'Cigars' 
 GROUP BY impt_manu.company_id, impt_manu.ein, impt_manu.legal_nm, TC5.tobacco_class_id 
 ORDER BY TOTTAX DESC
);
      EXCEPTION
        WHEN no_data_found then v_period_tot_taxes := 0;
    END;

--

  BEGIN

    For rec in calc_marketshare LOOP

--
    BEGIN

        -- insert the existing company addresses

      merge into tu_assessment_address rp

         using (select a.company_id, p.assessment_id, p.assessment_version, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total

                  from tu_address a, tu_assessment p

                  where a.company_id = rec.company_id

                  and p.assessment_id = v_assessment_id

                  and a.address_type_cd='PRIM') ad

            on (rp.company_id = ad.company_id

              and rp.assessment_id = ad.assessment_id

              and rp.address_type_cd = ad.address_type_cd

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.street_address = ad.street_address, rp.suite = ad.suite, rp.city = ad.city, rp.state = ad.state, rp.postal_cd = ad.postal_cd, rp.fax_num = ad.fax_num, rp.cntry_dial_cd = ad.cntry_dial_cd, rp.country_cd = ad.country_cd

        when not matched then insert

         (company_id, assessment_id, version_num, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total)

          values (rec.company_id, v_assessment_id, v_version_num, ad.address_type_cd, ad.street_address, ad.suite, ad.attention, ad.city, ad.state, ad.province, ad.postal_cd, ad.fax_num, ad.cntry_dial_cd, ad.country_cd, ad.hash_total);



    -- insert the existing company contacts

       merge into tu_assessment_contact rp

         using (select c.contact_id, c.company_id, p.assessment_id, p.assessment_version, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total

                  from primary_contact_vw c, tu_assessment p

                  where c.company_id = rec.company_id

                  and p.assessment_id = v_assessment_id) ct

            on (rp.company_id = ct.company_id

              and rp.assessment_id = ct.assessment_id

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.contact_id = ct.contact_id, rp.last_nm = ct.last_nm, rp.first_nm = ct.first_nm, rp.phone_num = ct.phone_num, rp.phone_ext = ct.phone_ext, rp.email_address = ct.email_address, rp.country_cd = ct.country_cd, rp.fax_num = ct.fax_num

        when not matched then insert

         (company_id, contact_id, assessment_id, version_num, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total)

          values (rec.company_id, ct.contact_id, v_assessment_id, v_version_num, ct.last_nm, ct.first_nm, ct.phone_num, ct.phone_ext, ct.cntry_dial_cd, ct.email_address, ct.country_cd, ct.fax_num, ct.hash_total);



    END;
--
-- test to see if address or contact has changed since previous version
    BEGIN
      SELECT UNIQUE 'N'

      INTO v_address_changed

      FROM TU_ASSESSMENT a

      INNER JOIN TU_ASSESSMENT_VERSION av on a.assessment_id=av.assessment_id

      INNER JOIN

       ( SELECT * FROM

        (SELECT c.company_id, nvl(aa.assessment_id, v_previous_assessment) as assessment_id, nvl(aa.version_num, v_previous_version) as version_num, nvl(hash_total, 1) as assessment_hash

          FROM TU_COMPANY c LEFT OUTER JOIN TU_ASSESSMENT_ADDRESS aa on c.company_id = aa.company_id

          WHERE c.company_id=rec.company_id)

        WHERE version_num=v_previous_version

        AND assessment_id=v_previous_assessment) aa on aa.assessment_id=av.assessment_id

      INNER JOIN

       ( SELECT c.company_id, nvl(hash_total, 1) as address_hash

        FROM TU_COMPANY c LEFT OUTER JOIN TU_ADDRESS a on c.company_id = a.company_id

        WHERE c.company_id=rec.company_id ) ad on aa.company_id = ad.company_id

      WHERE aa.assessment_hash = ad.address_hash;

        EXCEPTION

          when no_data_found then

          --DBMS_OUTPUT.PUT_LINE('EXCEPTION: ADDRESS CHANGED');

            v_address_changed := 'Y';

    END;

    -- now check contact for changes

    BEGIN

      SELECT UNIQUE 'N'

      INTO v_contact_changed

      FROM TU_ASSESSMENT a

      INNER JOIN TU_ASSESSMENT_VERSION av on a.assessment_id=av.assessment_id

      INNER JOIN

       ( SELECT * FROM

        (SELECT c.company_id, nvl(aa.assessment_id, v_previous_assessment) as assessment_id, nvl(aa.version_num, v_previous_version) as version_num, nvl(hash_total, 1) as assessment_hash

          FROM TU_COMPANY c LEFT OUTER JOIN TU_ASSESSMENT_CONTACT aa on c.company_id = aa.company_id

          WHERE c.company_id=rec.company_id)

        WHERE version_num=v_previous_version

        AND assessment_id=v_previous_assessment) aa on aa.assessment_id=av.assessment_id

      INNER JOIN

       ( SELECT c.company_id, nvl(hash_total, 1) as contact_hash

        FROM TU_COMPANY c LEFT OUTER JOIN TU_CONTACT a on c.company_id = a.company_id

        WHERE c.company_id=rec.company_id ) ad on aa.company_id = ad.company_id

      WHERE aa.assessment_hash = ad.contact_hash;

        EXCEPTION

          when no_data_found then

          --DBMS_OUTPUT.PUT_LINE('EXCEPTION: CONTACT CHANGED');

            v_contact_changed := 'Y';

    END;

--

    BEGIN


      -- calculate the current share

      if v_period_tot_taxes <> '0' then

        v_current_share := trunc(rec.TOTTAX/v_period_tot_taxes,6);

      else

        v_current_share := 0;

      end if;



      -- query for the previous share

      BEGIN

        select share_tot_taxes

        into v_previous_share

        from tu_market_share

        where company_id = rec.company_id

        and assessment_id = v_previous_assessment

        and tobacco_class_id = rec.tobacco_class_id

        and version_num = v_previous_version;

        EXCEPTION

          when others then

            v_previous_share := 0;

      END;

      -- insert market share record
      IF v_current_share > 0 OR v_previous_share > 0 THEN
        -- calculate delta
        v_delta_share := v_current_share - nvl(v_previous_share,0);
        v_rank := v_rank+1;

        -- insert into market share table

      BEGIN
        INSERT into tu_market_share

          (assessment_id,ein, company_id, tobacco_class_id,

           version_num, ms_rank,

           tot_vol_removed, tot_taxes_paid, share_tot_taxes,

           previous_share, delta_share, created_dt,author, original_corrected, address_changed_flag, contact_changed_flag,COMPANY_NAME
           )

        VALUES

          (v_assessment_id,rec.ein, NVL(rec.company_id,0), rec.tobacco_class_id,

           v_version_num, v_rank,

           rec.TOTVOL, rec.TOTTAX,

           v_current_share, v_previous_share, v_delta_share, sysdate,v_author,Case when v_previous_share>0 then 'C' 
                                                                                   when (v_previous_share is null or v_previous_share=0) then 'O'
                                                                                    else 'C' end,
           v_address_changed, v_contact_changed,rec.legal_nm
           );


      EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
                UPDATE tu_market_share
                 SET ms_rank=v_rank,
                     tot_vol_removed=rec.TOTVOL,
                     tot_taxes_paid=rec.TOTTAX, share_tot_taxes=v_current_share,
                     previous_share=v_previous_share, delta_share=v_delta_share
                 WHERE assessment_id=v_assessment_id
                 AND company_id=rec.company_id
                 AND tobacco_class_id=rec.tobacco_class_id
                 AND version_num=v_version_num;

        END;

      END IF;

    END;

    END LOOP;
------------------------Start of changes for ST# 2699-------
 BEGIN
  BEGIN
        Select max(Version_num)
        INTO v_pre_ver
        FROM TU_market_share
        WHERE assessment_id=v_assessment_id
        and version_num not in(  Select max(Version_num)
        FROM TU_market_share
        WHERE assessment_id=v_assessment_id) ;
  END;

  FOR i_val IN cur_pv (v_assessment_id,v_previous_version)
      LOOP
  BEGIN
        Select 'X'
        INTO V_exist
        FROM tu_market_share
        WHERE assessment_id=i_val.assessment_id
        and version_num=v_version_num
        AND company_id=i_val.company_id
        and TOBACCO_CLASS_ID=i_val.TOBACCO_CLASS_ID;
    EXCEPTION
     when others then V_exist:='A';
    END;

    --DBMS_OUTPUT.PUT_LINE('Entered into Story loop V_exist '||V_exist);

    IF  V_exist <>'X'
    THEN

          BEGIN
          Select 'X'
          into v_data_exist
          from tu_market_share
          where assessment_id=v_assessment_id
          and company_id=i_val.company_id
          and tobacco_class_id=i_val.tobacco_class_id
          and version_num=v_version_num;
          EXCEPTION
               when others then
               v_data_exist:='A';
          END;

if v_data_exist<>'X'
then

------below "if" is used for eliminating Company having previous share as null and continue to appear in market share new version---------
if (nvl(i_val.share_tot_taxes,0)>0)
then

          BEGIN
          INSERT into tu_market_share
                    (assessment_id,ein, company_id, tobacco_class_id,tot_vol_removed, tot_taxes_paid,PREVIOUS_SHARE,
                    version_num,delta_share,original_corrected, address_changed_flag, contact_changed_flag,company_name
                     )
          VALUES
                    (v_assessment_id,i_val.ein, i_val.company_id, i_val.tobacco_class_id,'','',i_val.share_tot_taxes,v_version_num,
                     0-abs(i_val.share_tot_taxes),
                     i_val.original_corrected, i_val.address_changed_flag, i_val.contact_changed_flag,
                     i_val.company_name
                     );
                --     DBMS_OUTPUT.PUT_LINE('insert successfull');
          Exception
          When Others then
          null;
          --DBMS_OUTPUT.PUT_LINE('insert failed'||sqlerrm);
          end;

    BEGIN

        -- insert the existing company addresses

      merge into tu_assessment_address rp

         using (select a.company_id, p.assessment_id, p.assessment_version, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total

                  from tu_address a, tu_assessment p

                  where a.company_id = i_val.company_id

                  and p.assessment_id = v_assessment_id

                  and a.address_type_cd='PRIM') ad

            on (rp.company_id = ad.company_id

              and rp.assessment_id = ad.assessment_id

              and rp.address_type_cd = ad.address_type_cd

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.street_address = ad.street_address, rp.suite = ad.suite, rp.city = ad.city, rp.state = ad.state, rp.postal_cd = ad.postal_cd, rp.fax_num = ad.fax_num, rp.cntry_dial_cd = ad.cntry_dial_cd, rp.country_cd = ad.country_cd

        when not matched then insert

         (company_id, assessment_id, version_num, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total)

          values (i_val.company_id, v_assessment_id, v_version_num, ad.address_type_cd, ad.street_address, ad.suite, ad.attention, ad.city, ad.state, ad.province, ad.postal_cd, ad.fax_num, ad.cntry_dial_cd, ad.country_cd, ad.hash_total);


    -- insert the existing company contacts

       merge into tu_assessment_contact rp

         using (select c.contact_id, c.company_id, p.assessment_id, p.assessment_version, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total

                  from primary_contact_vw c, tu_assessment p

                  where c.company_id = i_val.company_id

                  and p.assessment_id = v_assessment_id) ct

            on (rp.company_id = ct.company_id

              and rp.assessment_id = ct.assessment_id

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.contact_id = ct.contact_id, rp.last_nm = ct.last_nm, rp.first_nm = ct.first_nm, rp.phone_num = ct.phone_num, rp.phone_ext = ct.phone_ext, rp.email_address = ct.email_address, rp.country_cd = ct.country_cd, rp.fax_num = ct.fax_num

        when not matched then insert

         (company_id, contact_id, assessment_id, version_num, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total)

          values (i_val.company_id, ct.contact_id, v_assessment_id, v_version_num, ct.last_nm, ct.first_nm, ct.phone_num, ct.phone_ext, ct.cntry_dial_cd, ct.email_address, ct.country_cd, ct.fax_num, ct.hash_total);

    END;

end if;
end if;
end if;
commit;
END LOOP;

-------------------------Added below Code as part of Story CTPTUFA-4835-------------
IF v_submitind = 'Y' then

  BEGIN

    UPDATE TU_PERMIT_EXCL_AFFECT_ASSMNTS
    SET ASSESSMENT_STATUS='COMPLETE'
    where ASSMNT_RPT_DATA_YR=v_year
--  and ASSMNT_RPT_DATA_QTR=v_qtr
    AND ASSESSMENT_STATUS='RE-RUN'
    AND ASSMNT_TYPE='ANNU';

  EXCEPTION
  WHEN NO_DATA_FOUND then null;

  END;

    COMMIT;

END IF;
-------------------------

END;
------------------------END of changes for ST#2699----------

END;

END;

END calc_annual_cigar_test;

/

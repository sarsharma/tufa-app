--------------------------------------------------------
--  DDL for Procedure CALC_ANNUAL_MARKET_SHARE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."CALC_ANNUAL_MARKET_SHARE" (
    p_fiscal_yr IN NUMBER,
    author      IN VARCHAR2)
AS
BEGIN
  DECLARE
  BEGIN
    CALC_ANNUAL_QUARTER(p_fiscal_yr, 1, author);
    CALC_ANNUAL_QUARTER(p_fiscal_yr, 2, author);
    CALC_ANNUAL_QUARTER(p_fiscal_yr, 3, author);
    CALC_ANNUAL_QUARTER(p_fiscal_yr, 4, author);
    calc_annual_cigar_test(p_fiscal_yr, 1, author);
    calc_annual_cigar_test(p_fiscal_yr, 2, author);
    calc_annual_cigar_test(p_fiscal_yr, 3, author);
    calc_annual_cigar_test(p_fiscal_yr, 4, author);
  END;
END CALC_ANNUAL_MARKET_SHARE;

/

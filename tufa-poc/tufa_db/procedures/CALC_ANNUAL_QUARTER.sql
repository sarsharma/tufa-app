--------------------------------------------------------
--  DDL for Procedure CALC_ANNUAL_QUARTER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."CALC_ANNUAL_QUARTER" (assess_yr NUMBER, assess_qtr NUMBER, author VARCHAR2) AS

BEGIN

DECLARE

--  type tot_taxes IS VARRAY(5) OF NUMBER(14,2);
--  v_tot_pipe_taxes tot_taxes := tot_taxes(0.0,0.0,0.0,0.0,0.0);

  v_address_changed CHAR(1) := 'N';
  v_contact_changed CHAR(1) := 'N';
  v_period_tot_pipe_taxes NUMBER(14,2) := 0;
  v_period_tot_chew_taxes NUMBER(14,2) := 0;
  v_period_tot_snuff_taxes NUMBER(14,2) := 0;
  v_period_tot_cigarette_taxes NUMBER(14,2) := 0;
  v_period_tot_ryo_taxes NUMBER(14,2) := 0;
  v_previous_share NUMBER(8,6) := 0;
  v_previous_assessment NUMBER := null;
  v_previous_version NUMBER(2) := 0;
  v_max_version NUMBER(2) := 0;
  v_current_share NUMBER(8,6) := 0;
  v_delta_share NUMBER(8,6) := 0;
  v_assessment_id NUMBER := 0;
  v_year NUMBER(4) := assess_yr;
  v_qtr NUMBER(1) := assess_qtr;
  v_author VARCHAR2(50) :=author;
  v_marketshareType VARCHAR2(6) := 'FULLMS';
  v_submitind CHAR(1);
  v_version_num NUMBER(2) := '1';
  v_cigarette_rank NUMBER(4) := 0;
  v_chew_rank NUMBER(4) := 0;
  v_ryo_rank NUMBER(4) := 0;
  v_snuff_rank NUMBER(4) := 0;
  v_pipe_rank NUMBER(4) := 0;
  V_pre_ver number;
  v_exist  varchar2(5);
  v_data_exist varchar2(5);

 CURSOR CUR_PV (p_assignment_id number, p_version_num number)
  IS
  SELECT * from tu_market_share
  where ASSESSMENT_ID=p_assignment_id
  and version_num=p_version_num;


  CURSOR calc_marketshare IS
    SELECT company_id,ein,legal_nm, tc.tobacco_class_id, tc.tobacco_class_nm, SUM(TOTTAX) as TOTTAX, SUM(TOTVOL) as TOTVOL
    FROM
    (
      SELECT *
      FROM
      (SELECT
        tufa.company_id,tufa.ein as ein,tufa.legal_nm,
        tufa.tobacco_class_id,
        'IMPT' as permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
           CASE ( amend.acceptance_flag||amend.review_flag )
                WHEN 'IY'   THEN amend.amendtax

                WHEN 'IN'   THEN ingest.ingesttax
                WHEN 'NN' THEN  amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,

            CASE ( amend.acceptance_flag||amend.review_flag )
                WHEN 'IY'   THEN amend.amendvol

                WHEN 'IN'   THEN ingest.ingestvol
                WHEN 'NN' THEN  amend.amendvol
                ELSE tufa.tufavol
                END AS totvol


/*
            CASE ( amend.acceptance_flag )
                WHEN 'I'   THEN ingest.ingesttax
                WHEN 'N'   THEN amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,
            CASE (amend.acceptance_flag)
                WHEN 'I'   THEN ingest.ingestvol
                WHEN 'N'   THEN amend.amendvol
                ELSE tufa.tufavol
            END AS totvol
*/
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
      FROM
      (
        SELECT c.company_id,c.ein,c.legal_nm, taxes.tobacco_class_id, SUM(taxes.qty) as TUFAVOL, SUM(taxes.tax) as TUFATAX
        FROM tu_company c
        INNER JOIN tu_permit p on c.company_id = p.company_id
        INNER JOIN
        (
          SELECT pp_tobacco.permit_id, pp_tobacco.tobacco_class_id, NVL(details.removal_qty, 0) as qty, NVL(details.taxes_paid, 0) as tax
          FROM
          (
            SELECT permit_periods.permit_id, permit_periods.period_id, tc.tobacco_class_id
            FROM
              (SELECT pp.permit_id, pp.period_id
              FROM tu_permit_period pp
              INNER JOIN tu_rpt_period rp on pp.period_id = rp.period_id
              WHERE rp.fiscal_yr=v_year
              AND rp.quarter=v_qtr) permit_periods
            INNER JOIN tu_tobacco_class tc on tc.class_type_cd <> 'SPTP' AND tc.tobacco_class_nm <> 'Cigars'
          ) pp_tobacco
          LEFT OUTER JOIN
          (
            SELECT sf.permit_id, sf.period_id, fd.tobacco_class_id, fd.removal_qty, fd.taxes_paid
            FROM tu_submitted_form sf
            INNER JOIN tu_form_detail fd on sf.form_id = fd.form_id
            where sf.form_type_cd = '3852'
          ) details on
                  pp_tobacco.permit_id = details.permit_id
              AND pp_tobacco.period_id = details.period_id
              AND pp_tobacco.tobacco_class_id = details.tobacco_class_id
        ) taxes on taxes.permit_id = p.permit_id
        WHERE p.permit_type_cd='IMPT'
        -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        and (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------


        GROUP BY c.company_id,c.ein,c.legal_nm, taxes.tobacco_class_id
      ) tufa
---------------------- Start JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    c1.ein,
                    c1.company_id,
                    SUM(nvl(cbpingested.ingest_vol,0) ) AS ingestvol,
                    SUM(nvl(cbpingested.cbp_taxes_paid,0) ) AS ingesttax,
                    cbpingested.tobacco_class_id
                FROM
                    (
                        SELECT
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            SUM(nvl(cbpentry.estimated_tax,0) ) AS cbp_taxes_paid,
                            SUM(CASE
                                WHEN ttc.tobacco_class_id = 7 THEN cbpentry.qty_removed * 1000
                                ELSE cbpentry.qty_removed * 2.204
                            END) AS ingest_vol,
                            ttc.tobacco_class_nm,
                            CASE
                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                                ELSE '1-4'
                            END AS fiscal_qtr,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id   AS tobacco_class_id
                        FROM
                            tu_cbp_importer cbpdetail,
                            tu_cbp_entry cbpentry,
                            tu_tobacco_class ttc
                        WHERE
                            ( cbpentry.excluded_flag IS NULL
                              OR cbpentry.excluded_flag <> 'Y' )
                            AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
                            AND ( ttc.tobacco_class_id <> 14
                                  OR ttc.tobacco_class_nm <> 'Non-Taxable' )
                            AND ( ttc.tobacco_class_id <> 8
                                  OR ttc.tobacco_class_nm <> 'Cigars' )
                            AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
                            AND cbpdetail.association_type_cd IS NOT NULL
                            AND cbpdetail.fiscal_yr =v_year
                            AND cbpentry.fiscal_qtr =v_qtr
                        GROUP BY
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            ttc.tobacco_class_nm,
                            CASE
                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                                ELSE '1-4'
                            END,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id,
                            cbpentry.uom_cd
                    ) cbpingested,
                    tu_company c1
                WHERE
                    ( cbpingested.importer_ein = c1.ein
                      AND cbpingested.association_type_cd = 'IMPT'
                      OR cbpingested.consignee_ein = c1.ein
                      AND cbpingested.association_type_cd = 'CONS' )
                    AND cbpingested.cbp_taxes_paid != 0
                    AND c1.company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                GROUP BY
                    c1.company_id,
                    c1.ein,
                    cbpingested.tobacco_class_id
            ) ingest ON tufa.company_id = ingest.company_id
                        AND tufa.ein = ingest.ein
                        AND tufa.tobacco_class_id = ingest.tobacco_class_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      LEFT OUTER JOIN (
        SELECT CBP_COMPANY_ID as company_id, tc.tobacco_class_id, AMENDED_TOTAL_TAX as AMENDTAX, ca.acceptance_flag,ca.review_flag,
        CASE WHEN tc.tobacco_class_id = 7  or tc.tobacco_class_id = 8 THEN AMENDED_TOTAL_VOLUME * 1000
        ELSE  AMENDED_TOTAL_VOLUME * 2.204 END AS AMENDVOL
        FROM tu_cbp_amendment ca, tu_tobacco_class tc
        WHERE ca.tobacco_class_id = tc.tobacco_class_id
        and ca.fiscal_yr=v_year
        and ca.qtr=v_qtr
        and tc.tobacco_class_nm <> 'Cigars'
        AND cbp_company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

      ) amend on tufa.company_id = amend.company_id and tufa.tobacco_class_id = amend.tobacco_class_id
      )
            ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
      WHERE (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,1,instr(tcads.tobacco_class_nm,'/')-1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='IMPT'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
       and (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,instr(tcads.tobacco_class_nm,'/')+1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='IMPT'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------


      UNION All
     ----------------------------------------BEGIN INGESTED ONLY added on 16 oct 2018-----------------------------------------------------

      Select INGESTED_ONLY.Company_id,INGESTED_ONLY.ein,INGESTED_ONLY.legal_nm,INGESTED_ONLY.tobacco_class_id,INGESTED_ONLY.permit_type,INGESTED_ONLY.delta,
      --4240 Kyle - Was not pulling the TOTVOL data for MANU so grouped tobacco didn't show any volume
      Decode(INGESTED_ONLY.permit_type,
        'IMPT',decode( (SELECT distinct uom_cd FROM TU_CBP_IMPORTER TCI,TU_CBP_ENTRY CE
          WHERE tci.IMPORTER_EIN=INGESTED_ONLY.ein
          and TCI.CBP_IMPORTER_ID=CE.CBP_IMPORTER_ID
          AND TCI.FISCAL_YR=CE.FISCAL_YEAR
          and CE.FISCAL_YEAR=INGESTED_ONLY.fiscal_yr
          and to_char(CE.FISCAL_QTR)=INGESTED_ONLY.FISCAL_QTR
          and ce.tobacco_class_id=INGESTED_ONLY.tobacco_class_id),
          'K',(decode (INGESTED_ONLY.permit_type,'IMPT',(SELECT sum(CE.QTY_REMOVED)
              FROM TU_CBP_IMPORTER TCI,TU_CBP_ENTRY CE
              WHERE tci.IMPORTER_EIN=INGESTED_ONLY.ein
              and TCI.CBP_IMPORTER_ID=CE.CBP_IMPORTER_ID
              AND TCI.FISCAL_YR=CE.FISCAL_YEAR
              and CE.FISCAL_YEAR=INGESTED_ONLY.fiscal_yr
              and to_char(CE.FISCAL_QTR)=INGESTED_ONLY.FISCAL_QTR
              and ce.tobacco_class_id=INGESTED_ONLY.tobacco_class_id),0))*1000,
          'KG',(decode (INGESTED_ONLY.permit_type,'IMPT',(SELECT sum(CE.QTY_REMOVED)
              FROM TU_CBP_IMPORTER TCI,TU_CBP_ENTRY CE
              WHERE tci.IMPORTER_EIN=INGESTED_ONLY.ein
              and TCI.CBP_IMPORTER_ID=CE.CBP_IMPORTER_ID
              AND TCI.FISCAL_YR=CE.FISCAL_YEAR
              and CE.FISCAL_YEAR=INGESTED_ONLY.fiscal_yr
              and to_char(CE.FISCAL_QTR)=INGESTED_ONLY.FISCAL_QTR
              and ce.tobacco_class_id=INGESTED_ONLY.tobacco_class_id),0))*2.204,
           0
        ),
        'MANU',INGESTED_ONLY.TOTVOL
      ) TOTVOL
      from (
             SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id
             ,tcads.EIN,
             (Select c.legal_nm from tu_company c where c.ein=tcads.ein) as legal_nm ,
             (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_CLASS_NM) tobacco_class_id,
            -- tcads.TOBACCO_CLASS_NM,
             tcads.PERMIT_TYPE,
             ABS(tcads.Delta) delta,
              null as TOTVOL,
              tcads.fiscal_yr,
              tcads.fiscal_qtr--,tcads.permit_type
           from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and tcads.Delta_type='INGESTED'
           and tcads.FISCAL_YR=v_year   and tcads.fiscal_qtr=to_char(v_qtr)
           and tcads.tobacco_sub_type_1  is  null
           and  tcads.tobacco_sub_type_2 is null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
           and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------

           UNION

             SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
                    (Select c.legal_nm from tu_company c where c.ein=tcads.ein) as legal_nm ,
             (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_SUB_TYPE_1) tobacco_class_id,
             --TOBACCO_SUB_TYPE_1,
             tcads.PERMIT_TYPE,
             tobacco_sub_type_1_amndtx ,
             TOBACCO_SUB_TYPE_1_POUNDS as TOTVOL,
                tcads.fiscal_yr,
              tcads.fiscal_qtr
              from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and tcads.Delta_type='INGESTED'
           and tcads.FISCAL_YR=v_year   and tcads.fiscal_qtr=to_char(v_qtr)
           and tcads.tobacco_sub_type_1  is  not null
           -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
           and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------

           UNION

             SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
             (Select c.legal_nm from tu_company c where c.ein=tcads.ein) as legal_nm ,
             (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_SUB_TYPE_2) tobacco_class_id,
             --tcads.TOBACCO_SUB_TYPE_2,
             tcads.PERMIT_TYPE,
            tobacco_sub_type_2_amndtx ,
             TOBACCO_SUB_TYPE_2_POUNDS as TOTVOL,
                tcads.fiscal_yr,
              tcads.fiscal_qtr
               from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and tcads.Delta_type='INGESTED'
        and tcads.FISCAL_YR=v_year    and tcads.fiscal_qtr=to_char(v_qtr)
           and  tcads.tobacco_sub_type_2 is not null
           -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
           and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------
           ) INGESTED_ONLY

    ----------------------------------------END INGESTED ONLY added on 16 oct 2018-----------------------------------------------------

    UNION

   SELECT *
   FROM (
        SELECT
            tufa.company_id,
            tufa.ein,
            tufa.legal_nm,
            tufa.tobacco_class_id,
            'MANU' AS permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            CASE ( amend.acceptance_flag )

                WHEN 'I'   THEN nvl(nvl(amend.accept_ingest_tax,ingest.ingesttax),0)
                WHEN 'N'   THEN amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,
            CASE ( amend.acceptance_flag )
                WHEN 'I'   THEN nvl(ingest.ingestvol,0)
                WHEN 'N'   THEN amend.amendvol
                ELSE tufa.tufavol
            END AS totvol
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
        FROM
            (
                SELECT
                    c.company_id,
                    c.ein,
                    c.legal_nm,
                    taxes.tobacco_class_id,
                    SUM(taxes.qty) AS tufavol,
                    SUM(taxes.tax) AS tufatax
                FROM
                    tu_company c
                    INNER JOIN tu_permit p ON c.company_id = p.company_id
                    INNER JOIN (
                        SELECT
                            pp_tobacco.permit_id,
                            pp_tobacco.tobacco_class_id,
                            nvl(details.removal_qty,0) AS qty,
                            nvl(details.taxes_paid,0) AS tax
                        FROM
                            (
                                SELECT
                                    permit_periods.permit_id,
                                    permit_periods.period_id,
                                    tc.tobacco_class_id
                                FROM
                                    (
                                        SELECT
                                            pp.permit_id,
                                            pp.period_id
                                        FROM
                                            tu_permit_period pp
                                            INNER JOIN tu_rpt_period rp ON pp.period_id = rp.period_id
                                        WHERE
                                            rp.fiscal_yr =v_year
                                            AND rp.quarter =v_qtr
                                    ) permit_periods
                                    INNER JOIN tu_tobacco_class tc ON tc.class_type_cd <> 'SPTP'
                                                                      AND tc.tobacco_class_nm <> 'Cigars'
                            ) pp_tobacco
                            LEFT OUTER JOIN (
                                SELECT
                                    sf.permit_id,
                                    sf.period_id,
                                    fd.tobacco_class_id,
                                    fd.removal_qty,
                                    fd.taxes_paid
                                FROM
                                    tu_submitted_form sf
                                    INNER JOIN tu_form_detail fd ON sf.form_id = fd.form_id
                                WHERE
                                    sf.form_type_cd = '3852'
                            ) details ON pp_tobacco.permit_id = details.permit_id
                                         AND pp_tobacco.period_id = details.period_id
                                         AND pp_tobacco.tobacco_class_id = details.tobacco_class_id
                    ) taxes ON taxes.permit_id = p.permit_id
                WHERE
                    p.permit_type_cd = 'MANU'
                            -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
               AND (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------

                GROUP BY
                    c.company_id,
                    c.ein,
                    c.legal_nm,
                    taxes.tobacco_class_id
            ) tufa
---------------------- Start JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
-- Only cigarette taxe data for now
            LEFT OUTER JOIN (
                SELECT
                    c.company_id,
                    c.ein,
                    tobacco_class_id,
                    sum(ingestvol) as ingestvol,
                    sum(ingesttax) as ingesttax
                FROM
                    (
                    --Replacement was done here to grab both ingested vol and tax data
                    --Kyle as per 21 March 2019
                        SELECT
                            ein_num,
                            tobacco_class_id,
                            SUM(ingestvol) ingestvol,
                            ingesttax
                        FROM
                            (
                                SELECT
                                    nvl(vol.ein_num,tax.ein_num) ein_num,
                                    nvl(vol.tobacco_class_id,tax.tobacco_class_id) tobacco_class_id,
                                    nvl(vol.ingestvol,0) ingestvol,
                                    nvl(tax.ingesttax,0) ingesttax
                                FROM
                                    (
-- INGESTED VOLUME DATA
-- KYLE 3/28/2019: Added UNIQUE since duplicate records were being pulled for Volume
                                        SELECT UNIQUE
                                            tc.ein_num,
                                            tc.ttb_company_id,
                                            ttc.tobacco_class_id,
                                            ttc.parent_class_id,
                                            nvl(tvt.pounds,0) AS ingestvol,
                                            tvt.permit_num
                                        FROM
                                            tu_ttb_company tc
                                            LEFT JOIN tu_ttb_permit tp ON tc.ttb_company_id = tp.ttb_company_id
                                            LEFT JOIN tu_ttb_volume_taxes_per_tt tvt ON tvt.ein = tc.ein_num
                                                                                        AND tc.fiscal_yr = tvt.fiscal_yr
                                            LEFT JOIN tu_tobacco_class ttc ON ttc.tobacco_class_nm = tvt.product
                                        WHERE
                                            tc.fiscal_yr =v_year
                                            AND tvt.qtr =v_qtr
                                            AND tvt.permit_num IN (
                                                SELECT
                                                    p.permit_num
                                                FROM
                                                    tu_permit p
                                                    JOIN tu_company c ON p.company_id = c.company_id
                                                WHERE
                                                    c.ein = tc.ein_num
                                            )
                                    ) vol
            -- Only care about tax data regardles in there is volume data to go with it
                                    RIGHT JOIN (
-- INGESTED TAX DATA
                                        SELECT
                                            ttbcmpy.ein_num,
                                            ttc.tobacco_class_id,
                                            NULL AS parent_class_id,
                                            SUM(nvl(ttb_taxes_paid,0) ) AS ingesttax,
                                            ttbpermit.permit_num
                                        FROM
                                            tu_ttb_company ttbcmpy
                                            INNER JOIN tu_ttb_permit ttbpermit ON ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id
                                            INNER JOIN tu_ttb_annual_tax ttbannualtx ON ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id
                                            INNER JOIN tu_tobacco_class ttc ON ttbannualtx.tobacco_class_id = ttc.tobacco_class_id
                                        WHERE
                                            ttc.tobacco_class_id <> 8
                                            AND ttbcmpy.fiscal_yr =v_year
                                            AND ttbannualtx.ttb_calendar_qtr =v_qtr
                                            AND ttbpermit.permit_num IN (
                                                SELECT
                                                    p.permit_num
                                                FROM
                                                    tu_permit p
                                                    JOIN tu_company c ON p.company_id = c.company_id
                                                WHERE
                                                    c.ein = ttbcmpy.ein_num
                                            )
                                        GROUP BY
                                            ttbcmpy.ein_num,
                                            ttc.tobacco_class_id,
                                            ttbpermit.permit_num
                                    ) tax ON vol.ein_num = tax.ein_num
                                             AND vol.permit_num = tax.permit_num
                                             AND CASE
                                        WHEN vol.parent_class_id IS NOT NULL THEN vol.parent_class_id
                                        ELSE vol.tobacco_class_id
                                    END = tax.tobacco_class_id
                            )
                        GROUP BY
                            ein_num,
                            tobacco_class_id,
                            ingesttax
                    ) ttbingested
                    INNER JOIN tu_company c
                    ON ttbingested.ein_num = c.ein
                    WHERE c.company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

                    GROUP BY c.company_id,c.ein,tobacco_class_id) ingest
                       ON tufa.company_id = ingest.company_id
                        AND tufa.tobacco_class_id = ingest.tobacco_class_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    ttb_company_id                AS company_id,
                    tc.tobacco_class_id,
                    amended_total_tax             AS amendtax,
                    ca.acceptance_flag,
                    CASE
                        WHEN tc.tobacco_class_id = 7
                             OR tc.tobacco_class_id = 8 THEN amended_total_volume * 1000
                        ELSE amended_total_volume
                    END AS amendvol,
                    accepted_ingested_total_tax   AS accept_ingest_tax
                FROM
                    tu_ttb_amendment ca,
                    tu_tobacco_class tc
                WHERE
                    ca.tobacco_class_id = tc.tobacco_class_id
                    AND ca.fiscal_yr =v_year
                    AND ca.qtr =v_qtr
                    AND tc.tobacco_class_nm <> 'Cigars'
                    AND ttb_company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

            ) amend ON tufa.company_id = amend.company_id
                       AND tufa.tobacco_class_id = amend.tobacco_class_id
    )
     ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
      WHERE (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,1,instr(tcads.tobacco_class_nm,'/')-1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='MANU'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
       and (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,instr(tcads.tobacco_class_nm,'/')+1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='MANU'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------

    ) combined
    INNER JOIN tu_tobacco_class tc on tc.tobacco_class_id = combined.tobacco_class_id
    --where ein='611208598'
    --and tc.tobacco_class_id=6
    GROUP BY company_id,ein,legal_nm, tc.tobacco_class_id, tc.tobacco_class_nm
    ORDER BY tc.tobacco_class_id, TOTTAX DESC;




BEGIN
-- determine submit status from true-up
  SELECT submitted_ind
    INTO v_submitind
  FROM tu_annual_trueup
  WHERE fiscal_yr = v_year;

-- get assessment information

  BEGIN

    SELECT assessment_id, assessment_yr, assessment_qtr

      INTO v_assessment_id, v_year, v_qtr

    FROM tu_assessment

    WHERE assessment_yr = v_year

      and assessment_qtr = v_qtr

      and assessment_type = 'ANNU';

    exception when no_data_found then v_assessment_id := '0';

  END;

 --   DBMS_OUTPUT.PUT_LINE('ASSESSMENT ID:' || v_assessment_id);



-- get existing version information

  BEGIN

    SELECT max(version_num)

    INTO v_max_version

    from tu_assessment_version

    where assessment_id = v_assessment_id;

    EXCEPTION

      when no_data_found then

        v_max_version := -1;

  END;

  if v_max_version IS NULL then
    v_max_version := -1;
  end if;

--    DBMS_OUTPUT.PUT_LINE('MAX VERSION:' || v_max_version);

-- calculate current version and previous version

  BEGIN

    -- highest version will always be the previous

    v_previous_version := v_max_version;

    IF v_submitind = 'N' then
      -- we are generating version 0 (temporary version)
      v_version_num := 0;
    ELSIF v_submitind = 'Y' then
      -- we are generating the next version
      v_version_num := v_previous_version + 1;
    END IF;

    if v_previous_version < 1 then
      -- no current true-up marketshare
      -- that means the previous version is the quarterly assessment
      BEGIN
        SELECT assessment_id
          INTO v_previous_assessment
        FROM tu_assessment
        WHERE assessment_yr = v_year
          and assessment_qtr = v_qtr
          and assessment_type = 'QTRY';
        exception when no_data_found then v_previous_assessment := null;
      END;
      if v_previous_assessment IS NOT NULL then
        BEGIN
            SELECT max(version_num)
            INTO v_previous_version
            from tu_market_share
            where assessment_id = v_previous_assessment;
            EXCEPTION
            when no_data_found then
              v_previous_version := 0;
        END;
      else
        -- couldn't find the quarterly assessment
        -- set previous assessment back to current
        v_previous_assessment := v_assessment_id;
      end if;
    else
      -- we have already generated a true-up market share
      -- compare this version to previously generated trueup
      v_previous_assessment := v_assessment_id;
    END IF;

    -- upsert assessment version
    MERGE INTO TU_ASSESSMENT_VERSION av using dual on (assessment_id = v_assessment_id and version_num=v_version_num)
    WHEN MATCHED THEN
      UPDATE SET created_by=v_author, created_dt=SYSDATE
    WHEN NOT MATCHED THEN
      INSERT (assessment_id, version_num, created_by, created_dt)
      VALUES (v_assessment_id, v_version_num, v_author, SYSDATE);

  END;
   -- DBMS_OUTPUT.PUT_LINE('CURRENT ASSESSMENT: ' || v_assessment_id);
    --DBMS_OUTPUT.PUT_LINE('CURRENT VERSION:' || v_version_num);
    --DBMS_OUTPUT.PUT_LINE('PREVIOUS ASSESSMENT: ' || v_previous_assessment);
    --DBMS_OUTPUT.PUT_LINE('PREVIOUS VERSION: ' || v_previous_version);

-- now delete all version zero (temporary) information

-- including marketshare, addresses, and contacts

  BEGIN

    DELETE from tu_market_share

    WHERE assessment_id = v_assessment_id

      and version_num = '0';

    EXCEPTION when no_data_found then NULL;

  END;

  BEGIN

    DELETE from tu_assessment_address

    WHERE assessment_id = v_assessment_id

      and version_num = '0';

    EXCEPTION when no_data_found then NULL;

  END;

  BEGIN

    DELETE from tu_assessment_contact

    WHERE assessment_id = v_assessment_id

      and version_num = '0';

    EXCEPTION when no_data_found then NULL;

  END;

--

  BEGIN
    SELECT SUM(TOTTAX) as TOTTAX
      into v_period_tot_pipe_taxes
    FROM
    (
    SELECT *
    FROM (
      SELECT
        tufa.company_id,tufa.ein,
        tufa.tobacco_class_id,
        'IMPT' as permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
              CASE ( amend.acceptance_flag||amend.review_flag )
                WHEN 'IY'   THEN amend.amendtax

                WHEN 'IN'   THEN ingest.ingesttax
                WHEN 'NN' THEN  amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,

            CASE ( amend.acceptance_flag||amend.review_flag )
                WHEN 'IY'   THEN amend.amendvol

                WHEN 'IN'   THEN ingest.ingestvol
                WHEN 'NN' THEN  amend.amendvol
                ELSE tufa.tufavol
                END AS totvol



        /*    CASE ( amend.acceptance_flag )
                WHEN 'I'   THEN ingest.ingesttax
                WHEN 'N'   THEN amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,
            CASE (amend.acceptance_flag)
                WHEN 'I'   THEN ingest.ingestvol
                WHEN 'N'   THEN amend.amendvol
                ELSE tufa.tufavol
            END AS totvol
        */
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      FROM
      (
        SELECT c.company_id,c.ein, taxes.tobacco_class_id, SUM(taxes.qty) as TUFAVOL, SUM(taxes.tax) as TUFATAX
        FROM tu_company c
        INNER JOIN tu_permit p on c.company_id = p.company_id
        INNER JOIN
        (
          SELECT pp_tobacco.permit_id, pp_tobacco.tobacco_class_id, NVL(details.removal_qty, 0) as qty, NVL(details.taxes_paid, 0) as tax
          FROM
          (
            SELECT permit_periods.permit_id, permit_periods.period_id, tc.tobacco_class_id
            FROM
              (SELECT pp.permit_id, pp.period_id
              FROM tu_permit_period pp
              INNER JOIN tu_rpt_period rp on pp.period_id = rp.period_id
              WHERE rp.fiscal_yr=v_year
              AND rp.quarter=v_qtr) permit_periods
            INNER JOIN tu_tobacco_class tc on tc.class_type_cd <> 'SPTP' AND tc.tobacco_class_nm <> 'Cigars'
          ) pp_tobacco
          LEFT OUTER JOIN
          (
            SELECT sf.permit_id, sf.period_id, fd.tobacco_class_id, fd.removal_qty, fd.taxes_paid
            FROM tu_submitted_form sf
            INNER JOIN tu_form_detail fd on sf.form_id = fd.form_id
            where sf.form_type_cd = '3852'
          ) details on
                  pp_tobacco.permit_id = details.permit_id
              AND pp_tobacco.period_id = details.period_id
              AND pp_tobacco.tobacco_class_id = details.tobacco_class_id
        ) taxes on taxes.permit_id = p.permit_id
        WHERE p.permit_type_cd='IMPT'
                -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        and (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------

        GROUP BY c.company_id,c.ein,taxes.tobacco_class_id
      ) tufa
---------------------- Start JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    c1.ein,
                    c1.company_id,
                    SUM(nvl(cbpingested.ingest_vol,0) ) AS ingestvol,
                    SUM(nvl(cbpingested.cbp_taxes_paid,0) ) AS ingesttax,
                    cbpingested.tobacco_class_id
                FROM
                    (
                        SELECT
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            SUM(nvl(cbpentry.estimated_tax,0) ) AS cbp_taxes_paid,
                            SUM(CASE
                                WHEN ttc.tobacco_class_id = 7 THEN cbpentry.qty_removed * 1000
                                ELSE cbpentry.qty_removed * 2.204
                            END) AS ingest_vol,
                            ttc.tobacco_class_nm,
                            CASE
                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                                ELSE '1-4'
                            END AS fiscal_qtr,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id   AS tobacco_class_id
                        FROM
                            tu_cbp_importer cbpdetail,
                            tu_cbp_entry cbpentry,
                            tu_tobacco_class ttc
                        WHERE
                            ( cbpentry.excluded_flag IS NULL
                              OR cbpentry.excluded_flag <> 'Y' )
                            AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
                            AND ( ttc.tobacco_class_id <> 14
                                  OR ttc.tobacco_class_nm <> 'Non-Taxable' )
                            AND ( ttc.tobacco_class_id <> 8
                                  OR ttc.tobacco_class_nm <> 'Cigars' )
                            AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
                            AND cbpdetail.association_type_cd IS NOT NULL
                            AND cbpdetail.fiscal_yr =v_year
                            AND cbpentry.fiscal_qtr =v_qtr
                        GROUP BY
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            ttc.tobacco_class_nm,
                            CASE
                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                                ELSE '1-4'
                            END,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id,
                            cbpentry.uom_cd
                    ) cbpingested,
                    tu_company c1
                WHERE
                    ( cbpingested.importer_ein = c1.ein
                      AND cbpingested.association_type_cd = 'IMPT'
                      OR cbpingested.consignee_ein = c1.ein
                      AND cbpingested.association_type_cd = 'CONS' )
                    AND cbpingested.cbp_taxes_paid != 0
                    AND c1.company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

                GROUP BY
                    c1.company_id,
                    c1.ein,
                    cbpingested.tobacco_class_id
            ) ingest ON tufa.company_id = ingest.company_id
                        AND tufa.ein = ingest.ein
                        AND tufa.tobacco_class_id = ingest.tobacco_class_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      LEFT OUTER JOIN (
        SELECT CBP_COMPANY_ID as company_id, tc.tobacco_class_id, AMENDED_TOTAL_TAX as AMENDTAX, AMENDED_TOTAL_VOLUME as AMENDVOL,
        ca.acceptance_flag,ca.review_flag
        FROM tu_cbp_amendment ca, tu_tobacco_class tc
        WHERE ca.tobacco_class_id = tc.tobacco_class_id
        and ca.fiscal_yr=v_year
        and ca.qtr=v_qtr
        and tc.tobacco_class_nm <> 'Cigars'
        AND cbp_company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

      ) amend on tufa.company_id = amend.company_id and tufa.tobacco_class_id = amend.tobacco_class_id
      )
     ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
      WHERE (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,1,instr(tcads.tobacco_class_nm,'/')-1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='IMPT'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
       and (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,instr(tcads.tobacco_class_nm,'/')+1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='IMPT'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------

      UNION All

     ----------------------------------------BEGIN INGESTED ONLY added on 16 oct 2018-----------------------------------------------------

Select * from (
       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_CLASS_NM) tobacco_class_id,
      -- tcads.TOBACCO_CLASS_NM,
       tcads.PERMIT_TYPE,
       ABS(tcads.Delta),
        null as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year
     and tcads.fiscal_qtr=to_char(v_qtr)
     and tobacco_sub_type_1  is  null
     and  tobacco_sub_type_2 is null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
           and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------     

     UNION

       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_SUB_TYPE_1) tobacco_class_id,
       --TOBACCO_SUB_TYPE_1,
       tcads.PERMIT_TYPE,
       tobacco_sub_type_1_amndtx ,
        TOBACCO_SUB_TYPE_1_POUNDS as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year   and tcads.fiscal_qtr=to_char(v_qtr)
     and tobacco_sub_type_1  is  not null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
           and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------
     UNION

       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_SUB_TYPE_2) tobacco_class_id,
       --tcads.TOBACCO_SUB_TYPE_2,
       tcads.PERMIT_TYPE,
      tobacco_sub_type_2_amndtx ,
       TOBACCO_SUB_TYPE_2_POUNDS as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year    and tcads.fiscal_qtr=to_char(v_qtr)
     and  tobacco_sub_type_2 is not null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
           and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------
) INGESTED_ONLY


    ----------------------------------------END INGESTED ONLY added on 16 oct 2018-----------------------------------------------------

      UNION

      SELECT *
      FROM
      (
        SELECT
            tufa.company_id,
            tufa.ein,
            tufa.tobacco_class_id,
            'MANU' AS permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------


           CASE ( amend.acceptance_flag )

                WHEN 'I'   THEN nvl(nvl(amend.accept_ingest_tax,ingest.ingesttax),0)
                WHEN 'N'   THEN amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,
            CASE ( amend.acceptance_flag )
                WHEN 'I'   THEN nvl(ingest.ingestvol,0)
                WHEN 'N'   THEN amend.amendvol
                ELSE tufa.tufavol
            END AS totvol
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
        FROM
            (
                SELECT
                    c.company_id,
                    c.ein,
                    taxes.tobacco_class_id,
                    SUM(taxes.qty) AS tufavol,
                    SUM(taxes.tax) AS tufatax
                FROM
                    tu_company c
                    INNER JOIN tu_permit p ON c.company_id = p.company_id
                    INNER JOIN (
                        SELECT
                            pp_tobacco.permit_id,
                            pp_tobacco.tobacco_class_id,
                            nvl(details.removal_qty,0) AS qty,
                            nvl(details.taxes_paid,0) AS tax
                        FROM
                            (
                                SELECT
                                    permit_periods.permit_id,
                                    permit_periods.period_id,
                                    tc.tobacco_class_id
                                FROM
                                    (
                                        SELECT
                                            pp.permit_id,
                                            pp.period_id
                                        FROM
                                            tu_permit_period pp
                                            INNER JOIN tu_rpt_period rp ON pp.period_id = rp.period_id
                                        WHERE
                                            rp.fiscal_yr =v_year
                                            AND rp.quarter =v_qtr
                                    ) permit_periods
                                    INNER JOIN tu_tobacco_class tc ON tc.class_type_cd <> 'SPTP'
                                                                      AND tc.tobacco_class_nm <> 'Cigars'
                            ) pp_tobacco
                            LEFT OUTER JOIN (
                                SELECT
                                    sf.permit_id,
                                    sf.period_id,
                                    fd.tobacco_class_id,
                                    fd.removal_qty,
                                    fd.taxes_paid
                                FROM
                                    tu_submitted_form sf
                                    INNER JOIN tu_form_detail fd ON sf.form_id = fd.form_id
                                WHERE
                                    sf.form_type_cd = '3852'
                            ) details ON pp_tobacco.permit_id = details.permit_id
                                         AND pp_tobacco.period_id = details.period_id
                                         AND pp_tobacco.tobacco_class_id = details.tobacco_class_id
                    ) taxes ON taxes.permit_id = p.permit_id
                WHERE
                    p.permit_type_cd = 'MANU'
                            -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
                AND (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------

                GROUP BY
                    c.company_id,
                    c.ein,
                    taxes.tobacco_class_id
            ) tufa
---------------------- Start JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
-- Only cigarette taxe data for now
            LEFT OUTER JOIN (
                SELECT
                    c.company_id,
                    c.ein,
                    tobacco_class_id,
                    ingestvol,
                    ingesttax
                FROM
                    (
                    --Replacement was done here to grab both ingested vol and tax data
                    --Kyle as per 21 March 2019
                        SELECT
                            ein_num,
                            tobacco_class_id,
                            SUM(ingestvol) ingestvol,
                            ingesttax
                        FROM
                            (
                                SELECT
                                    nvl(vol.ein_num,tax.ein_num) ein_num,
                                    nvl(vol.tobacco_class_id,tax.tobacco_class_id) tobacco_class_id,
                                    nvl(vol.ingestvol,0) ingestvol,
                                    nvl(tax.ingesttax,0) ingesttax
                                FROM
                                    (
-- INGESTED VOLUME DATA
-- KYLE 3/28/2019: Added UNIQUE since duplicate records were being pulled for Volume
                                        SELECT UNIQUE
                                            tc.ein_num,
                                            tc.ttb_company_id,
                                            ttc.tobacco_class_id,
                                            ttc.parent_class_id,
                                            nvl(tvt.pounds,0) AS ingestvol,
                                            tvt.permit_num
                                        FROM
                                            tu_ttb_company tc
                                            LEFT JOIN tu_ttb_permit tp ON tc.ttb_company_id = tp.ttb_company_id
                                            LEFT JOIN tu_ttb_volume_taxes_per_tt tvt ON tvt.ein = tc.ein_num
                                                                                        AND tc.fiscal_yr = tvt.fiscal_yr
                                            LEFT JOIN tu_tobacco_class ttc ON ttc.tobacco_class_nm = tvt.product
                                        WHERE
                                            tc.fiscal_yr =v_year
                                            AND tvt.qtr =v_qtr
                                            AND tvt.permit_num IN (
                                                SELECT
                                                    p.permit_num
                                                FROM
                                                    tu_permit p
                                                    JOIN tu_company c ON p.company_id = c.company_id
                                                WHERE
                                                    c.ein = tc.ein_num
                                            )
                                    ) vol
            -- Only care about tax data regardles in there is volume data to go with it
                                    RIGHT JOIN (
-- INGESTED TAX DATA
                                        SELECT
                                            ttbcmpy.ein_num,
                                            ttc.tobacco_class_id,
                                            NULL AS parent_class_id,
                                            SUM(nvl(ttb_taxes_paid,0) ) AS ingesttax,
                                            ttbpermit.permit_num
                                        FROM
                                            tu_ttb_company ttbcmpy
                                            INNER JOIN tu_ttb_permit ttbpermit ON ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id
                                            INNER JOIN tu_ttb_annual_tax ttbannualtx ON ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id
                                            INNER JOIN tu_tobacco_class ttc ON ttbannualtx.tobacco_class_id = ttc.tobacco_class_id
                                        WHERE
                                            ttc.tobacco_class_id <> 8
                                            AND ttbcmpy.fiscal_yr =v_year
                                            AND ttbannualtx.ttb_calendar_qtr =v_qtr
                                            AND ttbpermit.permit_num IN (
                                                SELECT
                                                    p.permit_num
                                                FROM
                                                    tu_permit p
                                                    JOIN tu_company c ON p.company_id = c.company_id
                                                WHERE
                                                    c.ein = ttbcmpy.ein_num
                                            )
                                        GROUP BY
                                            ttbcmpy.ein_num,
                                            ttc.tobacco_class_id,
                                            ttbpermit.permit_num
                                    ) tax ON vol.ein_num = tax.ein_num
                                             AND vol.permit_num = tax.permit_num
                                             AND CASE
                                        WHEN vol.parent_class_id IS NOT NULL THEN vol.parent_class_id
                                        ELSE vol.tobacco_class_id
                                    END = tax.tobacco_class_id
                            )
                        GROUP BY
                            ein_num,
                            tobacco_class_id,
                            ingesttax
                    ) ttbingested
                    INNER JOIN tu_company c ON ttbingested.ein_num = c.ein
                    where c.company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

            ) ingest ON tufa.company_id = ingest.company_id
                        AND tufa.tobacco_class_id = ingest.tobacco_class_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    ttb_company_id                AS company_id,
                    tc.tobacco_class_id,
                    amended_total_tax             AS amendtax,
                    ca.acceptance_flag,
                    CASE
                        WHEN tc.tobacco_class_id = 7
                             OR tc.tobacco_class_id = 8 THEN amended_total_volume * 1000
                        ELSE amended_total_volume
                    END AS amendvol,
                    accepted_ingested_total_tax   AS accept_ingest_tax
                FROM
                    tu_ttb_amendment ca,
                    tu_tobacco_class tc
                WHERE
                    ca.tobacco_class_id = tc.tobacco_class_id
                    AND ca.fiscal_yr =v_year
                    AND ca.qtr =v_qtr
                    AND tc.tobacco_class_nm <> 'Cigars'
                    AND ttb_company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
            ) amend ON tufa.company_id = amend.company_id
                       AND tufa.tobacco_class_id = amend.tobacco_class_id
    )
           ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
      WHERE (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,1,instr(tcads.tobacco_class_nm,'/')-1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='MANU'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
       and (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,instr(tcads.tobacco_class_nm,'/')+1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='MANU'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------

    ) combined
    INNER JOIN tu_tobacco_class tc on tc.tobacco_class_id = combined.tobacco_class_id
    WHERE tc.tobacco_class_nm = 'Pipe'
    GROUP BY tc.tobacco_class_id, tc.tobacco_class_nm;
  EXCEPTION
    WHEN no_data_found THEN v_period_tot_pipe_taxes := 0;
END;

--

  BEGIN
    SELECT SUM(TOTTAX) as TOTTAX
      into v_period_tot_chew_taxes
    FROM
    (
      SELECT *
      FROM

      (SELECT
        tufa.company_id,
        tufa.ein,
        tufa.tobacco_class_id,
        'IMPT' as permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            /*CASE ( amend.acceptance_flag )
                WHEN 'I'   THEN ingest.ingesttax
                WHEN 'N'   THEN amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,
            CASE (amend.acceptance_flag)
                WHEN 'I'   THEN ingest.ingestvol
                WHEN 'N'   THEN amend.amendvol
                ELSE tufa.tufavol
            END AS totvol
           */
                       CASE ( amend.acceptance_flag||amend.review_flag )
                WHEN 'IY'   THEN amend.amendtax

                WHEN 'IN'   THEN ingest.ingesttax
                WHEN 'NN' THEN  amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,

            CASE ( amend.acceptance_flag||amend.review_flag )
                WHEN 'IY'   THEN amend.amendvol

                WHEN 'IN'   THEN ingest.ingestvol
                WHEN 'NN' THEN  amend.amendvol
                ELSE tufa.tufavol
                END AS totvol

---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      FROM
      (
        SELECT c.company_id,c.ein, taxes.tobacco_class_id, SUM(taxes.qty) as TUFAVOL, SUM(taxes.tax) as TUFATAX
        FROM tu_company c
        INNER JOIN tu_permit p on c.company_id = p.company_id
        INNER JOIN
        (
          SELECT pp_tobacco.permit_id, pp_tobacco.tobacco_class_id, NVL(details.removal_qty, 0) as qty, NVL(details.taxes_paid, 0) as tax
          FROM
          (
            SELECT permit_periods.permit_id, permit_periods.period_id, tc.tobacco_class_id
            FROM
              (SELECT pp.permit_id, pp.period_id
              FROM tu_permit_period pp
              INNER JOIN tu_rpt_period rp on pp.period_id = rp.period_id
              WHERE rp.fiscal_yr=v_year
              AND rp.quarter=v_qtr) permit_periods
            INNER JOIN tu_tobacco_class tc on tc.class_type_cd <> 'SPTP' AND tc.tobacco_class_nm <> 'Cigars'
          ) pp_tobacco
          LEFT OUTER JOIN
          (
            SELECT sf.permit_id, sf.period_id, fd.tobacco_class_id, fd.removal_qty, fd.taxes_paid
            FROM tu_submitted_form sf
            INNER JOIN tu_form_detail fd on sf.form_id = fd.form_id
            where sf.form_type_cd = '3852'
          ) details on
                  pp_tobacco.permit_id = details.permit_id
              AND pp_tobacco.period_id = details.period_id
              AND pp_tobacco.tobacco_class_id = details.tobacco_class_id
        ) taxes on taxes.permit_id = p.permit_id
        WHERE p.permit_type_cd='IMPT'

                -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        AND (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------

        GROUP BY c.company_id,c.ein, taxes.tobacco_class_id
      ) tufa
---------------------- Start JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    c1.ein,
                    c1.company_id,
                    SUM(nvl(cbpingested.ingest_vol,0) ) AS ingestvol,
                    SUM(nvl(cbpingested.cbp_taxes_paid,0) ) AS ingesttax,
                    cbpingested.tobacco_class_id
                FROM
                    (
                        SELECT
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            SUM(nvl(cbpentry.estimated_tax,0) ) AS cbp_taxes_paid,
                            SUM(CASE
                                WHEN ttc.tobacco_class_id = 7 THEN cbpentry.qty_removed * 1000
                                ELSE cbpentry.qty_removed * 2.204
                            END) AS ingest_vol,
                            ttc.tobacco_class_nm,
                            CASE
                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                                ELSE '1-4'
                            END AS fiscal_qtr,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id   AS tobacco_class_id
                        FROM
                            tu_cbp_importer cbpdetail,
                            tu_cbp_entry cbpentry,
                            tu_tobacco_class ttc
                        WHERE
                            ( cbpentry.excluded_flag IS NULL
                              OR cbpentry.excluded_flag <> 'Y' )
                            AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
                            AND ( ttc.tobacco_class_id <> 14
                                  OR ttc.tobacco_class_nm <> 'Non-Taxable' )
                            AND ( ttc.tobacco_class_id <> 8
                                  OR ttc.tobacco_class_nm <> 'Cigars' )
                            AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
                            AND cbpdetail.association_type_cd IS NOT NULL
                            AND cbpdetail.fiscal_yr =v_year
                            AND cbpentry.fiscal_qtr =v_qtr
                        GROUP BY
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            ttc.tobacco_class_nm,
                            CASE
                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                                ELSE '1-4'
                            END,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id,
                            cbpentry.uom_cd
                    ) cbpingested,
                    tu_company c1
                WHERE
                    ( cbpingested.importer_ein = c1.ein
                      AND cbpingested.association_type_cd = 'IMPT'
                      OR cbpingested.consignee_ein = c1.ein
                      AND cbpingested.association_type_cd = 'CONS' )
                    AND cbpingested.cbp_taxes_paid != 0
                    AND c1.company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

                GROUP BY
                    c1.company_id,
                    c1.ein,
                    cbpingested.tobacco_class_id
            ) ingest ON tufa.company_id = ingest.company_id
                        AND tufa.ein = ingest.ein
                        AND tufa.tobacco_class_id = ingest.tobacco_class_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      LEFT OUTER JOIN (
        SELECT CBP_COMPANY_ID as company_id, tc.tobacco_class_id, AMENDED_TOTAL_TAX as AMENDTAX, AMENDED_TOTAL_VOLUME as AMENDVOL,
        ca.acceptance_flag,ca.review_flag
        FROM tu_cbp_amendment ca, tu_tobacco_class tc
        WHERE ca.tobacco_class_id = tc.tobacco_class_id
        and ca.fiscal_yr=v_year
        and ca.qtr=v_qtr
        and tc.tobacco_class_nm <> 'Cigars'
        AND cbp_company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

      ) amend on tufa.company_id = amend.company_id and tufa.tobacco_class_id = amend.tobacco_class_id

      )
     ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
       WHERE (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,1,instr(tcads.tobacco_class_nm,'/')-1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='IMPT'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
       and (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,instr(tcads.tobacco_class_nm,'/')+1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='IMPT'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------

      UNION All
     ----------------------------------------BEGIN INGESTED ONLY added on 16 oct 2018-----------------------------------------------------

Select * from (
       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_CLASS_NM) tobacco_class_id,
      -- tcads.TOBACCO_CLASS_NM,
       tcads.PERMIT_TYPE,
       ABS(tcads.Delta),
        null as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year   and tcads.fiscal_qtr=to_char(v_qtr)
     and tobacco_sub_type_1  is  null
     and  tobacco_sub_type_2 is null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
           and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------
     UNION

       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_SUB_TYPE_1) tobacco_class_id,
       --TOBACCO_SUB_TYPE_1,
       tcads.PERMIT_TYPE,
       tobacco_sub_type_1_amndtx ,
       TOBACCO_SUB_TYPE_1_POUNDS as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year   and tcads.fiscal_qtr=to_char(v_qtr)
     and tobacco_sub_type_1  is  not null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
           and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------
     UNION

       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_SUB_TYPE_2) tobacco_class_id,
       --tcads.TOBACCO_SUB_TYPE_2,
       tcads.PERMIT_TYPE,
      tobacco_sub_type_2_amndtx ,
      TOBACCO_SUB_TYPE_2_POUNDS as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year    and tcads.fiscal_qtr=to_char(v_qtr)
     and  tobacco_sub_type_2 is not null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
           and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------
) INGESTED_ONLY


    ----------------------------------------END INGESTED ONLY added on 16 oct 2018-----------------------------------------------------

      UNION

      SELECT *
      FROM
      (
        SELECT
            tufa.company_id,
            tufa.ein,
            tufa.tobacco_class_id,
            'MANU' AS permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            CASE ( amend.acceptance_flag )

                WHEN 'I'   THEN nvl(nvl(amend.accept_ingest_tax,ingest.ingesttax),0)
                WHEN 'N'   THEN amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,
            CASE ( amend.acceptance_flag )
                WHEN 'I'   THEN nvl(ingest.ingestvol,0)
                WHEN 'N'   THEN amend.amendvol
                ELSE tufa.tufavol
            END AS totvol
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
        FROM
            (
                SELECT
                    c.company_id,
                    c.ein,
                    taxes.tobacco_class_id,
                    SUM(taxes.qty) AS tufavol,
                    SUM(taxes.tax) AS tufatax
                FROM
                    tu_company c
                    INNER JOIN tu_permit p ON c.company_id = p.company_id
                    INNER JOIN (
                        SELECT
                            pp_tobacco.permit_id,
                            pp_tobacco.tobacco_class_id,
                            nvl(details.removal_qty,0) AS qty,
                            nvl(details.taxes_paid,0) AS tax
                        FROM
                            (
                                SELECT
                                    permit_periods.permit_id,
                                    permit_periods.period_id,
                                    tc.tobacco_class_id
                                FROM
                                    (
                                        SELECT
                                            pp.permit_id,
                                            pp.period_id
                                        FROM
                                            tu_permit_period pp
                                            INNER JOIN tu_rpt_period rp ON pp.period_id = rp.period_id
                                        WHERE
                                            rp.fiscal_yr =v_year
                                            AND rp.quarter =v_qtr
                                    ) permit_periods
                                    INNER JOIN tu_tobacco_class tc ON tc.class_type_cd <> 'SPTP'
                                                                      AND tc.tobacco_class_nm <> 'Cigars'
                            ) pp_tobacco
                            LEFT OUTER JOIN (
                                SELECT
                                    sf.permit_id,
                                    sf.period_id,
                                    fd.tobacco_class_id,
                                    fd.removal_qty,
                                    fd.taxes_paid
                                FROM
                                    tu_submitted_form sf
                                    INNER JOIN tu_form_detail fd ON sf.form_id = fd.form_id
                                WHERE
                                    sf.form_type_cd = '3852'
                            ) details ON pp_tobacco.permit_id = details.permit_id
                                         AND pp_tobacco.period_id = details.period_id
                                         AND pp_tobacco.tobacco_class_id = details.tobacco_class_id
                    ) taxes ON taxes.permit_id = p.permit_id
                WHERE
                    p.permit_type_cd = 'MANU'
                            -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
                AND (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------

                GROUP BY
                    c.company_id,
                    c.ein,
                    taxes.tobacco_class_id
            ) tufa
---------------------- Start JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
-- Only cigarette taxe data for now
            LEFT OUTER JOIN (
                SELECT
                    c.company_id,
                    c.ein,
                    tobacco_class_id,
                    ingestvol,
                    ingesttax
                FROM
                    (
                    --Replacement was done here to grab both ingested vol and tax data
                    --Kyle as per 21 March 2019
                        SELECT
                            ein_num,
                            tobacco_class_id,
                            SUM(ingestvol) ingestvol,
                            ingesttax
                        FROM
                            (
                                SELECT
                                    nvl(vol.ein_num,tax.ein_num) ein_num,
                                    nvl(vol.tobacco_class_id,tax.tobacco_class_id) tobacco_class_id,
                                    nvl(vol.ingestvol,0) ingestvol,
                                    nvl(tax.ingesttax,0) ingesttax
                                FROM
                                    (
-- INGESTED VOLUME DATA
-- KYLE 3/28/2019: Added UNIQUE since duplicate records were being pulled for Volume
                                        SELECT UNIQUE
                                            tc.ein_num,
                                            tc.ttb_company_id,
                                            ttc.tobacco_class_id,
                                            ttc.parent_class_id,
                                            nvl(tvt.pounds,0) AS ingestvol,
                                            tvt.permit_num
                                        FROM
                                            tu_ttb_company tc
                                            LEFT JOIN tu_ttb_permit tp ON tc.ttb_company_id = tp.ttb_company_id
                                            LEFT JOIN tu_ttb_volume_taxes_per_tt tvt ON tvt.ein = tc.ein_num
                                                                                        AND tc.fiscal_yr = tvt.fiscal_yr
                                            LEFT JOIN tu_tobacco_class ttc ON ttc.tobacco_class_nm = tvt.product
                                        WHERE
                                            tc.fiscal_yr =v_year
                                            AND tvt.qtr =v_qtr
                                            AND tvt.permit_num IN (
                                                SELECT
                                                    p.permit_num
                                                FROM
                                                    tu_permit p
                                                    JOIN tu_company c ON p.company_id = c.company_id
                                                WHERE
                                                    c.ein = tc.ein_num
                                            )
                                    ) vol
            -- Only care about tax data regardles in there is volume data to go with it
                                    RIGHT JOIN (
-- INGESTED TAX DATA
                                        SELECT
                                            ttbcmpy.ein_num,
                                            ttc.tobacco_class_id,
                                            NULL AS parent_class_id,
                                            SUM(nvl(ttb_taxes_paid,0) ) AS ingesttax,
                                            ttbpermit.permit_num
                                        FROM
                                            tu_ttb_company ttbcmpy
                                            INNER JOIN tu_ttb_permit ttbpermit ON ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id
                                            INNER JOIN tu_ttb_annual_tax ttbannualtx ON ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id
                                            INNER JOIN tu_tobacco_class ttc ON ttbannualtx.tobacco_class_id = ttc.tobacco_class_id
                                        WHERE
                                            ttc.tobacco_class_id <> 8
                                            AND ttbcmpy.fiscal_yr =v_year
                                            AND ttbannualtx.ttb_calendar_qtr =v_qtr
                                            AND ttbpermit.permit_num IN (
                                                SELECT
                                                    p.permit_num
                                                FROM
                                                    tu_permit p
                                                    JOIN tu_company c ON p.company_id = c.company_id
                                                WHERE
                                                    c.ein = ttbcmpy.ein_num
                                            )
                                        GROUP BY
                                            ttbcmpy.ein_num,
                                            ttc.tobacco_class_id,
                                            ttbpermit.permit_num
                                    ) tax ON vol.ein_num = tax.ein_num
                                             AND vol.permit_num = tax.permit_num
                                             AND CASE
                                        WHEN vol.parent_class_id IS NOT NULL THEN vol.parent_class_id
                                        ELSE vol.tobacco_class_id
                                    END = tax.tobacco_class_id
                            )
                        GROUP BY
                            ein_num,
                            tobacco_class_id,
                            ingesttax
                    ) ttbingested
                    INNER JOIN tu_company c ON ttbingested.ein_num = c.ein
                    where c.company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

            ) ingest ON tufa.company_id = ingest.company_id
                        AND tufa.tobacco_class_id = ingest.tobacco_class_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    ttb_company_id                AS company_id,
                    tc.tobacco_class_id,
                    amended_total_tax             AS amendtax,
                    ca.acceptance_flag,
                    CASE
                        WHEN tc.tobacco_class_id = 7
                             OR tc.tobacco_class_id = 8 THEN amended_total_volume * 1000
                        ELSE amended_total_volume
                    END AS amendvol,
                    accepted_ingested_total_tax   AS accept_ingest_tax
                FROM
                    tu_ttb_amendment ca,
                    tu_tobacco_class tc
                WHERE
                    ca.tobacco_class_id = tc.tobacco_class_id
                    AND ca.fiscal_yr =v_year
                    AND ca.qtr =v_qtr
                    AND tc.tobacco_class_nm <> 'Cigars'
                    AND TTB_company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

            ) amend ON tufa.company_id = amend.company_id
                       AND tufa.tobacco_class_id = amend.tobacco_class_id
    )
     ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
      WHERE (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,1,instr(tcads.tobacco_class_nm,'/')-1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='MANU'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
       and (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,instr(tcads.tobacco_class_nm,'/')+1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='MANU'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------


    ) combined
    INNER JOIN tu_tobacco_class tc on tc.tobacco_class_id = combined.tobacco_class_id
    WHERE tc.tobacco_class_nm = 'Chew'
    GROUP BY tc.tobacco_class_id, tc.tobacco_class_nm;
    EXCEPTION
      WHEN no_data_found THEN v_period_tot_chew_taxes := 0;
  END;

--

  BEGIN
    SELECT SUM(TOTTAX) as TOTTAX
      into v_period_tot_snuff_taxes
    FROM
    (
        SELECT *
        FROM
        (
        SELECT
        tufa.company_id,
        tufa.ein,
        tufa.tobacco_class_id,
        'IMPT' as permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            /*CASE ( amend.acceptance_flag )
                WHEN 'I'   THEN ingest.ingesttax
                WHEN 'N'   THEN amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,
            CASE (amend.acceptance_flag)
                WHEN 'I'   THEN ingest.ingestvol
                WHEN 'N'   THEN amend.amendvol
                ELSE tufa.tufavol
            END AS totvol*/
           CASE ( amend.acceptance_flag||amend.review_flag )
                WHEN 'IY'   THEN amend.amendtax

                WHEN 'IN'   THEN ingest.ingesttax
                WHEN 'NN' THEN  amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,

            CASE ( amend.acceptance_flag||amend.review_flag )
                WHEN 'IY'   THEN amend.amendvol

                WHEN 'IN'   THEN ingest.ingestvol
                WHEN 'NN' THEN  amend.amendvol
                ELSE tufa.tufavol
                END AS totvol

---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      FROM
      (
        SELECT c.company_id,c.ein, taxes.tobacco_class_id, SUM(taxes.qty) as TUFAVOL, SUM(taxes.tax) as TUFATAX
        FROM tu_company c
        INNER JOIN tu_permit p on c.company_id = p.company_id
        INNER JOIN
        (
          SELECT pp_tobacco.permit_id, pp_tobacco.tobacco_class_id, NVL(details.removal_qty, 0) as qty, NVL(details.taxes_paid, 0) as tax
          FROM
          (
            SELECT permit_periods.permit_id, permit_periods.period_id, tc.tobacco_class_id
            FROM
              (SELECT pp.permit_id, pp.period_id
              FROM tu_permit_period pp
              INNER JOIN tu_rpt_period rp on pp.period_id = rp.period_id
              WHERE rp.fiscal_yr=v_year
              AND rp.quarter=v_qtr) permit_periods
            INNER JOIN tu_tobacco_class tc on tc.class_type_cd <> 'SPTP' AND tc.tobacco_class_nm <> 'Cigars'
          ) pp_tobacco
          LEFT OUTER JOIN
          (
            SELECT sf.permit_id, sf.period_id, fd.tobacco_class_id, fd.removal_qty, fd.taxes_paid
            FROM tu_submitted_form sf
            INNER JOIN tu_form_detail fd on sf.form_id = fd.form_id
            where sf.form_type_cd = '3852'
          ) details on
                  pp_tobacco.permit_id = details.permit_id
              AND pp_tobacco.period_id = details.period_id
              AND pp_tobacco.tobacco_class_id = details.tobacco_class_id
        ) taxes on taxes.permit_id = p.permit_id
        WHERE p.permit_type_cd='IMPT'
                -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        AND (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------

        GROUP BY c.company_id,c.ein, taxes.tobacco_class_id
      ) tufa
---------------------- Start JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    c1.ein,
                    c1.company_id,
                    SUM(nvl(cbpingested.ingest_vol,0) ) AS ingestvol,
                    SUM(nvl(cbpingested.cbp_taxes_paid,0) ) AS ingesttax,
                    cbpingested.tobacco_class_id
                FROM
                    (
                        SELECT
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            SUM(nvl(cbpentry.estimated_tax,0) ) AS cbp_taxes_paid,
                            SUM(CASE
                                WHEN ttc.tobacco_class_id = 7 THEN cbpentry.qty_removed * 1000
                                ELSE cbpentry.qty_removed * 2.204
                            END) AS ingest_vol,
                            ttc.tobacco_class_nm,
                            CASE
                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                                ELSE '1-4'
                            END AS fiscal_qtr,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id   AS tobacco_class_id
                        FROM
                            tu_cbp_importer cbpdetail,
                            tu_cbp_entry cbpentry,
                            tu_tobacco_class ttc
                        WHERE
                            ( cbpentry.excluded_flag IS NULL
                              OR cbpentry.excluded_flag <> 'Y' )
                            AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
                            AND ( ttc.tobacco_class_id <> 14
                                  OR ttc.tobacco_class_nm <> 'Non-Taxable' )
                            AND ( ttc.tobacco_class_id <> 8
                                  OR ttc.tobacco_class_nm <> 'Cigars' )
                            AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
                            AND cbpdetail.association_type_cd IS NOT NULL
                            AND cbpdetail.fiscal_yr =v_year
                            AND cbpentry.fiscal_qtr =v_qtr
                        GROUP BY
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            ttc.tobacco_class_nm,
                            CASE
                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                                ELSE '1-4'
                            END,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id,
                            cbpentry.uom_cd
                    ) cbpingested,
                    tu_company c1
                WHERE
                    ( cbpingested.importer_ein = c1.ein
                      AND cbpingested.association_type_cd = 'IMPT'
                      OR cbpingested.consignee_ein = c1.ein
                      AND cbpingested.association_type_cd = 'CONS' )
                    AND cbpingested.cbp_taxes_paid != 0
                   AND c1.company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

                GROUP BY
                    c1.company_id,
                    c1.ein,
                    cbpingested.tobacco_class_id
            ) ingest ON tufa.company_id = ingest.company_id
                        AND tufa.ein = ingest.ein
                        AND tufa.tobacco_class_id = ingest.tobacco_class_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      LEFT OUTER JOIN (
        SELECT CBP_COMPANY_ID as company_id, tc.tobacco_class_id, AMENDED_TOTAL_TAX as AMENDTAX, AMENDED_TOTAL_VOLUME as AMENDVOL,
        ca.acceptance_flag,ca.review_flag
        FROM tu_cbp_amendment ca, tu_tobacco_class tc
        WHERE ca.tobacco_class_id = tc.tobacco_class_id
        and ca.fiscal_yr=v_year
        and ca.qtr=v_qtr
        and tc.tobacco_class_nm <> 'Cigars'
        and cbp_company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

      ) amend on tufa.company_id = amend.company_id and tufa.tobacco_class_id = amend.tobacco_class_id
      )
     ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
      WHERE (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,1,instr(tcads.tobacco_class_nm,'/')-1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='IMPT'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
       and (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,instr(tcads.tobacco_class_nm,'/')+1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='IMPT'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------



      UNION All
     ----------------------------------------BEGIN INGESTED ONLY added on 16 oct 2018-----------------------------------------------------

Select * from (
       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_CLASS_NM) tobacco_class_id,
      -- tcads.TOBACCO_CLASS_NM,
       tcads.PERMIT_TYPE,
       ABS(tcads.Delta),
        null as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year   and tcads.fiscal_qtr=to_char(v_qtr)
     and tobacco_sub_type_1  is  null
     and  tobacco_sub_type_2 is null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
           and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------
     UNION

       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_SUB_TYPE_1) tobacco_class_id,
       --TOBACCO_SUB_TYPE_1,
       tcads.PERMIT_TYPE,
       tobacco_sub_type_1_amndtx ,
       TOBACCO_SUB_TYPE_1_POUNDS as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year   and tcads.fiscal_qtr=to_char(v_qtr)
     and tobacco_sub_type_1  is  not null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
           and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------
     UNION

       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_SUB_TYPE_2) tobacco_class_id,
       --tcads.TOBACCO_SUB_TYPE_2,
       tcads.PERMIT_TYPE,
      tobacco_sub_type_2_amndtx ,
      TOBACCO_SUB_TYPE_2_POUNDS as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year    and tcads.fiscal_qtr=to_char(v_qtr)
     and  tobacco_sub_type_2 is not null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
           and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------     
     ) INGESTED_ONLY


    ----------------------------------------END INGESTED ONLY added on 16 oct 2018-----------------------------------------------------
      UNION

       SELECT *
       FROM
       (
        SELECT
            tufa.company_id,
            tufa.ein,
            tufa.tobacco_class_id,
            'MANU' AS permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            CASE ( amend.acceptance_flag )

                WHEN 'I'   THEN nvl(nvl(amend.accept_ingest_tax,ingest.ingesttax),0)
                WHEN 'N'   THEN amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,
            CASE ( amend.acceptance_flag )
                WHEN 'I'   THEN nvl(ingest.ingestvol,0)
                WHEN 'N'   THEN amend.amendvol
                ELSE tufa.tufavol
            END AS totvol
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
        FROM
            (
                SELECT
                    c.company_id,
                    c.ein,
                    taxes.tobacco_class_id,
                    SUM(taxes.qty) AS tufavol,
                    SUM(taxes.tax) AS tufatax
                FROM
                    tu_company c
                    INNER JOIN tu_permit p ON c.company_id = p.company_id
                    INNER JOIN (
                        SELECT
                            pp_tobacco.permit_id,
                            pp_tobacco.tobacco_class_id,
                            nvl(details.removal_qty,0) AS qty,
                            nvl(details.taxes_paid,0) AS tax
                        FROM
                            (
                                SELECT
                                    permit_periods.permit_id,
                                    permit_periods.period_id,
                                    tc.tobacco_class_id
                                FROM
                                    (
                                        SELECT
                                            pp.permit_id,
                                            pp.period_id
                                        FROM
                                            tu_permit_period pp
                                            INNER JOIN tu_rpt_period rp ON pp.period_id = rp.period_id
                                        WHERE
                                            rp.fiscal_yr =v_year
                                            AND rp.quarter =v_qtr
                                    ) permit_periods
                                    INNER JOIN tu_tobacco_class tc ON tc.class_type_cd <> 'SPTP'
                                                                      AND tc.tobacco_class_nm <> 'Cigars'
                            ) pp_tobacco
                            LEFT OUTER JOIN (
                                SELECT
                                    sf.permit_id,
                                    sf.period_id,
                                    fd.tobacco_class_id,
                                    fd.removal_qty,
                                    fd.taxes_paid
                                FROM
                                    tu_submitted_form sf
                                    INNER JOIN tu_form_detail fd ON sf.form_id = fd.form_id
                                WHERE
                                    sf.form_type_cd = '3852'
                            ) details ON pp_tobacco.permit_id = details.permit_id
                                         AND pp_tobacco.period_id = details.period_id
                                         AND pp_tobacco.tobacco_class_id = details.tobacco_class_id
                    ) taxes ON taxes.permit_id = p.permit_id
                WHERE
                    p.permit_type_cd = 'MANU'
        -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
               AND (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------
                    
                GROUP BY
                    c.company_id,
                    c.ein,
                    taxes.tobacco_class_id
            ) tufa
---------------------- Start JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
-- Only cigarette taxe data for now
            LEFT OUTER JOIN (
                SELECT
                    c.company_id,
                    c.ein,
                    tobacco_class_id,
                    ingestvol,
                    ingesttax
                FROM
                    (
                    --Replacement was done here to grab both ingested vol and tax data
                    --Kyle as per 21 March 2019
                        SELECT
                            ein_num,
                            tobacco_class_id,
                            SUM(ingestvol) ingestvol,
                            ingesttax
                        FROM
                            (
                                SELECT
                                    nvl(vol.ein_num,tax.ein_num) ein_num,
                                    nvl(vol.tobacco_class_id,tax.tobacco_class_id) tobacco_class_id,
                                    nvl(vol.ingestvol,0) ingestvol,
                                    nvl(tax.ingesttax,0) ingesttax
                                FROM
                                    (
-- INGESTED VOLUME DATA
-- KYLE 3/28/2019: Added UNIQUE since duplicate records were being pulled for Volume
                                        SELECT UNIQUE
                                            tc.ein_num,
                                            tc.ttb_company_id,
                                            ttc.tobacco_class_id,
                                            ttc.parent_class_id,
                                            nvl(tvt.pounds,0) AS ingestvol,
                                            tvt.permit_num
                                        FROM
                                            tu_ttb_company tc
                                            LEFT JOIN tu_ttb_permit tp ON tc.ttb_company_id = tp.ttb_company_id
                                            LEFT JOIN tu_ttb_volume_taxes_per_tt tvt ON tvt.ein = tc.ein_num
                                                                                        AND tc.fiscal_yr = tvt.fiscal_yr
                                            LEFT JOIN tu_tobacco_class ttc ON ttc.tobacco_class_nm = tvt.product
                                        WHERE
                                            tc.fiscal_yr =v_year
                                            AND tvt.qtr =v_qtr
                                            AND tvt.permit_num IN (
                                                SELECT
                                                    p.permit_num
                                                FROM
                                                    tu_permit p
                                                    JOIN tu_company c ON p.company_id = c.company_id
                                                WHERE
                                                    c.ein = tc.ein_num
                                            )
                                    ) vol
            -- Only care about tax data regardles in there is volume data to go with it
                                    RIGHT JOIN (
-- INGESTED TAX DATA
                                        SELECT
                                            ttbcmpy.ein_num,
                                            ttc.tobacco_class_id,
                                            NULL AS parent_class_id,
                                            SUM(nvl(ttb_taxes_paid,0) ) AS ingesttax,
                                            ttbpermit.permit_num
                                        FROM
                                            tu_ttb_company ttbcmpy
                                            INNER JOIN tu_ttb_permit ttbpermit ON ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id
                                            INNER JOIN tu_ttb_annual_tax ttbannualtx ON ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id
                                            INNER JOIN tu_tobacco_class ttc ON ttbannualtx.tobacco_class_id = ttc.tobacco_class_id
                                        WHERE
                                            ttc.tobacco_class_id <> 8
                                            AND ttbcmpy.fiscal_yr =v_year
                                            AND ttbannualtx.ttb_calendar_qtr =v_qtr
                                            AND ttbpermit.permit_num IN (
                                                SELECT
                                                    p.permit_num
                                                FROM
                                                    tu_permit p
                                                    JOIN tu_company c ON p.company_id = c.company_id
                                                WHERE
                                                    c.ein = ttbcmpy.ein_num
                                            )
                                        GROUP BY
                                            ttbcmpy.ein_num,
                                            ttc.tobacco_class_id,
                                            ttbpermit.permit_num
                                    ) tax ON vol.ein_num = tax.ein_num
                                             AND vol.permit_num = tax.permit_num
                                             AND CASE
                                        WHEN vol.parent_class_id IS NOT NULL THEN vol.parent_class_id
                                        ELSE vol.tobacco_class_id
                                    END = tax.tobacco_class_id
                            )
                        GROUP BY
                            ein_num,
                            tobacco_class_id,
                            ingesttax
                    ) ttbingested
                    INNER JOIN tu_company c ON ttbingested.ein_num = c.ein
                     where c.company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

            ) ingest ON tufa.company_id = ingest.company_id
                        AND tufa.tobacco_class_id = ingest.tobacco_class_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    ttb_company_id                AS company_id,
                    tc.tobacco_class_id,
                    amended_total_tax             AS amendtax,
                    ca.acceptance_flag,
                    CASE
                        WHEN tc.tobacco_class_id = 7
                             OR tc.tobacco_class_id = 8 THEN amended_total_volume * 1000
                        ELSE amended_total_volume
                    END AS amendvol,
                    accepted_ingested_total_tax   AS accept_ingest_tax
                FROM
                    tu_ttb_amendment ca,
                    tu_tobacco_class tc
                WHERE
                    ca.tobacco_class_id = tc.tobacco_class_id
                    AND ca.fiscal_yr =v_year
                    AND ca.qtr =v_qtr
                    AND tc.tobacco_class_nm <> 'Cigars'
                    and TTB_company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

            ) amend ON tufa.company_id = amend.company_id
                       AND tufa.tobacco_class_id = amend.tobacco_class_id
    )
     ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
       WHERE (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,1,instr(tcads.tobacco_class_nm,'/')-1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='MANU'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
       and (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,instr(tcads.tobacco_class_nm,'/')+1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='MANU'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------


    ) combined
    INNER JOIN tu_tobacco_class tc on tc.tobacco_class_id = combined.tobacco_class_id
    WHERE tc.tobacco_class_nm = 'Snuff'
    GROUP BY tc.tobacco_class_id, tc.tobacco_class_nm;
    EXCEPTION
      WHEN no_data_found THEN v_period_tot_snuff_taxes := 0;
  END;

--

  BEGIN
    SELECT SUM(TOTTAX) as TOTTAX
      into v_period_tot_cigarette_taxes
    FROM
    (
      SELECT *
      FROM
      (
      SELECT
        tufa.company_id,tufa.ein,
        tufa.tobacco_class_id,
        'IMPT' as permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
                        CASE ( amend.acceptance_flag||amend.review_flag )
                WHEN 'IY'   THEN amend.amendtax

                WHEN 'IN'   THEN ingest.ingesttax
                WHEN 'NN' THEN  amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,

            CASE ( amend.acceptance_flag||amend.review_flag )
                WHEN 'IY'   THEN amend.amendvol

                WHEN 'IN'   THEN ingest.ingestvol
                WHEN 'NN' THEN  amend.amendvol
                ELSE tufa.tufavol
                END AS totvol

/*CASE ( amend.acceptance_flag )
                WHEN 'I'   THEN ingest.ingesttax
                WHEN 'N'   THEN amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,
            CASE (amend.acceptance_flag)
                WHEN 'I'   THEN ingest.ingestvol
                WHEN 'N'   THEN amend.amendvol
                ELSE tufa.tufavol
            END AS totvol*/
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      FROM
      (
        SELECT c.company_id,c.ein, taxes.tobacco_class_id, SUM(taxes.qty) as TUFAVOL, SUM(taxes.tax) as TUFATAX
        FROM tu_company c
        INNER JOIN tu_permit p on c.company_id = p.company_id
        INNER JOIN
        (
          SELECT pp_tobacco.permit_id, pp_tobacco.tobacco_class_id, NVL(details.removal_qty, 0) as qty, NVL(details.taxes_paid, 0) as tax
          FROM
          (
            SELECT permit_periods.permit_id, permit_periods.period_id, tc.tobacco_class_id
            FROM
              (SELECT pp.permit_id, pp.period_id
              FROM tu_permit_period pp
              INNER JOIN tu_rpt_period rp on pp.period_id = rp.period_id
              WHERE rp.fiscal_yr=v_year
              AND rp.quarter=v_qtr) permit_periods
            INNER JOIN tu_tobacco_class tc on tc.class_type_cd <> 'SPTP' AND tc.tobacco_class_nm <> 'Cigars'
          ) pp_tobacco
          LEFT OUTER JOIN
          (
            SELECT sf.permit_id, sf.period_id, fd.tobacco_class_id, fd.removal_qty, fd.taxes_paid
            FROM tu_submitted_form sf
            INNER JOIN tu_form_detail fd on sf.form_id = fd.form_id
            where sf.form_type_cd = '3852'
          ) details on
                  pp_tobacco.permit_id = details.permit_id
              AND pp_tobacco.period_id = details.period_id
              AND pp_tobacco.tobacco_class_id = details.tobacco_class_id
        ) taxes on taxes.permit_id = p.permit_id
        WHERE p.permit_type_cd='IMPT'
                -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        AND (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------

        GROUP BY c.company_id,c.ein, taxes.tobacco_class_id
      ) tufa
---------------------- Start JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    c1.ein,
                    c1.company_id,
                    SUM(nvl(cbpingested.ingest_vol,0) ) AS ingestvol,
                    SUM(nvl(cbpingested.cbp_taxes_paid,0) ) AS ingesttax,
                    cbpingested.tobacco_class_id
                FROM
                    (
                        SELECT
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            SUM(nvl(cbpentry.estimated_tax,0) ) AS cbp_taxes_paid,
                            SUM(CASE
                                WHEN ttc.tobacco_class_id = 7 THEN cbpentry.qty_removed * 1000
                                ELSE cbpentry.qty_removed * 2.204
                            END) AS ingest_vol,
                            ttc.tobacco_class_nm,
                            CASE
                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                                ELSE '1-4'
                            END AS fiscal_qtr,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id   AS tobacco_class_id
                        FROM
                            tu_cbp_importer cbpdetail,
                            tu_cbp_entry cbpentry,
                            tu_tobacco_class ttc
                        WHERE
                            ( cbpentry.excluded_flag IS NULL
                              OR cbpentry.excluded_flag <> 'Y' )
                            AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
                            AND ( ttc.tobacco_class_id <> 14
                                  OR ttc.tobacco_class_nm <> 'Non-Taxable' )
                            AND ( ttc.tobacco_class_id <> 8
                                  OR ttc.tobacco_class_nm <> 'Cigars' )
                            AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
                            AND cbpdetail.association_type_cd IS NOT NULL
                            AND cbpdetail.fiscal_yr =v_year
                            AND cbpentry.fiscal_qtr =v_qtr
                        GROUP BY
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            ttc.tobacco_class_nm,
                            CASE
                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                                ELSE '1-4'
                            END,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id,
                            cbpentry.uom_cd
                    ) cbpingested,
                    tu_company c1
                WHERE
                    ( cbpingested.importer_ein = c1.ein
                      AND cbpingested.association_type_cd = 'IMPT'
                      OR cbpingested.consignee_ein = c1.ein
                      AND cbpingested.association_type_cd = 'CONS' )
                    AND cbpingested.cbp_taxes_paid != 0
                   AND c1.company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

                GROUP BY
                    c1.company_id,
                    c1.ein,
                    cbpingested.tobacco_class_id
            ) ingest ON tufa.company_id = ingest.company_id
                        AND tufa.ein = ingest.ein
                        AND tufa.tobacco_class_id = ingest.tobacco_class_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      LEFT OUTER JOIN (
        SELECT CBP_COMPANY_ID as company_id, tc.tobacco_class_id, AMENDED_TOTAL_TAX as AMENDTAX, AMENDED_TOTAL_VOLUME as AMENDVOL,
        ca.acceptance_flag,ca.review_flag
        FROM tu_cbp_amendment ca, tu_tobacco_class tc
        WHERE ca.tobacco_class_id = tc.tobacco_class_id
        and ca.fiscal_yr=v_year
        and ca.qtr=v_qtr
        and tc.tobacco_class_nm <> 'Cigars'
        and cbp_company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

      ) amend on tufa.company_id = amend.company_id and tufa.tobacco_class_id = amend.tobacco_class_id
      )
     ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
      WHERE (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,1,instr(tcads.tobacco_class_nm,'/')-1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='IMPT'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
       and (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,instr(tcads.tobacco_class_nm,'/')+1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='IMPT'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------



      UNION All

     ----------------------------------------BEGIN INGESTED ONLY added on 16 oct 2018-----------------------------------------------------

Select * from (
       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_CLASS_NM) tobacco_class_id,
      -- tcads.TOBACCO_CLASS_NM,
       tcads.PERMIT_TYPE,
       ABS(tcads.Delta),
        null as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year   and tcads.fiscal_qtr=to_char(v_qtr)
     and tobacco_sub_type_1  is  null
     and  tobacco_sub_type_2 is null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------
     UNION

       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_SUB_TYPE_1) tobacco_class_id,
       --TOBACCO_SUB_TYPE_1,
       tcads.PERMIT_TYPE,
       tobacco_sub_type_1_amndtx ,
       TOBACCO_SUB_TYPE_1_POUNDS as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year   and tcads.fiscal_qtr=to_char(v_qtr)
     and tobacco_sub_type_1  is  not null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion--------------------                                
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
-------------------------------------------------------------------------------------
     UNION

       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_SUB_TYPE_2) tobacco_class_id,
       --tcads.TOBACCO_SUB_TYPE_2,
       tcads.PERMIT_TYPE,
      tobacco_sub_type_2_amndtx ,
      TOBACCO_SUB_TYPE_2_POUNDS as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year    and tcads.fiscal_qtr=to_char(v_qtr)
     and  tobacco_sub_type_2 is not null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion--------------------                                
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
-------------------------------------------------------------------------------------
) INGESTED_ONLY


    ----------------------------------------END INGESTED ONLY added on 16 oct 2018-----------------------------------------------------

      UNION
      SELECT *
      FROM
      (
        SELECT
            tufa.company_id,
            tufa.ein,
            tufa.tobacco_class_id,
            'MANU' AS permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            CASE ( amend.acceptance_flag )

                WHEN 'I'   THEN nvl(nvl(amend.accept_ingest_tax,ingest.ingesttax),0)
                WHEN 'N'   THEN amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,
            CASE ( amend.acceptance_flag )
                WHEN 'I'   THEN nvl(ingest.ingestvol,0)
                WHEN 'N'   THEN amend.amendvol
                ELSE tufa.tufavol
            END AS totvol
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
        FROM
            (
                SELECT
                    c.company_id,
                    c.ein,
                    taxes.tobacco_class_id,
                    SUM(taxes.qty) AS tufavol,
                    SUM(taxes.tax) AS tufatax
                FROM
                    tu_company c
                    INNER JOIN tu_permit p ON c.company_id = p.company_id
                    INNER JOIN (
                        SELECT
                            pp_tobacco.permit_id,
                            pp_tobacco.tobacco_class_id,
                            nvl(details.removal_qty,0) AS qty,
                            nvl(details.taxes_paid,0) AS tax
                        FROM
                            (
                                SELECT
                                    permit_periods.permit_id,
                                    permit_periods.period_id,
                                    tc.tobacco_class_id
                                FROM
                                    (
                                        SELECT
                                            pp.permit_id,
                                            pp.period_id
                                        FROM
                                            tu_permit_period pp
                                            INNER JOIN tu_rpt_period rp ON pp.period_id = rp.period_id
                                        WHERE
                                            rp.fiscal_yr =v_year
                                            AND rp.quarter =v_qtr
                                    ) permit_periods
                                    INNER JOIN tu_tobacco_class tc ON tc.class_type_cd <> 'SPTP'
                                                                      AND tc.tobacco_class_nm <> 'Cigars'
                            ) pp_tobacco
                            LEFT OUTER JOIN (
                                SELECT
                                    sf.permit_id,
                                    sf.period_id,
                                    fd.tobacco_class_id,
                                    fd.removal_qty,
                                    fd.taxes_paid
                                FROM
                                    tu_submitted_form sf
                                    INNER JOIN tu_form_detail fd ON sf.form_id = fd.form_id
                                WHERE
                                    sf.form_type_cd = '3852'
                            ) details ON pp_tobacco.permit_id = details.permit_id
                                         AND pp_tobacco.period_id = details.period_id
                                         AND pp_tobacco.tobacco_class_id = details.tobacco_class_id
                    ) taxes ON taxes.permit_id = p.permit_id
                WHERE
                    p.permit_type_cd = 'MANU'
                            -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        AND (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------

                GROUP BY
                    c.company_id,
                    c.ein,
                    taxes.tobacco_class_id
            ) tufa
---------------------- Start JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
-- Only cigarette taxe data for now
            LEFT OUTER JOIN (
                SELECT
                    c.company_id,
                    c.ein,
                    tobacco_class_id,
                    ingestvol,
                    ingesttax
                FROM
                    (
                    --Replacement was done here to grab both ingested vol and tax data
                    --Kyle as per 21 March 2019
                        SELECT
                            ein_num,
                            tobacco_class_id,
                            SUM(ingestvol) ingestvol,
                            ingesttax
                        FROM
                            (
                                SELECT
                                    nvl(vol.ein_num,tax.ein_num) ein_num,
                                    nvl(vol.tobacco_class_id,tax.tobacco_class_id) tobacco_class_id,
                                    nvl(vol.ingestvol,0) ingestvol,
                                    nvl(tax.ingesttax,0) ingesttax
                                FROM
                                    (
-- INGESTED VOLUME DATA
-- KYLE 3/28/2019: Added UNIQUE since duplicate records were being pulled for Volume
                                        SELECT UNIQUE
                                            tc.ein_num,
                                            tc.ttb_company_id,
                                            ttc.tobacco_class_id,
                                            ttc.parent_class_id,
                                            nvl(tvt.pounds,0) AS ingestvol,
                                            tvt.permit_num
                                        FROM
                                            tu_ttb_company tc
                                            LEFT JOIN tu_ttb_permit tp ON tc.ttb_company_id = tp.ttb_company_id
                                            LEFT JOIN tu_ttb_volume_taxes_per_tt tvt ON tvt.ein = tc.ein_num
                                                                                        AND tc.fiscal_yr = tvt.fiscal_yr
                                            LEFT JOIN tu_tobacco_class ttc ON ttc.tobacco_class_nm = tvt.product
                                        WHERE
                                            tc.fiscal_yr =v_year
                                            AND tvt.qtr =v_qtr
                                            AND tvt.permit_num IN (
                                                SELECT
                                                    p.permit_num
                                                FROM
                                                    tu_permit p
                                                    JOIN tu_company c ON p.company_id = c.company_id
                                                WHERE
                                                    c.ein = tc.ein_num
                                            )
                                    ) vol
            -- Only care about tax data regardles in there is volume data to go with it
                                    RIGHT JOIN (
-- INGESTED TAX DATA
                                        SELECT
                                            ttbcmpy.ein_num,
                                            ttc.tobacco_class_id,
                                            NULL AS parent_class_id,
                                            SUM(nvl(ttb_taxes_paid,0) ) AS ingesttax,
                                            ttbpermit.permit_num
                                        FROM
                                            tu_ttb_company ttbcmpy
                                            INNER JOIN tu_ttb_permit ttbpermit ON ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id
                                            INNER JOIN tu_ttb_annual_tax ttbannualtx ON ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id
                                            INNER JOIN tu_tobacco_class ttc ON ttbannualtx.tobacco_class_id = ttc.tobacco_class_id
                                        WHERE
                                            ttc.tobacco_class_id <> 8
                                            AND ttbcmpy.fiscal_yr =v_year
                                            AND ttbannualtx.ttb_calendar_qtr =v_qtr
                                            AND ttbpermit.permit_num IN (
                                                SELECT
                                                    p.permit_num
                                                FROM
                                                    tu_permit p
                                                    JOIN tu_company c ON p.company_id = c.company_id
                                                WHERE
                                                    c.ein = ttbcmpy.ein_num
                                            )
                                        GROUP BY
                                            ttbcmpy.ein_num,
                                            ttc.tobacco_class_id,
                                            ttbpermit.permit_num
                                    ) tax ON vol.ein_num = tax.ein_num
                                             AND vol.permit_num = tax.permit_num
                                             AND CASE
                                        WHEN vol.parent_class_id IS NOT NULL THEN vol.parent_class_id
                                        ELSE vol.tobacco_class_id
                                    END = tax.tobacco_class_id
                            )
                        GROUP BY
                            ein_num,
                            tobacco_class_id,
                            ingesttax
                    ) ttbingested
                    INNER JOIN tu_company c ON ttbingested.ein_num = c.ein
                     where c.company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

            ) ingest ON tufa.company_id = ingest.company_id
                        AND tufa.tobacco_class_id = ingest.tobacco_class_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    ttb_company_id                AS company_id,
                    tc.tobacco_class_id,
                    amended_total_tax             AS amendtax,
                    ca.acceptance_flag,
                    CASE
                        WHEN tc.tobacco_class_id = 7
                             OR tc.tobacco_class_id = 8 THEN amended_total_volume * 1000
                        ELSE amended_total_volume
                    END AS amendvol,
                    accepted_ingested_total_tax   AS accept_ingest_tax
                FROM
                    tu_ttb_amendment ca,
                    tu_tobacco_class tc
                WHERE
                    ca.tobacco_class_id = tc.tobacco_class_id
                    AND ca.fiscal_yr =v_year
                    AND ca.qtr =v_qtr
                    AND tc.tobacco_class_nm <> 'Cigars'
                   AND ttb_company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

            ) amend ON tufa.company_id = amend.company_id
                       AND tufa.tobacco_class_id = amend.tobacco_class_id
    )
     ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
      WHERE (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,1,instr(tcads.tobacco_class_nm,'/')-1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='MANU'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
       and (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,instr(tcads.tobacco_class_nm,'/')+1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='MANU'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------


    ) combined
    INNER JOIN tu_tobacco_class tc on tc.tobacco_class_id = combined.tobacco_class_id
    WHERE tc.tobacco_class_nm = 'Cigarettes'
    GROUP BY tc.tobacco_class_id, tc.tobacco_class_nm;
    EXCEPTION
      WHEN no_data_found THEN v_period_tot_cigarette_taxes := 0;
  END;

--

  BEGIN
    SELECT SUM(TOTTAX) as TOTTAX
      into v_period_tot_ryo_taxes
    FROM
    (
     SELECT *
     FROM
     (
      SELECT
        tufa.company_id,tufa.ein,
        tufa.tobacco_class_id,
        'IMPT' as permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            CASE ( amend.acceptance_flag||amend.review_flag )
                WHEN 'IY'   THEN amend.amendtax

                WHEN 'IN'   THEN ingest.ingesttax
                WHEN 'NN' THEN  amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,

            CASE ( amend.acceptance_flag||amend.review_flag )
                WHEN 'IY'   THEN amend.amendvol

                WHEN 'IN'   THEN ingest.ingestvol
                WHEN 'NN' THEN  amend.amendvol
                ELSE tufa.tufavol
                END AS totvol
/*
            CASE ( amend.acceptance_flag )
                WHEN 'I'   THEN ingest.ingesttax
                WHEN 'N'   THEN amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,
            CASE (amend.acceptance_flag)
                WHEN 'I'   THEN ingest.ingestvol
                WHEN 'N'   THEN amend.amendvol
                ELSE tufa.tufavol
            END AS totvol
*/
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      FROM
      (
        SELECT c.company_id,c.ein, taxes.tobacco_class_id, SUM(taxes.qty) as TUFAVOL, SUM(taxes.tax) as TUFATAX
        FROM tu_company c
        INNER JOIN tu_permit p on c.company_id = p.company_id
        INNER JOIN
        (
          SELECT pp_tobacco.permit_id, pp_tobacco.tobacco_class_id, NVL(details.removal_qty, 0) as qty, NVL(details.taxes_paid, 0) as tax
          FROM
          (
            SELECT permit_periods.permit_id, permit_periods.period_id, tc.tobacco_class_id
            FROM
              (SELECT pp.permit_id, pp.period_id
              FROM tu_permit_period pp
              INNER JOIN tu_rpt_period rp on pp.period_id = rp.period_id
              WHERE rp.fiscal_yr=v_year
              AND rp.quarter=v_qtr) permit_periods
            INNER JOIN tu_tobacco_class tc on tc.class_type_cd <> 'SPTP' AND tc.tobacco_class_nm <> 'Cigars'
          ) pp_tobacco
          LEFT OUTER JOIN
          (
            SELECT sf.permit_id, sf.period_id, fd.tobacco_class_id, fd.removal_qty, fd.taxes_paid
            FROM tu_submitted_form sf
            INNER JOIN tu_form_detail fd on sf.form_id = fd.form_id
            where sf.form_type_cd = '3852'
          ) details on
                  pp_tobacco.permit_id = details.permit_id
              AND pp_tobacco.period_id = details.period_id
              AND pp_tobacco.tobacco_class_id = details.tobacco_class_id
        ) taxes on taxes.permit_id = p.permit_id
        WHERE p.permit_type_cd='IMPT'
                -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        and (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------

        GROUP BY c.company_id,c.ein, taxes.tobacco_class_id
      ) tufa
---------------------- Start JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    c1.ein,
                    c1.company_id,
                    SUM(nvl(cbpingested.ingest_vol,0) ) AS ingestvol,
                    SUM(nvl(cbpingested.cbp_taxes_paid,0) ) AS ingesttax,
                    cbpingested.tobacco_class_id
                FROM
                    (
                        SELECT
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            SUM(nvl(cbpentry.estimated_tax,0) ) AS cbp_taxes_paid,
                            SUM(CASE
                                WHEN ttc.tobacco_class_id = 7 THEN cbpentry.qty_removed * 1000
                                ELSE cbpentry.qty_removed * 2.204
                            END) AS ingest_vol,
                            ttc.tobacco_class_nm,
                            CASE
                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                                ELSE '1-4'
                            END AS fiscal_qtr,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id   AS tobacco_class_id
                        FROM
                            tu_cbp_importer cbpdetail,
                            tu_cbp_entry cbpentry,
                            tu_tobacco_class ttc
                        WHERE
                            ( cbpentry.excluded_flag IS NULL
                              OR cbpentry.excluded_flag <> 'Y' )
                            AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
                            AND ( ttc.tobacco_class_id <> 14
                                  OR ttc.tobacco_class_nm <> 'Non-Taxable' )
                            AND ( ttc.tobacco_class_id <> 8
                                  OR ttc.tobacco_class_nm <> 'Cigars' )
                            AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
                            AND cbpdetail.association_type_cd IS NOT NULL
                            AND cbpdetail.fiscal_yr =v_year
                            AND cbpentry.fiscal_qtr =v_qtr
                        GROUP BY
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            ttc.tobacco_class_nm,
                            CASE
                                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                                ELSE '1-4'
                            END,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id,
                            cbpentry.uom_cd
                    ) cbpingested,
                    tu_company c1
                WHERE
                    ( cbpingested.importer_ein = c1.ein
                      AND cbpingested.association_type_cd = 'IMPT'
                      OR cbpingested.consignee_ein = c1.ein
                      AND cbpingested.association_type_cd = 'CONS' )
                    AND cbpingested.cbp_taxes_paid != 0
                    AND c1.company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

                GROUP BY
                    c1.company_id,
                    c1.ein,
                    cbpingested.tobacco_class_id
            ) ingest ON tufa.company_id = ingest.company_id
                        AND tufa.ein = ingest.ein
                        AND tufa.tobacco_class_id = ingest.tobacco_class_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      LEFT OUTER JOIN (
        SELECT CBP_COMPANY_ID as company_id, tc.tobacco_class_id, AMENDED_TOTAL_TAX as AMENDTAX, AMENDED_TOTAL_VOLUME as AMENDVOL,
        ca.acceptance_flag,ca.review_flag
        FROM tu_cbp_amendment ca, tu_tobacco_class tc
        WHERE ca.tobacco_class_id = tc.tobacco_class_id
        and ca.fiscal_yr=v_year
        and ca.qtr=v_qtr
        and tc.tobacco_class_nm <> 'Cigars'
        AND cbp_company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
      ) amend on tufa.company_id = amend.company_id and tufa.tobacco_class_id = amend.tobacco_class_id
         )
     ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
      WHERE (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,1,instr(tcads.tobacco_class_nm,'/')-1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='IMPT'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
       and (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,instr(tcads.tobacco_class_nm,'/')+1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='IMPT'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------



      UNION All
     ----------------------------------------BEGIN INGESTED ONLY added on 16 oct 2018-----------------------------------------------------

Select * from (
       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_CLASS_NM) tobacco_class_id,
      -- tcads.TOBACCO_CLASS_NM,
       tcads.PERMIT_TYPE,
       ABS(tcads.Delta),
        null as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year   and tcads.fiscal_qtr=to_char(v_qtr)
     and tobacco_sub_type_1  is  null
     and  tobacco_sub_type_2 is null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion--------------------                                
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
-------------------------------------------------------------------------------------
     UNION

       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_SUB_TYPE_1) tobacco_class_id,
       --TOBACCO_SUB_TYPE_1,
       tcads.PERMIT_TYPE,
       tobacco_sub_type_1_amndtx ,
       TOBACCO_SUB_TYPE_1_POUNDS as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year   and tcads.fiscal_qtr=to_char(v_qtr)
     and tobacco_sub_type_1  is  not null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion--------------------                                
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
-------------------------------------------------------------------------------------
     UNION

       SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,tcads.EIN,
       (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_SUB_TYPE_2) tobacco_class_id,
       --tcads.TOBACCO_SUB_TYPE_2,
       tcads.PERMIT_TYPE,
      tobacco_sub_type_2_amndtx ,
      TOBACCO_SUB_TYPE_2_POUNDS as TOTVOL
     from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
     and tcads.FISCAL_YR=v_year    and tcads.fiscal_qtr=to_char(v_qtr)
     and  tobacco_sub_type_2 is not null
-------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        and (tcads.ein) not in 
                                  (    Select distinct ein
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion--------------------                                
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
-------------------------------------------------------------------------------------     
     ) INGESTED_ONLY


    ----------------------------------------END INGESTED ONLY added on 16 oct 2018-----------------------------------------------------
      UNION

      SELECT *
      FROM
      (
        SELECT
            tufa.company_id,
            tufa.ein,
            tufa.tobacco_class_id,
            'MANU' AS permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            CASE ( amend.acceptance_flag )

                WHEN 'I'   THEN nvl(nvl(amend.accept_ingest_tax,ingest.ingesttax),0)
                WHEN 'N'   THEN amend.amendtax
                ELSE tufa.tufatax
            END AS tottax,
            CASE ( amend.acceptance_flag )
                WHEN 'I'   THEN nvl(ingest.ingestvol,0)
                WHEN 'N'   THEN amend.amendvol
                ELSE tufa.tufavol
            END AS totvol
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
        FROM
            (
                SELECT
                    c.company_id,
                    c.ein,
                    taxes.tobacco_class_id,
                    SUM(taxes.qty) AS tufavol,
                    SUM(taxes.tax) AS tufatax
                FROM
                    tu_company c
                    INNER JOIN tu_permit p ON c.company_id = p.company_id
                    INNER JOIN (
                        SELECT
                            pp_tobacco.permit_id,
                            pp_tobacco.tobacco_class_id,
                            nvl(details.removal_qty,0) AS qty,
                            nvl(details.taxes_paid,0) AS tax
                        FROM
                            (
                                SELECT
                                    permit_periods.permit_id,
                                    permit_periods.period_id,
                                    tc.tobacco_class_id
                                FROM
                                    (
                                        SELECT
                                            pp.permit_id,
                                            pp.period_id
                                        FROM
                                            tu_permit_period pp
                                            INNER JOIN tu_rpt_period rp ON pp.period_id = rp.period_id
                                        WHERE
                                            rp.fiscal_yr =v_year
                                            AND rp.quarter =v_qtr
                                    ) permit_periods
                                    INNER JOIN tu_tobacco_class tc ON tc.class_type_cd <> 'SPTP'
                                                                      AND tc.tobacco_class_nm <> 'Cigars'
                            ) pp_tobacco
                            LEFT OUTER JOIN (
                                SELECT
                                    sf.permit_id,
                                    sf.period_id,
                                    fd.tobacco_class_id,
                                    fd.removal_qty,
                                    fd.taxes_paid
                                FROM
                                    tu_submitted_form sf
                                    INNER JOIN tu_form_detail fd ON sf.form_id = fd.form_id
                                WHERE
                                    sf.form_type_cd = '3852'
                            ) details ON pp_tobacco.permit_id = details.permit_id
                                         AND pp_tobacco.period_id = details.period_id
                                         AND pp_tobacco.tobacco_class_id = details.tobacco_class_id
                    ) taxes ON taxes.permit_id = p.permit_id
                WHERE
                    p.permit_type_cd = 'MANU'
                            -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
        and (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )
                                                                    
------------------------------------------------------------------------------------

                GROUP BY
                    c.company_id,
                    c.ein,
                    taxes.tobacco_class_id
            ) tufa
---------------------- Start JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
-- Only cigarette taxe data for now
            LEFT OUTER JOIN (
                SELECT
                    c.company_id,
                    c.ein,
                    tobacco_class_id,
                    ingestvol,
                    ingesttax
                FROM
                    (
                    --Replacement was done here to grab both ingested vol and tax data
                    --Kyle as per 21 March 2019
                        SELECT
                            ein_num,
                            tobacco_class_id,
                            SUM(ingestvol) ingestvol,
                            ingesttax
                        FROM
                            (
                                SELECT
                                    nvl(vol.ein_num,tax.ein_num) ein_num,
                                    nvl(vol.tobacco_class_id,tax.tobacco_class_id) tobacco_class_id,
                                    nvl(vol.ingestvol,0) ingestvol,
                                    nvl(tax.ingesttax,0) ingesttax
                                FROM
                                    (
-- INGESTED VOLUME DATA
-- KYLE 3/28/2019: Added UNIQUE since duplicate records were being pulled for Volume
                                        SELECT UNIQUE
                                            tc.ein_num,
                                            tc.ttb_company_id,
                                            ttc.tobacco_class_id,
                                            ttc.parent_class_id,
                                            nvl(tvt.pounds,0) AS ingestvol,
                                            tvt.permit_num
                                        FROM
                                            tu_ttb_company tc
                                            LEFT JOIN tu_ttb_permit tp ON tc.ttb_company_id = tp.ttb_company_id
                                            LEFT JOIN tu_ttb_volume_taxes_per_tt tvt ON tvt.ein = tc.ein_num
                                                                                        AND tc.fiscal_yr = tvt.fiscal_yr
                                            LEFT JOIN tu_tobacco_class ttc ON ttc.tobacco_class_nm = tvt.product
                                        WHERE
                                            tc.fiscal_yr =v_year
                                            AND tvt.qtr =v_qtr
                                            AND tvt.permit_num IN (
                                                SELECT
                                                    p.permit_num
                                                FROM
                                                    tu_permit p
                                                    JOIN tu_company c ON p.company_id = c.company_id
                                                WHERE
                                                    c.ein = tc.ein_num
                                            )
                                    ) vol
            -- Only care about tax data regardles in there is volume data to go with it
                                    RIGHT JOIN (
-- INGESTED TAX DATA
                                        SELECT
                                            ttbcmpy.ein_num,
                                            ttc.tobacco_class_id,
                                            NULL AS parent_class_id,
                                            SUM(nvl(ttb_taxes_paid,0) ) AS ingesttax,
                                            ttbpermit.permit_num
                                        FROM
                                            tu_ttb_company ttbcmpy
                                            INNER JOIN tu_ttb_permit ttbpermit ON ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id
                                            INNER JOIN tu_ttb_annual_tax ttbannualtx ON ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id
                                            INNER JOIN tu_tobacco_class ttc ON ttbannualtx.tobacco_class_id = ttc.tobacco_class_id
                                        WHERE
                                            ttc.tobacco_class_id <> 8
                                            AND ttbcmpy.fiscal_yr =v_year
                                            AND ttbannualtx.ttb_calendar_qtr =v_qtr
                                            AND ttbpermit.permit_num IN (
                                                SELECT
                                                    p.permit_num
                                                FROM
                                                    tu_permit p
                                                    JOIN tu_company c ON p.company_id = c.company_id
                                                WHERE
                                                    c.ein = ttbcmpy.ein_num
                                            )
                                        GROUP BY
                                            ttbcmpy.ein_num,
                                            ttc.tobacco_class_id,
                                            ttbpermit.permit_num
                                    ) tax ON vol.ein_num = tax.ein_num
                                             AND vol.permit_num = tax.permit_num
                                             AND CASE
                                        WHEN vol.parent_class_id IS NOT NULL THEN vol.parent_class_id
                                        ELSE vol.tobacco_class_id
                                    END = tax.tobacco_class_id
                            )
                        GROUP BY
                            ein_num,
                            tobacco_class_id,
                            ingesttax
                    ) ttbingested
                    INNER JOIN tu_company c ON ttbingested.ein_num = c.ein
                    where c.company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

            ) ingest ON tufa.company_id = ingest.company_id
                        AND tufa.tobacco_class_id = ingest.tobacco_class_id
                        
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    ttb_company_id                AS company_id,
                    tc.tobacco_class_id,
                    amended_total_tax             AS amendtax,
                    ca.acceptance_flag,
                    CASE
                        WHEN tc.tobacco_class_id = 7
                             OR tc.tobacco_class_id = 8 THEN amended_total_volume * 1000
                        ELSE amended_total_volume
                    END AS amendvol,
                    accepted_ingested_total_tax   AS accept_ingest_tax
                FROM
                    tu_ttb_amendment ca,
                    tu_tobacco_class tc
                WHERE
                    ca.tobacco_class_id = tc.tobacco_class_id
                    AND ca.fiscal_yr =v_year
                    AND ca.qtr =v_qtr
                    AND tc.tobacco_class_nm <> 'Cigars'
                    AND ttb_company_id not in 
                    ------------------------------------------------------------
                                  (    Select company_id
                                        from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or 
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

            ) amend ON tufa.company_id = amend.company_id
                       AND tufa.tobacco_class_id = amend.tobacco_class_id
    )
     ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
       WHERE (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,1,instr(tcads.tobacco_class_nm,'/')-1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='MANU'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
       and (ein,tobacco_class_id) NOT IN
      (
       SELECT tcads.EIN,(Select ttc1.tobacco_class_id from tu_tobacco_class ttc1
       where ttc1.TOBACCO_CLASS_NM=
       Decode(instr(tcads.tobacco_class_nm,'/'),0,tcads.tobacco_class_nm,Substr(tcads.tobacco_class_nm,instr(tcads.tobacco_class_nm,'/')+1))
       ) tobacco_class_id
       from tu_comparison_all_delta_sts tcads
       where tcads.STATUS='Excluded'
       and Delta_type='TUFA'
       and tcads.FISCAL_YR=v_year
       and tcads.fiscal_qtr=to_char(v_qtr)
       AND tcads.PERMIT_TYPE='MANU'
       and tcads.TOBACCO_CLASS_NM <>'Cigars'
     )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------


    ) combined
    INNER JOIN tu_tobacco_class tc on tc.tobacco_class_id = combined.tobacco_class_id
    WHERE tc.tobacco_class_nm = 'Roll-Your-Own'
    GROUP BY tc.tobacco_class_id, tc.tobacco_class_nm;
    EXCEPTION
      WHEN no_data_found THEN v_period_tot_ryo_taxes := 0;
  END;

--

  BEGIN

    For rec in calc_marketshare LOOP

--


    BEGIN

        -- insert the existing company addresses

      merge into tu_assessment_address rp

         using (select a.company_id, p.assessment_id, p.assessment_version, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total

                  from tu_address a, tu_assessment p

                  where a.company_id = rec.company_id

                  and p.assessment_id = v_assessment_id

                  and a.address_type_cd='PRIM') ad

            on (rp.company_id = ad.company_id

              and rp.assessment_id = ad.assessment_id

              and rp.address_type_cd = ad.address_type_cd

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.street_address = ad.street_address, rp.suite = ad.suite, rp.city = ad.city, rp.state = ad.state, rp.postal_cd = ad.postal_cd, rp.fax_num = ad.fax_num, rp.cntry_dial_cd = ad.cntry_dial_cd, rp.country_cd = ad.country_cd

        when not matched then insert

         (company_id, assessment_id, version_num, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total)

          values (rec.company_id, v_assessment_id, v_version_num, ad.address_type_cd, ad.street_address, ad.suite, ad.attention, ad.city, ad.state, ad.province, ad.postal_cd, ad.fax_num, ad.cntry_dial_cd, ad.country_cd, ad.hash_total);



    -- insert the existing company contacts

       merge into tu_assessment_contact rp

         using (select c.contact_id, c.company_id, p.assessment_id, p.assessment_version, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total

                  from primary_contact_vw c, tu_assessment p

                  where c.company_id = rec.company_id

                  and p.assessment_id = v_assessment_id) ct

            on (rp.company_id = ct.company_id

              and rp.assessment_id = ct.assessment_id

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.contact_id = ct.contact_id, rp.last_nm = ct.last_nm, rp.first_nm = ct.first_nm, rp.phone_num = ct.phone_num, rp.phone_ext = ct.phone_ext, rp.email_address = ct.email_address, rp.country_cd = ct.country_cd, rp.fax_num = ct.fax_num

        when not matched then insert

         (company_id, contact_id, assessment_id, version_num, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total)

          values (rec.company_id, ct.contact_id, v_assessment_id, v_version_num, ct.last_nm, ct.first_nm, ct.phone_num, ct.phone_ext, ct.cntry_dial_cd, ct.email_address, ct.country_cd, ct.fax_num, ct.hash_total);



    END;
--

-- test to see if address or contact has changed since previous version
    BEGIN
      SELECT UNIQUE 'N'

      INTO v_address_changed

      FROM TU_ASSESSMENT a

      INNER JOIN TU_ASSESSMENT_VERSION av on a.assessment_id=av.assessment_id

      INNER JOIN

       ( SELECT * FROM

        (SELECT c.company_id, nvl(aa.assessment_id, v_previous_assessment) as assessment_id, nvl(aa.version_num, v_previous_version) as version_num, nvl(hash_total, 1) as assessment_hash

          FROM TU_COMPANY c LEFT OUTER JOIN TU_ASSESSMENT_ADDRESS aa on c.company_id = aa.company_id

          WHERE c.company_id=rec.company_id)

        WHERE version_num=v_previous_version

        AND assessment_id=v_previous_assessment) aa on aa.assessment_id=av.assessment_id

      INNER JOIN

       ( SELECT c.company_id, nvl(hash_total, 1) as address_hash

        FROM TU_COMPANY c LEFT OUTER JOIN TU_ADDRESS a on c.company_id = a.company_id

        WHERE c.company_id=rec.company_id ) ad on aa.company_id = ad.company_id

      WHERE aa.assessment_hash = ad.address_hash;

        EXCEPTION

          when no_data_found then

          --DBMS_OUTPUT.PUT_LINE('EXCEPTION: ADDRESS CHANGED');

            v_address_changed := 'Y';

    END;



    -- now check contact for changes

    BEGIN

      SELECT UNIQUE 'N'

      INTO v_contact_changed

      FROM TU_ASSESSMENT a

      INNER JOIN TU_ASSESSMENT_VERSION av on a.assessment_id=av.assessment_id

      INNER JOIN

       ( SELECT * FROM

        (SELECT c.company_id, nvl(aa.assessment_id, v_previous_assessment) as assessment_id, nvl(aa.version_num, v_previous_version) as version_num, nvl(hash_total, 1) as assessment_hash

          FROM TU_COMPANY c LEFT OUTER JOIN TU_ASSESSMENT_CONTACT aa on c.company_id = aa.company_id

          WHERE c.company_id=rec.company_id)

        WHERE version_num=v_previous_version

        AND assessment_id=v_previous_assessment) aa on aa.assessment_id=av.assessment_id

      INNER JOIN

       ( SELECT c.company_id, nvl(hash_total, 1) as contact_hash

        FROM TU_COMPANY c LEFT OUTER JOIN TU_CONTACT a on c.company_id = a.company_id

        WHERE c.company_id=rec.company_id ) ad on aa.company_id = ad.company_id

      WHERE aa.assessment_hash = ad.contact_hash;

        EXCEPTION

          when no_data_found then

          --DBMS_OUTPUT.PUT_LINE('EXCEPTION: CONTACT CHANGED');

            v_contact_changed := 'Y';

    END;

--

    BEGIN

    IF rec.tobacco_class_nm = 'Chew' then



      -- calculate the current share

      if v_period_tot_chew_taxes <> '0' then

        v_current_share := trunc(rec.TOTTAX/v_period_tot_chew_taxes,6);

      else

        v_current_share := 0;

      end if;



      -- query for the previous share

      BEGIN

        select share_tot_taxes

        into v_previous_share

        from tu_market_share

        where company_id = rec.company_id

        and assessment_id = v_previous_assessment

        and tobacco_class_id = rec.tobacco_class_id

        and version_num = v_previous_version;

        EXCEPTION

          when others then

            v_previous_share := 0;

      END;

      IF v_current_share > 0 OR v_previous_share > 0 THEN

        -- calculate delta
        v_delta_share := v_current_share - nvl(v_previous_share,0);
        v_chew_rank := v_chew_rank+1;

        -- insert into market share table
    BEGIN
        INSERT into tu_market_share

          (assessment_id,ein, company_id, tobacco_class_id,

           version_num, ms_rank,

           tot_vol_removed, tot_taxes_paid, share_tot_taxes,

           previous_share, delta_share, created_dt,author, original_corrected,
           address_changed_flag, contact_changed_flag,company_name)

        VALUES

          (v_assessment_id,rec.ein, NVL(rec.company_id,0), rec.tobacco_class_id,

           v_version_num, v_chew_rank,

           rec.TOTVOL, rec.TOTTAX,

           v_current_share, v_previous_share, v_delta_share, sysdate,v_author, Case when v_previous_share>0 then 'C' 
                                                                                    when (v_previous_share is null or v_previous_share=0) then 'O' 
                                                                                    else 'C' end,
           v_address_changed, v_contact_changed,rec.legal_nm);
       EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
                UPDATE tu_market_share
                 SET ms_rank=v_chew_rank,
                     tot_vol_removed=rec.TOTVOL,
                     tot_taxes_paid=rec.TOTTAX, share_tot_taxes=v_current_share,
                     previous_share=v_previous_share, delta_share=v_delta_share,
                     ein=rec.ein,company_name=rec.legal_nm
                 WHERE assessment_id=v_assessment_id
                 AND company_id=rec.company_id
                 AND tobacco_class_id=rec.tobacco_class_id
                 AND version_num=v_version_num;

        END;

      END IF;

    ELSIF rec.tobacco_class_nm = 'Snuff' then



      -- calculate the current share

      if v_period_tot_snuff_taxes <> '0' then

        v_current_share := trunc(rec.TOTTAX/v_period_tot_snuff_taxes,6);

      else

        v_current_share := 0;

      end if;



      -- query for the previous share

      BEGIN

        select share_tot_taxes

        into v_previous_share

        from tu_market_share

        where company_id = rec.company_id

        and assessment_id = v_previous_assessment

        and tobacco_class_id = rec.tobacco_class_id

        and version_num = v_previous_version;

        EXCEPTION when others then v_previous_share := 0;

      END;


      IF v_current_share > 0 OR v_previous_share > 0 THEN

      -- calculate delta
        v_delta_share := v_current_share - nvl(v_previous_share,0);
        v_snuff_rank := v_snuff_rank+1;


    BEGIN
        INSERT into tu_market_share

          (assessment_id,ein, company_id, tobacco_class_id,

           version_num, ms_rank,

           tot_vol_removed, tot_taxes_paid, share_tot_taxes,

           previous_share, delta_share,

           created_dt, author, original_corrected,
           address_changed_flag, contact_changed_flag,company_name)

        VALUES

          (v_assessment_id,rec.ein, NVL(rec.company_id,0), rec.tobacco_class_id,

           v_version_num, v_snuff_rank,

           rec.TOTVOL, rec.TOTTAX,

           v_current_share, v_previous_share, v_delta_share, sysdate, v_author, Case when v_previous_share>0 then 'C' 
                                                                                    when (v_previous_share is null or v_previous_share=0) then 'O' 
                                                                                    else 'C' end,
           v_address_changed, v_contact_changed,rec.legal_nm);

     EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
                UPDATE tu_market_share
                 SET ms_rank=v_snuff_rank,
                     tot_vol_removed=rec.TOTVOL,
                     tot_taxes_paid=rec.TOTTAX, share_tot_taxes=v_current_share,
                     previous_share=v_previous_share, delta_share=v_delta_share,
                     ein=rec.ein,company_name=rec.legal_nm
                 WHERE assessment_id=v_assessment_id
                 AND company_id=rec.company_id
                 AND tobacco_class_id=rec.tobacco_class_id
                 AND version_num=v_version_num;

        END;

       END IF;

    ELSIF rec.tobacco_class_nm = 'Cigarettes' then

      -- calculate the current share

      if v_period_tot_cigarette_taxes <> '0' then

        v_current_share := trunc(rec.TOTTAX/v_period_tot_cigarette_taxes,6);

      else

        v_current_share := 0;

      end if;



      -- query for the previous share

      BEGIN

        select share_tot_taxes

        into v_previous_share

        from tu_market_share

        where company_id = rec.company_id

        and assessment_id = v_previous_assessment

        and tobacco_class_id = rec.tobacco_class_id

        and version_num = v_previous_version;

        EXCEPTION when others then v_previous_share := 0;

      END;

      IF v_current_share > 0 OR v_previous_share > 0 THEN

        -- calculate delta
        v_delta_share := v_current_share - nvl(v_previous_share,0);

        v_cigarette_rank := v_cigarette_rank+1;

     BEGIN
        INSERT into tu_market_share

          (assessment_id,ein, company_id, tobacco_class_id,

           version_num, ms_rank,

           tot_vol_removed, tot_taxes_paid, share_tot_taxes,

           previous_share, delta_share,

           created_dt, author, original_corrected,
           address_changed_flag, contact_changed_flag,company_name)

        VALUES

          (v_assessment_id,rec.ein, NVL(rec.company_id,0), rec.tobacco_class_id,

           v_version_num, v_cigarette_rank,

           rec.TOTVOL, rec.TOTTAX,

           v_current_share, v_previous_share, v_delta_share,

           sysdate,v_author, Case when v_previous_share>0 then 'C' 
                                  when (v_previous_share is null or v_previous_share=0) then 'O' 
                                  else 'C' end,
           v_address_changed, v_contact_changed,rec.legal_nm);

        EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
                UPDATE tu_market_share
                 SET ms_rank=v_cigarette_rank,
                     tot_vol_removed=rec.TOTVOL,
                     tot_taxes_paid=rec.TOTTAX, share_tot_taxes=v_current_share,
                     previous_share=v_previous_share, delta_share=v_delta_share,
                     ein=rec.ein,company_name=rec.legal_nm
                 WHERE assessment_id=v_assessment_id
                 AND company_id=rec.company_id
                 AND tobacco_class_id=rec.tobacco_class_id
                 AND version_num=v_version_num;

        END;

      END IF;

    ELSIF rec.tobacco_class_nm = 'Pipe' then


      -- calculate the current share

      if v_period_tot_pipe_taxes <> '0' then

        v_current_share := trunc(rec.TOTTAX/v_period_tot_pipe_taxes,6);

      else

        v_current_share := 0;

      end if;



      -- query for the previous share

      BEGIN

        select share_tot_taxes

        into v_previous_share

        from tu_market_share

        where company_id = rec.company_id

        and assessment_id = v_previous_assessment

        and tobacco_class_id = rec.tobacco_class_id

        and version_num = v_previous_version;

        EXCEPTION when others then v_previous_share := 0;

      END;

      IF v_current_share > 0 OR v_previous_share > 0 THEN

        -- calculate delta
        v_delta_share := v_current_share - nvl(v_previous_share,0);

        v_pipe_rank := v_pipe_rank+1;

    BEGIN
        INSERT into tu_market_share

          (assessment_id,ein, company_id, tobacco_class_id,

           version_num, ms_rank,

           tot_vol_removed, tot_taxes_paid, share_tot_taxes,

           previous_share, delta_share,

           created_dt, author, original_corrected,
           address_changed_flag, contact_changed_flag,company_name)

        VALUES

          (v_assessment_id,rec.ein,NVL(rec.company_id,0), rec.tobacco_class_id,

           v_version_num, v_pipe_rank,

           rec.TOTVOL, rec.TOTTAX,

           v_current_share, v_previous_share, v_delta_share,

           sysdate, v_author,Case when v_previous_share>0 then 'C' 
                                  when (v_previous_share is null or v_previous_share=0) then 'O' 
                                  else 'C' end,
           v_address_changed, v_contact_changed,rec.legal_nm);

       EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
                UPDATE tu_market_share
                 SET ms_rank=v_pipe_rank,
                     tot_vol_removed=rec.TOTVOL,
                     tot_taxes_paid=rec.TOTTAX, share_tot_taxes=v_current_share,
                     previous_share=v_previous_share, delta_share=v_delta_share,
                     ein=rec.ein,company_name=rec.legal_nm
                 WHERE assessment_id=v_assessment_id
                 AND company_id=rec.company_id
                 AND tobacco_class_id=rec.tobacco_class_id
                 AND version_num=v_version_num;

        END;

      END IF;

    ELSIF rec.tobacco_class_nm = 'Roll-Your-Own' then

      -- calculate the current share

      if v_period_tot_ryo_taxes <> '0' then

        v_current_share := trunc(rec.TOTTAX/v_period_tot_ryo_taxes,6);

      else

        v_current_share := 0;

      end if;



      -- query for the previous share

      BEGIN

        select share_tot_taxes

        into v_previous_share

        from tu_market_share

        where company_id = rec.company_id

        and assessment_id = v_previous_assessment

        and tobacco_class_id = rec.tobacco_class_id

        and version_num = v_previous_version;

        EXCEPTION when others then v_previous_share := 0;

      END;


      IF v_current_share > 0 OR v_previous_share > 0 THEN

        -- calculate delta

        v_delta_share := v_current_share - nvl(v_previous_share,0);

        v_ryo_rank := v_ryo_rank+1;

--dbms_output.put_line(rec.legal_nm);
   BEGIN
        INSERT into tu_market_share

          (assessment_id,ein, company_id, tobacco_class_id,

           version_num, ms_rank,

           tot_vol_removed, tot_taxes_paid, share_tot_taxes,

           previous_share, delta_share,

           created_dt,author, original_corrected,
           address_changed_flag, contact_changed_flag,company_name)

        VALUES

          (v_assessment_id,rec.ein, NVL(rec.company_id,0), rec.tobacco_class_id,

           v_version_num, v_ryo_rank,

           rec.TOTVOL, rec.TOTTAX,

           v_current_share, v_previous_share, v_delta_share,

           sysdate,v_author, Case when v_previous_share>0 then 'C' 
                                  when (v_previous_share is null or v_previous_share=0) then 'O' 
                                  else 'C' end,
           v_address_changed, v_contact_changed,rec.legal_nm);

       EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
                UPDATE tu_market_share
                 SET ms_rank=v_ryo_rank,
                     tot_vol_removed=rec.TOTVOL,
                     tot_taxes_paid=rec.TOTTAX, share_tot_taxes=v_current_share,
                     previous_share=v_previous_share, delta_share=v_delta_share,
                     ein=rec.ein,company_name=rec.legal_nm
                 WHERE assessment_id=v_assessment_id
                 AND company_id=rec.company_id
                 AND tobacco_class_id=rec.tobacco_class_id
                 AND version_num=v_version_num;

        END;


      END IF;

    END IF;

    END;

    COMMIT;

    END LOOP;
    
            ------------------------Start of changes for ST# 2699-------
--DBMS_OUTPUT.PUT_LINE('Start of Story');
 BEGIN
--DBMS_OUTPUT.PUT_LINE('Start of Story v_assessment_id '||v_assessment_id);
  BEGIN
        Select max(Version_num)
        INTO v_pre_ver
        FROM TU_market_share
        WHERE assessment_id=v_assessment_id
        and version_num not in(  Select max(Version_num)
        FROM TU_market_share
        WHERE assessment_id=v_assessment_id) ;
  END;

FOR i_val IN cur_pv (v_assessment_id,v_previous_version)
    LOOP
BEGIN
        Select 'X'
        INTO V_exist
        FROM tu_market_share
        WHERE assessment_id=i_val.assessment_id
        and version_num=v_version_num
        AND company_id=i_val.company_id
        and TOBACCO_CLASS_ID=i_val.TOBACCO_CLASS_ID;
    EXCEPTION
     when others then V_exist:='A';
    END;

    IF  V_exist <>'X'
    THEN

          BEGIN
          Select 'X'
          into v_data_exist
          from tu_market_share
          where assessment_id=v_assessment_id
          and company_id=i_val.company_id
          and tobacco_class_id=i_val.tobacco_class_id
          and version_num=v_version_num;
          EXCEPTION
               when others then
               v_data_exist:='A';
          END;

if v_data_exist<>'X'
then

------below "if" is used for eliminating Company having previous share as null and continue to appear in market share new version---------
if (nvl(i_val.share_tot_taxes,0)>0)
then

--dbms_output.put_line('i_val.company_name'|| i_val.company_name);
BEGIN
INSERT into tu_market_share
          (assessment_id,ein, company_id, tobacco_class_id,tot_vol_removed, tot_taxes_paid,PREVIOUS_SHARE,
          version_num,delta_share,original_corrected, address_changed_flag, contact_changed_flag,company_name
           )
VALUES
          (v_assessment_id,i_val.ein, i_val.company_id, i_val.tobacco_class_id,'','',i_val.share_tot_taxes,v_version_num,
           0-abs(i_val.share_tot_taxes),i_val.original_corrected, i_val.address_changed_flag, i_val.contact_changed_flag,i_val.company_name
           );
Exception
When Others then
null;
--DBMS_OUTPUT.PUT_LINE('insert failed'||sqlerrm);
end;

    BEGIN

        -- insert the existing company addresses

      merge into tu_assessment_address rp

         using (select a.company_id, p.assessment_id, p.assessment_version, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total

                  from tu_address a, tu_assessment p

                  where a.company_id = i_val.company_id

                  and p.assessment_id = v_assessment_id

                  and a.address_type_cd='PRIM') ad

            on (rp.company_id = ad.company_id

              and rp.assessment_id = ad.assessment_id

              and rp.address_type_cd = ad.address_type_cd

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.street_address = ad.street_address, rp.suite = ad.suite, rp.city = ad.city, rp.state = ad.state, rp.postal_cd = ad.postal_cd, rp.fax_num = ad.fax_num, rp.cntry_dial_cd = ad.cntry_dial_cd, rp.country_cd = ad.country_cd

        when not matched then insert

         (company_id, assessment_id, version_num, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total)

          values (i_val.company_id, v_assessment_id, v_version_num, ad.address_type_cd, ad.street_address, ad.suite, ad.attention, ad.city, ad.state, ad.province, ad.postal_cd, ad.fax_num, ad.cntry_dial_cd, ad.country_cd, ad.hash_total);



    -- insert the existing company contacts

       merge into tu_assessment_contact rp

         using (select c.contact_id, c.company_id, p.assessment_id, p.assessment_version, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total

                  from primary_contact_vw c, tu_assessment p

                  where c.company_id = i_val.company_id

                  and p.assessment_id = v_assessment_id) ct

            on (rp.company_id = ct.company_id

              and rp.assessment_id = ct.assessment_id

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.contact_id = ct.contact_id, rp.last_nm = ct.last_nm, rp.first_nm = ct.first_nm, rp.phone_num = ct.phone_num, rp.phone_ext = ct.phone_ext, rp.email_address = ct.email_address, rp.country_cd = ct.country_cd, rp.fax_num = ct.fax_num

        when not matched then insert

         (company_id, contact_id, assessment_id, version_num, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total)

          values (i_val.company_id, ct.contact_id, v_assessment_id, v_version_num, ct.last_nm, ct.first_nm, ct.phone_num, ct.phone_ext, ct.cntry_dial_cd, ct.email_address, ct.country_cd, ct.fax_num, ct.hash_total);

    END;

end if;
end if;
end if;
commit;
END LOOP;
    
    -------------------------Added below Code as part of Story CTPTUFA-4833------------- 
IF v_submitind = 'Y' then

  BEGIN
  
    UPDATE TU_PERMIT_EXCL_AFFECT_ASSMNTS 
    SET ASSESSMENT_STATUS='COMPLETE'          
    where ASSMNT_RPT_DATA_YR=v_year
--  and ASSMNT_RPT_DATA_QTR=v_qtr
    AND ASSESSMENT_STATUS='RE-RUN'
    AND ASSMNT_TYPE='ANNU';
 
  EXCEPTION
  WHEN NO_DATA_FOUND then null;
  
  END;
  
    COMMIT;

END IF;
-------------------------

END;
------------------------END of changes for ST#2699----------
END;

END;

END calc_annual_quarter;

/

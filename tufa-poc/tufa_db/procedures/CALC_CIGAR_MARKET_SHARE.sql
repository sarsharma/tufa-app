--------------------------------------------------------
--  DDL for Procedure CALC_CIGAR_MARKET_SHARE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."CALC_CIGAR_MARKET_SHARE" (assess_yr NUMBER, assess_qtr NUMBER, author VARCHAR2, marketshare_type VARCHAR2) AS

BEGIN

DECLARE
  v_period_tot_taxes NUMBER(16,2) := 0;
  v_previous_share NUMBER(8,6) := 0;
  v_previous_version NUMBER(2) := 0;
  v_max_version NUMBER(2) := 0;
  v_current_share NUMBER(8,6) := 0;
  v_delta_share NUMBER(8,6) := 0;
  v_total_volume NUMBER(20,6) := 0;
  v_assessment_id NUMBER := 0;
  v_year NUMBER(4) := assess_yr;
  v_qtr NUMBER(4) := assess_qtr;
  v_author VARCHAR2(50) :=author;
  v_marketshareType VARCHAR2(6) := marketshare_type;
  v_submitind CHAR(1);
  v_quarter1 CHAR(1);
  v_quarter2 CHAR(1);
  v_quarter3 CHAR(1);
  v_quarter4 CHAR(1);
  v_contact_assessment_id NUMBER := 0;
  v_contact_version NUMBER := 0;
  v_address_changed CHAR(1) := 'N';
  v_contact_changed CHAR(1) := 'N';
  v_version_num NUMBER(2) := '1';
  v_rank NUMBER(4) := 0;
  V_pre_ver number;
  v_exist  varchar2(5);
  v_data_exist varchar2(5);
  V_ACONLY_ASSESSMENT_ID  NUMBER := 0; 
  V_ACONLY_VERSION_NUM NUMBER(2) := 0;

 CURSOR CUR_PV (p_assignment_id number, p_version_num number)
  IS
  SELECT * from tu_market_share
  where ASSESSMENT_ID=p_assignment_id
  and version_num=p_version_num
  and tobacco_class_id=8;

  type taxes_type is record
  (
    company_id NUMBER,
    ein VARCHAR2(9),
    legal_nm VARCHAR2(100),
    tobacco_class_id NUMBER,
    tobacco_class_nm VARCHAR2(16),
    tottax NUMBER(14,2),
    tot_vol_removed NUMBER(20,6),
    share_tot_taxes NUMBER(8,6),
    delta_share NUMBER(8,6),
    previous_share NUMBER(8,6)
  );
  rec taxes_type;

  calc_marketshare sys_refcursor;

BEGIN

-- get assessment information

  BEGIN
    SELECT assessment_id, assessment_yr, submitted_ind --, SUBSTR(cigar_ind,1,1), SUBSTR(cigar_ind,2,1), SUBSTR(cigar_ind,3,1), SUBSTR(cigar_ind,4,1)
      INTO v_assessment_id, v_year, v_submitind --, v_quarter1, v_quarter2, v_quarter3, v_quarter4
    FROM tu_assessment
    WHERE assessment_yr = v_year
    AND assessment_qtr = v_qtr
    AND assessment_type = 'CIQT';
    EXCEPTION
     when no_data_found then v_assessment_id := '0';
  END;

/*  IF assess_qtr = 1 THEN

    v_quarter1 := v_submitind;

  ELSIF assess_qtr=2 THEN

    v_quarter2 := v_submitind;

  ELSIF assess_qtr=3 THEN

    v_quarter3 := v_submitind;

  ELSIF assess_qtr=4 THEN

    v_quarter4 := v_submitind;

  END IF;
*/
  v_contact_assessment_id := v_assessment_id;

-- get existing version information

  BEGIN

    SELECT max(version_num)
    INTO v_max_version
    from tu_market_share
    where assessment_id = v_assessment_id;
    EXCEPTION
      when no_data_found then
        v_max_version := -1;

  END;
      v_contact_version := v_max_version;


 /* if v_max_version IS NULL THEN
    v_max_version := -1;
  END IF;
*/
 /* if v_max_version < 1  then

    BEGIN

      SELECT v.assessment_id, MAX(v.version_num)

      INTO v_contact_assessment_id, v_contact_version

      FROM TU_ASSESSMENT a, TU_ASSESSMENT_VERSION v

      WHERE a.assessment_id=v.assessment_id

      AND a.ASSESSMENT_YR = v_year - 1

      AND a.assessment_type='CIGR'

      GROUP BY v.assessment_id;

      EXCEPTION

        WHEN no_data_found then

          v_contact_assessment_id := v_assessment_id;

          v_contact_version := v_max_version;

    END;

  ELSE

      -- DBMS_OUTPUT.PUT_LINE('PREVIOUS YEAR NOT FOUND');

      v_contact_assessment_id := v_assessment_id;

      v_contact_version := v_max_version;

  END IF;
*/

  BEGIN

    -- highest version will always be the previous
    v_previous_version := v_max_version;

    IF v_submitind='Y' then
      -- we are generating the next version
      v_version_num := v_previous_version + 1;

      -- set the new CIGAR_IND to indicate which quarters have been submitted
      --UPDATE tu_assessment
     -- SET CIGAR_IND=v_quarter1||v_quarter2||v_quarter3||v_quarter4
     -- where assessment_id=v_assessment_id;

    ELSE
      v_version_num := 0;
    END IF;

    -- upsert the assessment version
    MERGE INTO TU_ASSESSMENT_VERSION av using dual on (assessment_id = v_assessment_id and version_num=v_version_num)
    WHEN MATCHED THEN
      UPDATE SET cigar_qtr=assess_qtr, created_by=v_author, created_dt=SYSDATE, marketshare_type=v_marketshareType
    WHEN NOT MATCHED THEN
      INSERT (assessment_id, version_num, cigar_qtr, marketshare_type, created_by, created_dt)
      VALUES (v_assessment_id, v_version_num, assess_qtr, v_marketshareType, v_author, SYSDATE);

  END;

--DBMS_OUTPUT.PUT_LINE('Entering into if');

    IF v_marketshareType='ACONLY' 

      THEN
      
      BEGIN
      
      SELECT MAX(assessment_id) 
      INTO V_ACONLY_ASSESSMENT_ID
      FROM tu_assessment_version 
      WHERE assessment_id in (Select assessment_id from tu_assessment where assessment_type='CIQT' and ASSESSMENT_YR=v_year)
      AND version_num<>0
      AND marketshare_type='FULLMS';
      
      END;
     
      BEGIN
      
      SELECT MAX(VERSION_NUM) 
      INTO V_ACONLY_VERSION_NUM
      FROM tu_assessment_version 
      WHERE assessment_id=V_ACONLY_ASSESSMENT_ID 
      AND marketshare_type='FULLMS';
      
      END;
     
    
    END IF;


  IF v_marketshareType <> 'ACONLY' THEN
    -- get the total cigar taxes
    BEGIN
--DBMS_OUTPUT.PUT_LINE('v_marketshareType'||v_marketshareType);

      select sum(taxes_paid)
        into v_period_tot_taxes
      from  tu_company a,
               tu_permit b,
               tu_permit_period c,
               tu_rpt_period d,
               tu_submitted_form f,
               tu_form_detail g,
               tu_tobacco_class h
       where a.company_id=b.company_id
         and B.PERMIT_ID=C.PERMIT_ID
         and D.PERIOD_ID=C.PERIOD_ID
         and F.PERMIT_ID=C.PERMIT_ID
         and F.PERIOD_ID=C.PERIOD_ID
         and G.FORM_ID=F.FORM_ID
         and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID
         and F.FORM_TYPE_CD = '3852'
         and h.tobacco_class_nm = 'Cigars'
         and D.fiscal_yr=v_year
         --and d.quarter=v_qtr
-------------------------Added below Code as part of Story CTPTUFA-4834-------------
     and (a.company_id,b.permit_num) not in
                                  (    Select distinct company_id,permit_num
                                  from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   );
------------------------------------------------------------------------------------

    END;
    
      -- open refcursor with current data
    OPEN calc_marketshare FOR
    SELECT company_id,ein,legal_nm, tobacco_class_id, tobacco_class_nm, TOTTAX, 0.0, 0.0, 0.0, 0.0
    FROM (
      select a.company_id,a.ein,a.legal_nm, h.tobacco_class_id,
             tobacco_class_nm, SUM(taxes_paid) as TOTTAX
      from tu_company a,
               tu_permit b,
               tu_permit_period c,
               tu_rpt_period d,
               tu_submitted_form f,
               tu_form_detail g,
               tu_tobacco_class h
       where a.company_id=b.company_id
        and B.PERMIT_ID=C.PERMIT_ID
        and D.PERIOD_ID=C.PERIOD_ID
        and F.PERMIT_ID=C.PERMIT_ID
        and F.PERIOD_ID=C.PERIOD_ID
        and G.FORM_ID=F.FORM_ID
        and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID
        and h.tobacco_class_nm = 'Cigars'
        and f.form_type_cd='3852'
        and D.fiscal_yr=v_year
--        and a.company_id=2010
        --and d.quarter=v_qtr
-------------------------Added below Code as part of Story CTPTUFA-4834-------------
        and (a.company_id,b.permit_num) not in
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   )
------------------------------------------------------------------------------------
        group by a.company_id,a.ein,a.legal_nm, h.tobacco_class_id,
               tobacco_class_nm ) recs
      order by recs.TOTTAX DESC;

  ELSE
--DBMS_OUTPUT.PUT_LINE('Entering into if 2');
      -- open refcursor with current data
      OPEN calc_marketshare FOR
      SELECT *
      FROM (
        select ms.company_id,ms.ein,ms.company_name as legal_nm, ms.tobacco_class_id,
               tc.tobacco_class_nm, ms.tot_taxes_paid as TOTTAX, ms.tot_vol_removed, ms.share_tot_taxes, ms.delta_share, ms.previous_share
        from  tu_market_share ms,
              tu_tobacco_class tc
         where ms.tobacco_class_id=tc.tobacco_class_id
          and tc.tobacco_class_nm = 'Cigars'
          and ms.assessment_id = V_ACONLY_ASSESSMENT_ID
          and ms.version_num = V_ACONLY_VERSION_NUM) recs
        order by recs.TOTTAX DESC;
  END IF;

-- now delete all version zero (temporary) information

-- including marketshare, addresses, and contacts

  BEGIN

    DELETE from tu_market_share

    WHERE assessment_id = v_assessment_id

      and version_num = '0';

    EXCEPTION when no_data_found then NULL;

  END;

  BEGIN

    DELETE from tu_assessment_address

    WHERE assessment_id = v_assessment_id

      and version_num = '0';

    EXCEPTION when no_data_found then NULL;

  END;

  BEGIN

    DELETE from tu_assessment_contact

    WHERE assessment_id = v_assessment_id

      and version_num = '0';

    EXCEPTION when no_data_found then NULL;

  END;

--

  BEGIN
  LOOP
    FETCH calc_marketshare INTO  rec;
    EXIT WHEN calc_marketshare%NOTFOUND;
--
    BEGIN
      -- if it's not ACONLY we have to calculate volume, market share, previous share, and delta
      -- if it is ACONLY, we just have to pull those values out of the marketshare record
      IF v_marketshareType <> 'ACONLY' THEN
        -- get total volume removed
         select sum(removal_qty)

           into v_total_volume

          from  tu_company a,

                   tu_permit b,

                   tu_permit_period c,

                   tu_rpt_period d,

                   tu_submitted_form f,

                   tu_form_detail g,

                   tu_tobacco_class h

           where a.company_id=b.company_id

             and B.PERMIT_ID=C.PERMIT_ID

             and D.PERIOD_ID=C.PERIOD_ID

             and F.PERMIT_ID=C.PERMIT_ID

             and F.PERIOD_ID=C.PERIOD_ID

             and G.FORM_ID=F.FORM_ID

             and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID

             and F.FORM_TYPE_CD = '3852'

             and h.tobacco_class_nm = 'Cigars'

             and D.fiscal_yr=v_year
             --and d.quarter=v_qtr

             and a.company_id=rec.company_id

             and h.tobacco_class_id=rec.tobacco_class_id

-------------------------Added below Code as part of Story CTPTUFA-4834-------------
              and (a.company_id,b.permit_num) not in
                                  (    Select distinct company_id,permit_num
                                       from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= v_year+1
                                        and   ASSMNT_QTR=v_qtr
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR <v_year+1
                                            or
                                           (ASSMNT_QTR <=v_qtr
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=v_year+1)
                                          )
                                        )
                                   );
------------------------------------------------------------------------------------


          -- calculate the current share
          if v_period_tot_taxes <> '0' then
            v_current_share := trunc(rec.TOTTAX/v_period_tot_taxes,6);
          else
            v_current_share := 0;
          end if;
          -- query for the previous share
          BEGIN
            select share_tot_taxes

            into v_previous_share

            from tu_market_share

            where company_id = rec.company_id

            and assessment_id = v_assessment_id

            and tobacco_class_id = rec.tobacco_class_id

            and version_num = v_previous_version;

          EXCEPTION
            when others then
              v_previous_share := 0;
          END;

        --DBMS_OUTPUT.PUT_LINE('PREVIOUS SHARE:' || v_previous_share);
--DBMS_OUTPUT.PUT_LINE('address ONLY BEFORE ELSE');
      ELSE
  --    DBMS_OUTPUT.PUT_LINE('ADDRESS ONLY IF ELSE');
        v_total_volume := rec.tot_vol_removed;
        v_current_share := rec.share_tot_taxes;
        v_previous_share := rec.previous_share;
        v_delta_share := rec.delta_share;
      END IF;
    END;

  --DBMS_OUTPUT.PUT_LINE('ADDRESS ONLY TEST 1');
  
    BEGIN

        -- insert the existing company addresses

      merge into tu_assessment_address rp

         using (select a.company_id, p.assessment_id, p.assessment_version, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total

                  from tu_address a, tu_assessment p

                  where a.company_id = rec.company_id

                  and p.assessment_id = v_assessment_id

                  and a.address_type_cd='PRIM') ad

            on (rp.company_id = ad.company_id

              and rp.assessment_id = ad.assessment_id

              and rp.address_type_cd = ad.address_type_cd

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.street_address = ad.street_address, rp.suite = ad.suite, rp.city = ad.city, rp.state = ad.state, rp.postal_cd = ad.postal_cd, rp.fax_num = ad.fax_num, rp.cntry_dial_cd = ad.cntry_dial_cd, rp.country_cd = ad.country_cd

        when not matched then insert

         (company_id, assessment_id, version_num, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total)

          values (rec.company_id, v_assessment_id, v_version_num, ad.address_type_cd, ad.street_address, ad.suite, ad.attention, ad.city, ad.state, ad.province, ad.postal_cd, ad.fax_num, ad.cntry_dial_cd, ad.country_cd, ad.hash_total);

    -- insert the existing company contacts

       merge into tu_assessment_contact rp

         using (select c.contact_id, c.company_id, p.assessment_id, p.assessment_version, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, c.hash_total

                  from primary_contact_vw c, tu_assessment p

                  where c.company_id = rec.company_id

                  and p.assessment_id = v_assessment_id) ct

            on (rp.company_id = ct.company_id

              and rp.assessment_id = ct.assessment_id

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.contact_id = ct.contact_id, rp.last_nm = ct.last_nm, rp.first_nm = ct.first_nm, rp.phone_num = ct.phone_num, rp.phone_ext = ct.phone_ext, rp.email_address = ct.email_address, rp.country_cd = ct.country_cd, rp.fax_num = ct.fax_num

        when not matched then insert

         (company_id, contact_id, assessment_id, version_num, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total)

          values (rec.company_id, ct.contact_id, v_assessment_id, v_version_num, ct.last_nm, ct.first_nm, ct.phone_num, ct.phone_ext, ct.cntry_dial_cd, ct.email_address, ct.country_cd, ct.fax_num, ct.hash_total);

    END;

  --DBMS_OUTPUT.PUT_LINE('COMPANY=' || rec.company_id);

  -- check address for changes

    BEGIN

      SELECT UNIQUE 'N'

      INTO v_address_changed

      FROM TU_ASSESSMENT a

      INNER JOIN TU_ASSESSMENT_VERSION av on a.assessment_id=av.assessment_id

      INNER JOIN

       ( SELECT * FROM

        (SELECT c.company_id, nvl(aa.assessment_id, v_contact_assessment_id) as assessment_id, nvl(aa.version_num, v_contact_version) as version_num, nvl(hash_total, 1) as assessment_hash

          FROM TU_COMPANY c LEFT OUTER JOIN TU_ASSESSMENT_ADDRESS aa on c.company_id = aa.company_id

          WHERE c.company_id=rec.company_id)

        WHERE version_num=v_contact_version

        AND assessment_id=v_contact_assessment_id) aa on aa.assessment_id=av.assessment_id

      INNER JOIN

       ( SELECT c.company_id, nvl(hash_total, 1) as address_hash

        FROM TU_COMPANY c LEFT OUTER JOIN TU_ADDRESS a on c.company_id = a.company_id

        WHERE c.company_id=rec.company_id ) ad on aa.company_id = ad.company_id

      WHERE aa.assessment_hash = ad.address_hash;

        EXCEPTION

          when no_data_found then

          --DBMS_OUTPUT.PUT_LINE('EXCEPTION: ADDRESS CHANGED');

            v_address_changed := 'Y';

    END;
  -- now check contact for changes

    BEGIN

      SELECT UNIQUE 'N'

      INTO v_contact_changed

      FROM TU_ASSESSMENT a

      INNER JOIN TU_ASSESSMENT_VERSION av on a.assessment_id=av.assessment_id

      INNER JOIN

       ( SELECT * FROM

        (SELECT c.company_id, nvl(aa.assessment_id, v_contact_assessment_id) as assessment_id, nvl(aa.version_num, v_contact_version) as version_num, nvl(hash_total, 1) as assessment_hash

          FROM TU_COMPANY c LEFT OUTER JOIN TU_ASSESSMENT_CONTACT aa on c.company_id = aa.company_id

          WHERE c.company_id=rec.company_id)

        WHERE version_num=v_contact_version

        AND assessment_id=v_contact_assessment_id) aa on aa.assessment_id=av.assessment_id

      INNER JOIN

       ( SELECT c.company_id, nvl(hash_total, 1) as contact_hash

        FROM TU_COMPANY c LEFT OUTER JOIN TU_CONTACT a on c.company_id = a.company_id

        WHERE c.company_id=rec.company_id ) ad on aa.company_id = ad.company_id

      WHERE aa.assessment_hash = ad.contact_hash;

        EXCEPTION

          when no_data_found then

          --DBMS_OUTPUT.PUT_LINE('EXCEPTION: CONTACT CHANGED');
            if v_contact_version > 0 then
              v_contact_changed := 'Y';
            else
              v_contact_changed := 'N';
            end if;

    END;
--
    BEGIN

  --DBMS_OUTPUT.PUT_LINE('ADDRESS ONLY TEST 2');
      
--DBMS_OUTPUT.PUT_LINE('entering into insert '||v_current_share);
      -- insert into market share table
      if v_current_share > 0 OR v_previous_share > 0 THEN
        -- calculate delta
        v_delta_share := v_current_share - v_previous_share;
        v_rank := v_rank+1;

    BEGIN
  --DBMS_OUTPUT.PUT_LINE('ADDRESS ONLY TEST 3');
--DBMS_OUTPUT.PUT_LINE('insert Market Share');
        INSERT into tu_market_share

          (assessment_id, company_id, tobacco_class_id,

           version_num, ms_rank,

           tot_vol_removed, tot_taxes_paid, share_tot_taxes,

           previous_share, delta_share, created_dt, author,original_corrected, address_changed_flag, contact_changed_flag,ein,company_name
           )

        VALUES

          (v_assessment_id, rec.company_id, rec.tobacco_class_id,

           v_version_num, v_rank,

           v_total_volume, rec.TOTTAX,

           v_current_share, v_previous_share, v_delta_share, sysdate, v_author,

           Case when v_previous_share>0 then 'C' 
                when (v_previous_share is null or v_previous_share=0) then 'O'
                else 'C' end,

           v_address_changed, v_contact_changed,rec.ein,rec.legal_nm
           );

      EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
                UPDATE tu_market_share
                 SET ms_rank=v_rank,
                     tot_vol_removed=v_total_volume,
                     tot_taxes_paid=rec.TOTTAX, share_tot_taxes=v_current_share,
                     previous_share=v_previous_share, delta_share=v_delta_share,
                     ein=rec.ein,company_name=rec.legal_nm
                 WHERE assessment_id=v_assessment_id
                 AND company_id=rec.company_id
                 AND tobacco_class_id=rec.tobacco_class_id
                 AND version_num=v_version_num;

        END;

      end if;

    END;

    COMMIT;

--*/

    END LOOP;
    -- end loop, close cursor
    CLOSE calc_marketshare;

 ------------------------Start of changes for ST# 2699-------
--DBMS_OUTPUT.PUT_LINE('Start of Story');

 BEGIN
--DBMS_OUTPUT.PUT_LINE('Start of Story v_assessment_id '||v_assessment_id);
  BEGIN
        Select max(Version_num)
        INTO v_pre_ver
        FROM TU_market_share
        WHERE assessment_id=v_assessment_id
        and version_num not in(  Select max(Version_num)
        FROM TU_market_share
        WHERE assessment_id=v_assessment_id) ;
  END;
  FOR i_val IN cur_pv (v_assessment_id,v_previous_version)
      LOOP

    BEGIN
        Select 'X'
        INTO V_exist
        FROM tu_market_share
        WHERE assessment_id=i_val.assessment_id
        and version_num=v_version_num
        AND company_id=i_val.company_id
        and TOBACCO_CLASS_ID=i_val.TOBACCO_CLASS_ID;
    EXCEPTION
     WHEN others then V_exist:='A';
    END;

    --DBMS_OUTPUT.PUT_LINE('Entered into Story loop V_exist '||V_exist);

    IF  V_exist <>'X'
    THEN

          BEGIN
          Select 'X'
          into v_data_exist
          from tu_market_share
          where assessment_id=v_assessment_id
          and company_id=i_val.company_id
          and tobacco_class_id=i_val.tobacco_class_id
          and version_num=v_version_num;
          EXCEPTION
               when others then
               v_data_exist:='A';
          END;

if v_data_exist<>'X'
then
------below "if" is used for eliminating Company having previous share as null and continue to appear in market share new version---------
if (nvl(i_val.share_tot_taxes,0)>0)
then

BEGIN

INSERT into tu_market_share
          (assessment_id,ein, company_id, tobacco_class_id,tot_vol_removed, tot_taxes_paid,PREVIOUS_SHARE,
          version_num,delta_share,original_corrected, address_changed_flag, contact_changed_flag,company_name
           )
VALUES
          (v_assessment_id,i_val.ein, i_val.company_id, i_val.tobacco_class_id,'','',i_val.share_tot_taxes,v_version_num,
           0-abs(i_val.share_tot_taxes),
           i_val.original_corrected, i_val.address_changed_flag, i_val.contact_changed_flag,
           i_val.company_name
           );
          -- DBMS_OUTPUT.PUT_LINE('insert successfull');
Exception
When Others then
--DBMS_OUTPUT.PUT_LINE('insert failed'||sqlerrm);
null;
end;

    BEGIN

        -- insert the existing company addresses

      merge into tu_assessment_address rp

         using (select a.company_id, p.assessment_id, p.assessment_version, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total

                  from tu_address a, tu_assessment p

                  where a.company_id = i_val.company_id

                  and p.assessment_id = v_assessment_id

                  and a.address_type_cd='PRIM') ad

            on (rp.company_id = ad.company_id

              and rp.assessment_id = ad.assessment_id

              and rp.address_type_cd = ad.address_type_cd

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.street_address = ad.street_address, rp.suite = ad.suite, rp.city = ad.city, rp.state = ad.state, rp.postal_cd = ad.postal_cd, rp.fax_num = ad.fax_num, rp.cntry_dial_cd = ad.cntry_dial_cd, rp.country_cd = ad.country_cd

        when not matched then insert

         (company_id, assessment_id, version_num, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total)

          values (i_val.company_id, v_assessment_id, v_version_num, ad.address_type_cd, ad.street_address, ad.suite, ad.attention, ad.city, ad.state, ad.province, ad.postal_cd, ad.fax_num, ad.cntry_dial_cd, ad.country_cd, ad.hash_total);



    -- insert the existing company contacts

       merge into tu_assessment_contact rp

         using (select c.contact_id, c.company_id, p.assessment_id, p.assessment_version, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total

                  from primary_contact_vw c, tu_assessment p

                  where c.company_id = i_val.company_id

                  and p.assessment_id = v_assessment_id) ct

            on (rp.company_id = ct.company_id

              and rp.assessment_id = ct.assessment_id

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.contact_id = ct.contact_id, rp.last_nm = ct.last_nm, rp.first_nm = ct.first_nm, rp.phone_num = ct.phone_num, rp.phone_ext = ct.phone_ext, rp.email_address = ct.email_address, rp.country_cd = ct.country_cd, rp.fax_num = ct.fax_num

        when not matched then insert

         (company_id, contact_id, assessment_id, version_num, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total)

          values (i_val.company_id, ct.contact_id, v_assessment_id, v_version_num, ct.last_nm, ct.first_nm, ct.phone_num, ct.phone_ext, ct.cntry_dial_cd, ct.email_address, ct.country_cd, ct.fax_num, ct.hash_total);

    END;
end if;
end if;
end if;
commit;
    END LOOP;

-------------------------Added below Code as part of Story CTPTUFA-4834-------------
IF v_submitind = 'Y' then

  BEGIN

    UPDATE TU_PERMIT_EXCL_AFFECT_ASSMNTS
    SET ASSESSMENT_STATUS='COMPLETE'
    where ASSMNT_RPT_DATA_YR=v_year
    and ASSMNT_RPT_DATA_QTR=v_qtr
    AND ASSESSMENT_STATUS='RE-RUN'
    AND ASSMNT_TYPE='CIQT';


  EXCEPTION
  WHEN NO_DATA_FOUND then null;

  END;

    COMMIT;

END IF;
-------------------------

END;
------------------------END of changes for ST#2699----------

  END;

END;

END calc_cigar_market_share;

/

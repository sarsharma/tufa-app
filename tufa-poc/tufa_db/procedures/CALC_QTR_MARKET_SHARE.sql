--------------------------------------------------------
--  DDL for Procedure CALC_QTR_MARKET_SHARE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."CALC_QTR_MARKET_SHARE" (assess_yr NUMBER, assess_qtr NUMBER, author VARCHAR2) AS

BEGIN

DECLARE

  type tot_taxes IS VARRAY(5) OF NUMBER(14,2);
  v_tot_pipe_taxes tot_taxes := tot_taxes(0.0,0.0,0.0,0.0,0.0);
  v_period_tot_pipe_taxes NUMBER(14,2) := 0;
  v_period_tot_chew_taxes NUMBER(14,2) := 0;
  v_period_tot_snuff_taxes NUMBER(14,2) := 0;
  v_period_tot_cigarette_taxes NUMBER(14,2) := 0;
  v_period_tot_ryo_taxes NUMBER(14,2) := 0;
  v_previous_share NUMBER(8,6) := 0;
  v_previous_version NUMBER(2) := 0;
  v_original_corrected CHAR(1) := 'O';
  v_max_version NUMBER(2) := 0;
  v_current_share NUMBER(8,6) := 0;
  v_delta_share NUMBER(8,6) := 0;
  v_total_volume NUMBER(20,6) := 0;
  v_assessment_id NUMBER := 0;
  v_year NUMBER(4) := assess_yr;
  v_qtr NUMBER(1) := assess_qtr;
  v_author VARCHAR2(50) :=author;
  v_marketshareType VARCHAR2(6) := 'FULLMS';
  v_submitind CHAR(1);
  v_version_num NUMBER(2) := '1';
  v_cigarette_rank NUMBER(4) := 0;
  v_chew_rank NUMBER(4) := 0;
  v_ryo_rank NUMBER(4) := 0;
  v_snuff_rank NUMBER(4) := 0;
  v_pipe_rank NUMBER(4) := 0;
  v_pre_ver number;
  v_exist  varchar2(5);
  v_data_exist varchar2(5);

CURSOR CUR_PV (p_assignment_id number, p_version_num number)
  IS
  SELECT * from tu_market_share
  where ASSESSMENT_ID=p_assignment_id
  and version_num=p_version_num
  --and TOBACCO_CLASS_ID=7
 -- and delta_share not like '-%'
  ;

  CURSOR calc_marketshare IS
/*SELECT *
 FROM
   (
     SELECT recs.company_id,recs.ein,DETAILS.TOBACCO_CLASS_ID,DETAILS.tobacco_class_nm,SUM(details.taxes_paid) as TOTTAX
     FROM
        ((SELECT a.company_id,a.ein ,c.period_id,C.PERMIT_ID
          FROM tu_company a,
                 tu_permit b,
                 tu_permit_period c,
                 tu_rpt_period d
          WHERE a.company_id=b.company_id
          AND B.PERMIT_ID=C.PERMIT_ID
          AND D.PERIOD_ID=C.PERIOD_ID
          AND D.fiscal_yr=V_YEAR
          AND D.QUARTER=V_QTR
          ) recs
          LEFT OUTER JOIN
          (
            SELECT sf.permit_id, sf.period_id,
            fd.removal_qty, fd.taxes_paid,H.TOBACCO_CLASS_NM,H.TOBACCO_CLASS_ID
            FROM tu_submitted_form sf
            INNER JOIN tu_form_detail fd on sf.form_id = fd.form_id
            inner join tu_tobacco_class h on H.TOBACCO_CLASS_ID=fd.TOBACCO_CLASS_ID
            where sf.form_type_cd = '3852'
            and h.class_type_cd <> 'SPTP'
            and h.PARENT_CLASS_ID <> 8
          ) details on
                  recs.permit_id = details.permit_id
              AND recs.period_id = details.period_id
               )
          group by  recs.company_id,recs.ein,DETAILS.tobacco_class_id,DETAILS.tobacco_class_nm)
       --   where company_id=255
          order by TOTTAX DESC;
    */
    select *

     FROM

      (select a.company_id,a.legal_nm,a.ein, h.tobacco_class_id,

               tobacco_class_nm, SUM(taxes_paid) as TOTTAX

        from tu_company a,

                 tu_permit b,

                 tu_permit_period c,

                 tu_rpt_period d,

                 tu_submitted_form f,

                 tu_form_detail g,

                 tu_tobacco_class h

         where a.company_id=b.company_id

           and B.PERMIT_ID=C.PERMIT_ID

           and D.PERIOD_ID=C.PERIOD_ID

           and F.PERMIT_ID=C.PERMIT_ID

           and F.PERIOD_ID=C.PERIOD_ID

           and G.FORM_ID=F.FORM_ID

           and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID

           and h.class_type_cd <> 'SPTP'

           and h.tobacco_class_nm <> 'Cigars'

           and f.form_type_cd='3852'

           and D.fiscal_yr=v_year

           and D.QUARTER=v_qtr
-------------------------Added below Code as part of Story CTPTUFA-4833-------------
     and (a.company_id,b.permit_num) not in
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   )

------------------------------------------------------------------------------------
          group by a.company_id,a.legal_nm,a.ein, h.tobacco_class_id,

                 tobacco_class_nm ) recs

         order by recs.TOTTAX DESC;


BEGIN

-- get assessment information

  BEGIN

    SELECT assessment_id, assessment_yr, assessment_qtr,

           submitted_ind

      INTO v_assessment_id, v_year, v_qtr, v_submitind

    FROM tu_assessment

    WHERE assessment_yr = v_year

      and assessment_qtr = v_qtr

      and assessment_type = 'QTRY';

    exception when no_data_found then v_assessment_id := '0';

  END;


-- get existing version information

  BEGIN

    SELECT max(version_num)

    INTO v_max_version

    from tu_market_share

    where assessment_id = v_assessment_id;

    EXCEPTION

      when no_data_found then

        v_max_version := -1;

  END;

  if v_max_version IS NULL then
    v_max_version := -1;
  end if;

    --DBMS_OUTPUT.PUT_LINE('MAX VERSION:' || v_max_version);

-- calculate current version and previous version

  BEGIN

    -- highest version will always be the previous

    v_previous_version := v_max_version;

    IF v_submitind = 'N' then
      -- we are generating version 0 (temporary version)
      v_version_num := 0;
    ELSIF v_submitind = 'Y' then
      -- we are generating the next version
      v_version_num := v_previous_version + 1;
    END IF;

    -- upsert assessment version
    MERGE INTO TU_ASSESSMENT_VERSION av using dual on (assessment_id = v_assessment_id and version_num=v_version_num)
    WHEN MATCHED THEN
      UPDATE SET created_by=v_author, created_dt=SYSDATE
    WHEN NOT MATCHED THEN
      INSERT (assessment_id, version_num, created_by, created_dt)
      VALUES (v_assessment_id, v_version_num, v_author, SYSDATE);

  END;

  -- determine if original or corrected.
  IF v_previous_version > 0 THEN
    v_original_corrected := 'C';
  END IF;


-- now delete all version zero (temporary) information

-- including marketshare, addresses, and contacts

  BEGIN

    DELETE from tu_market_share

    WHERE assessment_id = v_assessment_id

      and version_num = '0';

    EXCEPTION when no_data_found then NULL;

  END;

  BEGIN

    DELETE from tu_assessment_address

    WHERE assessment_id = v_assessment_id

      and version_num = '0';

    EXCEPTION when no_data_found then NULL;

  END;

  BEGIN

    DELETE from tu_assessment_contact

    WHERE assessment_id = v_assessment_id

      and version_num = '0';

    EXCEPTION when no_data_found then NULL;

  END;

--

  BEGIN

    select sum(taxes_paid)

      into v_period_tot_pipe_taxes

    from  tu_company a,

             tu_permit b,

             tu_permit_period c,

             tu_rpt_period d,

             tu_submitted_form f,

             tu_form_detail g,

             tu_tobacco_class h

     where a.company_id=b.company_id

       and B.PERMIT_ID=C.PERMIT_ID

       and D.PERIOD_ID=C.PERIOD_ID

       and F.PERMIT_ID=C.PERMIT_ID

         and F.PERIOD_ID=C.PERIOD_ID

       and G.FORM_ID=F.FORM_ID

       and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID

       and F.FORM_TYPE_CD = '3852'

       and h.class_type_cd <> 'SPTP'

       and h.tobacco_class_nm = 'Pipe'

       and D.fiscal_yr=v_year

       and D.QUARTER=v_qtr
-------------------------Added below Code as part of Story CTPTUFA-4833-------------
     and (a.company_id,b.permit_num) not in
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   );

------------------------------------------------------------------------------------
  END;

--

  BEGIN

    select sum(taxes_paid)

      into v_period_tot_chew_taxes

    from  tu_company a,

             tu_permit b,

             tu_permit_period c,

             tu_rpt_period d,

             tu_submitted_form f,

             tu_form_detail g,

             tu_tobacco_class h

     where a.company_id=b.company_id

       and B.PERMIT_ID=C.PERMIT_ID

       and D.PERIOD_ID=C.PERIOD_ID

       and F.PERMIT_ID=C.PERMIT_ID

         and F.PERIOD_ID=C.PERIOD_ID

       and G.FORM_ID=F.FORM_ID

       and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID

       and F.FORM_TYPE_CD = '3852'

       and h.class_type_cd <> 'SPTP'

       and h.tobacco_class_nm = 'Chew'

       and D.fiscal_yr=v_year

       and D.QUARTER=v_qtr

-------------------------Added below Code as part of Story CTPTUFA-4833-------------
     and (a.company_id,b.permit_num) not in
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   );
------------------------------------------------------------------------------------
  END;

--

  BEGIN

    select sum(taxes_paid)

      into v_period_tot_snuff_taxes

    from  tu_company a,

             tu_permit b,

             tu_permit_period c,

             tu_rpt_period d,

             tu_submitted_form f,

             tu_form_detail g,

             tu_tobacco_class h

     where a.company_id=b.company_id

       and B.PERMIT_ID=C.PERMIT_ID

       and D.PERIOD_ID=C.PERIOD_ID

       and F.PERMIT_ID=C.PERMIT_ID

         and F.PERIOD_ID=C.PERIOD_ID

       and G.FORM_ID=F.FORM_ID

       and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID

       and F.FORM_TYPE_CD = '3852'

       and h.class_type_cd <> 'SPTP'

       and h.tobacco_class_nm = 'Snuff'

       and D.fiscal_yr=v_year

       and D.QUARTER=v_qtr
-------------------------Added below Code as part of Story CTPTUFA-4833-------------
     and (a.company_id,b.permit_num) not in
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID =2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   );

------------------------------------------------------------------------------------
  END;

--

  BEGIN

    select sum(taxes_paid)

      into v_period_tot_cigarette_taxes

    from  tu_company a,

             tu_permit b,

             tu_permit_period c,

             tu_rpt_period d,

             tu_submitted_form f,

             tu_form_detail g,

             tu_tobacco_class h

     where a.company_id=b.company_id

       and B.PERMIT_ID=C.PERMIT_ID

       and D.PERIOD_ID=C.PERIOD_ID

       and F.PERMIT_ID=C.PERMIT_ID

         and F.PERIOD_ID=C.PERIOD_ID

       and G.FORM_ID=F.FORM_ID

       and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID

       and F.FORM_TYPE_CD = '3852'

       and h.class_type_cd <> 'SPTP'

       and h.tobacco_class_nm = 'Cigarettes'

       and D.fiscal_yr=v_year

       and D.QUARTER=v_qtr
-------------------------Added below Code as part of Story CTPTUFA-4833-------------
     and (a.company_id,b.permit_num) not in
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   );

------------------------------------------------------------------------------------
  END;

--

  BEGIN

    select sum(taxes_paid)

      into v_period_tot_ryo_taxes

    from  tu_company a,

             tu_permit b,

             tu_permit_period c,

             tu_rpt_period d,

             tu_submitted_form f,

             tu_form_detail g,

             tu_tobacco_class h

     where a.company_id=b.company_id

       and B.PERMIT_ID=C.PERMIT_ID

       and D.PERIOD_ID=C.PERIOD_ID

       and F.PERMIT_ID=C.PERMIT_ID

         and F.PERIOD_ID=C.PERIOD_ID

       and G.FORM_ID=F.FORM_ID

       and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID

       and F.FORM_TYPE_CD = '3852'

       and h.class_type_cd <> 'SPTP'

       and h.tobacco_class_nm = 'Roll-Your-Own'

       and D.fiscal_yr=v_year

       and D.QUARTER=v_qtr

-------------------------Added below Code as part of Story CTPTUFA-4833-------------
     and (a.company_id,b.permit_num) not in
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   );

------------------------------------------------------------------------------------
  END;
--

  BEGIN

    For rec in calc_marketshare LOOP

--

    BEGIN

   select sum(removal_qty)

     into v_total_volume

    from  tu_company a,

             tu_permit b,

             tu_permit_period c,

             tu_rpt_period d,

             tu_submitted_form f,

             tu_form_detail g,

             tu_tobacco_class h

     where a.company_id=b.company_id

       and B.PERMIT_ID=C.PERMIT_ID

       and D.PERIOD_ID=C.PERIOD_ID

       and F.PERMIT_ID=C.PERMIT_ID

       and F.PERIOD_ID=C.PERIOD_ID

       and G.FORM_ID=F.FORM_ID

       and H.TOBACCO_CLASS_ID=G.TOBACCO_CLASS_ID

       and F.FORM_TYPE_CD = '3852'

       and h.class_type_cd <> 'SPTP'

       and D.fiscal_yr=v_year

       and D.QUARTER=v_qtr

       and a.company_id=rec.company_id

      and h.tobacco_class_id=rec.tobacco_class_id

-------------------------Added below Code as part of Story CTPTUFA-4833-------------
     and (a.company_id,b.permit_num) not in
                                  (    Select distinct company_id,permit_num
                                        from PERMIT_EXCL_STATUS_VW
                                        where
                                        (ASSMNT_YR= Decode(v_qtr,4,v_year+1,v_year)
                                        and   ASSMNT_QTR=Decode(v_qtr,4,1,v_qtr+1)
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and
                                          (
                                           ASSMNT_YR<Decode(v_qtr,4,v_year+1,v_year)
                                            or
                                           (ASSMNT_QTR <=(Decode(v_qtr,4,1,v_qtr+1)) and ASSMNT_YR=Decode(v_qtr,4,v_year+1,v_year))
                                          )
                                        )
                                   );

------------------------------------------------------------------------------------
    END;

    BEGIN

        -- insert the existing company addresses

      merge into tu_assessment_address rp

         using (select a.company_id, p.assessment_id, p.assessment_version, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total

                  from tu_address a, tu_assessment p

                  where a.company_id = rec.company_id

                  and p.assessment_id = v_assessment_id

                  and a.address_type_cd='PRIM') ad

            on (rp.company_id = ad.company_id

              and rp.assessment_id = ad.assessment_id

              and rp.address_type_cd = ad.address_type_cd

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.street_address = ad.street_address, rp.suite = ad.suite, rp.city = ad.city, rp.state = ad.state, rp.postal_cd = ad.postal_cd, rp.fax_num = ad.fax_num, rp.cntry_dial_cd = ad.cntry_dial_cd, rp.country_cd = ad.country_cd

        when not matched then insert

         (company_id, assessment_id, version_num, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total)

          values (rec.company_id, v_assessment_id, v_version_num, ad.address_type_cd, ad.street_address, ad.suite, ad.attention, ad.city, ad.state, ad.province, ad.postal_cd, ad.fax_num, ad.cntry_dial_cd, ad.country_cd, ad.hash_total);

    -- insert the existing company contacts

       merge into tu_assessment_contact rp

         using (select c.contact_id, c.company_id, p.assessment_id, p.assessment_version, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total

                  from primary_contact_vw c, tu_assessment p

                  where c.company_id = rec.company_id

                  and p.assessment_id = v_assessment_id) ct

            on (rp.company_id = ct.company_id

              and rp.assessment_id = ct.assessment_id

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.contact_id = ct.contact_id, rp.last_nm = ct.last_nm, rp.first_nm = ct.first_nm, rp.phone_num = ct.phone_num, rp.phone_ext = ct.phone_ext, rp.email_address = ct.email_address, rp.country_cd = ct.country_cd, rp.fax_num = ct.fax_num

        when not matched then insert

         (company_id, contact_id, assessment_id, version_num, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total)

          values (rec.company_id, ct.contact_id, v_assessment_id, v_version_num, ct.last_nm, ct.first_nm, ct.phone_num, ct.phone_ext, ct.cntry_dial_cd, ct.email_address, ct.country_cd, ct.fax_num, ct.hash_total);



    END;

--

    BEGIN

    IF rec.tobacco_class_nm = 'Chew' then

      -- calculate the current share

      if v_period_tot_chew_taxes <> '0' then

        v_current_share := trunc(rec.TOTTAX/v_period_tot_chew_taxes,6);

      else

        v_current_share := 0;

      end if;

      -- query for the previous share

      BEGIN

        select share_tot_taxes

        into v_previous_share

        from tu_market_share

        where company_id = rec.company_id

        and assessment_id = v_assessment_id

        and tobacco_class_id = rec.tobacco_class_id

        and version_num = v_previous_version;

        EXCEPTION
          when others then
              v_previous_share := 0;

      END;

      -- calculate delta

      v_delta_share := v_current_share - nvl(v_previous_share,0);

      v_chew_rank := v_chew_rank+1;

      -- insert into market share table
      if v_current_share > 0 OR v_previous_share > 0 THEN

     BEGIN
     INSERT into tu_market_share

          (assessment_id, company_id, tobacco_class_id,

           version_num, ms_rank,

           tot_vol_removed, tot_taxes_paid, share_tot_taxes,

           previous_share, delta_share, created_dt,author, original_corrected,ein,company_name)

        VALUES

          (v_assessment_id, rec.company_id, rec.tobacco_class_id,

           v_version_num, v_chew_rank,

           v_total_volume, rec.TOTTAX,

           v_current_share, v_previous_share, v_delta_share, sysdate,v_author, v_original_corrected,rec.ein,rec.legal_nm);
        EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
              UPDATE tu_market_share
                 SET ms_rank=v_chew_rank,
                     tot_vol_removed=v_total_volume,
                     tot_taxes_paid=rec.TOTTAX, share_tot_taxes=v_current_share,
                     previous_share=v_previous_share, delta_share=v_delta_share,
                     ein=rec.ein,company_name=rec.legal_nm
                 WHERE assessment_id=v_assessment_id
                 AND company_id=rec.company_id
                 AND tobacco_class_id=rec.tobacco_class_id
                 AND version_num=v_version_num;
                -- null;
      --COMMIT;
          END;

      end if;

    ELSIF rec.tobacco_class_nm = 'Snuff' then

      -- calculate the current share

      if v_period_tot_snuff_taxes <> '0' then

        v_current_share := trunc(rec.TOTTAX/v_period_tot_snuff_taxes,6);

      else

        v_current_share := 0;

      end if;

      -- query for the previous share

      BEGIN

        select share_tot_taxes

        into v_previous_share

        from tu_market_share

        where company_id = rec.company_id

        and assessment_id = v_assessment_id

        and tobacco_class_id = rec.tobacco_class_id

        and version_num = v_previous_version;

        EXCEPTION
          when others then
              v_previous_share := 0;

      END;

      -- calculate delta

      v_delta_share := v_current_share - nvl(v_previous_share,0);

      v_snuff_rank := v_snuff_rank+1;

      if v_current_share > 0 OR v_previous_share > 0 THEN

       BEGIN
        INSERT into tu_market_share

          (assessment_id, company_id, tobacco_class_id,

           version_num, ms_rank,

           tot_vol_removed, tot_taxes_paid, share_tot_taxes,

           previous_share, delta_share,

           created_dt, author, original_corrected,ein,company_name)

        VALUES

          (v_assessment_id, rec.company_id, rec.tobacco_class_id,
           v_version_num, v_snuff_rank,

           v_total_volume, rec.TOTTAX,

           v_current_share, v_previous_share, v_delta_share, sysdate, v_author, v_original_corrected,rec.ein,rec.legal_nm);

      EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
                UPDATE tu_market_share
                 SET ms_rank=v_snuff_rank,
                     tot_vol_removed=v_total_volume,
                     tot_taxes_paid=rec.TOTTAX, share_tot_taxes=v_current_share,
                     previous_share=v_previous_share, delta_share=v_delta_share,
                     ein=rec.ein,company_name=rec.legal_nm
                 WHERE assessment_id=v_assessment_id
                 AND company_id=rec.company_id
                 AND tobacco_class_id=rec.tobacco_class_id
                 AND version_num=v_version_num;

   --null;
   --COMMIT;
     --     END;
      END;

      end if;

    ELSIF rec.tobacco_class_nm = 'Cigarettes' then

      -- calculate the current share

      if v_period_tot_cigarette_taxes <> '0' then


        v_current_share := trunc(rec.TOTTAX/v_period_tot_cigarette_taxes,6);

      else

        v_current_share := 0;

      end if;

      -- query for the previous share

      BEGIN

        select share_tot_taxes

        into v_previous_share

        from tu_market_share

        where company_id = rec.company_id

        and assessment_id = v_assessment_id

        and tobacco_class_id = rec.tobacco_class_id

        and version_num = v_previous_version;

        EXCEPTION
          when others then
              v_previous_share := 0;

      END;

      -- calculate delta

      v_delta_share := v_current_share - nvl(v_previous_share,0);

      v_cigarette_rank := v_cigarette_rank+1;

      if v_current_share > 0 OR v_previous_share > 0 THEN

      BEGIN

        INSERT into tu_market_share

          (assessment_id, company_id, tobacco_class_id,

           version_num, ms_rank,

           tot_vol_removed, tot_taxes_paid, share_tot_taxes,

           previous_share, delta_share,

           created_dt, author, original_corrected,ein,COMPANY_NAME)

        VALUES

          (v_assessment_id, rec.company_id, rec.tobacco_class_id,

           v_version_num, v_cigarette_rank,

           v_total_volume, rec.TOTTAX,

           v_current_share, v_previous_share, v_delta_share,

           sysdate,v_author, v_original_corrected,rec.ein,rec.legal_nm);

          EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
                UPDATE tu_market_share
                 SET ms_rank=v_cigarette_rank,
                     tot_vol_removed=v_total_volume,
                     tot_taxes_paid=rec.TOTTAX, share_tot_taxes=v_current_share,
                     previous_share=v_previous_share, delta_share=v_delta_share,
                     ein=rec.ein,company_name=rec.legal_nm
                 WHERE assessment_id=v_assessment_id
                 AND company_id=rec.company_id
                 AND tobacco_class_id=rec.tobacco_class_id
                 AND version_num=v_version_num;

        END;
      end if;

    ELSIF rec.tobacco_class_nm = 'Pipe' then

      -- calculate the current share

      if v_period_tot_pipe_taxes <> '0' then

        v_current_share := trunc(rec.TOTTAX/v_period_tot_pipe_taxes,6);

      else

        v_current_share := 0;

      end if;

      -- query for the previous share

      BEGIN

        select share_tot_taxes

        into v_previous_share

        from tu_market_share

        where company_id = rec.company_id

        and assessment_id = v_assessment_id

        and tobacco_class_id = rec.tobacco_class_id

        and version_num = v_previous_version;

        EXCEPTION
          when others then
              v_previous_share := 0;

      END;

      -- calculate delta
      v_delta_share := v_current_share - nvl(v_previous_share,0);

      v_pipe_rank := v_pipe_rank+1;

      if v_current_share > 0 OR v_previous_share > 0 THEN

      BEGIN
        INSERT into tu_market_share

          (assessment_id, company_id, tobacco_class_id,

           version_num, ms_rank,

           tot_vol_removed, tot_taxes_paid, share_tot_taxes,

           previous_share, delta_share,

           created_dt, author, original_corrected,ein,COMPANY_NAME)

        VALUES

          (v_assessment_id, rec.company_id, rec.tobacco_class_id,

           v_version_num, v_pipe_rank,

           v_total_volume, rec.TOTTAX,

           v_current_share, v_previous_share, v_delta_share,

           sysdate, v_author, v_original_corrected,rec.ein,rec.legal_nm);

        EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
                UPDATE tu_market_share
                 SET ms_rank=v_pipe_rank,
                     tot_vol_removed=v_total_volume,
                     tot_taxes_paid=rec.TOTTAX, share_tot_taxes=v_current_share,
                     previous_share=v_previous_share, delta_share=v_delta_share,
                     ein=rec.ein,company_name=rec.legal_nm
                 WHERE assessment_id=v_assessment_id
                 AND company_id=rec.company_id
                 AND tobacco_class_id=rec.tobacco_class_id
                 AND version_num=v_version_num;

        END;
      end if;

    ELSIF rec.tobacco_class_nm = 'Roll-Your-Own' then

      -- calculate the current share

      if v_period_tot_ryo_taxes <> '0' then

        v_current_share := trunc(rec.TOTTAX/v_period_tot_ryo_taxes,6);

      else

        v_current_share := 0;

      end if;

      -- query for the previous share

      BEGIN

        select share_tot_taxes

        into v_previous_share

        from tu_market_share

        where company_id = rec.company_id

        and assessment_id = v_assessment_id

        and tobacco_class_id = rec.tobacco_class_id

        and version_num = v_previous_version;

        EXCEPTION
          when others then
              v_previous_share := 0;

      END;

      -- calculate delta
      v_delta_share := v_current_share - nvl(v_previous_share,0);

      v_ryo_rank := v_ryo_rank+1;

      if v_current_share > 0 OR v_previous_share > 0 THEN


    BEGIN
      INSERT into tu_market_share

          (assessment_id, company_id, tobacco_class_id,

           version_num, ms_rank,

           tot_vol_removed, tot_taxes_paid, share_tot_taxes,

           previous_share, delta_share,

           created_dt,author, original_corrected,ein,company_name)

        VALUES

          (v_assessment_id, rec.company_id, rec.tobacco_class_id,

           v_version_num, v_ryo_rank,

           v_total_volume, rec.TOTTAX,

           v_current_share, v_previous_share, v_delta_share,

           sysdate,v_author, v_original_corrected,rec.ein,rec.legal_nm);

        EXCEPTION
           WHEN DUP_VAL_ON_INDEX THEN
                UPDATE tu_market_share
                 SET ms_rank=v_ryo_rank,
                     tot_vol_removed=v_total_volume,
                     tot_taxes_paid=rec.TOTTAX, share_tot_taxes=v_current_share,
                     previous_share=v_previous_share, delta_share=v_delta_share,
                     ein=rec.ein,company_name=rec.legal_nm
                 WHERE assessment_id=v_assessment_id
                 AND company_id=rec.company_id
                 AND tobacco_class_id=rec.tobacco_class_id
                 AND version_num=v_version_num;

        END;

      end if;

    END IF;

    END;

    COMMIT;

    END LOOP;

        ------------------------Start of changes for ST# 2699-------

 BEGIN
  BEGIN
        Select max(Version_num)
        INTO v_pre_ver
        FROM TU_market_share
        WHERE assessment_id=v_assessment_id
        and version_num not in(  Select max(Version_num)
        FROM TU_market_share
        WHERE assessment_id=v_assessment_id) ;
  END;

FOR i_val IN cur_pv (v_assessment_id,v_previous_version)
    LOOP
DBMS_OUTPUT.PUT_LINE('v_assessment_id '||v_assessment_id);
DBMS_OUTPUT.PUT_LINE('v_version_num '||v_version_num);
DBMS_OUTPUT.PUT_LINE('i_val.company_id '||i_val.company_id);
DBMS_OUTPUT.PUT_LINE('v_assessment_id '||v_assessment_id);
BEGIN
        Select 'X'
        INTO V_exist
        FROM tu_market_share
        WHERE assessment_id=i_val.assessment_id
        and version_num=v_version_num
        AND company_id=i_val.company_id
        and TOBACCO_CLASS_ID=i_val.TOBACCO_CLASS_ID;
    EXCEPTION
     when others then V_exist:='A';
    END;

    if V_exist <>'X'
        then

        begin
        Select 'X'
        into v_data_exist
        from tu_market_share
        where assessment_id=v_assessment_id
        and company_id=i_val.company_id
        and tobacco_class_id=i_val.tobacco_class_id
        and version_num=v_version_num;
        exception
             when others then
             v_data_exist:='A';
        end;

if v_data_exist<>'X'
then
------below "if" is used for eliminating Company having previous share as null and continue to appear in market share new version---------
if (nvl(i_val.share_tot_taxes,0)>0)
then
begin
    INSERT into tu_market_share
              (assessment_id,ein, company_id, tobacco_class_id,tot_vol_removed, tot_taxes_paid,PREVIOUS_SHARE,
              version_num,delta_share,original_corrected, address_changed_flag, contact_changed_flag,COMPANY_NAME
               )
    VALUES
              (v_assessment_id,i_val.ein, i_val.company_id, i_val.tobacco_class_id,'','',i_val.share_tot_taxes,v_version_num,
               0-abs(i_val.share_tot_taxes),
               i_val.original_corrected, i_val.address_changed_flag, i_val.contact_changed_flag,
               i_val.company_name
               );
    Exception
    When Others then
    NULL;
    end;

    BEGIN

        -- insert the existing company addresses

      merge into tu_assessment_address rp

         using (select a.company_id, p.assessment_id, p.assessment_version, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total

                  from tu_address a, tu_assessment p

                  where a.company_id = i_val.company_id

                  and p.assessment_id = v_assessment_id

                  and a.address_type_cd='PRIM') ad

            on (rp.company_id = ad.company_id

              and rp.assessment_id = ad.assessment_id

              and rp.address_type_cd = ad.address_type_cd

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.street_address = ad.street_address, rp.suite = ad.suite, rp.city = ad.city, rp.state = ad.state, rp.postal_cd = ad.postal_cd, rp.fax_num = ad.fax_num, rp.cntry_dial_cd = ad.cntry_dial_cd, rp.country_cd = ad.country_cd

        when not matched then insert

         (company_id, assessment_id, version_num, address_type_cd, street_address, suite, attention, city, state, province, postal_cd, fax_num, cntry_dial_cd, country_cd, hash_total)

          values (i_val.company_id, v_assessment_id, v_version_num, ad.address_type_cd, ad.street_address, ad.suite, ad.attention, ad.city, ad.state, ad.province, ad.postal_cd, ad.fax_num, ad.cntry_dial_cd, ad.country_cd, ad.hash_total);



    -- insert the existing company contacts

       merge into tu_assessment_contact rp

         using (select c.contact_id, c.company_id, p.assessment_id, p.assessment_version, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total

                  from primary_contact_vw c, tu_assessment p

                  where c.company_id = i_val.company_id

                  and p.assessment_id = v_assessment_id) ct

            on (rp.company_id = ct.company_id

              and rp.assessment_id = ct.assessment_id

              and rp.version_num = v_version_num)

--        when matched then

--         update set

--            rp.contact_id = ct.contact_id, rp.last_nm = ct.last_nm, rp.first_nm = ct.first_nm, rp.phone_num = ct.phone_num, rp.phone_ext = ct.phone_ext, rp.email_address = ct.email_address, rp.country_cd = ct.country_cd, rp.fax_num = ct.fax_num

        when not matched then insert

         (company_id, contact_id, assessment_id, version_num, last_nm, first_nm, phone_num, phone_ext, cntry_dial_cd, email_address, country_cd, fax_num, hash_total)

          values (i_val.company_id, ct.contact_id, v_assessment_id, v_version_num, ct.last_nm, ct.first_nm, ct.phone_num, ct.phone_ext, ct.cntry_dial_cd, ct.email_address, ct.country_cd, ct.fax_num, ct.hash_total);

    END;

end if;
end if;
end if;
commit;
    END LOOP;
-------------------------Added below Code as part of Story CTPTUFA-4833-------------
IF v_submitind = 'Y' then

  BEGIN

    UPDATE TU_PERMIT_EXCL_AFFECT_ASSMNTS
    SET ASSESSMENT_STATUS='COMPLETE'
    where ASSMNT_RPT_DATA_YR=v_year
    and ASSMNT_RPT_DATA_QTR=v_qtr
    AND ASSESSMENT_STATUS='RE-RUN'
    AND ASSMNT_TYPE='QTRY';

  EXCEPTION
  WHEN NO_DATA_FOUND then null;

  END;

    COMMIT;

END IF;
-------------------------

END;
------------------------END of changes for ST#2699----------

  END;

END;

END calc_qtr_market_share;

/

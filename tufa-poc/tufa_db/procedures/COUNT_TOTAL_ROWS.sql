--------------------------------------------------------
--  DDL for Procedure COUNT_TOTAL_ROWS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."COUNT_TOTAL_ROWS" (
    TABLENAME1   IN VARCHAR2 DEFAULT NULL,
    TABLENAME2   IN VARCHAR2 DEFAULT NULL,
    COLUMNNAME  IN VARCHAR2 DEFAULT NULL,
    COLUMNVALUE IN VARCHAR2 DEFAULT NULL,
    ADD_HINT    IN VARCHAR2 DEFAULT 'N',
    TTL_COUNT OUT INTEGER)
AS
  SQL_STMT   VARCHAR(1000);
  SQL_HINT   VARCHAR(200);
  WHERE_STMT VARCHAR(200);
  TTL_CNT_1 INTEGER;
  TTL_CNT_2 INTEGER;
BEGIN
  IF TABLENAME1  IS NOT NULL THEN
    IF COLUMNNAME IS NOT NULL AND COLUMNVALUE IS NOT NULL THEN
      WHERE_STMT  := ' WHERE '|| COLUMNNAME || '=' || COLUMNVALUE;
    END IF;
    /* Added this to give the option to add an SQL hint.
        Some times this is needed to improve the performance.
        Other times it can hinder the performance
        Nov 6, 2018 */
    IF ADD_HINT = 'Y' THEN
      SQL_HINT := ' /*+ NO_QUERY_TRANSFORMATION(' || TABLENAME1 || ') */';
    END IF;
    SQL_STMT := 'SELECT ' || SQL_HINT || ' COUNT(*) FROM ' || TABLENAME1 || WHERE_STMT;
    DBMS_OUTPUT.PUT_LINE(' WHERE STATEMENT ' ||WHERE_STMT);
    EXECUTE IMMEDIATE SQL_STMT INTO TTL_CNT_1;
  END IF;
  IF TABLENAME2  IS NOT NULL THEN
    IF COLUMNNAME IS NOT NULL AND COLUMNVALUE IS NOT NULL THEN
      WHERE_STMT  := ' WHERE '|| COLUMNNAME || '=' || COLUMNVALUE;
    END IF;
    /* Added this to give the option to add an SQL hint.
        Some times this is needed to improve the performance.
        Other times it can hinder the performance
        Nov 6, 2018 */
    IF ADD_HINT = 'Y' THEN
      SQL_HINT := ' /*+ NO_QUERY_TRANSFORMATION(' || TABLENAME2 || ') */';
    END IF;
    SQL_STMT := 'SELECT ' || SQL_HINT || ' COUNT(*) FROM ' || TABLENAME2 || WHERE_STMT;
   DBMS_OUTPUT.PUT_LINE(' WHERE STATEMENT ' ||WHERE_STMT);
    EXECUTE IMMEDIATE SQL_STMT INTO TTL_CNT_2;
  END IF;


  /*Nothing will be returned if either of the counts are NULL
    Nov 6, 2018*/
  IF TTL_CNT_1 IS NULL THEN TTL_CNT_1 := 0;
  END IF;
  IF TTL_CNT_2 IS NULL THEN TTL_CNT_2 := 0;
  END IF;
  TTL_COUNT := TTL_CNT_1 + TTL_CNT_2;
  DBMS_OUTPUT.PUT_LINE(TTL_COUNT);
END;

/

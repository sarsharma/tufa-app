--------------------------------------------------------
--  DDL for Procedure LOAD_CBP
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."LOAD_CBP" (
    p_fiscal_yr IN NUMBER
) AS
BEGIN
    DECLARE
        v_mixed_total_tax        NUMBER;
        v_mixed_entry_no_count   NUMBER;
    BEGIN
        load_cbp_importer(p_fiscal_yr);
        load_cbp_entry(p_fiscal_yr);
        BEGIN
            SELECT
                NVL(SUM(estimated_tax),0),
                COUNT(DISTINCT(summary_entry_summary_number))
            INTO
                v_mixed_total_tax,
                v_mixed_entry_no_count
            FROM
                (
                    SELECT
                        ce.summary_entry_summary_number,
                        ce.estimated_tax
                    FROM
                        tu_cbp_importer ci
                        JOIN tu_cbp_entry ce ON ci.cbp_importer_id = ce.cbp_importer_id
                    WHERE
                        ce.fiscal_year = p_fiscal_yr
            -- Do not include default EXCLUDED lines
                        AND ci.association_type_cd <> 'EXCL'
            -- Only select ESNs that are mixed
                        AND ce.summary_entry_summary_number IN (
                -- Selecting ESNs that contain multipile HTS codes
                            SELECT
                                summary_entry_summary_number
                            FROM
                                tu_cbp_entry
                            WHERE
                                fiscal_year = p_fiscal_yr
                            GROUP BY
                                summary_entry_summary_number
                            HAVING
                                COUNT(DISTINCT hts_cd) > 1
                        )
                );

        END;

        UPDATE tu_cbp_metrics
        SET
            detail_file_total_tax_amt = v_mixed_total_tax,
            mixed_ent_smry_no_count = v_mixed_entry_no_count
        WHERE
            fiscal_yr = p_fiscal_yr;
    END;
END;

/

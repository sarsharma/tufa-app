--------------------------------------------------------
--  DDL for Procedure LOAD_CBP_IMPORTER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."LOAD_CBP_IMPORTER" (p_fiscal_yr in NUMBER) AS

BEGIN

DECLARE

  v_fiscal_yr NUMBER(4) := p_fiscal_yr;

  CURSOR load_importer IS

    select distinct decode(instr(consignee_ein,'-'),
      3, substr(consignee_ein, 1,2)||substr(consignee_ein, 4,7),
      4, substr(consignee_ein, 1,3)||substr(consignee_ein, 5,2)||substr(consignee_ein, 8,4),
      7, substr(consignee_ein, 1,6)||substr(consignee_ein,8,3))
      consignee_ein,
      decode(instr(importer_ein,'-'),
      3, substr(importer_ein, 1,2)||substr(importer_ein, 4,7),
      4, substr(importer_ein, 1,3)||substr(importer_ein, 5,2)||substr(importer_ein, 8,4),
      7, substr(consignee_ein, 1,6)||substr(consignee_ein,8,3))
      importer_ein,
      importer_name,
      consignee_name,
      decode(instr(importer_ein,'-'),3,'N','Y') import_ssn,
      decode(instr(consignee_ein,'-'),3,'N','Y') consign_ssn,
      decode(instr(importer_ein, '00'),1,'Y', 'N') importer_zero,
      decode(instr(consignee_ein, '00'),1,'Y', 'N') consign_zero
  from tu_cbp_upload
  where fiscal_yr = v_fiscal_yr;

v_imp_ein varchar2(9) := null;
v_imp_name varchar2(80) := null;
v_cons_name varchar2(80) := null;
v_cons_ein varchar2(9) := null;

v_default CHAR(4) := null;
v_automated CHAR(1) := 'N';
v_consexists char(1) := 'N';
v_importer_type  varchar2(8) := 'NOTEXIST';
v_consignee_type varchar2(8) := 'NOTEXIST';

--v_sqla varchar2(4000) := 'delete from tu_cbp_entry where fiscal_year=' || p_fiscal_yr;
--v_sqlb varchar2(4000)    := 'delete from tu_cbp_importer where fiscal_yr=' || p_fiscal_yr;

BEGIN
-- First, we drop all data in the table
--  execute immediate v_sqla;
--  execute immediate v_sqlb;
  -- loop through the cursor
  FOR rec IN load_importer LOOP
    -- set all the flags to N
    v_default := null;
    v_automated := 'N';
    v_consexists := 'N';

    -- set reused variables to null
    v_imp_ein := null;
    v_imp_name := null;
    v_cons_name :=null;
    v_cons_ein := null;

  --  DBMS_OUTPUT.PUT_LINE('IMPORTER EIN  =' || rec.importer_ein);
  --  DBMS_OUTPUT.PUT_LINE('CONSIGNEE EIN =' || rec.consignee_ein);

    -- determine the type of importer ein
    IF rec.importer_zero = 'Y' THEN
      v_importer_type := 'ZERO';
    ELSIF rec.import_ssn = 'Y' THEN
      if rec.consign_ssn IS NULL THEN
        v_importer_type := 'NULL';
      else
        v_importer_type := 'SSN';
      end if;
    ELSE
      BEGIN
      -- check for existing importer
      SELECT 'EXIST' into v_importer_type
        from tu_company
      WHERE ein=rec.importer_ein;
      EXCEPTION when no_data_found then
        v_importer_type := 'NOTEXIST';
      END;
      -- check for shipping importer
      IF v_importer_type = 'NOTEXIST' THEN
        BEGIN
          SELECT 'SHIP' into v_importer_type
            from tu_shipping_company sc
          WHERE sc.EIN=rec.importer_ein;
          EXCEPTION when no_data_found THEN
            v_importer_type := 'NOTEXIST';
          END;
      END IF;
    END IF;
   -- DBMS_OUTPUT.PUT_LINE('IMPORTER=' || v_importer_type);

    -- determine the type of consignee ein
    IF rec.consign_zero = 'Y' THEN
      v_consignee_type := 'ZERO';
    ELSIF rec.consign_ssn = 'Y' THEN
      v_consignee_type := 'SSN';
    ELSE
      BEGIN
      -- check for existing importer
      SELECT 'EXIST' into v_consignee_type
        from tu_company
      WHERE ein=rec.consignee_ein;
      EXCEPTION when no_data_found then
        v_consignee_type := 'NOTEXIST';
      END;
      -- check for shipping consignee
      IF v_consignee_type = 'NOTEXIST' THEN
        BEGIN
        SELECT 'SHIP' into v_consignee_type
          from tu_shipping_company sc
        WHERE sc.EIN=rec.consignee_ein;
        EXCEPTION when no_data_found THEN
          v_consignee_type := 'NOTEXIST';
        END;
      END IF;
    END IF;
    --DBMS_OUTPUT.PUT_LINE('CONSIGNEE=' || v_consignee_type);

    if v_consignee_type = 'EXIST' THEN
      -- buckets need to know if the consignee
      -- exists in TUFA.
      v_consexists := 'Y';
    END IF;

    -- query the default decision from tu_cbp_decision
    BEGIN
      SELECT DECISION
      INTO v_default
      FROM TU_CBP_DECISION
      WHERE importer_type=v_importer_type
      AND consignee_type=v_consignee_type;
    END;

   -- DBMS_OUTPUT.PUT_LINE('default=' || v_default);

    -- if both EIN's exist in TUFA but they don't
    -- match, user needs to decide which one
    IF v_importer_type = 'EXIST' AND v_consignee_type = 'EXIST' THEN
      IF rec.consignee_ein <> rec.importer_ein THEN
        v_default := null;
      end if;
    END IF;

    if v_default IS NOT NULL THEN
      v_automated := 'Y';
    END IF;

    -- need to mask SSN
    -- we should never see it nor store it.
    -- except in transient upload table
    if rec.import_ssn='Y' THEN
      v_imp_ein := 'SSN';
      v_imp_name := 'SSN Name';
    else
      v_imp_ein  := rec.importer_ein;
      v_imp_name := rec.importer_name;
    end if;

    -- mask SSN on consign
    if v_consignee_type='Y' THEN
      v_cons_ein := 'SSN';
    else
      v_cons_ein := rec.consignee_ein;
    END IF;

    IF rec.consignee_name IS NOT NULL THEN
      v_cons_name := rec.consignee_name;
    END IF;

    insert into tu_cbp_importer
      (fiscal_yr, consignee_ein,
       importer_ein, importer_nm,consignee_nm, association_type_cd,
       default_flag, consignee_exists_flag)
    values
      (v_fiscal_yr, v_cons_ein,
       v_imp_ein, v_imp_name,v_cons_name, v_default,
       v_automated, v_consexists);

  END LOOP;
END;
END;

/

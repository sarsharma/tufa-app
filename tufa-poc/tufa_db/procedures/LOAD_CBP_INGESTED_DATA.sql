--------------------------------------------------------
--  DDL for Procedure LOAD_CBP_INGESTED_DATA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."LOAD_CBP_INGESTED_DATA" 
(
  FISCAL_YR IN NUMBER
) AS
BEGIN
DECLARE
BEGIN
    IF FISCAL_YR IS NOT NULL THEN
        --Call LOAD_CBP_MATCHED_TAXES
        LOAD_CBP_MATCHED_TAXES(FISCAL_YR);
        --CALL LOAD_CBP_METRICS
        LOAD_CBP_METRICS(FISCAL_YR);
    END IF;
END;
END;

/

--------------------------------------------------------
--  DDL for Procedure LOAD_CBP_MATCHED_TAXES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."LOAD_CBP_MATCHED_TAXES" (
    P_FISCAL_YR   IN NUMBER DEFAULT NULL

) AS
CURSOR CUR_DETAIL_FILE
IS
SELECT DISTINCT a.entry_summary_number AS detail_summary_entry_number,
       b.entry_summary_number summary_entry_sum_number,
       b.TOTAL_ASCERTAINED_TAX_AMOUNT,
       a.IMPORTER_NUMBER,
       a.FISCAL_YEAR,
       a.CREATED_BY,
       (SELECT SUM(tcdf.IR_TAX_AMOUNT)
         FROM TU_CBP_DETAILS_FILE tcdf
         WHERE tcdf.ENTRY_SUMMARY_NUMBER=a.entry_summary_number
         AND tcdf.FISCAL_YEAR=P_FISCAL_YR) as TAXES_FROM_DETAILS
  FROM TU_CBP_DETAILS_FILE a
       FULL OUTER JOIN tu_cbp_summary_file b
         ON (a.entry_summary_number = b.entry_summary_number
            AND a.FISCAL_YEAR = b.FISCAL_YEAR)
  WHERE (NVL(a.FISCAL_YEAR,b.FISCAL_YEAR) =P_FISCAL_YR) ;

type details_data is record
(
       detail_summary_entry_number TU_CBP_DETAILS_FILE.entry_summary_number%TYPE,
       summary_entry_sum_number    tu_cbp_summary_file.entry_summary_number%type ,
       TOTAL_ASCERTAINED_TAX_AMOUNT tu_cbp_summary_file.TOTAL_ASCERTAINED_TAX_AMOUNT%TYPE,
       IMPORTER_NUMBER TU_CBP_DETAILS_FILE.IMPORTER_NUMBER%TYPE,
       FISCAL_YEAR TU_CBP_DETAILS_FILE.FISCAL_YEAR%TYPE,
       CREATED_BY TU_CBP_DETAILS_FILE.CREATED_BY%TYPE,
       TAXES_FROM_DETAILS TU_CBP_DETAILS_FILE.IR_TAX_AMOUNT%TYPE
);
type details_cur_data is table of details_data;

details_data_rec details_cur_data;

V_FISCAL_YEAR VARCHAR2(10);
V_EXISTS VARCHAR2(5);
V_TAXES  NUMBER;
V_SUMMARY_ESTIMATED_TAX NUMBER;
V_TOBACCO_CLASS_COUNT NUMBER;
V_MIXED_TYPE_FLAG VARCHAR2(5);
V_COMMIT_COUNT number;

BEGIN
V_FISCAL_YEAR:=P_FISCAL_YR;

open CUR_DETAIL_FILE;
loop
DBMS_OUTPUT.PUT_LINE('test');
fetch CUR_DETAIL_FILE
bulk collect into details_data_rec
limit 1000;
--DBMS_OUTPUT.PUT_LINE(details_data_rec(summary_entry_sum_number));
exit when details_data_rec.count=0;

--DBMS_OUTPUT.PUT_LINE('entering second loop');
forall i in 1..details_data_rec.last

    INSERT INTO TU_CBP_MATCHED_TAXES
                                    (
                                     ENTRY_SUMMARY_NUMBER ,
                                     IMPORTER_NUMBER,
                                     DETAIL_FILE_SUMMARIZED_TAX,
                                     SUMMARIZED_FILE_TAX,
                                     ENTRY_NUMBER_MATCH,
                                    -- MIXED_TOBACCO_TYPE_FLAG,
                                     FISCAL_YEAR,
                                     CREATED_BY,
                                     CREATED_DATE
                                     )
                                VALUES
                                (
                                     NVL(details_data_rec(i).detail_summary_entry_number,details_data_rec(i).summary_entry_sum_number) ,
                                     details_data_rec(i).IMPORTER_NUMBER,
                                     details_data_rec(i).TAXES_FROM_DETAILS,
                                     details_data_rec(i).TOTAL_ASCERTAINED_TAX_AMOUNT,
                                     case
                                     when details_data_rec(i).detail_summary_entry_number is null and
                                     details_data_rec(i).summary_entry_sum_number is not null then 'S'
                                     when details_data_rec(i).detail_summary_entry_number is not null
                                     and details_data_rec(i).summary_entry_sum_number is null then 'D'
                                     when  details_data_rec(i).detail_summary_entry_number=details_data_rec(i).summary_entry_sum_number then 'M'
                                     else 'NA'
                                     end
                                     ,
                                    -- V_MIXED_TYPE_FLAG,
                                     V_FISCAL_YEAR,
                                     details_data_rec(i).CREATED_BY,
                                     SYSDATE
                                     );
end loop;
commit;
END LOAD_CBP_MATCHED_TAXES;

/

--------------------------------------------------------
--  DDL for Procedure LOAD_CBP_MERGEDVIEW_DATA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."LOAD_CBP_MERGEDVIEW_DATA" (
                                                    P_FISCAL_YR   IN NUMBER DEFAULT NULL
                                                   ) AS
CURSOR CUR_MERGE_DATA
IS
SELECT DISTINCT DETAILS_LINE_NUMBER
                           ,DETAILS_ENTRY_SUMMARY_NUMBER
                           ,DETAILS_HTS_NUMBER
                           ,DETAILS_IMPORTER_NUMBER
                           ,DETAILS_IMPORTER_NAME
                           ,DETAILS_ULTIMATE_CONSIGNEE_NUM
                          ,DETAILS_ENTRY_DATE
                          ,DETAILS_FISCAL_YEAR
                          ,DETAILS_ULTIMATE_CONSIGNE_NAME
                          ,DETAILS_LINE_TARIFF_UOM1
                          ,DETAILS_LINE_TARIFF_QTY1
                          ,DETAILS_IR_TAX_AMOUNT
                          ,SUMMARIZED_FILE_TAX
                          ,SUMMARY_ENTRY_SUMMARY_DATE
                          ,DETAILS_ENTRY_SUMMARY_DATE
                          ,SUMMARY_ENTRY_SUMMARY_NUMBER
                          ,MIXED_TOBACCO_TYPE_FLAG
                          ,SUMMARY_ENTRY_DATE
                          ,DETAILS_ENTRY_TYPE_CODE
FROM CBP_MERGED_DATA_VW
WHERE DETAILS_FISCAL_YEAR=P_FISCAL_YR;
--AND DETAILS_ENTRY_SUMMARY_NUMBER='31501124602'

type merged_view_data is record
                          (
                           DETAILS_LINE_NUMBER CBP_MERGED_DATA_VW.DETAILS_LINE_NUMBER%TYPE
                          ,DETAILS_ENTRY_SUMMARY_NUMBER CBP_MERGED_DATA_VW.DETAILS_ENTRY_SUMMARY_NUMBER%TYPE
                          ,DETAILS_HTS_NUMBER CBP_MERGED_DATA_VW.DETAILS_HTS_NUMBER%TYPE
                          ,DETAILS_IMPORTER_NUMBER CBP_MERGED_DATA_VW.DETAILS_IMPORTER_NUMBER%TYPE
                          ,DETAILS_IMPORTER_NAME CBP_MERGED_DATA_VW.DETAILS_IMPORTER_NAME%TYPE
                          ,DETAILS_ULTIMATE_CONSIGNEE_NUM CBP_MERGED_DATA_VW.DETAILS_ULTIMATE_CONSIGNEE_NUM%TYPE
                          ,DETAILS_ENTRY_DATE CBP_MERGED_DATA_VW.DETAILS_ENTRY_DATE%TYPE
                          ,DETAILS_FISCAL_YEAR CBP_MERGED_DATA_VW.DETAILS_FISCAL_YEAR%TYPE
                          ,DETAILS_ULTIMATE_CONSIGNE_NAME CBP_MERGED_DATA_VW.DETAILS_ULTIMATE_CONSIGNE_NAME%TYPE
                          ,DETAILS_LINE_TARIFF_UOM1 CBP_MERGED_DATA_VW.DETAILS_LINE_TARIFF_UOM1%TYPE
                          ,DETAILS_LINE_TARIFF_QTY1 CBP_MERGED_DATA_VW.DETAILS_LINE_TARIFF_QTY1%TYPE
                          ,DETAILS_IR_TAX_AMOUNT CBP_MERGED_DATA_VW.DETAILS_IR_TAX_AMOUNT%TYPE
                          ,SUMMARIZED_FILE_TAX CBP_MERGED_DATA_VW.SUMMARIZED_FILE_TAX%TYPE
                          ,SUMMARY_ENTRY_SUMMARY_DATE CBP_MERGED_DATA_VW.SUMMARY_ENTRY_SUMMARY_DATE%TYPE
                          ,DETAILS_ENTRY_SUMMARY_DATE CBP_MERGED_DATA_VW.DETAILS_ENTRY_SUMMARY_DATE%TYPE
                          ,SUMMARY_ENTRY_SUMMARY_NUMBER CBP_MERGED_DATA_VW.SUMMARY_ENTRY_SUMMARY_NUMBER%TYPE
                          ,MIXED_TOBACCO_TYPE_FLAG CBP_MERGED_DATA_VW.MIXED_TOBACCO_TYPE_FLAG%TYPE
                          ,SUMMARY_ENTRY_DATE CBP_MERGED_DATA_VW.SUMMARY_ENTRY_DATE%TYPE
                          ,DETAILS_ENTRY_TYPE_CODE CBP_MERGED_DATA_VW.DETAILS_ENTRY_TYPE_CODE%TYPE
                          );

type merged_view_data_cur is table of merged_view_data;
details_data_rec merged_view_data_cur;

v_cbp_upload_id number;
BEGIN
v_cbp_upload_id:=0;

open CUR_MERGE_DATA;
loop
-- DBMS_OUTPUT.PUT_LINE('test');
fetch CUR_MERGE_DATA
bulk collect into details_data_rec
limit 1000;
--DBMS_OUTPUT.PUT_LINE(details_data_rec(summary_entry_sum_number));
exit when details_data_rec.count=0;


--DBMS_OUTPUT.PUT_LINE('ENTERING INTO LOOP');
--DBMS_OUTPUT.PUT_LINE('P_FISCAL_YR --'||P_FISCAL_YR);
      v_cbp_upload_id:=v_cbp_upload_id+1;
BEGIN
forall i in 1..details_data_rec.last

        Insert into TU_CBP_UPLOAD
                         (CBP_UPLOAD_ID
                          ,ENTRY_NO
                          ,LINE_NO
                          ,HTS_CD
                          ,IMPORTER_EIN
                          ,IMPORTER_NAME
                          ,CONSIGNEE_EIN
                          ,ENTRY_DATE
                          ,ENTRY_SUMM_DATE
                          ,ESTIMATED_TAX
                          ,UOM1
                          ,QTY1
                          ,FISCAL_YR
                          --ERRORS
                          ,CONSIGNEE_NAME
                          ,SUMMARIZED_FILE_TAX
                          ,DETAILS_ESTIMATED_TAX
                          ,SUMMARY_ENTRY_SUMMARY_DATE
                          ,DETAILS_ENTRY_SUMMARY_DATE
                          ,SUMMARY_ENTRY_SUMMARY_NUMBER
                          ,MIXED_TOBACCO_TYPE_FLAG
                          ,SUMMARY_ENTRY_DATE
                          ,ENTRY_TYPE)
       VALUES(             v_cbp_upload_id
                           ,details_data_rec(i).DETAILS_ENTRY_SUMMARY_NUMBER
                           ,details_data_rec(i).DETAILS_LINE_NUMBER
                           ,details_data_rec(i).DETAILS_HTS_NUMBER
                           ,details_data_rec(i).DETAILS_IMPORTER_NUMBER
                           ,details_data_rec(i).DETAILS_IMPORTER_NAME
                           ,details_data_rec(i).DETAILS_ULTIMATE_CONSIGNEE_NUM
                           ,details_data_rec(i).DETAILS_ENTRY_DATE
                           ,details_data_rec(i).DETAILS_ENTRY_SUMMARY_DATE
                           ,details_data_rec(i).DETAILS_IR_TAX_AMOUNT
                           ,details_data_rec(i).DETAILS_LINE_TARIFF_UOM1
                           ,details_data_rec(i).DETAILS_LINE_TARIFF_QTY1
                           ,P_FISCAL_YR
                           ,details_data_rec(i).DETAILS_ULTIMATE_CONSIGNE_NAME
                          ,details_data_rec(i).SUMMARIZED_FILE_TAX
                          ,details_data_rec(i).DETAILS_IR_TAX_AMOUNT
                          ,details_data_rec(i).SUMMARY_ENTRY_SUMMARY_DATE
                          ,details_data_rec(i).DETAILS_ENTRY_SUMMARY_DATE
                          ,details_data_rec(i).SUMMARY_ENTRY_SUMMARY_NUMBER
                          ,details_data_rec(i).MIXED_TOBACCO_TYPE_FLAG
                          ,details_data_rec(i).SUMMARY_ENTRY_DATE
                          ,details_data_rec(i).DETAILS_ENTRY_TYPE_CODE );
--DBMS_OUTPUT.PUT_LINE('DATA INSERTED SUCCESSFULLY');
exception
       when others then
  --     DBMS_OUTPUT.PUT_LINE(SQLERRM||'--'||sqlcode||' cbp_upload_id  ' || REC_MERGE_DATA.DETAILS_ENTRY_SUMMARY_NUMBER);
       RAISE_APPLICATION_ERROR(-20000, 'Error while Loading data into TU_CBP_UPLOAD');
END;
END LOOP;
--commit;
LOAD_CBP(P_FISCAL_YR);

-- This should only run when everything is successful -Kyle
BEGIN
UPDATE TU_CBP_METRICS
SET ACCEPTED='Y'
WHERE FISCAL_YR=P_FISCAL_YR;
END;

COMMIT;
END LOAD_CBP_MERGEDVIEW_DATA;

/

--------------------------------------------------------
--  DDL for Procedure LOAD_CBP_METRICS_COUNTS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."LOAD_CBP_METRICS_COUNTS" (
    p_fiscal_year IN NUMBER
) AS
BEGIN
    DECLARE
        v_exists                       CHAR;
        v_summary_entry_no_count       NUMBER;
        v_detail_entry_no_count        NUMBER;
        v_matched_entry_no_count       NUMBER;
        v_unmatch_entry_no_count       NUMBER;
        v_unmatch_sum_entry_no_count   NUMBER;
        v_unmatch_det_entry_no_count   NUMBER;
        v_mixed_total_tax              NUMBER;
        v_mixed_entry_no_count         NUMBER;
    BEGIN

    --Clear out old data for fiscal year
--    BEGIN
--        DELETE FROM TU_CBP_METRICS cm WHERE cm.FISCAL_YR = P_FISCAL_YEAR;
--    END;

    --Select Summary File Entry Summary Number Count: # of unique values of ENTRY_SUMMARY_NUMBER in TU_CBP_SUMMARY_FILE for FISCAL_YR
        BEGIN
            SELECT
                COUNT(DISTINCT entry_summary_number)
            INTO v_summary_entry_no_count
            FROM
                tu_cbp_summary_file csf
            WHERE
                csf.fiscal_year = p_fiscal_year;

-- if something errored I dont want any values saved - 8/7/2019
--        EXCEPTION WHEN OTHERS THEN
--            v_summary_entry_no_count := 0;

        END;
    --Detail File Entry Summary Number Count: # of unique values of ENTRY_SUMMARY_NUMBER in TU_CBP_LINE_ITEM_FILE for FISCAL_YEAR

        BEGIN
            SELECT
                COUNT(DISTINCT entry_summary_number)
            INTO v_detail_entry_no_count
            FROM
                tu_cbp_details_file cdf
            WHERE
                cdf.fiscal_year = p_fiscal_year;

--        EXCEPTION WHEN OTHERS THEN
--            v_detail_entry_no_count := 0;

        END;
    --Matched Entry Summary Numbers: # of records with value 'Y' for ENTRY_NUMBER_MATCH in TU_CBP_MATCHED_TAXES for FISCAL_YEAR

        BEGIN
            SELECT
                COUNT(DISTINCT entry_summary_number)
            INTO v_matched_entry_no_count
            FROM
                tu_cbp_matched_taxes cmt
            WHERE
                cmt.entry_number_match = 'M'
                AND cmt.fiscal_year = p_fiscal_year;

--        EXCEPTION WHEN OTHERS THEN
--            v_matched_entry_no_count := 0;

        END;
    --Unmatch Summary File Entry Summary Numbers: # of records with value 'N' for ENTRY_NUMBER_MATCH in TU_CBP_MATCHED_TAXES for FISCAL_YR

        BEGIN
            SELECT
                COUNT(DISTINCT entry_summary_number)
            INTO v_unmatch_entry_no_count
            FROM
                tu_cbp_matched_taxes cmt
            WHERE
                cmt.entry_number_match <> 'M'
                AND cmt.fiscal_year = p_fiscal_year;

--        EXCEPTION WHEN OTHERS THEN
--            v_unmatch_entry_no_count := 0;

        END;
    --Unique Summary File Entry Summary Numbers: # of records with value 'S' for ENTRY_NUMBER_MATCH in TU_CBP_MATCHED_TAXES for FISCAL_YR

        BEGIN
            SELECT
                COUNT(DISTINCT entry_summary_number)
            INTO v_unmatch_sum_entry_no_count
            FROM
                tu_cbp_matched_taxes cmt
            WHERE
                cmt.entry_number_match = 'S'
                AND cmt.fiscal_year = p_fiscal_year;

--        EXCEPTION WHEN OTHERS THEN
--            v_unmatch_sum_entry_no_count := 0;

        END;
    --Unique Detail File Entry Summary Numbers: # of records with value 'D' for ENTRY_NUMBER_MATCH in TU_CBP_MATCHED_TAXES for FISCAL_YR

        BEGIN
            SELECT
                COUNT(DISTINCT entry_summary_number)
            INTO v_unmatch_det_entry_no_count
            FROM
                tu_cbp_matched_taxes cmt
            WHERE
                cmt.entry_number_match = 'D'
                AND cmt.fiscal_year = p_fiscal_year;

--        EXCEPTION WHEN OTHERS THEN
--            v_unmatch_det_entry_no_count := 0;

        END;
    --Kyle 9/9/2019 - Removed mixed tobacco calculations for changes in regards to story 4842
--    --MIXED TOBBACO
--    --Detail File Total Tax Amount: Sum of DETAIL_FILE_SUMMARIZED_TAX or records with value 'Y' for MIXED_TOBACO_TYPE_FLAG in TU_CBP_MATCHED_TAXES for FISCAL_YR
--    BEGIN
--        SELECT NVL(SUM(NVL(DETAIL_FILE_SUMMARIZED_TAX,0)),0) INTO v_mixed_total_tax FROM TU_CBP_MATCHED_TAXES CMT WHERE CMT.MIXED_TOBACCO_TYPE_FLAG = 'Y' AND CMT.FISCAL_YEAR = P_FISCAL_YEAR;
--
----        EXCEPTION WHEN OTHERS THEN
----            v_mixed_total_tax := 0;
--    END;
--    --Number of Mixed Entry Summary Numbers: # of records with value 'Y' for MIXED_TOBACCO_TYPE_FLAG in TU_CBP_MATCHED_TAXES for FISCAL_YR
--    BEGIN
--        SELECT COUNT(ENTRY_SUMMARY_NUMBER) INTO v_mixed_entry_no_count FROM TU_CBP_MATCHED_TAXES CMT WHERE CMT.MIXED_TOBACCO_TYPE_FLAG = 'Y' AND CMT.FISCAL_YEAR = P_FISCAL_YEAR;
--
----        EXCEPTION WHEN OTHERS THEN
----            v_mixed_entry_no_count := 0;
--    END;

        BEGIN
            SELECT
                'X'
            INTO v_exists
            FROM
                tu_cbp_metrics
            WHERE
                fiscal_yr = p_fiscal_year;

        EXCEPTION
            WHEN no_data_found THEN
    --Insert the above data into TU_CBP_METRICS
                INSERT INTO tu_cbp_metrics (
                    fiscal_yr,
                    summary_file_ent_smry_count,
                    detail_file_ent_smry_no_count,
                    matched_ent_smry_no_count,
                    unmatched_ent_smry_no_count,
                    unique_smry_file_entry_smry_no,
                    unique_detail_file_ent_smry_no
                ) VALUES (
                    p_fiscal_year,
                    v_summary_entry_no_count,
                    v_detail_entry_no_count,
                    v_matched_entry_no_count,
                    v_unmatch_entry_no_count,
                    v_unmatch_sum_entry_no_count,
                    v_unmatch_det_entry_no_count
                );

        END;

    END;
END;

/

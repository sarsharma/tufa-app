--------------------------------------------------------
--  DDL for Procedure LOAD_HTS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."LOAD_HTS" AS

BEGIN

DECLARE

  v_chew_id NUMBER;
  v_snuff_id NUMBER;
  v_cigarette_id NUMBER;
  v_cigar_id NUMBER;
  v_pipe_id NUMBER;
  v_ryo_id NUMBER;
  v_sql1 varchar2(4000)    := 'TRUNCATE table tu_hts_code drop all storage cascade';

  BEGIN
    execute immediate v_sql1;
    BEGIN
      SELECT tobacco_class_id
        INTO v_chew_id
      FROM tu_tobacco_class
      WHERE tobacco_class_nm = 'Chew';

      SELECT tobacco_class_id
        INTO v_snuff_id
      FROM tu_tobacco_class
      WHERE tobacco_class_nm = 'Snuff';

      SELECT tobacco_class_id
        INTO v_cigarette_id
      FROM tu_tobacco_class
      WHERE tobacco_class_nm = 'Cigarettes';

      SELECT tobacco_class_id
        INTO v_cigar_id
      FROM tu_tobacco_class
      WHERE tobacco_class_nm = 'Cigars';

      SELECT tobacco_class_id
        INTO v_pipe_id
      FROM tu_tobacco_class
      WHERE tobacco_class_nm = 'Pipe';

      SELECT tobacco_class_id
        INTO v_ryo_id
      FROM tu_tobacco_class
      WHERE tobacco_class_nm = 'Roll-Your-Own';
    END;
    BEGIN

--RYO
      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_ryo_id, '2401201440');

      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_ryo_id, '2403192050');

      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_ryo_id, '2403912000');

--Chew
      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_chew_id, '2402992030');

--Pipe
      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_pipe_id, '2403110000');

      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_pipe_id, '2403192020');

--Snuff
      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_snuff_id, '2402992040');

--Cigar
      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_cigar_id, '2402100000');

      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_cigar_id, '2402103030');

      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_cigar_id, '2402103070');

      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_cigar_id, '2402106000');

      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_cigar_id, '2402108030');

      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_cigar_id, '2402108050');

      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_cigar_id, '2402108080');

--Cigarette
      INSERT into tu_hts_code
        (tobacco_class_id, hts_code)
      VALUES
        (v_cigarette_id, '2402200000');
    END;
  END;
commit;
END;

/

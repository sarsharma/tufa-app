--------------------------------------------------------
--  DDL for Procedure LOAD_TTB
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."LOAD_TTB" (p_fiscal_yr in NUMBER) AS

BEGIN

DECLARE
  BEGIN
    load_ttb_company(p_fiscal_yr);
    load_ttb_permit(p_fiscal_yr);
    load_ttb_details(p_fiscal_yr);
  END;
END;

/

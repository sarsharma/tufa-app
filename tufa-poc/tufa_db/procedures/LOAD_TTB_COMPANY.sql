--------------------------------------------------------
--  DDL for Procedure LOAD_TTB_COMPANY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."LOAD_TTB_COMPANY" (p_fiscal_yr in NUMBER) AS

BEGIN

DECLARE

  v_fiscal_yr NUMBER(4) := p_fiscal_yr;

  CURSOR load_ttbcompany IS

    select distinct company_nm,
           substr(ein_num,1,2)||substr(ein_num,4,7) ein
      from tu_ttb_upload;

  v_exists_flag CHAR(1) := 'N';
  v_ttb_exists_flag CHAR(1) := 'N';
  v_sql1 varchar2(4000)    := 'delete from tu_ttb_company';

  BEGIN
-- First, we drop all data in the table
   -- execute immediate v_sql1;
    FOR rec IN load_ttbcompany LOOP
    BEGIN
      SELECT 'Y'
        INTO v_exists_flag
      FROM tu_company
      WHERE ein=rec.ein;
    EXCEPTION when no_data_found then v_exists_flag := 'N';
    END;

    BEGIN
      SELECT 'Y'
        INTO v_ttb_exists_flag
      FROM tu_ttb_company c
      WHERE c.ein_num=rec.ein
      AND c.fiscal_yr=p_fiscal_yr;
    EXCEPTION when no_data_found then v_ttb_exists_flag := 'N';
    END;

    if(v_ttb_exists_flag = 'N') THEN
      BEGIN
        insert into tu_ttb_company
          (fiscal_yr, company_nm, ein_num, exists_intufa_flag)
        values
          (v_fiscal_yr, rec.company_nm, rec.ein, v_exists_flag);
      END;
    END IF;
    END LOOP;
    COMMIT;
  END;
END;

/

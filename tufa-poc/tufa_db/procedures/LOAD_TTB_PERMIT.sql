--------------------------------------------------------
--  DDL for Procedure LOAD_TTB_PERMIT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."LOAD_TTB_PERMIT" (p_fiscal_yr in NUMBER) AS

BEGIN

DECLARE

  v_fiscal_yr NUMBER(4) := p_fiscal_yr;
  v_earliest_dt VARCHAR(11);

  CURSOR load_ttbcompany IS

    select distinct substr(ein_num,1,2)||substr(ein_num,4,7) ein,
           permit_num, TPBD
      from tu_ttb_upload;


  v_exists_flag CHAR(1) := 'N';
  v_ttb_exists_flag CHAR(1) :='N';
  v_company_id NUMBER;

  BEGIN
    FOR rec IN load_ttbcompany LOOP

    BEGIN
        select to_char(min(to_date(TPBD,'MM/DD/YYYY')),'MM/DD/YYYY') into v_earliest_dt
            from tu_ttb_upload ttupload where ttupload.ein_num= substr(rec.ein,1,2)||'-'||substr(rec.ein,3,7) AND ttupload.PERMIT_NUM=rec.permit_num;
             DBMS_OUTPUT.PUT_LINE(v_earliest_dt ||' v_earliest_dt');

    END;

    BEGIN
      SELECT 'Y'
        INTO v_exists_flag
      FROM tu_company c, tu_permit p
      where c.company_id = p.company_id AND c.ein = rec.ein AND p.permit_num = rec.permit_num;
    EXCEPTION when no_data_found then v_exists_flag := 'N';
    END;

    BEGIN
      SELECT ttb_company_id
        INTO v_company_id
      FROM tu_ttb_company
      WHERE ein_num=rec.ein
        and fiscal_yr = v_fiscal_yr;
    EXCEPTION when no_data_found then v_company_id := '1';
    END;
    BEGIN
      SELECT 'Y'
        INTO v_ttb_exists_flag
      FROM tu_ttb_company c, tu_ttb_permit p
      WHERE c.ttb_company_id=p.ttb_company_id
      AND c.fiscal_yr=v_fiscal_yr
      AND c.ein_num=rec.ein
      AND p.permit_num=rec.permit_num;
    EXCEPTION when no_data_found then v_ttb_exists_flag := 'N';
    END;
    BEGIN
      IF v_ttb_exists_flag = 'N' THEN
        insert into tu_ttb_permit
          (ttb_company_id, permit_num, exists_intufa_flag, TPBD_DATE)
        values
          (v_company_id,rec.permit_num, v_exists_flag, to_date(v_earliest_dt, 'MM/DD/YYYY'));
      END IF;
    END;
    END LOOP;
    COMMIT;
  END;
END;

/

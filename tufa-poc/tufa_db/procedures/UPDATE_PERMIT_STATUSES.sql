--------------------------------------------------------
--  DDL for Procedure UPDATE_PERMIT_STATUSES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "CTP_TUFA_MVP13"."UPDATE_PERMIT_STATUSES" 
AS
BEGIN
  DECLARE
    CURSOR load_audit
    IS
      SELECT
       ein,
        PERMIT_ID,
        PERMIT_TYPE,
        PERMIT_STATUS,
        ISSUE_DT,
        CLOSED_DT,
        BUSINESS_NAME,
        ERRORS,
        DOC_STAGE_ID,
        STAGE_ID
      FROM TU_PERMIT_UPDATES_STAGE STAGE
      WHERE STAGE.PROCESSED_FLAG IS NULL
      ORDER BY ein,permit_status; -- order is needed to have active permits processed before closed ones.

    Type permit_array_type IS VARRAY(10) OF NUMBER;
    v_tufa_permit_ids permit_array_type;

    v_company_nm    VARCHAR(100);
    v_company_id    NUMBER;
    v_permit_id     VARCHAR(10);
    v_permit_status VARCHAR(10);
    v_permit_type_cd VARCHAR(10);
    v_permit_status_type_cd VARCHAR(10);
    v_permit_action VARCHAR2(40);
    v_permit_conflict_cnt NUMBER:=0;
    v_company_action VARCHAR2(40);
    v_company_status VARCHAR2(10);
    v_errors  VARCHAR2(2000);
    v_permit_type    VARCHAR2(5);
    v_permit_close_dt DATE;

    v_tufa_permit_num  VARCHAR2(11);
    v_tufa_permit_type_cd VARCHAR2(5);
    v_tufa_permit_close_dt DATE;
    v_tufa_permit_start_dt DATE;
    v_tufa_permit_end_dt DATE;
    v_tufa_permit_status_type_cd VARCHAR2(5);
    v_tufa_permit_status VARCHAR2(5);
    v_tufa_permit_id number;


    v_permit_valid_err_cnt Number;
    v_company_valid_err_cnt Number;

    v_error_cause VARCHAR2(150);
    --Kyle 3442 : Was a number whiched dropped the leading 0
    v_ein VARCHAR(9);
    v_audit_id NUMBER;

    v_issue_dt_fy_qtr NUMBER;
    v_issue_dt_fy_yr NUMBER;

    v_closed_dt_fy_qtr NUMBER;
    v_closed_dt_fy_yr NUMBER;

    v_tufa_cls_dt_fy_qtr NUMBER;
    v_tufa_cls_dt_fy_yr NUMBER;

    v_permit_excl VARCHAR(20);
    v_permit_excl_cnt number;

    v_excl_count  number;
    v_actv_permits_cnt number;

     rc sys_refcursor;

     v_assmnt_yr VARCHAR(20);
     v_assmnt_qtr VARCHAR(20);

  BEGIN

    FOR rec IN load_audit
    LOOP

--      DBMS_OUTPUT.PUT_LINE(rec.EIN || '     ' || rec.PERMIT_ID|| '     ' || rec.PERMIT_TYPE|| '     ' ||rec.ISSUE_DT|| '     ' ||rec.CLOSED_DT|| '     ' ||rec.BUSINESS_NAME|| '     ' ||rec.ERRORS);


    -- Initialize variables
     v_company_status:='';
     v_company_action:='';
     v_audit_id:='';
     v_permit_action:='';
     v_permit_conflict_cnt:=0;
     v_tufa_permit_type_cd :='';
     v_tufa_permit_start_dt :='';
     v_tufa_permit_end_dt :='';
     v_tufa_permit_status_type_cd :='';
     v_tufa_permit_status :='';
     v_errors:=rec.errors;
     v_permit_valid_err_cnt:=0;
     v_company_valid_err_cnt:=0;
     v_company_id := 0;

     BEGIN
        UPDATE TU_PERMIT_UPDATES_STAGE STAGE SET  STAGE.PROCESSED_FLAG='Y' WHERE STAGE.STAGE_ID=rec.STAGE_ID;
     END;

    --  Do not process the record from Staging if permit status is 'HOLD' or null
     IF rec.PERMIT_STATUS = 'HOLD' OR rec.PERMIT_STATUS IS NULL THEN
            continue;
     END IF;

    -- For exclusion scenarios calculate the Yr  and Qtr for  rec.ISSUE_DT
    IF(rec.ISSUE_DT IS NOT NULL) THEN

        SELECT MOD(TO_CHAR(rec.ISSUE_DT, 'Q'), 4) + 1 INTO v_issue_dt_fy_qtr FROM dual;

        SELECT CASE WHEN v_issue_dt_fy_qtr= 1 THEN
                        TO_CHAR(rec.ISSUE_DT, 'yyyy') + 1
                    ELSE
                        TO_NUMBER(TO_CHAR(rec.ISSUE_DT, 'yyyy'))
        END INTO  v_issue_dt_fy_yr   FROM   dual;
     END IF;

     -- For exclusion scenarios calculate the Yr and  Qtr for  rec.CLOSED_DT
    IF(rec.CLOSED_DT IS NOT NULL) THEN

        SELECT MOD(TO_CHAR(rec.CLOSED_DT, 'Q'), 4) + 1 INTO v_closed_dt_fy_qtr FROM dual;

        SELECT CASE WHEN v_closed_dt_fy_qtr = 1 THEN
                        TO_CHAR(rec.CLOSED_DT, 'yyyy') + 1
                    ELSE
                        TO_NUMBER(TO_CHAR(rec.CLOSED_DT, 'yyyy'))
        END INTO  v_closed_dt_fy_yr  FROM   dual;
     END IF;

    -- Validation Checks on Company and Permit Columns

     v_permit_valid_err_cnt := REGEXP_COUNT(rec.ERRORS,'permitNum:|permitType:|permitStatus:|issueDt:|closeDt:');
     v_company_valid_err_cnt := REGEXP_COUNT(rec.ERRORS,'einNum:|businessName:');

     IF  v_company_valid_err_cnt=0 THEN

            --Kyle 3442 : Was a number whiched dropped the leading 0
             v_ein := SUBSTR(rec.ein,1,2) || SUBSTR(rec.ein,4,7);
            BEGIN
                SELECT company_id,legal_nm,company_status INTO v_company_id,v_company_nm,v_company_status FROM tu_company WHERE ein=v_ein;
                EXCEPTION WHEN no_data_found THEN v_company_id := 0;
            END;

            IF v_company_id > 0 AND upper(v_company_nm) != upper(trim(rec.BUSINESS_NAME)) THEN

                v_company_action :='CONFLICT - NAME MISMATCH';
                v_permit_action :='';
                v_errors:=v_errors||' ; COMPANY_ERROR : [ Company '|| rec.BUSINESS_NAME ||' in TTB file mismatches with Company '|| v_company_nm ||' in TUFA ] ';

                INSERT INTO TU_PERMIT_UPDATES_AUDIT(COMPANY_NAME,COMPANY_ID,EIN,ACTION_DT,CREATED_DT,ERRORS,COMPANY_STATUS,COMPANY_ACTION,PERMIT_ACTION,DOC_STAGE_ID)
                VALUES( rec.BUSINESS_NAME,v_company_id,rec.ein,SYSDATE,SYSDATE,v_errors,v_company_status,v_company_action,v_permit_action,rec.DOC_STAGE_ID)
                RETURNING AUDIT_ID into v_audit_id;
            ELSIF  v_company_id >0 THEN

                v_company_action :='FOUND MATCH';

                INSERT INTO TU_PERMIT_UPDATES_AUDIT(COMPANY_NAME,COMPANY_ID,EIN,ACTION_DT,CREATED_DT,ERRORS,COMPANY_STATUS,COMPANY_ACTION,DOC_STAGE_ID)
                VALUES( rec.BUSINESS_NAME,v_company_id,rec.ein,SYSDATE,SYSDATE,v_errors, v_company_status,v_company_action,rec.DOC_STAGE_ID)
                RETURNING AUDIT_ID into v_audit_id;

            END IF;

      END IF;

      BEGIN
        IF v_permit_valid_err_cnt + v_company_valid_err_cnt >0  THEN
            BEGIN

              IF REGEXP_INSTR(rec.ERRORS, 'einNum')>0 THEN
                 v_errors:= v_errors||' ; COMPANY_ERROR : [ '|| rec.BUSINESS_NAME ||' has an Invalid EIN No.] ';
                 v_company_action :='INVALID EIN';
              END IF;

              IF REGEXP_INSTR(rec.ERRORS, 'businessName')>0 THEN
                 v_company_action :='INVALID NAME';
                 v_errors:= v_errors||' ; COMPANY_ERROR : [TTB Company name is blank.] ';
              END IF;

              IF REGEXP_INSTR(rec.ERRORS, 'permitNum')>0 THEN
                  v_permit_action := 'INVALID PERMIT FORMAT';
                  v_errors:=v_errors||'; PERMIT_ERROR : [ ' || rec.permit_id ||' does not have a valid permit format.]';
              END IF;

              IF REGEXP_INSTR(rec.ERRORS, 'permitStatus')>0 THEN
                  v_permit_action := 'INVALID PERMIT STATUS';
                  v_errors:=v_errors||'; PERMIT_ERROR : [ ' || rec.permit_id ||' has an invalid Permit Status of '||rec.permit_status ||'.]';
              END IF;

              IF REGEXP_INSTR(rec.ERRORS, 'issueDt')>0 THEN
                  v_permit_action := 'INVALID PERMIT DATE';
                  v_errors:=v_errors||'; PERMIT_ERROR : [ ' || rec.permit_id ||' has an invalid Issue Date of '|| to_char(rec.issue_dt,'MM/DD/YYYY') ||'.]';
              END IF;

              IF REGEXP_INSTR(rec.ERRORS, 'closeDt')>0 THEN
                  v_permit_action := 'INVALID PERMIT DATE';
                  v_errors:=v_errors||'; PERMIT_ERROR : [ ' || rec.permit_id ||' has an invalid Close Date of '|| to_char(rec.closed_dt,'MM/DD/YYYY') ||'.]';
              END IF;

              IF REGEXP_INSTR(rec.ERRORS, 'permitType')>0 THEN
                  v_permit_action := 'INVALID PERMIT TYPE';
                  v_errors:=v_errors||'; PERMIT_ERROR : [ ' || rec.permit_id ||' has an invalid Permit Type of '||rec.permit_type ||'.]';
              END IF;

              IF v_permit_valid_err_cnt > 0  AND v_company_id = 0 THEN
                    v_errors := v_errors||'; COMPANY_ERROR : ['||  rec.BUSINESS_NAME ||' has an invalid Permit. ] ';
                    v_company_action := 'INVALID PERMIT';
              END IF;

              IF v_permit_valid_err_cnt > 1 THEN
                     v_permit_action := 'INVALID PERMIT DATA';
              END IF;

              IF v_company_valid_err_cnt > 1 AND v_company_id = 0 THEN
                     v_company_action := 'INVALID DATA';
              END IF;

              IF (v_permit_valid_err_cnt + v_company_valid_err_cnt)> 1 AND v_company_id = 0 THEN
                    v_permit_action :='INVALID DATA';
              END IF ;

            END;

        END IF;
      END;


       -- Validation check for cases not done at the application level

       IF  REGEXP_INSTR(rec.ERRORS,'issueDt')=0 AND REGEXP_INSTR(rec.ERRORS, 'closeDt')=0 AND rec.closed_dt IS NOT NULL  THEN

            IF trunc(rec.issue_dt) > trunc(rec.closed_dt) THEN

                  v_permit_valid_err_cnt:=v_permit_valid_err_cnt+1;
                  v_company_valid_err_cnt:=v_company_valid_err_cnt+1;

                  IF  (v_permit_valid_err_cnt > 1) THEN
                        v_permit_action := 'INVALID DATA';
                  ELSE
                        v_permit_action := 'INVALID PERMIT DATE';
                  END IF;

                  IF v_company_id=0 THEN
                    IF  (v_company_valid_err_cnt > 1) THEN
                        v_company_action := 'INVALID DATA';
                    ELSE
                        v_company_action := 'INVALID PERMIT DATE';
                    END IF;

                    v_errors := v_errors||'; COMPANY_ERROR : [' || rec.permit_id ||' has an Issue Date  '|| to_char(rec.issue_dt,'MM/DD/YYYY') ||' after Close Date  '||  to_char(rec.closed_dt,'MM/DD/YYYY')||'.]';
                  END IF;

                  v_errors:=v_errors||'; PERMIT_ERROR : [ ' || rec.permit_id ||' has an Issue Date  '|| to_char(rec.issue_dt,'MM/DD/YYYY') ||' after Close Date  '||  to_char(rec.closed_dt,'MM/DD/YYYY')||'.]';

            END IF;

       ELSIF REGEXP_INSTR(rec.ERRORS,'issueDt')=0  THEN

            IF trunc(rec.issue_dt) > trunc(SYSDATE) THEN

                  v_permit_valid_err_cnt:=v_permit_valid_err_cnt+1;
                  v_company_valid_err_cnt:= v_company_valid_err_cnt +1;

                  IF  (v_permit_valid_err_cnt > 1) THEN
                        v_permit_action := 'INVALID DATA';
                  ELSE
                        v_permit_action := 'INVALID PERMIT DATE';
                  END IF;

                  IF(v_company_id = 0) THEN
                    IF  (v_company_valid_err_cnt > 1) THEN
                       v_company_action := 'INVALID DATA';
                    ELSE
                        v_company_action := 'INVALID PERMIT DATE';
                    END IF;
                     v_errors := v_errors||'; COMPANY_ERROR : ['|| rec.permit_id ||' has an Issue Date  '|| to_char(rec.issue_dt,'MM/DD/YYYY') ||' after Current Date  '||  to_char(SYSDATE,'MM/DD/YYYY')||'.]';
                  END IF;

                  v_errors:=v_errors||'; PERMIT_ERROR : [ ' || rec.permit_id ||' has an Issue Date  '|| to_char(rec.issue_dt,'MM/DD/YYYY') ||' after Current Date  '||  to_char(SYSDATE,'MM/DD/YYYY')||'.]';

            END IF;

       END IF;

      -- Return Conditions
      IF((v_permit_valid_err_cnt + v_company_valid_err_cnt)>0 ) THEN

                -- Invalid Permit wont be shown for a new Company
                IF(v_company_id = 0) THEN
                    v_permit_action:='';
                END IF;

                IF(v_audit_id!=0) THEN
                        UPDATE TU_PERMIT_UPDATES_AUDIT SET PERMIT_ID=rec.permit_id,PERMIT_ACTION=v_permit_action,CLOSE_DT=rec.closed_dt,ISSUE_DT=rec.issue_dt,ERRORS=v_errors,PERMIT_TYPE_CD= v_permit_type WHERE AUDIT_ID=v_audit_id;
                ELSE
                         INSERT INTO TU_PERMIT_UPDATES_AUDIT(COMPANY_NAME,EIN,PERMIT_ID,PERMIT_ACTION,COMPANY_ACTION,CLOSE_DT,ISSUE_DT,ERRORS,CREATED_DT,DOC_STAGE_ID)
                         VALUES( rec.BUSINESS_NAME,rec.ein,rec.permit_id,v_permit_action,v_company_action,rec.closed_dt,rec.issue_dt,v_errors,SYSDATE,rec.DOC_STAGE_ID);
                END IF;

                CONTINUE;
        END IF;


      -- Insert new company if EIN doesnot exist.
      BEGIN
          IF v_company_id =0 THEN
            BEGIN
              INSERT INTO TU_COMPANY (LEGAL_NM,EIN,COMPANY_STATUS,CREATED_DT) VALUES(rec.BUSINESS_NAME,v_ein,'ACTV',SYSDATE)
              RETURNING company_id into v_company_id;

              v_company_action :='ADDED';

              INSERT INTO TU_PERMIT_UPDATES_AUDIT(COMPANY_NAME,COMPANY_ID,EIN,ACTION_DT,CREATED_DT, COMPANY_STATUS,COMPANY_ACTION,DOC_STAGE_ID)
              VALUES( rec.BUSINESS_NAME,v_company_id,rec.ein,SYSDATE,SYSDATE,'ACTV',v_company_action,rec.DOC_STAGE_ID)
              RETURNING AUDIT_ID into v_audit_id;


            END;
          END IF;
      END;

       ---  PERMIT BASED UPDATES ---

      BEGIN
        IF rec.PERMIT_TYPE   ='MTP' THEN
               v_permit_type:='MANU';
            ELSE
              v_permit_type:='IMPT';
        END IF;
      END;



        --- Check if permit id from PERMIT_UPDATES_STAGE exists in TUFA.
      BEGIN
            SELECT p.permit_id,p.permit_num,p.close_dt,p.ttb_start_dt,p.ttb_end_dt,p.permit_type_cd,p.permit_status_type_cd
            INTO v_permit_id,v_tufa_permit_num,v_tufa_permit_close_dt,v_tufa_permit_start_dt,v_tufa_permit_end_dt,v_tufa_permit_type_cd,v_tufa_permit_status_type_cd
            FROM tu_permit p
            WHERE p.company_id = v_company_id
            AND UPPER(p.permit_num)  =UPPER( rec.permit_id);
      EXCEPTION
            WHEN OTHERS
            THEN v_permit_id := 0;
      END;

        -- For exclusion scenario calculate the Yr and Qtr for  v_tufa_permit_end_dt
    IF(v_tufa_permit_end_dt IS NOT NULL) THEN

        SELECT MOD(TO_CHAR(v_tufa_permit_end_dt, 'Q'), 4) + 1 INTO v_tufa_cls_dt_fy_qtr FROM dual;

        SELECT CASE WHEN v_tufa_cls_dt_fy_qtr = 1 THEN
                        TO_CHAR(v_tufa_permit_end_dt, 'yyyy') + 1
                    ELSE
                        TO_NUMBER(TO_CHAR(v_tufa_permit_end_dt, 'yyyy'))
        END INTO v_tufa_cls_dt_fy_yr   FROM   dual;

     END IF;

      -- calulate qtrs and fy
        -- Check if permits already exists with the same permit type code and permit status type.
        -- If it exists then log an error in TU_PERMIT_UPDATES_AUDIT table and add the permit to TU_PERMIT table.

      BEGIN

           SELECT p.permit_id BULK COLLECT
           INTO v_tufa_permit_ids FROM tu_permit p
           WHERE p.company_id = v_company_id
           AND UPPER(SUBSTR(p.permit_num,1,4))=UPPER(SUBSTR(rec.permit_id,1,4))
           AND p.PERMIT_TYPE_CD  = v_permit_type AND p.CLOSE_DT is null and p.PERMIT_STATUS_TYPE_CD='ACTV' AND rec.closed_dt is NULL AND rec.PERMIT_STATUS='ACT'
           AND v_permit_id=0;

           v_permit_conflict_cnt:= v_tufa_permit_ids.count;

              IF v_permit_conflict_cnt > 0 THEN
                  v_errors:=v_errors||' ; PERMIT_ERROR : [ An active Permit with same Type and State already exists in TUFA for this Company. ]';
                  v_permit_action:='CONFLICT - PERMIT EXISTS';

                  FOR i IN 1 ..v_tufa_permit_ids.COUNT LOOP
                    v_permit_id := v_tufa_permit_ids(i);
                    UPDATE TU_PERMIT_UPDATES_AUDIT SET PERMIT_ID=rec.permit_id,PERMIT_ACTION=v_permit_action,CLOSE_DT=rec.closed_dt,ISSUE_DT=rec.issue_dt,ERRORS=v_errors,PERMIT_STATUS_TUFA='N/A',PERMIT_STATUS='N/A',PERMIT_ID_TUFA=v_permit_id,PERMIT_TYPE_CD= v_permit_type WHERE AUDIT_ID=v_audit_id;
                  END LOOP;
                   -- No Action

                  CONTINUE;
              END IF;
      END;

      --  If there are no errors during ingestion and there is no close date on the permit and the permit does not exist in TUFA then create it.
      --  otherwise if there are no errors during ingestion and there is a close date and permit exists in TUFA then update the status of permit.

      BEGIN

         -- 1. Cases when ingested Permit does not exist in TUFA
         --     a. PERMIT ADDED AND CLOSED
         --     b. PERMIT not ADDED as other PERMITS in EXCLUDED status
         --     c. PERMIT ADDED

         --     a. PERMIT ADDED AND CLOSED
        IF v_permit_id=0 AND v_company_id!=0 AND rec.closed_dt is not Null THEN
          v_permit_action := 'ADDED AND CLOSED' ;
          v_errors:=v_errors||'; PERMIT_ERROR : ['|| rec.permit_id ||' added with an Issue Date of '||to_char(rec.issue_dt,'MM/DD/YYYY') ||' and a Closed Date of '|| to_char(rec.closed_dt,'MM/DD/YYYY')||' ]';

         -- check if active or close date of the new permit overlaps with an existing exclusion range. If so flag an exclusion conflict else add and close the permit.
          SELECT
        COUNT(*)
    INTO v_excl_count
    FROM
        permit_excl_status_vw exclvw
    WHERE
        exclvw.company_id = v_company_id
        AND exclvw.excl_scope_id IN (
            1,
            2
        )
        AND ( ( v_issue_dt_fy_yr < assmnt_yr -- if exclusion year falls between active and close dt years of the permit then flag exclusion
                AND assmnt_yr < v_closed_dt_fy_yr )
              OR ( v_issue_dt_fy_yr = v_closed_dt_fy_yr -- if exclusion period year is same as active and close dt years and exclusion qtr is between active and close dt qtrs then flag exclusion
                   AND v_issue_dt_fy_yr = assmnt_yr
                   AND v_issue_dt_fy_qtr <= assmnt_qtr
                   AND v_closed_dt_fy_qtr >= assmnt_qtr )
              OR ( v_issue_dt_fy_yr < v_closed_dt_fy_yr -- if exclusion period year is in  active dt year and greater than equal to active qtr then flag exclusion OR
              -- if exclusion period year is in  close dt year and less than equal to close dt qtr then flag exclusion
                   AND ( ( v_closed_dt_fy_yr = assmnt_yr
                           AND v_closed_dt_fy_qtr >= assmnt_qtr )
                         OR ( v_issue_dt_fy_yr = assmnt_yr
                              AND v_issue_dt_fy_qtr <= assmnt_qtr ) ) ) );


          IF(v_excl_count >0) THEN
                UPDATE TU_PERMIT_UPDATES_AUDIT SET PERMIT_ID=rec.permit_id,CLOSE_DT=rec.closed_dt,ISSUE_DT=rec.issue_dt,
                PERMIT_STATUS='N/A',PERMIT_TYPE_CD= v_permit_type,ACTION_ID=2,PERMIT_EXCL_RESOLVED='N' WHERE AUDIT_ID=v_audit_id;
                continue;
           END IF ;

           -- if exclusion conflict is not flagged then check if there is any other  active permit,  if not then permit closed status should appear on
            -- Exclusion tab as well with 'added and closed' status on permit tab
           SELECT count(*) INTO v_actv_permits_cnt FROM tu_permit p
           WHERE p.company_id = v_company_id
           AND UPPER(p.permit_num)!=UPPER(rec.permit_id)
           AND p.CLOSE_DT is null and p.PERMIT_STATUS_TYPE_CD='ACTV';

            IF(v_actv_permits_cnt = 0 ) THEN
                    UPDATE TU_PERMIT_UPDATES_AUDIT SET PERMIT_ID=rec.permit_id,CLOSE_DT=rec.closed_dt,ISSUE_DT=rec.issue_dt,
                    PERMIT_STATUS=rec.permit_status,PERMIT_TYPE_CD= v_permit_type,ACTION_ID=1,PERMIT_EXCL_RESOLVED='N' WHERE AUDIT_ID=v_audit_id;
            END IF;

           /** Add permit to TUFA with the TUFA active date and TTB issue date populated from the ingested TTB files Issue date
           Close the permit and populate the TUFA Close date and TTB Inactive date with the ingested files Closed Date **/
          INSERT INTO TU_PERMIT(COMPANY_ID,PERMIT_NUM,ISSUE_DT,TTB_START_DT,CLOSE_DT,TTB_END_DT,PERMIT_TYPE_CD, PERMIT_STATUS_TYPE_CD, CREATED_DT)
          VALUES( v_company_id,rec.permit_id,rec.ISSUE_DT,rec.ISSUE_DT,rec.closed_dt,rec.closed_dt,v_permit_type,'CLSD',SYSDATE)
          RETURNING permit_status_type_cd into v_tufa_permit_status_type_cd;

        END IF;

        IF v_permit_id=0 AND v_company_id!=0 AND rec.closed_dt is  Null AND rec.issue_dt is not Null THEN
          -- b. PERMIT NOT ADDED AS OTHER PERMITS IN EXCLUDED STATUS
                -- Check if other permits exist and excluded. If so check if the active date for this permit is on or prior to exclusion range

         select count(*) into v_excl_count FROM
                (
                    SELECT
                        assmnt_yr,
                        assmnt_qtr,
                        perm_excl_audit_id,
                        excl_scope_id,
                        MAX(perm_excl_audit_id) OVER(
                                PARTITION BY company_id
                    ) AS max_excl_id
                    FROM
                    permit_excl_status_vw  exclvw
                    where exclvw.company_id = v_company_id
                    AND exclvw.excl_scope_id in (1,2)
                )
            WHERE perm_excl_audit_id = max_excl_id
            AND  ((excl_scope_id = 2 AND  ( assmnt_yr > v_issue_dt_fy_yr OR (assmnt_qtr >=v_issue_dt_fy_qtr AND assmnt_yr=v_issue_dt_fy_yr)))
                    OR excl_scope_id =1);


         IF(v_excl_count >0) THEN
            UPDATE TU_PERMIT_UPDATES_AUDIT SET PERMIT_ID=rec.permit_id,CLOSE_DT=rec.closed_dt,ISSUE_DT=rec.issue_dt,
            PERMIT_STATUS='N/A',PERMIT_TYPE_CD= v_permit_type,ACTION_ID=2,PERMIT_EXCL_RESOLVED='N' WHERE AUDIT_ID=v_audit_id;
            CONTINUE;
         END IF;

          v_permit_action := 'ADDED';

          -- c. PERMIT ADDED
                 -- Add Permit (populate TUFA Active Date and TTB Issue Date with the ingested permits Issue date)
          INSERT INTO TU_PERMIT( COMPANY_ID, PERMIT_NUM, ISSUE_DT,TTB_START_DT,PERMIT_TYPE_CD, PERMIT_STATUS_TYPE_CD, CREATED_DT)
          VALUES( v_company_id,rec.permit_id,rec.ISSUE_DT,rec.ISSUE_DT,v_permit_type,'ACTV',SYSDATE)
          RETURNING permit_status_type_cd into v_tufa_permit_status_type_cd;
        END IF;

        -- 2. Cases when ingested Permit exists in TUFA
        --      a. PERMIT FOUND IN TUFA
        --      b. ISSUE DATE CHANGE
        --      c. CLOSE DATE CHANGE/PERMIT DATE CHANGE (BOTH ISSUE AND CLOSED DATE CHANGED)
        --      d. INGESTED PERMIT IS  CLOSED AND PERMIT IN TUFA IS ACTIVE
        --      e. INGESTED PERMIT IS ALREADY CLOSED IN TUFA
        --      f. PERMIT CLOSED IN TUFA BUT ACTIVE IN TTB FILE

         --  a. PERMIT FOUND IN TUFA
        IF v_permit_id!=0 AND  v_company_id!=0 AND v_tufa_permit_type_cd =  v_permit_type AND TO_CHAR(v_tufa_permit_start_dt, 'DD-Mon-YY') = TO_CHAR(rec.issue_dt, 'DD-Mon-YY') THEN
          v_permit_action := 'FOUND MATCH';
           -- No action
        END IF ;

        --   b. ISSUE DATE CHANGE
         IF v_permit_id!=0 AND v_company_id!=0 AND rec.issue_dt is not Null AND v_tufa_permit_start_dt IS NOT NULL  AND trunc(rec.issue_dt) != trunc(v_tufa_permit_start_dt) THEN
            v_permit_action := 'ISSUE DATE CHANGE' ;
            v_errors:=v_errors||'; PERMIT_ERROR : [ '||v_tufa_permit_num ||' Issue Date '||to_char(v_tufa_permit_start_dt,'MM/DD/YYYY') ||' has been updated to '|| to_char(rec.issue_dt,'MM/DD/YYYY') ||']';
            -- Update the TTB Issue date in TUFA to the ingested TTB issue date
            UPDATE TU_PERMIT P SET P.TTB_START_DT=rec.issue_dt WHERE P.PERMIT_ID=v_permit_id
            RETURNING permit_status_type_cd into v_tufa_permit_status_type_cd;
        END IF;

        --  c. CLOSE DATE CHANGE/PERMIT DATE CHANGE (BOTH ISSUE AND CLOSED DATE CHANGED)
        IF v_permit_id!=0 AND v_company_id!=0 AND rec.closed_dt is not Null AND v_tufa_permit_end_dt IS NOT NULL  AND trunc(rec.closed_dt) != trunc(v_tufa_permit_end_dt) THEN
          IF v_permit_action = 'ISSUE DATE CHANGE' THEN
              v_permit_action := 'PERMIT DATE CHANGES';
          ELSE
              v_permit_action := 'CLOSE DATE CHANGE' ;
          END IF;

         v_errors:=v_errors||'; PERMIT_ERROR : [ '||v_tufa_permit_num ||' Close Date '||to_char(v_tufa_permit_end_dt,'MM/DD/YYYY') ||' has been updated to '|| to_char(rec.closed_dt,'MM/DD/YYYY') ||']';
        --  Update the TTB closed date in TUFA to the ingested TTB issue date
         UPDATE TU_PERMIT P SET P.TTB_END_DT=rec.closed_dt WHERE P.PERMIT_ID=v_permit_id
         RETURNING permit_status_type_cd into v_tufa_permit_status_type_cd;

        --  Check if the permit is excluded and if the close date change is across a qtr (+/- one qtr) insert entry into PERMIT_UPDATES_AUDIT table with exclusion details.
         SELECT count(exclvw.permit_id) into  v_permit_excl_cnt
         from permit_excl_status_vw exclvw
         where
         exclvw.company_id = v_company_id
         AND exclvw.permit_id=v_permit_id
         AND exclvw.EXCL_SCOPE_ID in (1,2);

         IF(v_permit_excl_cnt > 0  AND ( (ABS(v_tufa_cls_dt_fy_qtr - v_closed_dt_fy_qtr)>=1 AND v_tufa_cls_dt_fy_yr = v_closed_dt_fy_yr) OR ABS(v_tufa_cls_dt_fy_yr - v_closed_dt_fy_yr)>=1)) THEN
                UPDATE TU_PERMIT_UPDATES_AUDIT SET PERMIT_ID=rec.permit_id,CLOSE_DT=rec.closed_dt,ISSUE_DT=rec.issue_dt,
                PERMIT_STATUS=rec.permit_status,PERMIT_TYPE_CD= v_permit_type,ACTION_ID=3,PERMIT_EXCL_RESOLVED='N',TUFA_PERMIT_CLS_DT=v_tufa_permit_end_dt WHERE AUDIT_ID=v_audit_id;
                CONTINUE;
         END IF;

        END IF;

        -- d. INGESTED PERMIT IS  CLOSED AND PERMIT IN TUFA IS ACTIVE
        IF v_permit_id!=0 AND v_company_id!=0 AND rec.closed_dt IS NOT NULL AND rec.permit_status='CLS' AND v_tufa_permit_status_type_cd!='CLSD' THEN
          v_permit_action := 'CLOSED';
          v_errors:=v_errors||'; PERMIT_ERROR : [ ' || rec.permit_id ||' has been closed in TUFA. ]';


          -- Close Permit in TUFA (Populate the TUFA Inactive Date and the TTB Closed Date fields with the Ingested Permit file Closed date)
          UPDATE TU_PERMIT P SET P.PERMIT_STATUS_TYPE_CD='CLSD',P.CLOSE_DT=rec.closed_dt,P.TTB_END_DT=rec.closed_dt WHERE P.PERMIT_ID=v_permit_id
          RETURNING permit_status_type_cd into v_tufa_permit_status_type_cd;

          -- Check if other permits are active for the company. If not closed permit should show up on  Exclude tab else on Permit tab
           SELECT p.permit_id
           BULK COLLECT INTO v_tufa_permit_ids FROM tu_permit p
           WHERE p.company_id = v_company_id
           AND UPPER(p.permit_num)!=UPPER(rec.permit_id)
           AND p.CLOSE_DT is null and p.PERMIT_STATUS_TYPE_CD='ACTV';


          IF v_tufa_permit_ids.count = 0 THEN
                UPDATE TU_PERMIT_UPDATES_AUDIT SET PERMIT_ID=rec.permit_id,CLOSE_DT=rec.closed_dt,ISSUE_DT=rec.issue_dt,
                PERMIT_STATUS=rec.permit_status,PERMIT_TYPE_CD= v_permit_type,ACTION_ID=1,PERMIT_EXCL_RESOLVED='N' WHERE AUDIT_ID=v_audit_id;
                 CONTINUE;
          END IF;

        -- e. PERMIT IS ALREADY CLOSED
       ELSIF v_permit_id!=0 AND v_company_id!=0 AND rec.closed_dt IS NOT NULL AND rec.permit_status='CLS' AND v_tufa_permit_status_type_cd='CLSD' AND trunc(rec.issue_dt) = trunc(v_tufa_permit_start_dt)
        AND trunc(rec.closed_dt) = trunc(v_tufa_permit_end_dt)
        THEN

          v_permit_action := 'CLOSED';
          v_errors:=v_errors||'; PERMIT_ERROR : [ ' || rec.permit_id ||' is already closed in TUFA. ]';

       END IF;

        --  f. PERMIT CLOSED IN TUFA BUT ACTIVE IN TTB FILE
        IF v_permit_id!=0 AND v_company_id!=0 AND rec.permit_status='ACT' AND v_tufa_permit_status_type_cd='CLSD' THEN
            v_permit_action := 'CONFLICT - PERMIT STATUS' ;
            v_errors:=v_errors||'; PERMIT_ERROR : [ ' || rec.permit_id ||' is closed in TUFA but is listed as active in the TTB Permit file.]';
            --  No Action
        END IF;


          DBMS_OUTPUT.PUT_LINE('v_permit_action '|| v_permit_action || '  v_company_action   ' ||v_company_action || ' v_errors ' || v_errors);

        UPDATE TU_PERMIT_UPDATES_AUDIT SET PERMIT_ID=rec.permit_id,PERMIT_ACTION=v_permit_action,CLOSE_DT=rec.closed_dt,ISSUE_DT=rec.issue_dt,ERRORS=v_errors,PERMIT_STATUS_TUFA=v_tufa_permit_status_type_cd,PERMIT_STATUS=rec.permit_status,PERMIT_ID_TUFA=v_permit_id,PERMIT_TYPE_CD= v_permit_type WHERE AUDIT_ID=v_audit_id;

      END;

    END LOOP;
    COMMIT;
  END;
END UPDATE_PERMIT_STATUSES;

/

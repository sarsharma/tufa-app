CREATE OR REPLACE FORCE VIEW PRIMARY_CONTACT_VW
(
    COMPANY_ID,
    CONTACT_ID,
    COUNTRY_CD,
    EMAIL_ADDRESS,
    FAX_NUM,
    FIRST_NM,
    LAST_NM,
    PHONE_EXT,
    CNTRY_DIAL_CD,
    PHONE_NUM
)
    BEQUEATH DEFINER
AS
    SELECT b.company_id,
           contact_id,
           country_cd,
           email_address,
           fax_num,
           first_nm,
           last_nm,
           phone_ext,
           cntry_dial_cd,
           phone_num
      FROM tu_contact b
     WHERE b.created_dt = (SELECT MIN (created_dt)
                             FROM tu_contact
                            WHERE company_id = b.company_id);


CREATE OR REPLACE FORCE EDITIONABLE VIEW "RANKED_QTRLY_ASSESSMENT_VW" ("ASSESSMENT_ID", "VERSION_NUM", "MS_RANK", "ASSESSMENT_YR", "ASSESSMENT_QTR", "LEGAL_NM", "EIN", "PERMITNUM", "TOT_VOL_REMOVED", "TOT_TAXES_PAID", "SHARE_TOT_TAXES", "STREET_ADDRESS", "CITY", "STATE", "PROVINCE", "POSTAL_CD", "CNTRY_DIAL_CD", "PHONE_NUM", "EMAIL_ADDRESS", "TOBACCO_CLASS_NM", "CREATED_DT")
AS
  SELECT a.assessment_id,
    c.version_num,
    c.ms_rank,
    a.assessment_yr,
    a.assessment_qtr,
    b.legal_nm,
    b.ein,
    '' PERMITNUM,
    c.tot_vol_removed,
    c.tot_taxes_paid,
    c.share_tot_taxes,
    e.street_address,
    e.city,
    e.state,
    e.province,
    e.postal_cd,
    f.cntry_dial_cd,
    f.phone_num,
    f.email_address,
    d.tobacco_class_nm,
    c.created_dt
  FROM tu_market_share c
    INNER JOIN tu_assessment a on c.assessment_id = a.assessment_id
    INNER JOIN tu_company b on c.company_id = b.company_id
    INNER JOIN tu_tobacco_class d on c.tobacco_class_id = d.tobacco_class_id
    LEFT OUTER JOIN tu_assessment_address e on (c.company_id = e.company_id AND c.assessment_id = e.assessment_id AND c.version_num = e.version_num)
    LEFT OUTER JOIN tu_assessment_contact f on (c.company_id = f.company_id AND c.assessment_id = f.assessment_id AND c.version_num = f.version_num)
  ORDER BY assessment_id, version_num, tobacco_class_nm,
    ms_rank ASC;

--------------------------------------------------------
--  DDL for Trigger DELTA_CHANGE_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."DELTA_CHANGE_TRIGGER" AFTER
    INSERT OR UPDATE OR DELETE ON tu_form_detail
    REFERENCING
            OLD AS old
            NEW AS new
    FOR EACH ROW
DECLARE
    v_tobacco_class_id   NUMBER;
    v_form_id            NUMBER;
    v_form_type_cd       CHAR(4);
    v_ein                VARCHAR2(100);
    v_permit_type        CHAR(4);
    v_fiscal_yr          NUMBER;
    v_quarter            VARCHAR(3);
    v_tobacco_class_nm   VARCHAR2(25);

    my_sqlerrm           VARCHAR2(500);
BEGIN
    IF inserting OR updating OR deleting THEN
        IF :old.tobacco_class_id IS NOT NULL THEN
            v_tobacco_class_id :=:old.tobacco_class_id;
            v_form_id :=:old.form_id;
        ELSE
            v_tobacco_class_id :=:new.tobacco_class_id;
            v_form_id :=:new.form_id;
        END IF;
    -- Select the form type and tobacco class id based on the new or old form id and tobacco class id

        SELECT
            sf.form_type_cd
        INTO v_form_type_cd
        FROM
            tu_submitted_form sf
        WHERE
            sf.form_id = v_form_id;

        IF v_form_type_cd = '3852' AND ( :old.taxes_paid <>:new.taxes_paid OR
        (:old.taxes_paid IS NULL and :new.taxes_paid IS NOT NULL AND :new.taxes_paid <> '0') OR
        (:new.taxes_paid IS NULL and :old.taxes_paid IS NOT NULL AND :old.taxes_paid <> '0')) THEN
            SELECT
                ein,
                permit_type_cd,
                fiscal_yr,
                DECODE(tc.tobacco_class_nm,'Cigars','1-4',TO_CHAR(quarter) ) AS quarter,
                DECODE(permit_type_cd,'IMPT',tc.tobacco_class_nm,'MANU',DECODE(ptc.tobacco_class_nm,'Chew-and-Snuff','Chew/Snuff'
               ,'Pipe-RYO','Pipe/Roll-Your-Own','Cigars','Cigars',tc.tobacco_class_nm) )
            INTO
                v_ein,
                v_permit_type,
                v_fiscal_yr,
                v_quarter,
                v_tobacco_class_nm
            FROM
                tu_company co
                LEFT JOIN tu_permit perm ON co.company_id = perm.company_id
                LEFT JOIN tu_period_status ps ON ps.permit_id = perm.permit_id
                LEFT JOIN tu_rpt_period rp ON rp.period_id = ps.period_id
                LEFT JOIN tu_submitted_form sf ON perm.permit_id = sf.permit_id
                                                  AND rp.period_id = sf.period_id
                LEFT JOIN tu_tobacco_class tc ON tc.tobacco_class_id = v_tobacco_class_id
                LEFT JOIN tu_tobacco_class ptc ON ptc.tobacco_class_id = tc.parent_class_id
            WHERE
                sf.form_id = v_form_id;

            UPDATE tu_comparison_all_delta_sts sts
            SET
                status = 'In Progress'
            WHERE
                sts.ein = v_ein
                AND sts.permit_type = v_permit_type
                AND sts.fiscal_yr = v_fiscal_yr
                AND sts.fiscal_qtr = v_quarter
                AND sts.tobacco_class_nm = v_tobacco_class_nm
                AND status NOT IN (
                    'Excluded X',
                    'Associated',
                    'In Progress X'
                );

        END IF;

    ELSE
        NULL;
    END IF;
--    EXCEPTION WHEN OTHERS THEN  
--        my_sqlerrm := SUBSTR(SQLERRM, 1,500);
--        INSERT INTO TU_TRIGGER_LOGGING VALUES (
--                    'delta_change_trigger', my_sqlerrm, SYSDATE);
--        raise_application_error(-20000, 'Error in delta_change_trigger. Please check the trigger logging table for more information');
END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."DELTA_CHANGE_TRIGGER" ENABLE;

--------------------------------------------------------
--  DDL for Trigger DETAIL_DELTA_CHANGE_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."DETAIL_DELTA_CHANGE_TRIGGER" AFTER
    UPDATE ON tu_cbp_entry
    REFERENCING
            OLD AS old
            NEW AS new
    FOR EACH ROW
DECLARE
    v_tobacco_class_id   NUMBER;
    v_form_id            NUMBER;
    v_form_type_cd       CHAR(4);
    v_ein                VARCHAR2(100);
    v_permit_type        CHAR(4);
    v_fiscal_yr          NUMBER;
    v_old_quarter        VARCHAR(3);
    v_new_quarter        VARCHAR(3);
    v_tobacco_class_nm   VARCHAR2(25);
    v_exists             CHAR(1);

    my_sqlerrm           VARCHAR2(500);
BEGIN
    IF updating AND :old.tobacco_class_id IS NOT NULL THEN
        IF :old.fiscal_qtr <>:new.fiscal_qtr OR :old.reallocated_flag <>:new.reallocated_flag OR :old.excluded_flag <>:new.excluded_flag
        OR (:old.reallocated_flag IS NULL AND :new.reallocated_flag IS NOT NULL AND :new.reallocated_flag <> 'N' ) 
        OR (:old.reallocated_flag IS NOT NULL AND :old.reallocated_flag <> 'N' AND :new.reallocated_flag IS NULL ) 
        OR (:old.excluded_flag IS NULL AND :new.excluded_flag IS NOT NULL AND :new.excluded_flag <> 'N' ) 
        OR (:old.excluded_flag IS NOT NULL AND :old.excluded_flag <> 'N' AND :new.excluded_flag IS NULL ) THEN


--    INSERT INTO TU_TRIGGER_LOGGING VALUES ('Line 1', :old.cbp_importer_id || ' ' || :old.cbp_entry_id, SYSDATE);

            SELECT
                CASE
                    WHEN co.ein IS NOT NULL THEN co.ein
                    ELSE imp.importer_ein
                END as
            INTO v_ein
            FROM
                tu_cbp_importer imp
                JOIN tu_company co ON 
                DECODE(imp.association_type_cd, 'IMPT', imp.importer_ein, 'CONS', imp.consignee_ein, DECODE(imp.consignee_exists_flag, 'N',imp.importer_ein, NULL)) = co.ein
            WHERE
                imp.cbp_importer_id =:old.cbp_importer_id;

            SELECT
                tobacco_class_nm
            INTO v_tobacco_class_nm
            FROM
                tu_tobacco_class
            WHERE
                tobacco_class_id =:old.tobacco_class_id;

            v_permit_type := 'IMPT';
            v_fiscal_yr :=:old.fiscal_year;
            IF :old.tobacco_class_id = 8 THEN
                v_old_quarter := '1-4';
                v_new_quarter := '1-4';
            ELSE
                v_old_quarter := TO_CHAR(:old.fiscal_qtr);
                v_new_quarter := TO_CHAR(:new.fiscal_qtr);
            END IF;

            UPDATE tu_comparison_all_delta_sts sts
            SET
                status = 'In Progress'
            WHERE
                sts.ein = v_ein
                AND sts.permit_type = v_permit_type
                AND sts.fiscal_yr = v_fiscal_yr
                AND sts.fiscal_qtr IN (
                    v_old_quarter,
                    v_new_quarter
                )
                AND sts.tobacco_class_nm = v_tobacco_class_nm
                AND status NOT IN (
                    'Excluded X',
                    'Associated',
                    'In Progress X'
                );

            IF :old.original_cbp_importer_id IS NOT NULL AND :old.fiscal_qtr <>:new.fiscal_qtr THEN
                SELECT
                    CASE
                        WHEN co.ein IS NOT NULL THEN co.ein
                        ELSE imp.importer_ein
                    END as
                INTO v_ein
                FROM
                    tu_cbp_importer imp
                    JOIN tu_company co ON 
                    DECODE(imp.association_type_cd, 'IMPT', imp.importer_ein, 'CONS', imp.consignee_ein, DECODE(imp.consignee_exists_flag, 'N',imp.importer_ein, NULL)) = co.ein
                WHERE
                    imp.cbp_importer_id =:old.original_cbp_importer_id;

                BEGIN
                    SELECT
                        'X'
                    INTO v_exists
                    FROM
                        tu_comparison_all_delta_sts sts
                    WHERE
                        sts.ein = v_ein
                        AND sts.permit_type = v_permit_type
                        AND sts.fiscal_yr = v_fiscal_yr
                        AND sts.fiscal_qtr = v_new_quarter
                        AND sts.tobacco_class_nm = v_tobacco_class_nm;

                EXCEPTION
                    WHEN no_data_found THEN
                        INSERT INTO tu_comparison_all_delta_sts (
                        ein,
                        permit_type,
                        fiscal_yr,
                        fiscal_qtr,
                        tobacco_class_nm,
                        status
                    ) VALUES (
                        v_ein,
                        v_permit_type,
                        v_fiscal_yr,
                        v_new_quarter,
                        v_tobacco_class_nm,
                        'Associated'
                    );
                END;

            END IF;

        END IF;

    ELSE
        NULL;
    END IF;

--    EXCEPTION WHEN OTHERS THEN  
--        my_sqlerrm := SUBSTR(SQLERRM, 1,500);
--        INSERT INTO TU_TRIGGER_LOGGING VALUES (
--                    'detail_delta_change_trigger', my_sqlerrm, SYSDATE);
--        raise_application_error(-20000, 'Error in detail_delta_change_trigger. Please check the trigger logging table for more information');
END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."DETAIL_DELTA_CHANGE_TRIGGER" ENABLE;

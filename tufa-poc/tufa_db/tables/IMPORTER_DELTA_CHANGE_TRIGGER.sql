--------------------------------------------------------
--  DDL for Trigger IMPORTER_DELTA_CHANGE_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."IMPORTER_DELTA_CHANGE_TRIGGER" AFTER
    UPDATE ON tu_cbp_importer
    REFERENCING
            OLD AS old
            NEW AS new
    FOR EACH ROW
DECLARE
    v_tobacco_class_id   NUMBER;
    v_ein                VARCHAR2(100);
    v_permit_type        CHAR(4);
    v_fiscal_yr          NUMBER;

    my_sqlerrm           VARCHAR2(500);
BEGIN
    IF updating IS NOT NULL THEN
        IF :old.include_flag <>:new.include_flag 
        OR (:old.include_flag IS NULL AND :new.include_flag IS NOT NULL AND :new.include_flag = 'N' )
        OR (:old.include_flag IS NOT NULL AND :old.include_flag = 'N' AND :new.include_flag IS NULL ) THEN


        v_permit_type := 'IMPT';
        v_ein := :old.importer_ein;
        v_fiscal_yr := :old.fiscal_yr;

            UPDATE tu_comparison_all_delta_sts sts
            SET
                status = 'In Progress'
            WHERE
                sts.ein = v_ein
                AND sts.permit_type = v_permit_type
                AND sts.fiscal_yr = v_fiscal_yr
                AND status NOT IN (
                    'Excluded X',
                    'Associated',
                    'In Progress X'
                );

        END IF;

    ELSE
        NULL;
    END IF;

--    EXCEPTION WHEN OTHERS THEN  
--        my_sqlerrm := SUBSTR(SQLERRM, 1,500);
--        INSERT INTO TU_TRIGGER_LOGGING VALUES (
--                    'importer_delta_change_trigger', my_sqlerrm, SYSDATE);
--        raise_application_error(-20000, 'Error in importer_delta_change_trigger: ' || my_sqlerrm);
END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."IMPORTER_DELTA_CHANGE_TRIGGER" ENABLE;

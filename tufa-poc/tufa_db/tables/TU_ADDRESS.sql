--------------------------------------------------------
--  DDL for Table TU_ADDRESS
--------------------------------------------------------

  CREATE TABLE "CTP_TUFA_MVP13"."TU_ADDRESS" 
   (	"COMPANY_ID" NUMBER, 
	"ADDRESS_TYPE_CD" CHAR(4 BYTE), 
	"STREET_ADDRESS" VARCHAR2(120 BYTE), 
	"SUITE" VARCHAR2(20 BYTE), 
	"ATTENTION" VARCHAR2(40 BYTE), 
	"CITY" VARCHAR2(40 BYTE), 
	"STATE" CHAR(2 BYTE), 
	"PROVINCE" VARCHAR2(20 BYTE), 
	"POSTAL_CD" VARCHAR2(10 BYTE), 
	"FAX_NUM" VARCHAR2(10 BYTE), 
	"CNTRY_DIAL_CD" VARCHAR2(5 BYTE), 
	"COUNTRY_CD" CHAR(2 BYTE), 
	"HASH_TOTAL" NUMBER, 
	"CREATED_BY" VARCHAR2(50 BYTE), 
	"CREATED_DT" DATE, 
	"MODIFIED_BY" VARCHAR2(50 BYTE), 
	"MODIFIED_DT" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CTP_TUFA_DATA" ;

--------------------------------------------------------
--  DDL for Trigger TU_ADDRESS_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_ADDRESS_T1" 
 BEFORE INSERT OR UPDATE
 ON tu_address
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE
	v_CHECKCNTRYval char(1);
	v_ADDRESSTYPEval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;

 END IF;

 IF :NEW.country_cd is NOT NULL THEN
 SELECT 'Y'
    into v_CHECKCNTRYval
    from tu_iso_cntry
   WHERE iso_cntry_cd = :new.country_cd;
 END IF;
 IF v_CHECKCNTRYval != 'Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Country Type Code -- '||:new.country_cd);
 END IF;

 SELECT 'Y'
    into v_ADDRESSTYPEval
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'ADDRESS_TYPE'
     and b.tu_ref_code = :new.address_type_cd;
 EXCEPTION when NO_DATA_FOUND
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Address Type Code  '||:new.address_type_cd);
  WHEN OTHERS then NULL;

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_ADDRESS_T1" ENABLE;

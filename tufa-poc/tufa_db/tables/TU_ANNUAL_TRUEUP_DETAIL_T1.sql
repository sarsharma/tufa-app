--------------------------------------------------------
--  DDL for Trigger TU_ANNUAL_TRUEUP_DETAIL_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_ANNUAL_TRUEUP_DETAIL_T1" 
 BEFORE INSERT OR UPDATE
 ON tu_annual_trueup_detail
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;
   END IF;
   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;
 ELSIF UPDATING THEN   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_ANNUAL_TRUEUP_DETAIL_T1" ENABLE;

--------------------------------------------------------
--  DDL for Trigger TU_ANNUAL_TRUEUP_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_ANNUAL_TRUEUP_T1" 
 BEFORE INSERT OR UPDATE
 ON TU_ANNUAL_TRUEUP
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

 IF :NEW.SUBMITTED_IND = 'Y'
   AND :NEW.SUBMITTED_DT IS NULL THEN
    RAISE_APPLICATION_ERROR(-20004,'TU_ANNUAL_TRUEUP.SUBMITTED_DT cannot be null');
 END IF;

IF :NEW.SUBMITTED_IND = 'Y'
   AND :NEW.SUBMITTED_BY IS NULL THEN
    RAISE_APPLICATION_ERROR(-20004,'TU_ANNUAL_TRUEUP.SUBMITTED_BY cannot be null');
 END IF;

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_ANNUAL_TRUEUP_T1" ENABLE;

--------------------------------------------------------
--  DDL for Trigger TU_ASSESSMENT_VERSION_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_ASSESSMENT_VERSION_T1" 
 BEFORE INSERT OR UPDATE
 ON TU_ASSESSMENT_VERSION
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
BEGIN
  IF UPDATING THEN
   /*** Populate modified_dt and modified_by  ***/
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
 END IF;
END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_ASSESSMENT_VERSION_T1" ENABLE;

--------------------------------------------------------
--  DDL for Trigger TU_COMPANY_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_COMPANY_T1" 
 BEFORE INSERT OR UPDATE
 ON TU_COMPANY
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE
    v_EIN_TEST_RESULT NUMBER;
    v_EIN             CHAR(9);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;

     v_EIN := :NEW.EIN;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;

    v_EIN := :NEW.EIN;

 END IF;

 SELECT nvl(LENGTH(TRIM(BOTH '\' FROM (TRANSLATE(v_EIN, '0123456789','\')))),0)
   INTO v_EIN_TEST_RESULT
   FROM dual;

 IF v_EIN_TEST_RESULT != 0 then
	RAISE_APPLICATION_ERROR(-20001,'EIN must be nine digits -- '||v_EIN);
 END IF;

 IF :NEW.company_status not in ('ACTV','INAC')
   THEN RAISE_APPLICATION_ERROR(-20004,'Invalid Company Status Code  '||:new.company_status);
 END IF;

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_COMPANY_T1" ENABLE;

--------------------------------------------------------
--  DDL for Trigger TU_CONTACT_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_CONTACT_T1" 
 BEFORE INSERT OR UPDATE
 ON tu_contact
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE
	v_CHECKCNTRYval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;

 END IF;

 IF :NEW.country_cd is NOT NULL THEN
 SELECT 'Y'
    into v_CHECKCNTRYval
    from tu_iso_cntry
   WHERE iso_cntry_cd = :new.country_cd;
 ELSE v_CHECKCNTRYval := 'Y';
 END IF;
 IF v_CHECKCNTRYval != 'Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Country Code -- '||:new.country_cd);
 END IF;

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_CONTACT_T1" ENABLE;

--------------------------------------------------------
--  DDL for Trigger TU_DOCUMENT_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_DOCUMENT_T1" 
 BEFORE INSERT OR UPDATE
 ON tu_document
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE
	v_DOCSTATUSval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;

	   SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;

 END IF;

 IF :NEW.DOC_STATUS_CD is NOT NULL THEN
 SELECT 'Y'
    into v_DOCSTATUSval
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'DOC_STATUS_TYPE'
     and b.tu_ref_code = :new.doc_status_cd;
 ELSE v_DOCSTATUSval := 'X';
 END IF;
 IF v_DOCSTATUSval != 'Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Document Status Code  '||:new.doc_status_cd);
 END IF;

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_DOCUMENT_T1" ENABLE;

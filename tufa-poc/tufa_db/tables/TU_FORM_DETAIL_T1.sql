--------------------------------------------------------
--  DDL for Trigger TU_FORM_DETAIL_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_FORM_DETAIL_T1" 
 BEFORE INSERT OR UPDATE
 ON tu_form_detail
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE v_REMOVALUOMval char(1);
        v_ACTIVITYval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;

 END IF;

/* IF :NEW.REMOVAL_UOM IS NOT NULL THEN
 SELECT 'Y'
    into v_REMOVALUOMval
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'REMOVAL_UOM'
     and b.tu_ref_code = :new.removal_uom;
 ELSE v_REMOVALUOMval := 'X';
 END IF;
 IF v_REMOVALUOMval != 'Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Unit of Measure Code  '||:new.removal_uom);
 END IF;

 IF :NEW.ACTIVITY_CD IS NOT NULL THEN
   BEGIN
     SELECT :NEW.ACTIVITY_CD
       into v_ACTIVITYval
       from tu_ref_type a, tu_ref_value b
     WHERE a.tu_ref_type_id = b.tu_ref_type_id
       and a.tu_ref_type_nm = 'ACTIVITY_TYPE'
       and b.tu_ref_code = :new.activity_cd;
   EXCEPTION when no_data_found
     THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Activity Type Code  '||:new.activity_cd);
   END;
   END IF;
*/

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_FORM_DETAIL_T1" ENABLE;

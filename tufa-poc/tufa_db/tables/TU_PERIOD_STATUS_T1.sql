--------------------------------------------------------
--  DDL for Trigger TU_PERIOD_STATUS_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_PERIOD_STATUS_T1" 
 BEFORE INSERT OR UPDATE
 ON TU_PERIOD_STATUS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE
    v_PERSTATUSval char(4);
    v_RECSTATUSTYPE char(4);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;

  ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;

  END IF;

 IF :NEW.period_status_type_cd is NOT NULL THEN
    BEGIN
      SELECT :NEW.period_status_type_cd
        into v_PERSTATUSval
        from tu_ref_type a, tu_ref_value b
      WHERE a.tu_ref_type_id = b.tu_ref_type_id
        and a.tu_ref_type_nm = 'PERIOD_STATUS_TYPE'
        and b.tu_ref_code = :new.period_status_type_cd;
    EXCEPTION when no_data_found
      THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Period Status Type Code  '||:new.period_status_type_cd);
    END;
    END IF;

 IF :NEW.recon_status_type_cd is NOT NULL THEN
    BEGIN
      SELECT :NEW.recon_status_type_cd
        into v_RECSTATUSTYPE
        from tu_ref_type a, tu_ref_value b
      WHERE a.tu_ref_type_id = b.tu_ref_type_id
        and a.tu_ref_type_nm = 'RECON_STATUS_TYPE'
        and b.tu_ref_code = :new.recon_status_type_cd;
    EXCEPTION when no_data_found
      THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Recon Status Type Code  '||:new.recon_status_type_cd);
    END;
    END IF;

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_PERIOD_STATUS_T1" ENABLE;

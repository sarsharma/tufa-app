--------------------------------------------------------
--  DDL for Table TU_PERMIT_EXCL_COMMENT_DOC
--------------------------------------------------------

  CREATE TABLE "CTP_TUFA_MVP13"."TU_PERMIT_EXCL_COMMENT_DOC" 
   (	"DOCUMENT_ID" NUMBER GENERATED BY DEFAULT ON NULL AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE , 
	"COMMENT_SEQ" NUMBER, 
	"DOC_FILE_NM" VARCHAR2(240 BYTE), 
	"DOC_DESC" VARCHAR2(240 BYTE), 
	"DOC_STATUS_CD" CHAR(4 BYTE), 
	"REPORT_PDF" BLOB, 
	"FORM_TYPES" VARCHAR2(100 BYTE), 
	"CREATED_BY" VARCHAR2(50 BYTE), 
	"CREATED_DT" DATE, 
	"MODIFIED_BY" VARCHAR2(50 BYTE), 
	"MODIFIED_DT" DATE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "CTP_TUFA_DATA" 
 LOB ("REPORT_PDF") STORE AS SECUREFILE (
  TABLESPACE "CTP_TUFA_DATA" ENABLE STORAGE IN ROW CHUNK 8192
  NOCACHE LOGGING  NOCOMPRESS  KEEP_DUPLICATES ) ;

--------------------------------------------------------
--  DDL for Trigger TU_PERMIT_PERIOD_FORMS_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_PERMIT_PERIOD_FORMS_T1" 
 BEFORE INSERT OR UPDATE
 ON TU_PERMIT_PERIOD_FORMS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE
	v_DEFECTTYPEval CHAR(1);
	v_FORMTYPEval CHAR(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;

 END IF;

IF :NEW.form_type_CD IS NOT NULL THEN
 BEGIN
 SELECT 'Y'
    into v_FORMTYPEval
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'FORM_TYPE'
     and b.tu_ref_code = :new.form_type_cd;
 EXCEPTION
  when no_data_found THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Forms Type Code  '||:new.form_type_cd);
 END;
END IF;

/* IF :NEW.defect_status_CD IS NOT NULL THEN
 BEGIN
 SELECT 'Y'
    into v_DEFECTTYPEval
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'DEFECT_STATUS_TYPE'
     and b.tu_ref_code = :new.defect_status_cd;
 EXCEPTION
  when no_data_found THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Defect Status Type Code  '||:new.defect_status_cd);
 END;
END IF; */

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_PERMIT_PERIOD_FORMS_T1" ENABLE;

--------------------------------------------------------
--  DDL for Trigger TU_PERMIT_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_PERMIT_T1" 
 BEFORE INSERT OR UPDATE
 ON tu_permit
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE v_PERMITTYPEval char(1);
        v_PERMITSTATUSTYPEval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;

 END IF;

 IF :new.permit_type_cd is NOT NULL THEN
  SELECT 'Y'
    into v_PERMITTYPEval
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'PERMIT_TYPE'
     and b.tu_ref_code = :new.permit_type_cd;
 ELSE v_PERMITTYPEval := 'X';
 END IF;
 IF v_PERMITTYPEval != 'Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Permit Type Code  '||:new.permit_type_cd);
 END IF;

 IF :new.permit_status_type_cd is NOT NULL THEN
  SELECT 'Y'
    into v_PERMITSTATUSTYPEval
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'PERMIT_STATUS_TYPE'
     and b.tu_ref_code = :new.permit_status_type_cd;
 ELSE v_PERMITTYPEval := 'X';
 END IF;
 IF v_PERMITSTATUSTYPEval != 'Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Permit Status Type Code  '||:new.permit_status_type_cd);
 END IF;

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_PERMIT_T1" ENABLE;

--------------------------------------------------------
--  DDL for Trigger TU_REF_TYPE_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_REF_TYPE_T1" 
 BEFORE INSERT OR UPDATE
 ON tu_ref_type
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.created_dt
	   FROM DUAL;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;

 END IF;

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_REF_TYPE_T1" ENABLE;

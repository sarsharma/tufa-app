--------------------------------------------------------
--  DDL for Trigger TU_SUBMITTED_FORM_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_SUBMITTED_FORM_T1" 
 BEFORE INSERT OR UPDATE
 ON tu_submitted_form
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE v_checkrefval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;

 END IF;

 SELECT 'Y'
    into v_checkrefval
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'FORM_TYPE'
     and b.tu_ref_code = :new.form_type_cd;
 EXCEPTION when NO_DATA_FOUND
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Form Type Code  '||:new.form_type_cd);
  WHEN OTHERS then NULL;

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_SUBMITTED_FORM_T1" ENABLE;

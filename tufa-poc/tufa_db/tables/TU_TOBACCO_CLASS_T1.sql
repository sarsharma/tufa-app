--------------------------------------------------------
--  DDL for Trigger TU_TOBACCO_CLASS_T1
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "CTP_TUFA_MVP13"."TU_TOBACCO_CLASS_T1" 
 BEFORE INSERT OR UPDATE
 ON tu_tobacco_class
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE v_CLASSTYPEval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;

     SELECT SYSDATE
       INTO :NEW.modified_dt
	   FROM DUAL;

 END IF;

 SELECT 'Y'
    into v_CLASSTYPEval
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'CLASS_TYPE'
     and b.tu_ref_code = :new.class_type_cd;
 EXCEPTION when NO_DATA_FOUND
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Tobacco Class Code  '||:new.class_type_cd);
  WHEN OTHERS then NULL;

END;
/
ALTER TRIGGER "CTP_TUFA_MVP13"."TU_TOBACCO_CLASS_T1" ENABLE;

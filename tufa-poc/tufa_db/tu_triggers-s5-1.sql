PROMPT
PROMPT===========================================================
PROMPT tu_audit_triggers-s5-1.sql - BEGIN
PROMPT===========================================================
PROMPT
/*-------------------------------------------------------------*/
/*
/*Purpose: Create triggers to populate audit columns on each /*   /*         table
/*
/*Created: 10/21/2016 by Tony Ireland
/*Updated: 11/10/2016 by Tony Ireland
/*  Added triggers for new tables:
/*    tu_permit_contact_xref
/*    tu_permit_address_xref
/*    tu_document_type
/*    tu_form_detail
/*
/*Updated: 11/22/2016 by Tony Ireland
/*  Added trigger for new table:
/*    tu_form_comment
/*
/*-------------------------------------------------------------*/
--

CREATE OR REPLACE TRIGGER TU_ADDRESS_T1
 BEFORE INSERT OR UPDATE
 ON tu_address
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE
	v_CHECKCNTRYval char(1);
	v_ADDRESSTYPEval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

 IF :NEW.country_cd is NOT NULL THEN
 SELECT 'Y'
    into v_CHECKCNTRYval 
    from tu_iso_cntry
   WHERE iso_cntry_cd = :new.country_cd;
 END IF;
 IF v_CHECKCNTRYval != 'Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Country Type Code -- '||:new.country_cd);
 END IF;

 SELECT 'Y'
    into v_ADDRESSTYPEval 
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'ADDRESS_TYPE'
     and b.tu_ref_code = :new.address_type_cd;
 EXCEPTION when NO_DATA_FOUND
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Address Type Code  '||:new.address_type_cd);
  WHEN OTHERS then NULL;

END;
/


CREATE OR REPLACE TRIGGER TU_PERIOD_STATUS_T1
 BEFORE INSERT OR UPDATE
 ON TU_PERIOD_STATUS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE
    v_PERSTATUSval char(4);
    v_STATUSTYPTYPE char(4);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

 IF :NEW.period_status_type_cd is NOT NULL THEN
    BEGIN
      SELECT :NEW.period_status_type_cd
        into v_PERSTATUSval
        from tu_ref_type a, tu_ref_value b
      WHERE a.tu_ref_type_id = b.tu_ref_type_id
        and a.tu_ref_type_nm = 'PERIOD_STATUS_TYPE'
        and b.tu_ref_code = :new.period_status_type_cd;
    EXCEPTION when no_data_found
      THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Period Status Type Code  '||:new.period_status_type_cd);
    END;
    END IF;

 IF :NEW.recon_status_type_cd is NOT NULL THEN
    BEGIN
      SELECT :NEW.recon_status_type_cd
        into v_STATUSTYPTYPE
        from tu_ref_type a, tu_ref_value b
      WHERE a.tu_ref_type_id = b.tu_ref_type_id
        and a.tu_ref_type_nm = 'RECON_STATUS_TYPE'
        and b.tu_ref_code = :new.recon_status_type_cd;
    EXCEPTION when no_data_found
      THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Recon Status Type Code  '||:new.recon_status_type_cd);
    END;
    END IF;

END;
/


CREATE OR REPLACE TRIGGER TU_DOCUMENT_T1
 BEFORE INSERT OR UPDATE
 ON tu_document
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE
	v_DOCSTATUSval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

 IF :NEW.DOC_STATUS_CD is NOT NULL THEN
 SELECT 'Y'
    into v_DOCSTATUSval 
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'DOC_STATUS_TYPE'
     and b.tu_ref_code = :new.doc_status_cd;
 ELSE v_DOCSTATUSval := 'X';
 END IF;
 IF v_DOCSTATUSval != 'Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Document Status Code  '||:new.doc_status_cd);
 END IF;

END;
/


CREATE OR REPLACE TRIGGER TU_RPT_PERIOD_T1
 BEFORE INSERT OR UPDATE
 ON tu_rpt_period
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

END;
/

CREATE OR REPLACE TRIGGER TU_PERMIT_T1
 BEFORE INSERT OR UPDATE
 ON tu_permit
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE v_PERMITTYPEval char(1);
        v_PERMITSTATUSTYPEval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

 IF :new.permit_type_cd is NOT NULL THEN
  SELECT 'Y'
    into v_PERMITTYPEval 
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'PERMIT_TYPE'
     and b.tu_ref_code = :new.permit_type_cd;
 ELSE v_PERMITTYPEval := 'X';
 END IF;
 IF v_PERMITTYPEval != 'Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Permit Type Code  '||:new.permit_type_cd);
 END IF;

 IF :new.permit_status_type_cd is NOT NULL THEN
  SELECT 'Y'
    into v_PERMITSTATUSTYPEval 
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'PERMIT_STATUS_TYPE'
     and b.tu_ref_code = :new.permit_status_type_cd;
 ELSE v_PERMITTYPEval := 'X';
 END IF;
 IF v_PERMITSTATUSTYPEval != 'Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Permit Status Type Code  '||:new.permit_status_type_cd);
 END IF;

END;
/


CREATE OR REPLACE TRIGGER TU_CONTACT_T1
 BEFORE INSERT OR UPDATE
 ON tu_contact
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE
	v_CHECKCNTRYval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

 IF :NEW.country_cd is NOT NULL THEN
 SELECT 'Y'
    into v_CHECKCNTRYval 
    from tu_iso_cntry
   WHERE iso_cntry_cd = :new.country_cd;
 ELSE v_CHECKCNTRYval := 'Y';
 END IF;
 IF v_CHECKCNTRYval != 'Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Country Code -- '||:new.country_cd);
 END IF;

END;
/

CREATE OR REPLACE TRIGGER TU_COMPANY_T1
 BEFORE INSERT OR UPDATE
 ON tu_company
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE
	v_EIN_TEST_RESULT NUMBER;

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;
 END IF;

 SELECT nvl(LENGTH(TRIM(TRANSLATE(:NEW.EIN, '0123456789',' '))),0) 
   INTO v_EIN_TEST_RESULT
   FROM dual;
 
 IF v_EIN_TEST_RESULT != 0 then
	RAISE_APPLICATION_ERROR(-20001,'EIN must be nine digits -- '||:NEW.EIN);
 END IF;

END;
/

CREATE OR REPLACE TRIGGER TU_PERMIT_PERIOD_T1
 BEFORE INSERT OR UPDATE
 ON tu_permit_period
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE
	v_SOURCETYPEval CHAR(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

 IF :NEW.SOURCE_CD IS NOT NULL THEN
 SELECT 'Y'
    into v_SOURCETYPEval 
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'SOURCE_TYPE'
     and b.tu_ref_code = :new.source_cd;
 ELSE v_SOURCETYPEval := 'X';
 END IF;
 IF v_SOURCETYPEval !='Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Source Code  '||:new.source_cd);
END IF;

END;
/

CREATE OR REPLACE TRIGGER TU_REF_TYPE_T1
 BEFORE INSERT OR UPDATE
 ON tu_ref_type
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

END;
/

CREATE OR REPLACE TRIGGER TU_REF_VALUE_T1
 BEFORE INSERT OR UPDATE
 ON tu_ref_value
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

END;
/

CREATE OR REPLACE TRIGGER TU_ISO_CNTRY_T1
 BEFORE INSERT OR UPDATE
 ON tu_iso_cntry
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

END;
/

CREATE OR REPLACE TRIGGER TU_STATE_T1
 BEFORE INSERT OR UPDATE
 ON tu_state
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

END;
/

CREATE OR REPLACE TRIGGER TU_SUBMITTED_FORM_T1
 BEFORE INSERT OR UPDATE
 ON tu_submitted_form
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE v_checkrefval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

 SELECT 'Y'
    into v_checkrefval
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'FORM_TYPE'
     and b.tu_ref_code = :new.form_type_cd;
 EXCEPTION when NO_DATA_FOUND
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Form Type Code  '||:new.form_type_cd);
  WHEN OTHERS then NULL;

END;
/

CREATE OR REPLACE TRIGGER TU_TOBACCO_CLASS_T1
 BEFORE INSERT OR UPDATE
 ON tu_tobacco_class
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE v_CLASSTYPEval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

 SELECT 'Y'
    into v_CLASSTYPEval 
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'CLASS_TYPE'
     and b.tu_ref_code = :new.class_type_cd;
 EXCEPTION when NO_DATA_FOUND
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Tobacco Class Code  '||:new.class_type_cd);
  WHEN OTHERS then NULL;

END;
/


CREATE OR REPLACE TRIGGER TU_DOCUMENT_TYPE_T1
 BEFORE INSERT OR UPDATE
 ON tu_document_type
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE v_DOCTYPEval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

 SELECT 'Y'
    into v_DOCTYPEval 
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'DOCUMENT_TYPE'
     and b.tu_ref_code = :new.doc_type_cd;
 EXCEPTION when NO_DATA_FOUND
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Document Type Code  '||:new.doc_type_cd);
  WHEN OTHERS then NULL;

END;
/

CREATE OR REPLACE TRIGGER TU_FORM_DETAIL_T1
 BEFORE INSERT OR UPDATE
 ON tu_form_detail
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE v_REMOVALUOMval char(1);
        v_ACTIVITYval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

/* IF :NEW.REMOVAL_UOM IS NOT NULL THEN
 SELECT 'Y'
    into v_REMOVALUOMval 
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'REMOVAL_UOM'
     and b.tu_ref_code = :new.removal_uom;
 ELSE v_REMOVALUOMval := 'X';
 END IF;
 IF v_REMOVALUOMval != 'Y'
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Unit of Measure Code  '||:new.removal_uom);
 END IF;

 IF :NEW.ACTIVITY_CD IS NOT NULL THEN
   BEGIN
     SELECT :NEW.ACTIVITY_CD 
       into v_ACTIVITYval 
       from tu_ref_type a, tu_ref_value b
     WHERE a.tu_ref_type_id = b.tu_ref_type_id
       and a.tu_ref_type_nm = 'ACTIVITY_TYPE'
       and b.tu_ref_code = :new.activity_cd;
   EXCEPTION when no_data_found 
     THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Activity Type Code  '||:new.activity_cd);
   END;
   END IF;
*/

END;
/


CREATE OR REPLACE TRIGGER TU_FORM_COMMENT_T1
 BEFORE INSERT OR UPDATE
 ON tu_form_comment
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

DECLARE v_checkrefval char(1);

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

 SELECT 'Y'
    into v_checkrefval
    from tu_ref_type a, tu_ref_value b
   WHERE a.tu_ref_type_id = b.tu_ref_type_id
     and a.tu_ref_type_nm = 'FORM_TYPE'
     and b.tu_ref_code = :new.form_type_cd;
 EXCEPTION when NO_DATA_FOUND
   THEN RAISE_APPLICATION_ERROR(-20003,'Invalid Form Type Code  '||:new.form_type_cd);
  WHEN OTHERS then NULL;

END;
/


CREATE OR REPLACE TRIGGER TU_REPORT_ADDRESS_T1
 BEFORE INSERT OR UPDATE
 ON tu_report_address
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

END;
/


CREATE OR REPLACE TRIGGER TU_REPORT_CONTACT_T1
 BEFORE INSERT OR UPDATE
 ON tu_report_contact
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER
       INTO :NEW.created_by
       FROM DUAL;
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN

   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

END;
/


CREATE OR REPLACE TRIGGER TU_ASSESSMENT_T1
 BEFORE INSERT OR UPDATE
 ON tu_assessment
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

END;
/


CREATE OR REPLACE TRIGGER TU_MARKET_SHARE_T1
 BEFORE INSERT OR UPDATE
 ON tu_market_share
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW

BEGIN
  IF INSERTING THEN
   /*** Populate created_dt and created_by  ***/
   IF :NEW.created_by IS NULL THEN
     SELECT USER                  
       INTO :NEW.created_by       
       FROM DUAL;                 
   END IF;

   IF :NEW.created_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.created_dt
       FROM DUAL;
   END IF;

 ELSIF UPDATING THEN
   
   /*** Populate modified_dt and modified_by  ***/
   IF :NEW.modified_by IS NULL THEN
     SELECT USER
        INTO :NEW.modified_by
        FROM DUAL;
   END IF;

   IF :NEW.modified_dt IS NULL THEN
     SELECT SYSDATE
       INTO :NEW.modified_dt
       FROM DUAL;
   END IF;

 END IF;

END;
/
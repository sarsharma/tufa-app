/* TUFA database install script                       */
spool tufa_install.log

/* DROP objects                       */
@tufa_sprint5_drop_objects
commit;

/* Create objects                       */
@tufa_sprint1_ref_ddl-v2
commit;
@tufa_sprint5-1_ddl
commit;

/* Create triggers                      */
@tu_triggers-s5-1
commit;

/* Insert reference types and values     */
@insert_reftypes-Sprint5-1
commit;
@insert_refvalues-Sprint5_1
commit;
@populate_tobacco_class

/* Populate Cntry and State */
@populate_tu_iso_cntry
@populate_tu_state

/* Market Share view */
@qtr_market_share_vw-v2

/* Stored Procedures */
@open_reporting_period.prc
@calc_qtr_market_share.prc

/* Assessment addresses and contacts */
@assessment_address
@assessment_contact

exit;

PROMPT
PROMPT===========================================================
PROMPT tufa_sprint1_ref_ddl-v2.sql - BEGIN
PROMPT===========================================================

/*-------------------------------------------------------------*/
/*
/*Purpose: Create reference tables, constraints and indexes for TBD schema
/*
/*Created: 09/18/1016 by Tony Ireland
/*
Changed TU_REF_VALUE.TU_REF_CODE to CHAR(4) to be consistent with the other tables
Changed TU_ISO_CNTRY.ISO_CNTRY_CD to CHAR(2) and in TU_STATE
Changed TU_STATE.STATE_CD to CHAR(2)
*/
/*-------------------------------------------------------------*/

--

drop table tu_ref_type cascade constraints purge;
drop table tu_ref_value cascade constraints purge;
drop table tu_iso_cntry cascade constraints purge;
drop table tu_state cascade constraints purge;


CREATE TABLE tu_ref_type
(
    tu_ref_type_id	 number generated always as identity NOT NULL ,
    tu_ref_type_nm       VARCHAR2(80) NOT NULL ,
    tu_ref_type_desc     VARCHAR2(2000) NULL,
    created_by           VARCHAR2(50) NULL ,
    created_dt           DATE NULL ,
    modified_by          VARCHAR2(50) NULL ,
    modified_dt          DATE NULL 
)
;


COMMENT ON TABLE tu_ref_type IS 'A meta-entity containing the values of common reference ''TYPE'' entities.  Criteria for inclusion is limited to those ''Types'' with only a few dozen values.  Larger Types that require searches for values are excluded.';

ALTER TABLE tu_ref_type
    ADD CONSTRAINT  tu_ref_type_pk PRIMARY KEY (tu_ref_type_id)
;

CREATE UNIQUE INDEX ref_type_uk ON tu_ref_type
(tu_ref_type_nm   ASC);


CREATE TABLE tu_ref_value
(
    tu_ref_type_value_id	   	NUMBER generated always as identity NOT NULL ,
    tu_ref_type_id	    		NUMBER NOT NULL,
    tu_ref_value         		VARCHAR2(255) NOT NULL ,
    tu_ref_value_abbr    		VARCHAR2(10) NULL ,
    tu_ref_code          		CHAR(4) NULL ,
    created_by           		VARCHAR2 (40) NULL ,
    created_dt           		DATE NULL ,
    modified_by          		VARCHAR2 (40) NULL ,
    modified_dt          		DATE NULL 
)
;

CREATE UNIQUE INDEX ref_value_pk ON tu_ref_value
(tu_ref_type_value_id  ASC);

ALTER TABLE tu_ref_value
    ADD CONSTRAINT  tu_ref_value_pk PRIMARY KEY (tu_ref_type_value_id)
;

CREATE UNIQUE INDEX ref_value_uk ON tu_ref_value
(tu_ref_type_id  ASC, tu_ref_value  ASC);

--

CREATE TABLE tu_iso_cntry
(
	iso_cntry_cd         CHAR(2) NOT NULL ,
	iso_country_nm       VARCHAR2(45) NOT NULL ,
	cntry_dial_cd	     VARCHAR2(5),
	created_by           VARCHAR2(50) NULL ,
	created_dt           DATE NULL ,
	modified_by          VARCHAR2(50) NULL ,
	modified_dt          DATE NULL
)
;

COMMENT ON TABLE tu_iso_cntry IS 'A political state or nation or its territory. ISO 3166 standard list of countries and abbreviations.';
COMMENT ON COLUMN tu_iso_cntry.iso_cntry_cd IS 'The two-digit code for a country as defined by the ISO standard.';
COMMENT ON COLUMN tu_iso_cntry.iso_country_nm IS 'The official name of the country per the ISO standard (English)';

ALTER TABLE tu_iso_cntry
    ADD CONSTRAINT  iso_cntry_pk PRIMARY KEY (iso_cntry_cd);

CREATE TABLE tu_state
(
	state_cd             CHAR(2) NOT NULL ,
	state_nm             VARCHAR2(45) NOT NULL ,
	iso_cntry_cd         CHAR(2) NOT NULL ,
	fips_state_cd	     NUMBER (2) NOT NULL ,
	created_by           VARCHAR2(50) NULL ,
	created_dt           DATE NULL ,
	modified_by          VARCHAR2(50) NULL ,
	modified_dt          DATE NULL
)
;

ALTER TABLE tu_state
    ADD CONSTRAINT  state_pk PRIMARY KEY (state_cd);

PROMPT
PROMPT===========================================================
PROMPT tufa_sprint1_ref_ddl.sql - END
PROMPT==========================================================


PROMPT
PROMPT===========================================================
PROMPT Drop tables and constraints
PROMPT===========================================================
PROMPT

/*-------------------------------------------------------------*/

DROP TABLE tu_address CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_contact CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_period_status CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_document CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_form_detail CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_tobacco_class CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_submitted_form CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_permit_period CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_rpt_period CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_permit CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_company CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_document_type CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_form_comment CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_report_address CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_report_contact CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_assessment CASCADE CONSTRAINTS PURGE;

DROP TABLE tu_market_share CASCADE CONSTRAINTS;


--------------------------------------------------------
--  DDL for View ADDRESS_VALIDATION_REPORT_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ADDRESS_VALIDATION_REPORT_VW" ("Company Name", "Permit Number", "EIN", "Fiscal Year", "Permit Status", "QUARTER", "Latest Month", "LINE#", "Primary Addresses", "Secondary Addresses", "Contact Names", "Contact Phone Numbers", "Contact Extensions", "Contact Email", "Contact Fax", "Company Comments", "Cigar Only") AS 
  SELECT Company_Name                      AS "Company Name",
           Permit_Number                     AS "Permit Number",
           EIN                               AS "EIN",
           fiscal_yr                         AS "Fiscal Year",
           PERMIT_STATUS_TYPE_CD             AS "Permit Status",
           Quarter,
           TO_CHAR (latest_month, 'MON')     AS "Latest Month",
           line#,
           --address_type_cd               AS "Address Type",
           --Addresses                     AS "Addresses",
           "Primary Addresses"               AS "Primary Addresses",
           "Secondary Addresses"             AS "Secondary Addresses",
           AGGNAMES                          AS "Contact Names",
           AGGPHONES                         AS "Contact Phone Numbers",
           AGGEXTS                           AS "Contact Extensions",
           AGGEMAILS                         AS "Contact Email",
           AGGFAX                            AS "Contact Fax",
           "COMPANY_COMMENTS"                AS "Company Comments",
           "Cigar Only"
      FROM (SELECT Company_Name,
                   Permit_Number,
                   EIN,
                   PERMIT_STATUS_TYPE_CD,
                   fiscal_yr,
                   Quarter,
                   Latest_Month,
                   ROW_NUMBER ()
                       OVER (
                           PARTITION BY Company_Name, Permit_Number --, fiscal_yr, quarter
                           ORDER BY
                               fiscal_yr DESC,
                               Quarter DESC,
                               TO_DATE (TO_CHAR (latest_month, 'MON'), 'MON') DESC)
                       --to_char(latest_month,'MM') DESC)
                       line#,
                   --      address_type_cd,
                   --  Addresses,
                   "Primary Addresses",
                   "Secondary Addresses",
                   AGGNAMES,
                   AGGPHONES,
                   AGGEMAILS,
                   AGGEXTS,
                   AGGFAX,
                   COMPANY_COMMENTS,
                   "Cigar Only"
              FROM (  SELECT /*+ USE_HASH(CN,PS,PP) ORDERED */
                             DISTINCT
                             legal_nm
                                 AS Company_Name,
                                SUBSTR (PE.PERMIT_NUM, 1, 2)
                             || '-'
                             || SUBSTR (pe.permit_num, 3, 2)
                             || '-'
                             || SUBSTR (pe.permit_num,
                                        5,
                                        LENGTH (pe.permit_num) - 4)
                                 AS Permit_Number,
                             SUBSTR (EIN, 1, 2) || '-' || SUBSTR (EIN, 3)
                                 AS EIN,
                             PE.PERMIT_STATUS_TYPE_CD,
                             rp.fiscal_yr,
                             rp.quarter
                                 AS Quarter,
                             TO_DATE (RP.MONTH || RP.YEAR, 'MONYYYY')
                                 AS Latest_Month,
                             --ad.address_type_cd,
                             DECODE (
                                 (NVL (
                                      (SELECT    DECODE (AD.ATTENTION,
                                                         NULL, '',
                                                         AD.ATTENTION || ', ')
                                              || DECODE (
                                                     AD.STREET_ADDRESS,
                                                     NULL, '',
                                                     AD.STREET_ADDRESS || ', ')
                                              || DECODE (
                                                     AD.SUITE,
                                                     NULL, '',
                                                       -- 'SUITE '||
                                                     AD.SUITE
                                                     || ', ')
                                              || DECODE (AD.CITY,
                                                         NULL, '',
                                                         AD.CITY || ', ')
                                              || DECODE (AD.STATE,
                                                         NULL, '',
                                                         AD.STATE || ', ')
                                              || DECODE (AD.POSTAL_CD,
                                                         NULL, '',
                                                         AD.POSTAL_CD || ', ')
                                              || DECODE (AD.country_cd,
                                                         NULL, '',
                                                         AD.country_cd)
                                         FROM tu_address ad
                                        WHERE     ad.company_id = co.company_id
                                              AND ad.address_type_cd = 'PRIM'),
                                      '')),
                                 'US', '',
                                 (NVL (
                                      (SELECT    DECODE (AD.ATTENTION,
                                                         NULL, '',
                                                         AD.ATTENTION || ', ')
                                              || DECODE (
                                                     AD.STREET_ADDRESS,
                                                     NULL, '',
                                                     AD.STREET_ADDRESS || ', ')
                                              || DECODE (
                                                     AD.SUITE,
                                                     NULL, '',
                                                        --'SUITE '||
                                                     AD.SUITE
                                                     || ', ')
                                              || DECODE (AD.CITY,
                                                         NULL, '',
                                                         AD.CITY || ', ')
                                              || DECODE (AD.STATE,
                                                         NULL, '',
                                                         AD.STATE || ', ')
                                              || DECODE (AD.POSTAL_CD,
                                                         NULL, '',
                                                         AD.POSTAL_CD || ', ')
                                              || DECODE (AD.country_cd,
                                                         NULL, '',
                                                         AD.country_cd)
                                         FROM tu_address ad
                                        WHERE     ad.company_id = co.company_id
                                              AND ad.address_type_cd = 'PRIM'),
                                      '')))
                                 "Primary Addresses",
                             DECODE (
                                 (NVL (
                                      (SELECT    DECODE (AD.ATTENTION,
                                                         NULL, '',
                                                         AD.ATTENTION || ', ')
                                              || DECODE (
                                                     AD.STREET_ADDRESS,
                                                     NULL, '',
                                                     AD.STREET_ADDRESS || ', ')
                                              || DECODE (
                                                     AD.SUITE,
                                                     NULL, '',
                                                        --'SUITE '||
                                                     AD.SUITE
                                                     || ', ')
                                              || DECODE (AD.CITY,
                                                         NULL, '',
                                                         AD.CITY || ', ')
                                              || DECODE (AD.STATE,
                                                         NULL, '',
                                                         AD.STATE || ', ')
                                              || DECODE (AD.POSTAL_CD,
                                                         NULL, '',
                                                         AD.POSTAL_CD || ', ')
                                              || DECODE (AD.country_cd,
                                                         NULL, '',
                                                         AD.country_cd)
                                         FROM tu_address ad
                                        WHERE     ad.company_id = co.company_id
                                              AND ad.address_type_cd = 'ALTN'),
                                      '')),
                                 'US', '',
                                 (NVL (
                                      (SELECT    DECODE (AD.ATTENTION,
                                                         NULL, '',
                                                         AD.ATTENTION || ', ')
                                              || DECODE (
                                                     AD.STREET_ADDRESS,
                                                     NULL, '',
                                                     AD.STREET_ADDRESS || ', ')
                                              || DECODE (
                                                     AD.SUITE,
                                                     NULL, '',
                                                        --'SUITE ' ||
                                                        AD.SUITE
                                                     || ', ')
                                              || DECODE (AD.CITY,
                                                         NULL, '',
                                                         AD.CITY || ', ')
                                              || DECODE (AD.STATE,
                                                         NULL, '',
                                                         AD.STATE || ', ')
                                              || DECODE (AD.POSTAL_CD,
                                                         NULL, '',
                                                         AD.POSTAL_CD || ', ')
                                              || DECODE (AD.country_cd,
                                                         NULL, '',
                                                         AD.country_cd)
                                         FROM tu_address ad
                                        WHERE     ad.company_id = co.company_id
                                              AND ad.address_type_cd = 'ALTN'),
                                      '')))
                                 "Secondary Addresses",
                             /*
                             CASE
                                 WHEN (ad.address_type_cd = 'PRIM')
                                 THEN
                                        CASE
                                            WHEN (AD.ATTENTION) IS NULL THEN ''
                                            ELSE (AD.ATTENTION) || ','
                                        END
                                     || CASE
                                            WHEN (AD.STREET_ADDRESS) IS NULL
                                            THEN
                                                'NULL' || ','
                                            ELSE
                                                (AD.STREET_ADDRESS) || ','
                                        END
                                     || CASE
                                            WHEN (AD.SUITE) IS NULL THEN ''
                                            ELSE 'SUITE ' || (AD.SUITE) || ','
                                        END
                                     || CASE
                                            WHEN (AD.CITY) IS NULL
                                            THEN
                                                'NULL' || ','
                                            ELSE
                                                (AD.CITY) || ','
                                        END
                                     || CASE
                                            WHEN (AD.STATE) IS NULL
                                            THEN
                                                'NULL' || ','
                                            ELSE
                                                (AD.STATE) || ','
                                        END
                                     || CASE
                                            WHEN (ad.postal_cd) IS NULL
                                            THEN
                                                'NULL' || ','
                                            ELSE
                                                (ad.postal_cd) || ','
                                        END
                                     || CASE
                                            WHEN (ad.country_cd) IS NULL
                                            THEN
                                                'NULL' || ','
                                            ELSE
                                                (ad.country_cd)
                                        END
                                 ELSE
                                     'N/A'
                             END
                                 AS "Primary Addresses",
                             CASE
                                 WHEN (ad.address_type_cd = 'ALTN')
                                 THEN
                                        CASE
                                            WHEN (AD.ATTENTION) IS NULL THEN ''
                                            ELSE (AD.ATTENTION) || ','
                                        END
                                     || CASE
                                            WHEN (AD.STREET_ADDRESS) IS NULL
                                            THEN
                                                'NULL' || ','
                                            ELSE
                                                (AD.STREET_ADDRESS) || ','
                                        END
                                     || CASE
                                            WHEN (AD.SUITE) IS NULL THEN ''
                                            ELSE 'SUITE ' || (AD.SUITE) || ','
                                        END
                                     || CASE
                                            WHEN (AD.CITY) IS NULL
                                            THEN
                                                'NULL' || ','
                                            ELSE
                                                (AD.CITY) || ','
                                        END
                                     || CASE
                                            WHEN (AD.STATE) IS NULL
                                            THEN
                                                'NULL' || ','
                                            ELSE
                                                (AD.STATE) || ','
                                        END
                                     || CASE
                                            WHEN (ad.postal_cd) IS NULL
                                            THEN
                                                'NULL' || ','
                                            ELSE
                                                (ad.postal_cd) || ','
                                        END
                                     || CASE
                                            WHEN (ad.country_cd) IS NULL
                                            THEN
                                                'NULL' || ','
                                            ELSE
                                                (ad.country_cd)
                                        END
                                 ELSE
                                     'N/A'
                             END
                                 AS "Secondary Addresses",*/
                             cn.AGGNAMES,
                             cn.AGGPHONES,
                             cn.AGGEMAILS,
                             cn.AGGEXTS,
                             cn.AGGFAX,
                             co.COMPANY_COMMENTS,
                             CASE
                                 WHEN cov.cigar_only = 'CGR_' THEN 'Y'
                                 ELSE 'N'
                             END
                                 AS "Cigar Only"
                        FROM tu_company      co,
                             --tu_address      ad,
                             tu_permit       pe,
                             tu_rpt_period   rp,
                             tu_permit_period pp,
                             tu_period_status ps,
                             cigar_only_vw   cov,
                             (SELECT DISTINCT
                                     company_id,
                                     --  LISTAGG (first_nm || ' ' || last_nm,
                                     --   ' ; ')
                                     LISTAGG (
                                         DECODE (first_nm || last_nm,
                                                 NULL, '',
                                                 first_nm || ' ' || last_nm),
                                         ' ; ')
                                     WITHIN GROUP (order by primary_Contact_Sequence ASC, created_Dt ASC, contact_Id ASC )
                                     OVER (PARTITION BY COMPANY_ID)
                                         AS AGGNAMES,
                                     /*LISTAGG (
                                           (CASE
                                               WHEN (first_nm) IS NULL
                                               THEN
                                                   ''
                                               ELSE
                                                   (first_nm)
                                           END
                                        || ' '
                                        || CASE
                                               WHEN (last_nm) IS NULL
                                               THEN
                                                   ''
                                               ELSE
                                                   (last_nm)
                                           END),';'
                                        )
                                    WITHIN GROUP (ORDER BY last_nm, first_nm)
                                    OVER (PARTITION BY COMPANY_ID)
                                        AS AGGNAMES,*/
                                     LISTAGG (
                                         DECODE (
                                             (   SUBSTR (
                                                     TRIM (' ' FROM phone_num),
                                                     1,
                                                     3)
                                              || '-'
                                              || SUBSTR (
                                                     TRIM (' ' FROM phone_num),
                                                     4,
                                                     3)
                                              || '-'
                                              || SUBSTR (
                                                     TRIM (' ' FROM phone_num),
                                                     7)),
                                             '--', '',
                                             (   SUBSTR (
                                                     TRIM (' ' FROM phone_num),
                                                     1,
                                                     3)
                                              || '-'
                                              || SUBSTR (
                                                     TRIM (' ' FROM phone_num),
                                                     4,
                                                     3)
                                              || '-'
                                              || SUBSTR (
                                                     TRIM (' ' FROM phone_num),
                                                     7))),
                                         ' ; ')
                                     --LISTAGG (phone_num, ' ; ')
                                     WITHIN GROUP (order by primary_Contact_Sequence ASC, created_Dt ASC, contact_Id ASC )
                                     OVER (PARTITION BY COMPANY_ID)
                                         AS AGGPHONES,
                                     LISTAGG (email_address, ' ; ')
                                     WITHIN GROUP (order by primary_Contact_Sequence ASC, created_Dt ASC, contact_Id ASC )
                                     OVER (PARTITION BY COMPANY_ID)
                                         AS AGGEMAILS,
                                     LISTAGG (phone_ext, ' ; ')
                                     WITHIN GROUP (order by primary_Contact_Sequence ASC, created_Dt ASC, contact_Id ASC )
                                     OVER (PARTITION BY COMPANY_ID)
                                         AS AGGEXTS,
                                     LISTAGG (
                                         DECODE (
                                             (   SUBSTR (
                                                     TRIM (' ' FROM fax_num),
                                                     1,
                                                     3)
                                              || '-'
                                              || SUBSTR (
                                                     TRIM (' ' FROM fax_num),
                                                     4,
                                                     3)
                                              || '-'
                                              || SUBSTR (
                                                     TRIM (' ' FROM fax_num),
                                                     7)),
                                             '--', '',
                                             (   SUBSTR (
                                                     TRIM (' ' FROM fax_num),
                                                     1,
                                                     3)
                                              || '-'
                                              || SUBSTR (
                                                     TRIM (' ' FROM fax_num),
                                                     4,
                                                     3)
                                              || '-'
                                              || SUBSTR (
                                                     TRIM (' ' FROM fax_num),
                                                     7))),
                                         ' ; ')
                                     WITHIN GROUP (order by primary_Contact_Sequence ASC, created_Dt ASC, contact_Id ASC )
                                     OVER (PARTITION BY COMPANY_ID)
                                         AS AGGFAX
                                FROM tu_contact) cn
                       WHERE     co.company_id = pe.company_id
                             --- AND co.company_id = ad.company_id(+)
                             AND co.company_id = cn.company_id(+)
                             AND pe.permit_id = pp.permit_id
                             AND rp.PERIOD_ID = pp.PERIOD_ID
                             AND ps.PERMIT_ID = pp.PERMIT_ID
                             AND ps.period_id = pp.PERIOD_ID
                             AND ps.PERMIT_ID = cov.PERMIT_ID(+)
                             AND ps.period_id = cov.PERIOD_ID(+)
                             --                             AND ad.address_type_cd = 'PRIM'
                             AND ps.period_status_type_cd != 'NSTD'
                    --    and rp.FISCAL_YR = 2018
                    --  and rp.quarter = 3
                    ORDER BY 1, 2, 3))
     WHERE line# = 1
;

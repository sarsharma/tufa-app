--------------------------------------------------------
--  DDL for View ALL_INGESTED_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ALL_INGESTED_VW" ("COMPANY_NM", "COMPANY_EIN", "ORIGINAL_COMPANY_NM", "ORIGINAL_EIN", "TAXES_PAID", "FISCAL_YEAR", "FISCAL_QTR", "TOBACCO_CLASS_NM", "TOBACCO_CLASS_ID", "COMPANY_TYPE", "INCLUDE_FLAG") AS 
  ( SELECT
     company_nm,
     company_ein,
     NULL AS original_company_nm,
     NULL AS original_ein,
     SUM(nvl(estimated_tax,0) ) AS taxes_paid,
     fiscal_year,
     fiscal_qtr,
     tobacco_class_nm,
     tobacco_class_id,
     'IMPT' AS company_type,
     nvl(include_flag,'Y') AS include_flag
 FROM
     (
         SELECT
--Need to some how get the Consignee NM if associtaion type is CONS and the entry as been associated to a parent
             CASE
                 WHEN c.legal_nm IS NOT NULL THEN c.legal_nm
                 ELSE cbpdetail.importer_nm
             END AS company_nm,
             CASE
                 WHEN c.ein IS NOT NULL THEN c.ein
                 ELSE cbpdetail.importer_ein
             END AS company_ein,
             cbpentry.fiscal_year,
             DECODE(ttc.tobacco_class_nm,'Cigars','1-4',cbpentry.fiscal_qtr) AS fiscal_qtr,
             ttc.tobacco_class_nm,
             ttc.tobacco_class_id,
             cbpentry.estimated_tax,
             cbpdetail.include_flag   AS include_flag
         FROM
             tu_cbp_importer cbpdetail
             JOIN tu_cbp_entry cbpentry ON cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
             JOIN tu_tobacco_class ttc ON cbpentry.tobacco_class_id = ttc.tobacco_class_id
            --JOIN ALL_DELTA_COMPARISON_STS_IMP adcsi ON (cbpdetail.cbp_importer_id=adcsi.cbp_IMPORTER_ID
              --                                          AND CBPENTRY.cbp_ENTRY_ID=ADCSI.CBP_ENTRY_ID)
             LEFT JOIN tu_company c ON CASE
                 WHEN cbpdetail.importer_ein = c.ein
                      AND cbpdetail.association_type_cd = 'IMPT' THEN 1
                 WHEN cbpdetail.consignee_ein = c.ein
                      AND cbpdetail.association_type_cd = 'CONS' THEN 1
                 ELSE 0
             END = 1
         WHERE
             ( cbpentry.excluded_flag IS NULL
               OR cbpentry.excluded_flag <> 'Y' )
             AND ( ttc.tobacco_class_id <> 14
                   OR ttc.tobacco_class_nm <> 'Non-Taxable' )
             AND ( cbpdetail.association_type_cd IS NULL
                   OR cbpdetail.association_type_cd <> 'EXCL' )
             AND ( cbpdetail.cbp_importer_id ) NOT IN (
                 SELECT
                     cbp_importer_id
                 FROM
                     ann_cmpy_assoc_delta_vw
                     WHERE
                     ann_cmpy_assoc_delta_vw.association_type_cd IS NOT NULL
             )
     )
 GROUP BY
     company_nm,
     company_ein,
     fiscal_year,
     fiscal_qtr,
     tobacco_class_nm,
     tobacco_class_id,
     nvl(include_flag,'Y')
 )
 UNION ALL
--  Importer ASSOCIATED_DELTAS
 ( SELECT
     company_nm,
     company_ein,
     original_company_nm,
     original_ein,
     SUM(nvl(estimated_tax,0) ) AS taxes_paid,
     fiscal_year,
     fiscal_qtr,
     tobacco_class_nm,
     tobacco_class_id,
     'IMPT' AS company_type,
     nvl(include_flag,'Y') AS include_flag
 FROM
     (
         SELECT
--Need to some how get the Consignee NM if associtaion type is CONS and the entry as been associated to a parent
             nvl(c2.legal_nm,cbpdetailparent.importer_nm) AS company_nm,
             nvl(c2.ein,cbpdetail.importer_ein) AS company_ein,
             nvl(c.legal_nm,cbpdetail.importer_nm) AS original_company_nm,
             nvl(c.ein,cbpdetail.importer_ein) AS original_ein,
             cbpentry.fiscal_year,
             DECODE(ttc.tobacco_class_nm,'Cigars','1-4',cbpentry.fiscal_qtr) AS fiscal_qtr,
             ttc.tobacco_class_nm,
             ttc.tobacco_class_id,
             cbpentry.estimated_tax,
             cbpdetail.include_flag   AS include_flag
         FROM
             tu_cbp_importer cbpdetail
             JOIN tu_cbp_entry cbpentry ON cbpdetail.cbp_importer_id = cbpentry.original_cbp_importer_id
             JOIN tu_cbp_importer cbpdetailparent ON cbpentry.cbp_importer_id = cbpdetailparent.cbp_importer_id
             JOIN tu_tobacco_class ttc ON cbpentry.tobacco_class_id = ttc.tobacco_class_id
             LEFT JOIN tu_company c ON CASE
                 WHEN cbpdetail.importer_ein = c.ein
                      AND cbpdetail.association_type_cd = 'IMPT' THEN 1
                 WHEN cbpdetail.consignee_ein = c.ein
                      AND cbpdetail.association_type_cd = 'CONS' THEN 1
                 ELSE 0
             END = 1
             LEFT JOIN tu_company c2 ON CASE
                 WHEN cbpdetailparent.importer_ein = c2.ein
                      AND cbpdetailparent.association_type_cd = 'IMPT' THEN 1
                 WHEN cbpdetailparent.consignee_ein = c2.ein
                      AND cbpdetailparent.association_type_cd = 'CONS' THEN 1
                 ELSE 0
             END = 1
         WHERE
             ( cbpentry.excluded_flag IS NULL
               OR cbpentry.excluded_flag <> 'Y' )
             AND ( ttc.tobacco_class_id <> 14
                   OR ttc.tobacco_class_nm <> 'Non-Taxable' )
             AND ( cbpdetail.association_type_cd IS NULL
                   OR cbpdetail.association_type_cd <> 'EXCL' )
             AND ( cbpdetail.cbp_importer_id ) NOT IN (
                 SELECT
                     cbp_importer_id
                 FROM
                     ann_cmpy_assoc_delta_vw
                     WHERE
                     ann_cmpy_assoc_delta_vw.association_type_cd IS NULL
             )
     )
 GROUP BY
     company_nm,
     company_ein,
     original_company_nm,
     original_ein,
     fiscal_year,
     fiscal_qtr,
     tobacco_class_nm,
     tobacco_class_id,
     nvl(include_flag,'Y')
 )
 UNION ALL
 SELECT
     company_nm,
     company_ein,
     NULL AS original_company_nm,
     NULL AS original_ein,
     SUM(taxes_paid) AS taxes_paid,
     fiscal_yr,
     fiscal_qtr,
     tobacco_class_nm,
     tobacco_class_id,
     company_type,
     nvl(include_flag,'Y')
 FROM
     ( (
         SELECT /*+ NO_INDEX(TTC) */
             ttbcmpy.company_nm   AS company_nm,
             ttbcmpy.ein_num      AS company_ein,
             ( nvl(ttbannualtx.ttb_taxes_paid,0) ) AS taxes_paid,
             ttbcmpy.fiscal_yr,
             CASE
                 WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(ttbannualtx.ttb_calendar_qtr)
                 ELSE '1-4'
             END AS fiscal_qtr,
             CASE
                 WHEN ttc.tobacco_class_nm = 'Chew-and-Snuff' THEN 'Chew/Snuff'
                 WHEN ttc.tobacco_class_nm = 'Pipe-RYO'       THEN 'Pipe/Roll-Your-Own'
                 ELSE ttc.tobacco_class_nm
             END AS tobacco_class_nm,
             ttc.tobacco_class_id,
             'MANU' AS company_type,
             nvl(ttbcmpy.include_flag,'Y') AS include_flag
         FROM
             tu_ttb_company ttbcmpy,
             tu_ttb_permit ttbpermit,
             tu_ttb_annual_tax ttbannualtx,
             tu_tobacco_class ttc
         WHERE
             ttbannualtx.tobacco_class_id = ttc.tobacco_class_id
             AND ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id
             AND ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id
     ) )
 GROUP BY
     company_nm,
     company_ein,
     fiscal_yr,
     fiscal_qtr,
     tobacco_class_nm,
     tobacco_class_id,
     company_type,
     nvl(include_flag,'Y')
;

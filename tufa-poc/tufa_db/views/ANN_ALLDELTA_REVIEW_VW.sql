--------------------------------------------------------
--  DDL for View ANN_ALLDELTA_REVIEW_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ANN_ALLDELTA_REVIEW_VW" ("EIN", "ORIGINAL_EIN", "IMPORTER_EIN", "CONSIGNEE_EIN", "CBP_TAXES_PAID", "CBP_QTYREMOVED", "SUMMARIZED_TAX", "TOBACCO_CLASS_NM", "FISCAL_QTR", "FISCAL_YEAR", "ASSIGNED_DT", "ASSOCIATION_TYPE_CD", "TOBACCO_CLASS_ID", "SUMMARY_ENTRY_SUMMARY_NUMBER", "FILE_TYPE") AS 
  SELECT
    a.ein,
    a.original_ein,
    a.importer_ein,
    a.consignee_ein,
    a.cbp_taxes_paid,
    a.cbp_qtyremoved,
    a.summarized_tax,
    a.tobacco_class_nm,
    a.fiscal_qtr,
    a.fiscal_year,
    a.assigned_dt,
    a.association_type_cd,
    a.tobacco_class_id,
    a.summary_entry_summary_number,
    (
        SELECT
            sts.file_type
        FROM
            tu_comparison_all_delta_sts sts
        WHERE
            sts.ein = CASE
                WHEN a.association_type_cd = 'CONS' THEN a.consignee_ein
                ELSE a.importer_ein
            END
            AND sts.fiscal_yr = a.fiscal_year
            AND sts.fiscal_qtr = TO_CHAR(a.fiscal_qtr)
            AND sts.tobacco_class_nm = a.tobacco_class_nm
            AND sts.permit_type='IMPT'
    ) AS file_type
FROM
    (
        SELECT
            CASE
                WHEN cbpdetail.association_type_cd = 'CONS' THEN cbpdetail.consignee_ein
                ELSE cbpdetail.importer_ein
            END AS ein,
            CASE
                WHEN original_cbpdetail.association_type_cd = 'CONS' THEN original_cbpdetail.consignee_ein
                ELSE original_cbpdetail.importer_ein
            END AS original_ein,
            cbpdetail.importer_ein,
            cbpdetail.consignee_ein,
            SUM(nvl(cbpentry.estimated_tax,0) ) AS cbp_taxes_paid,
            SUM(nvl(cbpentry.qty_removed,0) ) AS cbp_qtyremoved,
            nvl(cbpentry.summarized_file_tax,0) AS summarized_tax,
            ttc.tobacco_class_nm,
            CASE
                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                ELSE '1-4'
            END AS fiscal_qtr,
            cbpentry.fiscal_year,
            cbpentry.assigned_dt,
            cbpdetail.association_type_cd,
            ttc.tobacco_class_id   AS tobacco_class_id,
            cbpentry.summary_entry_summary_number
        FROM
            tu_cbp_importer cbpdetail join
            tu_cbp_entry cbpentry on cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id left join
            tu_cbp_importer original_cbpdetail on cbpentry.original_cbp_importer_id = original_cbpdetail.cbp_importer_id,
            tu_tobacco_class ttc
        WHERE
            ( cbpdetail.association_type_cd = 'IMPT'
              OR cbpdetail.association_type_cd = 'CONS'
              OR cbpdetail.consignee_exists_flag = 'N'
              AND cbpdetail.association_type_cd IS NULL )
            AND ( cbpentry.excluded_flag IS NULL
                  OR cbpentry.excluded_flag <> 'Y' )
            AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
            AND ( ttc.tobacco_class_id <> 14
                  OR ttc.tobacco_class_nm <> 'Non-Taxable' )
        GROUP BY
            cbpdetail.importer_ein,
            cbpdetail.consignee_ein,
            original_cbpdetail.consignee_ein,
            original_cbpdetail.importer_ein,
            nvl(cbpentry.summarized_file_tax,0),
            ttc.tobacco_class_nm,
            CASE
                WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                ELSE '1-4'
            END,
            cbpentry.assigned_dt,
            cbpentry.fiscal_year,
            cbpdetail.association_type_cd,
            original_cbpdetail.association_type_cd,
            ttc.tobacco_class_id,
            cbpentry.summary_entry_summary_number
    ) a
;

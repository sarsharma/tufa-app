--------------------------------------------------------
--  DDL for View ANN_CIGAR_IMPORTER_SUMMARY_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ANN_CIGAR_IMPORTER_SUMMARY_VW" ("MONTH_ACT", "TAXES_PAID", "CBP_TAXES_PAID", "SUMMARY_FILE_TAX", "FISCAL_QTR", "FISCAL_YEAR", "COMPANY_ID", "TOBACCO_CLASS_NM", "DIFFERENCE", "INGESTED_DIFFERENCE", "DETAILS_DIFFERENCE") AS 
  SELECT
    CASE
        WHEN a.assigned_dt IS NULL THEN TRIM(initcap(b.month) )
        WHEN b.month IS NULL THEN TRIM(initcap(a.assigned_dt) )
        ELSE TRIM(initcap(month) )
    END month_act,
    b.taxes_paid       AS taxes_paid,
    a.cbp_taxes_paid   AS cbp_taxes_paid,
    a.summary_file_tax,
    CASE
        WHEN a.fiscal_qtr IS NULL THEN b.quarter
        WHEN b.quarter IS NULL THEN a.fiscal_qtr
        ELSE a.fiscal_qtr
    END fiscal_qtr,
    CASE
        WHEN a.fiscal_year IS NULL THEN b.fiscal_yr
        WHEN b.fiscal_yr IS NULL THEN a.fiscal_year
        ELSE fiscal_yr
    END fiscal_year,
    CASE
        WHEN a.company_id IS NULL THEN b.company_id
        WHEN b.company_id IS NULL THEN a.company_id
        ELSE a.company_id
    END company_id,
    CASE
        WHEN a.tobacco_class_nm IS NULL THEN b.tobacco_class_nm
        WHEN b.tobacco_class_nm IS NULL THEN a.tobacco_class_nm
        ELSE b.tobacco_class_nm
    END tobacco_class_nm,
    coalesce( (b.taxes_paid - a.summary_file_tax),b.taxes_paid, (0 - a.summary_file_tax) ) AS difference,
    coalesce( (a.summary_file_tax - a.cbp_taxes_paid),a.summary_file_tax, (0 - a.cbp_taxes_paid) ) AS ingested_difference,
    coalesce( (b.taxes_paid - a.cbp_taxes_paid),b.taxes_paid, (0 - a.cbp_taxes_paid) ) AS details_difference
FROM
    (SELECT
                    legal_nm,
                    ein,
                    cbp_taxes_paid,
                    cbp_qtyremoved,
                    summarized_tax as summary_file_tax,
                    fiscal_qtr,
                    fiscal_year,
                    assigned_dt,
                    tobacco_class_nm,
                    tobacco_class_id,
                    company_id,
                    summary_entry_summary_number
FROM (SELECT * FROM (
                SELECT
                    c1.legal_nm,
                    cbpingested.importer_ein   AS ein,
                    SUM(nvl(cbpingested.cbp_taxes_paid,0) ) AS cbp_taxes_paid,
                    SUM(nvl(cbpingested.cbp_qtyremoved,0) ) AS cbp_qtyremoved,
                    null AS summarized_tax,
                    cbpingested.fiscal_qtr,
                    cbpingested.fiscal_year,
                    assigned_dt,
                    cbpingested.tobacco_class_nm,
                    cbpingested.tobacco_class_id,
                    c1.company_id,
                    cbpingested.summary_entry_summary_number
                FROM
                    tu_annual_trueup a1,
                    (
                        SELECT /*+ INDEX(CBPENTRY TU_CBP_ENTRY_UK) */
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            SUM(nvl(cbpentry.estimated_tax,0) ) AS cbp_taxes_paid,
                            SUM(nvl(cbpentry.qty_removed,0) ) AS cbp_qtyremoved,
                            nvl(cbpentry.summarized_file_tax,0) AS summarized_tax,
                            ttc.tobacco_class_nm,
                            cbpentry.fiscal_qtr,
                            cbpentry.fiscal_year,
                            cbpentry.assigned_dt,
                            cbpdetail.consignee_exists_flag,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id   AS tobacco_class_id,
                            cbpentry.summary_entry_summary_number
                        FROM
                            tu_cbp_importer cbpdetail,
                            tu_cbp_entry cbpentry,
                            tu_tobacco_class ttc
                        WHERE
                            ( cbpentry.excluded_flag IS NULL
                              OR cbpentry.excluded_flag <> 'Y' )
                            AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
                            AND ( ttc.tobacco_class_id <> 14
                                  OR ttc.tobacco_class_nm <> 'Non-Taxable' )
                            AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
                        GROUP BY
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            nvl(cbpentry.summarized_file_tax,0),
                            ttc.tobacco_class_nm,
                            cbpentry.fiscal_qtr,
                            cbpentry.assigned_dt,
                            cbpentry.fiscal_year,
                            cbpdetail.consignee_exists_flag,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id,
                            cbpentry.summary_entry_summary_number
                    ) cbpingested,
                    tu_company c1
                WHERE
                    ( cbpingested.importer_ein = c1.ein
                      AND cbpingested.association_type_cd = 'IMPT'
                      OR cbpingested.consignee_ein = c1.ein
                      AND cbpingested.association_type_cd = 'CONS'
                      OR ( cbpingested.consignee_ein = c1.ein
                           AND cbpingested.association_type_cd IS NULL
                           AND cbpingested.consignee_exists_flag = 'N' ) )
                    AND cbpingested.cbp_taxes_paid != 0
                    AND cbpingested.fiscal_year = a1.fiscal_yr
                GROUP BY
                    c1.legal_nm,
                    cbpingested.fiscal_qtr,
                    nvl(cbpingested.summarized_tax,0),
                    cbpingested.assigned_dt,
                    cbpingested.fiscal_year,
                    cbpingested.tobacco_class_nm,
                    cbpingested.tobacco_class_id,
                    c1.company_id,
                    cbpingested.importer_ein,
                    cbpingested.summary_entry_summary_number
            ) UNION (
            SELECT
                    c1.legal_nm,
                    cbpingested.importer_ein   AS ein,
                    null AS cbp_taxes_paid,
                    null AS cbp_qtyremoved,
                    nvl(cbpingested.summarized_tax,0) AS summarized_tax,
                    cbpingested.fiscal_qtr,
                    cbpingested.fiscal_year,
                    -- Added in the case that the summary file date doesn't match the details file
                    case when reallocated_flag = 'Y' then cbpingested.assigned_dt else to_char(cbpingested.summary_entry_summary_date, 'MON') end AS assigned_dt,
                    cbpingested.tobacco_class_nm,
                    cbpingested.tobacco_class_id,
                    c1.company_id,
                    cbpingested.summary_entry_summary_number
                FROM
                    tu_annual_trueup a1,
                    (
                        SELECT /*+ INDEX(CBPENTRY TU_CBP_ENTRY_UK) */
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            nvl(cbpentry.summarized_file_tax,0) AS summarized_tax,
                            ttc.tobacco_class_nm,
                            cbpentry.fiscal_qtr,
                            cbpentry.fiscal_year,
                            cbpentry.summary_entry_summary_date,
                            cbpentry.assigned_dt,
                            cbpentry.reallocated_flag,
                            cbpdetail.consignee_exists_flag,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id   AS tobacco_class_id,
                            cbpentry.summary_entry_summary_number
                        FROM
                            tu_cbp_importer cbpdetail,
                            tu_cbp_entry cbpentry,
                            tu_tobacco_class ttc
                        WHERE
                            ( cbpentry.excluded_flag IS NULL
                              OR cbpentry.excluded_flag <> 'Y' )
                            AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
                            AND ( ttc.tobacco_class_id <> 14
                                  OR ttc.tobacco_class_nm <> 'Non-Taxable' )
                            AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
                    ) cbpingested,
                    tu_company c1
                WHERE
                    ( cbpingested.importer_ein = c1.ein
                      AND cbpingested.association_type_cd = 'IMPT'
                      OR cbpingested.consignee_ein = c1.ein
                      AND cbpingested.association_type_cd = 'CONS'
                      OR ( cbpingested.consignee_ein = c1.ein
                           AND cbpingested.association_type_cd IS NULL
                           AND cbpingested.consignee_exists_flag = 'N' ) )
                    AND cbpingested.fiscal_year = a1.fiscal_yr
))) a
    FULL OUTER JOIN ( (
        SELECT
            *
        FROM
            (
                SELECT
                    SUM(taxes_paid) AS taxes_paid,
                    tobacco_class_nm,
                    company_id,
                    form_type_cd,
                    fiscal_yr,
                    quarter,
                    month,
                    permit_type_cd,
                    ein
                FROM
                    (
                        SELECT /*+ USE_NL(TC1,FD,SF,PS,A2,RP) ORDERED */
                            SUM(DECODE(pp.zero_flag,'Y',0,fd.taxes_paid) ) AS taxes_paid,
                            tc1.tobacco_class_nm,
                            c2.company_id,
                            nvl(sf.form_type_cd,'3852') AS form_type_cd,
                            rp.fiscal_yr,
                            rp.quarter   AS quarter,
                            rp.month,
                            p.permit_type_cd,
                            c2.ein,
                                          -- C2.COMPANY_ID,
                            pp.zero_flag
                        FROM
                            tu_company c2
                            INNER JOIN tu_permit p ON c2.company_id = p.company_id
                            INNER JOIN tu_permit_period pp ON p.permit_id = pp.permit_id
                            INNER JOIN tu_rpt_period rp ON rp.period_id = pp.period_id
                            INNER JOIN tu_annual_trueup a2 ON a2.fiscal_yr = rp.fiscal_yr
                            INNER JOIN tu_period_status ps ON pp.period_id = ps.period_id
                                                              AND pp.permit_id = ps.permit_id
                            LEFT JOIN tu_submitted_form sf ON sf.permit_id = pp.permit_id
                                                              AND sf.period_id = pp.period_id
                            LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id
                            LEFT JOIN tu_tobacco_class tc1 ON fd.tobacco_class_id = tc1.tobacco_class_id
                        GROUP BY
                            tc1.tobacco_class_nm,
                            nvl(sf.form_type_cd,'3852'),
                            rp.fiscal_yr,
                            rp.quarter,
                            rp.month,
                            p.permit_type_cd,
                            c2.ein,
                            c2.company_id,
                            pp.zero_flag
                    )
                GROUP BY
                    tobacco_class_nm,
                    company_id,
                    form_type_cd,
                    fiscal_yr,
                    quarter,
                    month,
                    permit_type_cd,
                    ein
            )
        WHERE
            form_type_cd = 3852
            AND permit_type_cd = 'IMPT'
    ) ) b ON a.company_id = b.company_id
             AND TO_DATE(a.assigned_dt,'MON') = TO_DATE(b.month,'MON')
             AND a.fiscal_year = b.fiscal_yr
             AND a.tobacco_class_nm = b.tobacco_class_nm
             AND a.fiscal_qtr = b.quarter
--                    AND b.permit_type_cd = 'IMPT'
ORDER BY
    fiscal_qtr,
    TO_DATE(month_act,'MON') ASC
;

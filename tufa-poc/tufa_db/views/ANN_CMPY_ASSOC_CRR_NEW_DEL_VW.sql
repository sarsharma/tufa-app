--------------------------------------------------------
--  DDL for View ANN_CMPY_ASSOC_CRR_NEW_DEL_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ANN_CMPY_ASSOC_CRR_NEW_DEL_VW" ("COMPANY_ID", "TUFA_LEGAL_NM", "ASSOCIATION_TYPE_CD", "CBP_IMPORTER_ID", "IMPORTER_NM", "IMP_IN_TUFA", "CONSG_IN_TUFA", "FISCAL_YR", "QUARTER", "CURRENT_DELTA_CIGARS", "CURRENT_DELTA_CIGS", "CURRENT_DELTA_CHEW", "CURRENT_DELTA_SNUFF", "CURRENT_DELTA_PIPE", "CURRENT_DELTA_RYO", "CURRENT_DELTA_UNKNOWN", "NEW_DELTA_CIGARS", "NEW_DELTA_CIGS", "NEW_DELTA_CHEW", "NEW_DELTA_SNUFF", "NEW_DELTA_PIPE", "NEW_DELTA_RYO", "NEW_DELTA_UNKNOWN", "IMP_MATCH", "CONSG_MATCH", "TAXES_CIGARS", "TAXES_CIGS", "TAXES_CHEW", "TAXES_SNUFF", "TAXES_PIPE", "TAXES_RYO", "TAXES_UNKNOWN", "CONSIGNEE_NM") AS 
  SELECT
       INGESTED.COMPANY_ID,
--       INGESTED.IMPORTER_EIN,
--       CURRENT_DELTA.COMPANY_ID AS TUFA_COMPANY_ID,
       INGESTED.LEGAL_NM AS TUFA_LEGAL_NM,
       INGESTED.ASSOCIATION_TYPE_CD AS ASSOCIATION_TYPE_CD ,
       INGESTED.CBP_IMPORTER_ID,
       CASE WHEN  INGESTED.IMP_IN_TUFA='Y' AND INGESTED.IMP_MATCH='Y'
            THEN INGESTED.LEGAL_NM
            ELSE INGESTED.IMPORTER_NM END AS IMPORTER_NM,
       INGESTED.IMP_IN_TUFA,
       INGESTED.CONSG_IN_TUFA,
       INGESTED.FISCAL_YR AS FISCAL_YR,
       CASE WHEN INGESTED.QUARTER!= CURRENT_DELTA.QUARTER  THEN CURRENT_DELTA.QUARTER
       ELSE to_char(INGESTED.QUARTER) END AS QUARTER,
       --- CURRENT DELTA
      CURRENT_DELTA.CURRENT_DELTA_CIGARS  AS CURRENT_DELTA_CIGARS ,
      CURRENT_DELTA.CURRENT_DELTA_CIGS AS CURRENT_DELTA_CIGS,
      CURRENT_DELTA.CURRENT_DELTA_CHEW  AS CURRENT_DELTA_CHEW,
      CURRENT_DELTA.CURRENT_DELTA_SNUFF  AS CURRENT_DELTA_SNUFF,
      CURRENT_DELTA.CURRENT_DELTA_PIPE AS CURRENT_DELTA_PIPE,
      CURRENT_DELTA.CURRENT_DELTA_RYO  AS CURRENT_DELTA_RYO,
      NULL AS CURRENT_DELTA_UNKNOWN,
        -- NEW DELTA
       CASE WHEN CURRENT_DELTA.CURRENT_DELTA_CIGARS IS NULL AND INGESTED.TAXES_CIGARS IS NULL THEN NULL
       WHEN CURRENT_DELTA.CURRENT_DELTA_CIGARS IS NULL AND INGESTED.TAXES_CIGARS IS NOT NULL THEN (0-INGESTED.TAXES_CIGARS)
       WHEN CURRENT_DELTA.CURRENT_DELTA_CIGARS IS NOT NULL AND INGESTED.TAXES_CIGARS IS NULL THEN CURRENT_DELTA.CURRENT_DELTA_CIGARS
--       WHEN CURRENT_DELTA.CURRENT_DELTA_CIGARS = INGESTED.TAXES_CIGARS THEN NULL
       ELSE CURRENT_DELTA.CURRENT_DELTA_CIGARS - INGESTED.TAXES_CIGARS END  AS NEW_DELTA_CIGARS,
       CASE WHEN CURRENT_DELTA.CURRENT_DELTA_CIGS IS NULL AND INGESTED.TAXES_CIGS IS NULL THEN NULL
       WHEN CURRENT_DELTA.CURRENT_DELTA_CIGS IS NULL AND INGESTED.TAXES_CIGS IS NOT NULL THEN (0-INGESTED.TAXES_CIGS)
       WHEN CURRENT_DELTA.CURRENT_DELTA_CIGS IS NOT NULL AND INGESTED.TAXES_CIGS IS NULL THEN CURRENT_DELTA.CURRENT_DELTA_CIGS
--       WHEN CURRENT_DELTA.CURRENT_DELTA_CIGS = INGESTED.TAXES_CIGS THEN NULL
       ELSE CURRENT_DELTA.CURRENT_DELTA_CIGS -  INGESTED.TAXES_CIGS END  AS NEW_DELTA_CIGS,
       CASE WHEN CURRENT_DELTA.CURRENT_DELTA_CHEW IS NULL AND INGESTED.TAXES_CHEW IS NULL THEN NULL
       WHEN CURRENT_DELTA.CURRENT_DELTA_CHEW IS NULL AND INGESTED.TAXES_CHEW IS NOT NULL THEN (0-INGESTED.TAXES_CHEW)
       WHEN CURRENT_DELTA.CURRENT_DELTA_CHEW IS NOT NULL AND INGESTED.TAXES_CHEW IS NULL THEN CURRENT_DELTA.CURRENT_DELTA_CHEW
--       WHEN CURRENT_DELTA.CURRENT_DELTA_CHEW = INGESTED.TAXES_CHEW THEN NULL
       ELSE CURRENT_DELTA.CURRENT_DELTA_CHEW -  INGESTED.TAXES_CHEW END  AS NEW_DELTA_CHEW,
       CASE WHEN CURRENT_DELTA.CURRENT_DELTA_SNUFF IS NULL AND INGESTED.TAXES_SNUFF IS NULL THEN NULL
       WHEN CURRENT_DELTA.CURRENT_DELTA_SNUFF IS NULL AND INGESTED.TAXES_SNUFF IS NOT NULL THEN (0-INGESTED.TAXES_SNUFF)
       WHEN CURRENT_DELTA.CURRENT_DELTA_SNUFF IS NOT NULL AND INGESTED.TAXES_SNUFF IS NULL THEN CURRENT_DELTA.CURRENT_DELTA_SNUFF
--       WHEN CURRENT_DELTA.CURRENT_DELTA_SNUFF = INGESTED.TAXES_SNUFF THEN NULL
       ELSE CURRENT_DELTA.CURRENT_DELTA_SNUFF -  INGESTED.TAXES_SNUFF END  AS NEW_DELTA_SNUFF,
       CASE WHEN CURRENT_DELTA.CURRENT_DELTA_PIPE IS NULL AND INGESTED.TAXES_PIPE IS NULL THEN NULL
       WHEN CURRENT_DELTA.CURRENT_DELTA_PIPE IS NULL AND INGESTED.TAXES_PIPE IS NOT NULL THEN (0-INGESTED.TAXES_PIPE)
       WHEN CURRENT_DELTA.CURRENT_DELTA_PIPE IS NOT NULL AND INGESTED.TAXES_PIPE IS NULL THEN CURRENT_DELTA.CURRENT_DELTA_PIPE
--       WHEN CURRENT_DELTA.CURRENT_DELTA_PIPE = INGESTED.TAXES_PIPE THEN NULL
       ELSE CURRENT_DELTA.CURRENT_DELTA_PIPE - INGESTED.TAXES_PIPE END  AS NEW_DELTA_PIPE,
       CASE WHEN CURRENT_DELTA.CURRENT_DELTA_RYO IS NULL AND INGESTED.TAXES_RYO IS NULL THEN NULL
       WHEN CURRENT_DELTA.CURRENT_DELTA_RYO IS NULL AND INGESTED.TAXES_RYO IS NOT NULL THEN (0-INGESTED.TAXES_RYO)
       WHEN CURRENT_DELTA.CURRENT_DELTA_RYO IS NOT NULL AND INGESTED.TAXES_RYO IS NULL THEN CURRENT_DELTA.CURRENT_DELTA_RYO
--       WHEN CURRENT_DELTA.CURRENT_DELTA_RYO = INGESTED.TAXES_RYO THEN NULL
       ELSE CURRENT_DELTA.CURRENT_DELTA_RYO -  INGESTED.TAXES_RYO END  AS NEW_DELTA_RYO,
       CASE WHEN INGESTED.QUARTER!= CURRENT_DELTA.QUARTER THEN NULL
       WHEN INGESTED.TAXES_UNKNOWN IS NOT NULL THEN (0-INGESTED.TAXES_UNKNOWN)
       ELSE NULL END AS NEW_DELTA_UNKNOWN,
       CASE WHEN CURRENT_DELTA.COMPANY_ID IS NULL AND INGESTED.IMP_IN_TUFA ='N' THEN  'Y'
       ELSE INGESTED.IMP_MATCH END IMP_MATCH,
       CASE WHEN CURRENT_DELTA.COMPANY_ID IS NULL AND INGESTED.IMP_IN_TUFA ='N' THEN 'N'
       ELSE INGESTED.CONSG_MATCH END CONSG_MATCH,
       (0-INGESTED.TAXES_CIGARS) AS TAXES_CIGARS,
       (0-INGESTED.TAXES_CIGS) AS TAXES_CIGS,
       (0-INGESTED.TAXES_CHEW) AS TAXES_CHEW,
       (0-INGESTED.TAXES_SNUFF) AS TAXES_SNUFF,
       (0-INGESTED.TAXES_PIPE) AS TAXES_PIPE,
       (0-INGESTED.TAXES_RYO) AS TAXES_RYO,
       (0-INGESTED.TAXES_UNKNOWN) AS TAXES_UNKNOWN,
        CASE WHEN  INGESTED.CONSG_IN_TUFA='Y' AND INGESTED.CONSG_MATCH='Y'
            THEN INGESTED.LEGAL_NM
            ELSE INGESTED.CONSIGNEE_NM END AS CONSIGNEE_NM
      FROM
      ( select * from ANN_CMPY_ASSOC_DELTA_VW ) INGESTED
            LEFT JOIN
      ( select * from ANN_CMPY_ASSOC_CURR_DELTA_VW ) CURRENT_DELTA
ON
  ( INGESTED.FISCAL_YR = CURRENT_DELTA.FISCAL_YR   AND INGESTED.QUARTER = CURRENT_DELTA.QUARTER
    AND ( CASE WHEN CURRENT_DELTA.COMPANY_ID IS NOT NULL AND INGESTED.COMPANY_ID = CURRENT_DELTA.COMPANY_ID THEN 1
              WHEN CURRENT_DELTA.COMPANY_ID IS NULL AND INGESTED.IMPORTER_EIN = CURRENT_DELTA.IMPORTER_EIN THEN 1
               ELSE 0 END ) =1
  )
order by ingested.fiscal_yr,ingested.cbp_importer_id,ingested.quarter,IMP_MATCH
;

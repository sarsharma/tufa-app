--------------------------------------------------------
--  DDL for View ANN_CMPY_ASSOC_CURR_DELTA_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ANN_CMPY_ASSOC_CURR_DELTA_VW" ("QUARTER", "COMPANY_ID", "FISCAL_YR", "IMPORTER_EIN", "CURRENT_DELTA_CIGARS", "CURRENT_DELTA_PIPE", "CURRENT_DELTA_SNUFF", "CURRENT_DELTA_CHEW", "CURRENT_DELTA_CIGS", "CURRENT_DELTA_RYO") AS 
  SELECT "QUARTER",
             "COMPANY_ID",
             "FISCAL_YR",
             "IMPORTER_EIN",
             "CURRENT_DELTA_CIGARS",
             "CURRENT_DELTA_PIPE",
             "CURRENT_DELTA_SNUFF",
             "CURRENT_DELTA_CHEW",
             "CURRENT_DELTA_CIGS",
             "CURRENT_DELTA_RYO"
        FROM (SELECT CLASSNAME,
                     QUARTER,
                     CURRENT_DELTA,
                     COMPANY_ID,
                     FISCAL_YR,
                     DECODE (COMPANY_ID, NULL, EIN, NULL) AS IMPORTER_EIN
                FROM (SELECT CASE
                                 WHEN     INGESTED.TOBACCO_CLASS_NM IS NULL
                                      AND REPORTS.TOBACCO_CLASS_NM IS NOT NULL
                                 THEN
                                     REPORTS.TOBACCO_CLASS_NM
                                 WHEN     INGESTED.TOBACCO_CLASS_NM IS NOT NULL
                                      AND REPORTS.TOBACCO_CLASS_NM IS NULL
                                 THEN
                                     INGESTED.TOBACCO_CLASS_NM
                                 ELSE
                                     INGESTED.TOBACCO_CLASS_NM
                             END
                                 AS CLASSNAME,
                             CASE
                                 WHEN     INGESTED.FISCAL_QTR IS NULL
                                      AND REPORTS.QUARTER IS NOT NULL
                                 THEN
                                     REPORTS.QUARTER
                                 WHEN     INGESTED.FISCAL_QTR IS NOT NULL
                                      AND REPORTS.QUARTER IS NULL
                                 THEN
                                     INGESTED.FISCAL_QTR
                                 ELSE
                                     INGESTED.FISCAL_QTR
                             END
                                 AS QUARTER,
                             NVL (TAXES_PAID, 0) - NVL (CBP_TAXES_PAID, 0)
                                 AS CURRENT_DELTA,
                             CASE
                                 WHEN     INGESTED.COMPANY_ID IS NULL
                                      AND REPORTS.COMPANY_ID IS NOT NULL
                                 THEN
                                     REPORTS.COMPANY_ID
                                 WHEN     INGESTED.COMPANY_ID IS NOT NULL
                                      AND REPORTS.COMPANY_ID IS NULL
                                 THEN
                                     INGESTED.COMPANY_ID
                                 ELSE
                                     INGESTED.COMPANY_ID
                             END
                                 AS COMPANY_ID,
                             CASE
                                 WHEN     INGESTED.FISCAL_YEAR IS NULL
                                      AND REPORTS.FISCAL_YR IS NOT NULL
                                 THEN
                                     REPORTS.FISCAL_YR
                                 WHEN     INGESTED.FISCAL_YEAR IS NOT NULL
                                      AND REPORTS.FISCAL_YR IS NULL
                                 THEN
                                     INGESTED.FISCAL_YEAR
                                 ELSE
                                     INGESTED.FISCAL_YEAR
                             END
                                 AS FISCAL_YR,
                             INGESTED.EIN
                        FROM ( ----------------------------------- INGESTED ------------------------------------------
                              SELECT   C1.LEGAL_NM,
                                       SUM (NVL (CBPINGESTED.CBP_TAXES_PAID, 0))
                                           AS CBP_TAXES_PAID,
                                       CBPINGESTED.FISCAL_QTR AS FISCAL_QTR,
                                       CBPINGESTED.FISCAL_YEAR,
                                       CBPINGESTED.TOBACCO_CLASS_NM,
                                       CBPINGESTED.TOBACCO_CLASS_ID,
                                       C1.COMPANY_ID,
                                       --  CBPINGESTED.IMPORTER_EIN
                                       DECODE (CBPINGESTED.ASSOCIATION_TYPE_CD,
                                               'IMPT', CBPINGESTED.IMPORTER_EIN,
                                               'CONS', CBPINGESTED.CONSIGNEE_EIN,
                                               CBPINGESTED.IMPORTER_EIN)
                                           AS EIN
                                  FROM TU_ANNUAL_TRUEUP A1
                                       INNER JOIN
                                       (  SELECT CBPDETAIL.IMPORTER_EIN,
                                                 CBPDETAIL.CONSIGNEE_EIN,
                                                 SUM (
                                                     NVL (CBPENTRY.ESTIMATED_TAX,
                                                          0))
                                                     AS CBP_TAXES_PAID,
                                                 TTC.TOBACCO_CLASS_NM,
                                                 TO_CHAR (CBPENTRY.FISCAL_QTR)
                                                     AS FISCAL_QTR,
                                                 CBPENTRY.FISCAL_YEAR,
                                                 CBPDETAIL.ASSOCIATION_TYPE_CD,
                                                 TTC.TOBACCO_CLASS_ID
                                                     AS TOBACCO_CLASS_ID,
                                                 CBPDETAIL.CONSIGNEE_EXISTS_FLAG,
                                                 CBPDETAIL.DEFAULT_FLAG
                                            FROM TU_CBP_IMPORTER CBPDETAIL,
                                                 TU_CBP_ENTRY CBPENTRY,
                                                 TU_TOBACCO_CLASS TTC
                                           WHERE     (   CBPENTRY.EXCLUDED_FLAG
                                                             IS NULL
                                                      OR CBPENTRY.EXCLUDED_FLAG <>
                                                             'Y')
                                                 AND CBPENTRY.TOBACCO_CLASS_ID =
                                                         TTC.TOBACCO_CLASS_ID
                                                 AND (   TTC.TOBACCO_CLASS_ID <> 14
                                                      OR TTC.TOBACCO_CLASS_NM <>
                                                             'Non-Taxable')
                                                 AND CBPDETAIL.CBP_IMPORTER_ID =
                                                         CBPENTRY.CBP_IMPORTER_ID
                                        GROUP BY CBPDETAIL.IMPORTER_EIN,
                                                 CBPDETAIL.CONSIGNEE_EIN,
                                                 TTC.TOBACCO_CLASS_NM,
                                                 CBPDETAIL.CONSIGNEE_EXISTS_FLAG,
                                                 TO_CHAR (CBPENTRY.FISCAL_QTR),
                                                 CBPENTRY.FISCAL_YEAR,
                                                 CBPDETAIL.ASSOCIATION_TYPE_CD,
                                                 TTC.TOBACCO_CLASS_ID,
                                                 CBPDETAIL.DEFAULT_FLAG)
                                       CBPINGESTED
                                           ON CBPINGESTED.FISCAL_YEAR =
                                                  A1.FISCAL_YR
                                       LEFT JOIN TU_COMPANY C1
                                           ON    (    CBPINGESTED.IMPORTER_EIN =
                                                          C1.EIN
                                                  AND CBPINGESTED.ASSOCIATION_TYPE_CD =
                                                          'IMPT')
                                              OR (    CBPINGESTED.CONSIGNEE_EIN =
                                                          C1.EIN
                                                  AND CBPINGESTED.ASSOCIATION_TYPE_CD =
                                                          'CONS')
                                 WHERE    (    CBPINGESTED.ASSOCIATION_TYPE_CD
                                                   IS NOT NULL
                                           AND CBPINGESTED.ASSOCIATION_TYPE_CD NOT IN
                                                   ('EXCL', 'UNKN'))
                                       OR     (    CBPINGESTED.IMPORTER_EIN NOT IN
                                                       (SELECT ein
                                                          FROM TU_COMPANY)
                                               AND CBPINGESTED.ASSOCIATION_TYPE_CD
                                                       IS NULL
                                               AND CBPINGESTED.DEFAULT_FLAG = 'N'
                                               AND CBPINGESTED.CONSIGNEE_EXISTS_FLAG =
                                                       'N')
                                          AND CBPINGESTED.CBP_TAXES_PAID != 0
                              GROUP BY C1.LEGAL_NM,
                                       CBPINGESTED.FISCAL_QTR,
                                       CBPINGESTED.FISCAL_YEAR,
                                       CBPINGESTED.TOBACCO_CLASS_NM,
                                       CBPINGESTED.TOBACCO_CLASS_ID,
                                       C1.COMPANY_ID,
                                       DECODE (
                                           CBPINGESTED.ASSOCIATION_TYPE_CD,
                                           'IMPT', CBPINGESTED.IMPORTER_EIN,
                                           'CONS', CBPINGESTED.CONSIGNEE_EIN,
                                           CBPINGESTED.IMPORTER_EIN)) INGESTED
                             -------------------------------------------- INGESTED END -------------------------------------------------------------
                             FULL OUTER JOIN
                             (  SELECT /*+ USE_NL(TC1,FD,SF,PS,A2,RP) ORDERED */
                                      SUM (NVL (FD.TAXES_PAID, 0)) AS TAXES_PAID,
                                       TC1.TOBACCO_CLASS_NM,
                                       NVL (SF.FORM_TYPE_CD, '3852')
                                           AS FORM_TYPE_CD,
                                       RP.FISCAL_YR,
                                       CASE
                                           WHEN TC1.TOBACCO_CLASS_NM IS NULL
                                           THEN
                                               TO_CHAR (RP.QUARTER)
                                           ELSE
                                               TO_CHAR (RP.QUARTER)
                                       END
                                           AS QUARTER,
                                       P.PERMIT_TYPE_CD,
                                       C2.EIN,
                                       C2.COMPANY_ID,
                                       DECODE (PP.ZERO_FLAG,
                                               'N', NULL,
                                               PP.ZERO_FLAG)
                                           AS ZERO_FLAG -- treat 'N' or null as same indicating a non zero report
                                  FROM TU_COMPANY C2
                                       INNER JOIN TU_PERMIT P
                                           ON C2.COMPANY_ID = P.COMPANY_ID
                                       INNER JOIN TU_PERMIT_PERIOD PP
                                           ON P.PERMIT_ID = PP.PERMIT_ID
                                       INNER JOIN TU_RPT_PERIOD RP
                                           ON RP.PERIOD_ID = PP.PERIOD_ID
                                       INNER JOIN TU_ANNUAL_TRUEUP A2
                                           ON A2.FISCAL_YR = RP.FISCAL_YR
                                       INNER JOIN TU_PERIOD_STATUS PS
                                           ON     PP.PERIOD_ID = PS.PERIOD_ID
                                              AND PP.PERMIT_ID = PS.PERMIT_ID
                                       --      AND PS.PERIOD_STATUS_TYPE_CD !='NSTD'
                                       LEFT JOIN TU_SUBMITTED_FORM SF
                                           ON     SF.PERMIT_ID = PP.PERMIT_ID
                                              AND SF.PERIOD_ID = PP.PERIOD_ID
                                       LEFT JOIN TU_FORM_DETAIL FD
                                           ON SF.FORM_ID = FD.FORM_ID
                                       LEFT JOIN TU_TOBACCO_CLASS TC1
                                           ON FD.TOBACCO_CLASS_ID =
                                                  TC1.TOBACCO_CLASS_ID
                                 WHERE     (   SF.FORM_TYPE_CD = '3852'
                                            OR SF.FORM_TYPE_CD IS NULL)
                                       AND P.PERMIT_TYPE_CD = 'IMPT'
                              GROUP BY TC1.TOBACCO_CLASS_NM,
                                       NVL (SF.FORM_TYPE_CD, '3852'),
                                       RP.FISCAL_YR,
                                       CASE
                                           WHEN TC1.TOBACCO_CLASS_NM IS NULL
                                           THEN
                                               TO_CHAR (RP.QUARTER)
                                           ELSE
                                               TO_CHAR (RP.QUARTER)
                                       END,
                                       P.PERMIT_TYPE_CD,
                                       C2.EIN,
                                       C2.COMPANY_ID,
                                       DECODE (PP.ZERO_FLAG,
                                               'N', NULL,
                                               PP.ZERO_FLAG)) REPORTS
                                 ON     REPORTS.QUARTER = INGESTED.FISCAL_QTR
                                    AND REPORTS.FISCAL_YR =
                                            INGESTED.FISCAL_YEAR
                                    AND INGESTED.COMPANY_ID =
                                            REPORTS.COMPANY_ID
                                    AND (   REPORTS.TOBACCO_CLASS_NM =
                                                INGESTED.TOBACCO_CLASS_NM
                                         OR REPORTS.ZERO_FLAG = 'Y') --AND REPORTS.PERMIT_TYPE_CD     = 'IMPT'
                                                                    ))
             PIVOT
                 (MAX (CURRENT_DELTA)
                 FOR CLASSNAME
                 IN ('Cigars' AS CURRENT_DELTA_CIGARS,
                    'Pipe' AS CURRENT_DELTA_PIPE,
                    'Snuff' AS CURRENT_DELTA_SNUFF,
                    'Chew' AS CURRENT_DELTA_CHEW,
                    'Cigarettes' AS CURRENT_DELTA_CIGS,
                    'Roll-Your-Own' AS CURRENT_DELTA_RYO))
    ORDER BY FISCAL_YR, COMPANY_ID, QUARTER
;

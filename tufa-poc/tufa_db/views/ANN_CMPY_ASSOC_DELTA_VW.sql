--------------------------------------------------------
--  DDL for View ANN_CMPY_ASSOC_DELTA_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ANN_CMPY_ASSOC_DELTA_VW" ("COMPANY_ID", "LEGAL_NM", "QUARTER", "FISCAL_YR", "ASSOCIATION_TYPE_CD", "CBP_IMPORTER_ID", "IMPORTER_NM", "CONSIGNEE_NM", "IMP_IN_TUFA", "CONSG_IN_TUFA", "IMP_MATCH", "CONSG_MATCH", "IMPORTER_EIN", "CONSIGNEE_EIN", "TAXES_CIGARS", "TAXES_PIPE", "TAXES_SNUFF", "TAXES_CHEW", "TAXES_CIGS", "TAXES_RYO", "TAXES_UNKNOWN") AS 
  SELECT "COMPANY_ID",
             "LEGAL_NM",
             "QUARTER",
             "FISCAL_YR",
             "ASSOCIATION_TYPE_CD",
             "CBP_IMPORTER_ID",
             "IMPORTER_NM",
             "CONSIGNEE_NM",
             "IMP_IN_TUFA",
             "CONSG_IN_TUFA",
             "IMP_MATCH",
             "CONSG_MATCH",
             "IMPORTER_EIN",
             "CONSIGNEE_EIN",
             "TAXES_CIGARS",
             "TAXES_PIPE",
             "TAXES_SNUFF",
             "TAXES_CHEW",
             "TAXES_CIGS",
             "TAXES_RYO",
             "TAXES_UNKNOWN"
        FROM (  SELECT COMPANY_ID,
                       LEGAL_NM,
                       DECODE (SUM (NVL (CBP_TAXES_PAID, 0)),
                               0, NULL,
                               SUM (NVL (CBP_TAXES_PAID, 0)))
                           AS TAXES_PAID,
                       FISCAL_QTR                        AS QUARTER,
                       FISCAL_YR,
                       DECODE (TOBACCO_CLASS_NM,
                               NULL, 'UNKNOWN',
                               TOBACCO_CLASS_NM)
                           AS TOBACCO_CLASS_NM,
                       ASSOCIATION_TYPE_CD,
                       CBP_IMPORTER_ID,
                       IMPORTER_NM,
                       CONSIGNEE_NM,
                       CASE
                           WHEN IMPORTER_EIN IN (SELECT EIN
                                                   FROM TU_COMPANY)
                           THEN
                               'Y'
                           ELSE
                               'N'
                       END
                           AS IMP_IN_TUFA,
                       CASE
                           WHEN CONSIGNEE_EIN IN (SELECT EIN
                                                    FROM TU_COMPANY)
                           THEN
                               'Y'
                           ELSE
                               'N'
                       END
                           AS CONSG_IN_TUFA,
                       DECODE (IMPORTER_EIN, EIN, 'Y', 'N') AS IMP_MATCH,
                       DECODE (CONSIGNEE_EIN, EIN, 'Y', 'N') AS CONSG_MATCH,
                       IMPORTER_EIN,
                       CONSIGNEE_EIN
                  FROM (SELECT C1.COMPANY_ID,
                               C1.EIN,
                               C1.LEGAL_NM,
                               CBPINGESTED.*,
                               MAX (C1.CREATED_DT)
                               OVER (PARTITION BY CBPINGESTED.CBP_IMPORTER_ID)
                                   AS CMPY_CRT_DT,
                               CBPINGESTED.CREATED_DT AS IN_CRT_DT
                          FROM TU_ANNUAL_TRUEUP A1
                               INNER JOIN
                               (  SELECT CBPDETAIL.CBP_IMPORTER_ID,
                                         CBPDETAIL.IMPORTER_NM,
                                         CBPDETAIL.CONSIGNEE_NM,
                                         CBPDETAIL.IMPORTER_EIN,
                                         CBPDETAIL.CONSIGNEE_EIN,
                                         CBPDETAIL.FISCAL_YR,
                                         SUM (NVL (CBPENTRY.ESTIMATED_TAX, 0))
                                             AS CBP_TAXES_PAID,
                                         TTC.TOBACCO_CLASS_NM,
                                         TTC.TOBACCO_CLASS_ID AS TOBACCO_CLASS_ID,
                                         CBPENTRY.FISCAL_QTR AS FISCAL_QTR,
                                         CBPDETAIL.ASSOCIATION_TYPE_CD,
                                         CBPDETAIL.CREATED_DT
                                    FROM TU_CBP_IMPORTER CBPDETAIL
                                         INNER JOIN TU_CBP_ENTRY CBPENTRY
                                             ON CBPDETAIL.CBP_IMPORTER_ID =
                                                    CBPENTRY.CBP_IMPORTER_ID
                                         LEFT JOIN TU_TOBACCO_CLASS TTC
                                             ON CBPENTRY.TOBACCO_CLASS_ID =
                                                    TTC.TOBACCO_CLASS_ID
                                   WHERE --  (CBPENTRY.EXCLUDED_FLAG IS NULL OR CBPENTRY.EXCLUDED_FLAG     <> 'Y')
                        --   CBPENTRY.TOBACCO_CLASS_ID  = TTC.TOBACCO_CLASS_ID
                                       --  AND (TTC.TOBACCO_CLASS_ID     <> 14
                           --  OR TTC.TOBACCO_CLASS_NM       <> 'Non-Taxable')
                 --  AND CBPDETAIL.CBP_IMPORTER_ID  = CBPENTRY.CBP_IMPORTER_ID
                                             CBPDETAIL.IMPORTER_EIN <>
                                                 CBPDETAIL.CONSIGNEE_EIN
                                         AND CBPDETAIL.DEFAULT_FLAG = 'N'
                                GROUP BY CBPDETAIL.CBP_IMPORTER_ID,
                                         CBPDETAIL.IMPORTER_NM,
                                         CBPDETAIL.CONSIGNEE_NM,
                                         CBPDETAIL.IMPORTER_EIN,
                                         CBPDETAIL.CONSIGNEE_EIN,
                                         CBPDETAIL.FISCAL_YR,
                                         TTC.TOBACCO_CLASS_NM,
                                         TTC.TOBACCO_CLASS_ID,
                                         CBPENTRY.FISCAL_QTR,
                                         CBPDETAIL.ASSOCIATION_TYPE_CD,
                                         CBPDETAIL.CREATED_DT) CBPINGESTED
                                   ON CBPINGESTED.FISCAL_YR = A1.FISCAL_YR
                               INNER JOIN TU_COMPANY C1
                                   ON     (   CBPINGESTED.CONSIGNEE_EIN = C1.EIN
                                           OR CBPINGESTED.IMPORTER_EIN = C1.EIN)
                                      AND (CBPINGESTED.IMPORTER_EIN <>
                                               CBPINGESTED.CONSIGNEE_EIN))
                       CMPY_ASSOC
--                 WHERE CMPY_CRT_DT <= IN_CRT_DT
              GROUP BY COMPANY_ID,
                       LEGAL_NM,
                       FISCAL_QTR,
                       FISCAL_YR,
                       TOBACCO_CLASS_NM,
                       ASSOCIATION_TYPE_CD,
                       CBP_IMPORTER_ID,
                       IMPORTER_NM,
                       CONSIGNEE_NM,
                       DECODE (IMPORTER_EIN, EIN, 'Y', 'N'),
                       DECODE (CONSIGNEE_EIN, EIN, 'Y', 'N'),
                       IMPORTER_EIN,
                       CONSIGNEE_EIN
              ORDER BY company_id)
             PIVOT
                 (MAX (TAXES_PAID)
                 FOR TOBACCO_CLASS_NM
                 IN ('Cigars' AS TAXES_CIGARS,
                    'Pipe' AS TAXES_PIPE,
                    'Snuff' AS TAXES_SNUFF,
                    'Chew' AS TAXES_CHEW,
                    'Cigarettes' AS TAXES_CIGS,
                    'Roll-Your-Own' AS TAXES_RYO,
                    'UNKNOWN' AS TAXES_UNKNOWN))
    ORDER BY company_id, imp_in_tufa, consg_in_tufa
;

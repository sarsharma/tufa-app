--------------------------------------------------------
--  DDL for View ANN_IMPORTER_DETAIL_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ANN_IMPORTER_DETAIL_VW" ("COMPANY_ID", "TAXES", "FISCAL_QTR", "FISCAL_YR", "MONTH", "TOBACCO_CLASS_NM", "TOBACCO_CLASS_ID") AS 
  SELECT
--     c1.legal_nm,
--     cbpingested.importer_ein   AS ein,
     c1.company_id,
     SUM(cbpingested.cbp_taxes_paid) AS taxes,
     cbpingested.fiscal_qtr,
     cbpingested.fiscal_year as fiscal_yr,
     trim(cbpingested.assigned_dt)    AS month,
     cbpingested.tobacco_class_nm,
     cbpingested.tobacco_class_id
 FROM
     tu_annual_trueup a1,
     (
         SELECT /*+ INDEX(CBPENTRY TU_CBP_ENTRY_UK) */
             cbpdetail.importer_ein,
             cbpdetail.consignee_ein,
             SUM(cbpentry.estimated_tax) AS cbp_taxes_paid,
             ttc.tobacco_class_nm,
             DECODE(ttc.tobacco_class_nm,'Cigars',5,cbpentry.fiscal_qtr) as fiscal_qtr,
             cbpentry.fiscal_year,
             cbpentry.assigned_dt,
             cbpdetail.consignee_exists_flag,
             cbpdetail.association_type_cd,
             ttc.tobacco_class_id   AS tobacco_class_id
         FROM
             tu_cbp_importer cbpdetail,
             tu_cbp_entry cbpentry,
             tu_tobacco_class ttc
         WHERE
             ( cbpentry.excluded_flag IS NULL
               OR cbpentry.excluded_flag <> 'Y' )
             AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
             AND ( ttc.tobacco_class_id <> 14
                   OR ttc.tobacco_class_nm <> 'Non-Taxable' )
             AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
         GROUP BY
             cbpdetail.importer_ein,
             cbpdetail.consignee_ein,
             ttc.tobacco_class_nm,
             DECODE(ttc.tobacco_class_nm,'Cigars',5,cbpentry.fiscal_qtr),
             cbpentry.assigned_dt,
             cbpentry.fiscal_year,
             cbpdetail.consignee_exists_flag,
             cbpdetail.association_type_cd,
             ttc.tobacco_class_id,
             cbpentry.summary_entry_summary_number
     ) cbpingested,
     tu_company c1
 WHERE
     ( cbpingested.importer_ein = c1.ein
       AND cbpingested.association_type_cd = 'IMPT'
       OR cbpingested.consignee_ein = c1.ein
       AND cbpingested.association_type_cd = 'CONS'
       OR ( cbpingested.consignee_ein = c1.ein
            AND cbpingested.association_type_cd IS NULL
            AND cbpingested.consignee_exists_flag = 'N' ) )
     AND cbpingested.fiscal_year = a1.fiscal_yr
 GROUP BY
--     c1.legal_nm,
--     cbpingested.importer_ein   AS ein,
     c1.company_id,
     cbpingested.fiscal_qtr,
     cbpingested.fiscal_year,
     cbpingested.assigned_dt,
     cbpingested.tobacco_class_nm,
     cbpingested.tobacco_class_id
;

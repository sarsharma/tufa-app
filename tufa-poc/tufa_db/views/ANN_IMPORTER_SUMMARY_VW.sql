--------------------------------------------------------
--  DDL for View ANN_IMPORTER_SUMMARY_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ANN_IMPORTER_SUMMARY_VW" ("COMPANY_ID", "TAXES", "FISCAL_QTR", "FISCAL_YR", "MONTH", "TOBACCO_CLASS_NM", "TOBACCO_CLASS_ID") AS 
  SELECT
--     legal_nm,
--     ein,
     company_id,
     SUM(taxes) AS taxes,      -- Now roll up the summary taxes for all ESNs in a period for that company
     fiscal_qtr,
     fiscal_yr,
     assigned_dt as month,
     tobacco_class_nm,
     tobacco_class_id
 FROM
     (
     -- Select unique summary databased on ESN
         SELECT DISTINCT
             c1.legal_nm,
             cbpingested.importer_ein   AS ein,
             cbpingested.summarized_tax AS taxes,
             cbpingested.fiscal_qtr,
             cbpingested.fiscal_year    AS fiscal_yr,
                    -- Added in the case that the summary file date doesn't match the details file
             DECODE(reallocated_flag,'Y',TRIM(cbpingested.assigned_dt),TO_CHAR(cbpingested.summary_entry_summary_date,'MON') ) as assigned_dt,
             cbpingested.tobacco_class_nm,
             cbpingested.tobacco_class_id,
             c1.company_id,
             cbpingested.summary_entry_summary_number
         FROM
             tu_annual_trueup a1,
             (
                 SELECT /*+ INDEX(CBPENTRY TU_CBP_ENTRY_UK) */
                     cbpdetail.importer_ein,
                     cbpdetail.consignee_ein,
                     nvl(cbpentry.summarized_file_tax,0) AS summarized_tax,
                     ttc.tobacco_class_nm,
                     DECODE(ttc.tobacco_class_nm,'Cigars',5,cbpentry.fiscal_qtr) AS fiscal_qtr,
                     cbpentry.fiscal_year,
                     cbpentry.summary_entry_summary_date,
                     cbpentry.assigned_dt,
                     cbpentry.reallocated_flag,
                     cbpdetail.consignee_exists_flag,
                     cbpdetail.association_type_cd,
                     ttc.tobacco_class_id   AS tobacco_class_id,
                     cbpentry.summary_entry_summary_number
                 FROM
                     tu_cbp_importer cbpdetail,
                     tu_cbp_entry cbpentry,
                     tu_tobacco_class ttc
                 WHERE
                     ( cbpentry.excluded_flag IS NULL
                       OR cbpentry.excluded_flag <> 'Y' )
                     AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
                     AND ( ttc.tobacco_class_id <> 14
                           OR ttc.tobacco_class_nm <> 'Non-Taxable' )
                     AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
             ) cbpingested,
             tu_company c1
         WHERE
             ( cbpingested.importer_ein = c1.ein
               AND cbpingested.association_type_cd = 'IMPT'
               OR cbpingested.consignee_ein = c1.ein
               AND cbpingested.association_type_cd = 'CONS'
               OR ( cbpingested.consignee_ein = c1.ein
                    AND cbpingested.association_type_cd IS NULL
                    AND cbpingested.consignee_exists_flag = 'N' ) )
             AND cbpingested.fiscal_year = a1.fiscal_yr
     )
 GROUP BY
--     legal_nm,
--     ein,
     fiscal_qtr,
     fiscal_yr,
     assigned_dt,
     tobacco_class_nm,
     tobacco_class_id,
     company_id
;

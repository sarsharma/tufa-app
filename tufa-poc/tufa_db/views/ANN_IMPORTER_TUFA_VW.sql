--------------------------------------------------------
--  DDL for View ANN_IMPORTER_TUFA_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ANN_IMPORTER_TUFA_VW" ("COMPANY_ID", "TAXES", "FISCAL_QTR", "FISCAL_YR", "MONTH", "TOBACCO_CLASS_NM", "TOBACCO_CLASS_ID") AS 
  SELECT
--legal_nm,
--     ein,
    company_id,
    SUM(taxes_paid) AS taxes,
    fiscal_qtr,
    fiscal_yr,
    month,
    tobacco_class_nm,
    tobacco_class_id
FROM
    (
        SELECT /*+ USE_NL(TC1,FD,SF,PS,A2,RP) ORDERED */
            SUM(DECODE(pp.zero_flag,'Y',0,nvl(fd.taxes_paid,0) ) ) AS taxes_paid,
            tc1.tobacco_class_nm,
            tc1.tobacco_class_id,
            c2.legal_nm,
            c2.company_id,
            nvl(sf.form_type_cd,'3852') AS form_type_cd,
            rp.fiscal_yr,
            DECODE(tc1.tobacco_class_nm,'Cigars',5,rp.quarter) AS fiscal_qtr,
            rp.month,
            p.permit_type_cd,
            c2.ein,
                                            -- C2.COMPANY_ID,
            pp.zero_flag
        FROM
            tu_company c2
            INNER JOIN tu_permit p ON c2.company_id = p.company_id
            INNER JOIN tu_permit_period pp ON p.permit_id = pp.permit_id
            INNER JOIN tu_rpt_period rp ON rp.period_id = pp.period_id
            INNER JOIN tu_annual_trueup a2 ON a2.fiscal_yr = rp.fiscal_yr
            INNER JOIN tu_period_status ps ON pp.period_id = ps.period_id
                                              AND pp.permit_id = ps.permit_id
            LEFT JOIN tu_submitted_form sf ON sf.permit_id = pp.permit_id
                                              AND sf.period_id = pp.period_id
            LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id
            LEFT JOIN tu_tobacco_class tc1 ON fd.tobacco_class_id = tc1.tobacco_class_id
        WHERE
            nvl(form_type_cd,3852) = 3852
            AND permit_type_cd = 'IMPT'
            -- If a MR has a selected tobacco but no data entered in 3852 treat it as 0
            AND period_status_type_cd <> 'NSTD'
        GROUP BY
            tc1.tobacco_class_nm,
            tc1.tobacco_class_id,
            nvl(sf.form_type_cd,'3852'),
            rp.fiscal_yr,
            DECODE(tc1.tobacco_class_nm,'Cigars',5,rp.quarter),
            rp.month,
            p.permit_type_cd,
            c2.legal_nm,
            c2.ein,
            c2.company_id,
            pp.zero_flag
    )
GROUP BY
--legal_nm,
--     ein,
    company_id,
    fiscal_qtr,
    fiscal_yr,
    month,
    tobacco_class_nm,
    tobacco_class_id
;

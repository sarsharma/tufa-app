--------------------------------------------------------
--  DDL for View ANN_TRUEUP_COMPARERESULTS_MANU
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ANN_TRUEUP_COMPARERESULTS_MANU" ("LEGAL_NM", "CLASSNAME", "QUARTERX", "DELTA", "PERMIT_TYPE_CD", "COMPANY_ID", "FISCAL_YR", "ACCEPTANCE_FLAG", "DELTA_CHANGE", "FDA_DELTA", "IN_PROGRESS_FLAG", "EIN", "STATUS", "PERMIT_NUM", "DELTAEXCISETAX_FLAG") AS 
  SELECT
        legal_nm,
        classname,
        quarterx,
        delta,
        permit_type_cd,
        company_id,
        fiscal_yr,
        acceptance_flag,
        delta_change,
        fda_delta,
        CASE
            WHEN in_progress_flag = 'Y' THEN
                'InProgress'
            WHEN in_progress_flag = 'N' THEN
                'NotInProgress'
            ELSE
                NULL
        END AS in_progress_flag,
        ein,
        'NA' AS status,
        permit_num,
        'Value' AS deltaexcisetax_flag
    FROM
        (
            SELECT
                ingested.legal_nm           AS legal_nm,
                ingested.ein_num            AS ein,
                ingested.tobacco_class_nm   AS classname,
                to_char(ingested.fiscal_qtr) AS quarterx,
                SUM(nvl(reports.taxes_paid, 0)) - nvl(ingested.ttb_taxes_paid, 0) AS delta,
                nvl(reports.permit_type_cd, 'MANU') AS permit_type_cd,
                ingested.company_id         AS company_id,
                ingested.fiscal_yr          AS fiscal_yr,
                ttbamnd.acceptance_flag     AS acceptance_flag,
                ttbamnd.in_progress_flag    AS in_progress_flag,
                CASE
                    WHEN ttbamnd.fda_delta IS NOT NULL
                         AND ( abs(ttbamnd.fda_delta - SUM(nvl(reports.taxes_paid, 0)) + nvl(ingested.ttb_taxes_paid, 0)) > 0
                               OR SUM(nvl(reports.taxes_paid, 0)) - nvl(ingested.ttb_taxes_paid, 0) = 0 ) THEN
                        'Y'
                    WHEN ttbamnd.fda_delta IS NULL
                         AND ttbamnd.acceptance_flag IN
                                      --4046 Kyle - Ingestedaccepted
                          (
                        'FDAaccepted',
                        'Ingestedaccepted',
                        'Amended'
                    ) THEN
                        'I'
                    ELSE
                        'N'
                END AS delta_change,
                ttbamnd.fda_delta           AS fda_delta,
                ( reports.permit_num
                  || ';'
                  || ingested.permit_num ) AS permit_num
            FROM
                (
                    SELECT
                        c1.legal_nm,
                        ttbingested.ein_num,
                        to_char(ttbingested.tobacco_class_nm) AS tobacco_class_nm,
                        ttbingested.fiscal_qtr,
                        nvl(ttbingested.ttb_taxes_paid, 0) AS ttb_taxes_paid,
                        c1.company_id,
                        a1.fiscal_yr AS fiscal_yr,
                        ttbingested.tobacco_class_id,
                        ttbingested.permit_num
                    FROM
                        tu_annual_trueup   a1,
                        (
                            SELECT
                                abc.ein_num,
                                abc.tobacco_class_nm,
                                abc.tobacco_class_id,
                                abc.fiscal_yr,
                                abc.fiscal_qtr,
                                abc.permit_num,
                                SUM(nvl(abc.ttb_taxes_paid, 0)) AS ttb_taxes_paid
                            FROM
                                (
                                    SELECT /*+ NO_INDEX(TTC) */
                                        ttbcmpy.ein_num,
                                        ttbannualtx.ttb_taxes_paid,
                                        CASE
                                            WHEN ttc.tobacco_class_nm = 'Chew-and-Snuff' THEN
                                                'Chew/Snuff'
                                            WHEN ttc.tobacco_class_nm = 'Pipe-RYO'       THEN
                                                'Pipe/Roll Your Own'
                                            ELSE
                                                ttc.tobacco_class_nm
                                        END AS tobacco_class_nm,
                                        ttc.tobacco_class_id,
                                        ttbcmpy.fiscal_yr,
                                        CASE
                                            WHEN ttc.tobacco_class_nm <> 'Cigars' THEN
                                                to_char(ttbannualtx.ttb_calendar_qtr)
                                            ELSE
                                                '1-4'
                                        END AS fiscal_qtr,
                                        (
                                            SELECT DISTINCT
                                                LISTAGG(ab.permit_num, ';') WITHIN GROUP(
                                                    ORDER BY
                                                        ab.created_dt
                                                ) OVER(
                                                    PARTITION BY ab.ttb_company_id
                                                )
                                            FROM
                                                tu_ttb_permit ab
                                            WHERE
                                                ab.ttb_company_id = ttbcmpy.ttb_company_id
                                        ) AS permit_num
                                    FROM
                                        tu_ttb_company      ttbcmpy,
                                        tu_ttb_permit       ttbpermit,
                                        tu_ttb_annual_tax   ttbannualtx,
                                        tu_tobacco_class    ttc
                                    WHERE
                                        ttbpermit.permit_num IN (
                                            SELECT
                                                permit_num
                                            FROM
                                                tu_permit
                                        )
                                        AND ttbannualtx.tobacco_class_id = ttc.tobacco_class_id
                                        AND ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id
                                        AND ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id
                                               --Mohan Bhutada: changes for TTB exclusion - US4946
                                               --comenting out this check as this will be done at the end of block
                                               --this block will get ttb_annual_tax_id for those TU_company_id not present in tu_ttb_amendment and which matches assmnt_yr,assmnt_qtr as per exclude
--                                    AND ttbannualtx.ttb_annual_tax_id NOT IN (
--                                        SELECT
--                                            c.ttb_annual_tax_id
--                                        FROM
--                                            (
--                                                SELECT
--                                                    vw.company_id,
--                                                    vw.assmnt_yr,
--                                                    vw.assmnt_qtr,
--                                                    ttbingested.ttb_annual_tax_id,
--                                                    vw.perm_excl_audit_id,
--                                                    ttbingested.tobacco_class_id
--                                                FROM
--                                                    permit_excl_status_vw vw,
--                                                    (
--                                                        SELECT
--                                                            ttc.ein_num,
--                                                            tp.permit_num,
--                                                            tta.ttb_annual_tax_id,
--                                                            tta.ttb_fiscal_yr,
--                                                            tta.ttb_calendar_qtr,
--                                                            tta.tobacco_class_id,
--                                                            tp.ttb_company_id
--                                                        FROM
--                                                            tu_ttb_annual_tax   tta
--                                                            INNER JOIN tu_ttb_permit       tp ON tp.ttb_permit_id = tta.ttb_permit_id
--                                                            INNER JOIN tu_ttb_company      ttc ON ttc.ttb_company_id = tp.ttb_company_id
--                                                    ) ttbingested
--                                                WHERE
--                                                    ( decode(vw.assmnt_qtr, 1, vw.assmnt_yr - 1, vw.assmnt_yr) = ttbingested.ttb_fiscal_yr
--                                                      AND decode(vw.assmnt_qtr, 1, 4, vw.assmnt_qtr - 1) = ttbingested.ttb_calendar_qtr
--                                                      AND vw.excl_scope_id IN (
--                                                        1,
--                                                        2
--                                                    )
--                                                      AND ttbingested.ein_num = vw.ein
--                                                      AND ttbingested.permit_num = vw.permit_num
--                                                      AND ttbingested.tobacco_class_id <> 8 )
--                                                    OR ( ttbingested.tobacco_class_id = 8
--                                                         AND ( ( vw.excl_scope_id = 1
--                                                                 AND vw.assmnt_qtr = 1
--                                                                 AND vw.assmnt_yr = ttbingested.ttb_fiscal_yr + 1 )
--                                                               OR ( ( vw.excl_scope_id = 1
--                                                                      AND vw.assmnt_qtr != 1
--                                                                      AND vw.assmnt_yr = ttbingested.ttb_fiscal_yr + 1
--                                                                      AND vw.assmnt_qtr - 1 = (
--                                                        SELECT
--                                                            COUNT(*)
--                                                        FROM
--                                                            permit_excl_status_vw vw1
--                                                        WHERE
--                                                            vw1.excl_scope_id = 2
--                                                            AND vw1.assmnt_yr = ttbingested.ttb_fiscal_yr + 1
--                                                            AND ttbingested.ein_num = vw1.ein
--                                                            AND ttbingested.permit_num = vw1.permit_num
--                                                    ) )
--                                                                    OR ( vw.excl_scope_id = 2
--                                                                         AND vw.assmnt_yr = ttbingested.ttb_fiscal_yr + 1
--                                                                         AND 4 = (
--                                                        SELECT
--                                                            COUNT(*)
--                                                        FROM
--                                                            permit_excl_status_vw vw1
--                                                        WHERE
--                                                            vw1.excl_scope_id = 2
--                                                            AND vw1.assmnt_yr = ttbingested.ttb_fiscal_yr + 1
--                                                            AND vw1.ein = ttbingested.ein_num
--                                                            AND ttbingested.permit_num = vw.permit_num
--                                                    ) ) ) )
--                                                         AND ttbingested.ein_num = vw.ein
--                                                         AND ttbingested.permit_num = vw.permit_num )
--                                            ) c
--                                        WHERE
--                                            ( c.company_id ) NOT IN (
--                                                SELECT
--                                                    d.ttb_company_id
--                                                FROM
--                                                    (
--                                                        SELECT
--                                                            ttam.fiscal_yr,
--                                                            ttam.qtr,
--                                                            decode(tc.parent_class_id, NULL, tc.tobacco_class_id, tc.parent_class_id
--                                                            ) AS parent_class_id,
--                                                            ttam.ttb_company_id
--                                                        FROM
--                                                            tu_ttb_amendment   ttam
--                                                            INNER JOIN tu_tobacco_class   tc ON ttam.tobacco_class_id = tc.tobacco_class_id
--                                                    ) d
--                                                WHERE
--                                                    ( ( d.parent_class_id != 8
--                                                        AND decode(c.assmnt_qtr, 1, c.assmnt_yr - 1, c.assmnt_yr) = d.fiscal_yr
--                                                        AND decode(c.assmnt_qtr, 1, 4, c.assmnt_qtr - 1) = d.qtr
--                                                        AND c.tobacco_class_id = d.parent_class_id )
--                                                      OR ( d.parent_class_id = 8
--                                                           AND c.assmnt_yr = d.fiscal_yr + 1
--                                                           AND d.qtr = 5 ) )
--                                            )
--                                    )
                                ) abc
                            GROUP BY                           --1,3,4,5,6,7
                                abc.ein_num,
                                abc.tobacco_class_nm,
                                abc.tobacco_class_id,
                                abc.fiscal_yr,
                                abc.fiscal_qtr,
                                abc.permit_num
                        ) ttbingested,
                        tu_company         c1
                    WHERE
                        ttbingested.ein_num = c1.ein
                        AND ttbingested.fiscal_yr = a1.fiscal_yr
                ) ingested
                INNER JOIN (
                    SELECT
                        SUM(nvl(fd.taxes_paid, 0)) AS taxes_paid,
                        CASE
                            WHEN tc1.tobacco_class_nm IN (
                                'Chew',
                                'Snuff'
                            ) THEN
                                'Chew/Snuff'
                            WHEN tc1.tobacco_class_nm IN (
                                'Roll-Your-Own',
                                'Pipe'
                            ) THEN
                                'Pipe/Roll Your Own'
                            ELSE
                                tc1.tobacco_class_nm
                        END AS tobacco_class_nm,
                        nvl(sf.form_type_cd, '3852') AS form_type_cd,
                        rp.fiscal_yr,
                        CASE
                            WHEN tc1.tobacco_class_nm IS NULL THEN
                                to_char(rp.quarter)
                            WHEN tc1.tobacco_class_nm <> 'Cigars' THEN
                                to_char(rp.quarter)
                            ELSE
                                '1-4'
                        END AS quarter,
                        p.permit_type_cd,
                        c2.ein,
                        c2.company_id,
                        pp.zero_flag,
                        (
                            SELECT DISTINCT
                                LISTAGG(a.permit_num, ';') WITHIN GROUP(
                                    ORDER BY
                                        a.created_dt
                                ) OVER(
                                    PARTITION BY a.company_id
                                )
                            FROM
                                tu_permit a
                            WHERE
                                a.company_id = c2.company_id
                        ) AS permit_num
                    FROM
                        tu_company          c2
                        INNER JOIN tu_permit           p ON c2.company_id = p.company_id
                        INNER JOIN tu_permit_period    pp ON p.permit_id = pp.permit_id
                        INNER JOIN tu_rpt_period       rp ON rp.period_id = pp.period_id
                        INNER JOIN tu_annual_trueup    a2 ON a2.fiscal_yr = rp.fiscal_yr
                        INNER JOIN tu_period_status    ps ON pp.period_id = ps.period_id
                                                          AND pp.permit_id = ps.permit_id
                        LEFT JOIN tu_submitted_form   sf ON sf.permit_id = pp.permit_id
                                                          AND sf.period_id = pp.period_id
                        LEFT JOIN tu_form_detail      fd ON sf.form_id = fd.form_id
                        LEFT JOIN tu_tobacco_class    tc1 ON fd.tobacco_class_id = tc1.tobacco_class_id
                    GROUP BY
                            CASE
                                WHEN tc1.tobacco_class_nm IN (
                                    'Chew',
                                    'Snuff'
                                ) THEN
                                    'Chew/Snuff'
                                WHEN tc1.tobacco_class_nm IN (
                                    'Roll-Your-Own',
                                    'Pipe'
                                ) THEN
                                    'Pipe/Roll Your Own'
                                ELSE
                                    tc1.tobacco_class_nm
                            END,
                            sf.form_type_cd,
                            rp.fiscal_yr,
                            CASE
                                WHEN tc1.tobacco_class_nm IS NULL THEN
                                    to_char(rp.quarter)
                                WHEN tc1.tobacco_class_nm <> 'Cigars' THEN
                                    to_char(rp.quarter)
                                ELSE
                                    '1-4'
                            END,
                            p.permit_type_cd,
                            c2.ein,
                            c2.company_id,
                            pp.zero_flag
                ) reports ON ( reports.quarter = ingested.fiscal_qtr
                               OR ingested.tobacco_class_nm = 'Cigars'
                               AND reports.zero_flag = 'Y' )
                             AND reports.fiscal_yr = ingested.fiscal_yr
                             AND ingested.company_id = reports.company_id
                             AND ( reports.tobacco_class_nm = ingested.tobacco_class_nm
                                   OR reports.zero_flag = 'Y' )
                             AND reports.permit_type_cd = 'MANU'
                             AND reports.form_type_cd = '3852'
                LEFT OUTER JOIN (
                    SELECT UNIQUE
                        tamnd.ttb_company_id AS cmpy_id,
                        decode(tamnd.qtr, 5, '1-4', tamnd.qtr) AS qtr,
                        decode(tamnd.acceptance_flag, 'Y', 'FDAaccepted',
                                       --4046 Kyle - Ingestedaccepted
                         'I', 'Ingestedaccepted',
                               'N', 'Amended', 'X', 'DeltaChange', 'E',
                               'ExcludeChange', 'noAction') acceptance_flag,
                        tamnd.in_progress_flag,
                        tamnd.fiscal_yr,
                        CASE
                            WHEN tc2.tobacco_class_nm IN (
                                'Chew',
                                'Snuff',
                                'Pipe',
                                'Roll-Your-Own'
                            ) THEN
                                tc2.parent_class_id
                            ELSE
                                tc2.tobacco_class_id
                        END AS tobacco_class_id,
                        tamnd.fda_delta
                    FROM
                        tu_ttb_amendment   tamnd,
                        tu_tobacco_class   tc2
                    WHERE
                        tc2.tobacco_class_id = tamnd.tobacco_class_id
                    GROUP BY
                        tamnd.ttb_company_id,
                        decode(tamnd.qtr, 5, '1-4', tamnd.qtr),
                        decode(tamnd.acceptance_flag, 'Y', 'FDAaccepted',
                                       --4046 Kyle - Ingestedaccepted
                         'I', 'Ingestedaccepted',
                               'N', 'Amended', 'X', 'DeltaChange', 'E',
                               'ExcludeChange', 'noAction'),
                        tamnd.in_progress_flag,
                        tamnd.fiscal_yr,
                        CASE
                                WHEN tc2.tobacco_class_nm IN (
                                    'Chew',
                                    'Snuff',
                                    'Pipe',
                                    'Roll-Your-Own'
                                ) THEN
                                    tc2.parent_class_id
                                ELSE
                                    tc2.tobacco_class_id
                            END,
                        tamnd.fda_delta
                ) ttbamnd ON ttbamnd.cmpy_id = ingested.company_id
                             AND ttbamnd.fiscal_yr = ingested.fiscal_yr
                             AND to_char(ttbamnd.qtr) = ingested.fiscal_qtr
                             AND ttbamnd.tobacco_class_id = ingested.tobacco_class_id
            WHERE ------------------------------------Changes for Story#4946 and Defect#5805-----------------------------
--------------------------------- Changes for Non Cigar Classes ------------------------------
                ( ( ingested.tobacco_class_id <> 8
                    AND ingested.company_id NOT IN (
                    SELECT DISTINCT
                        company_id
                    FROM
                        permit_excl_status_vw vw
                    WHERE
                        (
                        -- Kyle: Number error throwing
                         assmnt_yr = decode(ingested.fiscal_qtr, '4', ingested.fiscal_yr + 1, ingested.fiscal_yr)
                          AND assmnt_qtr = decode(ingested.fiscal_qtr, '1-4', 5, '4', 1,
                                                  ingested.fiscal_qtr + 1)
                          AND excl_scope_id = 2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                          AND vw.company_id NOT IN (
                            SELECT
                                tcam.ttb_company_id
                            FROM
                                tu_ttb_amendment   tcam
                                LEFT JOIN tu_tobacco_class   tc ON tcam.tobacco_class_id = tc.tobacco_class_id
                            WHERE
                                vw.assmnt_qtr = decode(tcam.qtr, 4, 1, tcam.qtr + 1)
                                AND vw.assmnt_yr = decode(tcam.qtr, 4, tcam.fiscal_yr + 1, tcam.fiscal_yr)
                                AND decode(tc.parent_class_id, NULL, tc.tobacco_class_id, tc.parent_class_id) = ingested.tobacco_class_id
                        ) )
------------------------Added below Conditions for full exclusion----------------------------
                        OR ( excl_scope_id = 1
                             AND decode(vw.assmnt_qtr, 1, vw.assmnt_yr - 1, vw.assmnt_yr) = ingested.fiscal_yr
                        -- Kyle: Number error throwing
                             AND decode(vw.assmnt_qtr, 1, 4, vw.assmnt_qtr - 1) = decode(ingested.fiscal_qtr, '1-4', 5, ingested.
                             fiscal_qtr)
                             AND vw.company_id NOT IN (
                            SELECT
                                tcam1.ttb_company_id
                            FROM
                                tu_ttb_amendment   tcam1
                                LEFT JOIN tu_tobacco_class   tc ON tcam1.tobacco_class_id = tc.tobacco_class_id
                            WHERE
                                vw.assmnt_qtr = decode(tcam1.qtr, 4, 1, tcam1.qtr + 1)
                                AND vw.assmnt_yr = decode(tcam1.qtr, 4, tcam1.fiscal_yr + 1, tcam1.fiscal_yr)
                                     AND decode(tc.parent_class_id, NULL, tc.tobacco_class_id, tc.parent_class_id) = ingested.tobacco_class_id
--                                OR ( vw.assmnt_qtr <= ( decode(tcam1.qtr, 4, 1, tcam1.qtr + 1) )
--                                     AND vw.assmnt_yr = decode(tcam1.qtr, 4, tcam1.fiscal_yr + 1, tcam1.fiscal_yr)
--                                     AND decode(tc.parent_class_id, NULL, tc.tobacco_class_id, tc.parent_class_id) = ingested.tobacco_class_id
--                                     )
                        ) )
                ) )
   --------------------------Chnages for Cigar Class-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                  OR ( ( ingested.tobacco_class_id = 8
                         AND ingested.company_id NOT IN (
                    SELECT DISTINCT
                        vw.company_id
                    FROM
                        permit_excl_status_vw vw
                    WHERE
                        ( ( vw.excl_scope_id = 1
                            AND vw.assmnt_qtr = 1
                            AND vw.assmnt_yr = ingested.fiscal_yr + 1 )
                          OR ( ( vw.excl_scope_id = 1
                                 AND vw.assmnt_qtr != 1
                                 AND vw.assmnt_yr = ingested.fiscal_yr + 1
                                 AND (select count(*) from tu_permit where company_id=ingested.company_id)*(vw.assmnt_qtr - 1) = (
                            SELECT
                                COUNT(*)
                            FROM
                                permit_excl_status_vw vw1
                            WHERE
                                vw1.excl_scope_id = 2
                                AND vw1.assmnt_yr = ingested.fiscal_yr + 1
                                AND ingested.ein_num = vw1.ein
                        ) )
                               OR ( vw.excl_scope_id = 2
                                    AND vw.assmnt_yr = ingested.fiscal_yr + 1
                                    AND (select count(*) from tu_permit where company_id=ingested.company_id)*4 = (
                            SELECT
                                COUNT(*)
                            FROM
                                permit_excl_status_vw vw1
                            WHERE
                                vw1.excl_scope_id = 2
                                AND vw1.assmnt_yr = ingested.fiscal_yr + 1
                                AND ingested.ein_num = vw1.ein
                        ) ) ) )
                        AND ( vw.company_id NOT IN (
                            SELECT
                                ttam1.ttb_company_id
                            FROM
                                tu_ttb_amendment ttam1
                            WHERE
                                ttam1.fiscal_yr + 1 = vw.assmnt_yr
                                AND ttam1.qtr = 5
                        ) )
                ) )
                            --OR
                           -- fd.tobacco_class_id = 8
                 ) )
            GROUP BY
                ingested.legal_nm,
                ingested.tobacco_class_nm,
                to_char(ingested.fiscal_qtr),
                nvl(reports.permit_type_cd, 'MANU'),
                ingested.company_id,
                ingested.fiscal_yr,
                ttbamnd.acceptance_flag,
                ttbamnd.in_progress_flag,
                ingested.ttb_taxes_paid,
                ingested.ein_num,
                ttbamnd.fda_delta,
                ( reports.permit_num
                  || ';'
                  || ingested.permit_num )
        )
    WHERE
        abs(delta) > 0.99
        OR delta_change = 'Y'
        OR acceptance_flag = 'DeltaChange'
;

--------------------------------------------------------
--  DDL for View ANN_TRUEUP_COMPARERES_IMP_SNG
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ANN_TRUEUP_COMPARERES_IMP_SNG" ("LEGAL_NM", "CLASSNAME", "QUARTERX", "DELTA", "PERMIT_TYPE_CD", "COMPANY_ID", "FISCAL_YR", "ACCEPTANCE_FLAG", "DELTA_CHANGE", "FDA_DELTA", "IN_PROGRESS_FLAG", "EIN", "STATUS", "PERMIT_NUM", "DELTAEXCISETAX_FLAG") AS 
  SELECT legal_nm,
           classname,
           quarterx,
           delta,
           permit_type_cd,
           company_id,
           fiscal_yr,
           acceptance_flag,
           delta_change,
           fda_delta,
           in_progress_flag,
           ein,
           status,
           permit_num,
           deltaexcisetax_flag
      FROM (SELECT legal_nm,
                   classname,
                   quarterx,
                   tufa_detail_delta AS delta,
                   permit_type_cd,
                   company_id,
                   fiscal_yr,
                   acceptance_flag,
                   delta_change,
                   fda_delta,
                   CASE
                       WHEN in_progress_flag = 'Y' THEN 'InProgress'
                       WHEN in_progress_flag = 'N' THEN 'NotInProgress'
                       ELSE NULL
                   END
                       AS in_progress_flag,
                   ein,
                   'NA'              AS status,
                   permit_num,
                   --            Changes for Story CTPTUFA-5868: Removing Summary file logic
                   --            CASE
                   --                       -- All the deltas match  or TUFA matches summary
                   --                WHEN ( ( ( abs(tufa_detail_delta) < 1 )
                   --                         AND ( abs(tufa_summary_delta) < 1 )
                   --                         AND ( abs(summary_detail_delta) < 1 ) )
                   --                       OR ( ( abs(tufa_detail_delta) >= 1 )
                   --                            AND ( abs(tufa_summary_delta) < 1 )
                   --                            AND ( abs(summary_detail_delta) >= 1 ) ) ) THEN
                   --                    'Match'
                   --                       -- summary and detail match then display delta of tufa and details
                   --                WHEN ( abs(tufa_detail_delta) >= 1 )
                   --                     AND ( abs(tufa_summary_delta) >= 1 )
                   --                     AND ( abs(summary_detail_delta) < 1 ) THEN
                   --                    'Value'
                   --                       -- none of the delta match and tufa matches with detail but not summary
                   --                WHEN ( ( ( abs(tufa_detail_delta) >= 1 )
                   --                         AND ( abs(tufa_summary_delta) >= 1 )
                   --                         AND ( abs(summary_detail_delta) >= 1 ) )
                   --                       OR ( ( abs(tufa_detail_delta) < 1 )
                   --                            AND ( abs(tufa_summary_delta) >= 1 )
                   --                            AND ( abs(summary_detail_delta) >= 1 ) ) ) THEN
                   --                    'Review'
                   --            END AS deltaexcisetax_flag
                   'Value'           AS deltaexcisetax_flag
              FROM (  SELECT ingested.legal_nm           AS legal_nm,
                             ingested.ein                AS ein,
                             ingested.tobacco_class_nm   AS classname,
                             TO_CHAR (ingested.fiscal_qtr) AS quarterx,
                               SUM (NVL (reports.taxes_paid, 0))
                             - NVL (ingested.cbp_taxes_paid, 0)
                                 AS tufa_detail_delta,
                             --                    Changes for Story CTPTUFA-5868: Removing Summary file logic
                             --                    SUM(nvl(reports.taxes_paid, 0)) - nvl(ingested.cbp_summary_taxes_paid, 0) AS tufa_summary_delta,
                             --                    nvl(ingested.cbp_summary_taxes_paid, 0) - nvl(ingested.cbp_taxes_paid, 0) AS summary_detail_delta,
                             NVL (reports.permit_type_cd, 'IMPT')
                                 AS permit_type_cd,
                             ingested.company_id         AS company_id,
                             ingested.fiscal_year        AS fiscal_yr,
                             cbpamnd.acceptance_flag     AS acceptance_flag,
                             cbpamnd.in_progress_flag    AS in_progress_flag,
                             CASE
                                 WHEN     cbpamnd.fda_delta IS NOT NULL
                                      AND (   ABS (
                                                    cbpamnd.fda_delta
                                                  - SUM (
                                                        NVL (
                                                            reports.taxes_paid,
                                                            0))
                                                  + NVL (
                                                        ingested.cbp_taxes_paid,
                                                        0)) > 0
                                           OR       SUM (
                                                        NVL (
                                                            reports.taxes_paid,
                                                            0))
                                                  - NVL (
                                                        ingested.cbp_taxes_paid,
                                                        0) = 0
                                              AND cbpamnd.fda_delta != 0)
                                 THEN
                                     'Y'
                                 WHEN     cbpamnd.fda_delta IS NULL
                                      AND cbpamnd.acceptance_flag IN
                                              --4046 Kyle - Ingestedaccepted
                                               ('FDAaccepted',
                                                'Ingestedaccepted',
                                                'Amended')
                                 THEN
                                     'I'
                                 ELSE
                                     'N'
                             END
                                 AS delta_change,
                             cbpamnd.fda_delta           AS fda_delta,
                             reports.permit_num
                        FROM (  SELECT cbpingestedfinal.legal_nm,
                                       cbpingestedfinal.ein,
                                       SUM (
                                           NVL (cbpingestedfinal.cbp_taxes_paid,
                                                0))
                                           AS cbp_taxes_paid,
                                       --                            Changes for Story CTPTUFA-5868: Removing Summary file logic
                                       --                            SUM(nvl(cbpingestedfinal.cbp_summary_taxes_paid, 0)) AS cbp_summary_taxes_paid,
                                       cbpingestedfinal.fiscal_qtr,
                                       cbpingestedfinal.fiscal_year,
                                       cbpingestedfinal.tobacco_class_nm,
                                       cbpingestedfinal.tobacco_class_id,
                                       cbpingestedfinal.company_id
                                  FROM (  SELECT c1.legal_nm,
                                                 c1.ein,
                                                 SUM (
                                                     NVL (
                                                         cbpingested.cbp_taxes_paid,
                                                         0))
                                                     AS cbp_taxes_paid,
                                                 --                                    Changes for Story CTPTUFA-5868: Removing Summary file logic
                                                 --                                    cbpingested.cbp_summary_taxes_paid,
                                                 DECODE (
                                                     cbpingested.tobacco_class_nm,
                                                     'Cigars', '1-4',
                                                     cbpingested.fiscal_qtr)
                                                     AS fiscal_qtr,
                                                 cbpingested.fiscal_year,
                                                 cbpingested.tobacco_class_nm,
                                                 cbpingested.tobacco_class_id,
                                                 c1.company_id
                                            --                                    Changes for Story CTPTUFA-5868: Removing Summary file logic
                                            --                                    ,
                                            --                                    cbpingested.summary_entry_summary_number
                                            FROM tu_annual_trueup a1,
                                                 (  SELECT cbpdetail.importer_ein,
                                                           cbpdetail.consignee_ein,
                                                           SUM (
                                                               NVL (
                                                                   cbpentry.estimated_tax,
                                                                   0))
                                                               AS cbp_taxes_paid,
                                                           ttc.tobacco_class_nm,
                                                           CASE
                                                               WHEN ttc.tobacco_class_nm <>
                                                                        'Cigars'
                                                               THEN
                                                                   TO_CHAR (
                                                                       cbpentry.fiscal_qtr)
                                                               ELSE
                                                                   '1-4'
                                                           END
                                                               AS fiscal_qtr,
                                                           cbpentry.fiscal_year,
                                                           cbpdetail.association_type_cd,
                                                           ttc.tobacco_class_id
                                                               AS tobacco_class_id,
                                                           cbpdetail.consignee_exists_flag
                                                      --                                            Changes for Story CTPTUFA-5868: Removing Summary file logic
                                                      --                                            ,
                                                      --                                            nvl(cbpentry.summarized_file_tax, 0) AS cbp_summary_taxes_paid,
                                                      --                                            cbpentry.summary_entry_summary_number
                                                      FROM tu_cbp_importer cbpdetail,
                                                           tu_cbp_entry cbpentry,
                                                           tu_tobacco_class ttc
                                                     WHERE     (   cbpentry.excluded_flag
                                                                       IS NULL
                                                                OR cbpentry.excluded_flag <>
                                                                       'Y')
                                                           AND cbpentry.tobacco_class_id =
                                                                   ttc.tobacco_class_id
                                                           AND (   ttc.tobacco_class_id <>
                                                                       14
                                                                OR ttc.tobacco_class_nm <>
                                                                       'Non-Taxable')
                                                           AND cbpdetail.cbp_importer_id =
                                                                   cbpentry.cbp_importer_id
                                                           AND cbpdetail.cbp_importer_id =
                                                                   cbpentry.cbp_importer_id
                                                  GROUP BY cbpdetail.importer_ein,
                                                           cbpdetail.consignee_ein,
                                                           ttc.tobacco_class_nm,
                                                           cbpdetail.consignee_exists_flag,
                                                           CASE
                                                               WHEN ttc.tobacco_class_nm <>
                                                                        'Cigars'
                                                               THEN
                                                                   TO_CHAR (
                                                                       cbpentry.fiscal_qtr)
                                                               ELSE
                                                                   '1-4'
                                                           END,
                                                           cbpentry.fiscal_year,
                                                           cbpdetail.association_type_cd,
                                                           ttc.tobacco_class_id --                                            Changes for Story CTPTUFA-5868: Removing Summary file logic
                               --                                            ,
 --                                            nvl(cbpentry.summarized_file_tax, 0),
 --                                            cbpentry.summary_entry_summary_number
                                                 ) cbpingested,
                                                 tu_company  c1
                                           WHERE     (    c1.ein =
                                                              DECODE (
                                                                  cbpingested.association_type_cd,
                                                                  'IMPT', cbpingested.importer_ein,
                                                                  cbpingested.consignee_ein)
                                                      AND DECODE (
                                                              cbpingested.association_type_cd,
                                                              NULL, cbpingested.consignee_exists_flag,
                                                              'N') = 'N')
                                                 -- Change for CTPTUFA-5597: To always show matched deltas where the ingested tax is $0.00 on matched grid
                                                 -- AND cbpingested.cbp_taxes_paid != 0
                                                 AND cbpingested.fiscal_year =
                                                         a1.fiscal_yr
                                        GROUP BY c1.legal_nm,
                                                 c1.ein,
                                                 --                                    Changes for Story CTPTUFA-5868: Removing Summary file logic
                                                 --                                    cbpingested.cbp_summary_taxes_paid,
                                                 DECODE (
                                                     cbpingested.tobacco_class_nm,
                                                     'Cigars', '1-4',
                                                     cbpingested.fiscal_qtr),
                                                 cbpingested.fiscal_year,
                                                 cbpingested.tobacco_class_nm,
                                                 cbpingested.tobacco_class_id,
                                                 c1.company_id --                                    Changes for Story CTPTUFA-5868: Removing Summary file logic
                                       --                                    ,
 --                                    nvl(cbpingested.cbp_summary_taxes_paid, 0),
 --                                    cbpingested.summary_entry_summary_number
                                       ) cbpingestedfinal
                              GROUP BY cbpingestedfinal.legal_nm,
                                       cbpingestedfinal.ein,
                                       cbpingestedfinal.fiscal_qtr,
                                       cbpingestedfinal.fiscal_year,
                                       cbpingestedfinal.tobacco_class_nm,
                                       cbpingestedfinal.tobacco_class_id,
                                       cbpingestedfinal.company_id) ingested
                             INNER JOIN
                             (  SELECT /*+ USE_NL(TC1,FD,SF,PS,A2,RP) ORDERED */
                                      SUM (NVL (fd.taxes_paid, 0)) AS taxes_paid,
                                       tc1.tobacco_class_nm,
                                       NVL (sf.form_type_cd, '3852')
                                           AS form_type_cd,
                                       rp.fiscal_yr,
                                       CASE
                                           WHEN tc1.tobacco_class_nm IS NULL
                                           THEN
                                               TO_CHAR (rp.quarter)
                                           WHEN tc1.tobacco_class_nm <> 'Cigars'
                                           THEN
                                               TO_CHAR (rp.quarter)
                                           ELSE
                                               '1-4'
                                       END
                                           AS quarter,
                                       p.permit_type_cd,
                                       c2.ein,
                                       c2.company_id,
                                       pp.zero_flag,
                                       (SELECT DISTINCT
                                               LISTAGG (
                                                   a.permit_num,
                                                   ';')
                                               WITHIN GROUP (ORDER BY
                                                                 a.created_dt)
                                               OVER (PARTITION BY a.company_id)
                                          FROM tu_permit a
                                         WHERE a.company_id = c2.company_id)
                                           permit_num
                                  FROM tu_company c2
                                       INNER JOIN tu_permit p
                                           ON c2.company_id = p.company_id
                                       INNER JOIN tu_permit_period pp
                                           ON p.permit_id = pp.permit_id
                                       INNER JOIN tu_rpt_period rp
                                           ON rp.period_id = pp.period_id
                                       INNER JOIN tu_annual_trueup a2
                                           ON a2.fiscal_yr = rp.fiscal_yr
                                       INNER JOIN tu_period_status ps
                                           ON     pp.period_id = ps.period_id
                                              AND pp.permit_id = ps.permit_id
                                       LEFT JOIN tu_submitted_form sf
                                           ON     sf.permit_id = pp.permit_id
                                              AND sf.period_id = pp.period_id
                                       LEFT JOIN tu_form_detail fd
                                           ON sf.form_id = fd.form_id
                                       LEFT JOIN tu_tobacco_class tc1
                                           ON fd.tobacco_class_id =
                                                  tc1.tobacco_class_id
                              GROUP BY tc1.tobacco_class_nm,
                                       NVL (sf.form_type_cd, '3852'),
                                       rp.fiscal_yr,
                                       CASE
                                           WHEN tc1.tobacco_class_nm IS NULL
                                           THEN
                                               TO_CHAR (rp.quarter)
                                           WHEN tc1.tobacco_class_nm <>
                                                    'Cigars'
                                           THEN
                                               TO_CHAR (rp.quarter)
                                           ELSE
                                               '1-4'
                                       END,
                                       p.permit_type_cd,
                                       c2.ein,
                                       c2.company_id,
                                       pp.zero_flag) reports
                                 ON     (   reports.quarter =
                                                ingested.fiscal_qtr
                                         OR (    ingested.tobacco_class_nm =
                                                     'Cigars'
                                             AND reports.zero_flag = 'Y'))
                                    AND reports.fiscal_yr =
                                            ingested.fiscal_year
                                    AND ingested.company_id =
                                            reports.company_id
                                    AND (   reports.tobacco_class_nm =
                                                ingested.tobacco_class_nm
                                         OR reports.zero_flag = 'Y')
                                    AND reports.permit_type_cd = 'IMPT'
                                    AND reports.form_type_cd = '3852'
                             LEFT OUTER JOIN
                             (SELECT UNIQUE
                                     camnd.cbp_company_id AS cmpy_id,
                                     DECODE (camnd.qtr, 5, '1-4', camnd.qtr)
                                         AS qtr,
                                     DECODE (camnd.acceptance_flag,
                                             'Y', 'FDAaccepted',
                                             --4046 Kyle - Ingestedaccepted
                                             'I', 'Ingestedaccepted',
                                             'N', 'Amended',
                                             'X', 'DeltaChange',
                                             'E', 'ExcludeChange',
                                             'noAction')
                                         acceptance_flag,
                                     camnd.in_progress_flag,
                                     camnd.fiscal_yr,
                                     tc2.tobacco_class_id AS tobacco_class_id,
                                     camnd.fda_delta
                                FROM tu_cbp_amendment camnd,
                                     tu_tobacco_class tc2
                               WHERE tc2.tobacco_class_id =
                                         camnd.tobacco_class_id) cbpamnd
                                 ON     cbpamnd.cmpy_id = ingested.company_id
                                    AND cbpamnd.fiscal_yr =
                                            ingested.fiscal_year
                                    AND TO_CHAR (cbpamnd.qtr) =
                                            ingested.fiscal_qtr
                                    AND cbpamnd.tobacco_class_id =
                                            ingested.tobacco_class_id
                       WHERE ------------------------------------Changes for Story#4946 and Defect#5805-----------------------------
 --------------------------------- Changes for Non Cigar Classes ------------------------------
                             (   (    ingested.tobacco_class_id <> 8
                                  AND ingested.company_id NOT IN
                                          (SELECT DISTINCT company_id
                                             FROM permit_excl_status_vw vw
                                            WHERE    ( -- Kyle: Number error throwing
                                                      assmnt_yr =
                                                              DECODE (
                                                                  ingested.fiscal_qtr,
                                                                  '4',   ingested.fiscal_year
                                                                       + 1,
                                                                  ingested.fiscal_year)
                                                      AND assmnt_qtr =
                                                              DECODE (
                                                                  ingested.fiscal_qtr,
                                                                  '1-4', 5,
                                                                  '4', 1,
                                                                    ingested.fiscal_qtr
                                                                  + 1)
                                                      AND excl_scope_id = 2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                                      AND vw.company_id NOT IN
                                                              (SELECT tcam.cbp_company_id
                                                                 FROM tu_cbp_amendment
                                                                      tcam
                                                                WHERE     vw.assmnt_qtr =
                                                                              DECODE (
                                                                                  tcam.qtr,
                                                                                  4, 1,
                                                                                    tcam.qtr
                                                                                  + 1)
                                                                      AND vw.assmnt_yr =
                                                                              DECODE (
                                                                                  tcam.qtr,
                                                                                  4,   tcam.fiscal_yr
                                                                                     + 1,
                                                                                  tcam.fiscal_yr)
                                                                      AND tcam.tobacco_class_id =
                                                                              ingested.tobacco_class_id))
                                                  ------------------------Added below Conditions for full exclusion----------------------------
                                                  OR (    excl_scope_id = 1
                                                      AND DECODE (
                                                              vw.assmnt_qtr,
                                                              1,   vw.assmnt_yr
                                                                 - 1,
                                                              vw.assmnt_yr) =
                                                              ingested.fiscal_year
                                                      -- Kyle: Number error throwing
                                                      AND DECODE (
                                                              vw.assmnt_qtr,
                                                              1, 4,
                                                              vw.assmnt_qtr - 1) =
                                                              DECODE (
                                                                  ingested.fiscal_qtr,
                                                                  '1-4', 5,
                                                                  ingested.fiscal_qtr)
                                                      AND vw.company_id NOT IN
                                                              (SELECT tcam1.cbp_company_id
                                                                 FROM tu_cbp_amendment
                                                                      tcam1
                                                                WHERE     vw.assmnt_qtr =
                                                                              DECODE (
                                                                                  tcam1.qtr,
                                                                                  4, 1,
                                                                                    tcam1.qtr
                                                                                  + 1)
                                                                      AND vw.assmnt_yr =
                                                                              DECODE (
                                                                                  tcam1.qtr,
                                                                                  4,   tcam1.fiscal_yr
                                                                                     + 1,
                                                                                  tcam1.fiscal_yr)
                                                                      AND tcam1.tobacco_class_id =
                                                                              ingested.tobacco_class_id --                                    OR ( vw.assmnt_qtr <= ( decode(tcam1.qtr, 4, 1, tcam1.qtr + 1) )
 --                                         AND vw.assmnt_yr = decode(tcam1.qtr, 4, tcam1.fiscal_yr + 1, tcam1.fiscal_yr)
 --                                         AND tcam1.tobacco_class_id = ingested.tobacco_class_id )
                                                              ))))
                              --------------------------Chnages for Cigar Class-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                              OR ( (    ingested.tobacco_class_id = 8
                                    AND ingested.company_id NOT IN
                                            (SELECT DISTINCT vw.company_id
                                               FROM permit_excl_status_vw vw
                                              WHERE     (       

                                                             ( vw.excl_scope_id =1 AND (    vw.assmnt_qtr =
                                                                             1
                                                                     AND vw.assmnt_yr =
                                                                               ingested.fiscal_year
                                                                             + 1)
                                                                 OR ( (    vw.excl_scope_id =
                                                                               1
                                                                       AND vw.assmnt_qtr !=
                                                                               1
                                                                       AND vw.assmnt_yr =
                                                                                 ingested.fiscal_year
                                                                               + 1
                                                                       AND(select count(*) from tu_permit where company_id=ingested.company_id)*
                                                                       (vw.assmnt_qtr
                                                                           - 1) =
                                                                               (SELECT COUNT (
                                                                                           *)
                                                                                  FROM permit_excl_status_vw
                                                                                       vw1
                                                                                 WHERE     vw1.excl_scope_id =
                                                                                               2
                                                                                       AND vw1.assmnt_yr =
                                                                                                 ingested.fiscal_year
                                                                                               + 1
                                                                                       AND ingested.ein =
                                                                                               vw1.ein))))
                                                         OR (    vw.excl_scope_id =
                                                                     2
                                                             AND vw.assmnt_yr =
                                                                       ingested.fiscal_year
                                                                     + 1
                                                             AND (select count(*) from tu_permit where company_id=ingested.company_id)*4 =
                                                                     (SELECT COUNT (
                                                                                 *)
                                                                        FROM permit_excl_status_vw
                                                                             vw1
                                                                       WHERE     vw1.excl_scope_id =
                                                                                     2
                                                                             AND vw1.assmnt_yr =
                                                                                       ingested.fiscal_year
                                                                                     + 1
                                                                             AND vw1.ein =
                                                                                     ingested.ein)))
                                                    AND (vw.company_id NOT IN
                                                             (SELECT tcbpam.cbp_company_id
                                                                FROM tu_cbp_amendment
                                                                     tcbpam
                                                               WHERE       tcbpam.fiscal_yr
                                                                         + 1 =
                                                                             vw.assmnt_yr
                                                                     AND tcbpam.qtr =
                                                                             5)))) --OR
                                                    -- fd.tobacco_class_id = 8
                                 ))
                    GROUP BY ingested.legal_nm,
                             ingested.tobacco_class_nm,
                             TO_CHAR (ingested.fiscal_qtr),
                             NVL (reports.permit_type_cd, 'IMPT'),
                             ingested.company_id,
                             ingested.fiscal_year,
                             cbpamnd.acceptance_flag,
                             cbpamnd.in_progress_flag,
                             ingested.cbp_taxes_paid,
                             ingested.ein,
                             cbpamnd.fda_delta,
                             reports.permit_num --                    Changes for Story CTPTUFA-5868: Removing Summary file logic
                                                       --                    ,
                         --                    ingested.cbp_summary_taxes_paid
                   ))
     WHERE   --    Changes for Story CTPTUFA-5868: Removing Summary file logic
                                        --    ( deltaexcisetax_flag = 'Review'
                                                                    --      OR
              (deltaexcisetax_flag = 'Value' AND ABS (delta) > 0.99)
           --     )
           OR delta_change = 'Y'
           OR acceptance_flag = 'DeltaChange'
;

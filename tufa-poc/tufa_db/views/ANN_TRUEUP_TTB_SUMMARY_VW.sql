--------------------------------------------------------
--  DDL for View ANN_TRUEUP_TTB_SUMMARY_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ANN_TRUEUP_TTB_SUMMARY_VW" ("FISCAL_YR", "QUARTER", "COMPANY_ID", "PERMIT_NUM", "CHEW", "SNUFF", "INGESTED_CHEW_SNUFF", "PIPE", "ROLL-YOUR-OWN", "INGESTED_PIPE_RYO", "CIGARETTES", "INGESTED_CIGARETTES") AS 
  (SELECT --REPORTS.FISCAL_YR,
          -- REPORTS.QUARTER,
           NVL(REPORTS.FISCAL_YR,INGESTED.FISCAL_YR) as "FISCAL_YR",
           NVL(REPORTS.QUARTER,INGESTED.QUARTER) as "QUARTER",
           NVL(REPORTS.COMPANY_ID,INGESTED.company_id) as "COMPANY_ID",
           NVL(REPORTS.permit_num,INGESTED.permit_num) as "PERMIT_NUM",
           REPORTS.CHEW AS CHEW,
           REPORTS.SNUFF AS SNUFF,
           INGESTED."Chew-and-Snuff" AS INGESTED_CHEW_SNUFF,
           REPORTS.PIPE AS PIPE,
           REPORTS."Roll-Your-Own" AS "ROLL-YOUR-OWN",
           INGESTED."Pipe-RYO" AS INGESTED_PIPE_RYO,
           REPORTS.Cigarettes AS CIGARETTES,
           INGESTED.Cigarettes AS INGESTED_CIGARETTES
      FROM (                                          -- get TTB ingested data
            (SELECT TTBINGESTED.QUARTER,
                    C.EIN,
                    C.COMPANY_ID,
                    A.FISCAL_YR,
                    P.PERMIT_NUM,
                    "Chew-and-Snuff",
                    "Pipe-RYO",
                    Cigarettes
               FROM TU_ANNUAL_TRUEUP A
                    INNER JOIN
                    (SELECT EIN_NUM,
                            FISCAL_YR,
                            QUARTER,
                            PERMIT_NUM,
                            "Chew-and-Snuff",
                            "Pipe-RYO",
                            Cigarettes
                       FROM (  SELECT TTBCMPY.EIN_NUM,
                                      TTC.TOBACCO_CLASS_NM AS TOBACCO_CLASS_NM,
                                      TTBCMPY.FISCAL_YR,
                                      TTBANNUALTX.TTB_CALENDAR_QTR AS QUARTER,
                                      TTBPERMIT.PERMIT_NUM,
                                      SUM (NVL (TTB_TAXES_PAID, 0))
                                         AS TTB_TAXES_PAID
                                 FROM TU_TTB_COMPANY TTBCMPY
                                      INNER JOIN
                                      TU_TTB_PERMIT TTBPERMIT
                                         ON TTBPERMIT.TTB_COMPANY_ID =
                                               TTBCMPY.TTB_COMPANY_ID
                                      INNER JOIN
                                      TU_TTB_ANNUAL_TAX TTBANNUALTX
                                         ON TTBANNUALTX.TTB_PERMIT_ID =
                                               TTBPERMIT.TTB_PERMIT_ID
                                      INNER JOIN
                                      TU_TOBACCO_CLASS TTC
                                         ON TTBANNUALTX.TOBACCO_CLASS_ID =
                                               TTC.TOBACCO_CLASS_ID
                             GROUP BY TTBCMPY.EIN_NUM,
                                      TTC.TOBACCO_CLASS_NM,
                                      TTBCMPY.FISCAL_YR,
                                      TTBANNUALTX.TTB_CALENDAR_QTR,
                                      TTBPERMIT.PERMIT_NUM) PIVOT (SUM (
                                                                      NVL (
                                                                         TTB_TAXES_PAID,
                                                                         0))
                                                            FOR (
                                                               TOBACCO_CLASS_NM)
                                                            IN  ('Chew-and-Snuff' AS "Chew-and-Snuff",
                                                                'Pipe-RYO' AS "Pipe-RYO",
                                                                'Cigarettes' AS Cigarettes))) TTBINGESTED
                       ON TTBINGESTED.FISCAL_YR = A.FISCAL_YR
                    INNER JOIN TU_COMPANY C ON TTBINGESTED.EIN_NUM = C.EIN
                    INNER JOIN TU_PERMIT P ON C.COMPANY_ID = P.COMPANY_ID
              WHERE P.PERMIT_NUM = TTBINGESTED.PERMIT_NUM) INGESTED
            -- join with reports if exist
            FULL OUTER JOIN
            (Select Permit_num,sum(Chew) as Chew,Sum(Snuff) as Snuff,Sum("Roll-Your-Own") as "Roll-Your-Own",
            Sum(Pipe) as Pipe,Sum(Cigarettes) as Cigarettes,
                    QUARTER,
                    EIN,
                    COMPANY_ID,
                    FISCAL_YR,
                    PERMIT_TYPE_CD,
                    FORM_TYPE_CD from
            ((SELECT PERMIT_NUM,
                    Decode(zero_flag,'Y',0,Chew) as chew,
                    Decode(zero_flag,'Y',0,Snuff) as Snuff,
                    Decode(zero_flag,'Y',0,"Roll-Your-Own") as "Roll-Your-Own" ,
                    Decode(zero_flag,'Y',0,Pipe) as Pipe,
                    Decode(zero_flag,'Y',0,Cigarettes) as Cigarettes,
                    QUARTER,
                    EIN,
                    COMPANY_ID,
                    FISCAL_YR,
                    PERMIT_TYPE_CD,
                    FORM_TYPE_CD
               FROM (  SELECT SUM (NVL (FD.TAXES_PAID, 0)) AS FDA_TAXES_PAID,
                              TC.TOBACCO_CLASS_NM AS TOBACCO_CLASS_NM,
                              NVL (SF.FORM_TYPE_CD, '3852') AS FORM_TYPE_CD,
                              RP.FISCAL_YR,
                              RP.QUARTER AS QUARTER,
                              P.PERMIT_TYPE_CD,
                              P.PERMIT_ID,
                              P.PERMIT_NUM,
                              C.EIN,
                              C.COMPANY_ID,
                              pp.zero_flag as zero_flag
                         FROM TU_COMPANY C
                              INNER JOIN TU_PERMIT P
                                 ON C.COMPANY_ID = P.COMPANY_ID
                              INNER JOIN TU_PERMIT_PERIOD PP
                                 ON P.PERMIT_ID = PP.PERMIT_ID
                              INNER JOIN TU_RPT_PERIOD RP
                                 ON RP.PERIOD_ID = PP.PERIOD_ID
                              INNER JOIN TU_ANNUAL_TRUEUP A
                                 ON A.FISCAL_YR = RP.FISCAL_YR
                              INNER JOIN
                              TU_PERIOD_STATUS PS
                                 ON     PP.PERMIT_ID = PS.PERMIT_ID
                                    AND PP.PERIOD_ID = PS.PERIOD_ID
                              LEFT JOIN
                              TU_SUBMITTED_FORM SF
                                 ON     SF.PERMIT_ID = PP.PERMIT_ID
                                    AND SF.PERIOD_ID = PP.PERIOD_ID
                              LEFT JOIN TU_FORM_DETAIL FD
                                 ON SF.FORM_ID = FD.FORM_ID
                              LEFT JOIN TU_TOBACCO_CLASS TC
                                 ON FD.TOBACCO_CLASS_ID = TC.TOBACCO_CLASS_ID
                        WHERE        P.PERMIT_TYPE_CD = 'MANU'
                                 AND SF.FORM_TYPE_CD = '3852'
                              OR PP.zero_flag = 'Y'
                     GROUP BY TC.TOBACCO_CLASS_NM,
                              SF.FORM_TYPE_CD,
                              RP.FISCAL_YR,
                              RP.QUARTER,
                              P.PERMIT_TYPE_CD,
                              P.PERMIT_ID,
                              P.PERMIT_NUM,
                              C.EIN,
                              C.COMPANY_ID,
                              pp.zero_flag) PIVOT (SUM (
                                                      NVL (FDA_TAXES_PAID, 0))
                                            FOR (tobacco_class_nm)
                                            IN  ('Chew' AS Chew,
                                                'Snuff' AS Snuff,
                                                'Roll-Your-Own' AS "Roll-Your-Own",
                                                'Pipe' AS Pipe,
                                                'Cigarettes' AS Cigarettes)))
                    )
                    Where permit_type_cd='MANU'
                    Group by permit_num,
                    QUARTER,
                    EIN,
                    COMPANY_ID,
                    FISCAL_YR,
                    PERMIT_TYPE_CD,
                    FORM_TYPE_CD
             )                                   REPORTS
               ON     REPORTS.QUARTER = INGESTED.QUARTER
                  AND REPORTS.FISCAL_YR = INGESTED.FISCAL_YR
                  AND REPORTS.COMPANY_ID = INGESTED.COMPANY_ID
                  AND REPORTS.PERMIT_NUM = INGESTED.PERMIT_NUM))
;

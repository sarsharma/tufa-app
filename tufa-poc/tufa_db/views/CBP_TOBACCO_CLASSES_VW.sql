--------------------------------------------------------
--  DDL for View CBP_TOBACCO_CLASSES_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."CBP_TOBACCO_CLASSES_VW" ("CBP_IMPORTER_ID", "TOBACCO_CLASSES") AS 
  SELECT cbp_importer_id,
             LISTAGG (tobacco, ', ') WITHIN GROUP (ORDER BY tobacco)
                 AS tobacco_classes
        FROM (SELECT UNIQUE
                     ce.cbp_importer_id,
                     NVL (tc.tobacco_class_nm, 'Unknown') tobacco
                FROM TU_CBP_ENTRY ce
                     LEFT OUTER JOIN TU_TOBACCO_CLASS tc
                         ON ce.tobacco_class_id = tc.tobacco_class_id) classes
    GROUP BY cbp_importer_id
;

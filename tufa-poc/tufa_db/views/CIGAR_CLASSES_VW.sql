--------------------------------------------------------
--  DDL for View CIGAR_CLASSES_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."CIGAR_CLASSES_VW" ("TOBACCO_CLASS_ID") AS 
  SELECT tc1.tobacco_class_id
      FROM TU_TOBACCO_CLASS  tc1
           LEFT OUTER JOIN TU_TOBACCO_CLASS tc2
               ON tc1.parent_class_id = tc2.tobacco_class_id
     WHERE    tc1.tobacco_class_nm LIKE '%Cigars%'
           OR tc2.tobacco_class_nm LIKE '%Cigars%'
;

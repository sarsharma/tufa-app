--------------------------------------------------------
--  DDL for View CIGAR_ONLY_PERMITS_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."CIGAR_ONLY_PERMITS_VW" ("PERMIT_ID") AS 
  select distinct permit_id
                       from tu_submitted_form
                        where form_num = 8
                         and permit_id not in (select distinct permit_id
                                                from tu_submitted_form where form_num != 8 and form_num != 0)
;

--------------------------------------------------------
--  DDL for View CIGAR_ONLY_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."CIGAR_ONLY_VW" ("PERMIT_ID", "PERIOD_ID", "CIGAR_ONLY") AS 
  SELECT pp.permit_id, pp.period_id, 'CGR_'
      FROM TU_PERMIT_PERIOD pp, TU_SUBMITTED_FORM sf
     WHERE     pp.permit_id = sf.permit_id
           AND pp.period_id = sf.period_id
           AND sf.form_type_cd = '3852'
           AND sf.form_id IN
                   (SELECT cigar_forms.form_id
                      FROM (SELECT sf.form_id, fd.tobacco_class_id
                              FROM TU_SUBMITTED_FORM sf, TU_FORM_DETAIL fd
                             WHERE sf.form_id = fd.form_id
                            INTERSECT
                            SELECT sf.form_id, vw.tobacco_class_id
                              FROM TU_SUBMITTED_FORM sf, cigar_classes_vw vw)
                           cigar_forms
                    MINUS
                    SELECT non_cigar.form_id
                      FROM (SELECT sf.form_id, fd.tobacco_class_id
                              FROM TU_SUBMITTED_FORM sf, TU_FORM_DETAIL fd
                             WHERE sf.form_id = fd.form_id
                            INTERSECT
                            SELECT sf.form_id, vw.tobacco_class_id
                              FROM TU_SUBMITTED_FORM     sf,
                                   non_cigar_classes_vw  vw) non_cigar)
;

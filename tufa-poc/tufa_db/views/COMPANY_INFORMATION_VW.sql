--------------------------------------------------------
--  DDL for View COMPANY_INFORMATION_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."COMPANY_INFORMATION_VW" ("LEGAL_NM", "PERMIT_NUM", "YEAR", "MONTH", "PERIOD_STATUS_TYPE_CD", "RECON_STATUS_TYPE_CD", "FIRST_NM", "LAST_NM", "PHONE_NUM", "EMAIL_ADDRESS") AS 
  SELECT co.legal_nm,
       pe.permit_num,
       rp.year,
       rp.month,
       ps.period_status_type_cd,
       ps.recon_status_type_cd,
       cn.first_nm,
       cn.last_nm,
       cn.phone_num,
       cn.email_address
       FROM tu_contact cn,
            tu_period_status ps,
            tu_permit_period pp,
            tu_rpt_period rp,
            tu_permit pe,
            tu_company co
      WHERE     co.company_id = pe.company_id
            AND co.company_id = cn.company_id(+)
            AND pe.permit_id = pp.permit_id
            AND rp.PERIOD_ID = pp.PERIOD_ID
            AND pp.PERMIT_ID = ps.PERMIT_ID
            AND pp.PERIOD_ID = ps.period_id
            AND (   period_status_type_cd = 'NSTD'
                 OR period_status_type_cd = 'ERRO')
   ORDER BY 1, 2
;

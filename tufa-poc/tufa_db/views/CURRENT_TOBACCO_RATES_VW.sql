--------------------------------------------------------
--  DDL for View CURRENT_TOBACCO_RATES_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."CURRENT_TOBACCO_RATES_VW" ("TOBACCO_CLASS_ID", "PARENT_CLASS_ID", "TOBACCO_CLASS_NM", "CLASS_TYPE_CD", "TAX_RATE_ID", "TAX_RATE_EFFECTIVE_DATE", "TAX_RATE_END_DATE", "TAX_RATE", "CONVERSION_RATE") AS 
  SELECT tc.TOBACCO_CLASS_ID,
          tc.PARENT_CLASS_ID,
          tc.TOBACCO_CLASS_NM,
          tc.CLASS_TYPE_CD,
          tr.TAX_RATE_ID,
          tr.TAX_RATE_EFFECTIVE_DATE,
          tr.TAX_RATE_END_DATE,
          tr.TAX_RATE,
          tr.CONVERSION_RATE
     FROM TU_TOBACCO_CLASS tc, TU_TAX_RATE tr
    WHERE     tc.tobacco_class_id = tr.tobacco_class_id
          AND tr.TAX_RATE_END_DATE =
                 (SELECT MAX (TAX_RATE_END_DATE) FROM TU_TAX_RATE)
;

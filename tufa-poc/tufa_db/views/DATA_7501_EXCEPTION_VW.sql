--------------------------------------------------------
--  DDL for View DATA_7501_EXCEPTION_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."DATA_7501_EXCEPTION_VW" ("Company Name", "Permit Number", "Year", "Month", "Report Status", "Exception") AS 
  SELECT /*+ NO_INDEX(A) */ DISTINCT legal_nm AS "Company Name",
       permit_num AS "Permit Number",
       year AS "Year",
       month AS "Month",
       period_status_type_cd AS "Report Status",
       DECODE(SUM(taxes_paid), 0, '7501 Data Not Entered') AS "Exception"
  FROM tu_company a,
       tu_permit b,
       tu_rpt_period c,
       tu_permit_period d,
       tu_period_status e,
       tu_document f,
       tu_submitted_form g,
       tu_form_detail h
 WHERE a.company_id = b.company_id
   AND b.permit_id = d.permit_id
   AND c.period_id = d.period_id
   AND d.permit_id = E.PERMIT_ID
   AND d.period_id = e.period_id
   AND d.permit_id = f.permit_id
   AND D.PERIOD_ID = f.period_id
   AND d.permit_id = g.permit_id (+)
   AND D.PERIOD_ID = g.period_id (+)
   AND g.form_id = h.form_id (+)
   AND permit_type_cd = 'IMPT'
   AND period_status_type_cd = 'ERRO'
   AND form_type_cd = '7501'
   AND doc_file_nm = 'ATCH'
   AND SUBSTR(form_types, INSTR(form_types, '7', 1, 1), 4) = '7501'
 GROUP BY legal_nm, permit_num, year, month, period_status_type_cd
HAVING NVL(SUM(taxes_paid), 0) = 0
 ORDER BY legal_nm, permit_num, year, month
;

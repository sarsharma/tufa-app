--------------------------------------------------------
--  DDL for View ERRO_REPORT_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."ERRO_REPORT_VW" ("Company Name", "EIN", "Permit Number", "FISCAL_YR", "YEAR", "MONTH", "Contact Names", "Contact Phone Numbers", "Contact Emails", "Zero Flag", "Cigar Only", "MIX_CLASSTYPE", "Company Comments", "Permit Comments", "Monthly_report_comment", "7501 Exception", "Calculated Tax Variance error") AS 
  WITH /*frms
             AS (SELECT SF1.period_id,
                        SF1.permit_id,
                        fc.user_comment,
                        fc.comment_date
                   FROM tu_submitted_form SF1, tu_form_comment fc
                  WHERE SF1.form_id = fc.form_id),*/
        d7501
        AS
            (SELECT /*+ FULL(SF2) */
                    SF2.permit_id, SF2.period_id, sf2.form_id
               FROM tu_submitted_form SF2, tu_document doc
              WHERE     SF2.permit_id = doc.permit_id
                    AND SF2.period_id = doc.period_id
                    AND SF2.form_type_cd = '7501'
                    AND doc.doc_file_nm = 'ATCH'
                    AND SUBSTR (doc.form_types,
                                INSTR (doc.form_types,
                                       '7',
                                       1,
                                       1),
                                4) = '7501')
      SELECT DISTINCT
             legal_nm
                 AS "Company Name",
             SUBSTR (co.ein, 1, 2) || '-' || SUBSTR (co.ein, 3)
                 AS EIN,
                SUBSTR (PE.PERMIT_NUM, 1, 2)
             || '-'
             || SUBSTR (pe.permit_num, 3, 2)
             || '-'
             || SUBSTR (pe.permit_num, 5, LENGTH (pe.permit_num) - 4)
                 AS "Permit Number",
             rp.fiscal_yr,
             year,
             month,
             NVL (cn.AGGNAMES, ' ')
                 AS "Contact Names",
             cn.AGGPHONES
                 AS "Contact Phone Numbers",
             cn.AGGEMAILS
                 AS "Contact Emails",
             CASE WHEN pp.zero_flag = 'Y' THEN 'Y' ELSE 'N' END
                 AS "Zero Flag",
             CASE WHEN cov.cigar_only = 'CGR_' THEN 'Y' ELSE 'N' END
                 AS "Cigar Only",
             CASE
                 WHEN     (CASE
                               WHEN cov.cigar_only = 'CGR_' THEN 'Y'
                               ELSE 'N'
                           END) =
                          'N'
                      AND ((SELECT COUNT (DISTINCT tfd.tobacco_class_id)
                              FROM tu_submitted_form tsf, tu_form_detail tfd
                             WHERE     tsf.form_id = tfd.form_id
                                   AND tsf.period_id = pp.period_id
                                   AND tsf.permit_id = pp.permit_id
                                   AND tsf.form_type_cd = 3852) >
                           1)
                 THEN
                     'Y'
                 ELSE
                     'N'
             END
                 AS MIX_CLASSTYPE,
             --  CASE WHEN d7501.permit_id IS NOT NULL THEN 'Y' ELSE 'N' END
             --    AS "7501 Exception",
             co.company_comments
                 AS "Company Comments",
             pe.permit_comments
                 AS "Permit Comments",
                (Select decode(comment_1,null,'',comment_1||';') from (SELECT DISTINCT
                        LISTAGG ('Documentation - ' || (tzrc.USER_COMMENT),
                                 ';')
                        WITHIN GROUP (ORDER BY tzrc.comment_date DESC)
                        OVER (PARTITION BY tzrc.permit_id, tzrc.period_id) as comment_1
                   FROM tu_zero_reports_comments tzrc
                  WHERE     tzrc.permit_id = ps.permit_id
                        AND tzrc.period_id = ps.period_id))
             || (Select decode(comment_2,null,'',comment_2||';') from (SELECT DISTINCT
                        LISTAGG (form_type_cd
                            || ' - '
                            || user_comment,';')
                            WITHIN GROUP (ORDER BY comment_date DESC)
                        OVER (PARTITION BY permit_id, period_id) as comment_2
                   FROM (SELECT tsf.permit_id,
                                tsf.period_id,
                                tfc.form_id,
                                tfc.user_comment,tfc.comment_date,
                                tsf.form_type_cd
                           FROM tu_form_comment tfc, tu_submitted_form tsf
                          WHERE     tfc.form_id = tsf.form_id
                                 and form_type_cd=3852
                                AND tsf.permit_id = ps.permit_id
                                AND tsf.period_id = ps.period_id)
                                ))
                  || (Select decode(comment_3,null,'',comment_3||';') from
                  (SELECT DISTINCT
                        LISTAGG ('5210.5'
                            || ' - '
                            || user_comment,';')
                            WITHIN GROUP (ORDER BY comment_date DESC)
                        OVER (PARTITION BY permit_id, period_id) as comment_3
                   FROM (SELECT tsf.permit_id,
                                tsf.period_id,
                                tfc.form_id,
                                tfc.user_comment,tfc.comment_date,
                                tsf.form_type_cd
                           FROM tu_form_comment tfc, tu_submitted_form tsf
                          WHERE     tfc.form_id = tsf.form_id
                                 and form_type_cd=2105
                                AND tsf.permit_id = ps.permit_id
                                AND tsf.period_id = ps.period_id)
                                ))
                ||
                  (Select decode(comment_4,null,'',comment_4||';') from (SELECT DISTINCT
                        LISTAGG ('5000.24'
                            || ' - '
                            || user_comment,';')
                            WITHIN GROUP (ORDER BY comment_date DESC)
                        OVER (PARTITION BY permit_id, period_id) as comment_4
                   FROM (SELECT tsf.permit_id,
                                tsf.period_id,
                                tfc.form_id,
                                tfc.user_comment,tfc.comment_date,
                                tsf.form_type_cd
                           FROM tu_form_comment tfc, tu_submitted_form tsf
                          WHERE     tfc.form_id = tsf.form_id
                                 and form_type_cd=5024
                                AND tsf.permit_id = ps.permit_id
                                AND tsf.period_id = ps.period_id)
                                ))
           ||
             (Select decode(comment_5,null,'',comment_5||';') from     (SELECT DISTINCT
                        LISTAGG ('5220.6'
                            || ' - '
                            || user_comment,';')
                            WITHIN GROUP (ORDER BY comment_date DESC)
                        OVER (PARTITION BY permit_id, period_id) comment_5
                   FROM (SELECT tsf.permit_id,
                                tsf.period_id,
                                tfc.form_id,
                                tfc.user_comment,tfc.comment_date,
                                tsf.form_type_cd
                           FROM tu_form_comment tfc, tu_submitted_form tsf
                          WHERE     tfc.form_id = tsf.form_id
                                 and form_type_cd=2206
                                AND tsf.permit_id = ps.permit_id
                                AND tsf.period_id = ps.period_id)
                                ))
                ||
                  (Select decode(comment_6,null,'',comment_6||';') from (SELECT DISTINCT
                        LISTAGG ('7501'
                            || ' - '
                            || user_comment,';')
                            WITHIN GROUP (ORDER BY comment_date DESC)
                        OVER (PARTITION BY permit_id, period_id) comment_6
                   FROM (SELECT tsf.permit_id,
                                tsf.period_id,
                                tfc.form_id,
                                tfc.user_comment,tfc.comment_date,
                                tsf.form_type_cd
                           FROM tu_form_comment tfc, tu_submitted_form tsf
                          WHERE     tfc.form_id = tsf.form_id
                                 and form_type_cd=7501
                                AND tsf.permit_id = ps.permit_id
                                AND tsf.period_id = ps.period_id)
                                ))
                 AS "Monthly_report_comment"
                 ,
             DECODE (
                 (SELECT DISTINCT 'X'
                    FROM DATA_7501_EXCEPTION_VW
                   WHERE     "Company Name" = co.legal_nm
                         AND "Permit Number" = pe.PERMIT_NUM
                         AND "Year" = rp.Year
                         AND "Month" = rp.month),
                 'X', 'Y',
                 'N')
                 "7501 Exception",
             DECODE (
                 (SELECT DISTINCT 'X'
                    FROM tu_submitted_form tsf, tu_form_detail tfd
                   WHERE     tsf.form_id = tfd.form_id
                         AND tsf.period_id = pp.period_id
                         AND tsf.permit_id = pp.permit_id
                         AND activity_cd = 'ERRO'
                         AND tsf.form_type_cd = 7501
                         AND tfd.line_num = 0),
                 'X', 'N',
                 DECODE (
                     (SELECT DISTINCT 'X'
                        FROM tu_submitted_form tsf, tu_form_detail tfd
                       WHERE     tsf.form_id = tfd.form_id
                             AND tsf.period_id = pp.period_id
                             AND tsf.permit_id = pp.permit_id
                             AND CALC_TX_ACT_CD_7501 = 'ERRO'
                             AND tsf.form_type_cd = 7501
                             AND tfd.line_num = 0),
                     'X', 'Y',
                     'N'))
                 AS "Calculated Tax Variance error"
        FROM tu_company      co,
             tu_permit       pe,
             tu_rpt_period   rp,
             tu_permit_period pp,
             tu_period_status ps,
             (SELECT DISTINCT
                     company_id,
                     LISTAGG (
                         DECODE (first_nm || last_nm,
                                 NULL, '',
                                 first_nm || ' ' || last_nm),
                         ' ; ')
                     WITHIN GROUP (order by primary_Contact_Sequence ASC, created_Dt ASC, contact_Id ASC)
                     OVER (PARTITION BY COMPANY_ID)        AS AGGNAMES,
                     LISTAGG (
                         DECODE (
                             (   SUBSTR (TRIM (' ' FROM phone_num), 1, 3)
                              || '-'
                              || SUBSTR (TRIM (' ' FROM phone_num), 4, 3)
                              || '-'
                              || SUBSTR (phone_num, 7)),
                             '--', '',
                             (   SUBSTR (TRIM (' ' FROM phone_num), 1, 3)
                              || '-'
                              || SUBSTR (TRIM (' ' FROM phone_num), 4, 3)
                              || '-'
                              || SUBSTR (TRIM (' ' FROM phone_num), 7))),
                         ' ; ')
                     WITHIN GROUP (order by primary_Contact_Sequence ASC, created_Dt ASC, contact_Id ASC)
                     OVER (PARTITION BY COMPANY_ID)        AS AGGPHONES,
                     LISTAGG (email_address, ' ; ')
                         WITHIN GROUP (order by primary_Contact_Sequence ASC, created_Dt ASC, contact_Id ASC)
                         OVER (PARTITION BY COMPANY_ID)    AS AGGEMAILS
                FROM tu_contact) cn,
             cigar_only_vw   cov,
             --  frms,
             d7501
       WHERE     co.company_id = pe.company_id
             AND co.company_id = cn.company_id(+)
             AND pe.permit_id = pp.permit_id
             AND rp.PERIOD_ID = pp.PERIOD_ID
             AND ps.PERMIT_ID = pp.PERMIT_ID
             AND ps.period_id = pp.PERIOD_ID
             AND ps.PERMIT_ID = cov.PERMIT_ID(+)
             AND ps.period_id = cov.PERIOD_ID(+)
             --AND pp.PERIOD_ID = frms.period_id(+)
             --AND pp.permit_id = frms.permit_id(+)
             AND ps.period_status_type_cd = 'ERRO'
             AND pp.permit_id = d7501.permit_id(+)
             AND pp.period_id = d7501.period_id(+)
    ORDER BY 1,
             2,
             3,
             4
;

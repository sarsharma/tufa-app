--------------------------------------------------------
--  DDL for View EXPORT_2000_FORM_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."EXPORT_2000_FORM_VW" ("PERIOD_ID", "PERMIT_ID", "TOBACCO_CLASS_NM", "SMALL_REMOVAL_QTY", "LARGE_REMOVAL_QTY", "REMOVAL_QTY", "DOL_DIFFERENCE", "ACTIVITY_CD") AS 
  SELECT sf.period_id,
           sf.permit_id,
           tc.tobacco_class_nm,
           --     sub.tobacco_class_id   AS sub_tobacco_class_id,
           sub.small AS small_removal_qty,
           sub.large AS large_removal_qty,
           fd.removal_qty,
           fd.dol_difference,
           fd.activity_cd
      FROM tu_submitted_form  sf
           LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id
           LEFT JOIN tu_tobacco_class tc
               ON fd.tobacco_class_id = tc.tobacco_class_id
           LEFT JOIN tu_tax_rate tr
               ON tc.tobacco_class_id = tr.tobacco_class_id
           LEFT JOIN
           (SELECT *
              FROM (SELECT DISTINCT tc.parent_class_id,
                                    tc.tobacco_class_nm,
                                    sf.period_id,
                                    sf.permit_id,
                                    NVL (fd.removal_qty, 0) AS removal_qty
                      FROM tu_submitted_form  sf
                           LEFT JOIN tu_form_detail fd
                               ON sf.form_id = fd.form_id
                           LEFT JOIN tu_tobacco_class tc
                               ON fd.tobacco_class_id = tc.tobacco_class_id
                     WHERE              --            sf.permit_id =:permit_id
                                --            AND sf.period_id =:period_id AND
                               sf.form_type_cd IN (2206, 2105)
                           AND fd.tobacco_class_id IN (9,
                                                       10,
                                                       11,
                                                       12)) sub
                   PIVOT
                       (MIN (removal_qty)
                       FOR tobacco_class_nm
                       IN ('Small' small, 'Large' large))) sub
               ON     sub.parent_class_id = fd.tobacco_class_id
                  AND sub.permit_id = sf.permit_id
                  AND sub.period_id = sf.period_id
     WHERE                                      --    sf.permit_id =:permit_id
                                        --    AND sf.period_id =:period_id AND
           fd.tobacco_class_id <= 8 AND sf.form_type_cd IN (2206, 2105)
;

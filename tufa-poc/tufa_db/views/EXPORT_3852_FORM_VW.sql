--------------------------------------------------------
--  DDL for View EXPORT_3852_FORM_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."EXPORT_3852_FORM_VW" ("PERIOD_ID", "PERMIT_ID", "TOBACCO_CLASS_NM", "REMOVAL_QTY", "TAX_RATE", "TAXES_PAID", "DOL_DIFFERENCE", "CALC_TAX_RATE", "ACTIVITY_CD") AS 
  SELECT sf.period_id,
           sf.permit_id,
           tc.tobacco_class_nm,
           fd.removal_qty,
           DECODE (fd.tobacco_class_id, 7, tr.tax_rate / 1000, tr.tax_rate)
               AS tax_rate,
           fd.taxes_paid,
           DECODE (fd.tobacco_class_id, 8, NULL, fd.dol_difference)
               AS dol_difference,
           DECODE (fd.tobacco_class_id, 8, 'N/A', fd.calc_tax_rate)
               AS calc_tax_rate,
           DECODE (fd.tobacco_class_id, 8, NULL, fd.activity_cd)
               AS activity_cd
      FROM tu_submitted_form  sf
           LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id
           LEFT JOIN tu_tobacco_class tc
               ON fd.tobacco_class_id = tc.tobacco_class_id
           LEFT JOIN tu_tobacco_class p_tc
               ON tc.parent_class_id = p_tc.tobacco_class_id
           LEFT JOIN tu_tax_rate tr
               ON tc.tobacco_class_id = tr.tobacco_class_id
     WHERE sf.form_type_cd = '3852'
;

--------------------------------------------------------
--  DDL for View EXPORT_5000_FORM_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."EXPORT_5000_FORM_VW" ("PERMIT_ID", "PERIOD_ID", "TOBACCO_CLASS_NM", "TAXES_3852", "TAX_AMOUNT_BREAKDOWN", "TAXES_PAID", "DOL_DIFFERENCE", "ACTIVITY_CD") AS 
  SELECT sf.permit_id,
           sf.period_id,
           -- Chew-and-Snuff to Chew/Snuff and Pipe-RYO to Pipe/RYO
           DECODE (tc.tobacco_class_nm,
                   'Chew-and-Snuff', 'Chew/Snuff',
                   'Pipe-RYO', 'Pipe/RYO',
                   tc.tobacco_class_nm)
               AS "TOBACCO_CLASS_NM",
           f3852.taxes_paid AS "TAXES_3852",
           bd.breakdown     AS "TAX_AMOUNT_BREAKDOWN",
           fd.taxes_paid,
           fd.dol_difference,
           fd.activity_cd
      FROM tu_submitted_form  sf
           LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id
           LEFT JOIN tu_tobacco_class tc
               ON fd.tobacco_class_id = tc.tobacco_class_id
           LEFT JOIN tu_tobacco_class p_tc
               ON tc.parent_class_id = p_tc.tobacco_class_id
           LEFT JOIN tu_tax_rate tr
               ON tc.tobacco_class_id = tr.tobacco_class_id
           -- Get total 3852 data per tobacco type (Pipe/RYO, Chew/Snuff, Cigars, Cigarettes)
           LEFT JOIN
           (  SELECT sf.permit_id,
                     sf.period_id,
                     NVL (p_tc.parent_class_id, fd.tobacco_class_id)
                         AS tobacco_class_id,
                     SUM (taxes_paid) AS taxes_paid
                FROM tu_submitted_form sf
                     LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id
                     LEFT JOIN tu_tobacco_class p_tc
                         ON p_tc.tobacco_class_id = fd.tobacco_class_id
               WHERE sf.form_type_cd = '3852'
            GROUP BY sf.permit_id,
                     sf.period_id,
                     NVL (p_tc.parent_class_id, fd.tobacco_class_id)) f3852
               ON     f3852.permit_id = sf.permit_id
                  AND f3852.period_id = sf.period_id
                  AND fd.tobacco_class_id = f3852.tobacco_class_id
           -- Get the taxes amount for each line as a ; seperated list
           LEFT JOIN
           (SELECT DISTINCT
                   sf.permit_id,
                   sf.period_id,
                   fd.tobacco_class_id,
                   LISTAGG (
                       '$' || fd.taxes_paid,
                       '; ')
                   WITHIN GROUP (ORDER BY fd.line_num)
                   OVER (
                       PARTITION BY fd.tobacco_class_id,
                                    sf.permit_id,
                                    sf.period_id)
                       AS breakdown
              FROM tu_submitted_form  sf
                   LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id
             WHERE sf.form_type_cd = '5024' AND fd.line_num <> 0) bd
               ON     bd.permit_id = sf.permit_id
                  AND bd.period_id = sf.period_id
                  AND fd.tobacco_class_id = bd.tobacco_class_id
     WHERE sf.form_type_cd = '5024' AND fd.line_num = 0
;

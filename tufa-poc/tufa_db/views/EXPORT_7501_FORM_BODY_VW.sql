--------------------------------------------------------
--  DDL for View EXPORT_7501_FORM_BODY_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."EXPORT_7501_FORM_BODY_VW" ("PERIOD_ID", "PERMIT_ID", "FORM_NUM", "TOBACCO_CLASS_NM", "LINE_NUM", "SUB_TOBACCO_CLASS_NM", "REMOVAL_QTY", "TAXES_PAID", "CALC_TAX_RATE", "DOL_DIFFERENCE") AS 
  SELECT sf.period_id,
             sf.permit_id,
             sf.form_num,
             -- Cigars use the parent class here but others use the regular
             DECODE (sf.form_num,
                     '8', NVL (p_tc.tobacco_class_nm, tc.tobacco_class_nm),
                     tc.tobacco_class_nm)
                 AS tobacco_class_nm,
             fd.line_num,
             -- Only show the type id for cigars
             DECODE (
                 sf.form_num,
                 '8', DECODE (tc.tobacco_class_nm,
                              'Cigars', 'None',
                              tc.tobacco_class_nm),
                 NULL)
                 AS "SUB_TOBACCO_CLASS_NM",
             fd.removal_qty,
             fd.taxes_paid,
             DECODE (fd.tobacco_class_id, 13, 'N/A', fd.calc_tax_rate)
                 AS calc_tax_rate,
             ROUND (fd.dol_difference, 2) AS dol_difference
        FROM tu_submitted_form sf
             LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id
             LEFT JOIN tu_tobacco_class tc
                 ON fd.tobacco_class_id = tc.tobacco_class_id
             LEFT JOIN tu_tobacco_class p_tc
                 ON tc.parent_class_id = p_tc.tobacco_class_id
       WHERE sf.form_type_cd = '7501' AND fd.line_num <> 0
    ORDER BY                           -- Line order should match ui ..4,3,2..
            fd.line_num DESC
;

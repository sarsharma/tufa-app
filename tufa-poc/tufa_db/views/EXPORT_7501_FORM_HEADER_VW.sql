--------------------------------------------------------
--  DDL for View EXPORT_7501_FORM_HEADER_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."EXPORT_7501_FORM_HEADER_VW" ("PERIOD_ID", "PERMIT_ID", "FORM_NUM", "TOBACCO_CLASS_NM", "REMOVAL_3852", "REMOVAL_QTY", "DOL_REMOVAL_DIFFERENCE", "REMOVAL_ACTIVITY", "TAX_3852", "TAXES_PAID", "DOL_DIFFERENCE", "ACTIVITY_CD") AS 
  SELECT sf.period_id,
           sf.permit_id,
           sf.form_num,
           tc.tobacco_class_nm,
           f3852.removal_qty            AS "REMOVAL_3852",
           fd.removal_qty,
           fd.dol_removal_difference,
           fd.removal_activity,
           f3852.taxes_paid             AS "TAX_3852",
           fd.taxes_paid,
           ROUND (fd.dol_difference, 2) AS "DOL_DIFFERENCE",
           fd.activity_cd
      FROM tu_submitted_form  sf
           LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id
           LEFT JOIN
           (SELECT sf.permit_id,
                   sf.period_id,
                   tobacco_class_id,
                   removal_qty,
                   taxes_paid
              FROM tu_submitted_form  sf
                   LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id
             WHERE sf.form_type_cd = '3852') f3852
               ON     f3852.permit_id = sf.permit_id
                  AND f3852.period_id = sf.period_id
                  AND f3852.tobacco_class_id = fd.tobacco_class_id
           LEFT JOIN tu_tobacco_class tc
               ON fd.tobacco_class_id = tc.tobacco_class_id
           LEFT JOIN tu_tobacco_class p_tc
               ON tc.parent_class_id = p_tc.tobacco_class_id
           LEFT JOIN tu_tax_rate tr
               ON tc.tobacco_class_id = tr.tobacco_class_id
     WHERE sf.form_type_cd = '7501' AND fd.line_num = 0
;

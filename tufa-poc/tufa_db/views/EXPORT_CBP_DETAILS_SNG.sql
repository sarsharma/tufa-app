--------------------------------------------------------
--  DDL for View EXPORT_CBP_DETAILS_SNG
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."EXPORT_CBP_DETAILS_SNG" ("ENTRY_SUMMARY_NUMBER", "IMPORTER_NUMBER", "IMPORTER_NAME", "ULTIMATE_CONSIGNEE_NUMBER", "ULTIMATE_CONSIGNEE_NAME", "ENTRY_SUMMARY_DATE", "ENTRY_DATE", "ENTRY_TYPE_CODE", "LINE_NUMBER", "HTS_NUMBER", "HTS_DESCRIPTION", "LINE_TARIFF_QTY1", "LINE_TARIFF_GOODS_VALUE_AMOUNT", "IR_TAX_AMOUNT", "COMPANY_ID", "FISCAL_YEAR", "FISCAL_QTR", "TOBACCO_CLASS_NM", "IMPORT_DATE") AS 
  SELECT DISTINCT
--     cbpingested.summary_entry_summary_number,
     cbpdetails.entry_summary_number,
--     cbpingested.importer_ein,
     cbpdetails.importer_number,
--     cbpingested.importer_nm,
     cbpdetails.importer_name,
--     cbpingested.consignee_ein,
     cbpdetails.ultimate_consignee_number,
--     cbpingested.consignee_nm,
     cbpdetails.ultimate_consignee_name,
     TO_CHAR(TO_DATE(cbpdetails.entry_summary_date,'YYYYMMDD') ) entry_summary_date,
     TO_CHAR(TO_DATE(cbpdetails.entry_date,'YYYYMMDD') ) entry_date,
     cbpdetails.entry_type_code,
--     cbpingested.line_num,
     cbpdetails.line_number,
     cbpdetails.hts_number,
     cbpdetails.hts_description,
--     cbpingested.qty_removed,
     cbpdetails.line_tariff_qty1,
     cbpdetails.line_tariff_goods_value_amount,
     cbpdetails.ir_tax_amount,
     --Used for filtering
     c1.company_id,
     cbpingested.fiscal_year,
     cbpingested.fiscal_qtr,
     cbpingested.tobacco_class_nm,
     TO_CHAR(TO_DATE(cbpdetails.IMPORT_DATE,'YYYYMMDD')) as IMPORT_DATE
 FROM
     (
         SELECT
             cbpdetail.importer_ein,
             cbpdetail.importer_nm,
             cbpdetail.consignee_ein,
             cbpdetail.consignee_nm,
             cbpdetail.consignee_exists_flag,
             cbpentry.entry_summ_dt,
             cbpentry.entry_dt,
             cbpentry.estimated_tax         AS cbp_taxes_paid,
             cbpentry.qty_removed           AS cbp_qtyremoved,
             cbpentry.summarized_file_tax   AS summarized_tax,
             ttc.tobacco_class_nm,
             CASE
                 WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                 ELSE '5'
             END AS fiscal_qtr,
             cbpentry.fiscal_year,
             cbpentry.assigned_dt,
             cbpdetail.association_type_cd,
             ttc.tobacco_class_id           AS tobacco_class_id,
             cbpentry.entry_num,
             cbpentry.line_num,
             cbpentry.qty_removed
         FROM
             tu_cbp_importer cbpdetail,
             tu_cbp_entry cbpentry,
             tu_tobacco_class ttc
         WHERE
             ( cbpentry.excluded_flag IS NULL
               OR cbpentry.excluded_flag <> 'Y' )
             AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
             AND ( ttc.tobacco_class_id <> 14
                   OR ttc.tobacco_class_nm <> 'Non-Taxable' )
             AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
     ) cbpingested
     LEFT JOIN tu_company c1 ON ( cbpingested.importer_ein = c1.ein
                                  AND cbpingested.association_type_cd = 'IMPT'
                                  OR cbpingested.consignee_ein = c1.ein
                                  AND cbpingested.association_type_cd = 'CONS'
                                  OR ( cbpingested.consignee_ein = c1.ein
                                  AND cbpingested.association_type_cd IS NULL
                                       AND cbpingested.consignee_exists_flag = 'N' ) ),
     tu_cbp_details_file cbpdetails
 WHERE
     cbpingested.cbp_taxes_paid != 0
     AND cbpingested.entry_num = cbpdetails.entry_summary_number
     AND cbpingested.fiscal_year = cbpdetails.fiscal_year
     AND cbpingested.line_num = cbpdetails.line_number
;

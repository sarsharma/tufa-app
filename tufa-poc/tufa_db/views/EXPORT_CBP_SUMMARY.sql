--------------------------------------------------------
--  DDL for View EXPORT_CBP_SUMMARY
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."EXPORT_CBP_SUMMARY" ("ENTRY_SUMMARY_NUMBER", "IMPORTER_NUMBER", "IMPORTER_NAME", "ULTIMATE_CONSIGNEE_NUMBER", "ULTIMATE_CONSIGNEE_NAME", "ENTRY_SUMMARY_DATE", "ENTRY_DATE", "ENTRY_TYPE_CODE_AND_DESC", "ESTIMATED_TAX", "TOTAL_ASCERTAINED_TAX_AMOUNT", "COMPANY_ID", "FISCAL_YEAR", "FISCAL_QTR", "TOBACCO_CLASS_NM") AS 
  SELECT DISTINCT
--     cbpingested.summary_entry_summary_number,
     cbpsummary.entry_summary_number,
--     cbpingested.importer_ein,
     cbpsummary.importer_number,
--     cbpingested.importer_nm,
     cbpsummary.importer_name,
--     cbpingested.consignee_ein,
     cbpsummary.ultimate_consignee_number,
--     cbpingested.consignee_nm,
     cbpsummary.ultimate_consignee_name,
     TO_CHAR(TO_DATE(cbpsummary.entry_summary_date,'YYYYMMDD'),'MM/DD/YYYY') entry_summary_date,
     TO_CHAR(TO_DATE(cbpsummary.entry_date,'YYYYMMDD'),'MM/DD/YYYY') entry_date,
     cbpsummary.entry_type_code_and_desc,
--     cbpingested.summarized_tax,
     cbpsummary.estimated_tax,
     cbpsummary.total_ascertained_tax_amount,
     --Used for filtering
     c1.company_id,
     cbpingested.fiscal_year,
     cbpingested.fiscal_qtr,
     cbpingested.tobacco_class_nm
 FROM
     (
         SELECT
             cbpdetail.importer_ein,
             cbpdetail.importer_nm,
             cbpdetail.consignee_ein,
             cbpdetail.consignee_nm,
             cbpdetail.consignee_exists_flag,
             cbpentry.entry_summ_dt,
             cbpentry.entry_dt,
             cbpentry.estimated_tax         AS cbp_taxes_paid,
             cbpentry.qty_removed           AS cbp_qtyremoved,
             cbpentry.summarized_file_tax   AS summarized_tax,
             ttc.tobacco_class_nm,
             CASE
                 WHEN ttc.tobacco_class_nm <> 'Cigars' THEN TO_CHAR(cbpentry.fiscal_qtr)
                 ELSE '1-4'
             END AS fiscal_qtr,
             cbpentry.fiscal_year,
             cbpentry.assigned_dt,
             cbpdetail.association_type_cd,
             ttc.tobacco_class_id           AS tobacco_class_id,
             cbpentry.summary_entry_summary_number,
             cbpentry.line_num
         FROM
             tu_cbp_importer cbpdetail,
             tu_cbp_entry cbpentry,
             tu_tobacco_class ttc
         WHERE
             ( cbpentry.excluded_flag IS NULL
               OR cbpentry.excluded_flag <> 'Y' )
             AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
             AND ( ttc.tobacco_class_id <> 14
                   OR ttc.tobacco_class_nm <> 'Non-Taxable' )
             AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
     ) cbpingested
     LEFT JOIN tu_company c1 ON ( cbpingested.importer_ein = c1.ein
                                  AND cbpingested.association_type_cd = 'IMPT'
                                  OR cbpingested.consignee_ein = c1.ein
                                  AND cbpingested.association_type_cd = 'CONS'
                                  OR ( cbpingested.consignee_ein = c1.ein
                                  AND cbpingested.association_type_cd IS NULL
                                       AND cbpingested.consignee_exists_flag = 'N' ) ),
     tu_cbp_summary_file cbpsummary
 WHERE
     cbpingested.summary_entry_summary_number = cbpsummary.entry_summary_number
     AND cbpingested.fiscal_year = cbpsummary.fiscal_year;

   COMMENT ON TABLE "CTP_TUFA_MVP13"."EXPORT_CBP_SUMMARY"  IS 'View used to export data used for the summary grid from the Summary File'
;

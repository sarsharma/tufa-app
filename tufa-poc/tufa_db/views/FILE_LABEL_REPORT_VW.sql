--------------------------------------------------------
--  DDL for View FILE_LABEL_REPORT_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."FILE_LABEL_REPORT_VW" ("Company Name", "PERMIT_NUMBER", "Fiscal Year") AS 
  SELECT /*+ FULL(RP) FULL(PE) */ co.legal_nm AS "Company Name",
       SUBSTR(PE.PERMIT_NUM, 1, 2) || '-' || SUBSTR(pe.permit_num, 3, 2) || '-' || SUBSTR(pe.permit_num, 5, LENGTH(pe.permit_num) - 4) AS Permit_Number,
       rp.FISCAL_YR AS "Fiscal Year"
  FROM tu_rpt_period rp,
       tu_permit pe,
       tu_period_status ps,
       tu_company co,
       tu_permit_period pp
 WHERE RP.period_id = ps.period_id
   AND PS.PERMIT_ID = pe.permit_id
   AND pe.company_id = co.company_id
   AND PE.PERMIT_ID = pp.permit_id
   AND rp.period_id = PP.PERIOD_ID
   AND (pp.source_cd = 'FAXX'
         OR pp.source_cd = 'EMAI')
 GROUP BY co.legal_nm, PE.PERMIT_NUM, rp.FISCAL_YR
 ORDER BY 1, 2
;

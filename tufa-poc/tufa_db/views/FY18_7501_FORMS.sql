--------------------------------------------------------
--  DDL for View FY18_7501_FORMS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."FY18_7501_FORMS" ("PERMIT_ID", "PERIOD_ID", "FORM_ID", "TOBACCO_CLASS_ID", "TAX_RATE", "CONVERSION_RATE", "REMOVAL_QTY", "TAXES_PAID", "CALC_TAXES", "CONV_TAXES") AS 
  SELECT sf.permit_id,
            sf.period_id,
            sf.form_id,
            fd.tobacco_class_id,
            tr.tax_rate,
            tr.conversion_rate,
            fd.removal_qty,
            FD.TAXES_PAID,
            CASE
             WHEN fd.tobacco_class_id = 13
              THEN ROUND(fd.advalorem_dol * TR.TAX_RATE,2)
             ELSE
              ROUND((FD.REMOVAL_QTY * tr.tax_rate), 2)
            END AS calc_taxes,
            CASE
             WHEN fd.tobacco_class_id != 13
              THEN ROUND ((fd.removal_qty * tr.conversion_rate), 2)
            END AS conv_taxes
       FROM tu_submitted_form sf,
            tu_form_detail fd,
            tu_rpt_period rp,
            tu_tax_rate tr
      WHERE     sf.form_type_cd = 7501
            AND rp.fiscal_yr = 2018
            AND sf.period_id = rp.period_id
            AND fd.form_id = sf.form_id
            and fd.tobacco_class_id != 8
            AND tr.tobacco_class_id = fd.tobacco_class_id
            AND fd.line_num != 0
   ORDER BY 1, 2, 3, 4
;

--------------------------------------------------------
--  DDL for View INGESTED_ONLY_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."INGESTED_ONLY_VW" ("EIN", "LEGAL_NM", "ORIGINAL_COMPANY_NM", "ORIGINAL_EIN", "TAXES_PAID", "FISCAL_YR", "QUARTER", "TOBACCO_CLASS_NM", "COMPANY_TYPE", "ONLY_SIDE", "STATUS") AS 
  SELECT ingested.company_ein AS ein,
          ingested.company_nm AS legal_nm,
          ingested.original_company_nm AS original_company_nm,
          ingested.original_ein AS original_ein,
          0 - NVL (ingested.taxes_paid, 0) AS taxes_paid,
          ingested.fiscal_year AS fiscal_yr,
          ingested.fiscal_qtr AS quarter,
          ingested.tobacco_class_nm AS tobacco_class_nm,
          ingested.company_type AS company_type,
          'INGESTED' AS only_side,
          NVL (
             (SELECT tcads.status
                FROM tu_comparison_all_delta_sts tcads
               WHERE     tcads.ein =
                            NVL (ingested.original_ein, ingested.company_ein)
                     AND tcads.fiscal_yr = ingested.fiscal_year
                     AND tcads.fiscal_qtr = ingested.fiscal_qtr
                     AND tcads.tobacco_class_nm = ingested.tobacco_class_nm
                     AND tcads.permit_type = ingested.company_type),
             'Review')
             AS status
     FROM all_ingested_vw ingested
          LEFT JOIN
          (SELECT *
             FROM all_tufa_vw
            WHERE zero_flag = 'N') tufa
             ON     ingested.fiscal_year = tufa.fiscal_year
                AND ingested.fiscal_qtr = tufa.quarter
                AND ingested.tobacco_class_nm = tufa.tobacco_class_nm
                AND ingested.company_type = tufa.company_type
                AND NVL (ingested.original_ein, ingested.company_ein) =
                       tufa.ein
    WHERE tufa.ein IS NULL AND ABS (NVL (ingested.taxes_paid, 0)) >= 1
   INTERSECT
   SELECT ingested.company_ein AS ein,
          ingested.company_nm AS legal_nm,
          ingested.original_company_nm AS original_company_nm,
          ingested.original_ein AS original_ein,
          0 - NVL (ingested.taxes_paid, 0) AS taxes_paid,
          ingested.fiscal_year AS fiscal_yr,
          ingested.fiscal_qtr AS quarter,
          ingested.tobacco_class_nm AS tobacco_class_nm,
          ingested.company_type AS company_type,
          'INGESTED' AS only_side,
          NVL (
             (SELECT tcads.status
                FROM tu_comparison_all_delta_sts tcads
               WHERE     tcads.ein =
                            NVL (ingested.original_ein, ingested.company_ein)
                     AND tcads.fiscal_yr = ingested.fiscal_year
                     AND tcads.fiscal_qtr = ingested.fiscal_qtr
                     AND tcads.tobacco_class_nm = ingested.tobacco_class_nm
                     AND tcads.permit_type = ingested.company_type),
             'Review')
             AS status
     FROM all_ingested_vw ingested
          LEFT JOIN
          (SELECT *
             FROM all_tufa_vw
            WHERE zero_flag = 'Y') tufa
             ON     NVL (ingested.original_ein, ingested.company_ein) =
                       tufa.ein
                AND ingested.fiscal_year = tufa.fiscal_year
                AND ingested.fiscal_qtr =
                       DECODE (ingested.tobacco_class_nm,
                               'Cigars', '1-4',
                               tufa.quarter)
                AND ingested.company_type = tufa.company_type
    WHERE tufa.ein IS NULL AND ABS (NVL (ingested.taxes_paid, 0)) >= 1
;

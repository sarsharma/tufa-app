--------------------------------------------------------
--  DDL for View NSTD_REPORT_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."NSTD_REPORT_VW" ("Company Name", "EIN", "Permit Number", "FISCAL_YR", "YEAR", "MONTH", "Period Status", "Recon Status", "Contact Names", "Contact Phone Numbers", "Contact Emails", "Zero Flag", "Cigar Only", "Permit Comments", "Company Comments", "Report Attachment", "CIGAR_HISTORY", "PERMIT_ID", "PERIOD_ID", "PERMIT_STATUS", "TUFA_INACTIVE_DATE", "TTB_CLOSE_DATE", "ALL_NSTD_FLAG") AS 
  WITH frms
         AS (SELECT sf.period_id,
                    sf.permit_id,
                    fc.user_comment,
                    fc.comment_date
               FROM tu_submitted_form sf, tu_form_comment fc
              WHERE sf.form_id = fc.form_id)
      SELECT /*+ USE_MERGE(FRMS,COV,CN,PS) ORDERED */
            legal_nm                                          AS "Company Name",
            substr(co.ein,1,2)||'-'||substr(co.ein,3) as EIN,
                SUBSTR (PE.PERMIT_NUM, 1, 2)
             || '-'
             || SUBSTR (pe.permit_num, 3, 2)
             || '-'
             || SUBSTR (pe.permit_num, 5, LENGTH (pe.permit_num) - 4)
                 AS "Permit Number",
             rp.fiscal_yr,
             year,
             month,
             ps.period_status_type_cd                         AS "Period Status",
             recon_status_type_cd                             AS "Recon Status",
             NVL(cn.AGGNAMES,' ')                             AS "Contact Names",
             cn.AGGPHONES                                     AS "Contact Phone Numbers",
             cn.AGGEMAILS                                     AS "Contact Emails",
             CASE WHEN pp.zero_flag = 'Y' THEN 'Y' ELSE 'N' END AS "Zero Flag",
             CASE WHEN cov.cigar_only = 'CGR_' THEN 'Y' ELSE 'N' END
                 AS "Cigar Only",
             pe.permit_comments
                 AS "Permit Comments",
             co.company_comments
                 AS "Company Comments",
            Decode((Select count(1) from tu_document td where td.PERMIT_ID=pp.permit_id and td.period_id=pp.period_id ),0,'N','Y') as "Report Attachment"
            ,
            Decode((Select distinct 'X' from tu_submitted_form tsf,tu_rpt_period trp,tu_form_detail tfd
where tsf.PERMIT_ID=pe.permit_id
and tsf.period_id=trp.period_id
and ((trp.FISCAL_YR =rp.fiscal_yr) or (trp.FISCAL_YR =(rp.fiscal_yr-1)))
and tsf.form_id=tfd.form_id
and tobacco_class_id=8),'X','Y','N') as Cigar_history,
pe.permit_id,
ps.period_id,
pe.PERMIT_STATUS_TYPE_CD as Permit_status,
pe.CLOSE_DT Tufa_inactive_date,
pe.TTB_END_DT as TTB_CLOSE_DATE,
Decode ((Select distinct 'X' from tu_period_status tps where tps.PERMIT_ID = pp.PERMIT_ID and tps.period_status_type_cd <> 'NSTD'),'X','N','Y') ALL_NSTD_FLAG
        FROM tu_company      co,
             tu_permit       pe,
             tu_rpt_period   rp,
             tu_permit_period pp,
             tu_period_status ps,
             (SELECT DISTINCT
                     company_id,
                     LISTAGG ( Decode(first_nm||last_nm,null,'',first_nm || ' ' || last_nm), ' ; ' )
                         WITHIN GROUP (order by primary_Contact_Sequence ASC, created_Dt ASC, contact_Id ASC)
                         OVER (PARTITION BY COMPANY_ID)
                         AS AGGNAMES,
                     LISTAGG (decode ((substr(trim(' ' from phone_num),1,3)||'-'||substr(trim(' ' from  phone_num),4,3)||'-'||substr(phone_num,7)),'--','',
                     (substr(trim(' ' from phone_num),1,3)||'-'||substr(trim(' ' from phone_num),4,3)||'-'||substr(trim(' ' from phone_num),7))), ' ; ')
                         WITHIN GROUP (order by primary_Contact_Sequence ASC, created_Dt ASC, contact_Id ASC)
                         OVER (PARTITION BY COMPANY_ID)
                         AS AGGPHONES,
                     LISTAGG (email_address, ' ; ')
                         WITHIN GROUP (order by primary_Contact_Sequence ASC, created_Dt ASC, contact_Id ASC)
                         OVER (PARTITION BY COMPANY_ID)
                         AS AGGEMAILS
                FROM tu_contact) cn,
             cigar_only_vw   cov,
             frms
       WHERE     co.company_id = pe.company_id
             AND co.company_id = cn.company_id(+)
             AND pe.permit_id = pp.permit_id
             AND rp.PERIOD_ID = pp.PERIOD_ID
             AND ps.PERMIT_ID = pp.PERMIT_ID
             AND ps.period_id = pp.PERIOD_ID
             AND ps.PERMIT_ID = cov.PERMIT_ID(+)
             AND ps.period_id = cov.PERIOD_ID(+)
             AND pp.PERIOD_ID = frms.period_id(+)
             AND pp.permit_id = frms.permit_id(+)
             AND ps.period_status_type_cd = 'NSTD'
    ORDER BY 1,
             2,
             3,
             4
;

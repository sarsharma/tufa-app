--------------------------------------------------------
--  DDL for View PERIOD_START_DT
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PERIOD_START_DT" ("PERIOD_ID", "PERIOD_START_DT") AS 
  select period_id, TO_DATE(rp.month || rp.year,'MONYYYY') as period_start_dt
 from tu_rpt_period rp
;

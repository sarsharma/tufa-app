--------------------------------------------------------
--  DDL for View PERIOD_TAX_RATE_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PERIOD_TAX_RATE_VW" ("PERIOD_ID", "TOBACCO_CLASS_ID", "TAX_RATE_ID", "TOBACCO_CLASS_NM", "TAX_RATE", "CONVERSION_RATE", "PARENT_CLASS_ID", "CLASS_TYPE_CD", "TAX_RATE_EFFECTIVE_DATE", "TAX_RATE_END_DATE", "THRESHOLD_VALUE") AS 
  SELECT period_rate.period_id,
          tc.tobacco_class_id,
          period_rate.tax_rate_id,
          tc.tobacco_class_nm,
          period_rate.tax_rate,
          period_rate.conversion_rate,
          tc.parent_class_id,
          tc.class_type_cd,
          period_rate.tax_rate_effective_date,
          period_rate.tax_rate_end_date,
          tc.threshold_value
     FROM tu_tobacco_class tc
          LEFT OUTER JOIN
          (SELECT pv.period_id, tr.*
             FROM tu_tax_rate tr, period_vw pv
            WHERE TRUNC (pv.END_DATE) BETWEEN TRUNC (
                                                 NVL (
                                                    tr.TAX_RATE_EFFECTIVE_DATE,
                                                    '01-JAN-1900'))
                                          AND TRUNC (
                                                 NVL (tr.TAX_RATE_END_DATE,
                                                      '31-DEC-9999'))) period_rate
             ON tc.tobacco_class_id = period_rate.tobacco_class_id
;

--------------------------------------------------------
--  DDL for View PERMIT_3852_REPORTS_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PERMIT_3852_REPORTS_VW" ("Tobacco Class", "Company Name", "Permit Number", "EIN", "Year") AS 
  SELECT DISTINCT
         i.tobacco_class_nm AS "Tobacco Class",
         legal_nm AS "Company Name",
            SUBSTR (b.PERMIT_NUM, 1, 2)
         || '-'
         || SUBSTR (b.permit_num, 3, 2)
         || '-'
         || SUBSTR (b.permit_num, 5, LENGTH (b.permit_num) - 4)
            AS "Permit Number",
         ein AS "EIN",
         year AS "Year"
    FROM tu_company a,
         tu_permit b,
         tu_rpt_period c,
         tu_permit_period d,
         tu_period_status e,
         tu_document f,
         tu_submitted_form g,
         tu_form_detail h,
         tu_tobacco_class i
   WHERE     a.company_id = b.company_id
         AND b.permit_id = d.permit_id
         AND c.period_id = d.period_id
         AND d.permit_id = E.PERMIT_ID
         AND d.period_id = e.period_id
         AND d.permit_id = f.permit_id
         AND D.PERIOD_ID = f.period_id
         AND C.YeaR = 2017
         AND d.permit_id = g.permit_id(+)
         AND D.PERIOD_ID = g.period_id(+)
         AND g.form_id = h.form_id(+)
         AND g.form_type_cd = '3852'
         AND h.removal_qty > 0
         AND I.TOBACCO_CLASS_ID = H.TOBACCO_CLASS_ID
ORDER BY 1, 2, 3
;

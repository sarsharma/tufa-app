--------------------------------------------------------
--  DDL for View PERMIT_COPYADDRESS_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PERMIT_COPYADDRESS_VW" ("BUSINESS_NAME", "EIN", "COMPANYID", "INGESTED_STREET", "INGESTED_STATE", "INGESTED_CITY", "INGESTED_ATTENTION", "INGESTED_COUNTRY", "INGESTED_SUITE", "INGESTED_PROVINCE", "INGESTED_POSTAL_CD", "DOC_STAGE_ID", "STAGE_ID", "ROW_NUM") AS 
  select "BUSINESS_NAME","EIN","COMPANYID","INGESTED_STREET","INGESTED_STATE","INGESTED_CITY","INGESTED_ATTENTION","INGESTED_COUNTRY","INGESTED_SUITE","INGESTED_PROVINCE","INGESTED_POSTAL_CD","DOC_STAGE_ID","STAGE_ID","ROW_NUM"
  from
(
SELECT
    a.business_name,
    a.ein,
    a.companyid,
    a.INGESTED_STREET,
    a.INGESTED_STATE,
    a.INGESTED_CITY,
    a.INGESTED_ATTENTION,
    a.INGESTED_COUNTRY,
    trim(a.INGESTED_SUITE) as INGESTED_SUITE,
    a.INGESTED_PROVINCE,
    a.INGESTED_POSTAL_CD,
    b.doc_stage_id,
    b.stage_id,
    ROW_NUMBER() over (partition by a.companyid, a.ein ORDER BY a.companyid, a.ein DESC) as row_num
FROM
    permit_getaddressinfo_vw a
    INNER JOIN tu_permit_updates_stage b ON a.ein = b.ein
    LEFT OUTER JOIN tu_address ta ON ta.company_id = a.companyid
WHERE
        a.permit_id IN (
            SELECT
                p.permit_num
            FROM
                tu_permit p
            WHERE
                p.permit_status_type_cd = 'ACTV'
        )
    AND trim(a.tufa_state) IS NULL
    AND trim(a.tufa_street) IS NULL
    AND trim(a.tufa_attention) IS NULL
    AND trim(a.tufa_province) IS NULL
    AND trim(a.tufa_suite) IS NULL
    AND trim(a.tufa_country) IS NULL
    and trim(a.tufa_city) is null
    and trim(tufa_postalcd) is null
    and trim(tufa_province) is null
    and a.INGESTED_STATE_VALID = 'Y'
   and NOT  (
   a.ingested_street is null
   and ingested_state is null
   and a.ingested_city is null
   and a.ingested_attention is null
   and a.ingested_country is null and a.ingested_province is null and a.ingested_postal_cd is null )
group by
    a.business_name,
    a.ein,
    a.companyid,
    a.INGESTED_STREET,
    a.INGESTED_STATE,
    a.INGESTED_CITY,
    a.INGESTED_ATTENTION,
    a.INGESTED_COUNTRY,
    a.INGESTED_SUITE,
    a.INGESTED_PROVINCE,
    a.INGESTED_POSTAL_CD,
    b.doc_stage_id,
    b.stage_id,
    a.tufa_country
    ) inn
where inn.row_num = 1
;

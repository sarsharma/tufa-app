--------------------------------------------------------
--  DDL for View PERMIT_COPYCONTACT_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PERMIT_COPYCONTACT_VW" ("BUSINESS_NAME", "EIN", "COMPANYID", "INGESTED_EMAIL", "INGESTED_PHONE", "INGESTED_FAX_NUM", "DOC_STAGE_ID", "STAGE_ID", "ROW_NUM") AS 
  SELECT
     "BUSINESS_NAME",
     "EIN",
     "COMPANYID",
     "INGESTED_EMAIL",
     "INGESTED_PHONE",
     "INGESTED_FAX_NUM",
     "DOC_STAGE_ID",
     "STAGE_ID",
     "ROW_NUM"
 FROM
     (
         SELECT
             a.business_name,
             a.ein,
             a.companyid,
             a.ingested_email,
             a.ingested_phone,
             a.ingested_fax_num,
             b.doc_stage_id,
             b.stage_id,
             ROW_NUMBER() OVER(
             -- Kyle 3930 2/5/2019
                 PARTITION BY a.companyid,a.ein,a.ingested_email,a.ingested_phone,a.ingested_fax_num
                 ORDER BY
             -- Kyle 3930 2/5/2019
                     a.companyid,a.ein,a.ingested_email,a.ingested_phone,a.ingested_fax_num DESC
             ) AS row_num
         FROM
             permit_getcontactinfo_vw a
--            INNER JOIN tu_permit_updates_stage b ON a.ein = b.ein
--            This join is unique
             -- Kyle 3930 2/5/2019
             INNER JOIN tu_permit_updates_stage b ON a.stage_id = b.stage_id
             LEFT OUTER JOIN tu_contact tc ON tc.company_id = a.companyid
         WHERE
             a.permit_id IN (
                 SELECT
                     p.permit_num
                 FROM
                     tu_permit p
                 WHERE
                     p.permit_status_type_cd = 'ACTV'
             )
             AND TRIM(tufa_phone) IS NULL
             AND TRIM(tufa_email) IS NULL
             AND TRIM(tufa_fax_num) IS NULL
             AND TRIM(tufa_country_dial_cd) IS NULL
             AND TRIM(tufa_fax_cntry_dial_cd) IS NULL
             AND tufa_phone_ext = 0
             AND ( TRIM(ingested_email) IS NOT NULL
                   OR TRIM(ingested_phone) IS NOT NULL )
         GROUP BY
             a.business_name,
             a.ein,
             a.companyid,
             a.ingested_email,
             a.ingested_phone,
             a.ingested_fax_num,
             b.doc_stage_id,
             b.stage_id
     ) inn
WHERE
    inn.row_num = 1
-- Kyle 3930 2/7/2019
ORDER BY
    inn.stage_id ASC
;

--------------------------------------------------------
--  DDL for View PERMIT_EXCL_AFFCTED_ASSMNTS_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PERMIT_EXCL_AFFCTED_ASSMNTS_VW" ("COMPANY_ID", "LEGAL_NM", "EIN", "PERMIT_ID", "PERMIT_NUM", "EXCLUSION_SCOPE_ID", "CREATED_DT", "CREATED_BY", "ASSESSMENT_STATUS", "ASSESSMENT_YR", "ASSESSMENT_QTR", "ASSESSMENT_TYPE", "ASSESSMENT_RPT_DATA_YR", "ASSESSMENT_RPT_DATA_QTR") AS 
  SELECT DISTINCT
           t_p_excl.company_id,
           t_company.legal_nm,
           t_company.ein,
           t_p_excl.permit_id,
           t_permit.permit_num,
           --        t_p_excl.PERM_EXCL_AUDIT_ID,
           t_p_excl.exclusion_scope_id,
           t_p_excl.created_dt,
           t_p_excl.created_by,
           t_aff_asmnt.assessment_status,
           t_aff_asmnt.assmnt_yr           AS "ASSESSMENT_YR",
           t_aff_asmnt.assmnt_qtr          AS "ASSESSMENT_QTR",
           t_aff_asmnt.assmnt_type         AS "ASSESSMENT_TYPE",
           t_aff_asmnt.assmnt_rpt_data_yr  AS "ASSESSMENT_RPT_DATA_YR",
           t_aff_asmnt.assmnt_rpt_data_qtr AS "ASSESSMENT_RPT_DATA_QTR"
      FROM (SELECT affected_assmnt_id,
                   perm_excl_audit_id,
                   assessment_status,
                   assmnt_yr,
                   assmnt_qtr,
                   assmnt_type,
                   assmnt_rpt_data_yr,
                   assmnt_rpt_data_qtr
              FROM (                       -- last in group perm_excl_audit_id
                    SELECT affected_assmnt_id,
                           perm_excl_audit_id,
                           assessment_status,
                           assmnt_yr,
                           assmnt_qtr,
                           assmnt_type,
                           assmnt_rpt_data_yr,
                           assmnt_rpt_data_qtr,
                           MAX (affected_assmnt_id)
                               OVER (PARTITION BY perm_excl_audit_id,
                                                  assmnt_yr,
                                                  assmnt_qtr,
                                                  assmnt_type)
                               AS max_aff_assmnt_id
                      FROM tu_permit_excl_affect_assmnts)
             WHERE     affected_assmnt_id = max_aff_assmnt_id
                   AND assessment_status = 'RE-RUN') t_aff_asmnt
           INNER JOIN (SELECT perm_excl_audit_id,
                              company_id,
                              permit_id,
                              assmnt_yr,
                              assmnt_qtr,
                              exclusion_scope_id,
                              created_by,
                              created_dt
                         FROM (SELECT perm_excl_audit_id,
                                      company_id,
                                      permit_id,
                                      assmnt_yr,
                                      assmnt_qtr,
                                      exclusion_scope_id,
                                      created_by,
                                      created_dt,
                                      MAX (perm_excl_audit_id)
                                          OVER (PARTITION BY company_id,
                                                             permit_id,
                                                             assmnt_yr,
                                                             assmnt_qtr)
                                          AS max_excl_id
                                 FROM tu_permit_excl_audit)
                        WHERE perm_excl_audit_id = max_excl_id) t_p_excl
               ON t_aff_asmnt.perm_excl_audit_id =
                      t_p_excl.perm_excl_audit_id
           INNER JOIN tu_company t_company
               ON t_company.company_id = t_p_excl.company_id
           INNER JOIN tu_permit t_permit
               ON t_permit.permit_id = t_p_excl.permit_id
;

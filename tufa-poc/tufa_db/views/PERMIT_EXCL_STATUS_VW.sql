--------------------------------------------------------
--  DDL for View PERMIT_EXCL_STATUS_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PERMIT_EXCL_STATUS_VW" ("COMPANY_ID", "LEGAL_NM", "EIN", "PERMIT_ID", "PERMIT_NUM", "PERM_EXCL_AUDIT_ID", "CREATED_DT", "CREATED_BY", "ASSMNT_YR", "ASSMNT_QTR", "FULL_PERM_EXCL_AUDIT_ID", "COMMENT_ID", "EXCL_SCOPE_DESC", "EXCL_SCOPE_ID") AS 
  SELECT
    t_p_excl.company_id,
    t_company.legal_nm,
    t_company.ein,
    t_p_excl.permit_id,
    t_permit.permit_num,
    t_p_excl.perm_excl_audit_id,
    t_p_excl.created_dt,
    t_p_excl.created_by,
    t_p_excl.assmnt_yr,
    t_p_excl.assmnt_qtr,
    t_p_excl.full_perm_excl_audit_id,
    t_p_excl.comment_id,
    excl_scope.excl_scope_desc,
    excl_scope.excl_scope_id
FROM
    (
        SELECT
            perm_excl_audit_id,
            company_id,
            permit_id,
            assmnt_yr,
            assmnt_qtr,
            exclusion_scope_id,
            created_by,
            created_dt,
            full_perm_excl_audit_id,
            comment_id
        FROM
            (
                SELECT
                    perm_excl_audit_id,
                    company_id,
                    permit_id,
                    assmnt_yr,
                    assmnt_qtr,
                    exclusion_scope_id,
                    created_by,
                    created_dt,
                    full_perm_excl_audit_id,
                    comment_id,
                    MAX(perm_excl_audit_id) OVER(
                        PARTITION BY company_id, permit_id, assmnt_yr, assmnt_qtr
                    ) AS max_excl_id
                FROM
                    tu_permit_excl_audit
            )
        WHERE perm_excl_audit_id = max_excl_id
UNION
SELECT
            perm_excl_audit_id,
            company_id,
            permit_id,
            assmnt_yr,
            assmnt_qtr,
            exclusion_scope_id,
            created_by,
            created_dt,
            full_perm_excl_audit_id,
            comment_id
            FROM
            tu_permit_excl_audit
             WHERE
            tu_permit_excl_audit.exclusion_scope_id = 3) t_p_excl
    INNER JOIN tu_permit_excl_scope   excl_scope ON excl_scope.excl_scope_id = t_p_excl.exclusion_scope_id
    INNER JOIN tu_company             t_company ON t_company.company_id = t_p_excl.company_id
    INNER JOIN tu_permit              t_permit ON t_permit.permit_id = t_p_excl.permit_id
ORDER BY
    t_company.legal_nm,
    t_permit.permit_num,
    t_p_excl.perm_excl_audit_id DESC,
    t_p_excl.created_dt DESC,
    t_p_excl.assmnt_yr ASC,
    t_p_excl.assmnt_qtr ASC
;

--------------------------------------------------------
--  DDL for View PERMIT_GAPS_TABLEAU_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PERMIT_GAPS_TABLEAU_VW" ("Company EIN", "Company name", "Permit number", "Permit type", "Permit start date", "Permit close date", "Permit status", "Report year", "Report month") AS 
  select distinct ein as "Company EIN", legal_nm as "Company name", permit_num as "Permit number",
                permit_type_cd as "Permit type", issue_dt as "Permit start date", close_dt as "Permit close date",
                decode(permit_status_type_cd,'ACTV','Active','Closed') as "Permit status",
                year as "Report year", month as "Report month"
from tu_company a,
     tu_permit b,
     tu_rpt_period c
where a.company_id = b.company_id
  and to_date(month||'-'||year, 'MON-YYYY') <= nvl(close_dt,to_date(month||'-'||year, 'MON-YYYY'))
  and to_date(month||'-'||year, 'MON-YYYY') >= nvl(issue_dt,to_date(month||'-'||year, 'MON-YYYY'))
minus
select distinct ein as "Company EIN", legal_nm as "Company name", permit_num as "Permit number",
                permit_type_cd as "Permit type", issue_dt as "Permit start date", close_dt as "Permit close date",
                decode(permit_status_type_cd,'ACTV','Active','Closed') as "Permit status",
                year as "Report year", month as "Report month"
from tu_rpt_period a,
     tu_company b,
     tu_permit c,
     tu_permit_period d
where B.COMPANY_ID = c.company_id
  and a.period_id = d.period_id
  and c.permit_id = d.permit_id
;

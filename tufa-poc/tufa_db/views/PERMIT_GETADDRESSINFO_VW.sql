--------------------------------------------------------
--  DDL for View PERMIT_GETADDRESSINFO_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PERMIT_GETADDRESSINFO_VW" ("BUSINESS_NAME", "EIN", "PERMIT_ID", "STAGE_ID", "DOC_STAGE_ID", "INGESTED_STREET", "INGESTED_STATE", "INGESTED_CITY", "INGESTED_POSTAL_CD", "INGESTED_PROVINCE", "INGESTED_SUITE", "INGESTED_ATTENTION", "INGESTED_COUNTRY", "TUFA_STREET", "TUFA_STATE", "TUFA_CITY", "TUFA_POSTALCD", "TUFA_PROVINCE", "TUFA_SUITE", "TUFA_ATTENTION", "TUFA_COUNTRY", "PERMITSTATUS", "ADDRESSSTATUS", "DOC_3852_ID", "MR_MONTH", "MR_FISCAL_YR", "COMPANYID", "INGESTED_ADDRESS", "TUFA_ADDRESS", "TUFA_ADDRESS1", "TUFA_ADDRESS2", "INGESTED_STATE_VALID") AS 
  SELECT business_name,
           ein,
           permit_id,
           STAGE_ID,
           DOC_STAGE_ID,
           ingested_street,
           ingested_state,
           ingested_city,
           ingested_postal_cd,
           ingested_province,
           ingested_suite,
           ingested_attention,
           ingested_country,
           tufa_street,
           tufa_state,
           tufa_city,
           tufa_postalcd,
           tufa_province,
           tufa_suite,
           tufa_attention,
           tufa_country,
           permitstatus,
           addressstatus,
           doc_3852_id,
           MR_MONTH,
           MR_FISCAL_YR,
           companyid,
           --(NVL(ingested_attention,'NOT APPLICABLE') || ','||
           --NVL(replace(ingested_suite,' ',null),'NOT APPLICABLE')||','||
            (   NVL (ingested_street, ' ')
             || ','
             || NVL (ingested_city, ' ')
             || ','
             || NVL (ingested_state, ' ')
             || ','
             || NVL (ingested_postal_cd, ' ')
             || ','
             || NVL (ingested_country, 'US'))
               ingested_address,
              ( --DECODE(NVL(INSTR(tufa_attention,';'),0),0,SUBSTR(tufa_attention,1),SUBSTR(tufa_attention,1,NVL((INSTR(tufa_attention,';')-1),'') )) ||','||
               DECODE (
                      NVL (INSTR (tufa_street, ';'), 0),
                      0, SUBSTR (tufa_street, 1),
                      SUBSTR (tufa_street,
                              1,
                              NVL ( (INSTR (tufa_street, ';') - 1), '')))
               || ','
               || --DECODE(NVL(INSTR(tufa_suite,';'),0),0,SUBSTR(tufa_suite,1),SUBSTR(tufa_suite,1,NVL((INSTR(tufa_suite,';')-1),'') ))||','||
                 DECODE (
                      NVL (INSTR (tufa_city, ';'), 0),
                      0, SUBSTR (tufa_city, 1),
                      SUBSTR (tufa_city,
                              1,
                              NVL ( (INSTR (tufa_city, ';') - 1), '')))
               || ','
               || DECODE (
                      NVL (INSTR (tufa_state_prov, ';'), 0),
                      0, SUBSTR (tufa_state_prov, 1),
                      SUBSTR (tufa_state_prov,
                              1,
                              NVL ( (INSTR (tufa_state_prov, ';') - 1), '')))
               || ','
               || DECODE (
                      NVL (INSTR (tufa_postalcd, ';'), 0),
                      0, SUBSTR (tufa_postalcd, 1),
                      SUBSTR (tufa_postalcd,
                              1,
                              NVL ( (INSTR (tufa_postalcd, ';') - 1), '')))
               || ','
               || DECODE (
                      NVL (INSTR (tufa_country, ';'), 0),
                      0, SUBSTR (tufa_country, 1),
                      SUBSTR (tufa_country,
                              1,
                              NVL ( (INSTR (tufa_country, ';') - 1), ''))))
           || '-DIFFERENTIATOR-'
           || CASE
                  WHEN (INSTR (tufa_attention, ';') > 0)
                  THEN --SUBSTR(tufa_attention,INSTR(tufa_attention,';')+1) ||','||
                         SUBSTR (tufa_street, INSTR (tufa_street, ';') + 1)
                      || ',' ||
                        SUBSTR (tufa_city, INSTR (tufa_city, ';') + 1)
                      || ',' ||
                        SUBSTR (tufa_state_prov, INSTR (tufa_state_prov, ';') + 1)
                      || ','
                      || --SUBSTR(tufa_suite,INSTR(tufa_suite,';')+1) ||','||SUBSTR(tufa_city,INSTR(tufa_city,';')+1) ||','||SUBSTR(tufa_state_prov,INSTR(tufa_state_prov,';')+1) ||','||
                        SUBSTR (tufa_postalcd, INSTR (tufa_postalcd, ';') + 1)
                      || ','
                      || SUBSTR (tufa_country, INSTR (tufa_country, ';') + 1)
                  ELSE
                      ' '
              END
               AS tufa_address,
           (   DECODE (
                   NVL (INSTR (tufa_attention, ';'), 0),
                   0, SUBSTR (tufa_attention, 1),
                   SUBSTR (tufa_attention,
                           1,
                           NVL ( (INSTR (tufa_attention, ';') - 1), '')))
            || ','
            || DECODE (
                   NVL (INSTR (tufa_street, ';'), 0),
                   0, SUBSTR (tufa_street, 1),
                   SUBSTR (tufa_street,
                           1,
                           NVL ( (INSTR (tufa_street, ';') - 1), '')))
            || ','
            || DECODE (
                   NVL (INSTR (tufa_suite, ';'), 0),
                   0, SUBSTR (tufa_suite, 1),
                   SUBSTR (tufa_suite,
                           1,
                           NVL ( (INSTR (tufa_suite, ';') - 1), '')))
            || ','
            || DECODE (
                   NVL (INSTR (tufa_city, ';'), 0),
                   0, SUBSTR (tufa_city, 1),
                   SUBSTR (tufa_city,
                           1,
                           NVL ( (INSTR (tufa_city, ';') - 1), '')))
            || ','
            || DECODE (
                   NVL (INSTR (tufa_state_prov, ';'), 0),
                   0, SUBSTR (tufa_state_prov, 1),
                   SUBSTR (tufa_state_prov,
                           1,
                           NVL ( (INSTR (tufa_state_prov, ';') - 1), '')))
            || ','
            || DECODE (
                   NVL (INSTR (tufa_postalcd, ';'), 0),
                   0, SUBSTR (tufa_postalcd, 1),
                   SUBSTR (tufa_postalcd,
                           1,
                           NVL ( (INSTR (tufa_postalcd, ';') - 1), '')))
            || ','
            || DECODE (
                   NVL (INSTR (tufa_country, ';'), 0),
                   0, SUBSTR (tufa_country, 1),
                   SUBSTR (tufa_country,
                           1,
                           NVL ( (INSTR (tufa_country, ';') - 1), ''))))
               Tufa_address1,
           CASE
               WHEN (INSTR (tufa_attention, ';') > 0)
               THEN
                      SUBSTR (tufa_attention,
                              INSTR (tufa_attention, ';') + 1)
                   || ','
                   || SUBSTR (tufa_street, INSTR (tufa_street, ';') + 1)
                   || ','
                   || SUBSTR (tufa_suite, INSTR (tufa_suite, ';') + 1)
                   || ','
                   || SUBSTR (tufa_city, INSTR (tufa_city, ';') + 1)
                   || ','
                   || SUBSTR (tufa_state_prov,INSTR (tufa_state_prov, ';') + 1)
                   || ','
                   || SUBSTR (tufa_postalcd, INSTR (tufa_postalcd, ';') + 1)
                   || ','
                   || SUBSTR (tufa_country, INSTR (tufa_country, ';') + 1)
               ELSE
                   ' '
           END
               AS Tufa_address2,
           DECODE (REGEXP_INSTR (errors, 'mailingState'), 0, 'Y', 'N')
               AS INGESTED_STATE_VALID
      FROM (  SELECT UPPER(b.business_name) AS business_name,
                     b.ein,
                     b.permit_id,
                     b.stage_id                       AS STAGE_ID,
                     b.doc_stage_id                   AS DOC_STAGE_ID,
                     UPPER (b.mailing_street)         AS ingested_street,
                     UPPER (b.mailing_state)          AS ingested_state,
                     UPPER (b.mailing_city)           AS ingested_city,
                     b.mailing_postal_cd              AS ingested_postal_cd,
                     UPPER (b.mailing_province)       AS ingested_province,
                     NVL (UPPER (b.mailing_suite), ' ') AS ingested_suite,
                     UPPER (b.mailing_attention)      AS ingested_attention,
                     b.country_cd                     AS ingested_country,
                     LISTAGG (NVL (UPPER (a.STREET_ADDRESS), ' '), ';')
                         WITHIN GROUP (ORDER BY ADDRESS_TYPE_CD)
                         AS tufa_street,
                     LISTAGG (
                         NVL (
                             UPPER (
                                 DECODE (a.province, NULL, a.state, a.province)),
                             ' '),
                         ';')
                     WITHIN GROUP (ORDER BY ADDRESS_TYPE_CD)
                         AS tufa_state_prov,
                     LISTAGG (NVL (UPPER (a.state), ' '), ';')
                         WITHIN GROUP (ORDER BY ADDRESS_TYPE_CD)
                         AS tufa_state,
                     LISTAGG (NVL (UPPER (a.city), ' '), ';')
                         WITHIN GROUP (ORDER BY ADDRESS_TYPE_CD)
                         AS tufa_city,
                     LISTAGG (NVL (a.postal_cd, ' '), ';')
                         WITHIN GROUP (ORDER BY ADDRESS_TYPE_CD)
                         AS tufa_postalcd,
                     LISTAGG (NVL (UPPER (a.province), ' '), ';')
                         WITHIN GROUP (ORDER BY ADDRESS_TYPE_CD)
                         AS tufa_province,
                     LISTAGG (NVL (UPPER (a.suite), ' '), ';')
                         WITHIN GROUP (ORDER BY ADDRESS_TYPE_CD)
                         AS tufa_suite,
                     LISTAGG (NVL (UPPER (a.attention), ' '), ';')
                         WITHIN GROUP (ORDER BY ADDRESS_TYPE_CD)
                         AS tufa_attention,
                     LISTAGG (NVL (a.country_cd, ' '), ';')
                         WITHIN GROUP (ORDER BY ADDRESS_TYPE_CD)
                         AS tufa_country,
                     b.PERMIT_STATUS                  AS permitstatus,
                     b.address_status                 AS addressstatus,
                     doc.document_id                  AS doc_3852_id,
                     doc.month                        AS MR_MONTH,
                     doc.fiscal_yr                    AS MR_FISCAL_YR,
                     c.company_id                     AS companyid,
                     b.errors                         AS errors
                FROM tu_company c
                     INNER JOIN tu_permit_updates_stage b
                         ON c.ein = REGEXP_REPLACE (b.ein, '[^a-zA-Z0-9]+', '')
                     LEFT OUTER JOIN tu_address a
                         ON a.company_id = c.company_id
                     INNER JOIN tu_permit p
                         ON     b.permit_id = p.permit_num
                            AND c.company_id = p.company_id
--                            AND p.permit_status_type_cd = 'ACTV'
                     LEFT JOIN
                     (SELECT permit_id,
                             document_id,
                             month,
                             fiscal_yr
                        FROM (SELECT DC.*,
                                     ROW_NUMBER ()
                                     OVER (
                                         PARTITION BY DC.permit_id
                                         ORDER BY
                                             DC.permit_id, DC.rpt_3852 DESC)
                                         AS row_num
                                FROM (SELECT p.permit_id,
                                             doc.document_id,
                                             TO_CHAR (
                                                 TO_DATE (rpt.month, 'mm'),
                                                 'Month')
                                                 AS month,
                                             rpt.fiscal_yr,
                                             TO_DATE (
                                                 rpt.month || '-' || rpt.year,
                                                 'MON-yyyy')
                                                 AS rpt_3852
                                        -- Kyle: Changed above from fiscal_year to year
                                        FROM tu_permit p
                                             INNER JOIN tu_permit_period pp
                                                 ON pp.permit_id = p.permit_id
                                             INNER JOIN tu_document doc
                                                 ON     doc.permit_id =
                                                            p.permit_id
                                                    AND doc.period_id =
                                                            pp.period_id
                                                    AND REGEXP_INSTR (
                                                            doc.form_types,
                                                            'FDA 3852') > 0
                                             INNER JOIN tu_rpt_period rpt
                                                 ON rpt.period_id =
                                                        pp.period_id) DC)
                       WHERE row_num = 1) doc
                         ON doc.permit_id = p.permit_id
            GROUP BY b.business_name,
                     b.ein,
                     b.permit_id,
                     b.doc_stage_id,
                     b.stage_id,
                     b.mailing_street,
                     b.mailing_state,
                     b.mailing_city,
                     b.mailing_postal_cd,
                     b.mailing_province,
                     b.mailing_suite,
                     b.mailing_attention,
                     b.country_cd,
                     b.PERMIT_STATUS,
                     b.address_status,
                     doc.document_id,
                     doc.month,
                     doc.fiscal_yr,
                     c.company_id,
                     b.errors
            ORDER BY b.business_name)
;

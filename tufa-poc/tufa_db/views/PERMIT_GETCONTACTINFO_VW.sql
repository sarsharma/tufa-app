--------------------------------------------------------
--  DDL for View PERMIT_GETCONTACTINFO_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PERMIT_GETCONTACTINFO_VW" ("BUSINESS_NAME", "EIN", "PERMIT_ID", "STAGE_ID", "DOC_STAGE_ID", "INGESTED_EMAIL", "INGESTED_PHONE", "INGESTED_FAX_NUM", "TUFA_EMAIL", "TUFA_PHONE", "TUFA_FAX_NUM", "TUFA_FIRST_NAME", "TUFA_LAST_NAME", "TUFA_PHONE_EXT", "TUFA_COUNTRY_DIAL_CD", "TUFA_FAX_CNTRY_DIAL_CD", "PERMITSTATUS", "CONTACTSTATUS", "COMPANYID", "DOC_3852_ID", "MR_MONTH", "MR_FISCAL_YR", "INGESTED_CONTACT", "TUFA_CONTACT") AS 
  SELECT UPPER(b.business_name) AS business_name,
             b.ein,
             b.permit_id,
             b.stage_id                       AS stage_id,
             b.doc_stage_id                   AS doc_stage_id,
             NVL (UPPER (b.email_address), ' ') AS ingested_email,
             NVL (b.premise_phone, ' ')       AS ingested_phone,
             NVL (b.fax_num, ' ')             AS ingested_fax_num,
             LISTAGG (NVL (UPPER (a.email_address), ' '), ';')
                 WITHIN GROUP (ORDER BY a.contact_id)
                 AS tufa_email,
             LISTAGG (NVL (a.phone_num, ' '), ';')
                 WITHIN GROUP (ORDER BY a.contact_id)
                 AS tufa_phone,
             LISTAGG (NVL (a.fax_num, ' '), ';')
                 WITHIN GROUP (ORDER BY a.contact_id)
                 AS tufa_fax_num,
             LISTAGG (NVL (UPPER (a.first_nm), ' '), ';')
                 WITHIN GROUP (ORDER BY a.contact_id)
                 AS tufa_first_name,
             LISTAGG (NVL (UPPER (a.last_nm), ' '), ';')
                 WITHIN GROUP (ORDER BY a.contact_id)
                 AS tufa_last_name,
             LISTAGG (NVL (a.phone_ext, 0), ';')
                 WITHIN GROUP (ORDER BY a.contact_id)
                 AS tufa_phone_ext,
             LISTAGG (NVL (a.cntry_dial_cd, ' '), ';')
                 WITHIN GROUP (ORDER BY a.contact_id)
                 AS tufa_country_dial_cd,
             LISTAGG (NVL (a.fax_cntry_dial_cd, ' '), ';')
                 WITHIN GROUP (ORDER BY a.contact_id)
                 AS tufa_fax_cntry_dial_cd,
             b.permit_status                  AS permitstatus,
             b.contact_status                 AS contactstatus,
             c.company_id                     AS companyid,
             doc.document_id                  AS doc_3852_id,
             doc.month                        AS mr_month,
             doc.fiscal_yr                    AS mr_fiscal_yr,
                NVL (UPPER (TRIM (' ' FROM b.email_address)), ' ')
             || ','
             -- Replace the hyphin from the phone as of 4189 2/20/2019
             || NVL (TRIM (' ' FROM REPLACE (b.premise_phone, '-', '')), ' ')
                 ingested_contact,
             LISTAGG (
                 NVL (
                     UPPER (
                            NVL (TRIM (' ' FROM a.email_address), ' ')
                         || ','
                         -- Replace the hyphin from the phone as of 4189 2/20/2019
                         || NVL (
                                TRIM (' ' FROM REPLACE (a.phone_num, '-', '')),
                                ' ')),
                     ' '),
                 '-DIFFERENTIATOR-')
             WITHIN GROUP (ORDER BY a.contact_id)
                 AS tufa_contact
        FROM tu_company c
             INNER JOIN tu_permit_updates_stage b
                 ON c.ein = REGEXP_REPLACE (b.ein, '[^a-zA-Z0-9]+', '')
             LEFT OUTER JOIN tu_contact a ON a.company_id = c.company_id
             INNER JOIN tu_permit p
                 ON     b.permit_id = p.permit_num
                    AND c.company_id = p.company_id
--                    AND p.permit_status_type_cd = 'ACTV'
             LEFT JOIN
             (SELECT permit_id,
                     document_id,
                     month,
                     fiscal_yr
                FROM (SELECT dc.*,
                             ROW_NUMBER ()
                             OVER (PARTITION BY dc.permit_id
                                   ORDER BY dc.permit_id, dc.rpt_3852 DESC)
                                 AS row_num
                        FROM (SELECT p.permit_id,
                                     doc.document_id,
                                     TO_CHAR (TO_DATE (rpt.month, 'mm'),
                                              'Month')
                                         AS month,
                                     rpt.fiscal_yr,
                                     TO_DATE (rpt.month || '-' || rpt.year,
                                              'MON-yyyy')
                                         AS rpt_3852
                                --Kyle: Changed above to year from fiscal_year
                                FROM tu_permit p
                                     INNER JOIN tu_permit_period pp
                                         ON pp.permit_id = p.permit_id
                                     INNER JOIN tu_document doc
                                         ON     doc.permit_id = p.permit_id
                                            AND doc.period_id = pp.period_id
                                            AND REGEXP_INSTR (doc.form_types,
                                                              'FDA 3852') > 0
                                     INNER JOIN tu_rpt_period rpt
                                         ON rpt.period_id = pp.period_id) dc)
               WHERE row_num = 1) doc
                 ON doc.permit_id = p.permit_id
    --  WHERE  b.permit_id IN ( SELECT  p.permit_num FROM tu_permit p where p.permit_status_type_cd = 'ACTV')
    GROUP BY b.business_name,
             b.ein,
             b.permit_id,
             b.doc_stage_id,
             b.email_address,
             b.stage_id,
             b.premise_phone,
             b.fax_num,
             b.permit_status,
             b.contact_status,
             c.company_id,
             doc.document_id,
             doc.month,
             doc.fiscal_yr
    ORDER BY b.business_name
;

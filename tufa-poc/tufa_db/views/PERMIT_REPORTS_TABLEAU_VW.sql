--------------------------------------------------------
--  DDL for View PERMIT_REPORTS_TABLEAU_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PERMIT_REPORTS_TABLEAU_VW" ("Company EIN", "Company Name", "Permit Number", "Permit Type", "Permit Start Date", "Permit Close Date", "Report Year", "Report Month", "QUARTER", "Tobacco Class Id", "REMOVAL_QTY", "Taxes Paid", "Report Status", "Report Attachment", "Report Recon Status", "PERMIT_ID", "PERIOD_ID", "Report Submission Type") AS 
  Select "Company EIN","Company Name","Permit Number","Permit Type","Permit Start Date","Permit Close Date","Report Year","Report Month","QUARTER","Tobacco Class Id","REMOVAL_QTY","Taxes Paid","Report Status","Report Attachment","Report Recon Status","PERMIT_ID","PERIOD_ID","Report Submission Type" from
(SELECT
    substr(co.ein,1,2)||'-'||substr(co.ein,3) as "Company EIN",
    co.legal_nm as "Company Name",
    --co.company_status as "Company Status",
     SUBSTR (P.PERMIT_NUM, 1, 2)
             || '-'
             || SUBSTR (p.permit_num, 3, 2)
             || '-'
             || SUBSTR (p.permit_num, 5, LENGTH (p.permit_num) - 4) as "Permit Number",
    p.permit_type_cd as "Permit Type",
    p.ISSUE_DT as "Permit Start Date",
    p.close_dt as "Permit Close Date",
    rp.fiscal_yr as "Report Year",
    rp.MONTH as "Report Month",
    rp.quarter,
   details.tobacco_class_id as "Tobacco Class Id",
    NVL (details.REMOVAL_QTY, 0) AS REMOVAL_QTY,
    NVL (details.TAXES_PAID, 0)  AS "Taxes Paid",
    ps.period_status_type_cd as "Report Status",
    --decode(to_char(do.document_id),NULL,'N','Y') as "Report Attachment",
    Decode((Select count(1) from tu_document td where td.PERMIT_ID=pp.permit_id and td.period_id=pp.period_id ),0,'N','Y') as "Report Attachment",
    ps.recon_status_type_cd as "Report Recon Status",
    ps.permit_id,
    ps.period_id,
   -- pp.zero_flag,
   -- CASE
     -- WHEN ps.period_status_type_cd = 'COMP'
     -- THEN TO_CHAR (ps.status_dt, 'DD/MON/YY')
     -- ELSE NULL
   -- END AS COMPLETED_DATE,
    pp.source_cd as "Report Submission Type"
    --ad.postal_cd,
    --ad.state
  FROM tu_company co INNER JOIN TU_PERMIT p ON co.COMPANY_ID=p.COMPANY_ID
  INNER JOIN TU_PERMIT_PERIOD pp on p.PERMIT_ID=pp.PERMIT_ID
  INNER JOIN TU_RPT_PERIOD rp on pp.period_id = rp.period_id
  INNER JOIN TU_PERIOD_STATUS ps ON (ps.PERMIT_ID=pp.PERMIT_ID AND ps.PERIOD_ID=pp.PERIOD_ID)
  LEFT OUTER JOIN (SELECT sf.permit_id, sf.period_id, fd.tobacco_class_id, NVL (fd.REMOVAL_QTY, 0) AS REMOVAL_QTY, NVL (fd.TAXES_PAID, 0)  AS TAXES_PAID
    FROM TU_SUBMITTED_FORM sf, tu_form_detail fd
     WHERE sf.form_id = fd.form_id
     AND sf.form_type_cd = '3852') details on (pp.permit_id=details.permit_id AND pp.period_id = details.period_id)
  --  LEFT OUTER JOIN TU_ADDRESS ad on co.company_id=ad.company_id
)
order by "Report Year" desc,quarter asc, TO_date("Report Month",'MON') ASC
;

--------------------------------------------------------
--  DDL for View PERMIT_STATUS_REPORT_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PERMIT_STATUS_REPORT_VW" ("Permit Number", "Permit Status Type Code", "Issue Date", "Close Date", "Permit Type Code") AS 
  select permit_num as "Permit Number", permit_status_type_cd as "Permit Status Type Code", issue_dt as "Issue Date", close_dt as "Close Date", permit_type_cd as "Permit Type Code"
from tu_permit
order by 1
;

--------------------------------------------------------
--  DDL for View PRIMARY_CONTACT_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."PRIMARY_CONTACT_VW" ("COMPANY_ID", "CONTACT_ID", "COUNTRY_CD", "EMAIL_ADDRESS", "FAX_NUM", "FIRST_NM", "LAST_NM", "PHONE_EXT", "CNTRY_DIAL_CD", "PHONE_NUM", "HASH_TOTAL") AS 
  SELECT b.company_id,
           contact_id,
           country_cd,
           email_address,
           fax_num,
           first_nm,
           last_nm,
           phone_ext,
           cntry_dial_cd,
           phone_num,
           hash_total
      FROM tu_contact b
     WHERE b.created_dt = (SELECT MIN (created_dt)
                             FROM tu_contact
                            WHERE company_id = b.company_id)
;

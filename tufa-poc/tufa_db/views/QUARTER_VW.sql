--------------------------------------------------------
--  DDL for View QUARTER_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."QUARTER_VW" ("QUARTER", "FISCAL_YR", "START_DATE", "END_DATE") AS 
  SELECT quarter,
             fiscal_yr,
             MIN (
                 TO_DATE ('01-' || month || '-' || year || ' 00:00:00',
                          'dd-MON-yyyy HH24:MI:SS'))
                 AS start_date,
             MAX (
                 TO_DATE (
                     end_day || '-' || month || '-' || year || ' 23:59:59',
                     'dd-MON-yyyy HH24:MI:SS'))
                 AS end_date
        FROM (SELECT quarter,
                     fiscal_yr,
                     year,
                     month,
                     CASE
                         WHEN month = 'JAN'
                         THEN
                             31
                         WHEN     month = 'FEB'
                              AND MOD (year, 4) = 0
                              AND (    MOD (year, 100) <> 0
                                   AND MOD (year, 400) <> 0)
                         THEN
                             29
                         WHEN month = 'MAR'
                         THEN
                             31
                         WHEN month = 'APR'
                         THEN
                             30
                         WHEN month = 'MAY'
                         THEN
                             31
                         WHEN month = 'JUN'
                         THEN
                             30
                         WHEN month = 'JUL'
                         THEN
                             31
                         WHEN month = 'AUG'
                         THEN
                             31
                         WHEN month = 'SEP'
                         THEN
                             30
                         WHEN month = 'OCT'
                         THEN
                             31
                         WHEN month = 'NOV'
                         THEN
                             30
                         WHEN month = 'DEC'
                         THEN
                             31
                         ELSE
                             28
                     END
                         AS end_day
                FROM TU_RPT_PERIOD)
    GROUP BY QUARTER, fiscal_yr
;

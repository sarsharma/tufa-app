--------------------------------------------------------
--  DDL for View RANKED_QTRLY_ASSESSMENT_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."RANKED_QTRLY_ASSESSMENT_VW" ("ASSESSMENT_ID", "VERSION_NUM", "MS_RANK", "ASSESSMENT_YR", "ASSESSMENT_QTR", "LEGAL_NM", "EIN", "PERMITNUM", "TOT_VOL_REMOVED", "TOT_TAXES_PAID", "SHARE_TOT_TAXES", "PREVIOUS_SHARE", "DELTA_SHARE", "STREET_ADDRESS", "SUITE", "CITY", "STATE", "PROVINCE", "POSTAL_CD", "CNTRY_DIAL_CD", "PHONE_NUM", "EMAIL_ADDRESS", "ADDRESS_CHANGED_FLAG", "CONTACT_CHANGED_FLAG", "TOBACCO_CLASS_NM", "CREATED_DT", "AUTHOR", "ORIGINAL_CORRECTED") AS 
  SELECT a.assessment_id,
           c.version_num,
           c.ms_rank,
           a.assessment_yr,
           a.assessment_qtr,
           (SELECT b.legal_nm
              FROM INGESTED_ONLY_VW b
             WHERE b.ein = c.ein AND ROWNUM = 1)
               AS "LEGAL_NM",
           -- nvl(c.company_name,(Select  b.legal_nm from INGESTED_ONLY_VW b where b.ein=c.ein  and rownum=1))
           -- as legal_nm1,
           c.ein,
           '' PERMITNUM,
           c.tot_vol_removed,
           c.tot_taxes_paid,
           c.share_tot_taxes,
           c.previous_share,
           c.delta_share,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL,
           c.address_changed_flag,
           c.contact_changed_flag,
           d.tobacco_class_nm,
           c.created_dt,
           c.author,
           c.original_corrected
      FROM tu_market_share  c
           INNER JOIN tu_assessment a ON c.assessment_id = a.assessment_id
           INNER JOIN tu_tobacco_class d
               ON c.tobacco_class_id = d.tobacco_class_id
     WHERE c.company_id = 0
    UNION ALL
    SELECT a.assessment_id,
           c.version_num,
           c.ms_rank,
           a.assessment_yr,
           a.assessment_qtr,
           c.company_name AS "LEGAL_NM",
           c.ein          AS ein,
           ''             PERMITNUM,
           c.tot_vol_removed,
           c.tot_taxes_paid,
           c.share_tot_taxes,
           c.previous_share,
           c.delta_share,
           e.street_address,
           e.suite,
           e.city,
           e.state,
           e.province,
           e.postal_cd,
           f.cntry_dial_cd,
           f.phone_num,
           f.email_address,
           c.address_changed_flag,
           c.contact_changed_flag,
           d.tobacco_class_nm,
           c.created_dt,
           c.author,
           c.original_corrected
      FROM tu_market_share  c
           INNER JOIN tu_assessment a ON c.assessment_id = a.assessment_id
           INNER JOIN tu_company b ON c.company_id = b.company_id
           INNER JOIN tu_tobacco_class d
               ON c.tobacco_class_id = d.tobacco_class_id
           LEFT OUTER JOIN tu_assessment_address e
               ON (    c.company_id = e.company_id
                   AND c.assessment_id = e.assessment_id
                   AND c.version_num = e.version_num)
           LEFT OUTER JOIN tu_assessment_contact f
               ON (    c.company_id = f.company_id
                   AND c.assessment_id = f.assessment_id
                   AND c.version_num = f.version_num)
;

--------------------------------------------------------
--  DDL for View RAW_INGESTED_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."RAW_INGESTED_VW" ("COMPANY_NM", "COMPANY_EIN", "ORIGINAL_COMPANY_NM", "ORIGINAL_EIN", "TAXES_PAID", "REMOVALS_AMOUNT", "FISCAL_YR", "FISCAL_QTR", "TOBACCO_CLASS_NM", "TOBACCO_CLASS_ID", "COMPANY_TYPE", "INCLUDE_FLAG", "PERMIT_NUM", "MONTH", "TPBD") AS 
  SELECT "COMPANY_NM",
           "COMPANY_EIN",
           "ORIGINAL_COMPANY_NM",
           "ORIGINAL_EIN",
           "TAXES_PAID",
           "REMOVALS_AMOUNT",
           "FISCAL_YR",
           "FISCAL_QTR",
           "TOBACCO_CLASS_NM",
           "TOBACCO_CLASS_ID",
           "COMPANY_TYPE",
           "INCLUDE_FLAG",
           "PERMIT_NUM",
           "MONTH",
           "TPBD"
      FROM (SELECT *
              FROM (  SELECT company_nm,
                             company_ein,
                             NULL                  AS original_company_nm,
                             NULL                  AS original_ein,
                             estimated_tax         AS taxes_paid,
                             qty_removed           AS removals_amount,
                             fiscal_yr,
                             fiscal_qtr,
                             tobacco_class_nm,
                             tobacco_class_id,
                             'IMPT'                AS company_type,
                             NVL (include_flag, 'Y') AS include_flag,
                             NULL                  AS permit_num,
                             month,
                             NULL                  AS TPBD
                        FROM (SELECT --Need to some how get the Consignee NM if associtaion type is CONS and the entry as been associated to a parent
                                    CASE
                                         WHEN c.legal_nm IS NOT NULL
                                         THEN
                                             c.legal_nm
                                         ELSE
                                             cbpdetail.importer_nm
                                     END
                                         AS company_nm,
                                     CASE
                                         WHEN c.ein IS NOT NULL THEN c.ein
                                         ELSE cbpdetail.importer_ein
                                     END
                                         AS company_ein,
                                     cbpentry.fiscal_year AS fiscal_yr,
                                     TO_CHAR (cbpentry.fiscal_qtr)
                                         AS fiscal_qtr,
                                     ttc.tobacco_class_nm,
                                     ttc.tobacco_class_id,
                                     cbpentry.estimated_tax,
                                     cbpentry.qty_removed,
                                     cbpdetail.include_flag AS include_flag,
                                     cbpentry.assigned_dt AS month
                                FROM tu_cbp_importer cbpdetail
                                     JOIN tu_cbp_entry cbpentry
                                         ON cbpdetail.cbp_importer_id =
                                                cbpentry.cbp_importer_id
                                     JOIN tu_tobacco_class ttc
                                         ON cbpentry.tobacco_class_id =
                                                ttc.tobacco_class_id
                                     --JOIN ALL_DELTA_COMPARISON_STS_IMP adcsi ON (cbpdetail.cbp_importer_id=adcsi.cbp_IMPORTER_ID
                                     --                                          AND CBPENTRY.cbp_ENTRY_ID=ADCSI.CBP_ENTRY_ID)
                                     LEFT JOIN tu_company c
                                         ON CASE
                                                WHEN     cbpdetail.importer_ein =
                                                             c.ein
                                                     AND cbpdetail.association_type_cd =
                                                             'IMPT'
                                                THEN
                                                    1
                                                WHEN     cbpdetail.consignee_ein =
                                                             c.ein
                                                     AND cbpdetail.association_type_cd =
                                                             'CONS'
                                                THEN
                                                    1
                                                ELSE
                                                    0
                                            END = 1
                               WHERE     NVL (cbpdetail.include_flag, 'Y') !=
                                             'N'
                                     AND (   cbpentry.excluded_flag IS NULL
                                          OR cbpentry.excluded_flag <> 'Y')
                                     AND (   ttc.tobacco_class_id <> 14
                                          OR ttc.tobacco_class_nm <>
                                                 'Non-Taxable')
                                     -- Excluding any UNKN associations
                                     AND (   cbpdetail.association_type_cd
                                                 IS NULL
                                          OR (    cbpdetail.association_type_cd <>
                                                      'EXCL'
                                              AND cbpdetail.association_type_cd <>
                                                      'UNKN'))
                                     AND NOT EXISTS
                                             (SELECT 'X'
                                                FROM ((SELECT cbp_importer_id
                                                         FROM (  SELECT c1.company_id,
                                                                        c1.legal_nm,
                                                                        SUM (
                                                                            NVL (cbpingested.cbp_taxes_paid,0))
                                                                            AS taxes_paid,
                                                                        cbpingested.fiscal_qtr
                                                                            AS quarter,
                                                                        cbpingested.fiscal_yr,
                                                                        DECODE (
                                                                            cbpingested.tobacco_class_nm,
                                                                            NULL, 'UNKNOWN',
                                                                            cbpingested.tobacco_class_nm)
                                                                            AS tobacco_class_nm,
                                                                        cbpingested.association_type_cd,
                                                                        cbpingested.cbp_importer_id,
                                                                        cbpingested.importer_nm,
                                                                        cbpingested.consignee_nm,
                                                                        CASE
                                                                            WHEN cbpingested.importer_ein IN
                                                                                     (SELECT ein
                                                                                        FROM tu_company)
                                                                            THEN
                                                                                'Y'
                                                                            ELSE
                                                                                'N'
                                                                        END
                                                                            AS imp_in_tufa,
                                                                        CASE
                                                                            WHEN cbpingested.consignee_ein IN
                                                                                     (SELECT ein
                                                                                        FROM tu_company)
                                                                            THEN
                                                                                'Y'
                                                                            ELSE
                                                                                'N'
                                                                        END
                                                                            AS consg_in_tufa,
                                                                        DECODE (
                                                                            cbpingested.importer_ein,
                                                                            c1.ein, 'Y',
                                                                            'N')
                                                                            AS imp_match,
                                                                        DECODE (
                                                                            cbpingested.consignee_ein,
                                                                            c1.ein, 'Y',
                                                                            'N')
                                                                            AS consg_match,
                                                                        cbpingested.importer_ein,
                                                                        cbpingested.consignee_ein
                                                                   FROM tu_annual_trueup
                                                                        a1
                                                                        INNER JOIN
                                                                        (  SELECT cbpdetail.cbp_importer_id,
                                                                                  cbpdetail.importer_nm,
                                                                                  cbpdetail.consignee_nm,
                                                                                  cbpdetail.importer_ein,
                                                                                  cbpdetail.consignee_ein,
                                                                                  cbpdetail.fiscal_yr,
                                                                                  SUM (
                                                                                      NVL (
                                                                                          cbpentry.estimated_tax,
                                                                                          0))
                                                                                      AS cbp_taxes_paid,
                                                                                  ttc.tobacco_class_nm,
                                                                                  ttc.tobacco_class_id
                                                                                      AS tobacco_class_id,
                                                                                  cbpentry.fiscal_qtr
                                                                                      AS fiscal_qtr,
                                                                                  cbpdetail.association_type_cd,
                                                                                  cbpdetail.include_flag
                                                                             FROM tu_cbp_importer
                                                                                  cbpdetail
                                                                                  INNER JOIN
                                                                                  tu_cbp_entry
                                                                                  cbpentry
                                                                                      ON cbpdetail.cbp_importer_id =
                                                                                             cbpentry.cbp_importer_id
                                                                                  LEFT JOIN
                                                                                  tu_tobacco_class
                                                                                  ttc
                                                                                      ON cbpentry.tobacco_class_id =
                                                                                             ttc.tobacco_class_id
                                                                            WHERE --  (CBPENTRY.EXCLUDED_FLAG IS NULL OR CBPENTRY.EXCLUDED_FLAG     <> 'Y')
                        --   CBPENTRY.TOBACCO_CLASS_ID  = TTC.TOBACCO_CLASS_ID
                                       --  AND (TTC.TOBACCO_CLASS_ID     <> 14
                           --  OR TTC.TOBACCO_CLASS_NM       <> 'Non-Taxable')
                 --  AND CBPDETAIL.CBP_IMPORTER_ID  = CBPENTRY.CBP_IMPORTER_ID
                                                                                      cbpdetail.importer_ein <>
                                                                                          cbpdetail.consignee_ein
                                                                                  AND cbpdetail.default_flag =
                                                                                          'N'
                                                                         GROUP BY cbpdetail.cbp_importer_id,
                                                                                  cbpdetail.importer_nm,
                                                                                  cbpdetail.consignee_nm,
                                                                                  cbpdetail.importer_ein,
                                                                                  cbpdetail.consignee_ein,
                                                                                  cbpdetail.fiscal_yr,
                                                                                  ttc.tobacco_class_nm,
                                                                                  ttc.tobacco_class_id,
                                                                                  cbpentry.fiscal_qtr,
                                                                                  cbpdetail.association_type_cd,
                                                                                  cbpdetail.include_flag)
                                                                        cbpingested ON cbpingested.fiscal_yr =
                                                                                   a1.fiscal_yr
                                                                        INNER JOIN
                                                                        tu_company
                                                                        c1
                                                                            ON     (   cbpingested.consignee_ein =
                                                                                           c1.ein
                                                                                    OR cbpingested.importer_ein =
                                                                                           c1.ein)
                                                                               AND (cbpingested.importer_ein <>
                                                                                        cbpingested.consignee_ein)
                                                               GROUP BY c1.company_id,
                                                                        c1.legal_nm,
                                                                        cbpingested.fiscal_qtr,
                                                                        cbpingested.fiscal_yr,
                                                                        cbpingested.tobacco_class_nm,
                                                                        cbpingested.association_type_cd,
                                                                        cbpingested.include_flag,
                                                                        cbpingested.cbp_importer_id,
                                                                        cbpingested.importer_nm,
                                                                        cbpingested.consignee_nm,
                                                                        DECODE (
                                                                            cbpingested.importer_ein,
                                                                            c1.ein, 'Y',
                                                                            'N'),
                                                                        DECODE (
                                                                            cbpingested.consignee_ein,
                                                                            c1.ein, 'Y',
                                                                            'N'),
                                                                        cbpingested.importer_ein,
                                                                        cbpingested.consignee_ein
                                                               ORDER BY c1.company_id)
                                                              PIVOT
                                                                  (MAX (
                                                                       taxes_paid)
                                                                  FOR tobacco_class_nm
                                                                  IN ('Cigars' AS taxes_cigars,
                                                                     'Pipe' AS taxes_pipe,
                                                                     'Snuff' AS taxes_snuff,
                                                                     'Chew' AS taxes_chew,
                                                                     'Cigarettes' AS taxes_cigs,
                                                                     'Roll-Your-Own' AS taxes_ryo,
                                                                     'UNKNOWN' AS taxes_unknown))
                                                        WHERE     association_type_cd
                                                                      IS NULL
                                                              AND NVL (
                                                                      include_flag,
                                                                      'Y') !=
                                                                      'N') -----------------------------------------------------
                                                                          ) acadv
                                               WHERE acadv.cbp_importer_id =
                                                         cbpdetail.cbp_importer_id))
                    ORDER BY TO_NUMBER (fiscal_qtr, '99'),
                             TO_DATE (month, 'mm') -- CTPTUFA-4589 Months for removal amount must display in the same order as the Reported Taxes, modified by Kalpana
                                                  )
            UNION ALL
            SELECT *
              FROM (  SELECT company_nm,
                             company_ein,
                             original_company_nm,
                             original_company_ein,
                             estimated_tax         AS taxes_paid,
                             qty_removed           AS removals_amount,
                             fiscal_yr,
                             TO_CHAR (fiscal_qtr)  AS fiscal_qtr_char,
                             tobacco_class_nm,
                             tobacco_class_id,
                             'IMPT'                AS company_type,
                             NVL (include_flag, 'Y') AS include_flag,
                             NULL                  AS permit_num,
                             month,
                             NULL                  AS TPBD
                        FROM (SELECT --Need to some how get the Consignee NM if associtaion type is CONS and the entry as been associated to a parent
                                    NVL (c2.legal_nm,
                                         cbpdetailparent.importer_nm)
                                         AS company_nm,
                                     NVL (c2.ein, cbpdetail.importer_ein)
                                         AS company_ein,
                                     NVL (c.legal_nm, cbpdetail.importer_nm)
                                         AS original_company_nm,
                                     NVL (c.ein, cbpdetail.importer_ein)
                                         AS original_company_ein,
                                     cbpentry.fiscal_year AS fiscal_yr,
                                     cbpentry.fiscal_qtr  AS fiscal_qtr,
                                     ttc.tobacco_class_nm,
                                     ttc.tobacco_class_id,
                                     cbpentry.estimated_tax,
                                     cbpentry.qty_removed,
                                     cbpdetail.include_flag AS include_flag,
                                     cbpentry.assigned_dt AS month
                                FROM tu_cbp_importer cbpdetail
                                     JOIN tu_cbp_entry cbpentry
                                         ON cbpdetail.cbp_importer_id =
                                                cbpentry.original_cbp_importer_id
                                     JOIN tu_cbp_importer cbpdetailparent
                                         ON cbpentry.cbp_importer_id =
                                                cbpdetailparent.cbp_importer_id
                                     JOIN tu_tobacco_class ttc
                                         ON cbpentry.tobacco_class_id =
                                                ttc.tobacco_class_id
                                     LEFT JOIN tu_company c
                                         ON CASE
                                                WHEN     cbpdetail.importer_ein =
                                                             c.ein
                                                     AND cbpdetail.association_type_cd =
                                                             'IMPT'
                                                THEN
                                                    1
                                                WHEN     cbpdetail.consignee_ein =
                                                             c.ein
                                                     AND cbpdetail.association_type_cd =
                                                             'CONS'
                                                THEN
                                                    1
                                                ELSE
                                                    0
                                            END = 1
                                     LEFT JOIN tu_company c2
                                         ON CASE
                                                WHEN     cbpdetailparent.importer_ein =
                                                             c2.ein
                                                     AND cbpdetailparent.association_type_cd =
                                                             'IMPT'
                                                THEN
                                                    1
                                                WHEN     cbpdetailparent.consignee_ein =
                                                             c2.ein
                                                     AND cbpdetailparent.association_type_cd =
                                                             'CONS'
                                                THEN
                                                    1
                                                ELSE
                                                    0
                                            END = 1
                               WHERE     NVL (cbpdetail.include_flag, 'Y') !=
                                             'N'
                                     AND (   cbpentry.excluded_flag IS NULL
                                          OR cbpentry.excluded_flag <> 'Y')
                                     AND (   ttc.tobacco_class_id <> 14
                                          OR ttc.tobacco_class_nm <>
                                                 'Non-Taxable')
                                     -- Excluding any UNKN associations
                                     AND (   cbpdetail.association_type_cd
                                                 IS NULL
                                          OR (    cbpdetail.association_type_cd <>
                                                      'EXCL'
                                              AND cbpdetail.association_type_cd <>
                                                      'UNKN'))
                                     AND NOT EXISTS
                                             (SELECT 'X'
                                                FROM ( ---------------------------------------
                                                      (SELECT cbp_importer_id
                                                         FROM (  SELECT c1.company_id,
                                                                        c1.legal_nm,
                                                                        SUM (
                                                                            NVL (
                                                                                cbpingested.cbp_taxes_paid,
                                                                                0))
                                                                            AS taxes_paid,
                                                                        cbpingested.fiscal_qtr
                                                                            AS quarter,
                                                                        cbpingested.fiscal_yr,
                                                                        DECODE (
                                                                            cbpingested.tobacco_class_nm,
                                                                            NULL, 'UNKNOWN',
                                                                            cbpingested.tobacco_class_nm)
                                                                            AS tobacco_class_nm,
                                                                        cbpingested.association_type_cd,
                                                                        cbpingested.cbp_importer_id,
                                                                        cbpingested.importer_nm,
                                                                        cbpingested.consignee_nm,
                                                                        CASE
                                                                            WHEN cbpingested.importer_ein IN
                                                                                     (SELECT ein
                                                                                        FROM tu_company)
                                                                            THEN
                                                                                'Y'
                                                                            ELSE
                                                                                'N'
                                                                        END
                                                                            AS imp_in_tufa,
                                                                        CASE
                                                                            WHEN cbpingested.consignee_ein IN
                                                                                     (SELECT ein
                                                                                        FROM tu_company)
                                                                            THEN
                                                                                'Y'
                                                                            ELSE
                                                                                'N'
                                                                        END
                                                                            AS consg_in_tufa,
                                                                        DECODE (
                                                                            cbpingested.importer_ein,
                                                                            c1.ein, 'Y',
                                                                            'N')
                                                                            AS imp_match,
                                                                        DECODE (
                                                                            cbpingested.consignee_ein,
                                                                            c1.ein, 'Y',
                                                                            'N')
                                                                            AS consg_match,
                                                                        cbpingested.importer_ein,
                                                                        cbpingested.consignee_ein
                                                                   FROM tu_annual_trueup
                                                                        a1
                                                                        INNER JOIN
                                                                        (  SELECT cbpdetail.cbp_importer_id,
                                                                                  cbpdetail.importer_nm,
                                                                                  cbpdetail.consignee_nm,
                                                                                  cbpdetail.importer_ein,
                                                                                  cbpdetail.consignee_ein,
                                                                                  cbpdetail.fiscal_yr,
                                                                                  SUM (
                                                                                      NVL (
                                                                                          cbpentry.estimated_tax,
                                                                                          0))
                                                                                      AS cbp_taxes_paid,
                                                                                  ttc.tobacco_class_nm,
                                                                                  ttc.tobacco_class_id
                                                                                      AS tobacco_class_id,
                                                                                  cbpentry.fiscal_qtr
                                                                                      AS fiscal_qtr,
                                                                                  cbpdetail.association_type_cd
                                                                             FROM tu_cbp_importer
                                                                                  cbpdetail
                                                                                  INNER JOIN
                                                                                  tu_cbp_entry
                                                                                  cbpentry
                                                                                      ON cbpdetail.cbp_importer_id =
                                                                                             cbpentry.cbp_importer_id
                                                                                  LEFT JOIN
                                                                                  tu_tobacco_class
                                                                                  ttc
                                                                                      ON cbpentry.tobacco_class_id =
                                                                                             ttc.tobacco_class_id
                                                                            WHERE --  (CBPENTRY.EXCLUDED_FLAG IS NULL OR CBPENTRY.EXCLUDED_FLAG     <> 'Y')
                        --   CBPENTRY.TOBACCO_CLASS_ID  = TTC.TOBACCO_CLASS_ID
                                       --  AND (TTC.TOBACCO_CLASS_ID     <> 14
                           --  OR TTC.TOBACCO_CLASS_NM       <> 'Non-Taxable')
                 --  AND CBPDETAIL.CBP_IMPORTER_ID  = CBPENTRY.CBP_IMPORTER_ID
                                                                                      cbpdetail.importer_ein <>
                                                                                          cbpdetail.consignee_ein
                                                                                  AND cbpdetail.default_flag =
                                                                                          'N'
                                                                         GROUP BY cbpdetail.cbp_importer_id,
                                                                                  cbpdetail.importer_nm,
                                                                                  cbpdetail.consignee_nm,
                                                                                  cbpdetail.importer_ein,
                                                                                  cbpdetail.consignee_ein,
                                                                                  cbpdetail.fiscal_yr,
                                                                                  ttc.tobacco_class_nm,
                                                                                  ttc.tobacco_class_id,
                                                                                  cbpentry.fiscal_qtr,
                                                                                  cbpdetail.association_type_cd) cbpingested
                                                                            ON cbpingested.fiscal_yr =
                                                                                   a1.fiscal_yr
                                                                        INNER JOIN
                                                                        tu_company
                                                                        c1
                                                                            ON     (   cbpingested.consignee_ein =
                                                                                           c1.ein
                                                                                    OR cbpingested.importer_ein =
                                                                                           c1.ein)
                                                                               AND (cbpingested.importer_ein <>
                                                                                        cbpingested.consignee_ein)
                                                               GROUP BY c1.company_id,
                                                                        c1.legal_nm,
                                                                        cbpingested.fiscal_qtr,
                                                                        cbpingested.fiscal_yr,
                                                                        cbpingested.tobacco_class_nm,
                                                                        cbpingested.association_type_cd,
                                                                        cbpingested.cbp_importer_id,
                                                                        cbpingested.importer_nm,
                                                                        cbpingested.consignee_nm,
                                                                        DECODE (
                                                                            cbpingested.importer_ein,
                                                                            c1.ein, 'Y',
                                                                            'N'),
                                                                        DECODE (
                                                                            cbpingested.consignee_ein,
                                                                            c1.ein, 'Y',
                                                                            'N'),
                                                                        cbpingested.importer_ein,
                                                                        cbpingested.consignee_ein
                                                               ORDER BY c1.company_id)
                                                              PIVOT
                                                                  (MAX (
                                                                       taxes_paid)
                                                                  FOR tobacco_class_nm
                                                                  IN ('Cigars' AS taxes_cigars,
                                                                     'Pipe' AS taxes_pipe,
                                                                     'Snuff' AS taxes_snuff,
                                                                     'Chew' AS taxes_chew,
                                                                     'Cigarettes' AS taxes_cigs,
                                                                     'Roll-Your-Own' AS taxes_ryo,
                                                                     'UNKNOWN' AS taxes_unknown))
                                                        WHERE association_type_cd
                                                                  IS NULL) ---------------------------------------
                                                                          ) acadv
                                               WHERE acadv.cbp_importer_id =
                                                         cbpdetail.cbp_importer_id --AND acadv.association_type_cd IS NULL
                                                                                  ))
                    ORDER BY TO_NUMBER (fiscal_qtr_char, '99'),
                             TO_DATE (month, 'mm') -- CTPTUFA-4589 Months for removal amount must display in the same order as the Reported Taxes, modified by Kalpana
                                                  )
            UNION ALL
            SELECT *
              FROM (  SELECT company_nm,
                             company_ein,
                             NULL                  AS original_company_nm,
                             NULL                  AS original_company_ein,
                             taxes_paid            AS taxes_paid,
                             NULL                  AS removals_amount,
                             fiscal_yr,
                             TO_CHAR (fiscal_qtr)  AS fiscal_qtr_char,
                             tobacco_class_nm,
                             tobacco_class_id,
                             company_type,
                             NVL (include_flag, 'Y') AS include_flag,
                             permit_num,
                             t_month               AS month,
                             tpbd                  AS TPBD
                        FROM (SELECT /*+ NO_INDEX(TTC) */
                                    ttbcmpy.company_nm          AS company_nm,
                                     ttbcmpy.ein_num            AS company_ein,
                                     (NVL (ttbannualtx.ttb_taxes_paid, 0))
                                         AS taxes_paid,
                                     ttbcmpy.fiscal_yr,
                                     ttbannualtx.ttb_calendar_qtr AS fiscal_qtr,
                                     CASE
                                         WHEN ttc.tobacco_class_nm =
                                                  'Chew-and-Snuff'
                                         THEN
                                             'Chew/Snuff'
                                         WHEN ttc.tobacco_class_nm = 'Pipe-RYO'
                                         THEN
                                             'Pipe/Roll-Your-Own'
                                         ELSE
                                             ttc.tobacco_class_nm
                                     END
                                         AS tobacco_class_nm,
                                     ttc.tobacco_class_id,
                                     'MANU'
                                         AS company_type,
                                     NVL (ttbcmpy.include_flag, 'Y')
                                         AS include_flag,
                                     ttbpermit.permit_num       AS permit_num,
                                     --             (
                                     --                 SELECT DISTINCT
                                     --                     ab.permit_num
                                     --                 FROM
                                     --                     tu_ttb_permit ab
                                     --                 WHERE
                                     --                     ab.ttb_company_id = ttbcmpy.ttb_company_id
                                     --             ) AS permit_num,
                                     ttbannualtx.month          AS t_month,
                                     ttbannualtx.tpbd
                                FROM tu_ttb_company   ttbcmpy,
                                     tu_ttb_permit    ttbpermit,
                                     tu_ttb_annual_tax ttbannualtx,
                                     tu_tobacco_class ttc
                               WHERE     NVL (ttbcmpy.include_flag, 'Y') != 'N'
                                     AND ttbannualtx.tobacco_class_id =
                                             ttc.tobacco_class_id
                                     AND ttbannualtx.ttb_permit_id =
                                             ttbpermit.ttb_permit_id
                                     AND ttbpermit.ttb_company_id =
                                             ttbcmpy.ttb_company_id)
                    ORDER BY permit_num,
                             TO_NUMBER (fiscal_qtr_char, '99'),
                             TO_DATE (month, 'mm') -- CTPTUFA-4589 Months for removal amount must display in the same order as the Reported Taxes, modified by Kalpana
                                                  )
            UNION ALL
            SELECT *
              FROM (  SELECT UNIQUE
                             tc.company_nm,
                             tc.ein_num                    AS company_ein,
                             NULL                          AS original_company_nm,
                             NULL                          AS original_company_ein,
                             NULL                          AS taxes_paid,
                             NVL (tvt.pounds, 0)           AS removals_amount,
                             tvt.fiscal_yr,
                             CAST (tvt.qtr AS VARCHAR2 (10)) AS fiscal_qtr_char,
                             tvt.tobacco_class_nm,
                             tvt.tobacco_class_id,
                             'MANU'                        AS company_type,
                             NULL                          AS include_flag,
                             tvt.permit_num                AS permit_num,
                             tvt.month                     AS month,
                             NULL                          AS TPBD
                        FROM (  SELECT CASE
                                           WHEN (  TO_CHAR (
                                                       TO_DATE (trv.period,
                                                                'MM-YY'),
                                                       'Q')
                                                 + 1) <= 4
                                           THEN
                                               (  TO_CHAR (
                                                      TO_DATE (trv.period,
                                                               'MM-YY'),
                                                      'Q')
                                                + 1)
                                           ELSE
                                               1
                                       END
                                           AS "QTR",
                                       REPLACE (trv.ein, '-', '') AS ein,
                                       mtc.tobacco_class_nm,
                                       mtc.tobacco_class_id,
                                       trv.pounds             AS pounds,
                                       trv.fiscal_yr,
                                       trv.id_permit          AS permit_num,
                                       TO_CHAR (TO_DATE (trv.period, 'MM-YY'),
                                                'MON')
                                           AS month
                                  FROM tu_ttb_removals_final trv
                                       LEFT OUTER JOIN
                                       tu_ttbvolume_tobaccoclass tc
                                           ON    trv.product =
                                                     tc.alias_tobacco_class_nm
                                              OR trv.product =
                                                     tc.alias_tobacco_class_nm2
                                              OR trv.product =
                                                     tc.alias_tobacco_class_nm3
                                              OR trv.product =
                                                     tc.alias_tobacco_class_nm4
                                       LEFT OUTER JOIN tu_tobacco_class mtc
                                           ON tc.tobacco_class_nm =
                                                  mtc.tobacco_class_nm
                              ORDER BY fiscal_yr,
                                       ein,
                                       qtr,
                                       product) tvt
                             LEFT JOIN tu_ttb_company tc
                                 ON tvt.ein = tc.ein_num
				 and tvt.fiscal_yr=tc.fiscal_yr
                    ORDER BY permit_num,
                             TO_NUMBER (fiscal_qtr_char, '99'),
                             TO_DATE (month, 'mm') -- CTPTUFA-4589 Months for removal amount must display in the same order as the Reported Taxes, modified by Kalpana
                                                  ))
;

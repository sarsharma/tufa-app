--------------------------------------------------------
--  DDL for View TABLEAU_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TABLEAU_VW" ("LEGAL_NM", "COMPANY_STATUS", "PERMIT_NUM", "PERMIT_CLOSE_DT", "PERMIT_TYPE_CD", "MONTH", "FISCAL_YR", "TOBACCO_CLASS_ID", "REMOVAL_QTY", "TAXES_PAID", "PERIOD_STATUS", "RECON_STATUS", "ZERO_FLAG", "SUBMISSION_DATE", "SOURCE_TYPE", "POSTAL_CD", "STATE") AS 
  SELECT co.legal_nm,
          co.company_status,
          p.permit_num,
          p.close_dt,
          p.permit_type_cd,
          rp.MONTH,
          rp.fiscal_yr,
          details.tobacco_class_id,
          NVL (details.REMOVAL_QTY, 0) AS REMOVAL_QTY,
          NVL (details.TAXES_PAID, 0) AS TAXES_PAID,
          ps.period_status_type_cd,
          ps.recon_status_type_cd,
          pp.zero_flag,
          CASE
             WHEN ps.period_status_type_cd = 'COMP'
             THEN
                TO_CHAR (ps.status_dt, 'DD/MON/YY')
             ELSE
                NULL
          END,
          pp.source_cd,
          ad.postal_cd,
          ad.state
     FROM tu_company co
          INNER JOIN TU_PERMIT p ON co.COMPANY_ID = p.COMPANY_ID
          INNER JOIN TU_PERMIT_PERIOD pp ON p.PERMIT_ID = pp.PERMIT_ID
          INNER JOIN TU_RPT_PERIOD rp ON pp.period_id = rp.period_id
          INNER JOIN TU_PERIOD_STATUS ps
             ON (ps.PERMIT_ID = pp.PERMIT_ID AND ps.PERIOD_ID = pp.PERIOD_ID)
          LEFT OUTER JOIN
          (SELECT sf.permit_id,
                  sf.period_id,
                  fd.tobacco_class_id,
                  NVL (fd.REMOVAL_QTY, 0) AS REMOVAL_QTY,
                  NVL (fd.TAXES_PAID, 0) AS TAXES_PAID
             FROM TU_SUBMITTED_FORM sf, tu_form_detail fd
            WHERE sf.form_id = fd.form_id AND sf.form_type_cd = '3852') details
             ON (    pp.permit_id = details.permit_id
                 AND pp.period_id = details.period_id)
          LEFT OUTER JOIN TU_ADDRESS ad ON co.company_id = ad.company_id
;

--------------------------------------------------------
--  DDL for View TEST1
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TEST1" ("COMPANY_ID", "EIN", "LEGAL_NM", "TOBACCO_CLASS_ID", "TOTTAX", "TOTVOL") AS 
  SELECT impt_manu.company_id, 
       impt_manu.ein, 
       impt_manu.legal_nm, 
       TC5.tobacco_class_id, 
       SUM(impt_manu.TOTTAX) AS TOTTAX, 
       SUM(impt_manu.TOTVOL) AS TOTVOL 
  FROM (SELECT * 
          FROM (SELECT company_id, 
                       ein, 
                       legal_nm, 
                       'IMPT' AS permit_type, 
                       CASE acceptance_flag WHEN 'I' THEN ingesttax 
                                            WHEN 'N' THEN amendtax 
                                            ELSE tufatax END AS tottax, 
                       CASE acceptance_flag WHEN 'I' THEN ingestvol 
                                            WHEN 'N' THEN amendvol 
                                            ELSE TUFAVOL END AS totvol 
                  FROM (SELECT TUFA1.company_id, 
                               TUFA1.ein, 
                               TUFA1.legal_nm, 
                               TUFATAX, 
                               TUFAVOL, 
                               INGESTTAX, 
                               INGESTVOL, 
                               AMENDTAX, 
                               AMENDVOL, 
                               acceptance_flag 
                          FROM (SELECT C1.company_id, 
                                       C1.ein, 
                                       C1.legal_nm, 
                                       SUM(qty) AS TUFAVOL, 
                                       SUM(tax) AS TUFATAX 
                                  FROM tu_company C1, 
                                       tu_permit P1, 
                                       (SELECT PP1.permit_id, 
                                               PP1.period_id, 
                                               NVL(DETAILS1.removal_qty, 0) AS qty, 
                                               NVL(DETAILS1.taxes_paid, 0) AS tax 
                                          FROM tu_permit P2 
                                               INNER JOIN tu_permit_period PP1 
                                                  ON P2.permit_id = PP1.permit_id 
                                               INNER JOIN tu_rpt_period RP1 
                                                  ON PP1.period_id = RP1.period_id 
                                               LEFT OUTER JOIN (SELECT PP2.permit_id, 
                                                                       PP2.period_id, 
                                                                       FD1.removal_qty, 
                                                                       FD1.taxes_paid 
                                                                  FROM tu_permit P3, 
                                                                       tu_permit_period PP2, 
                                                                       tu_rpt_period RP2, 
                                                                       tu_submitted_form SF1, 
                                                                       tu_form_detail FD1, 
                                                                       tu_tobacco_class TC1 
                                                                 WHERE TC1.tobacco_class_nm = 'Cigars' 
                                                                   AND SF1.form_type_cd = '3852' 
                                                                   AND TC1.tobacco_class_id = FD1.tobacco_class_id 
                                                                   AND SF1.form_id = FD1.form_id 
                                                                   AND SF1.permit_id = PP2.permit_id 
                                                                   AND SF1.period_id = PP2.period_id 
                                                                   AND PP2.period_id = RP2.period_id 
                                                                   AND P3.permit_id = PP2.permit_id) DETAILS1 
                                                 ON DETAILS1.permit_id = PP1.permit_id 
                                                    AND DETAILS1.period_id = PP1.period_id 
                                         WHERE P2.permit_type_cd = 'IMPT' 
                                           AND RP1.fiscal_yr = 2016) PERMIT_TAX1 
                                 WHERE (C1.company_id, P1.permit_num) NOT IN (SELECT DISTINCT company_id, 
                                                                                     permit_num 
                                                                                FROM PERMIT_EXCL_STATUS_VW PERMIT_EXCL_STATUS_VW1 
                                                                               WHERE ASSMNT_YR = 2016 + 1 
                                                                                 AND ASSMNT_QTR = 1 
                                                                                 AND EXCL_SCOPE_ID = 2 
                                                                                  OR EXCL_SCOPE_ID = 1 
                                                                                 AND (ASSMNT_YR < 2016 + 1 
                                                                                       OR ASSMNT_QTR <= 1 
                                                                                      AND ASSMNT_YR = 2016 + 1)) 
                                   AND PERMIT_TAX1.permit_id = P1.permit_id 
                                   AND C1.company_id = P1.company_id 
                                 GROUP BY C1.company_id, C1.ein, C1.legal_nm) TUFA1 
                               LEFT OUTER JOIN (SELECT /*+ INDEX(C11 EIN_UK) */ C11.ein, 
                                                       C11.company_id, 
                                                       SUM(NVL(cbpingested.ingest_vol, 0)) AS ingestvol, 
                                                       SUM(NVL(cbpingested.cbp_taxes_paid, 0)) AS ingesttax, 
                                                       cbpingested.tobacco_class_id 
                                                  FROM (SELECT cbpdetail.importer_ein, 
                                                               cbpdetail.consignee_ein, 
                                                               SUM(NVL(cbpentry.estimated_tax, 0)) AS cbp_taxes_paid, 
                                                               SUM(cbpentry.qty_removed * 1000) AS ingest_vol, 
                                                               TTC1.tobacco_class_nm, 
                                                               cbpentry.fiscal_year, 
                                                               cbpdetail.association_type_cd, 
                                                               TTC1.tobacco_class_id AS tobacco_class_id 
                                                          FROM tu_cbp_importer cbpdetail, 
                                                               tu_cbp_entry cbpentry, 
                                                               tu_tobacco_class TTC1 
                                                         WHERE (cbpentry.excluded_flag IS NULL 
                                                                 OR cbpentry.excluded_flag <> 'Y') 
                                                           AND cbpentry.tobacco_class_id = TTC1.tobacco_class_id 
                                    AND (TTC1.tobacco_class_id = 8 
                                     OR TTC1.tobacco_class_nm = 'Cigars') 
                                    AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id 
                                    AND cbpdetail.association_type_cd IS NOT NULL 
                                    AND cbpdetail.fiscal_yr = 2016 GROUP BY cbpdetail.importer_ein, cbpdetail.consignee_ein, TTC1.tobacco_class_nm, cbpentry.fiscal_year, cbpdetail.association_type_cd, TTC1.tobacco_class_id, cbpentry.uom_cd) cbpingested, tu_company C11 WHERE (cbpingested.importer_ein = C11.ein 
                                    AND cbpingested.association_type_cd = 'IMPT' 
                                     OR cbpingested.consignee_ein = C11.ein 
                                    AND cbpingested.association_type_cd = 'CONS') 
                                    AND cbpingested.cbp_taxes_paid != 0 
                                    AND C11.company_id NOT IN (SELECT DISTINCT company_id 
                                                                 FROM PERMIT_EXCL_STATUS_VW PERMIT_EXCL_STATUS_VW2 
                                                                WHERE ASSMNT_YR = 2016 + 1 
                                                                  AND ASSMNT_QTR = 1 
                                                                  AND EXCL_SCOPE_ID = 2 
                                                                   OR EXCL_SCOPE_ID = 1 
                                                                  AND (ASSMNT_YR < 2016 + 1 
                                                                        OR ASSMNT_QTR <= 1 
                                                                       AND ASSMNT_YR = 2016 + 1)) 
                                                 GROUP BY C11.company_id, C11.ein, cbpingested.tobacco_class_id) INGEST1 
                                 ON TUFA1.company_id = INGEST1.company_id 
                               LEFT OUTER JOIN (SELECT CBP_COMPANY_ID AS company_id, 
                                                       AMENDED_TOTAL_TAX AS AMENDTAX, 
                                                       AMENDED_TOTAL_VOLUME * 1000 AS AMENDVOL, 
                                                       CA1.acceptance_flag 
                                                  FROM tu_cbp_amendment CA1, 
                                                       tu_tobacco_class TC2 
                                                 WHERE CA1.tobacco_class_id = TC2.tobacco_class_id 
                                                   AND CA1.fiscal_yr = 2016 
                                                   AND TC2.tobacco_class_nm = 'Cigars' 
                                                   AND CBP_COMPANY_ID NOT IN (SELECT DISTINCT company_id 
                                                                                FROM PERMIT_EXCL_STATUS_VW PERMIT_EXCL_STATUS_VW3 
                                                                               WHERE ASSMNT_YR = 2016 + 1 
                                                                                 AND ASSMNT_QTR = 1 
                                                                                 AND EXCL_SCOPE_ID = 2 
                                                                                  OR EXCL_SCOPE_ID = 1 
                                                                                 AND (ASSMNT_YR < 2016 + 1 
                                                                                       OR ASSMNT_QTR <= 1 
                                                                                      AND ASSMNT_YR = 2016 + 1))) AMEND1 
                                 ON TUFA1.company_id = AMEND1.company_id) V2 
                 WHERE ein NOT IN (SELECT TCADS1.EIN 
                                     FROM tu_comparison_all_delta_sts TCADS1 
                                    WHERE TCADS1.STATUS = 'Excluded' 
                                      AND Delta_type = 'TUFA' 
                                      AND TCADS1.FISCAL_YR = 2016 
                                      AND TCADS1.PERMIT_TYPE = 'IMPT' 
                                      AND TCADS1.TOBACCO_CLASS_NM = 'Cigars')) V1 
        UNION ALL 
        SELECT INGESTED_ONLY.Company_id, 
               INGESTED_ONLY.ein, 
               INGESTED_ONLY.legal_nm, 
               INGESTED_ONLY.permit_type, 
               INGESTED_ONLY.delta, 
               DECODE(DECODE(INGESTED_ONLY.permit_type, 'IMPT', 
               (SELECT DISTINCT uom_cd 
                  FROM TU_CBP_IMPORTER TCI, 
                       TU_CBP_ENTRY CE 
                 WHERE tci.IMPORTER_EIN = INGESTED_ONLY.ein 
                   AND TCI.CBP_IMPORTER_ID = CE.CBP_IMPORTER_ID 
                   AND TCI.FISCAL_YR = CE.FISCAL_YEAR 
                   AND CE.FISCAL_YEAR = INGESTED_ONLY.fiscal_yr 
                   AND ce.tobacco_class_id = INGESTED_ONLY.tobacco_class_id), 0), 'K', DECODE(INGESTED_ONLY.permit_type, 'IMPT', 
               (SELECT SUM(CE.QTY_REMOVED) 
                  FROM TU_CBP_IMPORTER TCI, 
                       TU_CBP_ENTRY CE 
                 WHERE tci.IMPORTER_EIN = INGESTED_ONLY.ein 
                   AND TCI.CBP_IMPORTER_ID = CE.CBP_IMPORTER_ID 
                   AND TCI.FISCAL_YR = CE.FISCAL_YEAR 
                   AND CE.FISCAL_YEAR = INGESTED_ONLY.fiscal_yr 
                   AND ce.tobacco_class_id = INGESTED_ONLY.tobacco_class_id), 0) * 1000, 
               'KG', 
               DECODE(INGESTED_ONLY.permit_type, 'IMPT', 
               (SELECT SUM(CE.QTY_REMOVED) 
                  FROM TU_CBP_IMPORTER TCI, 
                       TU_CBP_ENTRY CE 
                 WHERE tci.IMPORTER_EIN = INGESTED_ONLY.ein 
                   AND TCI.CBP_IMPORTER_ID = CE.CBP_IMPORTER_ID 
                   AND TCI.FISCAL_YR = CE.FISCAL_YEAR 
                   AND CE.FISCAL_YEAR = INGESTED_ONLY.fiscal_yr 
                   AND ce.tobacco_class_id = INGESTED_ONLY.tobacco_class_id), 
               0) * 2.204, 
               0) TOTVOL 
          FROM (SELECT (SELECT c.company_id 
                          FROM tu_company c 
                         WHERE c.ein = TCADS2.ein) AS company_id, 
                       TCADS2.EIN, 
                       (SELECT c.legal_nm 
                          FROM tu_company c 
                         WHERE c.ein = TCADS2.ein) AS legal_nm, 
                       (SELECT ttc1.tobacco_class_id 
                          FROM tu_tobacco_class ttc1 
                         WHERE ttc1.TOBACCO_CLASS_NM = TCADS2.TOBACCO_CLASS_NM) tobacco_class_id, 
                       TCADS2.PERMIT_TYPE, 
                       ABS(TCADS2.Delta) delta, 
                       NULL AS TOTVOL, 
                       TCADS2.FISCAL_YR 
                  FROM tu_comparison_all_delta_sts TCADS2 
                 WHERE TCADS2.STATUS = 'MSReady' 
                   AND Delta_type = 'INGESTED' 
                   AND TCADS2.FISCAL_YR = 2016 
                   AND TCADS2.TOBACCO_CLASS_NM = 'Cigars' 
                   AND TCADS2.ein NOT IN (SELECT DISTINCT ein 
                                            FROM PERMIT_EXCL_STATUS_VW PERMIT_EXCL_STATUS_VW4 
                                           WHERE ASSMNT_YR = 2016 + 1 
                                             AND ASSMNT_QTR = 1 
                                             AND EXCL_SCOPE_ID = 2 
                                              OR EXCL_SCOPE_ID = 1 
                                             AND (ASSMNT_YR < 2016 + 1 
                                                   OR ASSMNT_QTR <= 1 
                                                  AND ASSMNT_YR = 2016 + 1))) INGESTED_ONLY 
        UNION 
        SELECT * 
          FROM (SELECT company_id, 
                       ein, 
                       legal_nm, 
                       'MANU' AS permit_type, 
                       CASE acceptance_flag WHEN 'I' THEN ingesttax 
                                            WHEN 'N' THEN amendtax 
                                            ELSE tufatax END AS tottax, 
                       CASE acceptance_flag WHEN 'I' THEN ingestvol 
                                            WHEN 'N' THEN amendvol 
                                            ELSE tufavol END AS totvol 
                  FROM (SELECT TUFA2.company_id, 
                               TUFA2.ein, 
                               TUFA2.legal_nm, 
                               TUFATAX, 
                               TUFAVOL, 
                               INGESTTAX, 
                               INGESTVOL, 
                               AMENDTAX, 
                               AMENDVOL, 
                               acceptance_flag 
                          FROM (SELECT C2.company_id, 
                                       C2.ein, 
                                       C2.legal_nm, 
                                       SUM(qty) AS TUFAVOL, 
                                       SUM(tax) AS TUFATAX 
                                  FROM tu_company C2, 
                                       tu_permit P4, 
                                       (SELECT PP3.permit_id, 
                                               PP3.period_id, 
                                               NVL(DETAILS2.removal_qty, 0) AS qty, 
                                               NVL(DETAILS2.taxes_paid, 0) AS tax 
                                          FROM tu_permit P5 
                                               INNER JOIN tu_permit_period PP3 
                                                  ON P5.permit_id = PP3.permit_id 
                                               INNER JOIN tu_rpt_period RP3 
                                                  ON PP3.period_id = RP3.period_id 
                                               LEFT OUTER JOIN (SELECT PP4.permit_id, 
                                                                       PP4.period_id, 
                                                                       FD2.removal_qty, 
                                                                       FD2.taxes_paid 
                                                                  FROM tu_permit P6, 
                                                                       tu_permit_period PP4, 
                                                                       tu_rpt_period RP4, 
                                                                       tu_submitted_form SF2, 
                                                                       tu_form_detail FD2, 
                                                                       tu_tobacco_class TC3 
                                                                 WHERE TC3.tobacco_class_nm = 'Cigars' 
                                                                   AND SF2.form_type_cd = '3852' 
                                                                   AND TC3.tobacco_class_id = FD2.tobacco_class_id 
                                                                   AND SF2.form_id = FD2.form_id 
                                                                   AND SF2.permit_id = PP4.permit_id 
                                                                   AND SF2.period_id = PP4.period_id 
                                                                   AND PP4.period_id = RP4.period_id 
                                                                   AND P6.permit_id = PP4.permit_id) DETAILS2 
                                                 ON DETAILS2.permit_id = PP3.permit_id 
                                                    AND DETAILS2.period_id = PP3.period_id 
                                         WHERE P5.permit_type_cd = 'MANU' 
                                           AND RP3.fiscal_yr = 2016) PERMIT_TAX2 
                                 WHERE (C2.company_id, P4.permit_num) NOT IN (SELECT DISTINCT company_id, 
                                                                                     permit_num 
                                                                                FROM PERMIT_EXCL_STATUS_VW PERMIT_EXCL_STATUS_VW5 
                                                                               WHERE ASSMNT_YR = 2016 + 1 
                                                                                 AND ASSMNT_QTR = 1 
                                                                                 AND EXCL_SCOPE_ID = 2 
                                                                                  OR EXCL_SCOPE_ID = 1 
                                                                                 AND (ASSMNT_YR < 2016 + 1 
                                                                                       OR ASSMNT_QTR <= 1 
                                                                                      AND ASSMNT_YR = 2016 + 1)) 
                                   AND PERMIT_TAX2.permit_id = P4.permit_id 
                                   AND C2.company_id = P4.company_id 
                                 GROUP BY C2.company_id, C2.ein, C2.legal_nm) TUFA2 
                               LEFT OUTER JOIN (SELECT C4.company_id, 
                                                       C4.ein, 
                                                       ingestvol, 
                                                       ingesttax 
                                                  FROM (SELECT ein_num, 
                                                               tobacco_class_id, 
                                                               ingestvol, 
                                                               ingesttax 
                                                          FROM (SELECT ttbcmpy.ein_num, 
                                                                       TTC2.tobacco_class_id, 
                                                                       0 AS ingestvol, 
                                                                       SUM(NVL(ttb_taxes_paid, 0)) AS ingesttax 
                                                                  FROM tu_ttb_company ttbcmpy, 
                                                                       tu_ttb_permit ttbpermit, 
                                                                       tu_ttb_annual_tax ttbannualtx, 
                                                                       tu_tobacco_class TTC2 
                                                                 WHERE TTC2.tobacco_class_id = 8 
                                                                   AND ttbcmpy.fiscal_yr = 2016 
                                                                   AND ttbpermit.permit_num IN (SELECT P7.permit_num 
                                                                                                  FROM tu_permit P7, 
                                                                                                       tu_company C3 
                                                                                                 WHERE C3.ein = ttbcmpy.ein_num 
                                                                                                   AND P7.company_id = C3.company_id) 
                                                                   AND ttbannualtx.tobacco_class_id = TTC2.tobacco_class_id 
                                    AND ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id 
                                    AND ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id GROUP BY ttbcmpy.ein_num, TTC2.tobacco_class_id) V5) ttbingested, tu_company C4 WHERE C4.company_id NOT IN (SELECT DISTINCT company_id 
                                                                                                                                                                                                        FROM PERMIT_EXCL_STATUS_VW PERMIT_EXCL_STATUS_VW6 
                                                                                                                                                                                                       WHERE ASSMNT_YR = 2016 + 1 
                                                                                                                                                                                                         AND ASSMNT_QTR = 1 
                                                                                                                                                                                                         AND EXCL_SCOPE_ID = 2 
                                                                                                                                                                                                          OR EXCL_SCOPE_ID = 1 
                                                                                                                                                                                                         AND (ASSMNT_YR < 2016 + 1 
                                                                                                                                                                                                               OR ASSMNT_QTR <= 1 
                                                                                                                                                                                                              AND ASSMNT_YR = 2016 + 1)) 
                                                   AND ttbingested.ein_num = C4.ein) INGEST2 
                                 ON TUFA2.company_id = INGEST2.company_id 
                               LEFT OUTER JOIN (SELECT TTB_COMPANY_ID AS company_id, 
                                                       AMENDED_TOTAL_TAX AS AMENDTAX, 
                                                       AMENDED_TOTAL_VOLUME * 1000 AS AMENDVOL, 
                                                       CA2.acceptance_flag 
                                                  FROM tu_ttb_amendment CA2, 
                                                       tu_tobacco_class TC4 
                                                 WHERE CA2.tobacco_class_id = TC4.tobacco_class_id 
                                                   AND CA2.fiscal_yr = 2016 
                                                   AND TC4.tobacco_class_nm = 'Cigars' 
                                                   AND TTB_COMPANY_ID NOT IN (SELECT DISTINCT company_id 
                                                                                FROM PERMIT_EXCL_STATUS_VW PERMIT_EXCL_STATUS_VW7 
                                                                               WHERE ASSMNT_YR = 2016 + 1 
                                                                                 AND ASSMNT_QTR = 1 
                                                                                 AND EXCL_SCOPE_ID = 2 
                                                                                  OR EXCL_SCOPE_ID = 1 
                                                                                 AND (ASSMNT_YR < 2016 + 1 
                                                                                       OR ASSMNT_QTR <= 1 
                                                                                      AND ASSMNT_YR = 2016 + 1))) AMEND2 
                                 ON TUFA2.company_id = AMEND2.company_id) V4 
                 WHERE ein NOT IN (SELECT TCADS3.EIN 
                                     FROM tu_comparison_all_delta_sts TCADS3 
                                    WHERE TCADS3.STATUS = 'Excluded' 
                                      AND Delta_type = 'TUFA' 
                                      AND TCADS3.FISCAL_YR = 2016 
                                      AND TCADS3.PERMIT_TYPE = 'MANU' 
                                      AND TCADS3.TOBACCO_CLASS_NM = 'Cigars')) V3) impt_manu, 
       tu_tobacco_class TC5 
 WHERE TC5.tobacco_class_nm = 'Cigars' 
 GROUP BY impt_manu.company_id, impt_manu.ein, impt_manu.legal_nm, TC5.tobacco_class_id 
 ORDER BY TOTTAX DESC
;

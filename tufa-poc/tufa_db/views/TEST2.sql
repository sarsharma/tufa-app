--------------------------------------------------------
--  DDL for View TEST2
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TEST2" ("COMPANY_ID", "EIN", "LEGAL_NM", "TOBACCO_CLASS_ID", "TOTTAX", "TOTVOL") AS 
  SELECT impt_manu.company_id,impt_manu.ein,impt_manu.legal_nm, tc.tobacco_class_id, SUM(impt_manu.TOTTAX) as TOTTAX, SUM(impt_manu.TOTVOL) as TOTVOL
    FROM

      (
      SELECT *
      FROM
      (SELECT
        company_id,ein,legal_nm,
        'IMPT' as permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            CASE ( acceptance_flag )
                WHEN 'I'   THEN ingesttax
                WHEN 'N'   THEN amendtax
                ELSE tufatax
            END AS tottax,
            CASE (acceptance_flag)
                WHEN 'I'   THEN ingestvol
                WHEN 'N'   THEN amendvol
                ELSE TUFAVOL
            END AS totvol
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      FROM
        (SELECT tufa.company_id,tufa.ein,tufa.legal_nm, TUFATAX, TUFAVOL, INGESTTAX, INGESTVOL, AMENDTAX, AMENDVOL, acceptance_flag
          FROM
            (SELECT c.company_id,c.ein,c.legal_nm, SUM(qty) as TUFAVOL, SUM(tax) as TUFATAX
              FROM tu_company c
              INNER JOIN tu_permit p on c.company_id = p.company_id
              INNER JOIN
                (select pp.permit_id, pp.period_id, NVL(details.removal_qty, 0) as qty, NVL(details.taxes_paid, 0) as tax
                  FROM tu_permit p
                  INNER JOIN tu_permit_period pp on p.permit_id = pp.permit_id
                  INNER JOIN tu_rpt_period rp on pp.period_id = rp.period_id
                  LEFT OUTER JOIN
                    (select pp.permit_id, pp.period_id, fd.removal_qty, fd.taxes_paid
                      FROM tu_permit p
                      INNER JOIN tu_permit_period pp on p.permit_id = pp.permit_id
                      INNER JOIN tu_rpt_period rp on pp.period_id = rp.period_id
                      INNER JOIN tu_submitted_form sf on (sf.permit_id = pp.permit_id and sf.period_id = pp.period_id)
                      INNER JOIN tu_form_detail fd on sf.form_id = fd.form_id
                      INNER JOIN tu_tobacco_class tc on tc.tobacco_class_id = fd.tobacco_class_id
                      WHERE tc.tobacco_class_nm = 'Cigars'
                      AND sf.form_type_cd = '3852') details on details.permit_id = pp.permit_id AND details.period_id = pp.period_id
                  WHERE p.permit_type_cd = 'IMPT'
                  -------------------------Added below Code as part of Story CTPTUFA-4834------------- 
                 
                                                                             
------------------------------------------------------------------------------------
                  AND rp.fiscal_yr = 2016) permit_tax on permit_tax.permit_id = p.permit_id
                  where 
-------------------------Added below Code as part of Story CTPTUFA-4834------------- 
                  (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                  from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= 2016+1
                                        and   ASSMNT_QTR=1
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR <2016+1
                                            or 
                                           (ASSMNT_QTR <=1 
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=2016+1)
                                          )
                                        )
                                   )                                                                  
------------------------------------------------------------------------------------
              GROUP BY c.company_id,c.ein,c.legal_nm) tufa
---------------------- START JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    c1.ein,
                    c1.company_id,
                    SUM(nvl(cbpingested.ingest_vol,0) ) AS ingestvol,
                    SUM(nvl(cbpingested.cbp_taxes_paid,0) ) AS ingesttax,
                    cbpingested.tobacco_class_id
                FROM
                    (
                        SELECT
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            SUM(nvl(cbpentry.estimated_tax,0) ) AS cbp_taxes_paid,
                            SUM(cbpentry.qty_removed * 1000) AS ingest_vol,
                            ttc.tobacco_class_nm,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id   AS tobacco_class_id
                        FROM
                            tu_cbp_importer cbpdetail,
                            tu_cbp_entry cbpentry,
                            tu_tobacco_class ttc
                        WHERE
                            ( cbpentry.excluded_flag IS NULL
                              OR cbpentry.excluded_flag <> 'Y' )
                            AND cbpentry.tobacco_class_id = ttc.tobacco_class_id
                            AND ( ttc.tobacco_class_id = 8
                                  OR ttc.tobacco_class_nm = 'Cigars' )
                            AND cbpdetail.cbp_importer_id = cbpentry.cbp_importer_id
                            AND cbpdetail.association_type_cd IS NOT NULL
                            AND cbpdetail.fiscal_yr =2016
                        GROUP BY
                            cbpdetail.importer_ein,
                            cbpdetail.consignee_ein,
                            ttc.tobacco_class_nm,
                            cbpentry.fiscal_year,
                            cbpdetail.association_type_cd,
                            ttc.tobacco_class_id,
                            cbpentry.uom_cd
                    ) cbpingested,
                    tu_company c1
                WHERE
                    ( cbpingested.importer_ein = c1.ein
                      AND cbpingested.association_type_cd = 'IMPT'
                      OR cbpingested.consignee_ein = c1.ein
                      AND cbpingested.association_type_cd = 'CONS' )
                    AND cbpingested.cbp_taxes_paid != 0
                    AND c1.company_id not in 
                    ------------------------------------------------------------
                                (    Select distinct company_id
                                     from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= 2016+1
                                        and   ASSMNT_QTR=1
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR <2016+1
                                            or 
                                           (ASSMNT_QTR <=1 
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=2016+1)
                                          )
                                        )
                                   )
                    ------------------------------------------------------------
                GROUP BY
                    c1.company_id,
                    c1.ein,
                    cbpingested.tobacco_class_id
            ) ingest ON tufa.company_id = ingest.company_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

            LEFT OUTER JOIN (
              SELECT CBP_COMPANY_ID as company_id, AMENDED_TOTAL_TAX as AMENDTAX, AMENDED_TOTAL_VOLUME * 1000 as AMENDVOL, ca.acceptance_flag
              FROM tu_cbp_amendment ca, tu_tobacco_class tc
              WHERE ca.tobacco_class_id = tc.tobacco_class_id
              and ca.fiscal_yr=2016
              and tc.tobacco_class_nm = 'Cigars'
              and CBP_COMPANY_ID not in 
                    ------------------------------------------------------------
                                (    Select distinct company_id
                                     from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= 2016+1
                                        and   ASSMNT_QTR=1
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR <2016+1
                                            or 
                                           (ASSMNT_QTR <=1 
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=2016+1)
                                          )
                                        )
                                   )
                    ------------------------------------------------------------

              ) amend on tufa.company_id = amend.company_id
              )
                          ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
            WHERE (ein) NOT IN
            (
             SELECT tcads.EIN
             from tu_comparison_all_delta_sts tcads
             where tcads.STATUS='Excluded'
             and Delta_type='TUFA'
             and tcads.FISCAL_YR=2016
             AND tcads.PERMIT_TYPE='IMPT'
             and tcads.TOBACCO_CLASS_NM ='Cigars'
           )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------

        )

      UNION All
     ----------------------------------------BEGIN INGESTED ONLY added on 16 oct 2018-----------------------------------------------------

              Select INGESTED_ONLY.Company_id,INGESTED_ONLY.ein,INGESTED_ONLY.legal_nm, --,INGESTED_ONLY.tobacco_class_id,
              INGESTED_ONLY.permit_type,INGESTED_ONLY.delta,
              (Decode(
         (decode (INGESTED_ONLY.permit_type,'IMPT',(SELECT distinct uom_cd FROM TU_CBP_IMPORTER TCI,TU_CBP_ENTRY CE
        WHERE tci.IMPORTER_EIN=INGESTED_ONLY.ein
        and TCI.CBP_IMPORTER_ID=CE.CBP_IMPORTER_ID
        AND TCI.FISCAL_YR=CE.FISCAL_YEAR
        and CE.FISCAL_YEAR=INGESTED_ONLY.fiscal_yr
        and ce.tobacco_class_id=INGESTED_ONLY.tobacco_class_id),0))
        ,'K',
        (decode (INGESTED_ONLY.permit_type,'IMPT',(SELECT sum(CE.QTY_REMOVED)
        FROM TU_CBP_IMPORTER TCI,TU_CBP_ENTRY CE
        WHERE tci.IMPORTER_EIN=INGESTED_ONLY.ein
        and TCI.CBP_IMPORTER_ID=CE.CBP_IMPORTER_ID
        AND TCI.FISCAL_YR=CE.FISCAL_YEAR
        and CE.FISCAL_YEAR=INGESTED_ONLY.fiscal_yr
        and ce.tobacco_class_id=INGESTED_ONLY.tobacco_class_id),0))*1000
        ,'KG'
        ,(decode (INGESTED_ONLY.permit_type,'IMPT',(SELECT sum(CE.QTY_REMOVED)
        FROM TU_CBP_IMPORTER TCI,TU_CBP_ENTRY CE
        WHERE tci.IMPORTER_EIN=INGESTED_ONLY.ein
        and TCI.CBP_IMPORTER_ID=CE.CBP_IMPORTER_ID
        AND TCI.FISCAL_YR=CE.FISCAL_YEAR
        and CE.FISCAL_YEAR=INGESTED_ONLY.fiscal_yr
        and ce.tobacco_class_id=INGESTED_ONLY.tobacco_class_id),0))*2.204,
        0))  TOTVOL
              from (
               SELECT (Select c.company_id from tu_company c where c.ein=tcads.ein) as company_id,
               tcads.EIN,
                (Select c.legal_nm from tu_company c where c.ein=tcads.ein) as legal_nm ,
               (Select ttc1.tobacco_class_id from tu_tobacco_class ttc1 where ttc1.TOBACCO_CLASS_NM=tcads.TOBACCO_CLASS_NM) tobacco_class_id,
               --tcads.TOBACCO_CLASS_NM,
               tcads.PERMIT_TYPE,
               ABS(tcads.Delta) delta,
                null as TOTVOL,
                tcads.FISCAL_YR
             from tu_comparison_all_delta_sts tcads where tcads.STATUS='MSReady' and Delta_type='INGESTED'
             and tcads.FISCAL_YR=2016
             and tcads.TOBACCO_CLASS_NM = 'Cigars'
-------------------------Added below Code as part of Story CTPTUFA-4834------------- 
              and (tcads.ein) not in 
                                  (    Select distinct ein
                                  from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= 2016+1
                                        and   ASSMNT_QTR=1
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2)
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR <2016+1
                                            or 
                                           (ASSMNT_QTR <=1 
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=2016+1)
                                          )
                                        )
                                   )                                                                  
------------------------------------------------------------------------------------

             ) INGESTED_ONLY


    ----------------------------------------END INGESTED ONLY added on 16 oct 2018-----------------------------------------------------

      UNION
     SELECT *
     FROM
     (
       SELECT
        company_id,ein,legal_nm,
        'MANU' as permit_type,
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            CASE ( acceptance_flag )
                WHEN 'I'   THEN ingesttax
                WHEN 'N'   THEN amendtax
                ELSE tufatax
            END AS tottax,
            CASE (acceptance_flag)
                WHEN 'I'   THEN ingestvol
                WHEN 'N'   THEN amendvol
                ELSE tufavol
            END AS totvol
---------------------- Start Select of Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

      FROM
        (SELECT tufa.company_id,tufa.ein,tufa.legal_nm, TUFATAX, TUFAVOL, INGESTTAX, INGESTVOL, AMENDTAX, AMENDVOL, acceptance_flag
          FROM
            (SELECT c.company_id,c.ein,c.legal_nm, SUM(qty) as TUFAVOL, SUM(tax) as TUFATAX
              FROM tu_company c
              INNER JOIN tu_permit p on c.company_id = p.company_id
              INNER JOIN
                (select pp.permit_id, pp.period_id, NVL(details.removal_qty, 0) as qty, NVL(details.taxes_paid, 0) as tax
                  FROM tu_permit p
                  INNER JOIN tu_permit_period pp on p.permit_id = pp.permit_id
                  INNER JOIN tu_rpt_period rp on pp.period_id = rp.period_id
                  LEFT OUTER JOIN
                    (select pp.permit_id, pp.period_id, fd.removal_qty, fd.taxes_paid
                      FROM tu_permit p
                      INNER JOIN tu_permit_period pp on p.permit_id = pp.permit_id
                      INNER JOIN tu_rpt_period rp on pp.period_id = rp.period_id
                      INNER JOIN tu_submitted_form sf on (sf.permit_id = pp.permit_id and sf.period_id = pp.period_id)
                      INNER JOIN tu_form_detail fd on sf.form_id = fd.form_id
                      INNER JOIN tu_tobacco_class tc on tc.tobacco_class_id = fd.tobacco_class_id
                      WHERE tc.tobacco_class_nm = 'Cigars'
                      AND sf.form_type_cd = '3852') details on details.permit_id = pp.permit_id AND details.period_id = pp.period_id
                  WHERE p.permit_type_cd = 'MANU'
                  AND rp.fiscal_yr = 2016) permit_tax on permit_tax.permit_id = p.permit_id
                  -------------------------Added below Code as part of Story CTPTUFA-4835------------- 
                 WHERE  (c.company_id,p.permit_num) not in 
                                  (    Select distinct company_id,permit_num
                                  from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= 2016+1
                                        and   ASSMNT_QTR=1
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR <2016+1
                                            or 
                                           (ASSMNT_QTR <=1 
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=2016+1)
                                          )
                                        )
                                   )                                                                  
------------------------------------------------------------------------------------

              GROUP BY c.company_id,c.ein,c.legal_nm) tufa
---------------------- START JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------
            LEFT OUTER JOIN (
                SELECT
                    c.company_id,
                    c.ein,
                    ingestvol,
                    ingesttax
                FROM
                    (
                        SELECT
                            ein_num,
                            tobacco_class_id,
                            ingestvol,
                            ingesttax
                        FROM
                            (
                                SELECT
                                    ttbcmpy.ein_num,
                                    ttc.tobacco_class_id,
                                    0 AS ingestvol,
                                    SUM(nvl(ttb_taxes_paid,0) ) AS ingesttax
                                FROM
                                    tu_ttb_company ttbcmpy
                                    INNER JOIN tu_ttb_permit ttbpermit ON ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id
                                    INNER JOIN tu_ttb_annual_tax ttbannualtx ON ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id
                                    INNER JOIN tu_tobacco_class ttc ON ttbannualtx.tobacco_class_id = ttc.tobacco_class_id
                                WHERE
                                    ttc.tobacco_class_id = 8
                                    AND ttbcmpy.fiscal_yr =2016
                                    AND ttbpermit.permit_num IN (
                                        SELECT
                                            p.permit_num
                                        FROM
                                            tu_permit p
                                            JOIN tu_company c ON p.company_id = c.company_id
                                        WHERE
                                            c.ein = ttbcmpy.ein_num
                                    )
                                GROUP BY
                                    ttbcmpy.ein_num,
                                    ttc.tobacco_class_id
                            )
                    ) ttbingested
                    INNER JOIN tu_company c ON ttbingested.ein_num = c.ein
                    WHERE c.company_id not in 
                    ------------------------------------------------------------
                                (    Select distinct company_id
                                     from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= 2016+1
                                        and   ASSMNT_QTR=1
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR <2016+1
                                            or 
                                           (ASSMNT_QTR <=1 
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=2016+1)
                                          )
                                        )
                                   )
                    ------------------------------------------------------------ 
            ) ingest ON tufa.company_id = ingest.company_id
---------------------- END JOIN on Ingested Matched as per 1 March 2019 ---------------------------------------------------------------

          LEFT OUTER JOIN (
              SELECT TTB_COMPANY_ID as company_id, AMENDED_TOTAL_TAX as AMENDTAX, AMENDED_TOTAL_VOLUME * 1000 as AMENDVOL, ca.acceptance_flag
              FROM tu_ttb_amendment ca, tu_tobacco_class tc
              WHERE ca.tobacco_class_id = tc.tobacco_class_id
              and ca.fiscal_yr=2016
              and tc.tobacco_class_nm = 'Cigars'
              and TTB_COMPANY_ID not in 
                    ------------------------------------------------------------
                                (    Select distinct company_id
                                     from PERMIT_EXCL_STATUS_VW 
                                        where 
                                        (ASSMNT_YR= 2016+1
                                        and   ASSMNT_QTR=1
                                        --and ASSMNT_QTR=d.quarter
                                        and (EXCL_SCOPE_ID=2 --or FULL_PERM_EXCL_AUDIT_ID is not null
                                        )
                                        )
------------------------Added below Conditions for full exclusion------------------------                                 
                                        OR
                                        (EXCL_SCOPE_ID=1
                                          and 
                                          (
                                           ASSMNT_YR <2016+1
                                            or 
                                           (ASSMNT_QTR <=1 
                                           --and ASSMNT_QTR<=d.quarter
                                           and ASSMNT_YR=2016+1)
                                          )
                                        )
                                   )
                    ------------------------------------------------------------
            ) amend on tufa.company_id = amend.company_id
         )
      ----------------Beginning of Excluded records for ALl Delta as per 16 Oct 2018---------
          WHERE (ein) NOT IN
            (
             SELECT tcads.EIN
             from tu_comparison_all_delta_sts tcads
             where tcads.STATUS='Excluded'
             and Delta_type='TUFA'
             and tcads.FISCAL_YR=2016
             AND tcads.PERMIT_TYPE='MANU'
             and tcads.TOBACCO_CLASS_NM ='Cigars'
           )
      ----------------End of Excluded records for ALl Delta as per 16 Oct 2018---------

        )
      ) impt_manu,
      tu_tobacco_class tc
      WHERE tc.tobacco_class_nm='Cigars'
      GROUP BY impt_manu.company_id,impt_manu.ein,impt_manu.legal_nm, tc.tobacco_class_id
      ORDER BY TOTTAX DESC
;

--------------------------------------------------------
--  DDL for View TOBACCO_CLASS_SUBMISSIONS_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TOBACCO_CLASS_SUBMISSIONS_VW" ("LEGAL_NM", "PERMIT_NUM", "PERMIT_TYPE", "MONTH", "FISCAL_YR", "TOBACCO_CLASS_NM", "REMOVAL_QTY", "TAXES_PAID", "SUBMISSION_DATE") AS 
  SELECT c.legal_nm,
             p.permit_num,
             p.permit_type_cd,
             rp.month,
             rp.fiscal_yr,
             tc.tobacco_class_nm,
             fd.removal_qty,
             fd.taxes_paid,
             CASE
              when ps.period_status_type_cd = 'COMP'
               then TO_CHAR(ps.status_dt,'DD-MON-YY')
              else null
           END
        FROM tu_submitted_form sf,
             tu_permit        p,
             tu_company       c,
             tu_form_detail   fd,
             tu_tobacco_class tc,
             tu_rpt_period    rp,
             tu_period_status ps
       WHERE     sf.form_id = fd.form_id
             AND sf.permit_id = p.permit_id
             AND p.company_id = c.company_id
             AND fd.tobacco_class_id = tc.tobacco_class_id
             AND sf.PERIOD_ID = rp.period_id
             AND sf.permit_id = ps.permit_id
             AND sf.period_id = ps.period_id
    ORDER BY c.company_id, sf.period_id, fd.tobacco_class_id
;

--------------------------------------------------------
--  DDL for View TTB_PERMIT_LIST_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TTB_PERMIT_LIST_VW" ("AUDIT_ID", "COMPANY_NAME", "COMPANY_ID", "EIN", "PERMIT_ID", "PERMIT_ACTION", "PERMIT_STATUS", "DOC_STAGE_ID", "ISSUE_DT", "CLOSE_DT", "ACTION_MSG", "ACTION", "PERMIT_EXCL_RESOLVED", "ACTION_CONTEXT") AS 
  SELECT AUDIT_ID,
           COMPANY_NAME,
           COMPANY_ID,
           EIN,
           PERMIT_ID,
           PERMIT_ACTION,
           PERMIT_STATUS,
           DOC_STAGE_ID,
           ISSUE_DT,
           CLOSE_DT,
           CASE
               WHEN TTB_P_ACTION.ACTION_ID = 3
               THEN
                   REPLACE (
                       REPLACE (
                           REPLACE (
                               TTB_P_ACTION.ACTION_MSG,
                               '?1',
                                  SUBSTR (PERMIT_ID, 1, 2)
                               || '-'
                               || SUBSTR (PERMIT_ID, 3, 2)
                               || '-'
                               || SUBSTR (PERMIT_ID, 5)),
                           '?2',
                           TUFA_PERMIT_CLS_DT),
                       '?3',
                       CLOSE_DT)
               WHEN TTB_P_ACTION.ACTION_ID = 1
               THEN
                   REPLACE (
                       TTB_P_ACTION.ACTION_MSG,
                       '?1',
                          SUBSTR (PERMIT_ID, 1, 2)
                       || '-'
                       || SUBSTR (PERMIT_ID, 3, 2)
                       || '-'
                       || SUBSTR (PERMIT_ID, 5))
               WHEN TTB_P_ACTION.ACTION_ID = 2
               THEN
                   REPLACE (
                       TTB_P_ACTION.ACTION_MSG,
                       '?1',
                          SUBSTR (PERMIT_ID, 1, 2)
                       || '-'
                       || SUBSTR (PERMIT_ID, 3, 2)
                       || '-'
                       || SUBSTR (PERMIT_ID, 5))
               ELSE
                   TTB_P_ACTION.ACTION_MSG
           END
               AS ACTION_MSG,
           TTB_P_ACTION.ACTION,
           PERMIT_EXCL_RESOLVED,
           TTB_P_ACTION.ACTION_CONTEXT
      FROM (SELECT AUDIT_ID,
                   COMPANY_NAME,
                   COMPANY_ID,
                   EIN,
                   PERMIT_ID,
                   PERMIT_ACTION,
                   PERMIT_STATUS,
                   DOC_STAGE_ID,
                   TO_CHAR (ISSUE_DT, 'MM/DD/YYYY') AS ISSUE_DT,
                   TO_CHAR (CLOSE_DT, 'MM/DD/YYYY') AS CLOSE_DT,
                   TO_CHAR (TUFA_PERMIT_CLS_DT, 'MM/DD/YYYY')
                       AS TUFA_PERMIT_CLS_DT,
                   ACTION_ID,
                   PERMIT_EXCL_RESOLVED
              FROM (SELECT AUDIT_ID,
                           COMPANY_NAME,
                           COMPANY_ID,
                           EIN,
                           PERMIT_ID,
                           PERMIT_ACTION,
                           PERMIT_STATUS,
                           DOC_STAGE_ID,
                           ISSUE_DT,
                           CLOSE_DT,
                           TUFA_PERMIT_CLS_DT,
                           ACTION_ID,
                           PERMIT_EXCL_RESOLVED,
                           MAX (AUDIT_ID)
                               OVER (PARTITION BY DOC_STAGE_ID,
                                                  COMPANY_ID,
                                                  EIN,
                                                  PERMIT_ID)
                               AS MAX_PERMIT_AUDIT_ID
                      FROM TU_PERMIT_UPDATES_AUDIT)
             WHERE AUDIT_ID = MAX_PERMIT_AUDIT_ID) P_UPDATES_AUDIT
           INNER JOIN TU_TTB_PERMIT_ACTION TTB_P_ACTION
               ON TTB_P_ACTION.ACTION_ID = P_UPDATES_AUDIT.ACTION_ID
;

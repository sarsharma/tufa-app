--------------------------------------------------------
--  DDL for View TTB_TOBACCO_CLASSES_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TTB_TOBACCO_CLASSES_VW" ("TTB_COMPANY_ID", "FISCAL_YR", "TOBACCO_CLASSES") AS 
  SELECT TTB_COMPANY_ID, FISCAL_YR,
    LISTAGG(tobacco, ', ') WITHIN GROUP (
  ORDER BY tobacco) AS tobacco_classes
  FROM
    (SELECT UNIQUE ttbcmpy.TTB_COMPANY_ID, ttbcmpy.FISCAL_YR,
      NVL(tc.TOBACCO_CLASS_NM, 'Unknown') tobacco
    FROM TU_TTB_COMPANY ttbcmpy
    INNER JOIN TU_TTB_PERMIT ttbpermit
    ON ttbcmpy.TTB_COMPANY_ID = ttbpermit.TTB_COMPANY_ID
    INNER JOIN TU_TTB_ANNUAL_TAX ttb_annual_tx
    ON ttb_annual_tx.TTB_PERMIT_ID = ttbpermit.TTB_PERMIT_ID
    INNER JOIN TU_TOBACCO_CLASS tc
    ON ttb_annual_tx.TOBACCO_CLASS_ID = tc.TOBACCO_CLASS_ID
    ) classes
  GROUP BY TTB_COMPANY_ID, FISCAL_YR
;

--------------------------------------------------------
--  DDL for View TTB_TOBACCO_PERMITS_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TTB_TOBACCO_PERMITS_VW" ("TTB_PERMIT_ID", "TOBACCO_CLASSES") AS 
  SELECT TTB_PERMIT_ID,
             LISTAGG (tobacco, ', ') WITHIN GROUP (ORDER BY tobacco)
                 AS tobacco_classes
        FROM (SELECT UNIQUE
                     ttbpermit.TTB_PERMIT_ID,
                     NVL (tc.TOBACCO_CLASS_NM, 'Unknown') tobacco
                FROM TU_TTB_PERMIT ttbpermit
                     INNER JOIN TU_TTB_ANNUAL_TAX ttb_annual_tx
                         ON ttb_annual_tx.TTB_PERMIT_ID =
                                ttbpermit.TTB_PERMIT_ID
                     INNER JOIN TU_TOBACCO_CLASS tc
                         ON ttb_annual_tx.TOBACCO_CLASS_ID =
                                tc.TOBACCO_CLASS_ID) classes
    GROUP BY TTB_PERMIT_ID
;

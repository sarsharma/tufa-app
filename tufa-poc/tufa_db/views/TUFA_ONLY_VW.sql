--------------------------------------------------------
--  DDL for View TUFA_ONLY_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TUFA_ONLY_VW" ("EIN", "LEGAL_NM", "TOBACCO_CLASS_NM", "QUARTER", "FISCAL_YR", "COMPANY_TYPE", "TAXES_PAID", "ONLY_SIDE", "ZERO_FLAG", "STATUS") AS 
  SELECT tufa.ein,
          tufa.legal_nm,
          tufa.tobacco_class_nm,
          tufa.quarter,
          tufa.fiscal_year AS fiscal_yr,
          tufa.company_type,
          tufa.taxes_paid,
          'TUFA' AS only_side,
          tufa.zero_flag,
          NVL (
             (SELECT tcads.status
                FROM tu_comparison_all_delta_sts tcads
               WHERE     tcads.ein = tufa.ein
                     AND tcads.fiscal_yr = tufa.fiscal_year
                     AND tcads.fiscal_qtr = tufa.quarter
                     AND tcads.tobacco_class_nm = tufa.tobacco_class_nm
                     AND tcads.permit_type = tufa.company_type),
             'Review')
             AS status
     FROM all_tufa_vw tufa
          LEFT JOIN
          all_ingested_vw ingested
             ON     ingested.company_ein = tufa.ein
                AND ingested.fiscal_year = tufa.fiscal_year
                AND ingested.fiscal_qtr = tufa.quarter
                AND ingested.tobacco_class_nm = tufa.tobacco_class_nm
                AND ingested.company_type = tufa.company_type
    WHERE     ingested.company_nm IS NULL
          AND ABS (tufa.taxes_paid) >= 1
          AND tufa.zero_flag = 'N'
;

--------------------------------------------------------
--  DDL for View TU_ASSESSMENT_REPORT_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TU_ASSESSMENT_REPORT_VW" ("PERIOD_ID", "MONTH", "YEAR", "FISCAL_YR", "QUARTER", "PERIOD_STATUS_TYPE_CD", "LEGAL_NM", "COMPANY_ID", "PERMIT_ID", "PERMIT_NUM", "PERMIT_TYPE_CD", "PERMIT_STATUS_TYPE_CD", "COMPANY_STATUS", "FORM_ID", "FORM_TYPE_CD") AS 
  SELECT rp.period_id,
  rp.month,
  rp.year,
  rp.fiscal_yr,
  rp.QUARTER,
  ps.PERIOD_STATUS_TYPE_CD,
  co.LEGAL_NM,
  co.company_id,
  p.permit_id,
  p.permit_num,
  p.permit_type_cd,
  p.permit_status_type_cd,
  co.COMPANY_STATUS,
  sf.form_id,
  sf.form_type_cd
FROM TU_PERMIT p,
  TU_PERMIT_PERIOD pp,
  TU_RPT_PERIOD rp,
  TU_PERIOD_STATUS ps,
  TU_COMPANY co,
  tu_submitted_form sf
WHERE p.PERMIT_ID           = pp.PERMIT_ID
AND rp.PERIOD_ID            = pp.PERIOD_ID
AND co.COMPANY_ID           = p.COMPANY_ID
AND (pp.permit_id           = ps.permit_id
AND pp.period_id            = ps.period_id)
and (sf.period_id            = pp.period_id
and sf.permit_id            = pp.permit_id)
ORDER BY p.permit_num ASC
;

--------------------------------------------------------
--  DDL for View TU_CBP_INGESTED_EXCL_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TU_CBP_INGESTED_EXCL_VW" ("COMPANY_ID", "EIN_NUM", "COMPANY_NM", "ASMT_YR", "ASMT_QTR", "CBP_YEAR", "CBP_QTR", "CBP_ENTRY_ID", "PERM_EXCL_AUDIT_ID", "CREATED_DT", "MODIFIED_DT", "CREATED_BY", "MODIFIED_BY") AS 
  SELECT tc.company_id,
           cbpimporter.importer_ein           AS EIN_NUM,
           cbpimporter.importer_nm            AS COMPANY_NM,
           CASE
               WHEN     cbpentry.fiscal_qtr = 4
                    AND cbpentry.tobacco_class_id <> 8
               THEN
                   cbpentry.fiscal_year + 1
               WHEN cbpentry.tobacco_class_id = 8
               THEN
                   cbpentry.fiscal_year + 1
               ELSE
                   cbpentry.fiscal_year
           END
               AS ASMT_YR,
           CASE
               WHEN     cbpentry.fiscal_qtr = 4
                    AND cbpentry.tobacco_class_id <> 8
               THEN
                   1
               WHEN cbpentry.tobacco_class_id = 8
               THEN
                   cbpentry.fiscal_qtr
               ELSE
                   cbpentry.fiscal_qtr + 1
           END
               AS asmt_qtr,
           cbpentry.fiscal_year               AS cbp_year,
           cbpentry.fiscal_qtr                AS cbp_qtr,
           cbp_ingest_excl.cbp_entry_id       AS CBP_ENTRY_ID,
           cbp_ingest_excl.perm_excl_audit_id AS PERM_EXCL_AUDIT_ID,
           cbp_ingest_excl.created_dt         AS created_dt,
           cbp_ingest_excl.modified_dt        AS modified_dt,
           cbp_ingest_excl.created_by         AS created_by,
           cbp_ingest_excl.modified_by        AS modified_by
      FROM tu_cbp_entry  cbpentry
           INNER JOIN tu_cbp_importer cbpimporter
               ON cbpimporter.cbp_importer_id = cbpentry.cbp_importer_id
           LEFT JOIN tu_company tc ON tc.ein = cbpimporter.importer_ein
           INNER JOIN
           (SELECT cbp_entry_id,
                   perm_excl_audit_id,
                   cbp_ingested_excl_id,
                   created_dt,
                   created_by,
                   modified_dt,
                   modified_by
              FROM (SELECT cbp_entry_id,
                           perm_excl_audit_id,
                           cbp_ingested_excl_id,
                           created_dt,
                           created_by,
                           modified_dt,
                           modified_by,
                           MAX (cbp_ingested_excl_id)
                               OVER (PARTITION BY cbp_entry_id)
                               AS max_cbp_excl_id
                      FROM tu_cbp_ingested_excl)
             WHERE cbp_ingested_excl_id = max_cbp_excl_id) cbp_ingest_excl
               ON cbp_ingest_excl.cbp_entry_id = cbpentry.cbp_entry_id
;

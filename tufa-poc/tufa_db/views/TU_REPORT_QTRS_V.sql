--------------------------------------------------------
--  DDL for View TU_REPORT_QTRS_V
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TU_REPORT_QTRS_V" ("FISCAL_YR", "FISCAL_QTR", "CAL_YR", "CAL_QTR", "MONTH_NM") AS 
  SELECT fiscal_yr, quarter, year,
       case
          when quarter = 1 then  4
          when quarter = 2 then  1
          when quarter = 3 then  2
          else  3
       end as cal_qtr,
       month
        FROM tu_rpt_period
        ORDER BY 1, 2
;

--------------------------------------------------------
--  DDL for View TU_TTB_INGESTED_EXCL_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TU_TTB_INGESTED_EXCL_VW" ("TTB_INGESTED_EXCL_ID", "PERM_EXCL_AUDIT_ID", "CREATED_DT", "MODIFIED_DT", "TTB_PERMIT_ID", "PERMIT_NUM", "TTB_COMPANY_ID", "COMPANY_NM", "EIN", "TTB_QTR", "TTB_YR", "TTB_ANNUAL_TAX_ID", "ASMT_YR", "ASMT_QTR") AS 
  SELECT ttb_ingest_excl.ttb_ingested_excl_id,
           ttb_ingest_excl.perm_excl_audit_id,
           ttb_ingest_excl.created_dt,
           ttb_ingest_excl.modified_dt,
           p.permit_id           AS ttb_permit_id,
           p.permit_num,
           c.company_id          AS ttb_company_id,
           c.legal_nm            AS company_nm,
           c.ein,
           tann.ttb_calendar_qtr AS ttb_qtr,
           tann.ttb_fiscal_yr    AS ttb_yr,
           tann.ttb_annual_tax_id,
           CASE
               WHEN tann.ttb_calendar_qtr = 4 AND tann.tobacco_class_id <> 8
               THEN
                   tann.ttb_fiscal_yr + 1
               WHEN tann.tobacco_class_id = 8
               THEN
                   tann.ttb_fiscal_yr + 1
               ELSE
                   tann.ttb_fiscal_yr
           END
               AS asmt_yr,
           CASE
               WHEN tann.ttb_calendar_qtr = 4 AND tann.tobacco_class_id <> 8
               THEN
                   1
               WHEN tann.tobacco_class_id = 8
               THEN
                   tann.ttb_calendar_qtr
               ELSE
                   tann.ttb_calendar_qtr + 1
           END
               AS asmt_qtr
      --select c.legal_nm
      FROM tu_ttb_company  tc
           INNER JOIN tu_ttb_permit tp
               ON tc.ttb_company_id = tp.ttb_company_id
           INNER JOIN tu_ttb_annual_tax tann
               ON tann.ttb_permit_id = tp.ttb_permit_id
           LEFT JOIN tu_company c ON c.ein = tc.ein_num
           LEFT JOIN tu_permit p
               ON     p.company_id = c.company_id
                  AND p.permit_num = tp.permit_num
           INNER JOIN
           (SELECT ttb_annual_tax_id,
                   perm_excl_audit_id,
                   ttb_ingested_excl_id,
                   created_dt,
                   created_by,
                   modified_dt,
                   modified_by
              FROM (SELECT ttb_annual_tax_id,
                           perm_excl_audit_id,
                           ttb_ingested_excl_id,
                           created_dt,
                           created_by,
                           modified_dt,
                           modified_by,
                           MAX (ttb_ingested_excl_id)
                               OVER (PARTITION BY ttb_annual_tax_id)
                               AS max_ttb_excl_id
                      FROM tu_ttb_ingested_excl)
             WHERE ttb_ingested_excl_id = max_ttb_excl_id) ttb_ingest_excl
               ON ttb_ingest_excl.ttb_annual_tax_id = tann.ttb_annual_tax_id
;

--------------------------------------------------------
--  DDL for View TU_TTB_VOLUME_TAXES_PER_TT
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TU_TTB_VOLUME_TAXES_PER_TT" ("QTR", "EIN", "PRODUCT", "POUNDS", "FISCAL_YR", "ESTIMATED_TAX", "PERMIT_NUM") AS 
  SELECT
    CASE
        WHEN ( TO_CHAR(TO_DATE(trv.period,'MM-YY'),'Q') + 1 ) <= 4 THEN ( TO_CHAR(TO_DATE(trv.period,'MM-YY'),'Q') + 1 )
        ELSE 1
    END AS "QTR",
    replace(trv.ein,'-','') AS ein,
    mtc.tobacco_class_nm   AS product,
    SUM(trv.pounds) AS pounds,
    trv.fiscal_yr,
    SUM(trv.pounds * tr.tax_rate) AS estimated_tax,
    trv.id_permit as permit_num
FROM
    tu_ttb_removals_final trv
    LEFT OUTER JOIN tu_ttbvolume_tobaccoclass tc ON trv.product = tc.alias_tobacco_class_nm
                                                    OR trv.product = tc.alias_tobacco_class_nm2
                                                    OR trv.product = tc.alias_tobacco_class_nm3
                                                    OR trv.product = tc.alias_tobacco_class_nm4
    LEFT OUTER JOIN tu_tobacco_class mtc ON tc.tobacco_class_nm = mtc.tobacco_class_nm
    LEFT OUTER JOIN tu_tax_rate tr ON mtc.tobacco_class_id = tr.tobacco_class_id
--                    INNER JOIN TU_PERMIT p ON trv.ID_PERMIT = p.permit_num
WHERE
    trv.fiscal_yr BETWEEN EXTRACT(YEAR FROM tax_rate_effective_date) AND EXTRACT(YEAR FROM tr.tax_rate_end_date)
GROUP BY
    CASE
        WHEN ( TO_CHAR(TO_DATE(trv.period,'MM-YY'),'Q') + 1 ) <= 4 THEN ( TO_CHAR(TO_DATE(trv.period,'MM-YY'),'Q') + 1 )
        ELSE 1
    END,
    replace(trv.ein,'-',''),
    mtc.tobacco_class_nm,
    trv.fiscal_yr,
    trv.id_permit
ORDER BY
    fiscal_yr,
    ein,
    qtr,
    product
;

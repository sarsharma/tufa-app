--------------------------------------------------------
--  DDL for View TU_TTB_VOLUME_VW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "CTP_TUFA_MVP13"."TU_TTB_VOLUME_VW" ("MONTH", "QTR", "FISCAL_YR", "ID_PERMIT", "EIN", "TTB_PRODUCT", "TTB_VOLUME", "TAX_RATE") AS 
  SELECT MONTH,
            QTR,
            TTBVolume.FISCAL_YR,
            TTBVolume.ID_PERMIT,
            EIN,
            LISTAGG (TTBVolume.PRODUCT, ',') WITHIN GROUP (ORDER BY PRODUCT)
               AS "TTB_PRODUCT",
            LISTAGG (TTBVolume.POUNDS, ',') WITHIN GROUP (ORDER BY PRODUCT)
               AS "TTB_VOLUME",
            LISTAGG (TTBVolume.tax_rate, ',') WITHIN GROUP (ORDER BY PRODUCT)
               AS "TAX_RATE"
       FROM (SELECT DISTINCT
                    TO_CHAR (TO_DATE (TRV.PERIOD, 'MM-YY'), 'MON') AS "MONTH",
                    TRV.PERIOD,
                    CASE
                       WHEN (TO_CHAR (TO_DATE (TRV.PERIOD, 'MM-YY'), 'Q') + 1) <=
                               4
                       THEN
                          (TO_CHAR (TO_DATE (TRV.PERIOD, 'MM-YY'), 'Q') + 1)
                       ELSE
                          1
                    END
                       AS "QTR",
                    TRV.ID_PERMIT,
                    TO_CHAR (
                       TO_DATE (TR.tax_rate_effective_date, 'DD-MM-YYYY'),
                       'MON-YY')
                       AS "TAX_DATE",
                    REPLACE (TRV.EIN, '-', '') AS EIN,
                    TRV.PRODUCT,
                    TRV.POUNDS,
                    TRV.FISCAL_YR,
                    TR.TAX_RATE
               FROM TU_TTB_REMOVALS_FINAL trv
                    LEFT OUTER JOIN
                    TU_TTBVOLUME_TOBACCOCLASS tc
                       ON    trv.product = tc.alias_tobacco_class_nm
                          OR trv.product = tc.alias_tobacco_class_nm2
                          OR trv.product = tc.alias_tobacco_class_nm3
                          OR trv.product = tc.alias_tobacco_class_nm4
                    LEFT OUTER JOIN TU_TOBACCO_CLASS mtc
                       ON tc.TOBACCO_CLASS_NM = mtc.TOBACCO_CLASS_NM
                    LEFT OUTER JOIN TU_TAX_RATE tr
                       ON mtc.TOBACCO_CLASS_ID = tr.TOBACCO_CLASS_ID
                    INNER JOIN TU_PERMIT p ON trv.ID_PERMIT = p.permit_num
              WHERE TRV.fiscal_yr BETWEEN EXTRACT (
                                             YEAR FROM tax_rate_effective_date)
                                      AND EXTRACT (
                                             YEAR FROM TR.tax_rate_END_date)) TTBVolume
   GROUP BY MONTH,
            QTR,
            TTBVolume.FISCAL_YR,
            TTBVolume.ID_PERMIT,
            EIN
;

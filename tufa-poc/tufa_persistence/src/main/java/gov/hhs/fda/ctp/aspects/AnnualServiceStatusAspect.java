package gov.hhs.fda.ctp.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.AnnualServiceStatusManager;
import gov.hhs.fda.ctp.persistence.model.ServiceStatusEntity;

@Aspect
@Component
/**
 * Used to lock services for the Annual TrueUp. Can be used for other services that require
 * locking. 
 * */
public class AnnualServiceStatusAspect {
	Logger logger = LoggerFactory.getLogger(AnnualServiceStatusAspect.class);

	@Autowired
	AnnualServiceStatusManager annualServiceStatusManager;

	@Autowired
	private SecurityContextUtil securityContextUtil;
	
	@Autowired
	private SessionFactory sessionFactory;

	// Ingestion Process
	@Pointcut("execution(*  gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.ingestCBP(..))"
			+ "|| execution(*  gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.ingestCBPLineItem(..))"
			+ "|| execution(*  gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.ingestTTB(..))"
			+ "|| execution(*  gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.ingestTTBRemovals(..))")
	private void ingestTrueUpFile() {
		//Aspect Method
	}

	// Deletion Process
	@Pointcut("execution(* gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.deleteIngestionFile(..))")
	private void deleteTrueUpFile() {
		//Aspect Method
	}

	// Metrics Process
	@Pointcut("execution(* gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.generateMetricData(..))")
	private void generateMetrics() {
		//Aspect Method
	}
	

	// Accept Process
	@Pointcut("execution(* gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.insertAcceptedCBPLegacyData(..))")
	private void acceptIngested() {
		//Aspect Method
	}

//	@Around("ingestTrueUpFile()")
	public Object lockIngestionService(ProceedingJoinPoint joinPoint) throws Throwable {
		Object[] args = joinPoint.getArgs();
		IngestionOutput ing = (IngestionOutput) args[0];
		Long fiscalYear = ing.getFiscalYear();

		// Place lock
		this.getLock(fiscalYear, "INGESTION");
		
		// Execute the service (Ingestion)
		Object retVal = joinPoint.proceed();
		
		// Remove lock
		this.releaseLock(fiscalYear, "INGESTION");
		
		// Return result
		return retVal;
	}

//	@Around("deleteTrueUpFile()")
	public Object lockDeletionService(ProceedingJoinPoint joinPoint) throws Throwable {
		Object[] args = joinPoint.getArgs();
		Long fiscalYear = Long.parseLong(args[0].toString());

		this.getLock(fiscalYear, "DELETION");
		
		// Execute the service (Ingestion)
		Object retVal = joinPoint.proceed();
		
		// Remove lock
		this.releaseLock(fiscalYear, "DELETION");
		
		// Return result
		return retVal;
	}

//	@Around("generateMetrics()")
	public Object lockMetricsService(ProceedingJoinPoint joinPoint) throws Throwable {
		Object[] args = joinPoint.getArgs();
		Long fiscalYear = Long.parseLong(args[0].toString());

		this.getLock(fiscalYear, "METRICS");
		
		// Execute the service (Ingestion)
		Object retVal = joinPoint.proceed();
		
		// Remove lock
		this.releaseLock(fiscalYear, "METRICS");
		
		// Return result
		return retVal;
	}
	
//	@Around("acceptIngested()")
	public Object lockAcceptIngestedService(ProceedingJoinPoint joinPoint) throws Throwable {
		Object[] args = joinPoint.getArgs();
		Long fiscalYear = Long.parseLong(args[0].toString());

		this.getLock(fiscalYear, "ACCEPT");
		
		// Execute the service (Ingestion)
		Object retVal = joinPoint.proceed();
		
		// Remove lock
		this.releaseLock(fiscalYear, "ACCEPT");
		
		// Return result
		return retVal;
	}

	private void getLock(Long fiscalYear, String service) throws Throwable {
		Session session = this.sessionFactory.getCurrentSession();
		
		String hql = " FROM ServiceStatusEntity WHERE service_name =:service AND fiscal_year =:fiscalYear";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("service", service);
		query.setParameter("fiscalYear", fiscalYear);
		
		ServiceStatusEntity e = (ServiceStatusEntity) query.uniqueResult();
		if (e == null) {
			// If null create new record
			e = new ServiceStatusEntity();
			e.setFiscalYear(fiscalYear);
			e.setServiceName(service);
			e.setStatus(Constants.IN_PROGRESS);
			e.setCreatedBy(securityContextUtil.getSessionUser());
			session.saveOrUpdate(e);
		} else if (!Constants.IN_PROGRESS.equals(e.getStatus())) {
			// If exists and not in progress update status
			e.setStatus(Constants.IN_PROGRESS);
			e.setModifiedBy(securityContextUtil.getSessionUser());
			this.annualServiceStatusManager.saveServiceStatus(e);
		} else {
			// Otherwise block service
			throw (new TufaException("Process Locked:This process has been locked by another user. Please wait while the process runs and refresh to see new data."));
		}
		logger.info(service + " for " + fiscalYear + " is locked");
	}

	private void releaseLock(Long fiscalYear, String service) {
		
		String hql = " FROM ServiceStatusEntity WHERE service_name =:service AND fiscal_year =:fiscalYear";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("service", service);
		query.setParameter("fiscalYear", fiscalYear);
		
		ServiceStatusEntity e = (ServiceStatusEntity) query.uniqueResult();
		if(e != null) {
			e.setStatus("Done");
			this.sessionFactory.getCurrentSession().saveOrUpdate(e);
		}
		logger.info(service + " for " + fiscalYear + " is unlocked");
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}

package gov.hhs.fda.ctp.aspects;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import gov.hhs.fda.ctp.common.beans.CBPColumnTemplateBean;
import gov.hhs.fda.ctp.common.beans.CBPFileColumnTemplate;
import gov.hhs.fda.ctp.common.beans.StaticTufaDataBean;
import gov.hhs.fda.ctp.common.beans.TTBColumnTemplateBean;
import gov.hhs.fda.ctp.common.beans.TTBFileColumnTemplate;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.util.ApplicationContextUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.AddressMgmtEntityManager;
import gov.hhs.fda.ctp.persistence.PermitEntityManager;
import gov.hhs.fda.ctp.persistence.TrueUpEntityManager;
import gov.hhs.fda.ctp.persistence.model.CBPFileColumnTemplateEntity;
import gov.hhs.fda.ctp.persistence.model.TTBFileColumnTemplateEntity;

@Aspect
@Component
public class IngestionFileMgmtAspect {

	Logger logger = LoggerFactory.getLogger(IngestionFileMgmtAspect.class);

	@Autowired
	private PermitEntityManager permitEntityManager;

	@Autowired
	private AddressMgmtEntityManager addressMgmtEntityManager;
	
	@Autowired
	private TrueUpEntityManager trueUpEntityManager;

	@Autowired
	private MapperWrapper mapperWrapper;

	@Pointcut("execution(* gov.hhs.fda.ctp.web.permitmgmt.PermitMgmtRestController.ingestPermitListFile(..))")
	private void ingestTTBPermitFile() {
		//This is Aspect Method
	}

	//To be used when required
	@Pointcut("execution(* gov.hhs.fda.ctp.web.trueup.TrueUpRestController.ingestFileUpload(..))")
	private void ingestCBPOrTTBOrTTBvFile() {
		//THis is Aspect Method
	}

	@Around("ingestTTBPermitFile()")
	public Object doLoadTTBFileTemplates(ProceedingJoinPoint pjp) throws Throwable {

		ConfigurableApplicationContext configContext = (ConfigurableApplicationContext) ApplicationContextUtil
				.getApplicationContext();
		DefaultListableBeanFactory beanRegistry = (DefaultListableBeanFactory) configContext.getBeanFactory();

		List<TTBFileColumnTemplateEntity> templateEntities = this.permitEntityManager.loadTTBFileTemplates();
		Map<String, Map<String, List<String>>> aliasesMap = this.permitEntityManager.loadTTBFileColAliases();

		List<TTBFileColumnTemplate> temps = new ArrayList<>();
		Mapper mapper = mapperWrapper.getMapper();

		TTBColumnTemplateBean ttbBean = new TTBColumnTemplateBean();
		for (TTBFileColumnTemplateEntity template : templateEntities) {
			TTBFileColumnTemplate temp = new TTBFileColumnTemplate();
			mapper.map(template, temp);
			temps.add(temp);
		}

		ttbBean.setTtbColumnTemplates(temps);

		if (beanRegistry.containsBean(Constants.TTB_COLOUMN_TEMPLATES))
			beanRegistry.removeBeanDefinition(Constants.TTB_COLOUMN_TEMPLATES);

		BeanDefinitionBuilder b = BeanDefinitionBuilder.rootBeanDefinition(TTBColumnTemplateBean.class);
		b.addPropertyValue(Constants.TTB_COLOUMN_TEMPLATES, temps);
		b.addPropertyValue("colAliasesMap", aliasesMap);

		beanRegistry.registerBeanDefinition(Constants.TTB_COLOUMN_TEMPLATES, b.getBeanDefinition());

		loadStateCodes();

		return pjp.proceed();
	}

	@Around("ingestCBPOrTTBOrTTBvFile()")
	public Object doLoadCBPFileTemplates(ProceedingJoinPoint pjp) throws Throwable {

		ConfigurableApplicationContext configContext = (ConfigurableApplicationContext) ApplicationContextUtil
				.getApplicationContext();
		DefaultListableBeanFactory beanRegistry = (DefaultListableBeanFactory) configContext.getBeanFactory();

		List<CBPFileColumnTemplateEntity> templateEntities = this.trueUpEntityManager.loadCBPFileTemplates();

		List<CBPFileColumnTemplate> temps = new ArrayList<>();
		Mapper mapper = mapperWrapper.getMapper();

		CBPColumnTemplateBean cbpBean = new CBPColumnTemplateBean();
		for (CBPFileColumnTemplateEntity template : templateEntities) {
			CBPFileColumnTemplate temp = new CBPFileColumnTemplate();
			mapper.map(template, temp);
			temps.add(temp);
		}

		cbpBean.setcbpColumnTemplates(temps);

		if (beanRegistry.containsBean(Constants.CBP_COLOUMN_TEMPLATES))
			beanRegistry.removeBeanDefinition(Constants.CBP_COLOUMN_TEMPLATES);

		BeanDefinitionBuilder b = BeanDefinitionBuilder.rootBeanDefinition(CBPColumnTemplateBean.class);
		b.addPropertyValue(Constants.CBP_COLOUMN_TEMPLATES, temps);
		
		beanRegistry.registerBeanDefinition(Constants.CBP_COLOUMN_TEMPLATES, b.getBeanDefinition());

		return pjp.proceed();
	}
	

	public void loadStateCodes() throws Throwable {

		ConfigurableApplicationContext configContext = (ConfigurableApplicationContext) ApplicationContextUtil
				.getApplicationContext();
		DefaultListableBeanFactory beanRegistry = (DefaultListableBeanFactory) configContext.getBeanFactory();

		if (beanRegistry.containsBean(Constants.STATIC_TUFA_DATA_BEAN)){
			StaticTufaDataBean staticTufaDataBean= (StaticTufaDataBean) beanRegistry.getBean(Constants.STATIC_TUFA_DATA_BEAN);
			if(staticTufaDataBean.getStateCodes()!=null && staticTufaDataBean.getStateCodes().size() >0)
					return;
		}

		BeanDefinitionBuilder b = BeanDefinitionBuilder.rootBeanDefinition(StaticTufaDataBean.class);

		List<String> stateCodes = this.addressMgmtEntityManager.getStateCd();

		b.addPropertyValue("stateCodes", stateCodes);

		beanRegistry.registerBeanDefinition(Constants.STATIC_TUFA_DATA_BEAN, b.getBeanDefinition());
	}

	public PermitEntityManager getPermitEntityManager() {
		return permitEntityManager;
	}

	public void setPermitEntityManager(PermitEntityManager permitEntityManager) {
		this.permitEntityManager = permitEntityManager;
	}

	public AddressMgmtEntityManager getAddressMgmtEntityManager() {
		return addressMgmtEntityManager;
	}

	public void setAddressMgmtEntityManager(AddressMgmtEntityManager addressMgmtEntityManager) {
		this.addressMgmtEntityManager = addressMgmtEntityManager;
	}

	public TrueUpEntityManager getTrueUpEntityManager() {
		return trueUpEntityManager;
	}

	public void setTrueUpEntityManager(TrueUpEntityManager trueUpEntityManager) {
		this.trueUpEntityManager = trueUpEntityManager;
	}

}

package gov.hhs.fda.ctp.aspects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import gov.hhs.fda.ctp.common.beans.IngestionWorkflowContext;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.ingestion.workflow.IngestionContext;
import gov.hhs.fda.ctp.common.ingestion.workflow.IngestionWFUserContext;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.common.util.ApplicationContextUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.TrueUpEntityManager;
import gov.hhs.fda.ctp.persistence.model.IngestionWorkflowContextEntity;

@Aspect
@Component
public class IngestionWorkflowContextMgmtAspect {

	Logger logger = LoggerFactory.getLogger(IngestionWorkflowContextMgmtAspect.class);

	@Autowired
	private TrueUpEntityManager trueUpEntityManager;

	@Autowired
	private MapperWrapper mapperWrapper;

	@Autowired
	private SecurityContextUtil securityContextUtil;

	@Pointcut("execution (* gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.saveorUpdateIngestionWorkflowContext(..))")
	private void saveUpdateIngestionWorkflowContext() {
		//this is Aspect Method

	}

	@Pointcut("execution (* gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.getIngestionWorkflowContext(..))")
	private void getIngestionWorkflowContext() {
		//This is aspect method

	}

	@AfterReturning(pointcut = "saveUpdateIngestionWorkflowContext()||getIngestionWorkflowContext()", returning = "currentContext")
	public Object setIngestionWorkflowContext(Object currentContext) throws Throwable {

		logger.debug(" setIngestionWorkflowContext :: retrieve ingestion workflow context ");

		// Return if current context not found
		List<IngestionWorkflowContext> currContextList = (List<IngestionWorkflowContext>) currentContext;
		IngestionWorkflowContext currContext;

		if(currContextList == null || currContextList.size() == 0)
			return currContextList;
		else
			currContext = currContextList.get(0);
		
		// refresh all the contexts
		List<IngestionWorkflowContext> contexts = this
				.transformIngestionWorkflowContextListToBeans(this.trueUpEntityManager.getIngestionContexts());

		ConfigurableApplicationContext configContext = (ConfigurableApplicationContext) ApplicationContextUtil
				.getApplicationContext();
		DefaultListableBeanFactory beanRegistry = (DefaultListableBeanFactory) configContext.getBeanFactory();

		if (beanRegistry.containsBean(Constants.INGESTION_CONTEXT))
			beanRegistry.removeBeanDefinition(Constants.INGESTION_CONTEXT);

		BeanDefinitionBuilder b = BeanDefinitionBuilder.rootBeanDefinition(IngestionContext.class);
		b.addPropertyValue("contexts", contexts);

		beanRegistry.registerBeanDefinition(Constants.INGESTION_CONTEXT, b.getBeanDefinition());

		
		// Update or add ingestion context for the current user
		String username = this.getSecurityContextUtil().getSessionUser();
		String contextId = username.concat(Long.toString(currContext.getFiscalYr()));

		if (beanRegistry.containsBean(Constants.INGESTION_WF_USER_CONTEXT)) {
			IngestionWFUserContext ingestionWFUserContext = (IngestionWFUserContext) beanRegistry
					.getBean(Constants.INGESTION_WF_USER_CONTEXT);
			// get current user
			ingestionWFUserContext.updateUserContext(contextId, currContext);
		} else {
			BeanDefinitionBuilder ucBean = BeanDefinitionBuilder.rootBeanDefinition(IngestionWFUserContext.class);
			Map<String, IngestionWorkflowContext> currentUserContexts = new HashMap<>();
			currentUserContexts.put(contextId, currContext);
			ucBean.addPropertyValue("currentUserContexts", currentUserContexts);
			// register current user context
			beanRegistry.registerBeanDefinition(Constants.INGESTION_WF_USER_CONTEXT, ucBean.getBeanDefinition());
		}

		return currContext;
	}

	private List<IngestionWorkflowContext> transformIngestionWorkflowContextListToBeans(
			List<IngestionWorkflowContextEntity> entities) {

		List<IngestionWorkflowContext> beanList = new ArrayList<>();
		if (entities != null) {
			Mapper mapper = mapperWrapper.getMapper();
			for (IngestionWorkflowContextEntity entry : entities) {
				IngestionWorkflowContext entryBean = new IngestionWorkflowContext();
				mapper.map(entry, entryBean);
				beanList.add(entryBean);
			}
		}
		return beanList;
	}

	public TrueUpEntityManager getTrueUpEntityManager() {
		return trueUpEntityManager;
	}

	public void setTrueUpEntityManager(TrueUpEntityManager trueUpEntityManager) {
		this.trueUpEntityManager = trueUpEntityManager;
	}

	public MapperWrapper getMapperWrapper() {
		return mapperWrapper;
	}

	public void setMapperWrapper(MapperWrapper mapperWrapper) {
		this.mapperWrapper = mapperWrapper;
	}

	public SecurityContextUtil getSecurityContextUtil() {
		return securityContextUtil;
	}

	public void setSecurityContextUtil(SecurityContextUtil securityContextUtil) {
		this.securityContextUtil = securityContextUtil;
	}
}

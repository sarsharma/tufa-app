package gov.hhs.fda.ctp.aspects;

import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.dozer.Mapper;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitAudit;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.persistence.PermitEntityManager;
import gov.hhs.fda.ctp.persistence.model.PermitAuditEntity;

@Aspect
@Component
public class PermitMgmtAuditUpdateAspect {

	Logger logger = LoggerFactory.getLogger(PermitMgmtAuditUpdateAspect.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private PermitEntityManager permitEntityManager;

	@Autowired
	private MapperWrapper mapperWrapper;

	@Pointcut("execution(* gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizCompImpl.updatePermit(..))")
	private void updatePermit() {
		//THis is aspect method
	}
	

	@Pointcut("execution(* gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizCompImpl.createPermit(..))")
	private void createPermit() {
		//THis is aspect method
	}

	@Pointcut("execution(* gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizCompImpl.updateCompany(..))")
	private void updateCompany() {
		//THis is aspect method
	}

	@Around("updatePermit()||createPermit()")
	public Object doPermitMgmtAuditPermitUpdate(ProceedingJoinPoint pjp) throws Throwable {

		logger.debug(" doPermitMgmtAuditPermitUpdate:: update permit changes to permit mgmt audit table");

		Object[] args = pjp.getArgs();
		Permit permitUpdate = (Permit) args[0];

		PermitAudit permitAudit = permitUpdate.getPermitAudit();

		Object retVal = pjp.proceed();
		Company company = (Company) retVal;

		if (permitAudit != null){
			permitAudit = this.insertPermitMgmtAudit(permitAudit);
			company.setPermitAudit(permitAudit);
		}

		return company;
	}

	@Around("updateCompany()")
	public Object doPermitMgmtAuditCompanyUpdate(ProceedingJoinPoint pjp) throws Throwable {

		logger.debug(" doPermitMgmtAuditCompanyUpdate:: update company changes to permit mgmt audit table");

		Object[] args = pjp.getArgs();
		Company companyUpdate = (Company) args[0];

		PermitAudit permitAudit = companyUpdate.getPermitAudit();

		Object retVal = pjp.proceed();
		Company company = (Company) retVal;

		if (permitAudit != null)
			permitAudit = this.insertPermitMgmtAudit(permitAudit);

		company.setPermitAudit(permitAudit);
		return company;
	}

	private PermitAudit insertPermitMgmtAudit(PermitAudit pAudit) {
		Mapper mapper = mapperWrapper.getMapper();
		PermitAuditEntity pAuditEntity = new PermitAuditEntity();
		mapper.map(pAudit, pAuditEntity);
		pAuditEntity.setActionDt(new Date());
		pAuditEntity.setCreatedDt(new Date());

		PermitAuditEntity paEntity = permitEntityManager.insertPermitMgmtAudit(pAuditEntity);

		PermitAudit paUpdate = new PermitAudit();
		mapper.map(paEntity, paUpdate);
		return paUpdate;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public PermitEntityManager getPermitEntityManager() {
		return permitEntityManager;
	}

	public void setPermitEntityManager(PermitEntityManager permitEntityManager) {
		this.permitEntityManager = permitEntityManager;
	}

}

package gov.hhs.fda.ctp.hibernate;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
public class DataSourceProperties {

	private String driverClassName;

	private String jdbcUrl;

	private String user;

	private String password;

	private Integer maxPoolSize;
	
	private Integer minIdle;

	private final Hibernate hibernate = new Hibernate();

	static class Hibernate {

		private String dialect;
		private String showSql;
		private String currentSessionContextClass;

		private final JDBC jdbc = new JDBC();

		static class JDBC {
			private String batchSize;

			public String getBatchSize() {
				return batchSize;
			}

			public void setBatchSize(String batchSize) {
				this.batchSize = batchSize;
			}
		}

		public String getDialect() {
			return dialect;
		}

		public void setDialect(String dialect) {
			this.dialect = dialect;
		}

		public String getShowSql() {
			return showSql;
		}

		public void setShowSql(String showSql) {
			this.showSql = showSql;
		}

		public String getCurrentSessionContextClass() {
			return currentSessionContextClass;
		}

		public void setCurrentSessionContextClass(String currentSessionContextClass) {
			this.currentSessionContextClass = currentSessionContextClass;
		}

		public JDBC getJdbc() {
			return jdbc;
		}

	}

	public Hibernate getHibernate() {
		return hibernate;
	}

	public Integer getMaxPoolSize() {
		return maxPoolSize;
	}

	public void setMaxPoolSize(Integer maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getJdbcUrl() {
		return jdbcUrl;
	}

	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getMinIdle() {
		return minIdle;
	}

	public void setMinIdle(Integer minIdle) {
		this.minIdle = minIdle;
	}

	
}

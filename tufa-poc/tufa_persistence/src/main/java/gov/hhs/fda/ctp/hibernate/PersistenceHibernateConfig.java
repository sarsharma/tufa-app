package gov.hhs.fda.ctp.hibernate;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "gov.hhs.fda.ctp" })
public class PersistenceHibernateConfig {

	@Autowired
	private DataSourceProperties dataSourceProperties;

	@Bean
	public HikariDataSource dataSource() {
		
		HikariDataSource ds = new HikariDataSource();
		ds.setDriverClassName(this.dataSourceProperties.getDriverClassName());
		ds.setJdbcUrl(this.dataSourceProperties.getJdbcUrl());
		ds.setUsername(this.dataSourceProperties.getUser());
		ds.setPassword(this.dataSourceProperties.getPassword());
		ds.setMaximumPoolSize(this.dataSourceProperties.getMaxPoolSize());
		ds.setMinimumIdle(this.dataSourceProperties.getMinIdle());
		return ds;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {

		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(new String[] { "gov.hhs.fda.ctp" });
		sessionFactory.setHibernateProperties(hibernateProperties());

		return sessionFactory;
	}

	@Bean
	public PlatformTransactionManager hibernateTransactionManager() {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(sessionFactory().getObject());
		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	private Properties hibernateProperties() {
		Properties props = new Properties();
		props.setProperty("hibernate.dialect", this.dataSourceProperties.getHibernate().getDialect());
		props.setProperty("hibernate.show_sql", this.dataSourceProperties.getHibernate().getShowSql());
		props.setProperty("hibernate.jdbc.batch_size",
				this.dataSourceProperties.getHibernate().getJdbc().getBatchSize());
		return props;
	}

	public DataSourceProperties getDataSourceProperties() {
		return dataSourceProperties;
	}

	public void setDataSourceProperties(DataSourceProperties dataSourceProperties) {
		this.dataSourceProperties = dataSourceProperties;
	}

}

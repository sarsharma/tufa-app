package gov.hhs.fda.ctp.hibernate;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Profile("persistenceingestmaptest")
@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:persistence/persistence.properties" })
/** 
 * This configuration does not have Mocks configured as this will be used in end to end testing which requires
 * actual class objects from Spring's application context.
 **/
public class PersistenceHibernateIngestMapTestConfig {
	
	
	@Autowired
	   private Environment env;
	 
	   @Bean
	   public LocalSessionFactoryBean sessionFactory() {
	      
		  LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	      sessionFactory.setDataSource(dataSource());
	      sessionFactory.setPackagesToScan(new String[] { "gov.hhs.fda.ctp" });
	      sessionFactory.setHibernateProperties(hibernateProperties());
	      return sessionFactory;
	      
	   }
	 
	   @Bean
	   public DataSource dataSource() {
		    
		   final DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
	        dataSource.setUrl(env.getProperty("jdbc.url"));
	        dataSource.setUsername(env.getProperty("jdbc.user"));
	        dataSource.setPassword(env.getProperty("jdbc.pass"));
	        return dataSource;
	   }
	 
	   @Bean
	   @Autowired
	   public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
	      HibernateTransactionManager txManager = new HibernateTransactionManager();
	      txManager.setSessionFactory(sessionFactory);
	 
	      return txManager;
	   }
	 
	   @Bean
	   public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
	      return new PersistenceExceptionTranslationPostProcessor();
	   }
	 
	   Properties hibernateProperties() {
	      
		   return new Properties() {
	         {
	            setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
	            setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
	         }
	      };
	   }	   

}

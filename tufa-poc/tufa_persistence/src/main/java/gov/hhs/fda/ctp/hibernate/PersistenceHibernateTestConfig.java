package gov.hhs.fda.ctp.hibernate;

import gov.hhs.fda.ctp.persistence.AddressMgmtEntityManager;
import gov.hhs.fda.ctp.persistence.AssessmentEntityManager;
import gov.hhs.fda.ctp.persistence.BaseEntityManager;
import gov.hhs.fda.ctp.persistence.CompanyMgmtEntityManager;
import gov.hhs.fda.ctp.persistence.ContactMgmtEntityManager;
import gov.hhs.fda.ctp.persistence.PermitEntityManager;
import gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager;
import gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.TrueUpEntityManager;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Profile("persistencetest")
@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:persistence/persistence.properties" })
public class PersistenceHibernateTestConfig {
	
	   @Autowired
	   private Environment env;
	 
	   @Bean
	   public LocalSessionFactoryBean sessionFactory() {
	      
		  LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	      sessionFactory.setDataSource(dataSource());
	      sessionFactory.setPackagesToScan(new String[] { "gov.hhs.fda.ctp" });
	      sessionFactory.setHibernateProperties(hibernateProperties());
	      return sessionFactory;
	      
	   }
	 
	   @Bean
	   public DataSource dataSource() {
		    
		   final DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
	        dataSource.setUrl(env.getProperty("jdbc.url"));
	        dataSource.setUsername(env.getProperty("jdbc.user"));
	        dataSource.setPassword(env.getProperty("jdbc.pass"));
	        return dataSource;
	   }
	 
	   @Bean
	   @Autowired
	   public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
	      HibernateTransactionManager txManager = new HibernateTransactionManager();
	      txManager.setSessionFactory(sessionFactory);
	 
	      return txManager;
	   }
	 
	   @Bean
	   public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
	      return new PersistenceExceptionTranslationPostProcessor();
	   }
	 
	   Properties hibernateProperties() {
	      
		   return new Properties() {
	         {
	            setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
	            setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
	         }
	      };
	   }	   
	   
	    @Bean
		@Primary 
		public BaseEntityManager baseEntityManager() {
			return Mockito.mock(BaseEntityManager.class);
		}
		
		@Bean
		@Primary
		public PermitPeriodEntityManager permitPeriodEntityManager() {
		    return Mockito.mock(PermitPeriodEntityManager.class);
		}
		
		@Bean
		@Primary
		public ReferenceCodeEntityManager referenceCodeEntityManager() {
		    return Mockito.mock(ReferenceCodeEntityManager.class);
		}
		
		@Bean
		@Primary
		public PermitEntityManager permitEntityManager() {
		    return Mockito.mock(PermitEntityManager.class);
		}
		
		@Bean
		@Primary
		public SimpleSQLManager simpleSQLManager() {
		    return Mockito.mock(SimpleSQLManager.class);
		}
		
		@Bean
		@Primary
		public AssessmentEntityManager assessmentEntityManager() {
		    return Mockito.mock(AssessmentEntityManager.class);
		}		
		
		@Bean
		@Primary 
		public PasswordEncoder passwordEncoder() {
			return Mockito.mock(PasswordEncoder.class);
		}
		
		@Bean
		@Primary
		public CompanyMgmtEntityManager companyMgmtEntityManager() {
		    return Mockito.mock(CompanyMgmtEntityManager.class);
		}
		
		@Bean
		@Primary
		public AddressMgmtEntityManager addressMgmtEntityManager() {
		    return Mockito.mock(AddressMgmtEntityManager.class);
		}	   
		
		@Bean
		@Primary
		public TrueUpEntityManager trueUpEntityManager() {
			return Mockito.mock(TrueUpEntityManager.class);
		}
		
		@Bean
		@Primary
		public ContactMgmtEntityManager contactMgmtEntityManager() {
			return Mockito.mock(ContactMgmtEntityManager.class);
		}

}

/**
 * 
 */
package gov.hhs.fda.ctp.persistence;

import java.util.List;
import java.util.Set;

import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.persistence.model.AddressEntity;
import gov.hhs.fda.ctp.persistence.model.ContactEntity;
import gov.hhs.fda.ctp.persistence.model.CountryEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateEntity;
import gov.hhs.fda.ctp.persistence.model.ReportAddressEntity;

/**
 * The Interface AddressMgmtEntityManager.
 *
 * @author akari
 */
public interface AddressMgmtEntityManager {
	
	/**
	 * Save or update.
	 *
	 * @param contact the contact
	 * @return address
	 */
	public AddressEntity saveOrUpdate(AddressEntity contact);
	
	/**
	 * Gets the country cd.
	 *
	 * @return the country cd
	 */
	public List<String> getCountryCd();
	
	/**
	 * Gets the country cd.
	 *
	 * @param countryNm the country nm
	 * @return the country cd
	 */
	public String getCountryCd(String countryNm);
	
	/**
	 * Gets the country nm.
	 *
	 * @param countryCd the country cd
	 * @return the country nm
	 */
	public String getCountryNm(String countryCd);
	
	/**
	 * Gets the country nm.
	 *
	 * @return the country nm
	 */
	public List<String> getCountryNm();
	
	/**
	 * Gets the State cd.
	 *
	 * @return the State cd
	 */
	public List<String> getStateCd();
	
	/**
	 * Gets the State cd.
	 *
	 * @param StateNm the State nm
	 * @return the State cd
	 */
	public String getStateCd(String StateNm);
	
	/**
	 * Gets the State nm.
	 *
	 * @param StateCd the State cd
	 * @return the State nm
	 */
	public String getStateNm(String StateCd);
	
	/**
	 * Gets the State nm.
	 *
	 * @return the State nm
	 */
	public List<String> getStateNm();
	
	/**
	 * Gets the country dial cd.
	 *
	 * @return the country dial cd
	 */
	public List<String> getCountryDialCd();
	
	/**
	 * Gets the countries.
	 *
	 * @return the countries
	 */
	public Set<CountryEntity> getCountries();
	
	/**
	 * Gets list of addresses from report address table.
	 * @param companyId
	 * @param periodId
	 * @return
	 */
	public List<ReportAddressEntity> getReportAddresses(int companyId, int permitId, int periodId);

	public void saveStatus(String ein);
	
	public void softDeleteAddress(long companyId, String addressTypeCd);
	
	public void updatePrimToAltnAddress(long companyId);
	
	public void delete(long companyId,  String addressType);

}

/**
 * 
 */
package gov.hhs.fda.ctp.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.PermitUpdateUpload;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.persistence.model.AddressEntity;
import gov.hhs.fda.ctp.persistence.model.AddressEntityBK;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.ContactEntity;
import gov.hhs.fda.ctp.persistence.model.CountryEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateEntity;
import gov.hhs.fda.ctp.persistence.model.ReportAddressEntity;

/**
 * The Class AddressMgmtEntityManagerImpl.
 *
 * @author akari
 */
@Repository("addressMgmtEntityManager")
public class AddressMgmtEntityManagerImpl extends BaseEntityManagerImpl implements AddressMgmtEntityManager {

	/** The mapper factory. */
    @Autowired	
	private MapperWrapper mapperWrapper;
    
	Logger logger = LoggerFactory.getLogger(AddressMgmtEntityManagerImpl.class);
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.ContactMgmtEntityManager#update(gov.hhs.fda.
	 * ctp.persistence.model.ContactEntity)
	 */
	@Override
	public AddressEntity saveOrUpdate(AddressEntity address) {
		logger.debug(" - Entering saveOrUpdate - ");
		// hash the phone number and email address to make it easier to see if anything has changed.
		int hashTotal = Objects.hash(address.getStreetAddress(), address.getSuite(), address.getCity(), address.getState(), address.getPostalCd());
		address.setHashTotal(hashTotal);
		getCurrentSession().saveOrUpdate(address);
		logger.debug(" - Exiting  saveOrUpdate - ");
		return address;
	}
	
	@Override
	public List<String> getCountryCd() {
		Query query = getCurrentSession().createSQLQuery("Select ISO_CNTRY_CD FROM TU_ISO_CNTRY");
		return query.list();
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.AddressMgmtEntityManager#getCountryNm()
	 */
	@Override
	public List<String> getCountryNm() {
		Query query = getCurrentSession().createSQLQuery("Select ISO_COUNTRY_NM FROM TU_ISO_CNTRY");
		return query.list();
	}

	@Override
	public List<String> getCountryDialCd() {
		Query query = getCurrentSession().createSQLQuery("Select CNTRY_DIAL_CD FROM TU_ISO_CNTRY");
		return query.list();
	}
	
	@Override
	public Set<CountryEntity> getCountries() {
        String hql = "FROM CountryEntity c";
        Query query = getCurrentSession().createQuery(hql);
        Set<CountryEntity> countries = new TreeSet<CountryEntity>(new CountryEntity());
        List<CountryEntity> countryList = query.list();
        countries.addAll(countryList);
        return countries;	    
	}

	@Override
	public String getCountryCd(String countryNm) {
		Query query = getCurrentSession().createSQLQuery("Select ISO_CNTRY_CD FROM TU_ISO_CNTRY where ISO_COUNTRY_NM = :countryNm");
		query.setParameter("countryNm", countryNm);
		return query.list().get(0).toString();
	}

	@Override
	public String getCountryNm(String countryCd) {
		Query query = getCurrentSession().createSQLQuery("Select ISO_COUNTRY_NM FROM TU_ISO_CNTRY where ISO_CNTRY_CD = :countryCd");
		query.setParameter("countryCd", countryCd);
		return query.list().get(0).toString();
	}

	@Override
	// Making the following method transactional as it is getting from a non-transactional context
	@Transactional
	public List<String> getStateCd() {
		Query query = getCurrentSession().createSQLQuery("Select STATE_CD FROM TU_STATE");
		return query.list();
	}

	@Override
	public String getStateCd(String StateNm) {
		Query query = getCurrentSession().createSQLQuery("Select STATE_CD FROM TU_STATE where STATE_NM = :StateNm");
		query.setParameter("StateNm", StateNm);
		return query.list().isEmpty()? null : query.list().get(0).toString();
	}

	@Override
	public String getStateNm(String StateCd) {
		Query query = getCurrentSession().createSQLQuery("Select STATE_NM FROM TU_STATE where STATE_CD = :stateCd");
		query.setParameter("stateCd", StateCd);
		return query.list().isEmpty()? null : query.list().get(0).toString();
	}

	@Override
	public List<String> getStateNm() {
		Query query = getCurrentSession().createSQLQuery("Select STATE_NM FROM TU_STATE");
		return query.list();
	}
	
	@Override
	public List<ReportAddressEntity> getReportAddresses(int companyId, int permitId, int periodId) {

		Query query = getCurrentSession().createQuery(
				"FROM ReportAddressEntity where COMPANY_ID = :companyId and PERMIT_ID = :permitId and PERIOD_ID = :periodId order by ADDRESS_TYPE_CD desc");
		query.setParameter("companyId", companyId);
		query.setParameter("permitId", permitId);
		query.setParameter("periodId", periodId);
		return query.list();
	}
	
	
	@Override
	public void saveStatus(String ein) {

		String sql = "UPDATE tu_permit_updates_stage set address_status= :address_status where EIN=:ein ";
		Query query = getCurrentSession().createSQLQuery(sql.toString());
		query.setParameter("address_status", "ADDED");
		query.setParameter("ein",ein);
		query.executeUpdate();

	}
	
	@Override
	public void softDeleteAddress(long companyId, String addressTypeCd){
		saveOrUpdateBackup(companyId, addressTypeCd);
		deleteAddress(companyId, addressTypeCd);
	}
	
	@SuppressWarnings("unchecked")
	public void saveOrUpdateBackup(long companyId, String addressTypeCd) {
		
		logger.debug("Inserting the address into backup table ", companyId);
		
		List<AddressEntity> results = null;
		Mapper mapper = mapperWrapper.getMapper();
		AddressEntity addressEntity = new AddressEntity();
		AddressEntityBK addressEntityBK = new AddressEntityBK();
		//CompanyEntity companyEntity = new CompanyEntity();
		Query query = getCurrentSession().createQuery("FROM AddressEntity ta WHERE ta.company.companyId = :companyId AND ta.addressTypeCd = :addressTypeCd");
			  query.setParameter("companyId", companyId);
			  query.setParameter("addressTypeCd", addressTypeCd);
			  
		results = query.list();
		if( null != results  && results.size() == 1 ){
			addressEntity = results.get(0);
		}
		//addressEntityBK.setCompany(companyEntity);
		mapper.map(addressEntity, addressEntityBK);
		addressEntityBK.setCompanyId(companyId);
		getCurrentSession().saveOrUpdate(addressEntityBK);
		getCurrentSession().flush();
		getCurrentSession().clear();
		logger.debug("Completed inserting the address into backup table ", companyId);
	}
	

	public void deleteAddress(long companyId, String addressTypeCd) {
		
		logger.debug("deleting the address ", companyId);

		Query query = getCurrentSession().createQuery("DELETE AddressEntity addressEntity  WHERE COMPANY_ID = :companyId and ADDRESS_TYPE_CD = :addressTypeCd");
			  query.setParameter("companyId", companyId);
			  query.setParameter("addressTypeCd", addressTypeCd);
			  query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();
	
		logger.debug("Completed deleting the address", companyId);

	}
	
	@Override
	public void updatePrimToAltnAddress(long companyId){
		logger.debug("updating the current prim address to altnerante ", companyId);

		Query query = getCurrentSession().createQuery("UPDATE AddressEntity addressEntity  set address_type_cd= 'ALTN' where company_id = :companyId AND address_type_cd = 'PRIM'");
			  query.setParameter("companyId", companyId);
			  query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();
	
		logger.debug("Completed updating the current prim address to altnerante", companyId);
	}

	@Override
	public void delete(long companyId,  String addressType) {
		deleteAddress(companyId, addressType);
	}
}

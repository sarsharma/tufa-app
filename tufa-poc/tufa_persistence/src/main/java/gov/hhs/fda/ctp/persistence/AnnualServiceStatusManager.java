package gov.hhs.fda.ctp.persistence;

import gov.hhs.fda.ctp.persistence.model.ServiceStatusEntity;

public interface AnnualServiceStatusManager {

	void saveServiceStatus(ServiceStatusEntity serviceStatus);

	ServiceStatusEntity getServiceStatus(String service, Long fiscalYear);

}

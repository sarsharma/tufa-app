package gov.hhs.fda.ctp.persistence;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.persistence.model.ServiceStatusEntity;

@Repository("annualServiceStatusManager")
@Transactional
public class AnnualServiceStatusManagerImp extends BaseEntityManagerImpl implements AnnualServiceStatusManager{
	
	Logger logger = LoggerFactory.getLogger(AnnualServiceStatusManagerImp.class);
	
	@Override
	public void saveServiceStatus(ServiceStatusEntity serviceStatus) {
		this.getCurrentSession().saveOrUpdate(serviceStatus);
	}
	
	@Override
	public ServiceStatusEntity getServiceStatus(String service, Long fiscalYear) {
		String hql = " FROM ServiceStatusEntity WHERE service_name =:service AND fiscal_year =:fiscalYear";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("service", service);
		query.setParameter("fiscalYear", fiscalYear);
		
		ServiceStatusEntity result = (ServiceStatusEntity) query.uniqueResult();
		
		return result;
	}
}

package gov.hhs.fda.ctp.persistence;

import java.util.List;
import java.util.Set;

import gov.hhs.fda.ctp.common.beans.AssessmentFilter;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentFilter;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.persistence.model.AssessmentEntity;
import gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity;

/**
 * The Interface AssessmentEntityManager.
 *
 * @author akari
 */
public interface AssessmentEntityManager {
	
	/**
	 * Adds the assessment.
	 *
	 * @param ae the ae
	 * @return the string
	 */
	public String addAssessment(AssessmentEntity ae);
	
	/**
	 * Gets the assessment.
	 *
	 * @param id the id
	 * @return the assessment
	 */
	public AssessmentEntity getAssessment(long id);
	
	/**
	 * Gets the assessments.
	 *
	 * @param criteria the criteria
	 * @return the assessments
	 */
	public List<AssessmentEntity> getAssessments(AssessmentFilter criteria);
	
	/**
	 * Get an assessment by year by quarter.
	 * 
	 * @param year
	 * @param qtr
	 * @return
	 */
	public AssessmentEntity getAssessmentByYearByQtr(int year, int qtr, String assessmentType);
	
	/**
	 * Update an assessment record.
	 * @param assessment
	 */
	public void updateAssessment(AssessmentEntity assessment);
	
	/**
	 * Gets the fiscal years.
	 *
	 * @return the fiscal years
	 */
	public Set<String> getFiscalYears();
	
	/**
	 * Generate the market share for a qtr/fiscal year.
	 *
	 * @param qtr the qtr
	 * @param fiscalYear the fiscal year
	 * @param Submit the submit
	 */
	
	public void generateQuarterlyMarketShare(int qtr, int fiscalYear, String Submit );	
	
	/**
	 * Generate the market share for a qtr/fiscal year.
	 *
	 * @param assess_qtr the assess qtr
	 * @param fiscalYear the fiscal year
	 * @param Submit the submit
	 * @param marketshareType the marketshare type
	 */
	
	public void generateCigarMarketShare(int assess_qtr, int fiscalYear, String Submit, String marketshareType );		

	/**
	 * Gets the cigar assessment.
	 *
	 * @param filter the filter
	 * @param assessmentType the assessment type
	 * @return the cigar assessment
	 */
	public List<AssessmentEntity> getCigarAssessment(CigarAssessmentFilter filter, String assessmentType);

    /**
     * Gets the support docs.
     *
     * @param assessmentId the assessment id
     * @return the support docs
     */
    public List<SupportingDocument> getSupportDocs(long assessmentId);
    
    public List<SupportingDocument> getCigarSupportDocs(long fiscalYr);
	
    /**
     * Save support document.
     *
     * @param supDoc the sup doc
     */
    public void saveSupportDocument(SupportingDocumentEntity supDoc);
    
    /**
     * Delete by doc id.
     *
     * @param asmtdocId the asmtdoc id
     */
    public void deleteByDocId(long asmtdocId);
    
    /**
     * Gets the doc by doc id.
     *
     * @param asmtdocId the asmtdoc id
     * @return the doc by doc id
     */
    public SupportingDocumentEntity getDocByDocId(long asmtdocId);
    

    
    public List<AssessmentEntity> getAssessmentByYear(long fiscalYr);
}

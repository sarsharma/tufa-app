package gov.hhs.fda.ctp.persistence;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Date;

import javax.persistence.ParameterMode;

import org.hibernate.Criteria;
import org.hibernate.query.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.common.beans.AssessmentFilter;
import gov.hhs.fda.ctp.common.beans.AssessmentVersionComparator;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentFilter;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.common.comparators.DescendingStringComparator;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.model.AssessmentEntity;
import gov.hhs.fda.ctp.persistence.model.AssessmentVersionEntity;
import gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity;

/**
 * The Class AssessmentEntityManagerImpl.
 */
@Repository("assessmentEntityManager")
public class AssessmentEntityManagerImpl extends BaseEntityManagerImpl implements AssessmentEntityManager {

	Logger logger = LoggerFactory.getLogger(AttachmentsEntityManagerImpl.class);

	@Autowired
	SimpleSQLManager sqlManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.AssessmentEntityManager#addAssessment(gov.hhs
	 * .fda.ctp.persistence.model.AssessmentEntity)
	 */
	@Override
	public String addAssessment(AssessmentEntity ae) {
		if (doesExist(ae.getAssessmentYr(), ae.getAssessmentQtr(), ae.getAssessmentType())) {
			return "0";
		}
		// add initial version 0
		Set<AssessmentVersionEntity> versions = new HashSet<>();
		AssessmentVersionEntity version = new AssessmentVersionEntity();
		version.setVersionNum(0L);
		version.setCreatedDt(new Date());
		version.setCreatedBy(getUser());
		versions.add(version);
		ae.setVersions(versions);
		this.getCurrentSession().save(ae);
		// CASCADE DOESN'T WORK WELL WITH ORACLE IDENTITY.
		version.setAssessmentId(ae.getAssessmentId());
		this.getCurrentSession().save(version);
		return "1";
	}

	@Override
	public AssessmentEntity getAssessment(long id) {
		return this.getCurrentSession().get(AssessmentEntity.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.AssessmentEntityManager#updateAssessment(gov.
	 * hhs.fda.ctp.persistence.model.AssessmentEntity)
	 */
	@Override
	public void updateAssessment(AssessmentEntity assessment) {
		this.getCurrentSession().update(assessment);
		this.getCurrentSession().flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gov.hhs.fda.ctp.persistence.AssessmentEntityManager#
	 * getAssessmentByYearByQtr(int, int)
	 */
	@Override
	public AssessmentEntity getAssessmentByYearByQtr(int year, int qtr, String assessmentType) {
		String hql = "FROM AssessmentEntity ae where ae.assessmentYr = :yr AND ae.assessmentQtr = :qtr AND trim(ae.assessmentType) = :assessmentType";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("yr", year);
		query.setParameter("qtr", qtr);
		query.setParameter("assessmentType", assessmentType);
		List<AssessmentEntity> result = query.list();
		//TODO: Error being thrown here: Index out of bounds
		return result.get(0);
	}
	@Override
	public List<AssessmentEntity> getAssessmentByYear(long fiscalYr) {
		String hql = "FROM AssessmentEntity ae where ae.assessmentYr = :fiscalYr AND ae.assessmentQtr <> :qtr AND trim(ae.assessmentType) = :assessmentType";
		Query query = getCurrentSession().createQuery(hql);
		query.setLong("fiscalYr", fiscalYr);
		query.setParameter("qtr", 0);
		query.setParameter("assessmentType", Constants.CIGR_QTRLY);
		List<AssessmentEntity> result = query.list();
		return result;
	}

	/**
	 * Does exist.
	 *
	 * @param year
	 *            the year
	 * @param qtr
	 *            the qtr
	 * @return true, if successful
	 */
	private boolean doesExist(int year, int qtr, String assessmentType) {
		String hql = "FROM AssessmentEntity ae where ae.assessmentYr = :yr AND ae.assessmentQtr = :qtr AND trim(ae.assessmentType) = :assessmentType";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("yr", year);
		query.setParameter("qtr", qtr);
		query.setParameter("assessmentType", assessmentType);
		if (query.list().isEmpty())
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.AssessmentEntityManager#getAssessments(gov.
	 * hhs.fda.ctp.common.beans.AssessmentFilter)
	 */
	@Override
	public List<AssessmentEntity> getAssessments(AssessmentFilter filter) {
		Integer[] quarters = { new Integer(1), new Integer(2), new Integer(3), new Integer(4) };
		Criteria criteria = getCurrentSession().createCriteria(AssessmentEntity.class);
		if (filter.getQuarterFilters() != null && !filter.getQuarterFilters().isEmpty()) {
			criteria.add(Restrictions.in("assessmentQtr", filter.getQuarterFilters().toArray()));
		} else {
			// we still want to include only quarters 1-4. quarter 0 indicates a
			// cigar assessment.
			criteria.add(Restrictions.in("assessmentQtr", (Object[]) quarters));
		}
		if (filter.getFiscalYrFilters() != null && !filter.getFiscalYrFilters().isEmpty()) {
			criteria.add(Restrictions.in("assessmentYr", filter.getFiscalYrFilters().toArray()));
		}
		if (filter.getStatusFilters() != null && !filter.getStatusFilters().isEmpty()) {
			criteria.add(Restrictions.in("submittedInd", filter.getStatusFilters().toArray()));
		}
		criteria.add(Restrictions.eq("assessmentType", Constants.QUARTERLY));
		criteria.addOrder(Order.desc("assessmentYr"));
		criteria.addOrder(Order.desc("assessmentQtr"));
		List<AssessmentEntity> assessments = criteria.list();

		/*
		 * Check to see if an assessment has versions greater than zero. If
		 * found set the submitted status as 'Y' else 'N'.
		 */
		for (AssessmentEntity assessmnt : assessments) {
			TreeSet<Long> versSorted = new TreeSet<Long>();
			Set<AssessmentVersionEntity> versions = assessmnt.getVersions();

			for (AssessmentVersionEntity versionEntity : versions)
				versSorted.add(versionEntity.getVersionNum());

			if (versSorted.higher(new Long(0)) != null) {
				assessmnt.setSubmittedInd("Y");
			} else
				assessmnt.setSubmittedInd("N");
		}

		return assessments;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gov.hhs.fda.ctp.persistence.AssessmentEntityManager#getFiscalYears()
	 */
	@Override
	public Set<String> getFiscalYears() {
		String sql = "SELECT Distinct(ASSESSMENT_YR) FROM TU_ASSESSMENT";
		Query query = getCurrentSession().createSQLQuery(sql);
		Set<String> years = new TreeSet<>(new DescendingStringComparator());
		@SuppressWarnings("unchecked")
		List<BigDecimal> results = query.list();
		if (results != null) {
			for (BigDecimal result : results) {
				years.add(result.toString());
			}
		}
		return years;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.AssessmentEntityManager#generateMarketShare(
	 * int, int, java.lang.String, int)
	 */
	@Override
	public void generateQuarterlyMarketShare(int qtr, int fiscalYear, String Submit) {
		ProcedureCall call;
		call = this.getCurrentSession().createStoredProcedureCall("calc_qtr_market_share");
		call.registerParameter("assess_yr", Integer.class, ParameterMode.IN);
		call.getParameterRegistration("assess_yr").bindValue(fiscalYear);
		call.registerParameter("assess_qtr", Integer.class, ParameterMode.IN);
		call.getParameterRegistration("assess_qtr").bindValue(qtr);
		call.registerParameter("author", String.class, ParameterMode.IN);
		call.getParameterRegistration("author").bindValue(getUser());
		call.getOutputs();
		return;
	}

	@Override
	public void generateCigarMarketShare(int assessQtr, int fiscalYear, String Submit, String marketShareType) {
		ProcedureCall call;
		call = this.getCurrentSession().createStoredProcedureCall("calc_cigar_market_share");
		call.registerParameter("assess_yr", Integer.class, ParameterMode.IN);
		call.getParameterRegistration("assess_yr").bindValue(fiscalYear);
		call.registerParameter("author", String.class, ParameterMode.IN);
		call.getParameterRegistration("author").bindValue(getUser());
		call.registerParameter("assess_qtr", Integer.class, ParameterMode.IN);
		call.getParameterRegistration("assess_qtr").bindValue(assessQtr);
		call.registerParameter("marketshare_type", String.class, ParameterMode.IN);
		call.getParameterRegistration("marketshare_type").bindValue(marketShareType);
		call.getOutputs();
		return;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.AssessmentEntityManager#getCigarAssessment(
	 * gov.hhs.fda.ctp.common.beans.CigarAssessmentFilter, java.lang.String)
	 */
	@Override
	public List<AssessmentEntity> getCigarAssessment(CigarAssessmentFilter filter, String assessmentType) {

		Criteria criteria = getCurrentSession().createCriteria(AssessmentEntity.class);

		criteria.add(Restrictions.in("assessmentType", assessmentType));

		if (filter != null) {
			Set<String> statii = filter.getStatusFilters();
			if (statii != null) {
				if (statii.contains("Not Submitted")) {
					criteria.add(Restrictions.eq("submittedInd", "N"));
				}
				if (statii.contains("Submitted")) {
					criteria.add(Restrictions.eq("submittedInd", "Y"));
				}
			}
		}
		criteria.add(Restrictions.ne("assessmentQtr", 0));

		criteria.addOrder(Order.desc("assessmentYr"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.AssessmentEntityManager#getSupportDocs(long)
	 */
	@Override
	public List<SupportingDocument> getSupportDocs(long assessmentId) {
		return this.sqlManager.findSupportingDocumentsByAssessmentId(assessmentId);
	}
	public List<SupportingDocument> getCigarSupportDocs(long fiscalYr) {
		return this.sqlManager.getCigarSupportDocs(fiscalYr);
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.AssessmentEntityManager#deleteByDocId(long)
	 */
	@Override
	public void deleteByDocId(long asmtdocId) {
		SupportingDocumentEntity doc = new SupportingDocumentEntity();
		doc.setAsmntDocId(asmtdocId);
		this.getCurrentSession().delete(doc);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.AssessmentEntityManager#saveSupportDocument(
	 * gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity)
	 */
	@Override
	public void saveSupportDocument(SupportingDocumentEntity supDoc) {
		supDoc.setAuthor(this.getUser());
		this.getCurrentSession().save(supDoc);
	}

	@Override
	public SupportingDocumentEntity getDocByDocId(long asmtdocId) {
		String hql = "FROM SupportingDocumentEntity doc where " + "doc.asmntDocId = :asmtdocId";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("asmtdocId", asmtdocId);
		List<SupportingDocumentEntity> result = query.list();
		return result.get(0);
	}
}

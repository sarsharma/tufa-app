package gov.hhs.fda.ctp.persistence;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitAttachmentsEntity;


/**
 * The Interface AttachmentsEntityManager.
 */
public interface AttachmentsEntityManager {
		
	/**
	 * Adds the attach.
	 *
	 * @param attach the attach
	 * @return the attachments entity
	 */
	public void addAttach(AttachmentsEntity attach);
	public void addPermitAttach(PermitAttachmentsEntity attach);

	/**
	 * Gets the permit using the permitId by using the Hibernate Session Object.
	 *
	 * @param docId the doc id
	 * @return the attachment
	 */
	public AttachmentsEntity getAttachment(long docId);
	
	public PermitAttachmentsEntity getPermitAttachment(long docId);
	
	public byte[] getAttachmentAsByteArry(long docId) throws SQLException;
	
	public byte[] getPermitAttachmentAsByteArry(long docId) throws SQLException;


	/**
	 * Find all.
	 *
	 * @return the list
	 */
	List<AttachmentsEntity> findAll();
	
	/**
	 * Find by ids.
	 *
	 * @param periodId the period id
	 * @param permitId the permit id
	 * @return the list
	 */
	List<AttachmentsEntity> findByIds(long periodId, long permitId);
	
	List<PermitAttachmentsEntity> findPermitDocbyId(long permitId);
	
	/**
	 * Delete by id.
	 *
	 * @param id the id
	 */
	void deleteById(long id,String attachFrm);
	
	Blob getBlob(byte[] doc) throws SQLException;

	public byte[] getByteArray(Blob blob) throws SQLException;
	
}

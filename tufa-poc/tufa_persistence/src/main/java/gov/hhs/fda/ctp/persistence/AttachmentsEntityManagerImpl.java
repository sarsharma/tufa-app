package gov.hhs.fda.ctp.persistence;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitAuditEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpEntity;

/**
 * The Class AttachmentsEntityManagerImpl.
 */
@Repository("attachmentsEntityManager")
public class AttachmentsEntityManagerImpl extends BaseEntityManagerImpl implements AttachmentsEntityManager {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(AttachmentsEntityManagerImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see gov.hhs.fda.ctp.persistence.AttachmentsEntityManager#findAll()
	 */
	@Autowired
	private SimpleSQLManager sqlManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<AttachmentsEntity> findAll() {
		Query query = getCurrentSession()
				.createQuery("SELECT attachments.documentId FROM AttachmentsEntity attachments");
		Long id = (Long) query.list().get(0);
		AttachmentsEntity attachment = getCurrentSession().get(AttachmentsEntity.class, id);
		List<AttachmentsEntity> returnList = new ArrayList<AttachmentsEntity>();
		returnList.add(attachment);
		return returnList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gov.hhs.fda.ctp.persistence.AttachmentsEntityManager#findByIds(long,
	 * long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<AttachmentsEntity> findByIds(long periodId, long permitId) {
		return this.sqlManager.findByIds(periodId, permitId);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.AttachmentsEntityManager#deleteById(long)
	 */
	@Override
	public void deleteById(long id,String attachFrm) {
		if("permit".equalsIgnoreCase(attachFrm)){
			PermitAttachmentsEntity doc = new PermitAttachmentsEntity();
			doc.setDocumentId(id);
			getCurrentSession().delete(doc);
		}else{
			/*AttachmentsEntity doc = new AttachmentsEntity();
			doc.setDocumentId(id);*/
			this.getCurrentSession().delete(getAttachment(id));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.AttachmentsEntityManager#getAttachment(long)
	 */
	@Override
	public AttachmentsEntity getAttachment(long docId) {
		return this.getCurrentSession().get(AttachmentsEntity.class, docId);
	}
	
	@Override
	public PermitAttachmentsEntity getPermitAttachment(long docId) {
		return this.getCurrentSession().get(PermitAttachmentsEntity.class, docId);
	}
	

	@Override
	public byte[] getAttachmentAsByteArry(long docId) throws SQLException {

		String sql = "select REPORT_PDF from TU_DOCUMENT TD where TD.document_Id=:docId";
		Query qry = this.getCurrentSession().createSQLQuery(sql);
		qry.setParameter("docId", docId);

		List reportLst = qry.list();
		Blob blob =  (Blob) reportLst.get(0);
	
		byte [] bytes  = this.getByteArray(blob);
		if(blob != null) {
			blob.free();
		}
		
		return bytes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.AttachmentsEntityManager#addAttach(gov.hhs.
	 * fda.ctp.persistence.model.AttachmentsEntity)
	 */
	@Override
	public void addAttach(AttachmentsEntity attach) {
		getCurrentSession().saveOrUpdate(attach);
	}
	
	public void addPermitAttach(PermitAttachmentsEntity attach){
		getCurrentSession().saveOrUpdate(attach);
	}
	
	@Override
	public Blob getBlob(byte[] doc) throws SQLException {
		return Hibernate.getLobCreator(this.getCurrentSession()).createBlob(doc);
	}

	@Override
	public byte[] getByteArray(Blob blob) throws SQLException {
		int offSet = 0;
		byte [] bytes  = {};
		if(blob != null) {
		    long pos = offSet+1;
			long blobLen = blob.length()-offSet;
			
			bytes = blob.getBytes(pos, (int)blobLen);
	         
			blob.free();
		}
		return bytes;
	}

	@Override
	public List<PermitAttachmentsEntity> findPermitDocbyId(long permitId) {
		return this.sqlManager.findByPermitId(permitId);
	
	}
	
	@Override
	public byte[] getPermitAttachmentAsByteArry(long docId) throws SQLException {

		Query query = getCurrentSession().createQuery(
				"SELECT permitAttachmentEntity.reportPdf FROM PermitAttachmentsEntity permitAttachmentEntity WHERE permitAttachmentEntity.documentId = :documentId");
		query.setParameter("documentId", docId);

		List reportLst = query.list();
		Blob blob =  (Blob) reportLst.get(0);
	
		byte [] bytes  = this.getByteArray(blob);
		if(blob != null) {
			blob.free();
		}
		
		return bytes;
	}
}

package gov.hhs.fda.ctp.persistence;

import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import gov.hhs.fda.ctp.persistence.model.TufaEntity;

/**
 * The Interface BaseEntityManager.
 */
public interface BaseEntityManager {

	/**
	 * Gets the session factory.
	 *
	 * @return the session factory
	 */
	public SessionFactory getSessionFactory();
	
	/**
	 * Gets the current session.
	 *
	 * @return the current session
	 */
	public Session getCurrentSession();
	
	/**
	 * Save entity.
	 *
	 * @param entity the entity
	 */
	public void saveEntity(TufaEntity entity);
	
	
	public String getUser();


	/**
	 * Test criterion.
	 *
	 * @param criterion
	 *            the criterion
	 * @return true, if successful
	 */
	public boolean testCriterion(String criterion);


	/**
	 * Test criterion.
	 *
	 * @param criterion
	 *            the criterion
	 * @return true, if successful
	 */
	public boolean testCriterion(Set<String> criterion);
		

	/**
	 * Safe to string.
	 *
	 * @param result
	 *            the result
	 * @return the string
	 */
	public String safeToString(Object result);
	
	public void setSQLQueryFilteredRowCount(int rowCount);
	
}

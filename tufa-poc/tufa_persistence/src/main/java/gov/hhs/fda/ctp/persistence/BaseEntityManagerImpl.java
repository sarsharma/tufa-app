package gov.hhs.fda.ctp.persistence;

import java.util.Set;

import javax.persistence.ParameterMode;
import javax.validation.constraints.Null;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.procedure.ProcedureOutputs;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.common.pagination.PaginationAttributes;
import gov.hhs.fda.ctp.common.security.UserContext;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.persistence.model.TufaEntity;
import gov.hhs.fda.ctp.querybuilder.OracleSqlQueryBuilder;

/**
 * The base class which will host the Hibernate sessionFactory and convenient
 * methods to retrieve hibernate session and other common methods that
 * subclasses can reuse.
 */
@Repository("baseEntityManager")
public abstract class BaseEntityManagerImpl implements BaseEntityManager {

	@Autowired
	private PaginationAttributes paginationAttributes;

	
	/** The session factory. */
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private SecurityContextUtil securityContextUtil;
	
	@Autowired 
	private OracleSqlQueryBuilder queryBuilder = new OracleSqlQueryBuilder();


	@Override
	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.BaseEntityManager#saveEntity(gov.hhs.fda.ctp.
	 * persistence.model.TufaEntity)
	 */
	@Override
	public void saveEntity(TufaEntity entity) {
		getCurrentSession().save(entity);
	}

	/**
	 * Update entity.
	 *
	 * @param entity
	 *            the entity
	 */
	public void updateEntity(TufaEntity entity) {
		getCurrentSession().saveOrUpdate(entity);
	}

	@Override
	public String getUser() {
		try {
			UserContext user = securityContextUtil.getPrincipal();
			return user.getUsername();
		} catch (Exception e) {
			return "UNITTEST";
		}
	}

	public PaginationAttributes getPaginationAttributes() {
		return paginationAttributes;
	}

	public void setPaginationAttributes(PaginationAttributes paginationAttributes) {
		this.paginationAttributes = paginationAttributes;
	}

	public void setSQLQueryTotalRowCount(int rowCount) {
		this.getPaginationAttributes().setItemsTotal(rowCount);
	}
	public void setSQLQueryFilteredRowCount(int rowCount) {
		this.getPaginationAttributes().setFilteredRows(rowCount);
	}
	public ProcedureCall getStoredProcedureByName(String storeProc) {
		return this.getCurrentSession().createStoredProcedureCall(storeProc);
	}

	public Integer getTotalCount(ProcedureCall procCall, String tableName1, String tableName2, String columnName,
			String columnValue, Integer FISCAL_YEAR, boolean ADD_HINT) {

		Integer rowCnt = 0;
		procCall.registerParameter("TABLENAME1", String.class, ParameterMode.IN).bindValue(tableName1);
		if(tableName2 != null){
			procCall.registerParameter("TABLENAME2", String.class, ParameterMode.IN).bindValue(tableName2);
		}
		procCall.registerParameter("COLUMNNAME", String.class, ParameterMode.IN).bindValue(columnName);
		procCall.registerParameter("COLUMNVALUE", String.class, ParameterMode.IN).bindValue(columnValue);
		
		if (FISCAL_YEAR  !=  null){
		    procCall.registerParameter("FISCAL_YEAR", Integer.class, ParameterMode.IN).bindValue(FISCAL_YEAR);
		}

	    /* Added this to give the option to add an SQL hint.
	        Some times this is needed to improve the performance.
	        Other times it can hinder the performance 
	        Nov 6, 2018 */
		if (ADD_HINT) {
			procCall.registerParameter("ADD_HINT", String.class, ParameterMode.IN).bindValue("Y");
		}
		procCall.registerParameter("TTL_COUNT", Integer.class, ParameterMode.OUT);
		ProcedureOutputs outputs = procCall.getOutputs();
		rowCnt = (Integer) outputs.getOutputParameterValue("TTL_COUNT");
		return rowCnt;
	}
	
	public Integer getTotalCountAmmendApprv(ProcedureCall procCall, String tableName1, String tableName2,
			 Integer FISCAL_YEAR) {

		Integer rowCnt = 0;
		procCall.registerParameter("TABLENAME1", String.class, ParameterMode.IN).bindValue(tableName1);
		if(tableName2 != null){
			procCall.registerParameter("TABLENAME2", String.class, ParameterMode.IN).bindValue(tableName2);
		}
		
		if (FISCAL_YEAR  !=  null){
		    procCall.registerParameter("FISCAL_YEAR", Integer.class, ParameterMode.IN).bindValue(FISCAL_YEAR);
		}
		procCall.registerParameter("TTL_COUNT", Integer.class, ParameterMode.OUT);
		ProcedureOutputs outputs = procCall.getOutputs();
		rowCnt = (Integer) outputs.getOutputParameterValue("TTL_COUNT");
		return rowCnt;
	}



	public Query getPaginatedSQLQuery(StringBuffer sql) {
		PaginationAttributes pgattrs = this.getPaginationAttributes();
		Integer activePg = pgattrs.getActivePage();
		Integer pageSize = pgattrs.getPageSize();
		String sortOrderArry[] = null;
		if (pgattrs.getSortBy() != null) {
			sql = sql.append(" ORDER BY "); 
			String[] sortParams = pgattrs.getSortBy();
			String sortOrders = pgattrs.getSortOrder();

			if (sortOrders != null)
				sortOrderArry = sortOrders.split(";");

			for (int i = 0; i < sortParams.length; i++) {
				sql.append(sortParams[i] + " ");

				if (sortOrders != null && sortOrderArry.length >= i + 1)
					sql.append(sortOrderArry[i]);

				if (i < sortParams.length - 1)
					sql.append(",");

			}
		}
		Query pgQuery = getCurrentSession().createSQLQuery(sql.toString());
		if (activePg != null && pageSize != null && !(activePg == 0 && pageSize ==0)) {
			pgQuery.setFirstResult((activePg - 1) * pageSize);
			pgQuery.setMaxResults(pageSize);
		}

		return pgQuery;
	}
	
	public Query getPaginatedSQLQuery(StringBuffer sql, String sortColumnName) {
		PaginationAttributes pgattrs = this.getPaginationAttributes();
		Integer activePg = pgattrs.getActivePage();
		Integer pageSize = pgattrs.getPageSize();
		String sortOrderArry[] = null;
		if (pgattrs.getSortBy() != null) {
			sql = sql.append(" ORDER BY "); 
			String[] sortParams = pgattrs.getSortBy();
			String sortOrders = pgattrs.getSortOrder();

			if (sortOrders != null)
				sortOrderArry = sortOrders.split(";");

			for (int i = 0; i < sortParams.length; i++) {
				if(sortColumnName!= null && (sortColumnName.equals("LEGAL_NM") || sortColumnName.equals("company_nm")) && sortParams[i].equals("1"))
					sql.append("LOWER("+sortColumnName+") ");
				else
					sql.append(sortParams[i] + " ");

				if (sortOrders != null && sortOrderArry.length >= i + 1)
					sql.append(sortOrderArry[i]);

				if (i < sortParams.length - 1)
					sql.append(",");

			}
		}
		Query pgQuery = getCurrentSession().createSQLQuery(sql.toString());
		if (activePg != null && pageSize != null && !(activePg == 0 && pageSize ==0)) {
			pgQuery.setFirstResult((activePg - 1) * pageSize);
			pgQuery.setMaxResults(pageSize);
		}

		return pgQuery;
	}


	@Override
	public boolean testCriterion(String criterion) {
		return (criterion != null && !criterion.isEmpty());
	}

	@Override
	public boolean testCriterion(Set<String> criterion) {
		return (criterion != null && !criterion.isEmpty());
	}

	@Override
	public String safeToString(Object result) {
		if (result != null) {
			return result.toString().trim();
		}
		return null;
	}

	public OracleSqlQueryBuilder getQueryBuilder() {
		return queryBuilder;
	}

	public void setQueryBuilder(OracleSqlQueryBuilder queryBuilder) {
		this.queryBuilder = queryBuilder;
	}

	@Override
	public SessionFactory  getSessionFactory() {
		return this.sessionFactory;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
}

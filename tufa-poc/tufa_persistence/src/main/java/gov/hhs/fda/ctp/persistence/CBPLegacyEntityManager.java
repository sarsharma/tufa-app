package gov.hhs.fda.ctp.persistence;

import gov.hhs.fda.ctp.persistence.model.CBPUploadEntity;

public interface CBPLegacyEntityManager {
	
	public void insertAcceptedCBPLegacyData(long fiscalYr);

}

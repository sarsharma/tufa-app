package gov.hhs.fda.ctp.persistence;

import org.springframework.stereotype.Repository;
import javax.persistence.ParameterMode;
import org.hibernate.procedure.ProcedureCall;
import gov.hhs.fda.ctp.persistence.model.CBPUploadEntity;

@Repository("CBPLegacyEntityManager")
public class CBPLegacyEntityManagerImpl extends BaseEntityManagerImpl implements CBPLegacyEntityManager {
	
	@Override
	public void insertAcceptedCBPLegacyData(long fiscalYr) {
		
		ProcedureCall procCall = this.getStoredProcedureByName("LOAD_CBP_MERGEDVIEW_DATA");
		procCall.registerParameter("P_FISCAL_YR", Long.class,  ParameterMode.IN).bindValue(fiscalYr);
		procCall.getOutputs();
	}
}

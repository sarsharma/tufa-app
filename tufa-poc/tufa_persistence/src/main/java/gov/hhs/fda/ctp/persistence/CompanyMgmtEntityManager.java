/**
 * 
 */
package gov.hhs.fda.ctp.persistence;

import java.sql.Blob;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.hibernate.procedure.ProcedureOutputs;

import gov.hhs.fda.ctp.common.beans.AffectedAssessmentbean;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.ExcludePermitBean;
import gov.hhs.fda.ctp.common.beans.ExcludePermitRequest;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.PermitCommentAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;
import gov.hhs.fda.ctp.persistence.model.PermitExcludeCommentEntity;

/**
 * The Interface CompanyMgmtEntityManager.
 *
 * @author tgunter
 */
public interface CompanyMgmtEntityManager {
    /**
     * Create a new company with an attached list of permits.
     * @param company
     * @return company
     */
    public CompanyEntity createCompany(CompanyEntity company);
    /**
     * Update an existing company.  Will not cascade changes to attached permits.
     * @param company
     * @return company
     */
    public CompanyEntity updateCompany(CompanyEntity company);
    /**
     * Get an existing company and attached permits by id.
     * @param id
     * @return company
     */
    public CompanyEntity getCompanyById(long id);
    
    public CompanyEntity getCompanyByEIN(String EIN);
    
    /**
     * Get a list of all companies.
     * 
     * @return List<CompanyEntity>
     */
    public List<CompanyEntity> getAllCompanies();

    /**
     * Get all permits for a company by companyId.  Filter by status.
     * @param status
     * @param companyId
     * @return
     */
    public List<PermitEntity> getPermits(String status, long companyId);
    
    public PermitEntity getPermitById(long cmpnyId, String permitNum);
    
	public CompanyEntity getActvCBPCompByEIN(String ein);
	
	public List<CompanyEntity> getCompanyTrackingInformationForExport(CompanySearchCriteria criteria);
	
	/**
     * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * @param companyId
     * @param permitId
     * @param exclusionType
     * @param request
	 * @param commentEntity 
     */
    public void updateExcludeAllPermits(long companyId, long permitId,String exclusionType, List<ExcludePermitRequest> request, PermitExcludeCommentEntity commentEntity);
    
    /**
     * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * @param permitId
     * @param companyId TODO
     */
    public List<Object[]> getPermitExcludeDetails(long permitId, long companyId);
    
    public List<Object[]> getExcludePermitsForCompany(long companyId);
    
	public List<PermitExcludeCommentEntity> getPermitExcludeComments(long companyId, long permitId);
    
    public PermitCommentAttachmentsEntity getPermitCommentAttachment(long docId);
    public List<PermitCommentAttachmentsEntity> findCommentDocbyId(long commentId);
	public byte[] getPermitCommentAttachmentAsByteArray(long docId) throws SQLException;
	public void addPermitCommentAttachment(PermitCommentAttachmentsEntity attach);
	public void deleteDocById(long docId);

    Blob getBlob(byte[] doc) throws SQLException;
	public byte[] getByteArray(Blob blob) throws SQLException;
	
	public void updatePermitExcludeComment(PermitExcludeCommentEntity commentEntity);
	
	/**
	 * CTPTUFA-4947 - Assessments: Show table under the Assessments page that lists all Permits marked as Excluded Pt2
	 * @param companyId
	 * @param permitId
	 * @return
	 */
	public List<Object[]> getAffectedAssessmentData();
	
    /**
     * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * this will send warning message for affected assessment data for permit being excluded
     * @param companyId
     * @param fiscalYear
     * @param quarter
     * @param exclusionScopeId
     * @return
     */
    public ProcedureOutputs getAffectedAssmtwarning(long permitId,long companyId,long fiscalYear,long quarter, long exclusionScopeId);
    
    /**
     * * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * this will send warning message for COmpany having active permit for the period for which permit is being excluded
     * @param companyId
     * @param permitId
     * @param fiscalYear
     * @param quarter
     * @param exclusionType
     * @return
     * @throws  
     */
    public boolean getWarningForPermitBeingExcluded(long companyId,
            long permitId, String fiscalYear, String quarter,
            long exclusionType)throws ParseException;
	
    public PermitEntity getPermitbyId(long companyId,long permitId);
    
    public String getAssociatedCompanyByEIN(long fiscalYear,long fiscalQtr, String tobaccoClassNm, String ein);

}

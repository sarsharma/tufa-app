/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.ParameterMode;

import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Hibernate;
import org.hibernate.query.Query;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.procedure.ProcedureOutputs;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.ExcludePermitBean;
import gov.hhs.fda.ctp.common.beans.ExcludePermitRequest;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.PermitCommentAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;
import gov.hhs.fda.ctp.persistence.model.PermitExcludeCommentEntity;


/**
 * The Class CompanyMgmtEntityManagerImpl.
 *
 * @author tgunter
 */
@Repository("companyMgmtEntityManager")
public class CompanyMgmtEntityManagerImpl  extends BaseEntityManagerImpl implements CompanyMgmtEntityManager {

    /** The logger. */
    Logger logger = LoggerFactory.getLogger(CompanyMgmtEntityManagerImpl.class);
    @Autowired
	private SimpleSQLManager sqlManager;
    enum Contex {
    	SEARCH, EXPORT;
    }
    
    /**
     * Create a new company.
     *
     * @param companyEntity the company entity
     * @return the company entity
     */
    @Override
    public CompanyEntity createCompany(CompanyEntity companyEntity) {
        getCurrentSession().save(companyEntity);
        Set<PermitEntity> permits = companyEntity.getPermits();
        if(permits != null && !permits.isEmpty()) {
            for(PermitEntity permit:permits) {
                getCurrentSession().saveOrUpdate(permit);
            }
        }
        
        return companyEntity;
    }

    /**
     * Update an existing company.
     *
     * @param companyEntity the company entity
     * @return the company entity
     */
    @Override
    public CompanyEntity updateCompany(CompanyEntity companyEntity) {
        getCurrentSession().update(companyEntity);
        return companyEntity;
    }

    /**
     * Read a company by id.
     *
     * @param id the id
     * @return the company by id
     */
    @SuppressWarnings("rawtypes")
	@Override
    public CompanyEntity getCompanyById(long id) {
        String hql = "FROM CompanyEntity companyEntity where companyEntity.companyId = :id";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("id", id);
        List results = query.list();
        return (CompanyEntity)((results!=null && !results.isEmpty()) ? results.get(0):null);
    }

    /**
     * Return a list of all companies.
     * @return List<CompanyEntity>
     */
    @Override
    public List<CompanyEntity> getAllCompanies() {
    	String hql = "FROM CompanyEntity";
    	Query query = getCurrentSession().createQuery(hql);
    	return query.list();
    }

    /**
     * Get all permits for a company by companyid, filter by status.
     *
     * @param permitStatus the permit status
     * @param companyId the company id
     * @return the permits
     */
	@Override
	public List<PermitEntity> getPermits(String permitStatus, long companyId) {
		String hql = " From CompanyEntity c join c.permits p where c.companyId = :companyId "
				+ " and p.permitStatusTypeCd = :status";

		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("companyId", companyId);
		query.setParameter("status", permitStatus);

		List<PermitEntity> permitList = new ArrayList<>();
		CompanyEntity cmpyEntity;
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.list();
		Object[] result = (results!=null && !results.isEmpty())? results.get(0):null;
		if(result != null) {
		    cmpyEntity = (CompanyEntity)result[0];
		    if(cmpyEntity.getPermits() != null) {
	            permitList = new ArrayList<>(cmpyEntity.getPermits());
		    }
		}
		return permitList;
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.CompanyMgmtEntityManager#getPermitById(long, java.lang.String)
	 */
	@Override
	public PermitEntity getPermitById(long cmpnyId, String permitNum) {
		String hql = " From CompanyEntity c join c.permits p where c.companyId = :companyId "
				+ " and p.permitNum like '"+permitNum+"%' and p.permitStatusTypeCd = 'ACTV'";

		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("companyId", cmpnyId);
		//query.setParameter("permitNo", permitNum);

		PermitEntity permit = null;
		CompanyEntity cmpyEntity;
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.list();
		Object[] result = (results!=null && !results.isEmpty())? results.get(0):null;
		if(result != null) {
			permit = (PermitEntity)result[1];
		}
		return permit;
	}
	
	@Override
	public CompanyEntity getCompanyByEIN(String EIN) {
		String hql = "From CompanyEntity ce where ce.EIN = :ein";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("ein", EIN);
		List<CompanyEntity> results = query.list();
		return (results != null && results.size() == 1) ? results.get(0) : null;
	}

	@Override
	public CompanyEntity getActvCBPCompByEIN(String ein) {
		String hql = "From CompanyEntity c JOIN c.permits p where c.EIN = :ein AND p.permitTypeCd = 'IMPT'";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("ein", ein);
		
		List<Object[]> results = query.list();
		Object[] result = (results!=null && !results.isEmpty())? results.get(0):null;
		CompanyEntity cmpyEntity = null;
		if(result != null) {
			cmpyEntity = (CompanyEntity) result[0];
		}
		
		return cmpyEntity;
	}
	
	@Override
	public List<CompanyEntity> getCompanyTrackingInformationForExport(CompanySearchCriteria criteria) {
		String hql = " FROM CompanyEntity c WHERE (c.welcomeLetterFlag = 'Y' AND c.welcomeLetterSentDt IS NOT NULL) OR c.welcomeLetterFlag = 'N'";
		Query query = this.getCurrentSession().createQuery(hql);
		List<CompanyEntity> results = query.list();
		return results;
	}

	 /**
     *CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     *this implementation is based on draft of stored procedure as of now. may chnage inpput and out parameters
     */
    @Override
    public void updateExcludeAllPermits(long companyId, long permitId,String exclusionType, List<ExcludePermitRequest> exclusionRange, PermitExcludeCommentEntity comment) {
        this.getCurrentSession().save(comment);
    	for(ExcludePermitRequest range : exclusionRange) {
            ProcedureCall procedureCall = this.getStoredProcedureByName("permit_excl_pkg.EXCLUDE_PERMIT_FROM_MKTSH");
            procedureCall.registerParameter("company_id", Long.class, ParameterMode.IN).bindValue(companyId);
            procedureCall.registerParameter("permit_id", Long.class, ParameterMode.IN).bindValue(permitId);
            procedureCall.registerParameter("exclusion_flag", String.class, ParameterMode.IN).bindValue(exclusionType);
            procedureCall.registerParameter("excl_asmt_fiscal_yr", Long.class, ParameterMode.IN).bindValue(Long.parseLong(range.getFiscalYear()));
            procedureCall.registerParameter("new_comment_id", Long.class, ParameterMode.IN).bindValue(comment.getCommentSeq());
            
            List<Long> quarterList = new ArrayList<Long>();
             
            for(String qtr : range.getQuarterselected()) {
                quarterList.add(Long.parseLong(qtr));
            }
            for(int i=0;i<quarterList.size();i++) {
                procedureCall.registerParameter("excl_qtr_"+quarterList.get(i), Long.class, ParameterMode.IN).bindValue(quarterList.get(i));
            }
            
//            procedureCall.registerParameter("excl_qtr_2", Long.class, ParameterMode.IN).bindValue(new Long(2));
//            procedureCall.registerParameter("excl_qtr_3", Long.class, ParameterMode.IN).bindValue(new Long(3));
//            procedureCall.registerParameter("excl_qtr_4", Long.class, ParameterMode.IN).bindValue(new Long(4));
            
            /*
             * TODO:once have fix , how to do in hibernate
             * Long[] qtrArray = new Long[quarterList.size()]; for(int
             * i=0;i<quarterList.size();i++) { qtrArray[i] = quarterList.get(i);
             * } Array array= null; OracleConnection conn = null; try { conn =
             * (OracleConnection)this.getCurrentSession().getSessionFactory().
             * getSessionFactoryOptions().getServiceRegistry().getService(
             * ConnectionProvider.class).getConnection(); //OracleConnection
             * oraConn = conn.unwrap(OracleConnection.class); array =
             * conn.createOracleArray("permit_excl_pkg.number_arry_type",
             * qtrArray); } catch (SQLException e) { // TODO Auto-generated
             * catch block e.printStackTrace(); }
             */
            
            procedureCall.registerParameter("user_logged", String.class, ParameterMode.IN).bindValue(getUser());
            ProcedureOutputs outputs = procedureCall.getOutputs();
            System.out.println(outputs);
//            int assessmentId = (Integer) outputs.getOutputParameterValue("assessment_id");
//            int assesmentType = (Integer) outputs.getOutputParameterValue("assessment_type");
        }
    }

    @Override
    public List<Object[]> getPermitExcludeDetails(long permitId, long companyId) {
        Query query = getCurrentSession().createSQLQuery("select vw.ASSMNT_QTR,vw.ASSMNT_YR,vw.CREATED_BY,vw.CREATED_DT,vw.EXCL_SCOPE_DESC,vw.excl_scope_id  "
                + "from permit_excl_status_vw vw where vw.company_id= :companyId and vw.permit_id= :permitId")
                .addScalar("ASSMNT_QTR", new StringType())
                .addScalar("ASSMNT_YR", new StringType())
                .addScalar("CREATED_BY", new StringType())
                .addScalar("CREATED_DT", new DateType())
                .addScalar("EXCL_SCOPE_DESC", new StringType())
                .addScalar("excl_scope_id", new LongType());
        query.setParameter("companyId", companyId);
        query.setParameter("permitId", permitId);
        List<Object[]> list = query.list();
        return list;
    }

    
    /**
     *CTPTUFA-5256-Company Details Page: Add a flag/indicator to the Permits Panel to indicate if a Permit is Excluded
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> getExcludePermitsForCompany(long companyId) {
        //select distinct t2.permit_id, max(t2.created_dt) as MaxDateTime from permit_excl_status_vw t2 where t2.excl_scope_id != 3 group by t2.permit_id
        Query query = getCurrentSession().createSQLQuery("select distinct t2.permit_id, max(t2.created_dt) as MaxDateTime from "
                + "permit_excl_status_vw t2 where t2.excl_scope_id != 3 and t2.company_id= :companyId group by t2.permit_id")
                .addScalar("PERMIT_ID", new LongType())
                .addScalar("MaxDateTime",new DateType());
        query.setParameter("companyId", companyId);
        List<Object[]> permitList = query.list();
        return permitList;
    }
    
    @Override
    public List<PermitExcludeCommentEntity> getPermitExcludeComments(long companyId, long permitId) {

    	Query query = getCurrentSession().createQuery("SELECT DISTINCT COM "
    			+ "FROM PermitExcludeCommentEntity as COM JOIN COM.audits as AUD LEFT JOIN COM.attachments "
    			+ "WHERE AUD.comapnyId = :comapnyId AND AUD.permitId = :permitId");
    	query.setParameter("comapnyId", companyId);
    	query.setParameter("permitId", permitId);
    	List<PermitExcludeCommentEntity> result = query.list();
    	return result;
    }
    
    @Override
	public void updatePermitExcludeComment(PermitExcludeCommentEntity commentEntity) {
		getCurrentSession().update(commentEntity);
	}
    
    /**
     *CTPTUFA-4947 - Assessments: Show table under the Assessments page that lists all Permits marked as Excluded Pt2
     */
    @Override
    public List<Object[]> getAffectedAssessmentData() {
        Query query = getCurrentSession().createSQLQuery("select vw.ASSESSMENT_YR,vw.ASSESSMENT_TYPE,vw.LEGAL_NM,vw.EIN,vw.PERMIT_NUM,vw.ASSESSMENT_QTR from permit_excl_affcted_assmnts_vw vw ORDER BY vw.ASSESSMENT_YR DESC, vw.ASSESSMENT_QTR DESC")
                .addScalar("ASSESSMENT_YR",new LongType())
                .addScalar("ASSESSMENT_TYPE",new StringType())
                .addScalar("LEGAL_NM",new StringType())
                .addScalar("EIN",new StringType())
                .addScalar("PERMIT_NUM",new StringType())
                .addScalar("ASSESSMENT_QTR",new LongType());
        List<Object[]> list = query.list();
        return list;
    }
	
	
	
	@Override
	public byte[] getPermitCommentAttachmentAsByteArray(long docId) throws SQLException {

		Query query = getCurrentSession().createQuery(
				"SELECT permitCommentAttachmentsEntity.reportPdf FROM PermitCommentAttachmentsEntity permitCommentAttachmentsEntity WHERE permitCommentAttachmentsEntity.documentId = :documentId");
		query.setParameter("documentId", docId);

		List reportLst = query.list();
		Blob blob =  (Blob) reportLst.get(0);
	
		byte [] bytes  = this.getByteArray(blob);
		if(blob != null) {
			blob.free();
		}
		
		return bytes;
	}
	public void deleteDocById(long docId){
		/*PermitCommentAttachmentsEntity doc = new PermitCommentAttachmentsEntity();
		doc.setDocumentId(docId);*/
		getCurrentSession().delete(getPermitCommentAttachment(docId));
	}

	@Override
	public PermitCommentAttachmentsEntity getPermitCommentAttachment(long docId) {
		return this.getCurrentSession().get(PermitCommentAttachmentsEntity.class, docId);

	}

	public void addPermitCommentAttachment(PermitCommentAttachmentsEntity attach){
		getCurrentSession().saveOrUpdate(attach);
	}
    
    @Override
	public Blob getBlob(byte[] doc) throws SQLException {
		return Hibernate.getLobCreator(this.getCurrentSession()).createBlob(doc);
	}

	@Override
	public byte[] getByteArray(Blob blob) throws SQLException {
		int offSet = 0;
		byte [] bytes  = {};
		if(blob != null) {
		    long pos = offSet+1;
			long blobLen = blob.length()-offSet;
			
			bytes = blob.getBytes(pos, (int)blobLen);
	         
			blob.free();
		}
		return bytes;
	}

	@Override
	public List<PermitCommentAttachmentsEntity> findCommentDocbyId(long commentId) {
		return this.sqlManager.findCommentDocbyId(commentId);
	}

    /**
     * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * this will send warning message for affected assessment data for permit being excluded
     */
    @Override
    public ProcedureOutputs getAffectedAssmtwarning(long permitId,long companyId, long fiscalYear,long quarter, long exclusionScopeId) {
        ProcedureCall procedureCall = this.getStoredProcedureByName("permit_excl_pkg.affected_assmnt_exist");
        procedureCall.registerParameter("assmnt_yr", Long.class, ParameterMode.IN).bindValue(fiscalYear);
        procedureCall.registerParameter("asmnt_qtr", Long.class, ParameterMode.IN).bindValue(quarter);
        procedureCall.registerParameter("company_id", Long.class, ParameterMode.IN).bindValue(companyId);
        procedureCall.registerParameter("exclusion_scope_id ", Long.class, ParameterMode.IN).bindValue(exclusionScopeId);
        procedureCall.registerParameter("permit_id", Long.class, ParameterMode.IN).bindValue(permitId);
        procedureCall.registerParameter("affected_assmnt_size", Long.class, ParameterMode.OUT);
        ProcedureOutputs outputs = procedureCall.getOutputs();
        return outputs;
    }

    
    
    /**
     ** CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * this will send warning message for COmpany having active permit for the period for which permit is being excluded
     * @throws ParseException 
     */
    @Override
    public boolean getWarningForPermitBeingExcluded(long companyId,long permitId, String fiscalYear, String quarter,long incomingExclusionType) throws ParseException {
        List<ExcludePermitBean> excludedList = getExistingExcludedPermit(companyId);
        //List<BigDecimal> tuPermitList = getPermitList(companyId);
       //List<Long> excludedPermit = (List)CollectionUtils.collect(excludedList, new BeanToPropertyValueTransformer("permitId"));   
        List<PermitEntity> tufaPermitList =  getAllPermits(companyId);
        String[] selecQtrs  = quarter.split(",");
       
        long fiscalYr = Long.parseLong(fiscalYear);
        DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
       // String excludePeriodPATTERN = "yyyy-mm";
        if(tufaPermitList != null && tufaPermitList.size() == 1 && tufaPermitList.get(0).getPermitId() == permitId ){
        	 return false;
        }
        //To get the fiscal year from assessment year/qtr
        
        //incoming is full/partial exclude : Check to see if there is another permit for the company
       if(incomingExclusionType == 1 || incomingExclusionType == 2) {
        	for(PermitEntity permit: tufaPermitList){
        		if(permit.getPermitId() != permitId){
        			Date activeDate= permit.getIssueDt();//this is active date that is displayed in the UI
        			Date closeDate = permit.getCloseDt();//this is inactive date that is displayed in the UI
        			String closeFiscalYrQtr = null;
        			String[] closedateParts = null;
        			long closeYr = 0;
        			long closeQtr= 0;
        			String activeFiscalYrQtr = null;
        			String[] activedateParts = null;
        			long activeYr = 0;
        			long activeQtr= 0;
        			if(null != activeDate){
        			 activeFiscalYrQtr = CTPUtil.getFiscalYrQtr(activeDate);
        			 activedateParts = activeFiscalYrQtr.split("-");
    				 activeYr = Long.parseLong(activedateParts[0]);
    				 activeQtr = Long.parseLong(activedateParts[1]);
        			}
        			
        			if( null != closeDate ){
        				closeFiscalYrQtr = CTPUtil.getFiscalYrQtr(closeDate);
        				closedateParts = closeFiscalYrQtr.split("-");
        				closeYr = Long.parseLong(closedateParts[0]);
        				closeQtr = Long.parseLong(closedateParts[1]);
        			}
        			for(String selQtr:selecQtrs){
        				long exQtr = Long.parseLong(selQtr);
        				long exFiscalYr = fiscalYr;
        				if(exQtr == 1){
        					exFiscalYr = fiscalYr - 1 ;
        					exQtr = 4;
        				}else{
        					exQtr = exQtr - 1;
        				}
        				// For Full exclusion : the excluded period should be with in the closed date; Partial exclusion : excluded period should be with in the active and closed date
        				if(((incomingExclusionType == 1) && 
        						(null == closeDate || ((exFiscalYr < closeYr) || ( exFiscalYr == closeYr && exQtr <= closeQtr) ) )) 
        						|| ((incomingExclusionType == 2) && 
        								((null != closeDate && null != activeDate && ((activeYr < exFiscalYr) ||  (activeYr == exFiscalYr && activeQtr <= exQtr) ) && ((exFiscalYr < closeYr) || ( exFiscalYr == closeYr && exQtr <= closeQtr) ) ) 
        										|| (null == closeDate && null != activeDate && ((activeYr < exFiscalYr) ||  (activeYr == exFiscalYr && activeQtr <= exQtr) )) ))){
        					return true;
        				}
        			}
        		}
        		
        	}
        }else {
            //warning message for include
            List<ExcludePermitBean> dupList = new ArrayList<ExcludePermitBean> (excludedList);
            for(ExcludePermitBean excludedPermitBean : excludedList) {
                if(excludedPermitBean.getPermitId() == permitId) {
                    dupList.remove(excludedPermitBean);
                }
            }
            return CollectionUtils.isNotEmpty(dupList) && dupList.size()>0;

        }
        /*if(incomingExclusionType == 1) {
            //incoming is full exclude and already have full/partial exclude
            //just check if there is any permit on Tu_permit which is active and not present in permit_excl_status_vw
            for(BigDecimal permit : tuPermitList) {
                if(permitId != permit.longValue() && !excludedPermit.contains(permit.longValue())) {
                     return true;
                }
             }
            } else if(incomingExclusionType == 2) {
                //partial exclusion
                //if already have partial and incoming in full
                //case incoming as partial and have already full exclude
                  for(ExcludePermitBean excludedPermitBean : excludedList) {
                    if(excludedPermitBean.getExcludeScopeId() == 1) {
                       if(fiscalYear.equals(excludedPermitBean.getAssessmentYear()) && Long.parseLong(quarter) > Long.parseLong(excludedPermitBean.getQuarter())) {
                            return true;
                      }
                    } else if(excludedPermitBean.getExcludeScopeId() == 2) {
                      //case incoming as partial and have already partial exclude
                        if(fiscalYear.equals(excludedPermitBean.getAssessmentYear()) && Long.parseLong(quarter) != Long.parseLong(excludedPermitBean.getQuarter())) {
                            return true;
                      } 
                    }
                }
                  for(BigDecimal permit : tuPermitList) {
                      if(permitId != permit.longValue() && !excludedPermit.contains(permit.longValue())) {
                           return true;
                      }
                   }
                  
            } else {
                //warning message for include
                List<ExcludePermitBean> dupList = excludedList;
                for(ExcludePermitBean excludedPermitBean : excludedList) {
                    if(excludedPermitBean.getPermitId() == permitId) {
                        dupList.remove(excludedPermitBean);
                        return CollectionUtils.isNotEmpty(dupList) && dupList.size()>0;
                    }
                }
            }*/
            return false;
    }
    
    private List<BigDecimal> getPermitList(long companyId) {
       // String statusCode = "ACTV";
        Query permitQuery = getCurrentSession().createSQLQuery("select permit.permit_id from tu_permit permit where permit.company_id= :companyId");
        permitQuery.setParameter("companyId", companyId);
        //permitQuery.setParameter("statusCode", statusCode);
        List<BigDecimal> permitList = permitQuery.list();
        return permitList;
    }
    private List<PermitEntity> getAllPermits(long companyId){
    	String hql = " FROM PermitEntity p WHERE companyEntity.companyId = :companyId ";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("companyId", companyId);
		List<PermitEntity> results = query.list();
		return results;
    }
    
    private List<ExcludePermitBean> getExistingExcludedPermit(long companyId) {
        Query query = getCurrentSession().createSQLQuery("select vw.ASSMNT_QTR,vw.ASSMNT_YR,vw.EXCL_SCOPE_ID,vw.PERMIT_ID "
                + "from permit_excl_status_vw vw where vw.company_id= :companyId and vw.EXCL_SCOPE_ID in(1,2)")
                .addScalar("ASSMNT_QTR", new StringType())
                .addScalar("ASSMNT_YR", new StringType())
                .addScalar("EXCL_SCOPE_ID", new LongType())
                .addScalar("PERMIT_ID", new LongType());
        query.setParameter("companyId", companyId);
        List<Object[]> list = query.list();
        List<ExcludePermitBean> excludeList = new ArrayList<ExcludePermitBean>();
        for(Object[] excludeDetailsList :list) {
            ExcludePermitBean bean = new ExcludePermitBean();
            bean.setQuarter(excludeDetailsList[0].toString());
            bean.setAssessmentYear(excludeDetailsList[1].toString());
            bean.setExcludeScopeId((Long)(excludeDetailsList[2]));
            bean.setPermitId(Long.parseLong(excludeDetailsList[3].toString()));
            excludeList.add(bean);
        }
        return excludeList;
    }
    
    @Override
    public PermitEntity getPermitbyId(long companyId,long permitId) {
        String hql = " From CompanyEntity c join c.permits p where c.companyId = :companyId  "
                + " and p.permitId= :permitId";
        Query query = this.getCurrentSession().createQuery(hql);
        query.setParameter("companyId", companyId);
        query.setParameter("permitId", permitId);
        PermitEntity permit = null;
        List<Object[]> results = query.list();
        Object[] result = (results!=null && !results.isEmpty())? results.get(0):null;
        if(result != null) {
            permit = (PermitEntity)result[1];
        }
        return permit;
    }
    @Override
    public String getAssociatedCompanyByEIN(long fiscalYear,long fiscalQtr, String tobaccoClassNm, String ein) {
    	return this.sqlManager.getAssociatedCompanyEIN(fiscalYear, fiscalQtr, tobaccoClassNm, ein);
    }
    
}

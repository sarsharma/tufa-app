/**
 * 
 */
package gov.hhs.fda.ctp.persistence;

import java.util.List;

import gov.hhs.fda.ctp.persistence.model.ContactEntity;
import gov.hhs.fda.ctp.persistence.model.ReportContactEntity;

/**
 * The Interface ContactMgmtEntityManager.
 *
 * @author akari
 */
public interface ContactMgmtEntityManager {
	
	/**
	 * Creates the.
	 *
	 * @param contact the contact
	 * @return contact
	 */
	public ContactEntity create(ContactEntity contact);

	/**
	 * Read by id.
	 *
	 * @param id the id
	 * @return contact
	 */
	public ContactEntity readById(long id);

	/**
	 * Update.
	 *
	 * @param contact the contact
	 * @return contact
	 */
	public ContactEntity update(ContactEntity contact);

	/**
	 * Delete by id.
	 *
	 * @param id the id
	 */
	public void deleteById(long id);
	
	
	/**
	 * Return contacts for monthly report.
	 * 
	 * @param companyId
	 * @param periodId
	 * @return
	 */
	public List<ReportContactEntity> getReportContacts(int companyId, int permitId, int periodId);

	public void saveStatus(String ein);

	public List<ContactEntity> getPrimaryContactsHistoryByCompany(Long companyId);

	public ContactEntity getPrimaryContactForCompany(Long companyId);

	public void delete(ContactEntity transformContactToEntity);

	public boolean companyHasPrimaryContact(Long companyId);
}

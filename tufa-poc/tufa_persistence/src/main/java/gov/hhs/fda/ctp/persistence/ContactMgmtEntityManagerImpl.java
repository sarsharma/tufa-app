/**
 * 
 */
package gov.hhs.fda.ctp.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.persistence.model.ContactEntity;
import gov.hhs.fda.ctp.persistence.model.ReportContactEntity;

/**
 * The Class ContactMgmtEntityManagerImpl.
 *
 * @author akari
 */
@Repository("contactMgmtEntityManager")
public class ContactMgmtEntityManagerImpl extends BaseEntityManagerImpl implements ContactMgmtEntityManager {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(ContactMgmtEntityManagerImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.ContactMgmtEntityManager#create(gov.hhs.fda.
	 * ctp.persistence.model.ContactEntity)
	 */
	@Override
	public ContactEntity create(ContactEntity contact) {
		logger.debug(" - Entering Create - ");
		// hash the phone number and email address to make it easier to see if anything has changed.
		int hashTotal = Objects.hash(contact.getPhoneNum(), contact.getEmailAddress());
		contact.setHashTotal(hashTotal);
		getCurrentSession().save(contact);
		logger.debug(" - Exiting Create - ");
		return contact;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gov.hhs.fda.ctp.persistence.ContactMgmtEntityManager#readById(long)
	 */
	@Override
	public ContactEntity readById(long id) {
		logger.debug(" - Entering readById - ");
		String hql = "FROM ContactEntity contactEntity where contactEntity.contactId = :id";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("id", id);
		@SuppressWarnings("rawtypes")
		List results = query.list();
		logger.debug(" - Exiting readById - ");
		return (ContactEntity) ((results != null && !results.isEmpty()) ? results.get(0) : null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.ContactMgmtEntityManager#update(gov.hhs.fda.
	 * ctp.persistence.model.ContactEntity)
	 */
	@Override
	public ContactEntity update(ContactEntity contact) {
		logger.debug(" - Entering update - ");
		// hash the phone number and email address to make it easier to see if anything has changed.
		int hashTotal = Objects.hash(contact.getPhoneNum(), contact.getEmailAddress());
		contact.setHashTotal(hashTotal);
		getCurrentSession().update(contact);
		logger.debug(" - Exiting  update - ");
		return contact;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gov.hhs.fda.ctp.persistence.ContactMgmtEntityManager#deleteById(long)
	 */
	@Override
	public void deleteById(long id) {
		logger.debug(" - Entering deleteById - ");
		ContactEntity contactEntity = new ContactEntity();
		contactEntity.setContactId(id);
		getCurrentSession().delete(contactEntity);
		logger.debug(" - Exiting deleteById - ");
	}

	/**
	 * Get all  report contacts for a given company, permit, and period.
	 */
	@Override
	public List<ReportContactEntity> getReportContacts(int companyId, int permitId, int periodId) {
		List<ReportContactEntity> contacts = null;
		Query query = getCurrentSession().createQuery(
				"FROM ReportContactEntity where COMPANY_ID = :companyId and PERMIT_ID = :permitId and PERIOD_ID = :periodId ORDER BY PRIMARY_CONTACT_SEQUENCE ASC, CREATED_DT ASC");
		query.setParameter("companyId", companyId);
		query.setParameter("permitId", permitId);
		query.setParameter("periodId", periodId);
		contacts = query.list();
		if(contacts == null) {
			contacts = new ArrayList<ReportContactEntity>();
		}
		for(ReportContactEntity contact : contacts) {
			getCurrentSession().setReadOnly(contact, true);
		}
		return contacts;
	}

	
	@Override
	public void saveStatus(String ein) {

		String sql = "UPDATE tu_permit_updates_stage set CONTACT_STATUS= :contact_status where EIN= :ein";
		Query query = getCurrentSession().createSQLQuery(sql.toString());
		query.setParameter("contact_status", "ADDED");
		query.setParameter("ein",ein);
		query.executeUpdate();

	}

	@Override
	public List<ContactEntity> getPrimaryContactsHistoryByCompany(Long companyId) {
		List<ContactEntity> contacts = new ArrayList<ContactEntity>();
		Query query = getCurrentSession().createQuery(
				"FROM ContactEntity where COMPANY_ID = :companyId AND PRIMARY_CONTACT_SEQUENCE IS NOT NULL ORDER BY PRIMARY_CONTACT_SEQUENCE ASC");
		query.setParameter("companyId", companyId);
		contacts = query.list();
		return contacts;
	}

	@Override
	//Invisable
	public ContactEntity getPrimaryContactForCompany(Long companyId) {
		ContactEntity primaryContact = null;
		Query query = getCurrentSession().createQuery(
				"FROM ContactEntity where COMPANY_ID = :companyId AND PRIMARY_CONTACT_SEQUENCE = 1");
		query.setParameter("companyId", companyId);
		primaryContact = (ContactEntity) query.uniqueResult();
		return primaryContact;
	}

	@Override
	public void delete(ContactEntity contact) {
		if(contact != null){
			this.getCurrentSession().delete(contact);
		}
	}
	
	@Override
	public boolean companyHasPrimaryContact(Long companyId) {
		Query query = getCurrentSession().createQuery(
				"FROM ContactEntity where COMPANY_ID = :companyId AND PRIMARY_CONTACT_SEQUENCE = 1");
		query.setParameter("companyId", companyId);
		return query.uniqueResult() != null;
	}
	
}

package gov.hhs.fda.ctp.persistence;

import gov.hhs.fda.ctp.persistence.model.ManufacturerEntity;

/**
 * The Interface ManufacturerEntityManager.
 */
public interface ManufacturerEntityManager {
	
	/**
	 * Register manufacturer.
	 *
	 * @param m the m
	 */
	public void registerManufacturer(ManufacturerEntity m);
}

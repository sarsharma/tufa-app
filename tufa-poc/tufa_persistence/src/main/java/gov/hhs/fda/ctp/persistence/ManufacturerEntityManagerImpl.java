package gov.hhs.fda.ctp.persistence;

import gov.hhs.fda.ctp.persistence.model.ManufacturerEntity;

/**
 * The Class ManufacturerEntityManagerImpl.
 */
public class ManufacturerEntityManagerImpl extends BaseEntityManagerImpl implements ManufacturerEntityManager{

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.ManufacturerEntityManager#registerManufacturer(gov.hhs.fda.ctp.persistence.model.ManufacturerEntity)
	 */
	@Override
	public void registerManufacturer(ManufacturerEntity m) {
		this.saveEntity(m);
	}
	
}

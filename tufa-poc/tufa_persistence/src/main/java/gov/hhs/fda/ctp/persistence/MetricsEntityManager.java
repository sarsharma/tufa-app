package gov.hhs.fda.ctp.persistence;

import gov.hhs.fda.ctp.common.beans.CBPMetrics;
import gov.hhs.fda.ctp.persistence.model.CBPMetricsEntity;

public interface MetricsEntityManager {
	
	public CBPMetricsEntity generateMetricData(long fiscalYr);
	
	public CBPMetricsEntity getGeneratedMetricData(long fiscalYr);
	
}

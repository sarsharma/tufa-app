package gov.hhs.fda.ctp.persistence;

import javax.persistence.ParameterMode;

import org.hibernate.query.Query;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.persistence.model.CBPMetricsEntity;

@Repository("metricsEntityManager")
public class MetricsEntityManagerImpl extends BaseEntityManagerImpl implements MetricsEntityManager {

	@Override
	public CBPMetricsEntity generateMetricData(long fiscalYr) {
		ProcedureCall procCall = this.getStoredProcedureByName("LOAD_CBP_METRICS");
		procCall.registerParameter("FISCAL_YR", Long.class,  ParameterMode.IN).bindValue(fiscalYr);
		procCall.getOutputs();
		
		CBPMetricsEntity entity = this.getGeneratedMetricData(fiscalYr);
		
		return entity;
	}

	@Override
	public CBPMetricsEntity getGeneratedMetricData(long fiscalYr) {
		String hql = "from CBPMetricsEntity m where m.fiscalYr =:fiscalYr";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setInteger("fiscalYr", (int) fiscalYr);
		CBPMetricsEntity entity = (CBPMetricsEntity) query.uniqueResult();
		return entity;
	}

}

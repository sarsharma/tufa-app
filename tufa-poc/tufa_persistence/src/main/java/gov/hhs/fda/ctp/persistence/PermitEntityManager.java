package gov.hhs.fda.ctp.persistence;

import java.util.List;
import java.util.Map;

import gov.hhs.fda.ctp.common.beans.PermitAudit;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.persistence.model.PermitAuditEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;
import gov.hhs.fda.ctp.persistence.model.PermitHistComments;
import gov.hhs.fda.ctp.persistence.model.PermitIgnoreEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateEntity;
import gov.hhs.fda.ctp.persistence.model.TTBFileColumnTemplateEntity;

public interface PermitEntityManager {
	
	/**
	 * Creates the permit by calling the BaseEntityManagerImpl saveEntity 
	 * method which in turn calls the Hibernate Session Object to save. 
	 */
	public PermitEntity createPermit(PermitEntity permitEntity);

	/**
	 * Gets the permit using the permitId by using the Hibernate Session Object. 
	 */
	public PermitEntity getPermit(long permitId);

	/**
	 * Updates the permit by calling the BaseEntityManagerImpl updateEntity 
	 * method which in turn calls the Hibernate Session Object to save. 
	 */
	public PermitEntity updatePermit(PermitEntity permitEntity);
	
	/**
	 * Use the criteria to query for results on permit history.
	 */
	public PermitHistoryCriteria getPermitHistories(PermitHistoryCriteria criteria);
	
	/**
	 * Permit save or update.
	 * @param permit
	 * @return
	 */
	public PermitEntity saveOrUpdatePermitContacts(PermitEntity entity);
	
	/**
	 * @param companyId
	 * @param permitId
	 * @param periodId
	 * @return
	 */
	public boolean deleteMonthlyReport(long companyId, long permitId, long periodId);
	
	
	public List<PermitUpdateEntity> stagePermitUpdatesList(PermitUpdateDocumentEntity document);
	
	public List<PermitAuditEntity> getPermitMgmtAuditList();

	public PermitUpdateDocumentEntity getPermitMgmtDocument(Long docId);

	public void updatePermitMgmtDocumentFileName(Long docId, String fileName);

	public List<PermitAuditEntity> getPermitMgmtAuditListByDocId(Long docId);

	public PermitAuditEntity insertPermitMgmtAudit(PermitAuditEntity pAudit);

	public List<TTBFileColumnTemplateEntity> loadTTBFileTemplates();

	public PermitIgnoreEntity insertIgnoreStatus(PermitIgnoreEntity updatedPermit);

	public Map<String, Map<String, List<String>>> loadTTBFileColAliases();
	
	/**
	 * @return
	 */
	public List<String> getallPermits();
	
	/**
	 * Persistance layer call to save/update comments for particular Permit
	 * @param permitHistComments
	 * @param permitId TODO
	 * @return
	 */
	public PermitHistComments saveUpdatePermitHistoryComments(PermitHistComments permitHistComments, long permitId);
	
	public List<PermitHistComments> getAllCommentsForPermit(long permitId);
	
	public PermitHistComments updatePermitHistoeyComment(String updatedComment, long permitCommentId);
	
	public PermitHistComments getPermitHistoryComment(long permitCommentId);

	public List<PermitAudit> getTTBPermitListExclEntries(Long docId);

	public PermitAuditEntity resolveTTBPermitExclusion(PermitAuditEntity permitAuditEntity);
	
}
package gov.hhs.fda.ctp.persistence;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.hibernate.Criteria;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.PermitAudit;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.FormDetailEntity;
import gov.hhs.fda.ctp.persistence.model.MissingForms;
import gov.hhs.fda.ctp.persistence.model.PeriodStatusEntity;
import gov.hhs.fda.ctp.persistence.model.PermitAuditEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;
import gov.hhs.fda.ctp.persistence.model.PermitHistComments;
import gov.hhs.fda.ctp.persistence.model.PermitIgnoreEntity;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateEntity;
import gov.hhs.fda.ctp.persistence.model.SubmittedFormEntity;
import gov.hhs.fda.ctp.persistence.model.TTBFileColumnTemplateEntity;

/**
 * The Class PermitEntityManagerImpl.
 */
@Repository("permitEntityManager")
public class PermitEntityManagerImpl extends BaseEntityManagerImpl implements PermitEntityManager {

	Logger logger = LoggerFactory.getLogger(PermitEntityManagerImpl.class);

	@Autowired
	private MapperWrapper mapperWrapper;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * gov.hhs.fda.ctp.persistence.PermitEntityManager#createPermit(gov.hhs.fda.
	 * ctp.persistence.model.PermitEntity)
	 */
	@Override
	public PermitEntity createPermit(PermitEntity permitEntity) {
		this.saveEntity(permitEntity);
		return permitEntity;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see gov.hhs.fda.ctp.persistence.PermitEntityManager#getPermit(long)
	 */
	@Override
	public PermitEntity getPermit(long permitId) {
		return this.getCurrentSession().get(PermitEntity.class, permitId);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * gov.hhs.fda.ctp.persistence.PermitEntityManager#updatePermit(gov.hhs.fda.
	 * ctp.persistence.model.PermitEntity)
	 */
	@Override
	public PermitEntity updatePermit(PermitEntity permitEntity) {
		this.updateEntity(permitEntity);
		return permitEntity;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * gov.hhs.fda.ctp.persistence.PermitEntityManager#getPermitHistories(gov.
	 * hhs.fda.ctp.common.beans.PermitHistoryCriteria)
	 */
	@Override
	public PermitHistoryCriteria getPermitHistories(PermitHistoryCriteria criteria) {
		/*
		 * currently supporting only permit search: permit name, permit type,
		 * permit status, months.
		 */
		Session session = this.getCurrentSession();
		Criteria cr = session.createCriteria(PermitEntity.class);
		if (criteria.getPermitNameFilter() != null && !criteria.getPermitNameFilter().isEmpty()) {
			cr.add(Restrictions.like("PERMIT_NUM", criteria.getPermitNameFilter()));
		}
		if (criteria.getMonthFilters() != null && !criteria.getMonthFilters().isEmpty()) {
			// cr.add(Restrictions.in("MONTH", values))
		}
		return new PermitHistoryCriteria();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see gov.hhs.fda.ctp.persistence.PermitEntityManager#
	 * saveOrUpdatePermitContacts(gov.hhs.fda.ctp.persistence.model.
	 * PermitEntity)
	 */
	@Override
	public PermitEntity saveOrUpdatePermitContacts(PermitEntity permit) {
		return permit;
	}

	@Override
	public boolean deleteMonthlyReport(long companyId, long permitId, long periodId) {
		boolean delSuccess = false;

		/*
		 * 1. Get Permit period
		 *
		 */
		PermitPeriodEntity permitPeriodEntity = getPermitPeriod(permitId, periodId);
		if (permitPeriodEntity != null) {

			/*
			 * 2. Delete all the Address and contacts associated with permit
			 * period.
			 */

			deleteReportAddresses(companyId, permitId, periodId);
			deleteReportContacts(companyId, permitId, periodId);

			/*
			 * 3. Delete all the form comments associated with permit period.
			 */
			List<SubmittedFormEntity> forms = permitPeriodEntity.getSubmittedForms();
			if (forms != null) {
				for (SubmittedFormEntity form : forms) {
					long formId = form.getFormId();
					deleteFormCommentsById(formId);
					deleteFormAdjustmentById(formId);
					}
			}

			/*
			 * 4. Delete all the forms and form details associated with permit
			 * period.
			 */
			if (forms != null) {
				for (SubmittedFormEntity form : forms) {
					Set<FormDetailEntity> details = form.getFormDetails();
					for (FormDetailEntity detail : details) {
						getCurrentSession().delete(detail);
					}
					getCurrentSession().delete(form);
				}
			}

			/*
			 * 5. Delete permitStatus entity.
			 */
			List<PeriodStatusEntity> statusList = permitPeriodEntity.getPeriodStatusList();
			if (statusList != null) {
				for (PeriodStatusEntity status : statusList) {
					getCurrentSession().delete(status);
				}
			}

			/*
			 * 6. Delete all the attachment documents associated with permit
			 * period.
			 */
			List<AttachmentsEntity> attachments = permitPeriodEntity.getAttachments();
			if (attachments != null) {
				for (AttachmentsEntity attachment : attachments) {
					getCurrentSession().delete(attachment);
				}
			}

			/*
			 * 7. Delete all the missing forms data
			 */
			List<MissingForms> missingforms = permitPeriodEntity.getMissingforms();
			if (missingforms != null) {
				for (MissingForms f : missingforms) {
					getCurrentSession().delete(f);
				}
			}

			/*
			 * 8. Delete the permit period
			 */
			getCurrentSession().delete(permitPeriodEntity);
		}

		delSuccess = true;

		return delSuccess;
	}

	private int deleteReportContacts(long companyId, long permitId, long periodId) {
		String hql = "Delete ReportContactEntity rc WHERE rc.companyId = :companyId and rc.permitId = :permitId and rc.periodId = :periodId";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("companyId", companyId);
		query.setParameter("permitId", permitId);
		query.setParameter("periodId", periodId);
		int result = query.executeUpdate();
		return result;
	}

	private int deleteReportAddresses(long companyId, long permitId, long periodId) {
		String hql = "Delete ReportAddressEntity ra WHERE ra.companyId = :companyId and ra.permitId = :permitId and ra.periodId = :periodId";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("companyId", companyId);
		query.setParameter("permitId", permitId);
		query.setParameter("periodId", periodId);
		int result = query.executeUpdate();

		return result;
	}

	/**
	 * Delete form comments by form id.
	 */
	private void deleteFormCommentsById(long formId) {
		String hql = "delete FROM FormCommentEntity comment WHERE comment.formId = :formId";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("formId", formId);
		query.executeUpdate();
	}
	private void deleteFormAdjustmentById(long formId) {
		String hql = "delete FROM TTBAdjustmentDetailEntity ttbAdjustmentDetailEntity WHERE ttbAdjustmentDetailEntity.formId = :formId";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("formId", formId);
		query.executeUpdate();
	}

	private PermitPeriodEntity getPermitPeriod(long permitId, long periodId) {
		String hql = "FROM PermitPeriodEntity pp WHERE pp.permitId = :permitId and pp.periodId = :periodId";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("permitId", permitId);
		query.setParameter("periodId", periodId);
		List<?> results = query.list();
		PermitPeriodEntity permitPeriodEntity = (results != null && !results.isEmpty())
				? (PermitPeriodEntity) results.get(0) : null;
		return permitPeriodEntity;
	}

	@Override
	public List<PermitUpdateEntity> stagePermitUpdatesList(PermitUpdateDocumentEntity document) {

		String getDocSql = "FROM PermitUpdateDocumentEntity doc WHERE doc.fileNm= :fileName";
		Query queryDocSql = getCurrentSession().createQuery(getDocSql);
		queryDocSql.setString("fileName", document.getFileNm());

		logger.debug("\t TUFA: Deleting contents from TABLE TU_PERMIT_UPDATES_DOCUMENT AND TU_PERMIT_UPDATES_STAGE");

		getCurrentSession().save(document);

		getCurrentSession().flush();
		getCurrentSession().clear();

		// Run updates against permits in TUFA
		Session session = this.getSessionFactory().getCurrentSession();
		ProcedureCall procCall = session.createStoredProcedureCall("UPDATE_PERMIT_STATUSES");
		procCall.getOutputs();
		return document.getPermits();
	}

	@Override
	public List<PermitAuditEntity> getPermitMgmtAuditList() {
		String hql = "from PermitAuditEntity pa";
		Query query = this.getCurrentSession().createQuery(hql);
		List<PermitAuditEntity> permitupdates = null;
		permitupdates = query.list();
		return (permitupdates != null) ? permitupdates : new ArrayList<PermitAuditEntity>();
	}

	@Override
	public List<PermitAuditEntity> getPermitMgmtAuditListByDocId(Long docId) {
		String hql = "from PermitAuditEntity pa " + "	 WHERE pa.docId= :docId";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setBigDecimal("docId", new BigDecimal(docId));

		List<PermitAuditEntity> permitupdates = query.list();
		return (permitupdates != null) ? permitupdates : new ArrayList<PermitAuditEntity>();
	}

	@Override
	public PermitUpdateDocumentEntity getPermitMgmtDocument(Long docId) {

		String getDocSql = "FROM PermitUpdateDocumentEntity doc WHERE doc.docId= :docId";
		Query queryDocSql = getCurrentSession().createQuery(getDocSql);
		queryDocSql.setLong("docId", docId);
		List result = queryDocSql.list();
		return (result != null && result.size() > 0) ? ((PermitUpdateDocumentEntity) result.get(0)) : null;

	}

	@Override
	public void updatePermitMgmtDocumentFileName(Long docId, String fileName) {

		String getDocSql = "Update PermitUpdateDocumentEntity doc set doc.fileNm=:filenam  WHERE doc.docId=:docId";
		Query sql = getCurrentSession().createQuery(getDocSql);

		sql.setLong("docId", docId);
		sql.setString("filenam", fileName);

		sql.executeUpdate();

	}

	@Override
	public PermitAuditEntity insertPermitMgmtAudit(PermitAuditEntity pAudit) {

		// Get last permit exclusion actions ('Exclude Tab') and insert with new
		// audit entry also set the tufa permit/company from last audit entry
		if (pAudit.getPermitAuditId() != null) {
			PermitAuditEntity pAuditEntityDB = (PermitAuditEntity) this.getCurrentSession().get(PermitAuditEntity.class,
					pAudit.getPermitAuditId());

			pAudit.setActionId(pAuditEntityDB.getActionId());
			pAudit.setPermitExclResolved(pAuditEntityDB.getPermitExclResolved());
			pAudit.setTufaPermitClsDt(pAuditEntityDB.getTufaPermitClsDt());
			pAudit.setPermitTufa(pAuditEntityDB.getPermitTufa());
			pAudit.setCompanyTufa(pAuditEntityDB.getCompanyTufa());
		}
		// massage permit exclusion flag coming from front end
		if (pAudit.getPermitExclResolved() != null) {
			if (pAudit.getPermitExclResolved().equalsIgnoreCase("true"))
				pAudit.setPermitExclResolved("Y");
			else
				pAudit.setPermitExclResolved("N");
		}

		this.getCurrentSession().save(pAudit);
		this.getCurrentSession().flush();
		this.getCurrentSession().refresh(pAudit);

		return pAudit;
	}

	@Override
	// Needs to be transactional as getting called from a non Transactional
	// context (IngestionFileMgmtAspect)
	@Transactional
	public List<TTBFileColumnTemplateEntity> loadTTBFileTemplates() {
		String hql = "from TTBFileColumnTemplateEntity ttbTmp";
		Query sql = getCurrentSession().createQuery(hql);
		List<TTBFileColumnTemplateEntity> templates = sql.list();
		return templates;
	}

	@Override
	// Needs to be transactional as getting called from a non Transactional
	// context (IngestionFileMgmtAspect)
	@Transactional
	public Map<String, Map<String, List<String>>> loadTTBFileColAliases() {

		String sql = "select * from TU_TTB_FILE_COL_ALIASES aliases";
		Query query = getCurrentSession().createSQLQuery(sql);
		List<Object[]> aliases = query.list();

		Map<String, Map<String, List<String>>> aliasMap = new HashMap<>();
		for (Object[] alias : aliases) {

			List<String> aliasValues = null;
			Map<String, List<String>> primaryValueMap = null;

			// Column name and primary value should not be null
			if (alias[0] != null && alias[1] != null) {

				// insert column name as key in aliasMap if does not exist and
				// instantiate the value.
				if (!aliasMap.containsKey(alias[0])) {
					aliasValues = new ArrayList<>();
					primaryValueMap = new HashMap<>();
					// Check primary values is not null
					primaryValueMap.put((String) alias[1], aliasValues);
					aliasMap.put((String) alias[0], primaryValueMap);

				} else {
					// insert new primary value
					primaryValueMap = aliasMap.get(alias[0]);
					aliasValues = new ArrayList<>();
					primaryValueMap.put((String) alias[1], aliasValues);
				}
			}

			// insert alias values
			for (int i = 2; i < alias.length; i++)
				aliasValues.add((String) alias[i]);

		}

		return aliasMap;
	}

	@Override
	public PermitIgnoreEntity insertIgnoreStatus(PermitIgnoreEntity updatedPermit) {

		this.getCurrentSession().save(updatedPermit);
		this.getCurrentSession().flush();
		this.getCurrentSession().refresh(updatedPermit);

		return updatedPermit;

	}

	/**
	 * Getting all permits in TUFA system
	 */
	@Override
	public List<String> getallPermits() {
		Query query = getCurrentSession().createSQLQuery("select tupermit.permit_num from tu_permit tupermit");
		List<String> permitLists = null;
		permitLists = query.list();
		return permitLists;
	}

	/**
	 * Implemaintation of persistance layer
	 */
	@Override
	public PermitHistComments saveUpdatePermitHistoryComments(PermitHistComments permitHistComments, long permitId) {
		if (permitHistComments.getPermitCommentId() != 0) {
			permitHistComments.setModifiedBy(getUser());
		}
		if (!StringUtils.isNotEmpty(permitHistComments.getAuthor())) {
			permitHistComments.setAuthor(getUser());
		}
		this.getCurrentSession().saveOrUpdate(permitHistComments);
		this.getCurrentSession().flush();
		// this.getCurrentSession().refresh(permitHistComments);
		return permitHistComments;
	}

	/**
	 *
	 */
	@Override
	public List<PermitHistComments> getAllCommentsForPermit(long permitId) {
		String hql = "from PermitHistComments phc WHERE phc.permitId= :permitId";
		Query query = getCurrentSession().createQuery(hql);
		// Query query = getCurrentSession().createSQLQuery("select * from
		// TU_PERMITT_HIST_COMMENTS phs where phs.permit_id = :permitId");
		query.setLong("permitId", permitId);
		List<PermitHistComments> permitComments = null;
		permitComments = query.list();

		return (permitComments != null) ? permitComments : new ArrayList<PermitHistComments>();
	}

	/**
	 *
	 */
	@Override
	public PermitHistComments updatePermitHistoeyComment(String userComments, long permitCommentId) {
		String updatehql = "Update PermitHistComments phc set phc.userComments= :userComments where phc.permitCommentId= :permitCommentId";
		Query query = this.getCurrentSession().createQuery(updatehql);
		query.setLong("permitCommentId", permitCommentId);
		query.setString("userComments", userComments);
		query.executeUpdate();
		PermitHistComments comment = getPermitHistoryComment(permitCommentId);
		return comment;
	}

	/**
	 *
	 */
	@Override
	public PermitHistComments getPermitHistoryComment(long permitCommentId) {
		String gethql = "from PermitHistComments phc where phc.permitCommentId= :permitCommentId";
		Query query = this.getCurrentSession().createQuery(gethql);
		query.setLong("permitCommentId", permitCommentId);
		List<?> results = query.list();
		PermitHistComments permtHistoryComment = (results != null && !results.isEmpty())
				? (PermitHistComments) results.get(0) : null;
		return permtHistoryComment;
	}

	@Override
	public List<PermitAudit> getTTBPermitListExclEntries(Long docId) {

		String sql = "SELECT  AUDIT_ID as auditId, " + "         upper(COMPANY_NAME) as companyName, "
				+ "         COMPANY_ID as companyId, " + "         EIN as einNum, " + "         PERMIT_ID as permitId, "
				+ "         PERMIT_ACTION as permitAction, " + "         PERMIT_STATUS as permitStatus, "
				+ "         DOC_STAGE_ID as docStageId, " + "         ISSUE_DT as issueDt, "
				+ "         CLOSE_DT as closeDt, " + "         ACTION_MSG as actionMsg, "
				+ "         ACTION as action, " + "         PERMIT_EXCL_RESOLVED as permitExclResolved, "
				+ "         ACTION_CONTEXT as actionContext "
				+ "         FROM TTB_PERMIT_LIST_VW WHERE DOC_STAGE_ID=:docId ";

		Query query = this.getCurrentSession().createSQLQuery(sql).setResultSetMapping("ttb_permit_excl_dto_mapping");

		query.setBigDecimal("docId", new BigDecimal(docId));

		List<PermitAudit> permitAudits = query.list();
		return permitAudits;
	}

	@Override
	public PermitAuditEntity resolveTTBPermitExclusion(PermitAuditEntity permitAuditEntity) {

		PermitAuditEntity pAudit = (PermitAuditEntity) this.getCurrentSession().get(PermitAuditEntity.class,
				permitAuditEntity.getPermitAuditId());
		pAudit.setPermitExclResolved(permitAuditEntity.getPermitExclResolved());
		this.getCurrentSession().update(pAudit);
		return pAudit;
	}

	public MapperWrapper getMapperWrapper() {
		return mapperWrapper;
	}

	public void setMapperWrapper(MapperWrapper mapperWrapper) {
		this.mapperWrapper = mapperWrapper;
	}

}

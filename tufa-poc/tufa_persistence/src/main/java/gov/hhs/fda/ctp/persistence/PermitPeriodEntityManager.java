package gov.hhs.fda.ctp.persistence;

import java.util.List;
import java.util.Set;

import gov.hhs.fda.ctp.common.beans.TTBAdjustmentDetail;
import gov.hhs.fda.ctp.persistence.model.*;

/**
 * The Interface PermitPeriodEntityManager.
 */
public interface PermitPeriodEntityManager {
   
    /**
     * Update permit period.
     *
     * @param permitperiod the permitperiod
     * @param attachs the attachs
     * @return the permit period entity
     */
    public PermitPeriodEntity updatePermitPeriod(PermitPeriodEntity permitperiod, List<AttachmentsEntity> attachs);
    
    /**
     * Gets the permit period by ids.
     *
     * @param periodId the period id
     * @param permitId the permit id
     * @return the permit period by ids
     */
    public PermitPeriodEntity getPermitPeriodByIds(long periodId, long permitId);
    
    /**
     * Gets the permit period by PK.
     *
     * @param pk the pk
     * @return the permit period by PK
     */
    public PermitPeriodEntity getPermitPeriodByPK(ActivityPK pk);
    
    /**
     * Delete by ids.
     *
     * @param periodId the period id
     * @param permitId the permit id
     * @return the permit period entity
     */
    public PermitPeriodEntity deleteByIds(long periodId, long permitId);

    /**
     * Gets the rpt period by id.
     *
     * @param periodId the period id
     * @return the rpt period by id
     */
    public RptPeriodEntity getRptPeriodById(long periodId);
    
    /**
     * Save or update permit period.
     *
     * @param permitPeriodEntity the permit period entity
     */
    public void saveOrUpdatePermitPeriod(PermitPeriodEntity permitPeriodEntity);

    /**
     * Gets the form detail by PK.
     *
     * @param pk the pk
     * @return the form detail by PK
     */
    public FormDetailEntity getFormDetailByPK(FormDetailPK pk);
    
    /**
     * Delete form details.
     *
     * @param details the details
     */
    public void deleteFormDetails(Set<FormDetailEntity> details);
        
    /**
     * Delete form comments by form id.
     * @param formId
     */
    public void deleteFormCommentsById(long formId);
    
    /**
     * Delete form comments with no associated form in formdetails table.
     * @param formIds
     * @param permitId
     * @param periodId
     */
    public void deleteOrphanedFormComments(List<Long> formIds,long permitId, long periodId);

    
    /**
     * Creates the rpt period.
     *
     * @param monthYear the month year
     * @return the string
     */
    public String createRptPeriod(String monthYear);

    /**
     * Gets or creates an individual form.
     * @param permitId
     * @param periodId
     * @param formNum
     * @param formTypeCd
     * @return
     */
    public SubmittedFormEntity getOrCreateSubmittedForm(long permitId, long periodId, long formNum, String formTypeCd);
    
    /**
     * Create a form comment.
     * @param comment
     * @return
     */
    public FormCommentEntity createOrUpdateFormComment(FormCommentEntity comment);
    
    /**
     * Get all comments for a form.
     * @param formId
     * @return
     */
    public List<FormCommentEntity> getFormComments(long formId);
    
    /**
     * Recon the report.
     * @param periodId
     * @param permitId
     * @return
     */
    public PeriodStatusEntity updatePeriodReconStatus(long periodId,long permitId);
    
    /**
     * Get recon status.
     * @param periodId
     * @param permitId
     * @return
     */
    public PeriodStatusEntity getPeriodReconStatus(long periodId,long permitId);
    
	public List<MissingForms> getMissingForms(long permitId, long periodId);
	
	public void saveMissingForms(MissingForms missingForms);
	
	public void deleteMissingForms(MissingForms missingForms);


	public List<ZeroReportCommentEntity> getZeroReportComments(long permitId, long periodId);

	public ZeroReportCommentEntity createOrUpdateZeroReportComment(ZeroReportCommentEntity comment);

	List<Export3852FormEntity> getExport3852Form(long permitId, long periodId);

	List<Export2000FormEntity> getExport2000Form(long permitId, long periodId);

	List<Export7501FormHeaderEntity> getExport7501FormHeader(long permitId, long periodId, long tobaccoClassId);

	List<Export7501FormBodyEntity> getExport7501FormBody(long permitId, long periodId, long tobaccoClassId);

	List<Export5000FormEntity> getExport5000Form(long permitId, long periodId);
	
	 public List<TTBAdjustmentDetailEntity> getTTBAdjustmentDetail(long formId,long tobaccoClassId);
	 
	 public void deleteTTBAdjustmentById(long formId, long tobaccoClassId);
	 
	 public void saveTTBAdjustments(List<TTBAdjustmentDetailEntity> ttbAdjustmentDetailsList);


}

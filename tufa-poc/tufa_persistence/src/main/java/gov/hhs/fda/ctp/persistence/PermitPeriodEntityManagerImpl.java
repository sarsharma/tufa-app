package gov.hhs.fda.ctp.persistence;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.ParameterMode;

import org.hibernate.query.Query;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.procedure.ProcedureOutputs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.common.beans.TTBAdjustmentDetail;
import gov.hhs.fda.ctp.persistence.model.ActivityPK;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.Export2000FormEntity;
import gov.hhs.fda.ctp.persistence.model.Export3852FormEntity;
import gov.hhs.fda.ctp.persistence.model.Export5000FormEntity;
import gov.hhs.fda.ctp.persistence.model.Export7501FormBodyEntity;
import gov.hhs.fda.ctp.persistence.model.Export7501FormHeaderEntity;
import gov.hhs.fda.ctp.persistence.model.FormCommentEntity;
import gov.hhs.fda.ctp.persistence.model.FormDetailEntity;
import gov.hhs.fda.ctp.persistence.model.FormDetailPK;
import gov.hhs.fda.ctp.persistence.model.MissingForms;
import gov.hhs.fda.ctp.persistence.model.PeriodStatusEntity;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;
import gov.hhs.fda.ctp.persistence.model.RptPeriodEntity;
import gov.hhs.fda.ctp.persistence.model.SubmittedFormEntity;
import gov.hhs.fda.ctp.persistence.model.TTBAdjustmentDetailEntity;
import gov.hhs.fda.ctp.persistence.model.ZeroReportCommentEntity;

/**
 * The Class PermitPeriodEntityManagerImpl.
 */
@Repository("permitPeriodEntityManager")
public class PermitPeriodEntityManagerImpl extends BaseEntityManagerImpl implements PermitPeriodEntityManager{

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(CompanyMgmtEntityManagerImpl.class);

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager#updatePermitPeriod(gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity, java.util.List)
	 */
	@Override
	public PermitPeriodEntity updatePermitPeriod(PermitPeriodEntity permitperiod, List<AttachmentsEntity> attachs) {
		 getCurrentSession().update(permitperiod);
	        if(attachs != null && !attachs.isEmpty()) {
	            for(AttachmentsEntity attach:attachs) {
	                getCurrentSession().saveOrUpdate(attach);
	            }
	        }
	        return permitperiod;
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager#getPermitPeriodByIds(long, long)
	 */
	@Override
	public PermitPeriodEntity getPermitPeriodByIds(long periodId, long permitId) {
		String hql = "FROM PermitPeriodEntity permitPeriodEntity where permitPeriodEntity.periodId = :id "
				+ "AND permitPeriodEntity.permitId = :permitid";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("id", periodId);
        query.setParameter("permitid", permitId);
        List results = query.list();
        return (PermitPeriodEntity)((results!=null && !results.isEmpty())?results.get(0):null);
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager#getPermitPeriodByPK(gov.hhs.fda.ctp.persistence.model.ActivityPK)
	 */
	@Override
	public PermitPeriodEntity getPermitPeriodByPK(ActivityPK pk) {
	    return getCurrentSession().get(PermitPeriodEntity.class,pk);
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager#getRptPeriodById(long)
	 */
	@Override
	public RptPeriodEntity getRptPeriodById(long periodId) {
	    return this.getCurrentSession().get(RptPeriodEntity.class, periodId);
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager#deleteByIds(long, long)
	 */
	@Override
	public PermitPeriodEntity deleteByIds(long periodId, long permitId) {
		PermitPeriodEntity permitPeriod = this.getPermitPeriodByIds(periodId, permitId);
		deleteAttachments(periodId, permitId);
		getCurrentSession().delete(permitPeriod);
		return permitPeriod;
	}

    /**
     * save or update the permit period with attached submitted forms.
     * loop through the form details manually because of the composite key.
     *
     * @param permitPeriodEntity the permit period entity
     */
    @Override
    public void saveOrUpdatePermitPeriod(PermitPeriodEntity permitPeriodEntity) {
        getCurrentSession().saveOrUpdate(permitPeriodEntity);
        getCurrentSession().flush();
        List<SubmittedFormEntity> forms = permitPeriodEntity.getSubmittedForms();
        if(forms == null)
        	return;
        for(SubmittedFormEntity form: forms) {
            Set<FormDetailEntity> details = form.getFormDetails();
            if(details == null)
            	continue;
            for(FormDetailEntity detail: details) {
                if(detail.getFormId() == null) {
                    detail.setFormId(form.getFormId());
                }
                getCurrentSession().saveOrUpdate(detail);
            }
        }
        getCurrentSession().flush();
        
    }

    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager#getFormDetailByPK(gov.hhs.fda.ctp.persistence.model.FormDetailPK)
     */
    @Override
    public FormDetailEntity getFormDetailByPK(FormDetailPK pk) {
        return getCurrentSession().get(FormDetailEntity.class,pk);
    }

    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager#deleteFormDetails(java.util.Set)
     */
    @Override
    public void deleteFormDetails(Set<FormDetailEntity> details) {
        for(FormDetailEntity detail: details) {
        	//if the TTB form has been removed , remove its adjustment details for all tobaccotype
        	deleteTTBAdjustmentById(detail.getFormId(),0);
            getCurrentSession().delete(detail);
        }        
    }

    /**
     * Delete form comments by form id.
     */
    @Override
	public void deleteFormCommentsById(long formId) {
		String hql = "delete FROM FormCommentEntity comment WHERE comment.formId = :formId";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("formId", formId);
		query.executeUpdate();
    }
    
    /**
     * Delete form comments with no associated form in formdetails table.
     * @param formId
     * @param permitId
     * @param periodId
     */
    @Override
    public void deleteOrphanedFormComments(List<Long> formIds,long permitId, long periodId){
    	
    	String hql = "delete FROM FormCommentEntity comment WHERE comment.formId in (:formIds)";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameterList("formIds", formIds);
		query.executeUpdate();
		
		String formDeleteHQL = "delete FROM SubmittedFormEntity form WHERE " +
					 "form.permitId = :permitId AND " +
				     "form.periodId = :periodId AND " +
					 "form.formId in (:fmIds) ";
		
		 Query formDelete= getCurrentSession().createQuery(formDeleteHQL);
		 formDelete.setParameter("permitId", permitId);
		 formDelete.setParameter("periodId", periodId);
		 formDelete.setParameterList("fmIds", formIds);
		 formDelete.executeUpdate();
		
    }

    @Override
    public SubmittedFormEntity getOrCreateSubmittedForm(long permitId, long periodId, long formNum, String formTypeCd) {
		   		
    	String hql = "FROM SubmittedFormEntity form WHERE " +
					 "form.permitId = :permitId AND " +
				     "form.periodId = :periodId AND " +
					 "form.formNum = :formNum AND " +
				     "form.formTypeCd = :formTypeCd";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("permitId", permitId);
		query.setParameter("periodId", periodId);
		query.setParameter("formNum", formNum);
		query.setParameter("formTypeCd", formTypeCd);
		List result = query.list();
		SubmittedFormEntity formEntity = (result!=null && result.size()>0)?(SubmittedFormEntity)result.get(0):null;
		if(formEntity == null) {
			formEntity = new SubmittedFormEntity();
			formEntity.setPermitId(permitId);
			formEntity.setPeriodId(periodId);
			formEntity.setFormNum(formNum);
			formEntity.setFormTypeCd(formTypeCd);
			getCurrentSession().save(formEntity);
			getCurrentSession().flush();
		}
		return formEntity;
    }

 /**
     * Create a form comment.
     * @param comment
     * @return
     */
    @Override
    public ZeroReportCommentEntity createOrUpdateZeroReportComment(ZeroReportCommentEntity comment)
    {
    	
     	if(comment.getId() != null && comment.getId().compareTo(0L) == 0 || comment.getId() == null){
    		comment.setId(null);
         	comment.setAuthor(getUser());
     	}else
     		comment.setModifiedBy(getUser());
     	
     	this.getCurrentSession().saveOrUpdate(comment);
    	getCurrentSession().flush();
    	return comment;
    
    }

    
    /**
     * Get all comments for a form.
     * @param formId
     * @return
     */
    @Override
    public List<ZeroReportCommentEntity> getZeroReportComments(long permitId, long periodId ) {			
    	String hql = "FROM ZeroReportCommentEntity comments WHERE " +
				 "comments.permitId = :permitId AND " +
				 "comments.periodId = :periodId " +
				 " ORDER BY comments.permitId";
				 Query query = getCurrentSession().createQuery(hql);
				 query.setParameter("permitId", permitId);
			     query.setParameter("periodId", periodId);;
		return query.list();
    }
    
    
    /**
     * Create a form comment.
     * @param comment
     * @return
     */
    @Override
    public FormCommentEntity createOrUpdateFormComment(FormCommentEntity comment) {
    	
    	if(comment.getCommentSeq() != null && comment.getCommentSeq().compareTo(0L) == 0 || comment.getCommentSeq() == null ){
    		comment.setCommentSeq(null);
        	comment.setAuthor(getUser());
    	}else
    		comment.setModifiedBy(getUser());
    	
    	
    	getCurrentSession().saveOrUpdate(comment);
    	return comment;
    }
    
    
    /**
     * Get all comments for a form.
     * @param formId
     * @return
     */
    @Override
    public List<FormCommentEntity> getFormComments(long formId) {
    	String hql = "FROM FormCommentEntity comments WHERE " +
				 "comments.formId = :formId " +
				 " ORDER BY comments.formId";
				 Query query = getCurrentSession().createQuery(hql);
		query.setParameter("formId", formId);
		return query.list();
    }
    
    @Override
    public List<TTBAdjustmentDetailEntity> getTTBAdjustmentDetail(long formId, long tobaccoClassId) {
    	StringBuilder hql = new StringBuilder("FROM TTBAdjustmentDetailEntity ttbAdjustmentDetailEntity WHERE ttbAdjustmentDetailEntity.formId = :formId ");
    	if(tobaccoClassId > 0) {
    		hql.append(" AND ttbAdjustmentDetailEntity.tobaccoClassId = :tobaccoClassId");
    	}
    	hql.append(" ORDER BY ttbAdjustmentDetailEntity.tobaccoClassId,ttbAdjustmentDetailEntity.adjFormId");
		Query query = getCurrentSession().createQuery(hql.toString());
		query.setParameter("formId", formId);
		if(tobaccoClassId > 0) {
			query.setParameter("tobaccoClassId", tobaccoClassId);
		}
		return query.list();
    }


	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.PermitPeriodEntityManager#createRptPeriod(java.lang.String)
	 */
	@Override
	public String createRptPeriod(String monthYear){

		ProcedureCall call = this.getCurrentSession().createStoredProcedureCall("OPEN_REPORTING_PERIOD");
		call.registerParameter("p_period_dt_i", String.class, ParameterMode.IN);
		call.getParameterRegistration("p_period_dt_i").bindValue(monthYear);
		call.registerParameter("p_ret_code_o", Integer.class, ParameterMode.OUT);
		ProcedureOutputs outputs = call.getOutputs();
		Integer result = (Integer) outputs.getOutputParameterValue( "p_ret_code_o" );

		return result.toString();
	}

	/**
	 * Delete attachments.
	 *
	 * @param periodId the period id
	 * @param permitId the permit id
	 */
	private void deleteAttachments(long periodId, long permitId){
		String hql = "FROM AttachmentsEntity attachments WHERE attachments.periodId = :periodid AND "
				+ "attachments.permitId = :permitid";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("periodid",periodId);
		query.setParameter("permitid",permitId);
		@SuppressWarnings("unchecked")
		List<AttachmentsEntity> attachments = query.list();
		for(AttachmentsEntity ae : attachments){
			getCurrentSession().delete(ae);
		}
	}

	@Override
	public PeriodStatusEntity updatePeriodReconStatus(long periodId,long permitId){

				String hql = "Update PeriodStatusEntity pStatEntity SET reconStatusTypeCd='APRV',reconStatusModifiedBy=:user, reconStatusModifiedDt=:mdt WHERE pStatEntity.periodId=:periodId AND pStatEntity.permitId=:permitId";
				Query query1 = getCurrentSession().createQuery(hql);
				query1.setParameter("user", getUser());
				query1.setParameter("mdt", new Date());
				query1.setParameter("periodId", periodId);
				query1.setParameter("permitId", permitId);
				query1.executeUpdate();

				String select = "From PeriodStatusEntity pStatEntity WHERE pStatEntity.periodId=:periodId AND pStatEntity.permitId=:permitId";
				Query query2 = getCurrentSession().createQuery(select);
				query2.setParameter("periodId", periodId);
				query2.setParameter("permitId", permitId);
				List result = query2.list();
				PeriodStatusEntity pStatEntity = (result!=null && result.size()>0)?(PeriodStatusEntity)result.get(0):null;
				return pStatEntity;

	}

	@Override
	public PeriodStatusEntity getPeriodReconStatus(long periodId,long permitId){

		String select = "From PeriodStatusEntity pStatEntity WHERE pStatEntity.periodId=:periodId AND pStatEntity.permitId=:permitId";
		Query query2 = getCurrentSession().createQuery(select);
		query2.setParameter("periodId", periodId);
		query2.setParameter("permitId", permitId);
		List result = query2.list();
		PeriodStatusEntity pStatEntity = (result!=null && result.size()>0)?(PeriodStatusEntity)result.get(0):null;
		return pStatEntity;

    }

	@Override
	public List<MissingForms> getMissingForms(long permitId, long periodId) {
		String hql = "FROM MissingForms forms WHERE forms.permitId = :permitId and forms.periodId = :periodId";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("permitId", permitId);
		query.setParameter("periodId", periodId);
		List<MissingForms> results = query.list();
		return results;
	}

	@Override
	public List<Export3852FormEntity> getExport3852Form(long permitId, long periodId) {
		String hql = "FROM Export3852FormEntity f3852 WHERE f3852.periodId=:periodId AND f3852.permitId=:permitId";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("permitId", permitId);
		query.setParameter("periodId", periodId);
		List<Export3852FormEntity> results = query.list();
		return results;
	}
	
	@Override
	public List<Export2000FormEntity> getExport2000Form(long permitId, long periodId) {
		String hql = "FROM Export2000FormEntity f2000 WHERE f2000.periodId=:periodId AND f2000.permitId=:permitId";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("permitId", permitId);
		query.setParameter("periodId", periodId);
		List<Export2000FormEntity> results = query.list();
		return results;
	}
	
	@Override
	public List<Export7501FormHeaderEntity> getExport7501FormHeader(long permitId, long periodId, long tobaccoClassId) {
		String hql = "FROM Export7501FormHeaderEntity f7501 WHERE f7501.periodId=:periodId AND f7501.permitId=:permitId AND f7501.formNum=:tobaccoClassId";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("permitId", permitId);
		query.setParameter("periodId", periodId);
		query.setParameter("tobaccoClassId", tobaccoClassId);
		List<Export7501FormHeaderEntity> results = query.list();
		return results;
	}
	
	@Override
	public List<Export7501FormBodyEntity> getExport7501FormBody(long permitId, long periodId, long tobaccoClassId) {
		String hql = "FROM Export7501FormBodyEntity f7501 WHERE f7501.periodId=:periodId AND f7501.permitId=:permitId AND f7501.formNum=:tobaccoClassId order by f7501.lineNum asc ";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("permitId", permitId);
		query.setParameter("periodId", periodId);
		query.setParameter("tobaccoClassId", tobaccoClassId);
		List<Export7501FormBodyEntity> results = query.list();
		return results;
	}
	
	@Override
	public List<Export5000FormEntity> getExport5000Form(long permitId, long periodId) {
		String hql = "FROM Export5000FormEntity f5000 WHERE f5000.periodId=:periodId AND f5000.permitId=:permitId";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("permitId", permitId);
		query.setParameter("periodId", periodId);
		List<Export5000FormEntity> results = query.list();
		return results;
	}

	@Override
	public void saveMissingForms(MissingForms missingForms) {
		this.getCurrentSession().save(missingForms);
	}

	@Override
	public void deleteMissingForms(MissingForms missingForms) {
		this.getCurrentSession().delete(missingForms);
	}
	
	/**
     * Delete TTBAdjustment by form id.
     */
    @Override
   	public void deleteTTBAdjustmentById(long formId, long tobaccoClassId) {
   		StringBuilder hql = new StringBuilder("delete FROM TTBAdjustmentDetailEntity ttbAdjustmentDetailEntity WHERE ttbAdjustmentDetailEntity.formId = :formId");
   		if(tobaccoClassId > 0) {
   			hql.append(" AND ttbAdjustmentDetailEntity.tobaccoClassId = :tobaccoClassId");
   		}
   		Query query = getCurrentSession().createQuery(hql.toString());
   		query.setParameter("formId", formId);
   		if(tobaccoClassId > 0) {
   			query.setParameter("tobaccoClassId", tobaccoClassId);
   		}
   		query.executeUpdate();
       }
    
    @Override
    public void saveTTBAdjustments(List<TTBAdjustmentDetailEntity> ttbAdjustmentDetailsList) {
    	getCurrentSession().flush();
        for(TTBAdjustmentDetailEntity ttbAdjustmentDetailEntity: ttbAdjustmentDetailsList) {
                getCurrentSession().saveOrUpdate(ttbAdjustmentDetailEntity);
            }
        getCurrentSession().flush();
        
    }
}

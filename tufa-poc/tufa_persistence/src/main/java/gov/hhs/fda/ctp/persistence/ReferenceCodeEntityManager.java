/**
 * 
 */
package gov.hhs.fda.ctp.persistence;

import java.util.List;
import java.util.Map;

import gov.hhs.fda.ctp.common.util.TobaccoClassFacade;
import gov.hhs.fda.ctp.persistence.model.CurrentTobaccoRateViewEntity;
import gov.hhs.fda.ctp.persistence.model.HTSCodeEntity;
import gov.hhs.fda.ctp.persistence.model.PeriodTaxRateViewEntity;
import gov.hhs.fda.ctp.persistence.model.TaxRateEntity;
import gov.hhs.fda.ctp.persistence.model.TobaccoClassEntity;

/**
 * The Interface ReferenceCodeEntityManager.
 *
 * @author tgunter
 */
public interface ReferenceCodeEntityManager {
    
    /**
     * Get a reference value based on type and code (i.e. "PERMIT_TYPE", "MANU" = "Manufacturer").
     *
     * @param type the type
     * @param code the code
     * @return String
     */
    public String getValue(String type, String code);

    /***
     * Get a reference code by type and value.
     * @param type
     * @param value
     * @return
     */
    public String getCode(String type, String value);

    
    /**
     * Get a map of all the values in a reference type (i.e. "PERMIT_TYPE" = {("MANU", "Manufacturer"), ("IMPT", "Importer")}).
     *
     * @param type the type
     * @return Map<String, String>
     */
    public Map<String, String> getValues(String type);
    
    /**
     * Get a map of all the keys in a reference type (i.e. "PERMIT_TYPE" = {("Manufacturer", "MANU), ("Importer", "IMPT")}).
     *
     * @param type the type
     * @return the keys
     */
    public Map<String, String> getKeys(String type);
    
    /**
     * Get a list of all the tobacco classes.
     * @return List<TobaccoClassEntity>
     */
    public List<PeriodTaxRateViewEntity> getTobaccoClasses(long periodId);
    
    /**
     * Get all tobacco classes.
     * @return
     */
    public List<TobaccoClassEntity> getAllTobaccoClasses();
    
    /**
     * Get a facade that wraps the list of all tobacco classes with simple get methods
     * @return
     */
    public TobaccoClassFacade getTobaccoClassFacade();

    /**
     * Get a map of all tobacco classes (key) mapped to their respective parent class (value).
     * @return Map<Long, Long>
     */ 
    public Map<Long, Long> getTobaccoParentMap(long periodId);

	List<CurrentTobaccoRateViewEntity> getLatestRates();

	void saveTaxRates(List<TaxRateEntity> rates);
	
	List<HTSCodeEntity> getHtsCodes();
	
	void saveOrEditCodes(HTSCodeEntity htsCode);
	
	HTSCodeEntity getCodeById(String htscode);
	
	/**
	 * Get a map that holds the tobacco class ids by name for a fiscal year.
	 * @param fiscalYr
	 * @return
	 */
	public Map<String, Long> getTobaccoClassNamesById();

	public Map<Long, String> getTobaccoClassIdNameMap();

    
}

/**
 * 
 */
package gov.hhs.fda.ctp.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.common.beans.TobaccoClass;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.util.TobaccoClassFacade;
import gov.hhs.fda.ctp.persistence.model.CurrentTobaccoRateViewEntity;
import gov.hhs.fda.ctp.persistence.model.HTSCodeEntity;
import gov.hhs.fda.ctp.persistence.model.PeriodTaxRateViewEntity;
import gov.hhs.fda.ctp.persistence.model.TaxRateEntity;
import gov.hhs.fda.ctp.persistence.model.TobaccoClassEntity;

/**
 * The Class ReferenceCodeEntityManagerImpl.
 *
 * @author tgunter
 */
@Repository("referenceCodeEntityManager")
public class ReferenceCodeEntityManagerImpl extends BaseEntityManagerImpl
        implements ReferenceCodeEntityManager {

	/** The mapper factory. */
    @Autowired
	private MapperWrapper mapperWrapper;

    /**
     * Get a reference value by type and code.  See interface for additional information.
     *
     * @param type the type
     * @param code the code
     * @return the value
     */
    @Override
    public String getValue(String type, String code) {        
        String hql = "select values.value from ReferenceType type " +
                     "inner join type.referenceValues values where type.name = :type and values.code = :code";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("type", type);
        query.setParameter("code", code);
        return (String)query.uniqueResult();
    }
    
    /**
     * Get a reference code by type and value.
     * @param type
     * @param value
     * @return
     */
    public String getCode(String type, String value) {
    	String hql = "select values.code from ReferenceType type " + 
    				 "inner join type.referenceValues values where type.name = :type and values.value = :value";
    	Query query = getCurrentSession().createQuery(hql);
        query.setParameter("type", type);
        query.setParameter("value", value);
        return (String)query.uniqueResult();
    }

    /**
     * Get a map of reference values for a reference type.  See interface for additional documentation.
     *
     * @param type the type
     * @return the values
     */
    @Override
    public Map<String, String> getValues(String type) {
        Map<String, String> returnVal = new HashMap<>();
        String hql = "select values.code, values.value from ReferenceType type " + 
                     "inner join type.referenceValues values where type.name = :type";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("type", type);
        @SuppressWarnings("unchecked")
		List<Object[]> results = query.list();
        for(Object[] result:results) {
            String abbreviation = (String)result[0];
            String name = (String)result[1];
            returnVal.put(abbreviation, name);
        }
        return returnVal;
    }

    /**
     * Get a map of reference keys for a reference type.  See interface for additional documentation.
     *
     * @param type the type
     * @return the keys
     */
    @Override
    public Map<String, String> getKeys(String type) {
        Map<String, String> returnVal = new HashMap<>();
        String hql = "select values.value, values.code from ReferenceType type " + 
                "inner join type.referenceValues values where type.name = :type";
       Query query = getCurrentSession().createQuery(hql);
       query.setParameter("type", type);
       @SuppressWarnings("unchecked")
       List<Object[]> results = query.list();
       for(Object[] result:results) {
           String value = (String)result[0];
           String code = (String)result[1];
           returnVal.put(value, code);
       }
       return returnVal;
    }
    
    /**
     * Get a List of all the tobacco classes.
     * @return List<PeriodTaxRateViewEntity>
     */
    @SuppressWarnings("unchecked")
	@Override
    public List<PeriodTaxRateViewEntity> getTobaccoClasses(long periodId) {
        String hql = "select tobaccoRate FROM PeriodTaxRateViewEntity tobaccoRate "+
                     "WHERE tobaccoRate.periodId = :periodId";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("periodId", periodId);
        return query.list();
    }
    
    /**
     * Get a Latest List of all the tobacco classes.
     * @return List<TobaccoClassEntity>
     */
    @SuppressWarnings("unchecked")
	@Override
    public List<CurrentTobaccoRateViewEntity> getLatestRates() {
        String hql = "FROM CurrentTobaccoRateViewEntity tc order by tc.tobaccoClassId";
        Query query = getCurrentSession().createQuery(hql);
        return query.list();
    }
    
    /**
     * Get a List of all the tobacco classes.
     * @return List<TobaccoClassEntity>
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<TobaccoClassEntity> getAllTobaccoClasses() {
        String hql = "FROM TobaccoClassEntity t";
        Query query = getCurrentSession().createQuery(hql);
        return query.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public TobaccoClassFacade getTobaccoClassFacade() {
        Mapper mapper = mapperWrapper.getMapper();
        String hql = "FROM TobaccoClassEntity t";
        Query query = getCurrentSession().createQuery(hql);
        List<TobaccoClassEntity> entities = query.list();
        List<TobaccoClass> classes = new ArrayList<TobaccoClass>();
        for(TobaccoClassEntity entity : entities) {
        	List<TaxRateEntity> rates = entity.getTaxRates();
        	for(TaxRateEntity rate : rates) {	
            	TobaccoClass tobacco = new TobaccoClass();
            	mapper.map(entity, tobacco);
	        	mapper.map(rate, tobacco);
	        	classes.add(tobacco);
        	}
        }
        return new TobaccoClassFacade(classes);
    }
    
    /**
     * Save a new set of tax rates and update the old.
     */
	@Override
	public void saveTaxRates(List<TaxRateEntity> rates) {
    	for(TaxRateEntity rate : rates) {
    		getCurrentSession().saveOrUpdate(rate);
    	}
    }

    /*
     * Get a map of all tobacco classes (key) mapped to their respective parent class (value).
     * @return Map<Long, Long>
     */ 
    @Override
    public Map<Long, Long> getTobaccoParentMap(long periodId) {
        List<TobaccoClassEntity> tobaccoClasses = this.getAllTobaccoClasses();
        Map<Long, Long> tobaccoParents = new HashMap<>();
        for(TobaccoClassEntity tobaccoClass : tobaccoClasses) {
            if(tobaccoClass.getParentClassId() == null) {
                tobaccoParents.put(tobaccoClass.getTobaccoClassId(), tobaccoClass.getTobaccoClassId());
            }
            else {
                tobaccoParents.put(tobaccoClass.getTobaccoClassId(), tobaccoClass.getParentClassId());
            }
        }
        return tobaccoParents;
    }
    
    @SuppressWarnings("unchecked")
	@Override
	public List<HTSCodeEntity> getHtsCodes() {
		String hql = "FROM HTSCodeEntity hts";
        Query query = getCurrentSession().createQuery(hql);
        return query.list();
	}

	@Override
	public void saveOrEditCodes(HTSCodeEntity htsCode) {
		this.getCurrentSession().saveOrUpdate(htsCode);
	}
	
	@Override
	public HTSCodeEntity getCodeById(String htscode) {
		String hql = "FROM HTSCodeEntity hts where hts.htsCode = :htscode";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("htscode", htscode);
        return (HTSCodeEntity)query.uniqueResult();
	}   
	
	/**
	 * Get map that holds ids by name for a fiscal year.
	 * @param fiscalYr
	 * @return
	 */
	@Override
	public Map<String, Long> getTobaccoClassNamesById() {
		Map<String, Long> returnMap = new HashMap<String, Long>();
		List<TobaccoClassEntity> results = this.getAllTobaccoClasses();
        for(TobaccoClassEntity tobacco: results) {
        	returnMap.put(tobacco.getTobaccoClassNm(), tobacco.getTobaccoClassId());
        }
        return returnMap;
	}

	@Override
	public Map<Long,String> getTobaccoClassIdNameMap() {
		Map<Long,String> returnMap = new HashMap<Long,String>();
		List<TobaccoClassEntity> results = this.getAllTobaccoClasses();
        for(TobaccoClassEntity tobacco: results) {
        	returnMap.put(tobacco.getTobaccoClassId(),tobacco.getTobaccoClassNm());
        }
        return returnMap;
	}

    
}
package gov.hhs.fda.ctp.persistence;

import gov.hhs.fda.ctp.persistence.model.UserEntity;

/**
 * The Interface RegistrationEntityManager.
 */
public interface RegistrationEntityManager {
	
	/**
	 * Register user.
	 *
	 * @param user the user
	 */
	public void registerUser(UserEntity user);

}

package gov.hhs.fda.ctp.persistence;

import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.persistence.model.UserEntity;

/**
 * The Class RegistrationEntityManagerImpl.
 */
@Repository("registrationEntityManager")
public class RegistrationEntityManagerImpl extends BaseEntityManagerImpl implements RegistrationEntityManager {

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.RegistrationEntityManager#registerUser(gov.hhs.fda.ctp.persistence.model.UserEntity)
	 */
	@Override
	public void registerUser(UserEntity user) {
		getCurrentSession().save(user);
	}
	
}

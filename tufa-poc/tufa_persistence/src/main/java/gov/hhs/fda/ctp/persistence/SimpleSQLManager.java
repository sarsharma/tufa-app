/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.AnnImporterGrid;
import gov.hhs.fda.ctp.common.beans.AssessmentReportStatus;
import gov.hhs.fda.ctp.common.beans.CBPDetailsFile;
import gov.hhs.fda.ctp.common.beans.CBPEntry;
import gov.hhs.fda.ctp.common.beans.CBPExportDetail;
import gov.hhs.fda.ctp.common.beans.CBPImporter;
import gov.hhs.fda.ctp.common.beans.CBPIngestestedSummaryDetailsData;
import gov.hhs.fda.ctp.common.beans.CBPLineEntryUpdateInfo;
import gov.hhs.fda.ctp.common.beans.CBPNewCompanies;
import gov.hhs.fda.ctp.common.beans.CBPSummaryFile;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentRptDetail;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.CompanyPermitHistory;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.ComparisonAllDeltaComments;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.ExcludePermitDetails;
import gov.hhs.fda.ctp.common.beans.NewCompaniesExportDetail;
import gov.hhs.fda.ctp.common.beans.PermitAudit;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.common.beans.PermitUpdateUpload;
import gov.hhs.fda.ctp.common.beans.PermitUpdatesDocument;
import gov.hhs.fda.ctp.common.beans.PreviousTrueupInfo;
import gov.hhs.fda.ctp.common.beans.QtReconRptDetail;
import gov.hhs.fda.ctp.common.beans.ReconExportDetail;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.common.beans.TobaccoTypeTTBVol;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail7501Period;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionHTSCodeSummary;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonSummary;
import gov.hhs.fda.ctp.common.beans.UnknownCompanyBucket;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.CBPDateConfigEntity;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonAllDeltaStatusEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonCommentAttachmentEntity;
import gov.hhs.fda.ctp.persistence.model.PermitAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitCommentAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateEntity;
import gov.hhs.fda.ctp.persistence.model.TTBPermitBucket;
import gov.hhs.fda.ctp.persistence.model.TrueUpSubDocumentEntity;

/**
 * The Interface SimpleSQLManager.
 *
 * @author tgunter
 */
public interface SimpleSQLManager {

	
	public List<CBPExportDetail> getCBPExportData(int year, int quarter, String tobaccoClass, String ein);
	/**
	 * Use simple SQL to retrieve the list of permit history.
	 *
	 * @param permitId
	 *            the permit id
	 * @return the permit history
	 */
	public List<CompanyPermitHistory> getPermitHistory(long permitId);

	/**
	 * Paginated permit history.
	 * @param permitId
	 * @return
	 */
	//public List<CompanyPermitHistory> getPaginatedPermitHistory(long permitId, int page, int rows);

	/**
	 * Get history count for permit.
	 * @param permitId
	 * @return
	 */
	//public int getPermitHistoryCount(long permitId);

	/**
	 * Get the full permit history for the company.
	 * @param companyId
	 * @return
	 */
	public List<CompanyPermitHistory> getCompanyPermitHistory(long companyId);

	/**
	 * Get all permit histories based on the criteria.
	 *
	 * @param criteria
	 *            the criteria
	 * @return the all permit history
	 */
	public PermitHistoryCriteria getAllPermitHistory(PermitHistoryCriteria criteria);

	/**
	 * Get all permit histories based on the criteria.
	 *
	 * @param criteria
	 *            the criteria
	 * @return the all companies
	 */
	public CompanySearchCriteria getAllCompanies(CompanySearchCriteria criteria);

	/**
	 * Get all available fiscal year values, descending.
	 *
	 * @return the available permit report fiscal years
	 */
	public Set<String> getAvailablePermitReportFiscalYears();

	/**
	 * Get all available calendar year values, descending.
	 *
	 * @return the available permit report calendar years
	 */
	public Set<String> getAvailablePermitReportCalendarYears();

	/**
	 * Get a consolidated view of all monthly reports per quarter.
	 * Also categorize each report as Cigar only and mixed.
	 * @param year
	 * @param quarter
	 * @return
	 */
	public Map<Long, AssessmentReportStatus> getReportStatusData(int year, int quarter);

	/**
	 * Find attachments by id.
	 *
	 * @param periodId
	 * @param permitId
	 * @return
	 */
	public List<AttachmentsEntity> findByIds(long periodId, long permitId);
	public List<PermitAttachmentsEntity> findByPermitId(long permitId);
	
	/**
	 * Find supporting documents by assessment id.
	 *
	 * @param assessmentId the assessment id
	 * @return the list
	 */
	public List<SupportingDocument> findSupportingDocumentsByAssessmentId(long assessmentId);
	
	public List<SupportingDocument> getCigarSupportDocs(long fiscalYr);

	/**
	 * Find docs by fiscal yr.
	 *
	 * @param fiscalYr the fiscal yr
	 * @return the list
	 */
	public List<TrueUpSubDocumentEntity> findDocsByFiscalYr(long fiscalYr);

	/**
	 * Get the reconciliation data based on the fiscal year and the quarter.
	 * @param year
	 * @param quarter
	 * @return
	 */

	public List<QtReconRptDetail> getReconReportData(long year, long quarter);

	/**
	 * Get the reconciliation data based on the fiscal year
	 * @param year
	 * @return
	 */
	public List<QtReconRptDetail> getReconReportDataForFiscalYear(long year);

	/**
	 * Retrieve the recon records for export.
	 * @param year
	 * @return
	 */
	public List<ReconExportDetail> getReconExportDataForFiscalYear(long year);

	/**
	 * Retrieve the recon records for export.
	 * @param year
	 * @param quarter
	 * @return
	 */
	public List<ReconExportDetail> getReconExportData(long year, long quarter);

	/**
	 * Retrieve the recon records for export.
	 * @param year
	 * @return
	 */
	public List<ReconExportDetail> getCigarReconExportData(long year);

	/**
	 * Get the records for the company comparison bucket for annual trueup.
	 * @param fiscalYear
	 * @return
	 */
	public List<CBPImporter> getCompanyComparisonBucket(Long fiscalYear);

	/**
	 * Get the records for the unknown companies bucket for annual trueup.
	 * @param fiscalYear
	 * @return
	 */
	public List<UnknownCompanyBucket> getUnknownCompaniesBucket(Long fiscalYear);

	/**
	 *
	 * @param year
	 * @return
	 */
	public List<CigarAssessmentRptDetail> getCigarAssessmentReportStatusData(long year);

	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType	 * MANU or IMPT
	 * @return
	 */
	public List<TrueUpComparisonSummary> getCigarComparisonSummary(long companyId, long fiscalYear, String permitType);

	
	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType	 * MANU or IMPT
	 * @return
	 */
	public AnnImporterGrid getImporterComparisonSummary(long companyId, long fiscalYear, long quarter, String tobaccoClass);


	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType	 * MANU or IMPT
	 * @param tobaccoClass "Cigarettes", "Chew/Snuff" "Pipe/RYO"
	 * @return
	 */
	public List<TrueUpComparisonSummary> getNonCigarManufacturerComparisonSummary(long companyId, long fiscalYear, long quarter, String permitType, String tobaccoClass);


	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType	 * MANU or IMPT
	 * @return
	 */
	public List<TrueUpComparisonImporterDetail> getImporterComparisonDetails(String ein, long fiscalYear, long quarter, String permitType, String tobaccoClass);

/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType	 * MANU or IMPT
	 * @return
	 */
	public List<TrueUpComparisonImporterIngestionDetail> getImporterComparisonIngestionDetails(String ein, long fiscalYear, long quarter, String permitType, String tobaccoClass, String ingestionType);

	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType	 * MANU or IMPT
	 * @return
	 */
	public List<TrueUpComparisonImporterIngestionHTSCodeSummary> getImporterComparisonIngestionHTSCodeSummary(String ein, long fiscalYear, long quarter, String permitType, String tobaccoClass);	
	
	
	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType
	 * @return
	 */
	public List<TrueUpComparisonManufacturerDetail> getCigarManufacturerComparisonDetails(long companyId, long fiscalYear,  long quarter, String permitType, String tobaccoClass);


	/**
	 *
	 * @param companyId
	 * @param fiscalYear
	 * @param permitType	 * MANU or IMPT
	 * @param tobaccoClass "Cigarettes", "Chew/Snuff" "Pipe/RYO"
	 * @return
	 */
	public List<TrueUpComparisonManufacturerDetail> getNonCigarManufacturerComparisonDetails(long companyId, long fiscalYear, long quarter, String permitType, String tobaccoClass);

	/**
	 *
	 * @param year
	 * @return
	 */
	public List<TrueUpComparisonResults> getComparisonResults(long fiscalYear, TrueUpCompareSearchCriteria csc);
	
	public List<TrueUpComparisonResults> getComparisonSingleResults(long fiscalYear, TrueUpCompareSearchCriteria csc);
	
	public List<TrueUpComparisonResults> getAllComparisonResults(long fiscalYear, TrueUpCompareSearchCriteria csc);	

	public List<TrueUpComparisonResults> getAllComparisonSingleResults(long fiscalYear, TrueUpCompareSearchCriteria csc);	

	public List<TTBPermitBucket> getTTBPermits(long fiscalYear);

	public List<CompanyIngestion> getCompaniesForIncludeBucket(Long fiscalYear);

	public void removeTrueUpDoc(long fiscalYr, String doctype);
	
	public void removeTTBVolumes(long fiscalYr);
	
	public void removeTTBEntries(long fiscalYr);
	
	public void removeCBPEntries(long fiscalYr);
	
	public void removeCBPSummaryData(long fiscalYr);
	
	public void removeCBPDetailsData(long fiscalYr);
	
	public Map<String, String> fetchFDA352MAN(long fiscalYr, long company_id, String classNm);
	
	public Map<String, String> fetchFDA352IMP(long fiscalYr, String ein, String classNm);
	
	public Map<String, String> fetchFDA352IMPSingle(long fiscalYr, String ein, String classNm);
	
	public Map<String, String> fetchOneSided(long fiscalYr, long company_id, String classNm, String reportType, String ein);
	
	public List<CBPNewCompanies> getCBPNewCompanies(long fiscalYear) throws SQLException, IOException;
	
	public List<PermitUpdatesDocument> getPermitUpdateFiles();
	
	public List<PermitUpdateUpload> getUpdatedPermits(Long docId);
	
	public List<CompanyPermitHistory> getPermitHistory3852(long permitId);
	
	public List<PermitAudit> getPermitMgmtAuditListByDocId(Long docId);
	
	public List<PermitUpdateUpload> getUpdatedPermitsContacts(Long docId);
	
	public List<PermitUpdateUpload> saveUpdatedPermits(PermitUpdateEntity updatedPermit);
	
	public List<PermitUpdateUpload> saveUpdatedContacts(PermitUpdateEntity updatedPermit);
	
	public List<PermitUpdateEntity> setIgnrContactStatusandDelete();
	
	public List<PermitUpdateEntity> setIgnrAddrStatusandDelete();
	
	public TrueUpComparisonResults getAllComparisonResultsCount(long fiscalYear,String inComingPage);
	
	public TrueUpComparisonResults getAllComparisonSingleResultsCount(long fiscalYear,String inComingPage);
	
	public List<ComparisonAllDeltaStatusEntity> getAllDeltasForStatusChange(TrueUpComparisonResults model,String searchCriteria) ;
	
	public List<ComparisonAllDeltaStatusEntity> getAllDeltasForStatusChangeSingle(TrueUpComparisonResults model,String searchCriteria) ;

	public List<CompanyEntity> getCompanyTrackingInformationForSearch(CompanySearchCriteria criteria);

	public List<TobaccoTypeTTBVol> getTTBVolumeTaxPerTobaccoType(TobaccoTypeTTBVol ttbVol);

	public List<NewCompaniesExportDetail> getNewCompaniesList();
	
	public List<CBPEntry> getHTSMissingAssociationData(long fiscalYear);
	
	public List<TrueUpComparisonResults> getDeltasAffectedByCompareAllActions(long fiscalYr, String ein, String allAction);
	
	public List<TrueUpComparisonResults> getDeltasAffectedByCompareAllActionsSingle(long fiscalYr, String ein, String allAction);
	
	public List<Address> getIngestedAddress();
	
	public List<Contact> getIngestedContacts();
	
	public String getDateOfLatestStartedreport(long permitId);
	
	public List<Long> getPeriodIdsAfterDate(Long permitId, String inactiveDate);
	
	public  CBPIngestestedSummaryDetailsData getImporterSummDetailData(String einType, String ein, long fiscalYear,
			String quarter, String permitType, String tobaccoClass);
	
	public List<CBPSummaryFile> getSummaryFileExport(String ein, String companyId, String fiscalYear, String quarter, String tobaccoType);
	
	public List<CBPDetailsFile> getDetailsFileExport(String ein, String companyId, String fiscalYear, String quarter, String tobaccoType,String IngestionType);
	
	public ComparisonAllDeltaComments updateComment(ComparisonAllDeltaComments comment,
			long commentSeq);
	
	public List<PermitCommentAttachmentsEntity> findCommentDocbyId(long commentId);
	
	public List<ExcludePermitDetails> getPermitExcludeList(String fiscalYear,long companyId,int scopeId);
	
	public List<CBPLineEntryUpdateInfo> getCBPLineEntry(CBPDateConfigEntity cbpDateConfigEntity, boolean isBulkReallocation) throws ParseException ;
	
	public long getCompnyIDForReallcoation(long cbpEntryId);
	
	public String getCompnyEINForReallcoation(long cbpEntryId);
	
	public String getTobaccoClassName(long tobaccoClassId);
	
	public boolean isIngestedLinedPresentForFiscalYear(long fiscalYear,String typeOfFile);
	
	public List<TrueUpComparisonResults> getAllAllDeltasDetailsForEIN(long fiscalYear,String permitType,String ein);
	
	public String getAssociatedCompanyEIN(long fiscalYear,long fiscalQtr, String tobaccoClassNm, String ein);
	
	public List<String> getAllNonZeroDeltaTobaccoClass(long fiscalYear,String permitType,String ein);
	public List<ComparisonAllDeltaStatusEntity> getAllDeltasFrmSingleView(TrueUpComparisonResults model);
	
	List<Object[]>getMixedActionDetaislForQuarter(long companyId,long fiscalYr, long quarter,String tobaccoClass);
	
	public void deleteMixedActionDetails(long amendmentId);
	
	List<Object[]>getMixedActionDetaislForQuarterManu(long companyId,long fiscalYr, long quarter,String tobaccoClass);
	
	public void deleteMixedActionDetailsManu(long amendmentId);
	
	public List<TrueUpComparisonImporterDetail7501Period> getPeriod7501Detail(String ein, long fiscalYr, String tobaccoClass, long quarter);
	
	public List<ComparisonCommentAttachmentEntity> findCompCommentDocbyId(long cbpCommentSeq, long ttbCommentSeq, long allDeltaCommentSeq) ;
	
	public long getDocCount(long cbpCommentSeq, long ttbCommentSeq, long allDeltaCommentSeq);
	
	public void processAdjustments(String fiscalYear);
	List<Long> getFiscalYearsWithNewDeltas();
	
	List<PreviousTrueupInfo> getPreviousTrueupInfo(String type,long currentYear,String ein);
	
}

/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.persistence.ParameterMode;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.procedure.ProcedureOutputs;
import org.hibernate.query.Query;
import org.hibernate.type.StandardBasicTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.AnnImporterGrid;
import gov.hhs.fda.ctp.common.beans.AnnImporterGridRecord;
import gov.hhs.fda.ctp.common.beans.AssessmentReportStatus;
import gov.hhs.fda.ctp.common.beans.AssessmentReportStatusMonth;
import gov.hhs.fda.ctp.common.beans.Attachment;
import gov.hhs.fda.ctp.common.beans.CBPDetailsFile;
import gov.hhs.fda.ctp.common.beans.CBPEntry;
import gov.hhs.fda.ctp.common.beans.CBPExportDetail;
import gov.hhs.fda.ctp.common.beans.CBPImporter;
import gov.hhs.fda.ctp.common.beans.CBPImporterTaxDeltas;
import gov.hhs.fda.ctp.common.beans.CBPIngestestedSummaryDetailsData;
import gov.hhs.fda.ctp.common.beans.CBPLineEntryUpdateInfo;
import gov.hhs.fda.ctp.common.beans.CBPNewCompanies;
import gov.hhs.fda.ctp.common.beans.CBPSummaryFile;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentRptDetail;
import gov.hhs.fda.ctp.common.beans.CompanyHistory;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.CompanyPermitHistory;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.ComparisonAllDeltaComments;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.ExcludePermitDetails;
import gov.hhs.fda.ctp.common.beans.NewCompaniesExportDetail;
import gov.hhs.fda.ctp.common.beans.PermitAudit;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.common.beans.PermitUpdateUpload;
import gov.hhs.fda.ctp.common.beans.PermitUpdatesDocument;
import gov.hhs.fda.ctp.common.beans.PreviousTrueupInfo;
import gov.hhs.fda.ctp.common.beans.QtReconRptDetail;
import gov.hhs.fda.ctp.common.beans.ReconExportDetail;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.common.beans.TobaccoClass;
import gov.hhs.fda.ctp.common.beans.TobaccoTypeTTBVol;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail7501Period;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetailPeriod;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetailPeriod;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionHTSCodeSummary;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetailPermit;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonSummary;
import gov.hhs.fda.ctp.common.beans.UnknownCompanyBucket;
import gov.hhs.fda.ctp.common.comparators.DescendingStringComparator;
import gov.hhs.fda.ctp.common.pagination.PaginationAttributes;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.model.AnnImporterIngestedDetailRecordViewEntity;
import gov.hhs.fda.ctp.persistence.model.AnnImporterIngestedSummaryRecordViewEntity;
import gov.hhs.fda.ctp.persistence.model.AnnImporterTufaRecordViewEntity;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.CBPDateConfigEntity;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonAllDeltaStatusEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonCommentAttachmentEntity;
import gov.hhs.fda.ctp.persistence.model.PermitAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitCommentAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateEntity;
import gov.hhs.fda.ctp.persistence.model.TTBPermitBucket;
import gov.hhs.fda.ctp.persistence.model.TrueUpSubDocumentEntity;
/**
 * Simple SQL lookups should be placed in here.
 *
 * @author tgunter
 *
 */
@Repository("simpleSQLManager")
public class SimpleSQLManagerImpl extends BaseEntityManagerImpl implements SimpleSQLManager {

	/** The reference code entity manager. */
	private ReferenceCodeEntityManager referenceCodeEntityManager;

	private AddressMgmtEntityManager addressMgmtEntityManager;
	
	
	

	/**
	 * @param referenceCodeEntityManager
	 *            the referenceCodeEntityManager to set
	 */
	@Autowired
	public void setReferenceCodeEntityManager(ReferenceCodeEntityManager referenceCodeEntityManager) {
		this.referenceCodeEntityManager = referenceCodeEntityManager;
	}
	
	@Autowired
	public void setAddressMgmtEntityManager(AddressMgmtEntityManager addressMgmtEntityManager) {
		this.addressMgmtEntityManager = addressMgmtEntityManager;
	}
	
	

	/** The Constant CONST_WHERE. */
	private static final String CONST_WHERE = " WHERE ";

	/** The Constant CONST_AND. */
	private static final String CONST_AND = " AND ";

	/** The Constant CONST_OR. */
	private static final String CONST_OR = " OR ";

	/** The Constant QUARTER_1_MONTHS. */
	private static final String QUARTER_1_MONTHS = "'OCT', 'NOV', 'DEC'";

	/** The Constant QUARTER_2_MONTHS. */
	private static final String QUARTER_2_MONTHS = "'JAN', 'FEB', 'MAR'";

	/** The Constant QUARTER_3_MONTHS. */
	private static final String QUARTER_3_MONTHS = "'APR', 'MAY', 'JUN'";

	/** The Constant QUARTER_4_MONTHS. */
	private static final String QUARTER_4_MONTHS = "'JUL', 'AUG', 'SEP'";

	private static final Integer DELETE_BATCH_SIZE = 5000;

	private Calendar cal;
	private Map<Long,Integer> totalCountMap = new HashMap<Long, Integer>();

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(SimpleSQLManagerImpl.class);
	
	@Autowired
	private SecurityContextUtil securityContextUtil;

	/*
	 * (non-Javadoc)
	 *
	 * @see gov.hhs.fda.ctp.persistence.SimpleSQLManager#getPermitHistory(long)
	 */
	@Override
	public List<CompanyPermitHistory> getPermitHistory(long permitId) {
		logger.debug("getPermitHistory({})", permitId);

		List<CompanyPermitHistory> returnList = new ArrayList<>();
		StringBuilder sql = new StringBuilder(
				"SELECT rp.period_id, rp.month, rp.year, rp.fiscal_yr, ps.PERIOD_STATUS_TYPE_CD, "
						+ "co.LEGAL_NM, co.company_id, p.permit_id, p.permit_num, p.permit_type_cd, p.permit_status_type_cd "
						+ "FROM TU_PERMIT p, TU_PERMIT_PERIOD pp, TU_RPT_PERIOD rp, TU_PERIOD_STATUS ps, TU_COMPANY co "
						+ "WHERE p.PERMIT_ID = pp.PERMIT_ID " + "AND rp.PERIOD_ID = pp.PERIOD_ID "
						+ "AND co.COMPANY_ID = p.COMPANY_ID "
						+ "AND (pp.permit_id = ps.permit_id and pp.period_id = ps.period_id) "
						+ "AND p.permit_id = :permit_id");
		sql.append(
				" ORDER BY ps.PERIOD_STATUS_TYPE_CD DESC, rp.year DESC, to_date(rp.month,'MON') DESC, p.permit_num ASC");
		Query query = getCurrentSession().createSQLQuery(sql.toString());
		query.setParameter("permit_id", permitId);
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.list();
		if (results != null) {
			for (Object[] result : results) {
				CompanyPermitHistory permitHistory = new CompanyPermitHistory();
				if (result[0] != null) {
					permitHistory.setReportPeriodId(Long.parseLong(result[0].toString()));
				}
				permitHistory.setMonth(result[1].toString());
				permitHistory.setYear(result[2].toString());
				permitHistory.setFiscalYear(result[3].toString());
				permitHistory.setReportStatusCd(result[4].toString());

				permitHistory.setCompanyName(safeToString(result[5]));
				permitHistory.setCompanyId(Long.parseLong(result[6].toString()));
				permitHistory.setPermitId(Long.parseLong(result[7].toString()));
				permitHistory.setPermitNum(safeToString(result[8]));
				permitHistory.setPermitTypeCd(safeToString(result[9]));
				permitHistory.setPermitStatusCd(safeToString(result[10]));
				returnList.add(permitHistory);
			}
		}

		return returnList;
	}

	@Override
	public List<CompanyPermitHistory> getPermitHistory3852(long permitId) {
		logger.debug("getPermitHistory({})", permitId);

		List<CompanyPermitHistory> returnList = new ArrayList<>();
		StringBuilder sql = new StringBuilder(
				"SELECT rp.period_id, TO_CHAR(TO_DATE(rp.month,'MM'),'Month') as month, rp.year, rp.fiscal_yr, ps.PERIOD_STATUS_TYPE_CD, "
						+ "co.LEGAL_NM, co.company_id, p.permit_id, p.permit_num, p.permit_type_cd, p.permit_status_type_cd, doc.document_id "
						+ "FROM TU_PERMIT p, TU_PERMIT_PERIOD pp, TU_RPT_PERIOD rp, TU_PERIOD_STATUS ps, TU_COMPANY co, TU_DOCUMENT doc "
						+ "WHERE p.PERMIT_ID = pp.PERMIT_ID AND rp.PERIOD_ID = pp.PERIOD_ID "
						+ "AND doc.PERIOD_ID = pp.PERIOD_ID AND doc.PERMIT_ID = pp.PERMIT_ID "
						+ "AND co.COMPANY_ID = p.COMPANY_ID "
						+ "AND (pp.permit_id = ps.permit_id and pp.period_id = ps.period_id) "
						+ "AND p.permit_id = :permit_id  AND ps.PERIOD_STATUS_TYPE_CD <> 'NSTD' AND doc.FORM_TYPES like '%3852%'");
		sql.append(
				" ORDER BY ps.PERIOD_STATUS_TYPE_CD DESC, rp.year DESC, to_date(rp.month,'MON') DESC, p.permit_num ASC");
		Query query = getCurrentSession().createSQLQuery(sql.toString());
		query.setParameter("permit_id", permitId);
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.list();
		if (results != null) {
			for (Object[] result : results) {
				CompanyPermitHistory permitHistory = new CompanyPermitHistory();
				if (result[0] != null) {
					permitHistory.setReportPeriodId(Long.parseLong(result[0].toString()));
				}
				permitHistory.setMonth(result[1].toString());
				permitHistory.setYear(result[2].toString());
				permitHistory.setFiscalYear(result[3].toString());
				permitHistory.setReportStatusCd(result[4].toString());

				permitHistory.setCompanyName(safeToString(result[5]));
				permitHistory.setCompanyId(Long.parseLong(result[6].toString()));
				permitHistory.setPermitId(Long.parseLong(result[7].toString()));
				permitHistory.setPermitNum(safeToString(result[8]));
				permitHistory.setPermitTypeCd(safeToString(result[9]));
				permitHistory.setPermitStatusCd(safeToString(result[10]));
				permitHistory.setDoc3852Id(Long.parseLong(result[11].toString()));
				returnList.add(permitHistory);
			}
		}

		return returnList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * gov.hhs.fda.ctp.persistence.SimpleSQLManager#getPaginatedPermitHistory(
	 * long)
	 */
	// @Override
	// public List<CompanyPermitHistory> getPaginatedPermitHistory(long
	// permitId, int page, int rows) {
	// logger.debug("getPaginatedPermitHistory({}, {}, {})", permitId, page,
	// rows);
	//
	// List<CompanyPermitHistory> returnList = new ArrayList<>();
	// // convert page and rows into start record and end record
	// int start = page * rows;
	// int end = start + rows;
	// /*
	// * select * 2 from ( 3 select *+ first_rows(25) * 4
	// * object_id,object_name, 5 row_number() over 6 (order by object_id) rn
	// * 7 from all_objects) 8 where rn between :n and :m 9 order by rn;
	// */
	// StringBuilder sql = new StringBuilder("SELECT * FROM ( SELECT /*+
	// first_rows(25) */ "
	// + "rp.period_id, rp.month, rp.year, rp.fiscal_yr,
	// ps.PERIOD_STATUS_TYPE_CD, "
	// + "co.LEGAL_NM, co.company_id, p.permit_id, p.permit_num,
	// p.permit_type_cd, p.permit_status_type_cd, co.EIN, "
	// + "row_number() over "
	// + "(ORDER BY ps.PERIOD_STATUS_TYPE_CD DESC, rp.year DESC,
	// to_date(rp.month,'MON') DESC, p.permit_num ASC) rn "
	// + "FROM TU_PERMIT p, TU_PERMIT_PERIOD pp, TU_RPT_PERIOD rp,
	// TU_PERIOD_STATUS ps, TU_COMPANY co "
	// + "WHERE p.PERMIT_ID = pp.PERMIT_ID AND rp.PERIOD_ID = pp.PERIOD_ID "
	// + "AND co.COMPANY_ID = p.COMPANY_ID "
	// + "AND (pp.permit_id = ps.permit_id and pp.period_id = ps.period_id) " +
	// "AND p.permit_id = :permit_id)"
	// + "WHERE rn between :start and :end ORDER BY rn");
	// Query query = getCurrentSession().createSQLQuery(sql.toString());
	// query.setParameter("permit_id", permitId);
	// query.setParameter("start", start);
	// query.setParameter("end", end);
	// @SuppressWarnings("unchecked")
	// List<Object[]> results = query.list();
	// if (results != null) {
	// for (Object[] result : results) {
	// CompanyPermitHistory permitHistory = new CompanyPermitHistory();
	// if (result[0] != null) {
	// permitHistory.setReportPeriodId(Long.parseLong(result[0].toString()));
	// }
	// permitHistory.setMonth(result[1].toString());
	// permitHistory.setYear(result[2].toString());
	// permitHistory.setFiscalYear(result[3].toString());
	// permitHistory.setReportStatusCd(result[4].toString());
	//
	// permitHistory.setCompanyName(safeToString(result[5]));
	// permitHistory.setCompanyId(Long.parseLong(result[6].toString()));
	// permitHistory.setPermitId(Long.parseLong(result[7].toString()));
	// permitHistory.setPermitNum(safeToString(result[8]));
	// permitHistory.setPermitTypeCd(safeToString(result[9]));
	// permitHistory.setPermitStatusCd(safeToString(result[10]));
	// permitHistory.setCompanyEIN(safeToString(result[11]));
	// returnList.add(permitHistory);
	// }
	// }
	//
	// return returnList;
	// }
	//
	// @Override
	// public int getPermitHistoryCount(long permitId) {
	//
	// logger.debug("getPermitHistoryCount({})", permitId);
	//
	// int returnVal = 0;
	// StringBuilder sql = new StringBuilder(
	// "SELECT COUNT(pp.period_id) FROM TU_PERMIT_PERIOD pp WHERE
	// pp.PERMIT_ID=:permit_id");
	// Query query = getCurrentSession().createSQLQuery(sql.toString());
	// query.setParameter("permit_id", permitId);
	// returnVal = query.executeUpdate();
	// return returnVal;
	// }

	/**
	 * Get full permit history for company.
	 */
	@Override
	public List<CompanyPermitHistory> getCompanyPermitHistory(long companyId) {
		logger.debug("getCompanyPermitHistory({})", companyId);

		List<CompanyPermitHistory> returnList = new ArrayList<>();
		StringBuilder sql = new StringBuilder(
				"SELECT rp.period_id, rp.month, rp.year, rp.fiscal_yr, ps.PERIOD_STATUS_TYPE_CD, "
						+ "co.LEGAL_NM, co.company_id, p.permit_id, p.permit_num, p.permit_type_cd, p.permit_status_type_cd, co.EIN "
						+ "FROM TU_PERMIT p, TU_PERMIT_PERIOD pp, TU_RPT_PERIOD rp, TU_PERIOD_STATUS ps, TU_COMPANY co "
						+ "WHERE p.PERMIT_ID = pp.PERMIT_ID " + "AND rp.PERIOD_ID = pp.PERIOD_ID "
						+ "AND co.COMPANY_ID = p.COMPANY_ID "
						+ "AND (pp.permit_id = ps.permit_id and pp.period_id = ps.period_id) "
						+ "AND co.company_id = :company_id");
		// sql.append(
		// " ORDER BY ps.PERIOD_STATUS_TYPE_CD DESC, rp.year DESC,
		// to_date(rp.month,'MON') DESC, p.permit_num ASC");
		// Query query = getCurrentSession().createSQLQuery(sql.toString());

		Query query = this.getPaginatedSQLQuery(new StringBuffer(sql));

		query.setParameter("company_id", companyId);

		@SuppressWarnings("unchecked")
		List<Object[]> results = query.list();
		if (results != null) {
			for (Object[] result : results) {
				CompanyPermitHistory permitHistory = new CompanyPermitHistory();
				if (result[0] != null) {
					permitHistory.setReportPeriodId(Long.parseLong(result[0].toString()));
				}
				permitHistory.setMonth(result[1].toString());
				permitHistory.setYear(result[2].toString());
				permitHistory.setFiscalYear(result[3].toString());
				permitHistory.setReportStatusCd(result[4].toString());

				permitHistory.setCompanyName(safeToString(result[5]));
				permitHistory.setCompanyId(Long.parseLong(result[6].toString()));
				permitHistory.setPermitId(Long.parseLong(result[7].toString()));
				permitHistory.setPermitNum(safeToString(result[8]));
				permitHistory.setPermitTypeCd(safeToString(result[9]));
				permitHistory.setPermitStatusCd(safeToString(result[10]));
				permitHistory.setCompanyEIN(safeToString(result[11]));
				returnList.add(permitHistory);
			}
		}

		return returnList;
	}

	/**
	 * When the data model stabilizes, we need to complete the hibernate mapping
	 * and refactor this into a true criteria search.
	 *
	 * @param criteria
	 *            the criteria
	 * @return the all permit history
	 */
	@Override
	public PermitHistoryCriteria getAllPermitHistory(PermitHistoryCriteria criteria) {
		if (logger.isInfoEnabled()) {
			logger.info("getAllPermitHistory({})", criteria.toString());
		}
		List<CompanyPermitHistory> returnList = new ArrayList<>();
		/*
		 * for some reason, hibernate does not understand the second left outer
		 * join with document. Need to address this in a later release.
		 */

		StringBuilder sql = new StringBuilder(
				" SELECT res.*, COUNT(*) OVER () RESULT_COUNT, ( SELECT LISTAGG(fiscal_yr, ';') "
						+ "WITHIN GROUP (ORDER BY fiscal_yr desc) fy_list FROM (select unique rp.fiscal_yr "
						+ " from TU_COMPANY c " + " INNER JOIN TU_PERMIT p " + " ON c.COMPANY_ID=p.COMPANY_ID "
						+ " INNER JOIN TU_PERMIT_PERIOD pp " + " ON pp.PERMIT_ID=p.PERMIT_ID "
						+ " INNER JOIN TU_RPT_PERIOD rp " + " ON pp.period_id=rp.period_id " + ") yrs ) uq_yrs "
						+ "FROM "
						+ "(SELECT UPPER(TRIM(c.legal_nm)) as legal_nm, c.company_id,p.permit_id,p.permit_num,p.permit_type_cd,p.permit_status_type_cd,"
						+ "rp.period_id,rp.month,rp.year, rp.fiscal_yr,"
						+ " COALESCE(ps.PERIOD_STATUS_TYPE_CD, 'NSTD') AS status,c.EIN, "
						+ " to_date((rp.month||'-'||rp.year),'MM-YYYY') as monthAndYear " + "FROM TU_COMPANY c "
						+ "INNER JOIN TU_PERMIT p " + "ON c.COMPANY_ID=p.COMPANY_ID "
						+ "INNER JOIN TU_PERMIT_PERIOD pp " + "ON pp.PERMIT_ID=p.PERMIT_ID "
						+ "INNER JOIN TU_RPT_PERIOD rp " + "ON pp.period_id=rp.period_id "
						+ "LEFT OUTER JOIN TU_PERIOD_STATUS ps " + "ON (pp.permit_id=ps.permit_id "
						+ "AND pp.period_id=ps.period_id) " + " ) res");
		boolean isFirstCondition = true;

		if (testCriterion(criteria.getPermitNameFilter())) {
			sql.append(
					" WHERE ((upper(res.PERMIT_NUM) LIKE :permit_num) OR (upper(res.LEGAL_NM) LIKE :company_name) OR (upper(res.EIN) LIKE :ein_number))");
			isFirstCondition = false;
		}
		if (testCriterion(criteria.getMonthFilters())) {
			if (isFirstCondition) {
				sql.append(CONST_WHERE);
				isFirstCondition = false;
			} else
				sql.append(CONST_AND);
			sql.append(" res.month IN (");
			sql.append(CTPUtil.returnSetAsCSV(criteria.getMonthFilters()));
			sql.append(") ");
		}
		if (testCriterion(criteria.getYearFilters())) {
			if (isFirstCondition) {
				sql.append(CONST_WHERE);
				isFirstCondition = false;
			} else
				sql.append(CONST_AND);
			sql.append(" res.fiscal_yr IN (");
			sql.append(CTPUtil.returnSetAsCSV(criteria.getYearFilters()));
			sql.append(") ");
		}
		if (testCriterion(criteria.getPermitStatusFilters())) {
			if (isFirstCondition) {
				sql.append(CONST_WHERE);
				isFirstCondition = false;
			} else
				sql.append(CONST_AND);
			sql.append(" res.permit_status_type_cd IN (");
			sql.append(CTPUtil.returnSetAsCSV(criteria.getPermitStatusFilters()));
			sql.append(") ");
		}
		if (testCriterion(criteria.getPermitTypeFilters())) {
			if (isFirstCondition) {
				sql.append(CONST_WHERE);
				isFirstCondition = false;
			} else
				sql.append(CONST_AND);
			sql.append(" res.permit_type_cd IN (");
			sql.append(CTPUtil.returnSetAsCSV(criteria.getPermitTypeFilters()));
			sql.append(") ");
		}
		/*
		 * replace this criterion when the hibernate join is fixed with document
		 */
		if (testCriterion(criteria.getReportStatusFilters())) {
			if (isFirstCondition) {
				sql.append(CONST_WHERE);
			} else
				sql.append(CONST_AND);
			sql.append(" res.status IN (");
			sql.append(CTPUtil.returnSetAsCSV(criteria.getReportStatusFilters()));
			sql.append(") ");
		}
		
		Query query1 = this.getPaginatedSQLQuery(new StringBuffer(sql));
				
		StringBuilder sql1 = new StringBuilder(query1.getQueryString());
		sql1.append(" , 13 DESC ");
		PaginationAttributes pgattrs = this.getPaginationAttributes();
		Integer activePg = pgattrs.getActivePage();
		Integer pageSize = pgattrs.getPageSize();
		Query query = getCurrentSession().createSQLQuery(sql1.toString());
		if (activePg != null && pageSize != null && !(activePg == 0 && pageSize ==0)) {
			query.setFirstResult((activePg - 1) * pageSize);
			query.setMaxResults(pageSize);
		}
		
		if (testCriterion(criteria.getPermitNameFilter())) {
			query.setParameter("permit_num", "%" + criteria.getPermitNameFilter().replace("-", "").toUpperCase() + "%");
			query.setParameter("company_name", "%" + criteria.getPermitNameFilter().toUpperCase() + "%");
			query.setParameter("ein_number", "%" + criteria.getPermitNameFilter().replace("-", "").toUpperCase() + "%");
		}

		// @SuppressWarnings("unchecked")
		List<Object[]> results = query.list();

		Integer itemsTotal = 0;
		String fiscalYearsList = "";

		if (results != null) {
			for (Object[] result : results) {
				CompanyPermitHistory permitHistory = new CompanyPermitHistory();
				permitHistory.setCompanyName(safeToString(result[0]));
				permitHistory.setCompanyId(Long.parseLong(result[1].toString()));
				permitHistory.setPermitId(Long.parseLong(result[2].toString()));
				permitHistory.setPermitNum(safeToString(result[3]));
				permitHistory.setPermitTypeCd(safeToString(result[4]));
				permitHistory.setPermitStatusCd(safeToString(result[5]));
				if (result[6] != null) {
					permitHistory.setReportPeriodId(Long.parseLong(result[6].toString()));
				}
				permitHistory.setMonth(safeToString(result[7]));
				permitHistory.setYear(safeToString(result[8]));
				permitHistory.setFiscalYear(safeToString(result[9]));
				permitHistory.setReportStatusCd(safeToString(result[10]));
				permitHistory.setCompanyEIN(safeToString(result[11]));

				returnList.add(permitHistory);

				itemsTotal = ((BigDecimal) result[13]).intValue();
				fiscalYearsList = (String) result[14];
			}
		}
		criteria.setResults(returnList);
		criteria.setItemsTotal(itemsTotal);
		criteria.setFiscalYears(fiscalYearsList);
		return criteria;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * gov.hhs.fda.ctp.persistence.SimpleSQLManager#getAllCompanies(gov.hhs.fda.
	 * ctp.common.beans.CompanySearchCriteria)
	 */
	@Override
	public CompanySearchCriteria getAllCompanies(CompanySearchCriteria criteria) {
		List<CompanyHistory> returnList = new ArrayList<>();
		StringBuilder sql = new StringBuilder(
				"SELECT UPPER(TRIM(co.LEGAL_NM)), co.COMPANY_ID, co.COMPANY_STATUS, LISTAGG(p.PERMIT_NUM || ':' || p.PERMIT_ID, ',')  WITHIN GROUP (ORDER BY "
						+ "p.PERMIT_NUM) AS active_permits, co.EIN "
						+ "FROM TU_PERMIT p INNER JOIN TU_COMPANY co ON co.COMPANY_ID = p.COMPANY_ID " + " ");

		boolean first = true;

		if (testCriterion(criteria.getCompanyNameFilter())) {
			if (first) {
				sql.append(CONST_WHERE);
				first = false;
			}
			sql.append(" ( upper(co.LEGAL_NM) LIKE :legal_name OR upper(co.EIN) LIKE :company_ein OR upper(p.PERMIT_NUM) LIKE :permit_num  ) ");
		}
		if (testCriterion(criteria.getCompanyStatusFilters())) {
			if (first) {
				sql.append(CONST_WHERE);
				first = false;
			} else {
				sql.append(CONST_AND);
			}
			sql.append(" co.COMPANY_STATUS IN (");
			sql.append(CTPUtil.returnSetAsCSV(criteria.getCompanyStatusFilters()));
			sql.append(") ");
		}

		sql.append("GROUP BY co.LEGAL_NM, co.EIN, co.COMPANY_ID, co.COMPANY_STATUS ORDER BY initcap(co.LEGAL_NM)");
		Query query = getCurrentSession().createSQLQuery(sql.toString());
		if (testCriterion(criteria.getCompanyNameFilter())) {
			query.setParameter("legal_name", "%" + criteria.getCompanyNameFilter().toUpperCase() + "%");
			query.setParameter("company_ein",
					"%" + criteria.getCompanyNameFilter().replace("-", "").toUpperCase() + "%");
			query.setParameter("permit_num",
					"%" + criteria.getCompanyNameFilter().replace("-", "").toUpperCase() + "%");
		}
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.list();
		if (results != null) {
			for (Object[] result : results) {
				CompanyHistory companyHistory = new CompanyHistory();
				companyHistory.setCompanyName(safeToString(result[0]));
				companyHistory.setCompanyId(Long.parseLong(result[1].toString()));
				companyHistory.setCompanyStatus(result[2].toString());
				companyHistory.setPermits(result[3].toString());
				companyHistory.setCompanyEIN(result[4].toString());
				returnList.add(companyHistory);
			}
		}
		criteria.setResults(returnList);

		return criteria;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see gov.hhs.fda.ctp.persistence.SimpleSQLManager#
	 * getAvailablePermitReportFiscalYears()
	 */
	@Override
	public Set<String> getAvailablePermitReportFiscalYears() {
		String sql = "SELECT DISTINCT(rp.YEAR) FROM TU_RPT_PERIOD rp INNER JOIN TU_PERMIT_PERIOD pp on rp.PERIOD_ID=pp.PERIOD_ID";
		Query query = getCurrentSession().createSQLQuery(sql);
		Set<String> years = new TreeSet<>(new DescendingStringComparator());
		@SuppressWarnings("unchecked")
		List<BigDecimal> results = query.list();
		if (results != null) {
			for (BigDecimal result : results) {
				years.add(result.toString());
			}
		}
		return years;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see gov.hhs.fda.ctp.persistence.SimpleSQLManager#
	 * getAvailablePermitReportCalendarYears()
	 */
	@Override
	public Set<String> getAvailablePermitReportCalendarYears() {
		String sql = "SELECT DISTINCT(rp.YEAR) FROM TU_RPT_PERIOD rp INNER JOIN TU_PERMIT_PERIOD pp on rp.PERIOD_ID=pp.PERIOD_ID";
		Query query = getCurrentSession().createSQLQuery(sql);
		Set<String> years = new TreeSet<>(new DescendingStringComparator());
		@SuppressWarnings("unchecked")
		List<BigDecimal> results = query.list();
		if (results != null) {
			for (BigDecimal result : results) {
				years.add(result.toString());
			}
		}
		return years;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * gov.hhs.fda.ctp.persistence.SimpleSQLManager#getReportStatusData(int,
	 * int)
	 */
	@Override
	public Map<Long, AssessmentReportStatus> getReportStatusData(int year, int quarter) {

		Map<String, String> periodStatusValues = referenceCodeEntityManager.getValues("PERIOD_STATUS_TYPE");
		Map<String, String> permitTypes = referenceCodeEntityManager.getValues("PERMIT_TYPE");

		StringBuilder sql = new StringBuilder(
				"SELECT t.period_id, t.month, t.year, t.fiscal_yr, t.PERIOD_STATUS_TYPE_CD, t.LEGAL_NM, t.company_id, t.permit_id, t.permit_num, t.permit_type_cd, t.permit_status_type_cd, cigar.cigar_only, NVL(t.zero_flag, 'N')  from ("
						+ "SELECT rp.period_id, " + "rp.month, " + "rp.year, " + "rp.fiscal_yr, "
						+ "ps.PERIOD_STATUS_TYPE_CD, " + "co.LEGAL_NM, " + "co.company_id, " + "p.permit_id, "
						+ "p.permit_num, " + "p.permit_type_cd, " + "p.permit_status_type_cd, " + "pp.zero_flag "
						+ "FROM TU_PERMIT p, " + "TU_PERMIT_PERIOD pp, " + "TU_RPT_PERIOD rp, "
						+ "TU_PERIOD_STATUS ps, " + "TU_COMPANY co " + "WHERE p.PERMIT_ID = pp.PERMIT_ID "
						+ "AND rp.PERIOD_ID  = pp.PERIOD_ID " + "AND co.COMPANY_ID = p.COMPANY_ID "
						+ "AND (pp.permit_id = ps.permit_id " + "AND pp.period_id  = ps.period_id) "
						+ "AND rp.FISCAL_YR = :year " + "AND rp.MONTH IN (" + getMonths(quarter) + "))t "
						+ "LEFT OUTER JOIN CIGAR_ONLY_VW cigar "
						+ "ON  (t.PERMIT_ID = cigar.PERMIT_ID AND t.PERIOD_ID  = cigar.PERIOD_ID) "
						+ "where t.FISCAL_YR  = :year " + "AND t.MONTH IN (" + getMonths(quarter) + ") ");
		sql.append(" ORDER BY t.permit_num ASC");
		Query query = getCurrentSession().createSQLQuery(sql.toString());
		query.setParameter("year", year);
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.list();
		Map<Long, AssessmentReportStatus> permits = new LinkedHashMap<>();
		if (results != null) {
			for (Object[] result : results) {
				AssessmentReportStatus reportStatus = new AssessmentReportStatus();
				AssessmentReportStatusMonth quarterMonth = new AssessmentReportStatusMonth();

				quarterMonth.setPeriodId(Long.parseLong(result[0].toString()));
				quarterMonth.setMonth(result[1].toString());
				reportStatus.setYear(Integer.parseInt(result[2].toString()));
				reportStatus.setFiscalYr(Integer.parseInt(result[3].toString()));
				quarterMonth.setPeriodStatusTypeCd(periodStatusValues.get(result[4].toString()));
				reportStatus.setLegalNm(result[5].toString());
				reportStatus.setCompanyId(Long.parseLong(result[6].toString()));
				reportStatus.setPermitId(Long.parseLong(result[7].toString()));
				reportStatus.setPermitNum(result[8].toString());
				reportStatus.setPermitTypeCd(permitTypes.get(result[9].toString()));
				reportStatus.setPermitStatusTypeCd(result[10].toString());
				quarterMonth.setCigarOnly(result[11] != null ? result[11].toString() : "");
				quarterMonth.setZeroFlag(result[12].toString());
				if (permits.containsKey(reportStatus.getPermitId())) {
					AssessmentReportStatus existingReportStatus = permits.get(reportStatus.getPermitId());
					existingReportStatus.getQuarterReports().add(quarterMonth);
				} else {
					reportStatus.getQuarterReports().add(quarterMonth);
					permits.put(reportStatus.getPermitId(), reportStatus);
				}
			}
		}

		return permits;
	}

	private String getMonths(int quarter) {

		String quarterMonths = "";

		switch (quarter) {
		case 2:
			quarterMonths = QUARTER_2_MONTHS;
			break;
		case 3:
			quarterMonths = QUARTER_3_MONTHS;
			break;
		case 4:
			quarterMonths = QUARTER_4_MONTHS;
			break;
		default:
			quarterMonths = QUARTER_1_MONTHS;
			break;
		}
		return quarterMonths;
	}

	/// TODO: Use this function
	@Override
	public List<AttachmentsEntity> findByIds(long periodId, long permitId) {
		Query query = getCurrentSession().createSQLQuery("Select DOCUMENT_ID, DOC_DESC, FORM_TYPES, "
				+ "CREATED_DT, REPORT_PDF FROM TU_DOCUMENT WHERE PERIOD_ID = :periodid AND PERMIT_ID = :permitid");
		query.setParameter("periodid", periodId);
		query.setParameter("permitid", permitId);
		List<Object[]> docData = query.list();
		List<AttachmentsEntity> documents = new ArrayList<>();
		for (Object[] obj : docData) {
			if (obj != null) {
				AttachmentsEntity document = new AttachmentsEntity();
				document.setDocumentId(((BigDecimal) obj[0]).longValue());
				document.setDocDesc(obj[1] != null ? obj[1].toString() : null);
				document.setFormTypes(obj[2] != null ? obj[2].toString() : null);
				document.setCreatedDt((Date) obj[3]);
				documents.add(document);
			}
		}
		return documents;
	}
	
	@Override
	public List<PermitAttachmentsEntity> findByPermitId(long permitId) {
		Query query = getCurrentSession().createSQLQuery("Select DOCUMENT_ID, DOC_DESC, FORM_TYPES, "
				+ "CREATED_DT, CREATED_BY FROM TU_PERMIT_DOCUMENT WHERE PERMIT_ID = :permitid");
		query.setParameter("permitid", permitId);
		List<Object[]> docData = query.list();
		List<PermitAttachmentsEntity> documents = new ArrayList<>();
		for (Object[] obj : docData) {
			if (obj != null) {
				PermitAttachmentsEntity document = new PermitAttachmentsEntity();
				document.setDocumentId(((BigDecimal) obj[0]).longValue());
				document.setDocDesc(obj[1] != null ? obj[1].toString() : null);
				document.setFormTypes(obj[2] != null ? obj[2].toString() : null);
				document.setCreatedDt((Date) obj[3]);
				document.setCreatedBy(obj[4] != null ? obj[4].toString() : null);
				documents.add(document);
			}
		}
		return documents;
	}
	
	@Override
	public List<SupportingDocument> findSupportingDocumentsByAssessmentId(long assessmentId) {
		String sql = "" + "SELECT "
				+ "  d.ASMNT_DOC_ID, d.ASSESSMENT_ID, d.DOCUMENT_NUMBER, d.FILENAME, d.DESCRIPTION, d.DATE_UPLOADED, d.AUTHOR, d.VERSION_NUM, "
				+ "  (SELECT CIGAR_QTR from TU_ASSESSMENT_VERSION v where v.assessment_id = d.assessment_id and v.version_num= d.VERSION_NUM) AS CIGAR_QTR "
				+ "FROM TU_ASSESSMENT_DOCUMENT d " + "WHERE d.ASSESSMENT_ID = :assess";
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("assess", assessmentId);
		List<Object[]> docData = query.list();
		List<SupportingDocument> documents = new ArrayList<>();
		for (Object[] obj : docData) {
			if (obj != null) {
				SupportingDocument document = new SupportingDocument();
				document.setAsmntDocId(((BigDecimal) obj[0]).longValue());
				document.setAssessmentId(((BigDecimal) obj[1]).longValue());
				document.setDocumentNumber(((BigDecimal) obj[2]).longValue());
				document.setFilename(obj[3] != null ? obj[3].toString() : null);
				document.setDescription(obj[4] != null ? obj[4].toString() : null);
				document.setDateUploaded((Date) obj[5]);
				document.setAuthor(obj[6] != null ? obj[6].toString() : null);
				document.setVersionNum(((BigDecimal) obj[7]).intValue());
				document.setCigarQtr(obj[8] != null ? ((BigDecimal) obj[8]).intValue() : 0);
				documents.add(document);
			}
		}
		return documents;
	}
	public List<SupportingDocument> getCigarSupportDocs(long fiscalYr){
		String sql = "SELECT TD.ASMNT_DOC_ID, TD.ASSESSMENT_ID, TD.DOCUMENT_NUMBER, TD.FILENAME, TD.DESCRIPTION, TD.DATE_UPLOADED, TD.AUTHOR, TD.VERSION_NUM, TA.ASSESSMENT_QTR FROM TU_ASSESSMENT TA" 
			         +" INNER JOIN TU_ASSESSMENT_DOCUMENT TD ON TD.ASSESSMENT_ID = TA.ASSESSMENT_ID "
			         +" WHERE TA.ASSESSMENT_YR = :fiscalYr AND TA.ASSESSMENT_TYPE = :assessType"
			         +" ORDER BY 8,5,3";
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("fiscalYr", fiscalYr);
		query.setParameter("assessType", Constants.CIGR_QTRLY);
		List<Object[]> docData = query.list();
		List<SupportingDocument> documents = new ArrayList<>();
		for (Object[] obj : docData) {
			if (obj != null) {
				SupportingDocument document = new SupportingDocument();
				document.setAsmntDocId(obj[0] != null ? ((BigDecimal) obj[0]).longValue():null);
				document.setAssessmentId(obj[1] != null ?((BigDecimal) obj[1]).longValue():null);
				document.setDocumentNumber(obj[2] != null ?((BigDecimal) obj[2]).longValue():null);
				document.setFilename(obj[3] != null ? obj[3].toString() : null);
				document.setDescription(obj[4] != null ? obj[4].toString() : null);
				document.setDateUploaded(obj[5] != null ?(Date) obj[5]:null);
				document.setAuthor(obj[6] != null ? obj[6].toString() : null);
				document.setVersionNum(obj[7] != null ? ((BigDecimal) obj[7]).intValue():0);
				document.setCigarQtr(obj[8] != null ? ((BigDecimal) obj[8]).intValue() : 0);
				documents.add(document);
			}
		}
		return documents;
	}
	@Override
	public List<TrueUpSubDocumentEntity> findDocsByFiscalYr(long fiscalYr) {
		Query query = getCurrentSession()
				.createSQLQuery("Select TRUEUP_SUB_DOC_ID, FISCAL_YR, DOCUMENT_NUMBER, FILENAME, DESCRIPTION, "
						+ "DATE_UPLOADED, AUTHOR, VERSION_NUM FROM TU_TRUEUP_SUBMISSION_DOCUMENT WHERE FISCAL_YR = :year");
		query.setParameter("year", fiscalYr);
		List<Object[]> docData = query.list();
		List<TrueUpSubDocumentEntity> documents = new ArrayList<>();
		for (Object[] obj : docData) {
			if (obj != null) {
				TrueUpSubDocumentEntity document = new TrueUpSubDocumentEntity();
				document.setTrueupSubDocId(((BigDecimal) obj[0]).longValue());
				document.setFiscalYr(((BigDecimal) obj[1]).longValue());
				document.setDocumentNumber(((BigDecimal) obj[2]).longValue());
				document.setFilename(obj[3] != null ? obj[3].toString() : null);
				document.setDescription(obj[4] != null ? obj[4].toString() : null);
				document.setDateUploaded((Date) obj[5]);
				document.setAuthor(obj[6] != null ? obj[6].toString() : null);
				document.setVersionNum(((BigDecimal) obj[7]).intValue());
				documents.add(document);
			}
		}
		return documents;
	}

	@Override
	public List<QtReconRptDetail> getReconReportData(long year, long quarter) {

		String sql = " SELECT LEGAL_NM, COMPANY_ID, PERMIT_NUM, MONTH, FISCAL_YR, RECON_STATUS_TYPE_CD, PERIOD_STATUS_TYPE_CD, PERIOD_ID, PERMIT_ID, PERMIT_TYPE_CD, REPORTED_TOBACCO "
				+ " FROM (SELECT CMPY.LEGAL_NM, CMPY.COMPANY_ID, PERMIT.PERMIT_NUM, RPTP.MONTH, RPTP.FISCAL_YR, P_STATUS.RECON_STATUS_TYPE_CD,  P_STATUS.PERIOD_STATUS_TYPE_CD, PP.PERIOD_ID, PP.PERMIT_ID, PERMIT.PERMIT_TYPE_CD,CIGAR.CIGAR_ONLY, "
				+ " ( CASE WHEN PP.ZERO_FLAG = 'Y' THEN 'Z' WHEN CIGAR.CIGAR_ONLY = 'CGR_'   THEN  'C' END  ) REPORTED_TOBACCO,SUB.FORM_ID,PP.ZERO_FLAG "
				+ " FROM " + " TU_PERMIT_PERIOD    PP "
				+ " INNER JOIN TU_RPT_PERIOD       RPTP ON PP.PERIOD_ID = RPTP.PERIOD_ID "
				+ " INNER JOIN TU_PERIOD_STATUS    P_STATUS ON PP.PERIOD_ID = P_STATUS.PERIOD_ID "
				+ " AND PP.PERMIT_ID = P_STATUS.PERMIT_ID "
				+ " INNER JOIN TU_PERMIT           PERMIT ON PP.PERMIT_ID = PERMIT.PERMIT_ID "
				+ " INNER JOIN TU_COMPANY          CMPY ON PERMIT.COMPANY_ID = CMPY.COMPANY_ID "
				+ " LEFT JOIN TU_SUBMITTED_FORM   SUB ON PP.PERIOD_ID = SUB.PERIOD_ID "
				+ " AND PP.PERMIT_ID = SUB.PERMIT_ID " + " AND SUB.FORM_TYPE_CD = '3852' "
				+ " LEFT JOIN CIGAR_ONLY_VW       CIGAR ON PP.PERMIT_ID = CIGAR.PERMIT_ID "
				+ " AND PP.PERIOD_ID = CIGAR.PERIOD_ID " + "  WHERE " + " RPTP.FISCAL_YR = :yr AND RPTP.QUARTER=:qrt "
				+ " AND P_STATUS.PERIOD_STATUS_TYPE_CD <> 'NSTD' " + " ) "
				+ " WHERE DECODE (FORM_ID,NULL,DECODE(REPORTED_TOBACCO,'Z',1,2),1)=1 "
				+ " ORDER BY RECON_STATUS_TYPE_CD DESC, PERMIT_NUM ASC ";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("yr", year);
		query.setParameter("qrt", quarter);
		List<Object[]> reconRptData = query.list();
		List<QtReconRptDetail> reconRptDetails = new ArrayList<>();
		for (Object[] obj : reconRptData) {
			QtReconRptDetail reconRptDetail = new QtReconRptDetail();
			if (obj[0] != null)
				reconRptDetail.setCompanyName(obj[0].toString());
			if (obj[1] != null)
				reconRptDetail.setCompanyId(((BigDecimal) obj[1]).longValue());
			if (obj[2] != null)
				reconRptDetail.setPermitNum(obj[2].toString());
			if (obj[3] != null)
				reconRptDetail.setMonth(obj[3].toString());
			if (obj[4] != null)
				reconRptDetail.setFiscalYear(obj[4].toString());
			if (obj[5] != null)
				reconRptDetail.setReconStatus(obj[5].toString());
			if (obj[6] != null)
				reconRptDetail.setReportStatus(obj[6].toString());
			if (obj[7] != null)
				reconRptDetail.setPeriodId(((BigDecimal) (obj[7])).longValue());
			if (obj[8] != null)
				reconRptDetail.setPermitId(((BigDecimal) (obj[8])).longValue());
			if (obj[9] != null)
				reconRptDetail.setPermitTypeCd(obj[9].toString());
			if (obj[10] != null)
				reconRptDetail.setReportedTobacco(obj[10].toString());

			reconRptDetails.add(reconRptDetail);
		}

		return reconRptDetails;

	}

	@Override
	public List<QtReconRptDetail> getReconReportDataForFiscalYear(long year) {

		String sql = " SELECT LEGAL_NM, COMPANY_ID, PERMIT_NUM, MONTH, FISCAL_YR, RECON_STATUS_TYPE_CD, PERIOD_STATUS_TYPE_CD, PERIOD_ID, PERMIT_ID, PERMIT_TYPE_CD, REPORTED_TOBACCO "
				+ " FROM (SELECT CMPY.LEGAL_NM, CMPY.COMPANY_ID, PERMIT.PERMIT_NUM, RPTP.MONTH, RPTP.FISCAL_YR, P_STATUS.RECON_STATUS_TYPE_CD,  P_STATUS.PERIOD_STATUS_TYPE_CD, PP.PERIOD_ID, PP.PERMIT_ID, PERMIT.PERMIT_TYPE_CD,CIGAR.CIGAR_ONLY, "
				+ " ( CASE WHEN PP.ZERO_FLAG = 'Y' THEN 'Z' WHEN CIGAR.CIGAR_ONLY = 'CGR_'   THEN  'C' END  ) REPORTED_TOBACCO,SUB.FORM_ID,PP.ZERO_FLAG "
				+ " FROM " + " TU_PERMIT_PERIOD    PP "
				+ " INNER JOIN TU_RPT_PERIOD       RPTP ON PP.PERIOD_ID = RPTP.PERIOD_ID "
				+ " INNER JOIN TU_PERIOD_STATUS    P_STATUS ON PP.PERIOD_ID = P_STATUS.PERIOD_ID "
				+ " AND PP.PERMIT_ID = P_STATUS.PERMIT_ID "
				+ " INNER JOIN TU_PERMIT           PERMIT ON PP.PERMIT_ID = PERMIT.PERMIT_ID "
				+ " INNER JOIN TU_COMPANY          CMPY ON PERMIT.COMPANY_ID = CMPY.COMPANY_ID "
				+ " LEFT JOIN TU_SUBMITTED_FORM   SUB ON PP.PERIOD_ID = SUB.PERIOD_ID "
				+ " AND PP.PERMIT_ID = SUB.PERMIT_ID " + " AND SUB.FORM_TYPE_CD = '3852' "
				+ " LEFT JOIN CIGAR_ONLY_VW       CIGAR ON PP.PERMIT_ID = CIGAR.PERMIT_ID "
				+ " AND PP.PERIOD_ID = CIGAR.PERIOD_ID " + "  WHERE " + " RPTP.FISCAL_YR = :yr "
				+ " AND P_STATUS.PERIOD_STATUS_TYPE_CD <> 'NSTD' " + " ) "
				+ " WHERE DECODE (FORM_ID,NULL,DECODE(REPORTED_TOBACCO,'Z',1,2),1)=1 "
				+ " ORDER BY RECON_STATUS_TYPE_CD DESC, PERMIT_NUM ASC ";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("yr", year);
		List<Object[]> reconRptData = query.list();
		List<QtReconRptDetail> reconRptDetails = new ArrayList<>();
		for (Object[] obj : reconRptData) {
			QtReconRptDetail reconRptDetail = new QtReconRptDetail();
			if (obj[0] != null)
				reconRptDetail.setCompanyName(obj[0].toString());
			if (obj[1] != null)
				reconRptDetail.setCompanyId(((BigDecimal) obj[1]).longValue());
			if (obj[2] != null)
				reconRptDetail.setPermitNum(obj[2].toString());
			if (obj[3] != null)
				reconRptDetail.setMonth(obj[3].toString());
			if (obj[4] != null)
				reconRptDetail.setFiscalYear(obj[4].toString());
			if (obj[5] != null)
				reconRptDetail.setReconStatus(obj[5].toString());
			if (obj[6] != null)
				reconRptDetail.setReportStatus(obj[6].toString());
			if (obj[7] != null)
				reconRptDetail.setPeriodId(((BigDecimal) (obj[7])).longValue());
			if (obj[8] != null)
				reconRptDetail.setPermitId(((BigDecimal) (obj[8])).longValue());
			if (obj[9] != null)
				reconRptDetail.setPermitTypeCd(obj[9].toString());
			if (obj[10] != null)
				reconRptDetail.setReportedTobacco(obj[10].toString());

			reconRptDetails.add(reconRptDetail);
		}

		return reconRptDetails;

	}

	/**
	 * Get list of recon export details.
	 * 
	 * @param year
	 * @param quarter
	 * @return
	 */
	@Override
	public List<ReconExportDetail> getReconExportData(long year, long quarter) {

		String sql = "SELECT a.assessment_yr, a.assessment_qtr, rp.month, c.legal_nm, c.ein, "
				+ "p.permit_num, detail.tobacco_class_id, NVL(detail.removal_qty, 0), NVL(detail.taxes_paid, 0), "
				+ "ps.recon_status_type_cd, ps.recon_status_modified_by, ps.recon_status_modified_dt "
				+ "FROM TU_ASSESSMENT a "
				+ "INNER JOIN TU_RPT_PERIOD rp on (a.assessment_yr = rp.fiscal_yr AND a.assessment_qtr = rp.quarter) "
				+ "INNER JOIN TU_PERMIT_PERIOD pp on rp.period_id = pp.period_id "
				+ "INNER JOIN TU_PERIOD_STATUS ps on (pp.permit_id = ps.permit_id and pp.period_id = ps.period_id) "
				+ "INNER JOIN TU_PERMIT p on pp.permit_id = p.permit_id "
				+ "INNER JOIN TU_COMPANY c on p.company_id = c.company_id " + "LEFT OUTER JOIN "
				+ "(SELECT sf.permit_id, sf.period_id, fd.tobacco_class_id, fd.removal_qty, fd.taxes_paid "
				+ "FROM TU_SUBMITTED_FORM sf, TU_FORM_DETAIL fd, TU_TOBACCO_CLASS tc "
				+ "WHERE sf.form_id = fd.form_id AND fd.tobacco_class_id=tc.tobacco_class_id AND sf.form_type_cd='3852') "
				+ "detail on (detail.permit_id = pp.permit_id and detail.period_id=pp.period_id) "
				+ "WHERE a.assessment_yr = :year " + " AND ps.PERIOD_STATUS_TYPE_CD <> 'NSTD' "
				+ "and a.assessment_qtr = :quarter " + "and a.assessment_type = 'QTRY' "
				+ "ORDER BY ps.recon_status_type_cd, UPPER(c.legal_nm)";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("year", year);
		query.setParameter("quarter", quarter);
		List<Object[]> reconExportData = query.list();
		List<ReconExportDetail> reconExportDetails = new ArrayList<>();
		for (Object[] obj : reconExportData) {
			ReconExportDetail reconExportDetail = new ReconExportDetail();
			if (obj[0] != null)
				reconExportDetail.setYear(((BigDecimal) obj[0]).intValue());
			if (obj[1] != null)
				reconExportDetail.setQuarter((int) ((BigDecimal) obj[1]).longValue());
			if (obj[2] != null)
				reconExportDetail.setMonth(obj[2].toString());
			if (obj[3] != null)
				reconExportDetail.setLegalNm(obj[3].toString());
			if (obj[4] != null)
				reconExportDetail.setEin(obj[4].toString());
			if (obj[5] != null)
				reconExportDetail.setPermitNum(obj[5].toString());
			if (obj[6] != null)
				reconExportDetail.setTobaccoClassId(((BigDecimal) obj[6]).intValue());
			if (obj[7] != null)
				reconExportDetail.setVolumeRemoved(((BigDecimal) obj[7]).doubleValue());
			if (obj[8] != null)
				reconExportDetail.setTax(((BigDecimal) obj[8]).doubleValue());
			if (obj[9] != null)
				reconExportDetail.setReconStatus(obj[9].toString());
			if (obj[10] != null)
				reconExportDetail.setReconUser(obj[10].toString());
			if (obj[11] != null)
				reconExportDetail.setReconDate((Date) obj[11]);
			reconExportDetails.add(reconExportDetail);
		}

		return reconExportDetails;

	}

	@Override
	public List<CBPNewCompanies> getCBPNewCompanies(long fiscalYear) throws SQLException, IOException {

		String sql = "SELECT ein, rtrim (xmlagg(xmlelement( e, company_nm,',').extract('//text()') ).getclobval(), ',') AS company_nm,  "
				+ "  regexp_replace(rtrim(xmlagg(XMLELEMENT(e, tobacco,', ').EXTRACT('//text()') ).GetClobVal(), ', '),'([^,]+)(,[ ]*\1)+','\1') AS tobacco_classes, "
				+ "  CASE "
				+ "  WHEN to_char(min(to_date(entrysummdate,'DD-MON-YY')),'DD-MON-YYYY') is  NOT NULL THEN to_char(min(to_date(entrysummdate,'DD-MON-YY')),'DD-MON-YYYY') "
				+ "  ELSE to_char(min(to_date(entrydate,'DD-MON-YY')),'DD-MON-YYYY') END AS entrysummdate, "
				+ "  SUM(tax) AS total_taxes " + "   FROM ("
				+ "	  SELECT  cbpimp.IMPORTER_EIN as EIN,cbpimp.IMPORTER_NM AS COMPANY_NM,ce.entry_summ_dt  AS entrysummdate,ce.entry_dt as entrydate, NVL(tc.TOBACCO_CLASS_NM, 'Unknown') tobacco, ce.ESTIMATED_TAX AS tax, "
				+ "	 'Importer' as association FROM TU_CBP_IMPORTER cbpimp INNER JOIN TU_CBP_ENTRY  ce "
				+ "	 ON cbpimp.CBP_IMPORTER_ID = ce.CBP_IMPORTER_ID INNER JOIN TU_TOBACCO_CLASS tc "
				+ "  ON ce.TOBACCO_CLASS_ID = tc.TOBACCO_CLASS_ID "
				+ "  WHERE cbpimp.DEFAULT_FLAG='N' AND (cbpimp.CONSIGNEE_EXISTS_FLAG='N' OR (cbpimp.CONSIGNEE_EXISTS_FLAG='Y' AND "
				+ "  cbpimp.ASSOCIATION_TYPE_CD = 'IMPT'))  AND cbpimp.FISCAL_YR=:fiscalYr "
				+ "  AND cbpimp.IMPORTER_EIN not in (select ein from TU_COMPANY) AND "
				+ "  (cbpimp.ASSOCIATION_TYPE_CD not in ('EXCL','UNKN') or cbpimp.ASSOCIATION_TYPE_CD is null "
				+ "  or cbpimp.CONSIGNEE_EIN is NULL)) GROUP BY EIN ";

		Query query = getCurrentSession().createSQLQuery(sql);

		query.setParameter("fiscalYr", fiscalYear);

		List<Object[]> resultSet = query.list();

		List<CBPNewCompanies> cmpnies = new ArrayList<>();

		for (Object[] obj : resultSet) {

			CBPNewCompanies cmpy = new CBPNewCompanies();

			if (obj[0] != null)
				cmpy.setImporterEIN(obj[0].toString());

			if (obj[1] != null) {
				String[] companies = null;
				// try {
				companies = clob2String((Clob) obj[1]).split(",");
				// } catch (SQLException e) {
				// e.printStackTrace();
				// } catch (IOException e) {
				// e.printStackTrace();
				// }
				Set<String> companiesSet = new HashSet<>(Arrays.asList(companies));
				cmpy.setImporterName(StringUtils.join(companiesSet, ","));

			}

			if (obj[2] != null) {
				String[] tobaccoClasses = null;
				// try {
				tobaccoClasses = clob2String((Clob) obj[2]).split(", ");
				// } catch (SQLException e) {
				// e.printStackTrace();
				// } catch (IOException e) {
				// e.printStackTrace();
				// }
				Set<String> tobaccoClassSet = new HashSet<>(Arrays.asList(tobaccoClasses));
				cmpy.setTobaccoName(tobaccoClassSet);
			}
			if (obj[3] != null) {
				cmpy.setentrysummdate(obj[3].toString());
			}
			if (obj[4] != null)
				cmpy.settaxamount(((BigDecimal) obj[4]).doubleValue());

			cmpnies.add(cmpy);
		}

		return cmpnies;

	}

	public List<CBPExportDetail> getCBPExportData(int year, int quarter, String tobaccoClass, String ein) {

		String sql = null;	
		if (tobaccoClass.equals("Roll Your Own")) {
			tobaccoClass = "Roll-Your-Own";
		}
		
		String sqlDateType = null;
		String dateType =null;
		String date1 =null;
		String date2 =null;
		sqlDateType = "SELECT DATE_TYPE from TU_CBP_DT_CONFIG WHERE FISCAL_YEAR= :year AND TOBACCO_CLASS_NM = :tobaccoClass AND EIN = :ein";
		
		Query query1 = getCurrentSession().createSQLQuery(sqlDateType);
		query1.setParameter("year", year);

		query1.setParameter("tobaccoClass", tobaccoClass);
		query1.setParameter("ein", ein);

		Object dateTypeData = query1.uniqueResult();
			
		if (dateTypeData!=null && (dateTypeData.toString()).equals("entryDt")) {
			dateType = "entryDt";
			date1="01-OCT" + Integer.toString(year-1);
			date2="30-SEP" + Integer.toString(year);
		}
		else 
			dateType="entrySummDt";
		
		String sqlCompany = "SELECT COMPANY_ID from TU_COMPANY WHERE EIN = :ein";
		
		Query queryCompany = getCurrentSession().createSQLQuery(sqlCompany);
		queryCompany.setParameter("ein", ein);
		Object companyExists = queryCompany.uniqueResult();
		if(companyExists!= null) {
			if(dateType.equalsIgnoreCase("entryDt"))
				sql = "SELECT cbpentry.ENTRY_NUM,cbpentry.LINE_NUM,cbpentry.ENTRY_SUMM_DT,cbpentry.ENTRY_DT,cbpentry.UOM_CD,cbpentry.ESTIMATED_TAX, "
						+ "cbpentry.HTS_CD,tc.TOBACCO_CLASS_NM,cbpentry.FISCAL_YEAR,cbpentry.FISCAL_QTR,cbpentry.EXCLUDED_FLAG,cbpentry.REALLOCATED_FLAG,cbpdetails.importer_number, "
						+ "cbpdetails.importer_name,cbpdetails.ultimate_consignee_number,cbpentry.qty_removed,cbpentry.entry_type_cd, cbpdetails.ultimate_consignee_name, cbpentry.MISSING_FLAG  FROM  TU_CBP_ENTRY  cbpentry "
						+ "INNER JOIN TU_CBP_IMPORTER cbpimporter ON  cbpentry.FISCAL_YEAR = cbpimporter.fiscal_yr left outer JOIN TU_TOBACCO_CLASS tc on cbpentry.TOBACCO_CLASS_ID = tc.TOBACCO_CLASS_ID "
						+ "LEFT OUTER JOIN TU_COMPANY c ON (cbpimporter.importer_ein = c.ein or cbpimporter.CONSIGNEE_EIN = c.ein ) ,tu_cbp_details_file cbpdetails WHERE cbpimporter.FISCAL_YR= :year "
						+ "and tc.TOBACCO_CLASS_NM = :tobaccoClass AND c.EIN =:ein and cbpentry.cbp_importer_id = cbpimporter.cbp_importer_id  AND  cbpentry.entry_num = cbpdetails.entry_summary_number AND cbpentry.fiscal_year = cbpdetails.fiscal_year AND cbpentry.line_num = cbpdetails.line_number "
						+ "AND ((cbpentry.ENTRY_DT >= :date1 AND cbpentry.ENTRY_DT <= :date2 "
						+ "AND cbpentry.entry_type_cd NOT IN (21,22)) OR cbpentry.REALLOCATED_FLAG ='Y') order by ENTRY_SUMM_DT ASC";
			else
				sql = "SELECT cbpentry.ENTRY_NUM,cbpentry.LINE_NUM,cbpentry.ENTRY_SUMM_DT,cbpentry.ENTRY_DT,cbpentry.UOM_CD,cbpentry.ESTIMATED_TAX, "
						+ "cbpentry.HTS_CD,tc.TOBACCO_CLASS_NM,cbpentry.FISCAL_YEAR,cbpentry.FISCAL_QTR,cbpentry.EXCLUDED_FLAG,cbpentry.REALLOCATED_FLAG,cbpdetails.importer_number, "
						+ "cbpdetails.importer_name,cbpdetails.ultimate_consignee_number,cbpentry.qty_removed,cbpentry.entry_type_cd, cbpdetails.ultimate_consignee_name, cbpentry.MISSING_FLAG FROM  TU_CBP_ENTRY  cbpentry "
						+ "INNER JOIN TU_CBP_IMPORTER cbpimporter ON  cbpentry.FISCAL_YEAR = cbpimporter.fiscal_yr left outer JOIN TU_TOBACCO_CLASS tc on cbpentry.TOBACCO_CLASS_ID = tc.TOBACCO_CLASS_ID "
						+ "LEFT OUTER JOIN TU_COMPANY c ON (cbpimporter.importer_ein = c.ein or cbpimporter.CONSIGNEE_EIN = c.ein ) ,tu_cbp_details_file cbpdetails WHERE cbpimporter.FISCAL_YR= :year "
						+ "and tc.TOBACCO_CLASS_NM = :tobaccoClass AND c.EIN =:ein and cbpentry.cbp_importer_id = cbpimporter.cbp_importer_id AND  cbpentry.entry_num = cbpdetails.entry_summary_number AND cbpentry.fiscal_year = cbpdetails.fiscal_year AND cbpentry.line_num = cbpdetails.line_number "
						+ "AND (cbpentry.entry_type_cd NOT IN (21,22) OR cbpentry.REALLOCATED_FLAG='Y') order by ENTRY_SUMM_DT ASC";
		}else {
			if(dateType.equalsIgnoreCase("entryDt"))
				sql = "SELECT cbpentry.ENTRY_NUM,cbpentry.LINE_NUM,cbpentry.ENTRY_SUMM_DT,cbpentry.ENTRY_DT,cbpentry.UOM_CD,cbpentry.ESTIMATED_TAX, "
						+ "cbpentry.HTS_CD,tc.TOBACCO_CLASS_NM,cbpentry.FISCAL_YEAR,cbpentry.FISCAL_QTR,cbpentry.EXCLUDED_FLAG,cbpentry.REALLOCATED_FLAG,cbpdetails.importer_number, "
						+ "cbpdetails.importer_name,cbpdetails.ultimate_consignee_number,cbpentry.qty_removed,cbpentry.entry_type_cd, null,cbpentry.MISSING_FLAG  FROM  TU_CBP_ENTRY  cbpentry "
						+ "INNER JOIN TU_CBP_IMPORTER cbpimporter ON  cbpentry.FISCAL_YEAR = cbpimporter.fiscal_yr left outer JOIN TU_TOBACCO_CLASS tc on cbpentry.TOBACCO_CLASS_ID = tc.TOBACCO_CLASS_ID ,tu_cbp_details_file cbpdetails "
						+ "WHERE cbpimporter.FISCAL_YR= :year "
						+ "and tc.TOBACCO_CLASS_NM = :tobaccoClass AND cbpimporter.IMPORTER_EIN =:ein and cbpentry.cbp_importer_id = cbpimporter.cbp_importer_id AND  cbpentry.entry_num = cbpdetails.entry_summary_number AND cbpentry.fiscal_year = cbpdetails.fiscal_year AND cbpentry.line_num = cbpdetails.line_number "
						+ "AND ((cbpentry.ENTRY_DT >= :date1 AND cbpentry.ENTRY_DT <= :date2 "
						+ "AND cbpentry.entry_type_cd NOT IN (21,22)) OR cbpentry.REALLOCATED_FLAG ='Y') order by ENTRY_SUMM_DT ASC";
			else
				sql = "SELECT cbpentry.ENTRY_NUM,cbpentry.LINE_NUM,cbpentry.ENTRY_SUMM_DT,cbpentry.ENTRY_DT,cbpentry.UOM_CD,cbpentry.ESTIMATED_TAX, "
						+ "cbpentry.HTS_CD,tc.TOBACCO_CLASS_NM,cbpentry.FISCAL_YEAR,cbpentry.FISCAL_QTR,cbpentry.EXCLUDED_FLAG,cbpentry.REALLOCATED_FLAG,cbpdetails.importer_number, "
						+ "cbpdetails.importer_name,cbpdetails.ultimate_consignee_number,cbpentry.qty_removed,cbpentry.entry_type_cd, null, cbpentry.MISSING_FLAG FROM  TU_CBP_ENTRY  cbpentry "
						+ "INNER JOIN TU_CBP_IMPORTER cbpimporter ON  cbpentry.FISCAL_YEAR = cbpimporter.fiscal_yr left outer JOIN TU_TOBACCO_CLASS tc on cbpentry.TOBACCO_CLASS_ID = tc.TOBACCO_CLASS_ID ,tu_cbp_details_file cbpdetails "
						+ "WHERE cbpimporter.FISCAL_YR= :year "
						+ "and tc.TOBACCO_CLASS_NM = :tobaccoClass AND cbpimporter.IMPORTER_EIN =:ein and cbpentry.cbp_importer_id = cbpimporter.cbp_importer_id AND  cbpentry.entry_num = cbpdetails.entry_summary_number AND cbpentry.fiscal_year = cbpdetails.fiscal_year AND cbpentry.line_num = cbpdetails.line_number "
						+ "AND (cbpentry.entry_type_cd NOT IN (21,22) OR cbpentry.REALLOCATED_FLAG='Y') order by ENTRY_SUMM_DT ASC";
			
		}

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("year", year);

//		if (quarter != 5) {
//			query.setParameter("quarter", quarter);
//		}
//		;

		query.setParameter("tobaccoClass", tobaccoClass);
		query.setParameter("ein", ein);

		if(dateType.equalsIgnoreCase("entryDt")) {
			query.setParameter("date1", date1);
			query.setParameter("date2", date2);
		}
		
		List<Object[]> cbpExportData = query.list();
		List<CBPExportDetail> cbpExportDetails = new ArrayList<>();
		for (Object[] obj : cbpExportData) {
			CBPExportDetail cbpExportDetail = new CBPExportDetail();
			if (obj[0] != null)
				cbpExportDetail.setEntryNo(obj[0].toString() + " ");
			if (obj[1] != null)
				cbpExportDetail.setLineNo(((BigDecimal) obj[1]).intValue());
			if (obj[2] != null) {
				cbpExportDetail.setEntrySummDate((Date) obj[2]);
				cal = Calendar.getInstance();
				cal.setTime((Date) obj[2]);

				if (cal.get(Calendar.YEAR) == year
						|| (cal.get(Calendar.YEAR) == year - 1 && (cal.get(Calendar.MONTH) == 9
								|| cal.get(Calendar.MONTH) == 10 || cal.get(Calendar.MONTH) == 11))) {
					cbpExportDetail.setSecondarydate("N");
				} else {
					cbpExportDetail.setSecondarydate("Y");
				}
			}

			if (obj[3] != null)
				cbpExportDetail.setEntryDate((Date) obj[3]);

			if(dateType.equalsIgnoreCase("entryDt"))
				cbpExportDetail.setSecondarydate("Y");
			else
				cbpExportDetail.setSecondarydate("N");

			if (obj[4] != null)
				cbpExportDetail.setUom1(obj[4].toString());
			if (obj[5] != null)
				cbpExportDetail.setEstimatedTax(((BigDecimal) obj[5]).doubleValue());
			if (obj[6] != null)
				cbpExportDetail.setHtsCd(obj[6].toString() + " ");
			if (obj[7] != null)
				cbpExportDetail.setTobaccoClass(obj[7].toString());
			if (obj[8] != null)
				cbpExportDetail.setFiscalYr(((BigDecimal) obj[8]).longValue());
			if (obj[9] != null)
				cbpExportDetail.setFiscalQtr(obj[9].toString());
			if (obj[10] != null)
				cbpExportDetail.setExcludedFlag(obj[10].toString());
			else if (obj[10] == null) {
				cbpExportDetail.setExcludedFlag("N");
			}
			if (obj[11] != null)
				cbpExportDetail.setReallocatedFlag(obj[11].toString());
			else if (obj[11] == null) {
				cbpExportDetail.setReallocatedFlag("N");
			}
			if (obj[12] != null)
				cbpExportDetail.setImporterEIN(obj[12].toString());
			if (obj[13] != null)
				cbpExportDetail.setImporterName(obj[13].toString());
			if (obj[14] != null)
				cbpExportDetail.setConsigneeEIN(obj[14].toString());
			if (obj[15] != null)
				cbpExportDetail.setQty1(((BigDecimal) obj[15]).doubleValue());
			if (obj[16] != null) {
				cbpExportDetail.setEntryTypeCd(((BigDecimal) obj[16]).intValue());
			}
			if (obj[17] != null) 
				cbpExportDetail.setConsigneeName(obj[17].toString());
			if (obj[18] != null) 
				cbpExportDetail.setMissingFlag(obj[18].toString());

			cbpExportDetails.add(cbpExportDetail);
		}

		return cbpExportDetails;

	}

	/**
	 * Get list of recon export details.
	 * 
	 * @param year
	 * @return
	 */
	@Override
	public List<ReconExportDetail> getReconExportDataForFiscalYear(long year) {

		String sql = "SELECT a.assessment_yr, a.assessment_qtr, rp.month, c.legal_nm, c.ein, "
				+ "p.permit_num, detail.tobacco_class_id, NVL(detail.removal_qty, 0), NVL(detail.taxes_paid, 0), "
				+ "ps.recon_status_type_cd, ps.recon_status_modified_by, ps.recon_status_modified_dt "
				+ "FROM TU_ASSESSMENT a "
				+ "INNER JOIN TU_RPT_PERIOD rp on (a.assessment_yr = rp.fiscal_yr AND a.assessment_qtr = rp.quarter) "
				+ "INNER JOIN TU_PERMIT_PERIOD pp on rp.period_id = pp.period_id "
				+ "INNER JOIN TU_PERIOD_STATUS ps on (pp.permit_id = ps.permit_id and pp.period_id = ps.period_id) "
				+ "INNER JOIN TU_PERMIT p on pp.permit_id = p.permit_id "
				+ "INNER JOIN TU_COMPANY c on p.company_id = c.company_id " + "LEFT OUTER JOIN "
				+ "(SELECT sf.permit_id, sf.period_id, fd.tobacco_class_id, fd.removal_qty, fd.taxes_paid "
				+ "FROM TU_SUBMITTED_FORM sf, TU_FORM_DETAIL fd, TU_TOBACCO_CLASS tc "
				+ "WHERE sf.form_id = fd.form_id AND fd.tobacco_class_id=tc.tobacco_class_id AND sf.form_type_cd='3852') "
				+ "detail on (detail.permit_id = pp.permit_id and detail.period_id=pp.period_id) "
				+ "WHERE a.assessment_yr = :year  " + "AND ps.PERIOD_STATUS_TYPE_CD <> 'NSTD' "
				+ "AND a.assessment_type = 'ANNU' " + "ORDER BY ps.recon_status_type_cd, UPPER(c.legal_nm)";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("year", year);
		List<Object[]> reconExportData = query.list();
		List<ReconExportDetail> reconExportDetails = new ArrayList<>();
		for (Object[] obj : reconExportData) {
			ReconExportDetail reconExportDetail = new ReconExportDetail();
			if (obj[0] != null)
				reconExportDetail.setYear(((BigDecimal) obj[0]).intValue());
			if (obj[1] != null)
				reconExportDetail.setQuarter((int) ((BigDecimal) obj[1]).longValue());
			if (obj[2] != null)
				reconExportDetail.setMonth(obj[2].toString());
			if (obj[3] != null)
				reconExportDetail.setLegalNm(obj[3].toString());
			if (obj[4] != null)
				reconExportDetail.setEin(obj[4].toString());
			if (obj[5] != null)
				reconExportDetail.setPermitNum(obj[5].toString());
			if (obj[6] != null)
				reconExportDetail.setTobaccoClassId(((BigDecimal) obj[6]).intValue());
			if (obj[7] != null)
				reconExportDetail.setVolumeRemoved(((BigDecimal) obj[7]).doubleValue());
			if (obj[8] != null)
				reconExportDetail.setTax(((BigDecimal) obj[8]).doubleValue());
			if (obj[9] != null)
				reconExportDetail.setReconStatus(obj[9].toString());
			if (obj[10] != null)
				reconExportDetail.setReconUser(obj[10].toString());
			if (obj[11] != null)
				reconExportDetail.setReconDate((Date) obj[11]);
			reconExportDetails.add(reconExportDetail);
		}

		return reconExportDetails;

	}

	@Override
	public List<CBPImporter> getCompanyComparisonBucket(Long fiscalYear) {

		String sql = "SELECT * FROM ANN_CMPY_ASSOC_CRR_NEW_DEL_VW DELTAVW WHERE DELTAVW.FISCAL_YR =:fiscalYr";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("fiscalYr", fiscalYear);
		List<Object[]> resultSet = query.list();

		Map<Long, CBPImporter> CBPImporterMap = new HashMap<>();

		for (Object[] obj : resultSet) {

			Long cbpimporterId = 0L;
			CBPImporter cbpImporter = null;

			// Get CBP Importer Id
			if (obj[3] != null)
				cbpimporterId = ((BigDecimal) obj[3]).longValue();

			if (!CBPImporterMap.containsKey(cbpimporterId)) {
				cbpImporter = new CBPImporter();
				cbpImporter.setCbpImporterId(cbpimporterId);

				Map<String, CBPImporterTaxDeltas> consigneeDeltasMap = new HashMap<>();
				consigneeDeltasMap.put("1", new CBPImporterTaxDeltas());
				consigneeDeltasMap.put("2", new CBPImporterTaxDeltas());
				consigneeDeltasMap.put("3", new CBPImporterTaxDeltas());
				consigneeDeltasMap.put("4", new CBPImporterTaxDeltas());

				cbpImporter.setConsigneeDeltas(consigneeDeltasMap);

				Map<String, CBPImporterTaxDeltas> importerDeltasMap = new HashMap<>();

				importerDeltasMap.put("1", new CBPImporterTaxDeltas());
				importerDeltasMap.put("2", new CBPImporterTaxDeltas());
				importerDeltasMap.put("3", new CBPImporterTaxDeltas());
				importerDeltasMap.put("4", new CBPImporterTaxDeltas());

				cbpImporter.setImporterDeltas(importerDeltasMap);

				CBPImporterMap.put(cbpimporterId, cbpImporter);
			} else
				cbpImporter = CBPImporterMap.get(cbpimporterId);

			String tufaLegalName = "";
			String importerName = "";
			String quarter = "";
			String consgInTufa = "";
			String impInTufa = "";
			String companyId = "";

			if (obj[0] != null)
				companyId = obj[0].toString();

			if (obj[1] != null)
				tufaLegalName = obj[1].toString();

			if (obj[2] != null) {
				String associationTypeCd = obj[2].toString();
				Boolean providedAnswer = true;

				cbpImporter.setAssociationTypeCd(associationTypeCd);
				cbpImporter.setProvidedAnswer(providedAnswer);
			}

			if (obj[4] != null) {
				importerName = obj[4].toString();
				cbpImporter.setImporterNm(importerName);
			}

			if (obj[5] != null) {
				impInTufa = obj[5].toString();
				cbpImporter.setImpInTufa(impInTufa);
			}

			if (obj[6] != null) {
				consgInTufa = obj[6].toString();
				cbpImporter.setConsgInTufa(consgInTufa);
			}

			if (obj[8] != null)
				quarter = obj[8].toString();

			String consigneeName = "";
			if (obj[32] != null) {
				consigneeName = obj[32].toString();
				cbpImporter.setConsigneeNm(consigneeName);
			}

			CBPImporterTaxDeltas cngTaxDelta = cbpImporter.getConsigneeDeltas().get(quarter);
			cngTaxDelta.setQuarter(quarter.toString());
			this.populateConsTTDeltaGrid(obj, cbpImporter, cngTaxDelta);

			CBPImporterTaxDeltas impTaxDelta = cbpImporter.getImporterDeltas().get(quarter);
			impTaxDelta.setQuarter(quarter.toString());
			this.populateImpTTDeltaGrid(obj, cbpImporter, impTaxDelta);

		}

		/***
		 * Remove cbpImporter if current deltas and new deltas are null.
		 * Additionally set New Delta as negation of Ingested tax in
		 * CBPImporterTaxDeltas if Current Delta and New Delta is Null. This is
		 * a fix to address the case where Monthly Report does not exist in a
		 * quarter that results in null values from DB query instead of one
		 * sided ingested data.
		 */

		for (Iterator<Map.Entry<Long, CBPImporter>> it = CBPImporterMap.entrySet().iterator(); it.hasNext();) {
			Map.Entry<Long, CBPImporter> entry = it.next();
			CBPImporter cbpimp = entry.getValue();

			Map<String, CBPImporterTaxDeltas> consigneeDeltaMap = cbpimp.getConsigneeDeltas();
			Map<String, CBPImporterTaxDeltas> importerDeltaMap = cbpimp.getImporterDeltas();

			Collection<CBPImporterTaxDeltas> consDeltas = consigneeDeltaMap.values();
			Collection<CBPImporterTaxDeltas> impDeltas = importerDeltaMap.values();

			boolean consDeltasEmpty = true;
			for (CBPImporterTaxDeltas delta : consDeltas) {
				if (!delta.checkCurrentDeltaEmpty() || !delta.checkNewDeltaEmpty()) {
					consDeltasEmpty = false;
					break;
				}
			}

			boolean impDeltasEmpty = true;
			for (CBPImporterTaxDeltas delta : impDeltas) {
				if (!delta.checkCurrentDeltaEmpty() || !delta.checkNewDeltaEmpty()) {
					impDeltasEmpty = false;
					break;
				}
			}

			if (consDeltasEmpty && impDeltasEmpty) {
				it.remove();
				continue;
			}

			// run loop for each quarter
			if (!"CONS".equalsIgnoreCase(cbpimp.getAssociationTypeCd())) {
				for (String qtr : consigneeDeltaMap.keySet()) {

					CBPImporterTaxDeltas consignDelta = consigneeDeltaMap.get(qtr);
					CBPImporterTaxDeltas impDelta = importerDeltaMap.get(qtr);
					// Set ingested data for Chew
					if (consignDelta.getDeltaChew() == null && consignDelta.getTaxesChewTufa() == null) {
						Double ingestedTax = this.getIngestedTaxFromCurrentAndNewDelta(impDelta.getTaxesChewTufa(),
								impDelta.getDeltaChew());
						consignDelta.setDeltaChew(ingestedTax);
					}
					if (consignDelta.getDeltaSnuff() == null && consignDelta.getTaxesSnuffTufa() == null) {
						Double ingestedTax = this.getIngestedTaxFromCurrentAndNewDelta(impDelta.getTaxesSnuffTufa(),
								impDelta.getDeltaSnuff());
						consignDelta.setDeltaSnuff(ingestedTax);
					}
					if (consignDelta.getDeltaRYO() == null && consignDelta.getTaxesRYOTufa() == null) {
						Double ingestedTax = this.getIngestedTaxFromCurrentAndNewDelta(impDelta.getTaxesRYOTufa(),
								impDelta.getDeltaRYO());
						consignDelta.setDeltaRYO(ingestedTax);
					}
					if (consignDelta.getDeltaPipe() == null && consignDelta.getTaxesPipeTufa() == null) {
						Double ingestedTax = this.getIngestedTaxFromCurrentAndNewDelta(impDelta.getTaxesPipeTufa(),
								impDelta.getDeltaPipe());
						consignDelta.setDeltaPipe(ingestedTax);
					}
					if (consignDelta.getDeltaCigarettes() == null && consignDelta.getTaxesCigsTufa() == null) {
						Double ingestedTax = this.getIngestedTaxFromCurrentAndNewDelta(impDelta.getTaxesCigsTufa(),
								impDelta.getDeltaCigarettes());
						consignDelta.setDeltaCigarettes(ingestedTax);
					}
					if (consignDelta.getDeltaCigars() == null && consignDelta.getTaxesCigarsTufa() == null) {
						Double ingestedTax = this.getIngestedTaxFromCurrentAndNewDelta(impDelta.getTaxesCigarsTufa(),
								impDelta.getDeltaCigars());
						consignDelta.setDeltaCigars(ingestedTax);
					}

				}

			}
		}

		List<CBPImporter> bucket = new ArrayList<>(CBPImporterMap.values());

		return bucket;

	}

	/************************************/

	private Double getIngestedTaxFromCurrentAndNewDelta(Double currentDelta, Double newDelta) {

		if (currentDelta == null && newDelta == null)
			return null;
		else if (currentDelta == null && newDelta != null)
			return newDelta;
		else if (currentDelta != null && newDelta != null)
			return currentDelta - newDelta;
		else if (currentDelta != null && newDelta == null)
			return currentDelta;

		return null;

	}

	private void populateConsTTDeltaGrid(Object[] obj, CBPImporter cbpImporter, CBPImporterTaxDeltas delta) {

		String consMatch = "N";
		if (obj[24] != null)
			consMatch = obj[24].toString();

		if (cbpImporter.getAssociationTypeCd() == null || cbpImporter.getAssociationTypeCd().equalsIgnoreCase("IMPT")
				|| cbpImporter.getAssociationTypeCd().equalsIgnoreCase("EXCL")) {
			// Do not switch Current Delta with New Delta

			if (cbpImporter.getConsgInTufa().equalsIgnoreCase("Y") && consMatch.equalsIgnoreCase("Y"))
				// populate current and new delta
				this.currentAndNewDelta(obj, delta);
			else if (cbpImporter.getConsgInTufa().equalsIgnoreCase("N"))
				// populate only ingested taxes
				this.newDeltaCBPImporterNotExists(obj, delta);

		} else if (cbpImporter.getAssociationTypeCd() != null) {

			// Switch Current Delta with New Delta

			if (cbpImporter.getAssociationTypeCd().equalsIgnoreCase("CONS")) {
				if (cbpImporter.getConsgInTufa().equalsIgnoreCase("Y") && consMatch.equalsIgnoreCase("Y"))
					// populate current delta that has changed after association
					this.currentDelta(obj, delta);
				if (cbpImporter.getConsgInTufa().equalsIgnoreCase("N"))
					// populate only ingested taxes
					this.newAsCurrentCBPImporterNotExists(obj, delta);
			}
		}

	}

	private void populateImpTTDeltaGrid(Object[] obj, CBPImporter cbpImporter, CBPImporterTaxDeltas delta) {

		String impMatch = "N";
		if (obj[23] != null)
			impMatch = obj[23].toString();

		if (cbpImporter.getAssociationTypeCd() == null || cbpImporter.getAssociationTypeCd().equalsIgnoreCase("CONS")
				|| cbpImporter.getAssociationTypeCd().equalsIgnoreCase("EXCL")) {
			// Do not switch Current Delta with New Delta
			if (cbpImporter.getImpInTufa().equalsIgnoreCase("N") && impMatch.equalsIgnoreCase("N"))
				// populate only ingested taxes
				this.newDeltaCBPImporterNotExists(obj, delta);
			else if (impMatch.equalsIgnoreCase("Y"))
				// populate current and new delta
				this.currentAndNewDelta(obj, delta);

		} else if (cbpImporter.getAssociationTypeCd() != null) {

			// Switch Current Delta with New Delta
			if (cbpImporter.getAssociationTypeCd().equalsIgnoreCase("IMPT")) {
				if (cbpImporter.getImpInTufa().equalsIgnoreCase("N") && impMatch.equalsIgnoreCase("N"))
					// populate only ingested taxes
					this.newAsCurrentCBPImporterNotExists(obj, delta);
				else if (impMatch.equalsIgnoreCase("Y"))
					// populate current delta that has changed after association
					this.currentDelta(obj, delta);
			}
		}

	}

	private boolean deltasEqual(Object o1, Object o2) {

		if (o1 == null && o2 != null)
			return false;
		if (o1 != null && o2 == null)
			return true;
		if (o1 == null && o2 == null)
			return true;

		double value1 = ((BigDecimal) o1).doubleValue();
		double value2 = ((BigDecimal) o2).doubleValue();

		return value1 == value2;
	}

	private void currentAndNewDelta(Object[] obj, CBPImporterTaxDeltas delta) {
		currentDelta(obj, delta);
		newDelta(obj, delta);
	}

	private void currentDelta(Object[] obj, CBPImporterTaxDeltas delta) {

		/************** CURRENT DELTA **********************************/

		if (obj[9] != null && !deltasEqual(obj[9], obj[16]))
			delta.setTaxesCigarsTufa(((BigDecimal) obj[9]).doubleValue());

		if (obj[10] != null && !deltasEqual(obj[10], obj[17]))
			delta.setTaxesCigsTufa(((BigDecimal) obj[10]).doubleValue());

		if (obj[11] != null && !deltasEqual(obj[11], obj[18]))
			delta.setTaxesChewTufa(((BigDecimal) obj[11]).doubleValue());

		if (obj[12] != null && !deltasEqual(obj[12], obj[19]))
			delta.setTaxesSnuffTufa(((BigDecimal) obj[12]).doubleValue());

		if (obj[13] != null && !deltasEqual(obj[13], obj[20]))
			delta.setTaxesPipeTufa(((BigDecimal) obj[13]).doubleValue());

		if (obj[14] != null && !deltasEqual(obj[14], obj[21]))
			delta.setTaxesRYOTufa(((BigDecimal) obj[14]).doubleValue());

	}

	private void newDelta(Object[] obj, CBPImporterTaxDeltas delta) {

		/*************************** NEW DELTA ********************************/

		if (obj[16] != null && !deltasEqual(obj[9], obj[16]))
			delta.setDeltaCigars(((BigDecimal) obj[16]).doubleValue());

		if (obj[17] != null && !deltasEqual(obj[10], obj[17]))
			delta.setDeltaCigarettes(((BigDecimal) obj[17]).doubleValue());

		if (obj[18] != null && !deltasEqual(obj[11], obj[18]))
			delta.setDeltaChew(((BigDecimal) obj[18]).doubleValue());

		if (obj[19] != null && !deltasEqual(obj[12], obj[19]))
			delta.setDeltaSnuff(((BigDecimal) obj[19]).doubleValue());

		if (obj[20] != null && !deltasEqual(obj[13], obj[20]))
			delta.setDeltaPipe(((BigDecimal) obj[20]).doubleValue());

		if (obj[21] != null && !deltasEqual(obj[14], obj[21]))
			delta.setDeltaRYO(((BigDecimal) obj[21]).doubleValue());

		/*** Unknown Tobacco Type **/
		if (obj[22] != null)
			delta.setDeltaUnknown(((BigDecimal) obj[22]).doubleValue());
	}

	private void newDeltaCBPImporterNotExists(Object[] obj, CBPImporterTaxDeltas delta) {

		if (obj[25] != null)
			delta.setDeltaCigars(((BigDecimal) obj[25]).doubleValue());

		if (obj[26] != null)
			delta.setDeltaCigarettes(((BigDecimal) obj[26]).doubleValue());

		if (obj[27] != null)
			delta.setDeltaChew(((BigDecimal) obj[27]).doubleValue());

		if (obj[28] != null)
			delta.setDeltaSnuff(((BigDecimal) obj[28]).doubleValue());

		if (obj[29] != null)
			delta.setDeltaPipe(((BigDecimal) obj[29]).doubleValue());

		if (obj[30] != null)
			delta.setDeltaRYO(((BigDecimal) obj[30]).doubleValue());

		if (obj[31] != null)
			delta.setDeltaUnknown(((BigDecimal) obj[31]).doubleValue());

	}

	private void newAsCurrentCBPImporterNotExists(Object[] obj, CBPImporterTaxDeltas delta) {

		if (obj[25] != null)
			delta.setTaxesCigarsTufa(((BigDecimal) obj[25]).doubleValue());

		if (obj[26] != null)
			delta.setTaxesCigsTufa(((BigDecimal) obj[26]).doubleValue());

		if (obj[27] != null)
			delta.setTaxesChewTufa(((BigDecimal) obj[27]).doubleValue());

		if (obj[28] != null)
			delta.setTaxesSnuffTufa(((BigDecimal) obj[28]).doubleValue());

		if (obj[29] != null)
			delta.setTaxesPipeTufa(((BigDecimal) obj[29]).doubleValue());

		if (obj[30] != null)
			delta.setTaxesRYOTufa(((BigDecimal) obj[30]).doubleValue());

		if (obj[31] != null)
			delta.setTaxesUnknownTufa(((BigDecimal) obj[31]).doubleValue());

	}

	/**
	 * Get list of recon export details.
	 * 
	 * @param year
	 * @return
	 */
	@Override
	public List<ReconExportDetail> getCigarReconExportData(long year) {

		String sql = "" + "SELECT a.assessment_yr, a.assessment_qtr, rp.month, c.legal_nm, c.ein, "
				+ "					p.permit_num, detail.tobacco_class_id, NVL(detail.removal_qty, 0), NVL(detail.taxes_paid, 0), "
				+ "					ps.recon_status_type_cd, ps.recon_status_modified_by, ps.recon_status_modified_dt "
				+ "					FROM TU_ASSESSMENT a "
				+ "					INNER JOIN TU_RPT_PERIOD rp on (a.assessment_yr = rp.fiscal_yr AND a.assessment_qtr = rp.quarter) "
				+ "					INNER JOIN TU_PERMIT_PERIOD pp on rp.period_id = pp.period_id "
				+ "					INNER JOIN TU_PERIOD_STATUS ps on (pp.permit_id = ps.permit_id and pp.period_id = ps.period_id) "
				+ "					INNER JOIN TU_PERMIT p on pp.permit_id = p.permit_id "
				+ "					INNER JOIN TU_COMPANY c on p.company_id = c.company_id "
				+ "					LEFT OUTER JOIN "
				+ "					(SELECT sf.permit_id, sf.period_id, fd.tobacco_class_id, fd.removal_qty, fd.taxes_paid "
				+ "					FROM TU_SUBMITTED_FORM sf, TU_FORM_DETAIL fd, TU_TOBACCO_CLASS tc "
				+ "					WHERE sf.form_id = fd.form_id AND fd.tobacco_class_id = tc.tobacco_class_id AND tc.TOBACCO_CLASS_NM = 'Cigars' AND sf.form_type_cd='3852') "
				+ "					detail on (detail.permit_id = pp.permit_id and detail.period_id = pp.period_id) "
				+ "          		WHERE a.assessment_yr = :year  AND a.assessment_type = 'CIQT' and detail.tobacco_class_id IS NOT NULL "
				+ "					ORDER BY ps.recon_status_type_cd, UPPER(c.legal_nm)";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("year", year);
		List<Object[]> reconExportData = query.list();
		List<ReconExportDetail> reconExportDetails = new ArrayList<>();
		for (Object[] obj : reconExportData) {
			ReconExportDetail reconExportDetail = new ReconExportDetail();
			if (obj[0] != null)
				reconExportDetail.setYear(((BigDecimal) obj[0]).intValue());
			if (obj[1] != null)
				reconExportDetail.setQuarter((int) ((BigDecimal) obj[1]).longValue());
			if (obj[2] != null)
				reconExportDetail.setMonth(obj[2].toString());
			if (obj[3] != null)
				reconExportDetail.setLegalNm(obj[3].toString());
			if (obj[4] != null)
				reconExportDetail.setEin(obj[4].toString());
			if (obj[5] != null)
				reconExportDetail.setPermitNum(obj[5].toString());
			if (obj[6] != null)
				reconExportDetail.setTobaccoClassId(((BigDecimal) obj[6]).intValue());
			if (obj[7] != null)
				reconExportDetail.setVolumeRemoved(((BigDecimal) obj[7]).doubleValue());
			if (obj[8] != null)
				reconExportDetail.setTax(((BigDecimal) obj[8]).doubleValue());
			if (obj[9] != null)
				reconExportDetail.setReconStatus(obj[9].toString());
			if (obj[10] != null)
				reconExportDetail.setReconUser(obj[10].toString());
			if (obj[11] != null)
				reconExportDetail.setReconDate((Date) obj[11]);
			reconExportDetails.add(reconExportDetail);
		}

		return reconExportDetails;

	}

	@Override
	public List<CigarAssessmentRptDetail> getCigarAssessmentReportStatusData(long year) {

		Map<String, String> permitTypes = referenceCodeEntityManager.getValues("PERMIT_TYPE");
		Map<String, String> reconStatusTypes = referenceCodeEntityManager.getValues("RECON_STATUS_TYPE");
		Map<String, String> periodStatusTypes = referenceCodeEntityManager.getValues("PERIOD_STATUS_TYPE");

		String sql = " SELECT  cmpy.LEGAL_NM, cmpy.COMPANY_ID, permit.PERMIT_NUM, rptp.MONTH, rptp.FISCAL_YR, p_status.RECON_STATUS_TYPE_CD, p_status.PERIOD_STATUS_TYPE_CD, pp.PERIOD_ID, "
				+ "                 pp.PERMIT_ID,permit.PERMIT_TYPE_CD,(CASE  "
				+ "                        WHEN COUNT(formDetail1.FORM_ID)=1 THEN 'C' "
				+ "                        END ) REPORTED_TOBACCO " + "              FROM TU_PERMIT_PERIOD pp  "
				+ "              INNER JOIN TU_RPT_PERIOD rptp  "
				+ "              ON  pp.PERIOD_ID = rptp.PERIOD_ID          "
				+ "              INNER JOIN TU_PERIOD_STATUS p_status  "
				+ "              ON  pp.PERIOD_ID = p_status.PERIOD_ID AND pp.PERMIT_ID = p_status.PERMIT_ID "
				+ "    				  INNER JOIN TU_PERMIT permit  "
				+ "              ON  pp.PERMIT_ID = permit.PERMIT_ID       "
				+ "    				  INNER JOIN TU_COMPANY cmpy  "
				+ "              ON  permit.COMPANY_ID = cmpy.COMPANY_ID   "
				+ "              INNER JOIN TU_COMPANY cmpy  "
				+ "              ON  permit.COMPANY_ID = cmpy.COMPANY_ID     "
				+ "    				  LEFT JOIN  TU_SUBMITTED_FORM forms  "
				+ "              ON forms.PERIOD_ID = pp.PERIOD_ID AND forms.PERMIT_ID = pp.PERMIT_ID AND forms.FORM_TYPE_CD= :formTypeCd  "
				+ "              INNER JOIN TU_FORM_DETAIL formDetail1 "
				+ "              ON forms.FORM_ID = formDetail1.FORM_ID  " + "              WHERE EXISTS ( "
				+ "                    SELECT * FROM TU_FORM_DETAIL formDetail2, TU_TOBACCO_CLASS ttype  "
				+ "                    WHERE formDetail2.form_id = formDetail1.form_id  AND ttype.TOBACCO_CLASS_ID=formDetail2.TOBACCO_CLASS_ID  AND ttype.TOBACCO_CLASS_NM='Cigars' "
				+ "              ) AND rptp.FISCAL_YR= :fiscalYr  "
				+ "              GROUP BY formDetail1.FORM_ID,cmpy.LEGAL_NM, cmpy.COMPANY_ID, permit.PERMIT_NUM, rptp.MONTH, rptp.FISCAL_YR, p_status.RECON_STATUS_TYPE_CD, p_status.PERIOD_STATUS_TYPE_CD, pp.PERIOD_ID, "
				+ "                      pp.PERMIT_ID, permit.PERMIT_TYPE_CD HAVING COUNT(*)>0";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("formTypeCd", "3852");
		query.setParameter("fiscalYr", year);
		List<Object[]> cigarAssessRptData = query.list();
		List<CigarAssessmentRptDetail> cigarAssessRptDetails = new ArrayList<>();

		for (Object[] obj : cigarAssessRptData) {

			CigarAssessmentRptDetail cigarAssessRptDetail = new CigarAssessmentRptDetail();
			if (obj[0] != null)
				cigarAssessRptDetail.setCompanyName(obj[0].toString());
			if (obj[1] != null)
				cigarAssessRptDetail.setCompanyId(((BigDecimal) obj[1]).longValue());
			if (obj[2] != null)
				cigarAssessRptDetail.setPermitNum(obj[2].toString());
			if (obj[3] != null)
				cigarAssessRptDetail.setMonth(obj[3].toString());
			cigarAssessRptDetail.setSortMonth(Constants.MMMToMMNumber.get(cigarAssessRptDetail.getMonth()));
			if (obj[4] != null)
				cigarAssessRptDetail.setFiscalYear(obj[4].toString());
			if (obj[5] != null)
				cigarAssessRptDetail.setReconStatus(reconStatusTypes.get(obj[5].toString()));
			if (obj[6] != null)
				cigarAssessRptDetail.setReportStatus(periodStatusTypes.get(obj[6].toString()));
			if (obj[7] != null)
				cigarAssessRptDetail.setPeriodId(((BigDecimal) (obj[7])).longValue());
			if (obj[8] != null)
				cigarAssessRptDetail.setPermitId(((BigDecimal) (obj[8])).longValue());
			if (obj[9] != null)
				cigarAssessRptDetail.setPermitTypeCd(permitTypes.get(obj[9].toString()));
			if (obj[10] != null)
				cigarAssessRptDetail.setReportedTobacco(obj[10].toString());

			cigarAssessRptDetails.add(cigarAssessRptDetail);
		}

		return cigarAssessRptDetails;

	}

	private boolean IsAtleastOneTobaccoTypeSelected(TrueUpCompareSearchCriteria csc) {
		if (csc.getSelTobaccoTypes().isShowCigarettes() || csc.getSelTobaccoTypes().isShowCigars()
				|| csc.getSelTobaccoTypes().isShowChews() || csc.getSelTobaccoTypes().isShowSnuffs()
				|| csc.getSelTobaccoTypes().isShowPipes() || csc.getSelTobaccoTypes().isShowRollYourOwn()) {
			return true;
		}
		return false;
	}

	// -- Combine Quarters for Cigar
	// -- Combine for Manufacturer
	// -- Combine for Manufacturer
	// -- Combine for Manufacturer
	@Override
	public List<TrueUpComparisonResults> getComparisonResults(long fiscalYear, TrueUpCompareSearchCriteria csc) {

		logger.debug("getComparisonResults() :: entering");
		logger.debug("getComparisonResults() :: building SQL");
		boolean addStatusTypeParameter = false;
		String status = "";

		// Building SQL
		StringBuilder manusql = new StringBuilder(
				" SELECT  LEGAL_NM, CLASSNAME, QUARTERX, DELTA, PERMIT_TYPE_CD, COMPANY_ID, ACCEPTANCE_FLAG, IN_PROGRESS_FLAG, EIN, PERMIT_NUM, DELTAEXCISETAX_FLAG  "
						+ "  FROM ANN_TRUEUP_COMPARERESULTS_MANU  cmpmanu " + "  WHERE cmpmanu.fiscal_yr = :fiscalYr ");

		if (testCriterion(csc.getStatusTypeFilter())) {

			manusql.append(CONST_AND);

			// status = "'" + StringUtils.join(csc.getStatusTypeFilter(), "','")
			// + "'";

			manusql.append(" ( UPPER(nvl(acceptance_flag,'NOACT')) IN (:status)");
			addStatusTypeParameter = true;

			if (csc.getStatusTypeFilter().contains("INPROGRESS")) {
				manusql.append(CONST_OR);
				manusql.append(" upper(IN_PROGRESS_FLAG) = 'INPROGRESS'");

			}
			manusql.append(" ) ");
		}

		if (testCriterion(csc.getCompanyFilter())) {
			manusql.append(CONST_AND);
			manusql.append(" UPPER(LEGAL_NM) LIKE UPPER(:companyFilter) ");
		}
		if (testCriterion(csc.getEinFilter())) {
			manusql.append(CONST_AND);
			manusql.append(" UPPER(EIN) LIKE UPPER(:einFilter) ");
		}
		if (testCriterion(csc.getpermitNumFilter())) {
			manusql.append(CONST_AND);
			manusql.append(" UPPER(PERMIT_NUM) LIKE UPPER(:permitNumFilter) ");
		}
		if (testCriterion(csc.getTobaccoClassFilter())) {
			manusql.append(CONST_AND);
			manusql.append(" UPPER(CLASSNAME) LIKE UPPER(:tobaccoClassFilter) ");
		}
		if (testCriterion(csc.getQuarterFilter())) {
			manusql.append(CONST_AND);
			manusql.append(" QUARTERX LIKE :quarterFilter ");
		}
		if (testCriterion(csc.getDeltaTaxFilter())) {
			manusql.append(CONST_AND);
			manusql.append(" DELTA LIKE :deltaFilter ");
		}

		StringBuilder impsql = new StringBuilder(
				"  SELECT LEGAL_NM, CLASSNAME, QUARTERX, DECODE(deltaexcisetax_flag, 'Review', 0.0, delta) as delta, PERMIT_TYPE_CD, COMPANY_ID, ACCEPTANCE_FLAG, IN_PROGRESS_FLAG, EIN, PERMIT_NUM, DELTAEXCISETAX_FLAG "
						+ "  FROM ANN_TRUEUP_COMPARERESULTS_IMP  cmpimp " + "  WHERE cmpimp.fiscal_yr = :fiscalYr ");

		if (testCriterion(csc.getStatusTypeFilter())) {
			impsql.append(CONST_AND);

			impsql.append(" ( UPPER(nvl(acceptance_flag,'NOACT')) IN (:status)");
			addStatusTypeParameter = true;

			if (csc.getStatusTypeFilter().contains("INPROGRESS")) {
				impsql.append(CONST_OR);
				impsql.append(" upper(IN_PROGRESS_FLAG) = 'INPROGRESS'");

			}

			impsql.append(" ) ");
		}

		if (testCriterion(csc.getCompanyFilter())) {
			impsql.append(CONST_AND);
			impsql.append(" UPPER(LEGAL_NM) LIKE UPPER(:companyFilter) ");
		}
		if (testCriterion(csc.getEinFilter())) {
			impsql.append(CONST_AND);
			impsql.append(" UPPER(EIN) LIKE UPPER(:einFilter) ");
		}

		if (testCriterion(csc.getpermitNumFilter())) {
			impsql.append(CONST_AND);
			impsql.append(" UPPER(PERMIT_NUM) LIKE UPPER(:permitNumFilter) ");
		}
		if (testCriterion(csc.getTobaccoClassFilter())) {
			impsql.append(CONST_AND);
			impsql.append(" UPPER(CLASSNAME) LIKE UPPER(:tobaccoClassFilter) ");
		}
		if (testCriterion(csc.getQuarterFilter())) {
			impsql.append(CONST_AND);
			impsql.append(" QUARTERX LIKE :quarterFilter ");
		}
		if (testCriterion(csc.getDeltaTaxFilter())) {
			String deltaTaxNumber = csc.getDeltaTaxFilter().replaceAll("[^0-9.]", "");
			deltaTaxNumber = deltaTaxNumber.indexOf(".") < 0 ? deltaTaxNumber : deltaTaxNumber.replaceAll("0*$", "").replaceAll("\\.$", "");
			csc.setDeltaTaxFilter(deltaTaxNumber);
			impsql.append(CONST_AND);
			if (CTPUtil.isOnlyNumbers(csc.getDeltaTaxFilter())) {
				impsql.append(" DELTA LIKE :deltaFilter ");
				impsql.append(CONST_AND);
				impsql.append(" DELTAEXCISETAX_FLAG = 'Value'");
			} else {
				impsql.append(" UPPER(DELTAEXCISETAX_FLAG) LIKE UPPER(:deltaFilter)");
			}
		}

		StringBuilder sql = new StringBuilder();
		if (testCriterion(csc.getPermitTypeFilter())) {
			if (csc.getPermitTypeFilter().trim().equalsIgnoreCase("MANU")) {
				sql = sql.append(manusql);
			}
			if (csc.getPermitTypeFilter().trim().equalsIgnoreCase("IMPT")) {
				sql = sql.append(impsql);
			}
		} else {
			sql = sql.append(manusql);
			sql = sql.append("  UNION ALL ");
			sql = sql.append(impsql);
		}

		sql = sql.insert(0, "SELECT res.*, COUNT(*) OVER() result_count FROM ( ").append(" ORDER BY 1,9,2,3,4 ) res");
		
		// Create Query
		Query pgQuery = this.getPaginatedSQLQuery(new StringBuffer(sql.toString()),"LEGAL_NM");

		// Set SQL parameters
		if (testCriterion(csc.getStatusTypeFilter())) {
			pgQuery.setParameterList("status", csc.getStatusTypeFilter());
		}
		if (testCriterion(csc.getCompanyFilter())) {
			pgQuery.setParameter("companyFilter", "%" + csc.getCompanyFilter() + "%");
		}
		if (testCriterion(csc.getEinFilter())) {
			pgQuery.setParameter("einFilter", "%" + csc.getEinFilter() + "%");
		}
		if (testCriterion(csc.getpermitNumFilter())) {
			pgQuery.setParameter("permitNumFilter", "%" + csc.getpermitNumFilter() + "%");
		}
		if (testCriterion(csc.getTobaccoClassFilter())) {
			pgQuery.setParameter("tobaccoClassFilter", "%" + csc.getTobaccoClassFilter() + "%");
		}
		if (testCriterion(csc.getQuarterFilter())) {
			pgQuery.setParameter("quarterFilter", "%" + csc.getQuarterFilter() + "%");
		}
		if (testCriterion(csc.getDeltaTaxFilter())) {
			pgQuery.setParameter("deltaFilter", "%" + csc.getDeltaTaxFilter() + "%");
		}
		// if (addStatusTypeParameter) {
		// pgQuery.setParameter("statusTypeFilter", status );
		// }
		pgQuery.setParameter("fiscalYr", fiscalYear);

		logger.debug("getComparisonResults() :: executing SQL");
		List<Object[]> trueupComparisonSQLData = pgQuery.list();
		// Get counts

		List<TrueUpComparisonResults> trueupComparisonResults = new ArrayList<>();

		logger.debug("getComparisonResults() :: building results");
		for (Object[] obj : trueupComparisonSQLData) {
			TrueUpComparisonResults resultData = new TrueUpComparisonResults();
			if (obj[0] != null) {
				resultData.setLegalName(obj[0].toString());
			}
			if (obj[1] != null) {
				resultData.setTobaccoClass(obj[1].toString());
			}
			if (obj[2] != null) {
				resultData.setQuarter(obj[2].toString());
			}
			if (obj[3] != null) {
				resultData.setTotalDeltaTax(((BigDecimal) obj[3]).doubleValue());
				resultData.setAbstotalDeltaTax(Math.abs(resultData.getTotalDeltaTax()));
			}
			if (obj[4] != null) {
				resultData.setPermitType("MANU".equals(obj[4].toString()) ? "Manufacturer" : "Importer");
			}
			if (obj[5] != null) {
				resultData.setCompanyId(((BigDecimal) obj[5]).intValue());
			}
			if (obj[6] != null) {
				resultData.setStatus(obj[6].toString());
			}

			if (obj[7] != null) {
				if (obj[7].toString().equals("InProgress")) {
					resultData.setStatus(obj[7].toString());
				}
			}

			if (obj[8] != null) {
				resultData.setEin(obj[8].toString());

			}

			if (obj[9] != null) {
				resultData.setPermitNum(removeDuplicatePermitNumbers(obj[9].toString()));

			}
			if (obj[10] != null) {
				resultData.setDeltaexcisetaxFlag(obj[10].toString());

			}

			if (obj[11] != null) {
			    if(totalCountMap.get(fiscalYear)==null || (totalCountMap.get(fiscalYear) != null 
			            && totalCountMap.get(fiscalYear) != ((BigDecimal) obj[11]).intValue()
			            && totalCountMap.get(fiscalYear) < ((BigDecimal) obj[11]).intValue()) 
			            || (totalCountMap.get(fiscalYear) != null && !csc.isAnyFilterSelected() 
			            && totalCountMap.get(fiscalYear) > ((BigDecimal) obj[11]).intValue())) {
			        totalCountMap.put(fiscalYear, ((BigDecimal) obj[11]).intValue());
			    }
			    this.setSQLQueryTotalRowCount(totalCountMap.get(fiscalYear));
				this.setSQLQueryFilteredRowCount(((BigDecimal) obj[11]).intValue());
			}

			//resultData.setTtlCntInProgress(0);
			resultData.setDataType("Matched");
			trueupComparisonResults.add(resultData);
		}
		if(trueupComparisonResults.size()==0)
        {
            if( csc.isAnyFilterSelected()) {
                this.setSQLQueryTotalRowCount(totalCountMap.get(fiscalYear));
            } else {
              //this is when file will get deleted
                this.setSQLQueryTotalRowCount(0);
            }
        } 
		logger.debug("getComparisonResults() :: returning results");

		return trueupComparisonResults;

	}

	public List<TrueUpComparisonResults> getComparisonSingleResults(long fiscalYear, TrueUpCompareSearchCriteria csc) {

		logger.debug("getComparisonSingleResults() :: entering");
		logger.debug("getComparisonSingleResults() :: building SQL");
		boolean addStatusTypeParameter = false;
		String status = "";

		// Building SQL
		StringBuilder manusql = new StringBuilder(
				" SELECT /*+ NO_QUERY_TRANSFORMATION(CMPMANU) */ LEGAL_NM, CLASSNAME, QUARTERX, DELTA, PERMIT_TYPE_CD, COMPANY_ID, ACCEPTANCE_FLAG, IN_PROGRESS_FLAG, EIN, PERMIT_NUM, DELTAEXCISETAX_FLAG  "
						+ "  FROM ANN_TRUEUP_COMPARERESULTS_MANU  cmpmanu " + "  WHERE cmpmanu.fiscal_yr = :fiscalYr ");

		if (testCriterion(csc.getStatusTypeFilter())) {

			manusql.append(CONST_AND);

			// status = "'" + StringUtils.join(csc.getStatusTypeFilter(), "','")
			// + "'";

			manusql.append(" ( UPPER(nvl(acceptance_flag,'NOACT')) IN (:status)");
			addStatusTypeParameter = true;

			if (csc.getStatusTypeFilter().contains("INPROGRESS")) {
				manusql.append(CONST_OR);
				manusql.append(" upper(IN_PROGRESS_FLAG) = 'INPROGRESS'");

			}
			manusql.append(" ) ");
		}

		if (testCriterion(csc.getCompanyFilter())) {
			manusql.append(CONST_AND);
			manusql.append(" UPPER(LEGAL_NM) LIKE UPPER(:companyFilter) ");
		}
		if (testCriterion(csc.getEinFilter())) {
			manusql.append(CONST_AND);
			manusql.append(" UPPER(EIN) LIKE UPPER(:einFilter) ");
		}
		if (testCriterion(csc.getpermitNumFilter())) {
			manusql.append(CONST_AND);
			manusql.append(" UPPER(PERMIT_NUM) LIKE UPPER(:permitNumFilter) ");
		}
		if (testCriterion(csc.getTobaccoClassFilter())) {
			manusql.append(CONST_AND);
			manusql.append(" UPPER(CLASSNAME) LIKE UPPER(:tobaccoClassFilter) ");
		}
		if (testCriterion(csc.getQuarterFilter())) {
			manusql.append(CONST_AND);
			manusql.append(" QUARTERX LIKE :quarterFilter ");
		}
		if (testCriterion(csc.getDeltaTaxFilter())) {
			manusql.append(CONST_AND);
			manusql.append(" DELTA LIKE :deltaFilter ");
		}

		StringBuilder impsql = new StringBuilder(
				"  SELECT  /*+ NO_QUERY_TRANSFORMATION(cmpimp) */ LEGAL_NM, CLASSNAME, QUARTERX, delta as delta, PERMIT_TYPE_CD, COMPANY_ID, ACCEPTANCE_FLAG, IN_PROGRESS_FLAG, EIN, PERMIT_NUM, DELTAEXCISETAX_FLAG "
						+ "  FROM ANN_TRUEUP_COMPARERES_IMP_SNG  cmpimp " + "  WHERE cmpimp.fiscal_yr = :fiscalYr ");

		if (testCriterion(csc.getStatusTypeFilter())) {
			impsql.append(CONST_AND);

			impsql.append(" ( UPPER(nvl(acceptance_flag,'NOACT')) IN (:status)");
			addStatusTypeParameter = true;

			if (csc.getStatusTypeFilter().contains("INPROGRESS")) {
				impsql.append(CONST_OR);
				impsql.append(" upper(IN_PROGRESS_FLAG) = 'INPROGRESS'");

			}

			impsql.append(" ) ");
		}

		if (testCriterion(csc.getCompanyFilter())) {
			impsql.append(CONST_AND);
			impsql.append(" UPPER(LEGAL_NM) LIKE UPPER(:companyFilter) ");
		}
		if (testCriterion(csc.getEinFilter())) {
			impsql.append(CONST_AND);
			impsql.append(" UPPER(EIN) LIKE UPPER(:einFilter) ");
		}

		if (testCriterion(csc.getpermitNumFilter())) {
			impsql.append(CONST_AND);
			impsql.append(" UPPER(PERMIT_NUM) LIKE UPPER(:permitNumFilter) ");
		}
		if (testCriterion(csc.getTobaccoClassFilter())) {
			impsql.append(CONST_AND);
			impsql.append(" UPPER(CLASSNAME) LIKE UPPER(:tobaccoClassFilter) ");
		}
		if (testCriterion(csc.getQuarterFilter())) {
			impsql.append(CONST_AND);
			impsql.append(" QUARTERX LIKE :quarterFilter ");
		}
		if (testCriterion(csc.getDeltaTaxFilter())) {
			impsql.append(CONST_AND);
			String deltaTaxNumber = csc.getDeltaTaxFilter().replaceAll("[^0-9.]", "");
			deltaTaxNumber = deltaTaxNumber.indexOf(".") < 0 ? deltaTaxNumber : deltaTaxNumber.replaceAll("0*$", "").replaceAll("\\.$", "");

			csc.setDeltaTaxFilter(deltaTaxNumber);
			if (CTPUtil.isOnlyNumbers(csc.getDeltaTaxFilter())) {
				impsql.append(" DELTA LIKE :deltaFilter ");
				impsql.append(CONST_AND);
				impsql.append(" DELTAEXCISETAX_FLAG = 'Value'");
			} else {
				impsql.append(" UPPER(DELTAEXCISETAX_FLAG) LIKE UPPER(:deltaFilter)");
			}
		}

		StringBuilder sql = new StringBuilder();
		if (testCriterion(csc.getPermitTypeFilter())) {
			if (csc.getPermitTypeFilter().trim().equalsIgnoreCase("MANU")) {
				sql = sql.append(manusql);
			}
			if (csc.getPermitTypeFilter().trim().equalsIgnoreCase("IMPT")) {
				sql = sql.append(impsql);
			}
		} else {
			sql = sql.append(manusql);
			sql = sql.append("  UNION ALL ");
			sql = sql.append(impsql);
		}

		sql = sql.insert(0, "SELECT res.*, COUNT(*) OVER() result_count FROM ( ").append(" ) res");
		
		// Create Query
		Query pgQuery = this.getPaginatedSQLQuery(new StringBuffer(sql.toString()),"LEGAL_NM");

		// Set SQL parameters
		if (testCriterion(csc.getStatusTypeFilter())) {
			pgQuery.setParameterList("status", csc.getStatusTypeFilter());
		}
		if (testCriterion(csc.getCompanyFilter())) {
			pgQuery.setParameter("companyFilter", "%" + csc.getCompanyFilter() + "%");
		}
		if (testCriterion(csc.getEinFilter())) {
			pgQuery.setParameter("einFilter", "%" + csc.getEinFilter() + "%");
		}
		if (testCriterion(csc.getpermitNumFilter())) {
			pgQuery.setParameter("permitNumFilter", "%" + csc.getpermitNumFilter() + "%");
		}
		if (testCriterion(csc.getTobaccoClassFilter())) {
			pgQuery.setParameter("tobaccoClassFilter", "%" + csc.getTobaccoClassFilter() + "%");
		}
		if (testCriterion(csc.getQuarterFilter())) {
			pgQuery.setParameter("quarterFilter", "%" + csc.getQuarterFilter() + "%");
		}
		if (testCriterion(csc.getDeltaTaxFilter())) {
			pgQuery.setParameter("deltaFilter", "%" + csc.getDeltaTaxFilter() + "%");
		}
		// if (addStatusTypeParameter) {
		// pgQuery.setParameter("statusTypeFilter", status );
		// }
		pgQuery.setParameter("fiscalYr", fiscalYear);

		logger.debug("getComparisonSingleResults() :: executing SQL");
		List<Object[]> trueupComparisonSQLData = pgQuery.list();
		// Get counts

		List<TrueUpComparisonResults> trueupComparisonResults = new ArrayList<>();

		logger.debug("getComparisonSingleResults() :: building results");
		for (Object[] obj : trueupComparisonSQLData) {
			TrueUpComparisonResults resultData = new TrueUpComparisonResults();
			if (obj[0] != null) {
				resultData.setLegalName(obj[0].toString());
			}
			if (obj[1] != null) {
				resultData.setTobaccoClass(obj[1].toString());
			}
			if (obj[2] != null) {
				resultData.setQuarter(obj[2].toString());
			}
			if (obj[3] != null) {
				resultData.setTotalDeltaTax(((BigDecimal) obj[3]).doubleValue());
				resultData.setAbstotalDeltaTax(Math.abs(resultData.getTotalDeltaTax()));
			}
			if (obj[4] != null) {
				resultData.setPermitType("MANU".equals(obj[4].toString()) ? "Manufacturer" : "Importer");
			}
			if (obj[5] != null) {
				resultData.setCompanyId(((BigDecimal) obj[5]).intValue());
			}
			if (obj[6] != null) {
				resultData.setStatus(obj[6].toString());
			}

			if (obj[7] != null) {
				if (obj[7].toString().equals("InProgress")) {
					resultData.setStatus(obj[7].toString());
				}
			}

			if (obj[8] != null) {
				resultData.setEin(obj[8].toString());

			}

			if (obj[9] != null) {
				resultData.setPermitNum(removeDuplicatePermitNumbers(obj[9].toString()));

			}
			if (obj[10] != null) {
				resultData.setDeltaexcisetaxFlag(obj[10].toString());

			}

			if (obj[11] != null) {
			    if(totalCountMap.get(fiscalYear)==null || (totalCountMap.get(fiscalYear) != null 
			            && totalCountMap.get(fiscalYear) != ((BigDecimal) obj[11]).intValue()
			            && totalCountMap.get(fiscalYear) < ((BigDecimal) obj[11]).intValue()) 
			            || (totalCountMap.get(fiscalYear) != null && !csc.isAnyFilterSelected() 
			            && totalCountMap.get(fiscalYear) > ((BigDecimal) obj[11]).intValue())) {
			        totalCountMap.put(fiscalYear, ((BigDecimal) obj[11]).intValue());
			    }
			    this.setSQLQueryTotalRowCount(totalCountMap.get(fiscalYear));
				this.setSQLQueryFilteredRowCount(((BigDecimal) obj[11]).intValue());
			}

			//resultData.setTtlCntInProgress(0);
			resultData.setDataType("Matched");
			trueupComparisonResults.add(resultData);
		}

		if(trueupComparisonResults.size()==0)
		{
		    if(csc.isAnyFilterSelected() && totalCountMap.size()>0) {
		        this.setSQLQueryTotalRowCount(totalCountMap.get(fiscalYear));
		    } else {
		      //this is when file will get deleted
	            this.setSQLQueryTotalRowCount(0);
		    }
		} 

		logger.debug("getComparisonSingleResults() :: returning results");

		return trueupComparisonResults;

	}

	
	@Override
	public TrueUpComparisonResults getAllComparisonResultsCount(long fiscalYear, String inComingPage) {
	    
	    /**
	     * this block is to separate out the call for total count  for Deltas action required message.  
	     */
	    
        TrueUpComparisonResults result = new TrueUpComparisonResults();
        if(inComingPage.toString().equalsIgnoreCase("Matched")) {
            
            ProcedureCall procCall = getCurrentSession().createStoredProcedureCall("CALC_ANN_TRUEUP_DELTA_CHANGE");
        
            procCall.registerParameter("P_FISCAL_YR", String.class, ParameterMode.IN).bindValue(Long.toString(fiscalYear));
            procCall.registerParameter("TTL_CNT_DELTA_CHNG", Integer.class, ParameterMode.OUT);
            procCall.registerParameter("TTL_CNT_DELTA_CHNG_ZERO", Integer.class, ParameterMode.OUT);
            procCall.registerParameter("TTL_CNT_ACCPT_APPRV", Integer.class, ParameterMode.OUT);
            procCall.registerParameter("TTL_CNT_COMPARE_RSLTS", Integer.class, ParameterMode.OUT);

            ProcedureOutputs outputs = procCall.getOutputs();
            //ttlCntDeltaChng = (Integer) outputs.getOutputParameterValue("TTL_CNT_DELTA_CHNG");
            Integer ttlCntDeltaChng = (Integer) outputs.getOutputParameterValue("TTL_CNT_DELTA_CHNG");
            Integer ttlCntDeltaChngZero = (Integer) outputs.getOutputParameterValue("TTL_CNT_DELTA_CHNG_ZERO");
            Integer ttlCntAccptApprv = (Integer) outputs.getOutputParameterValue("TTL_CNT_ACCPT_APPRV");
            result.setTtlCntDeltaChng(ttlCntDeltaChng);
            result.setTtlCntDeltaChngZero(ttlCntDeltaChngZero);
            result.setTtlCntAccptApprv(ttlCntAccptApprv);
            
            /**
             * for In progress
             */
            
             logger.debug("getComparisonResults() :: getting counts SQL");
             ProcedureCall procCall1 = this.getStoredProcedureByName("COUNT_TOTAL_ROWS_INPROGRESS");
             Integer inProgressCnt = this.getTotalCount(procCall1,"ANN_TRUEUP_COMPARERESULTS_MANU", "ANN_TRUEUP_COMPARERESULTS_IMP","IN_PROGRESS_FLAG", "'InProgress'", (int) fiscalYear, true);
             result.setTtlCntInProgress(inProgressCnt);
            
        } else {
            Integer allDeltasInProgressX = 0; // for excludes 
            Integer allDeltasInProgress = 0;
            Integer allDeltasInReview = 0;
            Integer ttlCntDeltaChng = 0;

         
           List<Object[]> trueupCountSQLData = null;
           String sql = "select status, count(*) as count from ANN_TRUEUP_CMPR_ALL_DELTAS_VW "  
                  + "where FISCAL_YR =:fiscalYear  and status in('Review', 'In Progress' ,'In Progress X') " 
                  + "group by status";

                 Query query = getCurrentSession().createSQLQuery(sql);
                 query.setParameter("fiscalYear", fiscalYear);
                 trueupCountSQLData = query.list();
                 
                 for (Object[] obj : trueupCountSQLData) {
                     
                     if(obj[0].toString() != null && obj[0].toString().equalsIgnoreCase("Review"))
                         allDeltasInReview =  ((BigDecimal) obj[1]).intValue();
                                             
                      
                     if(obj[0] != null && obj[0].toString().equalsIgnoreCase("In Progress"))                 
                         allDeltasInProgress = ((BigDecimal) obj[1]).intValue();
                                         
          
                     if(obj[0] != null && obj[0].toString().equalsIgnoreCase("In Progress X"))                   
                         allDeltasInProgressX = ((BigDecimal) obj[1]).intValue();                        
                     
                 }
                 
             
                 
             if(inComingPage.toString().equalsIgnoreCase("MarketShare"))
             {
                     ProcedureCall countProcedureAmmendApprv = this.getStoredProcedureByName("COUNT_ACCEPT_APPRV");
                     Integer allDeltasAmendApprov = this.getTotalCountAmmendApprv(countProcedureAmmendApprv,
                       "ANN_TRUEUP_COMPARERESULTS_IMP", "ANN_TRUEUP_COMPARERESULTS_MANU", (int) fiscalYear);
                     result.setTtlCntAccptApprv(allDeltasAmendApprov);
                     
                     ProcedureCall procCall = getCurrentSession().createStoredProcedureCall("CALC_ANN_TRUEUP_DELTA_CHANGE");
                 
                     procCall.registerParameter("P_FISCAL_YR", String.class, ParameterMode.IN).bindValue(Long.toString(fiscalYear));
                     procCall.registerParameter("TTL_CNT_DELTA_CHNG", Integer.class, ParameterMode.OUT);
                     procCall.registerParameter("TTL_CNT_DELTA_CHNG_ZERO", Integer.class, ParameterMode.OUT);
                     procCall.registerParameter("TTL_CNT_ACCPT_APPRV", Integer.class, ParameterMode.OUT);
                     procCall.registerParameter("TTL_CNT_COMPARE_RSLTS", Integer.class, ParameterMode.OUT);

                     ProcedureOutputs outputs = procCall.getOutputs();
                     ttlCntDeltaChng = (Integer) outputs.getOutputParameterValue("TTL_CNT_DELTA_CHNG");
                     result.setTtlCntDeltaChng(ttlCntDeltaChng);
                    
             }
         
              result.setttlDeltaCntInReview(allDeltasInProgress + allDeltasInProgressX + allDeltasInReview);
              result.setttlDeltaCntInProgress(allDeltasInProgress + allDeltasInProgressX);
        }
		return result;
	}

public TrueUpComparisonResults getAllComparisonSingleResultsCount(long fiscalYear, String inComingPage) {
	    
	    /**
	     * this block is to separate out the call for total count  for Deltas action required message.  
	     */
	    
        TrueUpComparisonResults result = new TrueUpComparisonResults();
        if(inComingPage.toString().equalsIgnoreCase("Matched")) {
            
            ProcedureCall procCall = getCurrentSession().createStoredProcedureCall("CALC_ANN_TRUEUP_DELTA_CHNG_SNG");
        
            procCall.registerParameter("P_FISCAL_YR", String.class, ParameterMode.IN).bindValue(Long.toString(fiscalYear));
            procCall.registerParameter("TTL_CNT_DELTA_CHNG", Integer.class, ParameterMode.OUT);
            procCall.registerParameter("TTL_CNT_DELTA_CHNG_ZERO", Integer.class, ParameterMode.OUT);
            procCall.registerParameter("TTL_CNT_ACCPT_APPRV", Integer.class, ParameterMode.OUT);
            procCall.registerParameter("TTL_CNT_COMPARE_RSLTS", Integer.class, ParameterMode.OUT);

            ProcedureOutputs outputs = procCall.getOutputs();
            //ttlCntDeltaChng = (Integer) outputs.getOutputParameterValue("TTL_CNT_DELTA_CHNG");
            Integer ttlCntDeltaChng = (Integer) outputs.getOutputParameterValue("TTL_CNT_DELTA_CHNG");
            Integer ttlCntDeltaChngZero = (Integer) outputs.getOutputParameterValue("TTL_CNT_DELTA_CHNG_ZERO");
            Integer ttlCntAccptApprv = (Integer) outputs.getOutputParameterValue("TTL_CNT_ACCPT_APPRV");
            result.setTtlCntDeltaChng(ttlCntDeltaChng);
            result.setTtlCntDeltaChngZero(ttlCntDeltaChngZero);
            result.setTtlCntAccptApprv(ttlCntAccptApprv);
            
            /**
             * for In progress
             */
            
             logger.debug("getAllComparisonSingleResults() :: getting counts SQL");
             ProcedureCall procCall1 = this.getStoredProcedureByName("COUNT_TOTAL_ROWS_INPROGRESS");
             Integer inProgressCnt = this.getTotalCount(procCall1,"ANN_TRUEUP_COMPARERESULTS_MANU", "ANN_TRUEUP_COMPARERES_IMP_SNG","IN_PROGRESS_FLAG", "'InProgress'", (int) fiscalYear, true);
             result.setTtlCntInProgress(inProgressCnt);
            
        } else {
            Integer allDeltasInProgressX = 0; // for excludes 
            Integer allDeltasInProgress = 0;
            Integer allDeltasInReview = 0;
            Integer ttlCntDeltaChng = 0;

         
           List<Object[]> trueupCountSQLData = null;
           String sql = "select status, count(*) as count from ANN_TRUEUP_CMPR_ALLDELT_SNG_VW "  
                  + "where FISCAL_YR =:fiscalYear  and status in('Review', 'In Progress' ,'In Progress X') " 
                  + "group by status";

                 Query query = getCurrentSession().createSQLQuery(sql);
                 query.setParameter("fiscalYear", fiscalYear);
                 trueupCountSQLData = query.list();
                 
                 for (Object[] obj : trueupCountSQLData) {
                     
                     if(obj[0].toString() != null && obj[0].toString().equalsIgnoreCase("Review"))
                         allDeltasInReview =  ((BigDecimal) obj[1]).intValue();
                                             
                      
                     if(obj[0] != null && obj[0].toString().equalsIgnoreCase("In Progress"))                 
                         allDeltasInProgress = ((BigDecimal) obj[1]).intValue();
                                         
          
                     if(obj[0] != null && obj[0].toString().equalsIgnoreCase("In Progress X"))                   
                         allDeltasInProgressX = ((BigDecimal) obj[1]).intValue();                        
                     
                 }
                 
             
                 
             if(inComingPage.toString().equalsIgnoreCase("MarketShare"))
             {
                     ProcedureCall countProcedureAmmendApprv = this.getStoredProcedureByName("COUNT_ACCEPT_APPRV_SNG");
                     Integer allDeltasAmendApprov = this.getTotalCountAmmendApprv(countProcedureAmmendApprv,
                       "ANN_TRUEUP_COMPARERES_IMP_SNG", "ANN_TRUEUP_COMPARERESULTS_MANU", (int) fiscalYear);
                     result.setTtlCntAccptApprv(allDeltasAmendApprov);
                     
                     ProcedureCall procCall = getCurrentSession().createStoredProcedureCall("CALC_ANN_TRUEUP_DELTA_CHNG_SNG");
                 
                     procCall.registerParameter("P_FISCAL_YR", String.class, ParameterMode.IN).bindValue(Long.toString(fiscalYear));
                     procCall.registerParameter("TTL_CNT_DELTA_CHNG", Integer.class, ParameterMode.OUT);
                     procCall.registerParameter("TTL_CNT_DELTA_CHNG_ZERO", Integer.class, ParameterMode.OUT);
                     procCall.registerParameter("TTL_CNT_ACCPT_APPRV", Integer.class, ParameterMode.OUT);
                     procCall.registerParameter("TTL_CNT_COMPARE_RSLTS", Integer.class, ParameterMode.OUT);

                     ProcedureOutputs outputs = procCall.getOutputs();
                     ttlCntDeltaChng = (Integer) outputs.getOutputParameterValue("TTL_CNT_DELTA_CHNG");
                     result.setTtlCntDeltaChng(ttlCntDeltaChng);
                    
             }
         
              result.setttlDeltaCntInReview(allDeltasInProgress + allDeltasInProgressX + allDeltasInReview);
              result.setttlDeltaCntInProgress(allDeltasInProgress + allDeltasInProgressX);
        }
		return result;
	}

	
	
	/**
	 * Returns all deltas (Matched, TUFA, Ingested) filtered down by the
	 * provided fiscal year and search criteria.
	 */
	@Override
	public List<TrueUpComparisonResults> getAllComparisonResults(long fiscalYear, TrueUpCompareSearchCriteria csc) {

		logger.debug("getAllComparisonResults() :: entering");
		logger.debug("getAllComparisonResults() :: building SQL");
		String status = "";
		StringBuilder alldeltas = new StringBuilder(
				"SELECT res.company_nm, res.tobacco_class_nm, res.fiscal_qtr, res.delta, res.permit_type, "
						+ "res.source, res.ein, res.original_company_nm, res.status, res.original_ein, res.delta_comment, res.permit_num, res.company_id,res.deltaexcisetax_flag, COUNT(*) OVER() result_count "
						+ "FROM ANN_TRUEUP_CMPR_ALL_DELTAS_VW res WHERE res.fiscal_yr = :fiscalYr ");

		// Tobacco Class Filters
		if (IsAtleastOneTobaccoTypeSelected(csc)) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" ( ");

			boolean isFirstTobaccoTypeSelection = false;

			if (csc.getSelTobaccoTypes().isShowCigarettes()) {
				if (isFirstTobaccoTypeSelection == false) {
					isFirstTobaccoTypeSelection = true;
				} else {
					alldeltas.append(CONST_OR);
				}
				alldeltas.append(" tobacco_class_nm = 'Cigarettes' ");
			}
			if (csc.getSelTobaccoTypes().isShowCigars()) {
				if (isFirstTobaccoTypeSelection == false) {
					isFirstTobaccoTypeSelection = true;
				} else {
					alldeltas.append(CONST_OR);
				}
				alldeltas.append(" tobacco_class_nm = 'Cigars' ");
			}
			if (csc.getSelTobaccoTypes().isShowChews()) {
				if (isFirstTobaccoTypeSelection == false) {
					isFirstTobaccoTypeSelection = true;
				} else {
					alldeltas.append(CONST_OR);
				}
				alldeltas.append(" tobacco_class_nm IN ( 'Chew','Chew/Snuff' ) ");
			}
			if (csc.getSelTobaccoTypes().isShowSnuffs()) {
				if (isFirstTobaccoTypeSelection == false) {
					isFirstTobaccoTypeSelection = true;
				} else {
					alldeltas.append(CONST_OR);
				}
				alldeltas.append(" tobacco_class_nm IN ( 'Snuff','Chew/Snuff' ) ");
			}
			if (csc.getSelTobaccoTypes().isShowPipes()) {
				if (isFirstTobaccoTypeSelection == false) {
					isFirstTobaccoTypeSelection = true;
				} else {
					alldeltas.append(CONST_OR);
				}
				alldeltas.append(" tobacco_class_nm IN ( 'Pipe','Pipe/Roll-Your-Own','Pipe/Roll Your Own' ) ");
			}
			if (csc.getSelTobaccoTypes().isShowRollYourOwn()) {
				if (isFirstTobaccoTypeSelection == false) {
					isFirstTobaccoTypeSelection = true;
				} else {
					alldeltas.append(CONST_OR);
				}
				alldeltas.append(
						" tobacco_class_nm IN ( 'Roll-Your-Own','Roll Your Own','Pipe/Roll-Your-Own','Pipe/Roll Your Own' ) ");
			}
			alldeltas.append(" ) ");
		}

		// Header filters
		if (testCriterion(csc.getCompanyFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" UPPER(company_nm) LIKE UPPER(:companyFilter) ");
		}
		if (testCriterion(csc.getEinFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" ein LIKE :einFilter ");
		}
		if (testCriterion(csc.getpermitNumFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" UPPER(permit_num) LIKE UPPER(:permitNumFilter) ");
		}
		if (testCriterion(csc.getTobaccoClassFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" UPPER(tobacco_class_nm) LIKE UPPER(:tobaccoClassFilter) ");
		}
		if (testCriterion(csc.getQuarterFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" fiscal_qtr LIKE :quarterFilter ");
		}
		if (testCriterion(csc.getDeltaTaxFilter())) {
			alldeltas.append(CONST_AND);
			String deltaTaxNumber = csc.getDeltaTaxFilter().replaceAll("[^0-9.]", "");
			deltaTaxNumber = deltaTaxNumber.indexOf(".") < 0 ? deltaTaxNumber : deltaTaxNumber.replaceAll("0*$", "").replaceAll("\\.$", "");
			csc.setDeltaTaxFilter(deltaTaxNumber);
			if (CTPUtil.isOnlyNumbers(csc.getDeltaTaxFilter())) {
				alldeltas.append(" delta LIKE :deltaFilter ");
				alldeltas.append(CONST_AND);
				alldeltas.append(" DELTAEXCISETAX_FLAG = 'Value'");
			} else {
				alldeltas.append(" UPPER(DELTAEXCISETAX_FLAG) LIKE UPPER(:deltaFilter)");
			}
		}
		if (testCriterion(csc.getPermitTypeFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" permit_type = :permitTypeFilter ");
		}
		if (testCriterion(csc.getDataTypeFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" source = :dataTypeFilter ");
		}
		if (testCriterion(csc.getOriginalNameFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" UPPER(ORIGINAL_COMPANY_NM) LIKE :originalCompanyFilter ");
		}
		if (testCriterion(csc.getAllDeltaStatusTypeFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" UPPER(nvl(STATUS,'NA')) IN (:status)");

		}

		alldeltas = alldeltas.insert(0, "SELECT res.* FROM ( ").append(" ORDER BY 1,7,2,3,4 ) res");
		
		// Get paginated result
		Query pgQuery = this.getPaginatedSQLQuery(new StringBuffer(alldeltas.toString()), "company_nm");

		if (testCriterion(csc.getCompanyFilter())) {
			pgQuery.setParameter("companyFilter", "%" + csc.getCompanyFilter() + "%");
		}
		if (testCriterion(csc.getEinFilter())) {
			pgQuery.setParameter("einFilter", "%" + csc.getEinFilter() + "%");
		}
		if (testCriterion(csc.getpermitNumFilter())) {
			pgQuery.setParameter("permitNumFilter", "%" + csc.getpermitNumFilter() + "%");
		}
		if (testCriterion(csc.getTobaccoClassFilter())) {
			pgQuery.setParameter("tobaccoClassFilter", "%" + csc.getTobaccoClassFilter() + "%");
		}
		if (testCriterion(csc.getQuarterFilter())) {
			pgQuery.setParameter("quarterFilter", "%" + csc.getQuarterFilter() + "%");
		}
		if (testCriterion(csc.getDeltaTaxFilter())) {
			pgQuery.setParameter("deltaFilter", "%" + csc.getDeltaTaxFilter() + "%");
		}
		if (testCriterion(csc.getPermitTypeFilter())) {
			pgQuery.setParameter("permitTypeFilter", csc.getPermitTypeFilter().trim().toUpperCase());
		}
		if (testCriterion(csc.getDataTypeFilter())) {
			pgQuery.setParameter("dataTypeFilter", csc.getDataTypeFilter().trim().toUpperCase());
		}
		if (testCriterion(csc.getOriginalNameFilter())) {
			pgQuery.setParameter("originalCompanyFilter", "%" + csc.getOriginalNameFilter().trim().toUpperCase() + "%");
		}
		if (testCriterion(csc.getAllDeltaStatusTypeFilter())) {
			pgQuery.setParameterList("status", csc.getAllDeltaStatusTypeFilter());
		}

		pgQuery.setParameter("fiscalYr", fiscalYear);

		logger.debug("getAllComparisonResults() :: executing SQL");
		List<Object[]> trueupComparisonSQLData = pgQuery.list();

		List<TrueUpComparisonResults> trueupComparisonResults = new ArrayList<>();

		logger.debug("getAllComparisonResults() :: building result");
		// Manage results
		for (Object[] obj : trueupComparisonSQLData) {
			TrueUpComparisonResults resultData = new TrueUpComparisonResults();
			if (obj[0] != null) {
				resultData.setLegalName(obj[0].toString());
			}
			if (obj[1] != null) {
				resultData.setTobaccoClass(obj[1].toString());
			}
			if (obj[2] != null) {
				resultData.setQuarter(obj[2].toString());
			}
			if (obj[3] != null) {
				resultData.setTotalDeltaTax(((BigDecimal) obj[3]).doubleValue());
				resultData.setAbstotalDeltaTax(Math.abs(resultData.getTotalDeltaTax()));
			}
			if (obj[4] != null) {
				resultData.setPermitType("MANU".equals(obj[4].toString()) ? "Manufacturer" : "Importer");
			}
			if (obj[5] != null) {
				resultData.setDataType(obj[5].toString());
			}
			if (obj[6] != null) {
				resultData.setEin(obj[6].toString());
			}
			if (obj[7] != null) {
				resultData.setOriginalName(obj[7].toString());
			}
			if (obj[8] != null) {
				resultData.setAllDeltaStatus(obj[8].toString());
			}
			if (obj[9] != null) {
				resultData.setOriginalEIN(obj[9].toString());
			}
			if (obj[10] != null) {
				resultData.setDeltaComment(obj[10].toString());
			}
			if (obj[11] != null) {
				resultData.setPermitNum(removeDuplicatePermitNumbers(obj[11].toString()));
			}
			if (obj[12] != null) {
				resultData.setCompanyId(((BigDecimal) obj[12]).longValue());
			}
			if (obj[13] != null) {
				resultData.setDeltaexcisetaxFlag(obj[13].toString());
			}
			if (obj[13] != null) {
				this.setSQLQueryTotalRowCount(((BigDecimal) obj[14]).intValue());
			}

			trueupComparisonResults.add(resultData);
		}

		logger.debug("getAllComparisonResults() :: retruning comparison results");
		return trueupComparisonResults;

	}

	/**
	 * Returns all deltas (Matched, TUFA, Ingested) filtered down by the
	 * provided fiscal year and search criteria.
	 */
	@Override
	public List<TrueUpComparisonResults> getAllComparisonSingleResults(long fiscalYear, TrueUpCompareSearchCriteria csc) {

		logger.debug("getAllComparisonSingleResults() :: entering");
		logger.debug("getAllComparisonSingleResults() :: building SQL");
		String status = "";
		StringBuilder alldeltas = new StringBuilder(
				"SELECT res.company_nm, res.tobacco_class_nm, res.fiscal_qtr, res.delta, res.permit_type, "
						+ "res.source, res.ein, res.original_company_nm, res.status, res.original_ein, res.delta_comment, res.permit_num, res.company_id,res.deltaexcisetax_flag, COUNT(*) OVER() result_count "
						+ "FROM ANN_TRUEUP_CMPR_ALLDELT_SNG_VW res WHERE res.fiscal_yr = :fiscalYr ");

		// Tobacco Class Filters
		if (IsAtleastOneTobaccoTypeSelected(csc)) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" ( ");

			boolean isFirstTobaccoTypeSelection = false;

			if (csc.getSelTobaccoTypes().isShowCigarettes()) {
				if (isFirstTobaccoTypeSelection == false) {
					isFirstTobaccoTypeSelection = true;
				} else {
					alldeltas.append(CONST_OR);
				}
				alldeltas.append(" tobacco_class_nm = 'Cigarettes' ");
			}
			if (csc.getSelTobaccoTypes().isShowCigars()) {
				if (isFirstTobaccoTypeSelection == false) {
					isFirstTobaccoTypeSelection = true;
				} else {
					alldeltas.append(CONST_OR);
				}
				alldeltas.append(" tobacco_class_nm = 'Cigars' ");
			}
			if (csc.getSelTobaccoTypes().isShowChews()) {
				if (isFirstTobaccoTypeSelection == false) {
					isFirstTobaccoTypeSelection = true;
				} else {
					alldeltas.append(CONST_OR);
				}
				alldeltas.append(" tobacco_class_nm IN ( 'Chew','Chew/Snuff' ) ");
			}
			if (csc.getSelTobaccoTypes().isShowSnuffs()) {
				if (isFirstTobaccoTypeSelection == false) {
					isFirstTobaccoTypeSelection = true;
				} else {
					alldeltas.append(CONST_OR);
				}
				alldeltas.append(" tobacco_class_nm IN ( 'Snuff','Chew/Snuff' ) ");
			}
			if (csc.getSelTobaccoTypes().isShowPipes()) {
				if (isFirstTobaccoTypeSelection == false) {
					isFirstTobaccoTypeSelection = true;
				} else {
					alldeltas.append(CONST_OR);
				}
				alldeltas.append(" tobacco_class_nm IN ( 'Pipe','Pipe/Roll-Your-Own','Pipe/Roll Your Own' ) ");
			}
			if (csc.getSelTobaccoTypes().isShowRollYourOwn()) {
				if (isFirstTobaccoTypeSelection == false) {
					isFirstTobaccoTypeSelection = true;
				} else {
					alldeltas.append(CONST_OR);
				}
				alldeltas.append(
						" tobacco_class_nm IN ( 'Roll-Your-Own','Roll Your Own','Pipe/Roll-Your-Own','Pipe/Roll Your Own' ) ");
			}
			alldeltas.append(" ) ");
		}

		// Header filters
		if (testCriterion(csc.getCompanyFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" UPPER(company_nm) LIKE UPPER(:companyFilter) ");
		}
		if (testCriterion(csc.getEinFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" ein LIKE :einFilter ");
		}
		if (testCriterion(csc.getpermitNumFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" UPPER(permit_num) LIKE UPPER(:permitNumFilter) ");
		}
		if (testCriterion(csc.getTobaccoClassFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" UPPER(tobacco_class_nm) LIKE UPPER(:tobaccoClassFilter) ");
		}
		if (testCriterion(csc.getQuarterFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" fiscal_qtr LIKE :quarterFilter ");
		}
		if (testCriterion(csc.getDeltaTaxFilter())) {
			alldeltas.append(CONST_AND);
			String deltaTaxNumber = csc.getDeltaTaxFilter().replaceAll("[^0-9.]", "");
			deltaTaxNumber = deltaTaxNumber.indexOf(".") < 0 ? deltaTaxNumber : deltaTaxNumber.replaceAll("0*$", "").replaceAll("\\.$", "");
			csc.setDeltaTaxFilter(deltaTaxNumber);
			if (CTPUtil.isOnlyNumbers(csc.getDeltaTaxFilter())) {
				alldeltas.append(" delta LIKE :deltaFilter ");
				alldeltas.append(CONST_AND);
				alldeltas.append(" DELTAEXCISETAX_FLAG = 'Value'");
			} else {
				alldeltas.append(" UPPER(DELTAEXCISETAX_FLAG) LIKE UPPER(:deltaFilter)");
			}
		}
		if (testCriterion(csc.getPermitTypeFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" permit_type = :permitTypeFilter ");
		}
		if (testCriterion(csc.getDataTypeFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" source = :dataTypeFilter ");
		}
		if (testCriterion(csc.getOriginalNameFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" UPPER(ORIGINAL_COMPANY_NM) LIKE :originalCompanyFilter ");
		}
		if (testCriterion(csc.getAllDeltaStatusTypeFilter())) {
			alldeltas.append(CONST_AND);
			alldeltas.append(" UPPER(nvl(STATUS,'NA')) IN (:status)");

		}

		// Get paginated result
		Query pgQuery = this.getPaginatedSQLQuery(new StringBuffer(alldeltas.toString()),"company_nm");

		if (testCriterion(csc.getCompanyFilter())) {
			pgQuery.setParameter("companyFilter", "%" + csc.getCompanyFilter() + "%");
		}
		if (testCriterion(csc.getEinFilter())) {
			pgQuery.setParameter("einFilter", "%" + csc.getEinFilter() + "%");
		}
		if (testCriterion(csc.getpermitNumFilter())) {
			pgQuery.setParameter("permitNumFilter", "%" + csc.getpermitNumFilter() + "%");
		}
		if (testCriterion(csc.getTobaccoClassFilter())) {
			pgQuery.setParameter("tobaccoClassFilter", "%" + csc.getTobaccoClassFilter() + "%");
		}
		if (testCriterion(csc.getQuarterFilter())) {
			pgQuery.setParameter("quarterFilter", "%" + csc.getQuarterFilter() + "%");
		}
		if (testCriterion(csc.getDeltaTaxFilter())) {
			pgQuery.setParameter("deltaFilter", "%" + csc.getDeltaTaxFilter() + "%");
		}
		if (testCriterion(csc.getPermitTypeFilter())) {
			pgQuery.setParameter("permitTypeFilter", csc.getPermitTypeFilter().trim().toUpperCase());
		}
		if (testCriterion(csc.getDataTypeFilter())) {
			pgQuery.setParameter("dataTypeFilter", csc.getDataTypeFilter().trim().toUpperCase());
		}
		if (testCriterion(csc.getOriginalNameFilter())) {
			pgQuery.setParameter("originalCompanyFilter", "%" + csc.getOriginalNameFilter().trim().toUpperCase() + "%");
		}
		if (testCriterion(csc.getAllDeltaStatusTypeFilter())) {
			pgQuery.setParameterList("status", csc.getAllDeltaStatusTypeFilter());
		}

		pgQuery.setParameter("fiscalYr", fiscalYear);

		logger.debug("getAllComparisonSingleResults() :: executing SQL");
		List<Object[]> trueupComparisonSQLData = pgQuery.list();

		List<TrueUpComparisonResults> trueupComparisonResults = new ArrayList<>();

		logger.debug("getAllComparisonSingleResults() :: building result");
		// Manage results
		for (Object[] obj : trueupComparisonSQLData) {
			TrueUpComparisonResults resultData = new TrueUpComparisonResults();
			if (obj[0] != null) {
				resultData.setLegalName(obj[0].toString());
			}
			if (obj[1] != null) {
				resultData.setTobaccoClass(obj[1].toString());
			}
			if (obj[2] != null) {
				resultData.setQuarter(obj[2].toString());
			}
			if (obj[3] != null) {
				resultData.setTotalDeltaTax(((BigDecimal) obj[3]).doubleValue());
				resultData.setAbstotalDeltaTax(Math.abs(resultData.getTotalDeltaTax()));
			}
			if (obj[4] != null) {
				resultData.setPermitType("MANU".equals(obj[4].toString()) ? "Manufacturer" : "Importer");
			}
			if (obj[5] != null) {
				resultData.setDataType(obj[5].toString());
			}
			if (obj[6] != null) {
				resultData.setEin(obj[6].toString());
			}
			if (obj[7] != null) {
				resultData.setOriginalName(obj[7].toString());
			}
			if (obj[8] != null) {
				resultData.setAllDeltaStatus(obj[8].toString());
			}
			if (obj[9] != null) {
				resultData.setOriginalEIN(obj[9].toString());
			}
			if (obj[10] != null) {
				resultData.setDeltaComment(obj[10].toString());
			}
			if (obj[11] != null) {
				resultData.setPermitNum(removeDuplicatePermitNumbers(obj[11].toString()));
			}
			if (obj[12] != null) {
				resultData.setCompanyId(((BigDecimal) obj[12]).longValue());
			}
			if (obj[13] != null) {
				resultData.setDeltaexcisetaxFlag(obj[13].toString());
			}
			if (obj[13] != null) {
				this.setSQLQueryTotalRowCount(((BigDecimal) obj[14]).intValue());
			}

			trueupComparisonResults.add(resultData);
		}

		logger.debug("getAllComparisonSingleResults() :: retruning comparison results");
		return trueupComparisonResults;

	}

	
	@Override
	public List<TrueUpComparisonSummary> getCigarComparisonSummary(long companyId, long fiscalYear, String permitType) {
		int numColumns = 4;
		String sql = "";
		List<Object[]> trueupComparisonSQLData = null;
		List<TrueUpComparisonSummary> trueupComparisonSummaryList = new ArrayList<>();

		if (permitType == "MANU") {
			sql = " SELECT  "+ 
					"     nvl(TO_CHAR(ingestedandtufa.quarter), 'Total') AS period, "+ 
				    "SUM(DECODE(ingestedandtufa.zero_flag, 'Y', 0, ingestedandtufa.taxes_paid)) AS fda3852, "+ 
				"     SUM(ingestedandtufa.ttb_taxes_paid) AS ingested, "+ 
				"     coalesce((SUM(DECODE(ingestedandtufa.zero_flag, 'Y', 0, ingestedandtufa.taxes_paid)) - SUM(ingestedandtufa.ttb_taxes_paid)), SUM "+ 
				"     (DECODE(ingestedandtufa.zero_flag, 'Y', 0, ingestedandtufa.taxes_paid)), 0 - SUM(ingestedandtufa.ttb_taxes_paid)) AS difference "+ 
				" FROM "+ 
				"     ( "+ 
				"         SELECT "+ 
				"             nvl(a.fiscal_yr, ttbingested.ingested_fiscal_yr) AS fiscal_yr, "+ 
				"             nvl(p.permit_num, ttbingested.permit_num) AS permit_num, "+ 
				"             detail.taxes_paid, "+ 
				"             ttbingested.ttb_taxes_paid, "+ 
				"             pp.zero_flag, "+ 
				"             nvl(rp.quarter, ttbingested.ingested_quarter) AS quarter, "+ 
				"             nvl(detail.tobacco_class_nm, ttbingested.tobacco_class_nm) AS tobacco_class_nm, "+ 
				"             ( "+ 
				"                 SELECT "+ 
				"                     permit_type_cd "+ 
				"                 FROM "+ 
				"                     tu_permit    tpa, "+ 
				"                     tu_company   tpc "+ 
				"                 WHERE "+ 
				"                     ( tpa.permit_num = nvl(p.permit_num, ttbingested.permit_num) ) "+ 
				"                     AND tpa.company_id = tpc.company_id "+ 
				"                     AND tpc.ein = ( nvl(c.ein, ttbingested.ein_num) ) "+ 
				"             ) AS permit_type_cd, "+ 
				"             ttbingested.ein_num, "+ 
				"             nvl(c.company_id,( "+ 
				"                 SELECT "+ 
				"                     tc.company_id "+ 
				"                 FROM "+ 
				"                     tu_company tc "+ 
				"                 WHERE "+ 
				"                     tc.ein = ttbingested.ein_num "+ 
				"             )) AS company_id "+ 
				"         FROM "+ 
				"             tu_annual_trueup   a "+ 
				"             INNER JOIN tu_rpt_period      rp ON ( a.fiscal_yr = rp.fiscal_yr ) "+ 
				"             INNER JOIN tu_permit_period   pp ON rp.period_id = pp.period_id "+ 
				"             INNER JOIN tu_period_status   ps ON ( pp.permit_id = ps.permit_id "+ 
				"                                                 AND pp.period_id = ps.period_id ) "+ 
				"             INNER JOIN tu_permit          p ON pp.permit_id = p.permit_id "+ 
				"             INNER JOIN tu_company         c ON p.company_id = c.company_id "+ 
				"             LEFT OUTER JOIN ( "+ 
				"                 SELECT "+ 
				"                     sf.permit_id, "+ 
				"                     sf.period_id, "+ 
				"                     fd.tobacco_class_id, "+ 
				"                     fd.removal_qty, "+ 
				"                     fd.taxes_paid, "+ 
				"                     tc.tobacco_class_nm, "+ 
				"                     tc.parent_class_id "+ 
				"                 FROM "+ 
				"                     tu_submitted_form   sf, "+ 
				"                     tu_form_detail      fd, "+ 
				"                     tu_tobacco_class    tc "+ 
				"                 WHERE "+ 
				"                     sf.form_id = fd.form_id "+ 
				"                     AND fd.tobacco_class_id = tc.tobacco_class_id "+ 
				"                     AND sf.form_type_cd = :formtypecd "+ 
				"             ) detail ON ( detail.permit_id = pp.permit_id "+ 
				"                           AND detail.period_id = pp.period_id ) "+ 
				"                         AND detail.tobacco_class_nm = 'Cigars' "+ 
				"             FULL OUTER JOIN ( "+ 
				"                 SELECT "+ 
				"                     ttbcmpy.ein_num, "+ 
				"                     ttbpermit.permit_num, "+ 
				"                     SUM(ttbannualtx.ttb_taxes_paid) AS ttb_taxes_paid, "+ 
				"                     ttbannualtx.tobacco_class_id, "+ 
				"                     ttc.tobacco_class_nm, "+ 
				"                     ttbannualtx.month              AS ingested_month, "+ 
				"                     ttbcmpy.fiscal_yr              AS ingested_fiscal_yr, "+ 
				"                     ttbannualtx.ttb_calendar_qtr   AS ingested_quarter, "+ 
				"                     ttbtc.company_id               AS ingested_company_id, "+ 
				"                     ttbtp.permit_type_cd           AS ingested_permit_type_cd, "+ 
				"                     ttbtp.permit_id                AS ingested_permit_id "+ 
				"                 FROM "+ 
				"                     tu_ttb_company      ttbcmpy "+ 
				"                     INNER JOIN tu_ttb_permit       ttbpermit ON ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id "+ 
				"                     INNER JOIN tu_ttb_annual_tax   ttbannualtx ON ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id "+ 
				"                     INNER JOIN tu_tobacco_class    ttc ON ttbannualtx.tobacco_class_id = ttc.tobacco_class_id "+ 
				"                     INNER JOIN tu_company          ttbtc ON ttbcmpy.ein_num = ttbtc.ein "+ 
				"                     INNER JOIN tu_permit           ttbtp ON ttbtp.permit_num = ttbpermit.permit_num "+ 
				"                                                   AND ttbtp.company_id = ttbtc.company_id "+ 
				"                 WHERE "+ 
				"                     ttbcmpy.fiscal_yr = :fiscalyr "+ 
				"                 GROUP BY "+ 
				"                     ttbcmpy.ein_num, "+ 
				"                     ttbpermit.permit_num, "+ 
				"                     ttbannualtx.tobacco_class_id, "+ 
				"                     ttc.tobacco_class_nm, "+ 
				"                     ttbannualtx.month, "+ 
				"                     ttbcmpy.fiscal_yr, "+ 
				"                     ttbannualtx.ttb_calendar_qtr, "+ 
				"                     ttbtc.company_id, "+ 
				"                     ttbtp.permit_type_cd, "+ 
				"                     ttbtp.permit_id "+ 
				"             ) ttbingested ON ttbingested.ein_num = c.ein "+ 
				"                              AND p.permit_num = ttbingested.permit_num "+ 
				"                              AND rp.month = ttbingested.ingested_month "+ 
				"                              AND ttbingested.tobacco_class_nm = 'Cigars' "+ 
				"                              AND ttbingested.ingested_fiscal_yr = a.fiscal_yr "+ 
				"     ) ingestedandtufa "+ 
				" WHERE "+ 
				"     ingestedandtufa.fiscal_yr = :fiscalyr "+ 
				"     AND ingestedandtufa.permit_type_cd = :permittype "+ 
				"     AND ingestedandtufa.company_id = :companyid "+ 
				"     AND ingestedandtufa.tobacco_class_nm = 'Cigars' "+ 
				" GROUP BY "+ 
				"     ROLLUP(ingestedandtufa.quarter) "+ 
				" ORDER BY "+ 
				"     ingestedandtufa.quarter ";;

			Query query = getCurrentSession().createSQLQuery(sql);
			query.setParameter("formtypecd", "3852");
			query.setParameter("fiscalyr", fiscalYear);
			query.setParameter("companyid", companyId);
			query.setParameter("permittype", permitType);

			trueupComparisonSQLData = query.list();

			// List<TrueUpComparisonSummary> trueupComparisonSummaryList = new
			// ArrayList<>();

			// Add Header Row - with Column Title
			TrueUpComparisonSummary headerrow = new TrueUpComparisonSummary();
			List<String> headercellValues = new ArrayList<>();
			headercellValues.add("Tobacco Class");
			headercellValues.add("Cigars");
			headercellValues.add("Cigars");
			headercellValues.add("Cigars");
			headerrow.setCellValues(headercellValues);
			headerrow.setNumColumns(numColumns);
			trueupComparisonSummaryList.add(headerrow);

			// Add 4 Quarters Data Row
			for (int i = 1; i < 5; i++) {
				boolean isQuarterFound = false;
				for (Object[] obj : trueupComparisonSQLData) {
					String Quarter = (obj[0] != null) ? obj[0].toString() : "";
					if (Quarter.equalsIgnoreCase(String.valueOf(i))) {
						isQuarterFound = true;
						TrueUpComparisonSummary row = new TrueUpComparisonSummary();
						List<String> cellValues = new ArrayList<>();
						for (int colIndex = 0; colIndex < numColumns; colIndex++) {

							String cellValue = "";
							if (obj[colIndex] != null)
								cellValue = obj[colIndex].toString();
							else if (obj[colIndex] == null)
								cellValue = null;

							if (colIndex == 0 && !"TOTAL".equalsIgnoreCase(cellValue)) {
								cellValues.add("Quarter " + cellValue);
							} else {

								// adding a hack for nvl as NA instead of O
								if (cellValue == null)
									cellValues.add("NA");
								else
									cellValues.add(cellValue);

							}

						}
						row.setCellValues(cellValues);
						row.setNumColumns(numColumns);
						trueupComparisonSummaryList.add(row);
						break;
					}
				}
				if (isQuarterFound == false) {
					TrueUpComparisonSummary row = new TrueUpComparisonSummary();
					List<String> cellValues = new ArrayList<>();
					// Must add 4 column
					cellValues.add("Quarter " + String.valueOf(i));
					cellValues.add("NA");
					cellValues.add("NA");
					cellValues.add("NA");
					row.setCellValues(cellValues);
					row.setNumColumns(numColumns);
					trueupComparisonSummaryList.add(row);
				}
			}

			// Add Total Row
			for (Object[] obj : trueupComparisonSQLData) {
				if (obj[0] != null && "TOTAL".equalsIgnoreCase(obj[0].toString())) {
					TrueUpComparisonSummary row = new TrueUpComparisonSummary();
					List<String> cellValues = new ArrayList<>();
					for (int colIndex = 0; colIndex < numColumns; colIndex++) {

						String cellValue = "";
						if (obj[colIndex] != null)
							cellValue = obj[colIndex].toString();
						else if (obj[colIndex] == null)
							cellValue = null;

						if (cellValue == null)
							cellValues.add("NA");
						else
							cellValues.add(cellValue);

					}
					row.setCellValues(cellValues);
					row.setNumColumns(numColumns);
					trueupComparisonSummaryList.add(row);
				}
			}

		}

		return trueupComparisonSummaryList;
	}

	@Override
	public AnnImporterGrid getImporterComparisonSummary(long companyId, long fiscalYear,
			long quarter, String tobaccoClass) {
		//Get the detail taxes for each month
				Query query = getCurrentSession().createQuery("from AnnImporterIngestedDetailRecordViewEntity summ "
						+ "where summ.companyId =:companyId and summ.fiscalYr =:fiscalYr and summ.fiscalQtr =:quarter and summ.tobaccoClassNm =:tobaccoClass");
				query.setParameter("fiscalYr", fiscalYear);
				query.setParameter("companyId", companyId);
				query.setParameter("quarter", quarter);
				query.setParameter("tobaccoClass", tobaccoClass);
				List<AnnImporterIngestedDetailRecordViewEntity> detail = query.list();
				
				//Get the summary taxes for each month
				query = getCurrentSession().createQuery("from AnnImporterIngestedSummaryRecordViewEntity summ "
						+ "where summ.companyId =:companyId and summ.fiscalYr =:fiscalYr and summ.fiscalQtr =:quarter and summ.tobaccoClassNm =:tobaccoClass");
				query.setParameter("fiscalYr", fiscalYear);
				query.setParameter("companyId", companyId);
				query.setParameter("quarter", quarter);
				query.setParameter("tobaccoClass", tobaccoClass);
				List<AnnImporterIngestedSummaryRecordViewEntity> summary = query.list();
				
				//Get the summary taxes for each month
				String quarterCondition = !"Cigars".equals(tobaccoClass) ? " and summ.fiscalQtr =:quarter " : "";
				query = getCurrentSession().createQuery("from AnnImporterTufaRecordViewEntity summ "
						+ "where summ.companyId =:companyId and summ.fiscalYr =:fiscalYr and (summ.tobaccoClassNm =:tobaccoClass OR summ.tobaccoClassNm IS NULL)" + quarterCondition);
				query.setParameter("fiscalYr", fiscalYear);
				query.setParameter("companyId", companyId);
				if(!"Cigars".equals(tobaccoClass))
					query.setParameter("quarter", quarter);
				query.setParameter("tobaccoClass", tobaccoClass);
				List<AnnImporterTufaRecordViewEntity> tufa = query.list();
				
				//Create empty objects for the grid that will hold each month
				List<AnnImporterGridRecord> records = new ArrayList<AnnImporterGridRecord>();
				
				//Retrive the static months for the quarter
				List<String> quarterMonths = Constants.MMMByQuarter.get(Long.valueOf(quarter).intValue());
				
				//Fill the grid with the selected months
				for(int i = 0; i < quarterMonths.size(); i++) {
					AnnImporterGridRecord rec = new AnnImporterGridRecord();
					rec.setMonth(quarterMonths.get(i));
					records.add(rec);
				}
				
				//Set the detail tax for each month
				for(AnnImporterIngestedDetailRecordViewEntity entity : detail){
					for(AnnImporterGridRecord rec : records) {
						if(rec.getMonth().equals(entity.getMonth())) rec.setDetailValue(entity.getTaxes());
					}
				}
				
				//Set the summay tax for each month
				for(AnnImporterIngestedSummaryRecordViewEntity entity : summary){
					for(AnnImporterGridRecord rec : records) {
						if(rec.getMonth().equals(entity.getMonth())) rec.setSummaryValue(entity.getTaxes());
					}
				}
					
				//Set the summay tax for each month
				for(AnnImporterTufaRecordViewEntity entity : tufa){
					for(AnnImporterGridRecord rec : records) {
						//Addition is needed since TUFA side can have 2 records if it has both started MR and Zero Reports
						if(rec.getMonth().equals(entity.getMonth())) rec.addTufaValue(entity.getTaxes());
					}
				}
				
				AnnImporterGrid grid = new AnnImporterGrid();
				grid.setSummaryMonths(records);
				grid.setTobaccoClass(tobaccoClass);
				
				return grid;

	}

	public CBPIngestestedSummaryDetailsData getImporterSummDetailData(String einType, String ein, long fiscalYear, String quarter,
			String permitType, String tobaccoClass) {

		CBPIngestestedSummaryDetailsData cbpdata = new CBPIngestestedSummaryDetailsData();
		String einColumn = "ein";
		
		if (tobaccoClass.equals("Roll Your Own")) {
			tobaccoClass = "Roll-Your-Own";
		} else if (tobaccoClass.equals("Cigars")) {
			quarter = "1-4";
		}
		
		if("original".equals(einType)) {
			einColumn = "original_ein";
		}

		String sql = "SELECT       SUM(summarized_tax) AS cbpsummarized_tax, "
				+ "      SUM(cbpingested_taxes) AS cbpingested_taxes, "
				+ "      SUM(cbp_detail_volume) AS cbp_detail_volume,       tax_rate   AS tufa_tax_rate, "
				+ "      file_type   FROM       (           SELECT "
				+ "              SUM(cbp_taxes_paid) AS cbpingested_taxes, "
				+ "              SUM(cbp_qtyremoved) AS cbp_detail_volume,               summarized_tax, "
				+ "              ttc.tax_rate,               file_type, "
				+ "              summary_entry_summary_number           FROM "
				+ "              ann_alldelta_review_vw annvw,               current_tobacco_rates_vw ttc "
				+ "          WHERE               nvl(annvw.tobacco_class_nm,:tobaccoClass) =:tobaccoClass "
				+ "              AND fiscal_qtr =:quarter               AND fiscal_year =:fiscalYr "
				+ "              AND " + einColumn + " =:ein               AND ttc.tobacco_class_nm =:tobaccoClass "
				+ "          GROUP BY               summarized_tax,               ttc.tax_rate, "
				+ "              file_type,               summary_entry_summary_number       ) "
				+ "  GROUP BY       tax_rate,       file_type";

		Query query = getCurrentSession().createSQLQuery(sql);

		query.setParameter("fiscalYr", fiscalYear);
		query.setParameter("ein", ein);
		query.setParameter("quarter", quarter);
		query.setParameter("tobaccoClass", tobaccoClass);

		List<Object[]> results = query.list();

		if (results != null) {

			for (Object[] obj : results) {

				if (obj[0] != null)
					cbpdata.setCbpSummaryTaxAmount(obj[0].toString());

				if (obj[1] != null)
					cbpdata.setCbpDetailTaxAmount(obj[1].toString());

				if (obj[2] != null)
					cbpdata.setCbpDetailVolume(obj[2].toString());

				if (obj[3] != null)
					cbpdata.setTufaTaxRate(obj[3].toString());
				if (obj[4] != null)
					cbpdata.setFiletype(obj[4].toString());

			}
		}

		return cbpdata;
	}

	private List<TrueUpComparisonSummary> buildImporterComparisonSummary(String tobaccoClass, int numColumns,
			long quarter, List<Object[]> trueupComparisonSQLData, TrueUpComparisonSummary headerRow) {
		List<TrueUpComparisonSummary> trueupComparisonSummaryList = new ArrayList<>();

		trueupComparisonSummaryList.add(headerRow);

		String[] selmonths = { "Oct", "Nov", "Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep" };

		if ((quarter > 0 && quarter < 5) && (!"CIGARS".endsWith(tobaccoClass.toUpperCase()))) {
			switch ((int) quarter) {
			case 1:
				selmonths = Arrays.copyOfRange(selmonths, 0, 3);
				break;

			case 2:
				selmonths = Arrays.copyOfRange(selmonths, 3, 6);
				break;

			case 3:
				selmonths = Arrays.copyOfRange(selmonths, 6, 9);
				break;

			case 4:
				selmonths = Arrays.copyOfRange(selmonths, 9, 12);
				break;
			}
		}

		for (String month : selmonths) {
			boolean isMonthFound = false;
			String missingmonth = null;
			for (Object[] obj : trueupComparisonSQLData) {
				String selectedMonth = month;
				String comparisonMonth = "";
				if (obj[0] != null) {
					comparisonMonth = obj[0].toString().trim();
				} else {
					continue;
				}

				if (selectedMonth.equalsIgnoreCase(comparisonMonth)) {
					isMonthFound = true;
					TrueUpComparisonSummary row = new TrueUpComparisonSummary();
					List<String> cellValues = new ArrayList<>();
					for (int colIndex = 0; colIndex < numColumns; colIndex++) {

						String cellValue = "";
						if (obj[colIndex] != null)
							cellValue = obj[colIndex].toString();
						else if (obj[colIndex] == null)
							cellValue = null;

						// adding a hack for nvl as NA instead of O
						if (cellValue == null)
							cellValues.add("NA");
						else
							cellValues.add(cellValue);

					}

					row.setCellValues(cellValues);
					row.setNumColumns(numColumns);
					trueupComparisonSummaryList.add(row);
					break;
				} else {
					missingmonth = selectedMonth;
				}
			}
			if (isMonthFound == false) {

				TrueUpComparisonSummary row = new TrueUpComparisonSummary();
				List<String> cellValues = new ArrayList<>();
				// Must add 4 column
				cellValues.add(missingmonth);
				cellValues.add("NA");
				cellValues.add("NA");
				cellValues.add("NA");
				row.setCellValues(cellValues);
				row.setNumColumns(numColumns);
				trueupComparisonSummaryList.add(row);

			}
		}

		// Add Total Row
		for (Object[] obj : trueupComparisonSQLData) {
			if (obj[0] != null && obj[0].toString().toUpperCase().contains("QUARTER")) {
				TrueUpComparisonSummary row = new TrueUpComparisonSummary();
				List<String> cellValues = new ArrayList<>();
				for (int colIndex = 0; colIndex < numColumns; colIndex++) {

					String cellValue = "";
					if (obj[colIndex] != null)
						cellValue = obj[colIndex].toString();
					else if (obj[colIndex] == null)
						cellValue = null;

					// adding a hack for nvl as NA instead of O
					if (cellValue == null)
						cellValues.add("NA");
					else
						cellValues.add(cellValue);

				}
				row.setCellValues(cellValues);
				row.setNumColumns(numColumns);
				trueupComparisonSummaryList.add(row);
			}
		}

		return trueupComparisonSummaryList;
	}

	@Override
	public List<TrueUpComparisonSummary> getNonCigarManufacturerComparisonSummary(long companyId, long fiscalYear,
			long quarter, String permitType, String tobaccoClass) {
		String sql = "";
		int numColumns = 0;

		List<TrueUpComparisonSummary> trueupComparisonSummaryList = new ArrayList<>();

		switch (tobaccoClass.toUpperCase()) {
		case "CIGARETTES": {
			numColumns = 4;

			sql = "SELECT NVL(TO_CHAR(permit_num), 'TOTAL')                                 AS PERMIT, "
					+ "  SUM(CIGARETTES)                                                        AS CIGARETTES, "
					+ "  SUM(INGESTED_CIGARETTES)                                               AS INGESTED_CIGARETTES, "
					+ "  COALESCE((  SUM(CIGARETTES) - SUM(INGESTED_CIGARETTES)), " + "    SUM(CIGARETTES) , "
					+ "    (0- SUM(INGESTED_CIGARETTES)) " + "    ) AS DIFFERENCE_CIGARETTES "
					+ "FROM ANN_TRUEUP_TTB_SUMMARY_VW SUMM_VW " + "WHERE SUMM_VW.FISCAL_YR=:fiscalYr "
					+ "AND SUMM_VW.QUARTER    =:quarter " + "AND SUMM_VW.COMPANY_ID =:company_id "
					+ "GROUP BY ROLLUP (SUMM_VW.permit_num) " + "ORDER BY (SUMM_VW.permit_num) ";

			// Add column titles
			TrueUpComparisonSummary headerrow = new TrueUpComparisonSummary();
			List<String> headercellValues = new ArrayList<>();
			headercellValues.add("Permit");
			headercellValues.add("Cigarettes");
			headercellValues.add("Cigarettes");
			headercellValues.add("Cigarettes");
			headerrow.setCellValues(headercellValues);
			headerrow.setNumColumns(numColumns);
			trueupComparisonSummaryList.add(headerrow);
			break;
		}

		case "CHEW-SNUFF": {
			numColumns = 6;

			sql = "  SELECT NVL(TO_CHAR(permit_num), 'TOTAL')                                               AS PERMIT, "
					+ " SUM(CHEW)                                                   AS \"CHEW\","
					+ " SUM(SNUFF)                                               AS \"SNUFF\","
					+ " COALESCE (SUM(CHEW) +  sum(SNUFF),SUM( CHEW), sum(SNUFF)) AS \"CHEW +SNUFF\","
					+ " SUM(INGESTED_CHEW_SNUFF)  AS INGESTED_CHEW_SNUFF,"
					+ " coalesce( (SUM(chew) + SUM(snuff) - SUM(ingested_chew_snuff) ),(SUM(chew) + SUM(snuff) ), (SUM(chew) - SUM(ingested_chew_snuff) ),(SUM(snuff) - SUM(ingested_chew_snuff) ), 0 - SUM(ingested_chew_snuff),  SUM(chew),SUM(snuff)) AS difference_chew_snuff "
					+ " FROM ANN_TRUEUP_TTB_SUMMARY_VW SUMM_VW "
					+ " WHERE SUMM_VW.FISCAL_YR=:fiscalYr  AND  SUMM_VW.QUARTER=:quarter AND SUMM_VW.COMPANY_ID=:company_id"
					+ " GROUP BY  ROLLUP (SUMM_VW.permit_num)" + " ORDER BY (SUMM_VW.permit_num)";

			// Add column titles
			TrueUpComparisonSummary headerrow = new TrueUpComparisonSummary();
			List<String> headercellValues = new ArrayList<>();
			headercellValues.add("Permit");
			headercellValues.add("Chew");
			headercellValues.add("Snuff");
			headercellValues.add("Chew/Snuff");
			headercellValues.add("Chew/Snuff");
			headercellValues.add("Chew/Snuff");
			headerrow.setCellValues(headercellValues);
			headerrow.setNumColumns(numColumns);
			trueupComparisonSummaryList.add(headerrow);
			break;
		}

		case "PIPE-ROLL YOUR OWN": {
			numColumns = 6;

			sql = "  SELECT NVL(TO_CHAR(permit_num), 'TOTAL')                                               AS PERMIT, "
					+ " SUM(PIPE)                                                   AS \"PIPE\", "
					+ " SUM(\"ROLL-YOUR-OWN\")                                                  AS \"ROLL YOUR OWN\","
					+ " COALESCE ( SUM(pipe) +  sum(\"ROLL-YOUR-OWN\"),SUM( pipe), sum(\"ROLL-YOUR-OWN\")) AS \"PIPE +ROLL YOUR OWN\","
					+ " SUM(INGESTED_PIPE_RYO)  AS INGESTED_PIPE_RYO,"
					+ " COALESCE ((SUM(pipe) + sum(\"ROLL-YOUR-OWN\") - SUM(ingested_pipe_ryo)),(SUM(pipe) + sum(\"ROLL-YOUR-OWN\")),(SUM(pipe)-SUM(ingested_pipe_ryo)),(SUM(\"ROLL-YOUR-OWN\")-SUM(ingested_pipe_ryo)),0-SUM(ingested_pipe_ryo),SUM(pipe),SUM(\"ROLL-YOUR-OWN\") ) AS difference_pipe_ryo "
					+ " FROM ANN_TRUEUP_TTB_SUMMARY_VW SUMM_VW "
					+ " WHERE SUMM_VW.FISCAL_YR=:fiscalYr  AND  SUMM_VW.QUARTER=:quarter AND SUMM_VW.COMPANY_ID=:company_id"
					+ " GROUP BY  ROLLUP (SUMM_VW.permit_num)" + " ORDER BY (SUMM_VW.permit_num)";
			// Add column titles
			TrueUpComparisonSummary headerrow = new TrueUpComparisonSummary();
			List<String> headercellValues = new ArrayList<>();
			headercellValues.add("Permit");
			headercellValues.add("Pipe");
			headercellValues.add("Roll Your Own");
			headercellValues.add("Pipe/Roll Your Own");
			headercellValues.add("Pipe/Roll Your Own");
			headercellValues.add("Pipe/Roll Your Own");
			headerrow.setCellValues(headercellValues);
			headerrow.setNumColumns(numColumns);
			trueupComparisonSummaryList.add(headerrow);
			break;
		}

		default:
			break;
		}

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setLong("fiscalYr", fiscalYear);
		query.setLong("quarter", quarter);
		query.setLong("company_id", companyId);

		List<Object[]> trueupComparisonSQLData = query.list();

		// Add Permit Totals - Data Row Values
		for (Object[] obj : trueupComparisonSQLData) {
			TrueUpComparisonSummary row = new TrueUpComparisonSummary();
			List<String> cellValues = new ArrayList<>();
			for (int colIndex = 0; colIndex < numColumns; colIndex++) {

				String cellValue = "";
				if (obj[colIndex] != null)
					cellValue = obj[colIndex].toString();
				else if (obj[colIndex] == null)
					cellValue = null;

				// adding a hack for null as NA instead of O
				if (cellValue == null)
					cellValues.add("NA");
				else
					cellValues.add(cellValue);
			}

			row.setCellValues(cellValues);

			row.setNumColumns(numColumns);
			trueupComparisonSummaryList.add(row);
		}
		return trueupComparisonSummaryList;
	}

	@Override
	public List<TrueUpComparisonImporterDetail> getImporterComparisonDetails(String ein, long fiscalYear,
			long quarter, String permitType, String tobaccoClass) {
		if (!this.isTobaccoClassStringValid(tobaccoClass))
			return null;

		String nonCigarByQuarterCondition = "";
		if ("ROLL YOUR OWN".equalsIgnoreCase(tobaccoClass) || "RYO".equalsIgnoreCase(tobaccoClass)) {
			tobaccoClass = "Roll-Your-Own";
		}
		// For valid quarter - which is for non cigar tobacco classes
		if ((quarter > 0 && quarter < 5) && (!"CIGARS".endsWith(tobaccoClass.toUpperCase()))) {
			nonCigarByQuarterCondition = " rp.quarter = :quarter AND ";
		}
		
		List<TrueUpComparisonImporterDetail7501Period> detail7501List = this.getPeriod7501Detail(ein, fiscalYear, tobaccoClass, quarter);
		List<TrueUpComparisonImporterDetail7501Period> detail7501ForPeriodList = new ArrayList<TrueUpComparisonImporterDetail7501Period>();
		String sql = "SELECT * " 
				+ "FROM "
				+ "  (SELECT a.* "
				+ "  FROM "
				+ "    (SELECT REPORTSTATUS, "
				+ "      MONTH, "
				+ "      QUARTER, "
				+ "      PERMIT_NUM, "
				+ "      PERMITID, "
				+ "      PERIODID, "
				+ "      TOBACCOCLASS_FDA3852TAXAMOUNT AS \"ESTIMATEDTAX\", "
                + "    (SELECT COUNT(*) FROM tu_submitted_form sf LEFT JOIN tu_form_detail fd ON sf.form_id = fd.form_id "
                + " LEFT JOIN tu_tobacco_class tc ON fd.tobacco_class_id = tc.tobacco_class_id "
                + " LEFT JOIN tu_tobacco_class ptc on tc.parent_class_id = ptc.tobacco_class_id"
                + " WHERE sf.form_type_cd = '7501' AND sf.permit_id = permitId AND sf.period_id = periodId"
                + " AND decode(ptc.tobacco_class_id, 8, ptc.tobacco_class_nm, tc.tobacco_class_nm) = :tobaccoClass AND line_num <> 0"
                + " GROUP BY sf.permit_id, sf.period_id ) AS \"7501_Line_Count\", "
                +		"removalqty"
				+ "    FROM "
				+ "      (SELECT p.permit_num, "
				+ "        p.permit_id                               AS PermitID, "
				+ "        NVL(detail.tobacco_class_nm, :tobaccoClass)      AS tobacco_class_nm, "
				+ "        ps.period_status_type_cd                  AS ReportStatus, "
				+ "        TO_CHAR(to_date(rp.month,'MON'), 'MONTH') AS MONTH, "
				+ "        rp.quarter                                AS Quarter, "
				+ "        pp.period_id                              AS PeriodID, "
				+ "        Decode(pp.zero_flag,'Y',0,detail.taxes_paid)      AS FDA3852TaxAmount, "
				+"		   detail.removal_qty AS removalqty		"			
				+ "      FROM TU_ANNUAL_TRUEUP a "
				+ "      INNER JOIN TU_RPT_PERIOD rp "
				+ "      ON (a.fiscal_yr = rp.fiscal_yr) "
				+ "      INNER JOIN TU_PERMIT_PERIOD pp "
				+ "      ON rp.period_id = pp.period_id "
				+ "      INNER JOIN TU_PERIOD_STATUS ps "
				+ "      ON (pp.permit_id = ps.permit_id "
				+ "      AND pp.period_id = ps.period_id) "
				+ "      INNER JOIN TU_PERMIT p "
				+ "      ON pp.permit_id = p.permit_id "
				+ "      INNER JOIN TU_COMPANY c "
				+ "      ON p.company_id = c.company_id "
				+ "      LEFT OUTER JOIN "
				+ "        (SELECT sf.permit_id, "
				+ "          sf.period_id, "
				+ "          fd.tobacco_class_id, "
				+ "          fd.removal_qty, "
				+ "          fd.taxes_paid, "
				+ "          tc.tobacco_class_nm, "
				+ "          tc.parent_class_id "
				+ "        FROM TU_SUBMITTED_FORM sf, "
				+ "          TU_FORM_DETAIL fd, "
				+ "          TU_TOBACCO_CLASS tc "
				+ "        WHERE sf.form_id       = fd.form_id "
				+ "        AND fd.tobacco_class_id=tc.tobacco_class_id "
				+ "        AND sf.form_type_cd    = '3852' "
				+ "        ) detail "
				+ "      ON (detail.permit_id                                                       = pp.permit_id "
				+ "      AND detail.period_id                                                       = pp.period_id) "
				+ "      AND detail.tobacco_class_nm                                                =  :tobaccoClass"
				+ " WHERE a.fiscal_yr = :fiscalYear"
				+ "      AND p.permit_type_cd                                                       = :permitType"
				+ "      AND "
				+ nonCigarByQuarterCondition
				+ "  "
				+ "       c.ein                                                           = :ein"
				+ "      ) PIVOT (SUM(FDA3852TaxAmount) AS FDA3852TaxAmount FOR (tobacco_class_nm) IN ('"
				+ tobaccoClass
				+ "'"
				+ " AS TOBACCOCLASS)) "
				+ "    GROUP BY (MONTH,QUARTER, PERMIT_NUM, PermitID, REPORTSTATUS, TOBACCOCLASS_FDA3852TAXAMOUNT, PeriodID,removalqty) "
				+ "    ORDER BY to_date(MONTH,'MON') ASC "
				+ "    )a "
				+ "  UNION ALL "
				+ "  SELECT b.* "
				+ "  FROM "
				+ "    (SELECT 'Sub Total', "
				+ "      MONTH, "
				+ "      quarter, "
				+ "      'Permit Num', "
				+ "      0, "
				+ "      PERIODID, "
				+ "      SUM(TOBACCOCLASS_FDA3852TAXAMOUNT) AS \"ESTIMATEDTAX\", NULL AS \"7501_Line_Count\", "
				+"		removalqty			"
				+ "    FROM "
				+ "      (SELECT p.permit_num, "
				+ "        p.permit_id                               AS PermitID, "
				+ "        NVL(detail.tobacco_class_nm, :tobaccoClass )" + "			     AS tobacco_class_nm, "
				+ "        ps.period_status_type_cd                  AS ReportStatus, "
				+ "        TO_CHAR(to_date(rp.month,'MON'), 'MONTH') AS MONTH, "
				+ "        rp.quarter                                AS Quarter, "
				+ "        pp.period_id                              AS PeriodID, "
				+ "        Decode(pp.zero_flag,'Y',0,detail.taxes_paid)                         AS FDA3852TaxAmount, "
				+"		   detail.removal_qty AS removalqty		"	
				+ "      FROM TU_ANNUAL_TRUEUP a "
				+ "      INNER JOIN TU_RPT_PERIOD rp "
				+ "      ON (a.fiscal_yr = rp.fiscal_yr) "
				+ "      INNER JOIN TU_PERMIT_PERIOD pp "
				+ "      ON rp.period_id = pp.period_id "
				+ "      INNER JOIN TU_PERIOD_STATUS ps "
				+ "      ON (pp.permit_id = ps.permit_id "
				+ "      AND pp.period_id = ps.period_id) "
				+ "      INNER JOIN TU_PERMIT p "
				+ "      ON pp.permit_id = p.permit_id "
				+ "      INNER JOIN TU_COMPANY c "
				+ "      ON p.company_id = c.company_id "
				+ "      LEFT OUTER JOIN "
				+ "        (SELECT sf.permit_id, "
				+ "          sf.period_id, "
				+ "          fd.tobacco_class_id, "
				+ "          fd.removal_qty, "
				+ "          fd.taxes_paid, "
				+ "          tc.tobacco_class_nm, "
				+ "          tc.parent_class_id "
				+ "        FROM TU_SUBMITTED_FORM sf, "
				+ "          TU_FORM_DETAIL fd, "
				+ "          TU_TOBACCO_CLASS tc "
				+ "        WHERE sf.form_id       = fd.form_id "
				+ "        AND fd.tobacco_class_id=tc.tobacco_class_id "
				+ "        AND sf.form_type_cd    = '3852' "
				+ "        ) detail "
				+ "      ON (detail.permit_id                                                       = pp.permit_id "
				+ "      AND detail.period_id                                                       = pp.period_id) "
				+ "      AND detail.tobacco_class_nm                                                = :tobaccoClass"
				+ "      WHERE a.fiscal_yr                                                          = "
				+ " :fiscalYear      AND p.permit_type_cd                                                       = :permitType "
				+ "      AND "
				+ nonCigarByQuarterCondition
				+ "       c.ein                                                           = :ein"
				+ "      ) PIVOT (SUM(FDA3852TaxAmount) AS FDA3852TaxAmount FOR (tobacco_class_nm) IN ('"
				+ tobaccoClass
				+ "'"
				+ " AS TOBACCOCLASS)) "
				+ "    GROUP BY MONTH, "
				+ "      QUARTER, "
				+ "      PeriodID, "
				+"		 removalqty" 
				+ "    ORDER BY QUARTER, "
				+ "      PeriodID, "
				+ "      MONTH "
				+ "    )b "
				+ "  ) "
				+ "ORDER BY QUARTER, "
				+ "  TO_CHAR(TO_DATE(MONTH, 'MM'),'mm'), "
				+ "  reportstatus ASC " 
				+ "";
		Query query = getCurrentSession().createSQLQuery(sql);
		if ((quarter > 0 && quarter < 5) && (!"CIGARS".endsWith(tobaccoClass.toUpperCase()))) {
			query.setLong("quarter", quarter);
		}
		query.setParameter("tobaccoClass", tobaccoClass);
		query.setLong("fiscalYear", fiscalYear);
		query.setParameter("permitType", permitType);
		query.setString("ein", ein);
		List<Object[]> trueupComparisonSQLData = query.list();

		// Add Data Row
		List<TrueUpComparisonImporterDetail> details = new ArrayList<>();
		TrueUpComparisonImporterDetail row = new TrueUpComparisonImporterDetail();
		 
		for (Object[] obj : trueupComparisonSQLData) {
			if (obj[0] != null) {
				String periodStatus = obj[0].toString();
				if ("Sub Total".equals(periodStatus)) {
					if (obj[6] != null) {

						row.setPeriodTotal(obj[6].toString());
					}
					if (obj[6] == null) {
						row.setPeriodTotal("NA");
					}
					if (obj[1] != null) {
						String periodMonth = obj[1].toString().trim();
						row.setPeriod(periodMonth);
					}
					if (obj[8] != null) {
						String removalQuantity = obj[8].toString().trim();
						row.setRemovalQuantity(removalQuantity);
					}
					details.add(row);
					// construct new row;
					row = new TrueUpComparisonImporterDetail();
				} else {
					TrueUpComparisonImporterDetailPeriod p = new TrueUpComparisonImporterDetailPeriod();
					String rptStatus = "";
					if (obj[0] != null) {
						rptStatus = obj[0].toString().trim();
						if ("NSTD".equalsIgnoreCase(rptStatus)) {
							p.setStatus("NS");
						} else if ("ERRO".equalsIgnoreCase(rptStatus)) {
							p.setStatus("E");
						} else if ("COMP".equalsIgnoreCase(rptStatus)) {
							p.setStatus("C");
						} else {
							p.setStatus("");
						}
					}
					if (obj[1] != null) {
						String rptMonth = obj[1].toString().trim();
						p.setMonth(rptMonth);
					}
					if (obj[2] != null) {
						p.setQuarter(((BigDecimal) obj[2]).intValue());
					}
					if (obj[3] != null) {
						String rptPermitNum = obj[3].toString().trim();
						p.setPermit(rptPermitNum);
					}

					if (obj[4] != null) {
						p.setPermitId(((BigDecimal) obj[4]).intValue());
					}
					if (obj[5] != null) {
						p.setPeriodId(((BigDecimal) obj[5]).intValue());
					}
					if (obj[6] != null) {
						if ("NSTD".equalsIgnoreCase(rptStatus)) {
							p.setTaxAmount("NA");
						} else {
							p.setTaxAmount(obj[6].toString());

						}
					}
					if (obj[7] != null) {
						p.setLineCount7501(((BigDecimal) obj[7]).intValue());
					}
					if (obj[6] == null) {
						p.setTaxAmount("NA");
					}
					if (obj[8] != null) {
						p.setQuantityRemoved(((BigDecimal) obj[8]).longValue());
					}

					row.getPeriodDetails().add(p);
				}
			}
		}

		// Sort the list
		String[] selmonths = { "October", "November", "December", "January", "February", "March", "April", "May",
				"June", "July", "August", "September" };
		List<TrueUpComparisonImporterDetail> sorteddetails = new ArrayList<>();
		if ((quarter > 0 && quarter < 5) && (!"CIGARS".endsWith(tobaccoClass.toUpperCase()))) {
			switch ((int) quarter) {
			case 1:
				selmonths = Arrays.copyOfRange(selmonths, 0, 3);
				break;

			case 2:
				selmonths = Arrays.copyOfRange(selmonths, 3, 6);
				break;

			case 3:
				selmonths = Arrays.copyOfRange(selmonths, 6, 9);
				break;

			case 4:
				selmonths = Arrays.copyOfRange(selmonths, 9, 12);
				break;
			}
		}

		for (String month : selmonths) {
			boolean isMonthFound = false;
			for (TrueUpComparisonImporterDetail founddetail : details) {
				String selectedMonth = month.toUpperCase();
				String comparisonMonth = founddetail.getPeriod().toUpperCase();
				if (selectedMonth.equalsIgnoreCase(comparisonMonth)) {
					isMonthFound = true;
					TrueUpComparisonImporterDetail foundrow = new TrueUpComparisonImporterDetail();
					detail7501ForPeriodList = new ArrayList<TrueUpComparisonImporterDetail7501Period>();
					List<TrueUpComparisonImporterDetail> filterList  = details.stream().filter(data->data.getPeriod().equalsIgnoreCase(selectedMonth)).collect(Collectors.toList());
					if(CollectionUtils.isNotEmpty(filterList)) {
						double total=0.0;
						double removalQuantity = 0.0;
						boolean isNAForAllEntries = true;
						for(TrueUpComparisonImporterDetail data :filterList) {
							if(!data.getPeriodTotal().equalsIgnoreCase("NA")) {
								total+=Double.parseDouble(data.getPeriodTotal());
								isNAForAllEntries = false;
							} if(data.getRemovalQuantity() != null) {
								removalQuantity+=Double.parseDouble(data.getRemovalQuantity());
							}
						}
						if(isNAForAllEntries) {
							foundrow.setPeriodTotal("NA");
						} else {
							foundrow.setPeriodTotal(String.valueOf(total));
						}
						foundrow.setRemovalQuantity(String.valueOf(removalQuantity));
					}
					foundrow.setPeriod(founddetail.getPeriod());
					for (TrueUpComparisonImporterDetailPeriod founddetailperiod : founddetail.getPeriodDetails()) {
						TrueUpComparisonImporterDetailPeriod foundperiod = new TrueUpComparisonImporterDetailPeriod();
						foundperiod.setStatus(founddetailperiod.getStatus());
						foundperiod.setMonth(founddetailperiod.getMonth());
						foundperiod.setQuarter(founddetailperiod.getQuarter());
						foundperiod.setPermit(founddetailperiod.getPermit());
						foundperiod.setPermitId(founddetailperiod.getPermitId());
						foundperiod.setPeriodId(founddetailperiod.getPeriodId());
						foundperiod.setTaxAmount(founddetailperiod.getTaxAmount());
						foundperiod.setLineCount7501(founddetailperiod.getLineCount7501());
						foundperiod.setQuantityRemoved(founddetailperiod.getQuantityRemoved());
						List<AttachmentsEntity> attachmentEntities = this.findByIds(founddetailperiod.getPeriodId(),
								founddetailperiod.getPermitId());
						if (attachmentEntities.size() > 0) {
							List<Attachment> attachments = new ArrayList<>();
							for (AttachmentsEntity ae : attachmentEntities) {
								Attachment document = new Attachment();
								document.setDocumentId(ae.getDocumentId());
								document.setDocDesc(ae.getDocDesc());
								document.setFormTypes(ae.getFormTypes());
								document.setCreatedDt(ae.getCreatedDt());
								attachments.add(document);
							}
							foundperiod.setAttachments(attachments);
						}
						foundrow.getPeriodDetails().add(foundperiod);
					}
					for(TrueUpComparisonImporterDetail7501Period line501:detail7501List) {
						if(line501.getPeriod().equalsIgnoreCase(selectedMonth)) {
							detail7501ForPeriodList.add(line501);
						}
					}
					foundrow.setPeriod7501Details(detail7501ForPeriodList);
					sorteddetails.add(foundrow);
					break;
				}
			}
			if (isMonthFound == false) {
				TrueUpComparisonImporterDetail missingrow = new TrueUpComparisonImporterDetail();
				missingrow.setPeriod(month);
				missingrow.setPeriodTotal("NA");
				missingrow.setPeriodDetails(null);
				sorteddetails.add(missingrow);
			}
		}
		return sorteddetails;

	}

	@Override
	public List<TrueUpComparisonImporterIngestionDetail> getImporterComparisonIngestionDetails(String ein,
			long fiscalYear, long quarter, String permitType, String tobaccoClass, String secondaryDateFlag) {
		String nonCigarByQuarterCondition = "";
		if ("ROLL YOUR OWN".equalsIgnoreCase(tobaccoClass) || "RYO".equalsIgnoreCase(tobaccoClass)) {
			tobaccoClass = "Roll-Your-Own";
		}
		// For valid quarter - which is for non cigar tobacco classes
		if ((quarter > 0 && quarter < 5) && (!"CIGARS".endsWith(tobaccoClass.toUpperCase()))) {
			nonCigarByQuarterCondition = " AND FISCAL_QTR = :quarter ";
		}

		String sql = "SELECT "
				+ "     assigned_dt, "
				+ "     entry_num, "
				+ "     entry_summ_dt, "
				+ "     entry_dt, "
				+ "     line_num, "
				+ "     estimated_tax, "
				+ "     qty_removed, "
				+ "     category, "
				+ "     entrydateflag, "
				+ "     itemcount, "
				+ "     CASE "
				+ "         WHEN assigned_dt IN ( "
				+ "             'JAN', "
				+ "             'FEB', "
				+ "             'MAR' "
				+ "         ) THEN 2 "
				+ "         WHEN assigned_dt IN ( "
				+ "             'APR', "
				+ "             'MAY', "
				+ "             'JUN' "
				+ "         ) THEN 3 "
				+ "         WHEN assigned_dt IN ( "
				+ "             'JUL', "
				+ "             'AUG', "
				+ "             'SEP' "
				+ "         ) THEN 4 "
				+ "         WHEN assigned_dt IN ( "
				+ "             'OCT', "
				+ "             'NOV', "
				+ "             'DEC' "
				+ "         ) THEN 1 "
				+ "     END AS quarter, "
				+ "     cbp_entry_id, "
				+ "     reallocated_flag, "
				+ "     excluded_flag, "
				+ "     entry_type_cd, "
				+ "		missing_flag "		
				+ " FROM "
				+ "     ( "
				+ "         SELECT "
				+ "             assigned_dt, "
				+ "             entry_num, "
				+ "             TO_CHAR(entry_summ_dt,'MM/DD/YYYY') AS entry_summ_dt, "
				+ "             TO_CHAR(entry_dt,'MM/DD/YYYY') AS entry_dt, "
				+ "             line_num, "
				+ "             estimated_tax, "
				+ "             qty_removed, "
				+ "             'Item' AS category, "
				+ "             'Valid' AS entrydateflag, "
				+ "             1 AS itemcount, "
				+ "             cbp_entry_id, "
				+ "             reallocated_flag, "
				+ "             excluded_flag, "
				+ "             entry_type_cd, "
				+ "				missing_flag "	
				+ "         FROM "
				+ "             tu_cbp_entry "
				+ "         WHERE "
				+ "             fiscal_year =:fiscalyear " + nonCigarByQuarterCondition
				+ "             AND cbp_importer_id IN ( "
				+ " 				SELECT "
				+ "                        cbpi1.cbp_importer_id "
                + "                     FROM"
                + "                         tu_cbp_importer cbpi1 "
                + "                    WHERE "
                + "                          cbpi1.fiscal_yr = :fiscalyear "
                + "                    AND "
                + "                        CASE "
                + "                            WHEN cbpi1.association_type_cd = 'IMPT' "
                + "                                OR ( cbpi1.association_type_cd IS NULL "
                + "                                AND cbpi1.consignee_exists_flag = 'N' ) THEN "
                + "                              cbpi1.importer_ein "
                + "                             WHEN cbpi1.association_type_cd = 'CONS' THEN "
                + "                              cbpi1.consignee_ein END = :ein"
				+ "             ) "
				+ "             AND ( entry_summ_dt IS NOT NULL "
				+ "                   OR entry_dt IS NOT NULL ) "
				+ "             AND tobacco_class_id IN ( "
				+ "                 SELECT "
				+ "                     tobacco_class_id "
				+ "                 FROM "
				+ "                     tu_tobacco_class "
				+ "                 WHERE "
				+ "                     upper(tobacco_class_nm) =:tobaccoupper "
				+ "             ) "
				+ "         UNION ALL "
				+ "         SELECT "
				+ "             assigned_dt, "
				+ "             '' AS entry_num, "
				+ "             '' AS entry_summ_dt, "
				+ "             '' AS entry_dt, "
				+ "             -1 AS line_num, "
				+ "             SUM(estimated_tax) AS estimated_tax, "
				+ "             -1 AS qty_removed, "
				+ "             'SubTotal' AS category, "
				+ "             'Valid' AS entrydateflag, "
				+ "             COUNT(estimated_tax) AS itemcount, "
				+ "             -1 AS cbp_entry_id, "
				+ "             '' AS reallocated_flag, "
				+ "             '' AS excluded_flag, "
				+ "             -1 AS entry_type_cd, "
				+"				'' AS missing_flag "
				+ "         FROM "
				+ "             tu_cbp_entry "
				+ "         WHERE "
				+ "             fiscal_year =:fiscalyear " + nonCigarByQuarterCondition
				+ "             AND cbp_importer_id IN ( "
				+ " 				SELECT "
				+ "                        cbpi1.cbp_importer_id "
                + "                     FROM"
                + "                         tu_cbp_importer cbpi1 "
                + "                    WHERE "
                + "                          cbpi1.fiscal_yr = :fiscalyear "
                + "                    AND "
                + "                        CASE "
                + "                            WHEN cbpi1.association_type_cd = 'IMPT' "
                + "                                OR ( cbpi1.association_type_cd IS NULL "
                + "                                AND cbpi1.consignee_exists_flag = 'N' ) THEN "
                + "                              cbpi1.importer_ein "
                + "                             WHEN cbpi1.association_type_cd = 'CONS' THEN "
                + "                              cbpi1.consignee_ein END = :ein"
				+ "             ) "
				+ "             AND ( entry_summ_dt IS NOT NULL "
				+ "                   OR entry_dt IS NOT NULL ) "
				+ "             AND tobacco_class_id IN ( "
				+ "                 SELECT "
				+ "                     tobacco_class_id "
				+ "                 FROM "
				+ "                     tu_tobacco_class "
				+ "                 WHERE "
				+ "                     upper(tobacco_class_nm) =:tobaccoupper "
				+ "             ) "
				+ "         GROUP BY "
				+ "             assigned_dt "
				+ "         UNION ALL "
				+ "         SELECT "
				+ "             assigned_dt, "
				+ "             entry_num, "
				+ "             '' AS entry_summ_dt, "
				+ "             '' AS entry_dt, "
				+ "             line_num, "
				+ "             estimated_tax, "
				+ "             qty_removed, "
				+ "             'Item' AS category, "
				+ "             'Invalid' AS entrydateflag, "
				+ "             1 AS itemcount, "
				+ "             cbp_entry_id, "
				+ "             reallocated_flag, "
				+ "             excluded_flag, "
				+ "             entry_type_cd, "
				+ "				missing_flag "
				+ "         FROM "
				+ "             tu_cbp_entry "
				+ "         WHERE "
				+ "             fiscal_year =:fiscalyear " + nonCigarByQuarterCondition
				+ "             AND cbp_importer_id IN ( "
				+ " 				SELECT "
				+ "                        cbpi1.cbp_importer_id "
                + "                     FROM"
                + "                         tu_cbp_importer cbpi1 "
                + "                    WHERE "
                + "                          cbpi1.fiscal_yr = :fiscalyear "
                + "                    AND "
                + "                        CASE "
                + "                            WHEN cbpi1.association_type_cd = 'IMPT' "
                + "                                OR ( cbpi1.association_type_cd IS NULL "
                + "                                AND cbpi1.consignee_exists_flag = 'N' ) THEN "
                + "                              cbpi1.importer_ein "
                + "                             WHEN cbpi1.association_type_cd = 'CONS' THEN "
                + "                              cbpi1.consignee_ein END = :ein"

				+ "             ) "
				+ "             AND ( entry_summ_dt IS NULL "
				+ "                   AND entry_dt IS NULL ) "
				+ "             AND tobacco_class_id IN ( "
				+ "                 SELECT "
				+ "                     tobacco_class_id "
				+ "                 FROM "
				+ "                     tu_tobacco_class "
				+ "                 WHERE "
				+ "                     upper(tobacco_class_nm) =:tobaccoupper "
				+ "             ) "
				+ "         UNION ALL "
				+ "         SELECT "
				+ "             assigned_dt, "
				+ "             '' AS entry_num, "
				+ "             '' AS entry_summ_dt, "
				+ "             '' AS entry_dt, "
				+ "             -1 AS line_num, "
				+ "             SUM(estimated_tax) AS estimated_tax, "
				+ "             -1 AS qty_removed, "
				+ "             'SubTotal' AS category, "
				+ "             'Invalid' AS entrydateflag, "
				+ "             COUNT(estimated_tax) AS itemcount, "
				+ "             -1 AS cbp_entry_id, "
				+ "             '' AS reallocated_flag, "
				+ "             '' AS excluded_flag, "
				+ "             -1 AS entry_type_cd, "
				+"				'' AS missing_flag "
				+ "         FROM "
				+ "             tu_cbp_entry "
				+ "         WHERE "
				+ "             fiscal_year =:fiscalyear " + nonCigarByQuarterCondition
				+ "             AND cbp_importer_id IN ( "
				+ " 				SELECT "
				+ "                        cbpi1.cbp_importer_id "
                + "                     FROM"
                + "                         tu_cbp_importer cbpi1 "
                + "                    WHERE "
                + "                          cbpi1.fiscal_yr = :fiscalyear "
                + "                    AND "
                + "                        CASE "
                + "                            WHEN cbpi1.association_type_cd = 'IMPT' "
                + "                                OR ( cbpi1.association_type_cd IS NULL "
                + "                                AND cbpi1.consignee_exists_flag = 'N' ) THEN "
                + "                              cbpi1.importer_ein "
                + "                             WHEN cbpi1.association_type_cd = 'CONS' THEN "
                + "                              cbpi1.consignee_ein END = :ein"

				+ "             ) "
				+ "             AND ( entry_summ_dt IS NULL "
				+ "                   AND entry_dt IS NULL ) "
				+ "             AND tobacco_class_id IN ( "
				+ "                 SELECT "
				+ "                     tobacco_class_id "
				+ "                 FROM "
				+ "                     tu_tobacco_class "
				+ "                 WHERE "
				+ "                     upper(tobacco_class_nm) =:tobaccoupper "
				+ "             ) "
				+ "         GROUP BY "
				+ "             assigned_dt "
				+ "     ) "
				+ " ORDER BY "
				+ "     quarter, "
				+ "     TO_CHAR(TO_DATE('2017-' "
				+ "                         || assigned_dt "
				+ "                         || '-01','YYYY-MM-DD'),'mm'), "
				+ "     entrydateflag, "
				+ "     category, "
				+ "     line_num ";
		Query query = getCurrentSession().createSQLQuery(sql);
		if ((quarter > 0 && quarter < 5) && (!"CIGARS".endsWith(tobaccoClass.toUpperCase()))) {
			query.setLong("quarter", quarter);
		}
		// query.setParameter("tobaccoClass", tobaccoClass);
		query.setParameter("tobaccoupper", tobaccoClass.toUpperCase());
		query.setLong("fiscalyear", fiscalYear);
		query.setString("ein", ein); 
		List<Object[]> trueupComparisonSQLData = query.list();
		// String[] fieldsArray = { "ASSIGNED_DT:0", "ENTRY_NUM:1",
		// "ENTRY_SUMM_DT:2", "ENTRY_DT:3", "LINE_NUM:4", "ESTIMATED_TAX:5",
		// "QTY_REMOVED:6", "Category:7", "EntryDateFlag:8", "ItemCount:9",
		// "Quarter:10", "CBP_ENTRY_ID:11", REALLOCATED_FLAG:12, EXCLUDED_FLAG:
		// 13 , ENTRY_TYPE_CD:14};
		// Arrays.asList(fieldsArray).indexOf("ASSIGNED_DT");

		// Add Data Row
		// Sort the list
		List<TrueUpComparisonImporterIngestionDetail> details = new ArrayList<>();
		TrueUpComparisonImporterIngestionDetail row = new TrueUpComparisonImporterIngestionDetail();
		for (Object[] obj : trueupComparisonSQLData) {
			if (obj[0] != null) {
				String itemCategory = obj[7].toString();
				if ("SubTotal".equals(itemCategory)) {
					if (obj[5] != null) {
						row.setPeriodTotal(obj[5].toString().trim());
					}
					if (obj[8] != null) {
						row.setPeriodEntryDateCategory((obj[8]).toString().trim());
					}
					if (obj[9] != null) {
						row.setPeriodEntryItemCount(((BigDecimal) obj[9]).intValue());
					}
					if (obj[0] != null) {
						String periodMonth = obj[0].toString().trim().toUpperCase();
						row.setPeriod(Constants.MMMToMMMM.get(periodMonth));
					}
					details.add(row);
					// construct new row;
					row = new TrueUpComparisonImporterIngestionDetail();
				} else {
					TrueUpComparisonImporterIngestionDetailPeriod p = new TrueUpComparisonImporterIngestionDetailPeriod();

					if (obj[0] != null) {
						String rptMonth = obj[0].toString().trim().toUpperCase();
						p.setMonth(Constants.MMMToMMMM.get(rptMonth));
					}
					p.setQuarter(quarter);
					if (obj[1] != null) {
						String entryNum = obj[1].toString().trim();
						p.setEntryNum(entryNum);
					}
					if (obj[2] != null) {	
					
						p.setEntrysummaryDate(CTPUtil.parseStringToDate(obj[2].toString()));
					
					} else {
						p.setEntrysummaryDate(null);
					}
					if (obj[3] != null) {			
						p.setEntryDate(CTPUtil.parseStringToDate(obj[3].toString()));
					} else {
						p.setEntryDate(null);
					}

					if (obj[4] != null) {
						p.setLineNum(((BigDecimal) obj[4]).intValue());
					}
					
					if (obj[5] != null ) {
                          p.setTaxAmount(obj[5].toString());                 
					}
					
					if (obj[6] != null) {
						p.setQtyRemoved(((BigDecimal) obj[6]).doubleValue());
					}
					if (obj[11] != null) {
						p.setEntryId(((BigDecimal) obj[11]).intValue());
					}
					if (obj[12] != null) {
						String reallocatedFlag = obj[12].toString().trim();
						p.setReallocatedFlag(reallocatedFlag);
						
					}
					
					
					if (obj[13] != null) {
						String excludedFlag = obj[13].toString().trim();
						p.setExcludedFlag(excludedFlag);
						
					}
					
					if (obj[14] != null) {
						p.setEntryTypeCd(((BigDecimal) obj[14]).intValue());
					}
					if (obj[15] != null) {
						p.setMissingFlag(obj[15].toString().trim());;
					}
					//This will be set to Y only when entry Date is selected
					p.setSecondaryDateFlag(secondaryDateFlag);
					row.getPeriodDetails().add(p);
					
				}
			}
		}

		String[] selmonths = { "October", "November", "December", "January", "February", "March", "April", "May",
				"June", "July", "August", "September" };
		List<TrueUpComparisonImporterIngestionDetail> sorteddetails = new ArrayList<>();
		if ((quarter > 0 && quarter < 5) && (!"CIGARS".endsWith(tobaccoClass.toUpperCase()))) {
			switch ((int) quarter) {
			case 1:
				selmonths = Arrays.copyOfRange(selmonths, 0, 3);
				break;

			case 2:
				selmonths = Arrays.copyOfRange(selmonths, 3, 6);
				break;

			case 3:
				selmonths = Arrays.copyOfRange(selmonths, 6, 9);
				break;

			case 4:
				selmonths = Arrays.copyOfRange(selmonths, 9, 12);
				break;
			}
		}

		for (String month : selmonths) {
			boolean isMonthFound = false;
			for (TrueUpComparisonImporterIngestionDetail founddetail : details) {
				String selectedMonth = month.toUpperCase();
				String comparisonMonth = founddetail.getPeriod().toUpperCase();
				if (selectedMonth.equalsIgnoreCase(comparisonMonth)) {
					isMonthFound = true;
					TrueUpComparisonImporterIngestionDetail foundrow = new TrueUpComparisonImporterIngestionDetail();
					foundrow.setPeriod(founddetail.getPeriod());
					foundrow.setPeriodTotal(founddetail.getPeriodTotal());
					foundrow.setPeriodEntryDateCategory(founddetail.getPeriodEntryDateCategory());
					foundrow.setPeriodEntryItemCount(founddetail.getPeriodEntryItemCount());
					//below check to see if all lines are excluded from quarter
					boolean isAnyLineIncluded = ((founddetail.getPeriodDetails().stream().filter(a->(a.getExcludedFlag() == null || a.getExcludedFlag().equalsIgnoreCase("N"))).collect(Collectors.toList()))!=null &&
					        (founddetail.getPeriodDetails().stream().filter(a->(a.getExcludedFlag() == null || a.getExcludedFlag().equalsIgnoreCase("N"))).collect(Collectors.toList()).size())>0);
					for (TrueUpComparisonImporterIngestionDetailPeriod founddetailperiod : founddetail
							.getPeriodDetails()) {
						TrueUpComparisonImporterIngestionDetailPeriod foundperiod = new TrueUpComparisonImporterIngestionDetailPeriod();
						foundperiod.setMonth(founddetailperiod.getMonth());
						foundperiod.setEntryNum(founddetailperiod.getEntryNum());
						foundperiod.setEntrysummaryDate(founddetailperiod.getEntrysummaryDate());						
						//foundperiod.setInfosortOrder("C");												
						foundperiod.setEntryDate(founddetailperiod.getEntryDate());
						foundperiod.setLineNum(founddetailperiod.getLineNum());
						foundperiod.setTaxAmount(founddetailperiod.getTaxAmount());
						foundperiod.setQtyRemoved(founddetailperiod.getQtyRemoved());
						foundperiod.setEntryId(founddetailperiod.getEntryId());
						foundperiod.setReallocatedFlag(founddetailperiod.getReallocatedFlag());						
						foundperiod.setExcludedFlag(founddetailperiod.getExcludedFlag());
						foundperiod.setMissingFlag(founddetailperiod.getMissingFlag());
						foundperiod.setSecondaryDateFlag(founddetailperiod.getSecondaryDateFlag());
						foundperiod.setQuarter(founddetailperiod.getQuarter());
						if("Y".equals(founddetailperiod.getExcludedFlag()))
						{ 
							foundperiod.setInfosortOrder("A");
							
						}
						else if("N".equals(founddetailperiod.getExcludedFlag()) && "Y".equals(founddetailperiod.getReallocatedFlag()))
						{ 
							foundperiod.setInfosortOrder("B");							
						}
						else 
						{
							foundperiod.setInfosortOrder("C");	
						}
						
						foundperiod.setEntryTypeCd(founddetailperiod.getEntryTypeCd());
						if ("Y".equals(founddetailperiod.getExcludedFlag())) {
						    if(isAnyLineIncluded) {
						        foundrow.setPeriodTotal(String.valueOf(Double.parseDouble(foundrow.getPeriodTotal())
	                                    - Double.parseDouble(founddetailperiod.getTaxAmount())));
	                            foundrow.setPeriodEntryItemCount(founddetail.getPeriodEntryItemCount() - 1);
						    }
						    else {
	                            foundrow.setPeriodTotal("NA");
	                        }
							
						} 
						foundrow.getPeriodDetails().add(foundperiod);
					}
					sorteddetails.add(foundrow);
					break;
				}
			}
			if (isMonthFound == false) {
				TrueUpComparisonImporterIngestionDetail missingrow = new TrueUpComparisonImporterIngestionDetail();
				missingrow.setPeriod(month);
				missingrow.setPeriodTotal("NA");
				missingrow.setPeriodEntryDateCategory("SubTotal");
				missingrow.setPeriodEntryItemCount(0);
				missingrow.setPeriodDetails(null);
				sorteddetails.add(missingrow);
			}
		}
		return sorteddetails;

	}

	@Override
	public List<TrueUpComparisonImporterIngestionHTSCodeSummary> getImporterComparisonIngestionHTSCodeSummary(
			 String ein, long fiscalYear, long quarter, String permitType, String tobaccoClass) {
		String nonCigarByQuarterCondition = "";
		if ("ROLL YOUR OWN".equalsIgnoreCase(tobaccoClass) || "RYO".equalsIgnoreCase(tobaccoClass)) {
			tobaccoClass = "Roll-Your-Own";
		}
		// For valid quarter - which is for non cigar tobacco classes
		if ((quarter > 0 && quarter < 5) && (!"CIGARS".endsWith(tobaccoClass.toUpperCase()))) {
			nonCigarByQuarterCondition = " AND FISCAL_QTR = :quarter ";
		}

		String sql = "" + "SELECT HTS_CD, " + "       Sum(ESTIMATED_TAX), " + "       Count(ESTIMATED_TAX) "
				+ "FROM   TU_CBP_ENTRY " + "WHERE  FISCAL_YEAR = :fiscalYear" + nonCigarByQuarterCondition
				+ "               AND CBP_IMPORTER_ID IN ( " + "                  SELECT a.CBP_IMPORTER_ID FROM "
				+ "                    (SELECT cbpi.CBP_IMPORTER_ID, "
				+ "                        (CASE WHEN cbpi.ASSOCIATION_TYPE_CD = 'IMPT' OR (cbpi.association_type_cd IS NULL AND cbpi.consignee_exists_flag = 'N') THEN cbpi.IMPORTER_EIN "
				+ "                         WHEN cbpi.ASSOCIATION_TYPE_CD = 'CONS' THEN cbpi.CONSIGNEE_EIN "
				+ "                         ELSE '' END) AS CompanyEIN, "
				+ "                        cbpi.ASSOCIATION_TYPE_CD " + "                    FROM TU_CBP_IMPORTER cbpi "
				+ "                    WHERE cbpi.FISCAL_YR = :fiscalYear"
				+ " AND cbpi.ASSOCIATION_TYPE_CD IN ('IMPT', 'CONS')  OR (cbpi.association_type_cd IS NULL AND cbpi.consignee_exists_flag = 'N')" + "                  )  a "
				//+ "                  INNER JOIN tu_company c " + "                  ON a.CompanyEIN = c.EIN "
				+ "                  WHERE  a.companyein = :ein              ) "
				+ "       AND ( ENTRY_SUMM_DT IS NOT NULL " + "              OR ENTRY_DT IS NOT NULL ) "
				+ "       AND ( EXCLUDED_FLAG IS NULL OR EXCLUDED_FLAG <> 'Y' )" + "       AND TOBACCO_CLASS_ID IN "
				+ "              (SELECT TOBACCO_CLASS_ID " + "                FROM TU_TOBACCO_CLASS "
				+ "                WHERE UPPER(TOBACCO_CLASS_NM) = :tobaccoUpper" + "              ) "
				+ "GROUP  BY HTS_CD " + "ORDER  BY HTS_CD";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("tobaccoUpper", tobaccoClass.toUpperCase());
		query.setLong("fiscalYear", fiscalYear);
		query.setString("ein", ein);
		// For valid quarter - which is for non cigar tobacco classes
		if ((quarter > 0 && quarter < 5) && (!"CIGARS".endsWith(tobaccoClass.toUpperCase()))) {
			query.setLong("quarter", quarter);
		}
		List<Object[]> trueupComparisonSQLData = query.list();

		// String[] fieldsArray = { "HTS_CD:0", "ESTIMATED_TAX:1",
		// "ESTIMATED_TAX:2" };
		// Arrays.asList(fieldsArray).indexOf("ASSIGNED_DT");

		// Add Data Row
		List<TrueUpComparisonImporterIngestionHTSCodeSummary> details = new ArrayList<>();
		for (Object[] obj : trueupComparisonSQLData) {
			if (obj[0] != null) {
				TrueUpComparisonImporterIngestionHTSCodeSummary p = new TrueUpComparisonImporterIngestionHTSCodeSummary();
				p.setTobaccoClass(tobaccoClass);
				if (obj[0] != null) {
					String htsCode = obj[0].toString().trim().toUpperCase();
					p.setHtsCode(htsCode);
				}
				if (obj[1] != null) {
					p.setTaxAmount(((BigDecimal) obj[1]).doubleValue());
				}
				if (obj[2] != null) {
					p.setLineCount(((BigDecimal) obj[2]).intValue());
				}
				details.add(p);
			}
		}

		return details;

	}

	// This condition includes both Cigars and Cigarettes
	@Override
	public List<TrueUpComparisonManufacturerDetail> getCigarManufacturerComparisonDetails(long companyId,
			long fiscalYear, long quarter, String permitType, String tobaccoClass) {
		if (!this.isTobaccoClassStringValid(tobaccoClass))
			return null;

		//String ByQuarterCondition = "";
		//ByQuarterCondition = " rp.quarter = :qtr AND ";

		String sql= "SELECT "+
			    "quarter, "+
			    "permit_num,"+
			    "permitid, "+
			    "reportstatus, "+
			    "month, "+
			    "periodid, "+
			    "estimatedtax, "+
			    "ttbtaxespaid, "+
			    "ttb5000_rec_cnt, "+
			    "ingested_thresh_cnt, "+
			    "( "+
			        "SELECT DISTINCT "+
			            "LISTAGG(document_id"+
			                    "|| ':'"+
			                    "|| TO_CHAR(created_dt, 'MM/DD/YYYY'), ' ; ') WITHIN GROUP( "+
			                "ORDER BY "+
			                    "document_id "+
			            ") OVER( "+
			             "   PARTITION BY permit_id, period_id "+
			            ") AS aggdocumnet_id "+
			        "FROM "+
			         "   tu_document "+
			        "WHERE "+
			         "   permit_id = permitid "+
			          "  AND period_id = periodid "+
			           " AND instr(form_types, 'FDA 3852') > 0 "+
			    ") doc_list ," + 
			    "    estimatedvol," + 
			    "    pounds,  product "+
			"FROM "+
			"    ( "+
			"        SELECT "+
			"            a.* "+
			"        FROM "+
			"            ( "+
			"                SELECT "+
			"                    quarter, "+
			"                    permit_num, "+
			"                    permitid, "+
			"                    reportstatus, "+
			"                    month, "+
			"                    periodid, "+
			"                    DECODE(zero_flag, 'Y', 0, tobaccoclass_fda3852taxamount) AS estimatedtax,  "+
			"                    SUM(ttbtaxpaid) AS ttbtaxespaid,  "+
			"                   TO_CHAR(ttb5000_rec_cnt) AS ttb5000_rec_cnt,  "+
			"                    TO_CHAR(ingested_thresh_cnt) AS ingested_thresh_cnt," + 
			"    				tobaccoclass_fda3852removalqty estimatedvol," + 
			"    				pounds, product "+
			"                FROM  "+
			"                    (  "+
	
			"                        SELECT "+
			"                            nvl(rp.quarter, ttbingested.ingested_quarter) AS quarter, "+
			"                            nvl(p.permit_num, ttbingested.permit_num) AS permit_num, "+
			"                            nvl(ps.permit_id, ttbingested.ingested_permit_id) AS permitid, "+
			"                            nvl(detail.tobacco_class_nm, ttbingested.tobacco_class_nm) AS tobacco_class_nm, "+
			"                            ps.period_status_type_cd     AS reportstatus, "+
			"                            nvl(rp.month, ttbingested.ingested_month) AS month, "+
			"                            ps.period_id                 AS periodid, "+
			"                            DECODE(pp.zero_flag, 'Y', 0, detail.taxes_paid) AS fda3852taxamount, "+
			"                            ttbingested.ttb_taxes_paid   AS ttbtaxpaid, "+
			"                            detail.ttb5000_rec_cnt, "+
			"                            ttbingested.ingested_thresh_cnt, "+
			"                            pp.zero_flag, "+
			"                            nvl(a.fiscal_yr, ttbingested.ingested_fiscal_yr) AS fiscal_yr," + 
			"            				removal_qty AS fda3852removalqty," + 
			"            				ttbview.ttb_volume           AS pounds, ttbview.ttb_product as product "+
			"                        FROM "+
			"                            tu_annual_trueup   a "+
			"                            INNER JOIN tu_rpt_period      rp ON ( a.fiscal_yr = rp.fiscal_yr ) "+
			"                            INNER JOIN tu_permit_period   pp ON rp.period_id = pp.period_id "+
			"                            INNER JOIN tu_period_status   ps ON ( pp.permit_id = ps.permit_id "+
			"                                                                AND pp.period_id = ps.period_id ) "+
			"                            INNER JOIN tu_permit          p ON pp.permit_id = p.permit_id "+
			"                            INNER JOIN tu_company         c ON p.company_id = c.company_id" + 
			"            				LEFT OUTER JOIN tu_ttb_volume_vw   ttbview ON rp.month = ttbview.month AND " +
			"							rp.quarter = ttbview.qtr AND rp.fiscal_yr = ttbview.fiscal_yr" + 
			"                                            AND ttbview.id_permit = p.permit_num AND ttbview.ein = c.ein"+
			"                            LEFT OUTER JOIN ( "+
			"                                SELECT DISTINCT "+
			"                                    fd3852.permit_id, "+
			"                                    fd3852.period_id, "+
			"                                    fd3852.tobacco_class_id, "+
			"                                    fd3852.removal_qty, "+
			"                                    fd3852.taxes_paid, "+
			"                                    fd3852.tobacco_class_nm, "+
			"                                    fd3852.parent_class_id, "+
			"                                    fd5000.ttb5000_rec_cnt "+
			"                                FROM "+
			"                                    ( "+
			"                                        SELECT "+
			"                                            sf.permit_id, "+
			"                                            sf.period_id, "+
			"                                            fd.tobacco_class_id,"+
			"                                            fd.removal_qty,"+
			"                                            fd.taxes_paid,"+
			"                                            tc.tobacco_class_nm,"+
			"                                            DECODE(tc.parent_class_id, NULL, fd.tobacco_class_id, tc.parent_class_id) AS parent_class_id"+
			"                                        FROM "+
			"                                            tu_submitted_form   sf,"+
			"                                            tu_form_detail      fd,"+
			"                                            tu_tobacco_class    tc "+
			"                                        WHERE "+                                            
			"											sf.form_id = fd.form_id "+
			"                                            AND fd.tobacco_class_id = tc.tobacco_class_id "+
			"                                            AND sf.form_type_cd IN ( "+
			"                                                '3852' "+
			"                                            ) "+
			"                                    ) fd3852 "+
			"                                    LEFT JOIN ( "+
			"                                        SELECT "+
			"                                            sf.permit_id, "+
			"                                            sf.period_id, "+
			"                                            fd.tobacco_class_id, "+
			"                                            COUNT(*) OVER( "+
			"                                                PARTITION BY sf.permit_id, sf.period_id, fd.tobacco_class_id "+
			"                                            ) AS ttb5000_rec_cnt "+
			"                                        FROM "+
			"                                            tu_submitted_form   sf, "+
			"                                            tu_form_detail      fd, "+
			"                                            tu_tobacco_class    tc "+
			"                                        WHERE "+
			"                                            sf.form_id = fd.form_id "+
			"                                            AND fd.tobacco_class_id = tc.tobacco_class_id "+
			"                                            AND sf.form_type_cd IN ( "+
			"                                                '5024' "+
			"                                            ) "+
			"                                            AND fd.line_num > 0 "+
			"											 AND fd.taxes_paid IS NOT NULL "+
			"                                    ) fd5000 ON fd3852.permit_id = fd5000.permit_id "+
			"                                                AND fd3852.period_id = fd5000.period_id "+
			"                                                AND fd3852.parent_class_id = fd5000.tobacco_class_id "+
			"                            ) detail ON ( detail.permit_id = pp.permit_id "+
			"                                          AND detail.period_id = pp.period_id ) "+
			"                                        AND detail.tobacco_class_nm = :tobaccoclassnm "+
			"                            FULL OUTER JOIN ( "+
			"                                SELECT "+
			"                                    ttbcmpy.ein_num, "+
			"                                    ttbpermit.permit_num, "+
			"                                    SUM(ttbannualtx.ttb_taxes_paid) AS ttb_taxes_paid, "+
			"                                    MAX( "+
			"                                        CASE "+
			"                                            WHEN to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) <= 7 THEN "+
			"                                                1 "+
			"                                            ELSE "+
			"                                                0 "+
			"                                        END "+
			"                                    ) + MAX( "+
			"                                        CASE "+
			"                                            WHEN to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) >= 8 "+
			"                                                 AND to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) <= 21 THEN "+
			"                                                1 "+
			"                                            ELSE "+
			"                                                0 "+
			"                                        END "+
			"                                    ) + MAX( "+
			"                                        CASE "+
			"                                            WHEN to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) >= 22 "+
			"                                                 AND to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) <= 31 THEN "+
			"                                                1 "+
			"                                            ELSE "+
			"                                                0 "+
			"                                        END "+
			"                                    ) AS ingested_thresh_cnt, "+
			"                                    ttbannualtx.tobacco_class_id, "+
			"                                    ttc.tobacco_class_nm, "+
			"                                    ttbannualtx.month              AS ingested_month, "+
			"                                    ttbcmpy.fiscal_yr              AS ingested_fiscal_yr, "+
			"                                    ttbannualtx.ttb_calendar_qtr   AS ingested_quarter, "+
			"                                    ttbtc.company_id               AS ingested_company_id, "+
			"                                    ttbtp.permit_type_cd           AS ingested_permit_type_cd, "+
			"                                    ttbtp.permit_id                AS ingested_permit_id "+
			"                                FROM "+
			"                                    tu_ttb_company      ttbcmpy "+
			"                                    INNER JOIN tu_ttb_permit       ttbpermit ON ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id "+
			"                                    INNER JOIN tu_ttb_annual_tax   ttbannualtx ON ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id "+
			"                                    INNER JOIN tu_tobacco_class    ttc ON ttbannualtx.tobacco_class_id = ttc.tobacco_class_id "+
			"                                    INNER JOIN tu_company          ttbtc ON ttbcmpy.ein_num = ttbtc.ein "+
			"                                    INNER JOIN tu_permit           ttbtp ON ttbtp.permit_num = ttbpermit.permit_num "+
			"                                                                  AND ttbtp.company_id = ttbtc.company_id "+
			"                                WHERE "+
			"                                    ttbcmpy.fiscal_yr = :fiscalYr "+
			"                                    AND ttbannualtx.ttb_calendar_qtr = :qtr "+
			"                                GROUP BY "+
			"                                    ttbcmpy.ein_num, "+
			"                                    ttbpermit.permit_num, "+
			"                                    ttbannualtx.tobacco_class_id, "+
			"                                    ttc.tobacco_class_nm, "+
			"                                    ttbannualtx.month, "+
			"                                    ttbcmpy.fiscal_yr, "+
			"                                    ttbannualtx.ttb_calendar_qtr, "+
			"                                    ttbtc.company_id, "+
			"                                    ttbtp.permit_type_cd, "+
			"                                    ttbtp.permit_id "+
			"                            ) ttbingested ON ttbingested.ein_num = c.ein "+
			"                                             AND rp.month = ttbingested.ingested_month "+
			"                                             AND p.permit_num = ttbingested.permit_num "+
			"                                             AND ttbingested.tobacco_class_nm = :tobaccoclassnm "+
			"                                             AND ttbingested.ingested_fiscal_yr = a.fiscal_yr "+
			"                        WHERE "+
			"                            nvl(a.fiscal_yr, ttbingested.ingested_fiscal_yr) = :fiscalYr "+
			"                            AND nvl(rp.quarter, ttbingested.ingested_quarter) = :qtr "+
			"                            AND nvl(c.company_id, ttbingested.ingested_company_id) = :companyid "+
			"                            AND nvl(p.permit_type_cd, ttbingested.ingested_permit_type_cd) = :permittypecd "+
			" 							 AND nvl(detail.tobacco_class_nm, nvl(ttbingested.tobacco_class_nm,:tobaccoclassnm)) = :tobaccoclassnm "+
	
			"                    ) PIVOT ( "+
			"                        SUM ( fda3852taxamount ) "+
			"                    AS fda3852taxamount," + 
			"    				SUM (fda3852removalqty) AS fda3852removalqty "+
			"                        FOR ( tobacco_class_nm ) "+
			"                        IN ( '"+tobaccoClass+"' AS tobaccoclass ) "+
			"                    ) "+
			"                GROUP BY ( "+
			"                    quarter, "+
			"                    permit_num, "+
			"                    permitid, "+
			"                    reportstatus, "+
			"                    month, "+
			"                    periodid, "+
			"                    DECODE(zero_flag, 'Y', 0, tobaccoclass_fda3852taxamount), " + 
			"    				tobaccoclass_fda3852removalqty," + 
			"    				pounds, product, "+
			"                    ttb5000_rec_cnt, "+
			"                    ingested_thresh_cnt "+
			"                ) "+
			"                ORDER BY "+
			"                    permit_num "+
			"            ) a "+
			"        UNION ALL "+
			"        SELECT "+
			"            b.* "+
			"        FROM "+
			"            ( "+
		
			"                SELECT "+
			"                    quarter, "+
			"                    permit_num, "+
			"                    permitid, "+
			"                    'Sub Total', "+
			"                    '', "+
			"                    0, "+
			"                    SUM(DECODE(zero_flag, 'Y', 0, tobaccoclass_fda3852taxamount)) AS estimatedtax, "+
			"                    SUM(ttbtaxpaid) AS ttbtaxespaid, "+
			"                    'ttb5000_rec_cnt', "+
			"                    'ingested_thresh_cnt' ," + 
			"    				SUM(tobaccoclass_fda3852removalqty) estimatedvol," + 
			"    				pounds, product "+
			"                FROM "+
			"                    ( "+
			"                                SELECT "+
			"                                     nvl(rp.quarter, ttbingested.ingested_quarter) AS quarter, "+
			"                                     nvl(p.permit_num, ttbingested.permit_num) AS permit_num, "+
			"                                     nvl(ps.permit_id, ttbingested.ingested_permit_id) AS permitid, "+
			"                                     nvl(detail.tobacco_class_nm, ttbingested.tobacco_class_nm) AS tobacco_class_nm, "+
			"                                    ps.period_status_type_cd     AS reportstatus, "+
			"                                    nvl(rp.month, ttbingested.ingested_month) AS month, "+
			"                                    ps.period_id                 AS periodid, "+
			"                                    DECODE(pp.zero_flag, 'Y', 0, detail.taxes_paid) AS fda3852taxamount, "+
			"                                    ttbingested.ttb_taxes_paid   AS ttbtaxpaid, "+
			"                                    pp.zero_flag, "+
			"                                    nvl(a.fiscal_yr, ttbingested.ingested_fiscal_yr) AS fiscal_yr," + 
			"            						removal_qty                  fda3852removalqty," + 
			"            						ttbview.ttb_volume           AS pounds , ttbview.ttb_product as product "+
			"                                FROM "+
			"                                    tu_annual_trueup   a "+
			"                                    INNER JOIN tu_rpt_period      rp ON ( a.fiscal_yr = rp.fiscal_yr ) "+
			"                                    INNER JOIN tu_permit_period   pp ON rp.period_id = pp.period_id "+
			"                                    INNER JOIN tu_period_status   ps ON ( pp.permit_id = ps.permit_id "+
			"                                                                        AND pp.period_id = ps.period_id ) "+
			"                                    INNER JOIN tu_permit          p ON pp.permit_id = p.permit_id "+
			"                                    INNER JOIN tu_company         c ON p.company_id = c.company_id " + 
			"            						LEFT OUTER JOIN tu_ttb_volume_vw   ttbview ON rp.month = ttbview.month AND " +
			"									rp.quarter = ttbview.qtr AND rp.fiscal_yr = ttbview.fiscal_yr" + 
			"                                            AND ttbview.id_permit = p.permit_num AND ttbview.ein = c.ein"+
			"                                    LEFT OUTER JOIN ( "+
			"                                        SELECT "+
			"                                            sf.permit_id, "+
			"                                            sf.period_id, "+
			"                                            fd.tobacco_class_id, "+
			"                                            fd.removal_qty, "+
			"                                            fd.taxes_paid, "+
			"                                            tc.tobacco_class_nm, "+
			"                                            tc.parent_class_id "+
			"                                        FROM "+
			"                                            tu_submitted_form   sf, "+
			"                                            tu_form_detail      fd, "+
			"                                            tu_tobacco_class    tc "+
			"                                        WHERE "+
			"                                            sf.form_id = fd.form_id "+
			"                                            AND fd.tobacco_class_id = tc.tobacco_class_id "+
			"                                            AND sf.form_type_cd = '3852' "+
			"                                    ) detail ON ( detail.permit_id = pp.permit_id "+
			"                                                  AND detail.period_id = pp.period_id ) "+
			"                                                AND detail.tobacco_class_nm = :tobaccoclassnm "+
			"                                    FULL OUTER JOIN ( "+
			"                                        SELECT "+
			"                                            ttbcmpy.ein_num, "+
			"                                            ttbpermit.permit_num, "+
			"                                            SUM(ttbannualtx.ttb_taxes_paid) AS ttb_taxes_paid, "+
			"                                            ttbannualtx.tobacco_class_id, "+
			"                                            ttc.tobacco_class_nm, "+
			"                                            ttbannualtx.month AS ingested_month, "+
			"                                            ttbcmpy.fiscal_yr AS ingested_fiscal_yr, "+
			"                                            ttbannualtx.ttb_calendar_qtr   AS ingested_quarter, "+
			"                                            ttbtc.company_id               AS ingested_company_id, "+
			"                                            ttbtp.permit_type_cd           AS ingested_permit_type_cd, "+
			"                                            ttbtp.permit_id                AS ingested_permit_id "+
			"                                        FROM "+
			"                                            tu_ttb_company      ttbcmpy "+
			"                                            INNER JOIN tu_ttb_permit       ttbpermit ON ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id "+
			"                                            INNER JOIN tu_ttb_annual_tax   ttbannualtx ON ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id "+
			"                                            INNER JOIN tu_tobacco_class    ttc ON ttbannualtx.tobacco_class_id = ttc.tobacco_class_id "+
			"                                            INNER JOIN tu_company          ttbtc ON ttbcmpy.ein_num = ttbtc.ein "+
			"                                            INNER JOIN tu_permit           ttbtp ON ttbtp.permit_num = ttbpermit.permit_num "+
			"                                                                                    AND ttbtp.company_id = ttbtc.company_id "+
			"                                        WHERE "+
			"                                            ttbcmpy.fiscal_yr = :fiscalYr "+
			"                                            AND ttbannualtx.ttb_calendar_qtr = :qtr "+
			"                                        GROUP BY "+
			"                                            ttbcmpy.ein_num, "+
			"                                            ttbpermit.permit_num, "+
			"                                            ttbannualtx.tobacco_class_id, "+
			"                                            ttc.tobacco_class_nm, "+
			"                                            ttbannualtx.month, "+
			"                                            ttbcmpy.fiscal_yr, "+
			"                                            ttbannualtx.ttb_calendar_qtr, "+
			"                                            ttbtc.company_id, "+
			"                                            ttbtp.permit_type_cd, "+
			"                                            ttbtp.permit_id "+
			"                                    ) ttbingested ON ttbingested.ein_num = c.ein "+
			"                                                     AND p.permit_num = ttbingested.permit_num "+
			"                                                     AND rp.month = ttbingested.ingested_month "+
			"                                                     AND ttbingested.tobacco_class_nm = :tobaccoclassnm "+
			"                                                     AND ttbingested.ingested_fiscal_yr = a.fiscal_yr "+
			"                                WHERE "+
			"                                 nvl(a.fiscal_yr, ttbingested.ingested_fiscal_yr) = :fiscalYr "+
			"                                 AND nvl(rp.quarter, ttbingested.ingested_quarter) = :qtr "+
			"                                 AND nvl(c.company_id, ttbingested.ingested_company_id) = :companyid "+
			"                                 AND nvl(p.permit_type_cd, ttbingested.ingested_permit_type_cd) = :permittypecd "+
			" 							 AND nvl(detail.tobacco_class_nm, nvl(ttbingested.tobacco_class_nm, :tobaccoclassnm)) = :tobaccoclassnm "+
			"                    ) PIVOT ( "+
			"                        SUM ( fda3852taxamount ) "+
			"                    AS fda3852taxamount ," + 
			"    				SUM (fda3852removalqty) AS fda3852removalqty"+
			"                        FOR ( tobacco_class_nm ) "+
			"                        IN ( '"+tobaccoClass+"' AS tobaccoclass ) "+
			"                    ) "+
			"                GROUP BY "+
			"                    quarter, "+
			"                    permit_num, "+
			"                    permitid "+
			"                ORDER BY "+
			"                    quarter, "+
			"                    permit_num "+
		
			"            ) b "+
			"    ) "+
			"ORDER BY "+
			"    quarter, "+
			"    permit_num, "+
			"    TO_DATE(month, 'MON') ASC, "+
			"    reportstatus ASC";

		System.out.println(sql);
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setLong("fiscalYr", fiscalYear);
		query.setLong("qtr", quarter);
		query.setLong("companyid", companyId);
		query.setString("tobaccoclassnm", tobaccoClass);
		query.setString("permittypecd", permitType);
		
		List<Object[]> trueupComparisonSQLData = query.list();

		// Add Data Row
		List<TrueUpComparisonManufacturerDetail> details = new ArrayList<>();
		TrueUpComparisonManufacturerDetail row = new TrueUpComparisonManufacturerDetail();
		for (Object[] obj : trueupComparisonSQLData) {
			//if (obj[3] != null) {
				//String periodStatus = obj[3].toString();
				if (obj[3]!= null && "Sub Total".equals(obj[3].toString())) {
					if (obj[0] != null) {
						int rptquarter = (((BigDecimal) obj[0]).intValue());
						row.setQuarter(rptquarter);
					}
					if (obj[1] != null) {
						String permitNum = obj[1].toString();
						row.setPermit(permitNum);
					}
					if (obj[2] != null) {
						long permitId = (((BigDecimal) obj[2]).intValue());
						row.setPermitId(permitId);
					}
					if (obj[6] != null) {
						row.setPeriodTotal(obj[6].toString());
					}
					if (obj[6] == null) {
						row.setPeriodTotal("NA");
					}

					if (obj[7] != null) {
						row.setIngestedTax(obj[7].toString());
					}
					if (obj[7] == null) {
						row.setIngestedTax("NA");

					}

					details.add(row);
					// construct new row;
					row = new TrueUpComparisonManufacturerDetail();
				} else {
					TrueUpComparisonManufacturerDetailPermit p = new TrueUpComparisonManufacturerDetailPermit();

					String rptStatus = "";
					//if (obj[3] != null) {
						//rptStatus = obj[3].toString();
						if (obj[3]!=null && "NSTD".equalsIgnoreCase(obj[3].toString())) {
							p.setStatus("NS");
						} else if (obj[3]!=null && "ERRO".equalsIgnoreCase(obj[3].toString())) {
							p.setStatus("E");
						} else if (obj[3]!=null && "COMP".equalsIgnoreCase(obj[3].toString())) {
							p.setStatus("C");
						} else {
							p.setStatus("");
						}
					//}
					if (obj[4] != null) {
						String rptMonth = obj[4].toString();
						p.setMonth(rptMonth);
					}
					if (obj[5] != null) {
						long periodId = (((BigDecimal) obj[5]).intValue());
						p.setPeriodId(periodId);
					}
					if (obj[6] != null) {

						p.setTaxAmount(obj[6].toString());

					}
					if (obj[6] == null) {
						p.setTaxAmount("NA");
					}

					if (obj[7] != null) {

						p.setIngestedTax(obj[7].toString());

					}
					if (obj[7] == null) {
						p.setIngestedTax("NA");
					}

					if (obj[8] != null)
						p.setTtb5000Cnt(obj[8].toString());

					if (obj[9] != null)
						p.setIngestedThreshCnt(obj[9].toString());

					if (obj[10] != null) {
						// Populate attachment Ids. Doc Ids are for 3852 for
						// now.
						String docStr = (String) obj[10];
						String[] docIds = docStr.split(";");
						List<Attachment> attachments = new ArrayList<>();
						for (String docId : docIds) {
							Attachment attach = new Attachment();
							String[] docCreateDt = docId.split(":");
							attach.setDocumentId(Long.parseLong(docCreateDt[0].trim()));
							attach.setCreatedDt(CTPUtil.parseStringToDate(docCreateDt[1].trim()));
							attach.setFormTypes("FDA 3852");
							attachments.add(attach);
						}
						p.setAttachments(attachments);
					}
					if (obj[11] != null) {

						p.setTobaccoclass1Tufattbvolume(obj[11].toString());

					}
					if (obj[11] == null) {
						p.setTobaccoclass1Tufattbvolume("NA");
					}
					
					
					p.setTobaccoClass1TTBvolume("NA");
					if (obj[12] != null) {

						if (obj[13] != null) {

							List<String> poundsdata = Arrays.asList(obj[12].toString().split(","));
							List<String> productdata = Arrays.asList(obj[13].toString().split(","));
							
							double poundsData = 0;
							int i = -1;
							if (tobaccoClass.equalsIgnoreCase("Cigars")) {
								if (productdata.contains("Large Cigars") || productdata.contains("Small Cigars") || productdata.contains("Cigars")) {
									for(String product : productdata) {
										if ("Large Cigars".equals(product) || "Small Cigars".equals(product) || "Cigars".equals(product)) {
											i = productdata.indexOf(product);
											poundsData = (poundsdata.get(i) == null ? 0 : Double.parseDouble(poundsdata.get(i))) + poundsData;											
										}
									}
									
									if(i == -1)
										p.setTobaccoClass1TTBvolume("NA");
									else
										p.setTobaccoClass1TTBvolume(Double.toString(poundsData));
								}
							}
							else
								p.setTobaccoClass1TTBvolume("NA");

						}
					}
					row.getPermitDetails().add(p);
				}
		}

		return details;
	}

	@Override
	public List<TrueUpComparisonManufacturerDetail> getNonCigarManufacturerComparisonDetails(long companyId,
			long fiscalYear, long quarter, String permitType, String tobaccoClass) {

		String tobaccoClass1 = "";
		String tobaccoClass2 = "";
		String parentTobaccoClass = "";

		
		String nonCigartobaccoClassCondition = "";
		if(tobaccoClass.toUpperCase().equalsIgnoreCase("PIPE-ROLL-YOUR-OWN"))
			tobaccoClass="PIPE-ROLL YOUR OWN";
		

		switch (tobaccoClass.toUpperCase()) {
		case "CHEW-SNUFF": {
			// numColumns = 3;
			tobaccoClass1 = "Chew";
			tobaccoClass2 = "Snuff";
			nonCigartobaccoClassCondition = " detail.tobacco_class_nm IN ('Chew', 'Snuff') ";
			parentTobaccoClass = "Chew-and-Snuff";
			break;
		}

		case "PIPE-ROLL YOUR OWN": {
			// numColumns = 3;
			tobaccoClass1 = "Pipe";
			tobaccoClass2 = "Roll-Your-Own";
			nonCigartobaccoClassCondition = " detail.tobacco_class_nm IN ('Pipe', 'Roll-Your-Own') ";
			parentTobaccoClass = "Pipe-RYO";
			break;
		}

		case "CIGARETTES": {
			// numColumns = 3;
			tobaccoClass1 = "Cigarettes";
//			tobaccoClass2 = "Cigarettes";
			nonCigartobaccoClassCondition = " detail.tobacco_class_nm = 'Cigarettes' ";
			parentTobaccoClass = "Cigarettes";
			break;
		}

		case "CIGARS": {
			// numColumns = 3;
			tobaccoClass1 = "Cigars";
//			tobaccoClass2 = "Roll-Your-Own";
			nonCigartobaccoClassCondition = " detail.tobacco_class_nm = 'Cigars' ";
			parentTobaccoClass = "Cigars";
			break;
		}

		default:
			break;
		}

		String sql =  "SELECT "+
						"    quarter, "+
						"    permit_num, "+
						"    permitid, "+
						"    reportstatus, "+
						"    month, "+
						"    periodid, "+
						"    ttbtaxespaid, "+
						"    DECODE(zero_flag, 'Y', 0, estimatedtax1) AS estimatedtax1, "+
						"    DECODE(zero_flag, 'Y', 0, estimatedtax2) AS estimatedtax2, "+
						"    DECODE(zero_flag, 'Y', 0, estimatedtaxtotal) AS estimatedtaxtotal, "+
						"    pounds, "+
						"    product, "+
						"    tax_rate, "+
						"    x, "+
						"    y, "+
						"    ( "+
						"        SELECT DISTINCT "+
						"            LISTAGG(document_id "+
						"                    || ':' "+
						"                    || TO_CHAR(created_dt, 'MM/DD/YYYY'), ' ; ') WITHIN GROUP( "+
						"                ORDER BY "+
						"                    document_id "+
						"            ) OVER( "+
						"                PARTITION BY permit_id, period_id "+
						"            ) AS aggdocumnet_id "+
						"        FROM "+
						"            tu_document "+
						"        WHERE "+
						"            permit_id = permitid "+
						"            AND period_id = periodid "+
						"            AND instr(form_types, 'FDA 3852') > 0 "+
						"    ) doc_list,"
						+ "tobaccoclass1_fda3852pounds," + 
						"    tobaccoclass2_fda3852pounds," + 
						"    estimatedpounds "+
						"FROM "+
						"    ( "+
						"        SELECT "+
						"            a.* "+
						"        FROM "+
						"            ( "+
						"                SELECT "+
						"                    quarter, "+
						"                    permit_num, "+
						"                    permitid, "+
						"                    reportstatus, "+
						"                    month, "+
						"                    periodid, "+
						"                    SUM(ttbtaxpaid) AS ttbtaxespaid, "+
						"                    tobaccoclass1_fda3852taxamount   AS estimatedtax1, "+
						"                    tobaccoclass2_fda3852taxamount   AS estimatedtax2, "+
						"                    coalesce((SUM(tobaccoclass1_fda3852taxamount + tobaccoclass2_fda3852taxamount)), SUM(tobaccoclass1_fda3852taxamount "+
						"                    ), SUM(tobaccoclass2_fda3852taxamount)) AS estimatedtaxtotal, "+
						"                    pounds, "+
						"                    product, "+
						"                    tax_rate, "+
						"                    TO_CHAR(ttb5000_rec_cnt) AS x, "+
						"                    TO_CHAR(ingested_thresh_cnt) AS y, "+
						"                    zero_flag," + 
						"					 tobaccoclass1_fda3852pounds," + 
						"                    tobaccoclass2_fda3852pounds," + 
						"                    coalesce((SUM(tobaccoclass1_fda3852pounds + tobaccoclass2_fda3852pounds)), SUM(tobaccoclass1_fda3852pounds), SUM(tobaccoclass2_fda3852pounds)) AS estimatedpounds "+
						"                FROM "+
						"                    ( "+
						"                        SELECT "+
						"                            nvl(rp.quarter, ttbingested.ingested_quarter) AS quarter, "+
						"                            nvl(p.permit_num, ttbingested.permit_num) AS permit_num, "+
						"                            nvl(ps.permit_id, ttbingested.ingested_permit_id) AS permitid, "+
						"                            nvl(detail.tobacco_class_nm, ttbingested.tobacco_class_nm) AS tobacco_class_nm, "+
						"                            nvl(a.fiscal_yr, ttbingested.ingested_fiscal_yr) AS fiscal_yr, "+
						"                            ps.period_status_type_cd     AS reportstatus, "+
						"                            nvl(rp.month, ttbingested.ingested_month) AS month, "+
						"                            ps.period_id                 AS periodid, "+
						"                            detail.taxes_paid            AS fda3852taxamount, "+
						"                            ttbingested.ttb_taxes_paid   AS ttbtaxpaid, "+
						"                            ttbview.ttb_volume           AS pounds, "+
						"                            ttbview.ttb_product          AS product, "+
						"                            ttbview.tax_rate, "+
						"                            detail.ttb5000_rec_cnt, "+
						"                            ttbingested.ingested_thresh_cnt, "+
						"                            pp.zero_flag, "+
						"                            ttbingested.ingested_permit_type_cd, " +
						"							 detail.removal_qty          AS fda3852pounds"+
						"                        FROM "+
						"                            tu_annual_trueup   a "+
						"                            INNER JOIN tu_rpt_period      rp ON ( a.fiscal_yr = rp.fiscal_yr ) "+
						"                            INNER JOIN tu_permit_period   pp ON rp.period_id = pp.period_id "+
						"                            INNER JOIN tu_period_status   ps ON ( pp.permit_id = ps.permit_id "+
						"                                                                AND pp.period_id = ps.period_id ) "+
						"                            INNER JOIN tu_permit          p ON pp.permit_id = p.permit_id "+
						"                            INNER JOIN tu_company         c ON p.company_id = c.company_id "+
						"                            LEFT OUTER JOIN tu_ttb_volume_vw   ttbview ON rp.month = ttbview.month "+
						"                                                                        AND rp.quarter = ttbview.qtr "+
						"                                                                        AND rp.fiscal_yr = ttbview.fiscal_yr "+
						"                                                                        AND ttbview.id_permit = p.permit_num "+
						"                                                                        AND ttbview.ein = c.ein "+
						"                            LEFT OUTER JOIN ( "+
						"                                SELECT DISTINCT "+
						"                                    fd3852.permit_id, "+
						"                                    fd3852.period_id, "+
						"                                    fd3852.tobacco_class_id, "+
						"                                    fd3852.removal_qty, "+
						"                                    fd3852.taxes_paid, "+
						"                                    fd3852.tobacco_class_nm, "+
						"                                    fd3852.parent_class_id, "+
						"                                    fd5000.ttb5000_rec_cnt "+
						"                                FROM "+
						"                                    ( "+
						"                                        SELECT "+
						"                                            sf.permit_id, "+
						"                                            sf.period_id, "+
						"                                            fd.tobacco_class_id, "+
						"                                            fd.removal_qty, "+
						"                                            fd.taxes_paid, "+
						"                                            tc.tobacco_class_nm, "+
						"                                            DECODE(tc.parent_class_id, NULL, fd.tobacco_class_id, tc.parent_class_id) AS parent_class_id "+
						"                                        FROM "+
						"                                            tu_submitted_form   sf, "+
						"                                            tu_form_detail      fd, "+
						"                                            tu_tobacco_class    tc "+
						"                                        WHERE "+
						"                                            sf.form_id = fd.form_id "+
						"                                            AND fd.tobacco_class_id = tc.tobacco_class_id "+
						"                                            AND sf.form_type_cd IN ( "+
						"                                                '3852' "+
						"                                            ) "+
						"                                    ) fd3852 "+
						"                                    LEFT JOIN ( "+
						"                                        SELECT "+
						"                                            sf.permit_id, "+
						"                                            sf.period_id, "+
						"                                            fd.tobacco_class_id, "+
						"                                            COUNT(*) OVER( "+
						"                                                PARTITION BY sf.permit_id, sf.period_id, fd.tobacco_class_id "+
						"                                            ) AS ttb5000_rec_cnt "+
						"                                        FROM "+
						"                                            tu_submitted_form   sf, "+
						"                                            tu_form_detail      fd, "+
						"                                            tu_tobacco_class    tc "+
						"                                        WHERE "+
						"                                            sf.form_id = fd.form_id "+
						"                                            AND fd.tobacco_class_id = tc.tobacco_class_id "+
						"                                            AND sf.form_type_cd IN ( "+
						"                                                '5024' "+
						"                                            ) "+
						"                                            AND fd.line_num > 0 "+
						"											 AND fd.taxes_paid IS NOT NULL "+
						"                                    ) fd5000 ON fd3852.permit_id = fd5000.permit_id "+
						"                                                AND fd3852.period_id = fd5000.period_id "+
						"                                                AND fd3852.parent_class_id = fd5000.tobacco_class_id "+
						"                            ) detail ON ( detail.permit_id = pp.permit_id "+
						"                                          AND detail.period_id = pp.period_id ) AND "+
						                                        nonCigartobaccoClassCondition +

						"                            FULL OUTER JOIN ( "+
						"                                SELECT "+
						"                                    ttbcmpy.ein_num, "+
						"                                    ttbpermit.permit_num, "+
						"                                    SUM(ttbannualtx.ttb_taxes_paid) AS ttb_taxes_paid, "+
						"                                    MAX( "+
						"                                        CASE "+
						"                                            WHEN to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) <= 7 THEN "+
						"                                                1 "+
						"                                            ELSE "+
						"                                                0 "+
						"                                        END "+
						"                                    ) + MAX( "+
						"                                        CASE "+
						"                                            WHEN to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) >= 8 "+
						"                                                 AND to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) <= 21 THEN "+
						"                                                1 "+
						"                                            ELSE "+
						"                                                0 "+
						"                                        END "+
						"                                    ) + MAX( "+
						"                                        CASE "+
						"                                            WHEN to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) >= 22 "+
						"                                                 AND to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) <= 31 THEN "+
						"                                                1 "+
						"                                            ELSE "+
						"                                                0 "+
						"                                        END "+
						"                                    ) AS ingested_thresh_cnt, "+
						"                                    ttbannualtx.tobacco_class_id, "+
						"                                    ttc.tobacco_class_nm, "+
						"                                    ttbannualtx.month              AS ingested_month, "+
						"                                    ttbcmpy.fiscal_yr              AS ingested_fiscal_yr, "+
						"                                    ttbannualtx.ttb_calendar_qtr   AS ingested_quarter, "+
						"                                    ttbtc.company_id               AS ingested_company_id, "+
						"                                    ttbtp.permit_type_cd           AS ingested_permit_type_cd, "+
						"                                    ttbtp.permit_id                AS ingested_permit_id "+
						"                                FROM "+
						"                                    tu_ttb_company      ttbcmpy "+
						"                                    INNER JOIN tu_ttb_permit       ttbpermit ON ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id "+
						"                                    INNER JOIN tu_ttb_annual_tax   ttbannualtx ON ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id "+
						"                                    INNER JOIN tu_tobacco_class    ttc ON ttbannualtx.tobacco_class_id = ttc.tobacco_class_id "+
						"                                    INNER JOIN tu_company          ttbtc ON ttbcmpy.ein_num = ttbtc.ein "+
						"                                    INNER JOIN tu_permit           ttbtp ON ttbtp.permit_num = ttbpermit.permit_num "+
						"                                                                  AND ttbtp.company_id = ttbtc.company_id "+
						"                                WHERE "+
						"                                    ttbcmpy.fiscal_yr = :fiscalyr "+
						"                                    AND ttbannualtx.ttb_calendar_qtr = :qtr "+
						"                                GROUP BY "+
						"                                    ttbcmpy.ein_num, "+
						"                                    ttbpermit.permit_num, "+
						"                                    ttbannualtx.tobacco_class_id, "+
						"                                    ttc.tobacco_class_nm, "+
						"                                    ttbannualtx.month, "+
						"                                    ttbcmpy.fiscal_yr, "+
						"                                    ttbannualtx.ttb_calendar_qtr, "+
						"                                    ttbtc.company_id, "+
						"                                    ttbtp.permit_type_cd, "+
						"                                    ttbtp.permit_id "+
						"                            ) ttbingested ON ttbingested.ein_num = c.ein "+
						"                                             AND rp.month = ttbingested.ingested_month "+
						"                                             AND p.permit_num = ttbingested.permit_num "+
						"                                             AND ttbingested.tobacco_class_nm = :parenttobaccoclass "+
						"                                             AND ttbingested.ingested_fiscal_yr = a.fiscal_yr "+
						"                        WHERE "+
						"                            nvl(a.fiscal_yr, ttbingested.ingested_fiscal_yr) = :fiscalyr "+
						"                            AND nvl(rp.quarter, ttbingested.ingested_quarter) = :qtr "+
						"                            AND nvl(c.company_id, ttbingested.ingested_company_id) = :companyid "+
						"                            AND nvl(p.permit_type_cd, ttbingested.ingested_permit_type_cd) = :permittypecd "+
						"                            AND 1 = "+
						"                                CASE "+
						"                                    WHEN detail.taxes_paid IS NULL "+
						"                                         AND ttbingested.tobacco_class_nm = :parenttobaccoclass THEN 1"+
						"									 WHEN pp.zero_flag = 'Y' THEN 1"+							
						"                                    WHEN detail.taxes_paid IS NOT NULL  AND "+
																	nonCigartobaccoClassCondition+
						"                                          THEN 1"+
						"                                    ELSE 0"+
						"                                END "+
						"                    ) PIVOT ( "+
						"                        SUM ( fda3852taxamount ) " + 
						"                        AS fda3852taxamount, " +
						"					SUM ( fda3852pounds ) AS fda3852pounds"+
						"                        FOR ( tobacco_class_nm ) "+
						"                        IN ( '"+tobaccoClass1+"' AS tobaccoclass1, '"+tobaccoClass2+"' AS tobaccoclass2 ) "+
						"                    ) "+
						"                GROUP BY ( "+
						"                    quarter, "+
						"                    permit_num, "+
						"                    permitid, "+
						"                    reportstatus, "+
						"                    month, "+
						"                    periodid, "+
						"                    tobaccoclass1_fda3852taxamount, "+
						"                    tobaccoclass2_fda3852taxamount," + 
						"                    tobaccoclass1_fda3852pounds," + 
						"                    tobaccoclass2_fda3852pounds,"+
						"                    pounds, "+
						"                    product, "+
						"                    tax_rate, "+
						"                    ttb5000_rec_cnt, "+
						"                    ingested_thresh_cnt, "+
						"                    zero_flag "+
						"                ) "+
						"                ORDER BY "+
						"                    permit_num "+
						"            ) a "+
						"        UNION ALL "+
						"        SELECT "+
						"            b.quarter, "+
						"            b.permit_num, "+
						"            b.permitid, "+
						"            'Sub Total', "+
						"            '', "+
						"            0, "+
						"            b.ttbtaxespaid1, "+
						"            b.estimatedtax11, "+
						"            b.estimatedtax21, "+
						"            b.estimatedtaxtotal1, "+
						"            'Pounds', "+
						"            'PRODUCT', "+
						"            'tax_rate', "+
						"            'ttb5000_rec_cnt' AS x, "+
						"            'ingested_thresh_cnt' AS y, "+
						"            NULL," + 
						"            removalqty1," + 
						"            removalqty2," + 
						"            estimatedpounds "+
						"        FROM "+
						"            ( "+
						"                SELECT "+
						"                    quarter, "+
						"                    permit_num, "+
						"                    permitid, "+
						"                    'Sub Total', "+
						"                    'DEC', "+
						"                    0, "+
						"                    SUM(ttbtaxespaid) AS ttbtaxespaid1, "+
						"                    SUM(estimatedtax1) AS estimatedtax11, "+
						"                    SUM(estimatedtax2) AS estimatedtax21, "+
						"                    SUM(estimatedtaxtotal) AS estimatedtaxtotal1, "+
						"                    'Pounds', "+
						"                    'PRODUCT', "+
						"                    'tax_rate', "+
						"                    'ttb5000_rec_cnt' AS x, "+
						"                    'ingested_thresh_cnt' AS y, "+
						"                    NULL," + 
						"                    tobaccoclass1_fda3852pounds AS removalqty1," + 
						"                    tobaccoclass1_fda3852pounds AS removalqty2," + 
						"                    estimatedpounds "+
						"                FROM "+
						"                    ( "+
						"                        SELECT "+
						"                            quarter, "+
						"                            permit_num, "+
						"                            permitid, "+
						"                            reportstatus, "+
						"                            month, "+
						"                            periodid, "+
						"                            SUM(ttbtaxpaid) AS ttbtaxespaid, "+
						"                            DECODE(zero_flag, 'Y', 0, tobaccoclass1_fda3852taxamount) AS estimatedtax1, "+
						"                            DECODE(zero_flag, 'Y', 0, tobaccoclass2_fda3852taxamount) AS estimatedtax2, "+
						"                            DECODE(zero_flag, 'Y', 0, coalesce((SUM(tobaccoclass1_fda3852taxamount + tobaccoclass2_fda3852taxamount "+
						"                            )), SUM(tobaccoclass1_fda3852taxamount), SUM(tobaccoclass2_fda3852taxamount))) AS estimatedtaxtotal, "+
						"                            pounds, "+
						"                            product, "+
						"                            tax_rate, "+
						"                            TO_CHAR(ttb5000_rec_cnt) AS x, "+
						"                            TO_CHAR(ingested_thresh_cnt) AS y, "+
						"                            zero_flag," + 
						"                            tobaccoclass1_fda3852pounds," + 
						"                            tobaccoclass2_fda3852pounds," + 
						"                            coalesce((SUM(tobaccoclass1_fda3852pounds + tobaccoclass2_fda3852pounds)), SUM(tobaccoclass1_fda3852pounds), SUM(tobaccoclass2_fda3852pounds)) AS estimatedpounds "+
						"                        FROM "+
						"                            ( "+
						"                                SELECT "+
						"                                    nvl(rp.quarter, ttbingested.ingested_quarter) AS quarter, "+
						"                                    nvl(p.permit_num, ttbingested.permit_num) AS permit_num, "+
						"                                    nvl(ps.permit_id, ttbingested.ingested_permit_id) AS permitid, "+
						"                                    nvl(detail.tobacco_class_nm, ttbingested.tobacco_class_nm) AS tobacco_class_nm, "+
						"                                    nvl(a.fiscal_yr, ttbingested.ingested_fiscal_yr) AS fiscal_yr, "+
						"                                    ps.period_status_type_cd     AS reportstatus, "+
						"                                    nvl(rp.month, ttbingested.ingested_month) AS month, "+
						"                                    ps.period_id                 AS periodid, "+
						"                                    detail.taxes_paid            AS fda3852taxamount, "+
						"                                    ttbingested.ttb_taxes_paid   AS ttbtaxpaid, "+
						"                                    ttbview.ttb_volume           AS pounds, "+
						"                                    ttbview.ttb_product          AS product, "+
						"                                    ttbview.tax_rate, "+
						"                                    detail.ttb5000_rec_cnt, "+
						"                                    ttbingested.ingested_thresh_cnt, "+
						"                                    pp.zero_flag, "+
						"                                    ttbingested.ingested_permit_type_cd," + 
						"                                    detail.removal_qty          AS fda3852pounds"+
						"                                FROM "+
						"                                    tu_annual_trueup   a "+
						"                                    INNER JOIN tu_rpt_period      rp ON ( a.fiscal_yr = rp.fiscal_yr ) "+
						"                                    INNER JOIN tu_permit_period   pp ON rp.period_id = pp.period_id "+
						"                                    INNER JOIN tu_period_status   ps ON ( pp.permit_id = ps.permit_id "+
						"                                                                        AND pp.period_id = ps.period_id ) "+
						"                                    INNER JOIN tu_permit          p ON pp.permit_id = p.permit_id "+
						"                                    INNER JOIN tu_company         c ON p.company_id = c.company_id "+
						"                                    LEFT OUTER JOIN tu_ttb_volume_vw   ttbview ON rp.month = ttbview.month "+
						"                                                                                AND rp.quarter = ttbview.qtr "+
						"                                                                                AND rp.fiscal_yr = ttbview.fiscal_yr "+
						"                                                                                AND ttbview.id_permit = p.permit_num "+
						"                                                                                AND ttbview.ein = c.ein "+
						"                                    LEFT OUTER JOIN ( "+
						"                                        SELECT DISTINCT "+
						"                                            fd3852.permit_id, "+
						"                                            fd3852.period_id, "+
						"                                            fd3852.tobacco_class_id, "+
						"                                            fd3852.removal_qty, "+
						"                                            fd3852.taxes_paid, "+
						"                                            fd3852.tobacco_class_nm, "+
						"                                            fd3852.parent_class_id, "+
						"                                            fd5000.ttb5000_rec_cnt "+
						"                                        FROM "+
						"                                            ( "+
						"                                                SELECT "+
						"                                                    sf.permit_id, "+
						"                                                    sf.period_id, "+
						"                                                    fd.tobacco_class_id, "+
						"                                                    fd.removal_qty, "+
						"                                                    fd.taxes_paid, "+
						"                                                    tc.tobacco_class_nm, "+
						"                                                    DECODE(tc.parent_class_id, NULL, fd.tobacco_class_id, tc.parent_class_id) AS parent_class_id "+
						"                                                FROM "+
						"                                                    tu_submitted_form   sf, "+
						"                                                    tu_form_detail      fd, "+
						"                                                    tu_tobacco_class    tc "+
						"                                                WHERE "+
						"                                                    sf.form_id = fd.form_id "+
						"                                                    AND fd.tobacco_class_id = tc.tobacco_class_id "+
						"                                                    AND sf.form_type_cd IN ( "+
						"                                                        '3852' "+
						"                                                    ) "+
						"                                            ) fd3852 "+
						"                                            LEFT JOIN ( "+
						"                                                SELECT "+
						"                                                    sf.permit_id, "+
						"                                                    sf.period_id, "+
						"                                                    fd.tobacco_class_id, "+
						"                                                    COUNT(*) OVER( "+
						"                                                        PARTITION BY sf.permit_id, sf.period_id, fd.tobacco_class_id "+
						"                                                    ) AS ttb5000_rec_cnt "+
						"                                                FROM "+
						"                                                    tu_submitted_form   sf, "+
						"                                                    tu_form_detail      fd, "+
						"                                                    tu_tobacco_class    tc "+
						"                                                WHERE "+
						"                                                    sf.form_id = fd.form_id "+
						"                                                    AND fd.tobacco_class_id = tc.tobacco_class_id "+
						"                                                    AND sf.form_type_cd IN ( "+
						"                                                        '5024' "+
						"                                                    ) "+
						"                                                    AND fd.line_num > 0 "+
						"											 		 AND fd.taxes_paid IS NOT NULL "+
						"                                            ) fd5000 ON fd3852.permit_id = fd5000.permit_id "+
						"                                                        AND fd3852.period_id = fd5000.period_id "+
						"                                                        AND fd3852.parent_class_id = fd5000.tobacco_class_id "+
						"                                    ) detail ON ( detail.permit_id = pp.permit_id "+
						"                                                  AND detail.period_id = pp.period_id ) AND "+ nonCigartobaccoClassCondition +
						"                                    FULL OUTER JOIN ( "+
						"                                        SELECT "+
						"                                            ttbcmpy.ein_num, "+
						"                                            ttbpermit.permit_num, "+
						"                                            SUM(ttbannualtx.ttb_taxes_paid) AS ttb_taxes_paid, "+
						"                                            MAX( "+
						"                                                CASE "+
						"                                                    WHEN to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) <= 7 THEN "+
						"                                                        1 "+
						"                                                    ELSE "+
						"                                                        0 "+
						"                                                END "+
						"                                            ) + MAX( "+
						"                                                CASE "+
						"                                                    WHEN to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) >= 8 "+
						"                                                         AND to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) <= "+
						"                                                         21 THEN "+
						"                                                        1 "+
						"                                                    ELSE "+
						"                                                        0 "+
						"                                                END "+
						"                                            ) + MAX( "+
						"                                                CASE "+
						"                                                    WHEN to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) >= 22 "+
						"                                                         AND to_number(TO_CHAR(TO_DATE(ttbannualtx.tpbd, 'dd-MM-YYYY'), 'dd')) <= "+
						"                                                         31 THEN "+
						"                                                        1 "+
						"                                                    ELSE "+
						"                                                        0 "+
						"                                                END "+
						"                                            ) AS ingested_thresh_cnt, "+
						"                                            ttbannualtx.tobacco_class_id, "+
						"                                            ttc.tobacco_class_nm, "+
						"                                            ttbannualtx.month              AS ingested_month, "+
						"                                            ttbcmpy.fiscal_yr              AS ingested_fiscal_yr, "+
						"                                            ttbannualtx.ttb_calendar_qtr   AS ingested_quarter, "+
						"                                            ttbtc.company_id               AS ingested_company_id, "+
						"                                            ttbtp.permit_type_cd           AS ingested_permit_type_cd, "+
						"                                            ttbtp.permit_id                AS ingested_permit_id "+
						"                                        FROM "+
						"                                            tu_ttb_company      ttbcmpy "+
						"                                            INNER JOIN tu_ttb_permit       ttbpermit ON ttbpermit.ttb_company_id = ttbcmpy.ttb_company_id "+
						"                                            INNER JOIN tu_ttb_annual_tax   ttbannualtx ON ttbannualtx.ttb_permit_id = ttbpermit.ttb_permit_id "+
						"                                            INNER JOIN tu_tobacco_class    ttc ON ttbannualtx.tobacco_class_id = ttc.tobacco_class_id "+
						"                                            INNER JOIN tu_company          ttbtc ON ttbcmpy.ein_num = ttbtc.ein "+
						"                                            INNER JOIN tu_permit           ttbtp ON ttbtp.permit_num = ttbpermit.permit_num "+
						"                                                                          AND ttbtp.company_id = ttbtc.company_id "+
						"                                        WHERE "+
						"                                            ttbcmpy.fiscal_yr = :fiscalyr "+
						"                                            AND ttbannualtx.ttb_calendar_qtr = :qtr "+
						"                                        GROUP BY "+
						"                                            ttbcmpy.ein_num, "+
						"                                            ttbpermit.permit_num, "+
						"                                            ttbannualtx.tobacco_class_id, "+
						"                                            ttc.tobacco_class_nm, "+
						"                                            ttbannualtx.month, "+
						"                                            ttbcmpy.fiscal_yr, "+
						"                                            ttbannualtx.ttb_calendar_qtr, "+
						"                                            ttbtc.company_id, "+
						"                                            ttbtp.permit_type_cd, "+
						"                                            ttbtp.permit_id "+
						"                                    ) ttbingested ON ttbingested.ein_num = c.ein "+
						"                                                     AND rp.month = ttbingested.ingested_month "+
						"                                                     AND p.permit_num = ttbingested.permit_num "+
						"                                                     AND ttbingested.tobacco_class_nm = :parenttobaccoclass "+
						"                                                     AND ttbingested.ingested_fiscal_yr = a.fiscal_yr "+
						"                                WHERE "+
						"                                    nvl(a.fiscal_yr, ttbingested.ingested_fiscal_yr) = :fiscalyr "+
						"                                    AND nvl(rp.quarter, ttbingested.ingested_quarter) = :qtr "+
						"                                    AND nvl(c.company_id, ttbingested.ingested_company_id) = :companyid "+
						"                                    AND nvl(p.permit_type_cd, ttbingested.ingested_permit_type_cd) = :permittypecd "+
						"                                    AND 1 = "+
						"                                        CASE "+
						"                                            WHEN detail.taxes_paid IS NULL "+		
						"                                                 AND ttbingested.tobacco_class_nm = :parenttobaccoclass THEN 1"+
						"									 		 WHEN pp.zero_flag = 'Y' THEN 1"+	
						"                                            WHEN detail.taxes_paid IS NOT NULL AND"+ nonCigartobaccoClassCondition +
						"                                                 THEN 1"+ 
						"                                            ELSE 0"+
						"                                        END "+
						"                            ) PIVOT ( "+
						"                                SUM ( fda3852taxamount ) "+
						"                            AS fda3852taxamount," + 
						"                                SUM ( fda3852pounds ) AS fda3852pounds"+
						"                                FOR ( tobacco_class_nm ) "+
						"                                IN ( '"+tobaccoClass1+"' AS tobaccoclass1, '"+tobaccoClass2+"' AS tobaccoclass2 ) "+
						"                            ) "+
						"                        GROUP BY ( "+
						"                            quarter, "+
						"                            permit_num, "+
						"                            permitid, "+
						"                            reportstatus, "+
						"                            month, "+
						"                            periodid, "+
						"                            tobaccoclass1_fda3852taxamount, "+
						"                            tobaccoclass2_fda3852taxamount, " + 
						"                            tobaccoclass1_fda3852pounds," + 
						"                            tobaccoclass2_fda3852pounds,"+
						"                            pounds, "+
						"                            product, "+
						"                            tax_rate, "+
						"                            ttb5000_rec_cnt, "+
						"                            ingested_thresh_cnt, "+
						"                            zero_flag "+
						"                        ) "+
						"                        ORDER BY "+
						"                            permit_num "+
						"                    ) "+
						"                GROUP BY "+
						"                    quarter, "+
						"                    permit_num, "+
						"                    permitid "+
						"                ORDER BY "+
						"                    quarter, "+
						"                    permit_num "+
						"            ) b "+
						"    ) "+
						"ORDER BY "+
						"    quarter, "+
						"    permit_num, "+
						"    TO_DATE(month, 'MON') ASC, "+
						"    reportstatus ASC";
						
		Query query = getCurrentSession().createSQLQuery(sql);
		System.out.println(sql);
		query.setLong("fiscalyr", fiscalYear);
		query.setLong("qtr", quarter);
		query.setString("parenttobaccoclass", parentTobaccoClass);
		query.setLong("companyid", companyId);
		query.setString("permittypecd", permitType);

		List<Object[]> trueupComparisonSQLData = query.list();

		// Add Data Row
		List<TrueUpComparisonManufacturerDetail> details = new ArrayList<>();
		TrueUpComparisonManufacturerDetail row = new TrueUpComparisonManufacturerDetail();
		

		double input1tcClass1;
		double input2tcClass1;
		double input1tcClass2;
		double input2tcClass2;
		double totaltcclass1;
		double totaltcclass2;
		double periodtotal1 = 0;
		double periodtotal2 = 0;
		int i = 0;
		
		for (Object[] obj : trueupComparisonSQLData) {
			//if (obj[3] != null) {
				//String periodStatus = obj[3].toString();
				if (obj[3] != null && "Sub Total".equals(obj[3].toString())) {
					if (obj[0] != null) {
						int rptquarter = (((BigDecimal) obj[0]).intValue());
						row.setQuarter(rptquarter);
					}
					if (obj[1] != null) {
						String permitNum = obj[1].toString();
						row.setPermit(permitNum);
					}
					if (obj[2] != null) {
						long permitId = (((BigDecimal) obj[2]).intValue());
						row.setPermitId(permitId);
					}

					if (obj[6] != null)
						row.setIngestedTax(obj[6].toString());
					else
						row.setIngestedTax("NA");

					if (obj[7] != null)
						row.setPeriodTotal(obj[7].toString());
					else
						row.setPeriodTotal("NA");

					if (obj[8] != null)
						row.setPeriodTotal2(obj[8].toString());
					else
						row.setPeriodTotal2("NA");

					if (obj[9] != null)
						row.setPeriodTotalCombined(obj[9].toString());
					else
						row.setPeriodTotalCombined("NA");

					details.add(row);
					// construct new row;
					row = new TrueUpComparisonManufacturerDetail();
				} else {
					TrueUpComparisonManufacturerDetailPermit p = new TrueUpComparisonManufacturerDetailPermit();
					//if (obj[3] != null) {
						//String rptStatus = obj[3].toString();
						if ( obj[3]!= null && "NSTD".equalsIgnoreCase(obj[3].toString())) {
							p.setStatus("NS");
						} else if (obj[3]!= null && "ERRO".equalsIgnoreCase(obj[3].toString())) {
							p.setStatus("E");
						} else if (obj[3]!= null && "COMP".equalsIgnoreCase(obj[3].toString())) {
							p.setStatus("C");
						} else {
							p.setStatus("");
						}
					//}
					if (obj[4] != null) {
						String rptMonth = obj[4].toString();
						p.setMonth(rptMonth);
					}

					if (obj[5] != null) {
						long periodId = (((BigDecimal) obj[5]).intValue());
						p.setPeriodId(periodId);
					}

					if (obj[6] != null)
						p.setIngestedTax(obj[6].toString());
					else
						p.setIngestedTax("NA");

					if (obj[7] != null) {
						p.setTaxAmount(obj[7].toString());
						periodtotal1 = Double.parseDouble(obj[7].toString());
					} else {
						p.setTaxAmount("NA");
						periodtotal1 = 0;
					}
					if (obj[8] != null) {
						p.setTaxAmount2(obj[8].toString());
						periodtotal2 = Double.parseDouble(obj[8].toString());
					} else {
						p.setTaxAmount2("NA");
						periodtotal2 = 0;
					}
					if (obj[9] != null)
						p.setTaxTotalAmount(String.format("%.2f", obj[9]).toString());
					else
						p.setTaxTotalAmount("NA");
					
					if (obj[16] != null)
						p.setTobaccoclass1Tufattbvolume(obj[16].toString());
					else
						p.setTobaccoclass1Tufattbvolume("NA");
					
					if (obj[17] != null)
						p.setTobaccoclass2Tufattbvolume(obj[17].toString());
					else
						p.setTobaccoclass2Tufattbvolume("NA");
					
					double totalFDA = 0;
					if (obj[16] != null || obj[17] != null) {
						if(obj[16] != null)
							totalFDA += Double.parseDouble((String) obj[16].toString());
						if(obj[17] != null)
							totalFDA += Double.parseDouble((String) obj[17].toString());
						p.setTobaccoTotalTufattbvolume(Double.toString(totalFDA));
					}
					else
						p.setTobaccoTotalTufattbvolume("NA");

					if (obj[11] != null) {

						if (obj[10] != null) {

							List<String> poundsdata = Arrays.asList(obj[10].toString().split(","));
							List<String> productdata = Arrays.asList(obj[11].toString().split(","));
							List<String> taxrate = Arrays.asList(obj[12].toString().split(","));


							if (parentTobaccoClass.equalsIgnoreCase("Pipe-RYO")) {
								if (productdata.contains("Pipe Tobacco") || productdata.contains("Pipe")) {

									if (productdata.contains("Pipe Tobacco")) {
										i = productdata.indexOf("Pipe Tobacco");
									} else if (productdata.contains("Pipe")) {
										i = productdata.indexOf("Pipe");
									}

									p.setTobaccoClass1TTBvolume(poundsdata.get(i));
									// TTB Calculated Tax for Class 1 =
									// TTBVolume in Pounds * Tax_rate

									input1tcClass1 = Double.parseDouble(poundsdata.get(i).toString());
									input2tcClass1 = Double.parseDouble(taxrate.get(i).toString());
									totaltcclass1 = input1tcClass1 * input2tcClass1;
									p.setTobaccoClass1TTBTAX(Double.toString(totaltcclass1));
									// Calculated Delta for Class 1
									p.setCalculatedDelta1(Double.toString(periodtotal1 - totaltcclass1));

								}

								if (productdata.contains("Roll-Your-Own Tobacco")
										|| productdata.contains("Roll Your Own Tobacco") || productdata.contains("RYO")
										|| productdata.contains("Roll Your Own")) {
									if (productdata.contains("Roll-Your-Own Tobacco")) {
										i = productdata.indexOf("Roll-Your-Own Tobacco");
									} else if (productdata.contains("Roll Your Own Tobacco")) {
										i = productdata.indexOf("Roll Your Own Tobacco");
									} else if (productdata.contains("RYO")) {
										i = productdata.indexOf("RYO");
									} else if (productdata.contains("Roll Your Own")) {
										i = productdata.indexOf("Roll Your Own");
									}

									p.setTobaccoClass2TTBvolume(poundsdata.get(i));
									// TTB Calculated Tax for Class 2 =
									// TTBVolume in Pounds * Tax_rate
									input1tcClass2 = Double.parseDouble(poundsdata.get(i).toString());
									input2tcClass2 = Double.parseDouble(taxrate.get(i).toString());
									totaltcclass2 = input1tcClass2 * input2tcClass2;

									p.setTobaccoClass2TTBTAX(Double.toString(totaltcclass2));

									p.setCalculatedDelta2(Double.toString(periodtotal2 - totaltcclass2));

								}

							} else if (parentTobaccoClass.equalsIgnoreCase("Chew-and-Snuff")) {
								if (productdata.contains("Chewing Tobacco") || productdata.contains("Chew")) {
									if (productdata.contains("Chewing Tobacco")) {
										i = productdata.indexOf("Chewing Tobacco");
									} else if (productdata.contains("Chew")) {
										i = productdata.indexOf("Chew");
									}

									p.setTobaccoClass1TTBvolume(poundsdata.get(i));
									// TTB Calculated Tax for Class 1 =
									// TTBVolume in Pounds * Tax_rate

									input1tcClass1 = Double.parseDouble(poundsdata.get(i).toString());
									input2tcClass1 = Double.parseDouble(taxrate.get(i).toString());
									totaltcclass1 = input1tcClass1 * input2tcClass1;
									p.setTobaccoClass1TTBTAX(Double.toString(totaltcclass1));
									// Calculated Delta for Class 1
									p.setCalculatedDelta1(Double.toString(periodtotal1 - totaltcclass1));
								}

								if (productdata.contains("Snuff") || productdata.contains("Snuff Tobacco")) {
									if (productdata.contains("Snuff")) {
										i = productdata.indexOf("Snuff");
									} else if (productdata.contains("Snuff Tobacco")) {
										i = productdata.indexOf("Snuff Tobacco");
									}
									p.setTobaccoClass2TTBvolume(poundsdata.get(i));
									// TTB Calculated Tax for Class 2 =
									// TTBVolume in Pounds * Tax_rate
									input1tcClass2 = Double.parseDouble(poundsdata.get(i).toString());
									input2tcClass2 = Double.parseDouble(taxrate.get(i).toString());
									totaltcclass2 = input1tcClass2 * input2tcClass2;
									p.setTobaccoClass2TTBTAX(Double.toString(totaltcclass2));
									// Calculated Delta for Class 2
									p.setCalculatedDelta2(Double.toString(periodtotal2 - totaltcclass2));

								}
							} else if ("Cigarettes".equals(parentTobaccoClass)) {
								if (productdata.contains("Small Cigarettes") || productdata.contains("Large Cigarettes") || productdata.contains("Cigarettes")) {
									for(String product : productdata) {
										if ("Small Cigarettes".equals(product) || "Large Cigarettes".equals(product) || "Cigarettes".equals(product)) {
											i = productdata.indexOf(product);
											String currentVolVal = p.getTobaccoClass1TTBvolume();
											String currentCalcTax = p.getTobaccoClass1TTBTAX();
											if (currentVolVal == null) {
												currentVolVal = "0";
											}
											if (currentCalcTax == null) {
												currentCalcTax = "0";
											}
											p.setTobaccoClass1TTBvolume(new BigDecimal(Double.parseDouble(currentVolVal) + Double.parseDouble(poundsdata.get(i))).toPlainString());
											p.setTobaccoClass1TTBTAX(Double.toString(Double.parseDouble(currentCalcTax) + (Double.parseDouble(poundsdata.get(i)) * Double.parseDouble(taxrate.get(i)))));
										}
									};
									
									p.setCalculatedDelta1(Double.toString(periodtotal1 - Double.parseDouble(p.getTobaccoClass1TTBTAX())));
								}
							}

						}

					}

					if (obj[13] != null)
						p.setTtb5000Cnt(obj[13].toString());
					if (obj[14] != null)
						p.setIngestedThreshCnt(obj[14].toString());

					if (obj[15] != null) {
						// Populate attachment Ids. Doc Ids are for 3852 for
						// now.
						String docStr = (String) obj[15];
						String[] docIds = docStr.split(";");
						List<Attachment> attachments = new ArrayList<>();
						for (String docId : docIds) {
							Attachment attach = new Attachment();
							String[] docCreateDt = docId.split(":");
							attach.setDocumentId(Long.parseLong(docCreateDt[0].trim()));
							attach.setCreatedDt(CTPUtil.parseStringToDate(docCreateDt[1].trim()));
							attach.setFormTypes("FDA 3852");
							attachments.add(attach);
						}
						p.setAttachments(attachments);
					}

					row.getPermitDetails().add(p);
				}
			
		}

		return details;
	}

	@Override
	public List<UnknownCompanyBucket> getUnknownCompaniesBucket(Long fiscalYear) {
		List<UnknownCompanyBucket> bucket = new ArrayList<>();
		String sql = "SELECT ci.fiscal_yr, ci.importer_ein, ci.importer_nm, ci.consignee_ein, classes.tobacco_classes, classes.total_taxes "
				+ "FROM TU_CBP_IMPORTER ci INNER JOIN "
				+ "(SELECT lstagg.CBP_IMPORTER_ID, LISTAGG(lstagg.tobacco, ',') WITHIN GROUP (ORDER BY lstagg.tobacco) as tobacco_classes, SUM(lstagg.taxes) as TOTAL_TAXES FROM "
				+ "(SELECT ci.CBP_IMPORTER_ID, NVL(tc.tobacco_class_nm, 'Unknown') as tobacco, ce.ESTIMATED_TAX as taxes "
				+ "FROM TU_CBP_IMPORTER ci   INNER JOIN TU_CBP_ENTRY ce on ci.CBP_IMPORTER_ID = ce.CBP_IMPORTER_ID "
				+ "LEFT OUTER JOIN TU_TOBACCO_CLASS tc on ce.tobacco_class_id = tc.tobacco_class_id "
				+ "WHERE ci.ASSOCIATION_TYPE_CD='UNKN' "
				+ "AND ci.fiscal_yr=:fiscalYr AND ce.fiscal_year=:fiscalYr) lstagg "
				+ "GROUP BY lstagg.CBP_IMPORTER_ID) classes on ci.CBP_IMPORTER_ID=classes.CBP_IMPORTER_ID ";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("fiscalYr", fiscalYear);
		List<Object[]> resultSet = query.list();
		for (Object[] obj : resultSet) {
			UnknownCompanyBucket unknownCompany = new UnknownCompanyBucket();
			if (obj[0] != null)
				unknownCompany.setFiscalYear(((BigDecimal) obj[0]).longValue());
			if (obj[1] != null)
				unknownCompany.setImporterEin(obj[1].toString());
			if (obj[2] != null)
				unknownCompany.setImporterName(obj[2].toString());
			if (obj[3] != null)
				unknownCompany.setConsigneeEin(obj[3].toString());
			if (obj[4] != null)
				unknownCompany.setTobaccoClasses(distinctClasses(obj[4].toString()));
			if (obj[5] != null)
				unknownCompany.setTotalTaxes(((BigDecimal) obj[5]).doubleValue());
			bucket.add(unknownCompany);
		}

		return bucket;
	}

	String distinctClasses(String classes) {
		HashSet<String> classSet = new HashSet<>();
		for (String tclass : Arrays.asList(classes.split(","))) {
			classSet.add(tclass);
		}
		List<String> arrtclasses = new ArrayList<>(classSet);
		Collections.sort(arrtclasses);
		return StringUtils.join(arrtclasses, ", ");
	}

	@Override
	public List<TTBPermitBucket> getTTBPermits(long fiscalYear) {
		List<TTBPermitBucket> ttbpermits = new ArrayList<>();

		String sql = "SELECT tp.permit_num, tp.ttb_permit_id, tc.company_nm, "
				+ "(select company_id from tu_company where tu_company.ein = tc.EIN_NUM) as companyId, "
				+ "tc.EIN_NUM, classes.tobacco_classes, "
				+ "SUM(tax.ttb_taxes_paid)  as total_taxes, tp.include_flag, tp.tpbd_date " + "FROM TU_TTB_COMPANY tc "
				+ "INNER JOIN TU_TTB_PERMIT tp on tc.ttb_company_id = tp.ttb_company_id "
				+ "INNER JOIN TTB_TOBACCO_PERMITS_VW classes on tp.ttb_permit_id=classes.ttb_permit_id "
				+ "INNER JOIN TU_TTB_ANNUAL_TAX tax on tp.ttb_permit_id=tax.ttb_permit_id " + "where "
				+ "tc.EIN_NUM in (select EIN from tu_company) and "
				+ "tp.permit_num not in (select permit_num from tu_permit "
				+ "inner join tu_company on tu_permit.company_id = tu_company.company_id "
				+ "where tu_company.ein = tc.ein_num) and tc.fiscal_yr = :fy "
				+ "GROUP BY tp.permit_num, tp.ttb_permit_id, tc.company_nm, tc.EIN_NUM, classes.tobacco_classes"
				+ ", tp.include_flag, tp.tpbd_date ";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("fy", fiscalYear);
		List<Object[]> resultSet = query.list();
		for (Object[] obj : resultSet) {
			TTBPermitBucket permit = new TTBPermitBucket();
			if (obj[0] != null)
				permit.setPermitNum(obj[0].toString());
			if (obj[1] != null)
				permit.setPermitId(((BigDecimal) obj[1]).longValue());
			if (obj[2] != null)
				permit.setCompanyNm(obj[2].toString());
			if (obj[3] != null)
				permit.setCompanyId(((BigDecimal) obj[3]).longValue());
			if (obj[4] != null)
				permit.setEin(obj[4].toString());
			if (obj[5] != null)
				permit.setTobaccoClasses(obj[5].toString());
			if (obj[6] != null)
				permit.setTotalTaxes(((BigDecimal) obj[6]).longValue());
			if (obj[7] != null) {
				permit.setIncludeFlag(obj[7].toString());
				permit.setProvidedAnswer(true);
			}
			if (obj[8] != null) {
				permit.setTpbdDt((Date) obj[8]);
			}
			ttbpermits.add(permit);
		}

		return ttbpermits;
	}

	@Override
	public List<CompanyIngestion> getCompaniesForIncludeBucket(Long fiscalYear) {

		String sql = "SELECT EIN,"
				+ "dbms_xmlgen.convert(rtrim(xmlagg(XMLELEMENT(e, COMPANY_NM,'; ').EXTRACT('//text()') ).GetClobVal(), '; ') ,1) AS company_nm,"
				+ " regexp_replace(rtrim(xmlagg(XMLELEMENT(e, tobacco,', ').EXTRACT('//text()') ).GetClobVal(), ', '),'([^,]+)(,[ ]*\1)+','\1') AS tobacco_classes, "
				+ " SUM(tax)  AS total_taxes, "
				+ " rtrim(xmlagg(XMLELEMENT(e, association,',').EXTRACT('//text()') ).GetClobVal(), ',') AS assoctype, "
				+ " INCLFLG FROM " + "(SELECT ttbcmpy.EIN_NUM AS EIN, " + "  ttbcmpy.COMPANY_NM    AS COMPANY_NM, "
				+ "  NVL(ttbcmpy.INCLUDE_FLAG,'Y') inclflg, " + "  NVL(tc.TOBACCO_CLASS_NM, 'Unknown') tobacco, "
				+ "  ttb_annual_tx.TTB_TAXES_PAID AS tax, 'Manufacturer' AS association "
				+ "FROM TU_TTB_COMPANY ttbcmpy INNER JOIN TU_TTB_PERMIT ttbpermit "
				+ "ON ttbcmpy.TTB_COMPANY_ID = ttbpermit.TTB_COMPANY_ID "
				+ "INNER JOIN TU_TTB_ANNUAL_TAX ttb_annual_tx ON ttb_annual_tx.TTB_PERMIT_ID = ttbpermit.TTB_PERMIT_ID "
				+ "INNER JOIN TU_TOBACCO_CLASS tc ON ttb_annual_tx.TOBACCO_CLASS_ID = tc.TOBACCO_CLASS_ID "
				+ "WHERE ttbcmpy.EXISTS_INTUFA_FLAG  ='N' AND ttbcmpy.FISCAL_YR=:fiscalYr "
				+ "AND ttbcmpy.ein_NUM NOT IN (SELECT ein FROM tu_company) UNION ALL  SELECT cbpimp.IMPORTER_EIN AS EIN,"
				+ "  cbpimp.IMPORTER_NM AS COMPANY_NM, NVL(cbpimp.INCLUDE_FLAG,'Y') inclflg, NVL(tc.TOBACCO_CLASS_NM, 'Unknown') tobacco,"
				+ "  ce.ESTIMATED_TAX AS tax,'Importer' AS association FROM TU_CBP_IMPORTER cbpimp INNER JOIN TU_CBP_ENTRY ce "
				+ "ON cbpimp.CBP_IMPORTER_ID = ce.CBP_IMPORTER_ID INNER JOIN TU_TOBACCO_CLASS tc ON ce.TOBACCO_CLASS_ID = tc.TOBACCO_CLASS_ID "
				+ "WHERE cbpimp.DEFAULT_FLAG ='N'  AND (cbpimp.CONSIGNEE_EXISTS_FLAG='N' OR (cbpimp.CONSIGNEE_EXISTS_FLAG ='Y' "
				+ "AND cbpimp.ASSOCIATION_TYPE_CD = 'IMPT')) AND cbpimp.FISCAL_YR =:fiscalYr "
				+ "AND cbpimp.IMPORTER_EIN NOT IN (SELECT ein FROM TU_COMPANY) AND (cbpimp.ASSOCIATION_TYPE_CD NOT IN ('EXCL','UNKN') "
				+ "OR cbpimp.ASSOCIATION_TYPE_CD IS NULL OR cbpimp.CONSIGNEE_EIN IS NULL)) GROUP BY EIN, INCLFLG";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("fiscalYr", fiscalYear);
		List<Object[]> resultSet = query.list();

		List<CompanyIngestion> cmpnies = new ArrayList<>();

		for (Object[] obj : resultSet) {
			CompanyIngestion cmpy = new CompanyIngestion();
			if (obj[0] != null)
				cmpy.setEinNo(obj[0].toString());
			if (obj[1] != null) {
				String[] companies = null;
				try {
					companies = clob2String((Clob) obj[1]).split("; ");
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				Set<String> setCompanies = new HashSet<>(Arrays.asList(companies));
				cmpy.setCompanyName(StringUtils.join(setCompanies, ", "));
			}
			if (obj[2] != null) {
				String[] tobaccoClasses = null;
				try {
					tobaccoClasses = clob2String((Clob) obj[2]).split(", ");
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				Set<String> tobaccoClassSet = new HashSet<>(Arrays.asList(tobaccoClasses));
				cmpy.setOtherTobaccoTypesReported(tobaccoClassSet);
			}

			if (obj[3] != null)
				cmpy.setTotalTaxes(((BigDecimal) obj[3]).doubleValue());
			if (obj[4] != null) {
				String[] types = null;
				try {
					types = clob2String((Clob) obj[4]).split(",");
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				Set<String> setTypes = new HashSet<>(Arrays.asList(types));
				cmpy.setAssociationType(StringUtils.join(setTypes, ", "));
			}
			if (obj[5] != null) {
				cmpy.setIncludeFlag(obj[5].toString());
			}
			cmpnies.add(cmpy);
		}

		return cmpnies;
	}

	public String clob2String(Clob clb) throws SQLException, IOException {
		if (clb == null)
			return "";

		StringBuffer str = new StringBuffer();
		String strng;

		BufferedReader bufferRead = new BufferedReader(clb.getCharacterStream());

		while ((strng = bufferRead.readLine()) != null)
			str.append(strng);

		return str.toString();
	}

	@Override
	public void removeTrueUpDoc(long fiscalYr, String doctype) {
		logger.debug("Removal of Trueup Document of type {} for Fiscal Year:{}", doctype, fiscalYr);

		Query query = getCurrentSession().createQuery("delete TrueUpDocumentEntity doc"
				+ " where doc.trueUpId in (select trueup.trueUpId from TrueUpEntity trueup"
				+ " where trueup.fiscalYear = :fiscalYr) " + " and doc.docDesc = :doctype");
		query.setParameter("fiscalYr", fiscalYr);
		query.setParameter("doctype", doctype);
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();

		logger.debug("Completed removal of Trueup Document of type {} for Fiscal Year:{}", doctype, fiscalYr);

	}

	@Override
	public void removeTTBVolumes(long fiscalYr) {
		logger.debug("Removing TTB Volumes for {0}:...", fiscalYr);

		Query query = getCurrentSession()
				.createQuery("delete TTBRemovalsEntity removal" + " where removal.fiscalYr = :fiscalYr");
		query.setParameter("fiscalYr", fiscalYr);
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();

		query = getCurrentSession()
				.createQuery("delete TTBRemovalsFinalEntity removal" + " where removal.fiscalYr = :fiscalYr");
		query.setParameter("fiscalYr", fiscalYr);
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();

		logger.debug("Completed removal of TTB Volumes for {0}:...", fiscalYr);

	}

	@Override
	public void removeTTBEntries(long fiscalYr) {
		logger.debug("Removing TTB Tax Entries for {0}:", fiscalYr);

		//Remove Data from TU_TTB_INGESTED_EXCL
		this.removeTTBIngestedExclude(fiscalYr);
		
		Query query = getCurrentSession()
				.createQuery("delete TTBAnnualTaxEntity annual" + " where annual.ttbFiscalYr = :fiscalYr");
		query.setParameter("fiscalYr", Long.toString(fiscalYr));
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();

		query = getCurrentSession().createQuery("delete TTBPermitEntity permit where permit.companyId in "
				+ "(select company.ttbCompanyId from TTBCompanyEntity company where company.fiscalYr = :Yr)");
		query.setParameter("Yr", Long.toString(fiscalYr));
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();

		query = getCurrentSession()
				.createQuery("delete TTBAmendmentEntity amend" + " where amend.fiscalYr = :fiscalYr");
		query.setParameter("fiscalYr", Long.toString(fiscalYr));
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();

		query = getCurrentSession()
				.createQuery("delete TTBCompanyEntity company" + " where company.fiscalYr = :fiscalYr");
		query.setParameter("fiscalYr", Long.toString(fiscalYr));
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();

		// Delete from TU_COMPARISON_ALL_DELTA_STS
		Query allDeltaQry = getCurrentSession()
				.createQuery("delete ComparisonAllDeltaStatusEntity allDeltaSts where allDeltaSts.fiscalYr = :fiscalYr "
						+ " AND allDeltaSts.permitType=:permitType");
		allDeltaQry.setParameter("fiscalYr", Long.toString(fiscalYr));
		allDeltaQry.setParameter("permitType", "MANU");
		allDeltaQry.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();

		logger.debug("Completed removal of TTB Tax Entries for {0}:...", fiscalYr);

	}

	@Override
	public void removeCBPEntries(long fiscalYr) {
		logger.debug("Removing CBP Entries for {0}:", fiscalYr);

		Query query = getCurrentSession()
				.createQuery("delete CBPAmendmentEntity amend where amend.fiscalYr = :fiscalYr");
		query.setParameter("fiscalYr", Long.toString(fiscalYr));
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();

		// Delete from TU_COMPARISON_ALL_DELTA_STS
		Query allDeltaQry = getCurrentSession()
				.createQuery("delete ComparisonAllDeltaStatusEntity allDeltaSts where allDeltaSts.fiscalYr = :fiscalYr"
						+ " AND allDeltaSts.permitType=:permitType");
		allDeltaQry.setParameter("fiscalYr", Long.toString(fiscalYr));
		allDeltaQry.setParameter("permitType", "IMPT");
		allDeltaQry.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();
		
		this.removeCBPRecords(fiscalYr);

		logger.debug("Completed removal of CBP Tax Entries for {0}:...", fiscalYr);
	}

	private void removeCBPRecords(long fiscalYr) {

		// Remove Legacy Data from the TU_CBP_UPLOAD table
		String cntLegacySql = "select count(*) from tu_cbp_upload clegacy where clegacy.fiscal_yr=:fiscalYr";
		Query cntLegacyQuery = this.getCurrentSession().createSQLQuery(cntLegacySql);
		cntLegacyQuery.setParameter("fiscalYr", fiscalYr);

		String deleteCBPLegacySQL = "delete from tu_cbp_upload clegacy where clegacy.fiscal_yr=:fiscalYr and rownum < :batchSize";
		Query deleteCBPLegacyQuery = this.getCurrentSession().createSQLQuery(deleteCBPLegacySQL);
		deleteCBPLegacyQuery.setParameter("fiscalYr", fiscalYr);
		deleteCBPLegacyQuery.setParameter("batchSize", DELETE_BATCH_SIZE);

		Integer ttlLegacyCnt = 0;
		List lstLegacy = cntLegacyQuery.list();
		if (lstLegacy != null && lstLegacy.size() > 0)
			ttlLegacyCnt = ((BigDecimal) lstLegacy.get(0)).intValue();

		while (ttlLegacyCnt > 0) {

			deleteCBPLegacyQuery.executeUpdate();
			getCurrentSession().flush();
			getCurrentSession().clear();

			lstLegacy = cntLegacyQuery.list();
			if (lstLegacy != null && lstLegacy.size() > 0)
				ttlLegacyCnt = ((BigDecimal) lstLegacy.get(0)).intValue();
			else
				break;
		}

		// Remove Data from the TU_CBP_MATCHED_TAXES table
		String cntMatchedSql = "select count(*) from tu_cbp_matched_taxes cmatched where cmatched.fiscal_year=:fiscalYr";
		Query cntMatchedQuery = this.getCurrentSession().createSQLQuery(cntMatchedSql);
		cntMatchedQuery.setParameter("fiscalYr", fiscalYr);

		String deleteCBPMatchedSQL = "delete from tu_cbp_matched_taxes cmatched where cmatched.fiscal_year=:fiscalYr and rownum < :batchSize";
		Query deleteCBPMatchedQuery = this.getCurrentSession().createSQLQuery(deleteCBPMatchedSQL);
		deleteCBPMatchedQuery.setParameter("fiscalYr", fiscalYr);
		deleteCBPMatchedQuery.setParameter("batchSize", DELETE_BATCH_SIZE);

		Integer ttlMatchedCnt = 0;
		List lstMatched = cntMatchedQuery.list();
		if (lstMatched != null && lstMatched.size() > 0)
			ttlMatchedCnt = ((BigDecimal) lstMatched.get(0)).intValue();

		while (ttlMatchedCnt > 0) {

			deleteCBPMatchedQuery.executeUpdate();
			getCurrentSession().flush();
			getCurrentSession().clear();

			lstMatched = cntMatchedQuery.list();
			if (lstMatched != null && lstMatched.size() > 0)
				ttlMatchedCnt = ((BigDecimal) lstMatched.get(0)).intValue();
			else
				break;
		}

		// Remove Data from the TU_CBP_METRICS table
		String deleteCBPMetricsSQL = "delete tu_cbp_metrics cmetrics where cmetrics.fiscal_yr=:fiscalYr";
		Query deleteCBPMetricsQuery = this.getCurrentSession().createSQLQuery(deleteCBPMetricsSQL);
		deleteCBPMetricsQuery.setParameter("fiscalYr", fiscalYr);
		deleteCBPMetricsQuery.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();
		
		
		// Remove data from TU_CBP_INGESTED_EXCL 
		 this.removeCBPIngestedExclude(fiscalYr);

		// Remove Legacy Data from the TU_CBP_ENTRY table
		String cntSql = "select count(*) from tu_cbp_entry ce where ce.fiscal_year=:fiscalYr";
		Query cntQury = this.getCurrentSession().createSQLQuery(cntSql);
		cntQury.setParameter("fiscalYr", fiscalYr);

		String deleteCBPEntrySQL = "delete from tu_cbp_entry ce where ce.fiscal_year=:fiscalYr and rownum < :batchSize";
		Query deleteCBPEntryQury = this.getCurrentSession().createSQLQuery(deleteCBPEntrySQL);
		deleteCBPEntryQury.setParameter("fiscalYr", fiscalYr);
		deleteCBPEntryQury.setParameter("batchSize", DELETE_BATCH_SIZE);

		Integer ttlCnt = 0;
		List lst = cntQury.list();
		if (lst != null && lst.size() > 0)
			ttlCnt = ((BigDecimal) lst.get(0)).intValue();

		while (ttlCnt > 0) {

			deleteCBPEntryQury.executeUpdate();
			getCurrentSession().flush();
			getCurrentSession().clear();

			lst = cntQury.list();
			if (lst != null && lst.size() > 0)
				ttlCnt = ((BigDecimal) lst.get(0)).intValue();
			else
				break;
		}

		// Remove Legacy Data from the TU_CBP_IMPORTER table
		String cntImpSql = "select count(*) from tu_cbp_importer cimp where cimp.fiscal_yr=:fiscalYr";
		Query cntImpQury = this.getCurrentSession().createSQLQuery(cntImpSql);
		cntImpQury.setParameter("fiscalYr", fiscalYr);

		String deleteCBPImpSQL = "delete from tu_cbp_importer cimp where cimp.fiscal_yr=:fiscalYr and rownum < :batchSize";
		Query deleteCBPImpQury = this.getCurrentSession().createSQLQuery(deleteCBPImpSQL);
		deleteCBPImpQury.setParameter("fiscalYr", fiscalYr);
		deleteCBPImpQury.setParameter("batchSize", DELETE_BATCH_SIZE);

		Integer ttlImpCnt = 0;
		List lstImp = cntImpQury.list();
		if (lstImp != null && lstImp.size() > 0)
			ttlImpCnt = ((BigDecimal) lstImp.get(0)).intValue();

		while (ttlImpCnt > 0) {

			deleteCBPImpQury.executeUpdate();
			getCurrentSession().flush();
			getCurrentSession().clear();

			lstImp = cntImpQury.list();
			if (lstImp != null && lstImp.size() > 0)
				ttlImpCnt = ((BigDecimal) lstImp.get(0)).intValue();
			else
				break;
		}

		// Delete from TU_CBP_AMENDMENT
		Query query = getCurrentSession()
				.createQuery("delete CBPAmendmentEntity amend where amend.fiscalYr = :fiscalYr");
		query.setParameter("fiscalYr", Long.toString(fiscalYr));
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();

		// Delete from TU_COMPARISON_ALL_DELTA_STS
		Query allDeltaQry = getCurrentSession()
				.createQuery("delete ComparisonAllDeltaStatusEntity allDeltaSts where allDeltaSts.fiscalYr = :fiscalYr"
						+ " AND allDeltaSts.permitType=:permitType");
		allDeltaQry.setParameter("fiscalYr", Long.toString(fiscalYr));
		allDeltaQry.setParameter("permitType", "IMPT");
		allDeltaQry.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();
	}
  
	public void removeCBPIngestedExclude(long fiscalYr){
		
		String deleteCBPExclSQL = "delete from TU_CBP_INGESTED_EXCL where CBP_ENTRY_ID in ( "
		+ "select cbpentry.CBP_ENTRY_ID "
		+ "from tu_cbp_entry cbpentry "
		+ "INNER JOIN tu_cbp_importer cbpimporter ON "
		+ "cbpimporter.cbp_importer_id = cbpentry.cbp_importer_id "
		+ " where  cbpimporter.FISCAL_YR = :fiscalYr)  ";
		Query deleteCBPExclQury = this.getCurrentSession().createSQLQuery(deleteCBPExclSQL);
		deleteCBPExclQury.setParameter("fiscalYr", fiscalYr);
		deleteCBPExclQury.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();
		
	}
	
      public void removeTTBIngestedExclude(long fiscalYr){
		
		String deleteTTBExclSQL = "delete from TU_TTB_INGESTED_EXCL where TTB_ANNUAL_TAX_ID  in ( "
                                   + "select ttbtax.TTB_ANNUAL_TAX_ID "
                                   + "from TU_TTB_ANNUAL_TAX ttbtax "
                                   + " WHERE TTB_FISCAL_YR = :fiscalYr) ";

		Query deleteTTBExclQury = this.getCurrentSession().createSQLQuery(deleteTTBExclSQL);
		deleteTTBExclQury.setParameter("fiscalYr", fiscalYr);
		deleteTTBExclQury.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();
		
	}
	
	@Override
	public void removeCBPSummaryData(long fiscalYr) {

		// Delete data from TU_CBP_SUMMARY_FILE table
		String cntSummarySql = "select count(*) from tu_cbp_summary_file csummary where csummary.fiscal_year=:fiscalYr";
		Query cntSummaryQuery = this.getCurrentSession().createSQLQuery(cntSummarySql);
		cntSummaryQuery.setParameter("fiscalYr", fiscalYr);

		String deleteCBPSummarySQL = "delete from tu_cbp_summary_file csummary where csummary.fiscal_year=:fiscalYr and rownum < :batchSize";
		Query deleteCBPSummaryQuery = this.getCurrentSession().createSQLQuery(deleteCBPSummarySQL);
		deleteCBPSummaryQuery.setParameter("fiscalYr", fiscalYr);
		deleteCBPSummaryQuery.setParameter("batchSize", DELETE_BATCH_SIZE);

		Integer ttlSummaryCnt = 0;
		List lstSummary = cntSummaryQuery.list();
		if (lstSummary != null && lstSummary.size() > 0)
			ttlSummaryCnt = ((BigDecimal) lstSummary.get(0)).intValue();

		while (ttlSummaryCnt > 0) {

			deleteCBPSummaryQuery.executeUpdate();
			getCurrentSession().flush();
			getCurrentSession().clear();

			lstSummary = cntSummaryQuery.list();
			if (lstSummary != null && lstSummary.size() > 0)
				ttlSummaryCnt = ((BigDecimal) lstSummary.get(0)).intValue();
			else
				break;
		}

		removeCBPRecords(fiscalYr);
	}

	@Override
	public void removeCBPDetailsData(long fiscalYr) {

		// Delete from TU_CBP_LINE_ITEM_FILE table
		String cntLineSql = "select count(*) from TU_CBP_DETAILS_FILE cbpline where cbpline.fiscal_year=:fiscalYr";
		Query cntLineQuery = this.getCurrentSession().createSQLQuery(cntLineSql);
		cntLineQuery.setParameter("fiscalYr", fiscalYr);

		String deleteCBPLineSql = "delete from TU_CBP_DETAILS_FILE cbpline where cbpline.fiscal_year=:fiscalYr and rownum < :batchSize";
		Query deleteCBPLineQuery = this.getCurrentSession().createSQLQuery(deleteCBPLineSql);
		deleteCBPLineQuery.setParameter("fiscalYr", fiscalYr);
		deleteCBPLineQuery.setParameter("batchSize", DELETE_BATCH_SIZE);

		Integer ttlLineCnt = 0;
		List lstLineFile = cntLineQuery.list();
		if (lstLineFile != null && lstLineFile.size() > 0)
			ttlLineCnt = ((BigDecimal) lstLineFile.get(0)).intValue();

		while (ttlLineCnt > 0) {

			deleteCBPLineQuery.executeUpdate();
			getCurrentSession().flush();
			getCurrentSession().clear();

			lstLineFile = cntLineQuery.list();
			if (lstLineFile != null && lstLineFile.size() > 0)
				ttlLineCnt = ((BigDecimal) lstLineFile.get(0)).intValue();
			else
				break;
		}

		removeCBPRecords(fiscalYr);
		removeCBPDTConfig(fiscalYr);
	}

	@Override
	public Map<String, String> fetchFDA352MAN(long fiscalYr, long company_id, String classNm) {
		String sql = "select /*+ NO_QUERY_TRANSFORMATION(CMPMANU) */ quarterx, fda_delta, acceptance_flag,IN_PROGRESS_FLAG from ANN_TRUEUP_COMPARERESULTS_MANU "
				+ "where FISCAL_YR=:fiscalYr and company_id =:companyId and classname=:class_Nm ";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("fiscalYr", fiscalYr);
		query.setParameter("companyId", company_id);
		query.setParameter("class_Nm", classNm.replace('-', '/'));
		List<Object[]> trueupComparisonSQLData = query.list();

		HashMap<String, String> resultMap = new HashMap<>();
		for (Object[] a : trueupComparisonSQLData) {
			if (a[0] != null) {
				if (a[1] != null && a[2] != null && ((BigDecimal) a[1]).longValue() == 0
						&& "DeltaChange".equals(a[2].toString()))
					resultMap.put(a[0].toString(), "DeltaChngZ");

				else if (a[3] != null && a[3].toString().equals("InProgress")) {
					resultMap.put(a[0].toString(), a[3] == null ? "" : a[3].toString());

				} else {
					resultMap.put(a[0].toString(), a[2] == null ? "" : a[2].toString());
				}
			}
		}
		return resultMap;
	}

	@Override
	public ComparisonAllDeltaComments updateComment(ComparisonAllDeltaComments comment, long comment_seq) {

		String sql = null;
		if (comment.resolveComment == true)
			sql = "UPDATE tu_all_delta_comment set comment_resolved= :comment_resolved where comment_seq= :comment_seq";
		else
			sql = "UPDATE tu_all_delta_comment set user_comment= :user_comment where comment_seq= :comment_seq";

		Query query = getCurrentSession().createSQLQuery(sql.toString());
		query.setParameter("comment_seq", comment_seq);

		if (comment.resolveComment == true)
			query.setParameter("comment_resolved", comment.resolveComment);
		else
			query.setParameter("user_comment", comment.userComment);

		query.executeUpdate();

		// List<ComparisonAllDeltaStatusCommentEntity> updatedComment =
		// getUpdatedPermitsContacts(updatedPermit.getDocStageId());

		return null;

	}

	@Override
	public Map<String, String> fetchFDA352IMP(long fiscalYr, String ein, String classNm) {
		String sql = "select quarterx, fda_delta, acceptance_flag,IN_PROGRESS_FLAG from ANN_TRUEUP_COMPARERESULTS_IMP "
				+ "where FISCAL_YR=:fiscalYr and ein =:ein and classname=:class_Nm ";

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("fiscalYr", fiscalYr);
		query.setParameter("ein", ein);
		query.setParameter("class_Nm", classNm);
		List<Object[]> trueupComparisonSQLData = query.list();

		HashMap<String, String> resultMap = new HashMap<>();
		for (Object[] a : trueupComparisonSQLData) {
			if (a[0] != null) {
				if (a[1] != null && a[2] != null && ((BigDecimal) a[1]).abs().longValue() < 1
						&& "DeltaChange".equals(a[2].toString())) {
					resultMap.put(a[0].toString(), "DeltaChngZ");
				}

				else if (a[3] != null && a[3].toString().equals("InProgress")) {
					resultMap.put(a[0].toString(), a[3] == null ? "" : a[3].toString());

				} else {
					resultMap.put(a[0].toString(), a[2] == null ? "" : a[2].toString());
				}

			}
		}
		return resultMap;
	}

	public Map<String, String> fetchFDA352IMPSingle(long fiscalYr, String ein, String classNm) {
		String sql = "select quarterx, fda_delta, acceptance_flag,IN_PROGRESS_FLAG from ANN_TRUEUP_COMPARERES_IMP_SNG "
				+ "where FISCAL_YR=:fiscalYr and ein =:ein and classname=:class_Nm ";
		
		if (classNm.equals("Roll Your Own")) {
			classNm = "Roll-Your-Own";
		}
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("fiscalYr", fiscalYr);
		query.setParameter("ein", ein);
		query.setParameter("class_Nm", classNm);
		List<Object[]> trueupComparisonSQLData = query.list();

		HashMap<String, String> resultMap = new HashMap<>();
		for (Object[] a : trueupComparisonSQLData) {
			if (a[0] != null) {
				if (a[1] != null && a[2] != null && ((BigDecimal) a[1]).abs().longValue() < 1
						&& "DeltaChange".equals(a[2].toString())) {
					resultMap.put(a[0].toString(), "DeltaChngZ");
				}

				else if (a[3] != null && a[3].toString().equals("InProgress")) {
					resultMap.put(a[0].toString(), a[3] == null ? "" : a[3].toString());

				} else {
					resultMap.put(a[0].toString(), a[2] == null ? "" : a[2].toString());
				}

			}
		}
		return resultMap;		
	}

	@Override
	public Map<String, String> fetchOneSided(long fiscalYr, long company_id, String classNm, String reportType, String ein) {	
		List<Integer> quarterList = new ArrayList<>(Arrays.asList(1, 2, 3,4,5));
		
		if (classNm.equals("Roll Your Own")) {
			classNm = "Roll-Your-Own";
		}
		if(reportType.equals("MANU")) {
			if(classNm.equals("Chew-Snuff")){
				classNm = "Chew/Snuff";
			}
			else if(classNm.equals("Pipe-Roll-Your-Own")){
				classNm = "Pipe/Roll-Your-Own";
			}
			else if(classNm.equals("Pipe-Roll Your Own")){
				classNm = "Pipe/Roll-Your-Own";
			}
			}
		String sql;
		if(reportType.equalsIgnoreCase("IMP")) {
			sql = "select fiscal_qtr, status from TU_COMPARISON_ALL_DELTA_STS "
					+ "where FISCAL_YR=:fiscalYr and tobacco_class_nm=:class_Nm and ein = :ein";
		}
		else {
			sql = "select fiscal_qtr, status from TU_COMPARISON_ALL_DELTA_STS "
					+ "where FISCAL_YR=:fiscalYr and tobacco_class_nm=:class_Nm and ein = (select ein from TU_COMPANY where company_id =:companyId)";
		}
		

		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("fiscalYr", fiscalYr);
		if(reportType.equalsIgnoreCase("IMP")) {
			query.setParameter("ein", ein);
			String sqlCompany = "SELECT COMPANY_ID from TU_COMPANY WHERE EIN = :ein";
			Query queryCompany = getCurrentSession().createSQLQuery(sqlCompany);
			queryCompany.setParameter("ein", ein);
			company_id = queryCompany.uniqueResult() != null?((BigDecimal)queryCompany.uniqueResult()).longValue():0;
		}
		else {
			query.setParameter("companyId", company_id);
		}
		query.setParameter("class_Nm", classNm);
		List<Object[]> trueupComparisonSQLData = query.list();

		HashMap<String, String> resultMap = new HashMap<>();
		for (Object[] a : trueupComparisonSQLData) {
			if (a[0] != null) {
				resultMap.put(a[0].toString(), a[1] == null ? "" : a[1].toString());
			}
		}
		/*check resultMap how many quarters are populated
		if nothing then  populate all quarter which are not excluded with empty.
		else check  those quarters  which are not populated and in excluded
		below code will check if action buttons to be disabled on UI based on permit exclude*/
		
		List<ExcludePermitDetails> excludedList = getPermitExcludeList(String.valueOf(fiscalYr), company_id, 3);
		
		List<Integer> excludequarterList = new ArrayList<Integer>();
		String className = classNm;
		excludedList.stream().forEach(bean->{
			if(!className.equalsIgnoreCase("Cigars")) {
				if(bean.getYear() == fiscalYr+1 && bean.getQuarter()==1) {
					excludequarterList.add(4);
				} else if(bean.getYear() == fiscalYr && bean.getQuarter()>=1 && bean.getQuarter()<=4) {
					excludequarterList.add(bean.getQuarter()-1);
				}
			} else if(className.equalsIgnoreCase("Cigars") && bean.getQuarter()==1 && bean.getYear()-1 == fiscalYr) {
				excludequarterList.add(5);
			}
		});
		if(resultMap.size()>0) {
			List<String> addedQuarter = new ArrayList(resultMap.keySet());
			if(!className.equalsIgnoreCase("Cigars")) {
				quarterList.removeAll(addedQuarter);
				if(CollectionUtils.isNotEmpty(excludedList) ) {
					/**
					 * this block will check when exclude is present . thenr checks if data tufa only.
					 */
					List<String> tufaOnlyQuarterList = new ArrayList<String>();
					String source = "TUFA";
					String tufaOnlySQL;
					if(reportType.equalsIgnoreCase("IMP")) {
						tufaOnlySQL = "select vw.FISCAL_QTR from ALL_DELTA_SNG_WO_EXCL vw where vw.FISCAL_YR = :fiscalYr and vw.TOBACCO_CLASS_NM = :class_Nm and ein=:ein and vw.SOURCE = :source ";
					} else {
						tufaOnlySQL = "select vw.FISCAL_QTR from ALL_DELTA_SNG_WO_EXCL vw where vw.FISCAL_YR = :fiscalYr and vw.TOBACCO_CLASS_NM = :class_Nm and ein=((select ein from TU_COMPANY where company_id =:companyId)) and vw.SOURCE = :source ";
					}
					Query tufaOnlyQuery = getCurrentSession().createNativeQuery(tufaOnlySQL);
					tufaOnlyQuery.setParameter("fiscalYr", fiscalYr);
					tufaOnlyQuery.setParameter("class_Nm", classNm);
					if(reportType.equalsIgnoreCase("IMP")) {
						tufaOnlyQuery.setParameter("ein", ein);
					}
					else {
						tufaOnlyQuery.setParameter("companyId", company_id);
					}
					tufaOnlyQuery.setParameter("source", source);
					tufaOnlyQuarterList = tufaOnlyQuery.list();
					if(CollectionUtils.isNotEmpty(tufaOnlyQuarterList)) {
						for(ExcludePermitDetails detail : excludedList) {
							if(detail.getExclusionScopeId() == 1) {
								tufaOnlyQuarterList.stream().forEachOrdered(tufaQrt-> {
									if(!className.equalsIgnoreCase("Cigars") && ((detail.getYear() == fiscalYr && detail.getQuarter()-1<=Integer.parseInt(tufaQrt)) || detail.getYear() < fiscalYr)) {
										excludequarterList.add(Integer.parseInt(tufaQrt));
									} else if(className.equalsIgnoreCase("Cigars") && detail.getYear()-1 <= fiscalYr) {
										excludequarterList.add(5);
									}
								});
							}
						}
					} 
					for(Integer qt : quarterList) {
						if(!excludequarterList.contains(qt) && resultMap.get(String.valueOf(qt)) == null) {
							resultMap.put(String.valueOf(qt), "");
						} else {
							if(excludequarterList.contains(qt) && resultMap.get(String.valueOf(qt)) != null) {
								resultMap.remove(qt);
							}
						}
					}
				} else {
					quarterList.stream().forEach(qt-> {
						if(!addedQuarter.contains(String.valueOf(qt))  && resultMap.get(String.valueOf(qt)) == null) {
							resultMap.put(String.valueOf(qt), "");
						}
					});
				}
			} 
			 
		} else {
			if(CollectionUtils.isNotEmpty(excludedList)) {
				List<String> tufaOnlyQuarterList = new ArrayList<String>();
				String source = "TUFA";
				String tufaOnlySQL;
				if(reportType.equalsIgnoreCase("IMP")) {
					tufaOnlySQL = "select vw.FISCAL_QTR from ALL_DELTA_SNG_WO_EXCL vw where vw.FISCAL_YR = :fiscalYr and vw.TOBACCO_CLASS_NM = :class_Nm and ein=:ein and vw.SOURCE = :source ";
				} else {
					tufaOnlySQL = "select vw.FISCAL_QTR from ALL_DELTA_SNG_WO_EXCL vw where vw.FISCAL_YR = :fiscalYr and vw.TOBACCO_CLASS_NM = :class_Nm and ein=((select ein from TU_COMPANY where company_id =:companyId)) and vw.SOURCE = :source ";
				}
				Query tufaOnlyQuery = getCurrentSession().createNativeQuery(tufaOnlySQL);
				tufaOnlyQuery.setParameter("fiscalYr", fiscalYr);
				tufaOnlyQuery.setParameter("class_Nm", classNm);
				if(reportType.equalsIgnoreCase("IMP")) {
					tufaOnlyQuery.setParameter("ein", ein);
				}
				else {
					tufaOnlyQuery.setParameter("companyId", company_id);
				}
				tufaOnlyQuery.setParameter("source", source);
				tufaOnlyQuarterList = tufaOnlyQuery.list();
				if(CollectionUtils.isNotEmpty(tufaOnlyQuarterList)) {
					for(ExcludePermitDetails detail : excludedList) {
						if(detail.getExclusionScopeId() == 1) {
							tufaOnlyQuarterList.stream().forEachOrdered(tufaQrt->{
								if(!className.equalsIgnoreCase("Cigars") && ((detail.getYear() == fiscalYr && detail.getQuarter()-1<=Integer.parseInt(tufaQrt)) || detail.getYear() < fiscalYr)) {
									excludequarterList.add(Integer.parseInt(tufaQrt));
								} else if(className.equalsIgnoreCase("Cigars") && detail.getYear()-1 <= fiscalYr) {
									excludequarterList.add(5);
								}
							});
						}
					}
				} 
				quarterList.stream().forEach(quarter-> {
						if(!excludequarterList.contains(quarter) && resultMap.get(String.valueOf(quarter)) == null) {
							resultMap.put(String.valueOf(quarter), "");
						}
				});
			} else {
				quarterList.stream().forEach(quarter-> {
					if(resultMap.get(String.valueOf(quarter)) == null) {
						resultMap.put(String.valueOf(quarter), "");
					}
				});
			}
		}
		//run check to see if the quarter has <1 amount.
		String zeroDollorSql;
		if(reportType.equalsIgnoreCase("IMP")) {
			zeroDollorSql = "select vw.FISCAL_QTR from ALL_DELTA_SNG_WO_EXCL vw where vw.FISCAL_YR = :fiscalYr and vw.TOBACCO_CLASS_NM = :class_Nm and ein=:ein ";
		} else {
			zeroDollorSql = "select vw.FISCAL_QTR from ALL_DELTA_SNG_WO_EXCL vw where vw.FISCAL_YR = :fiscalYr and vw.TOBACCO_CLASS_NM = :class_Nm and ein=((select ein from TU_COMPANY where company_id =:companyId)) ";
		}
		Query nonZeroDollorQuery = getCurrentSession().createNativeQuery(zeroDollorSql);
		nonZeroDollorQuery.setParameter("fiscalYr", fiscalYr);
		nonZeroDollorQuery.setParameter("class_Nm", classNm);
		if(reportType.equalsIgnoreCase("IMP")) {
			nonZeroDollorQuery.setParameter("ein", ein);
		}
		else {
			nonZeroDollorQuery.setParameter("companyId", company_id);
		}
		List<String> nonZeroDollorDeltaList = nonZeroDollorQuery.list();
		resultMap.keySet().stream().forEach(q->{
			if(!nonZeroDollorDeltaList.contains(q.equalsIgnoreCase("5")?"1-4":q) && !resultMap.get(q).equalsIgnoreCase("Associated")) {
				resultMap.put(q, null);
			}
		});
		return resultMap;
	}

	
	@Override
	public List<PermitUpdatesDocument> getPermitUpdateFiles() {

		String sql = "select stage_doc_id,doc_file_nm,alias_file_nm,created_by,to_char(created_dt,'MM/DD/YYYY') from  TU_PERMIT_UPDATES_DOCUMENT order by created_dt desc";
		Query query = this.getCurrentSession().createSQLQuery(sql);
		List<Object[]> permitupdatesFilenames = query.list();

		List<PermitUpdatesDocument> filenames = new ArrayList<>();

		for (Object[] file : permitupdatesFilenames) {
			PermitUpdatesDocument doc = new PermitUpdatesDocument();
			doc.setDocId(((BigDecimal) file[0]).longValue());
			if (file[1] != null)
				doc.setFileNm(file[1].toString());
			if (file[2] != null)
				doc.setAliasFileNm(file[2].toString());
			doc.setCreatedBy(file[3].toString());
			doc.setCreatedDt(file[4].toString());
			filenames.add(doc);
		}
		return filenames;
	}

	@Override
	public List<PermitUpdateUpload> getUpdatedPermitsContacts(Long docId) {

		List<PermitUpdateUpload> updatedPermits = new ArrayList<>();
		PermitUpdateUpload updatedPermitsDetail;
		List<String> tufaContactList = null;

		String hql = "SELECT * FROM PERMIT_GETCONTACTINFO_VW WHERE DOC_STAGE_ID = :docId";
		Query query = this.getCurrentSession().createSQLQuery(hql);
		query.setParameter("docId", docId);

		List<Object[]> permits = query.list();

		for (Object[] obj : permits) {

			String ingestedContact = null;
			String tufaConcatenatedContact = null;
			boolean isTufaContactPresent = false;
			boolean isIngestedContactPresent = false;
			boolean isMatched = false;
			tufaContactList = new ArrayList<String>();

			updatedPermitsDetail = new PermitUpdateUpload();

			if (obj[0] != null) {
				updatedPermitsDetail.setBusinessName(obj[0].toString());
			}
			if (obj[1] != null) {
				updatedPermitsDetail.setEinNum(obj[1].toString());
			}
			if (obj[2] != null) {
				updatedPermitsDetail.setPermitNum(obj[2].toString());
			}
			if (obj[3] != null) {
				updatedPermitsDetail.setPermitUpdateId(((BigDecimal) obj[3]).longValue());
			}
			if (obj[4] != null) {
				updatedPermitsDetail.setDocStageId(((BigDecimal) obj[4]).longValue());
			}
			if (CTPUtil.isNotNullorNotEmpty(obj[5], Constants.SEMICOLON_SEPERATOR)) {
				updatedPermitsDetail.setEmail(obj[5].toString());
			}
			if (CTPUtil.isNotNullorNotEmpty(obj[6], Constants.SEMICOLON_SEPERATOR)) {
				updatedPermitsDetail.setPremisePhone(obj[6].toString());
			}
			if (CTPUtil.isNotNullorNotEmpty(obj[7], Constants.SEMICOLON_SEPERATOR)) {
				updatedPermitsDetail.setFax(obj[7].toString());
			}
			if (obj[17] != null) {
				updatedPermitsDetail.setContactStatus(obj[17].toString());
			}
			if (obj[18] != null) {
				updatedPermitsDetail.setCompanyId(((BigDecimal) obj[18]).longValue());
			}
			if (obj[19] != null) {
				updatedPermitsDetail.setTufaDoc3852Id(((BigDecimal) obj[19]).longValue());
			}
			if (obj[20] != null) {
				updatedPermitsDetail.setMrMonth(obj[20].toString());
			}
			if (obj[21] != null) {
				updatedPermitsDetail.setMrFiscalYr(obj[21].toString());
			}

			// Ingested address
			if (obj[22] != null) {
				ingestedContact = obj[22].toString();
			}
			// TUFA contact concatenated
			if (obj[23] != null) {
				tufaConcatenatedContact = obj[23].toString();
				if (tufaConcatenatedContact.contains(Constants.DIFFERENTIATOR)) {
					tufaContactList = Arrays.asList(tufaConcatenatedContact.split(Constants.DIFFERENTIATOR));
				} else {
					tufaContactList.add(tufaConcatenatedContact);
				}
			}

			// check if contact address present
			if (CTPUtil.isNotNullorNotEmpty(ingestedContact, Constants.COMMA_SEPERATOR)) {
				isIngestedContactPresent = true;
			}
			if (null != tufaContactList) {
				for (int i = 0; i < tufaContactList.size(); i++) {
					if (CTPUtil.isNotNullorNotEmpty(tufaContactList.get(i), Constants.COMMA_SEPERATOR)) {
						isTufaContactPresent = true;
						// Check for Match
						if (isIngestedContactPresent && tufaContactList.get(i).equalsIgnoreCase(ingestedContact)) {
							isMatched = true;
							break;
						}
					}
				}
			}
			//Show on the contacts grid only if 
			//1. Contacts in Ingested and Tufa don't match or
			//2. An action has been taken or
			//3. Only if ingested contact is present
			
			if((!isMatched && isIngestedContactPresent) || obj[17] != null || (!isTufaContactPresent && isIngestedContactPresent)){
				updatedPermits.add(updatedPermitsDetail);
			}

		}

		return updatedPermits;

	}

	@Override
	public List<PermitUpdateUpload> getUpdatedPermits(Long docId) {

		List<Object[]> permits = null;
		List<PermitUpdateUpload> updatedPermits = new ArrayList<>();
		PermitUpdateUpload updatedPermitsDetail;
		List<String> tufaAddressList = null;

		String hql = "SELECT * FROM PERMIT_GETADDRESSINFO_VW WHERE DOC_STAGE_ID = :docId";
		Query query = this.getCurrentSession().createSQLQuery(hql);
		query.setParameter("docId", docId);
		permits = query.list();

		for (Object[] obj : permits) {
			String ingestedAddress = null;
			String tufaConcatenatedAddress = null;
			String tufaAddress1 = null;
			String tufaAddress2 = null;
			String ingestAddrWitoutCountryCd = null;
			boolean isTufaAddressPresent = false;
			boolean isIngestedAddressPresent = false;
			updatedPermitsDetail = new PermitUpdateUpload();
			tufaAddressList = new ArrayList<String>();

			if (obj[0] != null) {
				updatedPermitsDetail.setBusinessName(obj[0].toString());
			}
			if (obj[1] != null) {
				updatedPermitsDetail.setEinNum(obj[1].toString());
			}
			if (obj[2] != null) {
				updatedPermitsDetail.setPermitNum(obj[2].toString());
			}
			if (obj[3] != null) {
				updatedPermitsDetail.setPermitUpdateId(((BigDecimal) obj[3]).longValue());
			}
			if (obj[4] != null) {
				updatedPermitsDetail.setDocStageId(((BigDecimal) obj[4]).longValue());
			}
			if (CTPUtil.isNotNullorNotEmpty(obj[5], Constants.SEMICOLON_SEPERATOR)) {
				updatedPermitsDetail.setMailingStreet(obj[5].toString());
			}
			if (CTPUtil.isNotNullorNotEmpty(obj[6], Constants.SEMICOLON_SEPERATOR)) {
				updatedPermitsDetail.setMailingState(obj[6].toString());
			}
			if (CTPUtil.isNotNullorNotEmpty(obj[7], Constants.SEMICOLON_SEPERATOR)) {
				updatedPermitsDetail.setMailingCity(obj[7].toString());
			}
			if (CTPUtil.isNotNullorNotEmpty(obj[8], Constants.SEMICOLON_SEPERATOR)) {
				updatedPermitsDetail.setMailingPostalCd(obj[8].toString());
			}
			if (CTPUtil.isNotNullorNotEmpty(obj[9], Constants.SEMICOLON_SEPERATOR)) {
				updatedPermitsDetail.setMailingProvince(obj[9].toString());
			}
			if (CTPUtil.isNotNullorNotEmpty(obj[10], Constants.SEMICOLON_SEPERATOR)) {
				updatedPermitsDetail.setMailingText(obj[10].toString());
			}
			if (CTPUtil.isNotNullorNotEmpty(obj[11], Constants.SEMICOLON_SEPERATOR)) {
				updatedPermitsDetail.setMailingAttention(obj[11].toString());
			}
			if (obj[22] != null) {
				updatedPermitsDetail.setAddressStatus(obj[22].toString());
			}
			if (obj[23] != null) {
				updatedPermitsDetail.setTufaDoc3852Id(((BigDecimal) obj[23]).longValue());
			}
			if (obj[24] != null) {
				updatedPermitsDetail.setMrMonth(obj[24].toString());
			}
			if (obj[25] != null) {
				updatedPermitsDetail.setMrFiscalYr(obj[25].toString());
			}
			if (obj[26] != null) {
				updatedPermitsDetail.setCompanyId(((BigDecimal) obj[26]).longValue());
			}
			// Ingested address
			if (obj[27] != null) {
				ingestedAddress = StringUtils.normalizeSpace(obj[27].toString());
				ingestAddrWitoutCountryCd = ingestedAddress.substring(0, ingestedAddress.length() - 2);
			}
			// TUFA address concatenated
			if (obj[28] != null) {
				tufaConcatenatedAddress = obj[28].toString();
				if (tufaConcatenatedAddress.contains(Constants.DIFFERENTIATOR)) {
					tufaAddressList = Arrays.asList(tufaConcatenatedAddress.split(Constants.DIFFERENTIATOR));
					tufaAddress1 = StringUtils.normalizeSpace(tufaAddressList.get(0));
					tufaAddress2 = StringUtils.normalizeSpace(tufaAddressList.get(1));
				} else {
					tufaAddressList.add(tufaConcatenatedAddress);
				}
			}

			if (obj[31] != null)
				updatedPermitsDetail.setIngestedStateValid(obj[31].toString());

			// check if ingested address present
			if (CTPUtil.isNotNullorNotEmpty(ingestAddrWitoutCountryCd, Constants.COMMA_SEPERATOR)) {
				isIngestedAddressPresent = true;
			}
			// check if tufa address present
			if (CTPUtil.isNotNullorNotEmpty(tufaAddress1, Constants.COMMA_SEPERATOR)
					|| CTPUtil.isNotNullorNotEmpty(tufaAddress2, Constants.COMMA_SEPERATOR)) {
				isTufaAddressPresent = true;
			}
			
			//Show on the address grid only if:
			// 1.  TUFA address  and ingested address don't match or
			// 2.  action present or
			// 3.  Only ingested address present or
			// 4.  State invalid 
			
			if( (isIngestedAddressPresent && !(ingestedAddress.equalsIgnoreCase(tufaAddress1)
					|| ingestedAddress.equalsIgnoreCase(tufaAddress2))) || obj[22] != null || 
					(isIngestedAddressPresent && !isTufaAddressPresent)
					|| "N".equalsIgnoreCase(updatedPermitsDetail.getIngestedStateValid())) {
				updatedPermits.add(updatedPermitsDetail);
			}
		}
		return updatedPermits;
	}

	@Override
	public List<Address> getIngestedAddress() {

		String hql = "SELECT * FROM PERMIT_COPYADDRESS_VW";
		Query query = this.getCurrentSession().createSQLQuery(hql);

		List<Object[]> data = query.list();
		List<Address> addresses = new ArrayList<>();

		for (Object[] obj : data) {

			Address addressDetail = new Address();

			if (obj[1] != null) {
				addressDetail.setEin(obj[1].toString());
			}
			if (obj[2] != null)

			{
				addressDetail.setCompanyId(((BigDecimal) obj[2]).longValue());
				addressDetail.setAddressTypeCd("PRIM");
			}

			if (obj[3] != null)
				addressDetail.setStreetAddress(obj[3].toString());
			if (obj[4] != null)
				addressDetail.setState(obj[4].toString());
			if (obj[5] != null)
				addressDetail.setCity(obj[5].toString());

			if (obj[6] != null)
				addressDetail.setAttention(obj[6].toString());

			if (obj[7] != null)
				addressDetail.setCountryNm(obj[7].toString());

			if (obj[8] != null)
				addressDetail.setSuite(obj[8].toString());

			if (obj[9] != null)
				addressDetail.setProvince(obj[9].toString());

			if (obj[10] != null)
				addressDetail.setPostalCd(obj[10].toString());

			if (obj[11] != null)
				addressDetail.setDocStageId(obj[11].toString());
			if (obj[12] != null)
				addressDetail.setStageId(obj[12].toString());

			addresses.add(addressDetail);
		}

		return addresses;

	}

	@Override
	public List<Contact> getIngestedContacts() {

		String hql = "SELECT * FROM PERMIT_COPYCONTACT_VW";
		Query query = this.getCurrentSession().createSQLQuery(hql);

		List<Object[]> data = query.list();
		List<Contact> contacts = new ArrayList<>();

		for (Object[] obj : data) {

			Contact contactDetail = new Contact();

			if (obj[1] != null) {
				contactDetail.setEin(obj[1].toString());
			}

			if (obj[2] != null) {
				contactDetail.setCompanyId(((BigDecimal) obj[2]).longValue());
			}

			if (obj[3] != null)
				contactDetail.setEmailAddress(obj[3].toString());
			if (obj[4] != null)
				contactDetail.setPhoneNum(obj[4].toString());
			if (obj[5] != null)
				contactDetail.setFaxNum(obj[5].toString());
			if (obj[6] != null)
				contactDetail.setDocStageId(obj[6].toString());
			if (obj[7] != null)
				contactDetail.setStageId(obj[7].toString());

			contacts.add(contactDetail);
		}

		return contacts;

	}

	@Override
	public List<PermitUpdateUpload> saveUpdatedPermits(PermitUpdateEntity updatedPermit) {

		String sql = "UPDATE tu_permit_updates_stage set address_status= :address_status where DOC_STAGE_ID= :docstageid AND STAGE_ID= :stageid ";
		Query query = getCurrentSession().createSQLQuery(sql.toString());
		query.setParameter("address_status", updatedPermit.getAddressStatus());
		query.setParameter("docstageid", updatedPermit.getDocStageId());
		query.setParameter("stageid", updatedPermit.getPermitUpdateId());
		query.executeUpdate();

		List<PermitUpdateUpload> updatedPermits = getUpdatedPermits(updatedPermit.getDocStageId());

		return updatedPermits;
	}

	public List<PermitAudit> getPermitMgmtAuditListByDocId(Long docId) {

		String sql = " SELECT TO_CHAR(pa.AUDIT_ID)                     AS auditId, "
				+ "  UPPER(pa.COMPANY_NAME)                               AS companyName, "
				+ "  nvl(TO_CHAR(company.created_dt,'MM/DD/YYYY'),'')            AS companyCreateDt, "
				+ "  TO_CHAR(NVL(pa.COMPANY_ID,0))                 AS companyId, "
				+ "  TO_CHAR(pa.DOC_STAGE_ID)                      AS docStageId, "
				+ "  pa.EIN                                        AS einNum, "
				+ "  TO_CHAR(NVL(pa.PERMIT_ID,0))                  AS permitId, "
				+ "  pa.PERMIT_STATUS                              AS permitStatus, "
				+ "  pa.PERMIT_ACTION                              AS permitAction, "
				+ "  pa.COMPANY_STATUS                             AS companyStatus, "
				+ "  pa.COMPANY_ACTION                             AS companyAction, "
				+ "  NVL(TO_CHAR(pa.CLOSE_DT,'MM/DD/YYYY'),'')     AS closeDt, "
				+ "  NVL(TO_CHAR(pa.ISSUE_DT,'MM/DD/YYYY'),'')     AS issueDt, "
				+ "  NVL(TO_CHAR(pa.ACTION_DT,'MM/DD/YYYY'),'')    AS actionDt, "
				+ "  NVL(TO_CHAR(pa.CREATED_DT,'MM/DD/YYYY'),'')   AS createdDt, "
				+ "  pa.ERRORS                                     AS errors, "
				+ "  pa.PERMIT_STATUS_CHANGE_ACTION                AS permitStatusChangeAction, "
				+ "  TO_CHAR(NVL(pa.PERMIT_ID_TUFA,0))             AS permitIdTufa, "
				+ "  pa.COMPANY_STATUS_CHANGE_ACTION               AS companyStatusChangeAction, "
				+ "  pa.PERMIT_TYPE_CD                             AS permitTypeCd, "
				+ "  pa.PERMIT_STATUS_TUFA                         AS permitStatusTufa, "
				+ "  TO_CHAR(NVL(permit.permit_id,0))              AS pPermitId, "
				+ "  NVL(permit.permit_num,0)                      AS pPermitNum, "
				+ "  NVL(permit.PERMIT_STATUS_TYPE_CD,0)           AS pPermitStatusTypeCd , "
				+ "  NVL(TO_CHAR(permit.ISSUE_DT,'MM/DD/YYYY'),'') AS pIssueDt, "
				+ "  NVL(TO_CHAR(permit.CLOSE_DT,'MM/DD/YYYY'),'') AS pCloseDt, "
				+ "  NVL(TO_CHAR(permit.TTB_START_DT,'MM/DD/YYYY'),'')  AS pTTBStartDt, "
				+ "  NVL(TO_CHAR(permit.TTB_END_DT,'MM/DD/YYYY'),'')  AS pTTBEndDt,"
				+ "  company.legal_nm                              AS cLegalNm, "
				+ "  company.COMPANY_STATUS                        AS cCompanyStatus, "
				+ "  CASE WHEN RP.month IS NOT NULL AND RP.year IS NOT NULL THEN  "
				+ "    TO_CHAR(last_day(to_date(rp.month||'/'||rp.year,'MM/YYYY')),'MM/DD/YYYY') "
				+ "   ELSE null END  AS rpLastRptDt " + " FROM " + "  (SELECT paudit.*, "
				+ "    rank() over (partition BY doc_stage_id,company_id,permit_id order by action_dt DESC) rnk "
				+ "  FROM tu_permit_updates_audit paudit " + "  WHERE paudit.doc_stage_id = :docId " + "  ) pa "
				+ "LEFT JOIN TU_PERMIT permit " + "ON pa.permit_id_tufa = permit.permit_id "
				+ "LEFT JOIN TU_PERMIT_PERIOD pp  "
				+ "ON pp.permit_id= permit.permit_id AND pp.period_id in (select max(pp.period_id) from tu_permit_period pp where pp.permit_id = permit.permit_id) "
				+ "LEFT JOIN TU_RPT_PERIOD rp " + "ON pp.period_id = rp.period_id " + "LEFT JOIN TU_COMPANY company "
				+ "ON pa.company_id=company.company_id " + "WHERE rnk       = 1 " + "ORDER BY pa.permit_id_tufa ASC";

		Query query = this.getCurrentSession().createSQLQuery(sql).setResultSetMapping("permit_audit_dto_mapping");

		query.setBigDecimal("docId", new BigDecimal(docId));

		List<PermitAudit> permitAudits = query.list();

		return permitAudits;
	}

	@Override
	public List<PermitUpdateUpload> saveUpdatedContacts(PermitUpdateEntity updatedPermit) {

		String sql = "UPDATE tu_permit_updates_stage set contact_status= :contact_status where DOC_STAGE_ID= :docstageid AND STAGE_ID= :stageid ";
		Query query = getCurrentSession().createSQLQuery(sql.toString());
		query.setParameter("contact_status", updatedPermit.getContactStatus());
		query.setParameter("docstageid", updatedPermit.getDocStageId());
		query.setParameter("stageid", updatedPermit.getPermitUpdateId());
		query.executeUpdate();

		List<PermitUpdateUpload> updatedPermits = getUpdatedPermitsContacts(updatedPermit.getDocStageId());

		return updatedPermits;
	}

	@Override
	public List<PermitUpdateEntity> setIgnrContactStatusandDelete() {

		List<PermitUpdateEntity> permitupdatelist = new ArrayList<>();

		String sql1 = "select distinct  stg.business_name, Stg.Ein,STG.DOC_STAGE_ID,stg.stage_id,  stg.contact_status from Tu_Permit_Updates_Stage stg "
				+ " inner  join tu_permit_addr_cont_ignr ignr on " + " stg.ein = ignr.ein "
				+ " and upper(nvl(stg.premise_phone,0)) = upper(nvl(ignr.premise_phone,0)) "
				+ " and upper(nvl(stg.email_address,0)) = upper(nvl(ignr.email_address,0)) "
				+ " and stg.contact_status is null ";

		Query query = getCurrentSession().createSQLQuery(sql1);

		List<Object[]> results = query.list();

		for (Object[] obj : results) {
			PermitUpdateEntity pentity = new PermitUpdateEntity();
			if (obj[1] != null)
				pentity.setEinNum(obj[1].toString());

			if (obj[2] != null)
				pentity.setDocStageId(((BigDecimal) obj[2]).longValue());

			if (obj[3] != null)
				pentity.setPermitUpdateId(((BigDecimal) obj[3]).longValue());
			// set status as ignore
			pentity.setContactStatus("IGNR");

			permitupdatelist.add(pentity);
		}

		for (PermitUpdateEntity permit : permitupdatelist) {
			this.saveUpdatedContacts(permit);
		}

		this.deleteIgnrContactStatus();

		return permitupdatelist;
	}

	@Override
	public List<PermitUpdateEntity> setIgnrAddrStatusandDelete() {

		List<PermitUpdateEntity> permitupdatelist = new ArrayList<>();

		String sql1 = "select distinct stg.business_name,Stg.Ein,STG.DOC_STAGE_ID,stg.stage_id,  stg.address_status from Tu_Permit_Updates_Stage stg "
				+ "inner  join tu_permit_addr_cont_ignr ignr on " + " stg.ein = ignr.ein "
				+ " and upper(nvl(stg.mailing_street,0)) = upper(nvl(ignr.mailing_street,0)) "
				+ " and upper(nvl(stg.mailing_city,0)) = upper(nvl(ignr.mailing_city,0)) "
				+ " and upper( nvl(Stg.Mailing_State,0)) = upper(nvl(ignr.Mailing_State,0)) "
				+ " and upper(nvl(stg.mailing_postal_cd,0)) = upper(nvl(ignr.mailing_postal_cd,0)) "
				+ "and stg.address_status is null ";

		Query query = getCurrentSession().createSQLQuery(sql1);

		List<Object[]> results = query.list();

		for (Object[] obj : results) {
			PermitUpdateEntity pentity = new PermitUpdateEntity();
			if (obj[1] != null)
				pentity.setEinNum(obj[1].toString());

			if (obj[2] != null)
				pentity.setDocStageId(((BigDecimal) obj[2]).longValue());

			if (obj[3] != null)
				pentity.setPermitUpdateId(((BigDecimal) obj[3]).longValue());

			// set status as ignore
			pentity.setAddressStatus("IGNR");

			permitupdatelist.add(pentity);
		}

		for (PermitUpdateEntity permit : permitupdatelist) {
			this.saveUpdatedPermits(permit);
		}

		this.deleteIgnrAddressStatus();

		return permitupdatelist;
	}

	// Delete record from TU_PERMIT_ADDR_CONT_IGNR Table if status is REPLACED
	// OR ADDED
	public void deleteIgnrContactStatus() {
		List<PermitUpdateEntity> permitupdatelist = new ArrayList<>();
		String sql2 = "select distinct  stg.business_name, Stg.Ein,STG.DOC_STAGE_ID,stg.stage_id,  stg.contact_status from Tu_Permit_Updates_Stage stg "
				+ " inner  join tu_permit_addr_cont_ignr ignr on " + " stg.ein = ignr.ein "
				+ " and upper(nvl(stg.premise_phone,0)) = upper(nvl(ignr.premise_phone,0)) "
				+ " and upper(nvl(stg.email_address,0)) = upper(nvl(ignr.email_address,0)) "
				+ " and stg.contact_status IN ('ADDED' , 'REPLD')";

		Query query = getCurrentSession().createSQLQuery(sql2);

		List<Object[]> results2 = query.list();

		for (Object[] obj : results2) {
			PermitUpdateEntity pentity = new PermitUpdateEntity();
			if (obj[1] != null)
				this.deleteIgnrStatusContact(obj[1].toString());
		}
	}

	// Delete record from TU_PERMIT_ADDR_CONT_IGNR Table if status is REPLACED
	// OR ADDED
	public void deleteIgnrAddressStatus() {
		List<PermitUpdateEntity> permitupdatelist = new ArrayList<>();
		String sql2 = " select distinct  stg.business_name, Stg.Ein,STG.DOC_STAGE_ID,stg.stage_id,  stg.contact_status from Tu_Permit_Updates_Stage stg "
				+ " inner  join tu_permit_addr_cont_ignr ignr on " + " stg.ein = ignr.ein "
				+ " and upper(nvl(stg.mailing_street,0)) = upper(nvl(ignr.mailing_street,0)) "
				+ " and upper(nvl(stg.mailing_city,0)) = upper(nvl(ignr.mailing_city,0)) "
				+ " and upper( nvl(Stg.Mailing_State,0)) = upper(nvl(ignr.Mailing_State,0)) "
				+ " and upper(nvl(stg.mailing_postal_cd,0)) = upper(nvl(ignr.mailing_postal_cd,0)) "
				+ " and stg.address_status IN ('ADDED' , 'REPLD')";

		Query query = getCurrentSession().createSQLQuery(sql2);

		List<Object[]> results2 = query.list();

		for (Object[] obj : results2) {
			PermitUpdateEntity pentity = new PermitUpdateEntity();
			if (obj[1] != null)
				this.deleteIgnrStatusAddress(obj[1].toString());
		}
	}

	public void deleteIgnrStatusContact(String einnum) {

		Query query = getCurrentSession().createQuery(
				"delete PermitIgnoreEntity pe" + " where pe.einNum = :einnum and pe.contactStatus = :contact_status");
		query.setParameter("einnum", einnum);
		query.setParameter("contact_status", "IGNR");
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();

	}

	public void deleteIgnrStatusAddress(String einnum) {

		Query query = getCurrentSession().createQuery(
				"delete PermitIgnoreEntity pe" + " where pe.einNum = :einnum and pe.addressStatus =:address_Status");
		query.setParameter("einnum", einnum);
		query.setParameter("address_Status", "IGNR");
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();

	}

	public List<NewCompaniesExportDetail> getNewCompaniesList() {

		List<NewCompaniesExportDetail> newCompaniesExportDetailList = new ArrayList<NewCompaniesExportDetail>();
		NewCompaniesExportDetail newCompaniesExportDetail = null;
		String sql = this.getNewCompaniesSQL();
		Query query = this.getCurrentSession().createSQLQuery(sql);
		List<Object[]> companyDetails = query.list();
		for (Object[] obj : companyDetails) {
			newCompaniesExportDetail = new NewCompaniesExportDetail();
			if (null != obj[0]) {
				newCompaniesExportDetail.setCompanyName((String) obj[0]);
			}
			if (null != obj[1]) {
				newCompaniesExportDetail.setCompanyEIN((String) obj[1]);
			}
			if (null != obj[2]) {
				newCompaniesExportDetail.setCreatedDate((Date) obj[2]);
			}
			if (null != obj[3]) {
				newCompaniesExportDetail.setEmailId((String) obj[3]);
			}
			if (null != obj[4]) {
				newCompaniesExportDetail.setCompanyPrimAddress((String) obj[4]);
			}
			if (null != obj[5]) {
				newCompaniesExportDetail.setWelcomeLetterSentDate((Date) obj[5]);
			}
			newCompaniesExportDetailList.add(newCompaniesExportDetail);
		}
		return newCompaniesExportDetailList;
	}

	@Override
	public List<ComparisonAllDeltaStatusEntity> getAllDeltasForStatusChange(TrueUpComparisonResults model,
			String searchCriteria) {

		String allDeltasSQL = "SELECT * FROM  ANN_TRUEUP_CMPR_ALL_DELTAS_VW ALLDELTA WHERE ALLDELTA.fiscal_yr=:fiscalYr  AND ALLDELTA.source in ('INGESTED','TUFA') ";

		if (null != searchCriteria) {
			allDeltasSQL = allDeltasSQL + "	AND ALLDELTA.ein=:ein" + searchCriteria;
		} else {
			allDeltasSQL = allDeltasSQL + "		AND 	(ALLDELTA.ein=:ein OR ALLDELTA.original_ein=:ein) ";
		}

		allDeltasSQL = allDeltasSQL
				+ "        ORDER BY ALLDELTA.permit_type,ALLDELTA.tobacco_class_nm,ALLDELTA.fiscal_qtr ";

		Query qry = this.getCurrentSession().createSQLQuery(allDeltasSQL);

		qry.setString("ein", model.getEin());
		qry.setString("fiscalYr", model.getFiscalYr());

		List<Object[]> allDeltas = qry.list();

		List<ComparisonAllDeltaStatusEntity> deltaStatuses = new ArrayList<>();
		for (Object[] delta : allDeltas) {

			ComparisonAllDeltaStatusEntity status = new ComparisonAllDeltaStatusEntity();
			if (delta[0] != null)
				status.setEin(delta[0].toString());
			if (delta[4] != null)
				status.setFiscalYr(delta[4].toString());
			if (delta[5] != null)
				status.setTobaccoClassName(delta[5].toString());
			if (delta[6] != null)
				status.setFiscalQtr(delta[6].toString());
			if (delta[7] != null)
				status.setDelta(((BigDecimal) delta[7]).doubleValue());
			if (delta[8] != null)
				status.setPermitType(delta[8].toString());
			if (delta[9] != null)
				status.setDeltaType(delta[9].toString());
			if (delta[10] != null) {
				status.setDeltaStatus(delta[10].toString());
			}
			deltaStatuses.add(status);

		}

		return deltaStatuses;

	}

	@Override
	public List<ComparisonAllDeltaStatusEntity> getAllDeltasForStatusChangeSingle(TrueUpComparisonResults model,
			String searchCriteria) {

		String allDeltasSQL = "SELECT * FROM  ANN_TRUEUP_CMPR_ALLDELT_SNG_VW ALLDELTA WHERE ALLDELTA.fiscal_yr=:fiscalYr  AND ALLDELTA.source in ('INGESTED','TUFA') ";

		if (null != searchCriteria) {
			allDeltasSQL = allDeltasSQL + "	AND ALLDELTA.ein=:ein" + searchCriteria;
		} else {
			allDeltasSQL = allDeltasSQL + "		AND 	(ALLDELTA.ein=:ein OR ALLDELTA.original_ein=:ein) ";
		}

		allDeltasSQL = allDeltasSQL
				+ "        ORDER BY ALLDELTA.permit_type,ALLDELTA.tobacco_class_nm,ALLDELTA.fiscal_qtr ";

		Query qry = this.getCurrentSession().createSQLQuery(allDeltasSQL);

		qry.setString("ein", model.getEin());
		qry.setString("fiscalYr", model.getFiscalYr());

		List<Object[]> allDeltas = qry.list();

		List<ComparisonAllDeltaStatusEntity> deltaStatuses = new ArrayList<>();
		for (Object[] delta : allDeltas) {

			ComparisonAllDeltaStatusEntity status = new ComparisonAllDeltaStatusEntity();
			if (delta[0] != null)
				status.setEin(delta[0].toString());
			if (delta[4] != null)
				status.setFiscalYr(delta[4].toString());
			if (delta[5] != null)
				status.setTobaccoClassName(delta[5].toString());
			if (delta[6] != null)
				status.setFiscalQtr(delta[6].toString());
			if (delta[7] != null)
				status.setDelta(((BigDecimal) delta[7]).doubleValue());
			if (delta[8] != null)
				status.setPermitType(delta[8].toString());
			if (delta[9] != null)
				status.setDeltaType(delta[9].toString());
			if (delta[10] != null) {
				status.setDeltaStatus(delta[10].toString());
			}
			deltaStatuses.add(status);

		}

		return deltaStatuses;

	}
	
	@Override
	public List<TobaccoTypeTTBVol> getTTBVolumeTaxPerTobaccoType(TobaccoTypeTTBVol ttbVol) {

		String tobaccoClass = ttbVol.getTobaccoClass();

		String[] tobaccoTypes = tobaccoClass.split(" ");
		StringBuilder sqlbuilder = new StringBuilder("");
		sqlbuilder.append( " SELECT  ");
		if(!"Cigars".equalsIgnoreCase(tobaccoClass)) {
			sqlbuilder.append( " ttbVol.QTR,");
		}else {
			sqlbuilder.append( " 5 as quarter, ");
		}
		sqlbuilder.append( "ttbVol.EIN,ttbVol.PRODUCT,ttbVol.POUNDS,ttbVol.FISCAL_YR,ttbVol.ESTIMATED_TAX, "
				+ " CASE  ttbVol.PRODUCT "
				+ " WHEN allDelta.TOBACCO_SUB_TYPE_1 THEN allDelta.TOBACCO_SUB_TYPE_1_AMNDTX "
				+ " WHEN allDelta.TOBACCO_SUB_TYPE_2 THEN allDelta.TOBACCO_SUB_TYPE_2_AMNDTX "
				+ " ELSE null END AS AMENDED_TAX "
				// + " from TU_TTB_VOLUME_TAXES_PER_TT ttbVol "
				+ " FROM (SELECT ");
		if(!"Cigars".equalsIgnoreCase(tobaccoClass)) {
			sqlbuilder.append( "ttv.qtr, ");
		}
		sqlbuilder.append( "ttv.fiscal_yr, ttv.ein, ttv.product, SUM(ttv.pounds) as pounds, SUM(ttv.estimated_tax) as estimated_tax "
				+ " 	FROM tu_ttb_volume_taxes_per_tt ttv GROUP BY  " );
		if(!"Cigars".equalsIgnoreCase(tobaccoClass)) {
			sqlbuilder.append( "ttv.qtr, ");
		}
		sqlbuilder.append( " ttv.fiscal_yr, ttv.ein, ttv.product) ttbvol"
				+ " LEFT OUTER JOIN TU_COMPARISON_ALL_DELTA_STS allDelta " 
				+ " ON  allDelta.fiscal_yr=ttbVol.FISCAL_YR "
				+ " AND allDelta.ein = ttbVol.EIN " );
				if("Cigars".equalsIgnoreCase(tobaccoClass)) {
					sqlbuilder.append(" AND allDelta.fiscal_qtr = '1-4' ");
				}else {
					sqlbuilder.append(" AND allDelta.fiscal_qtr = to_char(ttbVol.qtr) " );
				}
				if("Cigarettes".equalsIgnoreCase(tobaccoClass) || "Cigars".equalsIgnoreCase(tobaccoClass)) {
					sqlbuilder.append(" AND alldelta.tobacco_class_nm = ttbvol.product");
				}else {
					sqlbuilder.append(" AND (allDelta.TOBACCO_SUB_TYPE_1 = ttbVol.product OR allDelta.TOBACCO_SUB_TYPE_2 = ttbVol.product)");
				}
				sqlbuilder.append( " AND allDelta.permit_type=:permitType  " 
				+ " WHERE ttbVol.fiscal_yr=:fiscalYr "
				+ " AND ttbVol.ein =:ein  "
				+ " AND ttbVol.product in (:tobaccoTypes) ");
				if(!"Cigars".equalsIgnoreCase(tobaccoClass)) {
					sqlbuilder.append(" AND ttbVol.qtr=:qtr " );
				}
		
		Query query = this.getCurrentSession().createSQLQuery(sqlbuilder.toString());

		query.setLong("fiscalYr", ttbVol.getFiscalYr());
		query.setString("ein", ttbVol.getEin());
		if(!"Cigars".equalsIgnoreCase(tobaccoClass)) {
			query.setInteger("qtr", ttbVol.getQtr());
		}
		query.setParameterList("tobaccoTypes", tobaccoTypes);
		query.setString("permitType", "MANU");

		List<Object[]> ttbVols = query.list();

		List<TobaccoTypeTTBVol> ttbVolTaxes = new ArrayList<>();
		
		if(ttbVols.isEmpty()) {
			String	sql1 = " SELECT  allDelta.fiscal_qtr, allDelta.EIN,   allDelta.TOBACCO_SUB_TYPE_1, allDelta.FISCAL_YR, allDelta.TOBACCO_SUB_TYPE_1_AMNDTX,allDelta.TOBACCO_SUB_TYPE_2, allDelta.TOBACCO_SUB_TYPE_2_AMNDTX"
					+ " FROM TU_COMPARISON_ALL_DELTA_STS allDelta " 
	                + " WHERE allDelta.fiscal_yr=:fiscalYr "
					+ " AND allDelta.ein =:ein"+"  AND allDelta.fiscal_qtr=:qtr " + "AND allDelta.permit_type=:permitType "
							+ "AND allDelta.TOBACCO_SUB_TYPE_1 in  (:tobaccoTypes) "
	                + "AND allDelta.TOBACCO_SUB_TYPE_2 IN (:tobaccoTypes) ";
			
			Query query1 = this.getCurrentSession().createSQLQuery(sql1);

			query1.setLong("fiscalYr", ttbVol.getFiscalYr());
			query1.setString("ein", ttbVol.getEin());
			query1.setParameter("qtr", String.valueOf(ttbVol.getQtr()));
			query1.setParameterList("tobaccoTypes", tobaccoTypes);
			query1.setString("permitType", "MANU");

			List<Object[]> ttbVols1 = query1.list();
			
			for (Object[] obj : ttbVols1) {

				TobaccoTypeTTBVol ttVolTax = new TobaccoTypeTTBVol();
				
				if (obj[0] != null)
					ttVolTax.setQtr(Integer.parseInt(obj[0].toString()));

				if (obj[1] != null)
					ttVolTax.setEin(obj[1].toString());

				if (obj[2] != null)
					ttVolTax.setTobaccoClass(obj[2].toString());

				if (obj[3] != null)
					ttVolTax.setFiscalYr(((BigDecimal) obj[3]).longValue());

				if (obj[4] != null)
					ttVolTax.setAmendedTax(((BigDecimal) obj[4]).doubleValue());

				ttbVolTaxes.add(ttVolTax);
				
				TobaccoTypeTTBVol ttVolTax1 = new TobaccoTypeTTBVol();
				
				if (obj[0] != null)
					ttVolTax1.setQtr(Integer.parseInt(obj[0].toString()));

				if (obj[1] != null)
					ttVolTax1.setEin(obj[1].toString());
				
				if (obj[5] != null)
					ttVolTax1.setTobaccoClass(obj[5].toString());
				
				if (obj[3] != null)
					ttVolTax1.setFiscalYr(((BigDecimal) obj[3]).longValue());
				
				if (obj[6] != null)
					ttVolTax1.setAmendedTax(((BigDecimal) obj[6]).doubleValue());
				
				ttbVolTaxes.add(ttVolTax1);
			}
			
		}
		else {
		for (Object[] obj : ttbVols) {

			TobaccoTypeTTBVol ttVolTax = new TobaccoTypeTTBVol();
			if (obj[0] != null)
				ttVolTax.setQtr(((BigDecimal) obj[0]).intValue());

			if (obj[1] != null)
				ttVolTax.setEin(obj[1].toString());

			if (obj[2] != null)
				ttVolTax.setTobaccoClass(obj[2].toString());

			if (obj[3] != null)
				ttVolTax.setPounds(((BigDecimal) obj[3]).doubleValue());

			if (obj[4] != null)
				ttVolTax.setFiscalYr(((BigDecimal) obj[4]).longValue());

			if (obj[5] != null)
				ttVolTax.setEstimatedTax(((BigDecimal) obj[5]).doubleValue());

			if (obj[6] != null)
				ttVolTax.setAmendedTax(((BigDecimal) obj[6]).doubleValue());

			ttbVolTaxes.add(ttVolTax);

		}
		}
		return ttbVolTaxes;
		

	}

	/**
	 * Get companies for the Company Tracking module. I had to place this here
	 * because of the COUNT(*) OVER(). This is needed for the paginated grid.
	 * This cannot be done in HQL. This query can be switched to HQL if that
	 * count is no longer needed.
	 */
	@Override
	public List<CompanyEntity> getCompanyTrackingInformationForSearch(CompanySearchCriteria criteria) {
		StringBuilder sql = new StringBuilder(this.getNewCompaniesSQL());
		if (testCriterion(criteria.getCompanyNameFilter())) {
			sql.append(" AND upper(CMP.LEGAL_NM) LIKE :legalName ");
		}
		if (testCriterion(criteria.getCompanyEINFilter())) {
			sql.append(" AND CMP.EIN LIKE :einNumber ");
		}
		if (criteria.getCompanyCreatedDtFilter() != null) {
			sql.append(
					" AND (Extract(Day from CMP.CREATED_DT) = :createdDtDay AND Extract(Month from CMP.CREATED_DT) = :createdDtMonth AND Extract(Year from CMP.CREATED_DT) = :createdDtYear) ");
		}
		if (criteria.getWelcomeLetterSentDtFilter() != null) {
			sql.append(
					" AND (Extract(Day from CMP.WELCOME_LETTER_SENT_DT) = :sentDtDay AND Extract(Month from CMP.WELCOME_LETTER_SENT_DT) = :sentDtMonth AND Extract(Year from CMP.WELCOME_LETTER_SENT_DT) = :sentDtYear) ");
		}
		if (testCriterion(criteria.getEmailAddressFlagFilter())) {
			sql.append(" AND CMP.EMAIL_ADDRESS_FLAG = :emailAddressFlag ");
		}
		if (testCriterion(criteria.getPhysicalAddressFlagFilter())) {
			sql.append(" AND CMP.PHYSICAL_ADDRESS_FLAG = :physicalAddressFlag ");
		}
		if (testCriterion(criteria.getWelcomeLetterFlagFilter())) {
			sql.append(" AND CMP.WELCOME_LETTER_FLAG = :welcomeLetterFlag ");
		}
		sql.insert(0, "SELECT res.*, COUNT(*) OVER() FROM ( ").append(" ) res");
		Query query = this.getPaginatedSQLQuery(new StringBuffer(sql.toString()));
		;

		if (testCriterion(criteria.getCompanyNameFilter())) {
			query.setParameter("legalName", "%" + criteria.getCompanyNameFilter().toUpperCase() + "%");
		}
		if (testCriterion(criteria.getCompanyEINFilter())) {
			query.setParameter("einNumber", "%" + criteria.getCompanyEINFilter() + "%");
		}
		if (criteria.getCompanyCreatedDtFilter() != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(criteria.getCompanyCreatedDtFilter());
			query.setParameter("createdDtDay", cal.get(Calendar.DATE));
			query.setParameter("createdDtMonth", cal.get(Calendar.MONTH) + 1);
			query.setParameter("createdDtYear", cal.get(Calendar.YEAR));
		}
		if (criteria.getWelcomeLetterSentDtFilter() != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(criteria.getWelcomeLetterSentDtFilter());
			query.setParameter("sentDtDay", cal.get(Calendar.DATE));
			query.setParameter("sentDtMonth", cal.get(Calendar.MONTH) + 1);
			query.setParameter("sentDtYear", cal.get(Calendar.YEAR));
		}
		if (testCriterion(criteria.getEmailAddressFlagFilter())) {
			query.setParameter("emailAddressFlag", criteria.getEmailAddressFlagFilter());
		}
		if (testCriterion(criteria.getPhysicalAddressFlagFilter())) {
			query.setParameter("physicalAddressFlag", criteria.getPhysicalAddressFlagFilter());
		}
		if (testCriterion(criteria.getWelcomeLetterFlagFilter())) {
			query.setParameter("welcomeLetterFlag", criteria.getWelcomeLetterFlagFilter());
		}

		List<CompanyEntity> companyEntityResults = new ArrayList<CompanyEntity>();
		List<Object[]> results = query.list();

		for (Object[] result : results) {
			CompanyEntity companyTrackingInformation = new CompanyEntity();
			if (result[0] != null) {
				companyTrackingInformation.setLegalName(result[0].toString());
			}
			if (result[1] != null) {
				companyTrackingInformation.setEIN(result[1].toString());
			}
			if (result[2] != null) {
				companyTrackingInformation.setCreatedDt((Date) result[2]);
			}
			if (result[5] != null) {
				companyTrackingInformation.setWelcomeLetterSentDt((Date) result[5]);
			}
			if (result[6] != null) {
				companyTrackingInformation.setWelcomeLetterFlag(result[6].toString());
			}
			if (result[7] != null) {
				companyTrackingInformation.setEmailAddressFlag(result[7].toString());
			}
			if (result[8] != null) {
				companyTrackingInformation.setPhysicalAddressFlag(result[8].toString());
			}
			if (result[9] != null) {
				companyTrackingInformation.setCompanyId(((BigDecimal) result[9]).longValue());
			}
			if (result[10] != null) {
				this.setSQLQueryTotalRowCount(((BigDecimal) result[10]).intValue());
				;
			}
			companyEntityResults.add(companyTrackingInformation);
		}

		return companyEntityResults;
	}

	private String getNewCompaniesSQL() {
		return "SELECT CMP.LEGAL_NM, CMP.EIN, CMP.CREATED_DT, CON.EMAIL_ADDRESS, NVL(ADR.ATTENTION,'')|| DECODE(ADR.ATTENTION, null,'',',') || NVL(ADR.STREET_ADDRESS,'') || DECODE(ADR.STREET_ADDRESS, null,'',',') "
				+ "|| NVL(ADR.SUITE,'')|| DECODE(ADR.SUITE, null,'',',') || NVL(ADR.CITY,'')|| DECODE(ADR.CITY, null,'',',') || NVL(DECODE(ADR.COUNTRY_CD, 'US',STATE.STATE_NM,ADR.PROVINCE),'')  || DECODE(DECODE(ADR.COUNTRY_CD, 'US',STATE.STATE_NM,ADR.PROVINCE), null,'',',')|| NVL(ADR.POSTAL_CD,'')|| DECODE(ADR.POSTAL_CD, null,'',',') || NVL(country.ISO_COUNTRY_NM,'') as ADDRESS, CMP.WELCOME_LETTER_SENT_DT, CMP.WELCOME_LETTER_FLAG, CMP.EMAIL_ADDRESS_FLAG, CMP.PHYSICAL_ADDRESS_FLAG, CMP.COMPANY_ID"
				+ " FROM TU_COMPANY CMP "
				+ " LEFT JOIN TU_ADDRESS ADR ON CMP.COMPANY_ID      = ADR.COMPANY_ID AND ADR.ADDRESS_TYPE_CD = 'PRIM'"
				+ " LEFT JOIN (SELECT CON.COMPANY_ID, MIN(CON.CONTACT_ID) TMP_CONTACT_ID FROM TU_CONTACT CON WHERE CON.primary_contact_sequence = 1 GROUP BY CON.COMPANY_ID )TMP ON CMP.COMPANY_ID = TMP.COMPANY_ID"
				+ " LEFT JOIN TU_CONTACT CON ON CON.CONTACT_ID      = TMP.TMP_CONTACT_ID"
				+ " LEFT JOIN TU_STATE state ON ADR.STATE = STATE.STATE_CD AND ADR.COUNTRY_CD='US'"
				+ " LEFT JOIN TU_ISO_CNTRY country ON country.ISO_CNTRY_CD = ADR.COUNTRY_CD"
				+ " WHERE ((CMP.WELCOME_LETTER_FLAG = 'Y' AND CMP.WELCOME_LETTER_SENT_DT    IS NOT NULL) OR CMP.WELCOME_LETTER_FLAG    = 'N')";

	}

	@Override
	public List<CBPEntry> getHTSMissingAssociationData(long fiscalYear) {

		String sql = "SELECT ce.cbp_entry_id, " + "  ce.estimated_tax, " + "  ce.entry_summ_dt, " + "  ce.entry_dt, "
				+ "  ce.hts_cd, " + "  tc.tobacco_class_id, " + "  tc.tobacco_class_nm, " + "  ci.cbp_importer_id, "
				+ "  ci.association_type_cd, " + "  ci.importer_ein, "
				+ "  CASE WHEN EXISTS (select * FROM tu_company c where ci.importer_ein = c.ein ) THEN  "
				+ "      (select c.legal_nm  FROM tu_company c where ci.importer_ein = c.ein) "
				+ "      ELSE  ci.importer_nm END  AS importer_nm, " + "  ci.consignee_ein, "
				+ "  CASE WHEN EXISTS (select * FROM tu_company c where ci.consignee_ein = c.ein ) THEN  "
				+ "      (select c.legal_nm  FROM tu_company c where ci.consignee_ein = c.ein) "
				+ "      ELSE  ci.consignee_nm END  AS consignee_nm, " + " other_tc.other_tobacco_types , "
				+ "  ce.fiscal_year " + "FROM tu_cbp_importer ci " + "INNER JOIN tu_cbp_entry ce "
				+ "ON ci.cbp_importer_id = ce.cbp_importer_id " + "LEFT JOIN tu_tobacco_class tc "
				+ "ON tc.tobacco_class_id = ce.tobacco_class_id " + "LEFT JOIN " + "  (SELECT cbp_importer_id, "
				+ "    LISTAGG(tobacco_class_nm,',') WITHIN GROUP ( "
				+ "  ORDER BY cbp_importer_id) other_tobacco_types " + "  FROM " + "    (SELECT ci.cbp_importer_id, "
				+ "      tc.tobacco_class_nm " + "    FROM tu_cbp_importer ci " + "    INNER JOIN tu_cbp_entry ce "
				+ "    ON ci.cbp_importer_id = ce.cbp_importer_id " + "    INNER JOIN tu_tobacco_class tc "
				+ "    ON tc.tobacco_class_id = ce.tobacco_class_id " + "    WHERE ci.fiscal_yr     =2018 "
				+ "    GROUP BY ci.cbp_importer_id, " + "      tc.tobacco_class_nm "
				+ "    ORDER BY ci.cbp_importer_id " + "    ) " + "  GROUP BY cbp_importer_id "
				+ "  ) other_tc ON ci.cbp_importer_id = other_tc.cbp_importer_id "
				+ "WHERE ci.fiscal_yr                 =:fiscalYr " + "AND ce.hts_cd  IS NULL "
				+ "AND ( (ci.association_type_cd  IS NOT NULL  AND ci.association_type_cd NOT IN ('EXCL','UNKN')) "
				+ "OR  EXISTS (select * from tu_company c where c.ein = ci.CONSIGNEE_EIN OR c.ein = ci.importer_ein) "
				+ ") " + " " + "ORDER BY ce.cbp_entry_id";

		Query query = this.getCurrentSession().createSQLQuery(sql);
		query.setLong("fiscalYr", fiscalYear);
		List<Object[]> cbpEntries = query.list();

		List<CBPEntry> cbpEnts = new ArrayList<CBPEntry>();

		for (Object[] obj : cbpEntries) {

			CBPEntry cbpEntry = new CBPEntry();
			CBPImporter cbpImporter = new CBPImporter();
			TobaccoClass tt = new TobaccoClass();

			if (obj[0] != null)
				cbpEntry.setCbpEntryId(((BigDecimal) obj[0]).longValue());
			if (obj[1] != null)
				cbpEntry.setEstimatedTax(((BigDecimal) obj[1]).doubleValue());
			if (obj[2] != null)
				cbpEntry.setEntrySummDt(CTPUtil.parseDateToString(((Date) obj[2])));
			if (obj[3] != null)
				cbpEntry.setEntryDt(CTPUtil.parseDateToString(((Date) obj[3])));
			if (obj[4] != null)
				cbpEntry.setHtsCd(obj[4].toString());
			if (obj[5] != null) {
				tt.setTobaccoClassId(((BigDecimal) obj[5]).longValue());
				cbpEntry.setTobaccoClassId(((BigDecimal) obj[5]).longValue());
				cbpEntry.setProvidedAnswer(true);
			}
			if (obj[6] != null) {
				tt.setTobaccoClassNm(obj[6].toString());
				cbpEntry.setTobaccoType(obj[6].toString());
			}
			if (obj[7] != null)
				cbpImporter.setCbpImporterId(((BigDecimal) obj[7]).longValue());
			if (obj[8] != null)
				cbpImporter.setAssociationTypeCd(obj[8].toString());
			if (obj[9] != null)
				cbpImporter.setImporterEIN(obj[9].toString());
			if (obj[10] != null)
				cbpImporter.setImporterNm(obj[10].toString());
			if (obj[11] != null)
				cbpImporter.setConsigneeEIN(obj[11].toString());
			if (obj[12] != null)
				cbpImporter.setConsigneeNm(obj[12].toString());
			if (obj[13] != null) {
				String[] otherTTArry = obj[13].toString().split(",");
				cbpImporter.setOtherTobaccoTypesReported(new HashSet<>(Arrays.asList(otherTTArry)));
			}

			if (obj[14] != null)
				cbpEntry.setFiscalYear(obj[14].toString());

			cbpEntry.setTobaccoClass(tt);
			cbpEntry.setCbpImporter(cbpImporter);
			cbpEnts.add(cbpEntry);
		}

		return cbpEnts;

	}

	@Override
	public List<TrueUpComparisonResults> getDeltasAffectedByCompareAllActions(long fiscalYr, String ein,
			String allAction) {

		String condition = "";
		String msReady = "MARKET SHARE READY";
		String reset = "RESET";
		String association = "Association";

		if (msReady.equalsIgnoreCase(allAction)) {
			condition = " AND status in ('In Progress','Review') "
					+ "  AND NOT (Tobacco_Class_NM IN ('Chew/Snuff','Pipe/Roll-Your-Own') "
					+ "  AND source IN ('INGESTED') ) "
					+ "  AND deltaexcisetax_flag <> 'Review' ";
		} else if (reset.equalsIgnoreCase(allAction)) {
			condition = " AND status not in ('Review') ";
		} else if (association.equalsIgnoreCase(allAction)) {
			condition = " AND status in ('In Progress','Review','Associated') ";
			condition += " AND source IN ('INGESTED') ";
		} else {
			condition = " AND status in ('In Progress','Review') ";
		}

		String sql = "SELECT ein,company_nm,fiscal_yr,permit_type,status,delta_cnt,tobacco_class_nm,delta,fiscal_qtr,original_ein, original_company_nm, deltaexcisetax_flag, source  FROM "
				+ "( "
				+ " SELECT ein,company_nm,fiscal_yr,permit_type,tobacco_class_nm, FISCAL_QTR,DELTA,count(*) over() as delta_cnt,status, original_ein, original_company_nm, deltaexcisetax_flag, source "
				+ " FROM ANN_TRUEUP_CMPR_ALL_DELTAS_VW ALLDELTA "
				+ " WHERE source in ('INGESTED','TUFA') AND ((ein =:ein AND ORIGINAL_EIN IS NULL ) OR (ORIGINAL_EIN = :ein ) )  AND  fiscal_yr=:fiscalYr  "
				+ condition + ")  Order by delta desc";

		Query query = this.getCurrentSession().createSQLQuery(sql);

		query.setParameter("ein", ein);
		query.setLong("fiscalYr", fiscalYr);

		List<Object[]> deltas = query.list();

		List<TrueUpComparisonResults> ttDeltasList = new ArrayList<>();
		for (Object[] obj : deltas) {
			TrueUpComparisonResults ttDeltas = new TrueUpComparisonResults();

			if (obj[0] != null)
				ttDeltas.setEin(obj[0].toString());
			if (obj[1] != null)
				ttDeltas.setLegalName(obj[1].toString());
			if (obj[2] != null)
				ttDeltas.setFiscalYr(obj[2].toString());
			if (obj[3] != null)
				ttDeltas.setPermitType(obj[3].toString());
			if (obj[4] != null)
				ttDeltas.setAllDeltaStatus(obj[4].toString());
			if (obj[5] != null)
				ttDeltas.setAffectedDeltaCnt(((BigDecimal) obj[5]).intValue());
			if (obj[6] != null)
				ttDeltas.setTobaccoClass(obj[6].toString());
			if (obj[7] != null)
				ttDeltas.setTotalDeltaTax(((BigDecimal) obj[7]).doubleValue());
			if (obj[8] != null)
				ttDeltas.setQuarter(obj[8].toString());
			if (obj[9] != null)
				ttDeltas.setOriginalEIN(obj[9].toString());
			if (obj[10] != null)
				ttDeltas.setOriginalName(obj[10].toString());
			if (obj[11] != null)
				ttDeltas.setDeltaexcisetaxFlag(obj[11].toString());
			if (obj[12] != null)
				ttDeltas.setDataType(obj[12].toString());
			ttDeltasList.add(ttDeltas);
		}

		return ttDeltasList;
	}

	@Override
	public List<TrueUpComparisonResults> getDeltasAffectedByCompareAllActionsSingle(long fiscalYr, String ein,
			String allAction) {

		String condition = "";
		String msReady = "MARKET SHARE READY";
		String reset = "RESET";
		String association = "Association";

		if (msReady.equalsIgnoreCase(allAction)) {
			condition = " AND status in ('In Progress','Review','Excluded') "
					+ "  AND NOT (Tobacco_Class_NM IN ('Chew/Snuff','Pipe/Roll-Your-Own') "
					+ "  AND source IN ('INGESTED') ) "
					+ "  AND deltaexcisetax_flag <> 'Review' ";
		} else if (reset.equalsIgnoreCase(allAction)) {
			condition = " AND status not in ('Review') ";
		} else if (association.equalsIgnoreCase(allAction)) {
			condition = " AND status in ('In Progress','Review','Associated') ";
			condition += " AND source IN ('INGESTED') ";
			condition += " AND permit_type IN ('IMPT') ";
		} else {
			condition = " AND status in ('In Progress','Review','MSReady') ";
		}

		String sql = "SELECT ein,company_nm,fiscal_yr,permit_type,status,delta_cnt,tobacco_class_nm,delta,fiscal_qtr,original_ein, original_company_nm, deltaexcisetax_flag, source  FROM "
				+ "( "
				+ " SELECT ein,company_nm,fiscal_yr,permit_type,tobacco_class_nm, FISCAL_QTR,DELTA,count(*) over() as delta_cnt,status, original_ein, original_company_nm, deltaexcisetax_flag, source "
				+ " FROM ANN_TRUEUP_CMPR_ALLDELT_SNG_VW ALLDELTA "
				+ " WHERE source in ('INGESTED','TUFA') AND ((ein =:ein AND ORIGINAL_EIN IS NULL ) OR (ORIGINAL_EIN = :ein ) )  AND  fiscal_yr=:fiscalYr  "
				+ condition + ")  Order by delta desc";

		Query query = this.getCurrentSession().createSQLQuery(sql);

		query.setParameter("ein", ein);
		query.setLong("fiscalYr", fiscalYr);

		List<Object[]> deltas = query.list();

		List<TrueUpComparisonResults> ttDeltasList = new ArrayList<>();
		for (Object[] obj : deltas) {
			TrueUpComparisonResults ttDeltas = new TrueUpComparisonResults();

			if (obj[0] != null)
				ttDeltas.setEin(obj[0].toString());
			if (obj[1] != null)
				ttDeltas.setLegalName(obj[1].toString());
			if (obj[2] != null)
				ttDeltas.setFiscalYr(obj[2].toString());
			if (obj[3] != null)
				ttDeltas.setPermitType(obj[3].toString());
			if (obj[4] != null)
				ttDeltas.setAllDeltaStatus(obj[4].toString());
			if (obj[5] != null)
				ttDeltas.setAffectedDeltaCnt(((BigDecimal) obj[5]).intValue());
			if (obj[6] != null)
				ttDeltas.setTobaccoClass(obj[6].toString());
			if (obj[7] != null)
				ttDeltas.setTotalDeltaTax(((BigDecimal) obj[7]).doubleValue());
			if (obj[8] != null)
				ttDeltas.setQuarter(obj[8].toString());
			if (obj[9] != null)
				ttDeltas.setOriginalEIN(obj[9].toString());
			if (obj[10] != null)
				ttDeltas.setOriginalName(obj[10].toString());
			if (obj[11] != null)
				ttDeltas.setDeltaexcisetaxFlag(obj[11].toString());
			if (obj[12] != null)
				ttDeltas.setDataType(obj[12].toString());
			ttDeltasList.add(ttDeltas);
		}

		return ttDeltasList;
	}

	
	@Override
	public String getDateOfLatestStartedreport(long permitId) {
		String sql = "SELECT " + "     pe.permit_id, " + "     pe.permit_num, "
				+ "     to_char(MAX(last_day(TO_DATE(rp.month || '/' || rp.year,'MM/YYYY') )),'MM/DD/YYYY') LATEST_DT "
				+ " FROM " + "     tu_permit pe, " + "     tu_permit_period pp, " + "     tu_rpt_period rp, "
				+ "     tu_period_status ps " + " WHERE " + "     pe.permit_id = pp.permit_id "
				+ "     AND pe.permit_id = pp.permit_id " + "     AND pp.period_id = rp.period_id "
				+ "     AND ( pp.permit_id = ps.permit_id " + "           AND pp.period_id = ps.period_id ) "
				+ "     AND pe.permit_id = :permitId " + "     AND ps.period_status_type_cd <> 'NSTD' " + " GROUP BY "
				+ "     pe.permit_id, " + "     pe.permit_num";
		Query query = this.getCurrentSession().createSQLQuery(sql);
		query.setParameter("permitId", permitId);

		List<Object[]> results = query.list();
		return (String) ((results == null || results.size() != 1) ? null : results.get(0)[2]);
	}

	@Override
	public List<Long> getPeriodIdsAfterDate(Long permitId, String inactiveDate) {
		// TODO Auto-generated method stub
		String sql = "SELECT " + "     ps.period_id " + " FROM " + "     tu_permit pe, " + "     tu_permit_period pp, "
				+ "     tu_rpt_period rp, " + "     tu_period_status ps " + " WHERE "
				+ "     pe.permit_id = pp.permit_id " + "     AND pe.permit_id = pp.permit_id "
				+ "     AND pp.period_id = rp.period_id " + "     AND ( pp.permit_id = ps.permit_id "
				+ "           AND pp.period_id = ps.period_id ) " + "     AND TO_DATE(rp.month "
				+ "               || '/' " + "               || rp.year,'Mon/yyyy') > TO_DATE(:date,'mm/dd/yyyy') "
				+ "     AND pe.permit_id = :permitId";
		Query query = this.getCurrentSession().createSQLQuery(sql);
		query.setParameter("date", inactiveDate);
		query.setParameter("permitId", permitId);

		List<Long> periodIds = new ArrayList<>();
		List<BigDecimal> results = query.list();
		for (BigDecimal obj : results) {
			if (obj != null) {
				periodIds.add((obj).longValue());
			}
		}
		return periodIds;
	}

	/**
	 * Method removes duplicate permit number values from a ";" DLM list
	 */
	private String removeDuplicatePermitNumbers(String permitNumbers) {
		HashSet<String> duplicatePerm = new HashSet<String>(Arrays.asList(permitNumbers.split(";")));
		String singlePermitNumber = StringUtils.join(duplicatePerm, ';');
		return singlePermitNumber;
	}

	@Override
	public List<CBPSummaryFile> getSummaryFileExport(String ein, String companyId, String fiscalYear, String quarter,
			String tobaccoType) {

		if (tobaccoType.equals("Roll Your Own")) {
			tobaccoType = "Roll-Your-Own";
		}

		String sql = "SELECT entry_summary_number, importer_number, importer_name, ultimate_consignee_number, ultimate_consignee_name, entry_summary_date, entry_date, entry_type_code_and_desc, estimated_tax, total_ascertained_tax_amount"
				+ " FROM EXPORT_CBP_SUMMARY WHERE fiscal_year=:fiscalYear and fiscal_qtr=:quarter and tobacco_class_nm=:tobaccoType";

		if (companyId != null && !companyId.equals("0")) {
			sql += " AND company_id=:companyId ";
		} else {
			sql += " AND importer_number=:ein ";
		}
		
		Query query = this.getCurrentSession().createSQLQuery(sql);
		query.setParameter("fiscalYear", fiscalYear);
		query.setParameter("quarter", quarter);
		query.setParameter("tobaccoType", tobaccoType);
		if (companyId != null && !companyId.equals("0")) {
			query.setParameter("companyId", companyId);
		} else {
			if(ein != null) {
				ein = ein.substring(0, 2) + "-" + ein.substring(2);
			}
			query.setParameter("ein", ein);
		}
		List<CBPSummaryFile> export = new ArrayList<CBPSummaryFile>();
		List<Object[]> results = query.list();
		for (Object[] result : results) {

			CBPSummaryFile row = new CBPSummaryFile();
			if (result[0] != null) {
				row.setEntrySummaryNumber(result[0].toString());
			}
			if (result[1] != null) {
				row.setImporterNumber(result[1].toString());
			}
			if (result[2] != null) {
				row.setImporterName(result[2].toString());
			}
			if (result[3] != null) {
				row.setUltimateConsigneeNumber(result[3].toString());
			}
			if (result[4] != null) {
				row.setUltimateConsigneeName(result[4].toString());
			}
			if (result[5] != null) {
				row.setEntrySummaryDate(result[5].toString());
			}
			if (result[6] != null) {
				row.setEntryDate(result[6].toString());
			}
			if (result[7] != null) {
				row.setEntryTypeCodeDescription(result[7].toString());
			}
			if (result[8] != null) {
				row.setEstimatedTax(Double.parseDouble(result[8].toString()));
			}
			if (result[9] != null) {
				row.setTotalAscertainedtax(Double.parseDouble(result[9].toString()));
			}

			export.add(row);
		}
		return export;
	}

	@Override
	public List<CBPDetailsFile> getDetailsFileExport(String ein, String companyId, String fiscalYear, String quarter,
			String tobaccoType, String ingestionType) {
		String sql;
		if (tobaccoType.equals("Roll Your Own")) 
			tobaccoType = "Roll-Your-Own";

				
		 if(quarter.equals("1-4"))
					quarter = "5";
		//ingestiontype used as missing literal just to avoid another api call as underline functionality is same
		 if(ingestionType.equalsIgnoreCase("missing")) {
			 sql = "SELECT entry_summary_number, importer_number, importer_name, ultimate_consignee_number, ultimate_consignee_name, entry_summary_date, entry_date, entry_type_code, line_number, hts_number, hts_description, line_tariff_qty1, line_tariff_goods_value_amount, ir_tax_amount,import_date"
						+ " FROM EXPORT_MISSING_CBP_LINES WHERE fiscal_year=:fiscalYear and tobacco_class_nm=:tobaccoType and missing_flag='Y' ";	
		 } else {
			 sql = "SELECT entry_summary_number, importer_number, importer_name, ultimate_consignee_number, ultimate_consignee_name, entry_summary_date, entry_date, entry_type_code, line_number, hts_number, hts_description, line_tariff_qty1, line_tariff_goods_value_amount, ir_tax_amount,import_date"
						+ " FROM EXPORT_CBP_DETAILS_SNG WHERE fiscal_year=:fiscalYear and fiscal_qtr=:quarter and tobacco_class_nm=:tobaccoType";
		 }
		 		
        
		if (companyId != null && !companyId.equals("0")) {
			sql += " AND company_id=:companyId ";
		} else {
			sql += " AND importer_number=:ein ";
		}
		
		Query query = this.getCurrentSession().createSQLQuery(sql);
		query.setParameter("fiscalYear", fiscalYear);
		if(!ingestionType.equalsIgnoreCase("missing")) {
			query.setParameter("quarter", quarter);
		}
		
		query.setParameter("tobaccoType", tobaccoType);
		if (companyId != null && !companyId.equals("0")) {
			query.setParameter("companyId", companyId);
		} else {
			if(ein != null) {
				ein = ein.substring(0, 2) + "-" + ein.substring(2);
			}
			query.setParameter("ein", ein);
		}
		List<CBPDetailsFile> export = new ArrayList<CBPDetailsFile>();
		List<Object[]> results = query.list();
		for (Object[] result : results) {
			CBPDetailsFile row = new CBPDetailsFile();
			if (result[0] != null) {
				row.setEntrySummaryNumber(result[0].toString());
			}
			if (result[1] != null) {
				row.setImporterNumber(result[1].toString());
			}
			if (result[2] != null) {
				row.setImporterName(result[2].toString());
			}
			if (result[3] != null) {
				row.setUltimateConsigneeNumber(result[3].toString());
			}
			if (result[4] != null) {
				row.setUltimateConsigneeName(result[4].toString());
			}
			if (result[5] != null) {
				row.setEntrySummaryDate(result[5].toString());
			}
			if (result[6] != null) {
				row.setEntryDate(result[6].toString());
			}
			if (result[7] != null) {
				row.setEntryTypeCode(result[7].toString());
			}
			if (result[8] != null) {
				row.setLineNumber(Long.parseLong(result[8].toString()));
			}
			if (result[9] != null) {
				row.setHtsNumber(Long.parseLong(result[9].toString()));
			}
			if (result[10] != null) {
				row.setHtsDescription(result[10].toString());
			}
			if (result[11] != null) {
				row.setLineTarrifQty1(Double.parseDouble(result[11].toString()));
			}
			if (result[12] != null) {
				row.setLineTariffGoodsValueAmount(Double.parseDouble(result[12].toString()));
			}
			if (result[13] != null) {
				row.setIrTaxAmount(Double.parseDouble(result[13].toString()));
			}
			if (result[14] != null) {
                row.setImportDate(result[14].toString());
            }

			export.add(row);
		}
		return export;
	}

	// Used if tobacco string is needed in the SQL in a place that won't allow
	// parameters: IE Pivot
	private boolean isTobaccoClassStringValid(String tobaccoClass) {
		switch (tobaccoClass) {
		case "Cigarettes":
		case "Cigars":
		case "Pipe":
		case "Roll Your Own":
		case "Chew":
		case "Snuff":
			return true;
		default:
			return false;
		}
	}
	@Override
	public List<PermitCommentAttachmentsEntity> findCommentDocbyId(long commentId) {
		Query query = getCurrentSession().createSQLQuery("Select DOCUMENT_ID, DOC_DESC, FORM_TYPES, "
				+ "CREATED_DT, CREATED_BY FROM TU_PERMIT_EXCL_COMMENT_DOC WHERE COMMENT_SEQ = :commentId");
		query.setParameter("commentId", commentId);
		List<Object[]> docData = query.list();
		List<PermitCommentAttachmentsEntity> documents = new ArrayList<>();
		for (Object[] obj : docData) {
			if (obj != null) {
				PermitCommentAttachmentsEntity document = new PermitCommentAttachmentsEntity();
				document.setDocumentId(((BigDecimal) obj[0]).longValue());
				document.setDocDesc(obj[1] != null ? obj[1].toString() : null);
				document.setFormTypes(obj[2] != null ? obj[2].toString() : null);
				document.setCreatedDt((Date) obj[3]);
				document.setCreatedBy(obj[4] != null ? obj[4].toString() : null);
				documents.add(document);
			}
		}
		return documents;
	}

	@Override
	public List<ExcludePermitDetails> getPermitExcludeList(String fiscalYear, long companyId,int scopeId) {
		Query query = getCurrentSession().createSQLQuery("select ASSMNT_YR,ASSMNT_QTR,excl_scope_id from PERMIT_EXCL_STATUS_VW  where  COMPANY_ID= :companyId and EXCL_SCOPE_ID != :scopeId");
		List<Integer>intList = new ArrayList<Integer>();
		query.setParameter("companyId", companyId);
		query.setParameter("scopeId", scopeId);
		List<Object[]> quarterList = query.list();
		List<ExcludePermitDetails> excludeList =  new ArrayList<ExcludePermitDetails>();
		for(Object[] obj: quarterList) {
			ExcludePermitDetails bean = new ExcludePermitDetails();
			bean.setYear(Long.parseLong((String)obj[0]));
			bean.setQuarter(Integer.parseInt((String)obj[1]));
			bean.setExclusionScopeId(((BigDecimal)obj[2]).intValue());
			excludeList.add(bean);
		}
		return excludeList;
	}
	/**
	 * remove date config table for single file
	 * @param fiscalYr
	 */
	public void removeCBPDTConfig(long fiscalYr){
		
		String deleteCBPDtConfigSQL = "delete from TU_CBP_DT_CONFIG where FISCAL_YEAR = :fiscalYr";
		Query deleteCBPExclQury = this.getCurrentSession().createSQLQuery(deleteCBPDtConfigSQL);
		deleteCBPExclQury.setParameter("fiscalYr", fiscalYr);
		deleteCBPExclQury.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();
		
	}
	
	
	public List<CBPLineEntryUpdateInfo> getCBPLineEntry(CBPDateConfigEntity cbpDateConfigEntity, boolean isBulkReallocation) throws ParseException {
		long prevYr = cbpDateConfigEntity.getFiscalYr() -1;
		String startDate = "01-OCT-"+prevYr;
		String endDate = "30-SEP-"+cbpDateConfigEntity.getFiscalYr();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		int resultNo = 0;	
		String excludeFlag = null;

		String sql = "SELECT "+
			    		"tce.cbp_entry_id, "+
			    		"tce.reallocated_flag, "+
			    		"tce.excluded_flag, "+
			    		"tce.assigned_dt, "+
			    		"tce.fiscal_qtr, "+
			    		"tce.fiscal_year, "+
			    		"tce.entry_summ_dt, "+
			    		"tce.entry_dt, "+
			    		"tce.missing_flag "+
					"FROM "+
						"tu_cbp_entry       tce, "+
						"tu_cbp_importer    tci, "+
						"tu_tobacco_class   ttc "+
					"WHERE "+
						"tci.cbp_importer_id = tce.cbp_importer_id "+
						"AND tce.tobacco_class_id = ttc.tobacco_class_id "+
						"AND tci.fiscal_yr = :fiscalyr "+
						"AND CASE "+
							"WHEN tci.association_type_cd = 'IMPT' OR (tci.association_type_cd IS NULL AND tci.consignee_exists_flag = 'N') THEN "+
								"tci.importer_ein "+
							"WHEN tci.association_type_cd = 'CONS' THEN "+
								"tci.consignee_ein "+
							" END =:ein  "+
						"AND ttc.tobacco_class_nm = :tobaccoclassnm "+
						"AND nvl(tce.reallocated_flag, 'N') <> 'Y' ";

		String outFiscalyrCheck = "   AND  ( entry_dt IS NULL OR entry_dt < :startdate OR entry_dt > :enddate ) ";
		
		if(isBulkReallocation){
			sql = sql +"     AND nvl(tce.excluded_flag, 'N') <> 'Y' ";
		}else {
			if("entryDt".equalsIgnoreCase(cbpDateConfigEntity.getDateType())){
				sql = sql +"     AND nvl(tce.excluded_flag, 'N') <> 'Y' ";
			}else if("entrySummDt".equalsIgnoreCase(cbpDateConfigEntity.getDateType())){
				sql = sql +"     AND nvl(tce.excluded_flag, 'N') = 'Y'  AND (tce.ENTRY_TYPE_CD NOT IN (21,22))";
			}
			sql = sql + outFiscalyrCheck;
		}
		
		Query query = this.getCurrentSession().createSQLQuery(sql);
		query.setLong("fiscalyr", cbpDateConfigEntity.getFiscalYr());
		query.setParameter("ein", cbpDateConfigEntity.getEin());
		query.setString("tobaccoclassnm", cbpDateConfigEntity.getTobaccoClassNm());
		if(!isBulkReallocation){
			query.setDate("startdate", simpleDateFormat.parse(startDate));
			query.setDate("enddate", simpleDateFormat.parse(endDate));
		}

		if("entryDt".equalsIgnoreCase(cbpDateConfigEntity.getDateType())){
			resultNo = 7;
			excludeFlag = "Y"; 
		}else if("entrySummDt".equalsIgnoreCase(cbpDateConfigEntity.getDateType())){
			excludeFlag = "N"; 
			resultNo = 6;
		}
		List<CBPLineEntryUpdateInfo> export = new ArrayList<CBPLineEntryUpdateInfo>();
		List<Object[]> results = query.list();
		for (Object[] result : results) {
			CBPLineEntryUpdateInfo row = new CBPLineEntryUpdateInfo();
			if (result[0] != null) {
				row.setCbEntryId(Long.parseLong(result[0].toString()));
			}
			if (result[1] != null) {
				row.setReallocateFlag(result[1].toString());
			}
			if (result[5] != null) {
				row.setAssignedYear(Long.parseLong(result[5].toString()));
			}
			if(isBulkReallocation){
				if (result[2] != null) {
					row.setExcludeFlag(result[2].toString());
				}
				String reallocationDate = "";
				//set the assigned month /quarter/year based on entrydate/entrySummdate
				if(result[resultNo] != null){
					reallocationDate = result[resultNo].toString();
					String[] reallocDateArray = reallocationDate.split("-");
					String mon = reallocDateArray[1].toString();
					row.setAssignedMonth(Constants.NNToMMM.get(mon));
					row.setAssignedQuarter(Long.valueOf(Constants.MMToQtr.get(mon)));
				}
			}else{
				row.setExcludeFlag(excludeFlag);
				if (result[3] != null) {
					row.setAssignedMonth(result[3].toString());
				}
				if (result[4] != null) {
					row.setAssignedQuarter(Long.parseLong(result[4].toString()));
				}
			}
			if (result[8] != null) {
				row.setMissingFlag(result[8].toString());
			}
			
			export.add(row);
		}
		return export;
	}

    @Override
    public long getCompnyIDForReallcoation(long cbpEntryId) {
    	long companyId = 0;
        Query query = this.getCurrentSession().createSQLQuery("select c.company_id from tu_company c " + 
                "left join tu_cbp_importer a on c.ein = decode(a.ASSOCIATION_TYPE_CD,'IMPT',a.IMPORTER_EIN,a.CONSIGNEE_EIN)  left join tu_cbp_entry b on b.CBP_importer_id = a.CBP_importer_id " + 
                "where b.CBP_ENTRY_ID = :cbpEntryId");
        query.setParameter("cbpEntryId", cbpEntryId);
        if(query.uniqueResult() != null){
        	companyId = ((BigDecimal)query.uniqueResult()).longValue();
        }
        return companyId;
    }

    @Override
    public String getCompnyEINForReallcoation(long cbpEntryId) {
    	String ein = "0";
        Query query = this.getCurrentSession().createSQLQuery("select c.ein from tu_company c " + 
                "left join tu_cbp_importer a on c.ein = decode(a.ASSOCIATION_TYPE_CD,'IMPT',a.IMPORTER_EIN,a.CONSIGNEE_EIN)  left join tu_cbp_entry b on b.CBP_importer_id = a.CBP_importer_id " + 
                "where b.CBP_ENTRY_ID = :cbpEntryId");
        query.setParameter("cbpEntryId", cbpEntryId);
        if(query.uniqueResult() != null){
        	ein = query.uniqueResult().toString();
        }
        return ein;
    }

	@Override
	public String getTobaccoClassName(long tobaccoClassId) {
		String tobaccoClass="";
		Query query = this.getCurrentSession().createSQLQuery("select tc.TOBACCO_CLASS_NM from tu_tobacco_class tc where tc.TOBACCO_CLASS_ID=:tobaccoClassId");
		query.setLong("tobaccoClassId", tobaccoClassId);
		 if(query.uniqueResult() != null){
			 tobaccoClass = ((String)query.uniqueResult().toString());
	        }
		return tobaccoClass;
	}
	
	public boolean isIngestedLinedPresentForFiscalYear(long fiscalYear,String typeOfFile) {
		if(typeOfFile.equalsIgnoreCase(Constants.TTB)) {
			String sqlQuery = "select ttbtax.TTB_ANNUAL_TAX_ID from tu_ttb_annual_tax ttbtax where ttbtax.TTB_FISCAL_YR=:fiscalYr";
			Query query = this.getCurrentSession().createSQLQuery(sqlQuery);
			query.setLong("fiscalYr", fiscalYear);
			List<Object[]> list= query.list();
			if(CollectionUtils.isNotEmpty(list) && list.size()>0) {
				return true;
			}
		} else {
			String sqlQuery = "select ttbtax.TTB_REMOVALS_FINAL_ID from tu_ttb_removals_final ttbtax where ttbtax.FISCAL_YR=:fiscalYr";
			Query query = this.getCurrentSession().createSQLQuery(sqlQuery);
			query.setLong("fiscalYr", fiscalYear);
			List<Object[]> list= query.list();
			if(CollectionUtils.isNotEmpty(list) && list.size()>0) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public List<TrueUpComparisonResults> getAllAllDeltasDetailsForEIN(long fiscalYear, String permitType,String ein) {
		StringBuilder alldeltas = new StringBuilder(
				"SELECT "
						+ "res.source, res.status,res.fiscal_qtr "
						+ "FROM ANN_TRUEUP_CMPR_ALLDELT_SNG_VW res WHERE res.fiscal_yr = :fiscalYr and res.ein=:ein and res.permit_type=:permitType");
		Query query =this.getCurrentSession().createSQLQuery(alldeltas.toString());
		query.setParameter("fiscalYr", fiscalYear);
		query.setParameter("ein", ein);
		query.setParameter("permitType", permitType);
		logger.debug("getAllComparisonSingleResults() :: executing SQL");
		List<Object[]> trueupComparisonSQLData = query.list();

		List<TrueUpComparisonResults> trueupComparisonResults = new ArrayList<>();

		logger.debug("getAllComparisonSingleResults() :: building result");
		// Manage results
		for (Object[] obj : trueupComparisonSQLData) {
			TrueUpComparisonResults resultData = new TrueUpComparisonResults();
			
			
			if (obj[0] != null) {
				resultData.setDataType(obj[0].toString());
			}
			
			if (obj[1] != null) {
				resultData.setAllDeltaStatus(obj[1].toString());
			}
			
			
			if (obj[2] != null) {
				resultData.setQuarter(obj[2].toString());
			}
			

			trueupComparisonResults.add(resultData);
		}

		logger.debug("getAllComparisonSingleResults() :: retruning comparison results");
		return trueupComparisonResults;
		
	}
	@Override
	public String getAssociatedCompanyEIN(long fiscalYear,long fiscalQtr, String tobaccoClassNm, String ein) {
		StringBuilder sql  = new StringBuilder("SELECT   tc.COMPANY_ID,tc.ein, tc.legal_nm  " + 
				"			FROM tu_company tc, " + 
				"			tu_cbp_importer ti, " + 
				"			(SELECT   " + 
				"			    		 distinct tce.cbp_importer_id   " + 
				"					 FROM   " + 
				"						 tu_cbp_entry       tce,   " + 
				"						 tu_cbp_importer    tci,   " + 
				"						 tu_tobacco_class   ttc   " + 
				"					 WHERE   " + 
				"						 tci.cbp_importer_id = tce.original_cbp_importer_id   " + 
				"						 AND tce.tobacco_class_id = ttc.tobacco_class_id   " + 
				"						 AND tci.fiscal_yr = :fiscalYr  " );
			if(fiscalQtr !=  5) {	
				sql.append("            AND tce.FISCAL_QTR = :fiscalQtr " );
			}
			sql.append("			    AND CASE   " + 
				"							 WHEN tci.association_type_cd = 'IMPT' OR (tci.association_type_cd IS NULL AND tci.consignee_exists_flag = 'N') THEN   " + 
				"								 tci.importer_ein   " + 
				"							 WHEN tci.association_type_cd = 'CONS' THEN   " + 
				"								 tci.consignee_ein   " + 
				"							  END =:ein    " + 
				"						 AND ttc.tobacco_class_nm = :tobaccoClassNm)t1 " + 
				"			WHERE t1.cbp_importer_id = ti.cbp_importer_id " + 
				"			AND ti.IMPORTER_EIN = tc.ein  ");
		
		logger.debug("getAssociatedCompanyByEIN() :: executing SQL");
		String companyId = "";

		Query query = this.getCurrentSession().createSQLQuery(sql.toString());
		query.setParameter("fiscalYr", fiscalYear);
		query.setParameter("ein", ein);
		if(fiscalQtr !=  5) {
			query.setParameter("fiscalQtr", fiscalQtr);
		}
		query.setParameter("tobaccoClassNm", tobaccoClassNm);

		List<Object[]> results = query.list();
		if( results != null && results.size() > 0 && results.get(0)[1] != null) {
			companyId = results.get(0)[1].toString();
		}

		logger.debug("getAssociatedCompanyByEIN() :: returning associated ein");
		return companyId;
		
	}

	@Override
	public List<String> getAllNonZeroDeltaTobaccoClass(long fiscalYear, String permitType, String ein) {
		StringBuilder alldeltas = new StringBuilder(
				"SELECT "
						+ "DISTINCT vw.tobacco_class_nm "
						+ "FROM ANN_TRUEUP_CALC_DELTA vw WHERE vw.fiscal_yr = :fiscalYr and (vw.ein=:ein or vw.original_ein=:ein) and vw.permit_type=:permitType");
		Query query =this.getCurrentSession().createSQLQuery(alldeltas.toString());
		query.setParameter("fiscalYr", fiscalYear);
		query.setParameter("ein", ein);
		query.setParameter("permitType", permitType);
		List<String[]> tobaccoClassList = query.list();
		if(CollectionUtils.isNotEmpty(tobaccoClassList)) {
			String[] tobaccoClassListArray = (String[]) tobaccoClassList.toArray(new String[tobaccoClassList.size()]);
			return Arrays.asList(tobaccoClassListArray);
		} 
		return null;
	}
	@Override
	public List<ComparisonAllDeltaStatusEntity> getAllDeltasFrmSingleView(TrueUpComparisonResults model){
		String commentQtr = "";
		String tobaccoClassName="";
		if("CIGARS".equalsIgnoreCase(model.getTobaccoClass())) {
			commentQtr = "1-4";
		}else {
			commentQtr = model.getUserComments().get(0).getCommentQtrs().replace(";", "','");
		}
		String permitType = "";
		if (model.getPermitType().equalsIgnoreCase("Importer") || model.getPermitType().equalsIgnoreCase("IMPT")) {
			permitType = "IMPT";
		} else {
			permitType = "MANU";
		}
		
		
		String allDeltasSQL = "SELECT * FROM  ANN_TRUEUP_CMPR_ALLDELT_SNG_VW ALLDELTA WHERE ALLDELTA.fiscal_yr=:fiscalYr  AND ALLDELTA.source in ('INGESTED','TUFA') "
							  +"AND (ALLDELTA.ein=:ein OR ALLDELTA.original_ein=:ein) "
				              +"AND ALLDELTA.TOBACCO_CLASS_NM =:ttnm AND ALLDELTA.PERMIT_TYPE=:permitType AND ALLDELTA.FISCAL_QTR IN ('" + commentQtr + "')";

	
		Query qry = this.getCurrentSession().createSQLQuery(allDeltasSQL);

		qry.setString("ein", model.getEin());
		qry.setString("fiscalYr", model.getFiscalYr());
		qry.setParameter("ttnm", model.getTobaccoClass());
		qry.setParameter("permitType", permitType);


		List<Object[]> allDeltas = qry.list();

		List<ComparisonAllDeltaStatusEntity> deltaStatuses = new ArrayList<>();
		for (Object[] delta : allDeltas) {

			ComparisonAllDeltaStatusEntity status = new ComparisonAllDeltaStatusEntity();
			if (delta[0] != null)
				status.setEin(delta[0].toString());
			if (delta[4] != null)
				status.setFiscalYr(delta[4].toString());
			if (delta[5] != null)
				status.setTobaccoClassName(delta[5].toString());
			if (delta[6] != null)
				status.setFiscalQtr(delta[6].toString());
			if (delta[7] != null)
				status.setDelta(((BigDecimal) delta[7]).doubleValue());
			if (delta[8] != null)
				status.setPermitType(delta[8].toString());
			if (delta[9] != null)
				status.setDeltaType(delta[9].toString());
			if (delta[10] != null) {
				status.setDeltaStatus(delta[10].toString());
			}
			deltaStatuses.add(status);
		}

		return deltaStatuses;

	}
	
	public List<Object[]>getMixedActionDetaislForQuarter(long companyId,long fiscalYr, long quarter,String tobaccoClass) {
		if ("ROLL YOUR OWN".equalsIgnoreCase(tobaccoClass) || "RYO".equalsIgnoreCase(tobaccoClass)) {
			tobaccoClass = "Roll-Your-Own";
		}
    	String sql = "select ent.Month,ent.Mixed_ind,ent.PREVIOUS_MONTH,ent.PREVIOUS_MIXED_IND from TU_AMNDMNT_MIXEDACT_DTL ent where ent.Amendment_id in "
    			+ "(select amd.CBP_AMENDMENT_ID from tu_cbp_amendment amd where amd.FISCAL_YR=:fiscalYr and amd.QTR=:quarter and amd.CBP_COMPANY_ID=:companyId and "
    			+ "amd.TOBACCO_CLASS_ID=(select tc.TOBACCO_CLASS_ID from tu_tobacco_class tc where tc.TOBACCO_CLASS_NM=:tobaccoClass) "
    			+ "and amd.ACCEPTANCE_FLAG='M')";
    	Query query = getCurrentSession().createSQLQuery(sql);
    	query.setParameter("fiscalYr", fiscalYr);
    	query.setParameter("quarter", quarter);
    	query.setParameter("companyId", companyId);
    	query.setParameter("tobaccoClass", tobaccoClass);
    	return query.list();
	}
	
	public void deleteMixedActionDetails(long amendmentId) {
		String sql = "delete from TU_AMNDMNT_MIXEDACT_DTL dtl where dtl.AMENDMENT_ID=:amendmentId";
		//getCurrentSession().delete(object);
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("amendmentId", amendmentId);
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();
	}
	
	public List<Object[]>getMixedActionDetaislForQuarterManu(long companyId,long fiscalYr, long quarter,String tobaccoClass) {
		if ("ROLL YOUR OWN".equalsIgnoreCase(tobaccoClass) || "RYO".equalsIgnoreCase(tobaccoClass)) {
			tobaccoClass = "Roll-Your-Own";
		}
		if("Chew-Snuff".equalsIgnoreCase(tobaccoClass)) {
			tobaccoClass = "Chew";
		}
		if("Pipe-Roll Your Own".equalsIgnoreCase(tobaccoClass))
			tobaccoClass = "Pipe";
		
		if("Cigars".equalsIgnoreCase(tobaccoClass))
			quarter = 5;
		
    	String sql = "select ent.Month,ent.Mixed_ind,ent.PREVIOUS_MONTH,ent.PREVIOUS_MIXED_IND from TU_TTB_AMNDMNT_MIXEDACT_DTL ent where ent.Amendment_id in "
    			+ "(select amd.TTB_AMENDMENT_ID from tu_ttb_amendment amd where amd.FISCAL_YR=:fiscalYr and amd.QTR=:quarter and amd.TTB_COMPANY_ID=:companyId and "
    			+ "amd.TOBACCO_CLASS_ID=(select tc.TOBACCO_CLASS_ID from tu_tobacco_class tc where tc.TOBACCO_CLASS_NM=:tobaccoClass) "
    			+ "and amd.ACCEPTANCE_FLAG='M')";
    	Query query = getCurrentSession().createSQLQuery(sql);
    	query.setParameter("fiscalYr", fiscalYr);
    	query.setParameter("quarter", quarter);
    	query.setParameter("companyId", companyId);
    	query.setParameter("tobaccoClass", tobaccoClass);
    	return query.list();
	}
	
	public void deleteMixedActionDetailsManu(long amendmentId) {
		String sql = "delete from TU_TTB_AMNDMNT_MIXEDACT_DTL dtl where dtl.AMENDMENT_ID=:amendmentId";
		//getCurrentSession().delete(object);
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("amendmentId", amendmentId);
		query.executeUpdate();
		getCurrentSession().flush();
		getCurrentSession().clear();
	}
	
	public List<TrueUpComparisonImporterDetail7501Period> getPeriod7501Detail(String ein, long fiscalYr, String tobaccoClass, long quarter) {
		
		List<TrueUpComparisonImporterDetail7501Period> detail7501List = new ArrayList<TrueUpComparisonImporterDetail7501Period>();
		TrueUpComparisonImporterDetail7501Period  lineDetail = null;
		
		StringBuilder sql = new StringBuilder( "SELECT " + 
				"    TO_CHAR(to_date(trp.month,'MON'), 'MONTH') AS MONTH,tp.permit_num, tfd.line_num,tfd.removal_qty,tfd.taxes_paid,tfd.calc_tax_rate,tfd.dol_difference,ttc.tobacco_class_nm " + 
				"FROM " + 
				"    tu_rpt_period   trp, " + 
				"    tu_permit       tp, " + 
				"    tu_submitted_FORM tsf, " + 
				"    tu_company      tc, " + 
				"    TU_FORM_DETAIL tfd, " + 
				"    TU_TOBACCO_CLASS ttc " + 
				"WHERE " + 
				"    tfd.form_id = tsf.form_id " + 
				"    AND tfd.tobacco_class_id = ttc.tobacco_class_id " + 
				"    AND trp.period_id = tsf.period_id " + 
				"    AND tp.permit_id = tsf.permit_id " + 
				"    AND tp.company_id = tc.company_id " + 
				"    AND tsf.form_type_cd = '7501' " + 
				"	 AND tfd.line_num <> 0" +
				"    AND tc.ein = :ein " + 
				"    AND trp.fiscal_yr = :fiscalYr " + 
				"    AND (ttc.tobacco_class_nm =:tobaccoClass OR (:tobaccoClass = 'Cigars' AND ttc.tobacco_class_nm IN ('Small', 'Large','Ad Valorem'))) ") ;
		if(quarter != 5) {
				sql.append("     AND trp.quarter = :quarter ") ;
		}
		sql.append("    ORDER BY trp.month,tp.permit_num,tfd.line_num ");
    	Query query = getCurrentSession().createSQLQuery(sql.toString());
    	query.setParameter("fiscalYr", fiscalYr);
    	query.setParameter("ein", ein);
    	query.setParameter("tobaccoClass", tobaccoClass);
    	if(quarter != 5) {
    		query.setParameter("quarter", quarter);
    	}
    	List<Object[]> resultObjList = query.list();

		for (Object[] res : resultObjList) {
			lineDetail = new TrueUpComparisonImporterDetail7501Period();
			if (res[0] != null) {
				lineDetail.setPeriod(res[0].toString().trim());
			}
			if (res[1] != null) {
				lineDetail.setPermitNum(res[1].toString());
			}
			if (res[2] != null) {
				lineDetail.setLineNum(Long.parseLong(res[2].toString()));
			}
			if (res[3] != null) {
				lineDetail.setQty(res[3].toString());
			}
			if (res[4] != null) {
				lineDetail.setTaxAmt(res[4].toString());
			}
			if (res[5] != null) {
				lineDetail.setCalcTaxRate(res[5].toString());
			}
			if (res[6] != null) {
				lineDetail.setTaxDiff(res[6].toString());
			}
			if (res[7] != null) {
				lineDetail.setType(res[7].toString());
			}
			
			detail7501List.add(lineDetail);
		}
    	return detail7501List;
	}
	
	
	
	@Override
	public List<ComparisonCommentAttachmentEntity> findCompCommentDocbyId(long cbpCommentSeq, long ttbCommentSeq, long allDeltaCommentSeq) {
		StringBuffer queryBuffer = new StringBuffer("Select DOCUMENT_ID, DOC_DESC, CREATED_DT, CREATED_BY FROM TU_COMPARISON_COMMENT_DOC WHERE ");
		
		if(cbpCommentSeq > 0) {
			queryBuffer.append(" CBP_COMMENT_SEQ = :cbpCommentSeq");
		}
		if(ttbCommentSeq > 0) {
			queryBuffer.append(" TTB_COMMENT_SEQ = :ttbCommentSeq");
		}
		if(allDeltaCommentSeq > 0) {
			if(cbpCommentSeq > 0 || ttbCommentSeq > 0) {
				queryBuffer.append(" AND ");
			}
			queryBuffer.append("ALLDELTA_COMMENT_SEQ = :allDeltaCommentSeq");
		}
		queryBuffer.append(" ORDER BY 3 DESC");
		
		Query query = getCurrentSession().createSQLQuery(queryBuffer.toString());
		if(cbpCommentSeq > 0) {
			query.setParameter("cbpCommentSeq", cbpCommentSeq);
		}
		if(ttbCommentSeq > 0) {
			query.setParameter("ttbCommentSeq", ttbCommentSeq);
		}
		if(allDeltaCommentSeq > 0) {
			query.setParameter("allDeltaCommentSeq", allDeltaCommentSeq);
		}
		List<Object[]> docData = query.list();
		List<ComparisonCommentAttachmentEntity> documents = new ArrayList<>();
		for (Object[] obj : docData) {
			if (obj != null) {
				ComparisonCommentAttachmentEntity document = new ComparisonCommentAttachmentEntity();
				document.setDocumentId(((BigDecimal) obj[0]).longValue());
				document.setDocDesc(obj[1] != null ? obj[1].toString() : null);
				document.setCreatedDt((Date) obj[2]);
				document.setCreatedBy(obj[3] != null ? obj[3].toString() : null);
				documents.add(document);
			}
		}
		return documents;
	}
	
	@Override
	public long getDocCount(long cbpCommentSeq, long ttbCommentSeq, long allDeltaCommentSeq) {
		StringBuffer queryBuffer = new StringBuffer("Select COUNT(*) FROM TU_COMPARISON_COMMENT_DOC WHERE ");
		
		if(cbpCommentSeq > 0) {
			queryBuffer.append(" CBP_COMMENT_SEQ = :cbpCommentSeq");
		}
		if(ttbCommentSeq > 0) {
			queryBuffer.append(" TTB_COMMENT_SEQ = :ttbCommentSeq");
		}
		if(allDeltaCommentSeq > 0) {
			queryBuffer.append("ALLDELTA_COMMENT_SEQ = :allDeltaCommentSeq");
		}
		queryBuffer.append(" ORDER BY 1 DESC");
		
		Query query = getCurrentSession().createSQLQuery(queryBuffer.toString());
		if(cbpCommentSeq > 0) {
			query.setParameter("cbpCommentSeq", cbpCommentSeq);
		}
		if(ttbCommentSeq > 0) {
			query.setParameter("ttbCommentSeq", ttbCommentSeq);
		}
		if(allDeltaCommentSeq > 0) {
			query.setParameter("allDeltaCommentSeq", allDeltaCommentSeq);
		}
		
		List countlist = query.list();
		return countlist != null && countlist.size() > 0? ((BigDecimal) countlist.get(0)).intValue():0;
	}
	
	@Override
	public List<Long> getFiscalYearsWithNewDeltas() {
		List<Long> fiscalYears = new ArrayList<>();
		
		StringBuilder sql = new StringBuilder("SELECT DISTINCT "
				+ "     anntrue.fiscal_yr "
				+ " FROM "
				+ "     tu_annual_trueup anntrue "
				+ "     LEFT JOIN tu_assessment assess ON assess.trueup_id = anntrue.trueup_id "
				+ "     JOIN tu_ingestion_workflow_context cont ON assess.assessment_yr = cont.fiscal_yr "
				+ " WHERE "
				+ "     assessment_type = 'ANNU' "
				+ "     AND anntrue.submitted_dt IS NOT NULL "
				+ "     AND EXISTS ( "
				+ "         SELECT  /*+ NO_QUERY_TRANSFORMATION(cmpimpsing) */ "
				+ "             'X' "
				+ "         FROM "
				+ "             ann_trueup_compareres_imp_sng cmpimpsing "
				+ "         WHERE "
				+ "             fiscal_yr = assess.assessment_yr "
				+ "             AND cont.context = 'detailOnly' "
				+ "             AND DECODE(DECODE(in_progress_flag,'InProgress',in_progress_flag,acceptance_flag),'InProgress','action_needed','DeltaChange' "
				+ "            ,DECODE(sign(abs(delta) - 1),-1,'no_action_needed','action_needed'),'noAction', 'action_needed', nvl(acceptance_flag,'action_needed') ) = 'action_needed' "
				+ "         UNION ALL "
				+ "         SELECT  /*+ NO_QUERY_TRANSFORMATION(cmpimp) */ "
				+ "             'X' "
				+ "         FROM "
				+ "             ann_trueup_compareresults_imp cmpimp "
				+ "         WHERE "
				+ "             fiscal_yr = assess.assessment_yr "
				+ "             AND cont.context = 'summdetail' "
				+ "             AND DECODE(DECODE(in_progress_flag,'InProgress',in_progress_flag,acceptance_flag),'InProgress','action_needed','DeltaChange' "
				+ "            ,DECODE(sign(abs(delta) - 1),-1,'no_action_needed','action_needed'),'noAction', 'action_needed', nvl(acceptance_flag,'action_needed') ) = 'action_needed' "
				+ "         UNION ALL "
				+ "         SELECT  /*+ NO_QUERY_TRANSFORMATION(cmpmanu) */ "
				+ "             'X' "
				+ "         FROM "
				+ "             ann_trueup_compareresults_manu cmpmanu "
				+ "         WHERE "
				+ "             fiscal_yr = assess.assessment_yr "
				+ "             AND DECODE(DECODE(in_progress_flag,'InProgress',in_progress_flag,acceptance_flag),'InProgress','action_needed','DeltaChange' "
				+ "            ,DECODE(sign(abs(delta) - 1),-1,'no_action_needed','action_needed'),'noAction', 'action_needed', nvl(acceptance_flag,'action_needed') ) = 'action_needed' "
				+ "     )");
    	Query query = getCurrentSession().createSQLQuery(sql.toString()).addScalar("fiscal_yr", StandardBasicTypes.LONG);
    	fiscalYears = query.list();
    	
    	sql = new StringBuilder("SELECT DISTINCT "
    			+ "     anntrue.fiscal_yr "
    			+ " FROM "
    			+ "     tu_annual_trueup anntrue "
    			+ "     LEFT JOIN tu_assessment assess ON assess.trueup_id = anntrue.trueup_id "
    			+ "     JOIN tu_ingestion_workflow_context cont ON assess.assessment_yr = cont.fiscal_yr "
    			+ " WHERE "
    			+ "     assessment_type = 'ANNU' "
    			+ "     AND anntrue.submitted_dt IS NOT NULL "
    			+ "     AND EXISTS ( "
    			+ "         SELECT "
    			+ "             'X' "
    			+ "         FROM "
    			+ "             ann_trueup_cmpr_all_deltas_vw alldelta "
    			+ "         WHERE "
    			+ "             alldelta.fiscal_yr = assess.assessment_yr "
    			+ "             AND status IN ('Review', 'In Progress') "
    			+ "     )");
    	query = getCurrentSession().createSQLQuery(sql.toString()).addScalar("fiscal_yr", StandardBasicTypes.LONG);
    	fiscalYears.addAll(query.list());

    	return fiscalYears;
	}

	@Override
	public void processAdjustments(String fiscalYear) {
		ProcedureCall procCall = getCurrentSession().createStoredProcedureCall("ANN_TRUEUP_ADJ_CHECK_PROCESS");
        procCall.registerParameter("P_FISCAL_YR", Long.class, ParameterMode.IN).bindValue(Long.parseLong(fiscalYear));
        procCall.registerParameter("logged_user", String.class, ParameterMode.IN).bindValue(securityContextUtil.getSessionUser());
        ProcedureOutputs outputs = procCall.getOutputs();
	}

	@Override
	public List<PreviousTrueupInfo> getPreviousTrueupInfo(String type, long currentYear, String ein) {
		String sql;
		List<PreviousTrueupInfo> responseList = new ArrayList<PreviousTrueupInfo>();
		if(type.equalsIgnoreCase("IMPT")) {
			sql = "select vw.fiscal_yr,vw.quarter,vw.tobacco_class_nm,vw.status,vw.amendment_id,vw.delta_type,vw.ein from prev_true_up_info_imp vw where vw.ein=:ein and vw.fiscal_yr < :currentYear  ";
		} else {
			sql = "select vw.fiscal_yr,vw.quarter,vw.tobacco_class_nm,vw.status,vw.amendment_id,vw.delta_type,vw.ein from prev_true_up_info_manu vw where  vw.ein=:ein and vw.fiscal_yr < :currentYear  ";
		}
		Query query = getCurrentSession().createSQLQuery(sql);
		//query.setParameter("companyId", companyId);
		query.setParameter("ein", ein);
		query.setParameter("currentYear", currentYear);
		List<Object[]> list = query.list();
		for (Object[] obj : list) {
			PreviousTrueupInfo info = new PreviousTrueupInfo();
			info.setFiscalYear(obj[0].toString());
			info.setQuarter(obj[1].toString().equalsIgnoreCase("1-4")?"5":obj[1].toString());
			info.setTobaccoClassName(obj[2].toString());
			if(obj[3] != null) {
				info.setStatus(obj[3].toString());
			}
			if(obj[4] != null) {
				info.setAmendmentId(((BigDecimal)obj[4]).longValue());
			}
			if(obj[5] != null) {
				info.setDeltaType(obj[5].toString());
			}
			if(obj[6] != null) {
				info.setEin(obj[6].toString());
			}
			responseList.add(info);
		}
		responseList = responseList.stream().filter(a-> StringUtils.isNoneEmpty(a.getStatus())).collect(Collectors.toList());
		if(type.equalsIgnoreCase("MANU")) {
			HashSet<Object> seen=new HashSet<>();
			responseList.removeIf(e->!seen.add(Arrays.asList(e.getFiscalYear(),e.getQuarter(),e.getTobaccoClassName(),e.getEin(),e.getDeltaType())));
		}
		for(PreviousTrueupInfo info : responseList) {
			//get the comments for each amendment id:
			String seqSQL;
			List<Long> commentSeqList  = new ArrayList<Long>(); 
			List<String> commentList  = new ArrayList<String>(); 
			if(type.equalsIgnoreCase("IMPT")) {
				long id = info.getAmendmentId();
				if(info.getDeltaType().equalsIgnoreCase("Matched")) {
					seqSQL = "select seq.COMMENT_SEQ from tu_cbp_detail_qtr_comments seq where seq.CBP_AMENDMENT_ID=:id";
				} else {
					seqSQL = "select seq.COMMENT_SEQ from tu_all_delta_join_comment seq where seq.CMP_ALL_DELTA_ID=:id";
				}
				Query sqlQuery = getCurrentSession().createSQLQuery(seqSQL);
				sqlQuery.setParameter("id", id);
				commentSeqList = sqlQuery.list();
				if(CollectionUtils.isNotEmpty(commentSeqList)) {
					if(info.getDeltaType().equalsIgnoreCase("Matched")) {
						seqSQL = "select cmt.user_comment from tu_cbp_qtr_comment cmt where cmt.comment_seq in(:commentSeqList)";
								
					} else {
						seqSQL = "select cmt.user_comment from tu_all_delta_comment cmt where cmt.comment_seq in(:commentSeqList)";
					}
					Query commentQuery = getCurrentSession().createSQLQuery(seqSQL);
					commentQuery.setParameter("commentSeqList", commentSeqList);
					commentList = commentQuery.list();
				}
				
			} else {
				//manu
				long id = info.getAmendmentId();
				if(info.getDeltaType().equalsIgnoreCase("Matched")) {
					seqSQL = "select seq.COMMENT_SEQ from tu_ttb_detail_qtr_comments seq where seq.ttb_AMENDMENT_ID=:id";
				} else {
					seqSQL = "select seq.COMMENT_SEQ from tu_all_delta_join_comment seq where seq.CMP_ALL_DELTA_ID=:id";
				}
				Query sqlQuery = getCurrentSession().createSQLQuery(seqSQL);
				sqlQuery.setParameter("id", id);
				commentSeqList = sqlQuery.list();
				if(CollectionUtils.isNotEmpty(commentSeqList)) {
					if(info.getDeltaType().equalsIgnoreCase("Matched")) {
						seqSQL = "select cmt.user_comment from tu_ttb_qtr_comment cmt where cmt.comment_seq in(:commentSeqList)";
								
					} else {
						seqSQL = "select cmt.user_comment from tu_all_delta_comment cmt where cmt.comment_seq in(:commentSeqList)";
					}
					Query commentQuery = getCurrentSession().createSQLQuery(seqSQL);
					commentQuery.setParameter("commentSeqList", commentSeqList);
					commentList = commentQuery.list();
				}
			}
			info.setComments(commentList);
			
		}
		return responseList;
	}

}

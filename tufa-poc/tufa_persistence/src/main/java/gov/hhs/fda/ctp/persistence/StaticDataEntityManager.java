package gov.hhs.fda.ctp.persistence;

import java.util.List;

import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.TobaccoClassEntity;

public interface StaticDataEntityManager {
	
	public List<TobaccoClassEntity> getTobaccoTypes();

	public List<TobaccoClassEntity> getTobaccoTypes(List<String> ttnames);
	
	public CompanyEntity getCompanyNmByEin(String ein);
}

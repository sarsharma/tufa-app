package gov.hhs.fda.ctp.persistence;

import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.TobaccoClassEntity;

@Repository("staticDataEntityManager")
public class StaticDataEntityManagerImpl extends BaseEntityManagerImpl implements StaticDataEntityManager {
	
	@SuppressWarnings("unchecked")
	public List<TobaccoClassEntity> getTobaccoTypes() {
		return this.getCurrentSession().createCriteria(TobaccoClassEntity.class).list();
	}

	public List<TobaccoClassEntity> getTobaccoTypes(List<String> ttnames) {

		String hql = "select tt from TobaccoClassEntity tt where tt.tobaccoClassNm in (:ttnames) ";
		Query q = this.getCurrentSession().createQuery(hql);
		q.setParameterList("ttnames", ttnames);
		List<TobaccoClassEntity> tt = q.list();
		return tt;
	}
	
	public CompanyEntity getCompanyNmByEin(String ein){		
		String hql = "select company from CompanyEntity company where company.EIN = :ein ";
		Query q = this.getCurrentSession().createQuery(hql);
		q.setParameter("ein", ein);
                if(q.list().isEmpty()){
                    return null;
                }
		return (CompanyEntity) q.list().get(0);
	}
}

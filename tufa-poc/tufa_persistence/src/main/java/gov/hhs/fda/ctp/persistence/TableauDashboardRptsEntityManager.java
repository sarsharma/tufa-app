package gov.hhs.fda.ctp.persistence;

import java.util.List;

import gov.hhs.fda.ctp.persistence.model.TableauDashboardRptTypeEntity;

public interface TableauDashboardRptsEntityManager {
	
	public List<TableauDashboardRptTypeEntity> getTableauDashboardReportTypes();

}

package gov.hhs.fda.ctp.persistence;

import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.persistence.model.TableauDashboardRptTypeEntity;

@Repository("tableauDashboardRptsEntityManager")
public class TableauDashboardRptsEntityManagerImpl  extends BaseEntityManagerImpl implements TableauDashboardRptsEntityManager{
	
	@Override
	public List<TableauDashboardRptTypeEntity> getTableauDashboardReportTypes(){
		
		String hql= "from TableauDashboardRptTypeEntity tdbEntity where tdbEntity.enable='Y' order by "
				+   "case when tdbEntity.displayValue = 'Assessment Address Validation Report' then '001' else tdbEntity.displayValue end";
		Query query = getCurrentSession().createQuery(hql);
		List<TableauDashboardRptTypeEntity>  tbdrptTypes = query.list();
		return tbdrptTypes;
		
	}

}

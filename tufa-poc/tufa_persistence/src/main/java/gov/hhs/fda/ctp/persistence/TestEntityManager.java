/**
 * 
 */
package gov.hhs.fda.ctp.persistence;

import java.util.List;

import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;

/**
 * The Interface TestEntityManager.
 *
 * @author tgunter
 */
public interface TestEntityManager {
    
    /**
     * Setup.
     *
     * @return the company entity
     */
    public CompanyEntity setup();
    
    /**
     * Tear down.
     *
     * @param companyId the companyId
     */
    public void tearDown(long companyId);
    
    /**
     * Tear down.
     *
     * @param companyName the companyName
     */
    public void tearDownByCompanyName(String companyName);    
    
    /**
     * Gets the permit periods.
     *
     * @param permitId the permit id
     * @return the permit periods
     */
    public List<PermitPeriodEntity> getPermitPeriods(Long permitId);
}

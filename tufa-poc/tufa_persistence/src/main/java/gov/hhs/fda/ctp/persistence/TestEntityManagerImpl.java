/**
 * 
 */
package gov.hhs.fda.ctp.persistence;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.persistence.model.AddressEntity;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.ContactEntity;
import gov.hhs.fda.ctp.persistence.model.FormDetailEntity;
import gov.hhs.fda.ctp.persistence.model.PeriodStatusEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;
import gov.hhs.fda.ctp.persistence.model.ReportAddressEntity;
import gov.hhs.fda.ctp.persistence.model.ReportContactEntity;
import gov.hhs.fda.ctp.persistence.model.RptPeriodEntity;
import gov.hhs.fda.ctp.persistence.model.SubmittedFormEntity;

/**
 * The Class TestEntityManagerImpl.
 *
 * @author tgunter
 */
@Repository("testEntityManager")
public class TestEntityManagerImpl extends BaseEntityManagerImpl implements TestEntityManager {
    
    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.persistence.TestEntityManager#setup()
     */
    @Override
    public CompanyEntity setup() {
        CompanyEntity company = createMockCompany();
        // create company
        getCurrentSession().save(company);

        // create permit, add to company
        PermitEntity permit = createMockPermit(company);
        Set<PermitEntity> permits = new HashSet<>();
        permits.add(permit);
        company.setPermits(permits);

        // create addresses
        Set<AddressEntity> addresses = createMockAddresses(company);
        company.setAddresses(addresses);
        
        getCurrentSession().save(company);
        getCurrentSession().saveOrUpdate(permit);
        for(AddressEntity address:addresses) {
                getCurrentSession().saveOrUpdate(address);
        }
        
        
        // create permit period
        PermitPeriodEntity permitPeriod = createMockPermitPeriod(permit);
        getCurrentSession().saveOrUpdate(permitPeriod);

        // create status
        PeriodStatusEntity status = createMockStatus(permitPeriod);
        getCurrentSession().saveOrUpdate(status);
        return company;
    }

    @Override
    public void tearDownByCompanyName(String companyName) {
    	Query query = getCurrentSession().
    	        createQuery("from CompanyEntity c where c.legalName =:LEGAL_NM");
    	query.setParameter("LEGAL_NM", companyName);
    	CompanyEntity retcompany = (CompanyEntity) query.uniqueResult();
    	if(retcompany != null){
    		if(retcompany.getCompanyId() > 0L){
    	    	tearDown(retcompany.getCompanyId());   			
    		}
    	}
    }
    
    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.persistence.TestEntityManager#tearDown(gov.hhs.fda.ctp.persistence.model.CompanyEntity)
     */
    @Override
    public void tearDown(long companyId) {
    	CompanyEntity retcompany = getCurrentSession().load(CompanyEntity.class, companyId);
        Set<PermitEntity> permits = retcompany.getPermits();


        // remove report contacts
        List<ReportContactEntity> reportContacts = getReportContacts(companyId);
        if(reportContacts != null) {
        	for(ReportContactEntity reportContact : reportContacts) {
        		getCurrentSession().delete(reportContact);
        	}
        }
        
        // delete all contacts
        Set<ContactEntity> contacts = retcompany.getContacts();
        if(contacts != null) {
        	for(ContactEntity contact : contacts) {
        		getCurrentSession().delete(contact);
        	}
        }


        // delete all addresses
        Set<AddressEntity> addresses = retcompany.getAddresses();
        if(addresses != null) {
	        for(AddressEntity address : addresses) {
	            getCurrentSession().delete(address);
	        }
        }

        // and report addresses
        List<ReportAddressEntity> reportAddresses = getReportAddresses(companyId);
        if(reportAddresses != null) {
        	for(ReportAddressEntity reportAddress : reportAddresses) {
        		getCurrentSession().delete(reportAddress);
        	}
        }

        // delete all permit periods then permits
        for(PermitEntity permit: permits) {
            tearDownPermitPeriods(permit);
            getCurrentSession().delete(permit);
        }
        
        // delete any market share records
        deleteMarketShareReoords(companyId);
        
        getCurrentSession().delete(retcompany);
    }

    /**
     * Utility method to delete all market share records by company id.
     * @param companyId
     */
	private void deleteMarketShareReoords(long companyId) {
		String sql = "DELETE FROM TU_MARKET_SHARE WHERE COMPANY_ID = :companyId";
		Query query = getCurrentSession().createSQLQuery(sql);
		query.setParameter("companyId", companyId);
		query.executeUpdate();
	}
    
    /**
     * Get a list of report addresses.
     * @param companyId
     * @return
     */
    private List<ReportAddressEntity> getReportAddresses(long companyId) {
		Query query = getCurrentSession().createQuery(
				"FROM ReportAddressEntity where COMPANY_ID = :companyId");
		query.setParameter("companyId", companyId);
		return query.list();
    }

    private List<ReportContactEntity> getReportContacts(long companyId) {
		Query query = getCurrentSession().createQuery(
				"FROM ReportContactEntity where COMPANY_ID = :companyId");
		query.setParameter("companyId", companyId);
		return query.list();
    }
    
    /**
     * Creates the mock company.
     *
     * @return the company entity
     */
    private CompanyEntity createMockCompany() {
        CompanyEntity mockCompany = new CompanyEntity();
        mockCompany.setEIN("013013013");
        mockCompany.setLegalName("UNIT TEST COMPANY");
        mockCompany.setCompanyStatus("ACTV");
        return mockCompany;
    }

    /**
     * Creates the mock permit.
     *
     * @param company the company
     * @return the permit entity
     */
    private PermitEntity createMockPermit(CompanyEntity company) {
        PermitEntity permit = new PermitEntity();
        permit.setIssueDt(new Date());
        permit.setPermitNum("UNITTEST013");
        permit.setPermitStatusTypeCd("ACTV");
        permit.setPermitTypeCd("MANU");
        permit.setCompanyEntity(company);
        return permit;
    }
    
    /**
     * Creates the mock addresses.
     *
     * @param company the company
     * @return the sets the
     */
    private Set<AddressEntity> createMockAddresses(CompanyEntity company) {
        AddressEntity primaryAddress = new AddressEntity();
        AddressEntity alternateAddress = new AddressEntity();
        Set<AddressEntity> addresses = new HashSet<>();
        primaryAddress.setAddressTypeCd("PRIM");
        primaryAddress.setCity("PRIMARY CITY");
        primaryAddress.setCompany(company);
        primaryAddress.setCountryCd("US");
        primaryAddress.setFaxNum(5555551212L);
        primaryAddress.setPostalCd("90210");
        primaryAddress.setState("CA");
        primaryAddress.setStreetAddress("112 Electric Avenue");
        primaryAddress.setSuite("13A");
        
        alternateAddress.setAddressTypeCd("ALTN");
        alternateAddress.setCity("ALTERNATE CITY");
        alternateAddress.setCompany(company);
        alternateAddress.setCountryCd("US");
        alternateAddress.setFaxNum(5555551212L);
        alternateAddress.setPostalCd("90210");
        alternateAddress.setState("CA");
        alternateAddress.setStreetAddress("1967 Long Road Home");
        alternateAddress.setSuite("13B");
        
        addresses.add(primaryAddress);
        addresses.add(alternateAddress);        
        return addresses;        
    }
    
    /**
     * Creates the mock permit period.
     *
     * @param permit the permit
     * @return the permit period entity
     */
    private PermitPeriodEntity createMockPermitPeriod(PermitEntity permit) {
        PermitPeriodEntity permitPeriod = new PermitPeriodEntity();
        List<RptPeriodEntity> reportPeriods = getRptPeriods();
        RptPeriodEntity reportPeriod = reportPeriods.get(0);
        permitPeriod.setPeriodId(reportPeriod.getPeriodId());
        permitPeriod.setPermitId(permit.getPermitId());
        permitPeriod.setSourceCd("EMAI");
        permitPeriod.setZeroFlag("N");
        return permitPeriod;
    }
    
    /**
     * Gets the rpt periods.
     *
     * @return the rpt periods
     */
    private List<RptPeriodEntity> getRptPeriods() {
        String hql = "FROM RptPeriodEntity";
        Query query = this.getCurrentSession().createQuery(hql);
        return  (List<RptPeriodEntity>) query.list();       
    }

    /**
     * Creates the mock status.
     *
     * @param permitPeriod the permit period
     * @return the period status entity
     */
    private PeriodStatusEntity createMockStatus(PermitPeriodEntity permitPeriod) {
        PeriodStatusEntity status = new PeriodStatusEntity();
        status.setPeriodId(permitPeriod.getPeriodId());
        status.setPermitId(permitPeriod.getPermitId());
        status.setPeriodStatusTypeCd("NSTD");
        status.setReconStatusTypeCd("UNAP");
        status.setPermitPeriodEntity(permitPeriod);
        status.setStatusDt(new Date());
        return status;
    }

    /* (non-Javadoc)
     * @see gov.hhs.fda.ctp.persistence.TestEntityManager#getPermitPeriods(java.lang.Long)
     */
    @Override
    public List<PermitPeriodEntity> getPermitPeriods(Long permitId) {
        String hql = "FROM PermitPeriodEntity pp WHERE pp.permitId = :permitId";
        Query query = this.getCurrentSession().createQuery(hql);
        query.setParameter("permitId", permitId);
        return (List<PermitPeriodEntity>) query.list();
    }
    
    /**
     * Tear down permit periods.
     *
     * @param permit the permit
     */
    private void tearDownPermitPeriods(PermitEntity permit) {
        List<PermitPeriodEntity> permitPeriods = getPermitPeriods(permit.getPermitId());
        if(permitPeriods == null) return;
        for(PermitPeriodEntity permitPeriod: permitPeriods) {
        	List<PeriodStatusEntity> statusList = permitPeriod.getPeriodStatusList();
        	if(statusList != null) {
	        	for(PeriodStatusEntity status : statusList) {
		            getCurrentSession().delete(status);
	        	}
        	}
            List<SubmittedFormEntity> forms = permitPeriod.getSubmittedForms();
            if(forms != null) {
	            for(SubmittedFormEntity form : forms) {
	                Set<FormDetailEntity> details = form.getFormDetails();
	                for(FormDetailEntity detail : details) {
	                    getCurrentSession().delete(detail);
	                }
	            }
            }
            List<AttachmentsEntity> attachments = permitPeriod.getAttachments();
            if(attachments != null) {
	            for(AttachmentsEntity attachment : attachments) {
	                getCurrentSession().delete(attachment);
	            }
            }
            getCurrentSession().delete(permitPeriod);
        }
    }

}

/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import java.sql.Blob;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.hibernate.query.Query;

import gov.hhs.fda.ctp.common.beans.CBPAmendment;
import gov.hhs.fda.ctp.common.beans.CBPEntry;
import gov.hhs.fda.ctp.common.beans.CBPImporter;
import gov.hhs.fda.ctp.common.beans.CBPLineEntryUpdateInfo;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.CompanyReallocationBeanToUpdate;
import gov.hhs.fda.ctp.common.beans.TTBAmendment;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpFilter;
import gov.hhs.fda.ctp.persistence.model.CBPAmendmentEntity;
import gov.hhs.fda.ctp.persistence.model.CBPDateConfigEntity;
import gov.hhs.fda.ctp.persistence.model.CBPEntryEntity;
import gov.hhs.fda.ctp.persistence.model.CBPFileColumnTemplateEntity;
import gov.hhs.fda.ctp.persistence.model.CBPImporterEntity;
import gov.hhs.fda.ctp.persistence.model.CBPLineItemEntity;
import gov.hhs.fda.ctp.persistence.model.CBPSummaryFileEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonAllDeltaStatusEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonCommentAttachmentEntity;
import gov.hhs.fda.ctp.persistence.model.IngestionWorkflowContextEntity;
import gov.hhs.fda.ctp.persistence.model.PermitCommentAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.RawIngested;
import gov.hhs.fda.ctp.persistence.model.TTBAmendmentEntity;
import gov.hhs.fda.ctp.persistence.model.TTBCompanyEntity;
import gov.hhs.fda.ctp.persistence.model.TTBPermitEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpSubDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.TufaEntity;

/**
 * @author tgunter
 *
 */
public interface TrueUpEntityManager {
	/**
	 * Create a new true up.
	 * @param trueUpEntity
	 * @return (String) result.
	 */
	public String createTrueUp(TrueUpEntity trueUpEntity);
	
	public void updateTrueUp(TrueUpEntity trueup);

	/**
	 * Get the list of true up data.
	 * @param filter
	 * @return List<TrueUpEntity>
	 */
	public List<TrueUpEntity> getTrueUps(TrueUpFilter filter);
	
	/**
	 * Get the list of true up data.
	 * @param filter
	 * @return List<TrueUpEntity>
	 */
	public List<TrueUpEntity> getAllTrueUps();


	/**
	 * Get the true-up by fiscal year.
	 * @param fiscalYear
	 * @return
	 */
	public TrueUpEntity getTrueUpByYear(long fiscalYear);

	/**
	 * Batch insert TTBUploadEntity data from ingestion.
	 * @param uploadData
	 */
	public void stageIngestionData(List<TufaEntity> uploadData, String type);
	
	/**
	 * Batch insert CBPSummaryEntity data from ingestion.
	 * @param uploadData
	 */
	public void stageCBPSummaryFile(List<CBPSummaryFileEntity> uploadData, long fiscalYear);

//	/**
//	 * Load the CBP data after staging.
//	 * @param fiscalYr
//	 */
//	public void loadCBP(Long fiscalYr);

	/**
	 * Load the TTB data after staging.
	 * @param fiscalYr
	 */
	public void loadTTB(Long fiscalYr);
	
	public void InsertTTBVolume();

	/**
	 * Save the true up document upload details.
	 * @param newDocument
	 */
	public void uploadDocument(long fiscalYear, TrueUpDocumentEntity newDocument);

	/**
	 * Get list of ingestion errors.
	 * @param type
	 * @return
	 */
	public List<TufaEntity> getIngestionErrors(String type, long fiscalYear) ;
	/**
	 * get all the true up documents for the fiscal year
	 * @param fiscalYear
	 * @return
	 */
	public List<TrueUpDocumentEntity> getTrueupDocuments(long fiscalYear);

	/**
	 * Save company comparison decision.
	 * @param bucket
	 */
	public List<CBPImporterEntity> updateCompanyComparisonBucket(List<CBPImporter> bucket);

    public List<TrueUpSubDocumentEntity> getSupportDocs(long fiscalYr);

    public void saveSupportDocument(TrueUpSubDocumentEntity supDoc);

    public TrueUpSubDocumentEntity getDocByDocId(long trueupSubdocId);

	/**
	 * Get the associate HTS code bucket.
	 * @param fiscalYear
	 * @return
	 */
	public List<CBPEntryEntity> getAssociateHTSBucket(long fiscalYear);

	//need to be removed.

	//public Map<String, HTSCodeEntity> getTobaccoClassesTypeMap();

	public List<CBPEntryEntity> updateHTSMissingAssociationData(List<CBPEntry> cbpEntries,long fiscalYr);

	public List<CBPEntryEntity> updateAssociateHTSBucket(List<CBPEntryEntity> cbpEntries,long fiscalYear);

	public List<CBPEntryEntity> getAssociateHTSMissingBucket(long fiscalYear);

	public void updateCompaniesForIncludeBucket(List<CompanyIngestion> companies, long fiscalYear);

	public TTBPermitEntity getTTBPermit(long permitId);

	public void saveTTBPermit(TTBPermitEntity permit);
	
	public void deleteIngestionFile(Long fiscalYr, String doctype);
	
	public List<TTBAmendmentEntity> getTTBAmendments(long fiscalYr,long companyId, List<TTBAmendment> tobaccoTypes);
	
	public  List<TTBAmendmentEntity> saveTTBAmendment(List<TTBAmendmentEntity> amendments);
	
	public List<TTBAmendmentEntity> saveTTBCompareComment(List<TTBAmendmentEntity> amendments);
	public List<TTBAmendmentEntity> saveTTBCompareComment(List<TTBAmendmentEntity> amendments, String ein, String tobaccoClass);


	List<TTBAmendmentEntity> updateTTBCompareFDAAcceptFlag(List<TTBAmendmentEntity> amendments);
	
	public List<CBPAmendmentEntity> getCBPAmendments(long fiscalYr,long companyId, List<CBPAmendment> tobaccoTypes);
	
	public  List<CBPAmendmentEntity> saveCBPAmendment(List<CBPAmendmentEntity> amendments);
	
	public List<CBPAmendmentEntity> saveCBPCompareComment(List<CBPAmendmentEntity> amendments);

	List<CBPAmendmentEntity> updateCBPCompareFDAAcceptFlag(List<CBPAmendmentEntity> amendments);
	
	public void generateAnnualMarketShare(int fiscalYear);
	
	public CompanyReallocationBeanToUpdate updateCBPIngestedLineEntryInfo(List<CBPLineEntryUpdateInfo> updateinfo);
	
	public TTBCompanyEntity getCompanyToIncldInMktSh(long fiscalYear, String ein);
	
	public CBPImporterEntity getCBPCompanyToIncldInMktSh(long fiscalYear, String ein);
	
	public void updateDeltaAssociation(List<TrueUpComparisonResults> deltaAssociation);

	public List<CBPEntryEntity> getAcctiveCBPEntryRecords(List<TrueUpComparisonResults> model);

	public void resetAllDeltaCompanyAssociation(List<TrueUpComparisonResults> model);

	public ComparisonAllDeltaStatusEntity updateCompareAllDeltaStatus(List<TrueUpComparisonResults> model);
	
	public ComparisonAllDeltaStatusEntity updateCompareAllDeltaStatusSingle(List<TrueUpComparisonResults> model);

	public Boolean checkIfAssociationExists(String ein, long fiscalYear, String tobaccoClass, long qtr);
	
	public List<RawIngested> getRawIngested(long fiscalYr, String ein, String qtr, String tobaccoClassNm, String type, boolean showOriginalAssociation, boolean getTax);

	public void deleteTTBCompareComment(List<TTBAmendmentEntity> amendEntities);

	public void deleteCBPCompareComment(List<CBPAmendmentEntity> amendEntities);

	public List<CBPFileColumnTemplateEntity> loadCBPFileTemplates();
	
	public void stageCBPLineItemFile(List<CBPLineItemEntity> uploadData, long fiscalYr);
	
	public List<CBPLineItemEntity> getUnmatchedDetailsExport(long fiscalYr);
	
	public List<CBPSummaryFileEntity> getUnmatchedSummaryExport (long fiscalYr);

	public List<ComparisonAllDeltaStatusEntity> getallDeltaComments(long fiscalYr, String ein,  String tobaccoClassName, String fiscalQtr,String permitType);

	public List<CBPEntryEntity> getCBPIngestedLineEntryInfo(long cbEntryId);
	
	public List<IngestionWorkflowContextEntity> saveorUpdateIngestionWorkflowContext(IngestionWorkflowContextEntity ctxEntityList);
	
	public List<IngestionWorkflowContextEntity> getIngestionWorkflowContext(long fiscal_yr);

	public void loadCBPSingleFileWF(Long fiscalYr);

	public List<IngestionWorkflowContextEntity> getIngestionContexts();
	
	public CompanyReallocationBeanToUpdate  saveOrUpdateDateSelection(CBPDateConfigEntity cbpDateConfigEntity) throws ParseException;
	
	public String getImporterCompareDetailsSelDate(String tobaccoclassNm, long fiscalYr,String ein);
	
	public List<CBPAmendmentEntity> saveCBPCompareComment(List<CBPAmendmentEntity> amendments, String ein);
	
	public void deleteCompareCommentDetailOnly(long commSeqCbp,long commSeqAllDelta, String permitType);
	
	public void updateCompareCommentDetailOnly(long commSeqCbp,long commSeqAllDelta, String usercomment, String permitType);
	
	public ComparisonCommentAttachmentEntity getCommentAttachment(long docId);
	
	public List<ComparisonCommentAttachmentEntity> findCommentDocbyId(long cbpCommentSeq, long ttbCommentSeq, long allDeltaCommentSeq);
    
	public byte[] getComparisonCommentAttachmentAsByteArray(long docId) throws SQLException;
	
	public void addComparisonCommentAttachment(ComparisonCommentAttachmentEntity attach);
	
	public void deleteDocById(long docId);
	
    Blob getBlob(byte[] doc) throws SQLException;
    
	public byte[] getByteArray(Blob blob) throws SQLException;
	
	public long getDocCount(long cbpCommentSeq, long ttbCommentSeq, long allDeltaCommentSeq);
	
	



}

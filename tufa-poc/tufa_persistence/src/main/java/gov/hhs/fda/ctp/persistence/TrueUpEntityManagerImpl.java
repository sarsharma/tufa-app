/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import java.sql.Blob;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.ParameterMode;

import org.apache.commons.collections.CollectionUtils;
import org.dozer.Mapper;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.CBPAmendment;
import gov.hhs.fda.ctp.common.beans.CBPEntry;
import gov.hhs.fda.ctp.common.beans.CBPImporter;
import gov.hhs.fda.ctp.common.beans.CBPLineEntryUpdateInfo;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.CompanyReallocationBeanToUpdate;
import gov.hhs.fda.ctp.common.beans.ComparisonAllDeltaComments;
import gov.hhs.fda.ctp.common.beans.ExcludePermitDetails;
import gov.hhs.fda.ctp.common.beans.TTBAmendment;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpFilter;
import gov.hhs.fda.ctp.common.comparators.CBPEntryByIdComparator;
import gov.hhs.fda.ctp.common.comparators.CBPImporterByIdComparator;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.model.AmdmntMixedActionEntity;
import gov.hhs.fda.ctp.persistence.model.AmdmntMixedActionTTBEntity;
import gov.hhs.fda.ctp.persistence.model.AssessmentEntity;
import gov.hhs.fda.ctp.persistence.model.AssessmentVersionEntity;
import gov.hhs.fda.ctp.persistence.model.CBPAmendmentEntity;
import gov.hhs.fda.ctp.persistence.model.CBPDateConfigEntity;
import gov.hhs.fda.ctp.persistence.model.CBPEntryEntity;
import gov.hhs.fda.ctp.persistence.model.CBPFileColumnTemplateEntity;
import gov.hhs.fda.ctp.persistence.model.CBPImporterEntity;
import gov.hhs.fda.ctp.persistence.model.CBPLineItemEntity;
import gov.hhs.fda.ctp.persistence.model.CBPQtrCommentEntity;
import gov.hhs.fda.ctp.persistence.model.CBPSummaryFileEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonAllDeltaStatusCommentEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonAllDeltaStatusEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonCommentAttachmentEntity;
import gov.hhs.fda.ctp.persistence.model.IngestionWorkflowContextEntity;
import gov.hhs.fda.ctp.persistence.model.PermitCommentAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.RawIngested;
import gov.hhs.fda.ctp.persistence.model.TTBAmendmentEntity;
import gov.hhs.fda.ctp.persistence.model.TTBCompanyEntity;
import gov.hhs.fda.ctp.persistence.model.TTBPermitEntity;
import gov.hhs.fda.ctp.persistence.model.TTBQtrCommentEntity;
import gov.hhs.fda.ctp.persistence.model.TTBUploadEntity;
import gov.hhs.fda.ctp.persistence.model.TobaccoClassEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpSubDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.TufaEntity;

/**
 * @author tgunter
 *
 */
@Repository("trueUpEntityManager")
public class TrueUpEntityManagerImpl extends StaticDataEntityManagerImpl implements TrueUpEntityManager {

	Logger logger = LoggerFactory.getLogger(TrueUpEntityManagerImpl.class);
	

	@Autowired
	private DozerBeanMapperFactoryBean mapperFactory;

	@Autowired
	private MapperWrapper mapperWrapper;

	@Autowired
	SimpleSQLManager sqlManager;
	/** The company mgmt entity manager. */
	@Autowired
	private CompanyMgmtEntityManager companyMgmtEntityManager;
	

	private final int BATCH_SIZE = 1000;

	@Autowired
	private SecurityContextUtil securityContextUtil;
	
	@Autowired
	private SimpleSQLManager simpleSQLManager;

	

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * gov.hhs.fda.ctp.persistence.TrueUpEntityManager#createTrueUp(gov.hhs.fda.
	 * ctp.persistence.model.TrueUpEntity)
	 */
	@Override
	public String createTrueUp(TrueUpEntity trueUpEntity) {
		long fiscalYear = trueUpEntity.getFiscalYear().longValue();
		TrueUpEntity existingEntity = (TrueUpEntity) getCurrentSession()
				.createQuery("FROM TrueUpEntity WHERE fiscalYear=:fiscalYear").setLong("fiscalYear", fiscalYear)
				.uniqueResult();
		if (existingEntity == null) {
			// create a full set of annual assessments for this trueup
			List<AssessmentEntity> assessments = new ArrayList<>();
			for (int qtr = 0; qtr < 5; qtr++) {
				AssessmentEntity assessment = new AssessmentEntity();
				assessment.setAssessmentQtr(qtr);
				assessment.setAssessmentYr((int) fiscalYear);
				assessment.setAssessmentType("ANNU");
				assessment.setCreatedBy(getUser());

				assessment.setTrueUpEntity(trueUpEntity);
				assessment.setSubmittedInd("N");
				assessment.setCigarSubmittedInd("NNNN");
				assessments.add(assessment);
			}
			trueUpEntity.setAssessments(assessments);
			trueUpEntity.setSubmittedInd("N");
			getCurrentSession().save(trueUpEntity);
			// create version zero for each assessment
			for (AssessmentEntity ae : assessments) {
				AssessmentVersionEntity version = new AssessmentVersionEntity();
				// for cigar assessment (quarter = 0), set the cigar quarter.
				if (ae.getAssessmentQtr() == 0) {
					version.setCigarQtr(4L);
				}
				version.setVersionNum(0L);
				version.setCreatedBy(getUser());
				version.setCreatedDt(new Date());
				version.setAssessmentId(ae.getAssessmentId());
				getCurrentSession().save(version);
			}
			return "1";
		} else {
			return "0";
		}
	}

	@Override
	public List<TrueUpEntity> getTrueUps(TrueUpFilter filter) {
		Criteria criteria = getCurrentSession().createCriteria(TrueUpEntity.class);
		if (filter.getFiscalYrFilters() != null && !filter.getFiscalYrFilters().isEmpty()) {
			criteria.add(Restrictions.in("fiscalYear", filter.getFiscalYrFilters().toArray()));
		}
		Set<String> statii = filter.getStatusFilters();
		if (statii != null) {
			if (statii.contains("Not Submitted")) {
				criteria.add(Restrictions.isNull("submittedDt"));
			}
			if (statii.contains("Submitted")) {
				criteria.add(Restrictions.isNotNull("submittedDt"));
			}
		}
		criteria.addOrder(Order.desc("fiscalYear"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	@Override
	public TrueUpEntity getTrueUpByYear(long fiscalYear) {
		return (TrueUpEntity) getCurrentSession().createQuery("FROM TrueUpEntity WHERE fiscalYear=:fiscalYear")
				.setLong("fiscalYear", fiscalYear).uniqueResult();
	}

	@Override
	public List<TrueUpDocumentEntity> getTrueupDocuments(long fiscalYear) {
		List<TrueUpDocumentEntity> docs = new ArrayList<>();
		TrueUpEntity trueUp = (TrueUpEntity) getCurrentSession()
				.createQuery("FROM TrueUpEntity WHERE fiscalYear=:fiscalYear").setLong("fiscalYear", fiscalYear)
				.uniqueResult();
		if (trueUp != null) {
			if (trueUp.getDocuments() != null) {
				docs.addAll(trueUp.getDocuments());
			}
		}
		return docs;
	}

	@Override
	public void uploadDocument(long fiscalYear, TrueUpDocumentEntity newDocument) {
		if (newDocument != null) {
			logger.debug("TUFA: Uploading document {} for Fiscal Year:{}", newDocument.getDocDesc(), fiscalYear);
		}

		TrueUpEntity trueUp = (TrueUpEntity) getCurrentSession()
				.createQuery("FROM TrueUpEntity WHERE fiscalYear=:fiscalYear").setLong("fiscalYear", fiscalYear)
				.uniqueResult();
		if (trueUp == null) {
			throw new TufaException("Invalid fiscal year" + fiscalYear);
		}
		Set<TrueUpDocumentEntity> docs = trueUp.getDocuments();
		if (docs == null) {
			docs = new HashSet<TrueUpDocumentEntity>();
			trueUp.setDocuments(docs);
		}
		if (docs.contains(newDocument)) {
			throw new TufaException("Document type " + newDocument.getDocDesc() + " already exists for this true-up.");
		}
		// newDocument.setTrueUpEntity(trueUp);
		newDocument.setTrueUpId(trueUp.getTrueUpId());
		// docs.add(newDocument);
		getCurrentSession().saveOrUpdate(newDocument);

		logger.debug("TUFA: Completed uploading of document");
	}

	@Override
	public List<TufaEntity> getIngestionErrors(String type, long fiscalYear) {
		String hql = null;
		String sql = null;
		if (Constants.CBP.equals(type)) {
			hql = "FROM CBPUploadEntity e where e.errors is not null";
		}

		if (Constants.CBPSumm.equals(type)) {

			hql = "SELECT cbpsumm FROM CBPSummaryFileEntity cbpsumm where cbpsumm.error is not null and cbpsumm.fiscalYr = :fiscalyr";
		}

		if (Constants.CBPLine.equals(type)) {

			hql = "SELECT cbpline FROM CBPLineItemEntity cbpline where cbpline.errors is not null and cbpline.fiscalYr = :fiscalyr";
		}

		if (Constants.TTB.equals(type)) {
			hql = "FROM TTBUploadEntity e where e.errors is not null";
			Query query = getCurrentSession().createQuery(hql);
		}

		if (Constants.TTB_VOLUME.equals(type)) {
			hql = "FROM TTBRemovalsEntity e where e.errors is not null";

		}

		Query query = getCurrentSession().createQuery(hql);

		if (Constants.CBPLine.equals(type) || Constants.CBPSumm.equals(type)) {
			query.setParameter("fiscalyr", fiscalYear);
			// query.executeUpdate();
		}

		return query.list();
	}

	/**
	 * batch insert the ttb data from ingestion.
	 */
	@Override
	public void stageIngestionData(List<TufaEntity> uploadData, String type) {
		String sql;
		Query query;

		logger.debug("TUFA: Staging Ingestion Data of {} ", type);

		if (Constants.CBP.equals(type)) {
			sql = "LOCK TABLE TU_CBP_UPLOAD IN EXCLUSIVE MODE WAIT 30";
			query = getCurrentSession().createSQLQuery(sql);
			query.executeUpdate();
			sql = "TRUNCATE TABLE TU_CBP_UPLOAD";
			query = getCurrentSession().createSQLQuery(sql);
			query.executeUpdate();

			logger.debug("\t TUFA: Deleting contents from TU_CBP_UPLOAD table");
		}
		if (Constants.TTB.equals(type)) {
			sql = "LOCK TABLE TU_TTB_UPLOAD IN EXCLUSIVE MODE WAIT 30";
			query = getCurrentSession().createSQLQuery(sql);
			query.executeUpdate();
			sql = "TRUNCATE TABLE TU_TTB_UPLOAD";
			query = getCurrentSession().createSQLQuery(sql);
			query.executeUpdate();

			logger.debug("\t TUFA: Deleting contents from TU_TTB_UPLOAD table");
		}

		if (Constants.TTB_VOLUME.equals(type)) {
			sql = "LOCK TABLE TU_TTB_REMOVALS IN EXCLUSIVE MODE WAIT 30";
			query = getCurrentSession().createSQLQuery(sql);
			query.executeUpdate();
			sql = "TRUNCATE TABLE TU_TTB_REMOVALS";
			query = getCurrentSession().createSQLQuery(sql);
			query.executeUpdate();

			logger.debug("\t TUFA: Deleting contents from TU_TTB_REMOVALS table");
		}

		getCurrentSession().flush();
		getCurrentSession().clear();

		int batchSize = 1000;
		for (int inserts = 0; inserts < uploadData.size(); inserts++) {
			TufaEntity insertEntity = uploadData.get(inserts);
			if (insertEntity instanceof TTBUploadEntity && ((TTBUploadEntity) insertEntity).getEinNum() != null
					&& !((TTBUploadEntity) insertEntity).getEinNum().contains("-"))
				((TTBUploadEntity) insertEntity).setEinNum(
						new StringBuilder(((TTBUploadEntity) insertEntity).getEinNum()).insert(2, "-").toString());
			getCurrentSession().save(insertEntity);
			if (inserts % batchSize == 0 && inserts > 0) {
				getCurrentSession().flush();
				getCurrentSession().clear();
			}
		}
		getCurrentSession().flush();
		getCurrentSession().clear();

		logger.debug("TUFA: Completed staging of ingestion data for {}", type);
	}

	
	public void stageCBPSummaryFile(List<CBPSummaryFileEntity> cbpsummary, long fiscalYear) {
		String hql = "DELETE CBPSummaryFileEntity  WHERE fiscalYr = :fiscalyr";
		Query hqlQuery = getCurrentSession().createQuery(hql);
		hqlQuery.setLong("fiscalyr", fiscalYear);
		int deletecount = hqlQuery.executeUpdate();
		if (deletecount > 0) {
			logger.debug("TUFA: Completed deleting previously ingested summary record for reingestion");
		}

		String sql;
		Query query;

		sql = "LOCK TABLE TU_CBP_SUMMARY_FILE IN EXCLUSIVE MODE WAIT 30";
		query = getCurrentSession().createSQLQuery(sql);
		query.executeUpdate();
		/*
		 * sql = "TRUNCATE TABLE TU_CBP_SUMMARY_FILE"; query =
		 * getCurrentSession().createSQLQuery(sql); query.executeUpdate();
		 */

		getCurrentSession().flush();
		getCurrentSession().clear();

		int batchSize = BATCH_SIZE;
		for (int inserts = 0; inserts < cbpsummary.size(); inserts++) {
			TufaEntity insertEntity = cbpsummary.get(inserts);
			getCurrentSession().save(insertEntity);
			if (inserts % batchSize == 0 && inserts > 0) {
				getCurrentSession().flush();
				getCurrentSession().clear();
			}
		}

		getCurrentSession().flush();
		getCurrentSession().clear();

	}

	/**
	 * Staging CBP Line Detail File
	 */
	public void stageCBPLineItemFile(List<CBPLineItemEntity> uploadData, long fiscalYr) {
		String hql = "DELETE CBPLineItemEntity  WHERE fiscalYr = :fiscalyr";
		Query query = getCurrentSession().createQuery(hql);
		query.setLong("fiscalyr", fiscalYr);
		int deletecount = query.executeUpdate();
		if (deletecount > 0) {
			logger.debug("TUFA: Completed deleting previously ingested record for reingestion");
		}

		logger.debug("TUFA: Staging Ingestion records for Details File for Fiscal Year:{}", fiscalYr);

		String sql = "LOCK TABLE TU_CBP_DETAILS_FILE IN EXCLUSIVE MODE WAIT 30";
		Query querySQL = getCurrentSession().createSQLQuery(sql);
		querySQL.executeUpdate();

		getCurrentSession().flush();
		getCurrentSession().clear();

		int batchSize = BATCH_SIZE;
		for (int inserts = 0; inserts < uploadData.size(); inserts++) {
			TufaEntity insertEntity = uploadData.get(inserts);
			getCurrentSession().save(insertEntity);
			if (inserts % batchSize == 0 && inserts > 0) {
				getCurrentSession().flush();
				getCurrentSession().clear();
			}
		}

		getCurrentSession().flush();
		getCurrentSession().clear();

	}

	@Override
	public void loadCBPSingleFileWF(Long fiscalYr) {

		logger.debug("TUFA: Loading CBP for Fiscal Year:{}", fiscalYr);

		int fiscalYear = fiscalYr.intValue();
		ProcedureCall call = this.getCurrentSession().createStoredProcedureCall("LOAD_CBP_MERGEDVIEW_DATA");
		call.registerParameter("p_fiscal_yr", Integer.class, ParameterMode.IN);
		call.getParameterRegistration("p_fiscal_yr").bindValue(fiscalYear);
		call.getOutputs();

		return;
	}

	@Override
	public void loadTTB(Long fiscalYr) {

		logger.debug("TUFA: Loading TTB for Fiscal Year:{}", fiscalYr);

		int fiscalYear = fiscalYr.intValue();
		ProcedureCall call = this.getCurrentSession().createStoredProcedureCall("load_ttb");
		call.registerParameter("p_fiscal_yr", Integer.class, ParameterMode.IN);
		call.getParameterRegistration("p_fiscal_yr").bindValue(fiscalYear);
		call.getOutputs();
		return;
	}

	@Override
	public List<CBPImporterEntity>  updateCompanyComparisonBucket(List<CBPImporter> bucket) {
		List<Long> ids = new ArrayList<>();
		List<CBPImporterEntity> importerList = new ArrayList<>();
		for (CBPImporter importer : bucket) {
			ids.add(importer.getCbpImporterId());
		}
		// retrieve the importers updated, ordered by id
		Query query = getCurrentSession().createQuery(
				"FROM CBPImporterEntity importer WHERE importer.cbpImporterId IN (:ids) ORDER BY importer.cbpImporterId DESC");
		query.setParameterList("ids", ids);
		importerList = query.list();

		// sort the bucket with same ordering
		Collections.sort(bucket, new CBPImporterByIdComparator());

		// copy the decision into the entity and update
		for (int index = 0; index < bucket.size(); index++) {
			CBPImporterEntity entity = importerList.get(index);
			entity.setAssociationTypeCd(bucket.get(index).getAssociationTypeCd());
			getCurrentSession().update(entity);
		}
		
		return importerList;
	}

	@Override
	public List<TrueUpSubDocumentEntity> getSupportDocs(long fiscalYr) {
		return this.sqlManager.findDocsByFiscalYr(fiscalYr);
	}

	@Override
	public void saveSupportDocument(TrueUpSubDocumentEntity supDoc) {
		supDoc.setAuthor(this.getUser());
		this.getCurrentSession().save(supDoc);
	}

	@Override
	public TrueUpSubDocumentEntity getDocByDocId(long trueupSubdocId) {
		String hql = "FROM TrueUpSubDocumentEntity doc where " + "doc.trueupSubDocId = :docId";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("docId", trueupSubdocId);
		List<TrueUpSubDocumentEntity> result = query.list();
		return result.get(0);
	}

	@Override
	public List<CBPEntryEntity> updateHTSMissingAssociationData(List<CBPEntry> cbpEntries, long fiscalYr) {
		// build list of ids to save
		List<Long> ids = new ArrayList<>();
		for (CBPEntry cbpEntry : cbpEntries) {
			ids.add(cbpEntry.getCbpEntryId());
		}

		String hql1 = "FROM CBPEntryEntity ce where ce.cbpEntryId (:ids) ORDER BY ce.cbpEntryId DESC";
		Query q1 = this.getCurrentSession().createQuery(hql1);
		q1.setParameterList("ids", ids);
		List<CBPEntryEntity> cbpEntriesEntityList = q1.list();

		// sort to make sure they are in same order as query
		Collections.sort(cbpEntries, new CBPEntryByIdComparator());

		for (int index = 0; index < cbpEntries.size(); index++) {
			CBPEntryEntity entity = cbpEntriesEntityList.get(index);
			CBPEntry entry = cbpEntries.get(index);
			entity.setTobaccoClassId(entry.getTobaccoClassId());
			getCurrentSession().update(entity);
		}

		return cbpEntriesEntityList;
	}

	@Override
	/**
	 * Get the associate HTS code bucket.
	 */
	public List<CBPEntryEntity> getAssociateHTSBucket(long fiscalYear) {
		String hql = "SELECT ce FROM CBPEntryEntity ce  inner join ce.cbpImporter as ci "
				+ " where ce.htsCd is not null and ce.defaultHtsFlag='N' and ce.fiscalYear=:fiscalYr "
				+ " and (ci.associationTypeCd not in ('EXCL','UNKN') OR ci.associationTypeCd is null)";

		Query query = this.getCurrentSession().createQuery(hql);
		query.setLong("fiscalYr", fiscalYear);
		List<CBPEntryEntity> cbpEntries = query.list();
		return cbpEntries;
	}

	@Override
	public List<CBPEntryEntity> updateAssociateHTSBucket(List<CBPEntryEntity> entries, long fiscalYear) {
		for (CBPEntryEntity entity : entries) {
			if (entity.getTobaccoClassId() != null) {
				String updateHQL = "update CBPEntryEntity cbe set cbe.tobaccoClassId =:tobaccoClassId " + "WHERE "
						+ "    cbe.cbpEntryId IN ( " + "        SELECT " + "            ce.cbpEntryId "
						+ "        FROM " + "            CBPEntryEntity  ce   "
						+ "            inner join ce.cbpImporter as ci " + "        WHERE "
						+ "            ce.htsCd =:htsCode " + "            AND ce.defaultHtsFlag = 'N' "
						+ "                AND ce.fiscalYear =:fiscalYr "
						+ "                    AND ( ci.associationTypeCd NOT IN ( " + "                'EXCL', "
						+ "                'UNKN' " + "            ) "
						+ "                          OR ci.associationTypeCd IS NULL ) " + "    )";
				Query updateQuery = this.getCurrentSession().createQuery(updateHQL);
				updateQuery.setLong("tobaccoClassId", entity.getTobaccoClassId());
				updateQuery.setString("htsCode", entity.getHtsCd());
				updateQuery.setLong("fiscalYr", fiscalYear);

				updateQuery.executeUpdate();
			}
		}
		return entries;
	}

	@Override
	/**
	 * Get the associate HTS code bucket.
	 */
	public List<CBPEntryEntity> getAssociateHTSMissingBucket(long fiscalYear) {
		String hql = "SELECT ce FROM CBPEntryEntity ce  inner join ce.cbpImporter as ci "
				+ " where ce.htsCd is null and ce.defaultHtsFlag='N' and ce.fiscalYear=:fiscalYr "
				+ "and (ci.associationTypeCd not in ('EXCL','UNKN') or ci.associationTypeCd is null) ";

		Query query = this.getCurrentSession().createQuery(hql);
		query.setLong("fiscalYr", fiscalYear);
		List<CBPEntryEntity> cbpEntries = query.list();
		return cbpEntries;
	}

	@Override
	public TTBPermitEntity getTTBPermit(long permitId) {
		String hql = "FROM TTBPermitEntity e where e.ttbPermitId = :permit";
		Query query = getCurrentSession().createQuery(hql);
		query.setLong("permit", permitId);
		return (TTBPermitEntity) query.list().get(0);
	}

	@Override
	public void saveTTBPermit(TTBPermitEntity permit) {
		getCurrentSession().save(permit);
	}

	@Override
	public void updateCompaniesForIncludeBucket(List<CompanyIngestion> companies, long fiscalYr) {

		Map<String, CompanyIngestion> cmpyMap = new HashMap<>();

		for (CompanyIngestion cmpy : companies) {
			cmpyMap.put(cmpy.getEinNo(), cmpy);
		}

		String hql1 = "SELECT ci FROM CBPImporterEntity ci where ci.importerEIN in (:eins) and ci.fiscalYr=:fiscalYr ORDER BY ci.importerEIN DESC";
		Query q1 = this.getCurrentSession().createQuery(hql1);
		q1.setParameterList("eins", cmpyMap.keySet());
		q1.setLong("fiscalYr", fiscalYr);

		List<CBPImporterEntity> cbpImporterEntityList = q1.list();

		for (CBPImporterEntity impEntity : cbpImporterEntityList) {
			impEntity.setIncludeFlag(cmpyMap.get(impEntity.getImporterEIN()).getIncludeFlag());
			this.getCurrentSession().update(impEntity);
		}

		String hql2 = "SELECT ttbcmpy FROM TTBCompanyEntity ttbcmpy where ttbcmpy.einNum in (:eins) and ttbcmpy.fiscalYr=:fiscalYr ORDER BY ttbcmpy.einNum DESC";
		Query q2 = this.getCurrentSession().createQuery(hql2);
		q2.setParameterList("eins", cmpyMap.keySet());
		q2.setLong("fiscalYr", fiscalYr);

		List<TTBCompanyEntity> ttbCompanyEntityList = q2.list();

		for (TTBCompanyEntity ttbCmpyEntity : ttbCompanyEntityList) {
			ttbCmpyEntity.setIncludeFlag(cmpyMap.get(ttbCmpyEntity.getEinNum()).getIncludeFlag());
			this.getCurrentSession().update(ttbCmpyEntity);
		}

	}

	@Override
	public List<TrueUpEntity> getAllTrueUps() {
		String hql = "FROM TrueUpEntity e";

		Query query = getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public void deleteIngestionFile(Long fiscalYr, String doctype) {

		logger.debug("TUFA: Deleting Ingestion File for type {} for Fiscal Year:{}...", doctype, fiscalYr);

		switch (doctype) {
		case Constants.CBP:
			this.sqlManager.removeCBPEntries(fiscalYr);
			break;
		case Constants.TTB:
			this.sqlManager.removeTTBEntries(fiscalYr);
			break;
		case Constants.TTB_VOLUME:
			this.sqlManager.removeTTBVolumes(fiscalYr);
			break;
		case Constants.CBPSumm:
			this.sqlManager.removeCBPSummaryData(fiscalYr);
			break;
		case Constants.CBPLine:
			this.sqlManager.removeCBPDetailsData(fiscalYr);
			break;
		}
		this.sqlManager.removeTrueUpDoc(fiscalYr, doctype);

		logger.debug("TUFA: Deleted Ingestion File for type {} for Fiscal Year:{}", doctype, fiscalYr);
	}

	@Override
	public List<TTBAmendmentEntity> getTTBAmendments(long fiscalYr, long companyId, List<TTBAmendment> tobaccoTypes) {

		List<String> tTypes = new ArrayList<>();

		for (TTBAmendment amdment : tobaccoTypes) {
			tTypes.add(amdment.getTobaccoType());
		}

		String hql = " select ttbAmdmt from TTBAmendmentEntity ttbAmdmt " + " where ttbAmdmt.fiscalYr=:fiscalYr  "
				+ " and ttbAmdmt.ttbCompanyId =:companyId and ttbAmdmt.qtr in (:qtr) and ttbAmdmt.tobaccoClass.tobaccoClassNm in (:tobaccoTypes) ";

		Query query = this.getCurrentSession().createQuery(hql);
		query.setLong("fiscalYr", fiscalYr);
		query.setLong("companyId", companyId);

		if (tobaccoTypes.get(0).getInclAllQtrs() != null && tobaccoTypes.get(0).getInclAllQtrs())
			query.setParameterList("qtr", Arrays.asList("1", "2", "3", "4"));
		else
			query.setParameterList("qtr", Arrays.asList(tobaccoTypes.get(0).getQtr()));

		query.setParameterList("tobaccoTypes", tTypes);

		List<TTBAmendmentEntity> amendments = query.list();

		return amendments;
	}

	@Override
	public List<TTBAmendmentEntity> saveTTBAmendment(List<TTBAmendmentEntity> amendments) {
		logger.debug("TUFA: Saving TTB Amendment Info...");

		List<TTBAmendmentEntity> amndmnts = new ArrayList<>();

		for (TTBAmendmentEntity amdtmnt : amendments) {
			// Needs to be removed. Is a work around to prevent deletion of
			// existing comments
			// when an amendment having empty comments is being created/updated
			// .
			this.setTTBDetailsComments(amdtmnt);
			
			amdtmnt.setFdaDelta(null);
			this.getCurrentSession().saveOrUpdate(amdtmnt);
			this.getCurrentSession().flush();
			this.getCurrentSession().refresh(amdtmnt);

			amndmnts.add(amdtmnt);
			
		}
		//below block will take care of adding the mixed action details.
		if(CollectionUtils.isNotEmpty(amndmnts)) {
			List<AmdmntMixedActionTTBEntity> list = new ArrayList<AmdmntMixedActionTTBEntity>();
			for(TTBAmendmentEntity entity : amndmnts) {
				long amendmentId = entity.getTtbAmendmentId();
				String selecthql = " FROM AmdmntMixedActionTTBEntity ent where ent.amendmentId =:  amendmentId";
				Query selecthqlQuery = getCurrentSession().createQuery(selecthql);
				selecthqlQuery.setLong("amendmentId", amendmentId);
				List<AmdmntMixedActionTTBEntity> mixedActionAmedList = selecthqlQuery.list();
				if(entity.getAcceptanceFlag().equalsIgnoreCase("M")) {
					entity.getMixedActionQuarterDetails().keySet().forEach(key->{ 
						AmdmntMixedActionTTBEntity mixedEntity = null;
						if(CollectionUtils.isNotEmpty(mixedActionAmedList)) {
							for(AmdmntMixedActionTTBEntity ent : mixedActionAmedList) {
								if((ent.getMonth()!=null ?ent.getMonth() : ent.getPreviousMonth()).equalsIgnoreCase(key)) {
									mixedEntity = ent;
									mixedEntity.setMonth(key);
									mixedEntity.setMixedIndicator(entity.getMixedActionQuarterDetails().get(key));
									mixedEntity.setAmendmentId(entity.getTtbAmendmentId());
									list.add(mixedEntity);
								}
							}
						} else {
							mixedEntity = new AmdmntMixedActionTTBEntity();
							mixedEntity.setMonth(key);
							mixedEntity.setMixedIndicator(entity.getMixedActionQuarterDetails().get(key));
							mixedEntity.setAmendmentId(entity.getTtbAmendmentId());
							list.add(mixedEntity);
						} 

					});
				} else {
					String hql = "DELETE AmdmntMixedActionTTBEntity  WHERE amendmentId = :amendmentId";
					Query hqlQuery = getCurrentSession().createQuery(hql);
					hqlQuery.setLong("amendmentId", amendmentId);
					int deletecount = hqlQuery.executeUpdate();
				}
			}
			if(CollectionUtils.isNotEmpty(list) && list.size()>0) {
				int batchSize = BATCH_SIZE;
				for (int inserts = 0; inserts < list.size(); inserts++) {
					AmdmntMixedActionTTBEntity insertEntity = list.get(inserts);
					getCurrentSession().save(insertEntity);
					if (inserts % batchSize == 0 && inserts > 0) {
						getCurrentSession().flush();
						getCurrentSession().clear();
					}
				}
				getCurrentSession().flush();
				getCurrentSession().clear();
			}
		}

		logger.debug("TUFA: Completed saving of TTB Amendment Info");
		amndmnts.forEach(amd->{
			if(amd.getAcceptanceFlag().equalsIgnoreCase("M")) {
				amd.setMixedActionVolume(amd.getAmendedTotalVolume());
				amd.setMixedActionTax(amd.getAmendedTotalTax());
				amd.setAmendedTotalTax(null);
				amd.setAmendedTotalVolume(null);
			} else {
				amd.setMixedActionVolume(null);
				amd.setMixedActionTax(null);
			}
		});
		
		return amndmnts;
	}

	private void setTTBDetailsComments(TTBAmendmentEntity amdtmnt) {

		if (amdtmnt != null && amdtmnt.getTtbAmendmentId() != null) {
			TTBAmendmentEntity amendment = this.getCurrentSession().get(TTBAmendmentEntity.class,
					amdtmnt.getTtbAmendmentId());
			if (amendment != null) {
				amdtmnt.setUserComments(amendment.getUserComments());
				this.getCurrentSession().evict(amendment);
			}
		}
	}

	@Override
	public List<TTBAmendmentEntity> saveTTBCompareComment(List<TTBAmendmentEntity> amendments) {
		logger.debug("TUFA: Saving TTB Comparison comment...");

		List<TTBAmendmentEntity> amndmnts = new ArrayList<>();

		// Assuming that 'amendments' list is for a mixed tobacco type
		// (e.g. 'Pipe-RYO') with 2 elements, supposedly 2 constituent tobacco
		// types such as Pipe
		// and RYO and both the elements will have the same comments.
		List<TTBQtrCommentEntity> newComments = amendments.get(0).getUserComments();

		List<TTBQtrCommentEntity> mergedComments = null;

		for (TTBAmendmentEntity amdtmnt : amendments) {

			TTBAmendmentEntity amdt = this.getTTBAmndmntByIdORByUniqCols(amdtmnt);

			if (amdt == null) {
				amdt = amdtmnt;
				mergedComments = newComments;
				for (TTBQtrCommentEntity comm : mergedComments)
					comm.setAuthor(securityContextUtil.getSessionUser());
				// Set In Progress status for the amendment as there is no
				// action taken for the delta yet(no entry in the amendment
				// table), only a comment is being added.
				amdt.setInProgressFlag("Y");
//				amdt.setEdgeStatusFlag("Y");
			} else {
				mergedComments = this.mergeTTBDetailsComments(newComments, amdt.getUserComments());
				if(null == amdt.getAcceptanceFlag() && ("N").equalsIgnoreCase(amdt.getInProgressFlag()) 
						&& ("Y").equalsIgnoreCase(amdt.getEdgeStatusFlag())) {
					amdt.setInProgressFlag("Y"); 
				}
			}
			

			if (amdt.getUserComments() == null)
				amdt.setUserComments(new ArrayList<TTBQtrCommentEntity>());

			amdt.setUserComments(mergedComments);

			for (TTBQtrCommentEntity comment : mergedComments) {
				if (comment.getTtbAmendments() == null)
					comment.setTtbAmendments(new ArrayList<TTBAmendmentEntity>());
				comment.getTtbAmendments().add(amdt);
			}

			this.getCurrentSession().saveOrUpdate(amdt);
			this.getCurrentSession().flush();
			this.getCurrentSession().refresh(amdt);

			amndmnts.add(amdt);
		}

		logger.debug("TUFA: Completed saving of TTB Comparison comment");

		return amndmnts;

	}
	
	@Override
	public List<TTBAmendmentEntity> saveTTBCompareComment(List<TTBAmendmentEntity> amendments, String ein, String tobaccoClass) {
		logger.debug("TUFA: Saving TTB Comparison comment...");

		List<TTBAmendmentEntity> amndmnts = new ArrayList<>();
		int qtr= 0;
		List<TTBQtrCommentEntity> mergedComments = null;
		// Assuming that 'amendments' list is for a mixed tobacco type
		// (e.g. 'Pipe-RYO') with 2 elements, supposedly 2 constituent tobacco
		// types such as Pipe
		// and RYO and both the elements will have the same comments.
		List<TTBQtrCommentEntity> newComments = amendments.get(0).getUserComments();
		TTBQtrCommentEntity newCommEntity = newComments.get(0);
		String commentQtrs = newCommEntity.getCommentQtrs();
		List<Integer> excludedQtrList = new ArrayList<Integer>();;
		

		
		long companyid = companyMgmtEntityManager.getCompanyByEIN(ein)!= null?companyMgmtEntityManager.getCompanyByEIN(ein).getCompanyId():0;
		//this means it is an ingest only company
		if(companyid != 0) {
				excludedQtrList = getExcludedQuarterList(Integer.parseInt(amendments.get(0).getFiscalYr()), companyid, commentQtrs);
		for (TTBAmendmentEntity amdtmnt : amendments) {
			qtr = Integer.parseInt(amdtmnt.getQtr());
			
			
			amdtmnt.setTtbCompanyId(companyid);
			TTBAmendmentEntity amdt = this.getTTBAmndmntByIdORByUniqCols(amdtmnt);

			if (amdt == null) {
				if(excludedQtrList.size() > 0 && excludedQtrList.contains(qtr) ) {
					continue;
				}
				amdt = amdtmnt;
				mergedComments = newComments;
				for (TTBQtrCommentEntity comm : mergedComments)
					comm.setAuthor(securityContextUtil.getSessionUser());
				// Set In Progress status for the amendment as there is no
				// action taken for the delta yet(no entry in the amendment
				// table), only a comment is being added.
				amdt.setInProgressFlag("Y");
//				amdt.setEdgeStatusFlag("Y");
			} else {
				mergedComments = this.mergeTTBDetailsComments(newComments, amdt.getUserComments());
				/*for (TTBQtrCommentEntity comtemp : newComments) {
						comtemp.setAuthor(securityContextUtil.getSessionUser());
						amdt.getUserComments().add(comtemp);
				}
				mergedComments = amdt.getUserComments();
				
*/
				if(null == amdt.getAcceptanceFlag() && ("N").equalsIgnoreCase(amdt.getInProgressFlag()) 
						&& ("Y").equalsIgnoreCase(amdt.getEdgeStatusFlag())) {
					amdt.setInProgressFlag("Y"); 
				}
			}

			if (amdt.getUserComments() == null)
				amdt.setUserComments(new ArrayList<TTBQtrCommentEntity>());

			amdt.setUserComments(mergedComments);

			for (TTBQtrCommentEntity comment : mergedComments) {
				if (comment.getTtbAmendments() == null)
					comment.setTtbAmendments(new ArrayList<TTBAmendmentEntity>());
				comment.getTtbAmendments().add(amdt);
			}

			this.getCurrentSession().saveOrUpdate(amdt);
			this.getCurrentSession().flush();
			this.getCurrentSession().refresh(amdt);

			amndmnts.add(amdt);
		}
		}
			TrueUpComparisonResults model = new TrueUpComparisonResults();
			List<ComparisonAllDeltaComments> userCommentslist = new ArrayList<ComparisonAllDeltaComments>();
			ComparisonAllDeltaComments  usercomment = new ComparisonAllDeltaComments();
			usercomment.setCommentQtrs(newCommEntity.getCommentQtrs());
			usercomment.setUserComment(newCommEntity.getUserComment());
			if(amndmnts.size() > 0) {
				for(TTBQtrCommentEntity matchcomm:amndmnts.get(0).getUserComments()) {
					if(newCommEntity.getUserComment().equals(matchcomm.getUserComment())) {
						usercomment.setMatchCommSeq(matchcomm.getCommentSeq());
						break;

					}
				}
			}
			model.setFiscalYr(amendments.get(0).getFiscalYr());
			model.setTobaccoClass(tobaccoClass);
			model.setEin(ein);
			model.setPermitType("MANU");
			userCommentslist.add(usercomment);
			model.setUserComments(userCommentslist);
			populateSaveCommentsAllDeltas(model,excludedQtrList) ;
			

		logger.debug("TUFA: Completed saving of TTB Comparison comment");

		return amndmnts;

	}

	private TTBAmendmentEntity getTTBAmndmntByIdORByUniqCols(TTBAmendmentEntity amdtmnt) {

		TTBAmendmentEntity amdt = null;
		if (amdtmnt.getTtbAmendmentId() != null)
			amdt = this.getCurrentSession().get(TTBAmendmentEntity.class, amdtmnt.getTtbAmendmentId());

		if (amdt == null) {

			String hql = "from TTBAmendmentEntity amendmnt where amendmnt.ttbCompanyId=:companyId and "
					+ " amendmnt.tobaccoClassId=:tobaccoClassId and amendmnt.fiscalYr=:fiscalYr and  amendmnt.qtr=:qtr ";
			Query qry = this.getCurrentSession().createQuery(hql);
			qry.setParameter("companyId", amdtmnt.getTtbCompanyId());
			qry.setParameter("tobaccoClassId", amdtmnt.getTobaccoClassId());
			qry.setParameter("fiscalYr", amdtmnt.getFiscalYr());
			qry.setParameter("qtr", amdtmnt.getQtr());

			amdt = (TTBAmendmentEntity) qry.uniqueResult();
		}

		return amdt;
	}

	private List<TTBQtrCommentEntity> mergeTTBDetailsComments(List<TTBQtrCommentEntity> updates,
			List<TTBQtrCommentEntity> dbComments) {

		String sessionUser = securityContextUtil.getSessionUser();

		for (TTBQtrCommentEntity comment : updates) {
			if (dbComments.contains(comment)) {
				TTBQtrCommentEntity dbComment = dbComments.get(dbComments.indexOf(comment));
				dbComment.setUserComment(comment.getUserComment());
				dbComment.setModifiedBy(sessionUser);

			} else {
				comment.setAuthor(sessionUser);
				dbComments.add(comment);
			}
		}

		return dbComments;
	}

	@Override
	public void deleteTTBCompareComment(List<TTBAmendmentEntity> amends) {

		TTBQtrCommentEntity comment = amends.get(0).getUserComments().get(0);

		for (TTBAmendmentEntity amendment : amends) {
			amendment = this.getTTBAmndmntByIdORByUniqCols(amendment);
			String sql = "delete from TU_TTB_DETAIL_QTR_COMMENTS amendcmmt where amendcmmt.comment_seq=:commentSeq "
					+ " and amendcmmt.ttb_amendment_id=:amendId";
			Query deleteQry = this.getCurrentSession().createSQLQuery(sql);
			deleteQry.setLong("commentSeq", comment.getCommentSeq());
			deleteQry.setLong("amendId", amendment.getTtbAmendmentId());
			deleteQry.executeUpdate();
			this.getCurrentSession().flush();
		}

		String hql = "delete from TU_TTB_QTR_COMMENT comments where comments.comment_seq=:commentSeq";
		Query deleteQry = this.getCurrentSession().createSQLQuery(hql);
		deleteQry.setLong("commentSeq", comment.getCommentSeq());
		deleteQry.executeUpdate();
		
		String dochql = "delete from TU_COMPARISON_COMMENT_DOC docs where docs.TTB_COMMENT_SEQ=:commentSeq";
		Query docdeleteQry = this.getCurrentSession().createSQLQuery(dochql);
		docdeleteQry.setLong("commentSeq", comment.getCommentSeq());
		docdeleteQry.executeUpdate();

	}

	@Override
	public List<TTBAmendmentEntity> updateTTBCompareFDAAcceptFlag(List<TTBAmendmentEntity> amendments) {
		logger.debug("TUFA: Updating TTB Compare FDA Accept Flag");

		List<TTBAmendmentEntity> amndmnts = new ArrayList<>();
		for (TTBAmendmentEntity amdtmnt : amendments) {
			if (amdtmnt.getTtbAmendmentId() != null) {
				String hql = "Update TTBAmendmentEntity ttbamd set ttbamd.amendedTotalTax=null, ttbamd.amendedTotalVolume= null, ttbamd.acceptanceFlag=:flag, ttbamd.inProgressFlag=:inprogflag, ttbamd.fdaDelta=:delta, ttbamd.acceptedIngestedTotalTax=:acceptedIngestedTax,  ttbamd.previousAcceptenceFlag = null, ttbamd.adjProcessFlag=:adjProcessFlag where ttbamd.ttbAmendmentId=:ttbAmendmentId";
				Query query = this.getCurrentSession().createQuery(hql);
				query.setParameter("ttbAmendmentId", amdtmnt.getTtbAmendmentId());
				query.setParameter("flag", amdtmnt.getAcceptanceFlag());
				query.setParameter("inprogflag", amdtmnt.getInProgressFlag());
				query.setParameter("acceptedIngestedTax", amdtmnt.getAcceptedIngestedTotalTax());
				query.setParameter("adjProcessFlag", amdtmnt.getAdjProcessFlag());
				query.setParameter("delta", null);
				query.executeUpdate();
			} else {
				amdtmnt.setFdaDelta(null);
				this.getCurrentSession().save(amdtmnt);
			}

			this.getCurrentSession().flush();
			amndmnts.add(amdtmnt);
		}

		for (TTBAmendmentEntity amdtmnt : amndmnts) {
			this.getCurrentSession().refresh(amdtmnt);
		}

		if(CollectionUtils.isNotEmpty(amndmnts)) {
			for(TTBAmendmentEntity entity : amndmnts) {
				if((entity.getAcceptanceFlag() != null &&  !entity.getAcceptanceFlag().equalsIgnoreCase("M"))
						|| (entity.getInProgressFlag() != null && entity.getInProgressFlag().equalsIgnoreCase("Y"))) {
					simpleSQLManager.deleteMixedActionDetailsManu(entity.getTtbAmendmentId());
//					});
				}
			}
		}
		
		return amndmnts;
	}

	@Override
	public List<CBPAmendmentEntity> getCBPAmendments(long fiscalYr, long companyId, List<CBPAmendment> tobaccoTypes) {

		List<String> tTypes = new ArrayList<>();

		for (CBPAmendment amdment : tobaccoTypes) {
			tTypes.add(amdment.getTobaccoType());
		}

		String hql = " select cbpAmdmt from CBPAmendmentEntity cbpAmdmt " + " where cbpAmdmt.fiscalYr=:fiscalYr  "
				+ " and cbpAmdmt.cbpCompanyId =:companyId and cbpAmdmt.qtr in (:qtr) and cbpAmdmt.tobaccoClass.tobaccoClassNm in (:tobaccoTypes)";

		Query query = this.getCurrentSession().createQuery(hql);
		query.setLong("fiscalYr", fiscalYr);
		query.setLong("companyId", companyId);
		if (tobaccoTypes.get(0).getInclAllQtrs() != null && tobaccoTypes.get(0).getInclAllQtrs())
			query.setParameterList("qtr", Arrays.asList("1", "2", "3", "4"));
		else
			query.setParameterList("qtr", Arrays.asList(tobaccoTypes.get(0).getQtr()));

		query.setParameterList("tobaccoTypes", tTypes);

		List<CBPAmendmentEntity> amendments = query.list();

		return amendments;
	}

	@Override
	public List<CBPAmendmentEntity> saveCBPAmendment(List<CBPAmendmentEntity> amendments) {
		logger.debug("TUFA: Saving TTB Amendment Info...");

		List<CBPAmendmentEntity> amndmnts = new ArrayList<>();
		for (CBPAmendmentEntity amdtmnt : amendments) {
			// Needs to be removed. Is a work around to prevent deletion of
			// existing comments
			// when an amendment having empty comments is being created/updated
			// .
			this.setCBPDetailsComments(amdtmnt);
			
			amdtmnt.setFdaDelta(null);
			this.getCurrentSession().saveOrUpdate(amdtmnt);
			this.getCurrentSession().flush();
			this.getCurrentSession().refresh(amdtmnt);
			amndmnts.add(amdtmnt);
		}
		//below block will take care of adding the mixed action details.
		if(CollectionUtils.isNotEmpty(amndmnts)) {
			List<AmdmntMixedActionEntity> list = new ArrayList<AmdmntMixedActionEntity>();
			for(CBPAmendmentEntity entity : amndmnts) {
				long amendmentId = entity.getCbpAmendmentId();
				String selecthql = " FROM AmdmntMixedActionEntity ent where ent.amendmentId =:  amendmentId";
				Query selecthqlQuery = getCurrentSession().createQuery(selecthql);
				selecthqlQuery.setLong("amendmentId", amendmentId);
				List<AmdmntMixedActionEntity> mixedActionAmedList = selecthqlQuery.list();
				if(entity.getAcceptanceFlag().equalsIgnoreCase("M")) {
					entity.getMixedActionQuarterDetails().keySet().forEach(key->{ 
						AmdmntMixedActionEntity mixedEntity = null;
						if(CollectionUtils.isNotEmpty(mixedActionAmedList)) {
							for(AmdmntMixedActionEntity ent : mixedActionAmedList) {
								if((ent.getMonth()!=null ?ent.getMonth() : ent.getPreviousMonth()).equalsIgnoreCase(key)) {
									mixedEntity = ent;
									mixedEntity.setMonth(key);
									mixedEntity.setMixedIndicator(entity.getMixedActionQuarterDetails().get(key));
									mixedEntity.setAmendmentId(entity.getCbpAmendmentId());
									list.add(mixedEntity);
								}
							}
						} else {
							mixedEntity = new AmdmntMixedActionEntity();
							mixedEntity.setMonth(key);
							mixedEntity.setMixedIndicator(entity.getMixedActionQuarterDetails().get(key));
							mixedEntity.setAmendmentId(entity.getCbpAmendmentId());
							list.add(mixedEntity);
						} 

					});
				} else {
					String hql = "DELETE AmdmntMixedActionEntity  WHERE amendmentId = :amendmentId";
					Query hqlQuery = getCurrentSession().createQuery(hql);
					hqlQuery.setLong("amendmentId", amendmentId);
					int deletecount = hqlQuery.executeUpdate();
				}
			}
			if(CollectionUtils.isNotEmpty(list) && list.size()>0) {
				int batchSize = BATCH_SIZE;
				for (int inserts = 0; inserts < list.size(); inserts++) {
					AmdmntMixedActionEntity insertEntity = list.get(inserts);
					getCurrentSession().save(insertEntity);
					if (inserts % batchSize == 0 && inserts > 0) {
						getCurrentSession().flush();
						getCurrentSession().clear();
					}
				}
				getCurrentSession().flush();
				getCurrentSession().clear();
			}
		}

		logger.debug("TUFA: Completed saving of TTB Amendment Info");
		amndmnts.forEach(amd->{
			if(amd.getAcceptanceFlag().equalsIgnoreCase("M")) {
				amd.setMixedActionVolume(amd.getAmendedTotalVolume());
				amd.setMixedActionTax(amd.getAmendedTotalTax());
				amd.setAmendedTotalTax(null);
				amd.setAmendedTotalVolume(null);
			} else {
				amd.setMixedActionVolume(null);
				amd.setMixedActionTax(null);
			}
		});
		return amndmnts;
	}

	private void setCBPDetailsComments(CBPAmendmentEntity amdtmnt) {

		if (amdtmnt != null && amdtmnt.getCbpAmendmentId() != null) {
			CBPAmendmentEntity amendment = this.getCurrentSession().get(CBPAmendmentEntity.class,
					amdtmnt.getCbpAmendmentId());
			if (amendment != null) {
				amdtmnt.setUserComments(amendment.getUserComments());
				this.getCurrentSession().evict(amendment);
			}
		}
	}

	@Override
	public List<CBPAmendmentEntity> saveCBPCompareComment(List<CBPAmendmentEntity> amendments) {
		logger.debug("TUFA: Saving CBP Comparison comment...");

		List<CBPAmendmentEntity> amndmnts = new ArrayList<>();

		List<CBPQtrCommentEntity> newComments = amendments.get(0).getUserComments();

		List<CBPQtrCommentEntity> mergedComments = null;

		for (CBPAmendmentEntity amdtmnt : amendments) {

			CBPAmendmentEntity amdt = this.getCBPAmndmntByIdORByUniqCols(amdtmnt);

			if (amdt == null) {
				amdt = amdtmnt;
				mergedComments = newComments;
				for (CBPQtrCommentEntity comm : mergedComments)
					comm.setAuthor(securityContextUtil.getSessionUser());
				// Set In Progress status for the amendment as there is no
				// action taken for the delta yet(no entry in the amendment
				// table), only a comment is being added.
				amdt.setInProgressFlag("Y");
//				amdt.setEdgeStatusFlag("Y");

			} else {
				mergedComments = this.mergeCBPDetailsComments(newComments, amdt.getUserComments());
				if(null == amdt.getAcceptanceFlag() && ("N").equalsIgnoreCase(amdt.getInProgressFlag()) 
						&& ("Y").equalsIgnoreCase(amdt.getEdgeStatusFlag())) {
					amdt.setInProgressFlag("Y"); 
				}
			}

			if (amdt.getUserComments() == null)
				amdt.setUserComments(new ArrayList<CBPQtrCommentEntity>());

			amdt.setUserComments(mergedComments);

			for (CBPQtrCommentEntity comment : mergedComments) {
				if (comment.getCbpAmendments() == null)
					comment.setCbpAmendments(new ArrayList<CBPAmendmentEntity>());
				comment.getCbpAmendments().add(amdt);
			}

			this.getCurrentSession().saveOrUpdate(amdt);
			this.getCurrentSession().flush();
			this.getCurrentSession().refresh(amdt);

			amndmnts.add(amdt);
		}

		logger.debug("TUFA: Completed saving of CBP Comparison comment");

		return amndmnts;
	}

	
	private CBPAmendmentEntity getCBPAmndmntByIdORByUniqCols(CBPAmendmentEntity amdtmnt) {

		CBPAmendmentEntity amdt = null;
		if (amdtmnt.getCbpAmendmentId() != null)
			amdt = this.getCurrentSession().get(CBPAmendmentEntity.class, amdtmnt.getCbpAmendmentId());

		if (amdt == null) {

			String hql = "from CBPAmendmentEntity amendmnt where amendmnt.cbpCompanyId=:companyId and "
					+ " amendmnt.tobaccoClassId=:tobaccoClassId and amendmnt.fiscalYr=:fiscalYr and  amendmnt.qtr=:qtr ";
			Query qry = this.getCurrentSession().createQuery(hql);
			qry.setParameter("companyId", amdtmnt.getCbpCompanyId());
			qry.setParameter("tobaccoClassId", amdtmnt.getTobaccoClassId());
			qry.setParameter("fiscalYr", amdtmnt.getFiscalYr());
			qry.setParameter("qtr", "1-4".equals(amdtmnt.getQtr()) ? "5" :amdtmnt.getQtr());

			amdt = (CBPAmendmentEntity) qry.uniqueResult();
		}

		return amdt;
	}

	private ComparisonAllDeltaStatusEntity getCmpStsDeltaByIdORByUniqCols(ComparisonAllDeltaStatusEntity cmpsts,
			TrueUpComparisonResults model) {

		ComparisonAllDeltaStatusEntity sts = null;

		if (cmpsts.getCmpAllDeltaId() != null)
			sts = this.getCurrentSession().get(ComparisonAllDeltaStatusEntity.class, cmpsts.getCmpAllDeltaId());

		String permitType = null;

		if (model.getPermitType().equalsIgnoreCase("Importer") || model.getPermitType().equalsIgnoreCase("IMPT")) {
			permitType = "IMPT";
		} else {
			permitType = "MANU";
		}

		if (sts == null) {
			String hql = "  FROM ComparisonAllDeltaStatusEntity deltaSts WHERE deltaSts.ein=:ein "
					+ " AND deltaSts.fiscalYr=:fiscalYr "
					+ " AND deltaSts.fiscalQtr=:qtr AND deltaSts.tobaccoClassName=:ttnm  AND deltaSts.permitType=:permitType ";

			Query query = this.getCurrentSession().createQuery(hql);
			query.setParameter("ein", model.getEin());
			query.setParameter("fiscalYr", model.getFiscalYr());
			query.setParameter("ttnm", model.getTobaccoClass());
			query.setParameter("permitType", permitType);
			query.setParameter("qtr", model.getQuarter());

			sts = (ComparisonAllDeltaStatusEntity) query.uniqueResult();
		}

		return cmpsts;
	}

	private List<CBPQtrCommentEntity> mergeCBPDetailsComments(List<CBPQtrCommentEntity> updates,
			List<CBPQtrCommentEntity> dbComments) {

		String sessionUser = securityContextUtil.getSessionUser();

		for (CBPQtrCommentEntity comment : updates) {
			if (dbComments.contains(comment)) {
				CBPQtrCommentEntity dbComment = dbComments.get(dbComments.indexOf(comment));
				dbComment.setUserComment(comment.getUserComment());
				dbComment.setModifiedBy(sessionUser);

			} else {
				comment.setAuthor(sessionUser);
				dbComments.add(comment);
			}
		}

		return dbComments;
	}

	private List<ComparisonAllDeltaStatusCommentEntity> mergeCmpAllDeltaStsComments(
			List<ComparisonAllDeltaStatusCommentEntity> updates,
			List<ComparisonAllDeltaStatusCommentEntity> dbComments) {

		String sessionUser = securityContextUtil.getSessionUser();

		for (ComparisonAllDeltaStatusCommentEntity comment : updates) {
			if (dbComments.contains(comment)) {
				ComparisonAllDeltaStatusCommentEntity dbComment = dbComments.get(dbComments.indexOf(comment));
				dbComment.setUserComment(comment.getUserComment());
				dbComment.setModifiedBy(sessionUser);

			} else {
				comment.setAuthor(sessionUser);
				dbComments.add(comment);
			}
		}

		return dbComments;
	}

	@Override
	public void deleteCBPCompareComment(List<CBPAmendmentEntity> amends) {

		CBPQtrCommentEntity comment = amends.get(0).getUserComments().get(0);

		for (CBPAmendmentEntity amdtmnt : amends) {

			CBPAmendmentEntity amendment = this.getCBPAmndmntByIdORByUniqCols(amdtmnt);
			String sql = "delete from TU_CBP_DETAIL_QTR_COMMENTS amendcmmt where amendcmmt.comment_seq=:commentSeq "
					+ " and amendcmmt.cbp_amendment_id=:amendId";
			Query deleteQry = this.getCurrentSession().createSQLQuery(sql);
			deleteQry.setLong("commentSeq", comment.getCommentSeq());
			deleteQry.setLong("amendId", amendment.getCbpAmendmentId());
			deleteQry.executeUpdate();
		}

		String hql = "delete from TU_CBP_QTR_COMMENT comments where comments.comment_seq=:commentSeq";
		Query deleteQry = this.getCurrentSession().createSQLQuery(hql);
		deleteQry.setLong("commentSeq", comment.getCommentSeq());
		deleteQry.executeUpdate();
		
		String dochql = "delete from TU_COMPARISON_COMMENT_DOC docs where docs.CBP_COMMENT_SEQ=:commentSeq";
		Query docdeleteQry = this.getCurrentSession().createSQLQuery(dochql);
		docdeleteQry.setLong("commentSeq", comment.getCommentSeq());
		docdeleteQry.executeUpdate();
	}

	@Override
	public List<CBPAmendmentEntity> updateCBPCompareFDAAcceptFlag(List<CBPAmendmentEntity> amendments) {
		logger.debug("TUFA: Updating CBP Compare FDA Accept Flag");

		List<CBPAmendmentEntity> amndmnts = new ArrayList<>();
		for (CBPAmendmentEntity amdtmnt : amendments) {
			if (amdtmnt.getCbpAmendmentId() != null) {
				if ("N" == amdtmnt.getreviewFlag()) {
					String hql = "Update CBPAmendmentEntity cbpamd set  cbpamd.acceptanceFlag=:flag,cbpamd.inProgressFlag=:inprogflag,cbpamd.fdaDelta=:delta,cbpamd.reviewFlag=:reviewFlag, cbpamd.previousAcceptenceFlag = null where cbpamd.cbpAmendmentId=:cbpAmendmentId";
					Query query = this.getCurrentSession().createQuery(hql);
					query.setLong("cbpAmendmentId", amdtmnt.getCbpAmendmentId());
					query.setParameter("flag", amdtmnt.getAcceptanceFlag());
					query.setParameter("inprogflag", amdtmnt.getInProgressFlag());
					query.setParameter("delta", null);
					query.setParameter("reviewFlag", amdtmnt.getreviewFlag());
					query.executeUpdate();
				} else {
					// as a fix when ammended and then later acceptIngested
					// (Review)
					//In order not to override the comments in database copy them over to the incoming cbp amendment instance 
					CBPAmendmentEntity cbpAmndDB = this.getCurrentSession().get(CBPAmendmentEntity.class, amdtmnt.getCbpAmendmentId());
					amdtmnt.setUserComments(cbpAmndDB.getUserComments());
					amdtmnt.setFdaDelta(null);
					this.getCurrentSession().evict(cbpAmndDB);
					this.getCurrentSession().saveOrUpdate(amdtmnt);
				}
			} else {
				amdtmnt.setFdaDelta(null);
				this.getCurrentSession().save(amdtmnt);
			}

			this.getCurrentSession().flush();
			amndmnts.add(amdtmnt);
		}

		for (CBPAmendmentEntity amdtmnt : amndmnts) {
			this.getCurrentSession().refresh(amdtmnt);
		}
		
		if(CollectionUtils.isNotEmpty(amndmnts)) {
			for(CBPAmendmentEntity entity : amndmnts) {
				if((entity.getAcceptanceFlag() != null &&  !entity.getAcceptanceFlag().equalsIgnoreCase("M"))
						|| (entity.getInProgressFlag() != null && entity.getInProgressFlag().equalsIgnoreCase("Y"))) {
					simpleSQLManager.deleteMixedActionDetails(entity.getCbpAmendmentId());
//					Query hqlQuery = getCurrentSession().createQuery("select AmdmntMixedActionEntity  WHERE amendmentId = :amendmentId");
//					long amendmentId = entity.getCbpAmendmentId();
//					hqlQuery.setParameter("amendmentId", new Long(amendmentId));
//					List<AmdmntMixedActionEntity> list = hqlQuery.list();
//					list.forEach(ent->{
//						getCurrentSession().delete(ent);
//					});
				}
			}
		}
		

		return amndmnts;
	}

	@Override
	public void InsertTTBVolume() {
		logger.debug("TUFA: Inserting TTB Volume");

		ProcedureCall call = this.getCurrentSession().createStoredProcedureCall("insert_ttbvolume");
		call.getOutputs();
		return;
	}

	@Override
	public void generateAnnualMarketShare(int fiscalYear) {
		ProcedureCall call;
		call = this.getCurrentSession().createStoredProcedureCall("calc_annual_market_share");
		call.registerParameter("p_fiscal_yr", Integer.class, ParameterMode.IN);
		call.getParameterRegistration("p_fiscal_yr").bindValue(fiscalYear);
		call.registerParameter("author", String.class, ParameterMode.IN);
		call.getParameterRegistration("author").bindValue(getUser());
		call.getOutputs();
		return;
	}

	@SuppressWarnings("unchecked")
    @Override
	public CompanyReallocationBeanToUpdate updateCBPIngestedLineEntryInfo(List<CBPLineEntryUpdateInfo> updateinfoList) {
	    int batchSize = 1000;
	    List<Long> cbpEntryList = updateinfoList.stream().map(CBPLineEntryUpdateInfo::getCbEntryId).collect(Collectors.toList());
	    long singleCbpEntryID = cbpEntryList.get(0);
	    long fiscalYear = updateinfoList.get(0).getAssignedYear();
	    long companyId = sqlManager.getCompnyIDForReallcoation(singleCbpEntryID);
	    String ein = sqlManager.getCompnyEINForReallcoation(singleCbpEntryID);
	    long tobaccoClassId = getCBPIngestedLineEntryInfo(singleCbpEntryID).get(0) !=null?getCBPIngestedLineEntryInfo(singleCbpEntryID).get(0).getTobaccoClassId():0;
	    Set<Long> quarterList = updateinfoList.stream().map(CBPLineEntryUpdateInfo::getAssignedQuarter).collect(Collectors.toSet());
	    List<Long>amendQtrTodelet = new ArrayList<Long>();
	    int cigarquarterLength = 0;
	    Query query = getCurrentSession().createQuery("From CBPEntryEntity c where c.cbpEntryId in (:cbpEntryList)");
	    List<Long> tempList = new ArrayList<Long>();
	    List<CBPEntryEntity>entryList = new ArrayList<CBPEntryEntity>();
	    for(int i=0;i<cbpEntryList.size();i++) {
	        tempList.add(cbpEntryList.get(i));
	        if(i%999==0 && i>0) {
	            query.setParameterList("cbpEntryList", tempList);
	            entryList.addAll(query.list());
	            tempList.removeAll(tempList);
	        }
	        if(i!=999 && i==cbpEntryList.size()-1) {
	            query.setParameterList("cbpEntryList", tempList);
                entryList.addAll(query.list());
                tempList.removeAll(tempList);
	        }
	    }
	    for(int i=0;i<entryList.size();i++) {
	        CBPEntryEntity entity = entryList.get(i);
	        CBPLineEntryUpdateInfo found = updateinfoList.stream().filter(a->a.getCbEntryId().longValue() == entity.getCbpEntryId().longValue()).findFirst().get();
	        entity.setExcludedFlag(found.getExcludeFlag());
            entity.setReallocatedFlag(found.getReallocateFlag());
            entity.setAssignedDt(found.getAssignedMonth());
            entity.setFiscalYear(String.valueOf(found.getAssignedYear()));
            entity.setFiscalQtr(String.valueOf(found.getAssignedQuarter()));
            entity.setMissingFlag(found.getMissingFlag());
            getCurrentSession().update(entity);
            if (i % batchSize == 0 && i > 0) {
                getCurrentSession().flush();
                getCurrentSession().clear();
            }
	    }
	    getCurrentSession().flush();
        getCurrentSession().clear();
        String tobaccoClass = sqlManager.getTobaccoClassName(tobaccoClassId);
	 	String secondaryDateFlag = "N"; 
    	if("entryDt".equalsIgnoreCase(getImporterCompareDetailsSelDate(tobaccoClass, fiscalYear,ein))) {
    		secondaryDateFlag = "Y";
    	}
        CompanyReallocationBeanToUpdate bean = new CompanyReallocationBeanToUpdate();
        bean.setCompanyId(companyId);
        bean.setSecondaryDateFlag(secondaryDateFlag);
        bean.setTobaccoClassId(tobaccoClassId);
        bean.setFiscalYear(updateinfoList.get(0).getAssignedYear());
        bean.setEin(ein);
        return bean;
	}
	
	@Override
	public void updateTrueUp(TrueUpEntity trueup) {
		if ("Y".equals(trueup.getSubmittedInd())) {
			trueup.setSubmittedBy(getUser());
			trueup.setSubmittedDt(new Date());
		}
		this.getCurrentSession().saveOrUpdate(trueup);
		this.getCurrentSession().flush();
	}

	@Override
	public TTBCompanyEntity getCompanyToIncldInMktSh(long fiscalYear, String ein) {

		String hql = " select cmpy from TTBCompanyEntity cmpy where cmpy.fiscalYr=:fiscalYr  "
				+ " and cmpy.einNum =:ein and cmpy.existsInTufaFlag='N' ";

		Query query = this.getCurrentSession().createQuery(hql);
		query.setLong("fiscalYr", fiscalYear);
		query.setParameter("ein", ein);

		List<TTBCompanyEntity> companies = query.list();
		TTBCompanyEntity cmpy = (companies != null && companies.size() > 0) ? companies.get(0) : null;

		if (cmpy != null && cmpy.getPermits() != null) {
			for (TTBPermitEntity permit : cmpy.getPermits()) {
				permit.setPermitStatusTypeCd("Active");
				permit.setPermitTypeCd("Manufacturer");
			}
		}

		return cmpy;
	}

	@Override
	public CBPImporterEntity getCBPCompanyToIncldInMktSh(long fiscalYear, String ein) {

		String hql = " select cmpy from CBPImporterEntity cmpy where cmpy.fiscalYr=:fiscalYr  "
				+ " and cmpy.importerEIN =:ein";

		Query query = this.getCurrentSession().createQuery(hql);
		query.setLong("fiscalYr", fiscalYear);
		query.setParameter("ein", ein);

		List<CBPImporterEntity> companies = query.list();
		CBPImporterEntity cmpy = (companies != null && companies.size() > 0) ? companies.get(0) : null;

		if (cmpy != null && cmpy.getEntry() != null) {
			for (CBPEntryEntity entry : cmpy.getEntry()) {

				entry.setEntryDt(entry.getEntryDt());
				entry.setEntrySummDt(entry.getEntrySummDt());

			}

		}

		return cmpy;
	}

	@Override
	public void updateDeltaAssociation(List<TrueUpComparisonResults> deltaAssociations) {
		// Get entries to update

		List<CBPEntryEntity> results = this.getAcctiveCBPEntryRecords(deltaAssociations);

		if (results != null && results.size() > 0) {

			List<TrueUpComparisonResults> statusUpdates = new ArrayList<TrueUpComparisonResults>();

			// Get the new
			String parentCmpyName = deltaAssociations.get(0).getLegalName();
			String parentCmpyEIN = deltaAssociations.get(0).getEin();

			// Create new CBPImporter record only once

			CBPImporterEntity parentCompany = new CBPImporterEntity();
			parentCompany.setAssociationTypeCd("IMPT");
			parentCompany.setFiscalYr(deltaAssociations.get(0).getFiscalYr());
			parentCompany.setImporterEIN(parentCmpyEIN);
			parentCompany.setImporterNm(parentCmpyName);
			parentCompany.setConsigneeEIN(parentCmpyEIN);
			parentCompany.setConsigneeExistsFlag("Y");
			parentCompany.setDefaultFlag("Y");
			this.saveEntity(parentCompany);

			// Update each entity record
			for (CBPEntryEntity result : results) {
				// Get the current CBPImporter
				CBPImporterEntity currentCompany = result.getCbpImporter();

				// Get the original CBPImporter
				CBPImporterEntity originalCompany = result.getOriginalCBPImporter();

				if (originalCompany != null) {
					// If there is an original CBP importer then update the
					// current CBP Importer
					result.setCbpImporter(parentCompany);
					// Update the CBPEntry
					this.getCurrentSession().update(result);
					this.getCurrentSession().flush();
					this.getCurrentSession().update(currentCompany);

					// If the current CPBImporter prior to the association has
					// not entry lines remove it
					if (currentCompany.getEntry().size() < 1) {
						this.getCurrentSession().delete(currentCompany);
					}
				} else {
					// If there is not original CBP importer then set the
					// current as the original
					result.setOriginalCBPImporter(currentCompany);
					result.setCbpImporter(parentCompany);

					this.getCurrentSession().update(result);
					this.getCurrentSession().flush();
				}

				// Set Status of Entry (Original EIN, Fiscal Year, Fiscal
				// Quarter, Tobacco Class)
				String tobaccoClass = result.getTobaccoClass().getTobaccoClassNm();
				String fiscalQtr = "Cigars".equals(tobaccoClass) ? "1-4" : result.getFiscalQtr();
				String fiscalYr = result.getFiscalYear();

				TrueUpComparisonResults comparisonResult = new TrueUpComparisonResults();
				comparisonResult.setEin(deltaAssociations.get(0).getOriginalEIN());
				comparisonResult.setFiscalYr(fiscalYr);
				comparisonResult.setQuarter(fiscalQtr);
				comparisonResult.setTobaccoClass(tobaccoClass);
				comparisonResult.setAllDeltaStatus("Associated");
				// wheimptn associating all the comment should be set only in
				// delta
				// from which action was taken
				// if (
				// deltaAssociations.get(0).getQuarter().equalsIgnoreCase(fiscalQtr)
				// &&
				// deltaAssociations.get(0).getTobaccoClass().equalsIgnoreCase(tobaccoClass))
				// {
				// comparisonResult.setDeltaComment(deltaAssociations.get(0).getDeltaComment());
				// } else {
				// comparisonResult.setKeepOriginalComment(true);
				// }

				comparisonResult.setPermitType("Importer".equals(deltaAssociations.get(0).getPermitType())
						|| "IMPT".equals(deltaAssociations.get(0).getPermitType()) ? "IMPT" : "MANU");

				statusUpdates.add(comparisonResult);

				if (originalCompany != null) {
					// Update the status of the Parent (Previous and New)
					TrueUpComparisonResults originalParent = new TrueUpComparisonResults();
					originalParent.setEin(currentCompany.getImporterEIN());
					originalParent.setFiscalYr(fiscalYr);
					originalParent.setQuarter(fiscalQtr);
					originalParent.setTobaccoClass(tobaccoClass);
					originalParent.setAllDeltaStatus(null);
					originalParent.setPermitType("Importer".equals(deltaAssociations.get(0).getPermitType())
							|| "IMPT".equals(deltaAssociations.get(0).getPermitType()) ? "IMPT" : "MANU");
					// originalParent.setKeepOriginalComment(true);
					statusUpdates.add(originalParent);
				}
				// Update the status of the Parent (Previous and New)
				TrueUpComparisonResults newParent = new TrueUpComparisonResults();
				newParent.setEin(parentCompany.getImporterEIN());
				newParent.setFiscalYr(fiscalYr);
				newParent.setQuarter(fiscalQtr);
				newParent.setTobaccoClass(tobaccoClass);
				newParent.setAllDeltaStatus(null);
				newParent.setPermitType("Importer".equals(deltaAssociations.get(0).getPermitType())
						|| "IMPT".equals(deltaAssociations.get(0).getPermitType()) ? "IMPT" : "MANU");
				// newParent.setKeepOriginalComment(true);
				statusUpdates.add(newParent);
			}

			for (TrueUpComparisonResults statusUpdate : statusUpdates) {
				this.updateCurrentDeltaStatus(statusUpdate);
			}

		}

	}

	@Override
	public void resetAllDeltaCompanyAssociation(List<TrueUpComparisonResults> model) {

		List<CBPEntryEntity> results = this.getAcctiveCBPEntryRecords(model);

		
		for (CBPEntryEntity result : results) {
			CBPImporterEntity parentCompany = result.getCbpImporter();
			CBPImporterEntity originalCompany = result.getOriginalCBPImporter();
			if (originalCompany != null) {
				result.setCbpImporter(originalCompany);
				result.setOriginalCBPImporter(null);

				// TODO: Batch update

				// Update the CBPEntry
				this.getCurrentSession().update(result);
				this.getCurrentSession().flush();
				// Update the CBPImporter to pull the new CBPEntries
				this.getCurrentSession().update(parentCompany);
				if (parentCompany.getEntry().size() < 1) {
					this.getCurrentSession().delete(parentCompany);
				}

				String tobaccoClass = result.getTobaccoClass().getTobaccoClassNm();
				String fiscalQtr = "Cigars".equals(tobaccoClass) ? "1-4" : result.getFiscalQtr();
				String fiscalYr = result.getFiscalYear();
				String permitType = ("Importer".equals(model.get(0).getPermitType())
						|| "IMPT".equals(model.get(0).getPermitType())) ? "IMPT" : "MANU";

				// Set Status of Entry (Original EIN, Fiscal Year, Fiscal
				// Quarter, Tobacco Class)
				TrueUpComparisonResults comparisonResult = new TrueUpComparisonResults();
				comparisonResult.setEin(model.get(0).getOriginalEIN());
				comparisonResult.setFiscalYr(fiscalYr);
				comparisonResult.setQuarter(fiscalQtr);
				comparisonResult.setTobaccoClass(tobaccoClass);
				comparisonResult.setAllDeltaStatus(null);
				// if ("single".equalsIgnoreCase(model.getDeltaActionSpan())) {
				// comparisonResult.setDeltaComment(model.get(0).getDeltaComment());
				// }
				comparisonResult.setPermitType(permitType);
				this.updateCurrentDeltaStatus(comparisonResult);

				// Update the status of the Parent (Previous)
				TrueUpComparisonResults originalParent = new TrueUpComparisonResults();
				originalParent.setEin(parentCompany.getImporterEIN());
				originalParent.setFiscalYr(fiscalYr);
				originalParent.setQuarter(fiscalQtr);
				originalParent.setTobaccoClass(tobaccoClass);
				originalParent.setAllDeltaStatus(null);
				originalParent.setPermitType(permitType);
				this.updateCurrentDeltaStatus(originalParent);

			}
		}
		
		// On reset of association  change status of  matched delta status to In-Progress (first affected by association).
		// Get amendment record from cbp_amendment table, if exists reset status to In-Progress.
		for (TrueUpComparisonResults delta : model) {
			CBPAmendmentEntity amdtmnt = new CBPAmendmentEntity();
			amdtmnt.setFiscalYr(delta.getFiscalYr());
			amdtmnt.setCbpCompanyId(delta.getCompanyId());
			amdtmnt.setQtr(delta.getQuarter());

			List<TobaccoClassEntity> tt = this.getTobaccoTypes();
			TobaccoClassEntity tbtype = tt.stream().filter(ttype -> ttype.getTobaccoClassNm().replaceAll("[-\\s]", "")
					.equalsIgnoreCase(delta.getTobaccoClass().replaceAll("[-\\s]", ""))).findFirst().get();
			amdtmnt.setTobaccoClassId(tbtype.getTobaccoClassId());

			amdtmnt = getCBPAmndmntByIdORByUniqCols(amdtmnt);

			// reset fields in cbp_amendment table to in-progress status
			if (amdtmnt != null) {
				amdtmnt.setAcceptanceFlag(null);
				amdtmnt.setInProgressFlag("Y");
				amdtmnt.setAmendedTotalTax(0D);
				amdtmnt.setAmendedTotalVolume(0D);
				this.getCurrentSession().saveOrUpdate(amdtmnt);
			}
		}
	
	}

	/**
	 * This is the base method for retrieving the CBPEntryRecord from the
	 * selection made on the All Deltas grid. If tobacco class and quarter are
	 * not set then all entries for the company and fiscal year will be
	 * returned. If the passed model is null then a null value is returned.
	 * 
	 * @return List of CBPEntryRecords for the provided information. Null if a
	 *         null is passed
	 */
	@Override
	public List<CBPEntryEntity> getAcctiveCBPEntryRecords(List<TrueUpComparisonResults> model) {

		List<CBPEntryEntity> results = new ArrayList<CBPEntryEntity>();

		for (TrueUpComparisonResults tcresults : model) {
			String ein = tcresults.getOriginalEIN();
			String fiscalYear = tcresults.getFiscalYr();
			String fiscalQuarter = tcresults.getQuarter();
			String tobaccoClassName = tcresults.getTobaccoClass();
			if(tobaccoClassName.equals("Roll Your Own")) {
				tobaccoClassName = "Roll-Your-Own";
			}

			// Select the entries that match what was selected using the
			// provided
			// values
			String hql = " SELECT DISTINCT ce FROM CBPEntryEntity ce JOIN FETCH ce.tobaccoClass tc , CBPImporterEntity ci "
					+ " WHERE NVL(ce.originalCBPImporter,ce.cbpImporter) = ci.cbpImporterId "
					+ " AND CASE WHEN ci.associationTypeCd = 'IMPT' OR (ci.associationTypeCd IS NULL AND ci.consigneeExistsFlag = 'N') THEN ci.importerEIN "
					+ " WHEN ci.associationTypeCd = 'CONS' THEN ci.consigneeEIN " + " ELSE '-1' " + " END = :ein "
					+ " AND ce.fiscalYear = :fiscalYr ";

			if (fiscalQuarter != null && !"1-4".equals(fiscalQuarter)) {
				hql += " AND ce.fiscalQtr =:fiscalQtr";
			}
			if (tobaccoClassName != null) {
				hql += " AND tc.tobaccoClassNm =:tobaccoClassName";
			}

			Query query = this.getCurrentSession().createQuery(hql);
			query.setParameter("ein", ein);
			query.setParameter("fiscalYr", fiscalYear);

			if (fiscalQuarter != null && !"1-4".equals(fiscalQuarter)) {
				query.setParameter("fiscalQtr", fiscalQuarter);
			}
			if (tobaccoClassName != null) {
				query.setParameter("tobaccoClassName", tobaccoClassName);
			}

			results.addAll(query.list());
		}

		return results;
	}

	/*
	 * I feel as there is way too much business logic for the delta statuses in
	 * this layer. Please make sure from now on keep any business logic in the
	 * business layer. Especially when it comes to the associations...
	 */
	@Override
	public ComparisonAllDeltaStatusEntity updateCompareAllDeltaStatus(List<TrueUpComparisonResults> model) {

		ComparisonAllDeltaStatusEntity compareAlldeltaStatus = new ComparisonAllDeltaStatusEntity();

		if (model.get(0).getAllDeltaStatus().equalsIgnoreCase("Associated")) {
			this.updateDeltaAssociation(model);
			for(TrueUpComparisonResults res : model) {
	               this.saveComment(res);
	        }
	           //this.saveComment(model.get(0));
		}

		if ((model.get(0).getAllDeltaStatus().equalsIgnoreCase("Reset")
				|| model.get(0).getAllDeltaStatus().equalsIgnoreCase("ResetAll"))
				&& model.get(0).getAllDeltaCurrentStatus().equalsIgnoreCase("Associated")) {
			this.resetAllDeltaCompanyAssociation(model);
			for(TrueUpComparisonResults res : model) {
                res.setAllDeltaStatus("ResestAssociated");   
                this.saveComment(res);
			}

		} else {

			for (TrueUpComparisonResults result : model) {

				if (result.getAllDeltaStatus() != null) {
					// just save the comment for all deltas for that ein and
					// fiscal year

					if ("saveAllComment".equalsIgnoreCase(result.getAllDeltaStatus()))
						this.saveAllComment(result);

					else {
						if (!result.getAllDeltaStatus().equalsIgnoreCase("Associated")) {
							this.updateCurrentDeltaStatus(result);
							compareAlldeltaStatus = this.saveComment(result);
						}
					}

				}

			}
		}

		return null;
	}
	
	@Override
	public ComparisonAllDeltaStatusEntity updateCompareAllDeltaStatusSingle(List<TrueUpComparisonResults> model) {

		ComparisonAllDeltaStatusEntity compareAlldeltaStatus = new ComparisonAllDeltaStatusEntity();

		if (model.get(0).getAllDeltaStatus().equalsIgnoreCase("Associated")) {
			this.updateDeltaAssociation(model);
			for(TrueUpComparisonResults res : model) {
	               this.saveComment(res);
	        }
	           //this.saveComment(model.get(0));
		}

		if ((model.get(0).getAllDeltaStatus().equalsIgnoreCase("Reset")
				|| model.get(0).getAllDeltaStatus().equalsIgnoreCase("ResetAll"))
				&& model.get(0).getAllDeltaCurrentStatus().equalsIgnoreCase("Associated")) {
			
			this.resetAllDeltaCompanyAssociation(model);
			for(TrueUpComparisonResults res : model) {
                res.setAllDeltaStatus("ResestAssociated");   
                this.saveComment(res);
			}

		} else {

			for (TrueUpComparisonResults result : model) {

				if (result.getAllDeltaStatus() != null) {
					// just save the comment for all deltas for that ein and
					// fiscal year

					if ("saveAllComment".equalsIgnoreCase(result.getAllDeltaStatus()))
						this.saveAllCommentSingle(result);

					else {
						if (!result.getAllDeltaStatus().equalsIgnoreCase("Associated")) {
							this.updateCurrentDeltaStatus(result);
							compareAlldeltaStatus = this.saveComment(result);
						}
					}

				}

			}
		}

		return null;
	}

	@Override
	public List<ComparisonAllDeltaStatusEntity> getallDeltaComments(long fiscalYr, String ein, String fiscalQtr,
			String tobaccoClassName, String permitType) {

		if (tobaccoClassName.equals("Chew-Snuff")) {
			tobaccoClassName = "Chew/Snuff";
		}
		else if (tobaccoClassName.equals("Pipe-Roll-Your-Own")|| tobaccoClassName.equals("Pipe-Roll Your Own")) {
			tobaccoClassName = "Pipe/Roll-Your-Own";
		}else if(tobaccoClassName.equals("Roll Your Own")) {
			tobaccoClassName = "Roll-Your-Own";
		}
		String hql ="";
		if( 0 == Integer.parseInt(fiscalQtr)) {
		 hql = " select cmpdeltastatus from ComparisonAllDeltaStatusEntity cmpdeltastatus "
				+ " where cmpdeltastatus.fiscalYr=:fiscalYr  "
				+ " and cmpdeltastatus.ein =:ein and cmpdeltastatus.tobaccoClassName =:tobaccoClassName "
				+ "and cmpdeltastatus.permitType =:permitType  ";
		}else {
			 hql = " select cmpdeltastatus from ComparisonAllDeltaStatusEntity cmpdeltastatus "
						+ " where cmpdeltastatus.fiscalYr=:fiscalYr  "
						+ " and cmpdeltastatus.ein =:ein and cmpdeltastatus.fiscalQtr =:fiscalQtr and cmpdeltastatus.tobaccoClassName =:tobaccoClassName "
						+ "and cmpdeltastatus.permitType =:permitType  ";
		}

		Query query = this.getCurrentSession().createQuery(hql);
		query.setLong("fiscalYr", fiscalYr);
		if( 0 != Integer.parseInt(fiscalQtr)) {
			query.setString("fiscalQtr", fiscalQtr);
		}
		query.setString("tobaccoClassName", tobaccoClassName);
		query.setString("ein", ein);
		query.setString("permitType", permitType);

		List<ComparisonAllDeltaStatusEntity> cmpstatus = query.list();

		return cmpstatus;
	}

	/**
	 * Used for updating the status of a single entry. Filtered by EIN, Fiscal
	 * Year, Permit Type, Tobacco Class, and Quarter.
	 * 
	 * The status that will be used is
	 * {@link TrueUpComparisonResults#getAllDeltaStatus()} of the passed model.
	 */
	private ComparisonAllDeltaStatusEntity updateCurrentDeltaStatus(TrueUpComparisonResults model) {

		String status = model.getAllDeltaStatus();
		// String stsChng = "";
		if(model.getTobaccoClass().equalsIgnoreCase("Pipe/Roll Your Own")) {
			model.setTobaccoClass("Pipe/Roll-Your-Own");
		}
		 
		if(model.getTobaccoClass().equalsIgnoreCase("Roll Your Own")) {
			model.setTobaccoClass("Roll-Your-Own");
		}

		if (status != null && (status.equalsIgnoreCase("reset") || status.equalsIgnoreCase("resetAll")))
			status = "Review";

		String permitType = "";

		if (model.getPermitType().equalsIgnoreCase("Importer") || model.getPermitType().equalsIgnoreCase("IMPT"))
			permitType = "IMPT";
		else
			permitType = "MANU";
		// Get record from ALL_DELTA_COMPARISON_STS based on
		// year,ein,quarter,tobacco_class_nm and update with new status
		// else insert new record in ALL_DELTA_COMPARISON_STS with
		// year,ein,quarter,tobacco_class_nm,status

		boolean isCompoundTobaccoType = false;

		if (model.getTobaccoClass().equalsIgnoreCase("Chew/Snuff")
				|| model.getTobaccoClass().equalsIgnoreCase("Pipe/Roll-Your-Own"))
			isCompoundTobaccoType = true;

		String selectSql = " FROM ComparisonAllDeltaStatusEntity deltaSts WHERE " + " deltaSts.ein=:ein "
				+ " AND deltaSts.fiscalYr=:fiscalYr" + " AND deltaSts.fiscalQtr=:qtr "
				+ " AND deltaSts.tobaccoClassName=:ttnm" + " AND deltaSts.permitType=:permitType ";

		Query selectQuery = this.getCurrentSession().createQuery(selectSql);
		selectQuery.setParameter("ein", model.getEin());
		selectQuery.setParameter("fiscalYr", model.getFiscalYr());
		selectQuery.setParameter("qtr", model.getQuarter());
		selectQuery.setParameter("ttnm", model.getTobaccoClass());
		selectQuery.setParameter("permitType", permitType);

		ComparisonAllDeltaStatusEntity deltaStatus = (ComparisonAllDeltaStatusEntity) selectQuery.uniqueResult();

		if (deltaStatus != null) {

			deltaStatus.setDeltaStatus("Review".equalsIgnoreCase(status) ? null : status);
			deltaStatus.setDelta(model.getTotalDeltaTax());
			deltaStatus.setFileType(model.getFileType());
			deltaStatus.setDeltaType(model.getDataType());
			deltaStatus.setPreviousStatus(null);

			if (!model.getKeepOriginalComment()) {
				// deltaStatus.setUserComments(model.getUserComments());
			}

			this.populateCompoundTobaccoTypeTaxVols(deltaStatus, model);

			this.getCurrentSession().saveOrUpdate(deltaStatus);
			this.getCurrentSession().flush();
			this.getCurrentSession().clear();

			return deltaStatus;

		} else if (!"Review".equalsIgnoreCase(status)) {

			ComparisonAllDeltaStatusEntity newDeltaStatus = new ComparisonAllDeltaStatusEntity();

			newDeltaStatus.setEin(model.getEin());
			newDeltaStatus.setFiscalYr(model.getFiscalYr());
			newDeltaStatus.setFiscalQtr(model.getQuarter());
			newDeltaStatus.setTobaccoClassName(model.getTobaccoClass());
			newDeltaStatus.setPermitType(permitType);
			newDeltaStatus.setDeltaStatus(status);
			newDeltaStatus.setDelta(model.getTotalDeltaTax());
			newDeltaStatus.setDeltaType(model.getDataType());
			newDeltaStatus.setFileType(model.getFileType());
			// newDeltaStatus.setDeltaComment(model.getDeltaComment());

			this.populateCompoundTobaccoTypeTaxVols(newDeltaStatus, model);

			this.getCurrentSession().save(newDeltaStatus);
			this.getCurrentSession().flush();
			this.getCurrentSession().clear();

			return newDeltaStatus;
		}

		return null;

	}

	private ComparisonAllDeltaStatusEntity populateCompoundTobaccoTypeTaxVols(ComparisonAllDeltaStatusEntity delta,
			TrueUpComparisonResults model) {

		Map<String, Double[]> ttTaxVols = model.getTobaccoSubTypeTxVol();

		if (ttTaxVols == null) {
			delta.setTobaccoSubType1(null);
			delta.setTobaccoSubType2(null);
			delta.setTobaccoSubType1Amndtx(null);
			delta.setTobaccoSubType1Amndtx(null);
			delta.setTobaccoSubType1Pounds(null);
			delta.setTobaccoSubType2Pounds(null);
			return delta;
		}

		List<String> ttNames = new ArrayList<>(ttTaxVols.keySet());

		String tt1 = ttNames.get(0);
		String tt2 = ttNames.size() > 1 ? ttNames.get(1) : null;

		delta.setTobaccoSubType1(tt1);
		Double[] taxvol = ttTaxVols.get(tt1);
		if (taxvol != null) {
			delta.setTobaccoSubType1Amndtx(taxvol[0]);
			delta.setTobaccoSubType1Pounds(taxvol.length > 1 ? taxvol[1] : null);
		}

		if (tt2 != null) {
			delta.setTobaccoSubType2(tt2);
			taxvol = ttTaxVols.get(tt2);
			if (taxvol != null) {
				delta.setTobaccoSubType2Amndtx(taxvol[0]);
				delta.setTobaccoSubType2Pounds(taxvol.length > 1 ? taxvol[1] : null);
			}
		}

		return delta;

	}

	/**
	 * Used for updating the status of ALL entries. ALL is filtered by EIN and
	 * Fiscal Year. Permit Type, Tobacco Class, and Quarter are ignored.
	 * 
	 * The status that will be used is
	 * {@link TrueUpComparisonResults#getAllDeltaStatus()} of the passed model.
	 */
	private void updateAllDeltaStatus(TrueUpComparisonResults model) {

		String status = model.getAllDeltaStatus();
		String permitType = "";

		if (status != null && (status.equalsIgnoreCase("reset") || status.equalsIgnoreCase("resetAll")))
			status = null;

		if (model.getPermitType().equalsIgnoreCase("Importer") || model.getPermitType().equalsIgnoreCase("IMPT"))
			permitType = "IMPT";
		else
			permitType = "MANU";

		String selectSql = "";

		// If Status is Market Share Ready update the Status of TUFA Only or
		// Ingested Only Deltas. For Ingested Only do not update the Compound
		// Tobacco Classes.
		String deltaType = model.getDataType();
		boolean isMarketShareReady = "MSReady".equalsIgnoreCase(status);

		String allDeltasSearchCriteria = "";

		if (isMarketShareReady) {

			allDeltasSearchCriteria = " AND NOT ( ALLDELTA.Tobacco_Class_NM IN ('Chew/Snuff','Pipe/Roll-Your-Own') "
					+ " AND ALLDELTA.source IN ('INGESTED')) AND status in ('In Progress','Review')";

			selectSql = " FROM ComparisonAllDeltaStatusEntity deltaSts WHERE " + " deltaSts.ein=:ein "
					+ " AND deltaSts.fiscalYr=:fiscalYr  AND (status IN ('In Progress') OR status IS NULL) "
					+ "AND NOT ( deltaSts.tobaccoClassName IN ('Chew/Snuff','Pipe/Roll-Your-Own') AND deltaSts.deltaType IN ('INGESTED')) ";
		} else if (status != null) {

			allDeltasSearchCriteria = "  AND status in ('In Progress','Review') ";

			selectSql = " FROM ComparisonAllDeltaStatusEntity deltaSts WHERE " + " deltaSts.ein=:ein "
					+ " AND deltaSts.fiscalYr=:fiscalYr AND (status  IN ('In Progress') OR status IS NULL) ";
		} else {

			allDeltasSearchCriteria = "  AND status not in ('Review') ";

			selectSql = " FROM ComparisonAllDeltaStatusEntity deltaSts WHERE " + " deltaSts.ein=:ein "
					+ " AND deltaSts.fiscalYr=:fiscalYr AND status is not null ";
		}

		Query selectQuery = this.getCurrentSession().createQuery(selectSql);
		selectQuery.setParameter("ein", model.getOriginalEIN() != null ? model.getOriginalEIN() : model.getEin());
		selectQuery.setParameter("fiscalYr", model.getFiscalYr());

		List<ComparisonAllDeltaStatusEntity> deltaStatuses = selectQuery.list();

		// The deltaStatuses list will never be null
		// If the status is null then only reset the status that exist
		if (status == null) {
			for (ComparisonAllDeltaStatusEntity deltaStatus : deltaStatuses) {
				if ("Associated".equalsIgnoreCase(deltaStatus.getDeltaStatus())) {
					// this.resetAllDeltaCompanyAssociation(deltaStatus);
				} else {

					// For Ingested Compound Tobacco Type Deltas set Tobacco Sub
					// Type fields as null
					deltaStatus.setTobaccoSubType1(null);
					deltaStatus.setTobaccoSubType2(null);
					deltaStatus.setTobaccoSubType1Amndtx(null);
					deltaStatus.setTobaccoSubType1Amndtx(null);
					deltaStatus.setDeltaStatus(status);
					this.getCurrentSession().saveOrUpdate(deltaStatus);
				}
			}
		} else {
			// Get all possible combinations
			List<ComparisonAllDeltaStatusEntity> combinations = this.sqlManager.getAllDeltasForStatusChange(model,
					allDeltasSearchCriteria);
			for (ComparisonAllDeltaStatusEntity combination : combinations) {
				boolean exist = false;
				// Check if the combination already exists
				for (ComparisonAllDeltaStatusEntity deltaStatus : deltaStatuses) {
					// If it does then update the existing record
					if (deltaStatus.getTobaccoClassName().equals(combination.getTobaccoClassName())
							&& deltaStatus.getFiscalQtr().equals(combination.getFiscalQtr())
							&& deltaStatus.getPermitType().equals(combination.getPermitType())) {
						exist = true;
						// deltaStatus.setDeltaComment(model.getDeltaComment());

						deltaStatus.setDeltaStatus(status);
						this.getCurrentSession().saveOrUpdate(deltaStatus);

					}
				}
				// If it doesn't then insert the new combination
				if (!exist) {
					combination.setDeltaStatus(status);
					this.getCurrentSession().saveOrUpdate(combination);
				}
			}
		}

	}

	@Override
	@Transactional
	public List<CBPFileColumnTemplateEntity> loadCBPFileTemplates() {
		String hql = "from CBPFileColumnTemplateEntity cbpTmp";
		Query sql = getCurrentSession().createQuery(hql);
		List<CBPFileColumnTemplateEntity> templates = sql.list();
		return templates;
	}

	/*
	 * private void
	 * resetAllDeltaCompanyAssociation(ComparisonAllDeltaStatusEntity status) {
	 * TrueUpComparisonResults deltaAssoc = new TrueUpComparisonResults();
	 * deltaAssoc.setEin(status.getEin());
	 * deltaAssoc.setFiscalYr(status.getFiscalYr());
	 * deltaAssoc.setQuarter(status.getFiscalQtr());
	 * deltaAssoc.setTobaccoClass(status.getTobaccoClassName());
	 * deltaAssoc.setPermitType(status.getPermitType());
	 * deltaAssoc.setDeltaActionSpan("single");
	 * deltaAssoc.setDeltaComment(status.getDeltaComment());
	 * this.resetAllDeltaCompanyAssociation(deltaAssoc); }
	 */

	// private void resetAllDeltaCompanyAssociation(TrueUpComparisonResults
	// model) {

	// TrueUpAllDeltaAssociation deltaAssoc = new TrueUpAllDeltaAssociation();
	// deltaAssoc.setCurrentCmpyEIN(model.getOriginalEIN());
	// deltaAssoc.setFiscalYr(model.getFiscalYr());
	// deltaAssoc.setPermitType(model.getPermitType());
	// deltaAssoc.setFiscalQtr(model.getQuarter());
	// deltaAssoc.setTobaccoClass(model.getTobaccoClass());
	// deltaAssoc.setDeltaComment(model.getDeltaComment());
	// deltaAssoc.setDeltaActionSpan(model.getDeltaActionSpan());

	// this.resetAllDeltaCompanyAssociation(deltaAssoc);

	// }

	@Override
	public Boolean checkIfAssociationExists(String ein, long fiscalYear, String tobaccoClass, long qtr) {
		StringBuilder selectSql = new StringBuilder(" Select entry FROM CBPImporterEntity importer, CBPEntryEntity entry "
		 + " WHERE importer.cbpImporterId = entry.cbpImporter "
		 + " AND CASE WHEN importer.associationTypeCd = 'IMPT' OR (importer.associationTypeCd IS NULL AND importer.consigneeExistsFlag = 'N') THEN importer.importerEIN "
		 + " WHEN importer.associationTypeCd = 'CONS' THEN importer.consigneeEIN  ELSE '-1' END = :ein "
		 + " AND importer.fiscalYr = :fiscalYear "
		 + " AND entry.tobaccoClass.tobaccoClassNm = :tobaccoClass "
		 + " AND entry.originalCBPImporter IS NOT NULL ");
		if(qtr > 0 && qtr!=5) {
			selectSql.append(" AND entry.fiscalQtr  = :qtr ");
		}

		Query query = this.getCurrentSession().createQuery(selectSql.toString());
		query.setString("ein", ein);
		query.setLong("fiscalYear", fiscalYear);
		if(qtr > 0 && qtr!=5) {
			query.setLong("qtr", qtr);
		}
		query.setParameter("tobaccoClass", "Roll Your Own".equals(tobaccoClass) ? "Roll-Your-Own" : tobaccoClass);
		List<CBPEntryEntity> result = query.list();
		return result.size() > 0 ? true : false;
	}

	public List<ComparisonAllDeltaStatusEntity> getDeltaSts(boolean isSaveAll, TrueUpComparisonResults model) {

		// gets data from the db instead of new comment
		Mapper mapper = mapperWrapper.getMapper();
		String permitType = null;
		String selectSql = "FROM ComparisonAllDeltaStatusEntity deltaSts WHERE deltaSts.ein=:ein "
				+ " AND deltaSts.fiscalYr=:fiscalYr";
		if (!isSaveAll) {
			selectSql = selectSql
					+ " AND deltaSts.fiscalQtr=:qtr AND deltaSts.tobaccoClassName=:ttnm  AND deltaSts.permitType=:permitType ";

			if (model.getPermitType().equalsIgnoreCase("Importer") || model.getPermitType().equalsIgnoreCase("IMPT")) {
				permitType = "IMPT";
			} else {
				permitType = "MANU";
			}
		}

		Query query = this.getCurrentSession().createQuery(selectSql);

		if (model.getAllDeltaStatus() != null && (model.getAllDeltaStatus().equalsIgnoreCase("Associated") || model.getAllDeltaStatus().equalsIgnoreCase("ResestAssociated")))
			query.setParameter("ein", model.getOriginalEIN());
		else
			query.setParameter("ein", model.getEin());

		query.setParameter("fiscalYr", model.getFiscalYr());

		// if save comment is true
		if (!isSaveAll) {
			query.setParameter("ttnm", model.getTobaccoClass());
			query.setParameter("permitType", permitType);
			query.setParameter("qtr", model.getQuarter());
		}

		List<ComparisonAllDeltaStatusEntity> deltaStatuses = query.list();

		return deltaStatuses;
	}

	public ComparisonAllDeltaStatusEntity saveComment(TrueUpComparisonResults model) {

		if (model.getUserComments() != null) {
			boolean isSaveAll = false;
			List<ComparisonAllDeltaStatusCommentEntity> newCommentsEntity = this.maptoCommentEntity(model);
			List<ComparisonAllDeltaStatusEntity> cmpSts = new ArrayList<>();
			List<ComparisonAllDeltaStatusEntity> deltaStatuses = this.getDeltaSts(isSaveAll, model);

			List<ComparisonAllDeltaStatusCommentEntity> mergedComments = null;

			for (ComparisonAllDeltaStatusEntity stsmnt : deltaStatuses) {

				ComparisonAllDeltaStatusEntity dbCmpSts = this.getCmpStsDeltaByIdORByUniqCols(stsmnt, model);

				if (dbCmpSts.getUserComments().size() == 0) {
					dbCmpSts = stsmnt;
					mergedComments = newCommentsEntity;
					for (ComparisonAllDeltaStatusCommentEntity comm : mergedComments)
						comm.setAuthor(securityContextUtil.getSessionUser());

				} else
					mergedComments = this.mergeCmpAllDeltaStsComments(newCommentsEntity, dbCmpSts.getUserComments());

				if (dbCmpSts.getUserComments() == null)
					dbCmpSts.setUserComments(new ArrayList<ComparisonAllDeltaStatusCommentEntity>());

				dbCmpSts.setUserComments(mergedComments);

				for (ComparisonAllDeltaStatusCommentEntity comment : mergedComments) {
					if (comment.getDeltaStatuses() == null)
						comment.setDeltaStatuses(new ArrayList<ComparisonAllDeltaStatusEntity>());
					comment.getDeltaStatuses().add(dbCmpSts);
				}

				this.getCurrentSession().saveOrUpdate(dbCmpSts);
				this.getCurrentSession().flush();
				this.getCurrentSession().refresh(dbCmpSts);

				cmpSts.add(dbCmpSts);
			}

			return cmpSts.get(0);
		} else
			return null;
	}

	public List<ComparisonAllDeltaStatusCommentEntity> maptoCommentEntity(TrueUpComparisonResults model) {

		Mapper mapper = mapperWrapper.getMapper();
		List<ComparisonAllDeltaStatusCommentEntity> newCommentsEntity = new ArrayList<>();
		List<ComparisonAllDeltaComments> newComments = model.getUserComments();

		for (ComparisonAllDeltaComments newcomm : newComments) {
			ComparisonAllDeltaStatusCommentEntity commentent = new ComparisonAllDeltaStatusCommentEntity();
			mapper.map(newcomm, commentent);
			newCommentsEntity.add(commentent);
		}
		return newCommentsEntity;
	}

	public ComparisonAllDeltaStatusEntity saveAllComment(TrueUpComparisonResults model) {

		boolean isSaveAll = true;

		List<ComparisonAllDeltaStatusCommentEntity> newCommentsEntity = new ArrayList<ComparisonAllDeltaStatusCommentEntity>();

		if (model.getUserComments() != null) {
			newCommentsEntity = this.maptoCommentEntity(model);
		}

		List<ComparisonAllDeltaStatusEntity> cmpSts = new ArrayList<>();
		List<ComparisonAllDeltaStatusEntity> deltaStatuses = this.getDeltaSts(isSaveAll, model);
		List<ComparisonAllDeltaStatusEntity> combinations = this.sqlManager.getAllDeltasForStatusChange(model, null);
		ComparisonAllDeltaStatusEntity compareStatus = new ComparisonAllDeltaStatusEntity();

		for (ComparisonAllDeltaStatusEntity combination : combinations) {
			boolean exist = false;
			// Check if the combination already exists
			for (ComparisonAllDeltaStatusEntity deltaStatus : deltaStatuses) {
				// If it does then update the existing record
				if (deltaStatus.getTobaccoClassName().equals(combination.getTobaccoClassName())
						&& deltaStatus.getFiscalQtr().equals(combination.getFiscalQtr())
						&& deltaStatus.getPermitType().equals(combination.getPermitType())) {
					exist = true;
					if ("Review".equalsIgnoreCase(deltaStatus.getDeltaStatus())
							|| null == deltaStatus.getDeltaStatus()) {

						deltaStatus.setDeltaStatus("In Progress");

					}

					this.getCurrentSession().saveOrUpdate(deltaStatus);
				}
			}
			// If it doesn't then insert the new combination
			if (!exist) {
				if ("Review".equalsIgnoreCase(combination.getDeltaStatus())) {
					combination.setDeltaStatus("In Progress");
					this.getCurrentSession().saveOrUpdate(combination);
				}
			}

		}

		if (model.getUserComments() != null) {
			compareStatus = this.saveOneCommentforMultipleStatus(newCommentsEntity, model,false);
		}

		return null;

	}
	
	public ComparisonAllDeltaStatusEntity saveAllCommentSingle(TrueUpComparisonResults model) {

		boolean isSaveAll = true;

		List<ComparisonAllDeltaStatusCommentEntity> newCommentsEntity = new ArrayList<ComparisonAllDeltaStatusCommentEntity>();

		if (model.getUserComments() != null) {
			newCommentsEntity = this.maptoCommentEntity(model);
		}

		List<ComparisonAllDeltaStatusEntity> cmpSts = new ArrayList<>();
		List<ComparisonAllDeltaStatusEntity> deltaStatuses = this.getDeltaSts(isSaveAll, model);
		List<ComparisonAllDeltaStatusEntity> combinations = this.sqlManager.getAllDeltasForStatusChangeSingle(model, null);
		ComparisonAllDeltaStatusEntity compareStatus = new ComparisonAllDeltaStatusEntity();

		for (ComparisonAllDeltaStatusEntity combination : combinations) {
			boolean exist = false;
			// Check if the combination already exists
			for (ComparisonAllDeltaStatusEntity deltaStatus : deltaStatuses) {
				// If it does then update the existing record
				if (deltaStatus.getTobaccoClassName().equals(combination.getTobaccoClassName())
						&& deltaStatus.getFiscalQtr().equals(combination.getFiscalQtr())
						&& deltaStatus.getPermitType().equals(combination.getPermitType())) {
					exist = true;
					if ("Review".equalsIgnoreCase(deltaStatus.getDeltaStatus())
							|| null == deltaStatus.getDeltaStatus()) {

						deltaStatus.setDeltaStatus("In Progress");

					}

					this.getCurrentSession().saveOrUpdate(deltaStatus);
				}
			}
			// If it doesn't then insert the new combination
			if (!exist) {
				if ("Review".equalsIgnoreCase(combination.getDeltaStatus())) {
					combination.setDeltaStatus("In Progress");
					this.getCurrentSession().saveOrUpdate(combination);
				}
			}

		}

		if (model.getUserComments() != null) {
			compareStatus = this.saveOneCommentforMultipleStatus(newCommentsEntity, model,false);
		}

		return null;

	}

	public ComparisonAllDeltaStatusEntity saveOneCommentforMultipleStatus(
			List<ComparisonAllDeltaStatusCommentEntity> newCommentsEntity, TrueUpComparisonResults model, boolean isSelectedQtrs) {
		List<ComparisonAllDeltaStatusEntity> cmpSts = new ArrayList<>();
		List<ComparisonAllDeltaStatusEntity> deltaStatuses = null;
		if(isSelectedQtrs) {
			deltaStatuses = this.getDeltaStatusSingle(model);
		}else {
			deltaStatuses = this.getDeltaSts(true, model);
		}
		List<ComparisonAllDeltaStatusCommentEntity> mergedComments = null;

		for (ComparisonAllDeltaStatusEntity stsmnt : deltaStatuses) {

			// Comments coming from the DB
			ComparisonAllDeltaStatusEntity dbCmpSts = this.getCmpStsDeltaByIdORByUniqCols(stsmnt, model);

			if (dbCmpSts.getUserComments() == null) {
				dbCmpSts = stsmnt;
				mergedComments = newCommentsEntity;
				for (ComparisonAllDeltaStatusCommentEntity comm : mergedComments)
					comm.setAuthor(securityContextUtil.getSessionUser());

			} else
				mergedComments = this.mergeCmpAllDeltaStsComments(newCommentsEntity, dbCmpSts.getUserComments());

			if (dbCmpSts.getUserComments() == null)
				dbCmpSts.setUserComments(new ArrayList<ComparisonAllDeltaStatusCommentEntity>());

			dbCmpSts.setUserComments(mergedComments);

			for (ComparisonAllDeltaStatusCommentEntity comment : mergedComments) {
				if (comment.getDeltaStatuses() == null)
					comment.setDeltaStatuses(new ArrayList<ComparisonAllDeltaStatusEntity>());
				comment.getDeltaStatuses().add(dbCmpSts);
			}

			this.getCurrentSession().saveOrUpdate(dbCmpSts);
			this.getCurrentSession().flush();
			this.getCurrentSession().refresh(dbCmpSts);

			cmpSts.add(dbCmpSts);
		}

		return cmpSts.size() > 0?cmpSts.get(0):new ComparisonAllDeltaStatusEntity();
	}

	@Override
	public List<RawIngested> getRawIngested(long fiscalYr, String ein, String qtr, String tobaccoClassNm, String type,
			boolean showOriginal, boolean getTax) {
		if (tobaccoClassNm != null)
			tobaccoClassNm = tobaccoClassNm.replace(" ", "/");

		String hql = "from RawIngested raw WHERE ";
		if (showOriginal) {
			hql += " (raw.ein =:ein OR raw.originalEin =:ein) ";
		} else {
			hql += " raw.ein =:ein AND raw.originalEin IS NULL ";
		}
		if ("MANU".equalsIgnoreCase(type) && getTax) {
			hql += " AND raw.tpbd IS NOT NULL ";
		} else if ("MANU".equalsIgnoreCase(type)) {
			hql += " AND raw.tpbd IS NULL ";
		}
		hql += "AND raw.fiscalYr =:fiscalYr AND raw.companyType =:type AND (raw.tobaccoClassNm LIKE '%' || :tobaccoClassNm || '%' OR :tobaccoClassNm LIKE '%' || raw.tobaccoClassNm || '%')";

		if (!"Cigars".equals(tobaccoClassNm))
			hql += "AND raw.quarter =:qtr";

		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("ein", ein);
		query.setParameter("fiscalYr", Long.toString(fiscalYr));
		query.setParameter("type", type);
		query.setParameter("tobaccoClassNm", "%" + tobaccoClassNm + "%");
		if (!"Cigars".equals(tobaccoClassNm))
			query.setParameter("qtr", qtr);

		List<RawIngested> results = query.list();
		return results;
	}

	@Override
	public List<CBPLineItemEntity> getUnmatchedDetailsExport(long fiscalYr) {

		String hql = "SELECT cbpDetails "
				+ "FROM CBPLineItemEntity  cbpDetails JOIN  CBPMatchedTaxesEntity cbpMatched ON "
				+ "cbpMatched.entrySummaryNumber = cbpDetails.entrySummaryNumber AND cbpMatched.fiscalYr = cbpDetails.fiscalYr "
				+ "where cbpMatched.fiscalYr = :fiscalYr AND " + "cbpMatched.entryNumberMatch = 'D'";

		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("fiscalYr", fiscalYr);

		List<CBPLineItemEntity> cbpLineItemEntityList = query.list();
		return cbpLineItemEntityList;
	}

	@Override
	public List<CBPSummaryFileEntity> getUnmatchedSummaryExport(long fiscalYr) {
		String hql = "SELECT cbpSummary "
				+ "FROM CBPSummaryFileEntity  cbpSummary  JOIN  CBPMatchedTaxesEntity cbpMatched ON "
				+ "cbpMatched.entrySummaryNumber = cbpSummary.entrySummaryNumber  AND cbpMatched.fiscalYr = cbpSummary.fiscalYr"
				+ " where cbpMatched.fiscalYr = :fiscalYr AND " + "cbpMatched.entryNumberMatch = 'S'";

		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("fiscalYr", fiscalYr);

		List<CBPSummaryFileEntity> cbpSummaryFileEntityList = query.list();
		return cbpSummaryFileEntityList;
	}

	@Override
	public List<CBPEntryEntity> getCBPIngestedLineEntryInfo(long cbpEntryId) {
		// TODO Auto-generated method stub
		List<CBPEntryEntity> results = new ArrayList<CBPEntryEntity>();
		String hql = " FROM CBPEntryEntity e WHERE e.cbpEntryId =:cbpEntryId";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("cbpEntryId", cbpEntryId);
		List<CBPEntryEntity> CBPEntryEntityList = query.list();
		return CBPEntryEntityList;
	}
	
	
	@Override
	public  List<IngestionWorkflowContextEntity> saveorUpdateIngestionWorkflowContext(IngestionWorkflowContextEntity workflowcontextmodel)
	{
		List<IngestionWorkflowContextEntity> results = new ArrayList<IngestionWorkflowContextEntity>();

		if (workflowcontextmodel != null) {

			IngestionWorkflowContextEntity contextEntity = (IngestionWorkflowContextEntity) getCurrentSession()
					.createQuery("FROM IngestionWorkflowContextEntity e WHERE e.fiscalYr =:fiscalYr")
					.setLong("fiscalYr", workflowcontextmodel.getFiscalYr()).uniqueResult();

			if (contextEntity == null) {
				getCurrentSession().save(workflowcontextmodel);
				this.getCurrentSession().refresh(workflowcontextmodel);
				results.add(workflowcontextmodel);
			} else {
				contextEntity.setContext(workflowcontextmodel.getContext());
				this.getCurrentSession().update(contextEntity);
				this.getCurrentSession().flush();
				this.getCurrentSession().refresh(contextEntity);
				results.add(contextEntity);
			}

		}
		return results;
			
	}	
	
	@Override
	public  List<IngestionWorkflowContextEntity> getIngestionWorkflowContext(long fiscalYr)
	{
		List<IngestionWorkflowContextEntity> results = new ArrayList<IngestionWorkflowContextEntity>();
		String hql = " FROM IngestionWorkflowContextEntity e WHERE e.fiscalYr =:fiscalYr";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("fiscalYr", fiscalYr);
		IngestionWorkflowContextEntity wfEntity= (IngestionWorkflowContextEntity) query.uniqueResult();	
		if(wfEntity!=null)
			results.add(wfEntity);
		
		return results;
	
	}
	
	 @Override
	 public  List<IngestionWorkflowContextEntity> getIngestionContexts()
	{
		String hql = " FROM IngestionWorkflowContextEntity e ";
		Query query = this.getCurrentSession().createQuery(hql);
		List<IngestionWorkflowContextEntity> contexts =  query.list();	
		return contexts;
	}
	 
	 @Override
	 public CompanyReallocationBeanToUpdate saveOrUpdateDateSelection(CBPDateConfigEntity cbpDateConfigEntity) throws ParseException {
		 this.getCurrentSession().saveOrUpdate(cbpDateConfigEntity);
		 List<CBPLineEntryUpdateInfo> cbpLineEntryUpdateInfoList= null;
		//if the selection is entry date - the records with date outside fiscal year will be excluded
	    cbpLineEntryUpdateInfoList = this.sqlManager.getCBPLineEntry(cbpDateConfigEntity, false);
	    CompanyReallocationBeanToUpdate bean = null;
	    if(null != cbpLineEntryUpdateInfoList && cbpLineEntryUpdateInfoList.size() > 0){
	    	bean = updateCBPIngestedLineEntryInfo(cbpLineEntryUpdateInfoList);
	    }
	    //update the assigned_dt based on the entry_date
	    cbpLineEntryUpdateInfoList = this.sqlManager.getCBPLineEntry(cbpDateConfigEntity, true);
	    if(null != cbpLineEntryUpdateInfoList && cbpLineEntryUpdateInfoList.size() > 0){
	    	List<Long> quarterList = cbpLineEntryUpdateInfoList.stream().map(CBPLineEntryUpdateInfo::getAssignedQuarter).collect(Collectors.toList());
	    	bean = updateCBPIngestedLineEntryInfo(cbpLineEntryUpdateInfoList);
	    }
	    return bean;
	 }
	 
	 @Override
	 public String getImporterCompareDetailsSelDate(String tobaccoclassNm, long fiscalYr,String ein){
		 //entrySummDt entryDt
		String hql = " FROM CBPDateConfigEntity WHERE  fiscalYr=:fiscalYr AND tobaccoClassNm=:tobaccoclassNm AND ein=:ein";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("fiscalYr", fiscalYr);
		query.setParameter("tobaccoclassNm", tobaccoclassNm);
		query.setParameter("ein", ein);
		CBPDateConfigEntity cbpDateConfigEntity =  (CBPDateConfigEntity) query.uniqueResult();	
		String selDate = null;
		if(cbpDateConfigEntity != null){
			selDate = cbpDateConfigEntity.getDateType();
		}else{
			selDate = "entrySummDt";
		}
		return selDate;
	}
		private void populateSaveCommentsAllDeltas(TrueUpComparisonResults model, List<Integer> excludedQtrList) {
			
			boolean isSaveAll = true;
			String tobaccoClassName = "";

			List<ComparisonAllDeltaStatusCommentEntity> newCommentsEntity = new ArrayList<ComparisonAllDeltaStatusCommentEntity>();

			if (model.getUserComments() != null) {
				newCommentsEntity = this.maptoCommentEntity(model);
			}
			if ("Chew-Snuff".equalsIgnoreCase(model.getTobaccoClass())) {
				tobaccoClassName = "Chew/Snuff";
			}else if ("Pipe-Roll-Your-Own".equals(model.getTobaccoClass())||"Pipe-Roll Your Own".equals(model.getTobaccoClass())) {
				tobaccoClassName = "Pipe/Roll-Your-Own";
			}else {
				tobaccoClassName = model.getTobaccoClass();
			}
			model.setTobaccoClass(tobaccoClassName);
			List<ComparisonAllDeltaStatusEntity> cmpSts = new ArrayList<>();
			List<ComparisonAllDeltaStatusEntity> deltaStatuses = this.getDeltaStatusSingle(model);
			List<ComparisonAllDeltaStatusEntity> combinations = this.sqlManager.getAllDeltasFrmSingleView(model);
			ComparisonAllDeltaStatusEntity compareStatus = new ComparisonAllDeltaStatusEntity();
			int count = 0;
			for (ComparisonAllDeltaStatusEntity combination : combinations) {
				boolean exist = false;
				// Check if the combination already exists
				for (ComparisonAllDeltaStatusEntity deltaStatus : deltaStatuses) {
					// If it does then update the existing record
					if (deltaStatus.getTobaccoClassName().equals(combination.getTobaccoClassName())
							&& deltaStatus.getFiscalQtr().equals(combination.getFiscalQtr())
							&& deltaStatus.getPermitType().equals(combination.getPermitType())) {
						exist = true;
						if ("Review".equalsIgnoreCase(deltaStatus.getDeltaStatus())
								|| null == deltaStatus.getDeltaStatus()) {

							deltaStatus.setDeltaStatus("In Progress");

						}
						count++;
						this.getCurrentSession().saveOrUpdate(deltaStatus);
					}
				}
				// If it doesn't then insert the new combination
				if (!exist) {
					if(excludedQtrList.size()>0 && excludedQtrList.contains(combination.getFiscalQtr())) {
						continue;
					}
					if ("Review".equalsIgnoreCase(combination.getDeltaStatus())) {
						combination.setDeltaStatus("In Progress");
						count++;
						this.getCurrentSession().saveOrUpdate(combination);
					}
				}

			}

			if (model.getUserComments() != null) {
				compareStatus = this.saveOneCommentforMultipleStatus(newCommentsEntity, model, true);
			}

		}
		public List<ComparisonAllDeltaStatusEntity> getDeltaStatusSingle(TrueUpComparisonResults model) {

			// gets data from the db instead of new comment
			String permitType = null;
			String commentQtr ="";
			String tobaccoClassName = "";
			if("CIGARS".equalsIgnoreCase(model.getTobaccoClass())) {
				commentQtr = "1-4";
			}else {
				commentQtr = model.getUserComments().get(0).getCommentQtrs().replace(";", "','");
			}
			
		
			String selectSql = "FROM ComparisonAllDeltaStatusEntity deltaSts WHERE deltaSts.ein=:ein "
					+ " AND deltaSts.fiscalYr=:fiscalYr"
					+ " AND deltaSts.tobaccoClassName=:ttnm  AND deltaSts.permitType=:permitType AND deltaSts.fiscalQtr IN(\'"+ commentQtr +"')";

			if (model.getPermitType().equalsIgnoreCase("Importer") || model.getPermitType().equalsIgnoreCase("IMPT")) {
				permitType = "IMPT";
			} else {
				permitType = "MANU";
			}

			Query query = this.getCurrentSession().createQuery(selectSql);

			/*if (model.getAllDeltaStatus().equalsIgnoreCase("Associated") || model.getAllDeltaStatus().equalsIgnoreCase("ResestAssociated"))
				query.setParameter("ein", model.getOriginalEIN());
			else*/
			query.setParameter("ein", model.getEin());
			query.setParameter("fiscalYr", model.getFiscalYr());
			query.setParameter("ttnm", model.getTobaccoClass());
			query.setParameter("permitType", permitType);

			List<ComparisonAllDeltaStatusEntity> deltaStatuses = query.list();

			return deltaStatuses;
		}
		private List<Integer> getExcludedQuarterList(long fiscalYr, long companyId,String commentQtrs) {
			List<Integer> quarterExcluded = new ArrayList<Integer>();
			List<Integer> allowedQtrs = new ArrayList<Integer>();
			int qtrNum = 0;
			
			
			List<ExcludePermitDetails> excludedQuarterList = simpleSQLManager.getPermitExcludeList(String.valueOf(fiscalYr),companyId, 3);
			for (ExcludePermitDetails excl : excludedQuarterList) {
				int qtr = 0;
				if (excl.getQuarter() == 1 && excl.getYear() - 1 == fiscalYr) {
					qtr = 4;
				}
				if (excl.getYear() == fiscalYr) {
					qtr = excl.getQuarter() - 1;
				}
				if(qtr>0)quarterExcluded.add(qtr);
			}
			
			return quarterExcluded;
		}
		
		@Override
		public List<CBPAmendmentEntity> saveCBPCompareComment(List<CBPAmendmentEntity> amendments, String ein) {
			logger.debug("TUFA: Saving CBP Comparison comment...");

			List<CBPAmendmentEntity> amndmnts = new ArrayList<>();
			int qtr= 0;
			List<CBPQtrCommentEntity> mergedComments = null;
			List<CBPQtrCommentEntity> newComments = amendments.get(0).getUserComments();
			CBPQtrCommentEntity newCommEntity = newComments.get(0);
			String commentQtrs = newCommEntity.getCommentQtrs();
			List<Integer> excludedQtrList = new ArrayList<Integer>();;
			
			long companyid = companyMgmtEntityManager.getCompanyByEIN(ein)!= null?companyMgmtEntityManager.getCompanyByEIN(ein).getCompanyId():0;
			//this means it is an ingest only company
			if(companyid != 0) {
				excludedQtrList = getExcludedQuarterList(Integer.parseInt(amendments.get(0).getFiscalYr()), companyid, commentQtrs);
			for (CBPAmendmentEntity amdtmnt : amendments) {
				qtr = Integer.parseInt(amdtmnt.getQtr());
				
				amdtmnt.setCbpCompanyId(companyid);
				CBPAmendmentEntity amdt = this.getCBPAmndmntByIdORByUniqCols(amdtmnt);
				
				if (amdt == null) {
					if(excludedQtrList.size() > 0 && excludedQtrList.contains(qtr) ) {
						continue;
					}
					amdt = amdtmnt;
					mergedComments = newComments;
					for (CBPQtrCommentEntity comm : mergedComments) {
						comm.setAuthor(securityContextUtil.getSessionUser());
					}
					// Set In Progress status for the amendment as there is no
					// action taken for the delta yet(no entry in the amendment
					// table), only a comment is being added.
					amdt.setInProgressFlag("Y");
//					amdt.setEdgeStatusFlag("Y");

				} else {
					mergedComments = this.mergeCBPDetailsComments(newComments, amdt.getUserComments());
					/*for (CBPQtrCommentEntity comtemp : newComments) {
							comtemp.setAuthor(securityContextUtil.getSessionUser());
							amdt.getUserComments().add(comtemp);
					}
					mergedComments = amdt.getUserComments();
*/
					if(null == amdt.getAcceptanceFlag() && ("N").equalsIgnoreCase(amdt.getInProgressFlag()) 
							&& ("Y").equalsIgnoreCase(amdt.getEdgeStatusFlag())) {
						amdt.setInProgressFlag("Y"); 
					}
				}
				if (amdt.getUserComments() == null)
					amdt.setUserComments(new ArrayList<CBPQtrCommentEntity>());

				amdt.setUserComments(mergedComments);

				for (CBPQtrCommentEntity comment : mergedComments) {
					if (comment.getCbpAmendments() == null)
						comment.setCbpAmendments(new ArrayList<CBPAmendmentEntity>());
					comment.getCbpAmendments().add(amdt);
				}

				this.getCurrentSession().saveOrUpdate(amdt);
				this.getCurrentSession().flush();
				this.getCurrentSession().refresh(amdt);

				amndmnts.add(amdt);
			}
			}
			logger.debug("TUFA: Completed saving of CBP Comparison comment");
				TrueUpComparisonResults model = new TrueUpComparisonResults();
				List<ComparisonAllDeltaComments> userCommentslist = new ArrayList<ComparisonAllDeltaComments>();
				ComparisonAllDeltaComments  usercomment = new ComparisonAllDeltaComments();
				usercomment.setCommentQtrs(newCommEntity.getCommentQtrs());
				usercomment.setUserComment(newCommEntity.getUserComment());
				if(amndmnts.size() > 0) {
						for(CBPQtrCommentEntity matchcomm:amndmnts.get(0).getUserComments()) {
							if(newCommEntity.getUserComment().equals(matchcomm.getUserComment())) {
								usercomment.setMatchCommSeq(matchcomm.getCommentSeq());
								break;
							}
						}
				}
				model.setFiscalYr(amendments.get(0).getFiscalYr());
				model.setTobaccoClass(amendments.get(0).getTobaccoClass().getTobaccoClassNm());
				model.setEin(ein);
				model.setPermitType("IMPT");
				userCommentslist.add(usercomment);
				model.setUserComments(userCommentslist);
				populateSaveCommentsAllDeltas(model,excludedQtrList) ;

			return amndmnts;
		}
		
		public void deleteCompareCommentDetailOnly(long commSeqCbp,long commSeqAllDelta, String permitType) {

			if(commSeqCbp > 0) {
				String sql = "";
				String hql = "";
				String docsql = "";
				if("IMPT".equalsIgnoreCase(permitType)) {
					sql = "delete from TU_CBP_DETAIL_QTR_COMMENTS amendcmmt where amendcmmt.comment_seq=:commentSeq ";
					hql = "delete from TU_CBP_QTR_COMMENT comments where comments.comment_seq=:commentSeq";
					docsql = "delete from TU_COMPARISON_COMMENT_DOC docs where docs.CBP_COMMENT_SEQ=:commentSeq";
				}else {
					sql = "delete from TU_TTB_DETAIL_QTR_COMMENTS amendcmmt where amendcmmt.comment_seq=:commentSeq ";
					hql = "delete from TU_TTB_QTR_COMMENT comments where comments.comment_seq=:commentSeq";
					docsql = "delete from TU_COMPARISON_COMMENT_DOC docs where docs.TTB_COMMENT_SEQ=:commentSeq";
				}
				Query deleteQry = this.getCurrentSession().createSQLQuery(sql);
				deleteQry.setLong("commentSeq", commSeqCbp);
				deleteQry.executeUpdate();
	
				Query hqldeleteQry = this.getCurrentSession().createSQLQuery(hql);
				deleteQry.setLong("commentSeq", commSeqCbp);
				deleteQry.executeUpdate();
				
				Query docsqldeleteQry = this.getCurrentSession().createSQLQuery(docsql);
				docsqldeleteQry.setLong("commentSeq", commSeqCbp);
				docsqldeleteQry.executeUpdate();
			}
			if(commSeqAllDelta > 0) {
				String sql = "delete from TU_ALL_DELTA_JOIN_COMMENT alldelta where alldelta.comment_seq=:commentSeq ";
				Query deleteQry = this.getCurrentSession().createSQLQuery(sql);
				deleteQry.setLong("commentSeq", commSeqAllDelta);
				deleteQry.executeUpdate();
	
				String hql = "delete from TU_ALL_DELTA_COMMENT comments where comments.comment_seq=:commentSeq";
				Query hqldeleteQry = this.getCurrentSession().createSQLQuery(hql);
				deleteQry.setLong("commentSeq", commSeqAllDelta);
				deleteQry.executeUpdate();
				
				String dochql = "delete from TU_COMPARISON_COMMENT_DOC docs where docs.ALLDELTA_COMMENT_SEQ=:commentSeq";
				Query dochqldeleteQry = this.getCurrentSession().createSQLQuery(dochql);
				dochqldeleteQry.setLong("commentSeq", commSeqAllDelta);
				dochqldeleteQry.executeUpdate();
			}
			this.getCurrentSession().flush();
		}
		
		public void updateCompareCommentDetailOnly(long commSeqCbp,long commSeqAllDelta, String usercomment, String permitType) {

			if(commSeqCbp > 0) {
				String updateHQL = "";
				if("IMPT".equalsIgnoreCase(permitType)) {
					updateHQL = "update TU_CBP_QTR_COMMENT comments set comments.USER_COMMENT=:usercomment where comments.comment_seq=:commentSeq";
				}else {
					updateHQL = "update TU_TTB_QTR_COMMENT comments set comments.USER_COMMENT=:usercomment where comments.comment_seq=:commentSeq";

				}
				Query updateQuery = this.getCurrentSession().createSQLQuery(updateHQL);
				updateQuery.setLong("commentSeq", commSeqCbp);
				updateQuery.setString("usercomment", usercomment);
				updateQuery.executeUpdate();
				this.getCurrentSession().flush();
			}
			if(commSeqAllDelta > 0) {
				String updateHQLalldelta = "update TU_ALL_DELTA_COMMENT comments set comments.USER_COMMENT=:usercomment where comments.comment_seq=:commentSeq";
				Query updateQueryalldelta = this.getCurrentSession().createSQLQuery(updateHQLalldelta);
				updateQueryalldelta.setLong("commentSeq", commSeqAllDelta);
				updateQueryalldelta.setString("usercomment", usercomment);
				updateQueryalldelta.executeUpdate();
			}
			this.getCurrentSession().flush();
		}
		
		public void deleteDocById(long docId){
			getCurrentSession().delete(getCommentAttachment(docId));
		}
		@Override
		public ComparisonCommentAttachmentEntity getCommentAttachment(long docId) {
			return this.getCurrentSession().get(ComparisonCommentAttachmentEntity.class, docId);
		}
		
		
		public void addComparisonCommentAttachment(ComparisonCommentAttachmentEntity attach){
			getCurrentSession().saveOrUpdate(attach);
		}
	    
	    @Override
		public Blob getBlob(byte[] doc) throws SQLException {
			return Hibernate.getLobCreator(this.getCurrentSession()).createBlob(doc);
		}

		@Override
		public byte[] getByteArray(Blob blob) throws SQLException {
			int offSet = 0;
			byte [] bytes  = {};
			if(blob != null) {
			    long pos = offSet+1;
				long blobLen = blob.length()-offSet;
				
				bytes = blob.getBytes(pos, (int)blobLen);
		         
				blob.free();
			}
			return bytes;
		}
		@Override
		public byte[] getComparisonCommentAttachmentAsByteArray(long docId) throws SQLException {

			Query query = getCurrentSession().createQuery(
					"SELECT comparisonCommentAttachmentEntity.reportPdf FROM ComparisonCommentAttachmentEntity comparisonCommentAttachmentEntity WHERE comparisonCommentAttachmentEntity.documentId = :documentId");
			query.setParameter("documentId", docId);

			List reportLst = query.list();
			Blob blob =  (Blob) reportLst.get(0);
		
			byte [] bytes  = this.getByteArray(blob);
			if(blob != null) {
				blob.free();
			}
			
			return bytes;
		}
		@Override
		public List<ComparisonCommentAttachmentEntity> findCommentDocbyId(long cbpCommentSeq, long ttbCommentSeq, long allDeltaCommentSeq) {
			return simpleSQLManager.findCompCommentDocbyId(cbpCommentSeq, ttbCommentSeq, allDeltaCommentSeq);
		}
		public long getDocCount(long cbpCommentSeq, long ttbCommentSeq, long allDeltaCommentSeq) {
			return simpleSQLManager.getDocCount(cbpCommentSeq, ttbCommentSeq, allDeltaCommentSeq);
		}

}

package gov.hhs.fda.ctp.persistence;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import gov.hhs.fda.ctp.persistence.model.RoleEntity;
import gov.hhs.fda.ctp.persistence.model.TufaEntity;
import gov.hhs.fda.ctp.persistence.model.UserEntity;

/**
 * 
 * UserAdminEntityManager contains CRUD operations to be performed on a User.
 * @author sarsharma
 *
 */
@Repository("userAdminEntityManager")
public class UserAdminEntityManagerImpl extends BaseEntityManagerImpl implements UserAdminEntityManager {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(UserAdminEntityManagerImpl.class);
	/**
	 * Get user by email. The method should only be called from Spring Security 
	 * implementation of UserDetailsService.
	 *
	 * @param email the email
	 * @return the user
	 */
	@Override
	public UserEntity getUser(String email){
		
		String hql = "select user FROM UserEntity user WHERE lower(user.email) = ?1";
		Query query = getCurrentSession().createQuery(hql).setParameter(1, email.toLowerCase());
		List results = query.getResultList();
		return  (results!=null && !results.isEmpty())?(UserEntity)results.get(0):null;
		
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.UserAdminEntityManager#getRoleByName(java.lang.String)
	 */
	@Override
	public RoleEntity getRoleByName(String role) {
		 
		String hql = "from RoleEntity role where role.name = :rolename";
		 Query query= this.getCurrentSession().createQuery(hql);
		 
		 query.setParameter("rolename", role);
		 
		 @SuppressWarnings("rawtypes")
		List results = query.list();
		 
		 return (RoleEntity)((results!=null && !results.isEmpty())?results.get(0):results); 
	}
	
	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.UserAdminEntityManager#getRoles
	 */
	@Override
	public Set<RoleEntity> getRoles() {
		String hql = "FROM RoleEntity role";
		Query query = this.getCurrentSession().createQuery(hql);
		List<RoleEntity> results = query.list();
		Set<RoleEntity> roles = new TreeSet<RoleEntity>();
		for(RoleEntity result : results) {
			roles.add(result);
		}
		return roles;
	}

	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.BaseEntityManagerImpl#saveEntity(gov.hhs.fda.ctp.persistence.model.TufaEntity)
	 */
	@Override
	public void saveEntity(TufaEntity entity){
		getCurrentSession().save(entity);
	}
	
	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.UserAdminEntityManager#mergeEntity(gov.hhs.fda.ctp.persistence.model.TufaEntity)
	 */
	@Override
	public void mergeEntity(TufaEntity entity){
		getCurrentSession().saveOrUpdate(entity);
	}
	
	/* (non-Javadoc)
	 * @see gov.hhs.fda.ctp.persistence.UserAdminEntityManager#getUsers()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<UserEntity> getUsers() {
		String hql = "FROM UserEntity user";
		Query query = getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public UserEntity getUser(long id) {
		logger.debug(" - Entering getUser By Id - ");
		String hql = "FROM UserEntity userEntity where userEntity.userId = :id";
		Query query = getCurrentSession().createQuery(hql);
		query.setParameter("id", id);
		@SuppressWarnings("rawtypes")
		List results = query.list();
		logger.debug(" - Exiting readById - ");
		return (UserEntity) ((results != null && !results.isEmpty()) ? results.get(0) : null);
	}

	@Override
	public UserEntity update(UserEntity user) {
		logger.debug(" - Entering update - ");
		getCurrentSession().update(user);
		logger.debug(" - Exiting  update - ");
		return user;
	}
}

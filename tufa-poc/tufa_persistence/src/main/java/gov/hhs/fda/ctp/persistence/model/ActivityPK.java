package gov.hhs.fda.ctp.persistence.model;

import java.io.Serializable;

/**
 * The Class ActivityPK.
 */
public class ActivityPK implements Serializable {
    
    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The permit id. */
	protected long permitId;
    
    /** The period id. */
    protected long periodId;

    /**
     * Instantiates a new activity PK.
     */
    public ActivityPK() {
    	// Empty Constructor
    }

    /**
     * Instantiates a new activity PK.
     *
     * @param permit the permit
     * @param period the period
     */
    public ActivityPK(long permit, long period) {
        this.permitId = permit;
        this.periodId = period;
    }
    // equals, hashCode
    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((permitId == 0) ? 0 : Long.valueOf(permitId).hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActivityPK activityPK = (ActivityPK) obj;
        if (this.permitId != activityPK.permitId) {
            return false;
        }
        return true;
    }
}

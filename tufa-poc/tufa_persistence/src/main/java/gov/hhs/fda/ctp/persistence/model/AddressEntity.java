/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class AddressEntity.
 *
 * @author tgunter
 */
@Entity
@Table(name = "tu_address")
public class AddressEntity extends BaseEntity {
    
    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
    
    /** The company. */
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ID", nullable=false, insertable = false, updatable = false)
    private CompanyEntity company;

    /** The address type cd. */
    @Id
    @Column(name = "ADDRESS_TYPE_CD", nullable = false, length = 4)
    private String addressTypeCd;

    /** The street address. */
    @Column(name = "STREET_ADDRESS", length = 50)
    private String streetAddress;

    /** The suite. */
    @Column(name = "SUITE", length = 20)
    private String suite;
    
    /** The city. */
    @Column(name = "CITY", length = 40)
    private String city;
    
    /** The state. */
    @Column(name = "STATE", length = 2)
    private String state;
    
    /** The postal cd. */
    @Column(name = "POSTAL_CD", length = 10)
    private String postalCd;

    /** The fax num. */
    @Column(name = "FAX_NUM", length = 10)
    private Long faxNum;

    /** The country cd. */
    @Column(name = "COUNTRY_CD", length = 2)
    private String countryCd;
    
    @Column(name = "HASH_TOTAL")
    private Integer hashTotal;
    
    /** The created by. */
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;

    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;

    /** attention */
    @Column(name = "ATTENTION", length = 20)
    private String attention;

    /** The province. */
    @Column(name = "PROVINCE", length = 20)
    private String province;
    
    /**
     * Gets the address type cd.
     *
     * @return the addressTypeCd
     */
    public String getAddressTypeCd() {
        return addressTypeCd;
    }

    /**
     * Sets the address type cd.
     *
     * @param addressTypeCd the addressTypeCd to set
     */
    public void setAddressTypeCd(String addressTypeCd) {
        this.addressTypeCd = addressTypeCd;
    }

    /**
     * Gets the street address.
     *
     * @return the streetAddress
     */
    public String getStreetAddress() {
        return streetAddress;
    }

    /**
     * Sets the street address.
     *
     * @param streetAddress the streetAddress to set
     */
    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    /**
     * Gets the suite.
     *
     * @return the suite
     */
    public String getSuite() {
        return suite;
    }

    /**
     * Sets the suite.
     *
     * @param suite the suite to set
     */
    public void setSuite(String suite) {
        this.suite = suite;
    }

    /**
     * Gets the city.
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the city.
     *
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets the state.
     *
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the state.
     *
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Gets the fax num.
     *
     * @return the faxNum
     */
    public Long getFaxNum() {
        return faxNum;
    }

    /**
     * Sets the fax num.
     *
     * @param faxNum the faxNum to set
     */
    public void setFaxNum(Long faxNum) {
        this.faxNum = faxNum;
    }

    /**
     * Gets the country cd.
     *
     * @return the countryCd
     */
    public String getCountryCd() {
        return countryCd;
    }

    /**
     * Sets the country cd.
     *
     * @param countryCd the countryCd to set
     */
    public void setCountryCd(String countryCd) {
        this.countryCd = countryCd;
    }

    /**
     * Gets the created by.
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the created by.
     *
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Gets the created dt.
     *
     * @return the createdDt
     */
    public Date getCreatedDt() {
        return createdDt;
    }

    /**
     * Sets the created dt.
     *
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    /**
     * Gets the modified by.
     *
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * Sets the modified by.
     *
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * Gets the modified dt.
     *
     * @return the modifiedDt
     */
    public Date getModifiedDt() {
        return modifiedDt;
    }

    /**
     * Sets the modified dt.
     *
     * @param modifiedDt the modifiedDt to set
     */
    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }

    /**
     * Gets the company.
     *
     * @return the company
     */
    public CompanyEntity getCompany() {
        return company;
    }

    /**
     * Sets the company.
     *
     * @param company the company to set
     */
    public void setCompany(CompanyEntity company) {
        this.company = company;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((addressTypeCd == null) ? 0 : addressTypeCd.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if(obj != null && getClass() == obj.getClass()) {
            if(this == obj || ((AddressEntity)obj).getAddressTypeCd().equals(this.addressTypeCd)) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("Address [type=").append(this.addressTypeCd).append("]").append("[address=").append(this.streetAddress).append("]");
        return builder.toString();
    }

	/**
	 * Gets the postal cd.
	 *
	 * @return the postalCd
	 */
	public String getPostalCd() {
		return postalCd;
	}

	/**
	 * Sets the postal cd.
	 *
	 * @param postalCd the postalCd to set
	 */
	public void setPostalCd(String postalCd) {
		this.postalCd = postalCd;
	}

	public String getProvince(){
		return province;
	}
	
	public void setProvince(String province){
		this.province = province;
	}
	
	public String getAttention(){
		return attention;
	}
	
	public void setAttention(String attention){
		this.attention = attention;
	}

	/**
	 * @return the hashTotal
	 */
	public Integer getHashTotal() {
		return hashTotal;
	}

	/**
	 * @param hashTotal the hashTotal to set
	 */
	public void setHashTotal(Integer hashTotal) {
		this.hashTotal = hashTotal;
	}
    
}

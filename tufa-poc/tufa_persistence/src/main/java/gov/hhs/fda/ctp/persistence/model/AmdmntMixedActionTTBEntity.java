package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TU_TTB_AMNDMNT_MIXEDACT_DTL")
public class AmdmntMixedActionTTBEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MIXED_DTLS_RECORD",nullable = false)
    private Long mixedDetailsRecord;

    @Column(name="AMENDMENT_ID", nullable=false)
    private Long amendmentId;

    @Column(name="MONTH")
    private String month;
    
    @Column(name="MIXED_IND")
    private String mixedIndicator;
    
    @Column(name="PREVIOUS_MONTH")
    private String previousMonth;
    
    @Column(name="PREVIOUS_MIXED_IND")
    private String previousInd;
	

	public Long getMixedDetailsRecord() {
		return mixedDetailsRecord;
	}

	public void setMixedDetailsRecord(Long mixedDetailsRecord) {
		this.mixedDetailsRecord = mixedDetailsRecord;
	}

	public Long getAmendmentId() {
		return amendmentId;
	}

	public void setAmendmentId(Long amendmentId) {
		this.amendmentId = amendmentId;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getMixedIndicator() {
		return mixedIndicator;
	}

	public void setMixedIndicator(String mixedIndicator) {
		this.mixedIndicator = mixedIndicator;
	}

	public String getPreviousMonth() {
		return previousMonth;
	}

	public void setPreviousMonth(String previousMonth) {
		this.previousMonth = previousMonth;
	}

	public String getPreviousInd() {
		return previousInd;
	}

	public void setPreviousInd(String previousInd) {
		this.previousInd = previousInd;
	}
}

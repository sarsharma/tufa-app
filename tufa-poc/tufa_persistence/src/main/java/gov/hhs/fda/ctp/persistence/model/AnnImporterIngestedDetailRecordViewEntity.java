package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name="ann_importer_detail_vw")
public class AnnImporterIngestedDetailRecordViewEntity extends BaseEntity {
	@Id
	@Column(name="company_id")
	private Long companyId;
	@Column(name="taxes")
	private Double taxes;
	@Id
	@Column(name="fiscal_qtr")
	private Long fiscalQtr;
	@Id
	@Column(name="fiscal_yr")
	private Long fiscalYr;
	@Column(name="tobacco_class_nm")
	private String tobaccoClassNm;
	@Id
	@Column(name="tobacco_class_id")
	private Long tobaccoClassId;
	@Id
	@Column(name="month")
	private String month;
	/**
	 * @return the companyId
	 */
	public Long getCompanyId() {
		return companyId;
	}
	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	/**
	 * @return the taxes
	 */
	public Double getTaxes() {
		return taxes;
	}
	/**
	 * @param taxes the taxes to set
	 */
	public void setTaxes(Double taxes) {
		this.taxes = taxes;
	}
	/**
	 * @return the fiscalQtr
	 */
	public Long getFiscalQtr() {
		return fiscalQtr;
	}
	/**
	 * @param fiscalQtr the fiscalQtr to set
	 */
	public void setFiscalQtr(Long fiscalQtr) {
		this.fiscalQtr = fiscalQtr;
	}
	/**
	 * @return the fiscalYr
	 */
	public Long getFiscalYr() {
		return fiscalYr;
	}
	/**
	 * @param fiscalYr the fiscalYr to set
	 */
	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}
	/**
	 * @return the tobaccoClassNm
	 */
	public String getTobaccoClassNm() {
		return tobaccoClassNm;
	}
	/**
	 * @param tobaccoClassNm the tobaccoClassNm to set
	 */
	public void setTobaccoClassNm(String tobaccoClassNm) {
		this.tobaccoClassNm = tobaccoClassNm;
	}
	/**
	 * @return the tobaccoClassId
	 */
	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}
	/**
	 * @param tobaccoClassId the tobaccoClassId to set
	 */
	public void setTobaccoClassId(Long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}
	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}
	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}
	
	
}

package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.DynamicInsert;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The Class AssessmentEntity.
 *
 * @author rnasina
 */
@DynamicInsert
@Entity
@Table(name="TU_ASSESSMENT")
public class AssessmentEntity extends BaseEntity{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The assessment id. */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ASSESSMENT_ID", unique = true, nullable=false) 
    private long assessmentId;
    
	/** The assessment yr. */
	@Column(name = "ASSESSMENT_YR", nullable=false, columnDefinition="Decimal(4,0)") 
    private int assessmentYr;
    
	/** The assessment qtr. */
	@Column(name = "ASSESSMENT_QTR", nullable = false, columnDefinition="Decimal(1,0)")
    private int assessmentQtr;
    
	/** The submitted ind. */
	@Column(name = "SUBMITTED_IND", columnDefinition="CHAR(1)", nullable=false)
    private String submittedInd;
	
	/** The cigarsubmitted ind. */
	@Column(name = "CIGAR_IND", columnDefinition="CHAR(4)", nullable=false)
    private String cigarsubmittedInd;
    
	/** The assessment type. */
	@Column(name = "ASSESSMENT_TYPE", columnDefinition="CHAR(4)")
    private String assessmentType;

	@JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "assessment")
    private Set<AssessmentVersionEntity> versions;
    
	/** supporting documents. */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "assessmentEntity")
    private List<SupportingDocumentEntity> supportDoc;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TRUEUP_ID", insertable = true, updatable = false)
    private TrueUpEntity trueUpEntity;

	/** The created by. */
    @Column(name = "CREATED_BY", length = 50, updatable=false)
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT", insertable = false, updatable = false)
    private Date createdDt;
    
    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")    
    private Date modifiedDt;

    
    /**
     * Sets the assessment id.
     *
     * @param asId the new assessment id
     */
    public void setAssessmentId(long asId){
    	this.assessmentId = asId;
    }
    
    /**
     * Gets the assessment id.
     *
     * @return the assessment id
     */
    public long getAssessmentId(){
    	return this.assessmentId;
    }
    
    /**
     * Sets the assessment yr.
     *
     * @param asYr the new assessment yr
     */
    public void setAssessmentYr(int asYr){
    	this.assessmentYr = asYr;
    }
    
    /**
     * Gets the assessment yr.
     *
     * @return the assessment yr
     */
    public int getAssessmentYr(){
    	return this.assessmentYr;
    }
    
    /**
     * Sets the assessment qtr.
     *
     * @param asQtr the new assessment qtr
     */
    public void setAssessmentQtr(int asQtr){
    	this.assessmentQtr = asQtr;
    }
    
    /**
     * Gets the assessment qtr.
     *
     * @return the assessment qtr
     */
    public int getAssessmentQtr(){
    	return this.assessmentQtr;
    }
    
	/**
     * Sets the submitted ind.
     *
     * @param subInd the new submitted ind
     */
    public void setSubmittedInd(String subInd){
    	this.submittedInd = subInd;
    }
    
    /**
     * Gets the submitted ind.
     *
     * @return the submitted ind
     */
    public String getSubmittedInd(){
    	return this.submittedInd;
    }
    
    
    /**
     * Sets the cigarsubmitted ind.
     *
     * @param subInd the new submitted ind
     */
    public void setCigarSubmittedInd(String cigarsubInd){
    	this.cigarsubmittedInd = cigarsubInd;
    }
    
    /**
     * Gets the cigarsubmitted ind.
     *
     * @return the submitted ind
     */
    public String getCigarSubmittedInd(){
    	return this.cigarsubmittedInd;
    }
    
    
    
    
    
    /**
     * Sets the assessment type.
     *
     * @param asType the new assessment type
     */
    public void setAssessmentType(String asType){
    	this.assessmentType = asType;
    }
    
    /**
     * Gets the assessment type.
     *
     * @return the assessment type
     */
    public String getAssessmentType(){
    	return this.assessmentType;
    }

    public List<SupportingDocumentEntity> getSupportDoc() {
		return supportDoc;
	}

	public void setSupportDoc(List<SupportingDocumentEntity> supportDoc) {
		this.supportDoc = supportDoc;
	}

	/**
	 * @return the versions
	 */
	public Set<AssessmentVersionEntity> getVersions() {
		return versions;
	}

	/**
	 * @param versions the versions to set
	 */
	public void setVersions(Set<AssessmentVersionEntity> versions) {
		this.versions = versions;
	}

	/**
	 * @return the trueUpEntity
	 */
	public TrueUpEntity getTrueUpEntity() {
		return trueUpEntity;
	}

	/**
	 * @param trueUpEntity the trueUpEntity to set
	 */
	public void setTrueUpEntity(TrueUpEntity trueUpEntity) {
		this.trueUpEntity = trueUpEntity;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}

	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}

	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

}

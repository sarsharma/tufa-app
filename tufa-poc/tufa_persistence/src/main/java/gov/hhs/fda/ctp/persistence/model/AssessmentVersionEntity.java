/**
 *
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author tgunter
 *
 */
@Entity
@Table(name="TU_ASSESSMENT_VERSION")
public class AssessmentVersionEntity extends BaseEntity {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "ASSESSMENT_ID", nullable = false)
    private Long assessmentId;

	@Id
    @Column(name = "VERSION_NUM", nullable = false)
    private Long versionNum;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ASSESSMENT_ID", insertable = false, updatable = false)
    private AssessmentEntity assessment;
    
    @Column(name = "CIGAR_QTR")
    private Long cigarQtr;

    @JsonIgnore
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "assessmentVersionEntity", orphanRemoval=true)
    private List<MarketShareViewEntity> marketShares;

	/** The created by. */
    @Column(name = "CREATED_BY", length = 50, updatable=false)
    private String createdBy;

    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="CREATED_DT",  updatable = false)
    private Date createdDt;

    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;

    /** The modified dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;

	/** The created by. */
    @Column(name = "MARKETSHARE_TYPE", length = 6)
    private String marketshareType;    


	/**
	 * @return the assessmentId
	 */
	public Long getAssessmentId() {
		return this.assessmentId;
	}

	/**
	 * @param assessmentId the assessmentId to set
	 */
	public void setAssessmentId(Long assessmentId) {
		this.assessmentId = assessmentId;
	}

	/**
	 * @return the versionNum
	 */
	public Long getVersionNum() {
		return this.versionNum;
	}

	/**
	 * @param cigar_qtr the cigar_qtr to set
	 */
	public void setCigarQtr(Long cigarQtr) {
		this.cigarQtr = cigarQtr;
	}

	/**
	 * @return the cigar_qtr
	 */
	public Long getCigarQtr() {
		return this.cigarQtr;
	}

	/**
	 * @param versionNum the versionNum to set
	 */
	public void setVersionNum(Long versionNum) {
		this.versionNum = versionNum;
	}
	
	
	
	/**
	 * @return the assessment
	 */
	public AssessmentEntity getAssessment() {
		return assessment;
	}

	/**
	 * @param assessment the assessment to set
	 */
	public void setAssessment(AssessmentEntity assessment) {
		this.assessment = assessment;
	}

	/**
	 * @return the marketShares
	 */
	public List<MarketShareViewEntity> getMarketShares() {
		return marketShares;
	}

	/**
	 * @param marketShares the marketShares to set
	 */
	public void setMarketShares(List<MarketShareViewEntity> marketShares) {
		this.marketShares = marketShares;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}

	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}

	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}


	/**
	 * Gets the marketshare type.
	 *
	 * @return the marketshare type
	 */
	public String getMarketshareType() {
		return marketshareType;
	}

	/**
	 * Sets the marketshare type.
	 *
	 * @param marketshareType the new marketshare type
	 */
	public void setMarketshareType(String marketshareType) {
		this.marketshareType = marketshareType;
	}

}

package gov.hhs.fda.ctp.persistence.model;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class AttachmentsEntity.
 *
 * @author rnasina
 */
@Entity
@Table(name="TU_DOCUMENT")
public class AttachmentsEntity extends BaseEntity{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The document id. */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DOCUMENT_ID", unique = true, nullable=false) 
    private long documentId;
    
	/** The permit id. */
	@Column(name = "PERMIT_ID", nullable=false) 
    private long permitId;
    
	/** The period id. */
	@Column(name = "PERIOD_ID", nullable = false)
    private long periodId;
    
	/** The doc file nm. */
	@Column(name = "DOC_FILE_NM", columnDefinition="CHAR(4)", nullable=false)
    private String docFileNm;
    
	/** The doc status cd. */
	@Column(name = "DOC_STATUS_CD", columnDefinition="CHAR(4)")
    private String docStatusCd;
    
    /** The report pdf. */
    @Lob @Basic(fetch = FetchType.LAZY)
	@Column(name="REPORT_PDF")
    private Blob reportPdf;
    
    /** The doc desc. */
    @Column(name = "DOC_DESC", length = 240)
    private String docDesc;
    
    /** The form types. */
    @Column(name = "FORM_TYPES", length = 100)
    private String formTypes;    

    /** The permit period entity. */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns( {
        @JoinColumn(name = "PERMIT_ID", referencedColumnName="PERMIT_ID", insertable = false, updatable = false),
        @JoinColumn(name = "PERIOD_ID", referencedColumnName="PERIOD_ID", insertable = false, updatable = false)
    })
    private PermitPeriodEntity permitPeriodEntity;
    
    /** The created by. */
    @Column(name = "CREATED_BY", length=50)
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;
    
    /** The modified by. */
    @Column(name = "MODIFIED_BY", length=50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    
    /**
     * Gets the report pdf.
     *
     * @return the report pdf
     */
    public Blob getReportPdf(){
    	return reportPdf;
    }
    
    /**
     * Sets the report pdf.
     *
     * @param bs the new report pdf
     */
    public void setReportPdf(Blob bs){
    	this.reportPdf = bs;
    }
	       
    /**
     * Gets the document id.
     *
     * @return the document id
     */
    public long getDocumentId() {
        return documentId;
    }
    
    /**
     * Sets the document id.
     *
     * @param docId the new document id
     */
    public void setDocumentId(long docId) {
        this.documentId = docId;
    }
       
    /**
     * Gets the permit id.
     *
     * @return the permit id
     */
    public long getPermitId() {
        return permitId;
    }
    
    /**
     * Sets the permit id.
     *
     * @param permitId the new permit id
     */
    public void setPermitId(long permitId) {
        this.permitId = permitId;
    }
    
    /**
     * Gets the period id.
     *
     * @return the period id
     */
    public long getPeriodId() {
        return periodId;
    }
    
    /**
     * Sets the period id.
     *
     * @param period the new period id
     */
    public void setPeriodId(long period) {
        this.periodId = period;
    }
    
    /**
     * Gets the doc file nm.
     *
     * @return the doc file nm
     */
    public String getDocFileNm() {
        return docFileNm;
    }
    
    /**
     * Sets the doc file nm.
     *
     * @param docFileNm the new doc file nm
     */
    public void setDocFileNm(String docFileNm) {
        this.docFileNm = docFileNm;
    }

    /**
     * Gets the doc status cd.
     *
     * @return the doc status cd
     */
    public String getDocStatusCd() {
        return docStatusCd;
    }
    
    /**
     * Sets the doc status cd.
     *
     * @param docStatusCd the new doc status cd
     */
    public void setDocStatusCd(String docStatusCd) {
        this.docStatusCd = docStatusCd;
    }
    
    /**
     * Gets the doc desc.
     *
     * @return the doc desc
     */
    public String getDocDesc() {
        return docDesc;
    }
    
    /**
     * Sets the doc desc.
     *
     * @param docDesc the new doc desc
     */
    public void setDocDesc(String docDesc) {
        this.docDesc = docDesc;
    }
    
    /**
     * Gets the permit period entity.
     *
     * @return the permit period entity
     */
    public PermitPeriodEntity getPermitPeriodEntity() {
        return permitPeriodEntity;
    }

    /**
     * Sets the permit period entity.
     *
     * @param permitPeriodEntity the new permit period entity
     */
    public void setPermitPeriodEntity(PermitPeriodEntity permitPeriodEntity) {
        this.permitPeriodEntity = permitPeriodEntity;
    }

    /**
     * Gets the created by.
     *
     * @return the created by
     */
    public String getCreatedBy() {
        return createdBy;
    }
    
    /**
     * Sets the created by.
     *
     * @param createdBy the new created by
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
    
    /**
     * Gets the modified by.
     *
     * @return the modified by
     */
    public String getModifiedBy() {
        return modifiedBy;
    }
    
    /**
     * Sets the modified by.
     *
     * @param modifiedBy the new modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

   
    /**
     * Gets the created dt.
     *
     * @return the created dt
     */
    public Date getCreatedDt() {
        return this.createdDt;
    }
    
    public void setFormTypes(String fTypes) {
        this.formTypes = fTypes;
    }

   
    /**
     * Gets the created dt.
     *
     * @return the created dt
     */
    public String getFormTypes() {
        return this.formTypes;
    }
    
    /**
     * Sets the created dt.
     *
     * @param createdDt the new created dt
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    /**
     * Gets the modified dt.
     *
     * @return the modified dt
     */
    public Date getModifiedDt() {
        return this.modifiedDt;
    }
    
    /**
     * Sets the modified dt.
     *
     * @param modifiedDt the new modified dt
     */
    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }
 
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((docStatusCd == null) ? 0 : docStatusCd.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AttachmentsEntity attachEntity = (AttachmentsEntity) obj;
        if (!this.docStatusCd.equals(attachEntity.docStatusCd)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("attachment [number=").append(docStatusCd).append("]").append("[id=").append(permitId).append("]");
        return builder.toString();
    }
}

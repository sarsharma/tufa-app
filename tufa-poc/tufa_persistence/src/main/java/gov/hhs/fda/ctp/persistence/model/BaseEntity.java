/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.io.Serializable;

/**
 * The Class BaseEntity.
 *
 * @author tgunter
 */
public class BaseEntity implements Serializable, TufaEntity {
    /**
     * Base entity simply provides the two interfaces our entities will all implement.
     */
    protected static final long serialVersionUID = 1L;

}

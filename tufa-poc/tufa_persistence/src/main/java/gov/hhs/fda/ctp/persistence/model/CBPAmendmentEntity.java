package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity
@Table(name="TU_CBP_AMENDMENT")
public class CBPAmendmentEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CBP_AMENDMENT_ID", nullable = false)
    private Long cbpAmendmentId;
	
	@Column(name = "CBP_COMPANY_ID")
    private Long cbpCompanyId;
	
	@Column(name = "TOBACCO_CLASS_ID")
    private Long tobaccoClassId;
	
	@Column(name = "FISCAL_YR")
    private String fiscalYr;
	
	@Column(name="QTR")
	private String qtr;
	
	@Column(name="AMENDED_TOTAL_TAX")
	private Double amendedTotalTax;
	
	@Column(name="AMENDED_TOTAL_VOLUME")
	private Double amendedTotalVolume;
		
	@Column(name="ACCEPTANCE_FLAG")
	private String acceptanceFlag;
	
	@Column(name="IN_PROGRESS_FLAG")
	private String inProgressFlag;
	
	@Column(name="EDGE_STATUS_FLAG")
	private String edgeStatusFlag;
	
	@Column(name="REVIEW_FLAG")
	private String reviewFlag;
	
	@Column(name="COMMENTS")
	private String comments;
	
	@Column(name="PREV_ACCEPT_FLAG")
	private String previousAcceptenceFlag;
	
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "TU_CBP_DETAIL_QTR_COMMENTS", 
        joinColumns = { @JoinColumn(name = "CBP_AMENDMENT_ID") }, 
        inverseJoinColumns = { @JoinColumn(name = "COMMENT_SEQ") }
    )
    private List<CBPQtrCommentEntity> userComments;
	
	@Column(name="FDA_DELTA")
	private Double fdaDelta;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="TOBACCO_CLASS_ID",insertable=false,updatable=false)
	private TobaccoClassEntity tobaccoClass;
	
	/** The created by. */
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="CREATED_DT")
    private Date createdDt;

    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;

    /** The modified dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    
    @Transient
    private Map<String,String> mixedActionQuarterDetails;
    
    @Transient
    private Double mixedActionVolume;
    
    @Transient
    private Double mixedActionTax;
    
    
    
    public Map<String, String> getMixedActionQuarterDetails() {
		return mixedActionQuarterDetails;
	}

	public void setMixedActionQuarterDetails(Map<String, String> mixedActionQuarterDetails) {
		this.mixedActionQuarterDetails = mixedActionQuarterDetails;
	}

	public String getInProgressFlag() {
		return inProgressFlag;
	}

	public void setInProgressFlag(String inProgressFlag) {
		this.inProgressFlag = inProgressFlag;
	}
	
	 public String getreviewFlag() {
			return reviewFlag;
		}

		public void setreviewFlag(String reviewFlag) {
			this.reviewFlag = reviewFlag;
		}

	public Long getCbpAmendmentId() {
		return cbpAmendmentId;
	}

	public void setCbpAmendmentId(Long cbpAmendmentId) {
		this.cbpAmendmentId = cbpAmendmentId;
	}

	public Long getCbpCompanyId() {
		return cbpCompanyId;
	}

	public void setCbpCompanyId(Long cbpCompanyId) {
		this.cbpCompanyId = cbpCompanyId;
	}

	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}

	public void setTobaccoClassId(Long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}

	public String getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getQtr() {
		return qtr;
	}

	public void setQtr(String qtr) {
		this.qtr = qtr;
	}

	public Double getAmendedTotalTax() {
		return amendedTotalTax;
	}

	public void setAmendedTotalTax(Double amendedTotalTax) {
		this.amendedTotalTax = amendedTotalTax;
	}

	public Double getAmendedTotalVolume() {
		return amendedTotalVolume;
	}

	public void setAmendedTotalVolume(Double amendedTotalVolume) {
		this.amendedTotalVolume = amendedTotalVolume;
	}

	public String getAcceptanceFlag() {
		return acceptanceFlag;
	}

	public void setAcceptanceFlag(String acceptanceFlag) {
		this.acceptanceFlag = acceptanceFlag;
	}

	public String getEdgeStatusFlag() {
		return edgeStatusFlag;
	}

	public void setEdgeStatusFlag(String edgeStatusFlag) {
		this.edgeStatusFlag = edgeStatusFlag;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Double getFdaDelta() {
		return fdaDelta;
	}

	public void setFdaDelta(Double fdaDelta) {
		this.fdaDelta = fdaDelta;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public TobaccoClassEntity getTobaccoClass() {
		return tobaccoClass;
	}

	public void setTobaccoClass(TobaccoClassEntity tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public List<CBPQtrCommentEntity> getUserComments() {
		return userComments;
	}

	public void setUserComments(List<CBPQtrCommentEntity> userComments) {
		this.userComments = userComments;
	}
	
	public void setPreviousAcceptanceFlag(String flag) {
		this.previousAcceptenceFlag = flag;
	}
	
	public String getPreviousAcceptanceflag() {
		return this.previousAcceptenceFlag;
	}

	public Double getMixedActionVolume() {
		return mixedActionVolume;
	}

	public void setMixedActionVolume(Double mixedActionVolume) {
		this.mixedActionVolume = mixedActionVolume;
	}

	public Double getMixedActionTax() {
		return mixedActionTax;
	}

	public void setMixedActionTax(Double mixedActionTax) {
		this.mixedActionTax = mixedActionTax;
	}

	
	
	
	
}

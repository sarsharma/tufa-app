/**
 *
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "TU_CBP_DT_CONFIG")
public class CBPDateConfigEntity extends BaseEntity {

	/** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
    
    
   
    @Id
	@Column(name = "TOBACCO_CLASS_NM")
	private String tobaccoClassNm;
    
    @Id
	@Column(name = "FISCAL_YEAR")
	private Long fiscalYr;
    
    @Id
	@Column(name = "EIN")
    private String ein;
    
    @Column(name = "DATE_TYPE")
	private String dateType;
    
	public String getDateType() {
		return dateType;
	}

	public void setDateType(String dateType) {
		this.dateType = dateType;
	}

	public String getTobaccoClassNm() {
		return tobaccoClassNm;
	}

	public void setTobaccoClassNm(String tobaccoClassNm) {
		this.tobaccoClassNm = tobaccoClassNm;
	}

	public Long getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getEin() {
		return ein;
	}

	public void setEin(String ein) {
		this.ein = ein;
	}

	
	
	
	}
/**
 *
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author tgunter
 *
 */
@Entity
@Table(name="TU_CBP_ENTRY")
public class CBPEntryEntity  extends BaseEntity {
	/** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "CBP_ENTRY_ID", nullable = false)
    private Long cbpEntryId;

    /** The company. */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CBP_IMPORTER_ID", nullable=false)
    private CBPImporterEntity cbpImporter;

    @Column(name = "ENTRY_NUM")
    private String entryNum;

    @Column(name = "LINE_NUM")
    private Long lineNum;

    @Column(name = "ENTRY_TYPE_CD")
    private String entryTypeCd;

    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="ENTRY_SUMM_DT")
    private Date entrySummDt;

    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="ENTRY_DT")
    private Date entryDt;

    @Column(name = "UOM_CD")
    private String uomCd;

    @Column(name = "TAX_RATE")
    private Double taxRate;

    @Column(name = "HTS_CD")
    private String htsCd;

    @Column(name = "DEFAULT_HTS_FLAG")
    private String defaultHtsFlag;

    @Column(name = "QTY_REMOVED")
    private Double qtyRemoved;

    @Column(name = "ESTIMATED_TAX")
    private Double estimatedTax;

    @Column(name = "ESTIMATED_DUTY")
    private Double estimatedDuty;

    @Column(name = "TOBACCO_CLASS_ID")
    private Long tobaccoClassId;

    @Column(name = "FISCAL_YEAR")
    private String fiscalYear;

    @Column(name = "ASSIGNED_DT")
    private String assignedDt;

    @Column(name = "FISCAL_QTR")
    private String fiscalQtr;

    /*@Column(name = "INCLUDE_FLAG")
    private String includeFlag;*/

    /** The created by. */
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="CREATED_DT")
    private Date createdDt;

    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;

    /** The modified dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;

    @Column(name = "REALLOCATED_FLAG")
    private String reallocatedFlag;
    
    @Column(name = "EXCLUDED_FLAG")
    private String excludedFlag;  

    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="SUMMARY_ENTRY_SUMMARY_DATE")
    private Date summaryEntrySummDt;

	@OneToOne
	@JoinColumn(name="TOBACCO_CLASS_ID",insertable=false,updatable=false)
	private TobaccoClassEntity tobaccoClass;
	
    /** The Original company. */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ORIGINAL_CBP_IMPORTER_ID", referencedColumnName="CBP_IMPORTER_ID", nullable=false)
	private CBPImporterEntity originalCBPImporter;
    
    @Column(name = "MISSING_FLAG")
    private String missingFlag;

	/**
	 * @return the cbpEntryId
	 */
	public Long getCbpEntryId() {
		return cbpEntryId;
	}

	/**
	 * @param cbpEntryId the cbpEntryId to set
	 */
	public void setCbpEntryId(Long cbpEntryId) {
		this.cbpEntryId = cbpEntryId;
	}

	/**
	 * @return the importer
	 */
	public CBPImporterEntity getCbpImporter() {
		return cbpImporter;
	}

	/**
	 * @param importer the importer to set
	 */
	public void setCbpImporter(CBPImporterEntity cbpImporter) {
		this.cbpImporter = cbpImporter;
	}

	/**
	 * @return the entryNum
	 */
	public String getEntryNum() {
		return entryNum;
	}

	/**
	 * @param entryNum the entryNum to set
	 */
	public void setEntryNum(String entryNum) {
		this.entryNum = entryNum;
	}

	/**
	 * @return the lineNum
	 */
	public Long getLineNum() {
		return lineNum;
	}

	/**
	 * @param lineNum the lineNum to set
	 */
	public void setLineNum(Long lineNum) {
		this.lineNum = lineNum;
	}

	/**
	 * @return the entryTypeCd
	 */
	public String getEntryTypeCd() {
		return entryTypeCd;
	}

	/**
	 * @param entryTypeCd the entryTypeCd to set
	 */
	public void setEntryTypeCd(String entryTypeCd) {
		this.entryTypeCd = entryTypeCd;
	}

	/**
	 * @return the entrySummDt
	 */
	public Date getEntrySummDt() {
		return entrySummDt;
	}

	/**
	 * @param entrySummDt the entrySummDt to set
	 */
	public void setEntrySummDt(Date entrySummDt) {
		this.entrySummDt = entrySummDt;
	}


	/**
	 * @return the uomCd
	 */
	public String getUomCd() {
		return uomCd;
	}

	/**
	 * @param uomCd the uomCd to set
	 */
	public void setUomCd(String uomCd) {
		this.uomCd = uomCd;
	}

	/**
	 * @return the taxRate
	 */
	public Double getTaxRate() {
		return taxRate;
	}

	/**
	 * @param taxRate the taxRate to set
	 */
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	/**
	 * @return the htsCd
	 */
	public String getHtsCd() {
		return htsCd;
	}

	/**
	 * @param htsCd the htsCd to set
	 */
	public void setHtsCd(String htsCd) {
		this.htsCd = htsCd;
	}

	/**
	 * @return the defaultHtsFlag
	 */
	public String getDefaultHtsFlag() {
		return defaultHtsFlag;
	}

	/**
	 * @param defaultHtsFlag the defaultHtsFlag to set
	 */
	public void setDefaultHtsFlag(String defaultHtsFlag) {
		this.defaultHtsFlag = defaultHtsFlag;
	}

	/**
	 * @return the qtyRemoved
	 */
	public Double getQtyRemoved() {
		return qtyRemoved;
	}

	/**
	 * @param qtyRemoved the qtyRemoved to set
	 */
	public void setQtyRemoved(Double qtyRemoved) {
		this.qtyRemoved = qtyRemoved;
	}

	/**
	 * @return the estimatedTax
	 */
	public Double getEstimatedTax() {
		return estimatedTax;
	}

	/**
	 * @param estimatedTax the estimatedTax to set
	 */
	public void setEstimatedTax(Double estimatedTax) {
		this.estimatedTax = estimatedTax;
	}

	/**
	 * @return the estimatedDuty
	 */
	public Double getEstimatedDuty() {
		return estimatedDuty;
	}

	/**
	 * @param estimatedDuty the estimatedDuty to set
	 */
	public void setEstimatedDuty(Double estimatedDuty) {
		this.estimatedDuty = estimatedDuty;
	}

	/**
	 * @return the tobaccoClassId
	 */
	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}

	/**
	 * @param tobaccoClassId the tobaccoClassId to set
	 */
	public void setTobaccoClassId(Long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}

	/**
	 * @return the fiscalYear
	 */
	public String getFiscalYear() {
		return fiscalYear;
	}

	public TobaccoClassEntity getTobaccoClass() {
		return tobaccoClass;
	}

	public void setTobaccoClass(TobaccoClassEntity tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}

	/**
	 * @param fiscalYear the fiscalYear to set
	 */
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	/**
	 * @return the fiscalQtr
	 */
	public String getFiscalQtr() {
		return fiscalQtr;
	}

	/**
	 * @param fiscalQtr the fiscalQtr to set
	 */
	public void setFiscalQtr(String fiscalQtr) {
		this.fiscalQtr = fiscalQtr;
	}


	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}

	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}

	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public Date getEntryDt() {
		return entryDt;
	}

	public void setEntryDt(Date entryDt) {
		this.entryDt = entryDt;
	}

	public String getAssignedDt() {
		return assignedDt;
	}

	public void setAssignedDt(String assignedDt) {
		this.assignedDt = assignedDt;
	}

    public String getReallocatedFlag() {
		return reallocatedFlag;
	}

	public void setReallocatedFlag(String reallocatedFlag) {
		this.reallocatedFlag = reallocatedFlag;
	}

	public String getExcludedFlag() {
		return excludedFlag;
	}

	public void setExcludedFlag(String excludedFlag) {
		this.excludedFlag = excludedFlag;
	}
	
	/**
	 * @return the summaryEntrySummDt
	 */
	public Date getSummaryEntrySummDt() {
		return summaryEntrySummDt;
	}

	/**
	 * @param summaryEntrySummDt the summaryEntrySummDt to set
	 */
	public void setSummaryEntrySummDt(Date summaryEntrySummDt) {
		this.summaryEntrySummDt = summaryEntrySummDt;
	}

	/**
	 * @return the originalCBPImporter
	 */
	public CBPImporterEntity getOriginalCBPImporter() {
		return originalCBPImporter;
	}

	/**
	 * @param originalCBPImporter the originalCBPImporter to set
	 */
	public void setOriginalCBPImporter(CBPImporterEntity originalCBPImporter) {
		this.originalCBPImporter = originalCBPImporter;
	}

	public String getMissingFlag() {
		return missingFlag;
	}

	public void setMissingFlag(String missingFlag) {
		this.missingFlag = missingFlag;
	}
	
	

}

package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TU_CBP_FILE_COL_TEMPLATE")
public class CBPFileColumnTemplateEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TEMPLATE_ID", unique = true, nullable = false)
	private long templateId;

	@Column(name = "COLUMN_COUNT")
	private Integer columnCount;

	@Column(name = "ENTRY_SUMMARY_NUMBER")
	private String entrySummaryNumber;

	@Column(name = "ENTRY_SUMMARY_DATE")
	private String entrySummaryDate;

	@Column(name = "ENTRY_DATE")
	private String entryDate;

	@Column(name = "HTS_NUMBER")
	private String htsNumber;

	@Column(name = "LINE_TARIFF_QTY1")
	private String lineTariffqty1;

	@Column(name = "IMPORTER_NUMBER")
	private String importerNumber;

	@Column(name = "IMPORTER_NAME")
	private String importerName;

	@Column(name = "ULTIMATE_CONSIGNEE_NUMBER")
	private String ultimateConsigneeNumber;

	@Column(name = "ULTIMATE_CONSIGNEE_NAME")
	private String ultimateConsigneeName;
	
	@Column(name = "HTS_DESCRIPTION")
	private String htsDesciption;

	@Column(name = "LINE_TARIFF_UOM1")
	private String lineTariffuom1;

	@Column(name = "LINE_NUMBER")
	private String lineNumber;

	@Column(name = "ENTRY_TYPE_CODE")
	private String entryTypeCode;

	@Column(name = "ENTRY_TYPE_CODE_AND_DESC")
	private String entryTypeCodeDesciption;

	@Column(name = "ESTIMATED_TAX")
	private String estimatedtax;
	
	@Column(name = "TOTAL_ASCERTAINED_TAX_AMOUNT")
	private String totalAscertainedTax;
	
	@Column(name = "LINE_TARIFF_GOODS_VALUE_AMOUNT")
	private String lineTariffGoodsValueAmount;
	
	@Column(name = "IR_TAX_AMOUNT")
	private String irTaxAmount;
	
	@Column(name = "IMPORT_DATE")
	private String importDate;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FORMAT_ID", insertable = false, updatable = false)
	private TTBFileColumnFormatEntity colFormat;
	

	public TTBFileColumnFormatEntity getColFormat() {
		return colFormat;
	}

	public void setColFormat(TTBFileColumnFormatEntity colFormat) {
		this.colFormat = colFormat;
	}

	public long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}

	public Integer getColumnCount() {
		return columnCount;
	}

	public void setColumnCount(Integer columnCount) {
		this.columnCount = columnCount;
	}

	public String getEntrySummaryNumber() {
		return entrySummaryNumber;
	}

	public void setEntrySummaryNumber(String entrySummaryNumber) {
		this.entrySummaryNumber = entrySummaryNumber;
	}

	public String getEntrySummaryDate() {
		return entrySummaryDate;
	}

	public void setEntrySummaryDate(String entrySummaryDate) {
		this.entrySummaryDate = entrySummaryDate;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getHtsNumber() {
		return htsNumber;
	}

	public void setHtsNumber(String htsNumber) {
		this.htsNumber = htsNumber;
	}

	public String getLineTariffqty1() {
		return lineTariffqty1;
	}

	public void setLineTariffqty1(String lineTariffqty1) {
		this.lineTariffqty1 = lineTariffqty1;
	}

	public String getImporterNumber() {
		return importerNumber;
	}

	public void setImporterNumber(String importerNumber) {
		this.importerNumber = importerNumber;
	}

	public String getImporterName() {
		return importerName;
	}

	public void setImporterName(String importerName) {
		this.importerName = importerName;
	}

	public String getUltimateConsigneeNumber() {
		return ultimateConsigneeNumber;
	}

	public void setUltimateConsigneeNumber(String ultimateConsigneeNumber) {
		this.ultimateConsigneeNumber = ultimateConsigneeNumber;
	}

	public String getUltimateConsigneeName() {
		return ultimateConsigneeName;
	}

	public void setUltimateConsigneeName(String ultimateConsigneeName) {
		this.ultimateConsigneeName = ultimateConsigneeName;
	}

	public String getHtsDesciption() {
		return htsDesciption;
	}

	public void setHtsDesciption(String htsDesciption) {
		this.htsDesciption = htsDesciption;
	}

	public String getLineTariffuom1() {
		return lineTariffuom1;
	}

	public void setLineTariffuom1(String lineTariffuom1) {
		this.lineTariffuom1 = lineTariffuom1;
	}

	public String getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getEntryTypeCode() {
		return entryTypeCode;
	}

	public void setEntryTypeCode(String entryTypeCode) {
		this.entryTypeCode = entryTypeCode;
	}

	public String getEntryTypeCodeDesciption() {
		return entryTypeCodeDesciption;
	}

	public void setEntryTypeCodeDesciption(String entryTypeCodeDesciption) {
		this.entryTypeCodeDesciption = entryTypeCodeDesciption;
	}

	public String getEstimatedtax() {
		return estimatedtax;
	}

	public void setEstimatedtax(String estimatedtax) {
		this.estimatedtax = estimatedtax;
	}

	public String getTotalAscertainedTax() {
		return totalAscertainedTax;
	}

	public void setTotalAscertainedTax(String totalAscertainedTax) {
		this.totalAscertainedTax = totalAscertainedTax;
	}

	public String getLineTariffGoodsValueAmount() {
		return lineTariffGoodsValueAmount;
	}

	public void setLineTariffGoodsValueAmount(String lineTariffGoodsValueAmount) {
		this.lineTariffGoodsValueAmount = lineTariffGoodsValueAmount;
	}

	public String getIrTaxAmount() {
		return irTaxAmount;
	}

	public void setIrTaxAmount(String irTaxAmount) {
		this.irTaxAmount = irTaxAmount;
	}

	public String getImportDate() {
		return importDate;
	}

	public void setImportDate(String importDate) {
		this.importDate = importDate;
	}




}

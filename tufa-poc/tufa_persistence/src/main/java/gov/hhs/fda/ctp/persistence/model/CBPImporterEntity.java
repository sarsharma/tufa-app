/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author tgunter
 *
 */
@Entity
@Table(name="TU_CBP_IMPORTER")
public class CBPImporterEntity extends BaseEntity {
	/** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;

    /** The id field. */
    @Id
    @Column(name = "CBP_IMPORTER_ID", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long cbpImporterId;
    
    @Column(name = "FISCAL_YR")
    private String fiscalYr;

    @Column(name = "IMPORTER_EIN")
    private String importerEIN;
 
    @Column(name = "IMPORTER_NM")
    private String importerNm;
    
    @Column(name = "CONSIGNEE_EIN")
    private String consigneeEIN;
        
    @Column(name = "ASSOCIATION_TYPE_CD")
    private String associationTypeCd;
    
    @Column(name = "DEFAULT_FLAG")
    private String defaultFlag;
    
    @Column(name = "INCLUDE_FLAG")
    private String includeFlag;
    
    @Column(name = "CONSIGNEE_EXISTS_FLAG")
    private String consigneeExistsFlag;

    /** The created by. */
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;

    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cbpImporter")
    private Set<CBPEntryEntity> entry;

	/**
	 * @return the cbpImporterId
	 */
	public Long getCbpImporterId() {
		return cbpImporterId;
	}

	/**
	 * @param cbpImporterId the cbpImporterId to set
	 */
	public void setCbpImporterId(Long cbpImporterId) {
		this.cbpImporterId = cbpImporterId;
	}

	/**
	 * @return the fiscalYr
	 */
	public String getFiscalYr() {
		return fiscalYr;
	}

	/**
	 * @param fiscalYr the fiscalYr to set
	 */
	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	/**
	 * @return the importerEIN
	 */
	public String getImporterEIN() {
		return importerEIN;
	}

	/**
	 * @param importerEIN the importerEIN to set
	 */
	public void setImporterEIN(String importerEIN) {
		this.importerEIN = importerEIN;
	}

	/**
	 * @return the importerNm
	 */
	public String getImporterNm() {
		return importerNm;
	}

	/**
	 * @param importerNm the importerNm to set
	 */
	public void setImporterNm(String importerNm) {
		this.importerNm = importerNm;
	}

	/**
	 * @return the consigneeEIN
	 */
	public String getConsigneeEIN() {
		return consigneeEIN;
	}

	/**
	 * @param consigneeEIN the consigneeEIN to set
	 */
	public void setConsigneeEIN(String consigneeEIN) {
		this.consigneeEIN = consigneeEIN;
	}

	/**
	 * @return the associationTypeCd
	 */
	public String getAssociationTypeCd() {
		return associationTypeCd;
	}

	/**
	 * @param associationTypeCd the associationTypeCd to set
	 */
	public void setAssociationTypeCd(String associationTypeCd) {
		this.associationTypeCd = associationTypeCd;
	}

	/**
	 * @return the defaultFlag
	 */
	public String getDefaultFlag() {
		return defaultFlag;
	}

	/**
	 * @param defaultFlag the defaultFlag to set
	 */
	public void setDefaultFlag(String defaultFlag) {
		this.defaultFlag = defaultFlag;
	}

	/**
	 * @return the consigneeExistsFlag
	 */
	public String getConsigneeExistsFlag() {
		return consigneeExistsFlag;
	}

	/**
	 * @param consigneeExistsFlag the consigneeExistsFlag to set
	 */
	public void setConsigneeExistsFlag(String consigneeExistsFlag) {
		this.consigneeExistsFlag = consigneeExistsFlag;
	}

	/**
	 * @return the entry
	 */
	public Set<CBPEntryEntity> getEntry() {
		return entry;
	}

	/**
	 * @param entry the entry to set
	 */
	public void setEntry(Set<CBPEntryEntity> entry) {
		this.entry = entry;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}

	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}

	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public String getIncludeFlag() {
		return includeFlag;
	}

	public void setIncludeFlag(String includeFlag) {
		this.includeFlag = includeFlag;
	}

}

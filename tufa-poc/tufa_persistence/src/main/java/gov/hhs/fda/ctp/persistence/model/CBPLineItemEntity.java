/**
 *
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Priti
 *
 */
@Entity
@Table(name = "TU_CBP_DETAILS_FILE")
public class CBPLineItemEntity extends BaseEntity {

	/** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
    
    /** The id field. */
    @Id
    @Column(name = "LINE_ITEM_ID", nullable = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "cbp_details_generator")
    @SequenceGenerator(name="cbp_details_generator", sequenceName ="CBP_LINE_ITEM_ID_SEQ",allocationSize=100000)
    private Long lineItemId;
    
    @Column(name = "ENTRY_SUMMARY_NUMBER")
	private String entrySummaryNumber;
    
	@Column(name = "ENTRY_SUMMARY_DATE")
	private String entrySummaryDate;
	
	@Column(name = "ENTRY_DATE")
    private String entryDate;
	
	@Column(name = "HTS_NUMBER")
    private Long htsNumber;
	
	@Column(name = "LINE_TARIFF_QTY1")
    private Double  lineTarrifQty1;
	
	@Column(name = "IMPORTER_NUMBER")
	private String importerNumber;

	@Column(name = "IMPORTER_NAME")
	private String importerName;
	
	@Column(name = "ULTIMATE_CONSIGNEE_NUMBER")
	private String ultimateConsigneeNumber;
	
	@Column(name = "ULTIMATE_CONSIGNEE_NAME")
	private String ultimateConsigneeName;
	
	@Column(name = "HTS_DESCRIPTION")
    private String htsDescription;
	
	@Column(name = "LINE_TARIFF_UOM1")
    private String lineTarrifUOM1;
	
	@Column(name = "LINE_NUMBER")
    private long lineNumber;
	
	@Column(name = "ENTRY_TYPE_CODE")
    private String entryTypeCode;
	
	@Column(name = "CREATED_BY")
	private String created_by;
	
	@Column(name = "CREATED_DATE")
	private Date created_date;
	
	@Column(name = "FISCAL_YEAR")
	private Long fiscalYr;

	@Column(name = "LINE_TARIFF_GOODS_VALUE_AMOUNT")
	private Double lineTariffGoodsValueAmount;
	
	@Column(name = "IR_TAX_AMOUNT")
	private Double irTaxAmount;
	
	
	@Column(name = "ERROR")
	private String  errors;
	
	@Column(name = "IMPORT_DATE")
	private String  importDate;

	public Long getLineItemId() {
		return lineItemId;
	}

	public void setLineItemId(Long lineItemId) {
		this.lineItemId = lineItemId;
	}

	public String getEntrySummaryNumber() {
		return entrySummaryNumber;
	}

	public void setEntrySummaryNumber(String entrySummaryNumber) {
		this.entrySummaryNumber = entrySummaryNumber;
	}

	public String getEntrySummaryDate() {
		return entrySummaryDate;
	}

	public void setEntrySummaryDate(String entrySummaryDate) {
		this.entrySummaryDate = entrySummaryDate;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public Long getHtsNumber() {
		return htsNumber;
	}

	public void setHtsNumber(Long htsNumber) {
		this.htsNumber = htsNumber;
	}


	public String getImporterNumber() {
		return importerNumber;
	}

	public void setImporterNumber(String importerNumber) {
		this.importerNumber = importerNumber;
	}

	public String getImporterName() {
		return importerName;
	}

	public void setImporterName(String importerName) {
		this.importerName = importerName;
	}

	public String getUltimateConsigneeNumber() {
		return ultimateConsigneeNumber;
	}

	public void setUltimateConsigneeNumber(String ultimateConsigneeNumber) {
		this.ultimateConsigneeNumber = ultimateConsigneeNumber;
	}

	public String getUltimateConsigneeName() {
		return ultimateConsigneeName;
	}

	public void setUltimateConsigneeName(String ultimateConsigneeName) {
		this.ultimateConsigneeName = ultimateConsigneeName;
	}

	public String getHtsDescription() {
		return htsDescription;
	}

	public void setHtsDescription(String htsDescription) {
		this.htsDescription = htsDescription;
	}


	public String getLineTarrifUOM1() {
		return lineTarrifUOM1;
	}

	public void setLineTarrifUOM1(String lineTarrifUOM1) {
		this.lineTarrifUOM1 = lineTarrifUOM1;
	}

	public long getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(long lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getEntryTypeCode() {
		return entryTypeCode;
	}

	public void setEntryTypeCode(String entryTypeCode) {
		this.entryTypeCode = entryTypeCode;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Long getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public Double getLineTariffGoodsValueAmount() {
		return lineTariffGoodsValueAmount;
	}

	public void setLineTariffGoodsValueAmount(Double lineTariffGoodsValueAmount) {
		this.lineTariffGoodsValueAmount = lineTariffGoodsValueAmount;
	}

	public Double getIrTaxAmount() {
		return irTaxAmount;
	}

	public void setIrTaxAmount(Double irTaxAmount) {
		this.irTaxAmount = irTaxAmount;
	}


	public String getImportDate() {
		return importDate;
	}

	public void setImportDate(String importDate) {
		this.importDate = importDate;
	}

	public Double getLineTarrifQty1() {
		return lineTarrifQty1;
	}

	public void setLineTarrifQty1(Double lineTarrifQty1) {
		this.lineTarrifQty1 = lineTarrifQty1;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}
	
	

	
	
	}
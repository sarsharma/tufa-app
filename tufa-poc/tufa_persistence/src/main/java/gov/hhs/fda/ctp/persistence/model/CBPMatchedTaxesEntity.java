/**
 *
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Priti
 *
 */
@Entity
@Table(name = "TU_CBP_MATCHED_TAXES")
public class CBPMatchedTaxesEntity extends BaseEntity {
	
	@Id
	@Column(name = "FISCAL_YEAR")
	private Long fiscalYr;

	/** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
    
    @Column(name = "ENTRY_SUMMARY_NUMBER")
	private String entrySummaryNumber;
    
	@Column(name = "IMPORTER_NUMBER")
	private String importerNumber;

	@Column(name = "DETAIL_FILE_SUMMARIZED_TAX")
	private Double detailFileSummarizedTax;
	
	@Column(name = "SUMMARIZED_FILE_TAX")
	private Double summarizedFileTax;
	
	@Column(name = "ENTRY_NUMBER_MATCH")
	private String entryNumberMatch;
	
	@Column(name = "MIXED_TOBACCO_TYPE_FLAG")
	private String mixedTobaccoTypeFlag;
	
	
	@Column(name = "CREATED_BY")
	private String created_by;
	
	@Column(name = "CREATED_DATE")
	private Date created_date;

	public String getEntrySummaryNumber() {
		return entrySummaryNumber;
	}

	public void setEntrySummaryNumber(String entrySummaryNumber) {
		this.entrySummaryNumber = entrySummaryNumber;
	}

	public String getImporterNumber() {
		return importerNumber;
	}

	public void setImporterNumber(String importerNumber) {
		this.importerNumber = importerNumber;
	}

	public Double getDetailFileSummarizedTax() {
		return detailFileSummarizedTax;
	}

	public void setDetailFileSummarizedTax(Double detailFileSummarizedTax) {
		this.detailFileSummarizedTax = detailFileSummarizedTax;
	}

	public Double getSummarizedFileTax() {
		return summarizedFileTax;
	}

	public void setSummarizedFileTax(Double summarizedFileTax) {
		this.summarizedFileTax = summarizedFileTax;
	}

	public String getEntryNumberMatch() {
		return entryNumberMatch;
	}

	public void setEntryNumberMatch(String entryNumberMatch) {
		this.entryNumberMatch = entryNumberMatch;
	}

	public String getMixedTobaccoTypeFlag() {
		return mixedTobaccoTypeFlag;
	}

	public void setMixedTobaccoTypeFlag(String mixedTobaccoTypeFlag) {
		this.mixedTobaccoTypeFlag = mixedTobaccoTypeFlag;
	}

	public Long getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	
	

	
	
	
	
	}
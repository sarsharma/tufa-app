package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Kyle
 *
 */
@Entity
@Table(name = "TU_CBP_METRICS")
public class CBPMetricsEntity extends BaseEntity {

	/***/
	private static final long serialVersionUID = 1L;
	
	/** The id field. */
    @Id
    @Column(name = "FISCAL_YR", nullable = false)
	private String fiscalYr;

    @Column(name = "SUMMARY_FILE_ENT_SMRY_COUNT", nullable = true)
	private Integer summaryFileEntrySummaryNoCount;

    @Column(name = "DETAIL_FILE_ENT_SMRY_NO_COUNT", nullable = true)
	private Integer detailFileEntrySummaryNoCount;

    @Column(name = "MATCHED_ENT_SMRY_NO_COUNT", nullable = true)
	private Integer matchedEntrySummaryNoCount;

    @Column(name = "UNMATCHED_ENT_SMRY_NO_COUNT", nullable = true)
	private Integer unmatchedEntrySummayNoCount;

    @Column(name = "UNIQUE_SMRY_FILE_ENTRY_SMRY_NO", nullable = true)
	private Integer uniqueSummaryFileEntrySummaryNo;

    @Column(name = "UNIQUE_DETAIL_FILE_ENT_SMRY_NO", nullable = true)
	private Integer uniqueDetailFileEntrySummaryNo;

    @Column(name = "DETAIL_FILE_TOTAL_TAX_AMT", nullable = true)
	private Double detailFileTotalTaxAmount;

    @Column(name = "MIXED_ENT_SMRY_NO_COUNT", nullable = true)
	private Integer mixedEntrySummaryNoCount;

    @Column(name = "CREATED_DT", nullable = true)
	private Date createdDt;

    @Column(name = "CREATED_BY", nullable = true)
	private String createdBy;

    @Column(name = "MODIFIED_DT", nullable = true)
	private Date modifiedDt;

    @Column(name = "MODIFIED_BY", nullable = true)
	private String modifiedBy;
    
    @Column(name = "ACCEPTED", nullable = true)
	private String accepted;
	
    /**
	 * @return the fiscalYr
	 */
	public String getFiscalYr() {
		return fiscalYr;
	}
	/**
	 * @param fiscalYr the fiscalYr to set
	 */
	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}
	/**
	 * @return the summaryFileEntrySummaryNoCount
	 */
	public Integer getSummaryFileEntrySummaryNoCount() {
		return summaryFileEntrySummaryNoCount;
	}
	/**
	 * @param summaryFileEntrySummaryNoCount the summaryFileEntrySummaryNoCount to set
	 */
	public void setSummaryFileEntrySummaryNoCount(Integer summaryFileEntrySummaryNoCount) {
		this.summaryFileEntrySummaryNoCount = summaryFileEntrySummaryNoCount;
	}
	/**
	 * @return the detailFileEntrySummaryNoCount
	 */
	public Integer getDetailFileEntrySummaryNoCount() {
		return detailFileEntrySummaryNoCount;
	}
	/**
	 * @param detailFileEntrySummaryNoCount the detailFileEntrySummaryNoCount to set
	 */
	public void setDetailFileEntrySummaryNoCount(Integer detailFileEntrySummaryNoCount) {
		this.detailFileEntrySummaryNoCount = detailFileEntrySummaryNoCount;
	}
	/**
	 * @return the matchedEntrySummaryNoCount
	 */
	public Integer getMatchedEntrySummaryNoCount() {
		return matchedEntrySummaryNoCount;
	}
	/**
	 * @param matchedEntrySummaryNoCount the matchedEntrySummaryNoCount to set
	 */
	public void setMatchedEntrySummaryNoCount(Integer matchedEntrySummaryNoCount) {
		this.matchedEntrySummaryNoCount = matchedEntrySummaryNoCount;
	}
	/**
	 * @return the unmatchedEntrySummayNoCount
	 */
	public Integer getUnmatchedEntrySummayNoCount() {
		return unmatchedEntrySummayNoCount;
	}
	/**
	 * @param unmatchedEntrySummayNoCount the unmatchedEntrySummayNoCount to set
	 */
	public void setUnmatchedEntrySummayNoCount(Integer unmatchedEntrySummayNoCount) {
		this.unmatchedEntrySummayNoCount = unmatchedEntrySummayNoCount;
	}
	/**
	 * @return the uniqueSummaryFileEntrySummaryNo
	 */
	public Integer getUniqueSummaryFileEntrySummaryNo() {
		return uniqueSummaryFileEntrySummaryNo;
	}
	/**
	 * @param uniqueSummaryFileEntrySummaryNo the uniqueSummaryFileEntrySummaryNo to set
	 */
	public void setUniqueSummaryFileEntrySummaryNo(Integer uniqueSummaryFileEntrySummaryNo) {
		this.uniqueSummaryFileEntrySummaryNo = uniqueSummaryFileEntrySummaryNo;
	}
	/**
	 * @return the uniqueDetailFileEntrySummaryNo
	 */
	public Integer getUniqueDetailFileEntrySummaryNo() {
		return uniqueDetailFileEntrySummaryNo;
	}
	/**
	 * @param uniqueDetailFileEntrySummaryNo the uniqueDetailFileEntrySummaryNo to set
	 */
	public void setUniqueDetailFileEntrySummaryNo(Integer uniqueDetailFileEntrySummaryNo) {
		this.uniqueDetailFileEntrySummaryNo = uniqueDetailFileEntrySummaryNo;
	}
	/**
	 * @return the detailFileTotalTaxAmount
	 */
	public Double getDetailFileTotalTaxAmount() {
		return detailFileTotalTaxAmount;
	}
	/**
	 * @param detailFileTotalTaxAmount the detailFileTotalTaxAmount to set
	 */
	public void setDetailFileTotalTaxAmount(Double detailFileTotalTaxAmount) {
		this.detailFileTotalTaxAmount = detailFileTotalTaxAmount;
	}
	/**
	 * @return the mixedEntrySummaryNoCount
	 */
	public Integer getMixedEntrySummaryNoCount() {
		return mixedEntrySummaryNoCount;
	}
	/**
	 * @param mixedEntrySummaryNoCount the mixedEntrySummaryNoCount to set
	 */
	public void setMixedEntrySummaryNoCount(Integer mixedEntrySummaryNoCount) {
		this.mixedEntrySummaryNoCount = mixedEntrySummaryNoCount;
	}
	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}
	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}
	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getAccepted() {
		return accepted;
	}
	public void setAccepted(String accepted) {
		this.accepted = accepted;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}

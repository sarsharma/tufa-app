package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "TU_CBP_QTR_COMMENT")
public class CBPQtrCommentEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COMMENT_SEQ", unique = true, nullable = false)
	private Long commentSeq;

	@Column(name = "USER_COMMENT")
	private String userComment;

	@Column(name = "QTR")
	private String qtr;
	
	@Column(name = "COMMENT_QTRS")
	private String commentQtrs;

	@Column(name = "COMMENT_AUTHOR")
	private String author;

	@Temporal(TemporalType.DATE)
	@CreationTimestamp
	@Column(name = "COMMENT_DATE")
	private Date commentDate;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@UpdateTimestamp
	@Column(name = "MODIFIED_DT")
	private Date modifiedDt;

	@ManyToMany(mappedBy = "userComments")
	private List<CBPAmendmentEntity> cbpAmendments;

	public Long getCommentSeq() {
		return commentSeq;
	}

	public void setCommentSeq(Long commentSeq) {
		this.commentSeq = commentSeq;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public List<CBPAmendmentEntity> getCbpAmendments() {
		return cbpAmendments;
	}

	public void setCbpAmendments(List<CBPAmendmentEntity> cbpAmendments) {
		this.cbpAmendments = cbpAmendments;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public String getQtr() {
		return qtr;
	}

	public void setQtr(String qtr) {
		this.qtr = qtr;
	}

	public String getCommentQtrs() {
		return commentQtrs;
	}

	public void setCommentQtrs(String commentQtrs) {
		this.commentQtrs = commentQtrs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 53;
		int result = (int) (this.commentSeq ^ (this.commentSeq >>> 32));
		result = prime * result + ((commentSeq == null) ? 0 : commentSeq.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (obj != null && getClass() == obj.getClass()) {
			if (this == obj)
				return true;
			if (((CBPQtrCommentEntity) obj).getCommentSeq() == null)
				return false;

			if (((CBPQtrCommentEntity) obj).getCommentSeq().equals(this.getCommentSeq()))
				return true;
		}

		return false;
	}

}

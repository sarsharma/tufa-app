/**
 *
 */
package gov.hhs.fda.ctp.persistence.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Priti
 *
 */
@Entity
@Table(name = "TU_CBP_SUMMARY_FILE")
public class CBPSummaryFileEntity extends BaseEntity {

	/** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
    
    /** The id field. */
    @Id
    @Column(name = "SUMMARY_ID", nullable = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "cbp_summ_generator")
    @SequenceGenerator(name="cbp_summ_generator", sequenceName ="CBP_SUMMARY_SUMMARY_ID_SEQ",allocationSize=100000)
    private Long cbpSummaryId;
	
	@Column(name = "ULTIMATE_CONSIGNEE_NUMBER")
	private String ultimateConsigneeNumber;
	
	@Column(name = "ULTIMATE_CONSIGNEE_NAME")
	private String ultimateConsigneeName;

	@Column(name = "IMPORTER_NUMBER")
	private String importerNumber;

	@Column(name = "IMPORTER_NAME")
	private String importerName;
	
	@Column(name = "ENTRY_DATE")
    private String EntryDate;
		
	@Column(name = "ENTRY_SUMMARY_DATE")
	private String entrySummaryDate;
	
	@Column(name = "ENTRY_SUMMARY_NUMBER")
	private String entrySummaryNumber;
	
	@Column(name = "ESTIMATED_TAX")
	private Double estimatedTax;
	
	@Column(name = "TOTAL_ASCERTAINED_TAX_AMOUNT")
	private Double totalAscertainedtax;
	
	@Column(name = "ENTRY_TYPE_CODE_AND_DESC")
	private String entryTypeCodeDescription;
	
	@Column(name = "FISCAL_YEAR")
	private Long fiscalYr;
	
	@Column(name = "CREATED_BY")
	private String created_by;
	
	@Column(name = "CREATED_DATE")
	private Date created_date;
	
	@Column(name = "ERROR")
	private String error;
	
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public long getFiscalYr() {
		return fiscalYr;
	}
	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}
	
	public Long getCbpSummaryId() {
		return cbpSummaryId;
	}

	public void setCbpSummaryId(Long cbpSummaryId) {
		this.cbpSummaryId = cbpSummaryId;
	}

	public String getultimateConsigneeNumber() {
		return ultimateConsigneeNumber;
	}

	public void setultimateConsigneeNumber(String ultimateConsigneeNumber) {
		this.ultimateConsigneeNumber = ultimateConsigneeNumber;
	}

	public String getultimateConsigneeName() {
		return ultimateConsigneeName;
	}

	public void setultimateConsigneeName(String ultimateConsigneeName) {
		this.ultimateConsigneeName = ultimateConsigneeName;
	}

	public String getImporterNumber() {
		return importerNumber;
	}

	public void setImporterNumber(String importerNumber) {
		this.importerNumber = importerNumber;
	}

	public String getImporterName() {
		return importerName;
	}

	public void setImporterName(String importerName) {
		this.importerName = importerName;
	}

	public String getEntryDate() {
		return EntryDate;
	}

	public void setEntryDate(String entryDate) {
		EntryDate = entryDate;
	}

	public String getentrySummaryDate() {
		return entrySummaryDate;
	}

	public void setentrySummaryDate(String entrySummaryDate) {
		this.entrySummaryDate = entrySummaryDate;
	}

	public String getentrySummaryNumber() {
		return entrySummaryNumber;
	}

	public void setentrySummaryNumber(String entrySummaryNumber) {
		this.entrySummaryNumber = entrySummaryNumber;
	}

	public Double getEstimatedTax() {
		return estimatedTax;
	}

	public void setEstimatedTax(Double estimatedTax) {
		this.estimatedTax = estimatedTax;
	}

	public Double gettotalAscertainedtax() {
		return totalAscertainedtax;
	}

	public void settotalAscertainedtax(Double totalAscertainedtax) {
		this.totalAscertainedtax = totalAscertainedtax;
	}

	public String getentryTypeCodeDescription() {
		return entryTypeCodeDescription;
	}

	public void setentryTypeCodeDescription(String entryTypeCodeDescription) {
		this.entryTypeCodeDescription = entryTypeCodeDescription;
	}
	
	public String getcreated_by() {
		return created_by;
	}


	public void setcreated_by(String created_by) {
		this.created_by = created_by;
	}


	public Date getcreated_date() {
		return created_date;
	}


	public void setcreated_date(Date date) {
		this.created_date = date;
	}


	
}
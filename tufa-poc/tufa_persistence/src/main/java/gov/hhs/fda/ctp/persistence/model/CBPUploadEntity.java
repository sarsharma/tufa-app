/**
 *
 */
package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author tgunter
 *
 */
@Entity
@Table(name = "TU_CBP_UPLOAD")
public class CBPUploadEntity extends BaseEntity {

	/** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CBP_UPLOAD_ID", unique = true, nullable = false)
	private Long cbpUploadId;

	@Column(name = "CONSIGNEE_EIN")
	private String consigneeEIN;

	@Column(name = "IMPORTER_EIN")
	private String importerEIN;

	@Column(name = "IMPORTER_NAME")
	private String importerName;
	
	@Column(name = "CONSIGNEE_NAME")
	private String consigneeName;

	@Column(name = "ENTRY_NO")
	private String entryNo;

	@Column(name = "LINE_NO")
	private Long lineNo;

	@Column(name = "ENTRY_SUMM_DATE")
	private String entrySummDate;

	@Column(name = "ENTRY_DATE")
    private String entryDate;

	@Column(name = "UOM1")
	private String uom1;

	@Column(name = "HTS_CD")
	private String htsCd;

	@Column(name = "QTY1")
	private Double qty1;

	@Column(name = "ESTIMATED_TAX")
	private Double estimatedTax;

	@Column(name = "FISCAL_YR")
	private Long fiscalYr;

	@Column(name = "ERRORS")
	private String errors;

	/**
	 * @return the cbpUploadId
	 */
	public Long getCbpUploadId() {
		return cbpUploadId;
	}

	/**
	 * @param cbpUploadId the cbpUploadId to set
	 */
	public void setCbpUploadId(Long cbpUploadId) {
		this.cbpUploadId = cbpUploadId;
	}

	/**
	 * @return the consigneeEIN
	 */
	public String getConsigneeEIN() {
		return consigneeEIN;
	}

	/**
	 * @param consigneeEIN the consigneeEIN to set
	 */
	public void setConsigneeEIN(String consigneeEIN) {
		this.consigneeEIN = consigneeEIN;
	}

	/**
	 * @return the importerEIN
	 */
	public String getImporterEIN() {
		return importerEIN;
	}

	/**
	 * @param importerEIN the importerEIN to set
	 */
	public void setImporterEIN(String importerEIN) {
		this.importerEIN = importerEIN;
	}

	/**
	 * @return the importerNm
	 */
	public String getImporterName() {
		return importerName;
	}

	/**
	 * @param importerNm the importerNm to set
	 */
	public void setImporterName(String importerNm) {
		this.importerName = importerNm;
	}

	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	/**
	 * @return the entryNum
	 */
	public String getEntryNo() {
		return entryNo;
	}

	/**
	 * @param entryNum the entryNum to set
	 */
	public void setEntryNo(String entryNo) {
		this.entryNo = entryNo;
	}

	/**
	 * @return the lineNum
	 */
	public Long getLineNo() {
		return lineNo;
	}

	/**
	 * @param lineNum the lineNum to set
	 */
	public void setLineNo(Long lineNo) {
		this.lineNo = lineNo;
	}

	/**
	 * @return the entrySummDt
	 */
	public String getEntrySummDate() {
		return entrySummDate;
	}

	/**
	 * @param entrySummDt the entrySummDt to set
	 */
	public void setEntrySummDate(String entrySummDate) {
		this.entrySummDate = entrySummDate;
	}

	/**
	 * @return the importDt
	 */
	public String getEntryDate() {
		return entryDate;
	}

	/**
	 * @param importDt the importDt to set
	 */
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	/**
	 * @return the uomCd
	 */
	public String getUom1() {
		return uom1;
	}

	/**
	 * @param uomCd the uomCd to set
	 */
	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}

	/**
	 * @return the htsCd
	 */
	public String getHtsCd() {
		return htsCd;
	}

	/**
	 * @param htsCd the htsCd to set
	 */
	public void setHtsCd(String htsCd) {
		this.htsCd = htsCd;
	}

	/**
	 * @return the qtyRemoved
	 */
	public Double getQty1() {
		return qty1;
	}

	/**
	 * @param qtyRemoved the qtyRemoved to set
	 */
	public void setQty1(Double qty1) {
		this.qty1 = qty1;
	}

	/**
	 * @return the estimatedTax
	 */
	public Double getEstimatedTax() {
		return estimatedTax;
	}

	/**
	 * @param estimatedTax the estimatedTax to set
	 */
	public void setEstimatedTax(Double estimatedTax) {
		this.estimatedTax = estimatedTax;
	}

	/**
	 * @return the fiscalYr
	 */
	public Long getFiscalYr() {
		return fiscalYr;
	}

	/**
	 * @param fiscalYr the fiscalYr to set
	 */
	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	/**
	 * @return the errors
	 */
	public String getErrors() {
		return errors;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(String errors) {
		this.errors = errors;
	}

}

/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.dozer.Mapping;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Filter;

/**
 * The Class CompanyEntity.
 *
 * @author tgunter
 */
@Entity
@Table(name = "tu_company", uniqueConstraints = {
        @UniqueConstraint(columnNames = "EIN") })
@DynamicUpdate
public class CompanyEntity extends BaseEntity {
    /**
     * Entity bean for tu_company table.
     */
    private static final long serialVersionUID = 1L;
    
    /** The company id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COMPANY_ID", unique = true, nullable = false)
    private Long companyId;
    
    /** The legal name. */
    @Column(name = "LEGAL_NM", nullable = false, length = 100)
    @Filter(name = "legalNameFilter", condition = "UPPER(legalName) LIKE UPPER('%:legalName%')")
    private String legalName;
    
    /** The ein. */
    @Column(name = "EIN", columnDefinition="char(9)", unique = true, nullable = false)
    @Mapping("einNumber")
    private String EIN;

    /** Comments about the company */
    @Column(name = "COMPANY_COMMENTS")
    private String companyComments;
    
    /** The permits. */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "companyEntity")
    private Set<PermitEntity> permits;
    
    /** The addresses. */
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "company")
    private Set<AddressEntity> addresses;
    
    /** The contacts. */
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "company")
    @OrderBy("primaryContactSequence ASC, createdDt ASC, contactId ASC")
    private Set<ContactEntity> contacts = new TreeSet<ContactEntity>();
    
    /** The created by. */
    /* trigger managed attributes */
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;
    
    /** The modified by. */
    @Column(name= "MODIFIED_BY", length=50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    
    /** The company status. */
    @Column(name = "COMPANY_STATUS", nullable = false, length = 4)
    private String companyStatus;
    
    @Column(name = "EMAIL_ADDRESS_FLAG", nullable = false, length = 1)
    private String emailAddressFlag = "N";
    
    @Column(name = "PHYSICAL_ADDRESS_FLAG", nullable = false, length = 1)
    private String physicalAddressFlag = "N";
    
    @Column(name = "WELCOME_LETTER_FLAG", nullable = false, length = 1)
    private String welcomeLetterFlag = "N";
    
    @Temporal(TemporalType.DATE) 
    @Column(name = "WELCOME_LETTER_SENT_DT")
    private Date welcomeLetterSentDt;
    
	/**
     * Gets the company id.
     *
     * @return the company id
     */
    public Long getCompanyId() {
        return this.companyId;
    }

    /**
     * Sets the company id.
     *
     * @param companyId the new company id
     */
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
    
    /**
     * Gets the legal name.
     *
     * @return the legal name
     */
    public String getLegalName() {
        return this.legalName;
    }

    /**
     * Sets the legal name.
     *
     * @param legalName the new legal name
     */
    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    /**
     * Gets the ein.
     *
     * @return the ein
     */
    public String getEIN() {
        return this.EIN;
    }

    /**
     * Sets the ein.
     *
     * @param EIN the new ein
     */
    public void setEIN(String EIN) {
        this.EIN = EIN;
    }
    
    
    /**
     * Gets the permits.
     *
     * @return the permits
     */
    public Set<PermitEntity> getPermits() {
        return permits;
    }

    /**
     * Sets the permits.
     *
     * @param permits the new permits
     */
    public void setPermits(Set<PermitEntity> permits) {
        this.permits = permits;
    }

    /**
     * Gets the addresses.
     *
     * @return the addresses
     */
    public Set<AddressEntity> getAddresses() {
        return addresses;
    }
    
    /**
     * Sets the addresses.
     *
     * @param addresses the new addresses
     */
    public void setAddresses(Set<AddressEntity> addresses) {
        this.addresses = addresses;
    }
    
    /**
     * Gets the contacts.
     *
     * @return the contacts
     */
	public Set<ContactEntity> getContacts() {
		return contacts;
	}

	/**
	 * Sets the contacts.
	 *
	 * @param contacts the new contacts
	 */
	public void setContacts(Set<ContactEntity> contacts) {
		this.contacts = contacts;
	}

    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((EIN == null) ? 0 : EIN.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if(obj != null && getClass() == obj.getClass()) {
            if(this == obj || ((CompanyEntity)obj).getEIN().equals(this.EIN)) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("Company [name=").append(legalName).append("]").append("[id=").append(companyId).append("]");
        return builder.toString();
    }

    /**
     * Gets the created by.
     *
     * @return the created by
     */
    /*
     * Mark all of these as transient.  These database
     * fields will be managed by triggers.
     */
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * Sets the created by.
     *
     * @param createdBy the new created by
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
    /**
     * Gets the modified by.
     *
     * @return the modified by
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }
    
    /**
     * Sets the modified by.
     *
     * @param modifiedBy the new modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    
    /**
     * Gets the created dt.
     *
     * @return the created dt
     */
    public Date getCreatedDt() {
        return this.createdDt;
    }
    
    /**
     * Sets the created dt.
     *
     * @param createdDt the new created dt
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }
    
    /**
     * Gets the modified dt.
     *
     * @return the modified dt
     */
    public Date getModifiedDt() {
        return this.modifiedDt;
    }
    
    /**
     * Sets the modified dt.
     *
     * @param modifiedDt the new modified dt
     */
    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }
    
    /**
     * Gets the company status.
     *
     * @return the company status
     */
    public String getCompanyStatus() {
        return this.companyStatus;
    }

    /**
     * Sets the company status.
     *
     * @param status the new company status
     */
    public void setCompanyStatus(String status) {
        this.companyStatus = status;
    }    
    
    /**
	 * @return the companyComments
	 */
	public String getCompanyComments() {
		return companyComments;
	}

	/**
	 * @param companyComments the companyComments to set
	 */
	public void setCompanyComments(String companyComments) {
		this.companyComments = companyComments;
	}

	/**
	 * @return the emailAddressFlag
	 */
	public String getEmailAddressFlag() {
		return emailAddressFlag;
	}

	/**
	 * @param emailAddressFlag the emailAddressFlag to set
	 */
	public void setEmailAddressFlag(String emailAddressFlag) {
		this.emailAddressFlag = emailAddressFlag;
	}

	/**
	 * @return the physicalAddressFlag
	 */
	public String getPhysicalAddressFlag() {
		return physicalAddressFlag;
	}

	/**
	 * @param physicalAddressFlag the physicalAddressFlag to set
	 */
	public void setPhysicalAddressFlag(String physicalAddressFlag) {
		this.physicalAddressFlag = physicalAddressFlag;
	}

	/**
	 * @return the welcomeLetterFlag
	 */
	public String getWelcomeLetterFlag() {
		return welcomeLetterFlag;
	}

	/**
	 * @param welcomeLetterFlag the welcomeLetterFlag to set
	 */
	public void setWelcomeLetterFlag(String welcomeLetterFlag) {
		this.welcomeLetterFlag = welcomeLetterFlag;
	}

	/**
	 * @return the welcomeLetterSentDt
	 */
	public Date getWelcomeLetterSentDt() {
		return welcomeLetterSentDt;
	}

	/**
	 * @param welcomeLetterSentDt the welcomeLetterSentDt to set
	 */
	public void setWelcomeLetterSentDt(Date welcomeLetterSentDt) {
		this.welcomeLetterSentDt = welcomeLetterSentDt;
	}

}

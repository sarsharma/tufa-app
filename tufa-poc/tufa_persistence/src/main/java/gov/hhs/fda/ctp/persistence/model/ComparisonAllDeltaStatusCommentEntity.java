package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "TU_ALL_DELTA_COMMENT ")
public class ComparisonAllDeltaStatusCommentEntity extends BaseEntity {
    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
  
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COMMENT_SEQ", unique = true, nullable = false)
    public Long commentSeq;
    
   
    @Column(name="USER_COMMENT")
    public String userComment;
    
    @Column(name="COMMENT_AUTHOR")
    public String author;
    
    @Temporal(TemporalType.DATE) 
    @CreationTimestamp
    @Column(name="COMMENT_DATE")
    public Date commentDate;
    
    @Column(name="COMMENT_RESOLVED")
    public Boolean resolveComment;
    
    @Column(name = "MODIFIED_BY")
    public String modifiedBy;

	@UpdateTimestamp
	@Column(name = "MODIFIED_DT")
	public Date modifiedDt;

	@Column(name="COMMENT_QTRS")
    public String commentQtrs;
	
	@Column(name="MATCH_COMM_SEQ")
    public Integer matchCommSeq;


	

	@ManyToMany(mappedBy = "userComments")
	public List<ComparisonAllDeltaStatusEntity> allDeltaStatuses;

	/**
	 * @return the commentSeq
	 */
	public Long getCommentSeq() {
		return commentSeq;
	}

	/**
	 * @param commentSeq the commentSeq to set
	 */
	public void setCommentSeq(Long commentSeq) {
		this.commentSeq = commentSeq;
	}

	/**
	 * @return the comment
	 */
	public String getUserComment() {
		return userComment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setUserComment(String comment) {
		this.userComment = comment;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the commentDate
	 */
	public Date getCommentDate() {
		return commentDate;
	}

	/**
	 * @param commentDate the commentDate to set
	 */
	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public Boolean getResolveComment() {
		return resolveComment;
	}

	public void setResolveComment(Boolean resolveComment) {
		this.resolveComment = resolveComment;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
    
	
	public List<ComparisonAllDeltaStatusEntity> getDeltaStatuses() {
		return allDeltaStatuses;
	}

	public void setDeltaStatuses(List<ComparisonAllDeltaStatusEntity> allDeltaStatuses) {
		this.allDeltaStatuses = allDeltaStatuses;
	}

	public String getCommentQtrs() {
		return commentQtrs;
	}

	public void setCommentQtrs(String commentQtrs) {
		this.commentQtrs = commentQtrs;
	}

	public Integer getMatchCommSeq() {
		return matchCommSeq;
	}

	public void setMatchCommSeq(Integer matchCommSeq) {
		this.matchCommSeq = matchCommSeq;
	}

	@Override
	public int hashCode() {
		final int prime = 53;
		int result = (int) (this.commentSeq ^ (this.commentSeq >>> 32));
		result = prime * result + ((commentSeq == null) ? 0 : commentSeq.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		
		if (obj != null && getClass() == obj.getClass()) {
			if (this == obj)
				return true;
			if (((ComparisonAllDeltaStatusCommentEntity) obj).getCommentSeq() == null)
				return false;

			if (((ComparisonAllDeltaStatusCommentEntity) obj).getCommentSeq().equals(this.getCommentSeq()))
				return true;
		}
		
		return false;
	}
}

package gov.hhs.fda.ctp.persistence.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="TU_COMPARISON_ALL_DELTA_STS")
public class ComparisonAllDeltaStatusEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CMP_ALL_DELTA_ID", nullable = false)
    private Long cmpAllDeltaId;
	
	@Column(name = "EIN")
    private String ein;
	
	@Column(name = "FISCAL_YR")
    private String fiscalYr;
	
	@Column(name = "FISCAL_QTR")
    private String fiscalQtr;
	
	@Column(name = "TOBACCO_CLASS_NM")
    private String tobaccoClassName;
	
	@Column(name = "PERMIT_TYPE")
    private String permitType;
	
	@Column(name = "STATUS")
    private String deltaStatus;
	
	@Column(name = "DELTA")
    private Double delta;
	
	@Column(name = "TOBACCO_SUB_TYPE_1")
    private String tobaccoSubType1;
	
	@Column(name = "TOBACCO_SUB_TYPE_1_AMNDTX")
    private Double tobaccoSubType1Amndtx;
	
	@Column(name="TOBACCO_SUB_TYPE_1_POUNDS")
	private Double tobaccoSubType1Pounds;
	
	@Column(name = "TOBACCO_SUB_TYPE_2")
    private String tobaccoSubType2;
	
	@Column(name = "TOBACCO_SUB_TYPE_2_AMNDTX")
    private Double tobaccoSubType2Amndtx;
	
	@Column(name="TOBACCO_SUB_TYPE_2_POUNDS")
	private Double tobaccoSubType2Pounds;
	
	@Column(name = "DELTA_TYPE")
    private String deltaType;
	
	//@Column(name = "DELTA_COMMENT")
	//private String deltaComment;
	
	@Column(name="FILE_TYPE")
	private String fileType;
	
	@Column(name="PREV_STATUS")
	private String previousStatus;
	
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "TU_ALL_DELTA_JOIN_COMMENT", 
        joinColumns = { @JoinColumn(name = "CMP_ALL_DELTA_ID") }, 
        inverseJoinColumns = { @JoinColumn(name = "COMMENT_SEQ") }
    )
    public List<ComparisonAllDeltaStatusCommentEntity> userComments;
	
	public Double getTobaccoSubType1Pounds() {
		return tobaccoSubType1Pounds;
	}

	public void setTobaccoSubType1Pounds(Double tobaccoSubType1Pounds) {
		this.tobaccoSubType1Pounds = tobaccoSubType1Pounds;
	}

	public Double getTobaccoSubType2Pounds() {
		return tobaccoSubType2Pounds;
	}

	public void setTobaccoSubType2Pounds(Double tobaccoSubType2Pounds) {
		this.tobaccoSubType2Pounds = tobaccoSubType2Pounds;
	}


	public Long getCmpAllDeltaId() {
		return cmpAllDeltaId;
	}

	public void setCmpAllDeltaId(Long cmpAllDeltaId) {
		this.cmpAllDeltaId = cmpAllDeltaId;
	}

	public String getEin() {
		return ein;
	}

	public void setEin(String ein) {
		this.ein = ein;
	}


	public String getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getFiscalQtr() {
		return fiscalQtr;
	}

	public void setFiscalQtr(String fiscalQtr) {
		this.fiscalQtr = fiscalQtr;
	}

	public String getTobaccoClassName() {
		return tobaccoClassName;
	}

	public void setTobaccoClassName(String tobaccoClassName) {
		this.tobaccoClassName = tobaccoClassName;
	}

	public String getPermitType() {
		return permitType;
	}

	public void setPermitType(String permitType) {
		this.permitType = permitType;
	}

	public String getDeltaStatus() {
		return deltaStatus;
	}

	public void setDeltaStatus(String deltaStatus) {
		this.deltaStatus = deltaStatus;
	}

	public Double getDelta() {
		return delta;
	}

	public void setDelta(Double delta) {
		this.delta = delta;
	}

	public String getTobaccoSubType1() {
		return tobaccoSubType1;
	}

	public void setTobaccoSubType1(String tobaccoSubType1) {
		this.tobaccoSubType1 = tobaccoSubType1;
	}

	

	public String getTobaccoSubType2() {
		return tobaccoSubType2;
	}

	public void setTobaccoSubType2(String tobaccoSubType2) {
		this.tobaccoSubType2 = tobaccoSubType2;
	}

	public Double getTobaccoSubType1Amndtx() {
		return tobaccoSubType1Amndtx;
	}

	public void setTobaccoSubType1Amndtx(Double tobaccoSubType1Amndtx) {
		this.tobaccoSubType1Amndtx = tobaccoSubType1Amndtx;
	}

	public Double getTobaccoSubType2Amndtx() {
		return tobaccoSubType2Amndtx;
	}

	public void setTobaccoSubType2Amndtx(Double tobaccoSubType2Amndtx) {
		this.tobaccoSubType2Amndtx = tobaccoSubType2Amndtx;
	}

	public String getDeltaType() {
		return deltaType;
	}

	public void setDeltaType(String deltaType) {
		this.deltaType = deltaType;
	}
	
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * @return the deltaComment
	 */
	/*public String getDeltaComment() {
		return deltaComment;
	}

	*//**
	 * @param deltaComment the deltaComment to set
	 *//*
	public void setDeltaComment(String deltaComment) {
		this.deltaComment = deltaComment;
	}*/
	
	public List<ComparisonAllDeltaStatusCommentEntity> getUserComments() {
		return userComments;
	}

	public void setUserComments(List<ComparisonAllDeltaStatusCommentEntity> userComments) {
		this.userComments = userComments;
	}
	
	public void setPreviousStatus(String status) {
		this.previousStatus = status;
	}
	
	public String getPreviousStatus() {
		return this.previousStatus;
	}
	
}

package gov.hhs.fda.ctp.persistence.model;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class AttachmentsEntity.
 *
 * @author rnasina
 */
@Entity
@Table(name="TU_COMPARISON_COMMENT_DOC")
public class ComparisonCommentAttachmentEntity extends BaseEntity{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The document id. */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DOCUMENT_ID", unique = true, nullable=false) 
    private long documentId;
    
	/** The CBP COMMENT_SEQ. */
	@Column(name = "CBP_COMMENT_SEQ") 
    private long cbpCommentSeq;
	
	/** The TTB COMMENT_SEQ. */
	@Column(name = "TTB_COMMENT_SEQ") 
    private long ttbCommentSeq;
	
	/** The ALL DELTA COMMENT_SEQ. */
	@Column(name = "ALLDELTA_COMMENT_SEQ") 
    private long allDeltaCommentSeq;
   
    
	/** The doc file nm. */
	@Column(name = "DOC_FILE_NM", columnDefinition="CHAR(4)", nullable=false)
    private String docFileNm;
    
	
	/** The doc desc. */
    @Column(name = "DOC_DESC", length = 240)
    private String docDesc;
   
	/** The doc status cd. */
	@Column(name = "DOC_STATUS_CD", columnDefinition="CHAR(4)")
    private String docStatusCd;
    
	 /** The report pdf. */
    @Lob @Basic(fetch = FetchType.LAZY)
	@Column(name="REPORT_PDF")
    private Blob reportPdf;
    
    /** The created by. */
    @Column(name = "CREATED_BY", length=50)
    private String createdBy;
    

	/** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;
    
    /** The modified by. */
    @Column(name = "MODIFIED_BY", length=50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;

	public long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}

	public long getCbpCommentSeq() {
		return cbpCommentSeq;
	}

	public void setCbpCommentSeq(long cbpCommentSeq) {
		this.cbpCommentSeq = cbpCommentSeq;
	}

	public long getTtbCommentSeq() {
		return ttbCommentSeq;
	}

	public void setTtbCommentSeq(long ttbCommentSeq) {
		this.ttbCommentSeq = ttbCommentSeq;
	}

	public long getAllDeltaCommentSeq() {
		return allDeltaCommentSeq;
	}

	public void setAllDeltaCommentSeq(long allDeltaCommentSeq) {
		this.allDeltaCommentSeq = allDeltaCommentSeq;
	}

	public String getDocFileNm() {
		return docFileNm;
	}

	public void setDocFileNm(String docFileNm) {
		this.docFileNm = docFileNm;
	}

	public String getDocDesc() {
		return docDesc;
	}

	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}

	public String getDocStatusCd() {
		return docStatusCd;
	}

	public void setDocStatusCd(String docStatusCd) {
		this.docStatusCd = docStatusCd;
	}

	public Blob getReportPdf() {
		return reportPdf;
	}

	public void setReportPdf(Blob reportPdf) {
		this.reportPdf = reportPdf;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
    
    
    
    }

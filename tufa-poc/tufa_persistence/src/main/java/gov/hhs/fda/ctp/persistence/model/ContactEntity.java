/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class ContactEntity.
 *
 * @author tgunter
 */
@Entity
@Table(name = "tu_contact")
public class ContactEntity extends BaseEntity implements Comparable<ContactEntity> {
    
    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
 
    /** The contact id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CONTACT_ID", unique = true, nullable = false)
    private Long contactId;

    /** The company. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ID", nullable=false)
    private CompanyEntity company;
    
    /** The last nm. */
    @Column(name = "LAST_NM", length = 40)
    private String lastNm;
    
    /** The first nm. */
    @Column(name = "FIRST_NM", length = 40)
    private String firstNm;
    
    /** The phone num. */
    @Column(name = "PHONE_NUM")
    private String phoneNum;

    /** The phone ext. */
    @Column(name = "PHONE_EXT")
    private String phoneExt;
    
    /** The email address. */
    @Column(name = "EMAIL_ADDRESS", length = 50)
    private String emailAddress;
    
    /** The country cd. */
    @Column(name = "COUNTRY_CD", length = 2)
    private String countryCd;
    
    @Column(name = "CNTRY_DIAL_CD", length = 2)
    private String cntryDialCd;

    /** The fax num. */
    @Column(name = "FAX_NUM")
    private String faxNum;

    /** The created by. */
    @Column(name = "CREATED_BY", length = 50, updatable=false)
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT", insertable = false, updatable = false)
    private Date createdDt;
    
    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")    
    private Date modifiedDt;
    
    @Column(name = "FAX_CNTRY_DIAL_CD", length = 2)
    private String cntryFaxDialCd;    
    
    @Column(name = "HASH_TOTAL")
    private Integer hashTotal;
    
    /** The sequnce number for primary contact */
    @Column(name = "PRIMARY_CONTACT_SEQUENCE")
    private Long primaryContactSequence;
    
    /**
     * Gets the contact id.
     *
     * @return the contactId
     */
    public Long getContactId() {
        return contactId;
    }

    /**
     * Sets the contact id.
     *
     * @param contactId the contactId to set
     */
    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    /**
     * Gets the last nm.
     *
     * @return the lastNm
     */
    public String getLastNm() {
        return lastNm;
    }

    /**
     * Sets the last nm.
     *
     * @param lastNm the lastNm to set
     */
    public void setLastNm(String lastNm) {
        this.lastNm = lastNm;
    }

    /**
     * Gets the first nm.
     *
     * @return the firstNm
     */
    public String getFirstNm() {
        return firstNm;
    }

    /**
     * Sets the first nm.
     *
     * @param firstNm the firstNm to set
     */
    public void setFirstNm(String firstNm) {
        this.firstNm = firstNm;
    }

    /**
     * Gets the phone num.
     *
     * @return the phoneNum
     */
    public String getPhoneNum() {
        return phoneNum;
    }

    /**
     * Sets the phone num.
     *
     * @param phoneNum the phoneNum to set
     */
    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getCntryDialCd() {
		return cntryDialCd;
	}

	public void setCntryDialCd(String cntryDialCd) {
		this.cntryDialCd = cntryDialCd;
	}

	/**
     * Gets the email address.
     *
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the email address.
     *
     * @param emailAddress the emailAddress to set
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

	/**
	 * Gets the country cd.
	 *
	 * @return the countryCd
	 */
	public String getCountryCd() {
		return countryCd;
	}

	/**
	 * Sets the country cd.
	 *
	 * @param countryCd the countryCd to set
	 */
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}

    /**
     * Gets the created by.
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the created by.
     *
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Gets the created dt.
     *
     * @return the createdDt
     */
    public Date getCreatedDt() {
        return createdDt;
    }

    /**
     * Sets the created dt.
     *
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    /**
     * Gets the modified by.
     *
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * Sets the modified by.
     *
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * Gets the modified dt.
     *
     * @return the modifiedDt
     */
    public Date getModifiedDt() {
        return modifiedDt;
    }

    /**
     * Sets the modified dt.
     *
     * @param modifiedDt the modifiedDt to set
     */
    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }


	public String getCntryFaxDialCd() {
		return cntryFaxDialCd;
	}

	public void setCntryFaxDialCd(String cntryFaxDialCd) {
		this.cntryFaxDialCd = cntryFaxDialCd;
	}

	/**
	 * Gets the company.
	 *
	 * @return the company
	 */
	public CompanyEntity getCompany() {
		return company;
	}

	/**
	 * Sets the company.
	 *
	 * @param company the company to set
	 */
	public void setCompany(CompanyEntity company) {
		this.company = company;
	}

	/**
	 * Compare the contacts using created date.
	 * @param contact
	 * @return
	 */
	@Override
	public int compareTo(ContactEntity contact) {
		if(this.getPrimaryContactSequence() == null){
			return -1;
		} else if (contact.getPrimaryContactSequence() == null) {
			return 1;
		} else {
			return contact.getPrimaryContactSequence().compareTo(this.getPrimaryContactSequence());
		}
	}

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
    	String hashString = this.lastNm + " " + this.firstNm + " " + this.emailAddress + " " + this.contactId;
    	return hashString.hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if(obj != null && getClass() == obj.getClass()) {
            if(this == obj || ((ContactEntity)obj).hashCode() == this.hashCode()) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[last name=").append(this.lastNm).append("]");
        return builder.toString();
    }

	/**
	 * @return the faxNum
	 */
	public String getFaxNum() {
		return faxNum;
	}

	/**
	 * @param faxNum the faxNum to set
	 */
	public void setFaxNum(String faxNum) {
		this.faxNum = faxNum;
	}

	/**
	 * @return the phoneExt
	 */
	public String getPhoneExt() {
		return phoneExt;
	}

	/**
	 * @param phoneExt the phoneExt to set
	 */
	public void setPhoneExt(String phoneExt) {
		this.phoneExt = phoneExt;
	}

	/**
	 * @return the hashTotal
	 */
	public Integer getHashTotal() {
		return hashTotal;
	}

	/**
	 * @param hashTotal the hashTotal to set
	 */
	public void setHashTotal(Integer hashTotal) {
		this.hashTotal = hashTotal;
	}

	/**
	 * @return the primaryContactSequence
	 */
	public Long getPrimaryContactSequence() {
		return primaryContactSequence;
	}

	/**
	 * @param primaryContactSequence the primaryContactSequence to set
	 */
	public void setPrimaryContactSequence(Long primaryContactSequence) {
		this.primaryContactSequence = primaryContactSequence;
	}
	
}

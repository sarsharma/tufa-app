/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class CountryEntity.
 *
 * @author tgunter
 */
@Entity
@Table(name = "TU_ISO_CNTRY")
public class CountryEntity extends BaseEntity implements Comparator<CountryEntity> {
    
    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;

    /** The country cd. */
    @Id
    @Column(name = "ISO_CNTRY_CD", nullable = false, length = 2)
    private String countryCd;

    /** The country nm. */
    @Column(name = "ISO_COUNTRY_NM", length = 45)
    private String countryNm;

    /** The country dial cd. */
    @Column(name = "CNTRY_DIAL_CD", length = 5)
    private String countryDialCd;


    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return countryCd.hashCode();
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if(obj != null && getClass() == obj.getClass()) {
            if(this == obj || ((CountryEntity)obj).getCountryCd().equals(this.countryCd)) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.countryNm;
    }

    /**
     * Gets the country cd.
     *
     * @return the countryCd
     */
    public String getCountryCd() {
        return countryCd;
    }

    /**
     * Sets the country cd.
     *
     * @param countryCd the countryCd to set
     */
    public void setCountryCd(String countryCd) {
        this.countryCd = countryCd;
    }

    /**
     * Gets the country nm.
     *
     * @return the countryNm
     */
    public String getCountryNm() {
        return countryNm;
    }

    /**
     * Sets the country nm.
     *
     * @param countryNm the countryNm to set
     */
    public void setCountryNm(String countryNm) {
        this.countryNm = countryNm;
    }

    /**
     * Gets the country dial cd.
     *
     * @return the countryDialCd
     */
    public String getCountryDialCd() {
        return countryDialCd;
    }

    /**
     * Sets the country dial cd.
     *
     * @param countryDialCd the countryDialCd to set
     */
    public void setCountryDialCd(String countryDialCd) {
        this.countryDialCd = countryDialCd;
    }
    
    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(CountryEntity country1, CountryEntity country2) {
        return country1.getCountryNm().compareTo(country2.getCountryNm());
    }

}

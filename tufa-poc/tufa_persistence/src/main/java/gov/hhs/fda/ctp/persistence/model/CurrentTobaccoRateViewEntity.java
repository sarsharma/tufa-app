/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;

/**
 * @author Anthony.Gunter
 *
 */
@Entity
@Immutable
@Table(name="CURRENT_TOBACCO_RATES_VW")
public class CurrentTobaccoRateViewEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="TOBACCO_CLASS_ID")
	private Long tobaccoClassId;
	
	@Column(name="PARENT_CLASS_ID")
	private Long parentClassId;

	@Column(name="TOBACCO_CLASS_NM")
	private String tobaccoClassNm;

	@Column(name="CLASS_TYPE_CD")
	private String classTypeCd;

	@Column(name="TAX_RATE_ID")
	private Long taxRateId;
	
    /** The effective dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="TAX_RATE_EFFECTIVE_DATE")
    private Date taxRateEffectiveDate;

    /** The effective dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="TAX_RATE_END_DATE")
    private Date taxRateEndDate;
	
	@Column(name="TAX_RATE")
	private Double taxRate;
	
	@Column(name="CONVERSION_RATE")
	private Double conversionRate;

	/**
	 * @return the tobaccoClassId
	 */
	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}

	/**
	 * @param tobaccoClassId the tobaccoClassId to set
	 */
	public void setTobaccoClassId(Long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}

	/**
	 * @return the parentClassId
	 */
	public Long getParentClassId() {
		return parentClassId;
	}

	/**
	 * @param parentClassId the parentClassId to set
	 */
	public void setParentClassId(Long parentClassId) {
		this.parentClassId = parentClassId;
	}

	/**
	 * @return the tobaccoClassNm
	 */
	public String getTobaccoClassNm() {
		return tobaccoClassNm;
	}

	/**
	 * @param string the tobaccoClassNm to set
	 */
	public void setTobaccoClassNm(String string) {
		this.tobaccoClassNm = string;
	}

	/**
	 * @return the classTypeCd
	 */
	public String getClassTypeCd() {
		return classTypeCd;
	}

	/**
	 * @param classTypeCd the classTypeCd to set
	 */
	public void setClassTypeCd(String classTypeCd) {
		this.classTypeCd = classTypeCd;
	}

	/**
	 * @return the taxRateId
	 */
	public Long getTaxRateId() {
		return taxRateId;
	}

	/**
	 * @param taxRateId the taxRateId to set
	 */
	public void setTaxRateId(Long taxRateId) {
		this.taxRateId = taxRateId;
	}

	/**
	 * @return the taxRateEffectiveDate
	 */
	public Date getTaxRateEffectiveDate() {
		return taxRateEffectiveDate;
	}

	/**
	 * @param taxRateEffectiveDate the taxRateEffectiveDate to set
	 */
	public void setTaxRateEffectiveDate(Date taxRateEffectiveDate) {
		this.taxRateEffectiveDate = taxRateEffectiveDate;
	}

	/**
	 * @return the taxRateEndDate
	 */
	public Date getTaxRateEndDate() {
		return taxRateEndDate;
	}

	/**
	 * @param taxRateEndDate the taxRateEndDate to set
	 */
	public void setTaxRateEndDate(Date taxRateEndDate) {
		this.taxRateEndDate = taxRateEndDate;
	}

	/**
	 * @return the taxRate
	 */
	public Double getTaxRate() {
		return taxRate;
	}

	/**
	 * @param taxRate the taxRate to set
	 */
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	/**
	 * @return the conversionRate
	 */
	public Double getConversionRate() {
		return conversionRate;
	}

	/**
	 * @param conversionRate the conversionRate to set
	 */
	public void setConversionRate(Double conversionRate) {
		this.conversionRate = conversionRate;
	}

}

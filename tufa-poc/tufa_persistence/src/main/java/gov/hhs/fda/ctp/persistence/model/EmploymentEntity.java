package gov.hhs.fda.ctp.persistence.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class EmploymentEntity.
 */
@Entity
@Table(name = "Employment")
public class EmploymentEntity {
	
	 	/** The user id. */
	 	@Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    @Column(name = "COMPANY_ID", unique = true, nullable = false)
	    private Long userId;

	    /** The company name. */
    	@Column(name = "COMPANY_NAME")
	    private String companyName;

	    /** The company address. */
    	@Column(name = "COMPANY_ADDRESS")
	    private String companyAddress;

	    /** The users. */
    	@OneToMany(cascade = CascadeType.ALL)
	    @JoinTable(name = "users_employment", joinColumns = @JoinColumn(name = "company_id", referencedColumnName = "company_id"), inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "user_id"))
	    private List<UserEntity> users;

		/**
		 * Gets the user id.
		 *
		 * @return the user id
		 */
		public Long getUserId() {
			return userId;
		}

		/**
		 * Sets the user id.
		 *
		 * @param userId the new user id
		 */
		public void setUserId(Long userId) {
			this.userId = userId;
		}

		/**
		 * Gets the company name.
		 *
		 * @return the company name
		 */
		public String getCompanyName() {
			return companyName;
		}

		/**
		 * Sets the company name.
		 *
		 * @param companyName the new company name
		 */
		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}

		/**
		 * Gets the company address.
		 *
		 * @return the company address
		 */
		public String getCompanyAddress() {
			return companyAddress;
		}

		/**
		 * Sets the company address.
		 *
		 * @param companyAddress the new company address
		 */
		public void setCompanyAddress(String companyAddress) {
			this.companyAddress = companyAddress;
		}

		/**
		 * Gets the users.
		 *
		 * @return the users
		 */
		public List<UserEntity> getUsers() {
			return users;
		}

		/**
		 * Sets the users.
		 *
		 * @param users the new users
		 */
		public void setUsers(List<UserEntity> users) {
			this.users = users;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
	    public int hashCode() {
	        final int prime = 31;
	        int result = 1;
	        result = (prime * result) + ((companyName == null) ? 0 : companyName.hashCode());
	        return result;
	    }

	    /* (non-Javadoc)
    	 * @see java.lang.Object#equals(java.lang.Object)
    	 */
    	@Override
	    public boolean equals(final Object obj) {
	        if (this == obj) {
	            return true;
	        }
	        if (obj == null) {
	            return false;
	        }
	        if (getClass() != obj.getClass()) {
	            return false;
	        }
	        final EmploymentEntity company = (EmploymentEntity) obj;
	        if (!companyName.equals(company.getCompanyName())) {
	            return false;
	        }
	        return true;
	    }

	    /* (non-Javadoc)
    	 * @see java.lang.Object#toString()
    	 */
    	@Override
	    public String toString() {
	        final StringBuilder builder = new StringBuilder();
	        builder.append("Company [companyName=").append(companyName);
	        return builder.toString();
	    }

	    
}

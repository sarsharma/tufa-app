package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name="EXPORT_2000_FORM_VW")
public class Export2000FormEntity extends BaseEntity {
	@Id
	@Column(name="PERMIT_ID")
	private Long permitId;
	
	@Id
	@Column(name="PERIOD_ID")
	private Long periodId;
	
	@Id
	@Column(name="TOBACCO_CLASS_NM")
	private String tobaccoClassNm;

	@Column(name="REMOVAL_QTY")
	private Double removalQty;
	
	@Column(name="SMALL_REMOVAL_QTY")
	private Double smallRemovalQty;

	@Column(name="LARGE_REMOVAL_QTY")
	private Double largeRemovalQty;
	
	@Column(name="DOL_DIFFERENCE")
	private Double dolDifference;
	
	@Column(name="ACTIVITY_CD")
	private String activityCd;
	
	@Column(name="ADJUSTMENT_REMOVAL")
	private Double adjustmentRemoval;
	
	@Column(name="GRAND_REMOVAL_TOTAL")
	private Double grandTotalRemoval;

	/**
	 * @return the permitId
	 */
	public Long getPermitId() {
		return permitId;
	}

	/**
	 * @param permitId the permitId to set
	 */
	public void setPermitId(Long permitId) {
		this.permitId = permitId;
	}

	/**
	 * @return the periodId
	 */
	public Long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId the periodId to set
	 */
	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the tobaccoClassNm
	 */
	public String getTobaccoClassNm() {
		return tobaccoClassNm;
	}

	/**
	 * @param tobaccoClassNm the tobaccoClassNm to set
	 */
	public void setTobaccoClassNm(String tobaccoClassNm) {
		this.tobaccoClassNm = tobaccoClassNm;
	}

	/**
	 * @return the removalQty
	 */
	public Double getRemovalQty() {
		return removalQty;
	}

	/**
	 * @param removalQty the removalQty to set
	 */
	public void setRemovalQty(Double removalQty) {
		this.removalQty = removalQty;
	}

	/**
	 * @return the smallRemovalQty
	 */
	public Double getSmallRemovalQty() {
		return smallRemovalQty;
	}

	/**
	 * @param smallRemovalQty the smallRemovalQty to set
	 */
	public void setSmallRemovalQty(Double smallRemovalQty) {
		this.smallRemovalQty = smallRemovalQty;
	}

	/**
	 * @return the largeRemovalQty
	 */
	public Double getLargeRemovalQty() {
		return largeRemovalQty;
	}

	/**
	 * @param largeRemovalQty the largeRemovalQty to set
	 */
	public void setLargeRemovalQty(Double largeRemovalQty) {
		this.largeRemovalQty = largeRemovalQty;
	}

	/**
	 * @return the dolDifference
	 */
	public Double getDolDifference() {
		return dolDifference;
	}

	/**
	 * @param dolDifference the dolDifference to set
	 */
	public void setDolDifference(Double dolDifference) {
		this.dolDifference = dolDifference;
	}

	/**
	 * @return the activityCd
	 */
	public String getActivityCd() {
		return activityCd;
	}

	/**
	 * @param activityCd the activityCd to set
	 */
	public void setActivityCd(String activityCd) {
		this.activityCd = activityCd;
	}

	public Double getAdjustmentRemoval() {
		return adjustmentRemoval;
	}

	public void setAdjustmentRemoval(Double adjustmentRemoval) {
		this.adjustmentRemoval = adjustmentRemoval;
	}

	public Double getGrandTotalRemoval() {
		return grandTotalRemoval;
	}

	public void setGrandTotalRemoval(Double grandTotalRemoval) {
		this.grandTotalRemoval = grandTotalRemoval;
	}
}

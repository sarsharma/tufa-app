package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name="EXPORT_3852_FORM_VW")
public class Export3852FormEntity extends BaseEntity {
	
	@Id
	@Column(name="PERMIT_ID")
	private Long permitId;
	
	@Id
	@Column(name="PERIOD_ID")
	private Long periodId;
	
	@Id
	@Column(name="TOBACCO_CLASS_NM")
	private String tobaccoClassNm;

	@Column(name="REMOVAL_QTY")
	private Double removalQty;

	@Column(name="TAX_RATE")
	private Double taxRate;
	
	@Column(name="TAXES_PAID")
	private Double taxesPaid;
	
	@Column(name="DOL_DIFFERENCE")
	private Double dolDifference;
	
	@Column(name="CALC_TAX_RATE")
	private String calcTaxRate;
	
	@Column(name="ACTIVITY_CD")
	private String activityCd;

	/**
	 * @return the permitId
	 */
	public Long getPermitId() {
		return permitId;
	}

	/**
	 * @param permitId the permitId to set
	 */
	public void setPermitId(Long permitId) {
		this.permitId = permitId;
	}

	/**
	 * @return the periodId
	 */
	public Long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId the periodId to set
	 */
	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the tobaccoClassNm
	 */
	public String getTobaccoClassNm() {
		return tobaccoClassNm;
	}

	/**
	 * @param tobaccoClassNm the tobaccoClassNm to set
	 */
	public void setTobaccoClassNm(String tobaccoClassNm) {
		this.tobaccoClassNm = tobaccoClassNm;
	}

	/**
	 * @return the removalQty
	 */
	public Double getRemovalQty() {
		return removalQty;
	}

	/**
	 * @param removalQty the removalQty to set
	 */
	public void setRemovalQty(Double removalQty) {
		this.removalQty = removalQty;
	}

	/**
	 * @return the taxRate
	 */
	public Double getTaxRate() {
		return taxRate;
	}

	/**
	 * @param taxRate the taxRate to set
	 */
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	/**
	 * @return the taxesPaid
	 */
	public Double getTaxesPaid() {
		return taxesPaid;
	}

	/**
	 * @param taxesPaid the taxesPaid to set
	 */
	public void setTaxesPaid(Double taxesPaid) {
		this.taxesPaid = taxesPaid;
	}

	/**
	 * @return the dolDifference
	 */
	public Double getDolDifference() {
		return dolDifference;
	}

	/**
	 * @param dolDifference the dolDifference to set
	 */
	public void setDolDifference(Double dolDifference) {
		this.dolDifference = dolDifference;
	}

	/**
	 * @return the calcTaxRate
	 */
	public String getCalcTaxRate() {
		return calcTaxRate;
	}

	/**
	 * @param calcTaxRate the calcTaxRate to set
	 */
	public void setCalcTaxRate(String calcTaxRate) {
		this.calcTaxRate = calcTaxRate;
	}

	/**
	 * @return the activityCd
	 */
	public String getActivityCd() {
		return activityCd;
	}

	/**
	 * @param activityCd the activityCd to set
	 */
	public void setActivityCd(String activityCd) {
		this.activityCd = activityCd;
	}

}

package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name="EXPORT_5000_FORM_VW")
public class Export5000FormEntity extends BaseEntity {
	@Id
	@Column(name="PERMIT_ID")
	private Long permitId;
	
	@Id
	@Column(name="PERIOD_ID")
	private Long periodId;
	
	@Id
	@Column(name="TOBACCO_CLASS_NM")
	private String tobaccoClassNm;

	@Column(name="TAXES_3852")
	private Double tax3852;

	@Column(name="TAX_AMOUNT_BREAKDOWN")
	private String taxAmountBreakDown;
	
	@Column(name="TAX_IDS")
	private String taxIds;
	
	@Column(name="GRAND_TOTAL")
	private Double grandTotal;
	
	@Column(name="DOL_DIFFERENCE")
	private Double dolDifference;
	
	@Column(name="ACTIVITY_CD")
	private String activityCd;
	
	@Column(name="ADJUSTMENT_TOTAL")
	private Double adjustmentTotal;

	/**
	 * @return the permitId
	 */
	public Long getPermitId() {
		return permitId;
	}

	/**
	 * @param permitId the permitId to set
	 */
	public void setPermitId(Long permitId) {
		this.permitId = permitId;
	}

	/**
	 * @return the periodId
	 */
	public Long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId the periodId to set
	 */
	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the tobaccoClassNm
	 */
	public String getTobaccoClassNm() {
		return tobaccoClassNm;
	}

	/**
	 * @param tobaccoClassNm the tobaccoClassNm to set
	 */
	public void setTobaccoClassNm(String tobaccoClassNm) {
		this.tobaccoClassNm = tobaccoClassNm;
	}

	/**
	 * @return the tax3852
	 */
	public Double getTax3852() {
		return tax3852;
	}

	/**
	 * @param tax3852 the tax3852 to set
	 */
	public void setTax3852(Double tax3852) {
		this.tax3852 = tax3852;
	}

	/**
	 * @return the taxAmountBreakDown
	 */
	public String getTaxAmountBreakDown() {
		return taxAmountBreakDown;
	}

	/**
	 * @param taxAmountBreakDown the taxAmountBreakDown to set
	 */
	public void setTaxAmountBreakDown(String taxAmountBreakDown) {
		this.taxAmountBreakDown = taxAmountBreakDown;
	}

	/**
	 * @return the taxesPaid
	 */
	public Double getGrandTotal() {
		return grandTotal;
	}

	/**
	 * @param taxesPaid the taxesPaid to set
	 */
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	/**
	 * @return the dolDifference
	 */
	public Double getDolDifference() {
		return dolDifference;
	}

	/**
	 * @param dolDifference the dolDifference to set
	 */
	public void setDolDifference(Double dolDifference) {
		this.dolDifference = dolDifference;
	}

	/**
	 * @return the activityCd
	 */
	public String getActivityCd() {
		return activityCd;
	}

	/**
	 * @param activityCd the activityCd to set
	 */
	public void setActivityCd(String activityCd) {
		this.activityCd = activityCd;
	}

	public Double getAdjustmentTotal() {
		return adjustmentTotal;
	}

	public void setAdjustmentTotal(Double adjustmentTotal) {
		this.adjustmentTotal = adjustmentTotal;
	}

	public String getTaxIds() {
		return taxIds;
	}

	public void setTaxIds(String taxIds) {
		this.taxIds = taxIds;
	}
}

package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name="EXPORT_7501_FORM_BODY_VW")
public class Export7501FormBodyEntity extends BaseEntity {
	@Id
	@Column(name="PERMIT_ID")
	private Long permitId;
	
	@Id
	@Column(name="PERIOD_ID")
	private Long periodId;

	@Id
	@Column(name="FORM_NUM")
	private Long formNum;

	@Id
	@Column(name="LINE_NUM")
	private Long lineNum;
	
	@Column(name="TOBACCO_CLASS_NM")
	private String tobaccoClassNm;
	
	@Column(name="SUB_TOBACCO_CLASS_NM")
	private String subTobaccoClassNm;

	@Column(name="REMOVAL_QTY")
	private Double removalQty;
	
	@Column(name="TAXES_PAID")
	private Double taxesPaid;
	
	@Column(name="DOL_DIFFERENCE")
	private Double dolDifference;
	
	@Column(name="CALC_TAX_RATE")
	private String calcTaxRate;

	/**
	 * @return the permitId
	 */
	public Long getPermitId() {
		return permitId;
	}

	/**
	 * @param permitId the permitId to set
	 */
	public void setPermitId(Long permitId) {
		this.permitId = permitId;
	}

	/**
	 * @return the periodId
	 */
	public Long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId the periodId to set
	 */
	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the formNum
	 */
	public Long getFormNum() {
		return formNum;
	}

	/**
	 * @param formNum the formNum to set
	 */
	public void setFormNum(Long formNum) {
		this.formNum = formNum;
	}

	/**
	 * @return the lineNum
	 */
	public Long getLineNum() {
		return lineNum;
	}

	/**
	 * @param lineNum the lineNum to set
	 */
	public void setLineNum(Long lineNum) {
		this.lineNum = lineNum;
	}

	/**
	 * @return the tobaccoClassNm
	 */
	public String getTobaccoClassNm() {
		return tobaccoClassNm;
	}

	/**
	 * @param tobaccoClassNm the tobaccoClassNm to set
	 */
	public void setTobaccoClassNm(String tobaccoClassNm) {
		this.tobaccoClassNm = tobaccoClassNm;
	}

	/**
	 * @return the subTobaccoClassNm
	 */
	public String getSubTobaccoClassNm() {
		return subTobaccoClassNm;
	}

	/**
	 * @param subTobaccoClassNm the subTobaccoClassNm to set
	 */
	public void setSubTobaccoClassNm(String subTobaccoClassNm) {
		this.subTobaccoClassNm = subTobaccoClassNm;
	}

	/**
	 * @return the removalQty
	 */
	public Double getRemovalQty() {
		return removalQty;
	}

	/**
	 * @param removalQty the removalQty to set
	 */
	public void setRemovalQty(Double removalQty) {
		this.removalQty = removalQty;
	}

	/**
	 * @return the taxesPaid
	 */
	public Double getTaxesPaid() {
		return taxesPaid;
	}

	/**
	 * @param taxesPaid the taxesPaid to set
	 */
	public void setTaxesPaid(Double taxesPaid) {
		this.taxesPaid = taxesPaid;
	}

	/**
	 * @return the dolDifference
	 */
	public Double getDolDifference() {
		return dolDifference;
	}

	/**
	 * @param dolDifference the dolDifference to set
	 */
	public void setDolDifference(Double dolDifference) {
		this.dolDifference = dolDifference;
	}

	/**
	 * @return the calcTaxRate
	 */
	public String getCalcTaxRate() {
		return calcTaxRate;
	}

	/**
	 * @param calcTaxRate the calcTaxRate to set
	 */
	public void setCalcTaxRate(String calcTaxRate) {
		this.calcTaxRate = calcTaxRate;
	}
	
	
}

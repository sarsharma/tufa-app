package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name="EXPORT_7501_FORM_HEADER_VW")
public class Export7501FormHeaderEntity extends BaseEntity {
	@Id
	@Column(name="PERMIT_ID")
	private Long permitId;
	
	@Id
	@Column(name="PERIOD_ID")
	private Long periodId;
	
	@Id
	@Column(name="FORM_NUM")
	private Long formNum;
	
	@Column(name="TOBACCO_CLASS_NM")
	private String tobaccoClassNm;
	
	@Column(name="REMOVAL_3852")
	private Double removal3852;

	@Column(name="REMOVAL_QTY")
	private Double removalQty;
	
	@Column(name="DOL_REMOVAL_DIFFERENCE")
	private Double dolRemovalDifference;

	@Column(name="REMOVAL_ACTIVITY")
	private String removalActivityCd;

	@Column(name="TAX_3852")
	private Double tax3852;

	/**
	 * @return the permitId
	 */
	public Long getPermitId() {
		return permitId;
	}

	/**
	 * @param permitId the permitId to set
	 */
	public void setPermitId(Long permitId) {
		this.permitId = permitId;
	}

	/**
	 * @return the periodId
	 */
	public Long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId the periodId to set
	 */
	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the formNum
	 */
	public Long getFormNum() {
		return formNum;
	}

	/**
	 * @param formNum the formNum to set
	 */
	public void setFormNum(Long formNum) {
		this.formNum = formNum;
	}

	/**
	 * @return the tobaccoClassNm
	 */
	public String getTobaccoClassNm() {
		return tobaccoClassNm;
	}

	/**
	 * @param tobaccoClassNm the tobaccoClassNm to set
	 */
	public void setTobaccoClassNm(String tobaccoClassNm) {
		this.tobaccoClassNm = tobaccoClassNm;
	}

	/**
	 * @return the removal3852
	 */
	public Double getRemoval3852() {
		return removal3852;
	}

	/**
	 * @param removal3852 the removal3852 to set
	 */
	public void setRemoval3852(Double removal3852) {
		this.removal3852 = removal3852;
	}

	/**
	 * @return the removalQty
	 */
	public Double getRemovalQty() {
		return removalQty;
	}

	/**
	 * @param removalQty the removalQty to set
	 */
	public void setRemovalQty(Double removalQty) {
		this.removalQty = removalQty;
	}

	/**
	 * @return the dolRemovalDifference
	 */
	public Double getDolRemovalDifference() {
		return dolRemovalDifference;
	}

	/**
	 * @param dolRemovalDifference the dolRemovalDifference to set
	 */
	public void setDolRemovalDifference(Double dolRemovalDifference) {
		this.dolRemovalDifference = dolRemovalDifference;
	}

	/**
	 * @return the removalActivityCd
	 */
	public String getRemovalActivityCd() {
		return removalActivityCd;
	}

	/**
	 * @param removalActivityCd the removalActivityCd to set
	 */
	public void setRemovalActivityCd(String removalActivityCd) {
		this.removalActivityCd = removalActivityCd;
	}

	/**
	 * @return the tax3852
	 */
	public Double getTax3852() {
		return tax3852;
	}

	/**
	 * @param tax3852 the tax3852 to set
	 */
	public void setTax3852(Double tax3852) {
		this.tax3852 = tax3852;
	}

	/**
	 * @return the taxesPaid
	 */
	public Double getTaxesPaid() {
		return taxesPaid;
	}

	/**
	 * @param taxesPaid the taxesPaid to set
	 */
	public void setTaxesPaid(Double taxesPaid) {
		this.taxesPaid = taxesPaid;
	}

	/**
	 * @return the dolDifference
	 */
	public Double getDolDifference() {
		return dolDifference;
	}

	/**
	 * @param dolDifference the dolDifference to set
	 */
	public void setDolDifference(Double dolDifference) {
		this.dolDifference = dolDifference;
	}

	/**
	 * @return the activityCd
	 */
	public String getActivityCd() {
		return activityCd;
	}

	/**
	 * @param activityCd the activityCd to set
	 */
	public void setActivityCd(String activityCd) {
		this.activityCd = activityCd;
	}

	@Column(name="TAXES_PAID")
	private Double taxesPaid;
	
	@Column(name="DOL_DIFFERENCE")
	private Double dolDifference;

	@Column(name="ACTIVITY_CD")
	private String activityCd;

}

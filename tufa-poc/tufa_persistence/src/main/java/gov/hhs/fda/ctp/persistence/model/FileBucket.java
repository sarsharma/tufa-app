package gov.hhs.fda.ctp.persistence.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * The Class FileBucket.
 */
public class FileBucket {

	/** The file. */
	List<MultipartFile> file;
	
	/** The description. */
	String description;
	
	/** The zeroflag. */
	String zeroflag;

	/**
	 * Gets the file.
	 *
	 * @return the file
	 */
	public List<MultipartFile> getFile() {
		return file;
	}

	/**
	 * Sets the file.
	 *
	 * @param file the new file
	 */
	public void setFile(List<MultipartFile> file) {
		this.file = file;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the flag.
	 *
	 * @return the flag
	 */
	public String getFlag() {
		return zeroflag;
	}

	/**
	 * Sets the flag.
	 *
	 * @param zeroflag the new flag
	 */
	public void setFlag(String zeroflag) {
		this.zeroflag = zeroflag;
	}
}
/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author tgunter
 *
 */
@Entity
@Table(name = "TU_FORM_COMMENT")
public class FormCommentEntity extends BaseEntity {
    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
    
    /** The form id. */
    @Column(name="FORM_ID")
    private Long formId;
    
    @Column(name="FORM_NUM")
    private Long formNum;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COMMENT_SEQ", unique = true, nullable = false)
    private Long commentSeq;
    
   
    @Column(name="USER_COMMENT")
    private String userComment;
    
    @Column(name="COMMENT_AUTHOR")
    private String author;
    
    @Temporal(TemporalType.DATE) 
    @CreationTimestamp
    @Column(name="COMMENT_DATE")
    private Date commentDate;
    
    @Column(name="COMMENT_RESOLVED")
    private Boolean resolveComment;
    
    @Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@UpdateTimestamp
	@Column(name = "MODIFIED_DT")
	private Date modifiedDt;


	/**
	 * @return the formId
	 */
	public Long getFormId() {
		return formId;
	}

	/**
	 * @param formId the formId to set
	 */
	public void setFormId(Long formId) {
		this.formId = formId;
	}

	/**
	 * @return the formNum
	 */
	public Long getFormNum() {
		return formNum;
	}

	/**
	 * @param formNum the formNum to set
	 */
	public void setFormNum(Long formNum) {
		this.formNum = formNum;
	}

	/**
	 * @return the commentSeq
	 */
	public Long getCommentSeq() {
		return commentSeq;
	}

	/**
	 * @param commentSeq the commentSeq to set
	 */
	public void setCommentSeq(Long commentSeq) {
		this.commentSeq = commentSeq;
	}

	/**
	 * @return the comment
	 */
	public String getUserComment() {
		return userComment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setUserComment(String comment) {
		this.userComment = comment;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the commentDate
	 */
	public Date getCommentDate() {
		return commentDate;
	}

	/**
	 * @param commentDate the commentDate to set
	 */
	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public Boolean getResolveComment() {
		return resolveComment;
	}

	public void setResolveComment(Boolean resolveComment) {
		this.resolveComment = resolveComment;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
    

}

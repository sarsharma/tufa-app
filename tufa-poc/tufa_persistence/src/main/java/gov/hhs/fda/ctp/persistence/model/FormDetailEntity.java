/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class FormDetailEntity.
 *
 * @author tgunter
 */
@Entity
@IdClass(FormDetailPK.class)
@Table(name = "TU_FORM_DETAIL")
public class FormDetailEntity extends BaseEntity {
    
    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
    
    /** The form id. */
    @Id
    @Column(name="FORM_ID")
    private Long formId;
    
    /** The submitted form entity. */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FORM_ID", insertable = false, updatable = false)
    private SubmittedFormEntity submittedFormEntity;
    
    /** The tobacco class id. */
    @Id
    @Column(name="TOBACCO_CLASS_ID")
    private Long tobaccoClassId;
    
    /** The removal qty. */
    @Column(name="REMOVAL_QTY")
    private Double removalQty;
    
    /** The Valorem Dollar Amount. */
    @Column(name="ADVALOREM_DOL")
    private Double valoremDol;
    
    /** The removal uom. */
    @Column(name="REMOVAL_UOM")
    private String removalUom;
    
    /** The taxes paid. */
    @Column(name="TAXES_PAID")
    private Double taxesPaid;
    
    /** The dol difference. */
    @Column(name="DOL_DIFFERENCE")
    private Double dolDifference;
    
    /** The activity cd. */
    @Column(name="ACTIVITY_CD")
    private String activityCd;
    
    /** The created by. */
    @Column(name="CREATED_BY")
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;
    
    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    
    /** The line num. */
    @Column(name="LINE_NUM")
    private Long lineNum;
    
    /** The filer entry num. */
    @Column(name="FILERENTRY_NUM")
    private String filerEntryNum;
    
    @Column(name="CALC_TX_VAR_7501")
    private Double calcTxVar7501;
    
    @Column(name="CALC_TX_ACT_CD_7501")
    private String calcTxActCd7501;
    
    @Column(name="CALC_TAX_RATE")
    private Double calcTaxRate;
    
    @Column(name="DOL_REMOVAL_DIFFERENCE")
    private Double dolRemovalDifference;
    
    @Column(name="REMOVAL_ACTIVITY")
    private String removalActivity;
    
    @Column(name="ADJUSTMENTA_TOTAL")
    private Double adjATotal;
    
    @Column(name="ADJUSTMENTB_TOTAL")
    private Double adjBTotal;
    
    @Column(name="GRAND_TOTAL")
    private Double grandTotal;
    
    @Column(name="ADJUSTMENT_TOTAL")
    private Double adjustmentTotal;
    
    @Column(name="ENABLE_ADJ")
    private String enableAdj;
    
    /** The removal qty. */
    @Column(name="ADJUSTMENT_REMOVAL")
    private Double adjustmentRemoval;
    
    /** The removal qty. */
    @Column(name="GRAND_REMOVAL_TOTAL")
    private Double grandRemovalTotal;
    
   

    /**
     * Gets the form id.
     *
     * @return the formId
     */
    public Long getFormId() {
        return formId;
    }
    
    /**
     * Sets the form id.
     *
     * @param formId the formId to set
     */
    public void setFormId(Long formId) {
        this.formId = formId;
    }
    
    /**
     * Gets the tobacco class id.
     *
     * @return the tobaccoClassId
     */
    public Long getTobaccoClassId() {
        return tobaccoClassId;
    }
    
    /**
     * Sets the tobacco class id.
     *
     * @param tobaccoClassId the tobaccoClassId to set
     */
    public void setTobaccoClassId(Long tobaccoClassId) {
        this.tobaccoClassId = tobaccoClassId;
    }
    
    /**
     * Gets the removal qty.
     *
     * @return the removalQty
     */
    public Double getRemovalQty() {
        return removalQty;
    }
    
    /**
     * Sets the removal qty.
     *
     * @param removalQty the removalQty to set
     */
    public void setRemovalQty(Double removalQty) {
        this.removalQty = removalQty;
    }
    
    public Double getValoremDol() {
		return valoremDol;
	}

	public void setValoremDol(Double valoremDol) {
		this.valoremDol = valoremDol;
	}

	/**
     * Gets the removal uom.
     *
     * @return the removalUom
     */
    public String getRemovalUom() {
        return removalUom;
    }
    
    /**
     * Sets the removal uom.
     *
     * @param removalUom the removalUom to set
     */
    public void setRemovalUom(String removalUom) {
        this.removalUom = removalUom;
    }
    
    /**
     * Gets the taxes paid.
     *
     * @return the taxesPaid
     */
    public Double getTaxesPaid() {
        return taxesPaid;
    }
    
    /**
     * Sets the taxes paid.
     *
     * @param taxesPaid the taxesPaid to set
     */
    public void setTaxesPaid(Double taxesPaid) {
        this.taxesPaid = taxesPaid;
    }
    
    /**
     * Gets the activity cd.
     *
     * @return the activityCd
     */
    public String getActivityCd() {
        return activityCd;
    }
    
    /**
     * Sets the activity cd.
     *
     * @param activityCd the activityCd to set
     */
    public void setActivityCd(String activityCd) {
        this.activityCd = activityCd;
    }
    
    /**
     * Gets the created by.
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }
    
    /**
     * Sets the created by.
     *
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
    /**
     * Gets the created dt.
     *
     * @return the createdDt
     */
    public Date getCreatedDt() {
        return createdDt;
    }
    
    /**
     * Sets the created dt.
     *
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }
    
    /**
     * Gets the modified by.
     *
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }
    
    /**
     * Sets the modified by.
     *
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    
    /**
     * Gets the modified dt.
     *
     * @return the modifiedDt
     */
    public Date getModifiedDt() {
        return modifiedDt;
    }
    
    /**
     * Sets the modified dt.
     *
     * @param modifiedDt the modifiedDt to set
     */
    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }
    
    /**
     * Gets the submitted form entity.
     *
     * @return the submittedFormEntity
     */
    public SubmittedFormEntity getSubmittedFormEntity() {
        return submittedFormEntity;
    }
    
    /**
     * Sets the submitted form entity.
     *
     * @param submittedFormEntity the submittedFormEntity to set
     */
    public void setSubmittedFormEntity(SubmittedFormEntity submittedFormEntity) {
        this.submittedFormEntity = submittedFormEntity;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FormDetailEntity detail = (FormDetailEntity)obj;
        return toString().equals(detail.toString());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("#"+this.formId+"#"+this.lineNum+"#"+this.tobaccoClassId+"#");
        return builder.toString();
    }

    /**
     * Gets the line num.
     *
     * @return the lineNum
     */
    public Long getLineNum() {
        return lineNum;
    }
    
    /**
     * Sets the line num.
     *
     * @param lineNum the lineNum to set
     */
    public void setLineNum(Long lineNum) {
        this.lineNum = lineNum;
    }
    
    /**
     * Gets the dol difference.
     *
     * @return the dolDifference
     */
    public Double getDolDifference() {
        return dolDifference;
    }
    
    /**
     * Sets the dol difference.
     *
     * @param dolDifference the dolDifference to set
     */
    public void setDolDifference(Double dolDifference) {
        this.dolDifference = dolDifference;
    }
    
    /**
     * Gets the filer entry num.
     *
     * @return the filerEntryNum
     */
    public String getFilerEntryNum() {
        return filerEntryNum;
    }
    
    /**
     * Sets the filer entry num.
     *
     * @param filerEntryNum the filerEntryNum to set
     */
    public void setFilerEntryNum(String filerEntryNum) {
        this.filerEntryNum = filerEntryNum;
    }

	public Double getCalcTxVar7501() {
		return calcTxVar7501;
	}

	public void setCalcTxVar7501(Double calcTxVar7501) {
		this.calcTxVar7501 = calcTxVar7501;
	}

	public String getCalcTxActCd7501() {
		return calcTxActCd7501;
	}

	public void setCalcTxActCd7501(String calcTxActCd7501) {
		this.calcTxActCd7501 = calcTxActCd7501;
	}

	/**
	 * @return the calcTaxRate
	 */
	public Double getCalcTaxRate() {
		return calcTaxRate;
	}

	/**
	 * @param calcTaxRate the calcTaxRate to set
	 */
	public void setCalcTaxRate(Double calcTaxRate) {
		this.calcTaxRate = calcTaxRate;
	}

	/**
	 * @return the dolRemovalDifference
	 */
	public Double getDolRemovalDifference() {
		return dolRemovalDifference;
	}

	/**
	 * @param dolRemovalDifference the dolRemovalDifference to set
	 */
	public void setDolRemovalDifference(Double dolRemovalDifference) {
		this.dolRemovalDifference = dolRemovalDifference;
	}

	/**
	 * @return the removalActivity
	 */
	public String getRemovalActivity() {
		return removalActivity;
	}

	/**
	 * @param removalActivity the removalActivity to set
	 */
	public void setRemovalActivity(String removalActivity) {
		this.removalActivity = removalActivity;
	}

	public Double getAdjATotal() {
		return adjATotal;
	}

	public void setAdjATotal(Double adjATotal) {
		this.adjATotal = adjATotal;
	}

	public Double getAdjBTotal() {
		return adjBTotal;
	}

	public void setAdjBTotal(Double adjBTotal) {
		this.adjBTotal = adjBTotal;
	}

	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public Double getAdjustmentTotal() {
		return adjustmentTotal;
	}

	public void setAdjustmentTotal(Double adjustmentTotal) {
		this.adjustmentTotal = adjustmentTotal;
	}

	public String getEnableAdj() {
		return enableAdj;
	}

	public void setEnableAdj(String enableAdj) {
		this.enableAdj = enableAdj;
	}

	public Double getAdjustmentRemoval() {
		return adjustmentRemoval;
	}

	public void setAdjustmentRemoval(Double adjustmentRemoval) {
		this.adjustmentRemoval = adjustmentRemoval;
	}

	public Double getGrandRemovalTotal() {
		return grandRemovalTotal;
	}

	public void setGrandRemovalTotal(Double grandRemovalTotal) {
		this.grandRemovalTotal = grandRemovalTotal;
	}

	
	
	
}

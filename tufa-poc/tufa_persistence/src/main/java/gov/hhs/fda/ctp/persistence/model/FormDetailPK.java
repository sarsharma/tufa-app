/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.io.Serializable;

/**
 * The Class FormDetailPK.
 *
 * @author tgunter
 */
public class FormDetailPK implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The form id. */
    protected Long formId;
    
    /** The tobacco class id. */
    protected Long tobaccoClassId;
    
    /** The line num. */
    protected Long lineNum;
    
    /**
     * Instantiates a new form detail PK.
     */
    public FormDetailPK() {
    	// Empty Constructor
    }

    /**
     * Instantiates a new form detail PK.
     *
     * @param formId the form id
     * @param tobaccoClassId the tobacco class id
     * @param lineNum the line num
     */
    public FormDetailPK(Long formId, Long tobaccoClassId, Long lineNum) {
        this.formId = formId;
        this.tobaccoClassId = tobaccoClassId;
        this.lineNum = lineNum;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FormDetailPK detail = (FormDetailPK)obj;
        return toString().equals(detail.toString());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("#"+this.formId+"#"+this.lineNum+"#"+this.tobaccoClassId+"#");
        return builder.toString();
    }

}

/**
 *
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author rnasina
 *
 */
@Entity
@Table(name = "TU_HTS_CODE")
public class HTSCodeEntity extends BaseEntity {
    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="HTS_CODE_ID", nullable=false)
    private Long htsCodeId;

    @Column(name="TOBACCO_CLASS_ID", nullable=false)
    private Long tobaccoClassId;

    @Column(name="HTS_CODE")
    private String htsCode;

    @Column(name="HTL_CODE_NOTES")
    private String htsCodeNotes;

    /** The created by. */
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="CREATED_DT")
    private Date createdDt;

    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;

    /** The modified dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;


	@OneToOne
	@JoinColumn(name="TOBACCO_CLASS_ID",insertable=false,updatable=false)
	private TobaccoClassEntity tobaccoClass;

    /**
	 * @return the htsCodeId
	 */
	public Long getHtsCodeId() {
		return htsCodeId;
	}

	/**
	 * @param htsCodeId the htsCodeId to set
	 */
	public void setHtsCodeId(Long htsCodeId) {
		this.htsCodeId = htsCodeId;
	}

	/**
	 * @return the tobaccoClassId
	 */
	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}

	/**
	 * @param tobaccoClassId the tobaccoClassId to set
	 */
	public void setTobaccoClassId(Long fiscalYear) {
		this.tobaccoClassId = fiscalYear;
	}

	/**
	 * @return the htsCode
	 */
	public String getHtsCode() {
		return htsCode;
	}

	/**
	 * @param htsCode the htsCode to set
	 */
	public void setHtsCode(String code) {
		this.htsCode = code;
	}

	/**
	 * @return the htsCodeNotes
	 */
	public String getHtsCodeNotes() {
		return htsCodeNotes;
	}

	/**
	 * @param htsCodeNotes the htsCodeNotes to set
	 */
	public void setHtsCodeNotes(String htsCodeNotes) {
		this.htsCodeNotes = htsCodeNotes;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}

	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}

	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public TobaccoClassEntity getTobaccoClass() {
		return tobaccoClass;
	}

	public void setTobaccoClass(TobaccoClassEntity tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}

}

package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Priti
 *
 */
@Entity
@Table(name = "TU_INGESTION_WORKFLOW_CONTEXT")
public class IngestionWorkflowContextEntity extends BaseEntity {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "INGEST_WORKFLOW_CONTEXT_ID",updatable = true, nullable = false)
    private Long ingestionWorkflowId;

    @Column(name="FISCAL_YR", nullable=false)
    private Long fiscalYr;

    @Column(name="CONTEXT")
    private String context;

	public Long getIngestionWorkflowId() {
		return ingestionWorkflowId;
	}

	public void setIngestionWorkflowId(Long ingestionWorkflowId) {
		this.ingestionWorkflowId = ingestionWorkflowId;
	}

	public Long getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}
    
}
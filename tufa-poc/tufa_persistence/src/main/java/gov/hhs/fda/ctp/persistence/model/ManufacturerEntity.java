package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class ManufacturerEntity.
 */
@Entity
@Table(name = "manufacturer")
public class ManufacturerEntity implements TufaEntity{
	
	/** The manufacturer id. */
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "MANUFACTURER_ID", unique = true, nullable = false)
    private Long manufacturerId;
	
	/** The first name. */
	@Column(name = "FIRST_NAME")
    private String firstName;

    /** The last name. */
    @Column(name = "LAST_NAME")
    private String lastName;
    
    /** The gender. */
    @Column(name = "GENDER")
    private String gender;
    
    /** The dob. */
    @Column(name = "DOB")
	private String dob;
    
    /** The selectedstate. */
    @Column(name = "STATE")
	private String selectedstate;

	/**
	 * Gets the manufacturer id.
	 *
	 * @return the manufacturer id
	 */
	public Long getManufacturerId() {
		return manufacturerId;
	}

	/**
	 * Sets the manufacturer id.
	 *
	 * @param manufacturerId the new manufacturer id
	 */
	public void setManufacturerId(Long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the gender.
	 *
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * Sets the gender.
	 *
	 * @param gender the new gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * Gets the dob.
	 *
	 * @return the dob
	 */
	public String getDob() {
		return dob;
	}

	/**
	 * Sets the dob.
	 *
	 * @param dob the new dob
	 */
	public void setDob(String dob) {
		this.dob = dob;
	}

	/**
	 * Gets the selectedstate.
	 *
	 * @return the selectedstate
	 */
	public String getSelectedstate() {
		return selectedstate;
	}

	/**
	 * Sets the selectedstate.
	 *
	 * @param selectedstate the new selectedstate
	 */
	public void setSelectedstate(String selectedstate) {
		this.selectedstate = selectedstate;
	}

}

/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

import org.hibernate.annotations.Immutable;

/**
 * @author tgunter
 *
 */
@Entity
@Immutable
@Table(name="RANKED_QTRLY_ASSESSMENT_VW")
public class MarketShareViewEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ASSESSMENT_ID")
	private Long assessmentId;

	@Id
	@Column(name="VERSION_NUM")
	private Long versionNum;

	@Id
	@Column(name="MS_RANK")
	private Long msRank;
	
	@Id
	@Column(name="TOBACCO_CLASS_NM")
	private String tobaccoClassNm;
	
	@Id
	@Column(name="EIN")
	private String EIN;
	
	
	@Column(name="ASSESSMENT_YR")
	private Long assessmentYr;
	
	@Column(name="ASSESSMENT_QTR")
	private Long assessmentQtr;
	
	@Column(name="LEGAL_NM")
	private String legalName;
	
	@Column(name="AUTHOR")
	private String author;
	
	
	@Column(name="PERMITNUM")
	private String permitNum;
	
	@Column(name="TOT_VOL_REMOVED")
	private Double totVolRemoved;
	
	@Column(name="TOT_TAXES_PAID")
	private Double totTaxesPaid;
	
	@Column(name="SHARE_TOT_TAXES")
	private Double shareTotTaxes;
	
	@Column(name="PREVIOUS_SHARE")
	private Double previousShare;
	
	@Column(name="DELTA_SHARE")
	private Double deltaShare;
	
	@Column(name="STREET_ADDRESS")
	private String streetAddress;
	
	@Column(name="SUITE")
	private String suite;
	
	@Column(name="CITY")
	private String city;
	
	@Column(name="STATE")
	private String state;
	
	@Column(name="PROVINCE")
	private String province;
	
	@Column(name="POSTAL_CD")
	private String postalCd;
	
	@Column(name="CNTRY_DIAL_CD")
	private String cntryDialCd;
	
	@Column(name="PHONE_NUM")
	private String phoneNum;
	
	@Column(name="EMAIL_ADDRESS")
	private String emailAddress;
	
	@Column(name="ADDRESS_CHANGED_FLAG")
	private String addressChangedFlag;
	
	@Column(name="CONTACT_CHANGED_FLAG")
	private String contactChangedFlag;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DT")
	private Date createdDt;
	
	@Column(name="ORIGINAL_CORRECTED")
	private String originalCorrected;
	    
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns( {
        @JoinColumn(name = "ASSESSMENT_ID", referencedColumnName="ASSESSMENT_ID", insertable = false, updatable = false),
        @JoinColumn(name = "VERSION_NUM", referencedColumnName="VERSION_NUM", insertable = false, updatable = false)
    })
    private AssessmentVersionEntity assessmentVersionEntity;

	/**
	 * @return the assessmentId
	 */
	public Long getAssessmentId() {
		return assessmentId;
	}

	/**
	 * @param assessmentId the assessmentId to set
	 */
	public void setAssessmentId(Long assessmentId) {
		this.assessmentId = assessmentId;
	}

	/**
	 * @return the versionNum
	 */
	public Long getVersionNum() {
		return versionNum;
	}

	/**
	 * @param versionNum the versionNum to set
	 */
	public void setVersionNum(Long versionNum) {
		this.versionNum = versionNum;
	}

	/**
	 * @return the msRank
	 */
	public Long getMsRank() {
		return msRank;
	}

	/**
	 * @param msRank the msRank to set
	 */
	public void setMsRank(Long msRank) {
		this.msRank = msRank;
	}

	/**
	 * @return the assessmentYr
	 */
	public Long getAssessmentYr() {
		return assessmentYr;
	}

	/**
	 * @param assessmentYr the assessmentYr to set
	 */
	public void setAssessmentYr(Long assessmentYr) {
		this.assessmentYr = assessmentYr;
	}

	/**
	 * @return the assessmentQtr
	 */
	public Long getAssessmentQtr() {
		return assessmentQtr;
	}

	/**
	 * @param assessmentQtr the assessmentQtr to set
	 */
	public void setAssessmentQtr(Long assessmentQtr) {
		this.assessmentQtr = assessmentQtr;
	}

	/**
	 * @return the legalName
	 */
	public String getLegalName() {
		return legalName;
	}

	/**
	 * @param legalName the legalName to set
	 */
	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	/**
	 * @return the eIN
	 */
	public String getEIN() {
		return EIN;
	}

	/**
	 * @param eIN the eIN to set
	 */
	public void setEIN(String eIN) {
		EIN = eIN;
	}

	/**
	 * @return the permitNum
	 */
	public String getPermitNum() {
		return permitNum;
	}

	/**
	 * @param permitNum the permitNum to set
	 */
	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}

	/**
	 * @return the totVolRemoved
	 */
	public Double getTotVolRemoved() {
		return totVolRemoved;
	}

	/**
	 * @param totVolRemoved the totVolRemoved to set
	 */
	public void setTotVolRemoved(Double totVolRemoved) {
		this.totVolRemoved = totVolRemoved;
	}

	/**
	 * @return the totTaxesPaid
	 */
	public Double getTotTaxesPaid() {
		return totTaxesPaid;
	}

	/**
	 * @param totTaxesPaid the totTaxesPaid to set
	 */
	public void setTotTaxesPaid(Double totTaxesPaid) {
		this.totTaxesPaid = totTaxesPaid;
	}

	/**
	 * @return the shareTotTaxes
	 */
	public Double getShareTotTaxes() {
		return shareTotTaxes;
	}

	/**
	 * @param shareTotTaxes the shareTotTaxes to set
	 */
	public void setShareTotTaxes(Double shareTotTaxes) {
		this.shareTotTaxes = shareTotTaxes;
	}

	public Double getPreviousShare() {
		return previousShare;
	}

	public void setPreviousShare(Double previousShare) {
		this.previousShare = previousShare;
	}

	public Double getDeltaShare() {
		return deltaShare;
	}

	public void setDeltaShare(Double deltaShare) {
		this.deltaShare = deltaShare;
	}

	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}

	/**
	 * @param streetAddress the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	/**
	 * @return the suite
	 */
	public String getSuite() {
		return suite;
	}

	/**
	 * @param suite the suite to set
	 */
	public void setSuite(String suite) {
		this.suite = suite;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	
	/**
	 * @return the state
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param state the state to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * @param province the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * @return the postalCd
	 */
	public String getPostalCd() {
		return postalCd;
	}

	/**
	 * @param postalCd the postalCd to set
	 */
	public void setPostalCd(String postalCd) {
		this.postalCd = postalCd;
	}

	/**
	 * @return the cntryDialCd
	 */
	public String getCntryDialCd() {
		return cntryDialCd;
	}

	/**
	 * @param cntryDialCd the cntryDialCd to set
	 */
	public void setCntryDialCd(String cntryDialCd) {
		this.cntryDialCd = cntryDialCd;
	}

	/**
	 * @return the phoneNum
	 */
	public String getPhoneNum() {
		return phoneNum;
	}

	/**
	 * @param phoneNum the phoneNum to set
	 */
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the tobaccoClassNm
	 */
	public String getTobaccoClassNm() {
		return tobaccoClassNm;
	}

	/**
	 * @param tobaccoClassNm the tobaccoClassNm to set
	 */
	public void setTobaccoClassNm(String tobaccoClassNm) {
		this.tobaccoClassNm = tobaccoClassNm;
	}

	/**
	 * @return the addressChangedFlag
	 */
	public String getAddressChangedFlag() {
		return addressChangedFlag;
	}

	/**
	 * @param addressChangedFlag the addressChangedFlag to set
	 */
	public void setAddressChangedFlag(String addressChangedFlag) {
		this.addressChangedFlag = addressChangedFlag;
	}

	/**
	 * @return the contactChangedFlag
	 */
	public String getContactChangedFlag() {
		return contactChangedFlag;
	}

	/**
	 * @param contactChangedFlag the contactChangedFlag to set
	 */
	public void setContactChangedFlag(String contactChangedFlag) {
		this.contactChangedFlag = contactChangedFlag;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	
    /**
	 * @return the originalCorrected
	 */
	public String getOriginalCorrected() {
		return originalCorrected;
	}

	/**
	 * @param originalCorrected the originalCorrected to set
	 */
	public void setOriginalCorrected(String originalCorrected) {
		this.originalCorrected = originalCorrected;
	}

    /**
	 * @return the assessmentVersionEntity
	 */
	public AssessmentVersionEntity getAssessmentVersionEntity() {
		return assessmentVersionEntity;
	}

	/**
	 * @param assessmentVersionEntity the assessmentVersionEntity to set
	 */
	public void setAssessmentVersionEntity(AssessmentVersionEntity assessmentVersionEntity) {
		this.assessmentVersionEntity = assessmentVersionEntity;
	}

	@Override
    public int hashCode() {
    	return this.toString().hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if(obj != null && getClass() == obj.getClass()) {
            if(this == obj || ((MarketShareViewEntity)obj).hashCode() == this.hashCode()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
    	String returnString = "" + this.assessmentId + " " + this.versionNum + " " + this.msRank + " " + this.tobaccoClassNm;
    	return returnString;
    }

	
}
/**
 *
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class MissingForms.
 *
 * @author rnasina
 */
@Entity
@Table(name = "tu_permit_period_forms")
public class MissingForms extends BaseEntity {

    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;

    /** The permit id. */
    @Id
    @Column(name = "PERMIT_ID", nullable=false)
    private long permitId;

    /** The period id. */
    @Id
    @Column(name = "PERIOD_ID", nullable=false)
    private long periodId;

    @Id
    @Column(name = "FORM_TYPE_CD", length = 4, nullable=false)
    private String formTypeCd;

    @Column(name = "DEFECT_DESC", length = 200)
    private String defectDesc;

    @Column(name = "DEFECT_STATUS_CD", length = 4)
    private String defectStatusCd;

    /** The created by. */
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="CREATED_DT")
    private Date createdDt;

    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;

    /** The modified dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;

    /** The permit period entity. */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns( {
        @JoinColumn(name = "PERMIT_ID", referencedColumnName="PERMIT_ID", insertable = false, updatable = false),
        @JoinColumn(name = "PERIOD_ID", referencedColumnName="PERIOD_ID", insertable = false, updatable = false)
    })
    private PermitPeriodEntity permitPeriodEntity;

    public PermitPeriodEntity getPermitPeriodEntity() {
		return permitPeriodEntity;
	}

	public void setPermitPeriodEntity(PermitPeriodEntity permitPeriodEntity) {
		this.permitPeriodEntity = permitPeriodEntity;
	}

	public long getPermitId() {
		return permitId;
	}

	public void setPermitId(long permitId) {
		this.permitId = permitId;
	}

	public long getPeriodId() {
		return periodId;
	}

	public void setPeriodId(long periodId) {
		this.periodId = periodId;
	}

	public String getFormTypeCd() {
		return formTypeCd;
	}

	public void setFormTypeCd(String formTypeCd) {
		this.formTypeCd = formTypeCd;
	}

	public String getDefectDesc() {
		return defectDesc;
	}

	public void setDefectDesc(String defectDesc) {
		this.defectDesc = defectDesc;
	}

	public String getDefectStatusCd() {
		return defectStatusCd;
	}

	public void setDefectStatusCd(String defectStatusCd) {
		this.defectStatusCd = defectStatusCd;
	}

    /**
     * Gets the created by.
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the created by.
     *
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Gets the created dt.
     *
     * @return the createdDt
     */
    public Date getCreatedDt() {
        return createdDt;
    }

    /**
     * Sets the created dt.
     *
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    /**
     * Gets the modified by.
     *
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * Sets the modified by.
     *
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * Gets the modified dt.
     *
     * @return the modifiedDt
     */
    public Date getModifiedDt() {
        return modifiedDt;
    }

    /**
     * Sets the modified dt.
     *
     * @param modifiedDt the modifiedDt to set
     */
    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }

}

/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class PeriodStatusEntity.
 *
 * @author tgunter
 */
@Entity
@Table(name="TU_PERIOD_STATUS")
public class PeriodStatusEntity extends BaseEntity {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The permit status id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PERMIT_STATUS_ID", unique = true, nullable = false)
    private Long permitStatusId;
    
    /** The permit id. */
    @Column(name = "PERMIT_ID", nullable=false) 
    private Long permitId;
    
    /** The period id. */
    @Column(name = "PERIOD_ID", nullable = false)
    private Long periodId;
    
    /** The period status type cd. */
    @Column(name = "PERIOD_STATUS_TYPE_CD")
    private String periodStatusTypeCd;
    
    @Column(name = "RECON_STATUS_TYPE_CD")
    private String reconStatusTypeCd;
    
    /** The status dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="STATUS_DT")
    private Date statusDt;
    
    /** The created by. */
    @Column(name="CREATED_BY")
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;
    
    /** The modified by. */
    @Column(name="MODIFIED_BY")
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    
    
    /** The modified by. */
    @Column(name="RECON_STATUS_MODIFIED_BY")
    private String reconStatusModifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="RECON_STATUS_MODIFIED_DT")
    private Date reconStatusModifiedDt;
    
    
    /** The permit period entity. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns( {
        @JoinColumn(name = "PERMIT_ID", referencedColumnName="PERMIT_ID", insertable = false, updatable = false),
        @JoinColumn(name = "PERIOD_ID", referencedColumnName="PERIOD_ID", insertable = false, updatable = false)
    })
    private PermitPeriodEntity permitPeriodEntity;

    /**
     * Gets the permit status id.
     *
     * @return the permitStatusId
     */
    public Long getPermitStatusId() {
        return permitStatusId;
    }
    
    /**
     * Sets the permit status id.
     *
     * @param permitStatusId the permitStatusId to set
     */
    public void setPermitStatusId(Long permitStatusId) {
        this.permitStatusId = permitStatusId;
    }
    
    /**
     * Gets the permit id.
     *
     * @return the permitId
     */
    public Long getPermitId() {
        return permitId;
    }
    
    /**
     * Sets the permit id.
     *
     * @param permitId the permitId to set
     */
    public void setPermitId(Long permitId) {
        this.permitId = permitId;
    }
    
    /**
     * Gets the period id.
     *
     * @return the periodId
     */
    public Long getPeriodId() {
        return periodId;
    }
    
    /**
     * Sets the period id.
     *
     * @param periodId the periodId to set
     */
    public void setPeriodId(Long periodId) {
        this.periodId = periodId;
    }
    
    /**
     * Gets the period status type cd.
     *
     * @return the prmtStatusTypeCd
     */
    public String getPeriodStatusTypeCd() {
        /* this REALLLY needs to be fixed on the database side.  Remove this later */
        return StringUtils.stripEnd(periodStatusTypeCd, " ");
    }
    
    /**
     * Sets the period status type cd.
     *
     * @param prmtStatusTypeCd the prmtStatusTypeCd to set
     */
    public void setPeriodStatusTypeCd(String prmtStatusTypeCd) {
        this.periodStatusTypeCd = prmtStatusTypeCd;
    }
    
	/**
	 * @return the reconStatusTypeCd
	 */
	public String getReconStatusTypeCd() {
		return reconStatusTypeCd;
	}

	/**
	 * @param reconStatusTypeCd the reconStatusTypeCd to set
	 */
	public void setReconStatusTypeCd(String reconStatusTypeCd) {
		this.reconStatusTypeCd = reconStatusTypeCd;
	}

	/**
     * Gets the status dt.
     *
     * @return the statusDt
     */
    public Date getStatusDt() {
        return statusDt;
    }
    
    /**
     * Sets the status dt.
     *
     * @param statusDt the statusDt to set
     */
    public void setStatusDt(Date statusDt) {
        this.statusDt = statusDt;
    }
    
    /**
     * Gets the created by.
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }
    
    /**
     * Sets the created by.
     *
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
    /**
     * Gets the created dt.
     *
     * @return the createdDt
     */
    public Date getCreatedDt() {
        return createdDt;
    }
    
    /**
     * Sets the created dt.
     *
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }
    
    /**
     * Gets the modified by.
     *
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }
    
    /**
     * Sets the modified by.
     *
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    
    /**
     * Gets the modified dt.
     *
     * @return the modifiedDt
     */
    public Date getModifiedDt() {
        return modifiedDt;
    }
    
    /**
     * Sets the modified dt.
     *
     * @param modifiedDt the modifiedDt to set
     */
    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }
    
    /**
     * Gets the permit period entity.
     *
     * @return the permitPeriodEntity
     */
    public PermitPeriodEntity getPermitPeriodEntity() {
        return permitPeriodEntity;
    }
    
    /**
     * Sets the permit period entity.
     *
     * @param permitPeriodEntity the permitPeriodEntity to set
     */
    public void setPermitPeriodEntity(PermitPeriodEntity permitPeriodEntity) {
        this.permitPeriodEntity = permitPeriodEntity;
    }

	public String getReconStatusModifiedBy() {
		return reconStatusModifiedBy;
	}

	public void setReconStatusModifiedBy(String reconStatusModifiedBy) {
		this.reconStatusModifiedBy = reconStatusModifiedBy;
	}

	public Date getReconStatusModifiedDt() {
		return reconStatusModifiedDt;
	}

	public void setReconStatusModifiedDt(Date reconStatusModifiedDt) {
		this.reconStatusModifiedDt = reconStatusModifiedDt;
	}
    
}

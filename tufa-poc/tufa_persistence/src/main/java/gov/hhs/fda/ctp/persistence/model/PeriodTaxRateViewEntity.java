/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;

/**
 * @author tgunter
 *
 */
@Entity
@Immutable
@Table(name="PERIOD_TAX_RATE_VW")
public class PeriodTaxRateViewEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="PERIOD_ID")
	private Long periodId;

	@Id
	@Column(name="TOBACCO_CLASS_ID")
	private Long tobaccoClassId;
	
	@Id
	@Column(name="TAX_RATE_ID")
	private Long taxRateId;
	
	@Column(name="TOBACCO_CLASS_NM")
	private String tobaccoClassNm;
	
	@Column(name="TAX_RATE")
	private Double taxRate;
	
	@Column(name="CONVERSION_RATE")
	private Double conversionRate;
	
	@Column(name="PARENT_CLASS_ID")
	private Long parentClassId;
	
	@Column(name="CLASS_TYPE_CD")
	private String classTypeCd;
	
    @Temporal(TemporalType.DATE)   
	@Column(name="TAX_RATE_EFFECTIVE_DATE")
	private Date taxRateEffectiveDate;

    @Temporal(TemporalType.DATE)   
	@Column(name="TAX_RATE_END_DATE")
	private Date taxRateEndDate;
    
    @Column(name="THRESHOLD_VALUE")
    private Double thresholdValue;

	public Double getThresholdValue() {
		return thresholdValue;
	}

	public void setThresholdValue(Double thresholdValue) {
		this.thresholdValue = thresholdValue;
	}

	/**
	 * @return the periodId
	 */
	public Long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId the periodId to set
	 */
	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the tobaccoClassId
	 */
	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}

	/**
	 * @param tobaccoClassId the tobaccoClassId to set
	 */
	public void setTobaccoClassId(Long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}

	/**
	 * @return the tobaccoClassNm
	 */
	public String getTobaccoClassNm() {
		return tobaccoClassNm;
	}

	/**
	 * @param tobaccoClassNm the tobaccoClassNm to set
	 */
	public void setTobaccoClassNm(String tobaccoClassNm) {
		this.tobaccoClassNm = tobaccoClassNm;
	}

	/**
	 * @return the taxRate
	 */
	public Double getTaxRate() {
		return taxRate;
	}

	/**
	 * @param taxRate the taxRate to set
	 */
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	/**
	 * @return the conversionRate
	 */
	public Double getConversionRate() {
		return conversionRate;
	}

	/**
	 * @param conversionRate the conversionRate to set
	 */
	public void setConversionRate(Double conversionRate) {
		this.conversionRate = conversionRate;
	}

	/**
	 * @return the taxRateId
	 */
	public Long getTaxRateId() {
		return taxRateId;
	}

	/**
	 * @param taxRateId the taxRateId to set
	 */
	public void setTaxRateId(Long taxRateId) {
		this.taxRateId = taxRateId;
	}

	/**
	 * @return the parentClassId
	 */
	public Long getParentClassId() {
		return parentClassId;
	}

	/**
	 * @param parentClassId the parentClassId to set
	 */
	public void setParentClassId(Long parentClassId) {
		this.parentClassId = parentClassId;
	}

	/**
	 * @return the classTypeCd
	 */
	public String getClassTypeCd() {
		return classTypeCd;
	}

	/**
	 * @param classTypeCd the classTypeCd to set
	 */
	public void setClassTypeCd(String classTypeCd) {
		this.classTypeCd = classTypeCd;
	}

	/**
	 * @return the taxRateEffectiveDate
	 */
	public Date getTaxRateEffectiveDate() {
		return taxRateEffectiveDate;
	}

	/**
	 * @param taxRateEffectiveDate the taxRateEffectiveDate to set
	 */
	public void setTaxRateEffectiveDate(Date taxRateEffectiveDate) {
		this.taxRateEffectiveDate = taxRateEffectiveDate;
	}

	/**
	 * @return the taxRateEndDate
	 */
	public Date getTaxRateEndDate() {
		return taxRateEndDate;
	}

	/**
	 * @param taxRateEndDate the taxRateEndDate to set
	 */
	public void setTaxRateEndDate(Date taxRateEndDate) {
		this.taxRateEndDate = taxRateEndDate;
	}

}

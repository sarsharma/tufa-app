package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import gov.hhs.fda.ctp.common.beans.PermitAudit;

@Entity
@Table(name = "TU_PERMIT_UPDATES_AUDIT")
@SqlResultSetMapping(
	    name = "permit_audit_dto_mapping",
	    classes = @ConstructorResult(
	        targetClass = PermitAudit.class,
	        columns = {
		        @ColumnResult(name = "auditId"),
	            @ColumnResult(name = "companyName"),
	            @ColumnResult(name = "companyCreateDt"),
	            @ColumnResult(name = "companyId"),
	            @ColumnResult(name = "docStageId"),
	            @ColumnResult(name = "einNum"),
	            @ColumnResult(name = "permitId"),
	            @ColumnResult(name = "permitStatus"),
	            @ColumnResult(name = "permitAction"),
	            @ColumnResult(name = "companyStatus"),
	            @ColumnResult(name = "companyAction"),
	            @ColumnResult(name = "closeDt"),
	            @ColumnResult(name = "issueDt"),
	            @ColumnResult(name = "actionDt"),
	            @ColumnResult(name = "createdDt"),
	            @ColumnResult(name = "errors"),
	            @ColumnResult(name = "permitStatusChangeAction"),
	            @ColumnResult(name = "permitIdTufa"),
	            @ColumnResult(name = "companyStatusChangeAction"),
	            @ColumnResult(name = "permitTypeCd"),
	            @ColumnResult(name = "permitStatusTufa"),
	            @ColumnResult(name = "pPermitId"),
	            @ColumnResult(name = "pPermitNum"),
	            @ColumnResult(name = "pPermitStatusTypeCd"),
	            @ColumnResult(name = "pIssueDt"),
	            @ColumnResult(name = "pCloseDt"),
	            @ColumnResult(name = "cLegalNm"),
		        @ColumnResult(name = "cCompanyStatus"),
		        @ColumnResult(name = "rpLastRptDt"),
		        @ColumnResult(name = "pTTBStartDt"),
		        @ColumnResult(name = "pTTBEndDt")

	        }
	    )
	)
public class PermitAuditEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AUDIT_ID", unique = true, nullable = false)
	private Long permitAuditId;

	@Column(name = "COMPANY_NAME")
	private String companyName;

	@Column(name = "COMPANY_ID")
	private Long companyId;

	@Column(name = "EIN", columnDefinition = "char(9)")
	private String einNum;

	@Column(name = "PERMIT_ID")
	private String permitNum;

	@Column(name = "PERMIT_ACTION")
	private String permitAction;

	@Column(name = "PERMIT_STATUS")
	private String permitStatus;

	@Column(name = "PERMIT_STATUS_TUFA")
	private String permitStatusTufa;

	@Column(name = "DOC_STAGE_ID")
	private Long docId;

	@Temporal(TemporalType.DATE)
	@Column(name = "ISSUE_DT")
	private Date issueDt;

	@Temporal(TemporalType.DATE)
	@Column(name = "CLOSE_DT")
	private Date closeDt;

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DT")
	private Date createdDt;

	@Temporal(TemporalType.DATE)
	@Column(name = "ACTION_DT")
	private Date actionDt;

	@Column(name = "ERRORS")
	private String errors;

	@Column(name = "COMPANY_STATUS")
	private String companyStatus;

	@Column(name = "COMPANY_ACTION")
	private String companyAction;

	@Column(name = "PERMIT_TYPE_CD")
	private String permitTypeCd;

	@Column(name = "PERMIT_STATUS_CHANGE_ACTION")
	private String permitStatusChangeAction;

	@Column(name = "COMPANY_STATUS_CHANGE_ACTION")
	private String companyStatusChangeAction;

	@Column(name = "PERMIT_ID_TUFA")
	private Long permitIdTufa;
	
	@Column(name = "PERMIT_EXCL_RESOLVED")
	private String permitExclResolved;
	
	@Column(name = "ACTION_ID")
	private Long actionId;
	
	@Temporal(TemporalType.DATE)
	@Column(name ="TUFA_PERMIT_CLS_DT")
	private Date tufaPermitClsDt;
	
	@OneToOne
	@JoinColumn(name = "ACTION_ID", referencedColumnName = "ACTION_ID", nullable = true, insertable = false, updatable = false)
	@NotFound(action = NotFoundAction.IGNORE)
	private TTBPermitActionEntity action;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PERMIT_ID_TUFA", referencedColumnName = "PERMIT_ID", nullable = true, insertable = false, updatable = false)
	@NotFound(action = NotFoundAction.IGNORE)
	private PermitEntity permitTufa;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "COMPANY_ID", referencedColumnName = "COMPANY_ID", nullable = true, insertable = false, updatable = false)
	@NotFound(action = NotFoundAction.IGNORE)
	private CompanyEntity companyTufa;

	public Long getPermitAuditId() {
		return permitAuditId;
	}

	public void setPermitAuditId(Long permitAuditId) {
		this.permitAuditId = permitAuditId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getEinNum() {
		return einNum;
	}

	public void setEinNum(String einNum) {
		this.einNum = einNum;
	}

	public String getPermitNum() {
		return permitNum;
	}

	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}

	public String getPermitAction() {
		return permitAction;
	}

	public void setPermitAction(String permitAction) {
		this.permitAction = permitAction;
	}

	public Date getIssueDt() {
		return issueDt;
	}

	public void setIssueDt(Date issueDt) {
		this.issueDt = issueDt;
	}

	public Date getCloseDt() {
		return closeDt;
	}

	public void setCloseDt(Date closeDt) {
		this.closeDt = closeDt;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getActionDt() {
		return actionDt;
	}

	public void setActionDt(Date actionDt) {
		this.actionDt = actionDt;
	}

	public String getErrors() {
		return errors;
	}

	public String getPermitStatus() {
		return permitStatus;
	}

	public void setPermitStatus(String permitStatus) {
		this.permitStatus = permitStatus;
	}

	public String getPermitStatusTufa() {
		return permitStatusTufa;
	}

	public void setPermitStatusTufa(String permitStatusTufa) {
		this.permitStatusTufa = permitStatusTufa;
	}

	public String getPermitTypeCd() {
		return permitTypeCd;
	}

	public Date getTufaPermitClsDt() {
		return tufaPermitClsDt;
	}

	public void setTufaPermitClsDt(Date tufaPermitClsDt) {
		this.tufaPermitClsDt = tufaPermitClsDt;
	}

	public Long getActionId() {
		return actionId;
	}

	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}

	public String getPermitStatusChangeAction() {
		return permitStatusChangeAction;
	}

	public void setPermitStatusChangeAction(String permitStatusChangeAction) {
		this.permitStatusChangeAction = permitStatusChangeAction;
	}

	public PermitEntity getPermitTufa() {
		return permitTufa;
	}

	public void setPermitTufa(PermitEntity permitTufa) {
		this.permitTufa = permitTufa;
	}

	public String getCompanyStatusChangeAction() {
		return companyStatusChangeAction;
	}

	public void setCompanyStatusChangeAction(String companyStatusChangeAction) {
		this.companyStatusChangeAction = companyStatusChangeAction;
	}

	public void setPermitTypeCd(String permitTypeCd) {
		this.permitTypeCd = permitTypeCd;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public Long getDocId() {
		return docId;
	}

	public Long getPermitIdTufa() {
		return permitIdTufa;
	}

	public void setPermitIdTufa(Long permitIdTufa) {
		this.permitIdTufa = permitIdTufa;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getCompanyStatus() {
		return companyStatus;
	}

	public void setCompanyStatus(String companyStatus) {
		this.companyStatus = companyStatus;
	}

	public CompanyEntity getCompanyTufa() {
		return companyTufa;
	}

	public void setCompanyTufa(CompanyEntity companyTufa) {
		this.companyTufa = companyTufa;
	}

	public String getCompanyAction() {
		return companyAction;
	}

	

	public void setCompanyAction(String companyAction) {
		this.companyAction = companyAction;
	}

	public String getPermitExclResolved() {
		return permitExclResolved;
	}

	public void setPermitExclResolved(String permitExclResolved) {
		this.permitExclResolved = permitExclResolved;
	}

	public TTBPermitActionEntity getAction() {
		return action;
	}

	public void setAction(TTBPermitActionEntity action) {
		this.action = action;
	}

}

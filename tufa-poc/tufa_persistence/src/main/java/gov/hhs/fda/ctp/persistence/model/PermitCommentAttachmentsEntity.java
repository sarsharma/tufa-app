package gov.hhs.fda.ctp.persistence.model;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class AttachmentsEntity.
 *
 * @author rnasina
 */
@Entity
@Table(name="TU_PERMIT_EXCL_COMMENT_DOC")
public class PermitCommentAttachmentsEntity extends BaseEntity{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The document id. */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DOCUMENT_ID", unique = true, nullable=false) 
    private long documentId;
    
	/** The COMMENT_SEQ. */
	@Column(name = "COMMENT_SEQ", nullable=false) 
    private long commentId;
    
	    
	/** The doc file nm. */
	@Column(name = "DOC_FILE_NM", columnDefinition="CHAR(4)", nullable=false)
    private String docFileNm;
    
	/** The doc status cd. */
	@Column(name = "DOC_STATUS_CD", columnDefinition="CHAR(4)")
    private String docStatusCd;
    
    /** The report pdf. */
    @Lob @Basic(fetch = FetchType.LAZY)
	@Column(name="REPORT_PDF")
    private Blob reportPdf;
    
    /** The doc desc. */
    @Column(name = "DOC_DESC", length = 240)
    private String docDesc;
    
    /** The form types. */
    @Column(name = "FORM_TYPES", length = 100)
    private String formTypes;    


    /** The created by. */
    @Column(name = "CREATED_BY", length=50)
    private String createdBy;
    

	/** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;
    
    /** The modified by. */
    @Column(name = "MODIFIED_BY", length=50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="COMMENT_SEQ",insertable=false,updatable=false)
    private PermitExcludeCommentEntity comment;
    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((docStatusCd == null) ? 0 : docStatusCd.hashCode());
        return result;
    }

    public long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}

	public long getCommentId() {
		return commentId;
	}

	public void setCommentId(long commentId) {
		this.commentId = commentId;
	}

	public String getDocFileNm() {
		return docFileNm;
	}

	public void setDocFileNm(String docFileNm) {
		this.docFileNm = docFileNm;
	}

	public String getDocStatusCd() {
		return docStatusCd;
	}

	public void setDocStatusCd(String docStatusCd) {
		this.docStatusCd = docStatusCd;
	}

	public Blob getReportPdf() {
		return reportPdf;
	}

	public void setReportPdf(Blob reportPdf) {
		this.reportPdf = reportPdf;
	}

	public String getDocDesc() {
		return docDesc;
	}

	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}

	public String getFormTypes() {
		return formTypes;
	}

	public void setFormTypes(String formTypes) {
		this.formTypes = formTypes;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PermitCommentAttachmentsEntity attachEntity = (PermitCommentAttachmentsEntity) obj;
        if (!this.docStatusCd.equals(attachEntity.docStatusCd)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("attachment [number=").append(docStatusCd).append("]").append("[id=").append(commentId).append("]");
        return builder.toString();
    }
}

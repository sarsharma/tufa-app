/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.DynamicUpdate;

/**
 * The Class PermitEntity.
 *
 * @author tgunter
 */
@Entity
@Table(name = "tu_permit", uniqueConstraints = {
@UniqueConstraint(columnNames = "PERMIT_NUM")})
@DynamicUpdate
public class PermitEntity extends BaseEntity {
    /**
     * Entity bean for tu_permit table.
     */
    private static final long serialVersionUID = 1L;
    
    /** The permit id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PERMIT_ID", unique = true, nullable=false)    
    private long permitId;
    
    /** The permit num. */
    @Column(name = "PERMIT_NUM", unique=true, nullable = false)
    private String permitNum;
    
    /** The issue dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name = "ISSUE_DT")
    private Date issueDt;
    
    /** The close dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name = "CLOSE_DT")
    private Date closeDt;
    
    /** The TTB START dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name = "TTB_START_DT")
    private Date ttbstartDt;
    
    /** The TTB  END  dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name = "TTB_END_DT")
    private Date ttbendDt;
    
    /** The permit type cd. */
    @Column(name = "PERMIT_TYPE_CD", nullable=false)
    private String permitTypeCd;
    
    /** The permit status type cd. */
    @Column(name = "PERMIT_STATUS_TYPE_CD", nullable=false)
    private String permitStatusTypeCd;
    
    /** The company entity. */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COMPANY_ID", nullable=false)
    private CompanyEntity companyEntity;
    
    /** The created by. */
    @Column(name = "CREATED_BY")
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;
    
    /** The modified by. */
    @Column(name = "MODIFIED_BY")
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    
    /** Permit comments */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "permitId")
    private List<PermitHistComments> permitComments;
    
    /**
     * Gets the permit id.
     *
     * @return the permit id
     */
    public long getPermitId() {
        return permitId;
    }
    
    /**
     * Sets the permit id.
     *
     * @param permitId the new permit id
     */
    public void setPermitId(long permitId) {
        this.permitId = permitId;
    }
    
    /**
     * Gets the permit num.
     *
     * @return the permit num
     */
    public String getPermitNum() {
        return permitNum;
    }
    
    /**
     * Sets the permit num.
     *
     * @param permitNum the new permit num
     */
    public void setPermitNum(String permitNum) {
    	if(permitNum == null) permitNum = "";
    	this.permitNum = permitNum.toUpperCase();
    }
    
    /**
     * Gets the issue dt.
     *
     * @return the issue dt
     */
    public Date getIssueDt() {
        return issueDt;
    }
    
    /**
     * Sets the issue dt.
     *
     * @param issueDt the new issue dt
     */
    public void setIssueDt(Date issueDt) {
        this.issueDt = issueDt;
    }
    
    /**
     * Gets the close dt.
     *
     * @return the close dt
     */
    public Date getCloseDt() {
        return closeDt;
    }
    
    /**
     * Sets the close dt.
     *
     * @param closeDt the new close dt
     */
    public void setCloseDt(Date closeDt) {
        this.closeDt = closeDt;
    }
    
    /**
     * Gets the ttb start dt.
     *
     * @return the ttb start dt.
     */
    public Date getttbstartDt() {
        return ttbstartDt;
    }
    
    /**
     * Sets the ttbstart dt.
     *
     * @param ttbstart the new ttbstart dt
     */
    public void setttbstartDt(Date ttbstartDt) {
        this.ttbstartDt = ttbstartDt;
    }
    
    /**
     * Gets the close dt.
     *
     * @return the close dt
     */
    public Date getttbendDt() {
        return ttbendDt;
    }
    
    /**
     * Sets the close dt.
     *
     * @param closeDt the new close dt
     */
    public void setttbendDt(Date ttbendDt) {
        this.ttbendDt = ttbendDt;
    }
    
    
    
    /**
     * Gets the permit type cd.
     *
     * @return the permit type cd
     */
    public String getPermitTypeCd() {
        return permitTypeCd;
    }
    
    /**
     * Sets the permit type cd.
     *
     * @param permitTypeCd the new permit type cd
     */
    public void setPermitTypeCd(String permitTypeCd) {
        this.permitTypeCd = permitTypeCd;
    }

    /**
     * Gets the permit status type cd.
     *
     * @return the permit status type cd
     */
    public String getPermitStatusTypeCd() {
        return permitStatusTypeCd;
    }
    
    /**
     * Sets the permit status type cd.
     *
     * @param permitStatusTypeCd the new permit status type cd
     */
    public void setPermitStatusTypeCd(String permitStatusTypeCd) {
        this.permitStatusTypeCd = permitStatusTypeCd;
    }

    /**
	 * @return the permitComments
	 */
	public List<PermitHistComments> getPermitComments() {
		return permitComments;
	}

	/**
	 * @param permitComments the permitComments to set
	 */
	public void setPermitComments(List<PermitHistComments> permitComments) {
		this.permitComments = permitComments;
	}

	/**
     * Gets the company entity.
     *
     * @return the company entity
     */
    public CompanyEntity getCompanyEntity() {
        return companyEntity;
    }

    /**
     * Sets the company entity.
     *
     * @param companyEntity the new company entity
     */
    public void setCompanyEntity(CompanyEntity companyEntity) {
        this.companyEntity = companyEntity;
    }

    /**
     * Gets the created by.
     *
     * @return the created by
     */
    public String getCreatedBy() {
        return createdBy;
    }
    
    /**
     * Sets the created by.
     *
     * @param createdBy the new created by
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
    /**
     * Gets the modified by.
     *
     * @return the modified by
     */
    public String getModifiedBy() {
        return modifiedBy;
    }
    
    /**
     * Sets the modified by.
     *
     * @param modifiedBy the new modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * Gets the created dt.
     *
     * @return the created dt
     */
    public Date getCreatedDt() {
        return this.createdDt;
    }
    
    /**
     * Sets the created dt.
     *
     * @param createdDt the new created dt
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    /**
     * Gets the modified dt.
     *
     * @return the modified dt
     */
    public Date getModifiedDt() {
        return this.modifiedDt;
    }
    
    /**
     * Sets the modified dt.
     *
     * @param modifiedDt the new modified dt
     */
    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }
 
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((permitNum == null) ? 0 : permitNum.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PermitEntity permitEntity = (PermitEntity) obj;
        if (!this.permitNum.equals(permitEntity.permitNum)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("Permit [number=").append(permitNum).append("]").append("[id=").append(permitId).append("]");
        return builder.toString();
    }
     
}

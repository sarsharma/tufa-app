package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "TU_PERMIT_EXCL_AUDIT")
public class PermitExcludeAuditEntity extends BaseEntity {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="PERM_EXCL_AUDIT_ID", unique = true, nullable=false)
    private long permitExcludeAuditId;
    
    @Column(name="COMPANY_ID")
    private long comapnyId;
    
    @Column(name="PERMIT_ID")
    private long permitId;
    
    @Column(name="ASSMNT_YR")
    private long assessmentYr;

    @Column(name="ASSMNT_QTR")
    private long assessmentQtr;

    @Column(name="EXCLUSION_SCOPE_ID")
    private long exclusionScopeId;

    @Column(name="FULL_PERM_EXCL_AUDIT_ID")
    private Long fullPermitExcludeAuditId;

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="COMMENT_ID",insertable=false,updatable=false)
    private PermitExcludeCommentEntity comment;
    
    @Column(name="CREATED_BY")
    private String createdBy;

	@Temporal(TemporalType.DATE) 
    @UpdateTimestamp
    @Column(name="CREATED_DT")
    private Date createdDt;

	/**
	 * @return the permitExcludeAuditId
	 */
	public long getPermitExcludeAuditId() {
		return permitExcludeAuditId;
	}

	/**
	 * @param permitExcludeAuditId the permitExcludeAuditId to set
	 */
	public void setPermitExcludeAuditId(long permitExcludeAuditId) {
		this.permitExcludeAuditId = permitExcludeAuditId;
	}

	/**
	 * @return the comapnyId
	 */
	public long getComapnyId() {
		return comapnyId;
	}

	/**
	 * @param comapnyId the comapnyId to set
	 */
	public void setComapnyId(long comapnyId) {
		this.comapnyId = comapnyId;
	}

	/**
	 * @return the permitId
	 */
	public long getPermitId() {
		return permitId;
	}

	/**
	 * @param permitId the permitId to set
	 */
	public void setPermitId(long permitId) {
		this.permitId = permitId;
	}

	/**
	 * @return the assessmentYr
	 */
	public long getAssessmentYr() {
		return assessmentYr;
	}

	/**
	 * @param assessmentYr the assessmentYr to set
	 */
	public void setAssessmentYr(long assessmentYr) {
		this.assessmentYr = assessmentYr;
	}

	/**
	 * @return the assessmentQtr
	 */
	public long getAssessmentQtr() {
		return assessmentQtr;
	}

	/**
	 * @param assessmentQtr the assessmentQtr to set
	 */
	public void setAssessmentQtr(long assessmentQtr) {
		this.assessmentQtr = assessmentQtr;
	}

	/**
	 * @return the exclusionScopeId
	 */
	public long getExclusionScopeId() {
		return exclusionScopeId;
	}

	/**
	 * @param exclusionScopeId the exclusionScopeId to set
	 */
	public void setExclusionScopeId(long exclusionScopeId) {
		this.exclusionScopeId = exclusionScopeId;
	}

	/**
	 * @return the fullPermitExcludeAuditId
	 */
	public long getFullPermitExcludeAuditId() {
		return fullPermitExcludeAuditId;
	}

	/**
	 * @param fullPermitExcludeAuditId the fullPermitExcludeAuditId to set
	 */
	public void setFullPermitExcludeAuditId(long fullPermitExcludeAuditId) {
		this.fullPermitExcludeAuditId = fullPermitExcludeAuditId;
	}

	/**
	 * @return the commentId
	 */
	public PermitExcludeCommentEntity getCommentId() {
		return comment;
	}

	/**
	 * @param commentId the commentId to set
	 */
	public void setCommentId(PermitExcludeCommentEntity commentId) {
		this.comment = commentId;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}

	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	
	
}

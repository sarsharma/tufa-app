package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "tu_permit_excl_comment")
public class PermitExcludeCommentEntity extends BaseEntity {
	/**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="COMMENT_SEQ", unique = true, nullable=false)
    private long commentSeq;
    
    @Column(name="USER_COMMENT")
    private String userComment;
    
    @Column(name="COMMENT_AUTHOR")
    private String author;
    
    @Temporal(TemporalType.DATE)
    @CreationTimestamp
    @Column(name="COMMENT_DATE")
    private Date commentDate;
    
    @Column(name="MODIFIED_BY")
    private String modifiedBy;

	@Temporal(TemporalType.DATE) 
    @UpdateTimestamp
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    
    @Column(name="COMMENT_RESOLVED")
    private Boolean resolveComment;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy="comment")
    private List<PermitExcludeAuditEntity> audits;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="comment")
    private List<PermitCommentAttachmentsEntity> attachments;
    
    /**
	 * @return the permitCommentId
	 */
	public long getCommentSeq() {
		return commentSeq;
	}

	/**
	 * @param permitCommentId the permitCommentId to set
	 */
	public void setCommentSeq(long commentSeq) {
		this.commentSeq = commentSeq;
	}

	/**
	 * @return the userComment
	 */
	public String getUserComment() {
		return userComment;
	}

	/**
	 * @param userComment the userComment to set
	 */
	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the commentDate
	 */
	public Date getCommentDate() {
		return commentDate;
	}

	/**
	 * @param commentDate the commentDate to set
	 */
	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}

	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	/**
	 * @return the resolveComment
	 */
	public Boolean getResolveComment() {
		return resolveComment;
	}

	/**
	 * @param resolveComment the resolveComment to set
	 */
	public void setResolveComment(Boolean resolveComment) {
		this.resolveComment = resolveComment;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the audits
	 */
	public List<PermitExcludeAuditEntity> getAudits() {
		return audits;
	}

	/**
	 * @param audits the audits to set
	 */
	public void setAudits(List<PermitExcludeAuditEntity> audits) {
		this.audits = audits;
	}

	/**
	 * @return the documents
	 */
	public List<PermitCommentAttachmentsEntity> getAttachments() {
		return attachments;
	}

	/**
	 * @param documents the documents to set
	 */
	public void setDocuments(List<PermitCommentAttachmentsEntity> documents) {
		this.attachments = documents;
	}
}

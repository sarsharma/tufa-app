package gov.hhs.fda.ctp.persistence.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author Mohan.Bhutada
 *
 */
@Entity
@Table(name="TU_PERMITT_HIST_COMMENTS")
public class PermitHistComments extends BaseEntity{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="PMT_COMMENT_ID", unique = true, nullable=false)
    private long permitCommentId;
    
    @Column(name="USER_COMMENTS")
    private String userComment;
    
    @Column(name="COMMENT_AUTHOR")
    private String author;
    
    @Column(name="CREATED_BY")
    private String createdBy;
    
    @Temporal(TemporalType.DATE)
    @CreationTimestamp
    @Column(name="COMMENT_DATE")
    private Date commentDate;
    
    @Temporal(TemporalType.DATE)
    @CreationTimestamp
    @Column(name="CREATED_DT")
    private Date createdDate;
    
    @Column(name="MODIFIED_BY")
    private String modifiedBy;
    
    @Temporal(TemporalType.DATE) 
    @UpdateTimestamp
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    
    @Column(name="COMMENT_RESOLVED")
    private Boolean resolveComment;

    @Column(name="PERMIT_ID",nullable=false)
    private long permitId;

    public long getPermitCommentId() {
        return permitCommentId;
    }

    public void setPermitCommentId(long permitCommentId) {
        this.permitCommentId = permitCommentId;
    }

    public String getUserComment() {
        return userComment;
    }

    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDt() {
        return modifiedDt;
    }

    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }



    public Boolean getResolveComment() {
        return resolveComment;
    }

    public void setResolveComment(Boolean resolveComment) {
        this.resolveComment = resolveComment;
    }

    public long getPermitId() {
        return permitId;
    }

    public void setPermitId(long permitId) {
        this.permitId = permitId;
    }
    
    
    
    
    
    
    

}

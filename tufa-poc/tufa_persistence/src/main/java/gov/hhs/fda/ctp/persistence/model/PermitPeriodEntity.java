package gov.hhs.fda.ctp.persistence.model;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The Class PermitPeriodEntity.
 *
 * @author rnasina
 */
@Entity
@IdClass(ActivityPK.class)
@Table(name = "tu_permit_period")
public class PermitPeriodEntity extends BaseEntity implements Serializable {
    /**
     * Entity bean for tu_company table.
     */
    private static final long serialVersionUID = 1L;
    
    /** The permit id. */
    @Id
    @Column(name = "PERMIT_ID", nullable=false) 
    private long permitId;
    
    /** The period id. */
    @Id
    @Column(name = "PERIOD_ID", nullable=false)
    private long periodId;

    /** The attachments. */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "permitPeriodEntity")
    private List<AttachmentsEntity> attachments;
    
    /** missing forms. */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "permitPeriodEntity")
    private List<MissingForms> missingforms;

	/** The period status entity. */
    @JsonIgnore
    @OneToMany(fetch=FetchType.EAGER, mappedBy = "permitPeriodEntity")
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    private List<PeriodStatusEntity> periodStatusList;
    
    /** The submitted forms. */
    @JsonIgnore
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "permitPeriodEntity", orphanRemoval=true)
    @Cascade({CascadeType.ALL})
    private List<SubmittedFormEntity> submittedForms;
    
    /** The created by. */
    /* trigger managed attributes */
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="CREATED_DT")
    private Date createdDt;
    
    /** The modified by. */
    @Column(name= "MODIFIED_BY", length=50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    
    /** The zero flag. */
    @Column(name="ZERO_FLAG", columnDefinition="CHAR(1)")
	private String zeroFlag;

    /** The source cd. */
    @Column(name="SOURCE_CD", columnDefinition="CHAR(4)")
    private String sourceCd;
        
	/**
	 * Gets the zero flag.
	 *
	 * @return the zero flag
	 */
	public String getZeroFlag() {
		return zeroFlag;
	}

	/**
	 * Sets the zero flag.
	 *
	 * @param flag the new zero flag
	 */
	public void setZeroFlag(String flag) {
		this.zeroFlag = flag;
	}
	
	
	/**
	 * Gets the source cd.
	 *
	 * @return the source cd
	 */
	public String getSourceCd() {
		return sourceCd;
	}

	/**
	 * Sets the source cd.
	 *
	 * @param sourceCd the new source cd
	 */
	public void setSourceCd(String sourceCd) {
		this.sourceCd = sourceCd;
	}
    
    /**
     * Gets the permit id.
     *
     * @return the permit id
     */
    public long getPermitId() {
        return this.permitId;
    }

    /**
     * Sets the permit id.
     *
     * @param permit the new permit id
     */
    public void setPermitId(long permit) {
        this.permitId = permit;
    }
   
    /**
     * Gets the period id.
     *
     * @return the period id
     */
    public long getPeriodId() {
        return this.periodId;
    }

    /**
     * Sets the period id.
     *
     * @param period the new period id
     */
    public void setPeriodId(long period) {
        this.periodId = period;
    }
    
    public List<PeriodStatusEntity> getPeriodStatusList() {
		return periodStatusList;
	}

	public void setPeriodStatusList(List<PeriodStatusEntity> periodStatusList) {
		this.periodStatusList = periodStatusList;
	}

	/**
     * Gets the attachments.
     *
     * @return the attachments
     */
    public List<AttachmentsEntity> getAttachments() {
        return attachments;
    }

    /**
     * Sets the attachments.
     *
     * @param attachments the new attachments
     */
    public void setAttachments(List<AttachmentsEntity> attachments) {
        this.attachments = attachments;
    }
    
    
    public List<MissingForms> getMissingforms() {
		return missingforms;
	}

	public void setMissingforms(List<MissingForms> missingforms) {
		this.missingforms = missingforms;
	}

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((permitId == 0) ? 0 : Long.valueOf(permitId).hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PermitPeriodEntity permitPeriodEntity = (PermitPeriodEntity) obj;
        if (this.periodId != permitPeriodEntity.periodId) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("PermitPeriod [name=").append(permitId).append("]").append("[id=").append(periodId).append("]");
        return builder.toString();
    }

    /*
     * Mark all of these as transient.  These database
     * fields will be managed by triggers.
     */
    
    /**
     * Gets the created by.
     *
     * @return the created by
     */
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * Sets the created by.
     *
     * @param createdBy the new created by
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
   
    /**
     * Gets the modified by.
     *
     * @return the modified by
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }
    
    /**
     * Sets the modified by.
     *
     * @param modifiedBy the new modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
       
    /**
     * Gets the created dt.
     *
     * @return the created dt
     */
    public Date getCreatedDt() {
        return this.createdDt;
    }
    
    /**
     * Sets the created dt.
     *
     * @param createdDt the new created dt
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }
     
    /**
     * Gets the modified dt.
     *
     * @return the modified dt
     */
    public Date getModifiedDt() {
        return this.modifiedDt;
    }
    
    /**
     * Sets the modified dt.
     *
     * @param modifiedDt the new modified dt
     */
    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }

    /**
     * Gets the submitted forms.
     *
     * @return the submittedForms
     */
    public List<SubmittedFormEntity> getSubmittedForms() {
        return submittedForms;
    }

    /**
     * Sets the submitted forms.
     *
     * @param submittedForms the submittedForms to set
     */
    public void setSubmittedForms(List<SubmittedFormEntity> submittedForms) {
        if(submittedForms == null) {
            this.submittedForms.clear();
        }
        else {
            this.submittedForms = submittedForms;
        }
    }
}
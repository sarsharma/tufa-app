package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="TU_PERMIT_UPDATES_DOCUMENT")
public class PermitUpdateDocumentEntity {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "STAGE_DOC_ID", unique = true, nullable = false)
    private Long docId;
	
	@Column(name="DOC_FILE_NM")
    private String fileNm;
    
	@Column(name="ALIAS_FILE_NM")
    private String aliasFileNm;
	
	@Column(name="CREATED_BY")
    private String createdBy;
	
	@CreationTimestamp
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;
	
	@JsonIgnore
	@Lob @Basic(fetch = FetchType.LAZY)
	@Column(name="DOCUMENT")
	private byte[] document;
	
	@JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,cascade=CascadeType.ALL, mappedBy = "document")
    private List<PermitUpdateEntity> permits;
	
	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public String getFileNm() {
		return fileNm;
	}

	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public byte[] getDocument() {
		return document;
	}

	public void setDocument(byte[] document) {
		this.document = document;
	}

	public List<PermitUpdateEntity> getPermits() {
		return permits;
	}

	public void setPermits(List<PermitUpdateEntity> permits) {
		this.permits = permits;
	}

	public String getAliasFileNm() {
		return aliasFileNm;
	}

	public void setAliasFileNm(String aliasFileNm) {
		this.aliasFileNm = aliasFileNm;
	}
	
	
	
	
}

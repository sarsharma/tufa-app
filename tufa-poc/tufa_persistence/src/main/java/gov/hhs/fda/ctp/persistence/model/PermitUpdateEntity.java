package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="TU_PERMIT_UPDATES_STAGE")
public class PermitUpdateEntity {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "STAGE_ID", unique = true, nullable = false)
    private Long permitUpdateId;
	 
	@Column(name = "EIN", columnDefinition="char(9)", unique = true, nullable = false)
	private String einNum;
	 
    @Column(name = "PERMIT_ID", unique=true, nullable = false)
    private String permitNum;
    
    @Column(name="PERMIT_STATUS")
	private String permitStatus;
	
    @Temporal(TemporalType.DATE)   
    @Column(name = "ISSUE_DT")
    private Date issueDt;
    
    @Temporal(TemporalType.DATE)   
    @Column(name = "CLOSED_DT")
    private Date closeDt;
    
    @CreationTimestamp
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;
    
    @UpdateTimestamp
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;

    @Column(name="ERRORS")
    private String errors;
    
    @Column(name="BUSINESS_NAME")
    private String businessName;
	
    @Column(name="MAILING_STREET")
	private String mailingStreet;
	
    @Column(name="MAILING_CITY")
	private String mailingCity;
	
    @Column(name="MAILING_STATE")
	private String mailingState;
	
    @Column(name="MAILING_POSTAL_CD")
	private String mailingPostalCd;
    
    @Column(name="MAILING_TEXT")
    private String mailingText;
    
    @Column(name="EMAIL_ADDRESS")
    private String email;
    
    @Column(name="PREMISE_PHONE")
    private String premisePhone;
    
    @Column(name="PREMISE_PHONE_2")
    private String premisePhone2;

    @Column(name="PERMIT_TYPE")
    private String permitType;
    
    @Column(name="DOC_FILE_NM")
    private String fileNm;
    
    @Column(name="CREATED_BY")
    private String createdBy;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DOC_STAGE_ID", referencedColumnName="STAGE_DOC_ID", insertable = true, updatable = false)
    private PermitUpdateDocumentEntity document;
    
    @Column(name="ADDRESS_STATUS")
    private String addressStatus;
    
    @Column(name="CONTACT_STATUS")
    private String contactStatus;
    
    @Column(name = "DOC_STAGE_ID", insertable = false, updatable = false,nullable = false)
    private Long docStageId;
    
	public Long getPermitUpdateId() {
		return permitUpdateId;
	}

	public void setPermitUpdateId(Long permitUpdateId) {
		this.permitUpdateId = permitUpdateId;
	}

	public String getPermitNum() {
		return permitNum;
	}

	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}

	public Date getIssueDt() {
		return issueDt;
	}

	public void setIssueDt(Date issueDt) {
		this.issueDt = issueDt;
	}

	public Date getCloseDt() {
		return closeDt;
	}

	public void setCloseDt(Date closeDt) {
		this.closeDt = closeDt;
	}

	public String getErrors() {
		return errors;
	}

	public String getPermitStatus() {
		return permitStatus;
	}

	public void setPermitStatus(String permitStatus) {
		this.permitStatus = permitStatus;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public String getEinNum() {
		return einNum;
	}

	public void setEinNum(String einNum) {
		this.einNum = einNum;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getMailingStreet() {
		return mailingStreet;
	}

	public void setMailingStreet(String mailingStreet) {
		this.mailingStreet = mailingStreet;
	}

	public String getPremisePhone2() {
		return premisePhone2;
	}

	public void setPremisePhone2(String premisePhone2) {
		this.premisePhone2 = premisePhone2;
	}

	public String getMailingCity() {
		return mailingCity;
	}

	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}

	public String getMailingState() {
		return mailingState;
	}

	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}

	public String getMailingText() {
		return mailingText;
	}

	public void setMailingText(String mailingText) {
		this.mailingText = mailingText;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPremisePhone() {
		return premisePhone;
	}

	public PermitUpdateDocumentEntity getDocument() {
		return document;
	}

	public void setDocument(PermitUpdateDocumentEntity document) {
		this.document = document;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getFileNm() {
		return fileNm;
	}

	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}

	public String getPermitType() {
		return permitType;
	}

	public void setPermitType(String permitType) {
		this.permitType = permitType;
	}

	public void setPremisePhone(String premisePhone) {
		this.premisePhone = premisePhone;
	}

	public String getMailingPostalCd() {
		return mailingPostalCd;
	}

	public void setMailingPostalCd(String mailingPostalCd) {
		this.mailingPostalCd = mailingPostalCd;
	}

	public String getAddressStatus() {
		return addressStatus;
	}

	public void setAddressStatus(String addressStatus) {
		this.addressStatus = addressStatus;
	}

	public String getContactStatus() {
		return contactStatus;
	}

	public void setContactStatus(String contactStatus) {
		this.contactStatus = contactStatus;
	}

	public Long getDocStageId() {
		return docStageId;
	}

	public void setDocStageId(Long docStageId) {
		this.docStageId = docStageId;
	}

}

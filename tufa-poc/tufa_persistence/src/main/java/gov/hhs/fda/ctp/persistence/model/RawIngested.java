package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name="RAW_INGESTED_VW")
public class RawIngested extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COMPANY_NM")
	private String companyNm;

	@Id
	@Column(name="COMPANY_EIN")
	private String ein;
	
	@Id
	@Column(name="ORIGINAL_EIN")
	private String originalEin;

	@Id
	@Column(name="ORIGINAL_COMPANY_NM")
	private String originalCompanyNm;
	
	@Id
	@Column(name="COMPANY_TYPE")
	private String companyType;

	@Id
	@Column(name="MONTH")
	private String month;

	@Id
	@Column(name="FISCAL_QTR")
	private String quarter;

	@Id
	@Column(name="FISCAL_YR")
	private String fiscalYr;

	@Id
	@Column(name="TOBACCO_CLASS_NM")
	private String tobaccoClassNm;

	@Id
	@Column(name="TAXES_PAID")
	private String taxesPaid;

	@Id
	@Column(name="INC_ADJ")
	private String incAdj;

	@Id
	@Column(name="DEC_ADJ")
	private String decAdj;

	@Id
	@Column(name="REMOVALS_AMOUNT")
	private String removalsAmount;

	@Id
	@Column(name="PERMIT_NUM")
	private String permitNum;
	
	
	@Column(name="TPBD")
	private String tpbd;
	
	
	/**
	 * @return the companyNm
	 */
	public String getCompanyNm() {
		return companyNm;
	}

	/**
	 * @param companyNm the companyNm to set
	 */
	public void setCompanyNm(String companyNm) {
		this.companyNm = companyNm;
	}

	/**
	 * @return the ein
	 */
	public String getEin() {
		return ein;
	}

	/**
	 * @param ein the ein to set
	 */
	public void setEin(String ein) {
		this.ein = ein;
	}

	/**
	 * @return the originalEin
	 */
	public String getOriginalEin() {
		return originalEin;
	}

	/**
	 * @param originalEin the originalEin to set
	 */
	public void setOriginalEin(String originalEin) {
		this.originalEin = originalEin;
	}

	/**
	 * @return the originalCompanyNm
	 */
	public String getOriginalCompanyNm() {
		return originalCompanyNm;
	}

	/**
	 * @param originalCompanyNm the originalCompanyNm to set
	 */
	public void setOriginalCompanyNm(String originalCompanyNm) {
		this.originalCompanyNm = originalCompanyNm;
	}

	/**
	 * @return the type
	 */
	public String getCompanyType() {
		return companyType;
	}

	/**
	 * @param type the type to set
	 */
	public void setCompanyType(String type) {
		this.companyType = type;
	}

	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return the quarter
	 */
	public String getQuarter() {
		return quarter;
	}

	/**
	 * @param quarter the quarter to set
	 */
	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	/**
	 * @return the fiscalYr
	 */
	public String getFiscalYr() {
		return fiscalYr;
	}

	/**
	 * @param fiscalYr the fiscalYr to set
	 */
	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	/**
	 * @return the tobaccoClass
	 */
	public String getTobaccoClassNm() {
		return tobaccoClassNm;
	}

	/**
	 * @param tobaccoClass the tobaccoClass to set
	 */
	public void setTobaccoClassNm(String tobaccoClassNm) {
		this.tobaccoClassNm = tobaccoClassNm;
	}

	/**
	 * @return the reportedTaxes
	 */
	public String getTaxesPaid() {
		return taxesPaid;
	}

	/**
	 * @param reportedTaxes the reportedTaxes to set
	 */
	public void setTaxesPaid(String taxesPaid) {
		this.taxesPaid = taxesPaid;
	}

	public String getIncAdj() {
		return incAdj;
	}

	public void setIncAdj(String incAdj) {
		this.incAdj = incAdj;
	}

	public String getDecAdj() {
		return decAdj;
	}

	public void setDecAdj(String decAdj) {
		this.decAdj = decAdj;
	}

	/**
	 * @return the removalsAmount
	 */
	public String getRemovalsAmount() {
		return removalsAmount;
	}

	/**
	 * @param removalsAmount the removalsAmount to set
	 */
	public void setRemovalsAmount(String removalsAmount) {
		this.removalsAmount = removalsAmount;
	}

	/**
	 * @return the permitNum
	 */
	public String getPermitNum() {
		return permitNum;
	}

	/**
	 * @param permitNum the permitNum to set
	 */
	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}
	
	
	
	/**
	 * @return the TPBD
	 */
	public String getTPBD() {
		return tpbd;
	}

	/**
	 * @param set TPBD Data
	 */
	public void setTPBD(String tpbd) {
		this.tpbd = tpbd;
	}
	
}

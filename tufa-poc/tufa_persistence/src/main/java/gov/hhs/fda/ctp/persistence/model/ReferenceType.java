/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class ReferenceType.
 *
 * @author tgunter
 */
@Entity
@Table(name = "tu_ref_type")
public class ReferenceType extends BaseEntity {
    /**
     * Entity bean  for the tu_ref_type table.
     */
    private static final long serialVersionUID = 1L;
    
    /** The reference type id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TU_REF_TYPE_ID", nullable = false)
    private Long referenceTypeId;

    /** The name. */
    @Column(name = "TU_REF_TYPE_NM", nullable = false)
    private String name;
    
    /** The description. */
    @Column(name = "TU_REF_TYPE_DESC")
    private String description;
    
    /** The reference values. */
    @OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER, mappedBy = "referenceType")
    private List<ReferenceValue> referenceValues;

    /**
     * Gets the reference type id.
     *
     * @return the referenceTypeId
     */
    public Long getReferenceTypeId() {
        return referenceTypeId;
    }

    /**
     * Sets the reference type id.
     *
     * @param referenceTypeId the referenceTypeId to set
     */
    public void setReferenceTypeId(Long referenceTypeId) {
        this.referenceTypeId = referenceTypeId;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the reference values.
     *
     * @return the referenceValues
     */
    public List<ReferenceValue> getReferenceValues() {
        return referenceValues;
    }

    /**
     * Sets the reference values.
     *
     * @param referenceValues the referenceValues to set
     */
    public void setReferenceValues(List<ReferenceValue> referenceValues) {
        this.referenceValues = referenceValues;
    }

}

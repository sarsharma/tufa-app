/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The Class ReferenceValue.
 *
 * @author tgunter
 */
@Entity
@Table(name = "tu_ref_value")
public class ReferenceValue extends BaseEntity {
    /**
     * Entity bean  for the tu_ref_value table.
     */
    private static final long serialVersionUID = 1L;
    
    /** The reference type value id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TU_REF_TYPE_VALUE_ID", nullable = false)
    private Long referenceTypeValueId;
        
    /** The code. */
    @Column(name = "TU_REF_CODE")
    private String code;
    
    /** The value. */
    @Column(name = "TU_REF_VALUE", nullable = false)
    private String value;

    /** The reference type. */
    @ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name = "TU_REF_TYPE_ID", nullable=false)
    private ReferenceType referenceType;

    /**
     * Gets the reference type value id.
     *
     * @return the referenceTypeValueId
     */
    public Long getReferenceTypeValueId() {
        return referenceTypeValueId;
    }

    /**
     * Sets the reference type value id.
     *
     * @param referenceTypeValueId the referenceTypeValueId to set
     */
    public void setReferenceTypeValueId(Long referenceTypeValueId) {
        this.referenceTypeValueId = referenceTypeValueId;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the abbreviation.
     *
     * @param code the code to set
     */
    public void setAbbreviation(String code) {
        this.code = code;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the reference type.
     *
     * @return the referenceType
     */
    public ReferenceType getReferenceType() {
        return referenceType;
    }

    /**
     * Sets the reference type.
     *
     * @param referenceType the referenceType to set
     */
    public void setReferenceType(ReferenceType referenceType) {
        this.referenceType = referenceType;
    }
    
}

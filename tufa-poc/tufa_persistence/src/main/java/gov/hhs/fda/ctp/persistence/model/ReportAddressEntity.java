/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class to hold Address in Monthly Reports.
 * 
 * @author akari
 *
 */
@Entity
@Table(name = "tu_report_address")
public class ReportAddressEntity extends BaseEntity {

	/** The Constant serialVersionUID. */
	protected static final long serialVersionUID = 1L;

	/** The company. */
	@Id
	@Column(name = "COMPANY_ID", nullable = false)
	private long companyId;
	
	/** The permit. */
	@Id
	@Column(name = "PERMIT_ID", nullable = false)
	private long permitId;

	/** The period id. */
	@Id
	@Column(name = "PERIOD_ID", nullable = false)
	private long periodId;

	/** The address type cd. */
	@Id
	@Column(name = "ADDRESS_TYPE_CD", nullable = false, length = 4)
	private String addressTypeCd;

	/** The street address. */
	@Column(name = "STREET_ADDRESS", length = 50)
	private String streetAddress;

	/** The suite. */
	@Column(name = "SUITE", length = 20)
	private String suite;

	/** The city. */
	@Column(name = "CITY", length = 40)
	private String city;

	/** The state. */
	@Column(name = "STATE", length = 2)
	private String state;
	
	/** The province. */
	@Column(name = "PROVINCE", length = 20)
	private String province;	

	/** The postal cd. */
	@Column(name = "POSTAL_CD", length = 10)
	private String postalCd;

	/** The fax num. */
	@Column(name = "FAX_NUM", length = 10)
	private Long faxNum;

	/** The country cd. */
	@Column(name = "COUNTRY_CD", length = 2)
	private String countryCd;
	
	@Column(name = "ATTENTION", length = 50)
	private String attention;

	/** The created by. */
	@Column(name = "CREATED_BY", length = 50)
	private String createdBy;

	/** The created dt. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DT")
	private Date createdDt;

	/** The modified by. */
	@Column(name = "MODIFIED_BY", length = 50)
	private String modifiedBy;

	/** The modified dt. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DT")
	private Date modifiedDt;

	/**
	 * @return the addressTypeCd
	 */
	public String getAddressTypeCd() {
		return addressTypeCd;
	}

	/**
	 * @param addressTypeCd
	 *            the addressTypeCd to set
	 */
	public void setAddressTypeCd(String addressTypeCd) {
		this.addressTypeCd = addressTypeCd;
	}

	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}

	/**
	 * @param streetAddress
	 *            the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	/**
	 * @return the suite
	 */
	public String getSuite() {
		return suite;
	}

	/**
	 * @param suite
	 *            the suite to set
	 */
	public void setSuite(String suite) {
		this.suite = suite;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * @param province
	 *            the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * @return the postalCd
	 */
	public String getPostalCd() {
		return postalCd;
	}

	/**
	 * @param postalCd
	 *            the postalCd to set
	 */
	public void setPostalCd(String postalCd) {
		this.postalCd = postalCd;
	}

	/**
	 * @return the faxNum
	 */
	public Long getFaxNum() {
		return faxNum;
	}

	/**
	 * @param faxNum
	 *            the faxNum to set
	 */
	public void setFaxNum(Long faxNum) {
		this.faxNum = faxNum;
	}

	/**
	 * @return the countryCd
	 */
	public String getCountryCd() {
		return countryCd;
	}

	/**
	 * @param countryCd
	 *            the countryCd to set
	 */
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}

	/**
	 * @return the attention
	 */
	public String getAttention() {
		return attention;
	}

	/**
	 * @param attention the attention to set
	 */
	public void setAttention(String attention) {
		this.attention = attention;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}

	/**
	 * @param createdDt
	 *            the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}

	/**
	 * @param modifiedDt
	 *            the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	/**
	 * @return the companyId
	 */
	public long getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the permitId
	 */
	public long getPermitId() {
		return permitId;
	}

	/**
	 * @param permitId the permitId to set
	 */
	public void setPermitId(long permitId) {
		this.permitId = permitId;
	}

	/**
	 * @return the periodId
	 */
	public long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId the periodId to set
	 */
	public void setPeriodId(long periodId) {
		this.periodId = periodId;
	}

}

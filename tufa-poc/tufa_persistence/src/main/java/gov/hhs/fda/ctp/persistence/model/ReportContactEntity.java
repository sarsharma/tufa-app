/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class ContactEntity to hold contacts from monthly reports.
 * 
 * @author akari
 *
 */
@Entity
@Table(name = "tu_report_contact")
public class ReportContactEntity implements Comparable<ReportContactEntity>, Serializable {

	/** The Constant serialVersionUID. */
	protected static final long serialVersionUID = 1L;

	/** The contact id. */
	@Id
	@Column(name = "CONTACT_ID", unique = true, nullable = false)
	private long contactId;

	/** The company. */
	@Column(name = "COMPANY_ID", nullable = false)
	private long companyId;

	/** The permit */
	@Id
	@Column(name = "PERMIT_ID", nullable = false)
	private long permitId;

	@Id
	@Column(name = "PERIOD_ID", nullable = false)
	private long periodId;

	/** The last nm. */
	@Column(name = "LAST_NM", length = 40)
	private String lastNm;

	/** The first nm. */
	@Column(name = "FIRST_NM", length = 40)
	private String firstNm;

	/** The phone num. */
	@Column(name = "PHONE_NUM")
	private String phoneNum;

	/** The phone ext. */
	@Column(name = "PHONE_EXT", length = 4)
	private Long phoneExt;

	/** The email address. */
	@Column(name = "EMAIL_ADDRESS", length = 50)
	private String emailAddress;

	/** The country cd. */
	@Column(name = "COUNTRY_CD", length = 2)
	private String countryCd;

	/** The country dial code. */
	@Column(name = "CNTRY_DIAL_CD", length = 2)
	private String cntryDialCd;

	/** The fax num. */
	@Column(name = "FAX_NUM", length = 10)
	private String faxNum;

	/** The created by. */
	@Column(name = "CREATED_BY", length = 50)
	private String createdBy;

	/** The created dt. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DT")
	private Date createdDt;

	/** The modified by. */
	@Column(name = "MODIFIED_BY", length = 50)
	private String modifiedBy;

	/** The modified dt. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DT")
	private Date modifiedDt;

	/** The country dial code. */
	@Column(name = "FAX_CNTRY_DIAL_CD", length = 2)
	private String cntryFaxDialCd;
	
	/** The sequnce number for primary contact */
    @Column(name = "PRIMARY_CONTACT_SEQUENCE")
    private Long primaryContactSequence;

	/**
	 * @return the lastNm
	 */
	public String getLastNm() {
		return lastNm;
	}

	/**
	 * @param lastNm
	 *            the lastNm to set
	 */
	public void setLastNm(String lastNm) {
		this.lastNm = lastNm;
	}

	/**
	 * @return the firstNm
	 */
	public String getFirstNm() {
		return firstNm;
	}

	/**
	 * @param firstNm
	 *            the firstNm to set
	 */
	public void setFirstNm(String firstNm) {
		this.firstNm = firstNm;
	}

	/**
	 * @return the phoneNum
	 */
	public String getPhoneNum() {
		return phoneNum;
	}

	/**
	 * @param phoneNum
	 *            the phoneNum to set
	 */
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	/**
	 * @return the cntryDialCd
	 */
	public String getCntryDialCd() {
		return cntryDialCd;
	}

	/**
	 * @param cntryDialCd
	 *            the cntryDialCd to set
	 */
	public void setCntryDialCd(String cntryDialCd) {
		this.cntryDialCd = cntryDialCd;
	}

	/**
	 * @return the phoneExt
	 */
	public Long getPhoneExt() {
		return phoneExt;
	}

	/**
	 * @param phoneExt
	 *            the phoneExt to set
	 */
	public void setPhoneExt(Long phoneExt) {
		this.phoneExt = phoneExt;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the countryCd
	 */
	public String getCountryCd() {
		return countryCd;
	}

	/**
	 * @param countryCd
	 *            the countryCd to set
	 */
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}

	/**
	 * @return the faxNum
	 */
	public String getFaxNum() {
		return faxNum;
	}

	/**
	 * @param faxNum
	 *            the faxNum to set
	 */
	public void setFaxNum(String faxNum) {
		this.faxNum = faxNum;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}

	/**
	 * @param createdDt
	 *            the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}

	/**
	 * @param modifiedDt
	 *            the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public String getCntryFaxDialCd() {
		return cntryFaxDialCd;
	}

	public void setCntryFaxDialCd(String cntryFaxDialCd) {
		this.cntryFaxDialCd = cntryFaxDialCd;
	}

	/**
	 * Compare the contacts using created date.
	 * 
	 * @param contact1
	 * @param contact2
	 * @return
	 */
	@Override
	public int compareTo(ReportContactEntity contact) {
		if(this.getPrimaryContactSequence() == null){
			return -1;
		} else if (contact.getPrimaryContactSequence() == null) {
			return 1;
		} else {
			return contact.getPrimaryContactSequence().compareTo(this.getPrimaryContactSequence());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		String hashString = this.lastNm + " " + this.firstNm + " " + this.emailAddress + " " + this.contactId;
		return hashString.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (obj != null && getClass() == obj.getClass()) {
			if (this == obj || ((ReportContactEntity) obj).hashCode() == this.hashCode()) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("[last name=").append(this.lastNm).append("]");
		return builder.toString();
	}

	/**
	 * @return the contactId
	 */
	public long getContactId() {
		return contactId;
	}

	/**
	 * @param contactId
	 *            the contactId to set
	 */
	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	/**
	 * @return the companyId
	 */
	public long getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId
	 *            the companyId to set
	 */
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the permitId
	 */
	public long getPermitId() {
		return permitId;
	}

	/**
	 * @param permitId
	 *            the permitId to set
	 */
	public void setPermitId(long permitId) {
		this.permitId = permitId;
	}

	/**
	 * @return the periodId
	 */
	public long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId
	 *            the periodId to set
	 */
	public void setPeriodId(long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the primaryContactSequence
	 */
	public Long getPrimaryContactSequence() {
		return primaryContactSequence;
	}

	/**
	 * @param primaryContactSequence the primaryContactSequence to set
	 */
	public void setPrimaryContactSequence(Long primaryContactSequence) {
		this.primaryContactSequence = primaryContactSequence;
	}

}

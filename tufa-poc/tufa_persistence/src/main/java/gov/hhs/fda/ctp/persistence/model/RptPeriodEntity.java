/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class RptPeriodEntity.
 *
 * @author tgunter
 */
@Entity
@Table(name = "TU_RPT_PERIOD")
public class RptPeriodEntity extends BaseEntity {
    
    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
    
    /** The period id. */
    @Id
    @Column(name = "PERIOD_ID", unique = true, nullable = false)
    private Long periodId;
    
    /** The year. */
    @Column(name = "YEAR")
    private Long year;
    
    /** The month. */
    @Column(name="MONTH")
    private String month;
    
    /** The quarter. */
    @Column(name="QUARTER")
    private Long quarter;
    
    /** The fiscal yr. */
    @Column(name="FISCAL_YR")
    private Long fiscalYr;
    
    /** The created by. */
    @Column(name="CREATED_BY")
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;
    
    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;

    /**
     * Gets the period id.
     *
     * @return the periodId
     */
    public Long getPeriodId() {
        return periodId;
    }
    
    /**
     * Sets the period id.
     *
     * @param periodId the periodId to set
     */
    public void setPeriodId(Long periodId) {
        this.periodId = periodId;
    }
    
    /**
     * Gets the year.
     *
     * @return the year
     */
    public Long getYear() {
        return year;
    }
    
    /**
     * Sets the year.
     *
     * @param year the year to set
     */
    public void setYear(Long year) {
        this.year = year;
    }
    
    /**
     * Gets the month.
     *
     * @return the month
     */
    public String getMonth() {
        return month;
    }
    
    /**
     * Sets the month.
     *
     * @param month the month to set
     */
    public void setMonth(String month) {
        this.month = month;
    }
    
    /**
     * Gets the quarter.
     *
     * @return the quarter
     */
    public Long getQuarter() {
        return quarter;
    }
    
    /**
     * Sets the quarter.
     *
     * @param quarter the quarter to set
     */
    public void setQuarter(Long quarter) {
        this.quarter = quarter;
    }
    
    /**
     * Gets the fiscal yr.
     *
     * @return the fiscalYr
     */
    public Long getFiscalYr() {
        return fiscalYr;
    }
    
    /**
     * Sets the fiscal yr.
     *
     * @param fiscalYr the fiscalYr to set
     */
    public void setFiscalYr(Long fiscalYr) {
        this.fiscalYr = fiscalYr;
    }
    
    /**
     * Gets the created by.
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }
    
    /**
     * Sets the created by.
     *
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
    /**
     * Gets the created dt.
     *
     * @return the createdDt
     */
    public Date getCreatedDt() {
        return createdDt;
    }
    
    /**
     * Sets the created dt.
     *
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }
    
    /**
     * Gets the modified by.
     *
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }
    
    /**
     * Sets the modified by.
     *
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    
    /**
     * Gets the modified dt.
     *
     * @return the modifiedDt
     */
    public Date getModifiedDt() {
        return modifiedDt;
    }
    
    /**
     * Sets the modified dt.
     *
     * @param modifiedDt the modifiedDt to set
     */
    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }
    
}

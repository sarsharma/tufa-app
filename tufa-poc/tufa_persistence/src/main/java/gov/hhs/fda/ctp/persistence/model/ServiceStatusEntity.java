package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="TU_ANNUAL_SERVICE_STATUS")
public class ServiceStatusEntity extends BaseEntity {
//	@Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name="ID")
//	private Long id;
//	
	@Id
	@Column(name="FISCAL_YEAR")
	private Long fiscalYear;
	
	@Id
	@Column(name="SERVICE_NAME", length = 30)
	private String serviceName;
	
	@Column(name="STATUS", length = 20)
	private String status;
	
    @Column(name = "CREATED_BY", length = 50, updatable=false)
    private String createdBy;
    
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT", insertable = false, updatable = false)
    private Date createdDt;
    
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;
    
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")    
    private Date modifiedDt;

	/**
	 * @return the id
	 */
//	public Long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(Long id) {
//		this.id = id;
//	}

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the fiscalYear
	 */
	public Long getFiscalYear() {
		return fiscalYear;
	}

	/**
	 * @param fiscalYear the fiscalYear to set
	 */
	public void setFiscalYear(Long fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}

	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}

	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
    
    
}

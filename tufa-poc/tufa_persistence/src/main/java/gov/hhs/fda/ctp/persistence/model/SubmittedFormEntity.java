/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;

/**
 * The Class SubmittedFormEntity.
 *
 * @author tgunter
 */

@Entity
@Table(name = "TU_SUBMITTED_FORM")
public class SubmittedFormEntity extends BaseEntity {
    
    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
    
    /** The form id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FORM_ID", unique = true, nullable = false)
    private Long formId;
    
    /** The permit id. */
    @Column(name = "PERMIT_ID")
    private Long permitId;
    
    /** The period id. */
    @Column(name="PERIOD_ID")
    private Long periodId;
    
    /** The form type cd. */
    @Column(name = "FORM_TYPE_CD")
    private String formTypeCd;
    
    /** The form num. */
    @Column(name = "FORM_NUM")
    private Long formNum;
    
    /** The created by. */
    @Column(name="CREATED_BY")
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;
    
    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    
    /** The permit period entity. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns( {
        @JoinColumn(name = "PERMIT_ID", referencedColumnName="PERMIT_ID", insertable = false, updatable = false),
        @JoinColumn(name = "PERIOD_ID", referencedColumnName="PERIOD_ID", insertable = false, updatable = false)
    })
    private PermitPeriodEntity permitPeriodEntity;
    
    /** The form details. */
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "submittedFormEntity", cascade= CascadeType.REMOVE)
    private Set<FormDetailEntity> formDetails;
    
    
    /*
     * selected tobacco types
     */
    @Column(name="SEL_TOBACCO_TYPES")
    private String selectedTobaccoTypes;

    /**
     * Gets the form id.
     *
     * @return the formId
     */
    public Long getFormId() {
        return formId;
    }
    
    /**
     * Sets the form id.
     *
     * @param formId the formId to set
     */
    public void setFormId(Long formId) {
        this.formId = formId;
    }
    
    /**
     * Gets the permit id.
     *
     * @return the permitId
     */
    public Long getPermitId() {
        return permitId;
    }
    
    /**
     * Sets the permit id.
     *
     * @param permitId the permitId to set
     */
    public void setPermitId(Long permitId) {
        this.permitId = permitId;
    }
    
    /**
     * Gets the period id.
     *
     * @return the periodId
     */
    public Long getPeriodId() {
        return periodId;
    }
    
    /**
     * Sets the period id.
     *
     * @param periodId the periodId to set
     */
    public void setPeriodId(Long periodId) {
        this.periodId = periodId;
    }
    
    /**
     * Gets the form type cd.
     *
     * @return the formTypeCd
     */
    public String getFormTypeCd() {
        return formTypeCd;
    }
    
    /**
     * Sets the form type cd.
     *
     * @param formTypeCd the formTypeCd to set
     */
    public void setFormTypeCd(String formTypeCd) {
        this.formTypeCd = formTypeCd;
    }
    
    /**
     * Gets the form num.
     *
     * @return the formNum
     */
    public Long getFormNum() {
        return formNum;
    }
    
    /**
     * Sets the form num.
     *
     * @param formNum the formNum to set
     */
    public void setFormNum(Long formNum) {
        this.formNum = formNum;
    }
    
    /**
     * Gets the created by.
     *
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }
    
    /**
     * Sets the created by.
     *
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
    /**
     * Gets the created dt.
     *
     * @return the createdDt
     */
    public Date getCreatedDt() {
        return createdDt;
    }
    
    /**
     * Sets the created dt.
     *
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }
    
    /**
     * Gets the modified by.
     *
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }
    
    /**
     * Sets the modified by.
     *
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    
    /**
     * Gets the modified dt.
     *
     * @return the modifiedDt
     */
    public Date getModifiedDt() {
        return modifiedDt;
    }
    
    /**
     * Sets the modified dt.
     *
     * @param modifiedDt the modifiedDt to set
     */
    public void setModifiedDt(Date modifiedDt) {
        this.modifiedDt = modifiedDt;
    }
    
    /**
     * Gets the permit period entity.
     *
     * @return the permitPeriodEntity
     */
    public PermitPeriodEntity getPermitPeriodEntity() {
        return permitPeriodEntity;
    }
    
    /**
     * Sets the permit period entity.
     *
     * @param permitPeriodEntity the permitPeriodEntity to set
     */
    public void setPermitPeriodEntity(PermitPeriodEntity permitPeriodEntity) {
        this.permitPeriodEntity = permitPeriodEntity;
    }
    
    /**
     * Gets the form details.
     *
     * @return the formDetails
     */
    public Set<FormDetailEntity> getFormDetails() {
        return formDetails;
    }
    
    /**
     * Sets the form details.
     *
     * @param formDetails the formDetails to set
     */
    public void setFormDetails(Set<FormDetailEntity> formDetails) {
        this.formDetails = formDetails;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SubmittedFormEntity form = (SubmittedFormEntity)obj;
        return toString().equals(form.toString());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("#"+this.permitId+"#"+this.periodId+"#"+this.formTypeCd+ "#" + this.formNum + "#");
        return builder.toString();
    }

	/**
	 * @return the selectedTobaccoTypes
	 */
	public String getSelectedTobaccoTypes() {
		return selectedTobaccoTypes;
	}

	/**
	 * @param selectedTobaccoTypes the selectedTobaccoTypes to set
	 */
	public void setSelectedTobaccoTypes(String selectedTobaccoTypes) {
		this.selectedTobaccoTypes = selectedTobaccoTypes;
	}


	
    
}

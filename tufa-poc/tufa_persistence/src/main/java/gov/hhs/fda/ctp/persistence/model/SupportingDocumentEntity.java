package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class AttachmentsEntity.
 *
 * @author rnasina
 */
@Entity
@Table(name="TU_ASSESSMENT_DOCUMENT")
public class SupportingDocumentEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** The document number */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ASMNT_DOC_ID", unique = true, nullable=false)
    private long asmntDocId;

	/** The assessment id. */
    @Column(name = "ASSESSMENT_ID", columnDefinition="NUMBER(4,0)", nullable=false)
    private long assessmentId;

	/** The document number */
    @Column(name = "DOCUMENT_NUMBER", columnDefinition="NUMBER(4,0)", nullable=false)
    private long documentNumber;

	/** The filename. */
	@Column(name = "FILENAME", columnDefinition="CHAR(200)")
    private String filename;

	/** The description. */
	@Column(name = "DESCRIPTION", columnDefinition="CHAR(200)")
    private String description;

    /** The version number. */
    @Column(name = "VERSION_NUM", columnDefinition="NUMBER(4,0)")
    private int versionNum;

    /** The dt uploaded. */
    @Temporal(TemporalType.DATE)
    @Column(name="DATE_UPLOADED")
    private Date dateUploaded;

	/** The description. */
	@Column(name = "AUTHOR", columnDefinition="CHAR(50)")
    private String author;

    /** The assessment pdf. */
    @Lob @Basic(fetch = FetchType.LAZY)
	@Column(name="ASSESSMENT_PDF")
    private byte[] assessmentPdf;

    /** The permit period entity. */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ASSESSMENT_ID", referencedColumnName="ASSESSMENT_ID", insertable = false, updatable = false)
    private AssessmentEntity assessmentEntity;

    /** The created by. */
    @Column(name = "CREATED_BY", length=50)
    private String createdBy;

    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="CREATED_DT")
    private Date createdDt;

    /** The modified by. */
    @Column(name = "MODIFIED_BY", length=50)
    private String modifiedBy;

    /** The modified dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;

	public long getAsmntDocId() {
		return asmntDocId;
	}

	public void setAsmntDocId(long asmntDocId) {
		this.asmntDocId = asmntDocId;
	}

	public long getAssessmentId() {
		return assessmentId;
	}

	public void setAssessmentId(long assessmentId) {
		this.assessmentId = assessmentId;
	}

	public long getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(long documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getVersionNum() {
		return versionNum;
	}

	public void setVersionNum(int versionNum) {
		this.versionNum = versionNum;
	}

	public Date getDateUploaded() {
		return dateUploaded;
	}

	public void setDateUploaded(Date dateUploaded) {
		this.dateUploaded = dateUploaded;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public byte[] getAssessmentPdf() {
		return assessmentPdf;
	}

	public void setAssessmentPdf(String assessmentPdf) {
		this.assessmentPdf = assessmentPdf.getBytes();
	}

	public AssessmentEntity getAssessment() {
		return assessmentEntity;
	}

	public void setAssessment(AssessmentEntity assessment) {
		this.assessmentEntity = assessment;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
}

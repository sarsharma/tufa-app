/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class FormDetailEntity.
 *
 *
 */
@Entity
@Table(name = "TU_TTB_ADJUSTMENT")
public class TTBAdjustmentDetailEntity extends BaseEntity {
    
    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
    
    /** The form id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ADJUSTMENT_FORM_ID", unique = true, nullable = false)
    private Long adjFormId;
    
    @Column(name="FORM_ID")
    private Long formId;
    
    /** The tobacco class id. */
    @Column(name="TOBACCO_CLASS_ID")
    private Long tobaccoClassId;
    
    /** The line num. */
    @Column(name="ADJ_LINE_NUM")
    private Long adjLineNum;
    
        /** The taxes paid. */
    @Column(name="TAX_AMOUNT")
    private Double taxAmount;
    
   /** The activity cd. */
    @Column(name="TAX_RETURN_ID")
    private String taxReturnId;
    
    /** The activity cd. */
    @Column(name="ADJ_TYPE")
    private String adjType;
    
    /** The activity cd. */
    @Column(name="LINE_DESC")
    private String lineDesc;
    
    
    /** The created by. */
    @Column(name="CREATED_BY")
    private String createdBy;
   
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;
    
    public Long getAdjFormId() {
		return adjFormId;
	}

	public void setAdjFormId(Long adjFormId) {
		this.adjFormId = adjFormId;
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}

	public void setTobaccoClassId(Long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}

	public Long getAdjLineNum() {
		return adjLineNum;
	}

	public void setAdjLineNum(Long adjLineNum) {
		this.adjLineNum = adjLineNum;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getTaxReturnId() {
		return taxReturnId;
	}

	public void setTaxReturnId(String taxReturnId) {
		this.taxReturnId = taxReturnId;
	}

	public String getAdjType() {
		return adjType;
	}

	public void setAdjType(String adjType) {
		this.adjType = adjType;
	}


	public String getLineDesc() {
		return lineDesc;
	}

	public void setLineDesc(String lineDesc) {
		this.lineDesc = lineDesc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	/** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    

	
	
}

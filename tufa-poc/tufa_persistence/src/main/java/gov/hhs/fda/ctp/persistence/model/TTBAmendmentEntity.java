package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity
@Table(name="TU_TTB_AMENDMENT")
public class TTBAmendmentEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TTB_AMENDMENT_ID", nullable = false)
    private Long ttbAmendmentId;
	
	@Column(name = "TTB_COMPANY_ID")
    private Long ttbCompanyId;
	
	@Column(name = "TOBACCO_CLASS_ID")
    private Long tobaccoClassId;
	
	@Column(name = "FISCAL_YR")
    private String fiscalYr;
	
	@Column(name="QTR")
	private String qtr;
	
	@Column(name="AMENDED_TOTAL_TAX")
	private Double amendedTotalTax;
	
	@Column(name="ACCEPTED_INGESTED_TOTAL_TAX")
	private Double acceptedIngestedTotalTax;
		
	@Column(name="ACCEPTANCE_FLAG")
	private String acceptanceFlag;
	
	@Column(name="AMENDED_TOTAL_VOLUME")
	private Double amendedTotalVolume;
		
	@Column(name="IN_PROGRESS_FLAG")
	private String inProgressFlag;
	
	@Column(name="COMMENTS")
	private String comments;
	
	@Column(name="FDA_DELTA")
	private Double fdaDelta;
	
	@Column(name="EDGE_STATUS_FLAG")
	private String edgeStatusFlag;
	
	@Column(name="PREV_ACCEPT_FLAG")
	private String previousAcceptenceFlag;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="TOBACCO_CLASS_ID",insertable=false,updatable=false)
	private TobaccoClassEntity tobaccoClass;
	
	/** The created by. */
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="CREATED_DT")
    private Date createdDt;

    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;

    /** The modified dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;
    
    @Column(name="ADJ_PROCESS_FLAG")
	private String adjProcessFlag;
    
    
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "TU_TTB_DETAIL_QTR_COMMENTS", 
        joinColumns = { @JoinColumn(name = "TTB_AMENDMENT_ID") }, 
        inverseJoinColumns = { @JoinColumn(name = "COMMENT_SEQ") }
    )
    private List<TTBQtrCommentEntity> userComments;

    @Transient
    private Map<String,String> mixedActionQuarterDetails;
    
    @Transient
    private Double mixedActionVolume;
    
    @Transient
    private Double mixedActionTax;
    
    public Map<String, String> getMixedActionQuarterDetails() {
		return mixedActionQuarterDetails;
	}

	public void setMixedActionQuarterDetails(Map<String, String> mixedActionQuarterDetails) {
		this.mixedActionQuarterDetails = mixedActionQuarterDetails;
	}
	
	public Double getMixedActionVolume() {
		return mixedActionVolume;
	}

	public void setMixedActionVolume(Double mixedActionVolume) {
		this.mixedActionVolume = mixedActionVolume;
	}

	public Double getMixedActionTax() {
		return mixedActionTax;
	}

	public void setMixedActionTax(Double mixedActionTax) {
		this.mixedActionTax = mixedActionTax;
	}
    
	public Long getTtbAmendmentId() {
		return ttbAmendmentId;
	}

	public void setTtbAmendmentId(Long ttbAmendmentId) {
		this.ttbAmendmentId = ttbAmendmentId;
	}

	public Long getTtbCompanyId() {
		return ttbCompanyId;
	}

	public void setTtbCompanyId(Long ttbCompanyId) {
		this.ttbCompanyId = ttbCompanyId;
	}

	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}

	public void setTobaccoClassId(Long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}

	public String getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getQtr() {
		return qtr;
	}

	public void setQtr(String qtr) {
		this.qtr = qtr;
	}

	public Double getAmendedTotalTax() {
		return amendedTotalTax;
	}

	public void setAmendedTotalTax(Double amendedTotalTax) {
		this.amendedTotalTax = amendedTotalTax;
	}

	public Double getAmendedTotalVolume() {
		return amendedTotalVolume;
	}

	public void setAmendedTotalVolume(Double amendedTotalVolume) {
		this.amendedTotalVolume = amendedTotalVolume;
	}

	/**
	 * @return the acceptedIngestedTotalTax
	 */
	public Double getAcceptedIngestedTotalTax() {
		return acceptedIngestedTotalTax;
	}

	/**
	 * @param acceptedIngestedTotalTax the acceptedIngestedTotalTax to set
	 */
	public void setAcceptedIngestedTotalTax(Double acceptedIngestedTotalTax) {
		this.acceptedIngestedTotalTax = acceptedIngestedTotalTax;
	}

	public String getAcceptanceFlag() {
		return acceptanceFlag;
	}

	public void setAcceptanceFlag(String acceptanceFlag) {
		this.acceptanceFlag = acceptanceFlag;
	}

	public String getInProgressFlag() {
		return inProgressFlag;
	}

	public void setInProgressFlag(String inProgressFlag) {
		this.inProgressFlag = inProgressFlag;
	}
	
	
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public String getEdgeStatusFlag() {
		return edgeStatusFlag;
	}

	public void setEdgeStatusFlag(String edgeStatusFlag) {
		this.edgeStatusFlag = edgeStatusFlag;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Double getFdaDelta() {
		return fdaDelta;
	}

	public void setFdaDelta(Double fdaDelta) {
		this.fdaDelta = fdaDelta;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public List<TTBQtrCommentEntity> getUserComments() {
		return userComments;
	}

	public void setUserComments(List<TTBQtrCommentEntity> userComments) {
		this.userComments = userComments;
	}

	public TobaccoClassEntity getTobaccoClass() {
		return tobaccoClass;
	}

	public void setTobaccoClass(TobaccoClassEntity tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
	
	public void setPreviousAcceptanceFlag(String flag) {
		this.previousAcceptenceFlag = flag;
	}
	
	public String getPreviousAcceptanceflag() {
		return this.previousAcceptenceFlag;
	}

	public String getAdjProcessFlag() {
		return adjProcessFlag;
	}

	public void setAdjProcessFlag(String adjProcessFlag) {
		this.adjProcessFlag = adjProcessFlag;
	}
	
	
}

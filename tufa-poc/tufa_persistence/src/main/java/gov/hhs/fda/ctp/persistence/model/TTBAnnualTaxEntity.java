package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="TU_TTB_ANNUAL_TAX")
public class TTBAnnualTaxEntity extends BaseEntity{


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "TTB_ANNUAL_TAX_ID", nullable = false)
	private Long ttbAnnualTaxId;

	/*@Column(name = "TTB_PERMIT_ID", nullable = false)
	private Long ttbPermitId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="TTB_PERMIT_ID",insertable=false,updatable=false)
	private List<TTBPermitEntity> permits;
	*/

	@Column(name = "TTB_FISCAL_YR")
    private String ttbFiscalYr;

	@Column(name = "TTB_CALENDAR_YR")
	private String ttbCalendarYr;

	@Column(name = "TTB_CALENDAR_QTR")
	private String ttbCalendarQtr;

	@OneToOne
	@JoinColumn(name="TOBACCO_CLASS_ID")
	private TobaccoClassEntity tobaccoClass;

	@Column(name = "TTB_TAXES_PAID")
    private Double ttbTaxesPaid;

	/** The created by. */
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="CREATED_DT")
    private Date createdDt;

    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;

    /** The modified dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;

	@Column(name = "INC_ADJ")
    private Double incAdj;
	
	@Column(name = "DEC_ADJ")
    private Double decAdj;

	public Long getTtbAnnualTaxId() {
		return ttbAnnualTaxId;
	}

	public String getTtbFiscalYr() {
		return ttbFiscalYr;
	}

	public String getTtbCalendarYr() {
		return ttbCalendarYr;
	}

	public String getTtbCalendarQtr() {
		return ttbCalendarQtr;
	}

	public TobaccoClassEntity getTobaccoClass() {
		return tobaccoClass;
	}

	public Double getTtbTaxesPaid() {
		return ttbTaxesPaid;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setTtbAnnualTaxId(Long ttbAnnualTaxId) {
		this.ttbAnnualTaxId = ttbAnnualTaxId;
	}

	public void setTtbFiscalYr(String ttbFiscalYr) {
		this.ttbFiscalYr = ttbFiscalYr;
	}

	public void setTtbCalendarYr(String ttbCalendarYr) {
		this.ttbCalendarYr = ttbCalendarYr;
	}

	public void setTtbCalendarQtr(String ttbCalendarQtr) {
		this.ttbCalendarQtr = ttbCalendarQtr;
	}

	public void setTobaccoClass(TobaccoClassEntity tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}

	public void setTtbTaxesPaid(Double ttbTaxesPaid) {
		this.ttbTaxesPaid = ttbTaxesPaid;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public Double getIncAdj() {
		return incAdj;
	}

	public void setIncAdj(Double incAdj) {
		this.incAdj = incAdj;
	}

	public Double getDecAdj() {
		return decAdj;
	}

	public void setDecAdj(Double decAdj) {
		this.decAdj = decAdj;
	}


}

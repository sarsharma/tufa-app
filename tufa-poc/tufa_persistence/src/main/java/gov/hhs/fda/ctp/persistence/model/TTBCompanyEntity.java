package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="TU_TTB_COMPANY")
public class TTBCompanyEntity  extends BaseEntity{

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The id field. */
    @Id
    @Column(name = "TTB_COMPANY_ID", nullable = false)
    private Long ttbCompanyId;
    
    @Column(name = "FISCAL_YR")
    private String fiscalYr;
    
    @Column(name = "EIN_NUM")
    private String einNum;
    
    @Column(name = "COMPANY_NM")
    private String companyNm;
    
    @Column(name = "EXISTS_INTUFA_FLAG")
    private String existsInTufaFlag;
    
    @Column(name="INCLUDE_FLAG")
    private String includeFlag;
    
    /** The created by. */
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;
    
    /** The created dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="CREATED_DT")
    private Date createdDt;

    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;
    
    /** The modified dt. */
    @Temporal(TemporalType.DATE)   
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ttbCompany")
    private Set<TTBPermitEntity> permits;
    
	public Long getTtbCompanyId() {
		return ttbCompanyId;
	}

	public void setTtbCompanyId(Long ttbCompanyId) {
		this.ttbCompanyId = ttbCompanyId;
	}

	public String getFiscalYr() {
		return fiscalYr;
	}

	public void setFiscalYr(String fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getEinNum() {
		return einNum;
	}

	public void setEinNum(String einNum) {
		this.einNum = einNum;
	}

	public String getCompanyNm() {
		return companyNm;
	}

	public void setCompanyNm(String companyNm) {
		this.companyNm = companyNm;
	}

	public String getExistsInTufaFlag() {
		return existsInTufaFlag;
	}

	public void setExistsInTufaFlag(String existsInTufaFlag) {
		this.existsInTufaFlag = existsInTufaFlag;
	}

	public String getIncludeFlag() {
		return includeFlag;
	}

	public void setIncludeFlag(String includeFlag) {
		this.includeFlag = includeFlag;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public Set<TTBPermitEntity> getPermits() {
		return permits;
	}

	public void setPermits(Set<TTBPermitEntity> permits) {
		this.permits = permits;
	}
    
   /* @OneToMany(fetch = FetchType.LAZY, mappedBy = "cbpImporter")
    private Set<CBPEntryEntity> entry;*/
    
    
}

package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TU_TTB_FILE_COL_FORMAT")
public class TTBFileColumnFormatEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FORMAT_ID", unique = true, nullable = false)
	private long formatId;
	
    @Column(name = "DATE_FORMAT_1") 
	private String dateFormat1;

	public String getDateFormat1() {
		return dateFormat1;
	}

	public void setDateFormat1(String dateFormat1) {
		this.dateFormat1 = dateFormat1;
	}
	
}

package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TU_TTB_FILE_COL_TEMPLATE")
public class TTBFileColumnTemplateEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TEMPLATE_ID", unique = true, nullable = false)
	private long templateId;

	@Column(name = "COLUMN_COUNT")
	private Integer columnCount;

	@Column(name = "EIN")
	private String ein;

	@Column(name = "PERMIT_ID")
	private String permitId;

	@Column(name = "BUSINESS_NAME")
	private String businessName;

	@Column(name = "MAILING_STREET")
	private String mailingStreet;

	@Column(name = "MAILING_CITY")
	private String mailingCity;

	@Column(name = "MAILING_STATE")
	private String mailingState;

	@Column(name = "MAILING_POSTAL_CD")
	private String mailingPostalCd;

	@Column(name = "MAILING_TEXT")
	private String mailingText;

	@Column(name = "PREMISE_PHONE")
	private String premisePhone;
	
	@Column(name = "PREMISE_PHONE_2")
	private String premisePhone2;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "ISSUE_DATE")
	private String issueDate;

	@Column(name = "CLOSED_DATE")
	private String closedDate;

	@Column(name = "PERMIT_TYPE")
	private String permitType;

	@Column(name = "PERMIT_STATUS")
	private String permitStatus;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FORMAT_ID", insertable = false, updatable = false)
	private TTBFileColumnFormatEntity colFormat;
	
	public long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}

	public Integer getColumnCount() {
		return columnCount;
	}

	public void setColumnCount(Integer columnCount) {
		this.columnCount = columnCount;
	}

	public String getEin() {
		return ein;
	}

	public void setEin(String ein) {
		this.ein = ein;
	}

	public String getPermitId() {
		return permitId;
	}

	public void setPermitId(String permitId) {
		this.permitId = permitId;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getMailingStreet() {
		return mailingStreet;
	}

	public void setMailingStreet(String mailingStreet) {
		this.mailingStreet = mailingStreet;
	}

	public String getMailingCity() {
		return mailingCity;
	}

	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}

	public String getMailingState() {
		return mailingState;
	}

	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}

	public String getMailingPostalCd() {
		return mailingPostalCd;
	}

	public void setMailingPostalCd(String mailingPostalCd) {
		this.mailingPostalCd = mailingPostalCd;
	}

	public String getMailingText() {
		return mailingText;
	}

	public void setMailingText(String mailingText) {
		this.mailingText = mailingText;
	}

	public String getPremisePhone() {
		return premisePhone;
	}

	public void setPremisePhone(String premisePhone) {
		this.premisePhone = premisePhone;
	}
	
	public String getPremisePhone2() {
		return premisePhone2;
	}

	public void setPremisePhone2(String premisePhone2) {
		this.premisePhone2 = premisePhone2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}

	public String getPermitType() {
		return permitType;
	}

	public TTBFileColumnFormatEntity getColFormat() {
		return colFormat;
	}

	public void setColFormat(TTBFileColumnFormatEntity colFormat) {
		this.colFormat = colFormat;
	}

	public void setPermitType(String permitType) {
		this.permitType = permitType;
	}

	public String getPermitStatus() {
		return permitStatus;
	}

	public void setPermitStatus(String permitStatus) {
		this.permitStatus = permitStatus;
	}


}

package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

public class TTBPermitBucket {
	
	long permitId;
	String permitNum;
	long companyId;
	String companyNm;
	long totalTaxes;
	String tobaccoClasses;
	String includeFlag;
	boolean providedAnswer;
	String ein;
	Date tpbdDt;

	public String getEin() {
		return ein;
	}
	public void setEin(String ein) {
		this.ein = ein;
	}
	public boolean isProvidedAnswer() {
		return providedAnswer;
	}
	public void setProvidedAnswer(boolean providedAnswer) {
		this.providedAnswer = providedAnswer;
	}
	public String getIncludeFlag() {
		return includeFlag;
	}
	public void setIncludeFlag(String includeFlag) {
		this.includeFlag = includeFlag;
	}
	public long getPermitId() {
		return permitId;
	}
	public void setPermitId(long permitId) {
		this.permitId = permitId;
	}
	public String getPermitNum() {
		return permitNum;
	}
	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public String getCompanyNm() {
		return companyNm;
	}
	public void setCompanyNm(String companyNm) {
		this.companyNm = companyNm;
	}
	public long getTotalTaxes() {
		return totalTaxes;
	}
	public void setTotalTaxes(long totalTaxes) {
		this.totalTaxes = totalTaxes;
	}
	public String getTobaccoClasses() {
		return tobaccoClasses;
	}
	public void setTobaccoClasses(String tobaccoClasses) {
		this.tobaccoClasses = tobaccoClasses;
	}
	public Date getTpbdDt() {
		return tpbdDt;
	}
	public void setTpbdDt(Date tpbdDt) {
		this.tpbdDt = tpbdDt;
	}
}

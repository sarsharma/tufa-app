package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="TU_TTB_PERMIT")
public class TTBPermitEntity extends BaseEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "TTB_PERMIT_ID", nullable = false)
	private Long ttbPermitId;

	/** The modified dt. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_DT")
	private Date modifiedDt;

	/** The company. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TTB_COMPANY_ID", insertable = false, updatable = false)
	private TTBCompanyEntity ttbCompany;

	@Column(name = "TTB_COMPANY_ID", nullable = false)
	private long companyId;

	@Column(name = "PERMIT_NUM")
	private String permitNum;

	@Column(name = "EXISTS_INTUFA_FLAG")
	private String existsInTufaFlag;

	@Column(name = "INCLUDE_FLAG")
	private String includeFlag;

	/** The created by. */
	@Column(name = "CREATED_BY", length = 50)
	private String createdBy;

	/** The created dt. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DT")
	private Date createdDt;

	/** The modified by. */
	@Column(name = "MODIFIED_BY", length = 50)
	private String modifiedBy;

	/** The tpbd dt. */
	@Temporal(TemporalType.DATE)
	@Column(name = "TPBD_DATE")
	private Date tpbdDt;
	public Date getTpbdDt() {
		return tpbdDt;
	}

	public void setTpbdDt(Date tpbdDt) {
		this.tpbdDt = tpbdDt;
	}

	/*
	@OneToMany(fetch = FetchType.LAZY, mappedBy="permits")
	private TTBAnnualTaxEntity annualtax;
	*/
	@Transient
    private String permitStatusTypeCd;
	
	@Transient
    private String permitTypeCd;


	public Long getTtbPermitId() {
		return ttbPermitId;
	}

	public String getPermitNum() {
		return permitNum;
	}

	public String getExistsInTufaFlag() {
		return existsInTufaFlag;
	}

	public String getIncludeFlag() {
		return includeFlag;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}


	public void setTtbPermitId(Long ttbPermitId) {
		this.ttbPermitId = ttbPermitId;
	}

	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}

	public void setExistsInTufaFlag(String existsInTufaFlag) {
		this.existsInTufaFlag = existsInTufaFlag;
	}

	public void setIncludeFlag(String includeFlag) {
		this.includeFlag = includeFlag;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
	
	public TTBCompanyEntity getTtbCompany() {
		return ttbCompany;
	}

	public void setTtbCompany(TTBCompanyEntity ttbCompany) {
		this.ttbCompany = ttbCompany;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	/*
	public TTBAnnualTaxEntity getAnnualtax() {
		return annualtax;
	}

	public void setAnnualtax(TTBAnnualTaxEntity annualtax) {
		this.annualtax = annualtax;
	}
	*/

	public String getPermitStatusTypeCd() {
		return permitStatusTypeCd;
	}

	public void setPermitStatusTypeCd(String permitStatusTypeCd) {
		this.permitStatusTypeCd = permitStatusTypeCd;
	}

	public String getPermitTypeCd() {
		return permitTypeCd;
	}

	public void setPermitTypeCd(String permitTypeCd) {
		this.permitTypeCd = permitTypeCd;
	}

}

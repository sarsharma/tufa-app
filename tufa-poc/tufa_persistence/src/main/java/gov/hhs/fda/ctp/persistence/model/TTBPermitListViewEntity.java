package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;

import gov.hhs.fda.ctp.common.beans.PermitAudit;

@Immutable
@Entity
@Table(name = "TTB_PERMIT_LIST_VW")
@SqlResultSetMapping(
	    name = "ttb_permit_excl_dto_mapping",
	    classes = @ConstructorResult(
	        targetClass = PermitAudit.class,
	        columns = {
		        @ColumnResult(name = "auditId",type=Long.class),
		        @ColumnResult(name = "companyName",type=String.class),
		        @ColumnResult(name = "companyId",type=Long.class),
		        @ColumnResult(name = "einNum",type=String.class),
	            @ColumnResult(name = "permitId",type=String.class),
	            @ColumnResult(name = "permitAction",type=String.class),
	            @ColumnResult(name = "permitStatus",type=String.class),
	            @ColumnResult(name = "docStageId",type=Long.class),
	            @ColumnResult(name = "issueDt",type=String.class),
	            @ColumnResult(name = "closeDt",type=String.class),
	            @ColumnResult(name = "actionMsg",type=String.class),
	            @ColumnResult(name = "action",type=String.class),	
	            @ColumnResult(name = "permitExclResolved",type=String.class),
	            @ColumnResult(name = "actionContext",type=String.class)
	        }
	    )
	)
public class TTBPermitListViewEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AUDIT_ID", unique = true, nullable = false)
	private Long permitAuditId;

	@Column(name = "COMPANY_NAME")
	private String companyName;

	@Column(name = "COMPANY_ID")
	private Long companyId;

	@Column(name = "EIN", columnDefinition = "char(9)")
	private String einNum;

	@Column(name = "PERMIT_ID")
	private String permitNum;

	@Column(name = "PERMIT_ACTION")
	private String permitAction;

	@Column(name = "PERMIT_STATUS")
	private String permitStatus;

	@Column(name = "DOC_STAGE_ID")
	private Long docId;

	@Temporal(TemporalType.DATE)
	@Column(name = "ISSUE_DT")
	private Date issueDt;

	@Temporal(TemporalType.DATE)
	@Column(name = "CLOSE_DT")
	private Date closeDt;

	@Column(name = "ACTION_MSG")
	private String  actionMsg;
	
	@Column(name = "ACTION")
	private String  action;
	
	@Column(name = "ACTION_CONTEXT")
	private String  actionContext;
	
	@Column(name = "PERMIT_EXCL_RESOLVED")
	private String  permitExclResolved;
	
	
	public Long getPermitAuditId() {
		return permitAuditId;
	}

	public void setPermitAuditId(Long permitAuditId) {
		this.permitAuditId = permitAuditId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getEinNum() {
		return einNum;
	}

	public void setEinNum(String einNum) {
		this.einNum = einNum;
	}

	public String getPermitNum() {
		return permitNum;
	}

	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}

	public String getPermitAction() {
		return permitAction;
	}

	public void setPermitAction(String permitAction) {
		this.permitAction = permitAction;
	}

	public String getPermitStatus() {
		return permitStatus;
	}

	public void setPermitStatus(String permitStatus) {
		this.permitStatus = permitStatus;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public Date getIssueDt() {
		return issueDt;
	}

	public void setIssueDt(Date issueDt) {
		this.issueDt = issueDt;
	}

	public Date getCloseDt() {
		return closeDt;
	}

	public void setCloseDt(Date closeDt) {
		this.closeDt = closeDt;
	}

	public String getActionMsg() {
		return actionMsg;
	}

	public void setActionMsg(String actionMsg) {
		this.actionMsg = actionMsg;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getPermitExclResolved() {
		return permitExclResolved;
	}

	public void setPermitExclResolved(String permitExclResolved) {
		this.permitExclResolved = permitExclResolved;
	}

	public String getActionContext() {
		return actionContext;
	}

	public void setActionContext(String actionContext) {
		this.actionContext = actionContext;
	}
	

}

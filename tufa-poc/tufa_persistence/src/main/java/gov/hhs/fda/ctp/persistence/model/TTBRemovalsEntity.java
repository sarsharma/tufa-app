/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author tgunter
 *
 */
@Entity
@Table(name = "TU_TTB_REMOVALS")
public class TTBRemovalsEntity extends BaseEntity {
	/** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;

	@Id
    @Column(name = "TTB_REMOVALS_ID", unique = true, nullable = false)
	private Long ttbRemovalsId;
	
	@Column(name = "PERIOD")
	private String period;
	
	@Column(name = "NAME_OWNER")
	private String nameOwner;
	
	@Column(name="ID_PERMIT")
	private String idPermit;
	
	@Column(name="EIN")
	private String ein;
	
	@Column(name="PRODUCT")
	private String product;
	
	@Column(name="POUNDS")
	private Double pounds;
	
	@Column(name="FISCAL_YR")
	private Long fiscalYr;

	@Column(name="ERRORS")
	private String errors;
	
	@Column(name="UOM")
	private String uom;
	/**
	 * @return the ttbRemovalsId
	 */
	public Long getTtbRemovalsId() {
		return ttbRemovalsId;
	}

	/**
	 * @param ttbRemovalsId the ttbRemovalsId to set
	 */
	public void setTtbRemovalsId(Long ttbRemovalsId) {
		this.ttbRemovalsId = ttbRemovalsId;
	}

	/**
	 * @return the period
	 */
	public String getPeriod() {
		return period;
	}

	/**
	 * @param period the period to set
	 */
	public void setPeriod(String period) {
		this.period = period;
	}

	/**
	 * @return the nameOwner
	 */
	public String getNameOwner() {
		return nameOwner;
	}

	/**
	 * @param nameOwner the nameOwner to set
	 */
	public void setNameOwner(String nameOwner) {
		this.nameOwner = nameOwner;
	}

	/**
	 * @return the idPermit
	 */
	public String getIdPermit() {
		return idPermit;
	}

	/**
	 * @param idPermit the idPermit to set
	 */
	public void setIdPermit(String idPermit) {
		this.idPermit = idPermit;
	}

	/**
	 * @return the ein
	 */
	public String getEin() {
		return ein;
	}

	/**
	 * @param ein the ein to set
	 */
	public void setEin(String ein) {
		this.ein = ein;
	}

	/**
	 * @return the product
	 */
	public String getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(String product) {
		this.product = product;
	}

	/**
	 * @return the pounds
	 */
	public Double getPounds() {
		return pounds;
	}

	/**
	 * @param pounds the pounds to set
	 */
	public void setPounds(Double pounds) {
		this.pounds = pounds;
	}

	/**
	 * @return the fiscalYr
	 */
	public Long getFiscalYr() {
		return fiscalYr;
	}

	/**
	 * @param fiscalYr the fiscalYr to set
	 */
	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

}

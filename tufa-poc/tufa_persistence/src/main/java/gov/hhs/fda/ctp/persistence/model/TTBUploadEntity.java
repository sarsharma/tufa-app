/**
 *
 */
package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author tgunter
 *
 */
@Entity
@Table(name = "TU_TTB_UPLOAD")
public class TTBUploadEntity extends BaseEntity {

	/** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;

	@Id
    @Column(name = "TTB_UPLOAD_ID", unique = true, nullable = false)
    private Long ttbUploadId;

    @Column(name = "EIN_NUM", nullable = false)
	private String einNum;

    @Column(name = "FISCAL_YR",  nullable = false)
    private Long fiscalYr;

    @Column(name="TPBD", nullable = false)
	private String tpbd;

	@Column(name="PERMIT_NUM", nullable = false)
	private String permitNum;

	@Column(name = "COMPANY_NM", nullable = false)
	private String companyNm;

	@Column(name = "CHEWSNUFF_TAXES")
	private Double chewSnuffTaxes;

	@Column(name = "CIGARETTE_TAXES")
	private Double cigaretteTaxes;

	@Column(name = "CIGAR_TAXES")
	private Double cigarTaxes;

	@Column(name = "PIPERYO_TAXES")
	private Double pipeRyoTaxes;

	@Column(name = "INC_ADJ")
	private Double incAdj;

	@Column(name = "DEC_ADJ")
	private Double decAdj;
	
	@Column(name = "ERRORS")
	private String errors;

	/**
	 * @return the ttbUploadId
	 */
	public Long getTtbUploadId() {
		return ttbUploadId;
	}

	/**
	 * @param ttbUploadId the ttbUploadId to set
	 */
	public void setTtbUploadId(Long ttbUploadId) {
		this.ttbUploadId = ttbUploadId;
	}

	/**
	 * @return the einNum
	 */
	public String getEinNum() {
		return einNum;
	}

	/**
	 * @param einNum the einNum to set
	 */
	public void setEinNum(String einNum) {
		this.einNum = einNum;
	}

	/**
	 * @return the fiscalYr
	 */
	public Long getFiscalYr() {
		return fiscalYr;
	}

	/**
	 * @param fiscalYr the fiscalYr to set
	 */
	public void setFiscalYr(Long fiscalYr) {
		this.fiscalYr = fiscalYr;
	}

	/**
	 * @return the calendarQtr
	 */
	public String getTpbd() {
		return tpbd;
	}

	/**
	 * @param calendarQtr the calendarQtr to set
	 */
	public void setTpbd(String tpbd) {
		this.tpbd = tpbd;
	}

	/**
	 * @return the permitNum
	 */
	public String getPermitNum() {
		return permitNum;
	}

	/**
	 * @param permitNum the permitNum to set
	 */
	public void setPermitNum(String permitNum) {
		this.permitNum = permitNum;
	}

	/**
	 * @return the companyNm
	 */
	public String getCompanyNm() {
		return companyNm;
	}

	/**
	 * @param companyNm the companyNm to set
	 */
	public void setCompanyNm(String companyNm) {
		this.companyNm = companyNm;
	}

	/**
	 * @return the chewSnuffTaxes
	 */
	public Double getChewSnuffTaxes() {
		return chewSnuffTaxes;
	}

	/**
	 * @param chewSnuffTaxes the chewSnuffTaxes to set
	 */
	public void setChewSnuffTaxes(Double chewSnuffTaxes) {
		this.chewSnuffTaxes = chewSnuffTaxes;
	}

	/**
	 * @return the cigaretteTaxes
	 */
	public Double getCigaretteTaxes() {
		return cigaretteTaxes;
	}

	/**
	 * @param cigaretteTaxes the cigaretteTaxes to set
	 */
	public void setCigaretteTaxes(Double cigaretteTaxes) {
		this.cigaretteTaxes = cigaretteTaxes;
	}

	/**
	 * @return the cigarTaxes
	 */
	public Double getCigarTaxes() {
		return cigarTaxes;
	}

	/**
	 * @param cigarTaxes the cigarTaxes to set
	 */
	public void setCigarTaxes(Double cigarTaxes) {
		this.cigarTaxes = cigarTaxes;
	}

	/**
	 * @return the pipeRyoTaxes
	 */
	public Double getPipeRyoTaxes() {
		return pipeRyoTaxes;
	}

	/**
	 * @param pipeRyoTaxes the pipeRyoTaxes to set
	 */
	public void setPipeRyoTaxes(Double pipeRyoTaxes) {
		this.pipeRyoTaxes = pipeRyoTaxes;
	}

	/**
	 * @return the errors
	 */
	public String getErrors() {
		return errors;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(String errors) {
		this.errors = errors;
	}

	public Double getIncAdj() {
		return incAdj;
	}

	public void setIncAdj(Double incAdj) {
		this.incAdj = incAdj;
	}

	public Double getDecAdj() {
		return decAdj;
	}

	public void setDecAdj(Double decAdj) {
		this.decAdj = decAdj;
	}

}

package gov.hhs.fda.ctp.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TU_TABLEAU_DASHBOARD_RPT_TYPES")
public class TableauDashboardRptTypeEntity {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DASHBOARDRPT_TYPE_ID", unique = true, nullable = false)
    private Long dashboardRptTypeId;
	
	@Column(name = "DASHBOARD_TYPE", unique=true, nullable = false)
	private String dashboardType;
	
	@Column(name = "DISPLAY_VALUE", unique=true, nullable = false)
	private String displayValue;
	
	@Column(name = "ENABLE", unique=true, nullable = false)
	private Boolean enable;

	public String getDashboardType() {
		return dashboardType;
	}

	public void setDashboardType(String dashboardType) {
		this.dashboardType = dashboardType;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}

	public Long getDashboardRptTypeId() {
		return dashboardRptTypeId;
	}

	public void setDashboardRptTypeId(Long dashboardRptTypeId) {
		this.dashboardRptTypeId = dashboardRptTypeId;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

}

/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Anthony.Gunter
 *
 */
@Entity
@Table(name = "tu_tax_rate")
public class TaxRateEntity {
	/** tax rate id */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TAX_RATE_ID", unique = true, nullable=false)
    private Long taxRateId;

    @Column(name="TOBACCO_CLASS_ID")
    private Long tobaccoClassId;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "TOBACCO_CLASS_ID", insertable = false, updatable = false)
    private TobaccoClassEntity tobaccoClass;
        
    /** The tax rate. */
    @Column(name = "TAX_RATE")
    private Double taxRate;

    /** The tax rate. */
    @Column(name = "CONVERSION_RATE")
    private Double conversionRate;

    /** The tax rate effective rate. */
    @Temporal(TemporalType.DATE)   
    @Column(name = "TAX_RATE_EFFECTIVE_DATE")
    private Date taxRateEffectiveDate;

    /** The tax rate end date. */
    @Temporal(TemporalType.DATE)   
    @Column(name = "TAX_RATE_END_DATE")
    private Date taxRateEndDate;

	/**
	 * @return the taxRateId
	 */
	public Long getTaxRateId() {
		return taxRateId;
	}

	/**
	 * @param taxRateId the taxRateId to set
	 */
	public void setTaxRateId(Long taxRateId) {
		this.taxRateId = taxRateId;
	}

	/**
	 * @return the tobaccoClassId
	 */
	public Long getTobaccoClassId() {
		return tobaccoClassId;
	}

	/**
	 * @param tobaccoClassId the tobaccoClassId to set
	 */
	public void setTobaccoClassId(Long tobaccoClassId) {
		this.tobaccoClassId = tobaccoClassId;
	}

	/**
	 * @return the tobaccoClass
	 */
	public TobaccoClassEntity getTobaccoClass() {
		return tobaccoClass;
	}

	/**
	 * @param tobaccoClass the tobaccoClass to set
	 */
	public void setTobaccoClass(TobaccoClassEntity tobaccoClass) {
		this.tobaccoClass = tobaccoClass;
	}

	/**
	 * @return the taxRate
	 */
	public Double getTaxRate() {
		return taxRate;
	}

	/**
	 * @param taxRate the taxRate to set
	 */
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	/**
	 * @return the conversionRate
	 */
	public Double getConversionRate() {
		return conversionRate;
	}

	/**
	 * @param conversionRate the conversionRate to set
	 */
	public void setConversionRate(Double conversionRate) {
		this.conversionRate = conversionRate;
	}

	/**
	 * @return the taxRateEffectiveDate
	 */
	public Date getTaxRateEffectiveDate() {
		return taxRateEffectiveDate;
	}

	/**
	 * @param taxRateEffectiveDate the taxRateEffectiveDate to set
	 */
	public void setTaxRateEffectiveDate(Date taxRateEffectiveDate) {
		this.taxRateEffectiveDate = taxRateEffectiveDate;
	}

	/**
	 * @return the taxRateEndDate
	 */
	public Date getTaxRateEndDate() {
		return taxRateEndDate;
	}

	/**
	 * @param taxRateEndDate the taxRateEndDate to set
	 */
	public void setTaxRateEndDate(Date taxRateEndDate) {
		this.taxRateEndDate = taxRateEndDate;
	}
    
	/**
	 * Sort by end date.
	 * @param taxRate
	 * @return
	 */
	public int compareTo(TaxRateEntity taxRate) {
		return this.taxRateEndDate.compareTo(taxRate.getTaxRateEndDate());
	}
	
}

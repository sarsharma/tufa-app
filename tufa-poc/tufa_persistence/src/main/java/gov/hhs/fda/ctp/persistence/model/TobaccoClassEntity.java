/**
 *
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The Class TobaccoClassEntity.
 *
 * @author tgunter
 */
@Entity
@Table(name = "tu_tobacco_class")
public class TobaccoClassEntity extends BaseEntity {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

    /** The tobacco class id. */
    @Id
    @Column(name = "TOBACCO_CLASS_ID", unique = true)
    private Long tobaccoClassId;

    /** The parent class id. */
    @Column(name = "PARENT_CLASS_ID")
    private Long parentClassId;

    /** The tobacco class nm. */
    @Column(name = "TOBACCO_CLASS_NM", nullable=false)
    private String tobaccoClassNm;

    /** The class type cd. */
    @Column(name = "CLASS_TYPE_CD", nullable=false)
    private String classTypeCd;

    /** The class threshold value. */
    @Column(name = "THRESHOLD_VALUE", nullable=false)
    private Double thresholdValue;
    
    public Double getThresholdValue() {
		return thresholdValue;
	}

	public void setThresholdValue(Double thresholdValue) {
		this.thresholdValue = thresholdValue;
	}

	/** Tax rates have been extracted from tobacco class */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tobaccoClass")
    private List<TaxRateEntity> taxRates;

    /**
     * Gets the tobacco class nm.
     *
     * @return the tobaccoClassNm
     */
    public String getTobaccoClassNm() {
        return tobaccoClassNm;
    }

    /**
     * Sets the tobacco class nm.
     *
     * @param tobaccoClassNm the tobaccoClassNm to set
     */
    public void setTobaccoClassNm(String tobaccoClassNm) {
        this.tobaccoClassNm = tobaccoClassNm;
    }

    /**
     * Gets the class type cd.
     *
     * @return the classTypeCd
     */
    public String getClassTypeCd() {
        return classTypeCd;
    }

    /**
     * Sets the class type cd.
     *
     * @param classTypeCd the classTypeCd to set
     */
    public void setClassTypeCd(String classTypeCd) {
        this.classTypeCd = classTypeCd;
    }

    /**
     * Gets the tobacco class id.
     *
     * @return the tobaccoClassId
     */
    public Long getTobaccoClassId() {
        return tobaccoClassId;
    }

    /**
     * Sets the tobacco class id.
     *
     * @param tobaccoClassId the tobaccoClassId to set
     */
    public void setTobaccoClassId(Long tobaccoClassId) {
        this.tobaccoClassId = tobaccoClassId;
    }

    /**
     * Gets the parent class id.
     *
     * @return the parentClassId
     */
    public Long getParentClassId() {
        return parentClassId;
    }

    /**
     * Sets the parent class id.
     *
     * @param parentClassId the parentClassId to set
     */
    public void setParentClassId(Long parentClassId) {
        this.parentClassId = parentClassId;
    }

	/**
	 * @return the taxRates
	 */
	public List<TaxRateEntity> getTaxRates() {
		return taxRates;
	}

	/**
	 * @param taxRates the taxRates to set
	 */
	public void setTaxRates(List<TaxRateEntity> taxRates) {
		this.taxRates = taxRates;
	}

}

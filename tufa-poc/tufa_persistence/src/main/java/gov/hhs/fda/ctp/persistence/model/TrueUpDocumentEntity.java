/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author tgunter
 *
 */
@Entity
@Table(name = "TU_TRUEUP_DOCUMENT")
public class TrueUpDocumentEntity extends BaseEntity {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The assessment id. */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TRUEUP_DOC_ID", unique = true, nullable=false) 
	private Long trueUpDocId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRUEUP_ID", insertable = false, updatable = false)
    private TrueUpEntity trueUpEntity;
    
    @Column(name = "TRUEUP_ID", nullable = false)
	private long trueUpId;

	@Column(name = "TRUEUP_FILENAME")
	private String trueUpFilename;

	@Column(name = "DOC_DESC")
	private String docDesc;
	
	@Column(name = "ERROR_COUNT")
	private Long errorCount;
	
	@JsonIgnore
	@Lob @Basic(fetch = FetchType.LAZY)
	@Column(name="TRUEUP_DOCUMENT")
	private byte[] trueupDocument;

	@Column(name = "CREATED_BY")
	private String createdBy;
	
    @Temporal(TemporalType.DATE)
	@Column(name = "CREATED_DT")
	private Date createdDt;
	
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
	
    @Temporal(TemporalType.DATE)   
	@Column(name="MODIFIED_DT")
	private Date modifiedDt;

	/**
	 * @return the trueUpDocId
	 */
	public Long getTrueUpDocId() {
		return trueUpDocId;
	}

	/**
	 * @return the trueUpEntity
	 */
	public TrueUpEntity getTrueUpEntity() {
		return trueUpEntity;
	}

	/**
	 * @param trueUpEntity the trueUpEntity to set
	 */
	public void setTrueUpEntity(TrueUpEntity trueUpEntity) {
		this.trueUpEntity = trueUpEntity;
	}

	/**
	 * @param trueUpDocId the trueUpDocId to set
	 */
	public void setTrueUpDocId(Long trueUpDocId) {
		this.trueUpDocId = trueUpDocId;
	}
	
	public long getTrueUpId() {
		return trueUpId;
	}

	public void setTrueUpId(long trueUpId) {
		this.trueUpId = trueUpId;
	}

	/**
	 * @return the trueUpFilename
	 */
	public String getTrueUpFilename() {
		return trueUpFilename;
	}

	/**
	 * @param trueUpFilename the trueUpFilename to set
	 */
	public void setTrueUpFilename(String trueUpFilename) {
		this.trueUpFilename = trueUpFilename;
	}

	/**
	 * @return the docDesc
	 */
	public String getDocDesc() {
		return docDesc;
	}

	/**
	 * @param docDesc the docDesc to set
	 */
	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}

	/**
	 * @return the errorCount
	 */
	public Long getErrorCount() {
		return errorCount;
	}

	/**
	 * @param errorCount the errorCount to set
	 */
	public void setErrorCount(Long errorCount) {
		this.errorCount = errorCount;
	}

	/**
	 * @return the trueupDocument
	 */
	public byte[] getTrueupDocument() {
		return trueupDocument;
	}

	/**
	 * @param trueupDocument the trueupDocument to set
	 */
	public void setTrueupDocument(byte[] trueupDocument) {
		this.trueupDocument = trueupDocument;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}

	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}

	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
	
	

}

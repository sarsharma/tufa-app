/**
 *
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author tgunter
 *
 */
@Entity
@Table(name = "TU_ANNUAL_TRUEUP")
public class TrueUpEntity extends BaseEntity {
    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TRUEUP_ID", unique = true, nullable=false) 
    private long trueUpId;
    
    @Column(name="FISCAL_YR", nullable=false)
    private Long fiscalYear;

    @Temporal(TemporalType.DATE)
    @Column(name="SUBMITTED_DT")
    private Date submittedDt;

    @JsonIgnore
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "trueUpEntity")
    @Cascade({CascadeType.ALL})
    private Set<TrueUpDocumentEntity> documents;
    
    @OneToMany(fetch=FetchType.EAGER, mappedBy = "trueUpEntity")
    @Cascade({CascadeType.ALL})
    private List<AssessmentEntity> assessments;

    /** The created by. */
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    /** The created dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="CREATED_DT")
    private Date createdDt;

    /** The modified by. */
    @Column(name = "MODIFIED_BY", length = 50)
    private String modifiedBy;
    
    /** The Submitted indicator. */
    @Column(name="SUBMITTED_IND", columnDefinition="CHAR(1)", nullable=false)
    private String submittedInd;

    /** The modified by. */
    @Column(name = "SUBMITTED_BY", length = 50)
    private String submittedBy;

    /** The modified dt. */
    @Temporal(TemporalType.DATE)
    @Column(name="MODIFIED_DT")
    private Date modifiedDt;


	public long getTrueUpId() {
		return trueUpId;
	}

	public void setTrueUpId(long trueUpId) {
		this.trueUpId = trueUpId;
	}
    
	/**
	 * @return the fiscalYear
	 */
	public Long getFiscalYear() {
		return fiscalYear;
	}

	/**
	 * @param fiscalYear the fiscalYear to set
	 */
	public void setFiscalYear(Long fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	/**
	 * @return the submittedDt
	 */
	public Date getSubmittedDt() {
		return submittedDt;
	}

	/**
	 * @param submittedDt the submittedDt to set
	 */
	public void setSubmittedDt(Date submittedDt) {
		this.submittedDt = submittedDt;
	}

	/**
	 * @return the documents
	 */
	public Set<TrueUpDocumentEntity> getDocuments() {
		return documents;
	}

	/**
	 * @param documents the documents to set
	 */
	public void setDocuments(Set<TrueUpDocumentEntity> documents) {
		this.documents = documents;
	}

	/**
	 * @return the assessments
	 */
	public List<AssessmentEntity> getAssessments() {
		return assessments;
	}

	/**
	 * @param assessments the assessments to set
	 */
	public void setAssessments(List<AssessmentEntity> assessments) {
		this.assessments = assessments;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDt
	 */
	public Date getCreatedDt() {
		return createdDt;
	}

	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedDt
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}

	/**
	 * @param modifiedDt the modifiedDt to set
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	/**
	 * @return the submittedInd
	 */
	public String getSubmittedInd() {
		return submittedInd;
	}

	/**
	 * @param submittedInd the submittedInd to set
	 */
	public void setSubmittedInd(String submittedInd) {
		this.submittedInd = submittedInd;
	}

	/**
	 * @return the submittedBy
	 */
	public String getSubmittedBy() {
		return submittedBy;
	}

	/**
	 * @param submittedBy the submittedBy to set
	 */
	public void setSubmittedBy(String submittedBy) {
		this.submittedBy = submittedBy;
	}

}

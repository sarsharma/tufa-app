/**
 * 
 */
package gov.hhs.fda.ctp.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author tgunter
 *
 */
@Entity
@Table(name = "TU_ZERO_REPORTS_COMMENTS")
public class ZeroReportCommentEntity extends BaseEntity {
    /** The Constant serialVersionUID. */
    protected static final long serialVersionUID = 1L;
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ZERO_REPORTS_ID", unique = true, nullable = false)
    private Long Id;
    
    
    @Column(name = "PERMIT_ID", nullable=false) 
    private long permitId;
    
    /** The period id. */
   
    @Column(name = "PERIOD_ID", nullable=false)
    private long periodId;
  
    
    @Column(name="USER_COMMENT")
    private String userComment;
    
    @Column(name="COMMENT_AUTHOR")
    private String author;
    
    @Temporal(TemporalType.DATE) 
    @CreationTimestamp
    @Column(name="COMMENT_DATE")
    private Date commentDate;

    @Column(name="COMMENT_RESOLVED")
    private Boolean resolveComment;
    
    @Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@UpdateTimestamp
	@Column(name = "MODIFIED_DT")
	private Date modifiedDt;
    
    
	public Long getId() {
		return Id;
	}

	/**
	 */
	public void setId(Long Id) {
		this.Id = Id;
	}

    
    
    /**
     * Gets the permit id.
     *
     * @return the permit id
     */
    public long getPermitId() {
        return this.permitId;
    }

    /**
     * Sets the permit id.
     *
     * @param permit the new permit id
     */
    public void setPermitId(long permit) {
        this.permitId = permit;
    }
   
    /**
     * Gets the period id.
     *
     * @return the period id
     */
    public long getPeriodId() {
        return this.periodId;
    }

    /**
     * Sets the period id.
     *
     * @param period the new period id
     */
    public void setPeriodId(long period) {
        this.periodId = period;
    }
    
	/**
	 * @return the comment
	 */
	public String getUserComment() {
		return userComment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setUserComment(String comment) {
		this.userComment = comment;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the commentDate
	 */
	public Date getCommentDate() {
		return commentDate;
	}

	/**
	 * @param commentDate the commentDate to set
	 */
	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public Boolean getResolveComment() {
		return resolveComment;
	}

	public void setResolveComment(Boolean resolveComment) {
		this.resolveComment = resolveComment;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
    

}

/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.persistence.model.CountryEntity;
import gov.hhs.fda.ctp.persistence.model.ReportAddressEntity;
import gov.hhs.fda.ctp.persistence.*;

@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
public class TestAddressMgmtEntityManagerImpl {
	
	@Autowired
	AddressMgmtEntityManager entityManager;

	@Mock
	private Logger logger;

	@InjectMocks
	private AddressMgmtEntityManagerImpl addressMgmtEntityManagerImpl;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AddressMgmtEntityManagerImpl#saveOrUpdate(gov.hhs.fda.ctp.persistence.model.AddressEntity)}.
	 */
	@Test
	@Ignore
	public void testSaveOrUpdate() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AddressMgmtEntityManagerImpl#getCountryCd()}.
	 */
	@Test
	public void testGetCountryCd() throws Exception {
		List<String> countryCodes = entityManager.getCountryCd();
		assert (countryCodes != null);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AddressMgmtEntityManagerImpl#getCountryNm()}.
	 */
	@Test
	public void testGetCountryNm() throws Exception {
		List<String> countryNames = entityManager.getCountryNm();
		assert (countryNames != null);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AddressMgmtEntityManagerImpl#getCountryDialCd()}.
	 */
	@Test
	public void testGetCountryDialCd() throws Exception {
		List<String> countryDial = entityManager.getCountryDialCd();
		assert (countryDial != null);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AddressMgmtEntityManagerImpl#getCountries()}.
	 */
	@Test
	public void testGetCountries() throws Exception {
		Set<CountryEntity> countryNames = entityManager.getCountries();
		assert (countryNames != null);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AddressMgmtEntityManagerImpl#getCountryCd(java.lang.String)}.
	 
	@Test
	public void testGetCountryCdString() throws Exception {
		String countryCd = entityManager.getCountryCd("United Kingdom");
		assert (countryCd != null);
	}*/

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AddressMgmtEntityManagerImpl#getCountryNm(java.lang.String)}.
	 
	@Test
	public void testGetCountryNmString() throws Exception {
		String countryCd = entityManager.getCountryNm("TG");
		assert (countryCd != null);
	}*/

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AddressMgmtEntityManagerImpl#getStateCd()}.
	 */
	@Test
	public void testGetStateCd() throws Exception {
		List<String> stateCd = entityManager.getStateCd();
		assert (stateCd != null);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AddressMgmtEntityManagerImpl#getStateCd(java.lang.String)}.
	 
	@Test
	public void testGetStateCdString() throws Exception {
		String stateCd = entityManager.getStateCd("Maryland");
		assert (stateCd != null);
	}*/

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AddressMgmtEntityManagerImpl#getStateNm(java.lang.String)}.
	 */
	@Test
	@Ignore
	public void testGetStateNmString() throws Exception {
		String stateCd = entityManager.getStateCd("MD");
		assert (stateCd != null);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AddressMgmtEntityManagerImpl#getStateNm()}.
	 */
	@Test
	@Ignore
	public void testGetStateNm() throws Exception {
		List<String> stateCd = entityManager.getStateNm();
		assert (stateCd != null);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AddressMgmtEntityManagerImpl#getReportAddresses(int, int, int)}.
	 */
	@Test
	@Ignore
	public void testGetReportAddresses() throws Exception {
		List<ReportAddressEntity> mockAddresses = new ArrayList<ReportAddressEntity>();
		ReportAddressEntity rae = new ReportAddressEntity();
		rae.setCity("Bethesda"); 
		mockAddresses.add(rae);
		
		when(entityManager.getReportAddresses((Integer)notNull(),(Integer)notNull(),(Integer)notNull())
              ).thenReturn(mockAddresses);
		given(entityManager.getReportAddresses(1, 1, 1)).willReturn(mockAddresses);
        List<ReportAddressEntity> addresses = entityManager.getReportAddresses(1, 1, 1);
		assert (addresses != null);
	}

}

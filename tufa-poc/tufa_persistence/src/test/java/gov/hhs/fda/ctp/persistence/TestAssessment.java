/**
 * 
 */
package gov.hhs.fda.ctp.persistence;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.AssessmentFilter;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.persistence.model.AssessmentEntity;

/**
 * @author akari
 *
 */
@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
public class TestAssessment {

	@Autowired
	AssessmentEntityManager assessmentEntityManager;

	@Before
	public void setUp() {

	}

	@Test
	public void testGetAssessments() {
		List<AssessmentEntity> assessments = new ArrayList<>();
		AssessmentFilter filter = new AssessmentFilter();

		Set<Integer> quarters = new HashSet<>();
		quarters.add(1);
		quarters.add(2);
		filter.setQuarterFilters(quarters);

		Set<Integer> years = new HashSet<>();
		years.add(1801);
		years.add(2014);
		filter.setFiscalYrFilters(years);

		Set<String> status = new HashSet<>();
		status.add("N");
		status.add("Y");
		filter.setStatusFilters(status);

		assessments = assessmentEntityManager.getAssessments(filter);
		assert (assessments != null);
	}

	@Test
	public void testGetFisicalYears() {
		Set<String> years = new HashSet<>();
		years = assessmentEntityManager.getFiscalYears();
		assert (years != null);
	}

}

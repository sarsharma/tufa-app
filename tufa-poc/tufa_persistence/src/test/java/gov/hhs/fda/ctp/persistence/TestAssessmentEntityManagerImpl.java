/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;

/**
 * @author rabalasubramani3
 *
 */
@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
@Ignore
public class TestAssessmentEntityManagerImpl {
	@Mock
	private Logger logger;

	@Mock
	private SimpleSQLManager sqlManager;
	@InjectMocks
	private AssessmentEntityManagerImpl assessmentEntityManagerImpl;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#addAssessment(gov.hhs.fda.ctp.persistence.model.AssessmentEntity)}.
	 */
	@Test
	public void testAddAssessment() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#getAssessment(long)}.
	 */
	@Test
	public void testGetAssessment() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#updateAssessment(gov.hhs.fda.ctp.persistence.model.AssessmentEntity)}.
	 */
	@Test
	public void testUpdateAssessment() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#getAssessmentByYearByQtr(int, int)}.
	 */
	@Test
	public void testGetAssessmentByYearByQtr() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#doesExist(int, int)}.
	 */
	@Test
	public void testDoesExist() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#getAssessments(gov.hhs.fda.ctp.common.beans.AssessmentFilter)}.
	 */
	@Test
	public void testGetAssessments() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#getFiscalYears()}.
	 */
	@Test
	public void testGetFiscalYears() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#generateMarketShare(int, int)}.
	 */
	@Test
	public void testGenerateMarketShare() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#getCigarAssessment(gov.hhs.fda.ctp.common.beans.CigarAssessmentFilter, java.lang.String)}.
	 */
	@Test
	public void testGetCigarAssessment() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#getSupportDocs(long)}.
	 */
	@Test
	public void testGetSupportDocs() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#deleteByDocId(long)}.
	 */
	@Test
	public void testDeleteByDocId() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#saveSupportDocument(gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity)}.
	 */
	@Test
	public void testSaveSupportDocument() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AssessmentEntityManagerImpl#getDocByDocId(long)}.
	 */
	@Test
	public void testGetDocByDocId() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

}

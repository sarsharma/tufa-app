/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;

/**
 * @author rabalasubramani3
 *
 */
@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
@Ignore
public class TestAttachmentsEntityManagerImpl {
	@Mock
	private Logger logger;

	@InjectMocks
	private AttachmentsEntityManagerImpl attachmentsEntityManagerImpl;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AttachmentsEntityManagerImpl#findAll()}.
	 */
	@Test
	@Ignore
	public void testFindAll() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AttachmentsEntityManagerImpl#findByIds(long, long)}.
	 */
	@Test
	@Ignore
	public void testFindByIds() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AttachmentsEntityManagerImpl#deleteById(long)}.
	 */
	@Test
	@Ignore
	public void testDeleteById() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AttachmentsEntityManagerImpl#getAttachment(long)}.
	 */
	@Test
	@Ignore
	public void testGetAttachment() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.AttachmentsEntityManagerImpl#addAttach(gov.hhs.fda.ctp.persistence.model.AttachmentsEntity)}.
	 */
	@Test
	@Ignore
	public void testAddAttach() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

}

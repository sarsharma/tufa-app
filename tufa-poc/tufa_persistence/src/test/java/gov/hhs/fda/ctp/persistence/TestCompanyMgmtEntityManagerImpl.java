/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.persistence.model.AddressEntity;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.ContactEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;

/**
 * @author rabalasubramani3
 *
 */
@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
public class TestCompanyMgmtEntityManagerImpl {
	@Mock
	private Logger logger;

	@InjectMocks
	private CompanyMgmtEntityManagerImpl companyMgmtEntityManagerImpl;

    /*
     * company entity manager
     */
	@Autowired
    private CompanyMgmtEntityManager companyEntityManager;

    @Autowired
    public void setCompanyEntityManager(CompanyMgmtEntityManager companyEntityManager){
        this.companyEntityManager = companyEntityManager;
    }

    private CompanyEntity company;
    private CompanyEntity newCompany;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
        company = this.createMockCompany();
        newCompany = company;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.CompanyMgmtEntityManagerImpl#createCompany(gov.hhs.fda.ctp.persistence.model.CompanyEntity)}.
	 */
	@Test
	public void testCreateCompany() throws Exception {
		when(companyEntityManager.createCompany((CompanyEntity)notNull())
	              ).thenReturn(newCompany);
			given(companyEntityManager.createCompany(newCompany)).willReturn(company);
			company = this.companyEntityManager.createCompany(newCompany);
        Assert.assertEquals(company.getLegalName(), newCompany.getLegalName());
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.CompanyMgmtEntityManagerImpl#updateCompany(gov.hhs.fda.ctp.persistence.model.CompanyEntity)}.
	 
	@Test
	public void testUpdateCompany() throws Exception {
//        CompanyEntity company = this.createMockCompany();
//        CompanyEntity newCompany = this.companyEntityManager.createCompany(company);
        Assert.assertEquals(company.getLegalName(), newCompany.getLegalName());
        newCompany.setLegalName("XXXXXXXXXXXXXXX");
        newCompany = this.companyEntityManager.updateCompany(newCompany);
        Assert.assertEquals(newCompany.getLegalName(), "XXXXXXXXXXXXXXX");
	}
*/
	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.CompanyMgmtEntityManagerImpl#getCompanyById(long)}.
	 
	@Test
	public void testGetCompanyById() throws Exception {
//        CompanyEntity company = this.createMockCompany();
//        CompanyEntity newCompany = this.companyEntityManager.createCompany(company);
		when(companyEntityManager.getCompanyById((long)notNull())
	              ).thenReturn(company);
			given(companyEntityManager.getCompanyById(1L)).willReturn(company);
        newCompany = this.companyEntityManager.getCompanyById(1L);
        Assert.assertEquals(newCompany.getLegalName(), "ZZZZZZZZZZZZZZZ");
	}
*/
	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.CompanyMgmtEntityManagerImpl#getAllCompanies()}.
	 
	@Test
	public void testGetAllCompanies() throws Exception {
        List<CompanyEntity> companies = this.companyEntityManager.getAllCompanies();
        Assert.assertFalse(companies.isEmpty());
	}*/

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.CompanyMgmtEntityManagerImpl#getPermits(java.lang.String, long)}.
	 
	@Test
	public void testGetPermits() throws Exception {
//        CompanyEntity company = this.createMockCompany();
//        CompanyEntity newCompany = this.companyEntityManager.createCompany(company);
		PermitEntity permit = this.createMockPermit("timd123", newCompany);
		List<PermitEntity> permits = null;
		permits.add(permit);
		when(companyEntityManager.getPermits((String)notNull(),(long)notNull())
	              ).thenReturn(permits);
			given(companyEntityManager.getPermits("ACTV", 1L)).willReturn(permits);
        permits = this.companyEntityManager.getPermits("ACTV", 1L);
        assert (permits != null);
	}
    */
	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.CompanyMgmtEntityManagerImpl#getPermitById(long, java.lang.String)}.
	 
	@Test
	public void testGetPermitById() throws Exception {
//        CompanyEntity company = this.createMockCompany();
//        CompanyEntity newCompany = this.companyEntityManager.createCompany(company);
		PermitEntity permit = this.createMockPermit("timd123", newCompany);
		List<PermitEntity> permits = null;
		permits.add(permit);
		when(companyEntityManager.getPermits((String)notNull(),(long)notNull())
	              ).thenReturn(permits);
			given(companyEntityManager.getPermits("ACTV", 1L)).willReturn(permits);
        List<PermitEntity> permitst = this.companyEntityManager.getPermits("ACTV", 1L);
        Assert.assertNotNull(permitst);
        for( PermitEntity p : permitst ){
        	when(companyEntityManager.getPermitById((long)notNull(),(String)notNull())
  	              ).thenReturn(permit);
  			given(companyEntityManager.getPermitById(1L, "timd123")).willReturn(permit);
        	PermitEntity selPermit = this.companyEntityManager.getPermitById(newCompany.getCompanyId(), p.getPermitNum());
            Assert.assertNotNull(selPermit);
        }
	}
	*/

    /**
     * Create a mock CompanyEntity.
     * @return
     */
    private CompanyEntity createMockCompany() {
        CompanyEntity mockCompany = new CompanyEntity();
        mockCompany.setLegalName("ZZZZZZZZZZZZZZZ");
        mockCompany.setEIN("061234567");
        mockCompany.setCompanyStatus("ACTV");
        PermitEntity mockPermit = createMockPermit("ZZZZZZ1", mockCompany);
        Set<PermitEntity> permits = new HashSet<PermitEntity>();
        permits.add(mockPermit);
        mockCompany.setPermits(permits);
        /* mockCompany.setAddresses(this.createAddressSet()); */
        return mockCompany;
    }

    /**
     * Create a mock PermitEntity.
     * @param company
     * @return
     */
    private PermitEntity createMockPermit(String permitNum, CompanyEntity company) {
        Date now = new Date();
        PermitEntity mockPermit = new PermitEntity();
        mockPermit.setPermitNum(permitNum);
        mockPermit.setPermitTypeCd("MANU");
        mockPermit.setPermitStatusTypeCd("ACTV");
        mockPermit.setIssueDt(now);
        mockPermit.setCompanyEntity(company);
        /* mockPermit.setContacts(this.createContactList()); */
        return mockPermit;
    }

    @SuppressWarnings("unused")
	private Set<AddressEntity> createAddressSet() {
        Set<AddressEntity> addresses = new HashSet<AddressEntity>();
        AddressEntity primaryAddress = new AddressEntity();
        AddressEntity alternateAddress = new AddressEntity();
        primaryAddress.setStreetAddress("111 TEST PRIMARY CIRCLE");
        primaryAddress.setAddressTypeCd("PRIM");
        alternateAddress.setStreetAddress("222 TEST ALTERNATE WAY");
        alternateAddress.setAddressTypeCd("ALTN");
        addresses.add(primaryAddress);
        addresses.add(alternateAddress);
        return addresses;
    }

    @SuppressWarnings("unused")
	private List<ContactEntity> createContactList() {
        List<ContactEntity> contacts = new ArrayList<ContactEntity>();
        ContactEntity contact1 = new ContactEntity();
        ContactEntity contact2 = new ContactEntity();
        contact1.setFirstNm("TEST1");
        contact1.setLastNm("TESTER");
        contact2.setFirstNm("TEST2");
        contact2.setLastNm("TESTER");
        contacts.add(contact1);
        contacts.add(contact2);
        return contacts;
    }
}

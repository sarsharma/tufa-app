/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;

@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
public class TestContactMgmtEntityManagerImpl {
	@Autowired
	ContactMgmtEntityManager entityManager;
	
	@Mock
	private Logger logger;
	@InjectMocks
	private ContactMgmtEntityManagerImpl contactMgmtEntityManagerImpl;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ContactMgmtEntityManagerImpl#create(gov.hhs.fda.ctp.persistence.model.ContactEntity)}.
	 */
	@Test
	@Ignore
	public void testCreate() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ContactMgmtEntityManagerImpl#readById(long)}.
	 */
	@Test
	@Ignore
	public void testReadById() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ContactMgmtEntityManagerImpl#update(gov.hhs.fda.ctp.persistence.model.ContactEntity)}.
	 */
	@Test
	@Ignore
	public void testUpdate() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ContactMgmtEntityManagerImpl#deleteById(long)}.
	 */
	@Test
	@Ignore
	public void testDeleteById() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ContactMgmtEntityManagerImpl#getReportContacts(int, int, int)}.
	 */
	@Test
	@Ignore
	public void testGetReportContacts() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

}

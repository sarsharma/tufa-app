package gov.hhs.fda.ctp.persistence;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;

@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
public class TestHibernateConfig {

	@Autowired
	private PersistenceHibernateTestConfig hibernateConfig;

	@Test
	public void testSessionFactory(){
		Assert.assertNotNull(hibernateConfig);
		Assert.assertNotNull(hibernateConfig.sessionFactory());

	}
}

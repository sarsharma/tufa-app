/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;

/**
 * @author rabalasubramani3
 *
 */
@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
public class TestPermitEntityManagerImpl {

	@InjectMocks
	private CompanyMgmtEntityManagerImpl companyMgmtEntityManagerImpl;

    /*
     * company entity manager
     */
    private CompanyMgmtEntityManager companyEntityManager;

    @Autowired
    public void setCompanyEntityManager(CompanyMgmtEntityManager companyEntityManager){
        this.companyEntityManager = companyEntityManager;
    }

    /*
     * permit entity manager
     */
    private PermitEntityManager permitEntityManager;

    @Autowired
    public void setPermitEntityManager(PermitEntityManager permitEntityManager){
        this.permitEntityManager = permitEntityManager;
    }

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitEntityManagerImpl#createPermit(gov.hhs.fda.ctp.persistence.model.PermitEntity)}.
	 */
	@Test
	public void testCreatePermit() throws Exception {
        /* first, set up a company */
        CompanyEntity company = this.createMockCompany();
        /* set up a new permit for this company */
        PermitEntity permit = createMockPermit("ZZZZZZ2", company);
        /* create the permit */
        when(permitEntityManager.createPermit((PermitEntity)notNull())
                ).thenReturn(permit);
  		given(permitEntityManager.createPermit(permit)).willReturn(permit);
        PermitEntity newPermit = this.permitEntityManager.createPermit(permit);
        Assert.assertEquals(permit.getPermitNum(), newPermit.getPermitNum());
//        /* update the permit */
//        newPermit.setPermitNum("ZZZZZZ3");
//        newPermit = this.permitEntityManager.updatePermit(newPermit);
//        Assert.assertEquals(newPermit.getPermitNum(), "ZZZZZZ3");
//        /* get the permit */
//        newPermit = this.permitEntityManager.getPermit(newPermit.getPermitId());
//        Assert.assertEquals(newPermit.getPermitNum(), "ZZZZZZ3");
	}

    @Test
    public void testPermitObjectCore() {
        /* first, set up a company */
        CompanyEntity company = this.createMockCompany();
        /* create a new permit for this company, update it, then get it */
        PermitEntity permit = createMockPermit("ZZZZZZ2", company);
        when(permitEntityManager.createPermit((PermitEntity)notNull())
                ).thenReturn(permit);
  		given(permitEntityManager.createPermit(permit)).willReturn(permit);
        PermitEntity newPermit = this.permitEntityManager.createPermit(permit);
        Assert.assertEquals(permit.getPermitNum(), newPermit.getPermitNum());
        newPermit.setPermitNum("ZZZZZZ3");
        when(permitEntityManager.updatePermit((PermitEntity)notNull())
                ).thenReturn(newPermit);
  		given(permitEntityManager.createPermit(newPermit)).willReturn(newPermit);
        newPermit = this.permitEntityManager.updatePermit(newPermit);
        Assert.assertEquals(newPermit.getPermitNum(), "ZZZZZZ3");
    }


	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitEntityManagerImpl#updatePermit(gov.hhs.fda.ctp.persistence.model.PermitEntity)}.
	 */
	@Test
	public void testUpdatePermit() throws Exception {
        /* first, set up a company */
        CompanyEntity company = this.createMockCompany();
        /* set up a new permit for this company */
        PermitEntity permit = createMockPermit("ZZZZZZ2", company);
        /* create the permit */
        when(permitEntityManager.createPermit((PermitEntity)notNull())
                ).thenReturn(permit);
  		given(permitEntityManager.createPermit(permit)).willReturn(permit);
        PermitEntity newPermit = this.permitEntityManager.createPermit(permit);
        Assert.assertEquals(permit.getPermitNum(), newPermit.getPermitNum());
        /* update the permit */
        newPermit.setPermitNum("ZZZZZZ3");
        when(permitEntityManager.updatePermit((PermitEntity)notNull())
                ).thenReturn(newPermit);
  		given(permitEntityManager.createPermit(newPermit)).willReturn(newPermit);
        newPermit = this.permitEntityManager.updatePermit(newPermit);
        Assert.assertEquals(newPermit.getPermitNum(), "ZZZZZZ3");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitEntityManagerImpl#getPermitHistories(gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria)}.
	 */
	@Test
	@Ignore
	public void testGetPermitHistories() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitEntityManagerImpl#saveOrUpdatePermitContacts(gov.hhs.fda.ctp.persistence.model.PermitEntity)}.
	 */
	@Test
	@Ignore
	public void testSaveOrUpdatePermitContacts() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitEntityManagerImpl#deleteMonthlyReport(long, long, long)}.
	 */
	@Test
	@Ignore
	public void testDeleteMonthlyReport() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitEntityManagerImpl#deleteReportContacts(long, long, long)}.
	 */
	@Test
	@Ignore
	public void testDeleteReportContacts() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitEntityManagerImpl#deleteReportAddresses(long, long, long)}.
	 */
	@Test
	@Ignore
	public void testDeleteReportAddresses() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitEntityManagerImpl#deleteFormCommentsById(long)}.
	 */
	@Test
	@Ignore
	public void testDeleteFormCommentsById() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitEntityManagerImpl#getPermitPeriod(long, long)}.
	 */
	@Test
	@Ignore
	public void testGetPermitPeriod() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

    /**
     * Create a mock CompanyEntity.
     * @return
     */
    private CompanyEntity createMockCompany() {
        CompanyEntity mockCompany = new CompanyEntity();
        mockCompany.setLegalName("ZZZZZZZZZZZZZZZ");
        mockCompany.setEIN("061234567");
        mockCompany.setCompanyStatus("ACTV");
        PermitEntity mockPermit = createMockPermit("ZZZZZZ1", mockCompany);
        Set<PermitEntity> permits = new HashSet<PermitEntity>();
        permits.add(mockPermit);
        mockCompany.setPermits(permits);
        /* mockCompany.setAddresses(this.createAddressSet()); */
        return mockCompany;
    }

    /**
     * Create a mock PermitEntity.
     * @param company
     * @return
     */
    private PermitEntity createMockPermit(String permitNum, CompanyEntity company) {
        Date now = new Date();
        PermitEntity mockPermit = new PermitEntity();
        mockPermit.setPermitNum(permitNum);
        mockPermit.setPermitTypeCd("MANU");
        mockPermit.setPermitStatusTypeCd("ACTV");
        mockPermit.setIssueDt(now);
        mockPermit.setCompanyEntity(company);
        /* mockPermit.setContacts(this.createContactList()); */
        return mockPermit;
    }
}

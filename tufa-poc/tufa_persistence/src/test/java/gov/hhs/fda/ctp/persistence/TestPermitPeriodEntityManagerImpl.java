/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;

/**
 * @author rabalasubramani3
 *
 */
@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
@Ignore
public class TestPermitPeriodEntityManagerImpl {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#updatePermitPeriod(gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity, java.util.List)}.
	 */
	@Test
	public void testUpdatePermitPeriod() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#getPermitPeriodByIds(long, long)}.
	 */
	@Test
	public void testGetPermitPeriodByIds() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#getPermitPeriodByPK(gov.hhs.fda.ctp.persistence.model.ActivityPK)}.
	 */
	@Test
	public void testGetPermitPeriodByPK() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#getRptPeriodById(long)}.
	 */
	@Test
	public void testGetRptPeriodById() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#deleteByIds(long, long)}.
	 */
	@Test
	public void testDeleteByIds() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#saveOrUpdatePermitPeriod(gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity)}.
	 */
	@Test
	public void testSaveOrUpdatePermitPeriod() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#getFormDetailByPK(gov.hhs.fda.ctp.persistence.model.FormDetailPK)}.
	 */
	@Test
	public void testGetFormDetailByPK() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#deleteFormDetails(java.util.Set)}.
	 */
	@Test
	public void testDeleteFormDetails() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#deleteFormCommentsById(long)}.
	 */
	@Test
	public void testDeleteFormCommentsById() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#getOrCreateSubmittedForm(long, long, long, java.lang.String)}.
	 */
	@Test
	public void testGetOrCreateSubmittedForm() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#createOrUpdateFormComment(gov.hhs.fda.ctp.persistence.model.FormCommentEntity)}.
	 */
	@Test
	public void testCreateFormComment() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#getFormComments(long)}.
	 */
	@Test
	public void testGetFormComments() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#createRptPeriod(java.lang.String)}.
	 */
	@Test
	public void testCreateRptPeriod() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#deleteAttachments(long, long)}.
	 */
	@Test
	public void testDeleteAttachments() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#updatePeriodReconStatus(long, long)}.
	 */
	@Test
	public void testUpdatePeriodReconStatus() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#getPeriodReconStatus(long, long)}.
	 */
	@Test
	public void testGetPeriodReconStatus() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#getMissingForms(long, long)}.
	 */
	@Test
	public void testGetMissingForms() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#saveMissingForms(gov.hhs.fda.ctp.persistence.model.MissingForms)}.
	 */
	@Test
	public void testSaveMissingForms() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.PermitPeriodEntityManagerImpl#deleteMissingForms(gov.hhs.fda.ctp.persistence.model.MissingForms)}.
	 */
	@Test
	public void testDeleteMissingForms() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

}

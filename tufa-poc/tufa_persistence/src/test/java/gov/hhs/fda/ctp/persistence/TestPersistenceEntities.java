package gov.hhs.fda.ctp.persistence;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import gov.hhs.fda.ctp.persistence.model.AddressEntity;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.ContactEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;
import gov.hhs.fda.ctp.persistence.model.RoleEntity;
import gov.hhs.fda.ctp.persistence.model.UserEntity;

@ActiveProfiles("cmpmgmtpersistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CompanyEntity.class,PermitEntity.class,UserEntity.class,RoleEntity.class})
public class TestPersistenceEntities {

	@Test
	public void testCompanyEntityEqalsHashCode(){

		 CompanyEntity companyEntity1= new CompanyEntity();
		 companyEntity1.setEIN("061234567");

		 CompanyEntity companyEntity2= new CompanyEntity();
		 companyEntity2.setEIN("061234567");

		 Assert.assertTrue(companyEntity1.equals(companyEntity2) && companyEntity2.equals(companyEntity1));

		 Assert.assertTrue(companyEntity1.hashCode() == companyEntity2.hashCode());
	}


	@Test
	public void testPermitEntityEqalsHashCode(){

		 PermitEntity permitEntity1= new PermitEntity();
		 permitEntity1.setPermitNum("permitnum100");

		 PermitEntity permitEntity2= new PermitEntity();
		 permitEntity2.setPermitNum("permitnum100");

		 Assert.assertTrue(permitEntity1.equals(permitEntity2) && permitEntity2.equals(permitEntity1));

		 Assert.assertTrue(permitEntity1.hashCode() == permitEntity2.hashCode());
	}


	@Test
	public void testUserEntityEqalsHashCode(){

		 UserEntity userEntity1= new UserEntity();
		 userEntity1.setEmail("sarangsharma@email.com");

		 UserEntity userEntity2= new UserEntity();
		 userEntity2.setEmail("sarangsharma@email.com");

		 Assert.assertTrue(userEntity1.equals(userEntity2) && userEntity2.equals(userEntity1));

		 Assert.assertTrue(userEntity1.hashCode() == userEntity2.hashCode());
	}

	@Test
	public void testRoleEntityEqalsHashCode(){

		 RoleEntity roleEntity1= new RoleEntity();
		 roleEntity1.setName("Admin1");

		 RoleEntity roleEntity2= new RoleEntity();
		 roleEntity2.setName("Admin1");

		 Assert.assertTrue(roleEntity1.equals(roleEntity2) && roleEntity2.equals(roleEntity1));

		 Assert.assertTrue(roleEntity1.hashCode() == roleEntity2.hashCode());
	}

//	@Test
//	public void testResourceEntityEqalsHashCode(){
//
//		 ResourceEntity resourceEntity1= new ResourceEntity();
//		 resourceEntity1.setName("/companies/5/permits");
//
//		 ResourceEntity resourceEntity2= new ResourceEntity();
//		 resourceEntity2.setName("/companies/5/permits");
//
//		 Assert.assertTrue(resourceEntity1.equals(resourceEntity2) && resourceEntity2.equals(resourceEntity1));
//
//		 Assert.assertTrue(resourceEntity1.hashCode() == resourceEntity2.hashCode());
//	}

	@Test
	public void testAddressEntity() {
	    AddressEntity address1 = new AddressEntity();
	    address1.setAddressTypeCd("primary");
	    AddressEntity address2 = new AddressEntity();
	    address2.setAddressTypeCd("primary");

	    Assert.assertTrue(address1.equals(address2) && address2.equals(address1));

	    Assert.assertTrue(address1.hashCode() == address2.hashCode());
	}

	@Test
	public void testContactEntity() {
	    ContactEntity contact1 = new ContactEntity();
	    ContactEntity contact2 = new ContactEntity();
	    contact1.setContactId(new Long(1));
	    contact2.setContactId(new Long(1));

	    Assert.assertTrue(contact1.equals(contact2) && contact2.equals(contact1));

	    Assert.assertTrue(contact1.hashCode() == contact2.hashCode());
	}

}

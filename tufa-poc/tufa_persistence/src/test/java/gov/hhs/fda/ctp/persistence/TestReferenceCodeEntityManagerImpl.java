/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;

/**
 * @author rabalasubramani3
 *
 */
@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
@Ignore
public class TestReferenceCodeEntityManagerImpl {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#getValue(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testGetValue() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#getCode(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testGetCode() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#getValues(java.lang.String)}.
	 */
	@Test
	public void testGetValues() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#getKeys(java.lang.String)}.
	 */
	@Test
	public void testGetKeys() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#getTobaccoClasses(long)}.
	 */
	@Test
	public void testGetTobaccoClasses() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#getLatestClasses()}.
	 */
	@Test
	public void testGetLatestClasses() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#getAllTobaccoClasses()}.
	 */
	@Test
	public void testGetAllTobaccoClasses() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#saveTobaccoClasses(java.util.List)}.
	 */
	@Test
	public void testSaveTobaccoClasses() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#getTobaccoParentMap(long)}.
	 */
	@Test
	public void testGetTobaccoParentMap() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#getHtsCodes()}.
	 */
	@Test
	public void testGetHtsCodes() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#saveOrEditCodes(gov.hhs.fda.ctp.persistence.model.HTSCodeEntity)}.
	 */
	@Test
	public void testSaveOrEditCodes() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#getCodeById(java.lang.String)}.
	 */
	@Test
	public void testGetCodeById() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManagerImpl#getTobaccoClassMapForFiscalYear(long)}.
	 */
	@Test
	public void testGetTobaccoClassMapForFiscalYear() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

}

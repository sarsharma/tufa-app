/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import java.util.Map;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManager;

/**
 * @author tgunter
 *
 */
@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
public class TestReferenceValues {

    private ReferenceCodeEntityManager referenceCodeEntityManager;

    @Autowired
    public void setReferenceCodeEntityManager(ReferenceCodeEntityManager referenceCodeEntityManager){
        this.referenceCodeEntityManager = referenceCodeEntityManager;
    }
    /**
     * call both methods in reference value entity manager.
     */
    @Test
    @Ignore
    public void testReferenceLookups() {
        String manufacturer = referenceCodeEntityManager.getValue("PERMIT_TYPE", "MANU");
        assert (manufacturer == "Manufacturer");
        Map<String, String> permitTypes = referenceCodeEntityManager.getValues("PERMIT_TYPE");
        manufacturer = permitTypes.get("MANU");
        assert(manufacturer == "Manufacturer");
    }
}
/**
 * 
 */
package gov.hhs.fda.ctp.persistence;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;

/**
 * @author tgunter
 *
 */

@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
public class TestSimpleSQL {
	private SimpleSQLManager simpleSQLManager;

	@Autowired
	public void setSimpleSQLManager(SimpleSQLManager simpleSQLManager) {
		this.simpleSQLManager = simpleSQLManager;
	}

	@Test
	public void testSimpleSQL() {
		PermitHistoryCriteria criteria = new PermitHistoryCriteria();
		Set<String> permitTypeFilters = new HashSet<String>();
		Set<String> reportStatusFilters = new HashSet<String>();
		Set<String> permitStatusFilters = new HashSet<String>();
		Set<String> monthFilters = new HashSet<String>();
		permitTypeFilters.add("MANU");
		reportStatusFilters.add("NSTD");
		permitStatusFilters.add("ACTV");
		monthFilters.add("OCT");
		criteria.setPermitNameFilter("");
		criteria.setMonthFilters(monthFilters);
		criteria.setPermitStatusFilters(permitStatusFilters);
		criteria.setPermitTypeFilters(permitTypeFilters);
		criteria.setReportStatusFilters(reportStatusFilters);
		criteria = simpleSQLManager.getAllPermitHistory(criteria);
	}

	@Test
	public void testGetReportStatusData() {
		simpleSQLManager.getReportStatusData(2017, 2);
	}

}

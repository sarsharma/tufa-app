/**
 *
 */
package gov.hhs.fda.ctp.persistence;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.query.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.AssessmentReportStatus;
import gov.hhs.fda.ctp.common.beans.CBPImporter;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentRptDetail;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.CompanyPermitHistory;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.common.beans.QtReconRptDetail;
import gov.hhs.fda.ctp.common.beans.ReconExportDetail;
import gov.hhs.fda.ctp.common.beans.SelectTobaccoType;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonSummary;
import gov.hhs.fda.ctp.common.beans.UnknownCompanyBucket;
import gov.hhs.fda.ctp.common.pagination.PaginationAttributes;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.PeriodStatusEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;
import gov.hhs.fda.ctp.persistence.model.TTBPermitBucket;
import gov.hhs.fda.ctp.persistence.model.TrueUpSubDocumentEntity;

/**
 * @author rabalasubramani3
 *
 */
@ActiveProfiles("persistencetest")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceHibernateTestConfig.class)
@Transactional
public class TestSimpleSQLManagerImpl {
	@Mock
	private Logger logger;

	@Mock
	private ReferenceCodeEntityManager referenceCodeEntityManager;

	@Mock
	private SessionFactory hibernateSessionFactory;

	@Mock
	private PaginationAttributes pageAttributes;

	@InjectMocks
	private SimpleSQLManagerImpl simpleSQLManagerImpl = new SimpleSQLManagerImpl();

	@Autowired
	private SimpleSQLManager sqlManager;

	@Autowired
	PermitEntityManager permitManager;

	@Autowired
	PermitPeriodEntityManager permitPeriodManager;

	@Autowired
	CompanyMgmtEntityManager companyEntityManager;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getPermitHistory(long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetPermitHistory() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getPaginatedPermitHistory(long, int, int)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetPaginatedPermitHistory() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getPermitHistoryCount(long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetPermitHistoryCount() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getCompanyPermitHistory(long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetCompanyPermitHistory() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getAllPermitHistory(gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetAllPermitHistory() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getAllCompanies(gov.hhs.fda.ctp.common.beans.CompanySearchCriteria)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetAllCompanies() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getAvailablePermitReportFiscalYears()}
	 * .
	 */
	@Test
	@Ignore
	public void testGetAvailablePermitReportFiscalYears() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getAvailablePermitReportCalendarYears()}
	 * .
	 */
	@Test
	@Ignore
	public void testGetAvailablePermitReportCalendarYears() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getReportStatusData(int, int)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetReportStatusData() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#findByIds(long, long)}
	 * .
	 */
	@Test
	@Ignore
	public void testFindByIds() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#findByAssessmentId(long)}
	 * .
	 */
	@Test
	@Ignore
	public void testFindByAssessmentId() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#findDocsByFiscalYr(long)}
	 * .
	 */
	@Test
	@Ignore
	public void testFindDocsByFiscalYr() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getReconReportData(long, long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetReconReportData() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getReconReportDataForFiscalYear(long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetReconReportDataForFiscalYear() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getReconExportData(long, long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetReconExportData() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getReconExportDataForFiscalYear(long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetReconExportDataForFiscalYear() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getCompanyComparisonBucket(java.lang.Long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetCompanyComparisonBucket() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getCigarReconExportData(long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetCigarReconExportData() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getCigarAssessmentReportStatusData(long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetCigarAssessmentReportStatusData() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getComparisonResults(long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetComparisonResults() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getCigarComparisonSummary(long, long, java.lang.String)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetCigarComparisonSummary() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getNonCigarImporterComparisonSummary(long, long, long, java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetNonCigarImporterComparisonSummary() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getNonCigarManufacturerComparisonSummary(long, long, long, java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetNonCigarManufacturerComparisonSummary() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getImporterComparisonDetails(long, long, long, java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetImporterComparisonDetails() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getCigarManufacturerComparisonDetails(long, long, long, java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetCigarManufacturerComparisonDetails() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getNonCigarManufacturerComparisonDetails(long, long, long, java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetNonCigarManufacturerComparisonDetails() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getUnknownCompaniesBucket(java.lang.Long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetUnknownCompaniesBucket() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getTTBPermits(long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetTTBPermits() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getCompaniesForIncludeBucket(java.lang.Long)}
	 * .
	 */
	@Test
	@Ignore
	public void testGetCompaniesForIncludeBucket() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for
	 * {@link gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl#getPermitHistory(long)}
	 * .
	 */
	@Test
	@Transactional
	@Rollback(true)
	public void testSQLManager() throws Exception {
		// CompanyEntity company =
		// this.companyEntityManager.createCompany(this.createMockCompany());
		// List<CompanyPermitHistory> histories =
		// this.getMockCompanyPermitHistory();
		this.permitManager.saveOrUpdatePermitContacts(this.getMockPermit());
		this.permitPeriodManager.createRptPeriod("10-1999");
		PeriodStatusEntity periodSts = this.permitPeriodManager.updatePeriodReconStatus(1, 1);
		List<CompanyPermitHistory> permitHistory = this.sqlManager.getPermitHistory(1L);
		PermitHistoryCriteria permitCriteria = this.sqlManager.getAllPermitHistory(this.mockCriteria());
		// List<CompanyPermitHistory> paginatedPermit =
		// this.sqlManager.getPaginatedPermitHistory(1L, 1, 1);
		List<CompanyPermitHistory> companyHistory = this.sqlManager.getCompanyPermitHistory(1L);
		Set<String> fiscalyrs = this.sqlManager.getAvailablePermitReportFiscalYears();
		Set<String> calYrs = this.sqlManager.getAvailablePermitReportCalendarYears();
		CompanySearchCriteria compCriteria = this.sqlManager.getAllCompanies(this.mockCompanyCriteria());
		Map<Long, AssessmentReportStatus> stsData = this.sqlManager.getReportStatusData(1999, 2);
		List<AttachmentsEntity> documents = this.sqlManager.findByIds(1L, 1L);
		List<SupportingDocument> assessDocs = this.sqlManager.findSupportingDocumentsByAssessmentId(1L);
		List<TrueUpSubDocumentEntity> trueDocs = this.sqlManager.findDocsByFiscalYr(1999);
		List<QtReconRptDetail> reconData = this.sqlManager.getReconReportData(1999, 2);
		List<QtReconRptDetail> reconfiscalData = this.sqlManager.getReconReportDataForFiscalYear(1999);
		List<ReconExportDetail> reconExpData = this.sqlManager.getReconExportData(1999, 2);
		List<ReconExportDetail> reconExpfiscalData = this.sqlManager.getReconExportDataForFiscalYear(1999);
		List<CBPImporter> compBucket = this.sqlManager.getCompanyComparisonBucket(1999L);
		List<ReconExportDetail> cigreconData = this.sqlManager.getCigarReconExportData(1999);
		List<CigarAssessmentRptDetail> cigreportstsdata = this.sqlManager.getCigarAssessmentReportStatusData(1999L);
		TrueUpCompareSearchCriteria csc = new TrueUpCompareSearchCriteria();
		List<TrueUpComparisonResults> trueUpResult = this.sqlManager.getComparisonResults(1999, csc);
		List<TrueUpComparisonResults> allTrueUpResult = this.sqlManager.getAllComparisonResults(1999, csc);
		List<TrueUpComparisonSummary> cigSumm = this.sqlManager.getCigarComparisonSummary(1L, 1999, "MANU");
//		List<TrueUpComparisonSummary> noncigimp = this.sqlManager.getNonCigarImporterComparisonSummary(1L, 1999, 2,
//				"IMPT", "Cigarette");
		List<TrueUpComparisonManufacturerDetail> noncigmandetails = this.sqlManager
				.getNonCigarManufacturerComparisonDetails(1L, 1999, 2, "MANU", "CHEW-SNUFF");
		List<TrueUpComparisonSummary> noncigmancompsumm = this.sqlManager.getNonCigarManufacturerComparisonSummary(1L,
				1999, 2, "MANU", "CIGARETTES");
		List<TrueUpComparisonImporterDetail> impcompdetails = this.sqlManager.getImporterComparisonDetails("123456789", 1999, 2,
				"IMPT", "Cigarette");
		List<TrueUpComparisonManufacturerDetail> cigmandetails = this.sqlManager
				.getCigarManufacturerComparisonDetails(1L, 1999, 2, "MANU", "Cigar");
		List<UnknownCompanyBucket> unknowCompanies = this.sqlManager.getUnknownCompaniesBucket(1999L);
		List<TTBPermitBucket> ttbpermits = this.sqlManager.getTTBPermits(1999L);
		List<CompanyIngestion> companiesInclusion = this.sqlManager.getCompaniesForIncludeBucket(1999L);
		assert (permitHistory != null && permitCriteria != null && companyHistory != null && compCriteria != null
				&& fiscalyrs != null && calYrs != null && stsData != null && documents != null && assessDocs != null
				&& trueDocs != null && reconData != null && reconfiscalData != null && reconExpData != null
				&& reconExpfiscalData != null && compBucket != null && cigreconData != null && cigreportstsdata != null
				&& trueUpResult != null && cigSumm != null 
//				&& noncigimp != null 
				&& noncigmandetails != null
				&& noncigmancompsumm != null && impcompdetails != null && cigmandetails != null
				&& unknowCompanies != null && ttbpermits != null && companiesInclusion != null
				&& allTrueUpResult != null);
	}

	/*
	 * getAllComparisonResults TESTS
	 * --------------------------------------
	 */

	@Test
	public void testGetAllComparisonResultsNull() {

		Session session = Mockito.mock(Session.class);
		NativeQuery query = Mockito.mock(NativeQuery.class);

		Mockito.when(hibernateSessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createNativeQuery(Mockito.anyString())).thenReturn(query);

		Mockito.when(query.list()).thenReturn(this.getMockAllDeltaResultsNull());

		List<TrueUpComparisonResults> allComparisonResults = this.simpleSQLManagerImpl.getAllComparisonResults(1999L,
				this.mockTrueUpCompareSearchCriteria());
		assertNotNull(allComparisonResults);

		for (TrueUpComparisonResults result : allComparisonResults) {
			assertNull(result.getLegalName());
			assertNull(result.getTobaccoClass());
			assertNull(result.getQuarter());
			assertNull(result.getTotalDeltaTax());
			assertNull(result.getPermitType());
			assertNull(result.getDataType());
			assertNull(result.getEin());
			assertNull(result.getOriginalName());
			assertNull(result.getStatus());
			assertNull(result.getOriginalEIN());
			assertNull(result.getDeltaComment());
			assert (result.getDataType().equals("Matched"));
		}
	}

	@Test
	public void testGetAllComparisonResultsWithSearchCriteria() {

		Session session = Mockito.mock(Session.class);
		NativeQuery query = Mockito.mock(NativeQuery.class);

		Mockito.when(hibernateSessionFactory.getCurrentSession()).thenReturn(session);
		Mockito.when(session.createSQLQuery(Mockito.anyString())).thenReturn(query);

		List<Object[]> deltas = this.getMockAllDeltaResults();
		Mockito.when(query.list()).thenReturn(deltas);

		List<TrueUpComparisonResults> allComparisonResults = this.simpleSQLManagerImpl.getAllComparisonResults(1999L,
				this.mockTrueUpCompareSearchCriteriaWithValues());

		assertNotNull(allComparisonResults);
		assert (allComparisonResults.size() == 2);

		TrueUpComparisonResults result = allComparisonResults.get(0);
		assert (result.getLegalName().equals("COMPANY NAME"));
		assert (result.getTobaccoClass().equals("TOBACCO CLASS"));
		assert (result.getQuarter().equals("QUARTER"));
		assert (result.getTotalDeltaTax() == (new BigDecimal(1)).doubleValue());
		assert (result.getPermitType().equals("Manufacturer"));
		assert (result.getDataType().equals("DATA TYPE"));
		assert (result.getEin().equals("EIN "));
		assert (result.getOriginalName().equals("ORIGINAL NAME"));
		assert (result.getStatus().equals("STATUS"));
		assert (result.getOriginalEIN().equals("ORIGINAL EIN"));
		assert (result.getDeltaComment().equals("COMMENT"));
		assert (result.getDataType().equals("Matched"));

		result = allComparisonResults.get(1);
		assert (result.getLegalName().equals("COMPANY NAME"));
		assert (result.getTobaccoClass().equals("TOBACCO CLASS"));
		assert (result.getQuarter().equals("QUARTER"));
		assert (result.getTotalDeltaTax() == (new BigDecimal(1)).doubleValue());
		assert (result.getPermitType().equals("Importer"));
		assert (result.getDataType().equals("DATA TYPE"));
		assert (result.getEin().equals("EIN "));
		assert (result.getOriginalName().equals("ORIGINAL NAME"));
		assert (result.getStatus().equals("STATUS"));
		assert (result.getOriginalEIN().equals("ORIGINAL EIN"));
		assert (result.getDeltaComment().equals("COMMENT"));
		assert (result.getDataType().equals("Matched"));
	}

	/*
	 * Mock Data Methods
	 * --------------------------------------
	 */

	List<CompanyPermitHistory> getMockCompanyPermitHistory() {
		List<CompanyPermitHistory> histories = new ArrayList<>();
		CompanyPermitHistory company = new CompanyPermitHistory();
		company.setCompanyId(12345L);
		company.setCompanyName("TESTLEGAL");
		histories.add(company);
		return histories;
	}

	PermitPeriodEntity getMockPermitPeriod() {
		PermitPeriodEntity ppe = new PermitPeriodEntity();
		ppe.setPeriodId(1L);
		ppe.setPermitId(1L);
		return ppe;
	}

	/**
	 * Create a well-formed mock company.
	 * 
	 * @return
	 */
	CompanyEntity createMockCompany() {
		CompanyEntity mockCompany = new CompanyEntity();
		mockCompany.setCompanyId(12345L);
		mockCompany.setLegalName("TESTLEGAL");
		mockCompany.setEIN("061234567");
		return mockCompany;
	}

	PermitEntity getMockPermit() {
		PermitEntity mockPermit = new PermitEntity();
		mockPermit.setPermitNum("12345678901");
		mockPermit.setPermitTypeCd("TBD");
		mockPermit.setCompanyEntity(this.createMockCompany());
		mockPermit.setPermitStatusTypeCd("TBD");
		mockPermit.setPermitId(1L);
		return mockPermit;
	}

	PermitHistoryCriteria mockCriteria() {
		PermitHistoryCriteria criteria = new PermitHistoryCriteria();
		return criteria;
	}

	CompanySearchCriteria mockCompanyCriteria() {
		CompanySearchCriteria search = new CompanySearchCriteria();
		return search;
	}

	TrueUpCompareSearchCriteria mockTrueUpCompareSearchCriteria() {
		TrueUpCompareSearchCriteria search = new TrueUpCompareSearchCriteria();

		SelectTobaccoType stt = new SelectTobaccoType();
		search.setSelTobaccoTypes(stt);
		return search;
	}

	TrueUpCompareSearchCriteria mockTrueUpCompareSearchCriteriaWithValues() {
		TrueUpCompareSearchCriteria search = new TrueUpCompareSearchCriteria();
		search.setCompanyFilter("TEST");
		search.setDataTypeFilter("INGESTED");
		search.setDeltaTaxFilter("12345.54");
		search.setEinFilter("123456789");
		search.setOriginalNameFilter("TEST ORIGINAL");
		search.setPermitTypeFilter("IMPT");
		search.setQuarterFilter("1");
		//search.setAllDeltaStatusTypeFilter() = null;
		search.setTobaccoClassFilter("CIGARS");

		SelectTobaccoType stt = new SelectTobaccoType();
		stt.setShowCigars(true);
		stt.setShowChews(true);
		stt.setShowCigarettes(true);
		stt.setShowPipes(true);
		stt.setShowRollYourOwn(true);
		stt.setShowSnuffs(true);
		search.setSelTobaccoTypes(stt);

		return search;
	}

	List<Object[]> getMockAllDeltaResults() {
		List<Object[]> allDeltaResults = new ArrayList<Object[]>();

		Object[] delta = new Object[12];
		delta[0] = "COMPANY NAME";
		delta[1] = "TOBACCO CLASS";
		delta[2] = "QUARTER";
		delta[3] = new BigDecimal(1);
		delta[4] = "MANU";
		delta[5] = "DATA TYPE";
		delta[6] = "EIN";
		delta[7] = "ORIGINAL NAME";
		delta[8] = "STATUS";
		delta[9] = "ORIGINAL EIN";
		delta[10] = "COMMENT";
		delta[11] = new BigDecimal(1);

		allDeltaResults.add(delta);

		delta[0] = "COMPANY NAME";
		delta[1] = "TOBACCO CLASS";
		delta[2] = "QUARTER";
		delta[3] = new BigDecimal(1);
		delta[4] = "IMPT";
		delta[5] = "DATA TYPE";
		delta[6] = "EIN";
		delta[7] = "ORIGINAL NAME";
		delta[8] = "STATUS";
		delta[9] = "ORIGINAL EIN";
		delta[10] = "COMMENT";
		delta[11] = new BigDecimal(1);

		allDeltaResults.add(delta);

		return allDeltaResults;
	}

	List<Object[]> getMockAllDeltaResultsNull() {
		List<Object[]> allDeltaResults = new ArrayList<Object[]>();

		Object[] delta = new Object[12];

		allDeltaResults.add(delta);

		return allDeltaResults;
	}

}

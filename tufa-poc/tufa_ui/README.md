# SingCli

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run protractor` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

The npm command may be modified to target specific test suites/modules, as well as targetting different environments subject to test. Examples below:

- Below command will run automation tests against TUFA MVP15 relative URL, and target only the scenarios related to create-company test suite.
`npm run protractor --url=/tufaspringmvp15 -- --suite=create-company`
     
- If no url specified, the command will run automation tests against default UAT environment (http://tufa.test.fda.gov/tufanext/), and target only the scenarios related to create-company test suite.
`npm run protractor -- --suite=create-company`

Below are the currently available suite parameters for execution:
- `--suite=create-company` will execute E2E Create Company test suite.
- `--suite=create-permits` will execute E2E Create Permits test suite.
- `--suite=create-monthly-report` will execute E2E Create Monthly Report test suite.
- `--suite=quarterly-assessments` will execute E2E Quarterly Assessments test suite.
- `--suite=cigar-assessments` will execute E2E Cigar Assessments test suite.
- `--suite=annual-trueup-assessments` will execute E2E Annual True-up Assessments test suite.
- `--suite=daily-operations` will execute E2E Daily Operations test suite.
- `--suite=permit-history` will execute E2E Permit History test suite.
- `--suite=company-details` will execute E2E Company Details test suite.
- `--suite=importer` will execute E2E Importer test suite.
- `--suite=manufacturer` will execute E2E Manufacturer test suite.
- `--suite=axe` will execute 508 compliance accessibility testing, ranging from Login page to Company Detail page.

Results after each run will be available in /tufa_ui/E2E-Test-Reports/report.html file. Results will be overwritten after each run so it's recommended to save 'E2E-Test-Reports' directory compressed as zip for any reports meant for keeping.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Setting up Protractor

Run `npm install`  
Navigate to `\node_modules\.bin`  
Run `webdriver-manager update --versions.chrome {your chrome version}`  
Navigate back `cd ../../`  
Run `npm run protractor`  
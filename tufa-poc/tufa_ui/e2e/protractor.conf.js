// @ts-check
// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');
const HtmlReporter = require('protractor-beautiful-reporter');
const path = require('path');
const downloadsPath = path.resolve(__dirname, './data/downloaded-files');

/**
 * @type { import("protractor").Config }
 */
exports.config = {
  allScriptsTimeout: 90000,
  specs: [
    './src/**/*.e2e-spec.ts'
  ],

  capabilities: {
    // TODO: running with internet explorer currently not working from FDA machine; 
    // seems like security issue, FDA machine blocking standalone selenium from running locally.
    // browserName: 'internet explorer',
    // platform: 'ANY',
    // version: '11'

    browserName: 'chrome',
    chromeOptions: {
       'args': ['--headless', '--disable-gpu', '--window-size=1920x4000'],
      //TODO: comment above line and uncomment below line, for non-headless mode in Chrome, for debugging purposes
      // 'args': ['--no-sandbox', '--start-maximized'],
      useAutomationExtension: false, //required for FDA chrome browser, since it imposes some extensions which need disabling.
      prefs: {
        download: { 
          //prefs structure used for download scenarios, setting default download directory.
          prompt_for_download: false,
          default_directory: downloadsPath
        }
      }
    }
  },
  directConnect: true, //Should be used with Chrome browser only.

  suites: {
    'create-company': './src/specs/create-company.e2e-spec.ts',
    'create-permits': './src/specs/create-permits.e2e-spec.ts',
    'create-monthly-report': './src/specs/create-monthly-report.e2e-spec.ts',
    'download': './src/specs/*-download.e2e-spec.ts',
    'navigation': './src/specs/side-navigation.e2e-spec.ts',
    'quarterly-assessments': './src/specs/quarterly-assessments/*',
    'cigar-assessments': './src/specs/cigar-assessments/*',
    'annual-trueup-assessments': ['./src/specs/annual-true-up/test-cases-5817.e2e-spec.ts',
      './src/specs/annual-true-up/test-cases-6474.e2e-spec.ts',
      './src/specs/annual-true-up/test-cases-6543.e2e-spec.ts',
      './src/specs/annual-true-up/test-cases-6545.e2e-spec.ts',
      './src/specs/annual-true-up/test-cases-6552.e2e-spec.ts'],
    'annual-trueup-assessments-demo': ['./src/specs/annual-true-up/test-cases-5817.e2e-spec.ts'],
    'daily-operations': './src/specs/daily-operations.e2e-spec.ts',
    'permit-history': './src/specs/permit-history.e2e-spec.ts',
    'company-details': './src/specs/company-details.e2e-spec.ts',
    'importer': ['./src/specs/importer-monthly-report.e2e-spec.ts',
      './src/specs/importer-monthly-report-error.e2e-spec.ts',
      './src/specs/importer-monthly-report-comments.e2e-spec.ts',
      './src/specs/importer-monthly-report-upload.e2e-spec.ts',
      './src/specs/importer-download.e2e-spec.ts'],
    'manufacturer': ['./src/specs/manufacturer-monthly-report.e2e-spec.ts',
      './src/specs/manufacturer-download.e2e-spec.ts'],
      'manufacturer_monthly_report_sprint126': ['./src/specs/sprint126_manufacturer-monthly-report.e2e-spec.ts'],
    'axe': './src/axe/*'
  },
  // baseUrl: 'http://localhost:4200', //TODO: uncomment for testing in localhost environment
  baseUrl: 'https://tufa.test.fda.gov/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 120000,
    print: function () { }
  },
  onPrepare: async () => {
    require('fs-extra').emptyDirSync(downloadsPath);
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    jasmine.getEnv().addReporter(new HtmlReporter({
      baseDirectory: 'E2E-Test-Reports',
      gatherBrowserLogs: false,
      docTitle: 'TUFA App - End to End Test Results',
      screenshotsSubfolder: 'images',
      jsonsSubfolder: 'json',
      preserveDirectory: false,
      takeScreenShotsOnlyForFailedSpecs: true,
      clientDefaults: {
        showTotalDurationIn: "header",
        totalDurationFormat: "hms",
        columnSettings: {
          warningTime: 15000,
          dangerTime: 30000
        }
      }
    }).getJasmine2Reporter());

  },
  plugins: [
    {
      package: 'protractor-accessibility-reporter',
      options: {
        outputCSV: true,
        outputCSVFolder: './AXE-Test-Reports',
      }
    }
  ]
};
import { browser } from 'protractor';
import { CreateCompanyPage } from '../page/create-company.po';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { StartPage } from '../page/start.po';
import { ScriptUtils } from '../util/script-utils';

let accessibility = require('../../../node_modules/protractor-accessibility-reporter/index.js');
describe('Accessibility Test Reports', () => {
  let startPage: StartPage;
  let loginPage: LoginPage;
  let dailyOperationsPage: DailyOperationsPage;
  let createCompanyPage: CreateCompanyPage;

  beforeAll(() => {
    browser.waitForAngularEnabled().then(r => {
      console.log('waitforAngular is:' + r);
    });
    browser.waitForAngularEnabled(false).then(function() {
      startPage = new StartPage().get(process.env.npm_config_url);
    });
  });

  it('Should check accessibility for Login Page.', () => {
    loginPage = startPage.goToLoginPage();

    accessibility.runAxeTest('Login Page');
 });
  
  it('Should check accessibility for Daily Operations Page.', () => {
    dailyOperationsPage = loginPage.clickConfirmButton();

    accessibility.runAxeTest('Daily Operations Page');

  });

  it('Should check accessibility for Daily Operations Page.', () => {
      createCompanyPage = dailyOperationsPage.clickCreateCompanyButton();

      accessibility.runAxeTest('Create Company Page');

  });
  it('Should be able to create a new company with a unique EIN, and arrive at Company Details page.', () => {
    let uniqueEid = ScriptUtils.generateUniqueEin();
    console.log('Unique EID number: ' + uniqueEid);
    createCompanyPage.clearAllFields();
    createCompanyPage.setCompanyName('Automated Company 1');
    createCompanyPage.setEin(uniqueEid);
    createCompanyPage.setTtbPermit('AA-BB-12345');
    createCompanyPage.selectImporterTypeOption();
    createCompanyPage.setActiveDate('03/20/2020');
    createCompanyPage.clickSaveCompanyButton();    

    accessibility.runAxeTest('Company Detail Page');
  });
});

import { protractor } from "protractor/built/ptor";
import { element, by, browser } from "protractor";

export class MarketShareReadyModalComponent {
    EC = protractor.ExpectedConditions;

    static readonly modalTitleElement = element(by.xpath('//div[contains(text(),"Market Share Ready")]'));
    readonly volumeQ1InputElement = element(by.css('input#compound-tt-1'));
    readonly saveButtonElement = element(by.xpath('//div[contains(@id,"at-cbp-alldeltas-dialog-")]//button[@title="Save"]'));


    clickSaveButton() {
        browser.wait(this.EC.elementToBeClickable(this.saveButtonElement), 10000);
        return this.saveButtonElement.click();
    }

   
}
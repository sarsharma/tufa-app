import { by, element } from "protractor";
import { protractor } from "protractor/built/ptor";

export class MixedActionModalComponent {
    EC = protractor.ExpectedConditions;

    static readonly modalTitleElement = element.all(by.xpath('//div[contains(text(),"Mixed Action")]'));
    readonly acceptIngestedQ1Elements = element.all(by.xpath('//form[@id="compare-details-form-1"]//span[contains(text(),"Accept Ingested")]'));
    readonly acceptIngestedQ2Elements = element.all(by.xpath('//form[@id="compare-details-form-2"]//span[contains(text(),"Accept Ingested")]'));
    readonly acceptIngestedQ3Elements = element.all(by.xpath('//form[@id="compare-details-form-3"]//span[contains(text(),"Accept Ingested")]'));
    readonly acceptIngestedQ4Elements = element.all(by.xpath('//form[@id="compare-details-form-4"]//span[contains(text(),"Accept Ingested")]'));
    readonly acceptFda3852Q1Elements = element.all(by.xpath('//form[@id="compare-details-form-1"]//span[contains(text(),"Accept FDA 3852")]'));
    readonly acceptFda3852Q2Elements = element.all(by.xpath('//form[@id="compare-details-form-2"]//span[contains(text(),"Accept FDA 3852")]'));
    readonly acceptFda3852Q3Elements = element.all(by.xpath('//form[@id="compare-details-form-3"]//span[contains(text(),"Accept FDA 3852")]'));
    readonly acceptFda3852Q4Elements = element.all(by.xpath('//form[@id="compare-details-form-4"]//span[contains(text(),"Accept FDA 3852")]'));

    readonly volumeRemovedQ1TextElement = element(by.xpath('//form[@id="compare-details-form-1"]//div[@id="mixed-action-popup-content"]//b[contains(text(), "Volume Removed:")]'));
    readonly volumeRemovedQ2TextElement = element(by.xpath('//form[@id="compare-details-form-2"]//div[@id="mixed-action-popup-content"]//b[contains(text(), "Volume Removed:")]'));
    readonly volumeRemovedQ3TextElement = element(by.xpath('//form[@id="compare-details-form-3"]//div[@id="mixed-action-popup-content"]//b[contains(text(), "Volume Removed:")]'));
    readonly volumeRemovedQ4TextElement = element(by.xpath('//form[@id="compare-details-form-4"]//div[@id="mixed-action-popup-content"]//b[contains(text(), "Volume Removed:")]'));

    readonly taxAmountQ1TextElement = element(by.xpath('//form[@id="compare-details-form-1"]//div[@id="mixed-action-popup-content"]//b[contains(text(), "Tax Amount:")]'));
    readonly taxAmountQ2TextElement = element(by.xpath('//form[@id="compare-details-form-2"]//div[@id="mixed-action-popup-content"]//b[contains(text(), "Tax Amount:")]'));
    readonly taxAmountQ3TextElement = element(by.xpath('//form[@id="compare-details-form-3"]//div[@id="mixed-action-popup-content"]//b[contains(text(), "Tax Amount:")]'));
    readonly taxAmountQ4TextElement = element(by.xpath('//form[@id="compare-details-form-4"]//div[@id="mixed-action-popup-content"]//b[contains(text(), "Tax Amount:")]'));
    
    readonly saveButtonQ1Element = element(by.css('button#btn-mixedaction-1'));
    readonly saveButtonQ2Element = element(by.css('button#btn-mixedaction-2'));
    readonly saveButtonQ3Element = element(by.css('button#btn-mixedaction-3'));
    readonly saveButtonQ4Element = element(by.css('button#btn-mixedaction-4'));

    readonly cancelButtonQ1Element = element(by.xpath('//form[@id="compare-details-form-1"]//button[text()="Cancel"]'));
    readonly cancelButtonQ2Element = element(by.xpath('//form[@id="compare-details-form-2"]//button[text()="Cancel"]'));
    readonly cancelButtonQ3Element = element(by.xpath('//form[@id="compare-details-form-3"]//button[text()="Cancel"]'));
    readonly cancelButtonQ4Element = element(by.xpath('//form[@id="compare-details-form-4"]//button[text()="Cancel"]'));
   
}
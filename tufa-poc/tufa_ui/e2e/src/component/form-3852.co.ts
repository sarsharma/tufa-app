import * as path from "path";
import { browser, by, element, protractor } from "protractor";

export class Form3852Component {

    EC = protractor.ExpectedConditions;
    readonly formTitleElement = element(by.xpath('//strong[text()="FDA 3852"]'));

    readonly cigarettesTableRowElement = element(by.css('tr#tr-cgt'));
    readonly cigarettesRemovalsAmountFieldElement = element(by.css('input#cgt-rmv-qty'));
    readonly cigarettesExciseTaxFieldElement = element(by.css('input#cgt-tax-amt'));
    readonly cigarettesTaxDiffElement = element(by.css('td#cgt-tx-dif>span'));
    readonly cigarettesCallculatedTaxRateElement = element(by.css('td#cgt-tx-cal>span'));
    readonly cigarettesStatusElement = element(by.css('td#cgt-status>span'));
    readonly cigarettesStatusElementXpath = element(by.xpath('//*[@id="tr-cgt"]/td[7]/span'));
    readonly cigarettesCallculatedTaxRateManuElement = element(by.css('td#cgt-tx-calc>span'));

    readonly cigarsTableRowElement = element(by.css('tr#tr-cig'));
    readonly cigarsRemovalsAmountFieldElement = element(by.css('input#cig-rmv-qty'));
    readonly cigarsExciseTaxFieldElement = element(by.css('input#cig-tax-amt'));
    readonly cigarsTaxDiffElement = element(by.xpath('//*[@id="tr-cig"]/td[5]'));
    readonly cigarsCalculatedTaxRateElement = element(by.xpath('//*[@id="tr-cig"]/td[6]'));
    readonly cigarsStatusElement = element(by.xpath('//*[@id="tr-cig"]/td[7]'));

    readonly snuffTableRowElement = element(by.css('tr#tr-snf'));
    readonly snuffRemovalsAmountFieldElement = element(by.css('input#snf-rmv-qty'));
    readonly snuffExciseTaxFieldElement = element(by.css('input#snf-tax-amt'));
    readonly snuffTaxDiffElement = element(by.css('td#snf-tx-dif>span'));
    readonly snuffCallculatedTaxRateElement = element(by.css('td#snf-tx-cal>span'));
    readonly snuffStatusElement = element(by.css('td#snf-status>span'));
    readonly snuffStatusElementXpath = element(by.xpath('//*[@id="tr-snf"]/td[7]/span'));

    readonly chewTableRowElement = element(by.css('tr#tr-chw'));
    readonly chewRemovalsAmountFieldElement = element(by.css('input#chw-rmv-qty'));
    readonly chewExciseTaxFieldElement = element(by.css('input#chw-tax-amt'));
    readonly chewTaxDiffElement = element(by.css('td#chw-tx-dif>span'));
    readonly chewCallculatedTaxRateElement = element(by.css('td#chw-tx-cal>span'));
    readonly chewStatusElement = element(by.css('td#chw-status>span'));
    readonly chewStatusElementXpath = element(by.xpath('//*[@id="tr-chw"]/td[7]/span'));

    readonly pipeTableRowElement = element(by.css('tr#tr-pip'));
    readonly pipeRemovalsAmountFieldElement = element(by.css('input#pip-rmv-qty'));
    readonly pipeExciseTaxFieldElement = element(by.css('input#pip-tax-amt'));
    readonly pipeTaxDiffElement = element(by.css('td#pip-tx-dif>span'));
    readonly pipeCallculatedTaxRateElement = element(by.css('td#pip-tx-cal>span'));
    readonly pipeStatusElement = element(by.css('td#pip-status>span'));
    readonly pipeStatusElementXpath = element(by.xpath('//*[@id="tr-pip"]/td[7]/span'));

    readonly rollYourOwnTableRowElement = element(by.css('tr#tr-ryo'));
    readonly rollYourOwnRemovalsAmountFieldElement = element(by.css('input#ryo-rmv-qty'));
    readonly rollYourOwnExciseTaxFieldElement = element(by.css('input#ryo-tax-amt'));
    readonly rollYourOwnTaxDiffElement = element(by.css('td#ryo-tx-dif>span'));
    readonly rollYourOwnCallculatedTaxRateElement = element(by.css('td#ryo-tx-cal>span'));
    readonly rollYourOwnStatusElement = element(by.css('td#ryo-status>span'));
    readonly rollYourOwnStatusElementXpath = element(by.xpath('//*[@id="tr-ryo"]/td[7]/span'));

    readonly addCommentElement = element(by.xpath('//*[@id="impform3852"]/div/section/div[2]/div/div/div[2]/div[2]/button'));
    readonly saveCommentElement = element(by.xpath('//*[@id="save-comment"]'));

    readonly errorCommentElement = element(by.xpath('//*[@id="addcomment-form"]/div/div/ul/li[1]'));
    readonly commentTextElement = element(by.xpath('//*[@id="addcomment-form"]/div/div/textarea'));
    readonly cancelCommentElement = element(by.xpath('//*[@id="cancel-comment"]'));
    readonly commentTableElement = element(by.css('table[class = "table table-hover custom"]'));
    readonly commentTableDescriptionElement = element(by.css('table[class = "table table-hover custom"]>tbody>tr>td[class="setwidthdescr"]'));
    readonly commentTableAuthorElement = element(by.xpath('//*[@id="impform3852"]/div/section/div[2]/div/div/div[1]/div/div[2]/table/tbody/tr/td[2]'));
    readonly commentTableDateElement = element(by.css('table[class = "table table-hover custom"]>tbody>tr>td[class="dateWidth"]'));
    readonly commentTableResolvedElement = element(by.xpath('//*[@id="impform3852"]/div/section/div[2]/div/div/div[1]/div/div[2]/table/tbody/tr/td[4]/label'));
    readonly editCommentElement = element(by.css('i[class = "fa fa-pencil ng-star-inserted"]'));

    readonly uploadDocumentElement = element(by.css('button#btn-upload'));
    readonly uploadDocumentTitleElement = element(by.xpath('//*[@id="edit-document-title"]'));
    readonly selectFileElement = element(by.css('input#fileUpload'));
    readonly browseFileTextElement = element(by.xpath('//*[@id="editattachmentform"]/div[1]/div/div[1]/div/span'));
    readonly fileNameElement = element(by.xpath('//*[@id="fileNm"]'));
    readonly uploadedDocumentNameElement = element(by.xpath('//*[@id="impform-doc"]/section/div/table/tbody/tr[1]/td[1]/span/a'));
    readonly saveDocumentElement = element(by.xpath('//*[@id="btn-save-doc"]'));
    readonly selectFDA3852CheckboxElement = element(by.xpath('//*[@id="editattachmentform"]/div[3]/div/div[1]/label/input'));
    readonly selectTTB5220CheckboxElement = element(by.xpath('//*[@id="editattachmentform"]/div[3]/div/div[2]/label/input'));
    readonly selectCBP7501CheckboxElement = element(by.xpath('//*[@id="editattachmentform"]/div[3]/div/div[3]/label/input'));
    readonly selectAllElement = element(by.xpath('//*[@id="editattachmentform"]/div[3]/div/label/input'));
    readonly otherElement = element(by.xpath('//*[@id="editattachmentform"]/div[3]/div/div[4]/label/input'));
    readonly uploadingMessageElement = element(by.xpath('//div[contains(text(),"Uploading...")]'));

    readonly pdfDocRequiredMessageElement = element(by.xpath('//*[@id="editattachmentform"]/div[1]/div/div[2]/ul/li'));
    readonly fileNameRequiredMessageElement = element(by.xpath('//*[@id="editattachmentform"]/div[2]/div/ul/li'));
    readonly formTypeRequiredMessageElement = element(by.xpath('//*[@id="editattachmentform"]/div[3]/div/div[5]/ul/li'));
    readonly cancelPopUpElement = element(by.xpath('//*[@id="edit-document"]/div/div/div[3]/div/div[2]/button[2]'));
    readonly editDocumentElement = element(by.xpath('//*[@id="impform-doc"]/section/div/table/tbody/tr[1]/td[4]/div/button[2]'));
    readonly selectTTB5220CheckboxEditElement = element(by.xpath('//*[@id="editattachmentform"]/div[2]/div/div[2]/label/input'));

    readonly deleteDocumentElement = element(by.xpath('//*[@id="impform-doc"]/section/div/table/tbody/tr/td[4]/div/button[1]'));
    readonly confirmDeleteDocumentElement = element(by.xpath('//*[@id="btn-del-doc"]'));
    readonly downloadDocumentElement = element(by.xpath('//strong[text()="FDA 3852"]/ancestor::section//button[text()="Download"]'));
    readonly documentationDownloadElement = element(by.xpath('//div[@class="row-action-buttons"]/button[text()=" Download"]'));
    readonly downloadingMessageElement = element(by.xpath('//div[contains(text(),"Downloading...")]'));

    setCigarettesRemovalsAmount(value: string) {
        browser.wait(this.EC.elementToBeClickable(this.cigarettesRemovalsAmountFieldElement));
        this.cigarettesRemovalsAmountFieldElement.click();
        this.cigarettesRemovalsAmountFieldElement.clear();
        let promise = this.cigarettesRemovalsAmountFieldElement.sendKeys(value);
        browser.wait(this.EC.textToBePresentInElementValue(this.cigarettesRemovalsAmountFieldElement, value), 5000);
        return promise;
    }

    setCigarettesExciseTax(value: string) {
        this.cigarettesExciseTaxFieldElement.click();
        this.cigarettesExciseTaxFieldElement.clear();
        let promise = this.cigarettesExciseTaxFieldElement.sendKeys(value);
        browser.wait(this.EC.textToBePresentInElementValue(this.cigarettesExciseTaxFieldElement, value), 5000);
        return promise;
    }

    getCigarettesExciseTax() {
        return this.cigarettesExciseTaxFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }

    getCigarettesTaxDiffElement() {
        return this.cigarettesTaxDiffElement.getText();
    }

    getCigarettesCallculatedTaxRateElement() {
        return this.cigarettesCallculatedTaxRateElement.getText();
    }

    getCigarettesCallculatedTaxRateManuElement() {
        return this.cigarettesCallculatedTaxRateManuElement.getText();
    }

    getCigarettesStatusElement() {
        return this.cigarettesStatusElementXpath.getText();
    }

    setCigarsRemovalsAmount(value: string) {
        this.cigarsRemovalsAmountFieldElement.click();
        this.cigarsRemovalsAmountFieldElement.clear();
        let promise = this.cigarsRemovalsAmountFieldElement.sendKeys(value);
        browser.wait(this.EC.textToBePresentInElementValue(this.cigarsRemovalsAmountFieldElement, value), 5000);
        return promise;
    }

    setCigarsExciseTax(value: string) {
        this.cigarsExciseTaxFieldElement.click();
        this.cigarsExciseTaxFieldElement.clear();
        let promise = this.cigarsExciseTaxFieldElement.sendKeys(value);
        browser.wait(this.EC.textToBePresentInElementValue(this.cigarsExciseTaxFieldElement, value), 5000);
        return promise;
    }

    getCigarsExciseTax() {
        return this.cigarsExciseTaxFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }

    getCigarsTaxDiffElement() {
        return this.cigarsTaxDiffElement.getText();
    }

    getCigarsCallculatedTaxRateElement() {
        return this.cigarsCalculatedTaxRateElement.getText();
    }

    getCigarsStatusElement() {
        return this.cigarsStatusElement.getText();
    }

    setSnuffRemovalsAmount(value: string) {
        this.snuffRemovalsAmountFieldElement.click();
        this.snuffRemovalsAmountFieldElement.clear();
        let promise = this.snuffRemovalsAmountFieldElement.sendKeys(value);
        browser.wait(this.EC.textToBePresentInElementValue(this.snuffRemovalsAmountFieldElement, value), 5000);
        return promise;
    }

    setSnuffExciseTax(value: string) {
        this.snuffExciseTaxFieldElement.click();
        this.snuffExciseTaxFieldElement.clear();
        return this.snuffExciseTaxFieldElement.sendKeys(value);
    }

    getSnuffExciseTaxField() {
        return this.snuffExciseTaxFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }

    getSnuffTaxDiffElement() {
        return this.snuffTaxDiffElement.getText();
    }

    getSnuffCallculatedTaxRateElement() {
        return this.snuffCallculatedTaxRateElement.getText();
    }

    getSnuffStatusElement() {
        return this.snuffStatusElementXpath.getText();
    }

    setChewRemovalsAmount(value: string) {
        this.chewRemovalsAmountFieldElement.click();
        this.chewRemovalsAmountFieldElement.clear();
        let promise = this.chewRemovalsAmountFieldElement.sendKeys(value);
        browser.wait(this.EC.textToBePresentInElementValue(this.chewRemovalsAmountFieldElement, value), 5000);
        return promise;
    }

    setChewExciseTax(value: string) {
        this.chewExciseTaxFieldElement.click();
        this.chewExciseTaxFieldElement.clear();
        return this.chewExciseTaxFieldElement.sendKeys(value);
    }

    getChewExciseTaxField() {
        return this.chewExciseTaxFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }

    getChewTaxDiffElement() {
        return this.chewTaxDiffElement.getText();
    }

    getChewCallculatedTaxRateElement() {
        return this.chewCallculatedTaxRateElement.getText();
    }

    getChewStatusElement() {
        return this.chewStatusElementXpath.getText();
    }

    setPipeRemovalsAmount(value: string) {
        this.pipeRemovalsAmountFieldElement.click();
        this.pipeRemovalsAmountFieldElement.clear();
        let promise = this.pipeRemovalsAmountFieldElement.sendKeys(value);
        browser.wait(this.EC.textToBePresentInElementValue(this.pipeRemovalsAmountFieldElement, value), 5000);
        return promise;
    }

    setPipeExciseTax(value: string) {
        this.pipeExciseTaxFieldElement.click();

        this.pipeExciseTaxFieldElement.clear();
        return this.pipeExciseTaxFieldElement.sendKeys(value);
    }

    getPipeExciseTax() {
        return this.pipeExciseTaxFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }

    getPipeTaxDiffElement() {
        return this.pipeTaxDiffElement.getText();
    }

    getPipeCallculatedTaxRateElement() {
        return this.pipeCallculatedTaxRateElement.getText();
    }

    getPipesStatusElement() {
        return this.pipeStatusElementXpath.getText();
    }

    setRollYourOwnRemovalsAmount(value: string) {
        this.rollYourOwnRemovalsAmountFieldElement.click();
        this.rollYourOwnRemovalsAmountFieldElement.clear();
        let promise = this.rollYourOwnRemovalsAmountFieldElement.sendKeys(value);
        browser.wait(this.EC.textToBePresentInElementValue(this.rollYourOwnRemovalsAmountFieldElement, value), 5000);
        return promise;
    }

    setRollYourOwnExciseTax(value: string) {
        this.rollYourOwnExciseTaxFieldElement.click();
        this.rollYourOwnExciseTaxFieldElement.clear();
        return this.rollYourOwnExciseTaxFieldElement.sendKeys(value);
    }

    getRollYourOwnExciseTax() {
        return this.rollYourOwnExciseTaxFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }

    getRollYourOwnTaxDiffElement() {
        return this.rollYourOwnTaxDiffElement.getText();
    }

    getRollYourOwnCallculatedTaxRateElement() {
        return this.rollYourOwnCallculatedTaxRateElement.getText();
    }

    getRollYourOwnStatusElement() {
        return this.rollYourOwnStatusElementXpath.getText();
    }

    clickSaveUpload() {
        this.saveDocumentElement.click();
        browser.wait(this.EC.invisibilityOf(this.uploadingMessageElement));
        browser.wait(this.EC.visibilityOf(this.uploadedDocumentNameElement));
    }

    clickConfirmDelete() {
        browser.wait(this.EC.visibilityOf(this.confirmDeleteDocumentElement));
        this.confirmDeleteDocumentElement.click();
        // browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.wait(this.EC.invisibilityOf(this.uploadedDocumentNameElement));
        browser.wait(this.EC.elementToBeClickable(this.uploadDocumentElement));
    }

    clickDownloadDocument() {
        browser.wait(this.EC.and(
            this.EC.elementToBeClickable(this.downloadDocumentElement), 
            this.EC.visibilityOf(this.downloadDocumentElement))
        );
        this.downloadDocumentElement.click();
        browser.wait(this.EC.invisibilityOf(this.downloadingMessageElement));
    }

    clickDocumentationDownloadDocument() {
        browser.wait(this.EC.and(
            this.EC.elementToBeClickable(this.documentationDownloadElement), 
            this.EC.visibilityOf(this.documentationDownloadElement))
        );
        this.documentationDownloadElement.click();
        browser.wait(this.EC.invisibilityOf(this.downloadingMessageElement));
    }

    downloadingEnded() {
        browser.wait(this.EC.invisibilityOf(this.downloadingMessageElement));
    }

    clickCommentTableResolved() {
        browser.sleep(2000);
        browser.wait(this.EC.visibilityOf(this.commentTableResolvedElement));
        browser.wait(this.EC.elementToBeClickable(this.commentTableResolvedElement));
        browser.actions().mouseMove(this.commentTableResolvedElement).click().perform();
    }

    selectFile(absolutep: string, fileName: string) {
        var fileToUpload = absolutep.concat(fileName);
        var absolutePath = path.resolve(__dirname, fileToUpload);
        this.selectFileElement.sendKeys(absolutePath);
        browser.executeScript("document.evaluate('/html/body/app-root/app-layout/div[1]/main/div/div/div[3]/div[4]/div/div/div/div[2]/form/div[1]/div/div[1]/div/span' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue.textContent = '" + fileName + "';");
        browser.sleep(2000);
    }

    getCommentTableDescription() {
        browser.wait(this.EC.visibilityOf(this.commentTableDescriptionElement));
        this.commentTableDescriptionElement.getText();
    }

    saveComment() {
        browser.wait(this.EC.visibilityOf(this.saveCommentElement));
        this.saveCommentElement.click();
    }

    cancelComment() {
        browser.wait(this.EC.visibilityOf(this.cancelCommentElement));
        this.cancelCommentElement.click();
    }

    setCommentText(value: string) {
        browser.sleep(2000);
        browser.wait(this.EC.visibilityOf(this.commentTextElement));
        this.commentTextElement.click();
        this.commentTextElement.clear().then(() => {
            this.commentTextElement.sendKeys(value);
        });
    }

    checkDateFormat() {
        let date1 = this.commentTableDateElement.getText();

    }

}
import { by, element, browser, protractor } from "protractor";

export class Form5000Component {
    readonly formTitleElement = element(by.xpath('//strong[contains(text(),"TTB 5000.24")]'));

    readonly cigarTaxAmountBreakDownElements = element.all(by.xpath('//input[contains(@id,"5000_cigar_")]'));
    readonly cigarettesTaxAmountBreakDownElements = element.all(by.xpath('//input[contains(@id,"5000_cigarettes_")]'));
    readonly chewSnuffTaxAmountBreakDownElements = element.all(by.xpath('//input[contains(@id,"5000_shufchew_")]'));
    readonly pipeRollYourOwnTaxAmountBreakDownElements = element.all(by.xpath('//input[contains(@id,"5000_pipes_")]'));

    readonly allDifferenceTextElements = element.all(by.xpath('//span[text()="Difference"]/ancestor::tr//span'));
    readonly allActivityTextElements = element.all(by.xpath('(//span[text()="Activity"])[2]/ancestor::tr//span'));

    readonly downloadDocumentElement = element(by.xpath('//strong[text()="TTB 5000.24"]/ancestor::section//button[text()="Download"]'));
    readonly downloadingMessageElement = element(by.xpath('//div[contains(text(),"Downloading...")]'));

    //Sprint 126 Enable Adjustments and Schedule Adjustment A/B
    //FORM 5000.24
    readonly enableAllAdjustmentsElement = element(by.xpath('//strong[text()="TTB 5000.24"]/ancestor::section//input[@id="adjustment-all"]'));
    readonly enableAdjustmentCigarettesElement = element(by.xpath('//strong[text()="TTB 5000.24"]/ancestor::section//input[@id = "adjustment-Cigarattes"]'));
    readonly enableAdjustmentCigarsElement = element(by.xpath('//strong[text()="TTB 5000.24"]/ancestor::section//input[@id = "adjustment-Cigars"]'));
    readonly enableAdjustmentChewSnuffElement = element(by.xpath('//strong[text()="TTB 5000.24"]/ancestor::section//input[@id = "adjustment-ChewandSnuff"]'));
    readonly enableAdjustmentPipeRYOElement = element(by.xpath('//strong[text()="TTB 5000.24"]/ancestor::section//input[@id = "adjustment-PipeRYO"]'));
    readonly totalSchAAdjElements = element.all(by.xpath('//span[text()="Total Sch A Adj"]'));
    readonly totalSchBAdjElements = element.all(by.xpath('//span[text()="Total Sch B Adj"]'));
    readonly adjustmentTotalElements = element.all(by.xpath('//span[text()="Adjustment Total"]'));
    readonly viewButtonElement = element.all(by.xpath('//strong[text()="TTB 5000.24"]/ancestor::section//button[contains(text(),"View")]'));
    readonly taxReturnBreakdownID1Element = element.all(by.xpath('//span[text()="Tax Amount Breakdown"]/ancestor::tbody/tr[2]/td[2]'));
    readonly taxReturnBreakdownID2Element = element.all(by.xpath('//span[text()="Tax Amount Breakdown"]/ancestor::tbody/tr[3]/td[2]'));

    //Pop Up Elements
    readonly scheduleABAdjustmentsTitleElement = element(by.xpath('//div[text()="Schedule A/B Adjustments"]'));
    readonly scheduleABAdjustmentsClassElement = element(by.xpath('//div[contains(text(),"Class:")]'));
    readonly scheduleAAdjustmentsElement = element(by.xpath('//b[text()="Schedule A Adjustments"]'));
    readonly scheduleBAdjustmentsElement = element(by.xpath('//b[text()="Schedule B Adjustments"]'));

    readonly lineNumTableElement = element.all(by.xpath('//span[text()="Line #"]'));
    readonly taxReturnTableElement = element.all(by.xpath('//span[text()="Tax Return"]'));
    readonly amountTableElement = element.all(by.xpath('//span[text()="Amount"]'));
    readonly showDeleteTableElement = element.all(by.xpath('//span[text()="Show Delete"]'));

    readonly lineNumAFilterElement = element(by.css('input#scheduledA-adjLineNum'));
    readonly taxReturnAFilterElement = element(by.css('input#scheduledA-taxReturnId'));
    readonly amountAFilterElement = element(by.css('input#scheduledA-taxAmount')); 
    readonly showDeleteToggleAElement = element(by.xpath('//input[@id="deletetoggleA"]/ancestor::label/i'));

    readonly lineNumBFilterElement = element(by.css('input#scheduledB-adjLineNum'));
    readonly taxReturnBFilterElement = element(by.css('input#scheduledB-taxReturnId'));
    readonly amountBFilterElement = element(by.css('input#scheduledB-taxAmount'));
    readonly showDeleteToggleBElement = element(by.xpath('//input[@id="deletetoggleB"]/ancestor::label/i'));

    readonly lineNumDataAElement = element(by.xpath('//input[@id="scheduledA-adjLineNum"]/ancestor::table//tfoot/tr[1]/td/span'));
    readonly lineDescriptionDataAElement = element(by.xpath('//input[@id="scheduledA-adjLineNum"]/ancestor::table//tfoot/tr[1]/td/input'));
    readonly taxReturnDropdownAElement = element(by.xpath('//input[@id="scheduledA-adjLineNum"]/ancestor::table//tfoot/tr[1]/td[2]//select'));
    readonly taxReturnEditableInputAElement = element(by.xpath('//input[@id="scheduledA-adjLineNum"]/ancestor::table//tfoot/tr[1]/td[2]//input'));
    readonly amountInputAElement = element(by.xpath('//input[@id="scheduledA-adjLineNum"]/ancestor::table//tfoot/tr[1]/td[3]//input'));
    readonly addButtonAElement = element(by.xpath('//input[@id="scheduledA-adjLineNum"]/ancestor::table//tfoot/tr[1]/td[4]//button[@id="add-button"]'));
    readonly deleteButtonAElement = element.all(by.xpath('//input[@id="scheduledA-adjLineNum"]/ancestor::table//tfoot/tr[1]/td[4]//button')).get(0);
    readonly subTotalScheduleAElement = element(by.xpath('//span[contains(text(),"Subtotal Schedule A:")]'));

    readonly lineNumDataBElement = element(by.xpath('//input[@id="scheduledB-adjLineNum"]/ancestor::table//tfoot/tr[1]/td/span'));
    readonly lineDescriptionDataBElement = element(by.xpath('//input[@id="scheduledB-adjLineNum"]/ancestor::table//tfoot/tr[1]/td/input'));
    readonly taxReturnDropdownBElement = element(by.xpath('//input[@id="scheduledB-adjLineNum"]/ancestor::table//tfoot/tr[1]/td[2]//select'));
    readonly taxReturnEditableInputBElement = element(by.xpath('//input[@id="scheduledB-adjLineNum"]/ancestor::table//tfoot/tr[1]/td[2]//input'));
    readonly amountInputBElement = element(by.xpath('//input[@id="scheduledB-adjLineNum"]/ancestor::table//tfoot/tr[1]/td[3]//input'));
    readonly addButtonBElement = element(by.xpath('//input[@id="scheduledB-adjLineNum"]/ancestor::table//tfoot/tr[1]/td[4]//button[@id="add-button"]'));
    readonly deleteButtonBElement = element.all(by.xpath('//input[@id="scheduledB-adjLineNum"]/ancestor::table//tfoot/tr[1]/td[4]//button')).get(0);
    readonly subTotalScheduleBElement = element(by.xpath('//span[contains(text(),"Subtotal Schedule B:")]'));

    readonly scheduleABAdjustmentTotalElement = element(by.xpath('//b[contains(text(),"Schedule A/B Adjustment Total")]'));
    readonly saveAdjustmentsElement = element(by.css('button#btn-mixedaction'));
    readonly cancelAdjustmentsElement = element(by.xpath('//button[text()="Cancel"]'));

    //FORM 5210.5
    readonly enableAllAdjustments5210Element = element(by.css('input#adjustment-5210-all'));
    readonly cigarsEnableAdjustments5210Element = element(by.css('input#adjustment-5210-cigars'));    
    readonly cigarettesEnableAdjustments5210Element = element(by.css('input#adjustment-5210-cigarette'));  
    readonly chewEnableAdjustments5210Element = element(by.css('input#adjustment-5210-chew'));    
    readonly snuffEnableAdjustments5210Element = element(by.css('input#adjustment-5210-snuff')); 
    readonly snuffEnableAdjustments5210Element2 = element(by.css('input#adjustment-snuff'));    
    readonly pipeEnableAdjustments5210Element = element(by.css('input#adjustment-5210-pipe'));    
    readonly ryoEnableAdjustments5210Element = element(by.css('input#adjustment-5210-ryn'));

    readonly cigarTotalSticks5210Element = element(by.xpath('//input[@id="cig-lg-stk"]/ancestor::tr//span[text()="Total sticks:"]'));
    readonly cigarettesTotalSticks5210Element = element(by.xpath('//input[@id="cgt-lg-stk"]/ancestor::tr//span[text()="Total sticks:"]'));

    readonly cigarLineAdjustment5210Element = element(by.css('input#cig-qty52105-la'));
    readonly cigarettesLineAdjustment5210Element = element(by.css('input#cgt-qty52105-la'));
    readonly chewLineAdjustment5210Element = element(by.css('input#chw-qty52105-la'));    
    readonly snuffLineAdjustment5210Element = element(by.css('input#snf-qty52105-la'));    
    readonly pipeLineAdjustment5210Element = element(by.css('input#pip-qty52105-la'));    
    readonly ryoLineAdjustment5210Element = element(by.css('input#ryo-qty52105-la'));

    
    readonly pleaseWaitMessageElement = element(by.xpath('//div[contains(text(),"Please wait...")]'));
    readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Loading...")]'));



    EC = protractor.ExpectedConditions;

    setCigarTaxAmountBreakDown(value: string, index?: number) {
        (index!=null)?index:0;
        this.cigarTaxAmountBreakDownElements.get(index).clear();
        return this.cigarTaxAmountBreakDownElements.get(index).sendKeys(value);
    }
    
    setCigarettesTaxAmountBreakDown(value: string, index?: number) {
        (index!=null)?index:0;
        this.cigarettesTaxAmountBreakDownElements.get(index).clear();
        return this.cigarettesTaxAmountBreakDownElements.get(index).sendKeys(value);
    }

    setChewSnuffTaxAmountBreakDown(value: string, index?: number) {
        (index!=null)?index:0;
        this.chewSnuffTaxAmountBreakDownElements.get(index).clear();
        return this.chewSnuffTaxAmountBreakDownElements.get(index).sendKeys(value);
    }

    setPipeRollYourOwnTaxAmountBreakDown(value: string, index?: number) {
        (index!=null)?index:0;
        this.pipeRollYourOwnTaxAmountBreakDownElements.get(index).clear();
        return this.pipeRollYourOwnTaxAmountBreakDownElements.get(index).sendKeys(value);
    }

    getDifferenceText(index: number) {
        return this.allDifferenceTextElements.get(index).getText();
    }

    getActivityText(index: number) {
        return this.allActivityTextElements.get(index).getText();
    }
    
    clickDownloadDocument() {
        browser.wait(this.EC.and(
            this.EC.elementToBeClickable(this.downloadDocumentElement), 
            this.EC.visibilityOf(this.downloadDocumentElement))
        );
        this.downloadDocumentElement.click();
        browser.wait(this.EC.invisibilityOf(this.downloadingMessageElement));
    }

    selectOptionAdjA(lineNum: string, optionValue: string) {
        return element(by.xpath('//select[@id = "scheduleTaxA-' + lineNum + '"]/option[@value = "' + optionValue + '"]')).click();
    }

    selectOptionAdjB(lineNum: string, optionValue: string) {
        return element(by.xpath('//select[@id = "scheduleTaxB-' + lineNum + '"]/option[@value = "' + optionValue + '"]')).click();
    }

    getTotalAdjA(tobaccoClass : string){
        return element(by.xpath('//span[text()="Total Sch A Adj"]/ancestor::tr//td[' +tobaccoClass+ ']/span[1]'))
    }

    getTotalAdjB(tobaccoClass : string){
        return element(by.xpath('//span[text()="Total Sch B Adj"]/ancestor::tr//td[' +tobaccoClass+ ']/span[2]'))
    }

    getAdjustmentTotal(tobaccoClass : string){
        return element(by.xpath('//span[text()="Adjustment Total"]/ancestor::tr//td[' +tobaccoClass+ ']/span[3]'))
    }

    getGrandTotal(tobaccoClass : string){
        return element(by.xpath('//span/b[text()="Grand Total"]/ancestor::tr//td[' +tobaccoClass+ ']/span'))
    }

    getDifference(tobaccoClass : string){
        return element(by.xpath('//span[text()="Difference"]/ancestor::tr//td[' +tobaccoClass+ ']/span'))
    }

    getActivity(tobaccoClass : string){
        return element(by.xpath('//span[text()="Activity"]/ancestor::tr//td[' +tobaccoClass+ ']/span'))
    }

    waitForPleaseWait() {
        browser.wait(this.EC.invisibilityOf(this.pleaseWaitMessageElement));
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
      }

    getGrandTotable(tobaccoClass: string, value: string){
        return element(by.xpath('//b[text()="' + tobaccoClass +' Grand Total"]/ancestor::tr/td[' + value + ']/span/b'))
    }
}
import { browser, by, element, protractor } from "protractor";

export class Form5210Component {
    readonly formTitleElement = element(by.xpath('//strong[text()="TTB 5210.5"]'));
    readonly enableAllAdjustments = element(by.css('#adjustment-5210-all'));

    readonly cigarettesTableRowElement = element(by.xpath('//td[contains(text(),"Cigarettes")]'));
    readonly cigarettesSmallRemovalsAmountFieldElement = element(by.css('#cgt-sm-stk'));
    readonly cigarettesLargeRemovalsAmountFieldElement = element(by.css('#cgt-lg-stk'));
    readonly cigarettesLineAdjusmentFieldElement = element(by.css('#cgt-qty52105-la'));
    //TODO: find better xpath / add unique ID
    readonly cigarettesDifferenceRemovalsAmountElement = element(by.xpath('//td[contains(text(),"Cigarettes")]/parent::tr//span[@class="floatright"]'));
    readonly cigarettesActivityElement = element(by.xpath('//td[contains(text(),"Cigarettes")]/parent::tr//span[contains(@class,"largeleftmargin")]'));

    readonly cigarsTableRowElement = element(by.xpath('//td[contains(text(),"Cigars")]'));
    readonly cigarsSmallRemovalsAmountFieldElement = element(by.css('#cig-sm-stk'));
    readonly cigarsLargeRemovalsAmountFieldElement = element(by.css('#cig-lg-stk'));
    readonly cigarsLineAdjusmentFieldElement = element(by.css('#cig-qty52105-la'));
    //TODO: find better xpath / add unique ID
    readonly cigarsDifferenceRemovalsAmountElement = element(by.xpath('//td[contains(text(),"Cigars")]/parent::tr//span[@class="floatright"]'));
    readonly cigarsActivityElement = element(by.xpath('//td[contains(text(),"Cigars")]/parent::tr//span[contains(@class,"largeleftmargin")]'));

    readonly snuffTableRowElement = element(by.xpath('//label[contains(text(),"Snuff")]'));
    readonly snuffRemovalsAmountFieldElement = element(by.css('#snf-qty52105'));
    readonly snuffLineAdjusmentFieldElement = element(by.css('#snf-qty52105-la'));
    //TODO: find better xpath / add unique ID
    readonly snuffDifferenceRemovalsAmountElement = element(by.xpath('//strong[text()="TTB 5210.5"]/ancestor::section//label[contains(text(),"Snuff")]/ancestor::tr//span[@class="floatright"]'));
    readonly snuffActivityElement = element(by.xpath('//strong[text()="TTB 5210.5"]/ancestor::section//label[contains(text(),"Snuff")]/ancestor::tr//span[contains(@class,"largeleftmargin")]'));

    readonly chewTableRowElement = element(by.xpath('//label[contains(text(),"Chew")]'));
    readonly chewRemovalsAmountFieldElement = element(by.css('#chw-qty52105'));
    readonly chewLineAdjusmentFieldElement = element(by.css('#chw-qty52105-la'));
    //TODO: find better xpath / add unique ID
    readonly chewDifferenceRemovalsAmountElement = element(by.xpath('//strong[text()="TTB 5210.5"]/ancestor::section//label[contains(text(),"Chew")]/ancestor::tr//span[@class="floatright"]'));
    readonly chewActivityElement = element(by.xpath('//strong[text()="TTB 5210.5"]/ancestor::section//label[contains(text(),"Chew")]/ancestor::tr//span[contains(@class,"largeleftmargin")]'));

    readonly pipeTableRowElement = element(by.xpath('//label[contains(text(),"Pipe")]'));
    readonly pipeRemovalsAmountFieldElement = element(by.css('#pip-qty52105'));
    readonly pipeLineAdjusmentFieldElement = element(by.css('#pip-qty52105-la'));
    //TODO: find better xpath / add unique ID
    readonly pipeDifferenceRemovalsAmountElement = element(by.xpath('//strong[text()="TTB 5210.5"]/ancestor::section//label[contains(text(),"Pipe")]/ancestor::tr//span[@class="floatright"]'));
    readonly pipeActivityElement = element(by.xpath('//strong[text()="TTB 5210.5"]/ancestor::section//label[contains(text(),"Pipe")]/ancestor::tr//span[contains(@class,"largeleftmargin")]'));

    readonly rollYourOwnTableRowElement = element(by.xpath('//label[contains(text(),"Roll Your Own")]'));
    readonly rollYourOwnRemovalsAmountFieldElement = element(by.css('#ryo-qty52105'));
    readonly rollYourOwnLineAdjusmentFieldElement = element(by.css('#ryo-qty52105-la'));
    //TODO: find better xpath / add unique ID
    readonly rollYourOwnDifferenceRemovalsAmountElement = element(by.xpath('//strong[text()="TTB 5210.5"]/ancestor::section//label[contains(text(),"Roll Your Own")]/ancestor::tr//span[@class="floatright"]'));
    readonly rollYourOwnActivityElement = element(by.xpath('//strong[text()="TTB 5210.5"]/ancestor::section//label[contains(text(),"Roll Your Own")]/ancestor::tr//span[contains(@class,"largeleftmargin")]'));

    readonly downloadDocumentElement = element(by.xpath('//strong[text()="TTB 5210.5"]/ancestor::section//button[text()="Download"]'));
    readonly downloadingMessageElement = element(by.xpath('//div[contains(text(),"Downloading...")]'));

    EC = protractor.ExpectedConditions;
    
    setCigarettesSmallRemovalsAmount(value: string) {
        this.cigarettesSmallRemovalsAmountFieldElement.clear();
        this.cigarettesSmallRemovalsAmountFieldElement.sendKeys(value);
    }

    setCigarettesLargeRemovalsAmount(value: string) {
        this.cigarettesLargeRemovalsAmountFieldElement.clear();
        this.cigarettesLargeRemovalsAmountFieldElement.sendKeys(value);
    }

    setCigarettesLineAdjustmentAmount(value: string) {
        this.cigarettesLineAdjusmentFieldElement.clear();
        this.cigarettesLineAdjusmentFieldElement.sendKeys(value);
    }

    setCigarsSmallRemovalsAmount(value: string) {
        this.cigarsSmallRemovalsAmountFieldElement.clear();
        this.cigarsSmallRemovalsAmountFieldElement.sendKeys(value);
    }

    setCigarsLargeRemovalsAmount(value: string) {
        this.cigarsLargeRemovalsAmountFieldElement.clear();
        this.cigarsLargeRemovalsAmountFieldElement.sendKeys(value);
    }

    setCigarsLineAdjustmentAmount(value: string) {
        this.cigarsLineAdjusmentFieldElement.clear();
        this.cigarsLineAdjusmentFieldElement.sendKeys(value);
    }

    setSnuffRemovalsAmount(value: string) {
        this.snuffRemovalsAmountFieldElement.clear();
        this.snuffRemovalsAmountFieldElement.sendKeys(value);
    }

    setSnuffLineAdjustmentAmount(value: string) {
        this.snuffLineAdjusmentFieldElement.clear();
        this.snuffLineAdjusmentFieldElement.sendKeys(value);
    }

    setChewRemovalsAmount(value: string) {
        this.chewRemovalsAmountFieldElement.clear();
        this.chewRemovalsAmountFieldElement.sendKeys(value);
    }

    setChewLineAdjustmentAmount(value: string) {
        this.chewLineAdjusmentFieldElement.clear();
        this.chewLineAdjusmentFieldElement.sendKeys(value);
    }

    setPipeRemovalsAmount(value: string) {
        this.pipeRemovalsAmountFieldElement.clear();
        this.pipeRemovalsAmountFieldElement.sendKeys(value);
    }

    setPipeLineAdjustmentAmount(value: string) {
        this.pipeLineAdjusmentFieldElement.clear();
        this.pipeLineAdjusmentFieldElement.sendKeys(value);
    }

    setRollYourOwnRemovalsAmount(value: string) {
        this.rollYourOwnRemovalsAmountFieldElement.clear();
        this.rollYourOwnRemovalsAmountFieldElement.sendKeys(value);
    }

    setRollYourOwnLineAdjustmentAmount(value: string) {
        this.rollYourOwnLineAdjusmentFieldElement.clear();
        this.rollYourOwnLineAdjusmentFieldElement.sendKeys(value);
    }

    
    getCigarettesActivityElement() {
        return this.cigarettesActivityElement.getText();
    }

    getCigarettesDifferenceRemovalsAmountElement() {
        return this.cigarettesDifferenceRemovalsAmountElement.getText();
    }

    getCigarsActivityElement() {
        return this.cigarsActivityElement.getText();
    }

    getCigarsDifferenceRemovalsAmountElement() {
        return this.cigarsDifferenceRemovalsAmountElement.getText();
    }

    getSnuffActivityElement() {
        return this.snuffActivityElement.getText();
    }

    getSnuffDifferenceRemovalsAmountElement() {
        return this.snuffDifferenceRemovalsAmountElement.getText();
    }

    getChewActivityElement() {
        return this.chewActivityElement.getText();
    }

    getChewDifferenceRemovalsAmountElement() {
        return this.chewDifferenceRemovalsAmountElement.getText();
    }

    getPipeActivityElement() {
        return this.pipeActivityElement.getText();
    }

    getPipeDifferenceRemovalsAmountElement() {
        return this.pipeDifferenceRemovalsAmountElement.getText();
    }

    getRollYourOwnActivityElement() {
        return this.rollYourOwnActivityElement.getText();
    }

    getRollYourOwnDifferenceRemovalsAmountElement() {
        return this.rollYourOwnDifferenceRemovalsAmountElement.getText();
    }

    
	getCigarettesLargeRemovalsAmount() {
        return this.cigarettesLargeRemovalsAmountFieldElement.getAttribute('value');
    }

	getCigarettesSmallRemovalsAmount() {
        return this.cigarettesSmallRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }
	
    getCigarsLargeRemovalsAmount() {
        return this.cigarsLargeRemovalsAmountFieldElement.getAttribute('value');
    }

    getCigarsSmallRemovalsAmount() {
        return this.cigarsSmallRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }
	
	getSnuffRemovalsAmount() {
        return this.snuffRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }
	
	getChewRemovalsAmount() {
        return this.chewRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }
	
	 getPipeRemovalsAmount() {
        return this.pipeRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }
	
	getRollYourOwnRemovalsAmount() {
        return this.rollYourOwnRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }
    
    clickDownloadDocument() {
        browser.wait(this.EC.and(
            this.EC.elementToBeClickable(this.downloadDocumentElement), 
            this.EC.visibilityOf(this.downloadDocumentElement))
        );
        this.downloadDocumentElement.click();
        browser.wait(this.EC.invisibilityOf(this.downloadingMessageElement));
    }

    clickEnableAllAdjustments() {
        this.enableAllAdjustments.click();
    }
}
import { by, element, browser, protractor } from "protractor";

export class Form5220Component {
    readonly formTitleElement = element(by.xpath('//string[text()="TTB 5220.6"]'));

    readonly cigarettesTableRowElement = element(by.xpath('//*[@id="impforms"]/div[2]/div/div/section/div[1]/div[3]/div/table/tbody/tr[1]/td[1]'));
    readonly cigarettesSmallRemovalsAmountFieldElement = element(by.css('input#cgt-522-sm-amt'));
    readonly cigarettesLargeRemovalsAmountFieldElement = element(by.css('input#cgt-522-lg-amt'));
    readonly cigarettesTotalRemovalsAmountElement = element(by.css('span#cgt-522-total'));
    readonly cigarettesDifferenceRemovalsAmountElement = element(by.css('td#cgt-522-diff>span'));
    readonly cigarettesActivityElement = element(by.xpath('//*[@id="impforms"]/div[2]/div/div/section/div[1]/div[3]/div/table/tbody/tr[1]/td[4]/span'));

    readonly cigarsTableRowElement = element(by.xpath('//*[@id="impforms"]/div[2]/div/div/section/div[1]/div[3]/div/table/tbody/tr[2]/td[1]'));
    readonly cigarsSmallRemovalsAmountFieldElement = element(by.css('input#cig-522-sm-amt'));
    readonly cigarsLargeRemovalsAmountFieldElement = element(by.css('input#cig-522-lg-amt'));
    readonly cigarsTotalRemovalsAmountElement = element(by.css('span#cig-522-total'));
    readonly cigarsDifferenceRemovalsAmountElement = element(by.css('td#cig-522-diff>span'));
    readonly cigarsActivityElement = element(by.css('td#cig-522-status span'));

    readonly snuffTableRowElement = element(by.xpath('//*[@id="impforms"]/div[2]/div/div/section/div[1]/div[3]/div/table/tbody/tr[3]/td[1]'));
    readonly snuffRemovalsAmountFieldElement = element(by.css('input#snf-522-amt'));
    readonly snuffDifferenceRemovalsAmountElement = element(by.css('td#snf-522-diff>span'));
    readonly snuffActivityElement = element(by.xpath('//*[@id="impforms"]/div[2]/div/div/section/div[1]/div[3]/div/table/tbody/tr[3]/td[4]/span')); 

    readonly chewTableRowElement = element(by.xpath('//*[@id="impforms"]/div[2]/div/div/section/div[1]/div[3]/div/table/tbody/tr[5]/td[1]'));
    readonly chewRemovalsAmountFieldElement = element(by.css('input#chw-522-amt'));
    readonly chewDifferenceRemovalsAmountElement = element(by.css('td#chw-522-diff>span'));
    readonly chewActivityElement = element(by.xpath('//*[@id="impforms"]/div[2]/div/div/section/div[1]/div[3]/div/table/tbody/tr[5]/td[4]/span'));

    readonly pipeTableRowElement = element(by.xpath('//*[@id="impforms"]/div[2]/div/div/section/div[1]/div[3]/div/table/tbody/tr[7]/td[1]'));
    readonly pipeRemovalsAmountFieldElement = element(by.css('input#pip-522-amt'));
    readonly pipeDifferenceRemovalsAmountElement = element(by.css('td#pip-522-diff>span'));
    readonly pipeActivityElement = element(by.xpath('//*[@id="impforms"]/div[2]/div/div/section/div[1]/div[3]/div/table/tbody/tr[7]/td[4]/span'));

    readonly rollYourOwnTableRowElement = element(by.xpath('//*[@id="impforms"]/div[2]/div/div/section/div[1]/div[3]/div/table/tbody/tr[9]/td[1]'));
    readonly rollYourOwnRemovalsAmountFieldElement = element(by.css('input#ryo-522-amt'));
    readonly rollYourOwnDifferenceRemovalsAmountElement = element(by.css('td#ryo-522-diff>span'));
    readonly rollYourOwnActivityElement = element(by.xpath('//*[@id="impforms"]/div[2]/div/div/section/div[1]/div[3]/div/table/tbody/tr[9]/td[4]/span'));

    readonly downloadDocumentElement = element(by.xpath('//strong[text()="TTB 5220.6"]/ancestor::section//button[text()="Download"]'));
    readonly downloadingMessageElement = element(by.xpath('//div[contains(text(),"Downloading...")]'));

    EC = protractor.ExpectedConditions;
    
    setCigarettesSmallRemovalsAmount(value: string) {
        this.cigarettesSmallRemovalsAmountFieldElement.clear();
        this.cigarettesSmallRemovalsAmountFieldElement.sendKeys(value);
    }

    setCigarettesLargeRemovalsAmount(value: string) {
        this.cigarettesLargeRemovalsAmountFieldElement.clear();
        this.cigarettesLargeRemovalsAmountFieldElement.sendKeys(value);
    }

    setCigarsSmallRemovalsAmount(value: string) {
        this.cigarsSmallRemovalsAmountFieldElement.clear();
        this.cigarsSmallRemovalsAmountFieldElement.sendKeys(value);
    }

    setCigarsLargeRemovalsAmount(value: string) {
        this.cigarsLargeRemovalsAmountFieldElement.clear();
        this.cigarsLargeRemovalsAmountFieldElement.sendKeys(value);
    }

    setSnuffRemovalsAmount(value: string) {
        this.snuffRemovalsAmountFieldElement.clear();
        this.snuffRemovalsAmountFieldElement.sendKeys(value);
    }

    setChewRemovalsAmount(value: string) {
        this.chewRemovalsAmountFieldElement.clear();
        this.chewRemovalsAmountFieldElement.sendKeys(value);
    }

    setPipeRemovalsAmount(value: string) {
        this.pipeRemovalsAmountFieldElement.clear();
        this.pipeRemovalsAmountFieldElement.sendKeys(value);
    }

    setRollYourOwnRemovalsAmount(value: string) {
        this.rollYourOwnRemovalsAmountFieldElement.clear();
        this.rollYourOwnRemovalsAmountFieldElement.sendKeys(value);
    }

    
    getCigarettesActivityElement() {
        return this.cigarettesActivityElement.getText();
    }

    getCigarettesDifferenceRemovalsAmountElement() {
        return this.cigarettesDifferenceRemovalsAmountElement.getText();
    }

    getCigarsActivityElement() {
        return this.cigarsActivityElement.getText();
    }

    getCigarsDifferenceRemovalsAmountElement() {
        return this.cigarsDifferenceRemovalsAmountElement.getText();
    }

    getSnuffActivityElement() {
        return this.snuffActivityElement.getText();
    }

    getSnuffDifferenceRemovalsAmountElement() {
        return this.snuffDifferenceRemovalsAmountElement.getText();
    }

    getChewActivityElement() {
        return this.chewActivityElement.getText();
    }

    getChewDifferenceRemovalsAmountElement() {
        return this.chewDifferenceRemovalsAmountElement.getText();
    }

    getPipeActivityElement() {
        return this.pipeActivityElement.getText();
    }

    getPipeDifferenceRemovalsAmountElement() {
        return this.pipeDifferenceRemovalsAmountElement.getText();
    }

    getRollYourOwnActivityElement() {
        return this.rollYourOwnActivityElement.getText();
    }

    getRollYourOwnDifferenceRemovalsAmountElement() {
        return this.rollYourOwnDifferenceRemovalsAmountElement.getText();
    }

    
	getCigarettesLargeRemovalsAmount() {
        return this.cigarettesLargeRemovalsAmountFieldElement.getAttribute('value');
    }

	getCigarettesSmallRemovalsAmount() {
        return this.cigarettesSmallRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }
    
    getCigarsLargeRemovalsAmount() {
        return this.cigarsLargeRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }

	getCigarsSmallRemovalsAmount() {
        return this.cigarsSmallRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }
	
	getSnuffRemovalsAmount() {
        return this.snuffRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }
	
	getChewRemovalsAmount() {
        return this.chewRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }
	
	 getPipeRemovalsAmount() {
        return this.pipeRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }
	
	getRollYourOwnRemovalsAmount() {
        return this.rollYourOwnRemovalsAmountFieldElement.getAttribute('value').then(function (resultText) {
            return resultText;
        });
    }

    clickDownloadDocument() {
        browser.wait(this.EC.and(
            this.EC.elementToBeClickable(this.downloadDocumentElement), 
            this.EC.visibilityOf(this.downloadDocumentElement))
        );
        this.downloadDocumentElement.click();
        browser.wait(this.EC.invisibilityOf(this.downloadingMessageElement));
    }
    
}
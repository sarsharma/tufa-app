import { by, element, browser, Key } from "protractor";
import { protractor } from "protractor/built/ptor";

export class Form7501Component {
    readonly formTitleElement = element(by.xpath('//div[text()="CBP 7501"]'));

    readonly cigarettesFormElement = element(by.css('div#form-7501-cgt'));
    readonly cigarettes3852RemovalsQuantityElement = element(by.css('span#cgt-3852-rmv-qty'));
    readonly cigarettes7501RemovalsQuantityElement = element(by.css('span#cgt-7501-rmv-qty'));
    readonly cigarettesRemovalsQuantityDifferenceElement = element(by.css('span#cgt-rmv-qty-diff'));
    readonly cigarettesRemovalsQuantityStatusElement = element(by.css('span#cgt-rmv-qty-status'));
    readonly cigarettes3852TaxAmountElement = element(by.css('span#cgt-3852-tax-amt'));
    readonly cigarettes7501TaxAmountElement = element(by.css('span#cgt-7501-tax-amt'));
    readonly cigarettesTaxAmountDifferenceElement = element(by.css('span#cgt-tax-amt-diff'));
    readonly cigarettesTaxAmountStatusElement = element(by.css('span#cgt-tax-amt-status'));
    readonly cigarettesTaxAmountFilter = element(by.css('input#cig-filter-taxAmt'));
    readonly cigarettesQuantityAmountFilter = element(by.css('input#cig-filter-qty'));
    readonly cigarettesAddButton = element(by.css('button#cgt-add-button'));
    readonly cigarettesVolumeFieldElement = element(by.css('input#cigarette-vol-0'));
    readonly cigarettesTaxFieldElement = element(by.css('input#cigarette-tax-0'));
    readonly cigarettesLineCalcTaxRateElement = element(by.css('td#cgt-tx-cal-7501>span'));
    readonly cigarettesLineTaxDifferenceElement = element(by.css('td#cgt-tx-diff-7501>span'));
    readonly cigarettesTotalStatusElement = element(by.css('span#cgt-total-status'));

    readonly cigarsFormElement = element(by.css('div#form-7501-cig'));
    readonly cigars3852RemovalsQuantityElement = element(by.css('span#cig-3852-rmv-qty'));
    readonly cigars7501RemovalsQuantityElement = element(by.css('span#cig-7501-rmv-qty'));
    readonly cigarsRemovalsQuantityDifferenceElement = element(by.css('span#cig-rmv-qty-diff'));
    readonly cigarsRemovalsQuantityStatusElement = element(by.css('span#cig-rmv-qty-status'));
    readonly cigars3852TaxAmountElement = element(by.css('span#cig-3852-tax-amt'));
    readonly cigars7501TaxAmountElement = element(by.css('span#cig-7501-tax-amt'));
    readonly cigarsTaxAmountDifferenceElement = element(by.css('span#cig-tax-amt-diff'));
    readonly cigarsTaxAmountStatusElement = element(by.css('span#cig-tax-amt-status'));
    readonly cigarsAddButton = element(by.xpath('//div[@id="form-7501-cig"]//button[text()="Add"]'));
    readonly cigarLineItemElements = element.all(by.xpath('//tr[starts-with(@id,"cigar-")]'));
    readonly cigarTypeDropdownFirstElement = element(by.xpath('//select[(@id="cigars-type-0")]'));
    readonly cigarNetQuantityFirstElement = element(by.xpath('//tr[@id="cigar-0"]/td[3]//input'));
    readonly cigarLineNumberFirstElement = element(by.xpath('//tr[@id="cigar-0"]/td[1]//span'));
    readonly cigarExciseTaxFirstElement = element(by.xpath('//tr[@id="cigar-0"]/td[4]//input'));
    readonly cigarTotalStatusElement = element(by.css('span#cig-total-status'));
    
    readonly snuffFormElement = element(by.css('div#form-7501-snf'));
    readonly snuff3852RemovalsQuantityElement = element(by.css('span#snf-3852-rmv-qty'));
    readonly snuff7501RemovalsQuantityElement = element(by.css('span#snf-7501-rmv-qty'));
    readonly snuffRemovalsQuantityDifferenceElement = element(by.css('span#snf-rmv-qty-diff'));
    readonly snuffRemovalsQuantityStatusElement = element(by.css('span#snf-rmv-qty-status'));
    readonly snuff3852TaxAmountElement = element(by.css('span#snf-3852-tax-amt'));
    readonly snuff7501TaxAmountElement = element(by.css('span#snf-7501-tax-amt'));
    readonly snuffTaxAmountDifferenceElement = element(by.css('span#snf-tax-amt-diff'));
    readonly snuffTaxAmountStatusElement = element(by.css('span#snf-tax-amt-status'));
    readonly snuffAddButton = element(by.css('button#snf-add-button'));
    readonly snuffVolumeFieldElement = element(by.css('input#snuff-vol-0'));
    readonly snuffTaxFieldElement = element(by.css('input#snuff-tax-0'));
    readonly snuffTaxRateElement = element(by.css('td#snuff-tx-cal-7501>span'));
    readonly snuffTaxDiffElement = element(by.css('td#snuff-tx-diff-7501>span'))
    readonly snuffTotalStatusElement = element(by.css('span#snf-total-status'));

    readonly chewFormElement = element(by.css('div#form-7501-chw'));
    readonly chew3852RemovalsQuantityElement = element(by.css('span#chw-3852-rmv-qty'));
    readonly chew7501RemovalsQuantityElement = element(by.css('span#chw-7501-rmv-qty'));
    readonly chewRemovalsQuantityDifferenceElement = element(by.css('span#chw-rmv-qty-diff'));
    readonly chewRemovalsQuantityStatusElement = element(by.css('span#chw-rmv-qty-status'));
    readonly chew3852TaxAmountElement = element(by.css('span#chw-3852-tax-amt'));
    readonly chew7501TaxAmountElement = element(by.css('span#chw-7501-tax-amt'));
    readonly chewTaxAmountDifferenceElement = element(by.css('span#chw-tax-amt-diff'));
    readonly chewTaxAmountStatusElement = element(by.css('span#chw-tax-amt-status'));
    readonly chewAddButton = element(by.css('button#chw-add-button'));
    readonly chewVolumeFieldElement = element(by.css('input#chew-vol-0'));
    readonly chewTaxFieldElement = element(by.css('input#chew-tax-0'));
    readonly chewCalcTaxRateElement = element(by.css('td#chew-tx-cal-7501>span'));
    readonly chewTaxDiffElement = element(by.css('td#chew-tx-diff-7501>span'));
    readonly chewTotalStatusElement = element(by.css('span#chw-total-status'));

    readonly pipeFormElement = element(by.css('div#form-7501-pip'));
    readonly pipe3852RemovalsQuantityElement = element(by.css('span#pip-3852-rmv-qty'));
    readonly pipe7501RemovalsQuantityElement = element(by.css('span#pip-7501-rmv-qty'));
    readonly pipeRemovalsQuantityDifferenceElement = element(by.css('span#pip-rmv-qty-diff'));
    readonly pipeRemovalsQuantityStatusElement = element(by.css('span#pip-rmv-qty-status'));
    readonly pipe3852TaxAmountElement = element(by.css('span#pip-3852-tax-amt'));
    readonly pipe7501TaxAmountElement = element(by.css('span#pip-7501-tax-amt'));
    readonly pipeTaxAmountDifferenceElement = element(by.css('span#pip-tax-amt-diff'));
    readonly pipeTaxAmountStatusElement = element(by.css('span#pip-tax-amt-status'));
    readonly pipeAddButton = element(by.css('button#pip-add-button'));
    readonly pipeVolumeFieldElement = element(by.css('input#pipe-vol-0'));
    readonly pipeTaxFieldElement = element(by.css('input#pipe-tax-0'));
    readonly pipeCalcTaxRateElement = element(by.css('td#pipe-tx-cal-7501>span'));
    readonly pipeTaxDiffElement = element(by.css('td#pipe-tx-diff-7501>span'));
    readonly pipeTotalStatusElement = element(by.css('span#pip-total-status'));

    readonly rollYourOwnFormElement = element(by.css('div#form-7501-ryo'));
    readonly rollYourOwn3852RemovalsQuantityElement = element(by.css('span#ryo-3852-rmv-qty'));
    readonly rollYourOwn7501RemovalsQuantityElement = element(by.css('span#ryo-7501-rmv-qty'));
    readonly rollYourOwnRemovalsQuantityDifferenceElement = element(by.css('span#ryo-rmv-qty-diff'));
    readonly rollYourOwnRemovalsQuantityStatusElement = element(by.css('span#ryo-rmv-qty-status'));
    readonly rollYourOwn3852TaxAmountElement = element(by.css('span#ryo-3852-tax-amt'));
    readonly rollYourOwn7501TaxAmountElement = element(by.css('span#ryo-7501-tax-amt'));
    readonly rollYourOwnTaxAmountDifferenceElement = element(by.css('span#ryo-tax-amt-diff'));
    readonly rollYourOwnTaxAmountStatusElement = element(by.css('span#ryo-tax-amt-status'));
    readonly rollYourOwnAddButton = element(by.css('button#ryo-add-button'));
    readonly rollYourOwnVolumeFieldElement = element(by.css('input#ryo-vol-0'));
    readonly rollYourOwnTaxFieldElement = element(by.css('input#ryo-tax-0'));
    readonly rollYourOwnCalcTaxRateElement = element(by.css('td#ryo-tx-cal-7501>span'));
    readonly rollYourOwnTaxDiffElement = element(by.css('td#ryo-tx-diff-7501>span'));
    readonly rollYourOwnTotalStatusElement = element(by.css('span#ryo-total-status'));

    readonly cigarettesDownloadButtonElement = element(by.xpath('//h4[text()="Cigarettes"]/ancestor::section//button[text()="Download"]'));
    readonly cigarsDownloadButtonElement = element(by.xpath('//h4[text()="Cigars"]/ancestor::section//button[text()="Download"]'));
    readonly snuffDownloadButtonElement = element(by.xpath('//h4[text()="Snuff"]/ancestor::section//button[text()="Download"]'));
    readonly chewDownloadButtonElement = element(by.xpath('//h4[text()="Chew"]/ancestor::section//button[text()="Download"]'));
    readonly pipeDownloadButtonElement = element(by.xpath('//h4[text()="Pipe"]/ancestor::section//button[text()="Download"]'));
    readonly rollYourOwnDownloadButtonElement = element(by.xpath('//h4[text()="Roll Your Own"]/ancestor::section//button[text()="Download"]'));
    readonly downloadingMessageElement = element(by.xpath('//div[contains(text(),"Downloading...")]'));

    readonly EC = protractor.ExpectedConditions;

    clickCigarettesAddButton() {
        browser.wait(this.EC.elementToBeClickable(this.cigarettesAddButton));
        return this.cigarettesAddButton.click();
    }

    clickCigarsAddButton() {
        browser.wait(this.EC.elementToBeClickable(this.cigarsAddButton));
        let promise = this.cigarsAddButton.click();
        browser.sleep(1000).then(function() {
            console.log('Slept 1 second to stabilize cigar net quantity field.');
        });
        return promise;
    }

    selectCigarTypeDropdown(value: string) {
        browser.wait(this.EC.visibilityOf(this.cigarTypeDropdownFirstElement), 5000);
        return this.cigarTypeDropdownFirstElement.element(by.xpath('./option[@value="' + value + '"]')).click();
    }

    setCigarNetQuantity(value: string) {
        this.cigarNetQuantityFirstElement.sendKeys(value);
        browser.sleep(3000).then(function() {
            console.log('Slept 3 second to stabilize cigar net quantity field.');
        });
        return this.cigarLineNumberFirstElement.click();;
    }

    setCigarExciseTax(value: string) {
        return this.cigarExciseTaxFirstElement.sendKeys(value);
    }

    clickSnuffAddButton() {
        browser.wait(this.EC.elementToBeClickable(this.snuffAddButton));
        return this.snuffAddButton.click();
    }

    clickChewAddButton() {
        browser.wait(this.EC.elementToBeClickable(this.chewAddButton));
        return this.chewAddButton.click();
    }

    clickPipeAddButton() {
        browser.wait(this.EC.elementToBeClickable(this.pipeAddButton));
        return this.pipeAddButton.click();
    }

    clickRollYourOwnAddButton() {
        browser.wait(this.EC.elementToBeClickable(this.rollYourOwnAddButton));
        return this.rollYourOwnAddButton.click();
    }

    setCigarettesVolumeField(volume: string) {
        this.cigarettesVolumeFieldElement.click();
        this.cigarettesVolumeFieldElement.clear();
        return this.cigarettesVolumeFieldElement.sendKeys(volume);
    }

    setCigarettesTaxField(tax: string) {
        this.cigarettesTaxFieldElement.click();
        this.cigarettesTaxFieldElement.clear();
        return this.cigarettesTaxFieldElement.sendKeys(tax);
    }

    setSnuffVolumeField(volume: string) {
        this.snuffVolumeFieldElement.click();
        this.snuffVolumeFieldElement.clear();
        return this.snuffVolumeFieldElement.sendKeys(volume);
    }

    setSnuffTaxField(tax: string) {
        this.snuffTaxFieldElement.click();
        this.snuffTaxFieldElement.clear();
        return this.snuffTaxFieldElement.sendKeys(tax);
    }

    setChewVolumeField(volume: string) {
        this.chewVolumeFieldElement.click();
        this.chewVolumeFieldElement.clear();
        return this.chewVolumeFieldElement.sendKeys(volume);
    }

    setChewTaxField(tax: string) {
        this.chewTaxFieldElement.click();
        this.chewTaxFieldElement.clear();
        return this.chewTaxFieldElement.sendKeys(tax);
    }

    setPipeVolumeField(volume: string) {
        this.pipeVolumeFieldElement.click();
        this.pipeVolumeFieldElement.clear();
        return this.pipeVolumeFieldElement.sendKeys(volume);
    }

    setPipeTaxField(tax: string) {
        this.pipeTaxFieldElement.click();
        this.pipeTaxFieldElement.clear();
        return this.pipeTaxFieldElement.sendKeys(tax);
    }

    setRollYourOwnVolumeField(volume: string) {
        this.rollYourOwnVolumeFieldElement.click();
        this.rollYourOwnVolumeFieldElement.clear();
        return this.rollYourOwnVolumeFieldElement.sendKeys(volume);
    }

    setRollYourOwnTaxField(tax: string) {
        this.rollYourOwnTaxFieldElement.click();
        this.rollYourOwnTaxFieldElement.clear();
        return this.rollYourOwnTaxFieldElement.sendKeys(tax);
    }

    clickCigarettesDownloadButton() {
        browser.wait(this.EC.and(
            this.EC.elementToBeClickable(this.cigarettesDownloadButtonElement), 
            this.EC.visibilityOf(this.cigarettesDownloadButtonElement))
        );
        this.cigarettesDownloadButtonElement.click();
        return browser.wait(this.EC.invisibilityOf(this.downloadingMessageElement));
    }

    clickCigarsDownloadButton() {
        browser.wait(this.EC.and(
            this.EC.elementToBeClickable(this.cigarsDownloadButtonElement), 
            this.EC.visibilityOf(this.cigarsDownloadButtonElement))
        );
        this.cigarsDownloadButtonElement.click();
        return browser.wait(this.EC.invisibilityOf(this.downloadingMessageElement));
    }

    clickSnuffDownloadButton() {
        browser.wait(this.EC.and(
            this.EC.elementToBeClickable(this.snuffDownloadButtonElement), 
            this.EC.visibilityOf(this.snuffDownloadButtonElement))
        );
        this.snuffDownloadButtonElement.click();
        return browser.wait(this.EC.invisibilityOf(this.downloadingMessageElement));
    }

    clickChewDownloadButton() {
        browser.wait(this.EC.and(
            this.EC.elementToBeClickable(this.chewDownloadButtonElement), 
            this.EC.visibilityOf(this.chewDownloadButtonElement))
        );
        this.chewDownloadButtonElement.click();
        return browser.wait(this.EC.invisibilityOf(this.downloadingMessageElement));
    }

    clickPipeDownloadButton() {
        browser.wait(this.EC.and(
            this.EC.elementToBeClickable(this.pipeDownloadButtonElement), 
            this.EC.visibilityOf(this.pipeDownloadButtonElement))
        );
        this.pipeDownloadButtonElement.click();
        return browser.wait(this.EC.invisibilityOf(this.downloadingMessageElement));
    }

    clickRollYourOwnDownloadButton() {
        browser.wait(this.EC.and(
            this.EC.elementToBeClickable(this.rollYourOwnDownloadButtonElement), 
            this.EC.visibilityOf(this.rollYourOwnDownloadButtonElement))
        );
        this.rollYourOwnDownloadButtonElement.click();
        return browser.wait(this.EC.invisibilityOf(this.downloadingMessageElement));
    }
}
import { browser, by, element, protractor } from "../../../node_modules/protractor";
import { AssessmentsPage } from "../page/assessments/assessments.po";
import { CompanyDetailPage } from "../page/company-detail.po";
import { DailyOperationsPage } from "../page/daily-operations.po";
import { LogoutPage } from "../page/logout.po";

export class BreadcrumbNavigationComponent {

    EC = protractor.ExpectedConditions;

    readonly dailyOperationsLink = element(by.css('a#lnk-dly-ops'));

    readonly fileManagementLink = element(by.css('a#lnk-filemgmt'));

    readonly companyTrackingLink = element(by.css('a#lnk-comp-tracking'));

    readonly companyDetailsLink = element(by.css('a#lnk-cmp-det'));
    readonly permitHistoryLink = element(by.css('a#lnk-pmt-his'));

    readonly assessmentsLink = element(by.css('a#lnk-assessments'));
    readonly quarterlyAssessmentLink = element(by.css('a#lnk-qa'));
    readonly cigarAssessmentLink = element(by.css('a#lnk-ca'));
    readonly annualTrueUpLink = element(by.css('a#lnk-at'));
    readonly companreDetailsLink = element(by.css('a#lnk-comdetails'));
    readonly dailyOperationsPageTitleElement = element(by.xpath('//h1[text()="Daily Operations"]'));

    readonly companyDetailsPermitHistoryBCElement =  element(by.xpath('//*[@id="permitHistoryForm"]/ol/li[2]/a'));
    readonly dailyOperationsPermitHistoryBCElement =  element(by.xpath('//*[@id="permitHistoryForm"]/ol/li[1]/a'));

    clickDailyOperationsLink() {
        this.dailyOperationsLink.click();
        browser.wait(this.EC.visibilityOf(this.dailyOperationsPageTitleElement));
        return new DailyOperationsPage();
    }

    clickFileManagmentLink() {
        this.fileManagementLink.click();
        // return new FileManagementPage();
    }

    clickCompanyTrackingLink() {
        this.companyTrackingLink.click();
        // return new ComapnyTrackingPage();
    }

    clickCompanyDetailsLink() {
        this.companyDetailsLink.click();
        return new CompanyDetailPage();
    }

    clickPermitHistoryLink() {
        this.permitHistoryLink.click();
        //return new PermitHistoryPage();
    }

    clickAssessmentsLink() {
        this.assessmentsLink.click();
        return new AssessmentsPage();
    }

    clickQuarterlyAssessmentLink() {
        this.quarterlyAssessmentLink.click();
        // return new QuarterlyAssessmentPage();
    }

    clickCigarAssessmentLink() {
        this.cigarAssessmentLink.click();
        // return new CigarAssessmentPage();
    }

    clickAnnualTrueUpLinkLink() {
        this.annualTrueUpLink.click();
        // return new AnnualTrueUpPage();
    }

    clickCompareDetailsLink() {
        this.companreDetailsLink.click();
        //return new CompareDetailsPage();
    }
}

export class SideNavigtionComponent {
    EC = protractor.ExpectedConditions;

    static readonly colapseElement = element(by.css('div#toggle'));

    // Need to target the icon rather than the full link due to how the colapse works
    readonly dashboardNavElement = element(by.css('a#dashboard>span>.fa'));
    readonly dailyOperationsNavElement = element(by.css('a#daily_operations>span>.fa'));
    readonly assessmentsNavElement = element(by.css('a#mnu_assessments>span>.fa'));
    readonly permitManagmentNavElement = element(by.css('a#permit-mgmt>span>.fa'));
    readonly companyTrackingNavElement = element(by.css('a#company_tracking>span>.fa'));
    readonly ratesCodesNavElement = element(by.css('a#rates_codes>span>.fa'));
    readonly adminNavElement = element(by.css('a#admin>span>.fa'));
    readonly logoutNavElement = element(by.css('a#logout_user>span>.fa'));

    clickDashboardNavElement() {
        this.dashboardNavElement.click();
        //return Dashboard page
    }

    clickDailyOperationsNavElement() {
        this.dailyOperationsNavElement.click();
        return new DailyOperationsPage();
    }

    clickAssessmentsNavElement() {
        this.assessmentsNavElement.click();
        return new AssessmentsPage();
    }

    clickPermitManagmentNavElement() {
        this.permitManagmentNavElement.click();
        //return Permit page
    }

    clickCompanyTrackingNavElement() {
        this.companyTrackingNavElement.click();
        //return Company page
    }

    clickRatesCodesNavElement() {
        this.ratesCodesNavElement.click();
        //return Rates and Codes page
    }

    clickAdminNavElement() {
        this.adminNavElement.click();
        //return Admin page
    }

    clickLogoutNavElement() {
        this.logoutNavElement.click();
        return new LogoutPage();
    }
}
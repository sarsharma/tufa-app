import { browser, by, element, protractor } from "../../../node_modules/protractor";

export class TobaccoClassSelectionComponent {
    EC = protractor.ExpectedConditions;

    readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Please wait...")]'));

    readonly cigarettesSelection = element(by.css('toggle-button-tufa[title="Cigarettes"]'));
    readonly cigarsSelection = element(by.css('toggle-button-tufa[title="Cigars"]'));
    readonly snuffSelection = element(by.css('toggle-button-tufa[title="Snuff"]'));
    readonly chewSelection = element(by.css('toggle-button-tufa[title="Chew"]'));
    readonly pipeSelection = element(by.css('toggle-button-tufa[title="Pipe"]'));
    readonly rollYourOwnSelection = element(by.css('toggle-button-tufa[title="Roll Your Own"]'));
    readonly selectAllSelection = element(by.css('toggle-button-tufa[title="Select All"]'));
    readonly allSelectedTobaccoClassesElements = element.all(by.css('label.btn-primary'));

    clickCigarettesSelection() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        this.cigarettesSelection.click();
    }

    clickCigarsSelection() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        this.cigarsSelection.click();
    }

    clickSnuffSelection() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        this.snuffSelection.click();
    }

    clickChewSelection() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        this.chewSelection.click();
    }

    clickPipeSelection() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        this.pipeSelection.click();
    }

    clickRollYourOwnSelection() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        this.rollYourOwnSelection.click();
    }

    clickSelectAllSelection() {
        browser.executeScript("arguments[0].scrollIntoView();", this.selectAllSelection.getWebElement());
        browser.wait(this.EC.and(this.EC.visibilityOf(this.selectAllSelection), this.EC.elementToBeClickable(this.selectAllSelection)));
        browser.sleep(1000);
        return this.selectAllSelection.click();
    }

    resetSelection() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        this.clickSelectAllSelection();
        // browser.wait(this.EC.visibilityOf(this.allSelectedTobaccoClassesElements.get(0)));
        this.clickSelectAllSelection();
        // browser.wait(this.EC.invisibilityOf(this.allSelectedTobaccoClassesElements.get(0)));
    }

    getAllSelectedTobaccoClasses() {
        return this.allSelectedTobaccoClassesElements;
    };
}
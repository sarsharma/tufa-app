import { browser, by, element, protractor } from "../../../node_modules/protractor";
import * as path from 'path';

export class UploadDocumentModalComponent {
    EC = protractor.ExpectedConditions;

    readonly titleElement = element(by.css('h2#add-document-title'));

    readonly fileInputFieldElement = element(by.xpath('//input[@id="fileUpload"]'));
    readonly fileInputFileNameElement = element(by.css('.fileinput-filename'));
    readonly fileNameFieldElement = element(by.css('input#fileNm'));
    readonly descriptionFieldElement = element(by.css('input#description'));
    readonly quarterSelectElement = element(by.css('select#quarter')); //For cigar assessment only
    readonly versionSelectElement = element(by.css('select#versions'));

    readonly documentRequiredErrorMessageElement = element(by.xpath('//li[text()="Error: Document is required"]'));
    readonly fileNameRequiredErrorMessageElement = element(by.xpath('//li[text()="Error: File Name is required"]'));
    readonly quarterRequiredErrorMessageElement = element(by.xpath('//li[text()="Error: Quarter is required"]')); //For cigar assessment only
    readonly versionRequiredErrorMessageElement = element(by.xpath('//li[text()="Error: Version is required"]'));

    readonly uploadButtonElement = element(by.css('button#btn-save-doc'));
    readonly cancelButtonElement = element(by.xpath('//button[contains(text(),"Cancel")]'));

    readonly fileInputFileNameAbsolutePath = '/html[1]/body[1]/app-root[1]/app-layout[1]/div[1]/main[1]/div[1]/div[2]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/tabset[1]/div[1]/tab[3]/div[1]/div[1]/div[2]/section[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[1]/div[1]/span[1]';
    
    setfileInputField(absolutep: string, fileName: string) {
        this.fileInputFieldElement.sendKeys(path.resolve(absolutep, fileName));
        /* Below javascript execution is required because of behavior on file upload modal; after file is selected;
        the file input field remains blank, but needs to hold the filename so that the file is actually uploaded. */
        browser.executeScript(`document.evaluate("${this.fileInputFileNameAbsolutePath}" ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue.textContent = "${fileName}";`);

        return browser.sleep(2000);
    }

    selectQuarter(quarter: string) {
        this.quarterSelectElement.click();
        return element(by.xpath('//div[@class="modal-content"]//option[contains(text(),"' + quarter + '")]')).click();
    }

    selectVersion(version: string) {
        this.versionSelectElement.click();
        return element(by.xpath('//div[@class="modal-content"]//option[@value="' + version + '"]')).click();
    }

}
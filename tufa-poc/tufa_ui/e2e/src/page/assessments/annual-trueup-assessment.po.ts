import { by, element, protractor, browser } from "protractor";
import { CompareDetailsPage } from "../compare-details.po";

export class AnnualTrueUpAssessmentPage {
    EC = protractor.ExpectedConditions;

    readonly pageTitleElement = element(by.xpath('//h1[text()="Annual True-Up"]'));

    //Tabs
    readonly ingestTabButtonElement = element(by.css('a#at-ingets-tab-link'));
    readonly mapTabButtonElement = element(by.css('a#tab-rpt-link'));
    readonly compareTabButtonElement = element(by.css('a#tab-com-link'));
    readonly reconcileTabButtonElement = element(by.css('a#tab-rec-link'));
    readonly generateAndSubmitTabButtonElement = element(by.css('a#tab-rpt-link'));

    readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Loading...")]'));
    readonly loadingCompanyAssociationsMessageElement = element(by.xpath('//div[contains(text(),"Loading Company Associations")]'));
    readonly fetchingDocumentsMessageElement = element(by.xpath('//div[contains(text(),"Fetching documents")]'));

    //Ingest Tab Selectors 

    //Map Tab Selectors 
    
    //Compare Tab Selectors 
    readonly matchedRadioElement = element(by.xpath('//label[@for="rd-matched"]'));
    readonly allDeltasRadioElement = element(by.xpath('//label[@for="rd-alldeltas"]'));
    readonly cigarettesSelection = element(by.css('toggle-button-tufa[title="Cigarettes"]'));
    readonly cigarsSelection = element(by.css('toggle-button-tufa[title="Cigars"]'));
    readonly snuffSelection = element(by.css('toggle-button-tufa[title="Snuffs"]'));
    readonly chewSelection = element(by.css('toggle-button-tufa[title="Chews"]'));
    readonly pipeSelection = element(by.css('toggle-button-tufa[title="Pipes"]'));
    readonly rollYourOwnSelection = element(by.css('toggle-button-tufa[title="Roll Your Own"]'));
    readonly selectAllSelection = element(by.css('toggle-button-tufa[title="SelectAll"]'));
    readonly compareTabActionRequiredElement = element(by.xpath('//tab[@id="tab-com"]//strong[contains(text(),"ACTION REQUIRED")]'));
    readonly exportAsCsvButtonElement = element(by.xpath('//tab[@id="tab-com"]//button[@id="btn-exp-rcon"]'));
    readonly deltaCompareTableRowElements = element.all(by.xpath('//div[@id="tab2"]//table[@id="tbl-rpt"]//tr[position()>1]'));
    readonly matchedCompareTableRowElements = element.all(by.xpath('//div[@id="tab2"]//table[@id="tbl-rpt"]//tr[position()>1]'));
    readonly permitTypeFilter = element(by.css('select#filter-permitType'));
    readonly dataTypeFilter = element(by.css('select#filter-data'));
    readonly actionFilterButtonElement = element(by.xpath('//select[@id="multiselectmatched"]/parent::td//button'))
    readonly companyNameFilter = element(by.css('input#filter-legalName'));
    readonly tobaccoClassTypeFilter = element(by.css('input#filter-tobaccoClass'));
    readonly quarterFilter = element(by.css('input#filter-quarter'));
    readonly deltaExciseTaxLinkElements = element.all(by.css('a#anchor-at-alldeltas-popup'));
    readonly matchedExciseTaxLinkElements = element.all(by.xpath('//a[contains(@href,"app/assessments/compare")]'));
    readonly inProgressMatchedExciseTaxLinkElements = element.all(by.xpath('//i[@title="In Progress"]/ancestor::tr//a[contains(@href,"app/assessments/compare")]'));
    readonly companyNameSortingButtonElement = element(by.xpath('//mfdefaultsorter[contains(@by,"legalName")]/a'));
    
    
    //Reconcile Tab Selectors 

    //Generate & Submit Tab Selectors 
    readonly generateTabActionRequiredElement = element(by.xpath('//tab[@id="tab-rpt"]//strong[contains(text(),"ACTION REQUIRED:")]'));
    readonly generateTabActionRecommendedElement = element(by.xpath('//tab[@id="tab-rpt"]//strong[contains(text(),"ACTION RECOMMENDED: ")]'));

    waitForLoading() {
        return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
    }

    clickCompareTabButton() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement), 10000);
        browser.wait(this.EC.elementToBeClickable(this.compareTabButtonElement), 10000);
        return this.compareTabButtonElement.click();
    }

    clickMatchedRadio() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.sleep(1500);
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.wait(this.EC.elementToBeClickable(this.matchedRadioElement));
        this.matchedRadioElement.click();
        return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
    }

    clickAllDeltasRadio() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.sleep(1500);
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.wait(this.EC.elementToBeClickable(this.allDeltasRadioElement));
        browser.sleep(2000);
        this.allDeltasRadioElement.click();
        return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
    }

    clickSelectAllCheckbox() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.wait(this.EC.elementToBeClickable(this.selectAllSelection));
        this.selectAllSelection.click();
        browser.wait(this.EC.and(this.EC.invisibilityOf(this.loadingMessageElement), this.EC.stalenessOf(this.loadingMessageElement)));
        return browser.sleep(3000);
    }

    filterByPermitType(permitType: string) {
        this.permitTypeFilter.element(by.xpath('./option[contains(text(), "' + permitType + '")]')).click();
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.sleep(1000);
        return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
}

    filterByDataType(dataType: string) {
        this.dataTypeFilter.element(by.xpath('./option[contains(text(), "' + dataType + '")]')).click();
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.sleep(1000);
        return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
}

    filterByAction(action: string) {
        this.actionFilterButtonElement.click();
        browser.sleep(1000);
        element(by.xpath('//li//label[contains(text(), "Select all")]')).click();
        browser.sleep(2000);
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.sleep(1000);
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        element(by.xpath('//li//label[contains(text(), "' + action + '")]')).click();        
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.sleep(1000);
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
}

    filterByCompanyName(companyName: string, skipEnterKey?:boolean) {
        this.companyNameFilter.clear();
        if(skipEnterKey) {
            return this.companyNameFilter.sendKeys(companyName);
        } else {
            this.companyNameFilter.sendKeys(companyName);
            this.companyNameFilter.sendKeys(protractor.Key.ENTER);
            browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
            browser.sleep(1000);
            return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        }
    }

    filterByTobaccoClass(tobaccoClass: string, skipEnterKey?:boolean) {
        this.tobaccoClassTypeFilter.clear();
        if(skipEnterKey) {
            return this.tobaccoClassTypeFilter.sendKeys(tobaccoClass);
        } else {
            this.tobaccoClassTypeFilter.sendKeys(tobaccoClass);
            this.tobaccoClassTypeFilter.sendKeys(protractor.Key.ENTER);
            browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
            browser.sleep(1000);
            return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        }
    }

    filterByQuarter(quarter: string, skipEnterKey?:boolean) {
        this.quarterFilter.clear();
        if(skipEnterKey) {
            return this.quarterFilter.sendKeys(quarter);
        } else {
            this.quarterFilter.sendKeys(quarter);
            this.quarterFilter.sendKeys(protractor.Key.ENTER);
            browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
            browser.sleep(1000);
            return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        }
    }

    sortByCompanyName() {
        this.companyNameSortingButtonElement.click();
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.sleep(1000);
        return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
}

    clickDeltaExciseTaxLink(index: number) {
        browser.sleep(2000);
        this.deltaExciseTaxLinkElements.get(index).click();
        return new CompareDetailsPage();
    }

    clickMatchedExciseTaxLink(index: number) {
        browser.sleep(2000);
        this.matchedExciseTaxLinkElements.get(index).click();
        return new CompareDetailsPage();
    }

    clickInProgressMatchedExciseTaxLink(index: number) {
        browser.sleep(2000);
        this.inProgressMatchedExciseTaxLinkElements.get(index).click();
        return new CompareDetailsPage();
    }

    
}
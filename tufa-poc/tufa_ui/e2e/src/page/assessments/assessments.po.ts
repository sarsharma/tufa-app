import { by, element, protractor, browser } from "protractor";
import { CreateAnnualTrueUpPage } from "../create-annual-true-up.po";
import { CreateCigarAssessmentPage } from "../create-cigar-assessment.po";
import { CreateQuarterlyAssessmentPage } from "../create-quarterly-assessment.po";
import { QuarterlyAssessmentPage } from "./quarterly-assessment.po";
import { CigarAssessmentPage } from "./cigar-assessment.po";
import { AnnualTrueUpAssessmentPage } from "./annual-trueup-assessment.po";

export class AssessmentsPage {

    EC = protractor.ExpectedConditions;

    /**
     * Selector Constant declarations for this page object
     */
    readonly pageTitleElement = element(by.xpath('//h1[text()="Assessments"]'));
    readonly createQuarterlyAssessmentButton = element(by.css('button#btn-cr-qtr'));
    readonly createCigarAssessmentButton = element(by.css('button#create-cigar'));
    readonly createAnnualTrueUpButton = element(by.css('button#create-annual'));
    readonly quarterlyAssessmentsTab = element(by.css('a#tab-quarter-link'));
    readonly cigarAssessmentsTab = element(by.css('a#tab-cigar-link'));
    readonly annualTrueUpTab = element(by.css('a#tab-annual-link'));
    readonly quarterlyAssessmentPageLink = element(by.xpath('//a[contains(@href, "assessments/quarterly/0")]'));
    readonly cigarAssessmentPageLink = element(by.xpath('//a[contains(@href, "assessments/cigar/0")]'));
    readonly annualTrueUpPageLink = element(by.xpath('//a[contains(@href, "assessments/annual/0")]'));
    readonly annualTrueUpPageExpandElement = element(by.xpath('//div[@id="tab3"]//a[text()="25"]/parent::li/a'));
    readonly loadingQAElement = element(by.xpath('//div[contains(text(),"Loading Quarterly Assessments...")]'));
    readonly loadingCAElement = element(by.xpath('//div[contains(text(),"Loading Cigar Assessments...")]'));
    readonly loadingATAElement = element(by.xpath('//div[contains(text(),"Loading Annual True-Up Assessments...")]'));
    readonly annualTrueUpActiveLink = element(by.xpath('//a[@class="nav-link active"]/span[text()="Annual True-Up"]'));
    readonly fiscalYearTableElements = element.all(by.xpath('//div[@id="tab1"]//tbody/tr/td[1]'));
    readonly cigarFiscalYearTableElements = element.all(by.xpath('//div[@id="tab2"]//tbody/tr/td[1]/a'));
    readonly quarterTableElements = element.all(by.xpath('//div[@id="tab1"]//tbody/tr/td[2]'));
    readonly monthsTableElements = element.all(by.xpath('//div[@id="tab1"]//tbody/tr/td[3]'));
    readonly submissionStatusTableElements = element.all(by.xpath('//div[@id="tab1"]//tbody/tr/td[4]/a'));
    readonly quarterTableFirstFourElements = element.all(by.xpath('(//div[@id="tab1"]//tbody/tr/td[2])[position()<5]'));
    readonly submittedStatusFilterCheckboxElement = element(by.css('label[for="0_status"]'));
    readonly notSubmittedStatusFilterCheckboxElement = element(by.css('label[for="1_status"]'));
    readonly cigarSubmittedStatusCheckboxElement = element(by.css('label[for="3_cigar_assess_status"]'));
    readonly cigarNotSubmittedStatusCheckboxElement = element(by.css('label[for="4_cigar_assess_status"]'))
    readonly q1FilterCheckboxElement = element(by.css('input[value="1"]'));
    readonly secondlatestYearFilterCheckboxElement = element(by.css('label[for="2_year"]'));
    readonly thirdlatestYearFilterCheckboxElement = element(by.css('label[for="3_year"]'));
    readonly submittedElements = element.all(by.xpath('//a[text()="Submitted"]'));
    readonly notSubmittedElements = element.all(by.xpath('//a[text()="Not Submitted"]'));    
    readonly cigarSubmittedElements = element.all(by.xpath('//div[@id="tab2"]//td[contains(text(),"Submitted (")]'));
    readonly cigarNotSubmittedElements = element.all(by.xpath('//div[@id="tab2"]//td[contains(text(),"Not Submitted")]'));    

    waitForLoadingAssessment(){
        browser.wait(this.EC.invisibilityOf(this.loadingQAElement));
        browser.wait(this.EC.invisibilityOf(this.loadingCAElement));
        browser.wait(this.EC.invisibilityOf(this.loadingATAElement));
    }

    clickCreateQuarterlyAssessmentButton() {
        this.createQuarterlyAssessmentButton.click();
        return new CreateQuarterlyAssessmentPage();
    }

    clickCreateCigarAssessmentButton() {
        this.createCigarAssessmentButton.click();
        return new CreateCigarAssessmentPage();
    }

    clickCreateAnnualTrueUp() {
        this.createAnnualTrueUpButton.click();
        return new CreateAnnualTrueUpPage();
    }

    clickCigarAssessmentTab() {
        browser.wait(this.EC.invisibilityOf(this.loadingQAElement));
        this.cigarAssessmentsTab.click();
        return browser.wait(this.EC.invisibilityOf(this.loadingCAElement));
    }
    
    clickQuarterlyAssessmentPageLink() {
        this.quarterlyAssessmentPageLink.click();
        // return new QuarterlyAssessmentPage();
    }

    clickCigarAssessmentPageLink() {
        this.cigarAssessmentPageLink.click();
        // return new CigarAssessmentPage();
    }

    clickAnnualTrueUpPageLink() {
        this.annualTrueUpPageLink.click();
        // return new AnnualtTrueUpPage();
    }

    clickAnnualTrueUpPageExpand() {
        browser.sleep(1000);
        browser.wait(this.EC.elementToBeClickable(this.annualTrueUpPageExpandElement));
        return this.annualTrueUpPageExpandElement.click();
    }

    clickOnYear(fiscalYear: string){
        return element(by.xpath('//tr[contains(.,"'+ fiscalYear +'")]/td[1]/a')).click();
    }
    
    clickAnnualTrueUp(){
        browser.wait(this.EC.elementToBeClickable(this.annualTrueUpTab));
        this.annualTrueUpTab.click();
        browser.wait(this.EC.visibilityOf(this.annualTrueUpActiveLink));
    }
    
    clickAssesmentTableLink(index: number) {
        this.submissionStatusTableElements.get(index).click();
        return new QuarterlyAssessmentPage();
    }

    clickQuarterlyAssessmentTableLinkByYear(year: string) {
        element(by.xpath('//div[@id="tab1"]//tbody/tr/td[1][text()="'+ year +'"]/ancestor::tr//td[4]/a')).click();
        return new QuarterlyAssessmentPage();
    }

    getQuarterlySubmissionStatusByYear(year: string) {
        browser.wait(this.EC.invisibilityOf(this.loadingQAElement));
        return element(by.xpath('//div[@id="tab1"]//tbody/tr/td[1][text()="'+ year +'"]/ancestor::tr//td[4]/a')).getText(); 
    }

    clickCigarAssessmentTableLinkByYear(year: string) {
        browser.wait(this.EC.invisibilityOf(this.loadingCAElement));
        element(by.xpath('//tab[@id="tab-cigar"]//tbody/tr/td[1]/a[text()="' + year + '"]')).click();
        return new CigarAssessmentPage();
    }

    getCigarSubmissionStatusByYear(year: string) {
        browser.wait(this.EC.invisibilityOf(this.loadingCAElement));
        return element(by.xpath('//tab[@id="tab-cigar"]//tbody/tr/td[1]/a[text()="' + year + '"]/ancestor::tr//td[2]')).getText(); 
    }

    clickAnnualTrueUpTableLinkByYear(year: string) {
        browser.wait(this.EC.invisibilityOf(this.loadingATAElement));
        element(by.xpath('//div[@id="tab3"]//tbody/tr/td[1]/a[text()="'+ year +'"]')).click();
        //TODO: sleep required for stabilization, should be removed
        browser.sleep(5000);
        return new AnnualTrueUpAssessmentPage();
    }

    getAnnualTrueUpSubmissionStatusByYear(year: string) {
        browser.wait(this.EC.invisibilityOf(this.loadingATAElement));
        return element(by.xpath('//div[@id="tab3"]//tbody/tr/td[1]/a[text()="'+ year +'"]/ancestor::tr//td[2]')).getText(); 
    }
}
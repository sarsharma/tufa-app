import { by, element, protractor, browser } from "protractor";
import { MonthlyReportPage } from "../monthly-report.po";
import { UploadDocumentModalComponent } from "../../component/upload-document-modal.co";

export class CigarAssessmentPage {
    EC = protractor.ExpectedConditions;

    readonly pageTitleElement = element(by.xpath('//h1[text()="Cigar Assessment"]'));
    readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Loading...")]'));
    readonly pleaseWaitMessageElement = element(by.xpath('//div[contains(text(),"Please wait...")]'));
    readonly marketShareLoadingMessageElement = element(by.xpath('//div[contains(text(),"Generating Market Share...")]'));
    readonly submitMarketShareLoadingElement = element(by.xpath('//div[contains(text(),"Submitting Market Share...")]'));
    
    readonly reportStatusButtonElement = element(by.css('a#tab-rpt-link'));
    readonly reconciliationReportButtonElement = element(by.css('a#tab-rec-link'));
    readonly generateAndSubmitButtonElement = element(by.css('a#tab-sub-link'));

    // Report Status Table
    readonly ttbPermitTableElements = element.all(by.xpath('//table[@id="tbl-rpt"]//td[2]'));
    readonly reportStatusTableElements = element.all(by.xpath('//table[@id="tbl-rpt"]//td[4]/a'));
    readonly displayButton25ItemsElement = element(by.xpath('//div[@id="tab1"]//a[text()="25"]/parent::li[contains(@class,"active")]'));

    //Reconciliation Reports Table
    readonly reconStatusSelectElement = element(by.css('select#filter-reconStatus'));
    readonly approvedFilterElement = element(by.css('option[value="Approved"]'));
    readonly notApprovedFilterElement = element(by.css('option[value="Not Approved"]'));
    readonly reconReportTtbPermitTableElements = element.all(by.xpath('(//table[@id="tbl-recon"]//td[2])[position() > 1]'));
    readonly reconReportTableReviewElements = element.all(by.xpath('//table[@id="tbl-recon"]//td[6]/a'));
    readonly sortByTtbPermitElement = element(by.xpath('//a[contains(text(),"TTB PERMIT")]'));

    //Generate and Submit Tab
    readonly q1ExpandButtonElement = element(by.xpath('(//div[@class="panel-title"]//i)[1]'));
    readonly q2ExpandButtonElement = element(by.xpath('(//div[@class="panel-title"]//i)[2]'));
    readonly q3ExpandButtonElement = element(by.xpath('(//div[@class="panel-title"]//i)[3]'));
    readonly q4ExpandButtonElement = element(by.xpath('(//div[@class="panel-title"]//i)[4]'));
    readonly q1GenerateButtonElement = element(by.xpath('(//button[text()="Generate"])[1]'));
    readonly q2GenerateButtonElement = element(by.xpath('(//button[text()="Generate"])[2]'));
    readonly q3GenerateButtonElement = element(by.xpath('(//button[text()="Generate"])[3]'));
    readonly q4GenerateButtonElement = element(by.xpath('(//button[text()="Generate"])[4]'));
    readonly q1AddressChangesOnlyButton = element(by.css('label#lbl-ac-only-0'));
    
    readonly cigarAssessmentSummaryHeaderElement = element(by.xpath('//h2[text()="Cigar Assessment Summary"]'));
    readonly cigarAssessmentGeneratedTextElement = element(by.xpath('//p[contains(text(), "Generated: ")]'));
    readonly generatedDownloadButtonElement = element(by.css('button#btn-save'));
    readonly q1DownloadButtonElement = element(by.xpath('//span[text()="Quarter 1"]/ancestor::accordion-group//button[text()="Download as CSV"][@id="btn-save-q1"]'));
    readonly q2DownloadButtonElement = element(by.xpath('//span[text()="Quarter 2"]/ancestor::accordion-group//button[text()="Download as CSV"][@id="btn-save-q1"]'));
    readonly q3DownloadButtonElement = element(by.xpath('//span[text()="Quarter 3"]/ancestor::accordion-group//button[text()="Download as CSV"][@id="btn-save-q1"]'));
    readonly q4DownloadButtonElement = element(by.xpath('//span[text()="Quarter 4"]/ancestor::accordion-group//button[text()="Download as CSV"][@id="btn-save-q1"]'));
    readonly submitButtonElement = element(by.css('button#btn-mkt-share'));

    readonly submissionDocumentationHeaderElement = element(by.xpath('//h2[text()="Submission Documentation"]'))

    readonly submittedStatusTextElement = element(by.xpath('//span[contains(text(),"Submitted")]'));
    readonly uploadDocumentButtonElement = element(by.css('button#btn-upload'))
    readonly uploadTableElement = element(by.css('table#tb-supprt-docs'));

    readonly breadcrumbLinkElements = element.all(by.xpath('//li[@class="breadcrumb-item"]/a'));

    //Error and warning banners
    readonly actionRequiredWarningElement = element(by.xpath('//b[contains(text(),"ACTION REQUIRED")]'));
    readonly reportsNotApprovedWarningElement = element(by.xpath('//span[contains(text(), "cannot be submitted until all Monthly Reports are Approved")]'))

    uploadModalComponent = new UploadDocumentModalComponent();

    clickReportStatusButton(index: number) {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        this.reportStatusTableElements.get(index).click();
        return new MonthlyReportPage();
    }

    clickBreadcrumbLink(index: number) {
        browser.wait(this.EC.elementToBeClickable(this.breadcrumbLinkElements.get(index)));
        return this.breadcrumbLinkElements.get(index).click();
    }

    clickSortByTtbPermit() {
        return this.sortByTtbPermitElement.click();
    }

    clickReconciliationReportButton() {
        browser.wait(this.EC.elementToBeClickable(this.reconciliationReportButtonElement));
        this.reconciliationReportButtonElement.click();
        return browser.wait(this.EC.elementToBeClickable(this.reconReportTableReviewElements.get(0)));
    }

    selectApprovedFilter() {
        this.reconStatusSelectElement.click();
        browser.wait(this.EC.elementToBeClickable(this.approvedFilterElement));
        return this.approvedFilterElement.click();
    }
    
    selectNotApprovedFilter() {
        this.reconStatusSelectElement.click();
        browser.wait(this.EC.elementToBeClickable(this.notApprovedFilterElement));
        return this.notApprovedFilterElement.click();
    }
    
    clickReconReportTableReview(index: number) {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.wait(this.EC.elementToBeClickable(this.reconReportTableReviewElements.get(index)));
        this.reconReportTableReviewElements.get(index).click();
        //TODO: added sleep to help with stability; if removed, the page will throw application error popup.
        browser.sleep(2000);
        browser.wait(this.EC.visibilityOf(MonthlyReportPage.breadcrumbLinkElement));
        return new MonthlyReportPage();
    }

    clickUploadDocumentButton() {
        this.uploadDocumentButtonElement.click();
        return browser.wait(this.EC.visibilityOf(this.uploadModalComponent.titleElement));
    }

    clickGenerateAndSubmitButton() {
        browser.wait(this.EC.elementToBeClickable(this.generateAndSubmitButtonElement));
        this.generateAndSubmitButtonElement.click();
        browser.wait(this.EC.invisibilityOf(this.marketShareLoadingMessageElement));
        return browser.wait(this.EC.invisibilityOf(this.pleaseWaitMessageElement));
    }

    clickQ1GenerateButton() {
        browser.wait(this.EC.elementToBeClickable(this.q1GenerateButtonElement));
        this.q1GenerateButtonElement.click();
        browser.wait(this.EC.invisibilityOf(this.marketShareLoadingMessageElement));
        return browser.wait(this.EC.visibilityOf(this.cigarAssessmentSummaryHeaderElement));
    }
    
    clickQ2GenerateButton() {
        browser.wait(this.EC.elementToBeClickable(this.q2GenerateButtonElement));
        this.q2GenerateButtonElement.click();
        browser.wait(this.EC.invisibilityOf(this.marketShareLoadingMessageElement));
        return browser.wait(this.EC.visibilityOf(this.cigarAssessmentSummaryHeaderElement));
    }
    
    clickQ3GenerateButton() {
        browser.wait(this.EC.elementToBeClickable(this.q3GenerateButtonElement));
        this.q3GenerateButtonElement.click();
        browser.wait(this.EC.invisibilityOf(this.marketShareLoadingMessageElement));
        return browser.wait(this.EC.visibilityOf(this.cigarAssessmentSummaryHeaderElement));
    }

    clickQ4GenerateButton() {
        browser.wait(this.EC.elementToBeClickable(this.q4GenerateButtonElement));
        this.q4GenerateButtonElement.click();
        browser.wait(this.EC.invisibilityOf(this.marketShareLoadingMessageElement));
        return browser.wait(this.EC.visibilityOf(this.cigarAssessmentSummaryHeaderElement));
    }

    getCigarAssessmentSubmittedTextElement(index: number) {
        return element(by.xpath('//accordion-group[' + index + ']//p[contains(text(), "Submitted: ")]'));
    }

    getCigarAssessmentAuthorTextElement(index: number) {
        return element(by.xpath('//accordion-group[' + index + ']//p[contains(text(), "Author: ")]'));
    }

    clickSubmitButton() {
        browser.wait(this.EC.elementToBeClickable(this.submitButtonElement));
        this.submitButtonElement.click();
        browser.wait(this.EC.invisibilityOf(this.submitMarketShareLoadingElement)); 
        return browser.wait(this.EC.invisibilityOf(this.cigarAssessmentSummaryHeaderElement));
    }
}
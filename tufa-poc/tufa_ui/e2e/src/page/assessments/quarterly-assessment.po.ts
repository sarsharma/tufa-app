import { by, element, protractor, browser } from "protractor";
import { MonthlyReportPage } from "../monthly-report.po";
import { UploadDocumentModalComponent } from "../../component/upload-document-modal.co";

export class QuarterlyAssessmentPage {
    EC = protractor.ExpectedConditions;

    readonly pageTitleElement = element(by.xpath('//h1[text()="Quarterly Assessment"]'));
    readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Loading...")]'));
    readonly marketShareLoadingMessageElement = element(by.xpath('//div[contains(text(),"Generating Market Share...")]'));
    readonly submitMarketShareLoadingElement = element(by.xpath('//div[contains(text(),"Submitting Market Share...")]'));
    
    readonly reportStatusButtonElement = element(by.css('a#tab-rpt-link'));
    readonly reconciliationReportButtonElement = element(by.css('a#tab-rec-link'));
    readonly generateAndSubmitButtonElement = element(by.css('a#tab-sub-link'));

    // Report Status Table
    readonly ttbPermitsTableElements = element.all(by.xpath('//table[@id="tbl-rpt"]//td[2]'))
    readonly firstMonthTableElements = element.all(by.xpath('//table[@id="tbl-rpt"]//td[3]/a'));
    readonly secondMonthTableElements = element.all(by.xpath('//table[@id="tbl-rpt"]//td[4]/a'));
    readonly thirdMonthTableElements = element.all(by.xpath('//table[@id="tbl-rpt"]//td[5]/a'));
    readonly displayButton25ItemsElement = element(by.xpath('//div[@id="tab1"]//a[text()="25"]/parent::li[contains(@class,"active")]'));

    //Reconciliation Reports Table
    readonly reconStatusSelectElement = element(by.css('select#filter-reconStatus'));
    readonly approvedFilterElement = element(by.css('option[value="Approved"]'));
    readonly notApprovedFilterElement = element(by.css('option[value="Not Approved"]'));
    readonly reconReportTtbPermitTableElements = element.all(by.xpath('(//table[@id="tbl-recon"]//td[2])[position() > 1]'));
    readonly reconReportTableReviewElements = element.all(by.xpath('//table[@id="tbl-recon"]//td[6]/a'));

    //Generate and Submit Tab
    readonly generateMarketShareButtonElement = element(by.css('button#btn-mkt-share'));
    readonly quarterlyAssessmentSummaryHeaderElement = element(by.xpath('//h2[text()="Quarterly Assessment Summary"]'));
    readonly quarterlyAssessmentGeneratedTextElement = element(by.xpath('//p[contains(text(), "Generated: ")]'));
    readonly quarterlyAssessmentSubmittedTextElement = element(by.xpath('//p[contains(text(), "Submitted: ")]'));
    readonly quarterlyAssessmentAuthorTextElement = element(by.xpath('//p[contains(text(), "Author: ")]'));
    readonly cigarettesDownloadButtonElement = element(by.css('button[aria-label="Download CSV report for Cigarettes"]'));
    readonly chewDownloadButtonElement = element(by.css('button[aria-label="Download CSV report for Chew"]'));
    readonly snuffDownloadButtonElement = element(by.css('button[aria-label="Download CSV report for Snuff"]'));
    readonly pipeDownloadButtonElement = element(by.css('button[aria-label="Download CSV report for Pipe"]'));
    readonly ryoDownloadButtonElement = element(by.css('button[aria-label="Download CSV report for Roll-Your-Own"]'));
    readonly submitButtonElement = element(by.css('button#btn-mkt-share[title="Submit Quarterly Assessment"]'));
    readonly submittedStatusTextElement = element(by.xpath('//span[contains(text(),"Submitted")]'));
    readonly submissionDocumentationHeaderElement = element(by.xpath('//h2[text()="Submission Documentation"]'))
    readonly uploadDocumentButtonElement = element(by.css('button#btn-upload'))
    readonly uploadTableElement = element(by.css('table#tb-supprt-docs'));
    readonly assessmentLatestVersionElement = element(by.xpath('(//select[@id="sel-mkt-ver"]/option)[1]'));

    readonly fiscalYearFieldElement = element(by.css('input#fiscal-year'));
    readonly quarterDropdownElement = element(by.css('select#cal-qtr'));
    readonly q1OptionElement = element(by.css('option[value="Q1 - Oct, Nov, Dec"]'));
    readonly q2OptionElement = element(by.css('option[value="Q2 - Jan, Feb, Mar"]'));
    readonly q3OptionElement = element(by.css('option[value="Q3 - Apr, May, Jun"]'));
    readonly q4OptionElement = element(by.css('option[value="Q4 - Jul, Aug, Sep"]'));
    readonly createButton = element(by.css('button#btn-cr-qtr'));
    readonly fiscalYearRequiredErrorElement = element(by.xpath('//li[contains(text(),"Error: Fiscal Year requires appropriate format")]'));
    readonly quarterRequiredErrorElement = element(by.xpath('//li[contains(text(),"Error: Quarter is required")]'));
    readonly assesmentCreatedSuccessMessageElement = element(by.xpath('//span[text()="Successfully generated the assessment for selected Fiscal Year and Quarter"]'));
    readonly assesmentAlreadyExistingErrorElement = element(by.xpath('//span[contains(text(),"assessment already exists")]'));
    readonly breadcrumbLinkElements = element.all(by.xpath('//li[@class="breadcrumb-item"]/a'));

    //Error and warning banners
    readonly actionRequiredWarningElement = element(by.xpath('//b[contains(text(),"ACTION REQUIRED")]'));
    readonly reportsNotApprovedWarningElement = element(by.xpath('//span[contains(text(), "cannot be submitted until all Monthly Reports are Approved")]'))

    uploadModalComponent = new UploadDocumentModalComponent();

    clickFirstMonthButton(index: number) {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        //TODO: remove sleep time, inconsistent behavior is this is not used.
        browser.sleep(3000);
        this.firstMonthTableElements.get(index).click();
        return new MonthlyReportPage();
    }

    clickSecondMonthButton(index: number) {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        this.secondMonthTableElements.get(index).click();
        return new MonthlyReportPage();
    }
    
    clickThirdMonthButton(index: number) {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        this.thirdMonthTableElements.get(index).click();
        return new MonthlyReportPage();
    }

    clickBreadcrumbLink(index: number) {
        browser.wait(this.EC.elementToBeClickable(this.breadcrumbLinkElements.get(index)));
        return this.breadcrumbLinkElements.get(index).click();
    }

    clickReconciliationReportButton() {
        browser.wait(this.EC.elementToBeClickable(this.reconciliationReportButtonElement));
        this.reconciliationReportButtonElement.click();
        return browser.wait(this.EC.elementToBeClickable(this.reconReportTableReviewElements.get(0)));
    }

    selectApprovedFilter() {
        this.reconStatusSelectElement.click();
        browser.wait(this.EC.elementToBeClickable(this.approvedFilterElement));
        return this.approvedFilterElement.click();
    }
    
    selectNotApprovedFilter() {
        this.reconStatusSelectElement.click();
        browser.wait(this.EC.elementToBeClickable(this.notApprovedFilterElement));
        return this.notApprovedFilterElement.click();
    }
    
    clickReconReportTableReview(index: number) {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.wait(this.EC.elementToBeClickable(this.reconReportTableReviewElements.get(index)));
        this.reconReportTableReviewElements.get(index).click();
        //TODO: added sleep to help with stability; if removed, the page will throw application error popup.
        browser.sleep(2000);
        browser.wait(this.EC.visibilityOf(MonthlyReportPage.breadcrumbLinkElement));
        return new MonthlyReportPage();
    }

    clickUploadDocumentButton() {
        this.uploadDocumentButtonElement.click();
        return browser.wait(this.EC.visibilityOf(this.uploadModalComponent.titleElement));
    }

    clickGenerateAndSubmitButton() {
        browser.wait(this.EC.elementToBeClickable(this.generateAndSubmitButtonElement));
        this.generateAndSubmitButtonElement.click();
        return browser.wait(this.EC.invisibilityOf(this.marketShareLoadingMessageElement));
    }
    
    clickGenerateMarketShareButton() {
        browser.wait(this.EC.elementToBeClickable(this.generateMarketShareButtonElement));
        this.generateMarketShareButtonElement.click();
        browser.wait(this.EC.invisibilityOf(this.marketShareLoadingMessageElement));
        return browser.wait(this.EC.visibilityOf(this.quarterlyAssessmentSummaryHeaderElement));
    }

    clickSubmitButton() {
        browser.wait(this.EC.elementToBeClickable(this.submitButtonElement));
        this.submitButtonElement.click();
        browser.wait(this.EC.invisibilityOf(this.submitMarketShareLoadingElement)); 
        return browser.wait(this.EC.visibilityOf(this.submissionDocumentationHeaderElement));
    }
}
import { browser, by, element, protractor } from 'protractor';

/**
 * Page Object representing Daily Operations page.
 */
export class CompanyDetailPage {
  EC = protractor.ExpectedConditions;
  
  /**
   * Selector Constant declarations for this page object
   */
  static readonly pageTitleElement = element(by.xpath('//h1[text()="Company Details"]'));
  readonly companyDetailsPageTitleElement = element(by.xpath('//h1[text()="Company Details"]'));
  readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Loading...")]'));
  readonly excludePanelElement = element(by.xpath('//*[@id="accordion"]/div/div[1]/h4/a'));
  readonly assessmentFYElement = element(by.xpath('//*[@id="year"]'));
  readonly assessmentQuarterExcludedQ1 = element(by.css('toggle-button-tufa[title="quarter 1"]'));
  readonly commentsExcludeElement = element(by.xpath('//*[@id="commentdesc"]'));
  readonly saveExcludeElement = element(by.css('button[title="Save Permit Information"]'));

  readonly companyNameElement = element(by.xpath('//*[@id="cmp-name"]'));
  readonly einElement = element(by.xpath('//*[@id="cmp-ein"]/span'));
  readonly createdElement = element(by.xpath('//*[@id="cmp-dtcr"]/span'));
  readonly statusElement = element(by.xpath('//*[@id="cmp-sts"]/span'));

  readonly editCompanyElement = element(by.xpath('//*[@id="btn-ed-comp"]'));
  readonly editCompanyTitleElement = element(by.xpath('//*[@id="edit-comp-title"]'));
  readonly editCompanyNameElement = element(by.xpath('//*[@id="company-name"]'));
  readonly editEINElement = element(by.xpath('//*[@id="ein-number"]'));
  readonly editCompanyStatusElement = element(by.xpath('//*[@id="cmp-status"]/option[@value = "ACTV"]'));
  readonly saveCompanyPopUpElement = element(by.xpath('//*[@id="upd-cmp"]'));
  readonly cancelCompanyPopUpElement = element(by.xpath('//*[@id="edit-company"]/div/div/div[4]/button[2]'));

  readonly commentsElement = element(by.css('textarea#ta-comm-'));
  readonly saveCommentsElement = element(by.xpath('//*[@id="btn-comment"]'));
  readonly savingMessageElement = element(by.xpath('//div[contains(text(),"Saving...")]'));
  readonly pleaseWaitMessageElement = element(by.xpath('//div[contains(text(),"Please wait...")]'));

  readonly permitNumberAscElement = element.all(by.css('th[id="h-pmt-no"] span[class="fa fa-caret-up ng-star-inserted"]'));
  readonly permitTableHeaderElements =  element.all(by.css('table[summary="Company Permits"] th a'));

  readonly editPrimaryAddressElement = element(by.xpath('//*[@id="ed-addr1"]'));
  readonly editAlternateAddressElement = element(by.xpath('//*[@id="ed-addr2"]'));
  readonly editCompanyAddressTitleElement = element(by.xpath('//*[@id="edit-address-title"]'));
  readonly editAttentionCompanyAddressElement = element(by.xpath('//*[@id="attention"]'));
  readonly editStreetAddressCompanyAddressElement = element(by.xpath('//*[@id="street-address"]'));
  readonly editSuiteCompanyAddressElement = element(by.xpath('//*[@id="suite"]'));
  readonly editCityCompanyAddressElement = element(by.xpath('//*[@id="city"]'));
  readonly editStateCompanyAddressElement = element(by.xpath('//*[@id="state"]'));
  readonly editPostalCodeCompanyAddressElement = element(by.xpath('//*[@id="postalCode"]'));
  readonly editCountryCompanyAddressElement = element(by.xpath('//*[@id="country"]'));
  readonly saveCompanyAddressElement = element(by.xpath('//*[@id="btn-save-addr"]'));
  readonly cancelCompanyAddressElement = element(by.xpath('//*[@id="edit-address"]/div/div/div[3]/button[2]'));
  
  readonly selectUnitedStatesElement = element(by.xpath('//option[@value="UNITED STATES"]'));
  readonly selectNoCityElement = element(by.xpath('//*[@id="state"]/option[1]'));

  readonly addContactButtonElement = element(by.xpath('//*[@id="btn-add-ct"]'))

  readonly firstNameContactElement = element(by.xpath('//*[@id="add-first-name"]'));
  readonly lastNameContactElement = element(by.xpath('//*[@id="add-last-name"]'));
  readonly emailContactElement = element(by.xpath('//*[@id="add-email-address"]'));
  readonly countryCodeContactElement = element(by.xpath('//*[@id="phone-countrycode"]'));
  readonly phoneNumbercontactElement = element(by.xpath('//*[@id="add-phone-number"]'));
  readonly saveContactButtonElement = element(by.xpath('//*[@id="btn-save"]'));
  readonly cancelcontactButtonElement = element(by.xpath('//*[@id="addcontactform"]/div[2]/button[2]'));
  readonly emailContacErrortElement = element(by.xpath('//*[@id="add-email-address"]/following-sibling::ul/li[@class="parsley-custom-error-message"]'));
  
  readonly deleteContactButtonElement = element.all(by.xpath('//button[text()="Delete "]'));
  readonly deleteContactPopUpElement = element(by.xpath('//div[text()="Delete Contact"]'));
  readonly confirmButtonDeleteContactPopUpElement = element(by.xpath('//*[@id="btn-del-doc"]'));
  readonly cancelButtonDeleteContactPopUpElement = element(by.xpath('//*[@id="confirm-popup"]/div/div/div[3]/button[2]'));

  readonly editContactButtonElement = element.all(by.xpath('//button[text()="Delete "]/following-sibling::button'));
  readonly editContactTitleElement = element(by.xpath('//*[@id="edit-contact-title"]'));
  readonly firstNameEditContactElement = element(by.xpath('//*[@id="first-name"]'));
  readonly lastNameEditContactElement = element(by.xpath('//*[@id="last-name"]'));
  readonly saveButtonEditPOCElement = element(by.xpath('//button[text()="Save Contact"]'));
  readonly applicationErrorElement = element(by.xpath('//*[@id="error-message-modal"]/div/div/div[3]/button'));
  readonly breadcrumbLinkElements = element.all(by.xpath('//li[@class="breadcrumb-item"]/a'));

  getEditFirstNamePOC(){
    browser.wait(this.EC.visibilityOf(this.firstNameEditContactElement));
    browser.wait(this.EC.presenceOf(this.firstNameEditContactElement));
    return this.firstNameEditContactElement;
  }

  waitForAddContactPOC(){
    browser.wait(this.EC.elementToBeClickable(this.addContactButtonElement));
  }

  waitForDeleteContactPOC(){
    browser.wait(this.EC.elementToBeClickable(this.deleteContactButtonElement.get(1)));
  }

  getDeleteContactPopUpElement(){
    browser.wait(this.EC.visibilityOf(this.deleteContactPopUpElement));
    browser.wait(this.EC.elementToBeClickable(this.confirmButtonDeleteContactPopUpElement));
    browser.wait(this.EC.elementToBeClickable(this.cancelButtonDeleteContactPopUpElement));
    return this.deleteContactPopUpElement
  }

  getSaveContactButtonElement(){
    browser.wait(this.EC.visibilityOf(this.saveContactButtonElement));
    return this.saveContactButtonElement
  }

  getEditCompanyAddressTitle(){
    browser.wait(this.EC.visibilityOf(this.editCompanyAddressTitleElement));
    return this.editCompanyAddressTitleElement
  }

  setComments(value: string){
    browser.wait(this.EC.presenceOf(this.commentsElement));
    this.commentsElement.clear();
    browser.sleep(1000);
    this.commentsElement.sendKeys(value);
  }

  setLongComments(value: string){
    browser.wait(this.EC.presenceOf(this.commentsElement));
    this.commentsElement.sendKeys(value);
    for(var i = 0; i < 10; i++){
      this.commentsElement.sendKeys(value);
    } 
  }

  clickSaveComments(){
    this.saveCommentsElement.click();
    browser.wait(this.EC.invisibilityOf(this.savingMessageElement));
  }

  setCompanyName(value: string){
    browser.wait(this.EC.presenceOf(this.editCompanyNameElement));
    this.editCompanyNameElement.click();
    this.editCompanyNameElement.clear();
    browser.sleep(1000);
    this.editCompanyNameElement.sendKeys(value);
  }

  setCompanyEIN(value: string){
    this.editEINElement.click();
    this.editEINElement.clear();
    this.editEINElement.sendKeys(value);
  }

  clickEditCompanyPopUp(){
    this.editCompanyElement.click();
    browser.wait(this.EC.visibilityOf(this.editCompanyTitleElement));
  }

  getCompanyDetailPageTitle(){
    browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
    browser.wait(this.EC.visibilityOf(this.companyDetailsPageTitleElement));
    return this.companyDetailsPageTitleElement.getText();
  } 

  clickonPermit(permitNum: string){
    browser.wait(this.EC.elementToBeClickable(element(by.linkText(permitNum))));
    element(by.linkText(permitNum)).click();
  }

  getPermitStatus(permitNum: string){
    return element(by.xpath('//tr[contains(.,"'+permitNum+'")]/td[2]')).getText();
  }

  getTufaInactiveDate(permitNum: string){
    return element(by.xpath('//tr[contains(.,"'+permitNum+'")]/td[4]')).getText();
  }

  getTTBClosedDate(permitNum: string){
    return element(by.xpath('//tr[contains(.,"'+permitNum+'")]/td[6]')).getText();
  }

  getSelectPermitNumberDropdown(permitNum: string){
    browser.wait(this.EC.visibilityOf(element(by.xpath('//option[text()=" '+permitNum+' "]'))));
    return element(by.xpath('//option[text()=" '+permitNum+' "]'));
  }

  waitForLoading() {
    browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  waitForPleaseWait() {
    browser.wait(this.EC.invisibilityOf(this.pleaseWaitMessageElement));
    browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  waitForLoadingCompany(companyName: string) {
    browser.wait(this.EC.visibilityOf(element(by.xpath('//h2[text() = "'+companyName+'"]'))));
  }

  getPermit(permitNumber: string){
    browser.wait(this.EC.visibilityOf(element(by.xpath('//a[text()="'+permitNumber+'"]'))));
    return element(by.xpath('//a[text()="'+permitNumber+'"]'));
  }

  getPermitCancel(permitNumber: string){
    return element(by.xpath('//a[text()="'+permitNumber+'"]'));
  }

  getPermitExcludeStatus(permitNumber: string){
    browser.wait(this.EC.visibilityOf(element(by.xpath('//a[text()="'+permitNumber+'"]/following-sibling::span'))));
    return element(by.xpath('//a[text()="'+permitNumber+'"]/following-sibling::span'));
  }

    clickBreadcrumbLink(index: number) {
      //TODO: added sleep to help with stability; if removed, the page will throw application error popup.
      browser.sleep(3000);
      browser.wait(this.EC.visibilityOf(this.breadcrumbLinkElements.get(index)));
      browser.wait(this.EC.elementToBeClickable(this.breadcrumbLinkElements.get(index)));
      return this.breadcrumbLinkElements.get(index).click();
  }
}

import { by, element, protractor, browser } from "protractor";
import { MarketShareReadyModalComponent } from "../component/assessments/market-share-ready-modal.co";
import { MonthlyReportPage } from "./monthly-report.po";
import { MixedActionModalComponent } from "../component/assessments/mixed-action-modal.co";

export class CompareDetailsPage {
    EC = protractor.ExpectedConditions;

    readonly pageBreadCrumbElement = element(by.xpath('//a[contains(text(),"Compare Details")]'));
    readonly annualTrueUpBreadCrumbElement = element(by.xpath('//a[contains(text(),"Annual True-Up")]'));
    
    readonly loadingIconElement = element(by.css('div.ng-busy-default-spinner'));
    readonly alertDialogElement = element(by.css('div#toast-container'));
    readonly companyTitleElement = element(by.css('h2#cmp-name'));
    readonly companyInfoFormElement = element(by.xpath('//form[@id="company-info-form"]'));
    readonly companyInfoExpandButtonElement = element(by.xpath('//form[@id="company-info-form"]//a[@aria-controls="#collapseCompanyPanel"]'));

    readonly tobaccoBarCheckboxElements = element.all(by.xpath('//app-tobacco-button-bar//label'));
    readonly cigarettesCheckboxLabelElement = element(by.xpath('//label[@for="Cigarettes-check"]'));
    readonly cigarsCheckboxLabelElement = element(by.xpath('//label[@for="Cigars-check"]'));
    readonly chewSnuffCheckboxLabelElement = element(by.xpath('//label[@for="Chew/Snuff-check"]'));
    readonly pipeRyoCheckboxLabelElement = element(by.xpath('//label[@for="Pipe/Roll Your Own-check"]'));
    readonly chewCheckboxLabelElement = element(by.xpath('//label[@for="Chew-check"]'));
    readonly pipeCheckboxLabelElement = element(by.xpath('//label[@for="Pipe-check"]'));
    readonly snuffCheckboxLabelElement = element(by.xpath('//label[@for="Snuff-check"]'));
    readonly ryoCheckboxLabelElement = element(by.xpath('//label[@for="Roll Your Own-check"]'));

    readonly cigarettesCheckboxElement = element(by.xpath('//label[@for="Cigarettes-check"]/input'));
    readonly cigarsCheckboxElement = element(by.xpath('//label[@for="Cigars-check"]/input'));
    readonly chewSnuffCheckboxElement = element(by.xpath('//label[@for="Chew/Snuff-check"]/input'));
    readonly pipeRyoCheckboxElement = element(by.xpath('//label[@for="Pipe/Roll Your Own-check"]/input'));
    readonly chewCheckboxElement = element(by.xpath('//label[@for="Chew-check"]/input'))
    readonly pipeCheckboxElement = element(by.xpath('//label[@for="Pipe-check"]/input'));
    readonly snuffCheckboxElement = element(by.xpath('//label[@for="Snuff-check"]/input'));
    readonly ryoCheckboxElement = element(by.xpath('//label[@for="Roll Your Own-check"]/input'));

    readonly quarterOneHeaderElement = element(by.xpath('//a[contains(text(),"Quarter 1")]'));
    readonly quarterOneDetailsElement = element(by.xpath('//form[@id="compare-details-form-1"]//span[text()="Details"]'));
    readonly quarterOneAmountElement = element(by.xpath('//form[@id="compare-details-form-1"]//a[@id="anchortagingestedsummarydelta"]'));
    readonly quarterOneTableElement = element(by.xpath('//form[@id="compare-details-form-1"]//accordion-group[@id="accordion"]//table'));
    readonly quarterOneAcceptFda3852ButtonElement = element(by.xpath('//form[@id="compare-details-form-1"]//button[@id="btn-acc-fda"]'));
    readonly quarterOneAcceptIngestedButtonElement = element(by.xpath('//form[@id="compare-details-form-1"]//button[@id="btn-acc-ingest"]'));
    readonly quarterOneInProgressButtonElement = element(by.xpath('//form[@id="compare-details-form-1"]//button[contains(text(),"In Progress")]'));
    readonly quarterOneAmendSectionElement = element(by.xpath('//manu-details-amend-mini//form')); 

    readonly quarterTwoHeaderElement = element(by.xpath('//a[contains(text(),"Quarter 2")]'));
    readonly quarterTwoDetailsElement = element(by.xpath('//form[@id="compare-details-form-2"]//span[text()="Details"]'));
    readonly quarterTwoAmountElement = element(by.xpath('//form[@id="compare-details-form-2"]//a[@id="anchortagingestedsummarydelta"]'));
    readonly quarterTwoTableElement = element(by.xpath('//form[@id="compare-details-form-2"]//accordion-group[@id="accordion"]//table'));
    readonly quarterTwoExcludeButtonElement = element(by.xpath('//form[@id="compare-details-form-2"]//button[@id="btn-exclude-2"]'));
    readonly quarterTwoMarketShareReadyButtonElement = element(by.xpath('//form[@id="compare-details-form-2"]//button[@id="btn-ms-ready-2"]'));
    readonly quarterTwoInProgressButtonElement = element(by.xpath('//form[@id="compare-details-form-2"]//button[@id="btn-association-iprg-2"]'));
    readonly quarterTwoContinueButtonElement = element(by.xpath('//form[@id="compare-details-form-2"]//button[@id="btn-continue-2"]'));

    readonly quarterThreeHeaderElement = element(by.xpath('//a[contains(text(),"Quarter 3")]'));
    readonly quarterThreeDetailsElement = element(by.xpath('//form[@id="compare-details-form-3"]//span[text()="Details"]'));
    readonly quarterThreeAmountElement = element(by.xpath('//form[@id="compare-details-form-3"]//a[@id="anchortagingestedsummarydelta"]'));
    readonly quarterThreeTableElement = element(by.xpath('//form[@id="compare-details-form-3"]//accordion-group[@id="accordion"]//table'));
    readonly quarterThreeExcludeButtonElement = element(by.xpath('//form[@id="compare-details-form-3"]//button[@id="btn-exclude-3"]'));
    readonly quarterThreeMarketShareReadyButtonElement = element(by.xpath('//form[@id="compare-details-form-3"]//button[@id="btn-ms-ready-3"]'));
    readonly quarterThreeInProgressButtonElement = element(by.xpath('//form[@id="compare-details-form-3"]//button[@id="btn-association-iprg-3"]'));
    readonly quarterThreeContinueButtonElement = element(by.xpath('//form[@id="compare-details-form-3"]//button[@id="btn-continue-3"]'));

    readonly quarterFourHeaderElement = element(by.xpath('//a[contains(text(),"Quarter 4")]'));
    readonly quarterFourDetailsElement = element(by.xpath('//form[@id="compare-details-form-4"]//span[text()="Details"]'));
    readonly quarterFourAmountElement = element(by.xpath('//form[@id="compare-details-form-4"]//a[@id="anchortagingestedsummarydelta"]'));
    readonly quarterFourTableElement = element(by.xpath('//form[@id="compare-details-form-4"]//accordion-group[@id="accordion"]//table'));
    readonly quarterFourExcludeButtonElement = element(by.xpath('//form[@id="compare-details-form-4"]//button[@id="btn-exclude-4"]'));
    readonly quarterFourMarketShareReadyButtonElement = element(by.xpath('//form[@id="compare-details-form-4"]//button[@id="btn-ms-ready-4"]'));
    readonly quarterFourInProgressButtonElement = element(by.xpath('//form[@id="compare-details-form-4"]//button[@id="btn-association-iprg-4"]'));
    readonly quarterFourContinueButtonElement = element(by.xpath('//form[@id="compare-details-form-4"]//button[@id="btn-continue-4"]'));

    readonly associationButtonElements = element.all(by.xpath('//button[contains(@id,"btn-association-")]'));
    readonly excludeButtonElements = element.all(by.xpath('//button[contains(@id,"btn-exclude-")]'));
    readonly marketShareReadyButtonElements = element.all(by.xpath('//button[contains(@id,"btn-ms-ready-")]'));
    readonly inProgressButtonElements = element.all(by.xpath('//button[contains(@id,"btn-association-iprg-")]'));
    readonly continueButtonElements = element.all(by.xpath('//button[contains(@id,"btn-continue-")]'));

    readonly acceptFda3852ButtonElements = element.all(by.xpath('//button[contains(@id,"btn-acc-fda")]'));
    readonly acceptIngestedButtonElements = element.all(by.xpath('//button[contains(@id,"btn-acc-ingest")]'));
    readonly acceptMixedActionButtonElements = element.all(by.xpath('//button[contains(@id,"btn-mixed-action")]'));
    readonly matchedInProgressButtonElements = element.all(by.xpath('//button[contains(text(),"In Progress")]')); 
    readonly matchedContinueButtonElements = element.all(by.xpath('//button[contains(@id,"btn-in-continue")]'));

    readonly cigarQ1TableElement = element(by.xpath('//div[@id="collapse-1"]//accordion-group'));
    readonly cigarQ2TableElement = element(by.xpath('//div[@id="collapse-2"]//accordion-group'));
    readonly cigarQ3TableElement = element(by.xpath('//div[@id="collapse-3"]//accordion-group'));
    readonly cigarQ4TableElement = element(by.xpath('//div[@id="collapse-4"]//accordion-group'));
    readonly cigarQ1Through4DetailsSectionElement = element(by.xpath('//a[text()=" Quarter 1-4 "]/ancestor::form//div[contains(@id,"accordionDetailsPanel")]'));

    readonly marketShareReadyQ1ButtonElement = element(by.css('button#btn-ms-ready-1'));
    readonly marketShareReadyQ3ButtonElement = element(by.css('button#btn-ms-ready-3'));
    readonly marketShareReadyQ4ButtonElement = element(by.css('button#btn-ms-ready-4'));
    readonly inProgressQ1ButtonElement = element(by.css('button#btn-association-iprg-1')); 
    readonly inProgressQ3ButtonElement = element(by.css('button#btn-association-iprg-3')); 
    readonly inProgressQ4ButtonElement = element(by.css('button#btn-association-iprg-4')); 

    readonly q1PinkCheckIconElement = element(by.xpath('//form[@id="compare-details-form-1"]//i[@class="fa fa-check-circle text-fun"]'));
    readonly q3PinkCheckIconElement = element(by.xpath('//form[@id="compare-details-form-3"]//i[@class="fa fa-check-circle text-fun"]'));
    readonly q4PinkCheckIconElement = element(by.xpath('//form[@id="compare-details-form-4"]//i[@class="fa fa-check-circle text-fun"]'));
    readonly wrenchIconElement = element(by.xpath('//i[contains(@class, "fa-wrench")]'));
    readonly commentsSectionElement = element(by.xpath('//span[text()="Comments"]/ancestor::section'));

    msReadyModal = new MarketShareReadyModalComponent();
    mixedActionModal = new MixedActionModalComponent();
    
    waitForLoading() {
        browser.wait(this.EC.invisibilityOf(this.loadingIconElement));
        browser.sleep(1500);
        browser.wait(this.EC.invisibilityOf(this.loadingIconElement));
        browser.sleep(1500);
        browser.wait(this.EC.invisibilityOf(this.loadingIconElement));
        return browser.wait(this.EC.invisibilityOf(this.alertDialogElement));
    }

    clickQuarterOneHeader() {
        browser.wait(this.EC.elementToBeClickable(this.quarterOneHeaderElement), 10000);
        this.quarterOneHeaderElement.click();        
        return browser.wait(this.EC.visibilityOf(this.quarterOneTableElement), 10000);
    }

    clickQuarterTwoHeader() {
        browser.wait(this.EC.elementToBeClickable(this.quarterTwoHeaderElement), 10000);
        this.quarterTwoHeaderElement.click();
        return browser.wait(this.EC.visibilityOf(this.quarterTwoTableElement), 10000);
    }

    clickQuarterThreeHeader() {
        browser.wait(this.EC.elementToBeClickable(this.quarterThreeHeaderElement), 10000);
        this.quarterThreeHeaderElement.click();
        return browser.wait(this.EC.visibilityOf(this.quarterThreeTableElement), 10000);
    }

    clickQuarterFourHeader() {
        browser.wait(this.EC.elementToBeClickable(this.quarterFourHeaderElement), 10000);
        this.quarterFourHeaderElement.click();
        return browser.wait(this.EC.visibilityOf(this.quarterFourTableElement), 10000);
    }

    clickMarketShareReadyQ1Button() {
        browser.wait(this.EC.elementToBeClickable(this.marketShareReadyQ1ButtonElement), 10000);
        this.marketShareReadyQ1ButtonElement.click();
        return this.waitForLoading();
    }

    clickInProgressQ1Button() {
        browser.wait(this.EC.elementToBeClickable(this.inProgressQ1ButtonElement), 10000);
        this.inProgressQ1ButtonElement.click();
        return this.waitForLoading();
    }

    clickInProgressQ3Button() {
        browser.wait(this.EC.elementToBeClickable(this.inProgressQ3ButtonElement), 10000);
        this.inProgressQ3ButtonElement.click();
        return this.waitForLoading();
    }

    clickInProgressQ4Button() {
        browser.wait(this.EC.elementToBeClickable(this.inProgressQ4ButtonElement),10000);
        this.inProgressQ4ButtonElement.click();
        return this.waitForLoading();
    }

    clickMarketShareReadyQ3Button() {
        browser.wait(this.EC.elementToBeClickable(this.marketShareReadyQ3ButtonElement), 10000);
        this.marketShareReadyQ3ButtonElement.click();
        return this.waitForLoading();
    }
    clickMarketShareReadyQ4Button() {
        browser.wait(this.EC.elementToBeClickable(this.marketShareReadyQ4ButtonElement), 10000);
        this.marketShareReadyQ4ButtonElement.click();
        return this.waitForLoading();
    }

    clickMonthAccordionExpand(month: string) {
        browser.wait(this.EC.elementToBeClickable(element(by.xpath('(//span[contains(text(),"' + month + '")])[1]/parent::div/span/i'))), 10000);
        return element(by.xpath('(//span[contains(text(),"' + month + '")])[1]/parent::div/span/i')).click();
    }

    clickMonthlyReportLink(month: string) {
        browser.wait(this.EC.elementToBeClickable(element(by.xpath('//a[contains(text(),"' + month + '")]'))), 10000);
        element(by.xpath('//a[contains(text(),"' + month + '")]')).click();
        browser.sleep(3000);
        return new MonthlyReportPage();
    }
   
}
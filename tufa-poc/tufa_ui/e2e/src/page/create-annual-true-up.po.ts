import { by, element, protractor } from "protractor";

export class CreateAnnualTrueUpPage {

    EC = protractor.ExpectedConditions;

    static readonly pageTitelElement = element(by.xpath('//h1[text()="Create Annual True-Up"]'));
    readonly fiscalYearFieldElement = element(by.css('input#fiscal-year'));
    readonly createButton = element(by.css('button#btn-cr-tu'));
}
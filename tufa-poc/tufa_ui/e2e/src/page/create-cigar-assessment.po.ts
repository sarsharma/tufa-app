import { by, element, protractor, browser } from "protractor";

export class CreateCigarAssessmentPage {

    EC = protractor.ExpectedConditions;

    static readonly pageTitleElement = element(by.xpath('//h1[text()="Create Cigar Assessment"]'));
    readonly fiscalYearFieldElement = element(by.css('input#fiscal-year'));
    readonly createButton = element(by.css('button#btn-cr-tu'));
    readonly fiscalYearRequiredErrorElement = element(by.xpath('//li[contains(text(),"Error: Fiscal Year requires appropriate format")]'));
    readonly assesmentCreatedSuccessMessageElement = element(by.xpath('//span[contains(text(),"Successfully created the")]'));
    readonly assesmentAlreadyExistingErrorElement = element(by.xpath('//span[contains(text(),"ssessment already exists")]'));
    
    clickCreateButton() {
        this.createButton.click();
        return browser.wait(this.EC.or(this.EC.visibilityOf(this.assesmentCreatedSuccessMessageElement)
            ,this.EC.visibilityOf(this.assesmentAlreadyExistingErrorElement)
            ,this.EC.visibilityOf(this.fiscalYearRequiredErrorElement)
            ));
    }
    
}
import { browser, by, element, protractor } from 'protractor';
import { CompanyDetailPage } from './company-detail.po';

/**
 * Page Object representing Daily Operations page.
 */
export class CreateCompanyPage {
  EC = protractor.ExpectedConditions;
  
  /**
   * Selector Constant declarations for this page object
   */
  static readonly pageTitleElement = element(by.xpath('//h1[text()="Create Company"]'));
  readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Saving...")]'));
  readonly companyNameFieldELement = element(by.css('input#company-name'));
  readonly einFieldELement = element(by.css('input#ein-number'));
  readonly ttbPermitFieldELement = element(by.css('input#ttb-permit'));
  readonly typeDropdownElement = element(by.css('#permit-type'));
  readonly selectTypeOptionElement = element(by.xpath('//option[@value="Select Type"]'));
  readonly manufacturerTypeOptionElement = element(by.xpath('//option[@value="Manufacturer"]'));
  readonly importerTypeOptionElement = element(by.xpath('//option[@value="Importer"]'));
  readonly activeDateElement = element(by.css('input#active-date'))
  readonly saveCompanyButtonElement = element(by.css('button#btn-create'));
  readonly companyNameErrorMessageElement = element(by.xpath('//li[contains(text(), "Error: Company Name is required")]'));
  readonly einNumberErrorMessageElement = element(by.xpath('//li[contains(text(), "Error: EIN requires appropriate format")]'));
  readonly existingEinNumberErrorMessageElement = element(by.xpath('//li[@class="parsley-EIN-Exists"]'))
  readonly ttbPermitErrorMessageElement = element(by.xpath('//li[contains(text(), "Error: TTB Permit requires appropriate format")]'));
  readonly typeErrorMessageElement = element(by.xpath('//li[contains(text(), "Error: Type is required")]'));
  readonly activeDateErrorMessageElement = element(by.xpath('//li[contains(text(), "Error: TUFA Active Date is required")]'));

  getCompanyName() {
    return this.companyNameFieldELement.getAttribute('value');
  }

  setCompanyName(companyName: string) {
    return this.companyNameFieldELement.sendKeys(companyName);
  }

  getEin() {
    return this.einFieldELement.getAttribute('value');
  }

  setEin(ein: string) {
    return this.einFieldELement.sendKeys(ein);
  }

  getTtbPermit() {
    return this.ttbPermitFieldELement.getAttribute('value');
  }

  setTtbPermit(ttbPermit: string) {
    return this.ttbPermitFieldELement.sendKeys(ttbPermit);
  }

  selectTypeByValue(value: string) {
    this.typeDropdownElement.click();
    return this.typeDropdownElement.element('./option[@value="' + value + '"]');
  }
  
  selectTypeOption() {
    return this.selectTypeOptionElement.click();
  }

  selectManufacturerTypeOption() {
    return this.manufacturerTypeOptionElement.click();
  }

  selectImporterTypeOption() {
    return this.importerTypeOptionElement.click();
  }

  getActiveDate() {
    return this.activeDateElement.getAttribute('value');
  }
  
  setActiveDate(activeDate: string) {
    this.activeDateElement.sendKeys(activeDate);
    return this.activeDateElement.sendKeys(protractor.Key.ENTER);
  }

  clickSaveCompanyButton() {
    this.saveCompanyButtonElement.click();
    //TODO: below wait statements should not be required if waitForAngularEnabled() is set to true; this is currently not possible
    // due to script timeout happening throughout TUFA application.
    browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
    browser.wait(this.EC.or(
      this.EC.visibilityOf(this.existingEinNumberErrorMessageElement),
      this.EC.visibilityOf(this.einNumberErrorMessageElement),
      this.EC.visibilityOf(CompanyDetailPage.pageTitleElement)
    ));
    //End TODO 
    return new CompanyDetailPage();
  }

  clearAllFields() {
    this.companyNameFieldELement.clear();
    this.einFieldELement.clear();
    this.ttbPermitFieldELement.clear();
    this.selectTypeOption();
    this.activeDateElement.clear(); 
    this.companyNameFieldELement.click();
    return this;
  }

}

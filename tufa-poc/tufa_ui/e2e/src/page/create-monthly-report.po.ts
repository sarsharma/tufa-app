import { by, element, protractor, browser } from 'protractor';

export class CreateMonthlyReportPage {

    EC = protractor.ExpectedConditions;


    /**
    * Selector Constant declarations for this page object
    */
    static readonly pageTitleElement = element(by.xpath('//h1[text()="Create Monthly Reports"]'));
    readonly yearFieldELement = element(by.css('input#fiscal-year'));
    readonly monthDropDownFieldELement = element(by.css('#cal-month'));
    readonly selectTypeOptionElement = element(by.xpath('//option[@value="Month"]'));
    readonly janTypeOptionElement = element(by.xpath('//option[@value="January"]'));
    readonly FebTypeOptionElement = element(by.xpath('//option[@value="February"]'));
    readonly createmonthlyButtonElement = element(by.css('button#btn-cr-mon'));
    readonly successfulCreationMessageElement = element(by.xpath('//span[contains(text(), "Successfully generated")]'));
    readonly alertElement = element(by.css('span.alert-text'));

    getFiscalYear() {
        return this.yearFieldELement.getAttribute('value');
    }

    setFiscalYear(fiscalYear: string) {
        return this.yearFieldELement.sendKeys(fiscalYear);
    }

    selectMonth(month: string) {
        return this.monthDropDownFieldELement.element(by.css('option[value="' + month + '"]')).click();
    }

    selectTypeOption() {
        return this.selectTypeOptionElement.click();
    }

    selectJanuaryTypeOption() {
        return this.janTypeOptionElement.click();
    }

    selectFebruaryTypeOption() {
        return this.FebTypeOptionElement.click();
    }

    clickMonthlyReportButton() {
        this.createmonthlyButtonElement.click();

    }


    clearAllFields() {
        this.yearFieldELement.clear();

        this.selectTypeOption();

        return this;
    }

    getAlertMessage() {
        browser.wait(this.EC.visibilityOf(this.alertElement));
        return this.alertElement.isPresent();
    }


    createMonthlyReport(fiscalYear: string, month: String){
        this.clearAllFields();
        this.setFiscalYear(fiscalYear);
        element(by.xpath('//option[@value="'+month+'"]')).click();
        this.clickMonthlyReportButton();
        return this;
    }

}


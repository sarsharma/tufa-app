import { browser, by, element, protractor } from 'protractor';

export class CreatePermitPage {

    EC = protractor.ExpectedConditions;

    static readonly pageTitleElement = element(by.xpath('//h1[text()="Company Details"]'));
    readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Loading...")]'));
    readonly addPermitElement = element(by.css('button#btn-cr-pmt'));
    readonly TTBPermitElement = element(by.css('input#permit-num'));
    readonly selectTypeManuOptionElement = element(by.xpath('//option[@value="Manufacturer"]'));
    readonly selectTypeImpOptionElement = element(by.xpath('//option[@value="Importer"]'));
    readonly tufaActiveDateElement = element(by.css('input#active-date'));
    readonly savePermitElement = element(by.css('button#btn-create'));
    readonly cancelPermitElement = element(by.xpath('//*[@id="edit-permit"]/div/div/div[3]/div/div/div[2]/button[2]'));
    readonly TTBPermitErrorElement = element(by.xpath('//*[@id="permit-num"]/ancestor::div/ul/li'));
    readonly typeErrorElement = element(by.xpath('//*[@id="permit-type"]/ancestor::div/ul/li'));
    readonly tufaActiveDateErrorElement = element(by.xpath('//*[@id="act-dtpckr"]/div/div[2]/ul/li'));
    readonly warningMessageElement = element(by.xpath('//*[text() ="Warning: Active Permit(s) with the same State and Type already exist for this Company. Do you still wish to save the Permit?"]'));
    
   getWarningMessage() {
        browser.wait(this.EC.visibilityOf(this.warningMessageElement));  
        return this.warningMessageElement;
    }

    clickAddPermitButton() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.wait(this.EC.elementToBeClickable(this.addPermitElement), 5000);   
        this.addPermitElement.click();
    }

    setTTBPermit(fiscalYear: string) {     
        browser.wait(this.EC.visibilityOf(this.TTBPermitElement));  
        this.TTBPermitElement.clear(); 
        return this.TTBPermitElement.sendKeys(fiscalYear);      
    }

    selectTypeManuOption() {
        browser.wait(this.EC.visibilityOf(this.selectTypeManuOptionElement));   
        return this.selectTypeManuOptionElement.click();
    }
    
    selectTypeImpOption() {
        browser.wait(this.EC.visibilityOf(this.selectTypeImpOptionElement));   
        return this.selectTypeImpOptionElement.click();
    }

    selectTypeNoOption() {
        browser.wait(this.EC.visibilityOf(this.selectTypeImpOptionElement));   
        return this.selectTypeImpOptionElement.click();
    }

    setTufaActiveDate(fiscalYear: string) {     
        browser.wait(this.EC.visibilityOf(this.tufaActiveDateElement));   
        browser.wait(this.EC.presenceOf(this.tufaActiveDateElement));   
        this.tufaActiveDateElement.clear();
        return this.tufaActiveDateElement.sendKeys(fiscalYear, protractor.Key.ENTER);      
    }

    clickSavePermit() {      
        return browser.actions().mouseMove(this.savePermitElement).click();
    }

    createNewPermit(ttbPermit: string, tufaActiveDate: string){ //Active date in format : 'MM/DD/YYYY'
    this.clickAddPermitButton();
    this.setTTBPermit(ttbPermit);
    this.selectTypeImpOption();
    this.setTufaActiveDate(tufaActiveDate);
    this.clickSavePermit();
    }

}
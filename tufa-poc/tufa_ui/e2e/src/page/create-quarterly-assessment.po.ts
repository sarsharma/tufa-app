import { by, element, protractor, browser } from "protractor";

export class CreateQuarterlyAssessmentPage {
    EC = protractor.ExpectedConditions;

    static readonly pageTitelElement = element(by.xpath('//h1[text()="Create Quarterly Assessment"]'));
    readonly fiscalYearFieldElement = element(by.css('input#fiscal-year'));
    readonly quarterDropdownElement = element(by.css('select#cal-qtr'));
    readonly q1OptionElement = element(by.css('option[value="Q1 - Oct, Nov, Dec"]'));
    readonly q2OptionElement = element(by.css('option[value="Q2 - Jan, Feb, Mar"]'));
    readonly q3OptionElement = element(by.css('option[value="Q3 - Apr, May, Jun"]'));
    readonly q4OptionElement = element(by.css('option[value="Q4 - Jul, Aug, Sep"]'));
    readonly createButton = element(by.css('button#btn-cr-qtr'));
    readonly fiscalYearRequiredErrorElement = element(by.xpath('//li[contains(text(),"Error: Fiscal Year requires appropriate format")]'));
    readonly quarterRequiredErrorElement = element(by.xpath('//li[contains(text(),"Error: Quarter is required")]'));
    readonly assesmentCreatedSuccessMessageElement = element(by.xpath('//span[text()="Successfully generated the assessment for selected Fiscal Year and Quarter"]'));
    readonly assesmentAlreadyExistingErrorElement = element(by.xpath('//span[contains(text(),"assessment already exists")]'));

    clickCreateButton() {
        this.createButton.click();
        return browser.wait(this.EC.or(this.EC.visibilityOf(this.assesmentCreatedSuccessMessageElement)
            ,this.EC.visibilityOf(this.assesmentAlreadyExistingErrorElement)
            ,this.EC.visibilityOf(this.fiscalYearRequiredErrorElement)
            ,this.EC.visibilityOf(this.quarterRequiredErrorElement)
            ));
    }

}
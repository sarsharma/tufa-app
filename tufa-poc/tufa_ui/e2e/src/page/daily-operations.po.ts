import { browser, by, element, protractor } from 'protractor';
import { CreatePermitPage } from '../page/create-permits.po';
import { CreateCompanyPage } from './create-company.po';
import { CreateMonthlyReportPage } from './create-monthly-report.po';
import { MonthlyReportPage } from './monthly-report.po';
import { PermitHistoryPage } from './permit-history.po';
import { CompanyDetailPage } from './company-detail.po';

/**
 * Page Object representing Daily Operations page.
 */
export class DailyOperationsPage {
  EC = protractor.ExpectedConditions;

  /**
   * Selector Constant declarations for this page object
   */
  static readonly pageTitleElement = element(by.xpath('//h1[text()="Daily Operations"]'));

  readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Please wait...")]'));

  readonly pageTitleElementDaily = element(by.xpath('//h1[text()="Daily Operations"]'));
  readonly createCompanyButtonElement = element(by.css('button#create-company'));
  readonly createMonthlyReportButtonElement = element(by.css('button#create-monthly'));
  readonly keywordSearchInputElement = element(by.css('#keyword-search-input'));
  readonly companyTypeOptionElement = element(by.xpath('//option[@value="COMPANY"]'));
  readonly allStatusTypeOptionElement = element(by.xpath('//option[@value="All"]'));
  readonly searchButtonElement = element(by.css('button#search_btn'));
  readonly extendTableDisplayElement = element(by.xpath('//ul/li/a[(text()="50")]'));
  readonly januaryMonthCheckboxElement = element(by.css('#chkmon_JAN'));
  readonly februaryMonthCheckboxElement = element(by.css('#chkmon_FEB'));
  readonly marchMonthCheckboxElement = element(by.css('#chkmon_MAR'));
  readonly aprilMonthCheckboxElement = element(by.css('#chkmon_APR'));
  readonly mayMonthCheckboxElement = element(by.css('#chkmon_MAY'));
  readonly juneMonthCheckboxElement = element(by.css('#chkmon_JUN'));
  readonly julyMonthCheckboxElement = element(by.css('#chkmon_JUL'));
  readonly augustMonthCheckboxElement = element(by.css('#chkmon_AUG'));
  readonly septemberMonthCheckboxElement = element(by.css('#chkmon_SEP'));
  readonly octoberMonthCheckboxElement = element(by.css('#chkmon_OCT'));
  readonly novemberMonthCheckboxElement = element(by.css('#chkmon_NOV'));
  readonly decemberMonthCheckboxElement = element(by.css('#chkmon_DEC'));
  readonly firstReportSearchResultElement = element(by.xpath('//*[@id="content"]/div/div/div[1]/div[1]/section[2]/div/div/table/tbody/tr[1]/td[2]/a'));
  readonly firstCompanySearchResultElement = element(by.xpath('//*[@id="content"]/div/div/div[1]/div[1]/section[2]/div/div/table/tbody/tr[1]/td[2]/a'));
  readonly firstPermitNumberResultElement = element(by.xpath('//*[@id="content"]/div/div/div[1]/div[1]/section[2]/div/div/table/tbody/tr[1]/td[1]/span/a'));
  readonly searchResultElement = element(by.xpath('//*[@id="content"]/div/div/div[1]/div[1]/section[2]/div/div/table/tbody/tr[1]'));
  readonly einReportSearchResultElement = element(by.xpath('//*[@id="content"]/div/div/div[1]/div[1]/section[2]/div/div/table/tbody/tr[1]/td[3]'));
  readonly einCompanySearchResultElement = element(by.xpath('//*[@id="content"]/div/div/div[1]/div[1]/section[2]/div/div/table/tbody/tr[1]/td[2]'));
  readonly allPermitNumbers = element.all(by.css(`table[class='table table-hover'] tbody tr td:nth-child(1)`));
  readonly allEIN = element.all(by.css(`table[class='table table-hover'] tbody tr td:nth-child(3)`));
  readonly einSortElement = element(by.css('#h-cmp-ein>mfdefaultsorter>a'));
  readonly pagination3Element = element(by.xpath('//*[@id="content"]/div/div/div[1]/div[1]/section[2]/div/div/table/tfoot/tr/td/mfbootstrappaginator/mfpaginator/ul[1]/li[4]/a'));
  readonly periodOfActivityElement = element(by.xpath('//*[@id="content"]/div/div/div[1]/div[1]/section[2]/div/div/table/tbody/tr[1]/td[4]/a'));
  readonly doBreadcrumbElement = element(by.xpath('//*[@id="dly-ops"]'));
  readonly filterElement = element(by.xpath('//*[@id="content"]/div/div/div[1]/div[2]/section[2]/div/p/strong'));
  readonly periodOfActivityElements = element.all(by.xpath('//table[@summary="Search Permit Results"]//td[4]/a'));
  readonly ttbPermitFirstLinkElement = element(by.xpath('(//table[@summary="Search Permit Results"]//td[1]//a)[1]'));

  clickCreateCompanyButton() {
    this.createCompanyButtonElement.click();
    return new CreateCompanyPage();
  }

  clickCreateMonthlyReportButton() {
    this.createMonthlyReportButtonElement.click();
    return new CreateMonthlyReportPage();
  }

  setSearchText(searchText: string) {
    this.keywordSearchInputElement.click();
    this.keywordSearchInputElement.clear();
    return this.keywordSearchInputElement.sendKeys(searchText);
  }

  selectCompanyTypeOption() {
    this.companyTypeOptionElement.click();
    browser.wait(this.EC.visibilityOf(this.allStatusTypeOptionElement));
    return this.allStatusTypeOptionElement;
  }

  allStatusTypeOption() {
    return this.allStatusTypeOptionElement.click();
  }

  clickSearchButton() {
    this.searchButtonElement.click();
    browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
    return browser.sleep(3000);
  }

  clickExtendDisplayButton() {
    this.extendTableDisplayElement.isPresent().then(isPresent => {
      if(isPresent) {
        let extendElement = this.extendTableDisplayElement;
        browser.wait(this.EC.elementToBeClickable(extendElement)).then(function() {
          browser.sleep(1000);
          let promise = extendElement.click();
          browser.waitForAngular().then(function () {
            browser.sleep(1000);
            return promise;
          });
        }).catch((error) => {
          console.log('Unable to find extend button, continuing. error: ' + error);
          return;
        });
      } else {
        return;
      }
    });   
  }

  clickJanuaryMonthCheckbox() {
    browser.wait(this.EC.visibilityOf(this.januaryMonthCheckboxElement));
    this.januaryMonthCheckboxElement.click();
    return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickFebruaryMonthCheckbox() {
    browser.wait(this.EC.visibilityOf(this.februaryMonthCheckboxElement));
    this.februaryMonthCheckboxElement.click();
    return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickMarchMonthCheckbox() {
    browser.wait(this.EC.visibilityOf(this.marchMonthCheckboxElement));
    this.marchMonthCheckboxElement.click();
    return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickAprilMonthCheckbox() {
    browser.wait(this.EC.visibilityOf(this.aprilMonthCheckboxElement));
    this.aprilMonthCheckboxElement.click();
    return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickMayMonthCheckbox() {
    browser.wait(this.EC.visibilityOf(this.mayMonthCheckboxElement));
    this.mayMonthCheckboxElement.click();
    return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickJuneMonthCheckbox() {
    browser.wait(this.EC.visibilityOf(this.juneMonthCheckboxElement));
    this.juneMonthCheckboxElement.click();
    return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickJulyMonthCheckbox() {
    browser.wait(this.EC.visibilityOf(this.julyMonthCheckboxElement));
    this.julyMonthCheckboxElement.click();
    return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickAugustMonthCheckbox() {
    browser.wait(this.EC.visibilityOf(this.augustMonthCheckboxElement));
    this.augustMonthCheckboxElement.click();
    return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickSeptemberMonthCheckbox() {
    browser.wait(this.EC.visibilityOf(this.septemberMonthCheckboxElement));
    this.septemberMonthCheckboxElement.click();
    return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickOctoberMonthCheckbox() {
    browser.wait(this.EC.visibilityOf(this.octoberMonthCheckboxElement));
    this.octoberMonthCheckboxElement.click();
    return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickNovemberMonthCheckbox() {
    browser.wait(this.EC.visibilityOf(this.novemberMonthCheckboxElement));
    this.novemberMonthCheckboxElement.click();
    return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickDecemberMonthCheckbox() {
    browser.wait(this.EC.visibilityOf(this.decemberMonthCheckboxElement));
    this.decemberMonthCheckboxElement.click();
    return browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickPeriodOfActivityElement() {
    this.periodOfActivityElement.click();
    let monthlyReportPage = new MonthlyReportPage();
    browser.wait(this.EC.visibilityOf(monthlyReportPage.pageTitleElement));
    return monthlyReportPage;
  }

  clickPeriodOfActivity(period: string) {
    let periodElement = element(by.cssContainingText('a', period));
    browser.wait(this.EC.visibilityOf(this.filterElement));
    browser.wait(this.EC.elementToBeClickable(periodElement));
    periodElement.click();
    let monthlyReportPage = new MonthlyReportPage();
    browser.wait(this.EC.visibilityOf(monthlyReportPage.pageTitleElement));
    return monthlyReportPage;
  }

  clickPagination3Element() {
    this.pagination3Element.click();
    browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  clickEinSortElement() {
    this.einSortElement.click();
    browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  getAllEinElement() {
    return this.allEIN;
  }

  getFirstReportSearchResult() {
    return this.firstReportSearchResultElement.getText();
  }

  getFirstCompanySearchResult() {
    return this.firstCompanySearchResultElement.getText();
  }

  geteinReportSearchResult() {
    return this.einReportSearchResultElement.getText();
  }

  geteinCompanySearchResult() {
    return this.einCompanySearchResultElement.getText();
  }

  goToCompanyDetailsPage(companyName: string) {
    this.setSearchText(companyName);
    this.clickSearchButton();
    this.firstReportSearchResultElement.click();
    return new CreatePermitPage;
  }

  isPeriodOfActivityExisting(period: string) {
    return element(by.xpath('//table[@summary="Search Permit Results"]//td[4]/a[text()="' + period + '"]')).isPresent();
  }

  clickTtbPermitFirstLink() {
    this.ttbPermitFirstLinkElement.click();
    browser.sleep(2000).then(function() {
        console.log('Slept 2 seconds');
    });
    return new PermitHistoryPage();
  }

  clickCompanyDetailsLink() {
    this.firstCompanySearchResultElement.click();
    return new CompanyDetailPage();
  }
}


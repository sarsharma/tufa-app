import { browser, by, element, protractor } from 'protractor';
import * as path from "path";

/**
 * Page Object representing Ingesting File page.
 */
export class IngestFilePage {
  EC = protractor.ExpectedConditions;

  readonly annualTrueUPTitleElement = element(by.xpath('//*[@id="content"]/div/h1'));
  readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Loading...")]'));
  readonly ingestTabElement = element(by.xpath('//*[@id="at-ingets-tab-link"]'));
  readonly summaryandDetailsFilesRadioElement = element(by.xpath('//*[@id="rd-summdet"]'));
  readonly detailsOnlyFileRadioElement = element(by.xpath('//*[@id="rd-detail"]'));

  readonly ingestTTBFileButtonElement = element(by.xpath('//*[@id="tab1"]/div/accordion/accordion-group[1]//button[@id="btn-upload"]'));
  readonly ingestCBPFileButtonElement = element(by.xpath('//*[@id="tab1"]/div/accordion/accordion-group[2]//button[@id="btn-upload"]'));
  readonly selectFileInputElement = element(by.xpath('//*[@id="fileIngest"]'));
  readonly browseFileNameElement = element(by.xpath('//*[@id="ingestfileform"]/div[1]/div/div[1]/div/span'));
  readonly fileTypeTTBTaxRadioElement = element(by.xpath('//*[@id="TTB Tax"]'));
  readonly fileTypeTTBVolumeRadioElement = element(by.xpath('//*[@id="TTB Volume"]'));

  readonly ingestFileButtonElement = element(by.xpath('//*[@id="btn-ingest-file"]'));
  readonly cancelPopUpElement = element(by.xpath('//*[@id="ingest-file"]/div/div/div[3]/button[2]'));
  readonly fileIngestionInProgressPopUpElement = element(by.xpath('//*[text()=" File Ingestion can take several minutes. "]'));
  readonly excelDocRequiredErrorMessageElement = element(by.xpath('//*[@id="pdfdocerror"]//*[text()="Error: Excel Document is required"]'));
  readonly fileTypeRequiredErrorMessageElement = element(by.xpath('//*[@id="fileTypesError"]//*[text()="Error: Please select a file type for ingestion"]'));
  readonly unableToTaxIngestAlertMessageElement = element(by.xpath('//div[contains(text(),"Ingested TTB Tax already present for fiscal year")]'));
  readonly unableToVolumeIngestAlertMessageElement = element(by.xpath('//div[contains(text(),"Ingested TTB Volume Data already present for fiscal year")]'));
  readonly deleteIngestedFilePopUpElement = element(by.xpath('//*[text()="Are you sure you want to delete?"]'));
  readonly confirmDeletePopUpElement = element(by.xpath('//*[@id="btn-del-doc"]'));
  readonly cancelDeletePopUpElement = element(by.xpath('//*[@id="delete-ingest"]/div/div/div[3]/button[2]'));
  readonly deletingMessageElement = element(by.xpath('//div[contains(text(),"Deleting...")]'));

  readonly getMetricsButtonElement = element(by.xpath('//*[@title="Get Metrics"]'));
  readonly generatingMetricsPopupElement = element(by.xpath('//*[text()="Generating Metrics"]'));

  readonly uniqueSummaryFileEntryValuesElement = element(by.xpath('//*[text()="Unique Summary File Entry Summary Numbers"]/ancestor::tr/td[2]/span'));
  readonly uniqueDetailsFileEntryValuesElement = element(by.xpath('//*[text()="Unique Details File Entry Summary Numbers"]/ancestor::tr/td[2]/span'));
  readonly uniqueDetailsFileEntryExportElement = element(by.xpath('//*[text()="Unique Details File Entry Summary Numbers"]/ancestor::tr/td[3]/button[@title="Download Unique Details File Entry Summary Numbers"]'));
  readonly acceptIngestedButtonElement = element(by.xpath('//*[@title = "Accept Ingested"]'));
  readonly acceptingIngestedValuesPopupElement = element(by.xpath('//*[text()="Accepting Ingested Values"]'));

  waitForLoading() {
    browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
  }

  waitForIngestionPopUp() {
    browser.wait(this.EC.visibilityOf(this.ingestFileButtonElement));
    browser.wait(this.EC.elementToBeClickable(this.ingestFileButtonElement));
  }

  waitForIngestionComplete() {
    browser.wait(this.EC.visibilityOf(this.fileIngestionInProgressPopUpElement));
    browser.wait(this.EC.invisibilityOf(this.fileIngestionInProgressPopUpElement));
  }

  waitForDeletePopUp() {
    browser.wait(this.EC.visibilityOf(this.deleteIngestedFilePopUpElement));
    browser.wait(this.EC.visibilityOf(this.confirmDeletePopUpElement))
  }

  waitForDeletePopUpToClose() {
    browser.wait(this.EC.invisibilityOf(this.deletingMessageElement));
  }

  selectFile(absolutep: string, fileName: string) {
    var fileToUpload = absolutep.concat(fileName);
    var absolutePath = path.resolve(__dirname, fileToUpload);
    this.selectFileInputElement.sendKeys(absolutePath);
  }
  
  getCompletedIngestedFileName(fileName: string){
    return element(by.xpath('//tr[contains(.,"'+fileName+'")]'));
  }

  getDeleteButton(fileName: string){
    return element(by.xpath('//tr[contains(.,"'+fileName+'")]/td[3]/div/div[2]/button'));
  }
}
import { browser, by, element, protractor } from 'protractor';
import { DailyOperationsPage } from './daily-operations.po';
import { LogoutPage } from './logout.po';

/**
 * Page Object representing login page.
 */
export class LoginPage {
  EC = protractor.ExpectedConditions;
  
  /**
   * Selector Constant declarations for this page object
   */
  static readonly confirmButtonElement = element(by.css('button#btn-del-doc'));
  readonly cancelButtonElement = element(by.xpath('//button[text()="Cancel"]'));
  
  get() {
    browser.get('/tufaspringmvp16');
    return this;
  }

  clickConfirmButton() {
    LoginPage.confirmButtonElement.click();
    browser.wait(this.EC.visibilityOf(DailyOperationsPage.pageTitleElement));
    browser.sleep(3000);
    return new DailyOperationsPage();
  }

  clickCancelButton() {
    this.cancelButtonElement.click();
    return new LogoutPage();
  }

}

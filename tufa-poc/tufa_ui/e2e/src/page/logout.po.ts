import { by, element, protractor } from 'protractor';
import { LoginPage } from './login.po';

/**
 * Page Object representing Logout page.
 */
export class LogoutPage {
  EC = protractor.ExpectedConditions;

  /**
   * Selector Constant declarations for this page object
   */
  readonly clickHereLinkElement = element(by.css('a#log_back_in'));

  goToLoginPage() {
    this.clickHereLinkElement.click();
    return new LoginPage();
  }

}

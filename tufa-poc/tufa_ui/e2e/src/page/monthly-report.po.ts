import { browser, by, element, protractor } from "protractor";
import { QuarterlyAssessmentPage } from "./assessments/quarterly-assessment.po";

export class MonthlyReportPage {
    EC = protractor.ExpectedConditions;

    readonly pageTitleElement = element(by.xpath('//h1[text()="Monthly Report"]'));
    readonly saveReportElement = element(by.css('button#btn-save-pd'));
    readonly reportStatusElement = element(by.xpath('//*[@id="period-status"]/strong'));
    readonly savingMessageElement = element(by.xpath('//div[contains(text(),"Saving...")]'));
    readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Loading...")]'));
    readonly pleaseWaitMessageElement = element(by.xpath('//div[contains(text(),"Please wait...")]'));
    readonly missingDocWarningElement = element(by.xpath('//li[contains(text(), "Missing supporting documentation")]'));
    readonly noDocProvidedWarningElement = element(by.xpath('//li[contains(text(), "No documentation was provided")]'));
    readonly form3852WarningElement = element(by.xpath('//li[contains(text(), "Invalid information in form FDA 3852")]'));
    readonly form5210WarningElement = element(by.xpath('//li[contains(text(), "Invalid information in form TTB 5210.5")]'));
    readonly form5000WarningElement = element(by.xpath('//li[contains(text(), "Invalid information in form TTB 5000.24")]'));
    readonly form7501WarningElement = element(by.xpath('//li[contains(text(), "Invalid information in form CBP 7501")]'));
    readonly form5220WarningElement = element(by.xpath('//li[contains(text(), "Invalid information in form TTB 5220.6")]'));
    readonly assessmentSectionHeaderElement = element(by.xpath('//h4[text()="Assessments"]'))
    readonly assessmentUserTextElement = element(by.xpath('//span[contains(text(),"User")]/parent::li'));
    readonly assessmentTimestampTextElement = element(by.xpath('//span[contains(text(),"Timestamp")]/parent::li'));
    readonly assessmentStatusTextElement = element(by.xpath('//span[contains(text(),"Status")]/parent::li'));
    readonly assessmentReconButtonElement = element(by.css('button#btn-recon'));
    readonly breadcrumbLinkElements = element.all(by.xpath('//li[@class="breadcrumb-item"]/a'));
    static readonly breadcrumbLinkElement = element(by.xpath('//li[@class="breadcrumb-item"]/a[1]'));

    clickSaveReport() {
        browser.wait(this.EC.invisibilityOf(this.savingMessageElement));
        this.saveReportElement.click();
        browser.wait(this.EC.invisibilityOf(this.savingMessageElement));
    }

    getReportStatusElement(status: string) {
        return this.reportStatusElement.getText();
    }

    waitforPageMRtoLoad(){
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.wait(this.EC.invisibilityOf(this.pleaseWaitMessageElement));
        browser.wait(this.EC.visibilityOf(this.pageTitleElement));
    }

    clickBreadcrumbLink(index: number) {
        browser.wait(this.EC.elementToBeClickable(this.breadcrumbLinkElements.get(index)));
        return this.breadcrumbLinkElements.get(index).click();
    }

    clickAssessmentReconButton() {
        browser.wait(this.EC.elementToBeClickable(this.assessmentReconButtonElement));
        this.assessmentReconButtonElement.click();
        //TODO: sleep added for stability, should be removed if possible
        return browser.sleep(2000);
    }

}
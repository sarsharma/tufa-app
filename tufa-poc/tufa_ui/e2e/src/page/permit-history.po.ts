import { browser, by, element, protractor } from "protractor";

export class PermitHistoryPage {

    EC = protractor.ExpectedConditions;
    readonly permitHistoryTitleElement = element(by.xpath('//*[@id="permitHistoryForm"]/h1'));
    readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Loading...")]'));
    readonly permitNumberTitleElement = element(by.css('h3#pmt-num'));
    readonly companyNameTitleElement = element(by.css('h2#cmp-name'));
    readonly permitNumberElement = element(by.xpath('//*[@id="permitNum"]'));
    readonly typeElement = element(by.css('input#permit-type'));
    readonly permitStatusClosedElement = element(by.xpath('//*[@id="permitStatusTypeCd"]/option[1]'));
    readonly permitStatusActiveElement = element(by.xpath('//*[@id="permitStatusTypeCd"]/option[2]'));
    readonly tufaActiveDateElement = element(by.css('input#active-date'));
    readonly ttbIssueDateElement = element(by.css('input#ttb-issue-date'));
    readonly savePermitButtonElement = element(by.buttonText('Save'));
    readonly closeDateErrorElement = element.all(by.xpath('//*[@id="cls-dtpckr"]/div/div[2]/ul/li'));
    readonly tufaInactiveDateElement = element(by.xpath('//*[@id="close-date"]'));
    readonly permitClosedDatePickerElement = element.all(by.xpath('//*[@id="cls-dtpckr"]/div/div[1]/input'));

    readonly addCommentElement = element(by.xpath('//*[@id="btn-save-comm"]'));
    readonly addCommentTitleElement = element(by.xpath('//*[@id="title-comment"]'));
    readonly commentDescriptionElement = element(by.xpath('//textarea[@title="Comment Description"]'));
    readonly saveCommentElement = element(by.xpath('//*[@id="save-comment"]'));
    readonly cancelCommentElement = element(by.xpath('//*[@id="cancel-comment"]'));
    readonly requiredErrorCommentElement = element(by.xpath('//ul[contains(.,"Error: Comment description is required")]'));
    readonly resolveCommentElement = element(by.xpath('//table[@class ="table table-hover custom"]/tbody/tr[1]/td[4]/label'));
    readonly firstCommentElement = element(by.xpath('//*[@id="permitHistoryForm"]/div/div[3]/div/section/div/div/div/div[1]/div/div[2]/table/tbody/tr[1]/td[1]'));

    readonly periodOfActivityDescElement = element(by.xpath('//*[@id="h-pd-act"]/mfdefaultsorter/a/span'));
    readonly firstPeriodOfActivityElement = element(by.xpath('//*[@id="permitHistoryForm"]/div/div[4]/div/section/div/table/tbody/tr[1]/td[1]/a'));
    readonly reportStatusElement = element(by.xpath('//*[@id="h-rpt-sta"]/mfdefaultsorter/a'));
    readonly reportStatusAscElement = element(by.xpath('//*[@id="h-rpt-sta"]/mfdefaultsorter/a/span[@class="fa fa-caret-up ng-star-inserted"]'));

    readonly allMRDeleteElements = element.all(by.css(`table[class='table  table-hover'] tbody tr td:nth-child(3) button`));
    readonly deletePopUpTitleElement = element(by.xpath('//*[@id="confirm-popup"]/div/div/div[1]/div'));
    readonly confirmDeletePopUpElement = element(by.xpath('//*[@id="btn-del-doc"]'));
    readonly cancelDeletePopUpElement = element(by.xpath('//*[@title="Cancel"]'));
    readonly statusExcludedElement = element(by.xpath('//h3[text()= " Status: Excluded"]'));
    readonly breadcrumbLinkElements = element.all(by.xpath('//li[@class="breadcrumb-item"]/a'));

    getReportStatusAsc() {
        browser.wait(this.EC.visibilityOf(this.reportStatusAscElement));
        browser.wait(this.EC.presenceOf(this.reportStatusAscElement));
        return this.reportStatusAscElement;
    }

    waitForPermitHistoryTitle() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        browser.wait(this.EC.visibilityOf(this.permitHistoryTitleElement));
        browser.wait(this.EC.presenceOf(this.permitNumberTitleElement));
        return browser.wait(this.EC.elementToBeClickable(this.savePermitButtonElement));
    }

    waitForCommentPopUp() {
        browser.wait(this.EC.visibilityOf(this.addCommentTitleElement));
    }

    waitForLoading() {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
    }

    getCommentDescription(commentText: string) {
        return element(by.xpath('//tr[contains(.,"' + commentText + '")]/td[1]'));
    }

    getCommentAuthor(commentText: string) {
        return element(by.xpath('//tr[contains(.,"' + commentText + '")]/td[2]'));
    }

    getCommentDateAdded(commentText: string) {
        return element(by.xpath('//tr[contains(.,"' + commentText + '")]/td[3]'));
    }

    getCommentResolved() {
        browser.wait(this.EC.elementToBeClickable(this.resolveCommentElement));
        return this.resolveCommentElement;
    }

    getEditComment(commentText: string) {
        browser.wait(this.EC.elementToBeClickable(element(by.xpath('//tr[contains(.,"' + commentText + '")]/td[1]/i'))));
        return element(by.xpath('//tr[contains(.,"' + commentText + '")]/td[1]/i'));
    }

    waitForDeletePopUp() {
        browser.wait(this.EC.visibilityOf(this.deletePopUpTitleElement));
    }

    clickBreadcrumbLink(index: number) {
        //TODO: 3 second sleep added below due to page inconsistency; getting application error otherwise. 
        browser.sleep(3000);
        browser.wait(this.EC.elementToBeClickable(this.breadcrumbLinkElements.get(index)));
        return this.breadcrumbLinkElements.get(index).click();
    }

    deletePeriodOfActivity(period: string) {
        browser.wait(this.EC.invisibilityOf(this.loadingMessageElement));
        element(by.xpath('//a[contains(text(),"' + period + '")]/ancestor::tr//button[contains(text(),"Delete")]')).click();
        this.waitForDeletePopUp();
        browser.wait(this.EC.elementToBeClickable(this.confirmDeletePopUpElement));
        browser.sleep(2000);
        this.confirmDeletePopUpElement.click();
        return browser.sleep(2000);
    }
 
    isPeriodOfActivityExisting(period: string) {
        return element(by.xpath('//tbody//td[1]/a[text()="' + period + '"]')).isPresent();
    }
}
import { browser, by, element, protractor } from 'protractor';

/**
 * Page Object representing Daily Operations page.
 */
export class PermitManagementPage {
  EC = protractor.ExpectedConditions;
  
  readonly permitManagementTitleElement = element(by.xpath('//*[text()="Permit Management"]'));
  readonly loadingMessageElement = element(by.xpath('//div[contains(text(),"Loading...")]'));
  readonly uploadDocumentButtonElement = element(by.xpath('//*[@id="btn-upload"]'));
  readonly excelDocRequiredErrorMessageElement = element(by.xpath('//*[@id="pdfdocerror"]//*[text()="Error: Excel Document is required"]'));
  readonly fileNameequiredErrorMessageElement = element(by.xpath('//*[text()="Error: File Name is required"]'));  
  readonly selectFileInputElement = element(by.xpath('//*[@id="fileIngest"]'));
  readonly fileNameInputElement = element(by.xpath('//*[@id="fileNm"]'));
  readonly ingestFilePopUpElement = element(by.xpath('//*[@id="btn-ingest-file"]'));
  readonly cancelPopUpElement = element(by.xpath('//*[@id="ingest-modal-file"]/div/div/div[3]/button[2]'));
  readonly fileIngestionInProgressPopUpElement = element(by.xpath('//*[text()=" File Ingestion can take several minutes. "]'));
  
  readonly editButtonElements = element.all(by.xpath('//button[contains(text(), "Edit")]'));
  readonly editDocumentPopUpTitleElement = element(by.xpath('//*[@id="ingest-modal-file"]'));
  
  readonly downloadButtonElements = element.all(by.xpath('//button[contains(text(), "Download")]'));
}
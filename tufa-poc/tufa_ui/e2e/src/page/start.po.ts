import { browser, by, element, protractor } from 'protractor';
import { LoginPage } from './login.po';

/**
 * Page Object representing Start page, which comes up when hitting the base URL directly.
 */
export class StartPage {
  EC = protractor.ExpectedConditions;

  /**
   * Selector Constant declarations for this page object
   */
  readonly startPageAppRootElement = element(by.xpath('//body//app-root[1]'));
  readonly loadingTextElement = element(by.css(''))

  /**
   * Navigates to TUFA site, depending on the relative URL which was passed through command-line parameters, for example:
   * npm run protractor  --url=/tufaspringmvp16 -- --suite=create-company
   * 
   * If no parameter is specified when running command, the relative url will default to '/tufanext'
   * An absolute URL is also supported, in case the hostname should change. Default hostname is specified by /e2e/protractor.conf.js file 'baseUrl' property
   * 
   * @param relativeUrl optional parameter which will set the relative URL; if null, the default relative URL will be UAT environment. ('/tufanext')
   */
  get(relativeUrl?: string) {
    if(relativeUrl) {
      browser.get(relativeUrl);
    } else {
      browser.get('/tufanext');
    }
    return this;
  }

  goToLoginPage() {
    browser.wait(this.EC.visibilityOf(LoginPage.confirmButtonElement)).then(function () {
      console.log("goToLoginPage true");
      return new LoginPage();

    }).catch(function () {
      console.log("goToLoginPage error catch");
      return new LoginPage();

    });
    return new LoginPage();

  }

}

import { browser } from "protractor";
import { SideNavigtionComponent } from "../../component/navigation.co";
import { AssessmentsPage } from "../../page/assessments/assessments.po";
import { LoginPage } from "../../page/login.po";
import { StartPage } from "../../page/start.po";
import { IngestFilePage } from "../../page/ingesting-file.po";


describe('Assessment Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentPage: AssessmentsPage;
    let ingestFilePage: IngestFilePage;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            loginPage.clickConfirmButton();
            sideNavigtionComponent = new SideNavigtionComponent();
            assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
            assessmentPage.waitForLoadingAssessment();
            assessmentPage.clickAnnualTrueUp();
            assessmentPage.waitForLoadingAssessment();
            assessmentPage.clickOnYear('2008');
            ingestFilePage = new IngestFilePage();
            ingestFilePage.getDeleteButton('2008_TTB Tax File.xlsx').isPresent().then(isExisting => {
                if (isExisting) {
                    ingestFilePage.getDeleteButton('2008_TTB Tax File.xlsx').click();
                    ingestFilePage.waitForDeletePopUp();
                    ingestFilePage.confirmDeletePopUpElement.click();
                    ingestFilePage.waitForDeletePopUpToClose();
                }
            });

            ingestFilePage.getDeleteButton('2008_TTB Volume File.xlsx').isPresent().then(isExisting => {
                if (isExisting) {
                    ingestFilePage.getDeleteButton('2008_TTB Volume File.xlsx').click();
                    ingestFilePage.waitForDeletePopUp();
                    ingestFilePage.confirmDeletePopUpElement.click();
                    ingestFilePage.waitForDeletePopUpToClose();
                }
            });
        });
    });

    it('Should be able to navigate to File Ingestion tab.', () => {
        ingestFilePage.waitForLoading();
        expect(ingestFilePage.summaryandDetailsFilesRadioElement.isSelected()).toBe(true, 'Expect ingested page to be displayed and double file ingestion to be selected');
    });

    it('Validate the TTB Volume File pop up throws an error', () => {
        ingestFilePage.ingestTTBFileButtonElement.click();
        ingestFilePage.waitForIngestionPopUp();
        ingestFilePage.ingestFileButtonElement.click();
        expect(ingestFilePage.excelDocRequiredErrorMessageElement.isDisplayed()).toBe(true, 'Expect TTB Error: Excel Document is required to be displayed');
        expect(ingestFilePage.fileTypeRequiredErrorMessageElement.isDisplayed()).toBe(true, 'Expect TTB Error: Please select a file type for ingestion to be displayed');
    });

    it('Validate the TTB Tax File is ingested', () => {
        ingestFilePage.selectFile('../documents/', '2008_TTB Tax File.xlsx')
        ingestFilePage.fileTypeTTBTaxRadioElement.click();
        ingestFilePage.ingestFileButtonElement.click();
        ingestFilePage.waitForIngestionComplete();
        expect(ingestFilePage.getCompletedIngestedFileName('2008_TTB Tax File.xlsx').isPresent()).toBe(true, 'Expect TTB Tax File to be ingested');
    });

    it('Validate the TTB Tax File pop up is unable to ingest if file is existing', () => {
        ingestFilePage.ingestTTBFileButtonElement.click();
        ingestFilePage.waitForIngestionPopUp();
        ingestFilePage.selectFile('../documents/', '2008_TTB Tax File.xlsx')
        ingestFilePage.fileTypeTTBTaxRadioElement.click();
        ingestFilePage.ingestFileButtonElement.click();
        ingestFilePage.waitForIngestionComplete();
        expect(ingestFilePage.unableToTaxIngestAlertMessageElement.isDisplayed()).toBe(true, 'Expect TTB Tax unable to ingest file message to be displayed');
    });

    it('Validate the TTB Volume File is ingested', () => {
        ingestFilePage.selectFile('../documents/', '2008_TTB Volume File.xlsx')
        ingestFilePage.fileTypeTTBVolumeRadioElement.click();
        ingestFilePage.ingestFileButtonElement.click();
        ingestFilePage.waitForIngestionComplete();
        expect(ingestFilePage.getCompletedIngestedFileName('2008_TTB Volume File.xlsx').isPresent()).toBe(true, 'Expect TTB Volume File to be ingested');
    });

    it('Validate TTB Ingest File Button is disabled.', () => {
        expect(ingestFilePage.ingestTTBFileButtonElement.isEnabled()).toBe(false, 'Expect TTB Ingest File button to be disabled');
    });

    it('Validate TTB Tax file to be deleted', () => {
        ingestFilePage.getDeleteButton('2008_TTB Tax File.xlsx').click();
        ingestFilePage.waitForDeletePopUp();
        ingestFilePage.confirmDeletePopUpElement.click();
        ingestFilePage.waitForDeletePopUpToClose();
        ingestFilePage.waitForLoading();
        expect(ingestFilePage.getCompletedIngestedFileName('2008_TTB Tax File.xlsx').isPresent()).toBe(false, 'Expect TTB Tax File to be deleted');
    });

    it('Validate the TTB Volume File pop up is unable to ingest if file is existing', () => {
        ingestFilePage.ingestTTBFileButtonElement.click();
        ingestFilePage.waitForIngestionPopUp();
        ingestFilePage.selectFile('../documents/', '2008_TTB Volume File.xlsx')
        ingestFilePage.fileTypeTTBVolumeRadioElement.click();
        ingestFilePage.ingestFileButtonElement.click();
        ingestFilePage.waitForIngestionComplete();
        expect(ingestFilePage.unableToVolumeIngestAlertMessageElement.isDisplayed()).toBe(true, 'Expect TTB Volume File unable to ingest file message to be displayed');
    });
});
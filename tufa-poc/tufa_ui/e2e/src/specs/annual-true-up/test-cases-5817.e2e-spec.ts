import { browser } from "protractor";
import { SideNavigtionComponent } from "../../component/navigation.co";
import { AssessmentsPage } from "../../page/assessments/assessments.po";
import { LoginPage } from "../../page/login.po";
import { StartPage } from "../../page/start.po";
import { AnnualTrueUpAssessmentPage } from "e2e/src/page/assessments/annual-trueup-assessment.po";
import { CompareDetailsPage } from "e2e/src/page/compare-details.po";


describe('Test cases 5817 - Summary view for Manu NC automation', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentPage: AssessmentsPage;
    let annualTrueUpAssessmentPage: AnnualTrueUpAssessmentPage;
    let compareDetailsPage: CompareDetailsPage;
    
    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            loginPage.clickConfirmButton();
            sideNavigtionComponent = new SideNavigtionComponent();
            assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
        });
    });
    it('Compare page navigation -> Verify that Compare tab > All deltas grid gets populated', () => {
        //TODO:remove
        browser.sleep(5000);
        assessmentPage.clickAnnualTrueUp();
        assessmentPage.waitForLoadingAssessment();
        assessmentPage.clickAnnualTrueUpPageExpand();
        annualTrueUpAssessmentPage = assessmentPage.clickAnnualTrueUpTableLinkByYear('2006');
        annualTrueUpAssessmentPage.clickCompareTabButton();
        annualTrueUpAssessmentPage.clickAllDeltasRadio();

        expect(annualTrueUpAssessmentPage.deltaCompareTableRowElements.count()).toBe(0, 'Expected Compare tab All Deltas table to be empty');
        annualTrueUpAssessmentPage.filterByPermitType('Manufacturer');
        annualTrueUpAssessmentPage.filterByCompanyName('Test Manu 1');
        annualTrueUpAssessmentPage.filterByTobaccoClass('CHEW/SNUFF');

        annualTrueUpAssessmentPage.clickSelectAllCheckbox();

        expect(annualTrueUpAssessmentPage.deltaCompareTableRowElements.count()).not.toBe(0, 'Expected Compare tab All Deltas table to be populated after selecting all tobacco types');
     });


     it('TTB details ->  Summary view is available on the TTB  Details page ', () => {
        compareDetailsPage = annualTrueUpAssessmentPage.clickDeltaExciseTaxLink(0);
        compareDetailsPage.waitForLoading();

        expect(compareDetailsPage.pageBreadCrumbElement.isDisplayed()).toBe(true, 'Expected Compare page breadcrumb to be displayed');
        expect(compareDetailsPage.companyInfoFormElement.isDisplayed()).toBe(true, 'Expected Compare page company info form to be displayed');
     });

     it('Verify quarters -> Four drawers will appear on the tab one for each quarter  (Quarters 1-4)', () => {
        expect(compareDetailsPage.quarterOneHeaderElement.isDisplayed()).toBe(true, 'Expected Quarters 1 section to be displayed');
        expect(compareDetailsPage.quarterTwoHeaderElement.isDisplayed()).toBe(true, 'Expected Quarters 2 section to be displayed');
        expect(compareDetailsPage.quarterThreeHeaderElement.isDisplayed()).toBe(true, 'Expected Quarters 3 section to be displayed');
        expect(compareDetailsPage.quarterFourHeaderElement.isDisplayed()).toBe(true, 'Expected Quarters 4 section to be displayed');
     });

     it('Verify Quarter 2 drawer expanded by default', () => {

        // expect(compareDetailsPage.quarterOneDetailsElement.isDisplayed()).toBe(true, 'Expected Quarter 1 drawer to be expanded by default.');
        expect(compareDetailsPage.quarterTwoDetailsElement.isDisplayed()).toBe(true, 'Expected Quarter 2 drawer to be expanded by default.');
     });

     it('Expand Quarter 1 where Delta is Matched, and verify the details', () => {
        compareDetailsPage.quarterOneDetailsElement.isDisplayed().then(isDisplayed => {
            if(!isDisplayed){
                compareDetailsPage.clickQuarterOneHeader();
            }
            
            expect(compareDetailsPage.quarterOneTableElement.isDisplayed()).toBe(true, 'Expected table to be displayed');
            expect(compareDetailsPage.quarterOneAcceptFda3852ButtonElement.isDisplayed()).toBe(true, 'Expected Accept FDA 3852 button to be displayed');
            expect(compareDetailsPage.quarterOneAcceptIngestedButtonElement.isDisplayed()).toBe(true, 'Expected Accept Ingested button to be displayed');
            expect(compareDetailsPage.quarterOneInProgressButtonElement.isDisplayed()).toBe(true, 'Expected In Progress button to be displayed');
            expect(compareDetailsPage.quarterOneAmendSectionElement.isDisplayed()).toBe(true, 'Expected amend section element to be displayed');
        });
     });

     it('Expand Quarter 2 where Delta is Ingested Only, and verify the details', () => {
        compareDetailsPage.quarterTwoDetailsElement.isDisplayed().then(isDisplayed => {
            if(!isDisplayed){
                compareDetailsPage.clickQuarterTwoHeader();
            }
            
            expect(compareDetailsPage.quarterTwoAmountElement.getText()).toContain('Ingest Only', 'Expected amount to contain Ingest Only');
            expect(compareDetailsPage.quarterTwoTableElement.isDisplayed()).toBe(true, 'Expected table to be displayed');
            expect(compareDetailsPage.quarterTwoExcludeButtonElement.isDisplayed()).toBe(true, 'Expected Exclude button to be displayed');
            expect(compareDetailsPage.quarterTwoMarketShareReadyButtonElement.isDisplayed()).toBe(true, 'Expected Market Share button to be displayed');
            expect(compareDetailsPage.quarterTwoInProgressButtonElement.isDisplayed()).toBe(true, 'Expected In Progress button to be displayed');
            expect(compareDetailsPage.quarterTwoContinueButtonElement.isDisplayed()).toBe(true, 'Expected Continue button to be displayed');
        });
     });

    it('Verify Comment section', () => {

        expect(compareDetailsPage.commentsSectionElement.isDisplayed()).toBe(true, 'Expected Comments section to be displayed.');
     });

     it('Verify multiple quarter drawers can be opened at one time', () => {
        compareDetailsPage.quarterTwoDetailsElement.isDisplayed().then(isDisplayed => {
            if(!isDisplayed) {
                compareDetailsPage.clickQuarterTwoHeader();
            }
            compareDetailsPage.quarterOneDetailsElement.isDisplayed().then(isDisplayed2 => {
                if(!isDisplayed2){
                    compareDetailsPage.clickQuarterOneHeader();
                }
                expect(compareDetailsPage.quarterOneDetailsElement.isDisplayed()).toBe(true, 'Expected Quarter 1 drawer to be expanded by default.');
                expect(compareDetailsPage.quarterTwoDetailsElement.isDisplayed()).toBe(true, 'Expected Quarter 2 drawer to be expanded by default.');
            });
        });
     });

     //Q3 TUFA Only scenario will happen at the end because we need to access a different Compare page
     it('Expand Quarter 3 where Delta is TUFA Only, and verify the details', () => {
        browser.navigate().back();
        annualTrueUpAssessmentPage.waitForLoading();
        annualTrueUpAssessmentPage.companyNameFilter.clear();
        annualTrueUpAssessmentPage.filterByCompanyName('Test Manu 5');
        browser.sleep(3000);
        compareDetailsPage = annualTrueUpAssessmentPage.clickDeltaExciseTaxLink(0);
        compareDetailsPage.waitForLoading();
        
        compareDetailsPage.quarterThreeDetailsElement.isDisplayed().then(isDisplayed => {
            if(!isDisplayed){
                compareDetailsPage.clickQuarterThreeHeader();
            }
            
            expect(compareDetailsPage.quarterThreeAmountElement.getText()).toContain('TUFA Only', 'Expected amount to contain Ingest Only');
            expect(compareDetailsPage.quarterThreeTableElement.isDisplayed()).toBe(true, '');
            expect(compareDetailsPage.quarterThreeExcludeButtonElement.isDisplayed()).toBe(true, '');
            expect(compareDetailsPage.quarterThreeMarketShareReadyButtonElement.isDisplayed()).toBe(true, '');
            expect(compareDetailsPage.quarterThreeInProgressButtonElement.isDisplayed()).toBe(true, '');
            expect(compareDetailsPage.quarterThreeContinueButtonElement.isDisplayed()).toBe(true, '');
        });
     });
});
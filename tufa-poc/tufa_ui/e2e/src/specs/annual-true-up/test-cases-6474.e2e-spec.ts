import { AnnualTrueUpAssessmentPage } from "e2e/src/page/assessments/annual-trueup-assessment.po";
import { CompareDetailsPage } from "e2e/src/page/compare-details.po";
import { browser } from "protractor";
import { SideNavigtionComponent } from "../../component/navigation.co";
import { AssessmentsPage } from "../../page/assessments/assessments.po";
import { LoginPage } from "../../page/login.po";
import { StartPage } from "../../page/start.po";

describe('Test cases 6474 Automation Scripts - Annual True Up: Revamp of the Compare Details Page', () => {
   let startPage: StartPage;
   let loginPage: LoginPage;
   let sideNavigtionComponent: SideNavigtionComponent;
   let assessmentPage: AssessmentsPage;
   let annualTrueUpAssessmentPage: AnnualTrueUpAssessmentPage;
   let compareDetailsPage: CompareDetailsPage;
   
   beforeAll(() => {
      browser.waitForAngularEnabled(false).then(function () {
         startPage = new StartPage().get(process.env.npm_config_url);
         loginPage = startPage.goToLoginPage();
         loginPage.clickConfirmButton();
         sideNavigtionComponent = new SideNavigtionComponent();
         assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
         //TODO:remove
         browser.sleep(5000);
         assessmentPage.clickAnnualTrueUp();
         assessmentPage.waitForLoadingAssessment();
         assessmentPage.clickAnnualTrueUpPageExpand();
         annualTrueUpAssessmentPage = assessmentPage.clickAnnualTrueUpTableLinkByYear('2018');
         annualTrueUpAssessmentPage.clickCompareTabButton();
         annualTrueUpAssessmentPage.clickAllDeltasRadio();
         annualTrueUpAssessmentPage.waitForLoading();         
      });
   });
   
   afterEach(() => {
      compareDetailsPage.annualTrueUpBreadCrumbElement.click();
      annualTrueUpAssessmentPage.waitForLoading();
   });
   
   it('Compare (All Deltas Grid) -> Select Non-cigar TUFA only delta -> Verify that the action buttons available are as follows for All Deltas Importer Deltas: Exclude, Market Share Ready, In progress, Continue', () => {
      annualTrueUpAssessmentPage.filterByTobaccoClass('PIPE', true);
      annualTrueUpAssessmentPage.filterByPermitType('Importer');
      annualTrueUpAssessmentPage.filterByDataType('TUFA');
      
      annualTrueUpAssessmentPage.clickSelectAllCheckbox();
      
      browser.sleep(2000);
      annualTrueUpAssessmentPage.waitForLoading();
      compareDetailsPage = annualTrueUpAssessmentPage.clickDeltaExciseTaxLink(0);
      compareDetailsPage.waitForLoading();
      
      expect(compareDetailsPage.excludeButtonElements.count()).toBeGreaterThan(0, 'Expected Exclude button to be present');
      expect(compareDetailsPage.marketShareReadyButtonElements.count()).toBeGreaterThan(0, 'Expected Market Share Ready button to be present');
      expect(compareDetailsPage.inProgressButtonElements.count()).toBeGreaterThan(0, 'Expected In Progress button to be present');
      expect(compareDetailsPage.continueButtonElements.count()).toBeGreaterThan(0, 'Expected Continue button to be present');
   });
   
   it('Compare (All Deltas Grid) -> Select Cigar TUFA only delta -> Verify that the action buttons available are as follows for All Deltas Importer Deltas: Exclude, Market Share Ready, In progress, Continue', () => {
      annualTrueUpAssessmentPage.filterByTobaccoClass('CIGARS');      
      browser.sleep(2000);
      annualTrueUpAssessmentPage.waitForLoading();
      compareDetailsPage = annualTrueUpAssessmentPage.clickDeltaExciseTaxLink(0);
      compareDetailsPage.waitForLoading();
      
      expect(compareDetailsPage.excludeButtonElements.count()).toBeGreaterThan(0, 'Expected Exclude button to be present');
      expect(compareDetailsPage.marketShareReadyButtonElements.count()).toBeGreaterThan(0, 'Expected Market Share Ready button to be present');
      expect(compareDetailsPage.inProgressButtonElements.count()).toBeGreaterThan(0, 'Expected In Progress button to be present');
      expect(compareDetailsPage.continueButtonElements.count()).toBeGreaterThan(0, 'Expected Continue button to be present');
   });
   
   it('Compare (All Deltas Grid) -> Select Non-Cigar Ingested only Importer delta -> Verify that the action buttons available are as follows for All Deltas Importer Deltas: Association, Exclude, Market Share Ready, In progress, Continue', () => {
      annualTrueUpAssessmentPage.filterByTobaccoClass('PIPE', true);
      annualTrueUpAssessmentPage.filterByDataType('Ingested');
      
      browser.sleep(2000);
      annualTrueUpAssessmentPage.waitForLoading();
      compareDetailsPage = annualTrueUpAssessmentPage.clickDeltaExciseTaxLink(0);
      compareDetailsPage.waitForLoading();
      
      expect(compareDetailsPage.associationButtonElements.count()).toBeGreaterThan(0, 'Expected Association button to be present');
      expect(compareDetailsPage.excludeButtonElements.count()).toBeGreaterThan(0, 'Expected Exclude button to be present');
      expect(compareDetailsPage.marketShareReadyButtonElements.count()).toBeGreaterThan(0, 'Expected Market Share Ready button to be present');
      expect(compareDetailsPage.inProgressButtonElements.count()).toBeGreaterThan(0, 'Expected In Progress button to be present');
      expect(compareDetailsPage.continueButtonElements.count()).toBeGreaterThan(0, 'Expected Continue button to be present');
   });
   
   it('Compare (All Deltas Grid) -> Select Cigar Ingested only Importer delta -> Verify that the action buttons available are as follows for All Deltas Importer Deltas: Association, Exclude, Market Share Ready, In progress, Continue', () => {
      annualTrueUpAssessmentPage.filterByTobaccoClass('CIGARS');
      
      browser.sleep(2000);
      annualTrueUpAssessmentPage.waitForLoading();
      compareDetailsPage = annualTrueUpAssessmentPage.clickDeltaExciseTaxLink(0);
      compareDetailsPage.waitForLoading();
      
      expect(compareDetailsPage.associationButtonElements.count()).toBeGreaterThan(0, 'Expected Association button to be present');
      expect(compareDetailsPage.excludeButtonElements.count()).toBeGreaterThan(0, 'Expected Exclude button to be present');
      expect(compareDetailsPage.marketShareReadyButtonElements.count()).toBeGreaterThan(0, 'Expected Market Share Ready button to be present');
      expect(compareDetailsPage.inProgressButtonElements.count()).toBeGreaterThan(0, 'Expected In Progress button to be present');
      expect(compareDetailsPage.continueButtonElements.count()).toBeGreaterThan(0, 'Expected Continue button to be present');
   });
   
   it('Compare (Matched Grid) -> Select Non-Cigar Importer delta -> Verify that the action buttons available are as follows for Matched Importer Deltas: Accept 3852, Accept Ingested, Mixed action, In Progress', () => {
      annualTrueUpAssessmentPage.clickMatchedRadio();
      annualTrueUpAssessmentPage.waitForLoading();
      annualTrueUpAssessmentPage.filterByAction('In Progress');
      annualTrueUpAssessmentPage.filterByPermitType('Importer');
      annualTrueUpAssessmentPage.filterByTobaccoClass('PIPE');
      annualTrueUpAssessmentPage.waitForLoading();

      browser.sleep(2000);
      annualTrueUpAssessmentPage.waitForLoading();
      // compareDetailsPage = annualTrueUpAssessmentPage.clickInProgressMatchedExciseTaxLink(0);
      compareDetailsPage = annualTrueUpAssessmentPage.clickMatchedExciseTaxLink(0);
      compareDetailsPage.waitForLoading();
      
      expect(compareDetailsPage.acceptFda3852ButtonElements.count()).toBeGreaterThan(0, 'Expected Accept FDA 3852 button to be present');
      expect(compareDetailsPage.acceptIngestedButtonElements.count()).toBeGreaterThan(0, 'Expected Accept Ingested button to be present');
      expect(compareDetailsPage.acceptMixedActionButtonElements.count()).toBeGreaterThan(0, 'Expected Accept Mixed Action button to be present');
      expect(compareDetailsPage.matchedInProgressButtonElements.count()).toBeGreaterThan(0, 'Expected In Progress button to be present');
   });
   
   it('Compare (Matched Grid) -> Select Non-Cigar Importer delta -> Verify that the action buttons available are as follows for Matched Importer Deltas: Accept 3852, Accept Ingested, Mixed action, In Progress', () => {
      compareDetailsPage.waitForLoading();
      annualTrueUpAssessmentPage.filterByTobaccoClass('CIGARS');
      
      browser.sleep(2000);
      annualTrueUpAssessmentPage.waitForLoading();
      // compareDetailsPage = annualTrueUpAssessmentPage.clickInProgressMatchedExciseTaxLink(0);
      compareDetailsPage = annualTrueUpAssessmentPage.clickMatchedExciseTaxLink(0);
      compareDetailsPage.waitForLoading();
      
      expect(compareDetailsPage.acceptFda3852ButtonElements.count()).toBeGreaterThan(0, 'Expected Accept FDA 3852 button to be present');
      expect(compareDetailsPage.acceptIngestedButtonElements.count()).toBeGreaterThan(0, 'Expected Accept Ingested button to be present');
      expect(compareDetailsPage.acceptMixedActionButtonElements.count()).toBeGreaterThan(0, 'Expected Accept Mixed Action button to be present');
      expect(compareDetailsPage.matchedInProgressButtonElements.count()).toBeGreaterThan(0, 'Expected In Progress button to be present');
   });
   
});
import { AnnualTrueUpAssessmentPage } from "e2e/src/page/assessments/annual-trueup-assessment.po";
import { CompareDetailsPage } from "e2e/src/page/compare-details.po";
import { browser, protractor } from "protractor";
import { SideNavigtionComponent } from "../../component/navigation.co";
import { AssessmentsPage } from "../../page/assessments/assessments.po";
import { LoginPage } from "../../page/login.po";
import { StartPage } from "../../page/start.po";
import { MarketShareReadyModalComponent } from "../../component/assessments/market-share-ready-modal.co";
import { TobaccoClassSelectionComponent } from "../../component/tobacco-class-selection.co";
import { Form3852Component } from "../../component/form-3852.co";
import { MonthlyReportPage } from "../../page/monthly-report.po";

describe('Test cases 6543 Automation Scripts - TTB Details - Ability to click to the Details page for other Tobacco Classes for the same Company', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentPage: AssessmentsPage;
    let annualTrueUpAssessmentPage: AnnualTrueUpAssessmentPage;
    let compareDetailsPage: CompareDetailsPage;
    let monthlyReportPage: MonthlyReportPage;
    let tobaccoClassSelection: TobaccoClassSelectionComponent;
    let form3852Component: Form3852Component;

    beforeAll(() => {
      browser.waitForAngularEnabled(false).then(function () {
         startPage = new StartPage().get(process.env.npm_config_url);
         loginPage = startPage.goToLoginPage();
         loginPage.clickConfirmButton();
         sideNavigtionComponent = new SideNavigtionComponent();
         tobaccoClassSelection = new TobaccoClassSelectionComponent();
         form3852Component = new Form3852Component();   
         assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
      });
    });

    it('Compare page navigation -> Verify that Compare tab > All Deltas grid displayed', () => {
         //TODO:remove
         browser.sleep(5000);
         assessmentPage.clickAnnualTrueUp();
         assessmentPage.waitForLoadingAssessment();
         assessmentPage.clickAnnualTrueUpPageExpand();
         annualTrueUpAssessmentPage = assessmentPage.clickAnnualTrueUpTableLinkByYear('2003');
         annualTrueUpAssessmentPage.clickCompareTabButton();
         annualTrueUpAssessmentPage.clickAllDeltasRadio();
         annualTrueUpAssessmentPage.waitForLoading();

         expect(annualTrueUpAssessmentPage.deltaCompareTableRowElements.count()).toBe(0, 'Expected Compare tab All Deltas table to be displayed');
     });

     it('CBP details page -> All tobacco class boxes displayed', () => {
         annualTrueUpAssessmentPage.filterByCompanyName('Automation 6543', true);
         annualTrueUpAssessmentPage.filterByQuarter('4', true);
         annualTrueUpAssessmentPage.filterByTobaccoClass('PIPE', true);
         annualTrueUpAssessmentPage.clickSelectAllCheckbox();

         browser.sleep(2000);
         annualTrueUpAssessmentPage.waitForLoading();
         compareDetailsPage = annualTrueUpAssessmentPage.clickDeltaExciseTaxLink(0);
         compareDetailsPage.waitForLoading();

         expect(compareDetailsPage.cigarettesCheckboxElement.isDisplayed()).toBe(true, 'Expected Cigarettes Checkbox to be displayed');
         expect(compareDetailsPage.cigarsCheckboxElement.isDisplayed()).toBe(true, 'Expected Cigars Checkbox to be displayed');
         expect(compareDetailsPage.chewCheckboxElement.isDisplayed()).toBe(true, 'Expected Chew Checkbox to be displayed');
         expect(compareDetailsPage.pipeCheckboxElement.isDisplayed()).toBe(true, 'Expected Pipe Checkbox to be displayed');
         expect(compareDetailsPage.snuffCheckboxElement.isDisplayed()).toBe(true, 'Expected Snuff Checkbox to be displayed');
         expect(compareDetailsPage.ryoCheckboxElement.isDisplayed()).toBe(true, 'Expected Roll Your Own Checkbox to be displayed');
     });

     it('Validate the tobacco type checkbox order', () => {
         expect(compareDetailsPage.tobaccoBarCheckboxElements.get(0).getText()).toContain('Cigarettes', 'Expected first checkbox to be for Cigarettes Tobacco type');
         expect(compareDetailsPage.tobaccoBarCheckboxElements.get(1).getText()).toContain('Cigars', 'Expected first checkbox to be for Cigars Tobacco type');
         expect(compareDetailsPage.tobaccoBarCheckboxElements.get(2).getText()).toContain('Chew', 'Expected first checkbox to be for Chew Tobacco type');
         expect(compareDetailsPage.tobaccoBarCheckboxElements.get(3).getText()).toContain('Pipe', 'Expected first checkbox to be for Pipe Tobacco type');
         expect(compareDetailsPage.tobaccoBarCheckboxElements.get(4).getText()).toContain('Snuff', 'Expected first checkbox to be for Snuff Tobacco type');
         expect(compareDetailsPage.tobaccoBarCheckboxElements.get(5).getText()).toContain('Roll Your Own', 'Expected first checkbox to be for Roll Your Own Tobacco type');
     });

     it('Verify default selection of Pipe tobacco class', () => {  
        
      expect(compareDetailsPage.pipeCheckboxElement.isSelected()).toBe(true, 'Expected Pipe Checkbox to be selected by default');
     });

     it('Verify Quarter 4 drawer is expanded by default', () => {  
        
      expect(compareDetailsPage.quarterFourDetailsElement.isDisplayed()).toBe(true, 'Expected Q4 drawer element to be expanded by default');
     });

     it('Verify tobacco class buttons are enabled on the top of page', () => {
        expect(compareDetailsPage.cigarettesCheckboxElement.isEnabled()).toBe(false, 'Expected Cigarettes Checkbox to not be enabled by default');
        expect(compareDetailsPage.cigarsCheckboxElement.isEnabled()).toBe(true, 'Expected Cigars Checkbox to be enabled by default');
        expect(compareDetailsPage.chewCheckboxElement.isEnabled()).toBe(true, 'Expected Chew Checkbox to be enabled by default');
        expect(compareDetailsPage.pipeCheckboxElement.isEnabled()).toBe(false, 'Expected Pipe Checkbox to be disabled by default');
        expect(compareDetailsPage.snuffCheckboxElement.isEnabled()).toBe(true, 'Expected Snuff Checkbox to be enabled by default');
        expect(compareDetailsPage.ryoCheckboxElement.isEnabled()).toBe(true, 'Expected Roll Your Own Checkbox to be enabled by default');
     });

     it('Switch to Tobacco class Snuff, and verify checkbox selection and Q1 drawer to be expanded', () => { 
      compareDetailsPage.snuffCheckboxLabelElement.click();
      compareDetailsPage.waitForLoading();

      expect(compareDetailsPage.snuffCheckboxElement.isSelected()).toBe(true, 'Expected Snuff Checkbox to be selected');
      expect(compareDetailsPage.cigarettesCheckboxElement.isSelected()).toBe(false, 'Expected Cigarettes Checkbox to not be selected');
      expect(compareDetailsPage.chewCheckboxElement.isSelected()).toBe(false, 'Expected Chew Checkbox to not be selected');
      expect(compareDetailsPage.pipeCheckboxElement.isSelected()).toBe(false, 'Expected Pipe Checkbox to not be selected');
      expect(compareDetailsPage.cigarsCheckboxElement.isSelected()).toBe(false, 'Expected Cigars Checkbox to not be selected');
      expect(compareDetailsPage.ryoCheckboxElement.isSelected()).toBe(false, 'Expected Roll Your Own Checkbox to not be selected');

      expect(compareDetailsPage.quarterOneDetailsElement.isDisplayed()).toBe(true, 'Expected Q1 details to be expanded on page');

     });

     it('Switch Tobacco Class to Cigars and verify Checkbox selection, and Q1 drawer to be expanded', () => {
        compareDetailsPage.cigarsCheckboxLabelElement.click();
        compareDetailsPage.waitForLoading();

        expect(compareDetailsPage.cigarsCheckboxElement.isSelected()).toBe(true, 'Expected Cigars Checkbox to be selected');
        expect(compareDetailsPage.cigarettesCheckboxElement.isSelected()).toBe(false, 'Expected Cigarettes Checkbox to not be selected');
        expect(compareDetailsPage.chewCheckboxElement.isSelected()).toBe(false, 'Expected Chew Checkbox to not be selected');
        expect(compareDetailsPage.pipeCheckboxElement.isSelected()).toBe(false, 'Expected Pipe Checkbox to not be selected');
        expect(compareDetailsPage.snuffCheckboxElement.isSelected()).toBe(false, 'Expected Snuff Checkbox to not be selected');
        expect(compareDetailsPage.ryoCheckboxElement.isSelected()).toBe(false, 'Expected Roll Your Own Checkbox to not be selected');

        expect(compareDetailsPage.cigarQ1Through4DetailsSectionElement.isDisplayed()).toBe(true, 'Expected Cigar Q 1-4 details to be displayed on page');
     });


     it('In Pipe tobacco class, click Market Share Ready and Verify action saved and pink icon displayed', () => {
        compareDetailsPage.pipeCheckboxLabelElement.click();
        compareDetailsPage.waitForLoading();

        compareDetailsPage.quarterFourDetailsElement.isDisplayed().then(isDisplayed => {
           if(!isDisplayed) {
            compareDetailsPage.quarterFourHeaderElement.click();
            browser.wait(protractor.ExpectedConditions.visibilityOf(compareDetailsPage.quarterFourDetailsElement), 10000);
           }
        });
      
        compareDetailsPage.clickMarketShareReadyQ4Button();
        compareDetailsPage.msReadyModal.clickSaveButton();
        browser.wait(protractor.ExpectedConditions.invisibilityOf(MarketShareReadyModalComponent.modalTitleElement));
        compareDetailsPage.waitForLoading();

        
        expect(compareDetailsPage.q4PinkCheckIconElement.isDisplayed()).toBe(true, 'Expected pink save icon to be displayed');

     });

     it('Switch to different tobacco class then back to Pipe, then verify changes to be persisted', () => {
        compareDetailsPage.waitForLoading();  
        browser.wait(protractor.ExpectedConditions.elementToBeClickable(compareDetailsPage.cigarsCheckboxLabelElement));
        compareDetailsPage.cigarsCheckboxLabelElement.click();
        compareDetailsPage.waitForLoading();  
        compareDetailsPage.pipeCheckboxLabelElement.click();
        compareDetailsPage.waitForLoading();
        expect(compareDetailsPage.q4PinkCheckIconElement.isDisplayed()).toBe(true, 'Expected pink save icon to be persisted');
     });

     it('Should reset MS Ready status for Pipe tobacco class Q4', () => {
         compareDetailsPage.quarterFourDetailsElement.isDisplayed().then(isDisplayed => {
            if(!isDisplayed) {
            compareDetailsPage.quarterFourHeaderElement.click();
            browser.wait(protractor.ExpectedConditions.visibilityOf(compareDetailsPage.quarterFourDetailsElement), 10000);
            }
         });
        compareDetailsPage.clickInProgressQ4Button();
        
        expect(compareDetailsPage.q4PinkCheckIconElement.isPresent()).toBe(false, 'Expected pink save icon to not be present');
     });

     it('Should edit data in Monthly Report and verify in TTB details page that Cigarettes checkbox is enabled', () => {
      compareDetailsPage.chewCheckboxLabelElement.click();
      compareDetailsPage.waitForLoading();
      compareDetailsPage.quarterTwoHeaderElement.click();
      browser.sleep(3000).then(function() {
         compareDetailsPage.clickMonthAccordionExpand('January')
         monthlyReportPage = compareDetailsPage.clickMonthlyReportLink('January');
         tobaccoClassSelection.clickCigarettesSelection();
         form3852Component.setCigarettesRemovalsAmount('55555');
         monthlyReportPage.clickSaveReport();
         browser.sleep(1000).then(function() {
            monthlyReportPage.clickBreadcrumbLink(2);
            compareDetailsPage.waitForLoading();
            expect(compareDetailsPage.cigarettesCheckboxElement.isEnabled()).toBe(true, 'Expected Cigarettes Checkbox to be enabled');
         });
      });
      

     });

     it('Should reset Monthly Report and verify in TTB details page that Cigarettes checkbox is disabled', () => {
      monthlyReportPage = compareDetailsPage.clickMonthlyReportLink('January');
      tobaccoClassSelection.clickCigarettesSelection();
      monthlyReportPage.clickSaveReport();
      browser.sleep(1000).then(function() {
         monthlyReportPage.clickBreadcrumbLink(2);
         compareDetailsPage.waitForLoading();
         expect(compareDetailsPage.cigarettesCheckboxElement.isEnabled()).toBe(false, 'Expected Cigarettes Checkbox to be disabled');
      });

     });
});
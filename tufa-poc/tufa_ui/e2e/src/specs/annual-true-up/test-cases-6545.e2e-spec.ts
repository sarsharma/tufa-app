import { browser } from "protractor";
import { SideNavigtionComponent } from "../../component/navigation.co";
import { AssessmentsPage } from "../../page/assessments/assessments.po";
import { LoginPage } from "../../page/login.po";
import { StartPage } from "../../page/start.po";
import { AnnualTrueUpAssessmentPage } from "e2e/src/page/assessments/annual-trueup-assessment.po";
import { CompareDetailsPage } from "e2e/src/page/compare-details.po";
import { protractor } from "protractor/built/ptor";
import { MarketShareReadyModalComponent } from "../../component/assessments/market-share-ready-modal.co";
import { MonthlyReportPage } from "e2e/src/page/monthly-report.po";
import { TobaccoClassSelectionComponent } from "../../component/tobacco-class-selection.co";
import { Form3852Component } from "../../component/form-3852.co";

describe('Test cases 6545 - TTB Details - Ability to click other tobacco classes', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentPage: AssessmentsPage;
    let annualTrueUpAssessmentPage: AnnualTrueUpAssessmentPage;
    let compareDetailsPage: CompareDetailsPage;
    let monthlyReportPage: MonthlyReportPage;
    let tobaccoClassSelection: TobaccoClassSelectionComponent;
    let form3852Component: Form3852Component;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            loginPage.clickConfirmButton();
            sideNavigtionComponent = new SideNavigtionComponent();
            assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
        });
    });
    it('Compare page navigation -> Verify that Compare tab > Matched grid displayed', () => {
        //TODO:remove
        browser.sleep(5000);
        assessmentPage.clickAnnualTrueUp();
        assessmentPage.waitForLoadingAssessment();
        assessmentPage.clickAnnualTrueUpPageExpand();
      //   annualTrueUpAssessmentPage = assessmentPage.clickAnnualTrueUpTableLinkByYear('2017');
        annualTrueUpAssessmentPage = assessmentPage.clickAnnualTrueUpTableLinkByYear('2006');
        annualTrueUpAssessmentPage.clickCompareTabButton();
        annualTrueUpAssessmentPage.clickMatchedRadio();
        annualTrueUpAssessmentPage.waitForLoading();

        expect(annualTrueUpAssessmentPage.matchedCompareTableRowElements.count()).not.toBe(0, 'Expected Compare tab Matched table to be populated after clicking on Compare button');

     });

     it('TTB details page -> All tobacco class boxes displayed', () => {
        annualTrueUpAssessmentPage.filterByQuarter('1', true);
        annualTrueUpAssessmentPage.filterByTobaccoClass('CHEW/SNUFF', true);
      //   annualTrueUpAssessmentPage.filterByCompanyName('Cigar Full Exclude');
        annualTrueUpAssessmentPage.filterByCompanyName('Test Manu 1');
        annualTrueUpAssessmentPage.waitForLoading();
        compareDetailsPage = annualTrueUpAssessmentPage.clickMatchedExciseTaxLink(0);
        compareDetailsPage.waitForLoading();

        expect(compareDetailsPage.cigarettesCheckboxElement.isDisplayed()).toBe(true, 'Expected Cigarettes Checkbox to be displayed');
        expect(compareDetailsPage.cigarsCheckboxElement.isDisplayed()).toBe(true, 'Expected Cigars Checkbox to be displayed');
        expect(compareDetailsPage.chewSnuffCheckboxElement.isDisplayed()).toBe(true, 'Expected Chew/Snuff Checkbox to be displayed');
        expect(compareDetailsPage.pipeRyoCheckboxElement.isDisplayed()).toBe(true, 'Expected Pipe/Roll Your Own Checkbox to be displayed');

     });

     it('Verify default selection of tobacco class', () => {
        
        expect(compareDetailsPage.chewSnuffCheckboxElement.isSelected()).toBe(true, 'Expected Chew/Snuff Checkbox to be select by default');
     });

     it('Verify quarter drawer is expanded by default', () => {
        
        expect(compareDetailsPage.quarterOneTableElement.isDisplayed()).toBe(true, 'Expected Q1 drawer element to be expanded by default');
     });

     it('Verify tobacco class buttons are enabled on the top of page', () => {
        
        expect(compareDetailsPage.chewSnuffCheckboxElement.isEnabled()).toBe(false, 'Expected Chew/Snuff Checkbox to be disabled by default');
        expect(compareDetailsPage.cigarettesCheckboxElement.isEnabled()).toBe(true, 'Expected Cigarettes Checkbox to be enabled by default');
        expect(compareDetailsPage.cigarsCheckboxElement.isEnabled()).toBe(true, 'Expected Cigars Checkbox to be enabled by default');
        expect(compareDetailsPage.pipeRyoCheckboxElement.isEnabled()).toBe(true, 'Expected Pipe/Roll Your Own Checkbox to be enabled by default');
     });

     it('Switch Tobacco Class to Cigarettes and verify Checkbox selection, and Q1 drawer to be expanded', () => {
        compareDetailsPage.cigarettesCheckboxLabelElement.click();
        compareDetailsPage.waitForLoading();

        expect(compareDetailsPage.chewSnuffCheckboxElement.isEnabled()).toBe(true, 'Expected Chew/Snuff Checkbox to be enabled');
        expect(compareDetailsPage.cigarettesCheckboxElement.isEnabled()).toBe(false, 'Expected Cigarettes Checkbox to be disabled');
        expect(compareDetailsPage.cigarsCheckboxElement.isEnabled()).toBe(true, 'Expected Cigars Checkbox to be enabled');
        expect(compareDetailsPage.pipeRyoCheckboxElement.isEnabled()).toBe(true, 'Expected Pipe/Roll Your Own Checkbox to be enabled');

        expect(compareDetailsPage.quarterOneTableElement.isDisplayed()).toBe(true, 'Expected Q1 drawer element to be expanded');
     });

     it('Switch Tobacco Class to Cigar and verify Checkbox selection, and all drawers to be expanded', () => {
        compareDetailsPage.cigarsCheckboxLabelElement.click();
        compareDetailsPage.waitForLoading();

        expect(compareDetailsPage.chewSnuffCheckboxElement.isEnabled()).toBe(true, 'Expected Chew/Snuff Checkbox to be enabled');
        expect(compareDetailsPage.cigarettesCheckboxElement.isEnabled()).toBe(true, 'Expected Cigarettes Checkbox to be enabled');
        expect(compareDetailsPage.cigarsCheckboxElement.isEnabled()).toBe(false, 'Expected Cigars Checkbox to be disabled');
        expect(compareDetailsPage.pipeRyoCheckboxElement.isEnabled()).toBe(true, 'Expected Pipe/Roll Your Own Checkbox to be enabled');

        expect(compareDetailsPage.cigarQ1TableElement.isDisplayed()).toBe(true, 'Expected Cigar Q1 drawer element to be expanded ');
        expect(compareDetailsPage.cigarQ2TableElement.isDisplayed()).toBe(true, 'Expected Cigar Q2 drawer element to be expanded ');
        expect(compareDetailsPage.cigarQ3TableElement.isDisplayed()).toBe(true, 'Expected Cigar Q3 drawer element to be expanded ');
        expect(compareDetailsPage.cigarQ4TableElement.isDisplayed()).toBe(true, 'Expected Cigar Q4 drawer element to be expanded ');

     });

     it('In Pipe/Roll Your Own tobacco class, click Market Share Ready and Verify action saved and pink icon displayed', () => {
        compareDetailsPage.pipeRyoCheckboxLabelElement.click();
        compareDetailsPage.waitForLoading();
        compareDetailsPage.clickQuarterThreeHeader();
      //   compareDetailsPage.clickMarketShareReadyQ1Button();
        compareDetailsPage.clickMarketShareReadyQ3Button();
      //   compareDetailsPage.msReadyModal.volumeQ1InputElement.clear();
      //   compareDetailsPage.msReadyModal.volumeQ1InputElement.sendKeys('600');
        compareDetailsPage.msReadyModal.clickSaveButton();
        browser.wait(protractor.ExpectedConditions.invisibilityOf(MarketShareReadyModalComponent.modalTitleElement));
        compareDetailsPage.waitForLoading();

        
        expect(compareDetailsPage.q3PinkCheckIconElement.isDisplayed()).toBe(true, 'Expected pink save icon to be displayed');

     });

     it('Switch to different tobacco class then back to Pipe/Roll Your Own, then verify changes to be persisted', () => {
        compareDetailsPage.cigarettesCheckboxLabelElement.click();
        compareDetailsPage.waitForLoading();
        compareDetailsPage.pipeRyoCheckboxLabelElement.click();
        compareDetailsPage.waitForLoading();
        compareDetailsPage.clickQuarterThreeHeader();

        expect(compareDetailsPage.q3PinkCheckIconElement.isDisplayed()).toBe(true, 'Expected pink save icon to be displayed');

     });

     it('Should reset MS Ready status for Pipe/Roll Your Own', () => {
        compareDetailsPage.clickInProgressQ3Button();
        
        expect(compareDetailsPage.q3PinkCheckIconElement.isPresent()).toBe(false, 'Expected pink save icon to not be present');

     });

   //   it('Should edit data in Monthly Report and verify in TTB details page that checkbox is disabled/enabled', () => {
   //    compareDetailsPage.cigarettesCheckboxLabelElement.click();
   //    compareDetailsPage.waitForLoading();
   //    compareDetailsPage.quarterOneDetailsElement.isDisplayed().then(isDisplayed => {
   //       if(!isDisplayed) {
   //        compareDetailsPage.quarterOneHeaderElement.click();
   //        browser.wait(protractor.ExpectedConditions.visibilityOf(compareDetailsPage.quarterOneDetailsElement), 10000);
   //       }
   //    });
   //    monthlyReportPage = compareDetailsPage.clickMonthlyReportLink('Oct');
   //    tobaccoClassSelection = new TobaccoClassSelectionComponent();
   //    form3852Component = new Form3852Component();
   //    tobaccoClassSelection.clickCigarettesSelection();
   //    monthlyReportPage.clickSaveReport();
   //    browser.sleep(1000).then(function() {
   //       monthlyReportPage.clickBreadcrumbLink(2);
   //       compareDetailsPage.waitForLoading();
   //       compareDetailsPage.pipeRyoCheckboxLabelElement.click();
   //       compareDetailsPage.waitForLoading(); 
   //       expect(compareDetailsPage.cigarettesCheckboxElement.isEnabled()).toBe(true, 'Expected Cigarettes Checkbox to be enabled');
   //    });

     
   //   });
});
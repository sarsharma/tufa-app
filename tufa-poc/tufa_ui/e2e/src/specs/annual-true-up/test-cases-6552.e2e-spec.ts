import { browser } from "protractor";
import { SideNavigtionComponent } from "../../component/navigation.co";
import { AssessmentsPage } from "../../page/assessments/assessments.po";
import { LoginPage } from "../../page/login.po";
import { StartPage } from "../../page/start.po";
import { AnnualTrueUpAssessmentPage } from "e2e/src/page/assessments/annual-trueup-assessment.po";
import { CompareDetailsPage } from "e2e/src/page/compare-details.po";

let scenarioSearchFilterData = require('../../../data/scenario-data/json/true-up-cases-6552-data.json');

describe('Test cases 6552 - Annual True Up: Clicking into an All Deltas delta will bring user to the Compare Details Page', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentPage: AssessmentsPage;
    let annualTrueUpAssessmentPage: AnnualTrueUpAssessmentPage;
    let compareDetailsPage: CompareDetailsPage;
    
    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            loginPage.clickConfirmButton();
            sideNavigtionComponent = new SideNavigtionComponent();
            assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
        });
    });
    it('Compare page navigation -> Verify that Compare tab > All deltas grid gets populated', () => {
        //TODO:remove
        browser.sleep(5000);
        assessmentPage.clickAnnualTrueUp();
        assessmentPage.waitForLoadingAssessment();
        assessmentPage.clickAnnualTrueUpPageExpand();
        annualTrueUpAssessmentPage = assessmentPage.clickAnnualTrueUpTableLinkByYear('2018');
        annualTrueUpAssessmentPage.clickCompareTabButton();
        annualTrueUpAssessmentPage.clickAllDeltasRadio();

        annualTrueUpAssessmentPage.clickSelectAllCheckbox();

        expect(annualTrueUpAssessmentPage.deltaCompareTableRowElements.count()).not.toBe(0, 'Expected Compare tab All Deltas table to be populated after selecting all tobacco types');
 
    });
    
    scenarioSearchFilterData.forEach(searchObject => {
        processSearchIteration(searchObject);  
    });

    function processSearchIteration(searchObject) { 
        it('All deltas grid: Tobacco Class: ' + searchObject.tobacco_class + '; Company name: ' +searchObject.company_name + '; Quarter: ' + searchObject.quarter, () => {
            annualTrueUpAssessmentPage.waitForLoading();
            browser.sleep(2000);
            annualTrueUpAssessmentPage.filterByCompanyName(searchObject.company_name, true);
            annualTrueUpAssessmentPage.filterByTobaccoClass(searchObject.tobacco_class, true);
            annualTrueUpAssessmentPage.filterByQuarter(searchObject.quarter);
            annualTrueUpAssessmentPage.waitForLoading();
            browser.sleep(2000);

            annualTrueUpAssessmentPage.deltaExciseTaxLinkElements.count().then(count => {
                if(count > 0) {
                    compareDetailsPage = annualTrueUpAssessmentPage.clickDeltaExciseTaxLink(0);
                    compareDetailsPage.waitForLoading();
                    
                    browser.sleep(1000);
                    expect(compareDetailsPage.pageBreadCrumbElement.isDisplayed()).toBe(true, 'Expected Compare page breadcrumb to be displayed');
                    expect(compareDetailsPage.companyTitleElement.getText()).toContain(searchObject.company_name, 'Expected compare page title to contain company name: ' + searchObject.company_name);
                    
                    compareDetailsPage.annualTrueUpBreadCrumbElement.click();
                    annualTrueUpAssessmentPage.waitForLoading();
        
                } else {
                    fail('ERROR: There was no Delta excise links in table with the current search filter criteria: Tobacco Class: ' 
                    + searchObject.tobacco_class + '; Company name: ' +searchObject.company_name + '; Quarter: ' + searchObject.quarter);
                }
            })
        });

    }
});
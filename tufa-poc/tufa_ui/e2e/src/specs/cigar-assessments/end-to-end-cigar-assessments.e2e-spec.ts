import * as fs from 'fs';
import * as path from 'path';
import { browser, protractor } from "protractor";
import { Form3852Component } from "../../component/form-3852.co";
import { SideNavigtionComponent } from "../../component/navigation.co";
import { TobaccoClassSelectionComponent } from "../../component/tobacco-class-selection.co";
import { AssessmentsPage } from "../../page/assessments/assessments.po";
import { CigarAssessmentPage } from "../../page/assessments/cigar-assessment.po";
import { CreateMonthlyReportPage } from "../../page/create-monthly-report.po";
import { DailyOperationsPage } from "../../page/daily-operations.po";
import { LoginPage } from "../../page/login.po";
import { MonthlyReportPage } from "../../page/monthly-report.po";
import { PermitHistoryPage } from "../../page/permit-history.po";
import { StartPage } from "../../page/start.po";
import { ScriptUtils } from "../../util/script-utils";
import { StaticValues } from "../../util/static-values";
import { TestValues } from "../../util/test-values";


describe('Cigar Assessment Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentPage: AssessmentsPage;
    let monthlyReportPage: MonthlyReportPage;
    let permitHistoryPage: PermitHistoryPage;
    let tobaccoClassSelection: TobaccoClassSelectionComponent;
    let form3852Component: Form3852Component;
    let createMonthlyReportPage: CreateMonthlyReportPage;
    let cigarAssessmentsPage: CigarAssessmentPage;
    
    let downloadsDir = path.resolve(__dirname, '../../../data/downloaded-files/');
    let expectedFilesDir = path.resolve(__dirname, '../../../data/expected-files/');
    let uploadDir = path.resolve(__dirname, '../../../src/documents/');
    let uploadFileName = 'WI-TI-30001_5_16_19.pdf';
    let importerReportNumber = '563487438';
    let manufacturerReportNumber = '564378238';

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            loginPage.clickConfirmButton();
            sideNavigtionComponent = new SideNavigtionComponent();
            form3852Component = new Form3852Component();
            assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
        });
    });
    describe('End to End Scripts', () => { 
        beforeAll(() => {
            tobaccoClassSelection = new TobaccoClassSelectionComponent();
            dailyOperationsPage = sideNavigtionComponent.clickDailyOperationsNavElement();
            //Ensure Importer monthly report is deleted before proceeding.
            dailyOperationsPage.setSearchText(importerReportNumber);
            dailyOperationsPage.clickSearchButton();
            browser.sleep(2000).then(function() {
                console.log('Slept 2 seconds');
            });

            permitHistoryPage = dailyOperationsPage.clickTtbPermitFirstLink();
            permitHistoryPage.isPeriodOfActivityExisting(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_1).then(isExisting => { 
                if(isExisting) {
                    permitHistoryPage.deletePeriodOfActivity(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_1);
                    permitHistoryPage.waitForPermitHistoryTitle();
                }
            });
            permitHistoryPage.isPeriodOfActivityExisting(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_2).then(isExisting => { 
                if(isExisting) {
                    permitHistoryPage.deletePeriodOfActivity(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_2);
                    permitHistoryPage.waitForPermitHistoryTitle();
                }
            });
            permitHistoryPage.isPeriodOfActivityExisting(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_3).then(isExisting => { 
                if(isExisting) {
                    permitHistoryPage.deletePeriodOfActivity(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_3);
                    permitHistoryPage.waitForPermitHistoryTitle();
                }
            });
            permitHistoryPage.isPeriodOfActivityExisting(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_4).then(isExisting => { 
                if(isExisting) {
                    permitHistoryPage.deletePeriodOfActivity(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_4);
                    permitHistoryPage.waitForPermitHistoryTitle();
                }
            });

            dailyOperationsPage = sideNavigtionComponent.clickDailyOperationsNavElement();
            //Ensure Manufacturer monthly report is deleted before proceeding.
            dailyOperationsPage.setSearchText(manufacturerReportNumber);
            dailyOperationsPage.clickSearchButton();
            browser.sleep(2000).then(function() {
                console.log('Slept 2 seconds');
            });
            permitHistoryPage = dailyOperationsPage.clickTtbPermitFirstLink();
            permitHistoryPage.isPeriodOfActivityExisting(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_1).then(isExisting => { 
                if(isExisting) {
                    permitHistoryPage.deletePeriodOfActivity(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_1);
                    permitHistoryPage.waitForPermitHistoryTitle();
                }
            });
            permitHistoryPage.isPeriodOfActivityExisting(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_2).then(isExisting => { 
                if(isExisting) {
                    permitHistoryPage.deletePeriodOfActivity(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_2);
                    permitHistoryPage.waitForPermitHistoryTitle();
                }
            });
            permitHistoryPage.isPeriodOfActivityExisting(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_3).then(isExisting => { 
                if(isExisting) {
                    permitHistoryPage.deletePeriodOfActivity(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_3);
                    permitHistoryPage.waitForPermitHistoryTitle();
                }
            });
            permitHistoryPage.isPeriodOfActivityExisting(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_4).then(isExisting => { 
                if(isExisting) {    
                    permitHistoryPage.deletePeriodOfActivity(TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_4);
                    permitHistoryPage.waitForPermitHistoryTitle();
                }
            });

            //Create monthly reports
            dailyOperationsPage = sideNavigtionComponent.clickDailyOperationsNavElement();
            createMonthlyReportPage = dailyOperationsPage.clickCreateMonthlyReportButton();
            createMonthlyReportPage.setFiscalYear(TestValues.CIGAR_ASSESSMENT_YEAR);
            createMonthlyReportPage.selectMonth(TestValues.CIGAR_ASSESSMENT_MONTH_1);
            createMonthlyReportPage.clickMonthlyReportButton();
            browser.wait(protractor.ExpectedConditions.visibilityOf(createMonthlyReportPage.successfulCreationMessageElement));
            createMonthlyReportPage.selectMonth(TestValues.CIGAR_ASSESSMENT_MONTH_2);
            createMonthlyReportPage.clickMonthlyReportButton();
            browser.wait(protractor.ExpectedConditions.visibilityOf(createMonthlyReportPage.successfulCreationMessageElement));
            createMonthlyReportPage.selectMonth(TestValues.CIGAR_ASSESSMENT_MONTH_3);
            createMonthlyReportPage.clickMonthlyReportButton();
            browser.wait(protractor.ExpectedConditions.visibilityOf(createMonthlyReportPage.successfulCreationMessageElement));
            createMonthlyReportPage.selectMonth(TestValues.CIGAR_ASSESSMENT_MONTH_4);
            createMonthlyReportPage.clickMonthlyReportButton();
            browser.wait(protractor.ExpectedConditions.visibilityOf(createMonthlyReportPage.successfulCreationMessageElement));

            setCigarData(importerReportNumber, TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_1, sideNavigtionComponent);
            setCigarData(importerReportNumber, TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_2, sideNavigtionComponent);
            setCigarData(importerReportNumber, TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_3, sideNavigtionComponent);
            setCigarData(importerReportNumber, TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_4, sideNavigtionComponent);
            setCigarData(manufacturerReportNumber, TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_1, sideNavigtionComponent);
            setCigarData(manufacturerReportNumber, TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_2, sideNavigtionComponent);
            setCigarData(manufacturerReportNumber, TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_3, sideNavigtionComponent);
            setCigarData(manufacturerReportNumber, TestValues.CIGAR_ASSESSMENT_MONTH_FY_YEAR_4, sideNavigtionComponent);

        });

        it('Should navigate to Cigar Assessment "Report status" page, then verify sorting and pagination.', () => {
            assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
            //TODO:remove
            browser.sleep(5000);
            assessmentPage.clickCigarAssessmentTab();
            cigarAssessmentsPage = assessmentPage.clickCigarAssessmentTableLinkByYear(TestValues.CIGAR_ASSESSMENT_YEAR);
            browser.wait(protractor.ExpectedConditions.invisibilityOf(cigarAssessmentsPage.loadingMessageElement));

            expect(cigarAssessmentsPage.displayButton25ItemsElement.isPresent()).toBe(true, 'Expected default pagination to be 25');

            let element = cigarAssessmentsPage.ttbPermitTableElements;
            element.map(function(eachName){
                return eachName.getText().then(function(unSorted){
                    return unSorted;
                });
            }).then(function(unSorted){
                    var sorted = unSorted.slice();
                    sorted = sorted.sort((a: number, b: number) => a + b); 
                    expect(sorted).toEqual(unSorted, 'Expected default sorting to be by TTB Permit ascending order');
            });
         });

        it('Should click on Reconciliation Report and verify default sorting and filter.', () => {
            cigarAssessmentsPage.clickReconciliationReportButton();
            expect(cigarAssessmentsPage.notApprovedFilterElement.isDisplayed()).toEqual(true, 'Expected default filter to be set to "Unapproved"');
            let element = cigarAssessmentsPage.reconReportTtbPermitTableElements;
            element.map(function(eachName){
                return eachName.getText().then(function(unSorted){
                    return unSorted;
                });
            }).then(function(unSorted){
                    var sorted = unSorted.slice();
                    sorted = sorted.sort((a: number, b: number) => a + b); 
                    expect(sorted).toEqual(unSorted, 'Expected default sorting to be by TTB Permit ascending order');
            });
        });

        it('Should go to monthly report and verify Assessment status of "Not Approved".', () => {
            monthlyReportPage = cigarAssessmentsPage.clickReconReportTableReview(0);
            
            expect(monthlyReportPage.assessmentSectionHeaderElement.isDisplayed()).toEqual(true, 'Expected Assessment to be filed and assessment section to be displayed');
            expect(monthlyReportPage.assessmentUserTextElement.getText()).toContain(' ', 'Expected Assessment User to not be empty');
            expect(monthlyReportPage.assessmentTimestampTextElement.getText()).toEqual('Timestamp:', 'Expected Timestamp to be empty');
            expect(monthlyReportPage.assessmentStatusTextElement.getText()).toContain('Not Approved', 'Expected status to be "Not Approved"');
        });

        it('Should go to Generate and Submit, then verify "ACTION REQUIRED" warning message.', () => {
            browser.navigate().back();
            cigarAssessmentsPage.clickGenerateAndSubmitButton();
            expect(cigarAssessmentsPage.actionRequiredWarningElement.isDisplayed()).toEqual(true, 'Expected "ACTION REQUIRED" warning message to show');
        });
        
        it('Should click on Quarter 1 Generate Market Share button, then verify that report cannot be submitted until reports are approved.', () => {
            cigarAssessmentsPage.q1ExpandButtonElement.click();
            cigarAssessmentsPage.clickQ1GenerateButton();
            
            expect(cigarAssessmentsPage.submitButtonElement.isPresent()).toEqual(false, 'Expected Submit button to not be present');
            expect(cigarAssessmentsPage.reportsNotApprovedWarningElement.isDisplayed()).toEqual(true, 'Expected warning message stating reports cannot be submitted');
        });

        it('Should approve all reports and verify status has changed to approved.', () => {
            cigarAssessmentsPage.clickReconciliationReportButton();
            cigarAssessmentsPage.clickSortByTtbPermit();

            for(let i = 0; i < 8; i++) {
                performReportRecon(cigarAssessmentsPage);
            }
            cigarAssessmentsPage.selectApprovedFilter();

            expect(cigarAssessmentsPage.reconReportTtbPermitTableElements.count()).toBeGreaterThanOrEqual(8, 'Expected to perform Recon on all reports successfully');
        });

        it('Should generate market share for Q1, then verify "ACTION REQUIRED" warning message is no longer displayed, and submit button is available.', () => {
            cigarAssessmentsPage.clickGenerateAndSubmitButton();
            cigarAssessmentsPage.q1ExpandButtonElement.click();
            cigarAssessmentsPage.clickQ1GenerateButton();

            expect(cigarAssessmentsPage.actionRequiredWarningElement.isPresent()).toEqual(false, 'Expected "ACTION REQUIRED" warning message to be hidden');
            expect(cigarAssessmentsPage.submitButtonElement.isDisplayed()).toEqual(true, 'Expected "Submit" button to be available');
            expect(cigarAssessmentsPage.cigarAssessmentGeneratedTextElement.getText()).not.toEqual('Generated:', 'Expected "Generated" info text to contain value');
            expect(cigarAssessmentsPage.getCigarAssessmentAuthorTextElement(1).getText()).not.toEqual('Author:', 'Expected "Author" info text to contain value');
            expect(cigarAssessmentsPage.generatedDownloadButtonElement.isDisplayed()).toBe(true);
        });

        it('Should submit market share for Q1, and verify cigar assessment history.', () => {
            cigarAssessmentsPage.clickSubmitButton();

            expect(cigarAssessmentsPage.getCigarAssessmentSubmittedTextElement(1).isDisplayed()).toBe(true, 'Expected Submmitted timestamp text to show');
            expect(cigarAssessmentsPage.getCigarAssessmentAuthorTextElement(1).getText()).not.toEqual('Author:', 'Expected "Author" info text to contain value');
        });


        describe('Download Scenarios for Cigar Assessment - Quarter 1', () => { 
            it('Should click Q1 Download as CSV button and verify file is downloaded and filename is correct.', () => {
                cigarAssessmentsPage.q1DownloadButtonElement.click();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(isDownloaded => {
                    expect(isDownloaded).toBe(true);
                    fs.readdirSync(downloadsDir).forEach(downloadedFile => { 
                        expect(downloadedFile).toContain('FY_' + (parseInt(TestValues.CIGAR_ASSESSMENT_YEAR)+1), 'Expected filename to contain year number incremented by 1.');
                        expect(downloadedFile).toContain('Q1', 'Expected filename to contain Q1.');
                        expect(downloadedFile).toContain('Cigars', 'Expected filename to contain Cigars');
                    });

                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

            it('Should verify Q1 CSV file to contain correct expected values.', () => { 
                fs.readdirSync(downloadsDir).forEach(downloadedFile => {
                    try {
                        const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, downloadedFile), { encoding: 'utf8' });
                        const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.CIGAR_ASSESSMENT_EXPECTED_Q1_FILENAME), { encoding: 'utf8' });
                        expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                    } catch (err) {
                        fail(`ERROR: Exception thrown: ${err.message}`);
                    }
                });
            });
    
            afterAll(async () => {
                cigarAssessmentsPage.q1ExpandButtonElement.click();
                require('fs-extra').emptyDirSync(downloadsDir);
                await browser.sleep(2000);
            });
        });

        it('Should generate market share for Q2, then verify "ACTION REQUIRED" warning message is no longer displayed, and submit button is available.', () => {
            cigarAssessmentsPage.q2ExpandButtonElement.click();
            cigarAssessmentsPage.clickQ2GenerateButton();

            expect(cigarAssessmentsPage.actionRequiredWarningElement.isPresent()).toEqual(false, 'Expected "ACTION REQUIRED" warning message to be hidden');
            expect(cigarAssessmentsPage.submitButtonElement.isDisplayed()).toEqual(true, 'Expected "Submit" button to be available');
            expect(cigarAssessmentsPage.cigarAssessmentGeneratedTextElement.getText()).not.toEqual('Generated:', 'Expected "Generated" info text to contain value');
            expect(cigarAssessmentsPage.getCigarAssessmentAuthorTextElement(2).getText()).not.toEqual('Author:', 'Expected "Author" info text to contain value');
            expect(cigarAssessmentsPage.generatedDownloadButtonElement.isDisplayed()).toBe(true);
        });

        it('Should submit market share for Q2, and verify cigar assessment history.', () => {
            cigarAssessmentsPage.clickSubmitButton();

            expect(cigarAssessmentsPage.getCigarAssessmentSubmittedTextElement(2).isDisplayed()).toBe(true, 'Expected Submmitted timestamp text to show');
            expect(cigarAssessmentsPage.getCigarAssessmentAuthorTextElement(2).getText()).not.toEqual('Author:', 'Expected "Author" info text to contain value');
        });


        describe('Download Scenarios for Cigar Assessment - Quarter 2', () => { 
            it('Should click Q2 Download as CSV button and verify file is downloaded and filename is correct.', () => {
                cigarAssessmentsPage.q2DownloadButtonElement.click();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(isDownloaded => {
                    expect(isDownloaded).toBe(true);
                    fs.readdirSync(downloadsDir).forEach(downloadedFile => { 
                        expect(downloadedFile).toContain('FY_' + (parseInt(TestValues.CIGAR_ASSESSMENT_YEAR)+1), 'Expected filename to contain year number incremented by 1.');
                        expect(downloadedFile).toContain('Q2', 'Expected filename to contain Q2.');
                        expect(downloadedFile).toContain('Cigars', 'Expected filename to contain Cigars');
                    });

                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

            it('Should verify Q2 CSV file to contain correct expected values.', () => { 
                fs.readdirSync(downloadsDir).forEach(downloadedFile => {
                    try {
                        const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, downloadedFile), { encoding: 'utf8' });
                        const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.CIGAR_ASSESSMENT_EXPECTED_Q2_FILENAME), { encoding: 'utf8' });
                        expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                    } catch (err) {
                        fail(`ERROR: Exception thrown: ${err.message}`);
                    }
                });
            });
    
            afterAll(async () => {
                cigarAssessmentsPage.q2ExpandButtonElement.click();
                require('fs-extra').emptyDirSync(downloadsDir);
                await browser.sleep(2000);
            });
        });

        it('Should generate market share for Q3, then verify "ACTION REQUIRED" warning message is no longer displayed, and submit button is available.', () => {
            cigarAssessmentsPage.q3ExpandButtonElement.click();
            cigarAssessmentsPage.clickQ3GenerateButton();

            expect(cigarAssessmentsPage.actionRequiredWarningElement.isPresent()).toEqual(false, 'Expected "ACTION REQUIRED" warning message to be hidden');
            expect(cigarAssessmentsPage.submitButtonElement.isDisplayed()).toEqual(true, 'Expected "Submit" button to be available');
            expect(cigarAssessmentsPage.cigarAssessmentGeneratedTextElement.getText()).not.toEqual('Generated:', 'Expected "Generated" info text to contain value');
            expect(cigarAssessmentsPage.getCigarAssessmentAuthorTextElement(3).getText()).not.toEqual('Author:', 'Expected "Author" info text to contain value');
            expect(cigarAssessmentsPage.generatedDownloadButtonElement.isDisplayed()).toBe(true);
        });

        it('Should submit market share for Q3, and verify cigar assessment history.', () => {
            cigarAssessmentsPage.clickSubmitButton();

            expect(cigarAssessmentsPage.getCigarAssessmentSubmittedTextElement(3).isDisplayed()).toBe(true, 'Expected Submmitted timestamp text to show');
            expect(cigarAssessmentsPage.getCigarAssessmentAuthorTextElement(3).getText()).not.toEqual('Author:', 'Expected "Author" info text to contain value');
        });


        describe('Download Scenarios for Cigar Assessment - Quarter 3', () => { 
            it('Should click Q3 Download as CSV button and verify file is downloaded and filename is correct.', () => {
                cigarAssessmentsPage.q3DownloadButtonElement.click();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(isDownloaded => {
                    expect(isDownloaded).toBe(true);
                    fs.readdirSync(downloadsDir).forEach(downloadedFile => { 
                        expect(downloadedFile).toContain('FY_' + (parseInt(TestValues.CIGAR_ASSESSMENT_YEAR)+1), 'Expected filename to contain year number incremented by 1.');
                        expect(downloadedFile).toContain('Q3', 'Expected filename to contain Q3.');
                        expect(downloadedFile).toContain('Cigars', 'Expected filename to contain Cigars');
                    });

                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

            it('Should verify Q3 CSV file to contain correct expected values.', () => { 
                fs.readdirSync(downloadsDir).forEach(downloadedFile => {
                    try {
                        const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, downloadedFile), { encoding: 'utf8' });
                        const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.CIGAR_ASSESSMENT_EXPECTED_Q3_FILENAME), { encoding: 'utf8' });
                        expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                    } catch (err) {
                        fail(`ERROR: Exception thrown: ${err.message}`);
                    }
                });
            });
    
            afterAll(async () => {
                cigarAssessmentsPage.q3ExpandButtonElement.click();
                require('fs-extra').emptyDirSync(downloadsDir);
                await browser.sleep(2000);
            });
        });

        it('Should generate market share for Q4, then verify "ACTION REQUIRED" warning message is no longer displayed, and submit button is available.', () => {
            cigarAssessmentsPage.q4ExpandButtonElement.click();
            cigarAssessmentsPage.clickQ4GenerateButton();

            expect(cigarAssessmentsPage.actionRequiredWarningElement.isPresent()).toEqual(false, 'Expected "ACTION REQUIRED" warning message to be hidden');
            expect(cigarAssessmentsPage.submitButtonElement.isDisplayed()).toEqual(true, 'Expected "Submit" button to be available');
            expect(cigarAssessmentsPage.cigarAssessmentGeneratedTextElement.getText()).not.toEqual('Generated:', 'Expected "Generated" info text to contain value');
            expect(cigarAssessmentsPage.getCigarAssessmentAuthorTextElement(4).getText()).not.toEqual('Author:', 'Expected "Author" info text to contain value');
            expect(cigarAssessmentsPage.generatedDownloadButtonElement.isDisplayed()).toBe(true);
        });

        it('Should submit market share for Q4, and verify cigar assessment history.', () => {
            cigarAssessmentsPage.clickSubmitButton();

            expect(cigarAssessmentsPage.getCigarAssessmentSubmittedTextElement(4).isDisplayed()).toBe(true, 'Expected Submmitted timestamp text to show');
            expect(cigarAssessmentsPage.getCigarAssessmentAuthorTextElement(4).getText()).not.toEqual('Author:', 'Expected "Author" info text to contain value');
        });


        describe('Download Scenarios for Cigar Assessment - Quarter 4', () => { 
            it('Should click Q4 Download as CSV button and verify file is downloaded and filename is correct.', () => {
                cigarAssessmentsPage.q4DownloadButtonElement.click();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(isDownloaded => {
                    expect(isDownloaded).toBe(true);
                    fs.readdirSync(downloadsDir).forEach(downloadedFile => { 
                        expect(downloadedFile).toContain('FY_' + (parseInt(TestValues.CIGAR_ASSESSMENT_YEAR)+1), 'Expected filename to contain year number incremented by 1.');
                        expect(downloadedFile).toContain('Q4', 'Expected filename to contain Q4.');
                        expect(downloadedFile).toContain('Cigars', 'Expected filename to contain Cigars');
                    });

                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

            it('Should verify Q4 CSV file to contain correct expected values.', () => { 
                fs.readdirSync(downloadsDir).forEach(downloadedFile => {
                    try {
                        const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, downloadedFile), { encoding: 'utf8' });
                        const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.CIGAR_ASSESSMENT_EXPECTED_Q4_FILENAME), { encoding: 'utf8' });
                        expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                    } catch (err) {
                        fail(`ERROR: Exception thrown: ${err.message}`);
                    }
                });
            });
    
            afterAll(async () => {
                cigarAssessmentsPage.q4ExpandButtonElement.click();
                require('fs-extra').emptyDirSync(downloadsDir);
                await browser.sleep(2000);
            });
        });
    
        it('Should verify upload document popup is displayed.', () => {
        
            expect(cigarAssessmentsPage.submissionDocumentationHeaderElement.isDisplayed()).toEqual(true, 'Expected Submission Documentation section to be displayed');
            cigarAssessmentsPage.clickUploadDocumentButton();
            expect(cigarAssessmentsPage.uploadModalComponent.titleElement.isDisplayed()).toEqual(true, 'Expected Upload Document popup modal to be displayed');
        });
    
        it('Should verify upload document popup error messages are shown.', () => {
            cigarAssessmentsPage.uploadModalComponent.uploadButtonElement.click();
    
            expect(cigarAssessmentsPage.uploadModalComponent.documentRequiredErrorMessageElement.isDisplayed()).toBe(true, 'Expected "Document Required" error message to be displayed');
            expect(cigarAssessmentsPage.uploadModalComponent.fileNameRequiredErrorMessageElement.isDisplayed()).toBe(true, 'Expected "File Name Required" error message to be displayed');
            expect(cigarAssessmentsPage.uploadModalComponent.quarterRequiredErrorMessageElement.isDisplayed()).toBe(true, 'Expected "Quarter Required" error message to be displayed');
            expect(cigarAssessmentsPage.uploadModalComponent.versionRequiredErrorMessageElement.isDisplayed()).toBe(true, 'Expected "Version Required" error message to be displayed');
        });

        it('Should click upload button and verify the document is uploaded successfully.', () => {
            cigarAssessmentsPage.uploadModalComponent.setfileInputField(uploadDir, uploadFileName);
            cigarAssessmentsPage.uploadModalComponent.selectQuarter('Q1');
            cigarAssessmentsPage.uploadModalComponent.selectVersion('1');
            cigarAssessmentsPage.uploadModalComponent.uploadButtonElement.click();
            browser.wait(protractor.ExpectedConditions.invisibilityOf(cigarAssessmentsPage.loadingMessageElement));
    
            expect(cigarAssessmentsPage.uploadTableElement.isDisplayed()).toBe(true, 'Expected Document Upload table to be displayed');
        });
    
        it('Should verify new cigar assessment status to be Submitted (4/4) in Cigar Assessments Home page.', () => {
            expect(cigarAssessmentsPage.submittedStatusTextElement.getText()).toEqual('Submitted (4/4)', 'Expected Quarterly Assessment status to be submitted');    
            assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
            assessmentPage.clickCigarAssessmentTab();
            expect(assessmentPage.getCigarSubmissionStatusByYear(TestValues.CIGAR_ASSESSMENT_YEAR)).toEqual('Submitted (4/4)', 'Expected to find submitted status in Assessments Home Page');    
        });


        //TODO: Address / Contact info change scenario not feasible for automation
        // describe('Address Change Scenarios for Cigar Assessments', () => { 
            
        //     it('Should update company address and contact info, then verify the download is showing "Y" flag for address and contact.', () => {
        //         dailyOperationsPage = sideNavigtionComponent.clickDailyOperationsNavElement();
        //         //Ensure Importer monthly report is deleted before proceeding.
        //         dailyOperationsPage.setSearchText(importerReportNumber);
        //         dailyOperationsPage.clickSearchButton();
        //         browser.sleep(2000).then(function() {
        //             console.log('Slept 2 seconds');
        //         });

        //         // change primary address and contact info for company
        //         companyDetailsPage = dailyOperationsPage.clickCompanyDetailsLink();
        //         companyDetailsPage.waitForLoading();
        //         companyDetailsPage.editPrimaryAddressElement.click();
        //         browser.wait(protractor.ExpectedConditions.visibilityOf(companyDetailsPage.getEditCompanyAddressTitle()));
        //         companyDetailsPage.editStreetAddressCompanyAddressElement.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        //         companyDetailsPage.editStreetAddressCompanyAddressElement.sendKeys(protractor.Key.DELETE);
        //         companyDetailsPage.editStreetAddressCompanyAddressElement.sendKeys(ScriptUtils.generateRandomString(10));
        //         companyDetailsPage.saveCompanyAddressElement.click();
        //         companyDetailsPage.waitForLoading();
        //         companyDetailsPage.editContactButtonElement.get(0).click();
        //         companyDetailsPage.firstNameContactElement.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        //         companyDetailsPage.firstNameContactElement.sendKeys(protractor.Key.DELETE);
        //         companyDetailsPage.firstNameContactElement.sendKeys(ScriptUtils.generateRandomString(10));
        //         companyDetailsPage.saveContactButtonElement.click(); 
        //         companyDetailsPage.waitForLoading();

        //         //return to Cigar Assessments generate and submit and check downloaded file to contain address changed flags.
        //         assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
        //         browser.sleep(5000);
        //         assessmentPage.clickCigarAssessmentTab();
        //         cigarAssessmentsPage = assessmentPage.clickCigarAssessmentTableLinkByYear(TestValues.CIGAR_ASSESSMENT_YEAR);
        //         browser.wait(protractor.ExpectedConditions.invisibilityOf(cigarAssessmentsPage.loadingMessageElement));
        //         cigarAssessmentsPage.clickGenerateAndSubmitButton();
        //         cigarAssessmentsPage.q1ExpandButtonElement.click();
        //         cigarAssessmentsPage.q1AddressChangesOnlyButton.click();
        //         cigarAssessmentsPage.clickQ1GenerateButton();
        //         cigarAssessmentsPage.clickSubmitButton();
        //         cigarAssessmentsPage.q1DownloadButtonElement.click();
        //         browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(isDownloaded => {
        //             expect(isDownloaded).toBe(true);
        //             fs.readdirSync(downloadsDir).forEach(downloadedFile => { 
        //                 try {
        //                     const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, downloadedFile), { encoding: 'utf8' });
        //                     const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.CIGAR_ASSESSMENT_EXPECTED_ADDRESS_CHANGE_FILENAME), { encoding: 'utf8' });
        //                     expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
        //                 } catch (err) {
        //                     fail(`ERROR: Exception thrown: ${err.message}`);
        //                 }    
        //             });
        //         }).catch(function(ex) {
        //             throw('An error has occurred with file download: ' + ex);
        //         });
        //     });
            
        //     afterAll(async () => {
        //         require('fs-extra').emptyDirSync(downloadsDir);
        //         await browser.sleep(2000);
        //     });    
        // });
    });

    /**
     * Helper functions
     */

    function setCigarData(reportNumber, period, sideNavigtionComponent) {
        dailyOperationsPage = sideNavigtionComponent.clickDailyOperationsNavElement();
        dailyOperationsPage.setSearchText(reportNumber);
        dailyOperationsPage.clickSearchButton();

        monthlyReportPage = dailyOperationsPage.clickPeriodOfActivity(period);
        tobaccoClassSelection.getAllSelectedTobaccoClasses().count().then(count => {
            //If there is tobacco classes currently selected, attempt to clear the form in order to start fresh.
            if(count > 0) {
                tobaccoClassSelection.resetSelection();
                monthlyReportPage.clickSaveReport();
                browser.waitForAngular();
            }
        });
        tobaccoClassSelection.clickCigarsSelection();
        form3852Component.setCigarsRemovalsAmount('1000');
        form3852Component.setCigarsExciseTax('1000');
        monthlyReportPage.clickSaveReport();
        browser.sleep(2000).then(function() {
            console.log('Slept 2 seconds');
        });
    }

    function performReportRecon(cigarAssessmentsPage) {
        monthlyReportPage = cigarAssessmentsPage.clickReconReportTableReview(0);
        monthlyReportPage.clickAssessmentReconButton();
        browser.wait(protractor.ExpectedConditions.invisibilityOf(cigarAssessmentsPage.loadingMessageElement));
    }

});


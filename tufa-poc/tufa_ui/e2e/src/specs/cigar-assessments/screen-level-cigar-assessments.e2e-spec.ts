import { browser, protractor } from "protractor";
import { SideNavigtionComponent } from "../../component/navigation.co";
import { AssessmentsPage } from "../../page/assessments/assessments.po";
import { LoginPage } from "../../page/login.po";
import { StartPage } from "../../page/start.po";
import { PermitHistoryPage } from "../../page/permit-history.po";
import { CompanyDetailPage } from "../../page/company-detail.po";
import { CigarAssessmentPage } from "../../page/assessments/cigar-assessment.po";
import { CreateCigarAssessmentPage } from "../../page/create-cigar-assessment.po";

describe('Quarterly Assessment Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentPage: AssessmentsPage;
    let permitHistoryPage: PermitHistoryPage;
    let companyDetailPage: CompanyDetailPage;
    let cigarAssessmentsPage: CigarAssessmentPage;
    let createCigarAssessmentPage: CreateCigarAssessmentPage;
    
    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            loginPage.clickConfirmButton();
            sideNavigtionComponent = new SideNavigtionComponent();
            assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
        });
    });

    describe('Screen Level Scenarios', () => { 
        it('Should be able to Navigate to Cigar Assessment page.', () => {
            browser.wait(protractor.ExpectedConditions.invisibilityOf(assessmentPage.loadingQAElement));
            assessmentPage.cigarAssessmentsTab.click();
            browser.wait(protractor.ExpectedConditions.invisibilityOf(assessmentPage.loadingCAElement));
            expect(assessmentPage.pageTitleElement.isDisplayed()).toBe(true, 'Expected Assesment page title to be displayed');
        });

        it('Should validate assessment page layout to be correct.', () => {
            expect(assessmentPage.quarterlyAssessmentsTab.isDisplayed()).toBe(true, 'Expected Quarterly Assesment tab to be displayed');
            expect(assessmentPage.cigarAssessmentsTab.isDisplayed()).toBe(true, 'Expected Cigar Assesment tab to be displayed');
            expect(assessmentPage.annualTrueUpTab.isDisplayed()).toBe(true, 'Expected Annual True Up tab title to be displayed');
        });
        
        it('Should verify Cigar assessment primary sorting to be by descending fiscal year.', () => {
            let element = assessmentPage.cigarFiscalYearTableElements;
            element.map(function(eachName){
                return eachName.getText().then(function(unSorted){
                    return unSorted;
                });
            }).then(function(unSorted){
                    var sorted = unSorted.slice();
                    sorted = sorted.sort((a: number, b: number) => a + b); 
                    expect(sorted).toEqual(unSorted);

            });
        });
        
        it('Should filter by Not Submitted status and verify only not submitted cigar assesment records are displayed.', () => {
            assessmentPage.cigarNotSubmittedStatusCheckboxElement.click();

            expect(assessmentPage.cigarSubmittedElements.count()).toBe(0, 'Expected all Submitted assesments to be hidden');
        });


        it('Should filter by Submitted status and verify only submitted cigar assessment records are displayed.', () => {
            assessmentPage.cigarNotSubmittedStatusCheckboxElement.click();
            assessmentPage.cigarSubmittedStatusCheckboxElement.click();

            expect(assessmentPage.cigarNotSubmittedElements.count()).toBe(0, 'Expected all Not Submitted assesments to be hidden');
        });

        it('Should navigate to Monthly Report page using breadcrumbs.', () => {
            cigarAssessmentsPage = assessmentPage.clickCigarAssessmentTableLinkByYear('2017');
            cigarAssessmentsPage.clickReportStatusButton(0).clickBreadcrumbLink(3);
            permitHistoryPage = new PermitHistoryPage();

            expect(permitHistoryPage.permitHistoryTitleElement.isDisplayed()).toBe(true, 'Expected breadcrumb to arrive at Permit History page');
        });

        it('Should navigate to Company Details page using breadcrumbs.', () => {
            permitHistoryPage.clickBreadcrumbLink(2);
            companyDetailPage = new CompanyDetailPage();
            browser.wait(protractor.ExpectedConditions.invisibilityOf(companyDetailPage.loadingMessageElement));
            expect(companyDetailPage.companyDetailsPageTitleElement.isDisplayed()).toBe(true, 'Expected breadcrumb to arrive at Company Details page');
        });

        it('Should navigate to Cigar Assessment page using breadcrumbs.', () => {
            companyDetailPage.clickBreadcrumbLink(1);
            cigarAssessmentsPage = new CigarAssessmentPage();
            browser.wait(protractor.ExpectedConditions.invisibilityOf(cigarAssessmentsPage.loadingMessageElement));

            expect(cigarAssessmentsPage.pageTitleElement.isDisplayed()).toBe(true, 'Expected breadcrumb to arrive at Cigar Assessment page');
        });

        it('Should navigate to Assessment page using breadcrumbs.', () => {
            cigarAssessmentsPage.clickBreadcrumbLink(0);
            assessmentPage = new AssessmentsPage();
            browser.wait(protractor.ExpectedConditions.invisibilityOf(assessmentPage.loadingQAElement));
            browser.wait(protractor.ExpectedConditions.invisibilityOf(assessmentPage.loadingCAElement));
            browser.wait(protractor.ExpectedConditions.invisibilityOf(assessmentPage.loadingATAElement));

            expect(assessmentPage.pageTitleElement.isDisplayed()).toBe(true, 'Expected breadcrumb to arrive at Assessment page');
        });

        it('Should display error messages when fiscal year value is not entered while creating Cigar assessment.', () => {
            createCigarAssessmentPage = assessmentPage.clickCreateCigarAssessmentButton();
            createCigarAssessmentPage.clickCreateButton();

           expect(createCigarAssessmentPage.fiscalYearRequiredErrorElement.isDisplayed()).toBe(true, 'Expected Fiscal Year error message to be displayed');
        });

         //TODO: cigar assesment creation scenario is not feasible, because once it's created it can no longer be created again.
        
        it('Should show assessment existing error message when trying to create already existing cigar assessment.', () => {
            createCigarAssessmentPage.fiscalYearFieldElement.sendKeys('2017');
            createCigarAssessmentPage.clickCreateButton();
            
            expect(createCigarAssessmentPage.assesmentAlreadyExistingErrorElement.isDisplayed()).toBe(true, 'Expected error message stating assesment already exists');
         });
    
    });

});
import { browser } from 'protractor';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { StartPage } from '../page/start.po';
import { PermitHistoryPage } from '../page/permit-history.po';
import { CompanyDetailPage } from '../page/company-detail.po';
import { ScriptUtils } from '../util/script-utils';
import { BreadcrumbNavigationComponent } from '../component/navigation.co';
import { CreatePermitPage } from '../page/create-permits.po';
import { protractor } from 'protractor/built/ptor';

describe('Company Details Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let permitHistoryPage: PermitHistoryPage;
    let companyDetailsPage: CompanyDetailPage;
    let breadcrumbNavigation: BreadcrumbNavigationComponent;
    let createPermitPage: CreatePermitPage;
    let permitNumber = 'FL-TI-00993';

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            dailyOperationsPage = loginPage.clickConfirmButton();
            dailyOperationsPage.goToCompanyDetailsPage('Dufry Free Seattle Jv');
            permitHistoryPage = new PermitHistoryPage(); 
            companyDetailsPage = new CompanyDetailPage();
            breadcrumbNavigation = new BreadcrumbNavigationComponent();
            createPermitPage = new CreatePermitPage();
        });
    });

    it('Should be able to navigate to a Inactive Company Details page.', () => {
        expect(companyDetailsPage.getCompanyDetailPageTitle()).toEqual('Company Details', 'Expect Company Details title to be displayed');
        breadcrumbNavigation.clickDailyOperationsLink()
    });

    it('Should be able to navigate to a Active Company Details page.', () => {
        dailyOperationsPage.goToCompanyDetailsPage('If, LLC');
        expect(companyDetailsPage.getCompanyDetailPageTitle()).toEqual('Company Details', 'Expect Company Details title to be displayed');
    });

    it('Validate that all valid fields display on company details landing.', () => {
        expect(companyDetailsPage.companyNameElement.getText()).toEqual('If, Llc', 'Expect Company Name to be displayed');
        expect(companyDetailsPage.einElement.getText()).toEqual('47-3539597', 'Expect Company EIN to be displayed');
        // expect(companyDetailsPage.createdElement.getText()).toEqual('04/10/2020', 'Expect Company Creation Date to be displayed');
        expect(companyDetailsPage.createdElement.getText()).toEqual('08/11/2020', 'Expect Company Creation Date to be displayed');
        expect(companyDetailsPage.statusElement.getText()).toEqual('Active', 'Expect Company Status to be displayed');
    });

    it('Validate Edit Company Popup displays.', () => {
        companyDetailsPage.clickEditCompanyPopUp();
        expect(companyDetailsPage.editCompanyTitleElement.getText()).toEqual('Edit Company', 'Expect Edit Company popup to be displayed');
    });

    it('Validate Edit Company fields are editable.', () => {
        companyDetailsPage.setCompanyName('Edited Company');
        companyDetailsPage.setCompanyEIN('1234567890');
        companyDetailsPage.editCompanyStatusElement.click();
        expect(companyDetailsPage.editCompanyNameElement.getAttribute('value')).toEqual('Edited Company', 'Expect Company Name to be edited');
        expect(companyDetailsPage.editEINElement.getAttribute('value')).toEqual('123456789', 'Expect Company EIN to be edited and only 9 numbers to be displayed');
        expect(companyDetailsPage.editCompanyStatusElement.getText()).toEqual(' Active ', 'Expect Active Company Status to be displayed');
    });

    it('Validate Edit Company popup can be cancelled.', () => {
        companyDetailsPage.cancelCompanyPopUpElement.click();
        expect(companyDetailsPage.getCompanyDetailPageTitle()).toEqual('Company Details', 'Expect Edit Company popup to be cancelled');
    });

    it('Validate Edit Company and Save.', () => {
        companyDetailsPage.clickEditCompanyPopUp();
        companyDetailsPage.setCompanyName('Edited Company');
        companyDetailsPage.saveCompanyPopUpElement.click();
        companyDetailsPage.getCompanyDetailPageTitle();
        companyDetailsPage.waitForLoadingCompany('Edited Company');
        expect(companyDetailsPage.companyNameElement.getText()).toEqual('Edited Company', 'Expect Company Name is edited');
        
        //Reverting back to original EIN
        companyDetailsPage.clickEditCompanyPopUp();
        companyDetailsPage.setCompanyName('If, Llc');
        companyDetailsPage.saveCompanyPopUpElement.click();
        companyDetailsPage.getCompanyDetailPageTitle();
    });

    it('Validate user can save a comment.', () => {
        companyDetailsPage.setComments('Test Comments');
        companyDetailsPage.clickSaveComments();
        expect(companyDetailsPage.commentsElement.getAttribute('value')).toEqual('Test Comments', 'Expect comment is added');
    });

    it('Validate user cannot save a comment more than 4000 characters.', () => {
        let comment = ScriptUtils.generateRandomString(401);
        companyDetailsPage.setLongComments(comment);
        companyDetailsPage.clickSaveComments();
        companyDetailsPage.commentsElement.getAttribute('value').then(function(value){
            expect(value.length).toEqual(4000, 'Expect comment cannot be more than 4000 in length');
        });        
    });

    it('Validate user can edit the comment.', () => {
        companyDetailsPage.setComments('Test Comments');
        companyDetailsPage.clickSaveComments();
        expect(companyDetailsPage.commentsElement.getAttribute('value')).toEqual('Test Comments', 'Expect comment is edited'); 
    });

    it('Validate comment should be saved when breadcrumbing.', () => {
        breadcrumbNavigation.clickDailyOperationsLink();
        companyDetailsPage.waitForPleaseWait();
        dailyOperationsPage.firstCompanySearchResultElement.click();
        companyDetailsPage.waitForLoading();
        expect(companyDetailsPage.commentsElement.getAttribute('value')).toEqual('Test Comments', 'Expect comment is saved when breadcrumbing');
    });

    it('Validate permits are in the ascending order.', () => {
        expect(companyDetailsPage.permitNumberAscElement.isPresent()).toBe(true, 'Expect permits are in ascending order');
    });

    it('Validate permit table headers in permit panel.', () => {
        expect(companyDetailsPage.permitTableHeaderElements.get(0).getText()).toEqual('TTB PERMIT', 'Expect TTB PERMIT Column in permit panel');
        expect(companyDetailsPage.permitTableHeaderElements.get(1).getText()).toEqual('PERMIT STATUS', 'Expect PERMIT STATUS Column in permit panel');
        expect(companyDetailsPage.permitTableHeaderElements.get(2).getText()).toEqual('TUFA ACTIVE DATE', 'Expect TUFA ACTIVE DATE Column in permit panel');
        expect(companyDetailsPage.permitTableHeaderElements.get(3).getText()).toEqual('TUFA INACTIVE DATE', 'Expect TUFA INACTIVE DATE Column in permit panel');
        expect(companyDetailsPage.permitTableHeaderElements.get(4).getText()).toEqual('TTB ISSUE DATE', 'Expect TTB ISSUE DATE Column in permit panel');
        expect(companyDetailsPage.permitTableHeaderElements.get(5).getText()).toEqual('TTB CLOSED DATE', 'Expect TTB CLOSED DATE Column in permit panel');
    });

    it('Validate Add Permit popup.', () => {
        createPermitPage.clickAddPermitButton();
        expect(createPermitPage.TTBPermitElement.isPresent()).toBe(true, 'Expect TTB Permit Field to be displayed');
        expect(createPermitPage.selectTypeImpOptionElement.isPresent()).toBe(true, 'Expect Type to be displayed');
        expect(createPermitPage.tufaActiveDateElement.isPresent()).toBe(true, 'Expect TTB Active Date to be displayed');
        expect(createPermitPage.savePermitElement.isPresent()).toBe(true, 'Expect Save Permit button to be displayed');
        expect(createPermitPage.cancelPermitElement.isPresent()).toBe(true, 'Expect Cancel button to be displayed');
    });

    it('Validate all error messages in Add permit popup.', () => {
        createPermitPage.savePermitElement.click();
        browser.sleep(1000);
        try{
            companyDetailsPage.applicationErrorElement.click();
        }
        catch{
            console.log("no application error");
        }
        expect(createPermitPage.TTBPermitErrorElement.isPresent()).toBe(true, 'Expect TTB Permit Error to be displayed');
        expect(createPermitPage.typeErrorElement.isPresent()).toBe(true, 'Expect Type error to be displayed');
        expect(createPermitPage.tufaActiveDateErrorElement.isPresent()).toBe(true, 'Expect TTB Active Date Error to be displayed');
    });

    it('Validate Type error messages in Add permit popup.', () => {
        createPermitPage.setTTBPermit('NE-WL-12345');
        createPermitPage.setTufaActiveDate('03/31/2018');
        createPermitPage.savePermitElement.click();
        expect(createPermitPage.typeErrorElement.isPresent()).toBe(true, 'Expect Type error to be displayed');
    });

    it('Validate TTB Permit error messages in Add permit popup.', () => {
        createPermitPage.setTTBPermit('');
        createPermitPage.selectTypeImpOption();
        createPermitPage.setTufaActiveDate('03/31/2018');
        createPermitPage.savePermitElement.click();
        expect(createPermitPage.TTBPermitErrorElement.isPresent()).toBe(true, 'Expect TTB Permit Error to be displayed');
    });

    it('Validate Tufa Active Date error messages in Add permit popup.', () => {
        createPermitPage.cancelPermitElement.click();
        companyDetailsPage.waitForLoading();
        createPermitPage.clickAddPermitButton();
        createPermitPage.setTTBPermit('NE-WL-12345');
        createPermitPage.selectTypeImpOption();        
        createPermitPage.savePermitElement.click();
        expect(createPermitPage.tufaActiveDateErrorElement.isPresent()).toBe(true, 'Expect TTB Active Date Error to be displayed');
    });

    it('Validate Tufa Active Date cannot be future date.', () => {
        browser.sleep(1000);
        createPermitPage.setTufaActiveDate('03/31/2030');
        expect(createPermitPage.tufaActiveDateElement.getAttribute('value')).not.toEqual('03/31/2030', 'Expect TTB Active Date cannot be future date');
        expect(createPermitPage.tufaActiveDateElement.getAttribute('value')).toEqual(ScriptUtils.currentDate(), 'Expect TTB Active Date cannot be future date');
    });

    it('Validate Save Permit.', () => {
        createPermitPage.setTTBPermit(permitNumber);
        createPermitPage.selectTypeImpOption();
        createPermitPage.setTufaActiveDate('03/31/2018');
        createPermitPage.savePermitElement.click();
        companyDetailsPage.waitForLoading();
        createPermitPage.savePermitElement.isDisplayed().then(isPresent => {
            if (isPresent){
                browser.sleep(2000);
                console.log('inside isPresent if');
                createPermitPage.savePermitElement.click();
                companyDetailsPage.waitForLoading();         
            }
        });
        browser.sleep(2000);
        expect(companyDetailsPage.getPermit(permitNumber).getText()).toEqual(permitNumber, 'Expect new permit to be created');
    });   

    it('Validate Cancel Permit.', () => {
        createPermitPage.clickAddPermitButton();
        createPermitPage.setTTBPermit('NE-WP-99999');
        createPermitPage.selectTypeImpOption();
        createPermitPage.setTufaActiveDate('03/31/2018');
        createPermitPage.cancelPermitElement.click();
        companyDetailsPage.waitForLoading();
        expect(companyDetailsPage.getPermitCancel('NE-WP-99999').isPresent()).toBe(false, 'Expect no new permit is created');
    });

    it('Validate warning message when duplicate permit is added.', () => {
        createPermitPage.clickAddPermitButton();
        createPermitPage.setTTBPermit(permitNumber);
        createPermitPage.selectTypeImpOption();
        createPermitPage.setTufaActiveDate('03/31/2018');
        createPermitPage.savePermitElement.click();
        expect(createPermitPage.getWarningMessage().isPresent()).toBe(true, 'Expect warning message when adding duplicate permit');
        createPermitPage.cancelPermitElement.click();
        companyDetailsPage.waitForLoading();
    });

    it('Validate permit number cannot consist of only numbers.', () => {
        createPermitPage.clickAddPermitButton();
        createPermitPage.setTTBPermit('12345678');
        expect(createPermitPage.TTBPermitElement.getAttribute('value')).toBe('', 'Expect permit number cannot consist of only numbers');
    });

    it('Validate Importer Type is autopopulated when Permit number starts with FL-TI.', () => {
        createPermitPage.setTTBPermit('FL-TI-');
        expect(createPermitPage.selectTypeImpOptionElement.isSelected()).toBe(true, 'Expect Importer Type to be autopopulated when Permit number starts with FL-TI.');
    });

    it('Validate Manufacturer Type is autopopulated when Permit number starts with TP-FL.', () => {
        createPermitPage.setTTBPermit('TP-FL-');
        expect(createPermitPage.selectTypeManuOptionElement.isSelected()).toBe(true, 'Expect Manufacturer Type to be autopopulated when Permit number starts with TP-FL.');
        createPermitPage.cancelPermitElement.click();
        companyDetailsPage.waitForLoading();
    });

    it('Validate click and navigation to Permit', () => {
        companyDetailsPage.clickonPermit(permitNumber);
        permitHistoryPage.waitForPermitHistoryTitle();
        browser.waitForAngular().then(function () {
            expect(permitHistoryPage.permitHistoryTitleElement.isDisplayed()).toBe(true, 'Expect Permit History to be displayed after clicking on permit');
        });
    });

    it('Validate navigation to company Details page', () => {
        breadcrumbNavigation.companyDetailsPermitHistoryBCElement.click();
        companyDetailsPage.waitForLoading();
        expect(companyDetailsPage.getCompanyDetailPageTitle()).toEqual('Company Details', 'Expect Company Details title to be displayed after navigation');
    });

    it('Validate permit can be excluded', () => {
        companyDetailsPage.excludePanelElement.click();
        companyDetailsPage.getSelectPermitNumberDropdown(permitNumber).click();
        companyDetailsPage.assessmentFYElement.sendKeys('2016');
        companyDetailsPage.assessmentQuarterExcludedQ1.click();
        companyDetailsPage.waitForLoading();
        companyDetailsPage.commentsExcludeElement.sendKeys('Exclude comments');
        companyDetailsPage.saveExcludeElement.click();        
        companyDetailsPage.waitForLoading();
        expect(companyDetailsPage.getPermitExcludeStatus(permitNumber).isDisplayed()).toBe(true, 'Expect Permit Status is excluded.');
    });

    it('Validate edit button for primary address', () => {
        companyDetailsPage.editPrimaryAddressElement.click();
        expect(companyDetailsPage.getEditCompanyAddressTitle().isDisplayed()).toBe(true, 'Expect Edit button to be displayed for primary address.');
    });
    
    it('Validate fields in edit primary address popup', () => {
        browser.sleep(2000);
        expect(companyDetailsPage.editAttentionCompanyAddressElement.isPresent()).toBe(true, 'Expect Attention field in edit primary address popup.');
        expect(companyDetailsPage.editStreetAddressCompanyAddressElement.isPresent()).toBe(true, 'Expect Street Address field in edit primary address popup.');
        expect(companyDetailsPage.editSuiteCompanyAddressElement.isPresent()).toBe(true, 'Expect Suite field in edit primary address popup.');
        expect(companyDetailsPage.editCityCompanyAddressElement.isPresent()).toBe(true, 'Expect City field in edit primary address popup.');
        expect(companyDetailsPage.editStateCompanyAddressElement.isPresent()).toBe(true, 'Expect State field in edit primary address popup.');
        expect(companyDetailsPage.editPostalCodeCompanyAddressElement.isPresent()).toBe(true, 'Expect Postal Code field in edit primary address popup.');
        expect(companyDetailsPage.editCountryCompanyAddressElement.isPresent()).toBe(true, 'Expect Country field in edit primary address popup.');
    });

    it('Validate United States to be selected in edit address', () => {
        expect(companyDetailsPage.selectUnitedStatesElement.isSelected()).toBe(true, 'Expect United States to be selected.');
    });

    it('Validate United States to be selected in edit address', () => {
        companyDetailsPage.editPostalCodeCompanyAddressElement.clear();
        companyDetailsPage.editPostalCodeCompanyAddressElement.sendKeys('12345678901');
        companyDetailsPage.editPostalCodeCompanyAddressElement.getAttribute('value').then(function(value){
                    expect(value.length).toEqual(10, 'Expect postal code to be of 10 length');
        });
    });

    it('Validate No fields are required to save address', () => {
        companyDetailsPage.editAttentionCompanyAddressElement.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        companyDetailsPage.editAttentionCompanyAddressElement.sendKeys(protractor.Key.DELETE);
        companyDetailsPage.editStreetAddressCompanyAddressElement.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        companyDetailsPage.editStreetAddressCompanyAddressElement.sendKeys(protractor.Key.DELETE);
        companyDetailsPage.editSuiteCompanyAddressElement.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        companyDetailsPage.editSuiteCompanyAddressElement.sendKeys(protractor.Key.DELETE);
        companyDetailsPage.editCityCompanyAddressElement.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        companyDetailsPage.editCityCompanyAddressElement.sendKeys(protractor.Key.DELETE);
        companyDetailsPage.selectNoCityElement.click();
        companyDetailsPage.editPostalCodeCompanyAddressElement.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        companyDetailsPage.editPostalCodeCompanyAddressElement.sendKeys(protractor.Key.DELETE);
        companyDetailsPage.saveCompanyAddressElement.click();
        companyDetailsPage.waitForLoading();
        expect(companyDetailsPage.editPrimaryAddressElement.isDisplayed()).toBe(true, 'Expect No fields are required to save address.');
    });

    it('Validate edit button for alternate address', () => {
        companyDetailsPage.editAlternateAddressElement.click();
        expect(companyDetailsPage.getEditCompanyAddressTitle().isDisplayed()).toBe(true, 'Expect Edit button to be displayed for alternate address.');
    });

    it('Validate fields in edit Alternate address popup', () => {
        expect(companyDetailsPage.editAttentionCompanyAddressElement.isPresent()).toBe(true, 'Expect Attention field in edit alternate address popup.');
        expect(companyDetailsPage.editStreetAddressCompanyAddressElement.isPresent()).toBe(true, 'Expect Street Address field in edit alternate address popup.');
        expect(companyDetailsPage.editSuiteCompanyAddressElement.isPresent()).toBe(true, 'Expect Suite field in edit alternate address popup.');
        expect(companyDetailsPage.editCityCompanyAddressElement.isPresent()).toBe(true, 'Expect City field in edit alternate address popup.');
        expect(companyDetailsPage.editStateCompanyAddressElement.isPresent()).toBe(true, 'Expect State field in edit alternate address popup.');
        expect(companyDetailsPage.editPostalCodeCompanyAddressElement.isPresent()).toBe(true, 'Expect Postal Code field in edit alternate address popup.');
        expect(companyDetailsPage.editCountryCompanyAddressElement.isPresent()).toBe(true, 'Expect Country field in edit alternate address popup.');
        browser.sleep(1000);
        companyDetailsPage.cancelCompanyAddressElement.click();
        companyDetailsPage.waitForLoading();
    });

    it('Validate Add Contact button is displayed', () => {
        expect(companyDetailsPage.addContactButtonElement.isDisplayed()).toBe(true, 'Expect Add Contact button to be displayed.');
    });

    it('Validate Save Contact button is disabled by default', () => {
        companyDetailsPage.waitForAddContactPOC();
        browser.sleep(1000);
        companyDetailsPage.addContactButtonElement.click();
        expect(companyDetailsPage.getSaveContactButtonElement().isEnabled()).toBe(false, 'Expect Save Contact button to be disabled.');
    });

    it('Validate Contact field values should be in uppercase', () => {
        companyDetailsPage.firstNameContactElement.sendKeys('John');
        companyDetailsPage.lastNameContactElement.sendKeys('Smith');
        companyDetailsPage.emailContactElement.sendKeys('johnsmith');
        companyDetailsPage.firstNameContactElement.getText().then(function(value){
            expect(value).toEqual(value.toUpperCase(), 'Expect first name to be in uppercase.');
        });
        companyDetailsPage.lastNameContactElement.getText().then(function(value){
            expect(value).toEqual(value.toUpperCase(), 'Expect last name to be in uppercase.');
        });
        companyDetailsPage.emailContactElement.getText().then(function(value){
            expect(value).toEqual(value.toUpperCase(), 'Expect email to be in uppercase.');
        });
    });

    it('Validate country code is auto populated.', () => {        
        expect(companyDetailsPage.countryCodeContactElement.getAttribute('value')).toEqual('+1', 'Expect country code to be auto populated.');
    });

    it('Validate email format error', () => {   
        companyDetailsPage.saveContactButtonElement.click();    
        expect(companyDetailsPage.emailContacErrortElement.isDisplayed()).toBe(true, 'Expect email should be in right format.');
    });

    it('Validate Delete button is display for Primary POC', () => {
        companyDetailsPage.cancelcontactButtonElement.click();
        companyDetailsPage.waitForLoading();
        companyDetailsPage.addContactButtonElement.click();
        companyDetailsPage.getSaveContactButtonElement();
        companyDetailsPage.firstNameContactElement.sendKeys('John');
        companyDetailsPage.lastNameContactElement.sendKeys('Smith');
        companyDetailsPage.saveContactButtonElement.click(); 
        companyDetailsPage.waitForLoading();
        companyDetailsPage.waitForDeleteContactPOC();
        expect(companyDetailsPage.deleteContactButtonElement.get(0).isDisplayed()).toBe(false, 'Expect Delete button should not display for Primary POC.');
        expect(companyDetailsPage.deleteContactButtonElement.get(1).isDisplayed()).toBe(true, 'Expect Delete button should display for other than Primary POC.');
    });

    it('Validate Edit POC', () => {
        companyDetailsPage.editContactButtonElement.get(1).click();
        expect(companyDetailsPage.getEditFirstNamePOC().getAttribute('value')).toEqual('JOHN', 'Expect JOHN in first name.');
        expect(companyDetailsPage.lastNameEditContactElement.getAttribute('value')).toEqual('SMITH', 'Expect SMITH in last name.');
        browser.sleep(1000);
        companyDetailsPage.saveButtonEditPOCElement.click(); 
        companyDetailsPage.waitForLoading();
        companyDetailsPage.waitForDeleteContactPOC();     
    });

    it('Validate delete POC functionality cancel button is clicked', () => {  
        browser.sleep(2000);
        let deleteCount = companyDetailsPage.deleteContactButtonElement.count(); 
        browser.waitForAngular().then(function () {
            companyDetailsPage.deleteContactButtonElement.get(1).click();
        });    
        expect(companyDetailsPage.getDeleteContactPopUpElement().isDisplayed()).toBe(true, 'Expect confirmation pop up opens.');
        browser.sleep(2000);
        companyDetailsPage.cancelButtonDeleteContactPopUpElement.click();
        companyDetailsPage.waitForLoading();
        companyDetailsPage.waitForDeleteContactPOC();
        expect(companyDetailsPage.deleteContactButtonElement.count()).toEqual(deleteCount, 'Expect no deletion when cancel action is taken.');
    });
    
    it('Validate delete POC functionality confirm button is clicked', () => { 
        let deleteCountAfter = companyDetailsPage.deleteContactButtonElement.count().then(function(value){return value-1;}); 
        companyDetailsPage.deleteContactButtonElement.get(1).click();
        companyDetailsPage.getDeleteContactPopUpElement();
        browser.sleep(2000);
        companyDetailsPage.confirmButtonDeleteContactPopUpElement.click();
        companyDetailsPage.waitForLoading();
        expect(companyDetailsPage.deleteContactButtonElement.count()).not.toEqual(deleteCountAfter, 'Expect deletion when confirm action is taken.');
    });
});
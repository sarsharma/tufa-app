import { browser } from "protractor";
import { BreadcrumbNavigationComponent, SideNavigtionComponent } from "../component/navigation.co";
import { AssessmentsPage } from "../page/assessments/assessments.po";
import { CreateAnnualTrueUpPage } from "../page/create-annual-true-up.po";
import { LoginPage } from "../page/login.po";
import { StartPage } from "../page/start.po";

describe('Create Annual True-Up Assessment Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentsPage: AssessmentsPage;
    let breadcrumbNav: BreadcrumbNavigationComponent;
    let createAnnualTrueUpPage: CreateAnnualTrueUpPage;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            loginPage.clickConfirmButton();
            sideNavigtionComponent = new SideNavigtionComponent();
            assessmentsPage = sideNavigtionComponent.clickAssessmentsNavElement();
            breadcrumbNav = new BreadcrumbNavigationComponent();
            createAnnualTrueUpPage = assessmentsPage.clickCreateAnnualTrueUp();
        });
    });


    it('Should navigate back to the Assessments page', () => {
        breadcrumbNav.clickAssessmentsLink();
        expect(assessmentsPage.createAnnualTrueUpButton.isDisplayed()).toBe(true, 'Create True-Up button is displayed');
        assessmentsPage.clickCreateAnnualTrueUp();
        expect(createAnnualTrueUpPage.fiscalYearFieldElement.isDisplayed()).toBe(true, 'Expect fiscal year to be displayed');
    });
});
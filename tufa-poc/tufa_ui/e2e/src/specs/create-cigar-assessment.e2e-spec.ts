import { browser } from "protractor";
import { BreadcrumbNavigationComponent, SideNavigtionComponent } from "../component/navigation.co";
import { AssessmentsPage } from "../page/assessments/assessments.po";
import { CreateCigarAssessmentPage } from "../page/create-cigar-assessment.po";
import { LoginPage } from "../page/login.po";
import { StartPage } from "../page/start.po";


describe('Create Cigar Assessment Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentsPage: AssessmentsPage;
    let createCigarAssessmentPage: CreateCigarAssessmentPage;
    let breadcrumbNav: BreadcrumbNavigationComponent;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            loginPage.clickConfirmButton();
            sideNavigtionComponent = new SideNavigtionComponent();
            assessmentsPage = sideNavigtionComponent.clickAssessmentsNavElement();
            createCigarAssessmentPage = assessmentsPage.clickCreateCigarAssessmentButton();
            breadcrumbNav = new BreadcrumbNavigationComponent();
        });
    });

    it('Should navigate back to the Assessments page', () => {
        breadcrumbNav.clickAssessmentsLink();
        expect(assessmentsPage.createAnnualTrueUpButton.isDisplayed()).toBe(true, 'Create True-Up button is displayed');
        assessmentsPage.clickCreateCigarAssessmentButton();
        expect(createCigarAssessmentPage.fiscalYearFieldElement.isDisplayed()).toBe(true, 'Expect fiscal year to be displayed');
    });
});
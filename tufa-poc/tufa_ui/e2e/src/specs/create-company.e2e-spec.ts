import { browser } from 'protractor';
import { LoginPage } from '../page/login.po';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { CreateCompanyPage } from '../page/create-company.po';
import { StartPage } from '../page/start.po';
import { CompanyDetailPage } from '../page/company-detail.po';
import { ScriptUtils } from '../util/script-utils';

describe('Create Company Scenarios', () => {
  let startPage: StartPage;
  let loginPage: LoginPage;
  let dailyOperationsPage: DailyOperationsPage;
  let createCompanyPage: CreateCompanyPage;

  beforeAll(() => {
    browser.waitForAngularEnabled(false).then(function() {
      startPage = new StartPage().get(process.env.npm_config_url);
      loginPage = startPage.goToLoginPage();
      dailyOperationsPage = loginPage.clickConfirmButton();
      createCompanyPage = dailyOperationsPage.clickCreateCompanyButton();
    }); 
  });

  it('Should display error messages on all required fields when clicking Save Company button without entering required fields.', () => {
    createCompanyPage.clickSaveCompanyButton();
    
    expect(createCompanyPage.companyNameErrorMessageElement.isDisplayed()).toBe(true, 'Expected error message for company name field to be visible.');
    expect(createCompanyPage.einNumberErrorMessageElement.isDisplayed()).toBe(true, 'Expected error message for EIN Number field to be visible.');
    expect(createCompanyPage.ttbPermitErrorMessageElement.isDisplayed()).toBe(true, 'Expected error message for TTB Permit field to be visible.');
    expect(createCompanyPage.typeErrorMessageElement.isDisplayed()).toBe(true, 'Expected error message for Type field to be visible.');
    expect(createCompanyPage.activeDateErrorMessageElement.isDisplayed()).toBe(true, 'Expected error message for TUFA Active Date field to be visible.');
  });
  
  it('Should be unable to create a company with an already existing EIN number.', () => {
    createCompanyPage.clearAllFields();
    createCompanyPage.setCompanyName('Automated Company 1');
    createCompanyPage.setEin('473539597'); //already existing EIN
    createCompanyPage.setTtbPermit('AA-BB-12345');
    createCompanyPage.selectImporterTypeOption();
    createCompanyPage.setActiveDate('03/20/2020');
    createCompanyPage.clickSaveCompanyButton();    

    expect(createCompanyPage.existingEinNumberErrorMessageElement.isDisplayed()).toBe(true, 'Expected error message stating that EIN number is already existing.');
  });

  it('Should auto-populate Type field to Importer value when TTB permit has "TI" as second set of letters.', () => {
    createCompanyPage.clearAllFields().setTtbPermit('FL-TI-12345');

    expect(createCompanyPage.typeDropdownElement.getAttribute('value')).toEqual('Importer', 'Expected Type Dropdown value to be Importer.');
  });

  it('Should auto-populate Type field to Manufacturer value when TTB permit has "TP" as first set of letters.', () => {
    createCompanyPage.clearAllFields().setTtbPermit('TP-FL-12345');

    expect(createCompanyPage.typeDropdownElement.getAttribute('value')).toEqual('Manufacturer', 'Expected Type Dropdown value to be Manufacturer.');
  });
  
  it('Should enter a future date in TUFA Active Date field, then verify the date was switched back to today.', () => {
    let today = new Date();
    let todayDate = String(today.getMonth() + 1).padStart(2, '0') + '/' +  String(today.getDate()).padStart(2, '0') + '/' + today.getFullYear();
    createCompanyPage.clearAllFields().setActiveDate('12/31/9999');

    expect(createCompanyPage.getActiveDate()).toEqual(todayDate, 'Expected future date entered to switch to today\'s date.');
  });
  
  it('Should be able to create a new company with a unique EIN, and arrive at Company Details page.', () => {
    let uniqueEid = ScriptUtils.generateUniqueEin();
    console.log('Unique EID number: ' + uniqueEid);
    createCompanyPage.clearAllFields();
    createCompanyPage.setCompanyName('Automated Company 1');
    createCompanyPage.setEin(uniqueEid);
    createCompanyPage.setTtbPermit('AA-BB-12345');
    createCompanyPage.selectImporterTypeOption();
    createCompanyPage.setActiveDate('03/20/2020');
    createCompanyPage.clickSaveCompanyButton();    

    //TODO: sleeps added for DEMO purposes only, to be deleted.
    // browser.sleep(5000).then(function() {
    //   console.log('Slept 5 seconds');
    // });


    expect(CompanyDetailPage.pageTitleElement.isDisplayed()).toBe(true, 'Expected Company Detail page to be visible.');
  });
});

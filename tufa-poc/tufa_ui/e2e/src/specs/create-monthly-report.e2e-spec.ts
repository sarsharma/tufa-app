import { browser } from 'protractor';
import { CreateMonthlyReportPage } from '../page/create-monthly-report.po';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { StartPage } from '../page/start.po';

describe('Create Monthly Report Scenarios', () => {
  let startPage: StartPage;
  let loginPage: LoginPage;
  let dailyOperationsPage: DailyOperationsPage;
  let createMonthlyReportPage: CreateMonthlyReportPage;

  beforeAll(() => {
    browser.waitForAngularEnabled(false).then(function () {
      startPage = new StartPage().get(process.env.npm_config_url);
      loginPage = startPage.goToLoginPage();
      dailyOperationsPage = loginPage.clickConfirmButton();
      createMonthlyReportPage = dailyOperationsPage.clickCreateMonthlyReportButton();
    });
  });


  it('Should be able to create a Monthly Report.', () => {
    createMonthlyReportPage.clearAllFields();
    createMonthlyReportPage.setFiscalYear('2030');
    createMonthlyReportPage.selectFebruaryTypeOption();
    createMonthlyReportPage.clickMonthlyReportButton();

    // browser.takeScreenshot().then(png => ScriptUtils.writeScreenShot(png, 'init'));
    expect(createMonthlyReportPage.getFiscalYear()).toEqual('2030', 'Expected MR Created.');
  });
  
  it('Should be able to create a Monthly Report.', () => {
    createMonthlyReportPage.clearAllFields();
    createMonthlyReportPage.createMonthlyReport('2031', 'March');
    expect(createMonthlyReportPage.getAlertMessage()).toBe(true, 'Expected MR Created.');
  });
});

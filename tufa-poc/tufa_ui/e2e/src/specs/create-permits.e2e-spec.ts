import { browser } from 'protractor';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { StartPage } from '../page/start.po';
import { CreatePermitPage } from '../page/create-permits.po';

describe('Create Permit Scenarios', () => {
  let startPage: StartPage;
  let loginPage: LoginPage;
  let dailyOperationsPage: DailyOperationsPage;
  let createPermitPage: CreatePermitPage;

  beforeAll(() => {
    browser.waitForAngularEnabled(false).then(function () {
      startPage = new StartPage().get(process.env.npm_config_url);
      loginPage = startPage.goToLoginPage();
      dailyOperationsPage = loginPage.clickConfirmButton();      
      createPermitPage = dailyOperationsPage.goToCompanyDetailsPage('America');
    });
  });

  it('Should be able to create a Permit.', () => {
    createPermitPage.clickAddPermitButton();
    createPermitPage.setTTBPermit('TT-FL-80001');
    createPermitPage.selectTypeImpOption();
    createPermitPage.setTufaActiveDate('03/31/2018');
    createPermitPage.clickSavePermit();
    expect(createPermitPage.addPermitElement.isPresent()).toBe(true, 'Expected Permit Created.');
  });

});
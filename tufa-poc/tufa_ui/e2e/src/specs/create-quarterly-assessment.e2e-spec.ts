import { browser } from "protractor";
import { SideNavigtionComponent } from "../component/navigation.co";
import { AssessmentsPage } from "../page/assessments/assessments.po";
import { CreateQuarterlyAssessmentPage } from "../page/create-quarterly-assessment.po";
import { LoginPage } from "../page/login.po";
import { StartPage } from "../page/start.po";


describe('Create Quarterly Assessment Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentPage: AssessmentsPage;
    let createQuarterlyAssessmentPage: CreateQuarterlyAssessmentPage;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            loginPage.clickConfirmButton();
            sideNavigtionComponent = new SideNavigtionComponent();
            assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
            createQuarterlyAssessmentPage = assessmentPage.clickCreateQuarterlyAssessmentButton();
        });
    });
});
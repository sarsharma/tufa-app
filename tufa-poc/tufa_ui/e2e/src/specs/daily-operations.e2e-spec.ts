import { browser, ElementFinder } from 'protractor';
import { BreadcrumbNavigationComponent } from "../component/navigation.co";
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { StartPage } from '../page/start.po';


describe('Create Daily Operations Scenarios', () => {
  let startPage: StartPage;
  let loginPage: LoginPage;
  let dailyOperationsPage: DailyOperationsPage;
  let breadcrumbNav: BreadcrumbNavigationComponent;

  beforeAll(() => {
    browser.waitForAngularEnabled(false).then(function () {
      startPage = new StartPage().get(process.env.npm_config_url);
      loginPage = startPage.goToLoginPage();
      dailyOperationsPage = loginPage.clickConfirmButton();
      breadcrumbNav = new BreadcrumbNavigationComponent();
    });
  });

  it('Validate landing page', () => {
    expect(dailyOperationsPage.pageTitleElementDaily.isPresent()).toBe(true, 'Expected report search result is displayed');
  });

  it('Validate search results - Report', () => {
    // dailyOperationsPage.setSearchText('123456789');
    dailyOperationsPage.setSearchText('435365234');
    dailyOperationsPage.clickSearchButton();
    expect(dailyOperationsPage.geteinReportSearchResult()).toEqual('43-5365234', 'Expected report with the entered ein is present');
  });

  it('Validate default Daily Operations Sort', async () => {
    dailyOperationsPage.setSearchText('AMERICA');
    dailyOperationsPage.clickSearchButton();
    let cellElements = dailyOperationsPage.allPermitNumbers;
    let textFields = await cellElements
      .map((el: ElementFinder) => el.getText()
        .then((text: string) => text));

    textFields.forEach((description: string, index: number) => {
      if (index > 0) {
        expect(textFields[index - 1] >= description).toBe(true, 'Expected permit numbers are in descending order');
      }
    });
  });

  it('Validate sorting change applied', async () => {
    dailyOperationsPage.clickEinSortElement();
    let cellElements = dailyOperationsPage.getAllEinElement();
    let textFields = await cellElements
      .map((el: ElementFinder) => el.getText()
        .then((text: string) => text));

    textFields.forEach((description: string, index: number) => {
      if (index > 0) {
        expect(textFields[index - 1] <= description).toBe(true, 'Expected ein are in ascending order');
      }
    });
  });

  it('Validate sorting applied on Pagination', async () => {
    dailyOperationsPage.clickPagination3Element();
    let cellElements = dailyOperationsPage.getAllEinElement();
    let textFields = await cellElements
      .map((el: ElementFinder) => el.getText()
        .then((text: string) => text));

    textFields.forEach((description: string, index: number) => {
      if (index > 0) {
        expect(textFields[index - 1] <= description).toBe(true, 'Expected ein are in ascending order after using pagination');
      }
    });
  });

  it('Validate results retained when following breadcrumbs', async () => {
    dailyOperationsPage.setSearchText('AMERICA');
    dailyOperationsPage.clickSearchButton();
    dailyOperationsPage.clickPeriodOfActivityElement();
    breadcrumbNav.clickDailyOperationsLink();
    let pageTitle = dailyOperationsPage.pageTitleElementDaily.getText();
    expect(pageTitle).toBe('Daily Operations', 'Expected navigation to Daily Operation page after breacrumbing from Monthly Reports page.');
  });

  it('Validate sorting retained when following breadcrumbs', async () => {

    dailyOperationsPage.clickEinSortElement();
    browser.sleep(3000);

    // Get order of elements before navigation
    let textFields = await dailyOperationsPage.getAllEinElement()
      .map((el: ElementFinder) => el.getText()
        .then((text: string) => text));

    dailyOperationsPage.clickPeriodOfActivityElement();
    breadcrumbNav.clickDailyOperationsLink();
    browser.sleep(3000);

    // Get order of elements after navigation
    let textFields2 = await dailyOperationsPage.getAllEinElement()
      .map((e2: ElementFinder) => e2.getText()
        .then((text: string) => text));

    // Make sure order matches
    textFields.forEach((description: string, index: number) => {
      expect(textFields[index] == textFields2[index]).toBe(true, 'Expected EIN to be ascending order. Index: ' + index);
    });
  });

});
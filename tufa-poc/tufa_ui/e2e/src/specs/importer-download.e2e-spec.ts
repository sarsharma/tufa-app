import { browser, protractor } from 'protractor';
import { Form3852Component } from '../component/form-3852.co';
import { TobaccoClassSelectionComponent } from '../component/tobacco-class-selection.co';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { MonthlyReportPage } from '../page/monthly-report.po';
import { StartPage } from '../page/start.po';
import { ScriptUtils } from '../util/script-utils';
import * as fs from 'fs';
import * as path from 'path';
import { StaticValues } from '../util/static-values';
import { Form5220Component } from '../component/form-5220.co';
import { Form7501Component } from '../component/form-7501.co';

describe('Importer Monthly Report Download Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let monthlyReportPage: MonthlyReportPage;
    let tobaccoClassSelection: TobaccoClassSelectionComponent;
    let form3852Component: Form3852Component;
    let form5220Component: Form5220Component;
    let form7501Component: Form7501Component;
    let downloadsDir = path.resolve(__dirname, '../../data/downloaded-files/');
    let expectedFilesDir = path.resolve(__dirname, '../../data/expected-files/');

    beforeAll( () => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            dailyOperationsPage = loginPage.clickConfirmButton();
            dailyOperationsPage.setSearchText('43-5365234');
            dailyOperationsPage.clickSearchButton();
            dailyOperationsPage.clickExtendDisplayButton();
            //instead of clickExtendDisplayButton(), we can also filter by month by calling clickAprilMonthCheckbox()
            // dailyOperationsPage.clickAprilMonthCheckbox();
            monthlyReportPage = dailyOperationsPage.clickPeriodOfActivity('April FY 2016');
            tobaccoClassSelection = new TobaccoClassSelectionComponent();
            form3852Component = new Form3852Component();
            form5220Component = new Form5220Component();
            form7501Component = new Form7501Component();
        });
    });

    it('Should be able to navigate to an Importer Monthly Report.', () => {
        expect(monthlyReportPage.pageTitleElement.isDisplayed()).toBe(true, 'Expect Monthly Report title to be displayed');
    });

    it('Reset report and verify Report status is "Not Started"', () => {
        tobaccoClassSelection.resetSelection();
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            expect(monthlyReportPage.getReportStatusElement('Not Started')).toEqual('Not Started', 'Reset Report Completed');
        });
    });
    
    describe('Download Scenarios for 3852 form', () => {
        it('Should verify all form 3852 values are entered correctly.', () => {
            tobaccoClassSelection.clickSelectAllSelection();
            form3852Component.setCigarettesRemovalsAmount('1000');
            form3852Component.setCigarsRemovalsAmount('1000');
            form3852Component.setCigarsExciseTax('1000');
            form3852Component.setSnuffRemovalsAmount('5000');
            form3852Component.setChewRemovalsAmount('4000');
            form3852Component.setPipeRemovalsAmount('1000');
            form3852Component.setRollYourOwnRemovalsAmount('2000');

            expect(form3852Component.getCigarettesTaxDiffElement()).toEqual('$0.00', 'Expect Cigarettes Difference Tax is $0.00');
            expect(form3852Component.getCigarettesCallculatedTaxRateElement()).toEqual('50.33', 'Expect Cigarettes Calculated Tax Rate is 50.33');
            expect(form3852Component.getCigarettesStatusElement()).toEqual('Valid', 'Expect Cigarettes Activity as Valid');
            expect(form3852Component.getCigarsTaxDiffElement()).toEqual('', 'Expect Cigar Tax Difference to be blank');
            expect(form3852Component.getCigarsCallculatedTaxRateElement()).toEqual('N/A', 'Expect Cigar Calculated Tax Rate is N/A');
            expect(form3852Component.getCigarsStatusElement()).toEqual('', 'Expect Cigar Activity as blank');
            expect(form3852Component.getSnuffExciseTaxField()).toEqual('7,550.00', 'Expect Snuff Excise Tax is 7,550.00');
            expect(form3852Component.getSnuffTaxDiffElement()).toEqual('$0.00', 'Expect Snuff Difference Tax is $0.00');
            expect(form3852Component.getSnuffCallculatedTaxRateElement()).toEqual('1.51', 'Expect Snuff Calculated Tax Rate is 1.51');
            expect(form3852Component.getSnuffStatusElement()).toEqual('Valid', 'Expect Snuff Activity as Valid');
            expect(form3852Component.getChewExciseTaxField()).toEqual('2,013.20', 'Expect Chew Excise Tax is 2,013.20');
            expect(form3852Component.getChewTaxDiffElement()).toEqual('$0.00', 'Expect Chew Difference Tax is $0.00');
            expect(form3852Component.getChewCallculatedTaxRateElement()).toEqual('0.5033', 'Expect Chew Calculated Tax Rate is 0.5033');
            expect(form3852Component.getChewStatusElement()).toEqual('Valid', 'Expect Chew Activity as Valid');
            expect(form3852Component.getPipeExciseTax()).toEqual('2,831.10', 'Expect Pipe Excise Tax is 2,813.10');
            expect(form3852Component.getPipeTaxDiffElement()).toEqual('$0.00', 'Expect Pipe Difference Tax is $0.00');
            expect(form3852Component.getPipeCallculatedTaxRateElement()).toEqual('2.8311', 'Expect Pipe Calculated Tax Rate is 2.8311');
            expect(form3852Component.getPipesStatusElement()).toEqual('Valid', 'Expect Pipe Activity as Valid');
            expect(form3852Component.getRollYourOwnExciseTax()).toEqual('49,560.00', 'Expect Roll Your Own Excise Tax is 49,560.00');
            expect(form3852Component.getRollYourOwnTaxDiffElement()).toEqual('$0.00', 'Expect Roll Your Own Difference Tax is $0.00');
            expect(form3852Component.getRollYourOwnCallculatedTaxRateElement()).toEqual('24.78', 'Expect Roll Your Own Calculated Tax Rate is 24.78');
            expect(form3852Component.getRollYourOwnStatusElement()).toEqual('Valid', 'Expect Roll Your Own Activity as Valid');
        });

        it('Should download form 3852 csv file and verify filename to be as expected.', () => {            
            monthlyReportPage.clickSaveReport();
            browser.sleep(1000).then( function() {
                form3852Component.clickDownloadDocument();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(() => {
                    expect(fs.existsSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_3852_FILENAME)))
                        .toBe(true, 'Expected file '+ StaticValues.IMPORTER_DOWNLOADED_FORM_3852_FILENAME +' to be present in download directory.');
                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

        });

        it('Should verify downloaded Form 3852 file to contain correct expected values.', () => { 
            try {
                const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_3852_FILENAME), { encoding: 'utf8' });
                const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.IMPORTER_EXPECTED_FORM_3852_FILENAME), { encoding: 'utf8' });
                expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
            } catch (err) {
                fail(`ERROR: Exception thrown: ${err.message}`);
            }
        });

        afterAll(async () => { 
            require('fs-extra').emptyDirSync(downloadsDir);
        });

    });

    describe('Download Scenarios for 5220.6 form', () => {
        it('Should verify all form 5220.6 values are entered correctly.', () => {
            form5220Component.setCigarsSmallRemovalsAmount('0');
            form5220Component.setCigarettesLargeRemovalsAmount('0');

            // Cigarettes check
            expect(form5220Component.getCigarettesSmallRemovalsAmount()).toEqual('1,000', 'Expected Cigarettes Removal Amount for small to be 1000');
            expect(form5220Component.getCigarettesLargeRemovalsAmount()).toEqual('0', 'Expected Cigarettes Removal Amount for large to be 0');
            expect(form5220Component.getCigarettesDifferenceRemovalsAmountElement()).toEqual('0', 'Expected Cigarettes Difference Removals to be 0 in 5210 form');
            expect(form5220Component.getCigarettesActivityElement()).toEqual('Valid', 'Expected Cigarettes Activity as Valid in 5210 form');
            // Cigars check
            expect(form5220Component.getCigarsSmallRemovalsAmount()).toEqual('0', 'Expected Cigars Removal Amount for small to be 0');
            expect(form5220Component.getCigarsLargeRemovalsAmount()).toEqual('1,000', 'Expected Cigars Removal Amount for large to be 1000');
            expect(form5220Component.getCigarsDifferenceRemovalsAmountElement()).toEqual('0', 'Expected Cigars Difference Removals to be 0 in 5210 form');
            expect(form5220Component.getCigarsActivityElement()).toEqual('Valid', 'Expected Cigars Activity as Valid in 5210 form');
            // Snuff Check
            expect(form5220Component.getSnuffRemovalsAmount()).toEqual('5,000.000', 'Expected Snuff Removal Amount to be 5,000.000');
            expect(form5220Component.getSnuffDifferenceRemovalsAmountElement()).toEqual('0.000', 'Expected Snuff Difference Removals to be 0.000 in 5210 form');
            expect(form5220Component.getSnuffActivityElement()).toEqual('Valid', 'Expected Snuff Activity as Valid in 5210 form');
            // Chew Check
            expect(form5220Component.getChewRemovalsAmount()).toEqual('4,000.000', 'Expected Chew Removal Amount to be 4,000.000');
            expect(form5220Component.getChewDifferenceRemovalsAmountElement()).toEqual('0.000', 'Expected Chew Difference Removals to be 0.000 in 5210 form');
            expect(form5220Component.getChewActivityElement()).toEqual('Valid', 'Expected Chew Activity as Valid in 5210 form');
            // Pipe Check
            expect(form5220Component.getPipeRemovalsAmount()).toEqual('1,000.000', 'Expected Pipe Removal Amount to be 1,000.000');
            expect(form5220Component.getPipeDifferenceRemovalsAmountElement()).toEqual('0.000', 'Expected Pipe Difference Removals to be 0.000 in 5210 form');
            expect(form5220Component.getPipeActivityElement()).toEqual('Valid', 'Expected Pipe Activity as Valid in 5210 form');
            // Roll Your Own Check
            expect(form5220Component.getRollYourOwnRemovalsAmount()).toEqual('2,000.000', 'Expected Roll Your Own Removal Amount to be 2,000.000');
            expect(form5220Component.getRollYourOwnDifferenceRemovalsAmountElement()).toEqual('0.000', 'Expected Roll Your Own Difference Removals to be 0.000 in 5210 form');
            expect(form5220Component.getRollYourOwnActivityElement()).toEqual('Valid', 'Expected Roll Your Own Activity as Valid in 5210 form');
        });

        it('Should download Form 5220.6 csv file and verify filename to be as expected.', () => {            
            monthlyReportPage.clickSaveReport();
            browser.sleep(1000).then( function() {
                form5220Component.clickDownloadDocument();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(() => {
                    expect(fs.existsSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_5220_FILENAME)))
                        .toBe(true, 'Expected file '+ StaticValues.IMPORTER_DOWNLOADED_FORM_5220_FILENAME +' to be present in download directory.');
                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

        });

        it('Should verify downloaded Form 5220.6 file to contain correct expected values.', () => { 
            try {
                const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_5220_FILENAME), { encoding: 'utf8' });
                const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.IMPORTER_EXPECTED_FORM_5220_FILENAME), { encoding: 'utf8' });
                expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
            } catch (err) {
                fail(`ERROR: Exception thrown: ${err.message}`);
            }
        });

        afterAll(async () => { 
            require('fs-extra').emptyDirSync(downloadsDir);
        });
    });

    describe('Download Scenarios for 7501 form', () => {
        beforeAll(() => { 
            //set up form 3852 before form 7501 download scenarios
            form3852Component.setCigarettesRemovalsAmount('2500');
            form3852Component.setSnuffRemovalsAmount('2500');
            form3852Component.setChewRemovalsAmount('2500');
            form3852Component.setPipeRemovalsAmount('2500');
            form3852Component.setRollYourOwnRemovalsAmount('2500');
            form3852Component.setCigarsRemovalsAmount('2500');

        });

        describe('Cigarettes CBP7501 form', () => {
            it('Should verify all form 7501 values are entered correctly.', () => {
                expect(form7501Component.cigarettes3852RemovalsQuantityElement.getText()).toBe('2,500');
                expect(form7501Component.cigarettes7501RemovalsQuantityElement.getText()).toBe('');
                expect(form7501Component.cigarettesRemovalsQuantityDifferenceElement.getText()).toBe('2,500');
                expect(form7501Component.cigarettesRemovalsQuantityStatusElement.getText()).toBe('Error');
                expect(form7501Component.cigarettes3852TaxAmountElement.getText()).toBe('$125.83');
                expect(form7501Component.cigarettes7501TaxAmountElement.getText()).toBe('$0.00');
                expect(form7501Component.cigarettesTaxAmountDifferenceElement.getText()).toBe('$125.83');
                expect(form7501Component.cigarettesTaxAmountStatusElement.getText()).toBe('Error');
                
                form7501Component.clickCigarettesAddButton();
                form7501Component.setCigarettesVolumeField('2000');

                expect(form7501Component.cigarettes3852RemovalsQuantityElement.getText()).toBe('2,500');
                expect(form7501Component.cigarettes7501RemovalsQuantityElement.getText()).toBe('2,000,000');
                expect(form7501Component.cigarettesRemovalsQuantityDifferenceElement.getText()).toBe('(1,997,500)');
                expect(form7501Component.cigarettesRemovalsQuantityStatusElement.getText()).toBe('Error');
                expect(form7501Component.cigarettesVolumeFieldElement.getAttribute('value')).toBe('2000');
                expect(form7501Component.cigarettesTaxFieldElement.getAttribute('value')).toBe('100,660.00');
                expect(form7501Component.cigarettesLineCalcTaxRateElement.getText()).toBe('50.33');
                expect(form7501Component.cigarettesLineTaxDifferenceElement.getText()).toBe('$0.00');
                expect(form7501Component.cigarettes3852TaxAmountElement.getText()).toBe('$125.83');
                expect(form7501Component.cigarettes7501TaxAmountElement.getText()).toBe('$100,660.00');
                expect(form7501Component.cigarettesTaxAmountDifferenceElement.getText()).toBe('($100,534.17)');
                expect(form7501Component.cigarettesTaxAmountStatusElement.getText()).toBe('Error');
            });
    
            it('Should download Form 7501 csv file and verify filename to be as expected.', () => {            
                monthlyReportPage.clickSaveReport();
                browser.sleep(1000).then( function() {
                    form7501Component.clickCigarettesDownloadButton();
                    browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(() => {
                        expect(fs.existsSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_7501_CIGARETTES_FILENAME)))
                            .toBe(true, 'Expected file '+ StaticValues.IMPORTER_DOWNLOADED_FORM_7501_CIGARETTES_FILENAME +' to be present in download directory.');
                    }).catch(function(ex) {
                        throw('An error has occurred with file download: ' + ex);
                    });
                });
    
            });
    
            it('Should verify downloaded Form 7501 file to contain correct expected values.', () => { 
                try {
                    const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_7501_CIGARETTES_FILENAME), { encoding: 'utf8' });
                    const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.IMPORTER_EXPECTED_FORM_7501_CIGARETTES_FILENAME), { encoding: 'utf8' });
                    expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                } catch (err) {
                    fail(`ERROR: Exception thrown: ${err.message}`);
                }
            });
    
            afterAll(async () => { 
                require('fs-extra').emptyDirSync(downloadsDir);
            });
        });

        describe('Snuff CBP7501 form', () => {
            it('Should verify all form 7501 values are entered correctly.', () => {
                expect(form7501Component.snuff3852RemovalsQuantityElement.getText()).toBe('2,500.000');
                expect(form7501Component.snuff7501RemovalsQuantityElement.getText()).toBe('');
                expect(form7501Component.snuffRemovalsQuantityDifferenceElement.getText()).toBe('2,500.000');
                expect(form7501Component.snuffRemovalsQuantityStatusElement.getText()).toBe('Error');
                expect(form7501Component.snuff3852TaxAmountElement.getText()).toBe('$3,775.00');
                expect(form7501Component.snuff7501TaxAmountElement.getText()).toBe('$0.00');
                expect(form7501Component.snuffTaxAmountDifferenceElement.getText()).toBe('$3,775.00');
                expect(form7501Component.snuffTaxAmountStatusElement.getText()).toBe('Error');

                form7501Component.clickSnuffAddButton();
                form7501Component.setSnuffVolumeField('2000');
                expect(form7501Component.snuff3852RemovalsQuantityElement.getText()).toBe('2,500.000');
                expect(form7501Component.snuff7501RemovalsQuantityElement.getText()).toBe('4,409.245');
                expect(form7501Component.snuffRemovalsQuantityDifferenceElement.getText()).toBe('(1,909.245)');
                expect(form7501Component.snuffRemovalsQuantityStatusElement.getText()).toBe('Error');
                expect(form7501Component.snuffVolumeFieldElement.getAttribute('value')).toBe('2000');
                expect(form7501Component.snuffTaxFieldElement.getAttribute('value')).toBe('6,657.89');
                expect(form7501Component.snuffTaxRateElement.getText()).toBe('1.509984');
                expect(form7501Component.snuffTaxDiffElement.getText()).toBe('$0.00');
                expect(form7501Component.snuff3852TaxAmountElement.getText()).toBe('$3,775.00');
                expect(form7501Component.snuff7501TaxAmountElement.getText()).toBe('$6,657.89');
                expect(form7501Component.snuffTaxAmountDifferenceElement.getText()).toBe('($2,882.89)');
                expect(form7501Component.snuffTaxAmountStatusElement.getText()).toBe('Error');
            });
    
            it('Should download Form 7501 csv file and verify filename to be as expected.', () => {            
                monthlyReportPage.clickSaveReport();
                browser.sleep(1000).then( function() {
                    form7501Component.clickSnuffDownloadButton();
                    browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(() => {
                        expect(fs.existsSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_7501_SNUFF_FILENAME)))
                            .toBe(true, 'Expected file '+ StaticValues.IMPORTER_DOWNLOADED_FORM_7501_SNUFF_FILENAME +' to be present in download directory.');
                    }).catch(function(ex) {
                        throw('An error has occurred with file download: ' + ex);
                    });
                });
    
            });
    
            it('Should verify downloaded Form 7501 file to contain correct expected values.', () => { 
                try {
                    const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_7501_SNUFF_FILENAME), { encoding: 'utf8' });
                    const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.IMPORTER_EXPECTED_FORM_7501_SNUFF_FILENAME), { encoding: 'utf8' });
                    expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                } catch (err) {
                    fail(`ERROR: Exception thrown: ${err.message}`);
                }
            });
    
            afterAll(async () => { 
                require('fs-extra').emptyDirSync(downloadsDir);
            });
        });

        describe('Chew CBP7501 form', () => {
            it('Should verify all form 7501 values are entered correctly.', () => {
                expect(form7501Component.chew3852RemovalsQuantityElement.getText()).toBe('2,500.000');
                expect(form7501Component.chew7501RemovalsQuantityElement.getText()).toBe('');
                expect(form7501Component.chewRemovalsQuantityDifferenceElement.getText()).toBe('2,500.000');
                expect(form7501Component.chewRemovalsQuantityStatusElement.getText()).toBe('Error');
                expect(form7501Component.chew3852TaxAmountElement.getText()).toBe('$1,258.25');
                expect(form7501Component.chew7501TaxAmountElement.getText()).toBe('$0.00');
                expect(form7501Component.chewTaxAmountDifferenceElement.getText()).toBe('$1,258.25');
                expect(form7501Component.chewTaxAmountStatusElement.getText()).toBe('Error');
                form7501Component.clickChewAddButton();
                form7501Component.setChewVolumeField('2000');
                expect(form7501Component.chew3852RemovalsQuantityElement.getText()).toBe('2,500.000');
                expect(form7501Component.chew7501RemovalsQuantityElement.getText()).toBe('4,409.245');
                expect(form7501Component.chewRemovalsQuantityDifferenceElement.getText()).toBe('(1,909.245)');
                expect(form7501Component.chewRemovalsQuantityStatusElement.getText()).toBe('Error');
                expect(form7501Component.chewVolumeFieldElement.getAttribute('value')).toBe('2000');
                expect(form7501Component.chewTaxFieldElement.getAttribute('value')).toBe('2,219.15');
                expect(form7501Component.chewCalcTaxRateElement.getText()).toBe('0.503295');
                expect(form7501Component.chewTaxDiffElement.getText()).toBe('$0.00');
                expect(form7501Component.chew3852TaxAmountElement.getText()).toBe('$1,258.25');
                expect(form7501Component.chew7501TaxAmountElement.getText()).toBe('$2,219.15');
                expect(form7501Component.chewTaxAmountDifferenceElement.getText()).toBe('($960.90)');
                expect(form7501Component.chewTaxAmountStatusElement.getText()).toBe('Error');
            });
    
            it('Should download Form 7501 csv file and verify filename to be as expected.', () => {            
                monthlyReportPage.clickSaveReport();
                browser.sleep(1000).then( function() {
                    form7501Component.clickChewDownloadButton();
                    browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(() => {
                        expect(fs.existsSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_7501_CHEW_FILENAME)))
                            .toBe(true, 'Expected file '+ StaticValues.IMPORTER_DOWNLOADED_FORM_7501_CHEW_FILENAME +' to be present in download directory.');
                    }).catch(function(ex) {
                        throw('An error has occurred with file download: ' + ex);
                    });
                });
    
            });
    
            it('Should verify downloaded Form 7501 file to contain correct expected values.', () => { 
                try {
                    const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_7501_CHEW_FILENAME), { encoding: 'utf8' });
                    const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.IMPORTER_EXPECTED_FORM_7501_CHEW_FILENAME), { encoding: 'utf8' });
                    expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                } catch (err) {
                    fail(`ERROR: Exception thrown: ${err.message}`);
                }
            });
    
            afterAll(async () => { 
                require('fs-extra').emptyDirSync(downloadsDir);
            });
        });

        describe('Pipe CBP7501 form', () => {
            it('Should verify all form 7501 values are entered correctly.', () => {
                expect(form7501Component.pipe3852RemovalsQuantityElement.getText()).toBe('2,500.000');
                expect(form7501Component.pipe7501RemovalsQuantityElement.getText()).toBe('');
                expect(form7501Component.pipeRemovalsQuantityDifferenceElement.getText()).toBe('2,500.000');
                expect(form7501Component.pipeRemovalsQuantityStatusElement.getText()).toBe('Error');
                expect(form7501Component.pipe3852TaxAmountElement.getText()).toBe('$7,077.75');
                expect(form7501Component.pipe7501TaxAmountElement.getText()).toBe('$0.00');
                expect(form7501Component.pipeTaxAmountDifferenceElement.getText()).toBe('$7,077.75');
                expect(form7501Component.pipeTaxAmountStatusElement.getText()).toBe('Error');
                form7501Component.clickPipeAddButton();
                form7501Component.setPipeVolumeField('2000');
                expect(form7501Component.pipe3852RemovalsQuantityElement.getText()).toBe('2,500.000');
                expect(form7501Component.pipe7501RemovalsQuantityElement.getText()).toBe('4,409.245');
                expect(form7501Component.pipeRemovalsQuantityDifferenceElement.getText()).toBe('(1,909.245)');
                expect(form7501Component.pipeRemovalsQuantityStatusElement.getText()).toBe('Error');
                expect(form7501Component.pipeVolumeFieldElement.getAttribute('value')).toBe('2000');
                expect(form7501Component.pipeTaxFieldElement.getAttribute('value')).toBe('12,482.88');
                expect(form7501Component.pipeCalcTaxRateElement.getText()).toBe('2.83107');
                expect(form7501Component.pipeTaxDiffElement.getText()).toBe('$0.00');
                expect(form7501Component.pipe3852TaxAmountElement.getText()).toBe('$7,077.75');
                expect(form7501Component.pipe7501TaxAmountElement.getText()).toBe('$12,482.88');
                expect(form7501Component.pipeTaxAmountDifferenceElement.getText()).toBe('($5,405.13)');
                expect(form7501Component.pipeTaxAmountStatusElement.getText()).toBe('Error');
            });
    
            it('Should download Form 7501 csv file and verify filename to be as expected.', () => {            
                monthlyReportPage.clickSaveReport();
                browser.sleep(1000).then( function() {
                    form7501Component.clickPipeDownloadButton();
                    browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(() => {
                        expect(fs.existsSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_7501_PIPE_FILENAME)))
                            .toBe(true, 'Expected file '+ StaticValues.IMPORTER_DOWNLOADED_FORM_7501_PIPE_FILENAME +' to be present in download directory.');
                    }).catch(function(ex) {
                        throw('An error has occurred with file download: ' + ex);
                    });
                });
    
            });
    
            it('Should verify downloaded Form 7501 file to contain correct expected values.', () => { 
                try {
                    const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_7501_PIPE_FILENAME), { encoding: 'utf8' });
                    const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.IMPORTER_EXPECTED_FORM_7501_PIPE_FILENAME), { encoding: 'utf8' });
                    expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                } catch (err) {
                    fail(`ERROR: Exception thrown: ${err.message}`);
                }
            });
    
            afterAll(async () => { 
                require('fs-extra').emptyDirSync(downloadsDir);
            });
        });

        describe('Roll Your Own CBP7501 form', () => {
            it('Should verify all form 7501 values are entered correctly.', () => {
                expect(form7501Component.rollYourOwn3852RemovalsQuantityElement.getText()).toBe('2,500.000');
                expect(form7501Component.rollYourOwn7501RemovalsQuantityElement.getText()).toBe('');
                expect(form7501Component.rollYourOwnRemovalsQuantityDifferenceElement.getText()).toBe('2,500.000');
                expect(form7501Component.rollYourOwnRemovalsQuantityStatusElement.getText()).toBe('Error');
                expect(form7501Component.rollYourOwn3852TaxAmountElement.getText()).toBe('$61,950.00');
                expect(form7501Component.rollYourOwn7501TaxAmountElement.getText()).toBe('$0.00');
                expect(form7501Component.rollYourOwnTaxAmountDifferenceElement.getText()).toBe('$61,950.00');
                expect(form7501Component.rollYourOwnTaxAmountStatusElement.getText()).toBe('Error');
                form7501Component.clickRollYourOwnAddButton();
                form7501Component.setRollYourOwnVolumeField('2000');
                expect(form7501Component.rollYourOwn3852RemovalsQuantityElement.getText()).toBe('2,500.000');
                expect(form7501Component.rollYourOwn7501RemovalsQuantityElement.getText()).toBe('4,409.245');
                expect(form7501Component.rollYourOwnRemovalsQuantityDifferenceElement.getText()).toBe('(1,909.245)');
                expect(form7501Component.rollYourOwnRemovalsQuantityStatusElement.getText()).toBe('Error');
                expect(form7501Component.rollYourOwnVolumeFieldElement.getAttribute('value')).toBe('2000');
                expect(form7501Component.rollYourOwnTaxFieldElement.getAttribute('value')).toBe('109,259.98');
                expect(form7501Component.rollYourOwnCalcTaxRateElement.getText()).toBe('24.779747');
                expect(form7501Component.rollYourOwnTaxDiffElement.getText()).toBe('$0.00');
                expect(form7501Component.rollYourOwn3852TaxAmountElement.getText()).toBe('$61,950.00');
                expect(form7501Component.rollYourOwn7501TaxAmountElement.getText()).toBe('$109,259.98');
                expect(form7501Component.rollYourOwnTaxAmountDifferenceElement.getText()).toBe('($47,309.98)');
                expect(form7501Component.rollYourOwnTaxAmountStatusElement.getText()).toBe('Error');
            });
    
            it('Should download Form 7501 csv file and verify filename to be as expected.', () => {            
                monthlyReportPage.clickSaveReport();
                browser.sleep(1000).then( function() {
                    form7501Component.clickRollYourOwnDownloadButton();
                    browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(() => {
                        expect(fs.existsSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_7501_ROLL_YOUR_OWN_FILENAME)))
                            .toBe(true, 'Expected file '+ StaticValues.IMPORTER_DOWNLOADED_FORM_7501_ROLL_YOUR_OWN_FILENAME +' to be present in download directory.');
                    }).catch(function(ex) {
                        throw('An error has occurred with file download: ' + ex);
                    });
                });
    
            });
    
            it('Should verify downloaded Form 7501 file to contain correct expected values.', () => { 
                try {
                    const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_7501_ROLL_YOUR_OWN_FILENAME), { encoding: 'utf8' });
                    const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.IMPORTER_EXPECTED_FORM_7501_ROLL_YOUR_OWN_FILENAME), { encoding: 'utf8' });
                    expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                } catch (err) {
                    fail(`ERROR: Exception thrown: ${err.message}`);
                }
            });
    
            afterAll(async () => { 
                require('fs-extra').emptyDirSync(downloadsDir);
            });
        });

        describe('Cigars CBP7501 form', () => {
            it('Should verify all form 7501 values are entered correctly.', () => {    
                form7501Component.clickCigarsAddButton();
                form7501Component.selectCigarTypeDropdown('None');
                form7501Component.setCigarNetQuantity('2000');
                form7501Component.clickCigarsAddButton();
                form7501Component.selectCigarTypeDropdown('Small');
                form7501Component.setCigarNetQuantity('2000');
                form7501Component.clickCigarsAddButton();
                form7501Component.selectCigarTypeDropdown('Large');
                form7501Component.setCigarNetQuantity('2000');
                form7501Component.clickCigarsAddButton();
                form7501Component.selectCigarTypeDropdown('Ad Valorem');
                form7501Component.setCigarNetQuantity('2000');

                expect(form7501Component.cigars3852RemovalsQuantityElement.getText()).toBe('2,500');
                expect(form7501Component.cigars7501RemovalsQuantityElement.getText()).toBe('8,000,000');
                expect(form7501Component.cigarsRemovalsQuantityDifferenceElement.getText()).toBe('(7,997,500)');
                expect(form7501Component.cigarsRemovalsQuantityStatusElement.getText()).toBe('Error');
                expect(form7501Component.cigars3852TaxAmountElement.getText()).toBe('$1,000.00');
                expect(form7501Component.cigars7501TaxAmountElement.getText()).toBe('$1,711,060.00');
                expect(form7501Component.cigarsTaxAmountDifferenceElement.getText()).toBe('($1,710,060.00)');
                expect(form7501Component.cigarsTaxAmountStatusElement.getText()).toBe('Error');
                expect(form7501Component.cigarTotalStatusElement.getText()).toBe('Valid')
            });
    
            it('Should download Form 7501 csv file and verify filename to be as expected.', () => {            
                monthlyReportPage.clickSaveReport();
                browser.sleep(1000).then( function() {
                    form7501Component.clickCigarsDownloadButton();
                    browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(() => {
                        expect(fs.existsSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_7501_CIGARS_FILENAME)))
                            .toBe(true, 'Expected file '+ StaticValues.IMPORTER_DOWNLOADED_FORM_7501_CIGARS_FILENAME +' to be present in download directory.');
                    }).catch(function(ex) {
                        throw('An error has occurred with file download: ' + ex);
                    });
                });
    
            });
    
            it('Should verify downloaded Form 7501 file to contain correct expected values.', () => { 
                try {
                    const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, StaticValues.IMPORTER_DOWNLOADED_FORM_7501_CIGARS_FILENAME), { encoding: 'utf8' });
                    const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.IMPORTER_EXPECTED_FORM_7501_CIGARS_FILENAME), { encoding: 'utf8' });
                    expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                } catch (err) {
                    fail(`ERROR: Exception thrown: ${err.message}`);
                }
            });
    
            afterAll(async () => { 
                require('fs-extra').emptyDirSync(downloadsDir);
            });
        });
    });
    
    it('Reset report and verify Report status is "Not Started"', () => {
        tobaccoClassSelection.clickSelectAllSelection();
        browser.wait(protractor.ExpectedConditions.invisibilityOf(tobaccoClassSelection.allSelectedTobaccoClassesElements.get(0)));
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            expect(monthlyReportPage.getReportStatusElement('Not Started')).toEqual('Not Started', 'Reset Report Completed');
        });
    });

});
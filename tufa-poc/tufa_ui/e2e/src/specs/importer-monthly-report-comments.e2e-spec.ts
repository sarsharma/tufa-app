import { browser } from 'protractor';
import { Form3852Component } from '../component/form-3852.co';
import { Form5220Component } from '../component/form-5220.co';
import { Form7501Component } from '../component/form-7501.co';
import { TobaccoClassSelectionComponent } from '../component/tobacco-class-selection.co';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { MonthlyReportPage } from '../page/monthly-report.po';
import { StartPage } from '../page/start.po';

describe('Create Importer Monthly Report Adding Comment Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let monthlyReportPage: MonthlyReportPage;
    let tobaccoClassSlection: TobaccoClassSelectionComponent;
    let form3852Component: Form3852Component;
    let form5220Component: Form5220Component;
    let form7501Component: Form7501Component;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            dailyOperationsPage = loginPage.clickConfirmButton();
            dailyOperationsPage.setSearchText('Importer');
            dailyOperationsPage.clickSearchButton();
            monthlyReportPage = dailyOperationsPage.clickPeriodOfActivityElement();
            tobaccoClassSlection = new TobaccoClassSelectionComponent();
            form3852Component = new Form3852Component();
            form5220Component = new Form5220Component();
            form7501Component = new Form7501Component();
        });
    });

    it('Should be able to navigate to a Monthly Report.', () => {
        expect(monthlyReportPage.pageTitleElement.isDisplayed()).toBe(true, 'Expect Monthly Report title to be displayed');
    });

    it('FDA 3852 Comments : Error message', () => {
        tobaccoClassSlection.clickCigarettesSelection();
        form3852Component.addCommentElement.click();
        form3852Component.saveComment();
        browser.waitForAngular().then(function () {
            expect(form3852Component.errorCommentElement.getText()).toEqual('Error: Comment description is required', 'Expect message Error: Comment description is required');
        });
        form3852Component.cancelComment();
    });

    it('FDA 3852 Comments : Cancel comments', () => {
        form3852Component.addCommentElement.click();
        form3852Component.setCommentText("Test Comment");
        form3852Component.cancelComment();
        browser.switchTo().alert().accept().then(
            function () {
                expect(form3852Component.commentTableElement.isPresent()).toBe(false, 'Verify that comments are not saved');
            }, function (e) {
                if (e.code !== browser.ErrorCode.NO_SUCH_ALERT) {
                    expect(e).toBe(true, 'Alert not present on cancel button');
                }
            });
    });

    it('FDA 3852 Comments : Cancel comments - Click Okay', () => {
        form3852Component.addCommentElement.click();
        form3852Component.setCommentText("Test Comment");
        form3852Component.cancelComment();
        browser.switchTo().alert().accept().then(function () {
            browser.waitForAngular().then(function () {
                expect(form3852Component.commentTableElement.isPresent()).toBe(false, 'Verify that comments are not saved');
            });
        });
    });

    it('FDA 3852 Comments : Save comments', () => {
        form3852Component.addCommentElement.click();
        form3852Component.setCommentText("Test FDA 3852");
        form3852Component.saveComment();
        browser.sleep(2000);
        browser.waitForAngular().then(function () {
            expect(form3852Component.commentTableDescriptionElement.getText()).toEqual('Test FDA 3852', 'Verify that comments are saved');
            expect(form3852Component.commentTableAuthorElement.isPresent()).toBe(true, 'Verify that Author is present');
            form3852Component.commentTableDateElement.getText().then(function (value) {
                let compareDate = new Date(value).toLocaleDateString('en-US', { year: "numeric", month: "2-digit", day: "2-digit" }) + " " + new Date(value).toLocaleTimeString('it-IT');
                expect(value).toEqual(compareDate, 'Date is the right format');
            });
            expect(form3852Component.commentTableResolvedElement.isPresent()).toBe(true, 'Verify that resolved checkbox is present');
        });
    });

    it('FDA 3852 Comments : Edit comments', () => {
        form3852Component.editCommentElement.click();
        form3852Component.setCommentText("Test FDA 3852 edit");
        form3852Component.saveComment();
        browser.sleep(2000);
        browser.waitForAngular().then(function () {
            form3852Component.getCommentTableDescription();
            expect(form3852Component.commentTableDescriptionElement.getText()).toEqual('Test FDA 3852 edit', 'Verify that comments are saved');
            expect(form3852Component.commentTableAuthorElement.isPresent()).toBe(true, 'Verify that Author is present');
            form3852Component.commentTableDateElement.getText().then(function (value) {
                let compareDate = new Date(value).toLocaleDateString('en-US', { year: "numeric", month: "2-digit", day: "2-digit" }) + " " + new Date(value).toLocaleTimeString('it-IT');
                expect(value).toEqual(compareDate, 'Date is the right format');
            });
            expect(form3852Component.commentTableResolvedElement.isPresent()).toBe(true, 'Verify that resolved checkbox is present');
        });
    });

    it('FDA 3852 Comments : Resolve the comment', () => {
        form3852Component.commentTableResolvedElement.click();
        browser.waitForAngular().then(function () {
            expect(form3852Component.commentTableResolvedElement.isEnabled()).toBe(true, 'Verify that resolved checkbox is disabled');
        });
        tobaccoClassSlection.clickCigarettesSelection();
    });
});

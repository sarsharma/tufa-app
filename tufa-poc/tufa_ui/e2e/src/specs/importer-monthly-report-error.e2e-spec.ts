import { browser } from 'protractor';
import { Form3852Component } from '../component/form-3852.co';
import { Form5220Component } from '../component/form-5220.co';
import { Form7501Component } from '../component/form-7501.co';
import { TobaccoClassSelectionComponent } from '../component/tobacco-class-selection.co';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { MonthlyReportPage } from '../page/monthly-report.po';
import { StartPage } from '../page/start.po';

describe('Create Importer Monthly Report For Error Scenarios ', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let monthlyReportPage: MonthlyReportPage;
    let tobaccoClassSelection: TobaccoClassSelectionComponent;
    let form3852Component: Form3852Component;
    let form5220Component: Form5220Component;
    let form7501Component: Form7501Component;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            dailyOperationsPage = loginPage.clickConfirmButton();
            dailyOperationsPage.setSearchText('43-5365234');
            dailyOperationsPage.clickSearchButton();
            dailyOperationsPage.clickExtendDisplayButton();
            monthlyReportPage = dailyOperationsPage.clickPeriodOfActivity('January FY 2016');
            tobaccoClassSelection = new TobaccoClassSelectionComponent();
            form3852Component = new Form3852Component();
            form5220Component = new Form5220Component();
            form7501Component = new Form7501Component();

            tobaccoClassSelection.getAllSelectedTobaccoClasses().count().then(count => {
                //If there is tobacco classes currently selected, attempt to clear the form in order to start fresh.
                if(count > 0) {
                    tobaccoClassSelection.resetSelection();
                    monthlyReportPage.clickSaveReport();
                    browser.waitForAngular();
                }
            });
        });
    });

    it('Should be able to navigate to a Monthly Report.', () => {
        expect(monthlyReportPage.pageTitleElement.isDisplayed()).toBe(true, 'Expect Monthly Report title to be displayed');
    });

    it('For Cigarettes in FDA 3852 form: Verify for error status', () => {
        tobaccoClassSelection.clickCigarettesSelection();
        form3852Component.setCigarettesRemovalsAmount('1000');
        expect(form3852Component.getCigarettesExciseTax()).toEqual('50.33', 'Expect Cigarettes Excise Tax is 50.33');
        form3852Component.setCigarettesExciseTax('60');
        expect(form3852Component.getCigarettesTaxDiffElement()).toEqual('$9.67', 'Expect Cigarettes Difference Tax is $9.67');
        expect(form3852Component.getCigarettesCallculatedTaxRateElement()).toEqual('60', 'Expect Cigarettes Calculated Tax Rate is 60');
        expect(form3852Component.getCigarettesStatusElement()).toEqual('Error', 'Expect Cigarettes Activity as Error');
    });

    it('For Snuff in FDA 3852 form: Verify for error status', () => {
        tobaccoClassSelection.clickSnuffSelection();
        form3852Component.setSnuffRemovalsAmount('6000');
        expect(form3852Component.getSnuffExciseTaxField()).toEqual('9,060.00', 'Expect Snuff Excise Tax is 9,060.00');
        form3852Component.setSnuffExciseTax('5000');
        expect(form3852Component.getSnuffTaxDiffElement()).toEqual('($4,060.00)', 'Expect Snuff Difference Tax is ($4,060.00)');
        expect(form3852Component.getSnuffCallculatedTaxRateElement()).toEqual('0.833333', 'Expect Snuff Calculated Tax Rate is 0.833333');
        expect(form3852Component.getSnuffStatusElement()).toEqual('Error', 'Expect Snuff Activity as Error');
    });

    it('For Chew in FDA 3852 form: Verify for error status', () => {
        tobaccoClassSelection.clickChewSelection();
        form3852Component.setChewRemovalsAmount('4000');
        expect(form3852Component.getChewExciseTaxField()).toEqual('2,013.20', 'Expect Chew Excise Tax is 2,013.20');
        form3852Component.setChewExciseTax('3000');
        expect(form3852Component.getChewTaxDiffElement()).toEqual('$986.80', 'Expect Chew Difference Tax is $986.80');
        expect(form3852Component.getChewCallculatedTaxRateElement()).toEqual('0.75', 'Expect Chew Calculated Tax Rate is 0.75');
        expect(form3852Component.getChewStatusElement()).toEqual('Error', 'Expect Chew Activity as Error');
    });

    it('For Pipe in FDA 3852 form: Verify for error status', () => {
        tobaccoClassSelection.clickPipeSelection();
        form3852Component.setPipeRemovalsAmount('5000');
        expect(form3852Component.getPipeExciseTax()).toEqual('14,155.50', 'Expect Pipe Excise Tax is 14,155.50');
        form3852Component.setPipeExciseTax('4155.50');
        expect(form3852Component.getPipeTaxDiffElement()).toEqual('($10,000.00)', 'Expect Pipe Difference Tax is ($10,000.00)');
        expect(form3852Component.getPipeCallculatedTaxRateElement()).toEqual('0.8311', 'Expect Pipe Calculated Tax Rate is 0.8311');
        expect(form3852Component.getPipesStatusElement()).toEqual('Error', 'Expect Pipe Activity as Error');
    });

    it('For Roll Your Own in FDA 3852 form: Verify for error status', () => {
        tobaccoClassSelection.clickRollYourOwnSelection();
        form3852Component.setRollYourOwnRemovalsAmount('3000');
        expect(form3852Component.getRollYourOwnExciseTax()).toEqual('74,340.00', 'Expect Roll Your Own Excise Tax is 74,340.00');
        form3852Component.setRollYourOwnExciseTax('54000');
        expect(form3852Component.getRollYourOwnTaxDiffElement()).toEqual('($20,340.00)', 'Expect Roll Your Own Difference Tax is ($20,340.00)');
        expect(form3852Component.getRollYourOwnCallculatedTaxRateElement()).toEqual('18', 'Expect Roll Your Own Calculated Tax Rate is 18');
        expect(form3852Component.getRollYourOwnStatusElement()).toEqual('Error', 'Expect Roll Your Own Activity as Error');
    });

    it('For Cigarettes in FDA 5220 form: Change the TTB 5220.6 value to 2000 for Cigarettes.', () => {
        form5220Component.setCigarettesSmallRemovalsAmount('2000');
        expect(form5220Component.getCigarettesDifferenceRemovalsAmountElement()).toEqual('(1,000)', 'Expect Cigarettes Difference Removals for small is 1,000 in 5220 form');
        expect(form5220Component.getCigarettesActivityElement()).toEqual('Error', 'Expect Cigarettes Activity as Error in 5220 form');
    });

    it('For Snuff in FDA 5220 form: Change the TTB 5220.6 value to 2000 for Snuff.', () => {
        form5220Component.setSnuffRemovalsAmount('2000');
        expect(form5220Component.getSnuffDifferenceRemovalsAmountElement()).toEqual('4,000.000', 'Expect Snuff Difference Removals for small is 4,000 in 5220 form');
        expect(form5220Component.getSnuffActivityElement()).toEqual('Error', 'Expect Snuff Activity as Error in 5220 form');
    });

    it('For Chew in FDA 5220 form: Change the TTB 5220.6 value to 5000 for Chew.', () => {
        form5220Component.setChewRemovalsAmount('5000');
        expect(form5220Component.getChewDifferenceRemovalsAmountElement()).toEqual('(1,000.000)', 'Expect Chew Difference Removals  is (1,000) in 5220 form');
        expect(form5220Component.getChewActivityElement()).toEqual('Error', 'Expect Chew Activity as Error in 5220 form');
    });

    it('For Pipe in FDA 5220 form: Change the TTB 5220.6 value to 2000 for Pipe.', () => {
        form5220Component.setPipeRemovalsAmount('2000');
        expect(form5220Component.getPipeDifferenceRemovalsAmountElement()).toEqual('3,000.000', 'Expect Pipe Difference Removals  is 3000 in 5220 form');
        expect(form5220Component.getPipeActivityElement()).toEqual('Error', 'Expect Pipe Activity as Error in 5220 form');
    });

    it('For Roll Your Own in FDA 5220 form: Change the TTB 5220.6 value to 2000 for Roll Your Own.', () => {
        form5220Component.setRollYourOwnRemovalsAmount('2000');
        expect(form5220Component.getRollYourOwnDifferenceRemovalsAmountElement()).toEqual('1,000.000', 'Expect RollYourOwn Difference Removals  is 1000 in 5220 form');
        expect(form5220Component.getRollYourOwnActivityElement()).toEqual('Error', 'Expect RollYourOwn Activity as Error in 5220 form');
    });

    //7501 Error
    it('Should add a new line to the Cigarette 7501 form and populate fields', () => {
        form7501Component.cigarettesAddButton.click();
        form7501Component.setCigarettesVolumeField('20');

        expect(form7501Component.cigarettesTaxFieldElement.getAttribute('value')).toBe('1,006.60', '');

        form7501Component.setCigarettesTaxField('650');

        expect(form7501Component.cigarettesRemovalsQuantityStatusElement.getText()).toBe('Error', '');
        expect(form7501Component.cigarettesTaxAmountStatusElement.getText()).toBe('Error', '');
        expect(form7501Component.cigarettesTotalStatusElement.getText()).toBe('Error', '');
    });

    it('Should add a new line to the Snuff 7501 form and populate fields', () => {
        form7501Component.snuffAddButton.click();
        form7501Component.setSnuffVolumeField('2000');

        expect(form7501Component.snuffTaxFieldElement.getAttribute('value')).toBe('6,657.89', '');

        form7501Component.setSnuffTaxField('650');

        expect(form7501Component.snuffRemovalsQuantityStatusElement.getText()).toBe('Error', '');
        expect(form7501Component.snuffTaxAmountStatusElement.getText()).toBe('Error', '');
        expect(form7501Component.snuffTotalStatusElement.getText()).toBe('Error', '');
    });

    it('Should add a new line to the Chew 7501 form and populate fields', () => {
        form7501Component.chewAddButton.click();
        form7501Component.setChewVolumeField('1000');

        expect(form7501Component.chewTaxFieldElement.getAttribute('value')).toBe('1,109.58', '');

        form7501Component.setChewTaxField('650');

        expect(form7501Component.chewRemovalsQuantityStatusElement.getText()).toBe('Error', '');
        expect(form7501Component.chewTaxAmountStatusElement.getText()).toBe('Error', '');
        expect(form7501Component.chewTotalStatusElement.getText()).toBe('Error', '');
    });

    it('Should add a new line to the Pipe 7501 form and populate fields', () => {
        form7501Component.pipeAddButton.click();
        form7501Component.setPipeVolumeField('200');

        expect(form7501Component.pipeTaxFieldElement.getAttribute('value')).toBe('1,248.29', '');

        form7501Component.setPipeTaxField('650');

        expect(form7501Component.pipeRemovalsQuantityStatusElement.getText()).toBe('Error', '');
        expect(form7501Component.pipeTaxAmountStatusElement.getText()).toBe('Error', '');
        expect(form7501Component.pipeTotalStatusElement.getText()).toBe('Error', '');
    });

    it('Should add a new line to the Roll Your Own 7501 form and populate fields', () => {
        form7501Component.rollYourOwnAddButton.click();
        form7501Component.setRollYourOwnVolumeField('2000');

        expect(form7501Component.rollYourOwnTaxFieldElement.getAttribute('value')).toBe('109,259.98', '');

        form7501Component.setRollYourOwnTaxField('650');

        expect(form7501Component.rollYourOwnRemovalsQuantityStatusElement.getText()).toBe('Error', '');
        expect(form7501Component.rollYourOwnTaxAmountStatusElement.getText()).toBe('Error', '');
        expect(form7501Component.rollYourOwnTotalStatusElement.getText()).toBe('Error', '');
    });

    
    
    it('Should show "No documentation was provided" warning message', () => {
        expect(monthlyReportPage.noDocProvidedWarningElement.isDisplayed()).toEqual(true, 'Expected "No documentation was provided" warning message');    
    });

    it('Should show "Invalid information in form FDA 3852" warning message', () => {
        expect(monthlyReportPage.form3852WarningElement.isDisplayed()).toEqual(true, 'Expected "Invalid information in form FDA 3852" warning message');    
    });

    it('Should show "Invalid information in form CBP 7501" warning message', () => {
        expect(monthlyReportPage.form7501WarningElement.isDisplayed()).toEqual(true, 'Expected "Invalid information in form CBP 7501" warning message');    
    });

    it('Should show "Invalid information in form TTB 5220.6" warning message', () => {
        expect(monthlyReportPage.form5220WarningElement.isDisplayed()).toEqual(true, 'Expected "Invalid information in form TTB 5220.6" warning message');    
    });
    
    it('Save the report', () => {
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            expect(monthlyReportPage.getReportStatusElement('Error')).toEqual('Error', 'Save Report Completed');
        });
    });
    
    it('Reset report', () => {
        tobaccoClassSelection.resetSelection();
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            expect(monthlyReportPage.getReportStatusElement('Not Started')).toEqual('Not Started', 'Reset Report Completed');
        });
    });
});

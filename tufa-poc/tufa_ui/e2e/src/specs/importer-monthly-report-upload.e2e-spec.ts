import { browser } from 'protractor';
import { Form3852Component } from '../component/form-3852.co';
import { Form5220Component } from '../component/form-5220.co';
import { Form7501Component } from '../component/form-7501.co';
import { TobaccoClassSelectionComponent } from '../component/tobacco-class-selection.co';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { MonthlyReportPage } from '../page/monthly-report.po';
import { StartPage } from '../page/start.po';

describe('Create Importer Monthly Report Upload Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let monthlyReportPage: MonthlyReportPage;
    let tobaccoClassSlection: TobaccoClassSelectionComponent;
    let form3852Component: Form3852Component;
    let form5220Component: Form5220Component;
    let form7501Component: Form7501Component;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            dailyOperationsPage = loginPage.clickConfirmButton();
            dailyOperationsPage.setSearchText('Importer');
            dailyOperationsPage.clickSearchButton();
            monthlyReportPage = dailyOperationsPage.clickPeriodOfActivityElement();
            tobaccoClassSlection = new TobaccoClassSelectionComponent();
            form3852Component = new Form3852Component();
            form5220Component = new Form5220Component();
            form7501Component = new Form7501Component();
        });
    });

    it('Should be able to navigate to a Monthly Report.', () => {
        expect(monthlyReportPage.pageTitleElement.isDisplayed()).toBe(true, 'Expect Monthly Report title to be displayed');
    });

    it('Importer Monthly Report: User will be uploading a Document to the Monthly Report', () => {
        browser.sleep(2000);
        form3852Component.uploadDocumentElement.click();
        browser.sleep(2000);
        expect(form3852Component.uploadDocumentTitleElement.isPresent()).toEqual(true, 'Verify Upload Document Pop up');
        form3852Component.selectFile('../documents/', '5mb-actual-data.pdf');
        // form3852Component.fileNameElement.getText().then(function(value){
        //     expect(form3852Component.browseFileTextElement.getText()).toEqual(value, 'Expect File Name equal to Browse File Name');
        // });
        form3852Component.selectFile('../documents/', 'WI-TI-30001_5_16_19.pdf');
        expect(form3852Component.browseFileTextElement.getText()).toEqual('WI-TI-30001_5_16_19.pdf', 'Verify File can be changed');
        form3852Component.fileNameElement.clear();
        form3852Component.fileNameElement.sendKeys("Change FileName");
        expect(form3852Component.fileNameElement.isPresent()).toBe(true, 'Verify user can edit file name');
        form3852Component.selectTTB5220CheckboxElement.click();
        browser.sleep(1000);
        form3852Component.selectFDA3852CheckboxElement.click();
        browser.sleep(1000);
        form3852Component.selectTTB5220CheckboxElement.click();
        browser.sleep(1000);
        expect(form3852Component.selectTTB5220CheckboxElement.isSelected()).toBe(false, 'Verify that TTB 5220.6 is not selected');
        form3852Component.selectAllElement.click();
        browser.sleep(1000);
        expect(form3852Component.selectFDA3852CheckboxElement.isSelected()).toBe(true, 'Verify that when select all is check FDA 3852 is checked');
        expect(form3852Component.selectTTB5220CheckboxElement.isSelected()).toBe(true, 'Verify that when select all is check TTB 5220.6 is checked');
        expect(form3852Component.selectCBP7501CheckboxElement.isSelected()).toBe(true, 'Verify that when select all is check CBP 7501 is checked');
        expect(form3852Component.otherElement.isSelected()).toBe(false, 'Verify that when select all is check other is not checked');

        form3852Component.selectAllElement.click();
        browser.sleep(1000);
        expect(form3852Component.selectFDA3852CheckboxElement.isSelected()).toBe(false, 'Verify that when select all is uncheck FDA 3852 is not checked');
        expect(form3852Component.selectTTB5220CheckboxElement.isSelected()).toBe(false, 'Verify that when select all is uncheck TTB 5220.6 is not checked');
        expect(form3852Component.selectCBP7501CheckboxElement.isSelected()).toBe(false, 'Verify that when select all is uncheck CBP 7501 is not checked');
        expect(form3852Component.otherElement.isSelected()).toBe(false, 'Verify that when select all is uncheck other is not checked');

        form3852Component.selectFDA3852CheckboxElement.click();
        form3852Component.clickSaveUpload();
        expect(form3852Component.uploadedDocumentNameElement.isPresent()).toBe(true, 'Verify file is uploaded');
    });

    it('Importer Monthly Report: User will verify a Document cannot be uploaded with no data entered', () => {
        form3852Component.uploadDocumentElement.click();
        browser.sleep(2000);
        form3852Component.clickSaveUpload();
        browser.sleep(2000);
        expect(form3852Component.pdfDocRequiredMessageElement.isDisplayed()).toBe(true, 'Verify Required Field Message: Error: PDF Document is required');
        expect(form3852Component.fileNameRequiredMessageElement.isDisplayed()).toBe(true, 'Verify Required Field Message: Error: File Name is required');
        expect(form3852Component.formTypeRequiredMessageElement.isDisplayed()).toBe(true, 'Verify Required Field Message: Error: Please choose a form type');
        form3852Component.cancelPopUpElement.click();
    });

    it('Importer Monthly Report: User will verify a Document can be Edited', () => {
        browser.sleep(2000);
        form3852Component.editDocumentElement.click();
        browser.sleep(2000);
        form3852Component.fileNameElement.clear();
        form3852Component.fileNameElement.sendKeys("Edited Filename");
        form3852Component.selectTTB5220CheckboxEditElement.click();
        form3852Component.clickSaveUpload();
        browser.sleep(2000);
        expect(form3852Component.uploadedDocumentNameElement.getText()).toEqual('Edited Filename', 'Verified File is edited');
    });

    it('Importer Monthly Report: User will verify a Document can be Downloaded', () => {
        form3852Component.clickDocumentationDownloadDocument();
        // expect(form3852Component.downloadingMessageElement.isPresent()).toBe(true, 'Verified downloading started');
        form3852Component.downloadingEnded();
        expect(form3852Component.uploadedDocumentNameElement.isPresent()).toBe(true, 'Verified downloading ended');
    });

    it('Importer Monthly Report: User will verify a Document can be Deleted', () => {
        browser.sleep(3000);
        form3852Component.deleteDocumentElement.click();
        browser.sleep(2000);
        form3852Component.clickConfirmDelete();
        expect(form3852Component.uploadedDocumentNameElement.isPresent()).toBe(false, 'Verified File is deleted');
    });
});

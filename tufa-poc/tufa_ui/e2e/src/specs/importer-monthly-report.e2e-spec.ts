import { browser } from 'protractor';
import { Form3852Component } from '../component/form-3852.co';
import { Form5220Component } from '../component/form-5220.co';
import { Form7501Component } from '../component/form-7501.co';
import { TobaccoClassSelectionComponent } from '../component/tobacco-class-selection.co';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { MonthlyReportPage } from '../page/monthly-report.po';
import { StartPage } from '../page/start.po';

describe('Create Importer Monthly Report Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let monthlyReportPage: MonthlyReportPage;
    let tobaccoClassSelection: TobaccoClassSelectionComponent;
    let form3852Component: Form3852Component;
    let form5220Component: Form5220Component;
    let form7501Component: Form7501Component;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            dailyOperationsPage = loginPage.clickConfirmButton();
            dailyOperationsPage.setSearchText('43-5365234');
            dailyOperationsPage.clickSearchButton();
            dailyOperationsPage.clickExtendDisplayButton();
            monthlyReportPage = dailyOperationsPage.clickPeriodOfActivity('January FY 2016');
            tobaccoClassSelection = new TobaccoClassSelectionComponent();
            form3852Component = new Form3852Component();
            form5220Component = new Form5220Component();
            form7501Component = new Form7501Component();

            
            tobaccoClassSelection.getAllSelectedTobaccoClasses().count().then(count => {
                //If there is tobacco classes currently selected, attempt to clear the form in order to start fresh.
                if(count > 0) {
                    tobaccoClassSelection.resetSelection();
                    monthlyReportPage.clickSaveReport();
                    browser.waitForAngular();
                }
            });
        });
    });

    it('Should be able to navigate to a Monthly Report.', () => {
        expect(monthlyReportPage.pageTitleElement.isDisplayed()).toBe(true, 'Expect Monthly Report title to be displayed');
    });

    it('Cigarettes line displays in the FDA 3852 form', () => {
        tobaccoClassSelection.clickCigarettesSelection();
        browser.waitForAngular().then(function () {
            expect(form3852Component.cigarettesTableRowElement.isDisplayed()).toBe(true, 'Expect Cigarettes FDA 3852 row to show');
        });
    });

    it('For Cigarettes in FDA 3852 form: Enter in 1000 into the Removals Amount section.', () => {
        form3852Component.setCigarettesRemovalsAmount('1000');
        expect(form3852Component.getCigarettesExciseTax()).toEqual('50.33', 'Expect Cigarettes Excise Tax is 50.33');
        expect(form3852Component.getCigarettesTaxDiffElement()).toEqual('$0.00', 'Expect Cigarettes Difference Tax is $0.00');
        expect(form3852Component.getCigarettesCallculatedTaxRateElement()).toEqual('50.33', 'Expect Cigarettes Calculated Tax Rate is 50.33');
        expect(form3852Component.getCigarettesStatusElement()).toEqual('Valid', 'Expect Cigarettes Activity as Valid');
    });

    it('Snuff line displays in the FDA 3852 form', () => {
        tobaccoClassSelection.clickSnuffSelection();
        browser.waitForAngular().then(function () {
            expect(form3852Component.snuffTableRowElement.isDisplayed()).toBe(true, 'Expect Snuff FDA 3852 row to show');
        });
    });

    it('For Snuff in FDA 3852 form: Enter in 5000 into the Removals Amount section.', () => {
        form3852Component.setSnuffRemovalsAmount('5000');
        expect(form3852Component.getSnuffExciseTaxField()).toEqual('7,550.00', 'Expect Snuff Excise Tax is 7,550.00');
        expect(form3852Component.getSnuffTaxDiffElement()).toEqual('$0.00', 'Expect Snuff Difference Tax is $0.00');
        expect(form3852Component.getSnuffCallculatedTaxRateElement()).toEqual('1.51', 'Expect Snuff Calculated Tax Rate is 1.51');
        expect(form3852Component.getSnuffStatusElement()).toEqual('Valid', 'Expect Snuff Activity as Valid');
    });

    it('Chew line displays in the FDA 3852 form', () => {
        tobaccoClassSelection.clickChewSelection();
        browser.waitForAngular().then(function () {
            expect(form3852Component.chewTableRowElement.isDisplayed()).toBe(true, 'Expect Chew FDA 3852 row to show');
        });
    });

    it('For Chew in FDA 3852 form: Enter in 4000 into the Removals Amount section.', () => {
        form3852Component.setChewRemovalsAmount('4000');
        expect(form3852Component.getChewExciseTaxField()).toEqual('2,013.20', 'Expect Chew Excise Tax is 2,013.20');
        expect(form3852Component.getChewTaxDiffElement()).toEqual('$0.00', 'Expect Chew Difference Tax is $0.00');
        expect(form3852Component.getChewCallculatedTaxRateElement()).toEqual('0.5033', 'Expect Chew Calculated Tax Rate is 0.5033');
        expect(form3852Component.getChewStatusElement()).toEqual('Valid', 'Expect Chew Activity as Valid');
    });

    it('Pipe line displays in the FDA 3852 form', () => {
        tobaccoClassSelection.clickPipeSelection();
        browser.waitForAngular().then(function () {
            expect(form3852Component.pipeTableRowElement.isDisplayed()).toBe(true, 'Expect Pipe FDA 3852 row to show');
        });
    });

    it('For Pipe in FDA 3852 form: Enter in 1000 into the Removals Amount section.', () => {
        form3852Component.setPipeRemovalsAmount('1000');
        expect(form3852Component.getPipeExciseTax()).toEqual('2,831.10', 'Expect Pipe Excise Tax is 2,813.10');
        expect(form3852Component.getPipeTaxDiffElement()).toEqual('$0.00', 'Expect Pipe Difference Tax is $0.00');
        expect(form3852Component.getPipeCallculatedTaxRateElement()).toEqual('2.8311', 'Expect Pipe Calculated Tax Rate is 2.8311');
        expect(form3852Component.getPipesStatusElement()).toEqual('Valid', 'Expect Pipe Activity as Valid');
    });

    it('Roll Your Own line displays in the FDA 3852 form', () => {
        tobaccoClassSelection.clickRollYourOwnSelection();
        browser.waitForAngular().then(function () {
            expect(form3852Component.rollYourOwnTableRowElement.isDisplayed()).toBe(true, 'Expect Roll Your Own FDA 3852 row to show');
        });
    });

    it('For Roll Your Own in FDA 3852 form: Enter in 2000 into the Removals Amount section.', () => {
        form3852Component.setRollYourOwnRemovalsAmount('2000');
        expect(form3852Component.getRollYourOwnExciseTax()).toEqual('49,560.00', 'Expect Roll Your Own Excise Tax is 49,560.00');
        expect(form3852Component.getRollYourOwnTaxDiffElement()).toEqual('$0.00', 'Expect Roll Your Own Difference Tax is $0.00');
        expect(form3852Component.getRollYourOwnCallculatedTaxRateElement()).toEqual('24.78', 'Expect Roll Your Own Calculated Tax Rate is 24.78');
        expect(form3852Component.getRollYourOwnStatusElement()).toEqual('Valid', 'Expect Roll Your Own Activity as Valid');
    });

    it('Cigar line displays in the FDA 3852 form', () => {
        tobaccoClassSelection.clickCigarsSelection();
        browser.waitForAngular().then(function () {
            expect(form3852Component.cigarsTableRowElement.isDisplayed()).toBe(true, 'Expect Cigar FDA 3852 row to show');
        });
    });

    it('For Cigar in FDA 3852 form: Enter in 4000 into the Removals Amount section.', () => {
        form3852Component.setCigarsRemovalsAmount('4000');
        expect(form3852Component.getCigarsExciseTax()).toEqual('', 'Expect No Cigar Excise Tax is 50.33');
        expect(form3852Component.getCigarsTaxDiffElement()).toEqual('', 'Expect No Cigar Difference Tax is null');
        expect(form3852Component.getCigarsCallculatedTaxRateElement()).toEqual('N/A', 'Expect Cigar Calculated Tax Rate is N/A');
        expect(form3852Component.getCigarsStatusElement()).toEqual('', 'Expect Cigar Activity as Nothing');
    });

    it('For Cigar in FDA 3852 form: Enter in 558.43 into the Excise Tax section.', () => {
        form3852Component.setCigarsExciseTax('558.43');
        expect(form3852Component.getCigarsTaxDiffElement()).toEqual('', 'Expect No Cigar Difference Tax is null');
        expect(form3852Component.getCigarsCallculatedTaxRateElement()).toEqual('N/A', 'Expect Cigar Calculated Tax Rate is N/A');
        expect(form3852Component.getCigarsStatusElement()).toEqual('', 'Expect Cigar Activity as Nothing');
    });

    it('Cigarettes line displays in the FDA 5220 form', () => {
        browser.waitForAngular().then(function () {
            expect(form5220Component.cigarettesTableRowElement.isDisplayed()).toBe(true, 'Expect Cigarettes FDA 5000 row to show');
        });
    });

    it('For Cigarettes in FDA 5220 form: Verify the Removal Amount for Cigarettes is 1000 for Small.', () => {
        expect(form5220Component.getCigarettesSmallRemovalsAmount()).toEqual('1,000', 'Expect Cigarettes Removal Amount for small is 1000');
        expect(form5220Component.getCigarettesDifferenceRemovalsAmountElement()).toEqual('0', 'Expect Cigarettes Difference Removals for small is 0 in 5220 form');
        expect(form5220Component.getCigarettesActivityElement()).toEqual('Valid', 'Expect Cigarettes Activity as Valid in 5220 form');
    });

    it('Snuff line displays in the FDA 5220 form', () => {
        browser.waitForAngular().then(function () {
            expect(form5220Component.snuffTableRowElement.isDisplayed()).toBe(true, 'Expect Snuff FDA 5220 row to show');
        });
    });

    it('For Snuff in FDA  5220 form: Verify the TTB 5220.6 form for Snuff .', () => {
        expect(form5220Component.getSnuffRemovalsAmount()).toEqual('5,000.000', 'Expect Snuff Removal Amount for small is 5000');
        expect(form5220Component.getSnuffDifferenceRemovalsAmountElement()).toEqual('0.000', 'Expect Snuff Difference Removals for small is 0 in 5220 form');
        expect(form5220Component.getSnuffActivityElement()).toEqual('Valid', 'Expect Snuff Activity as Valid in 5220 form');

    });

    it('Chew line displays in the FDA 5220 form', () => {
        browser.waitForAngular().then(function () {
            expect(form5220Component.chewTableRowElement.isDisplayed()).toBe(true, 'Expect Chew FDA 5220 row to show');
        });
    });

    it('For Chew in FDA 5220 form: Verify the TTB 5220.6 form for Chew ', () => {
        expect(form5220Component.getChewRemovalsAmount()).toEqual('4,000.000', 'Expect Chew Removal Amount for small is 4000');
        expect(form5220Component.getChewDifferenceRemovalsAmountElement()).toEqual('0.000', 'Expect Chew Difference Removals for small is 0 in 5220 form');
        expect(form5220Component.getChewActivityElement()).toEqual('Valid', 'Expect Chew Activity as Valid in 5220 form');

    });

    it('Pipe line displays in the FDA 5000 form', () => {
        browser.waitForAngular().then(function () {
            expect(form5220Component.pipeTableRowElement.isDisplayed()).toBe(true, 'Expect Pipe FDA 5220 row to show');
        });

    });

    it('For Pipe in FDA 5220 form: Verify the TTB 5220.6 form for Pipe ', () => {
        expect(form5220Component.getPipeRemovalsAmount()).toEqual('1,000.000', 'Expect Pipe Removal Amount for small is 1000');
        expect(form5220Component.getPipeDifferenceRemovalsAmountElement()).toEqual('0.000', 'Expect Pipe Difference Removals for small is 0 in 5220 form');
        expect(form5220Component.getPipeActivityElement()).toEqual('Valid', 'Expect Pipe Activity as Valid in 5220 form');

    });

    it('Roll Your Own line displays in the FDA 5220 form', () => {
        browser.waitForAngular().then(function () {
            expect(form5220Component.rollYourOwnTableRowElement.isDisplayed()).toBe(true, 'Expect Roll Your Own FDA 5220 row to show');
        });
    });

    it('For Roll Your Own in FDA 5220 form: Verify the TTB 5220.6 form For Roll Your Own ', () => {
        expect(form5220Component.getRollYourOwnRemovalsAmount()).toEqual('2,000.000', 'Expect RollYourOwn Removal Amount for small is 2000');
        expect(form5220Component.getRollYourOwnDifferenceRemovalsAmountElement()).toEqual('0.000', 'Expect RollYourOwn Difference Removals for small is 0 in 5220 form');
        expect(form5220Component.getRollYourOwnActivityElement()).toEqual('Valid', 'Expect RollYourOwn Activity as Valid in 5220 form');
    });

    // Kyle's 7501 form valid cases
    it('Should add a new line to the Cigarette 7501 form and populate fields', () => {
        form7501Component.clickCigarettesAddButton();
        form7501Component.setCigarettesVolumeField('1');
        expect(form7501Component.cigarettesTaxFieldElement.getAttribute('value')).toBe('50.33', '');
        expect(form7501Component.cigarettesRemovalsQuantityStatusElement.getText()).toBe('Valid', '');
        expect(form7501Component.cigarettesTaxAmountStatusElement.getText()).toBe('Valid', '');
        expect(form7501Component.cigarettesTotalStatusElement.getText()).toBe('Valid', '');
    });

    it('Should add a new line to the Snuff 7501 form and populate fields', () => {
        form7501Component.clickSnuffAddButton();
        form7501Component.setSnuffVolumeField('2268.00');
        expect(form7501Component.snuffTaxFieldElement.getAttribute('value')).toBe('7,550.05', '');
        expect(form7501Component.snuffRemovalsQuantityStatusElement.getText()).toBe('Valid', '');
        expect(form7501Component.snuffTaxAmountStatusElement.getText()).toBe('Valid', '');
        expect(form7501Component.snuffTotalStatusElement.getText()).toBe('Valid', '');
    });

    it('Should add a new line to the Chew 7501 form and populate fields', () => {
        form7501Component.clickChewAddButton();
        form7501Component.setChewVolumeField('1814');
        expect(form7501Component.chewTaxFieldElement.getAttribute('value')).toBe('2,012.77', '');
        expect(form7501Component.chewRemovalsQuantityStatusElement.getText()).toBe('Valid', '');
        expect(form7501Component.chewTaxAmountStatusElement.getText()).toBe('Valid', '');
        expect(form7501Component.chewTotalStatusElement.getText()).toBe('Valid', '');
    });

    it('Should add a new line to the Pipe 7501 form and populate fields', () => {
        form7501Component.clickPipeAddButton();
        form7501Component.setPipeVolumeField('453.59');
        expect(form7501Component.pipeTaxFieldElement.getAttribute('value')).toBe('2,831.05', '');
        expect(form7501Component.pipeRemovalsQuantityStatusElement.getText()).toBe('Valid', '');
        expect(form7501Component.pipeTaxAmountStatusElement.getText()).toBe('Valid', '');
        expect(form7501Component.pipeTotalStatusElement.getText()).toBe('Valid', '');
    });

    it('Should add a new line to the Roll Your Own 7501 form and populate fields', () => {
        form7501Component.clickRollYourOwnAddButton();
        form7501Component.setRollYourOwnVolumeField('907.180');
        expect(form7501Component.rollYourOwnTaxFieldElement.getAttribute('value')).toBe('49,559.23', '');
        expect(form7501Component.rollYourOwnRemovalsQuantityStatusElement.getText()).toBe('Valid', '');
        expect(form7501Component.rollYourOwnTaxAmountStatusElement.getText()).toBe('Valid', '');
        expect(form7501Component.rollYourOwnTotalStatusElement.getText()).toBe('Valid', '');
    });


    it('Save the report - Valid Cases', () => {
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            expect(monthlyReportPage.getReportStatusElement('Error')).toEqual('Error', 'Save Report Completed');
        });
    });

    it('Reset Valid report - Valid Cases', () => {
        tobaccoClassSelection.resetSelection();
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            expect(monthlyReportPage.getReportStatusElement('Not Started')).toEqual('Not Started', 'Reset Report Completed');
        });
    });
});

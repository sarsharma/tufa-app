import * as fs from 'fs';
import * as path from 'path';
import { browser, protractor } from 'protractor';
import { Form3852Component } from '../component/form-3852.co';
import { Form5000Component } from '../component/form-5000.co';
import { Form5210Component } from '../component/form-5210.co';
import { TobaccoClassSelectionComponent } from '../component/tobacco-class-selection.co';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { MonthlyReportPage } from '../page/monthly-report.po';
import { StartPage } from '../page/start.po';
import { CommonActions } from '../util/common-actions';
import { ScriptUtils } from '../util/script-utils';
import { StaticValues } from '../util/static-values';

describe('Manufacturer Monthly Report Download Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let monthlyReportPage: MonthlyReportPage;
    let tobaccoClassSelection: TobaccoClassSelectionComponent;
    let form3852Component: Form3852Component;
    let form5210Component: Form5210Component;
    let form5000Component: Form5000Component;
    let downloadsDir = path.resolve(__dirname, '../../data/downloaded-files/');
    let expectedFilesDir = path.resolve(__dirname, '../../data/expected-files/');

    beforeAll( () => {
        browser.waitForAngularEnabled(false).then(function () {
            CommonActions.login();
            monthlyReportPage = CommonActions.navigateToMonthlyReport('74-5546345', 'January FY 2016');
            tobaccoClassSelection = new TobaccoClassSelectionComponent();
            form3852Component = new Form3852Component();
            form5210Component = new Form5210Component();
            form5000Component = new Form5000Component();
        });
    });

    it('Should be able to navigate to a Manufacturer Monthly Report.', () => {
        expect(monthlyReportPage.pageTitleElement.isDisplayed()).toBe(true, 'Expect Monthly Report title to be displayed');
    });

    it('Reset report and verify Report status is "Not Started"', () => {
        tobaccoClassSelection.resetSelection();
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            expect(monthlyReportPage.getReportStatusElement('Not Started')).toEqual('Not Started', 'Reset Report Completed');
        });
    });
    
    describe('Download Scenarios for 3852 form', () => {
        it('Should verify all form 3852 values are entered correctly.', () => {
            tobaccoClassSelection.clickSelectAllSelection();
            form3852Component.setCigarettesRemovalsAmount('1000');
            form3852Component.setCigarsRemovalsAmount('1000');
            form3852Component.setCigarsExciseTax('1000');
            form3852Component.setSnuffRemovalsAmount('5000');
            form3852Component.setChewRemovalsAmount('4000');
            form3852Component.setPipeRemovalsAmount('1000');
            form3852Component.setRollYourOwnRemovalsAmount('2000');

            expect(form3852Component.getCigarettesTaxDiffElement()).toEqual('$0.00', 'Expect Cigarettes Difference Tax is $0.00');
            expect(form3852Component.getCigarettesCallculatedTaxRateManuElement()).toEqual('50.33', 'Expect Cigarettes Calculated Tax Rate is 50.33');
            expect(form3852Component.getCigarettesStatusElement()).toEqual('Valid', 'Expect Cigarettes Activity as Valid');
            expect(form3852Component.getCigarsTaxDiffElement()).toEqual('', 'Expect Cigar Tax Difference to be blank');
            expect(form3852Component.getCigarsCallculatedTaxRateElement()).toEqual('N/A', 'Expect Cigar Calculated Tax Rate is N/A');
            expect(form3852Component.getCigarsStatusElement()).toEqual('', 'Expect Cigar Activity as blank');
            expect(form3852Component.getSnuffExciseTaxField()).toEqual('7,550.00', 'Expect Snuff Excise Tax is 7,550.00');
            expect(form3852Component.getSnuffTaxDiffElement()).toEqual('$0.00', 'Expect Snuff Difference Tax is $0.00');
            expect(form3852Component.getSnuffCallculatedTaxRateElement()).toEqual('1.51', 'Expect Snuff Calculated Tax Rate is 1.51');
            expect(form3852Component.getSnuffStatusElement()).toEqual('Valid', 'Expect Snuff Activity as Valid');
            expect(form3852Component.getChewExciseTaxField()).toEqual('2,013.20', 'Expect Chew Excise Tax is 2,013.20');
            expect(form3852Component.getChewTaxDiffElement()).toEqual('$0.00', 'Expect Chew Difference Tax is $0.00');
            expect(form3852Component.getChewCallculatedTaxRateElement()).toEqual('0.5033', 'Expect Chew Calculated Tax Rate is 0.5033');
            expect(form3852Component.getChewStatusElement()).toEqual('Valid', 'Expect Chew Activity as Valid');
            expect(form3852Component.getPipeExciseTax()).toEqual('2,831.10', 'Expect Pipe Excise Tax is 2,813.10');
            expect(form3852Component.getPipeTaxDiffElement()).toEqual('$0.00', 'Expect Pipe Difference Tax is $0.00');
            expect(form3852Component.getPipeCallculatedTaxRateElement()).toEqual('2.8311', 'Expect Pipe Calculated Tax Rate is 2.8311');
            expect(form3852Component.getPipesStatusElement()).toEqual('Valid', 'Expect Pipe Activity as Valid');
            expect(form3852Component.getRollYourOwnExciseTax()).toEqual('49,560.00', 'Expect Roll Your Own Excise Tax is 49,560.00');
            expect(form3852Component.getRollYourOwnTaxDiffElement()).toEqual('$0.00', 'Expect Roll Your Own Difference Tax is $0.00');
            expect(form3852Component.getRollYourOwnCallculatedTaxRateElement()).toEqual('24.78', 'Expect Roll Your Own Calculated Tax Rate is 24.78');
            expect(form3852Component.getRollYourOwnStatusElement()).toEqual('Valid', 'Expect Roll Your Own Activity as Valid');
        });

        it('Should download form 3852 csv file and verify filename to be as expected.', () => {            
            monthlyReportPage.clickSaveReport();
            browser.sleep(1000).then( function() {
                form3852Component.clickDownloadDocument();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(() => {
                    expect(fs.existsSync(path.resolve(downloadsDir, StaticValues.MANUFACTURER_DOWNLOADED_FORM_3852_FILENAME)))
                        .toBe(true, 'Expected file '+ StaticValues.MANUFACTURER_DOWNLOADED_FORM_3852_FILENAME +' to be present in download directory.');
                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

        });

        it('Should verify downloaded Form 3852 file to contain correct expected values.', () => { 
            try {
                const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, StaticValues.MANUFACTURER_DOWNLOADED_FORM_3852_FILENAME), { encoding: 'utf8' });
                const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.MANUFACTURER_EXPECTED_FORM_3852_FILENAME), { encoding: 'utf8' });
                expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
            } catch (err) {
                fail(`ERROR: Exception thrown: ${err.message}`);
            }
        });

        afterAll(async () => { 
            require('fs-extra').emptyDirSync(downloadsDir);
        });

    });

    describe('Download Scenarios for 5210.5 form', () => {
        it('Should verify all form 5210.5 values are entered correctly.', () => {
            form5210Component.setCigarsSmallRemovalsAmount('0');
            form5210Component.setCigarettesLargeRemovalsAmount('0');

            form5210Component.clickEnableAllAdjustments();
            form5210Component.setCigarettesLineAdjustmentAmount('1');
            form5210Component.setCigarsLineAdjustmentAmount('1');
            form5210Component.setSnuffLineAdjustmentAmount('1');
            form5210Component.setChewLineAdjustmentAmount('1');
            form5210Component.setPipeLineAdjustmentAmount('1');
            form5210Component.setRollYourOwnLineAdjustmentAmount('1');

            // Cigarettes check
            expect(form5210Component.getCigarettesSmallRemovalsAmount()).toEqual('1,000', 'Expected Cigarettes Removal Amount for small to be 1000');
            expect(form5210Component.getCigarettesLargeRemovalsAmount()).toEqual('0', 'Expected Cigarettes Removal Amount for large to be 0');
            expect(form5210Component.getCigarettesDifferenceRemovalsAmountElement()).toEqual('1', 'Expected Cigarettes Difference Removals to be 1 in 5210 form');
            expect(form5210Component.getCigarettesActivityElement()).toEqual('Valid', 'Expected Cigarettes Activity as Valid in 5210 form');
            // Cigars check
            expect(form5210Component.getCigarsSmallRemovalsAmount()).toEqual('0', 'Expected Cigars Removal Amount for small to be 0');
            expect(form5210Component.getCigarsLargeRemovalsAmount()).toEqual('1,000', 'Expected Cigars Removal Amount for large to be 1000');
            expect(form5210Component.getCigarsDifferenceRemovalsAmountElement()).toEqual('1', 'Expected Cigars Difference Removals to be 1 in 5210 form');
            expect(form5210Component.getCigarsActivityElement()).toEqual('Valid', 'Expected Cigars Activity as Valid in 5210 form');
            // Snuff Check
            expect(form5210Component.getSnuffRemovalsAmount()).toEqual('5,000.000', 'Expected Snuff Removal Amount to be 5,000.000');
            expect(form5210Component.getSnuffDifferenceRemovalsAmountElement()).toEqual('1.000', 'Expected Snuff Difference Removals to be 1.000 in 5210 form');
            expect(form5210Component.getSnuffActivityElement()).toEqual('Valid', 'Expected Snuff Activity as Valid in 5210 form');
            // Chew Check
            expect(form5210Component.getChewRemovalsAmount()).toEqual('4,000.000', 'Expected Chew Removal Amount to be 4,000.000');
            expect(form5210Component.getChewDifferenceRemovalsAmountElement()).toEqual('1.000', 'Expected Chew Difference Removals to be 1.000 in 5210 form');
            expect(form5210Component.getChewActivityElement()).toEqual('Valid', 'Expected Chew Activity as Valid in 5210 form');
            // Pipe Check
            expect(form5210Component.getPipeRemovalsAmount()).toEqual('1,000.000', 'Expected Pipe Removal Amount to be 1,000.000');
            expect(form5210Component.getPipeDifferenceRemovalsAmountElement()).toEqual('1.000', 'Expected Pipe Difference Removals to be 1.000 in 5210 form');
            expect(form5210Component.getPipeActivityElement()).toEqual('Valid', 'Expected Pipe Activity as Valid in 5210 form');
            // Roll Your Own Check
            expect(form5210Component.getRollYourOwnRemovalsAmount()).toEqual('2,000.000', 'Expected Roll Your Own Removal Amount to be 2,000.000');
            expect(form5210Component.getRollYourOwnDifferenceRemovalsAmountElement()).toEqual('1.000', 'Expected Roll Your Own Difference Removals to be 1.000 in 5210 form');
            expect(form5210Component.getRollYourOwnActivityElement()).toEqual('Valid', 'Expected Roll Your Own Activity as Valid in 5210 form');
        });

        it('Should download Form 5210.5 csv file and verify filename to be as expected.', () => {            
            monthlyReportPage.clickSaveReport();
            browser.sleep(1000).then( function() {
                form5210Component.clickDownloadDocument();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(() => {
                    expect(fs.existsSync(path.resolve(downloadsDir, StaticValues.MANUFACTURER_DOWNLOADED_FORM_5210_FILENAME)))
                        .toBe(true, 'Expected file '+ StaticValues.MANUFACTURER_DOWNLOADED_FORM_5210_FILENAME +' to be present in download directory.');
                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

        });

        it('Should verify downloaded Form 5210.5 file to contain correct expected values.', () => { 
            try {
                const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, StaticValues.MANUFACTURER_DOWNLOADED_FORM_5210_FILENAME), { encoding: 'utf8' });
                const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.MANUFACTURER_EXPECTED_FORM_5210_FILENAME), { encoding: 'utf8' });
                expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
            } catch (err) {
                fail(`ERROR: Exception thrown: ${err.message}`);
            }
        });

        afterAll(async () => { 
            require('fs-extra').emptyDirSync(downloadsDir);
        });
    });

    describe('Download Scenarios for 5000.24 form', () => {
        it('Should verify all form 5000.24 values are entered correctly.', () => {
            form3852Component.setCigarettesRemovalsAmount('2500');
            form3852Component.setCigarsRemovalsAmount('2500');
            form3852Component.setCigarsExciseTax('1000');
            form3852Component.setSnuffRemovalsAmount('2500');
            form3852Component.setChewRemovalsAmount('2500');
            form3852Component.setPipeRemovalsAmount('2500');
            form3852Component.setRollYourOwnRemovalsAmount('2500');

            form5000Component.setCigarTaxAmountBreakDown('800', 0);
            form5000Component.setCigarTaxAmountBreakDown('200', 1);
            form5000Component.setCigarettesTaxAmountBreakDown('120', 0);
            form5000Component.setCigarettesTaxAmountBreakDown('5.83', 1);
            form5000Component.setChewSnuffTaxAmountBreakDown('3775', 0);
            form5000Component.setChewSnuffTaxAmountBreakDown('1258.25', 1);
            form5000Component.setPipeRollYourOwnTaxAmountBreakDown('7077.75', 0);
            form5000Component.setPipeRollYourOwnTaxAmountBreakDown('61950', 1);

            // Cigars check
            expect(form5000Component.getDifferenceText(1)).toEqual('$0.00', 'Expected Cigars difference amount to be $0.00');
            expect(form5000Component.getActivityText(1)).toEqual('Valid', 'Expect Cigars activity to be Valid');
            // Cigarettes check
            expect(form5000Component.getDifferenceText(2)).toEqual('$0.00', 'Expected Cigarettes difference amount to be $0.00');
            expect(form5000Component.getActivityText(2)).toEqual('Valid', 'Expect Cigarettes activity to be Valid');
            // Chew/Snuff check
            expect(form5000Component.getDifferenceText(3)).toEqual('$0.00', 'Expected Chew/Snuff difference amount to be $0.00');
            expect(form5000Component.getActivityText(3)).toEqual('Valid', 'Expect Chew/Snuff activity to be Valid');
            // Pipe/Roll Your Own check
            expect(form5000Component.getDifferenceText(4)).toEqual('$0.00', 'Expected Pipe/Roll Your Own difference amount to be $0.00');
            expect(form5000Component.getActivityText(4)).toEqual('Valid', 'Expect Pipe/Roll Your Own activity to be Valid');
        
        });

        it('Should download Form 5000.24 csv file and verify filename to be as expected.', () => {            
            monthlyReportPage.clickSaveReport();
            browser.sleep(1000).then( function() {
                form5000Component.clickDownloadDocument();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(() => {
                    expect(fs.existsSync(path.resolve(downloadsDir, StaticValues.MANUFACTURER_DOWNLOADED_FORM_5000_FILENAME)))
                        .toBe(true, 'Expected file '+ StaticValues.MANUFACTURER_DOWNLOADED_FORM_5000_FILENAME +' to be present in download directory.');
                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

        });

        it('Should verify downloaded Form 5000.24 file to contain correct expected values.', () => { 
            try {
                const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, StaticValues.MANUFACTURER_DOWNLOADED_FORM_5000_FILENAME), { encoding: 'utf8' });
                const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.MANUFACTURER_EXPECTED_FORM_5000_FILENAME), { encoding: 'utf8' });
                expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
            } catch (err) {
                fail(`ERROR: Exception thrown: ${err.message}`);
            }
        });

        afterAll(async () => { 
            require('fs-extra').emptyDirSync(downloadsDir);
        });
    });
    
    it('Reset report and verify Report status is "Not Started"', () => {
        tobaccoClassSelection.clickSelectAllSelection();
        browser.wait(protractor.ExpectedConditions.invisibilityOf(tobaccoClassSelection.allSelectedTobaccoClassesElements.get(0)));
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            browser.sleep(3000);
            expect(monthlyReportPage.getReportStatusElement('Not Started')).toEqual('Not Started', 'Reset Report Completed');
        });
    });

});
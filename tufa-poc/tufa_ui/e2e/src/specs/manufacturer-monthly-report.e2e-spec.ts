import { browser } from 'protractor';
import { Form3852Component } from '../component/form-3852.co';
import { Form7501Component } from '../component/form-7501.co';
import { TobaccoClassSelectionComponent } from '../component/tobacco-class-selection.co';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { MonthlyReportPage } from '../page/monthly-report.po';
import { StartPage } from '../page/start.po';
import { Form5210Component } from '../component/form-5210.co';
import { Form5000Component } from '../component/form-5000.co';

describe('Manufacturer Monthly Report Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let monthlyReportPage: MonthlyReportPage;
    let tobaccoClassSelection: TobaccoClassSelectionComponent;
    let form3852Component: Form3852Component;
    let form5210Component: Form5210Component;
    let form7501Component: Form7501Component;
    let form5000Component: Form5000Component;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            dailyOperationsPage = loginPage.clickConfirmButton();
            dailyOperationsPage.setSearchText('74-5546345');
            dailyOperationsPage.clickSearchButton();
            dailyOperationsPage.clickExtendDisplayButton();
            monthlyReportPage = dailyOperationsPage.clickPeriodOfActivity('January FY 2016');
            tobaccoClassSelection = new TobaccoClassSelectionComponent();
            form3852Component = new Form3852Component();
            form5210Component = new Form5210Component();
            form7501Component = new Form7501Component();
            form5000Component = new Form5000Component();
        });
    });

    /**
     * Success Validations
     */
    it('Should be able to navigate to a Monthly Report.', () => {
        expect(monthlyReportPage.pageTitleElement.isDisplayed()).toBe(true, 'Expect Monthly Report title to be displayed');
    });
    
    it('Reset report and verify Report status is "Not Started"', () => {
        tobaccoClassSelection.resetSelection();
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            expect(monthlyReportPage.getReportStatusElement('Not Started')).toEqual('Not Started', 'Reset Report Completed');
        });
    });
    
    it('Should show Cigarettes forms.', () => {
        tobaccoClassSelection.clickCigarettesSelection();
        browser.waitForAngular().then(function () {
            expect(form3852Component.cigarettesTableRowElement.isDisplayed()).toBe(true, 'Expect Cigarettes 3852 row to show');
        });
    });

    it('Cigarettes line displays in the FDA 3852 form', () => {
        browser.waitForAngular().then(function () {
            expect(form3852Component.cigarettesTableRowElement.isDisplayed()).toBe(true, 'Expect Cigarettes FDA 3852 row to show');
        });
    });

    it('For Cigarettes in FDA 3852 form: Enter in 1000 into the Removals Amount section.', () => {
        form3852Component.setCigarettesRemovalsAmount('1000');
        expect(form3852Component.getCigarettesExciseTax()).toEqual('50.33', 'Expect Cigarettes Excise Tax is 50.33');
        expect(form3852Component.getCigarettesTaxDiffElement()).toEqual('$0.00', 'Expect Cigarettes Difference Tax is $0.00');
        expect(form3852Component.getCigarettesCallculatedTaxRateManuElement()).toEqual('50.33', 'Expect Cigarettes Calculated Tax Rate is 50.33');
        expect(form3852Component.getCigarettesStatusElement()).toEqual('Valid', 'Expect Cigarettes Activity as Valid');
    });

    it('Snuff line displays in the FDA 3852 form', () => {
        tobaccoClassSelection.clickSnuffSelection();
        browser.waitForAngular().then(function () {
            expect(form3852Component.snuffTableRowElement.isDisplayed()).toBe(true, 'Expect Snuff FDA 3852 row to show');
        });
    });

    it('For Snuff in FDA 3852 form: Enter in 5000 into the Removals Amount section.', () => {
        form3852Component.setSnuffRemovalsAmount('5000');
        expect(form3852Component.getSnuffExciseTaxField()).toEqual('7,550.00', 'Expect Snuff Excise Tax is 7,550.00');
        expect(form3852Component.getSnuffTaxDiffElement()).toEqual('$0.00', 'Expect Snuff Difference Tax is $0.00');
        expect(form3852Component.getSnuffCallculatedTaxRateElement()).toEqual('1.51', 'Expect Snuff Calculated Tax Rate is 1.51');
        expect(form3852Component.getSnuffStatusElement()).toEqual('Valid', 'Expect Snuff Activity as Valid');
    });

    it('Chew line displays in the FDA 3852 form', () => {
        tobaccoClassSelection.clickChewSelection();
        browser.waitForAngular().then(function () {
            expect(form3852Component.chewTableRowElement.isDisplayed()).toBe(true, 'Expect Chew FDA 3852 row to show');
        });
    });

    it('For Chew in FDA 3852 form: Enter in 5000 into the Removals Amount section.', () => {
        form3852Component.setChewRemovalsAmount('4000');
        expect(form3852Component.getChewExciseTaxField()).toEqual('2,013.20', 'Expect Chew Excise Tax is 2,013.20');
        expect(form3852Component.getChewTaxDiffElement()).toEqual('$0.00', 'Expect Chew Difference Tax is $0.00');
        expect(form3852Component.getChewCallculatedTaxRateElement()).toEqual('0.5033', 'Expect Chew Calculated Tax Rate is 0.5033');
        expect(form3852Component.getChewStatusElement()).toEqual('Valid', 'Expect Chew Activity as Valid');
    });

    it('Pipe line displays in the FDA 3852 form', () => {
        tobaccoClassSelection.clickPipeSelection();
        browser.waitForAngular().then(function () {
            expect(form3852Component.pipeTableRowElement.isDisplayed()).toBe(true, 'Expect Pipe FDA 3852 row to show');
        });
    });

    it('For Pipe in FDA 3852 form: Enter in 5000 into the Removals Amount section.', () => {
        form3852Component.setPipeRemovalsAmount('1000');
        expect(form3852Component.getPipeExciseTax()).toEqual('2,831.10', 'Expect Pipe Excise Tax is 2,813.10');
        expect(form3852Component.getPipeTaxDiffElement()).toEqual('$0.00', 'Expect Pipe Difference Tax is $0.00');
        expect(form3852Component.getPipeCallculatedTaxRateElement()).toEqual('2.8311', 'Expect Pipe Calculated Tax Rate is 2.8311');
        expect(form3852Component.getPipesStatusElement()).toEqual('Valid', 'Expect Pipe Activity as Valid');
    });

    it('Roll Your Own line displays in the FDA 3852 form', () => {
        tobaccoClassSelection.clickRollYourOwnSelection();
        browser.waitForAngular().then(function () {
            expect(form3852Component.rollYourOwnTableRowElement.isDisplayed()).toBe(true, 'Expect Roll Your Own FDA 3852 row to show');
        });
    });

    it('For Roll Your Own in FDA 3852 form: Enter in 5000 into the Removals Amount section.', () => {
        form3852Component.setRollYourOwnRemovalsAmount('2000');
        expect(form3852Component.getRollYourOwnExciseTax()).toEqual('49,560.00', 'Expect Roll Your Own Excise Tax is 49,560.00');
        expect(form3852Component.getRollYourOwnTaxDiffElement()).toEqual('$0.00', 'Expect Roll Your Own Difference Tax is $0.00');
        expect(form3852Component.getRollYourOwnCallculatedTaxRateElement()).toEqual('24.78', 'Expect Roll Your Own Calculated Tax Rate is 24.78');
        expect(form3852Component.getRollYourOwnStatusElement()).toEqual('Valid', 'Expect Roll Your Own Activity as Valid');
    });
    
    it('Cigarettes line displays in the FDA 5210 form', () => {
        browser.waitForAngular().then(function () {
            expect(form5210Component.cigarettesTableRowElement.isDisplayed()).toBe(true, 'Expect Cigarettes FDA 5210 row to show');
        });
    });

    it('For Cigarettes in FDA 5210 form: Verify the Removal Amount for Cigarettes is 1000 for Small.', () => {
        expect(form5210Component.getCigarettesSmallRemovalsAmount()).toEqual('1,000', 'Expect Cigarettes Removal Amount for small is 1000');
        expect(form5210Component.getCigarettesDifferenceRemovalsAmountElement()).toEqual('0', 'Expect Cigarettes Difference Removals for small is 0 in 5210 form');
        expect(form5210Component.getCigarettesActivityElement()).toEqual('Valid', 'Expect Cigarettes Activity as Valid in 5210 form');
    });

    it('Snuff line displays in the FDA 5210 form', () => {
        browser.waitForAngular().then(function () {
            expect(form5210Component.snuffTableRowElement.isDisplayed()).toBe(true, 'Expect Snuff FDA 5210 row to show');
        });
    });

    it('For Snuff in FDA  5210 form: Verify the TTB 5210 form for Snuff .', () => {
        expect(form5210Component.getSnuffRemovalsAmount()).toEqual('5,000.000', 'Expect Snuff Removal Amount for small is 5000');
        expect(form5210Component.getSnuffDifferenceRemovalsAmountElement()).toEqual('0.000', 'Expect Snuff Difference Removals for small is 0 in 5210 form');
        expect(form5210Component.getSnuffActivityElement()).toEqual('Valid', 'Expect Snuff Activity as Valid in 5210 form');

    });

    it('Chew line displays in the FDA 5210 form', () => {
        browser.waitForAngular().then(function () {
            expect(form5210Component.chewTableRowElement.isDisplayed()).toBe(true, 'Expect Chew FDA 5210 row to show');
        });
    });

    it('For Chew in FDA 5210 form: Verify the TTB 5210 form for Chew ', () => {
        expect(form5210Component.getChewRemovalsAmount()).toEqual('4,000.000', 'Expect Chew Removal Amount for small is 4000');
        expect(form5210Component.getChewDifferenceRemovalsAmountElement()).toEqual('0.000', 'Expect Chew Difference Removals for small is 0 in 5210 form');
        expect(form5210Component.getChewActivityElement()).toEqual('Valid', 'Expect Chew Activity as Valid in 5210 form');

    });

    it('Pipe line displays in the FDA 5210 form', () => {
        browser.waitForAngular().then(function () {
            expect(form5210Component.pipeTableRowElement.isDisplayed()).toBe(true, 'Expect Pipe FDA 5210 row to show');
        });

    });

    it('For Pipe in FDA 5210 form: Verify the TTB 5210 form for Pipe ', () => {
        expect(form5210Component.getPipeRemovalsAmount()).toEqual('1,000.000', 'Expect Pipe Removal Amount for small is 1000');
        expect(form5210Component.getPipeDifferenceRemovalsAmountElement()).toEqual('0.000', 'Expect Pipe Difference Removals for small is 0 in 5210 form');
        expect(form5210Component.getPipeActivityElement()).toEqual('Valid', 'Expect Pipe Activity as Valid in 5210 form');

    });

    it('Roll Your Own line displays in the FDA 5210 form', () => {
        browser.waitForAngular().then(function () {
            expect(form5210Component.rollYourOwnTableRowElement.isDisplayed()).toBe(true, 'Expect Roll Your Own FDA 5210 row to show');
        });
    });

    it('For Roll Your Own in FDA 5220 form: Verify the TTB 5210 form For Roll Your Own ', () => {
        expect(form5210Component.getRollYourOwnRemovalsAmount()).toEqual('2,000.000', 'Expect RollYourOwn Removal Amount for small is 2000');
        expect(form5210Component.getRollYourOwnDifferenceRemovalsAmountElement()).toEqual('0.000', 'Expect RollYourOwn Difference Removals for small is 0 in 5210 form');
        expect(form5210Component.getRollYourOwnActivityElement()).toEqual('Valid', 'Expect RollYourOwn Activity as Valid in 5210 form');
    });

    it('Should verify success scenarios for Cigarettes in FDA 5000 form.', () => {
        browser.sleep(2000);
        form5000Component.setCigarettesTaxAmountBreakDown('50.33', 0);
        expect(form5000Component.getDifferenceText(1)).toEqual('$0.00', 'Expected Cigarettes difference amount to be $0.00');
        expect(form5000Component.getActivityText(1)).toEqual('Valid', 'Expect Cigarettes activity to be Valid');
    });

    it('Should verify success scenarios for Chew/Snuff in FDA 5000 form.', () => {
        form5000Component.setChewSnuffTaxAmountBreakDown('9563.20', 0);
        expect(form5000Component.getDifferenceText(2)).toEqual('$0.00', 'Expected Chew/Snuff difference amount to be $0.00');
        expect(form5000Component.getActivityText(2)).toEqual('Valid', 'Expect Chew/Snuff activity to be Valid');
    });

    it('Should verify success scenarios for Pipe/Roll Your Own in FDA 5000 form.', () => {
        form5000Component.setPipeRollYourOwnTaxAmountBreakDown('52391.10', 0);
        expect(form5000Component.getDifferenceText(3)).toEqual('$0.00', 'Expected Pipe/Roll Your Own difference amount to be $0.00');
        expect(form5000Component.getActivityText(3)).toEqual('Valid', 'Expect Pipe/Roll Your Own activity to be Valid');
    });

    /**
     * Error validations
     */

     //FDA 3852 error scenarios
    it('For Cigarettes in FDA 3852 form: Verify for error status', () => {
        form3852Component.setCigarettesRemovalsAmount('1000');
        expect(form3852Component.getCigarettesExciseTax()).toEqual('50.33', 'Expect Cigarettes Excise Tax is 50.33');
        form3852Component.setCigarettesExciseTax('60');
        expect(form3852Component.getCigarettesTaxDiffElement()).toEqual('$9.67', 'Expect Cigarettes Difference Tax is $9.67');
        expect(form3852Component.getCigarettesCallculatedTaxRateManuElement()).toEqual('60', 'Expect Cigarettes Calculated Tax Rate is 60');
        expect(form3852Component.getCigarettesStatusElement()).toEqual('Error', 'Expect Cigarettes Activity as Error');
    });
    it('For Snuff in FDA 3852 form: Verify for error status', () => {
        form3852Component.setSnuffRemovalsAmount('6000');
        expect(form3852Component.getSnuffExciseTaxField()).toEqual('9,060.00', 'Expect Snuff Excise Tax is 9,060.00');
        form3852Component.setSnuffExciseTax('5000');
        expect(form3852Component.getSnuffTaxDiffElement()).toEqual('($4,060.00)', 'Expect Snuff Difference Tax is ($4,060.00)');
        expect(form3852Component.getSnuffCallculatedTaxRateElement()).toEqual('0.833333', 'Expect Snuff Calculated Tax Rate is 0.833333');
        expect(form3852Component.getSnuffStatusElement()).toEqual('Error', 'Expect Snuff Activity as Error');
    });
    it('For Chew in FDA 3852 form: Verify for error status', () => {
        form3852Component.setChewRemovalsAmount('4000');
        expect(form3852Component.getChewExciseTaxField()).toEqual('2,013.20', 'Expect Chew Excise Tax is 2,013.20');
        form3852Component.setChewExciseTax('3000');
        expect(form3852Component.getChewTaxDiffElement()).toEqual('$986.80', 'Expect Chew Difference Tax is $986.80');
        expect(form3852Component.getChewCallculatedTaxRateElement()).toEqual('0.75', 'Expect Chew Calculated Tax Rate is 0.75');
        expect(form3852Component.getChewStatusElement()).toEqual('Error', 'Expect Chew Activity as Error');
    });

    it('For Pipe in FDA 3852 form: Verify for error status', () => {
        form3852Component.setPipeRemovalsAmount('5000');
        expect(form3852Component.getPipeExciseTax()).toEqual('14,155.50', 'Expect Pipe Excise Tax is 14,155.50');
        form3852Component.setPipeExciseTax('4155.50');
        expect(form3852Component.getPipeTaxDiffElement()).toEqual('($10,000.00)', 'Expect Pipe Difference Tax is ($10,000.00)');
        expect(form3852Component.getPipeCallculatedTaxRateElement()).toEqual('0.8311', 'Expect Pipe Calculated Tax Rate is 0.8311');
        expect(form3852Component.getPipesStatusElement()).toEqual('Error', 'Expect Pipe Activity as Error');
    });
    it('For Roll Your Own in FDA 3852 form: Verify for error status', () => {
        form3852Component.setRollYourOwnRemovalsAmount('3000');
        expect(form3852Component.getRollYourOwnExciseTax()).toEqual('74,340.00', 'Expect Roll Your Own Excise Tax is 74,340.00');
        form3852Component.setRollYourOwnExciseTax('54000');
        expect(form3852Component.getRollYourOwnTaxDiffElement()).toEqual('($20,340.00)', 'Expect Roll Your Own Difference Tax is ($20,340.00)');
        expect(form3852Component.getRollYourOwnCallculatedTaxRateElement()).toEqual('18', 'Expect Roll Your Own Calculated Tax Rate is 18');
        expect(form3852Component.getRollYourOwnStatusElement()).toEqual('Error', 'Expect Roll Your Own Activity as Error');
    });

    //FDA 5210 error scenarios
    it('For Cigarettes in FDA 5210 form: Change value to 2000 and verify error status.', () => {
        form5210Component.setCigarettesSmallRemovalsAmount('2000');
        expect(form5210Component.getCigarettesDifferenceRemovalsAmountElement()).toEqual('(1,000)', 'Expect Cigarettes Removal Amount for small is (1000)');
        expect(form5210Component.getCigarettesActivityElement()).toEqual('Error', 'Expect Cigarettes Activity as Error in 5210 form');
    });

    it('For Snuff in FDA 5210 form: Change value to 2000 for Snuff and verify error status.', () => {
        form5210Component.setSnuffRemovalsAmount('2000');
        expect(form5210Component.getSnuffDifferenceRemovalsAmountElement()).toEqual('4,000.000', 'Expect Snuff Removal Amount for small is 4,000');
        expect(form5210Component.getSnuffActivityElement()).toEqual('Error', 'Expect Snuff Activity as Error in 5210 form');

    });

    it('For Chew in FDA 5210 form: Change value to 5000 for Chew and verify error status.', () => {
        form5210Component.setChewRemovalsAmount('5000');
        expect(form5210Component.getChewDifferenceRemovalsAmountElement()).toEqual('(1,000.000)', 'Expect Chew Removal Amount for small is (1,000.000)');
        expect(form5210Component.getChewActivityElement()).toEqual('Error', 'Expect Chew Activity as Error in 5210 form');

    });

    it('For Pipe in FDA 5210 form: Change value to 2000 for Pipe and verify error status ', () => {
        form5210Component.setPipeRemovalsAmount('2000');
        expect(form5210Component.getPipeDifferenceRemovalsAmountElement()).toEqual('3,000.000', 'Expect Pipe Removal Amount for small is 3,000.000');
        expect(form5210Component.getPipeActivityElement()).toEqual('Error', 'Expect Pipe Activity as Error in 5210 form');
    });

    it('For Roll Your Own in FDA 5220 form: Change value to 2000 for Roll Your Own and verify error status.', () => {
        form5210Component.setRollYourOwnRemovalsAmount('2000');
        expect(form5210Component.getRollYourOwnDifferenceRemovalsAmountElement()).toEqual('1,000.000', 'Expect RollYourOwn Removal Amount for small is 1,000.000');
        expect(form5210Component.getRollYourOwnActivityElement()).toEqual('Error', 'Expect RollYourOwn Activity as Error in 5210 form');
    });

    
    it('Should show "Missing supporting documentation" warning message', () => {
        expect(monthlyReportPage.missingDocWarningElement.isDisplayed()).toEqual(true, 'Expected missing documentation warning message');    
    });

    it('Should show "Invalid information in form FDA 3852" warning message', () => {
        expect(monthlyReportPage.form3852WarningElement.isDisplayed()).toEqual(true, 'Expected "Invalid information in form FDA 3852" warning message');    
    });

    it('Should show "Invalid information in form TTB 5210.5" warning message', () => {
        expect(monthlyReportPage.form5210WarningElement.isDisplayed()).toEqual(true, 'Expected "Invalid information in form TTB 5210.5" warning message');    
    });

    it('Should show "Invalid information in form TTB 5000.24" warning message', () => {
        expect(monthlyReportPage.form5000WarningElement.isDisplayed()).toEqual(true, 'Expected "Invalid information in form TTB 5000.24" warning message');    
    });

    
    it('Save the report', () => {
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            expect(monthlyReportPage.getReportStatusElement('Error')).toEqual('Error', 'Save Report Completed');
        });
    });
    
    it('Reset report', () => {
        tobaccoClassSelection.resetSelection();
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            expect(monthlyReportPage.getReportStatusElement('Not Started')).toEqual('Not Started', 'Reset Report Completed');
        });
    });

});
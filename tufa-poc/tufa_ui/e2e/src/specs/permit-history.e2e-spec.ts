import { browser, ElementFinder } from 'protractor';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { StartPage } from '../page/start.po';
import { PermitHistoryPage } from '../page/permit-history.po';
import { CompanyDetailPage } from '../page/company-detail.po';
import { ScriptUtils } from '../util/script-utils';
import { MonthlyReportPage } from '../page/monthly-report.po';
import { BreadcrumbNavigationComponent } from '../component/navigation.co';

describe('Create Permit History Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let permitHistoryPage: PermitHistoryPage;
    let companyDetailsPage: CompanyDetailPage;
    let monthlyReportPage: MonthlyReportPage;
    let breadcrumbNavigation: BreadcrumbNavigationComponent;
    // let permitNum = 'TX-TI-37283';
    let permitNum = 'FL-TI-2323';

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            dailyOperationsPage = loginPage.clickConfirmButton();
            dailyOperationsPage.setSearchText(permitNum);
            dailyOperationsPage.clickSearchButton();
            permitHistoryPage = new PermitHistoryPage(); 
            companyDetailsPage = new CompanyDetailPage();
            monthlyReportPage = new MonthlyReportPage();
            breadcrumbNavigation = new BreadcrumbNavigationComponent();
        });
    });

    it('Should be able to navigate to Permit history page', () => {
        let permitNumber = dailyOperationsPage.firstPermitNumberResultElement.getText().then(function(value){
            return value
        })
        let companyName =  dailyOperationsPage.firstCompanySearchResultElement.getText().then(function(value){
            return value.toUpperCase();
        })
        dailyOperationsPage.firstPermitNumberResultElement.click();
        permitHistoryPage.waitForPermitHistoryTitle();
        browser.waitForAngular().then(function () {
            expect(permitHistoryPage.permitHistoryTitleElement.isDisplayed()).toBe(true, 'Expect Permit History to be displayed');
        });
        expect(permitHistoryPage.permitNumberTitleElement.getText()).toEqual(permitNumber, 'Expect permit number should match');
        expect(permitHistoryPage.companyNameTitleElement.getText().then(function(value){
            return value.toUpperCase(); })).toEqual(companyName, 'Expect company name should match ' );
    });

    it('Validate permit information fields', () => {
        permitHistoryPage.permitNumberElement.getAttribute('value').then(function(value){
            expect(value.length>0).toBe(true, 'Expect Permit Number is auto populated');
        });
        permitHistoryPage.typeElement.getAttribute('value').then(function(value){
            expect(value.length>0).toBe(true, 'Expect Permit Type is auto populated');
        });
        permitHistoryPage.permitStatusActiveElement.getAttribute('value').then(function(value){
            expect(value.length>0).toBe(true, 'Expect Permit Status is auto populated');
        });
        permitHistoryPage.tufaActiveDateElement.getAttribute('value').then(function(value){
            expect(value.length>0).toBe(true, 'Expect TUFA Active Date is auto populated');
        });
        permitHistoryPage.ttbIssueDateElement.getAttribute('value').then(function(value){
            expect(value.length>0).toBe(true, 'Expect TTB Issue Date is auto populated');
        });
    });

    it('Validate Edit TTB permit fields', () => {
        permitHistoryPage.permitNumberElement.clear();
        permitHistoryPage.permitNumberElement.sendKeys(permitNum);
        permitHistoryPage.savePermitButtonElement.click();
        expect(companyDetailsPage.getCompanyDetailPageTitle()).toEqual('Company Details', 'Expect Permit Number to be editable');
        companyDetailsPage.clickonPermit(permitNum);
    });

    it('Validate Type field is uneditable', () => {
        permitHistoryPage.waitForPermitHistoryTitle();
        permitHistoryPage.typeElement.sendKeys('disabled');
        permitHistoryPage.typeElement.getAttribute('value').then(function(value){
            expect(value).toEqual('Importer', "Expect Type Field to be disabled");
        });
    });

    it('Validate Error Message when Permit Status is closed', () => {
        permitHistoryPage.permitStatusClosedElement.click();
        permitHistoryPage.savePermitButtonElement.click();
        expect(permitHistoryPage.closeDateErrorElement.count()).toEqual(2, 'Expect two error message when permit status is closed');
    });

    it('Validate if permit can be closed', () => {
        permitHistoryPage.permitClosedDatePickerElement.get(0).sendKeys(ScriptUtils.currentDate());
        permitHistoryPage.permitClosedDatePickerElement.get(1).sendKeys(ScriptUtils.currentDate());
        permitHistoryPage.savePermitButtonElement.click();
        companyDetailsPage.getCompanyDetailPageTitle();
        ScriptUtils.pause(2000);
        expect(companyDetailsPage.getPermitStatus(permitNum)).toEqual('Closed', 'Expect Permit to be closed');
        expect(companyDetailsPage.getTufaInactiveDate(permitNum)).toEqual(ScriptUtils.currentDate(), 'Expect Tufa Inactive Date to be populated');
        expect(companyDetailsPage.getTTBClosedDate(permitNum)).toEqual(ScriptUtils.currentDate(), 'Expect TTB Closed Date to be populated');
    });

    it('Validate Excluded Permit Status', () => {
        companyDetailsPage.excludePanelElement.click();
        companyDetailsPage.getSelectPermitNumberDropdown(permitNum).click();
        companyDetailsPage.assessmentFYElement.sendKeys('2016');
        companyDetailsPage.assessmentQuarterExcludedQ1.click();
        companyDetailsPage.waitForLoading();
        browser.sleep(2000);
        companyDetailsPage.commentsExcludeElement.sendKeys('Exclude comments');
        companyDetailsPage.saveExcludeElement.click();
        companyDetailsPage.waitForLoading();
        browser.sleep(2000);
        companyDetailsPage.clickonPermit(permitNum);
        permitHistoryPage.waitForPermitHistoryTitle();
        expect(permitHistoryPage.statusExcludedElement.isPresent()).toBe(true, 'Expect permit Status excluded');
    });

    it('Validate Add Comment popup is open', () => {
        permitHistoryPage.waitForPermitHistoryTitle();
        permitHistoryPage.addCommentElement.click();
        permitHistoryPage.waitForCommentPopUp();
        expect(permitHistoryPage.addCommentTitleElement.isPresent()).toBe(true, 'Expect Add Comment popup to open');
    });

    it('Validate all elements are Present in Add Comment popup', () => {
        expect(permitHistoryPage.addCommentTitleElement.isPresent()).toBe(true, 'Expect Add Comment popup has the Add Comment title');
        expect(permitHistoryPage.saveCommentElement.isPresent()).toBe(true, 'Expect Add Comment popup has the save button');
        expect(permitHistoryPage.cancelCommentElement.isPresent()).toBe(true, 'Expect Add Comment popup has the cancel button');
    });

    it('Validate warning pop up if cancel button clicked on Add Comments popup', () => {
        permitHistoryPage.commentDescriptionElement.click(); 
        permitHistoryPage.commentDescriptionElement.sendKeys('Test that comment is not saved');
        permitHistoryPage.cancelCommentElement.click();
        browser.sleep(2000);
        browser.switchTo().alert().accept().then(
            function () {
                expect(permitHistoryPage.addCommentElement.isPresent()).toBe(true, 'Expect alert present on cancel button');
                expect(permitHistoryPage.getCommentDescription('Test that comment is not saved').isPresent()).toBe(false, 'Expect the comment not saved on Cancel button');
            });
    });

    it('Validate required error message on Add Comments popup', () => {
        permitHistoryPage.addCommentElement.click();
        permitHistoryPage.waitForCommentPopUp();
        permitHistoryPage.saveCommentElement.click();
        expect(permitHistoryPage.requiredErrorCommentElement.isPresent()).toBe(true, 'Expect required error message on comment popup');
    });

    it('Validate Add Comments popup saves comment', () => {
        let commentText = 'Test comments';
        permitHistoryPage.commentDescriptionElement.click(); 
        browser.sleep(2000);
        permitHistoryPage.commentDescriptionElement.clear().then(() => {
            permitHistoryPage.commentDescriptionElement.sendKeys(commentText);
        });
        permitHistoryPage.saveCommentElement.click();
        permitHistoryPage.waitForPermitHistoryTitle();
        browser.sleep(2000);
        expect(permitHistoryPage.addCommentElement.isPresent()).toBe(true, 'Expect comment popup closed');
        expect(permitHistoryPage.getCommentDescription(commentText).isPresent()).toBe(true, 'Expect the comment to be saved');
        expect(permitHistoryPage.getCommentAuthor(commentText).isPresent()).toBe(true, 'Expect the author to be displayed');
        expect(permitHistoryPage.getCommentDateAdded(commentText).isPresent()).toBe(true, 'Expect the date added to be displayed');
        expect(permitHistoryPage.getCommentResolved().isPresent()).toBe(true, 'Expect the resolved checkbox to be displayed');
    });

    it('Validate edit comments popup', () => {
        let commentText = 'Test comments';
        permitHistoryPage.getEditComment(commentText).click();
        permitHistoryPage.waitForCommentPopUp();
        expect(permitHistoryPage.addCommentTitleElement.isPresent()).toBe(true, 'Expect Edit Comment popup to open');
        browser.sleep(1000);
        permitHistoryPage.cancelCommentElement.click();
        permitHistoryPage.waitForPermitHistoryTitle();
    });

    // it('Validate comment can be resolved and unresolved', () => {
    //     let commentText = 'Test comments';
    //     permitHistoryPage.getCommentResolved().click();
    //     permitHistoryPage.waitForLoading();
    //     expect(permitHistoryPage.getEditComment(commentText).isPresent()).toBe(false, 'Expect comment is resolved');
    //     permitHistoryPage.waitForLoading();
    //     permitHistoryPage.getCommentResolved().click();
    //     permitHistoryPage.waitForLoading();
    //     expect(permitHistoryPage.getEditComment(commentText).isPresent()).toBe(true, 'Expect comment is unresolved');
    // });

    it('Validate Multiple Add Comments', () => {
        let commentText = 'Add Test Comment';
        permitHistoryPage.addCommentElement.click();
        permitHistoryPage.waitForCommentPopUp();
        permitHistoryPage.commentDescriptionElement.click(); 
        browser.sleep(2000);
        permitHistoryPage.commentDescriptionElement.clear().then(() => {
            permitHistoryPage.commentDescriptionElement.sendKeys(commentText);
        });
        permitHistoryPage.saveCommentElement.click();
        permitHistoryPage.waitForPermitHistoryTitle();
        browser.sleep(2000);
        expect(permitHistoryPage.firstCommentElement.getText()).toEqual(commentText, 'Expect the newly added comment at the top');
    });

    it('Validate Period Of Activity Sorting', () => {
        expect(permitHistoryPage.periodOfActivityDescElement.isPresent()).toBe(true,"Expected MR are sorted by Period Of Activity in descending order");
    });

    it('Validate Period Of Activity is a hyperlink to Monthly Reports', () => {
        permitHistoryPage.firstPeriodOfActivityElement.click();
        monthlyReportPage.waitforPageMRtoLoad();
        expect(monthlyReportPage.pageTitleElement.isPresent()).toBe(true,"Expected Monthly Report page when clicked on Period of Activity");
        breadcrumbNavigation.clickPermitHistoryLink();
        permitHistoryPage.waitForPermitHistoryTitle();
    });

    it('Validate report status field can be sorted', () => {
        permitHistoryPage.reportStatusElement.click();
        expect(permitHistoryPage.reportStatusAscElement.isPresent()).toBe(true,"Expected monthly reports to be sorted by report status");
    });

    it('Validate action column should display delete button', () => {
        expect(permitHistoryPage.allMRDeleteElements.isPresent()).toBe(true,"Expected action column to display delete button");
    });

    it('Validate delete button actions', () => {
        permitHistoryPage.allMRDeleteElements.first().click();
        permitHistoryPage.waitForDeletePopUp();
        expect(permitHistoryPage.deletePopUpTitleElement.isPresent()).toBe(true,"Expected Delete Popup opened");
        browser.sleep(1000);
        permitHistoryPage.cancelDeletePopUpElement.click();
        permitHistoryPage.waitForPermitHistoryTitle();
        expect(permitHistoryPage.allMRDeleteElements.isPresent()).toBe(true,"Expected Delete popup canceled");

        let firstPABefore = permitHistoryPage.firstPeriodOfActivityElement.getText().then(function(value){return value;});
        permitHistoryPage.allMRDeleteElements.first().click();
        permitHistoryPage.waitForDeletePopUp();
        browser.sleep(1000);
        permitHistoryPage.confirmDeletePopUpElement.click();
        permitHistoryPage.waitForPermitHistoryTitle();
        browser.sleep(3000);
        let firstPAAfter = permitHistoryPage.firstPeriodOfActivityElement.getText().then(function(value){return value;});
        expect(firstPAAfter).not.toEqual(firstPABefore, "Expected the Period of Activity is deleted");
    });

    it('Validate navigation to Company Details Page', () => {
        breadcrumbNavigation.companyDetailsPermitHistoryBCElement.click();
        expect(companyDetailsPage.getCompanyDetailPageTitle()).toEqual('Company Details', "Expected Company Details Page");
        companyDetailsPage.clickonPermit(permitNum);
        permitHistoryPage.waitForPermitHistoryTitle();
        breadcrumbNavigation.dailyOperationsPermitHistoryBCElement.click();
        expect(dailyOperationsPage.pageTitleElementDaily.isPresent()).toBe(true, 'Expected report search result is displayed');
    });
});

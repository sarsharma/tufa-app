import { browser, protractor } from "protractor";
import { SideNavigtionComponent } from "../../component/navigation.co";
import { AssessmentsPage } from "../../page/assessments/assessments.po";
import { LoginPage } from "../../page/login.po";
import { StartPage } from "../../page/start.po";
import { QuarterlyAssessmentPage } from "../../page/assessments/quarterly-assessment.po";
import { MonthlyReportPage } from "../../page/monthly-report.po";
import { PermitHistoryPage } from "../../page/permit-history.po";
import { DailyOperationsPage } from "../../page/daily-operations.po";
import { TobaccoClassSelectionComponent } from "../../component/tobacco-class-selection.co";
import { Form3852Component } from "../../component/form-3852.co";
import { CreateMonthlyReportPage } from "../../page/create-monthly-report.po";
import * as fs from 'fs';
import * as path from 'path';
import { StaticValues } from "../../util/static-values";
import { ScriptUtils } from "../../util/script-utils";
import { TestValues } from "../../util/test-values";


describe('Quarterly Assessment Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentPage: AssessmentsPage;
    let quarterlyAssessmentPage: QuarterlyAssessmentPage;
    let monthlyReportPage: MonthlyReportPage;
    let permitHistoryPage: PermitHistoryPage;
    let tobaccoClassSelection: TobaccoClassSelectionComponent;
    let form3852Component: Form3852Component;
    let createMonthlyReportPage: CreateMonthlyReportPage;
    let downloadsDir = path.resolve(__dirname, '../../../data/downloaded-files/');
    let expectedFilesDir = path.resolve(__dirname, '../../../data/expected-files/');
    let uploadDir = path.resolve(__dirname, '../../../src/documents/');
    let uploadFileName = 'WI-TI-30001_5_16_19.pdf';
    let importerReportNumber = '345678756';
    let manufacturerReportNumber = '53-7843873';

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            loginPage.clickConfirmButton();
            sideNavigtionComponent = new SideNavigtionComponent();
            form3852Component = new Form3852Component();
            assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
        });
    });
    describe('QA Market Share Scripts', () => { 
        beforeAll(() => {
            tobaccoClassSelection = new TobaccoClassSelectionComponent();
            dailyOperationsPage = sideNavigtionComponent.clickDailyOperationsNavElement();
            //Ensure Importer monthly report is deleted before proceeding.
            dailyOperationsPage.setSearchText(importerReportNumber);
            dailyOperationsPage.clickSearchButton();
            browser.sleep(2000).then(function() {
                console.log('Slept 2 seconds');
            })
            dailyOperationsPage.isPeriodOfActivityExisting(TestValues.QUARTERLY_ASSESSMENT_MONTH_FY_YEAR).then(isExisting => {
                if(isExisting) {
                    permitHistoryPage = dailyOperationsPage.clickTtbPermitFirstLink();
                    permitHistoryPage.deletePeriodOfActivity(TestValues.QUARTERLY_ASSESSMENT_MONTH_FY_YEAR);
                    permitHistoryPage.waitForPermitHistoryTitle();
                }
            });

            dailyOperationsPage = sideNavigtionComponent.clickDailyOperationsNavElement();
            //Ensure Manufacturer monthly report is deleted before proceeding.
            dailyOperationsPage.setSearchText(manufacturerReportNumber);
            dailyOperationsPage.clickSearchButton();
            browser.sleep(2000).then(function() {
                console.log('Slept 2 seconds');
            })
            dailyOperationsPage.isPeriodOfActivityExisting(TestValues.QUARTERLY_ASSESSMENT_MONTH_FY_YEAR).then(isExisting => {
                if(isExisting) {
                    permitHistoryPage = dailyOperationsPage.clickTtbPermitFirstLink();
                    permitHistoryPage.deletePeriodOfActivity(TestValues.QUARTERLY_ASSESSMENT_MONTH_FY_YEAR);
                    permitHistoryPage.waitForPermitHistoryTitle();
                }
            });

            //create Importer Report in order to complete Quarterly assessment scenarios
            dailyOperationsPage = sideNavigtionComponent.clickDailyOperationsNavElement();
            createMonthlyReportPage = dailyOperationsPage.clickCreateMonthlyReportButton();
            createMonthlyReportPage.setFiscalYear(TestValues.QUARTERLY_ASSESSMENT_YEAR);
            createMonthlyReportPage.selectMonth(TestValues.QUARTERLY_ASSESSMENT_MONTH);
            createMonthlyReportPage.clickMonthlyReportButton();
            browser.wait(protractor.ExpectedConditions.visibilityOf(createMonthlyReportPage.successfulCreationMessageElement));
            dailyOperationsPage = sideNavigtionComponent.clickDailyOperationsNavElement();

            dailyOperationsPage.setSearchText(importerReportNumber);
            dailyOperationsPage.clickSearchButton();
            monthlyReportPage = dailyOperationsPage.clickPeriodOfActivity(TestValues.QUARTERLY_ASSESSMENT_MONTH_FY_YEAR);
            tobaccoClassSelection.getAllSelectedTobaccoClasses().count().then(count => {
                //If there is tobacco classes currently selected, attempt to clear the form in order to start fresh.
                if(count > 0) {
                    tobaccoClassSelection.resetSelection();
                    monthlyReportPage.clickSaveReport();
                    browser.waitForAngular();
                }
            });
            tobaccoClassSelection.clickSelectAllSelection();
            form3852Component.setCigarettesRemovalsAmount('10000');
            form3852Component.setSnuffRemovalsAmount('10000');
            form3852Component.setChewRemovalsAmount('10000');
            form3852Component.setPipeRemovalsAmount('10000');
            form3852Component.setRollYourOwnRemovalsAmount('10000');
            monthlyReportPage.clickSaveReport();
            browser.sleep(10000).then(function() {
                console.log('Slept 10 seconds');
            })
            //create Manufacturer Report in order to complete Quarterly assessment scenarios
            dailyOperationsPage = sideNavigtionComponent.clickDailyOperationsNavElement();
            dailyOperationsPage.setSearchText(manufacturerReportNumber);
            dailyOperationsPage.clickSearchButton();
            monthlyReportPage = dailyOperationsPage.clickPeriodOfActivity(TestValues.QUARTERLY_ASSESSMENT_MONTH_FY_YEAR);
            tobaccoClassSelection.getAllSelectedTobaccoClasses().count().then(count => {
                //If there is tobacco classes currently selected, attempt to clear the form in order to start fresh.
                if(count > 0) {
                    tobaccoClassSelection.resetSelection();
                    monthlyReportPage.clickSaveReport();
                    browser.waitForAngular();
                }
            });
            tobaccoClassSelection.clickSelectAllSelection();
            form3852Component.setCigarettesRemovalsAmount('5000');
            form3852Component.setSnuffRemovalsAmount('5000');
            form3852Component.setChewRemovalsAmount('5000');
            form3852Component.setPipeRemovalsAmount('5000');
            form3852Component.setRollYourOwnRemovalsAmount('5000');
            monthlyReportPage.clickSaveReport();
            browser.sleep(10000).then(function() {
                console.log('Slept 10 seconds');
            })
        });

        it('Should navigate to quarterly assessment "Report status" page, then verify sorting and pagination.', () => {
            assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
            //TODO:remove
            browser.sleep(5000);

            //TODO; find a way to guarantee year assessment is visible and clickable
            quarterlyAssessmentPage = assessmentPage.clickQuarterlyAssessmentTableLinkByYear(TestValues.QUARTERLY_ASSESSMENT_YEAR);
            browser.wait(protractor.ExpectedConditions.invisibilityOf(quarterlyAssessmentPage.loadingMessageElement));

            expect(quarterlyAssessmentPage.displayButton25ItemsElement.isPresent()).toBe(true, 'Expected default pagination to be 25');

            let element = quarterlyAssessmentPage.ttbPermitsTableElements;
            element.map(function(eachName){
                return eachName.getText().then(function(unSorted){
                    return unSorted;
                });
            }).then(function(unSorted){
                    var sorted = unSorted.slice();
                    sorted = sorted.sort((a: number, b: number) => a + b); 
                    expect(sorted).toEqual(unSorted, 'Expected default sorting to be by TTB Permit ascending order');
            });
         });

        it('Should click on Reconciliation Report and verify default sorting and filter.', () => {
            quarterlyAssessmentPage.clickReconciliationReportButton();
            expect(quarterlyAssessmentPage.notApprovedFilterElement.isDisplayed()).toEqual(true, 'Expected default filter to be set to "Unapproved"');
            let element = quarterlyAssessmentPage.reconReportTtbPermitTableElements;
            element.map(function(eachName){
                return eachName.getText().then(function(unSorted){
                    return unSorted;
                });
            }).then(function(unSorted){
                    var sorted = unSorted.slice();
                    sorted = sorted.sort((a: number, b: number) => a + b); 
                    expect(sorted).toEqual(unSorted, 'Expected default sorting to be by TTB Permit ascending order');
            });
        });

        it('Should go to monthly report and verify Assessment status of "Not Approved".', () => {
            monthlyReportPage = quarterlyAssessmentPage.clickReconReportTableReview(0);
            
            expect(monthlyReportPage.assessmentSectionHeaderElement.isDisplayed()).toEqual(true, 'Expected Assessment to be filed and assessment section to be displayed');
            expect(monthlyReportPage.assessmentUserTextElement.getText()).toContain(' ', 'Expected Assessment User to not be empty');
            expect(monthlyReportPage.assessmentTimestampTextElement.getText()).toEqual('Timestamp:', 'Expected Timestamp to be empty');
            expect(monthlyReportPage.assessmentStatusTextElement.getText()).toContain('Not Approved', 'Expected status to be "Not Approved"');
        });

        it('Should go to Generate and Submit, then verify "ACTION REQUIRED" warning message.', () => {
            browser.navigate().back();
            quarterlyAssessmentPage.clickGenerateAndSubmitButton();
            expect(quarterlyAssessmentPage.actionRequiredWarningElement.isDisplayed()).toEqual(true, 'Expected "ACTION REQUIRED" warning message to show');
        });
        
        it('Should click Generate Market Share button, then verify that report cannot be submitted until reports are approved.', () => {
            quarterlyAssessmentPage.clickGenerateMarketShareButton();
            
            expect(quarterlyAssessmentPage.submitButtonElement.isPresent()).toEqual(false, 'Expected Submit button to not be present');
            expect(quarterlyAssessmentPage.reportsNotApprovedWarningElement.isDisplayed()).toEqual(true, 'Expected warning message stating reports cannot be submitted');
        });

        it('Should approve all reports and verify status has changed to approved.', () => {
            quarterlyAssessmentPage.clickReconciliationReportButton();
            //click review on first monthly report
            monthlyReportPage = quarterlyAssessmentPage.clickReconReportTableReview(0);
            monthlyReportPage.clickAssessmentReconButton();
            //click review on second monthly report
            monthlyReportPage = quarterlyAssessmentPage.clickReconReportTableReview(0);
            monthlyReportPage.clickAssessmentReconButton();

            quarterlyAssessmentPage.selectApprovedFilter();
            monthlyReportPage = quarterlyAssessmentPage.clickReconReportTableReview(0);

            expect(monthlyReportPage.assessmentSectionHeaderElement.isDisplayed()).toEqual(true, 'Expected Assessment to be filed and assessment section to be displayed');
            expect(monthlyReportPage.assessmentUserTextElement.getText()).toContain(' ', 'Expected Assessment User to not be empty');
            expect(monthlyReportPage.assessmentTimestampTextElement.getText()).not.toEqual('Timestamp:', 'Expected to have a timestamp value');
            expect(monthlyReportPage.assessmentStatusTextElement.getText()).not.toContain('Not Approved', 'Expected status to be "Approved"');
        });

        it('Should generate market share, then verify "ACTION REQUIRED" warning message is no longer displayed, and submit button is available.', () => {
            browser.navigate().back();
            quarterlyAssessmentPage.clickGenerateAndSubmitButton();
            quarterlyAssessmentPage.clickGenerateMarketShareButton();

            expect(quarterlyAssessmentPage.actionRequiredWarningElement.isPresent()).toEqual(false, 'Expected "ACTION REQUIRED" warning message to be hidden');
            expect(quarterlyAssessmentPage.submitButtonElement.isDisplayed()).toEqual(true, 'Expected "Submit" button to be available');
            expect(quarterlyAssessmentPage.quarterlyAssessmentGeneratedTextElement.getText()).not.toEqual('Generated:', 'Expected "Generated" info text to contain value');
            expect(quarterlyAssessmentPage.quarterlyAssessmentAuthorTextElement.getText()).not.toEqual('Author:', 'Expected "Author" info text to contain value');
            expect(quarterlyAssessmentPage.cigarettesDownloadButtonElement.isDisplayed()).toBe(true);
            expect(quarterlyAssessmentPage.chewDownloadButtonElement.isDisplayed()).toBe(true);
            expect(quarterlyAssessmentPage.snuffDownloadButtonElement.isDisplayed()).toBe(true);
            expect(quarterlyAssessmentPage.pipeDownloadButtonElement.isDisplayed()).toBe(true);
            expect(quarterlyAssessmentPage.ryoDownloadButtonElement.isDisplayed()).toBe(true);

        });

        it('Should submit market share, and verify quarterly assessment history.', () => {
            quarterlyAssessmentPage.clickSubmitButton();

            expect(quarterlyAssessmentPage.quarterlyAssessmentSubmittedTextElement.isDisplayed()).toBe(true, 'Expected Submmitted timestamp text to show');
            expect(quarterlyAssessmentPage.quarterlyAssessmentAuthorTextElement.getText()).not.toEqual('Author:', 'Expected "Author" info text to contain value');
        });


        describe('Download Scenarios for Quarterly Assessment - Cigarettes', () => { 
            it('Should click Cigarettes Download as CSV button and verify file is downloaded and filename is correct.', () => {
                quarterlyAssessmentPage.cigarettesDownloadButtonElement.click();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(isDownloaded => {
                    expect(isDownloaded).toBe(true);
                    fs.readdirSync(downloadsDir).forEach(downloadedFile => { 
                        expect(downloadedFile).toContain('FY_' + TestValues.QUARTERLY_ASSESSMENT_YEAR);
                        expect(downloadedFile).toContain('Cigarettes');
                    });

                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

            it('Should verify Cigarette CSV file to contain correct expected values.', () => { 
                fs.readdirSync(downloadsDir).forEach(downloadedFile => {
                    try {
                        const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, downloadedFile), { encoding: 'utf8' });
                        const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.QUARTERLY_ASSESSMENT_EXPECTED_CIGARETTES_SCENARIO), { encoding: 'utf8' });
                        expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                    } catch (err) {
                        fail(`ERROR: Exception thrown: ${err.message}`);
                    }
                });
            });
    
            afterAll(async () => {
                require('fs-extra').emptyDirSync(downloadsDir);
                await browser.sleep(2000);
            });
        });

        describe('Download Scenarios for Quarterly Assessment - Chew', () => { 
            it('Should click Chew Download as CSV button and verify file is downloaded and filename is correct.', () => {
                quarterlyAssessmentPage.chewDownloadButtonElement.click();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(isDownloaded => {
                    expect(isDownloaded).toBe(true);
                    fs.readdirSync(downloadsDir).forEach(downloadedFile => { 
                        expect(downloadedFile).toContain('FY_' + TestValues.QUARTERLY_ASSESSMENT_YEAR);
                        expect(downloadedFile).toContain('Chew');
                    });

                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

            it('Should verify Chew CSV file to contain correct expected values.', () => { 
                fs.readdirSync(downloadsDir).forEach(downloadedFile => {
                    try {
                        const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, downloadedFile), { encoding: 'utf8' });
                        const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.QUARTERLY_ASSESSMENT_EXPECTED_CHEW_SCENARIO), { encoding: 'utf8' });
                        expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                    } catch (err) {
                        fail(`ERROR: Exception thrown: ${err.message}`);
                    }
                });
            });
    
            afterAll(async () => {
                require('fs-extra').emptyDirSync(downloadsDir);
                await browser.sleep(2000);
            });
        });

        describe('Download Scenarios for Quarterly Assessment - Snuff', () => { 
            it('Should click Snuff Download as CSV button and verify file is downloaded and filename is correct.', () => {
                quarterlyAssessmentPage.snuffDownloadButtonElement.click();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(isDownloaded => {
                    expect(isDownloaded).toBe(true);
                    fs.readdirSync(downloadsDir).forEach(downloadedFile => { 
                        expect(downloadedFile).toContain('FY_' + TestValues.QUARTERLY_ASSESSMENT_YEAR);
                        expect(downloadedFile).toContain('Snuff');
                    });

                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

            it('Should verify Snuff CSV file to contain correct expected values.', () => { 
                fs.readdirSync(downloadsDir).forEach(downloadedFile => {
                    try {
                        const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, downloadedFile), { encoding: 'utf8' });
                        const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.QUARTERLY_ASSESSMENT_EXPECTED_SNUFF_SCENARIO), { encoding: 'utf8' });
                        expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                    } catch (err) {
                        fail(`ERROR: Exception thrown: ${err.message}`);
                    }
                });
            });
    
            afterAll(async () => {
                require('fs-extra').emptyDirSync(downloadsDir);
                await browser.sleep(2000);
            });
        });

        describe('Download Scenarios for Quarterly Assessment - Pipe', () => { 
            it('Should click Pipe Download as CSV button and verify file is downloaded and filename is correct.', () => {
                quarterlyAssessmentPage.pipeDownloadButtonElement.click();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(isDownloaded => {
                    expect(isDownloaded).toBe(true);
                    fs.readdirSync(downloadsDir).forEach(downloadedFile => { 
                        expect(downloadedFile).toContain('FY_' + TestValues.QUARTERLY_ASSESSMENT_YEAR);
                        expect(downloadedFile).toContain('Pipe');
                    });

                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

            it('Should verify Pipe CSV file to contain correct expected values.', () => { 
                fs.readdirSync(downloadsDir).forEach(downloadedFile => {
                    try {
                        const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, downloadedFile), { encoding: 'utf8' });
                        const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.QUARTERLY_ASSESSMENT_EXPECTED_PIPE_SCENARIO), { encoding: 'utf8' });
                        expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                    } catch (err) {
                        fail(`ERROR: Exception thrown: ${err.message}`);
                    }
                });
            });
    
            afterAll(async () => {
                require('fs-extra').emptyDirSync(downloadsDir);
                await browser.sleep(2000);
            });
        });

        describe('Download Scenarios for Quarterly Assessment - Roll Your Own', () => { 
            it('Should click Roll Your Own Download as CSV button and verify file is downloaded and filename is correct.', () => {
                quarterlyAssessmentPage.ryoDownloadButtonElement.click();
                browser.wait(ScriptUtils.fileToBeDownloaded, 10000).then(isDownloaded => {
                    expect(isDownloaded).toBe(true);
                    fs.readdirSync(downloadsDir).forEach(downloadedFile => { 
                        expect(downloadedFile).toContain('FY_' + TestValues.QUARTERLY_ASSESSMENT_YEAR);
                        expect(downloadedFile).toContain('Roll-Your-Own');
                    });

                }).catch(function(ex) {
                    throw('An error has occurred with file download: ' + ex);
                });
            });

            it('Should verify Roll Your Own CSV file to contain correct expected values.', () => { 
                fs.readdirSync(downloadsDir).forEach(downloadedFile => {
                    try {
                        const actualFileContents: string = fs.readFileSync(path.resolve(downloadsDir, downloadedFile), { encoding: 'utf8' });
                        const expectedFileContents: string = fs.readFileSync(path.resolve(expectedFilesDir, StaticValues.QUARTERLY_ASSESSMENT_EXPECTED_ROLL_YOUR_OWN_SCENARIO), { encoding: 'utf8' });
                        expect(actualFileContents).toBe(expectedFileContents, 'Downloaded file contents differ from expected download file');
                    } catch (err) {
                        fail(`ERROR: Exception thrown: ${err.message}`);
                    }
                });
            });
    
            afterAll(async () => {
                require('fs-extra').emptyDirSync(downloadsDir);
                await browser.sleep(2000);
            });
        });
    });

    it('Should submit quarterly assessment, then verify for submitted status.', () => {
        quarterlyAssessmentPage.clickGenerateMarketShareButton();
        quarterlyAssessmentPage.clickSubmitButton();

        expect(quarterlyAssessmentPage.submittedStatusTextElement.getText()).toEqual('Submitted', 'Expected Quarterly Assessment status to be submitted');
    });

    it('Should verify upload document popup is displayed.', () => {
        
        expect(quarterlyAssessmentPage.submissionDocumentationHeaderElement.isDisplayed()).toEqual(true, 'Expected Submission Documentation section to be displayed');
        quarterlyAssessmentPage.clickUploadDocumentButton();
        expect(quarterlyAssessmentPage.uploadModalComponent.titleElement.isDisplayed()).toEqual(true, 'Expected Upload Document popup modal to be displayed');
    });

    it('Should verify upload document popup error messages are shown.', () => {
        quarterlyAssessmentPage.uploadModalComponent.uploadButtonElement.click();

        expect(quarterlyAssessmentPage.uploadModalComponent.documentRequiredErrorMessageElement.isDisplayed()).toBe(true, 'Expected "Document Required" error message to be displayed');
        expect(quarterlyAssessmentPage.uploadModalComponent.fileNameRequiredErrorMessageElement.isDisplayed()).toBe(true, 'Expected "File Name Required" error message to be displayed');
        expect(quarterlyAssessmentPage.uploadModalComponent.versionRequiredErrorMessageElement.isDisplayed()).toBe(true, 'Expected "Version Required" error message to be displayed');
    });

    it('Should click upload button and verify the document is uploaded successfully.', () => {
        quarterlyAssessmentPage.uploadModalComponent.setfileInputField(uploadDir, uploadFileName);
        quarterlyAssessmentPage.uploadModalComponent.selectVersion('1');
        quarterlyAssessmentPage.uploadModalComponent.uploadButtonElement.click();
        browser.wait(protractor.ExpectedConditions.invisibilityOf(quarterlyAssessmentPage.loadingMessageElement));

        expect(quarterlyAssessmentPage.uploadTableElement.isDisplayed()).toBe(true, 'Expected Document Upload table to be displayed');
    });

    it('Should click on submit button and verify latest assessment version was updated.', () => {
        quarterlyAssessmentPage.assessmentLatestVersionElement.getAttribute('value').then(oldValue => {
            quarterlyAssessmentPage.clickGenerateMarketShareButton();
            quarterlyAssessmentPage.clickSubmitButton();

            expect(quarterlyAssessmentPage.assessmentLatestVersionElement.getAttribute('value')).not.toBe(oldValue, 'Expected new assessment version to differ from old value');
        });
    });

    it('Should verify new Assessment status is submitted in Quarterly Assessments Home page.', () => {
        expect(quarterlyAssessmentPage.submittedStatusTextElement.getText()).toEqual('Submitted', 'Expected Quarterly Assessment status to be submitted');    
        assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
        assessmentPage.waitForLoadingAssessment();
        browser.sleep(3000);
        expect(assessmentPage.getQuarterlySubmissionStatusByYear(TestValues.QUARTERLY_ASSESSMENT_YEAR)).toEqual('Submitted', 'Expected to find submitted status in Assessments Home Page');    

    });
    
    it('Should verify quarterly assessments to be filtered by Q1.', () => {
        assessmentPage.q1FilterCheckboxElement.click();
        assessmentPage.monthsTableElements.getWebElements().then(elements => {
            elements.forEach(element => {
                expect(element.getText()).toEqual('October, November, December', 'Expected quarterly assessments to be filtered by Q1 quarter');
            });
        });
    });
    
    
});
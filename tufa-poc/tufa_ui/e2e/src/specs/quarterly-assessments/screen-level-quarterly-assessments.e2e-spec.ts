import { browser, protractor } from "protractor";
import { SideNavigtionComponent } from "../../component/navigation.co";
import { AssessmentsPage } from "../../page/assessments/assessments.po";
import { LoginPage } from "../../page/login.po";
import { StartPage } from "../../page/start.po";
import { CreateQuarterlyAssessmentPage } from "../../page/create-quarterly-assessment.po";
import { QuarterlyAssessmentPage } from "../../page/assessments/quarterly-assessment.po";
import { PermitHistoryPage } from "../../page/permit-history.po";
import { CompanyDetailPage } from "../../page/company-detail.po";

describe('Quarterly Assessment Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentPage: AssessmentsPage;
    let quarterlyAssessmentPage: QuarterlyAssessmentPage;
    let permitHistoryPage: PermitHistoryPage;
    let companyDetailPage: CompanyDetailPage;
    let createQuarterlyAssessmentPage : CreateQuarterlyAssessmentPage;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            loginPage.clickConfirmButton();
            sideNavigtionComponent = new SideNavigtionComponent();
            assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
        });
    });

    describe('Screen Level Scenarios', () => { 
        it('Should be able to Navigate to Quarterly Assessment page.', () => {
            browser.wait(protractor.ExpectedConditions.invisibilityOf(assessmentPage.loadingQAElement));
            expect(assessmentPage.pageTitleElement.isDisplayed()).toBe(true, 'Expected Assesment page title to be displayed');
        });

        it('Should be able to verify Quarterly assessment page layout.', () => {
            expect(assessmentPage.quarterlyAssessmentsTab.isDisplayed()).toBe(true, 'Expected Quarterly Assesment tab to be displayed');
            expect(assessmentPage.cigarAssessmentsTab.isDisplayed()).toBe(true, 'Expected Cigar Assesment tab to be displayed');
            expect(assessmentPage.annualTrueUpTab.isDisplayed()).toBe(true, 'Expected Annual True Up tab title to be displayed');
        });
        
        it('Should verify Quarterly assesment primary sorting of descending fiscal year.', () => {
            let element = assessmentPage.fiscalYearTableElements;
            element.map(function(eachName){
                return eachName.getText().then(function(unSorted){
                    return unSorted;
                });
            }).then(function(unSorted){
                    var sorted = unSorted.slice();
                    sorted = sorted.sort((a: number, b: number) => a + b); 
                    expect(sorted).toEqual(unSorted);

            });
        });

        it('Should verify Quarterly assesment secondary sorting of descending quarter.', () => {
            let element = assessmentPage.quarterTableFirstFourElements;
            element.map(async function(eachName){
                const unSorted = await eachName.getText();
                return unSorted;
            }).then(function(unSorted){
                    var sorted = unSorted.slice();
                    sorted = sorted.sort((a: number, b: number) => a + b); 
                    expect(sorted).toEqual(unSorted);
            });
        });

        it('Should filter by Submitted status and verify only submitted assesment records are displayed.', () => {
            assessmentPage.submittedStatusFilterCheckboxElement.click();

            expect(assessmentPage.notSubmittedElements.count()).toBe(0, 'Expected all Not Submitted assesments to be hidden');
        });

        it('Should filter by Not Submitted status and verify only not submitted assesment records are displayed.', () => {
            assessmentPage.submittedStatusFilterCheckboxElement.click();
            assessmentPage.notSubmittedStatusFilterCheckboxElement.click();

            expect(assessmentPage.submittedElements.count()).toBe(0, 'Expected all Submitted assesments to be hidden');
        });

        it('Should filter by year and verify only assesment records from that year are displayed.', () => {
            assessmentPage.secondlatestYearFilterCheckboxElement.getText().then(year => {
                assessmentPage.notSubmittedStatusFilterCheckboxElement.click();
                assessmentPage.secondlatestYearFilterCheckboxElement.click();
                assessmentPage.fiscalYearTableElements.getWebElements().then(elements => {
                    elements.forEach(element => {
                        expect(element.getText()).toEqual(year, 'Expected elements to be filtered by year ' + year);
                    });
                });
            });
        });

        it('Should navigate to Permit History page using breadcrumbs.', () => {
            quarterlyAssessmentPage = assessmentPage.clickAssesmentTableLink(0);
            quarterlyAssessmentPage.clickFirstMonthButton(0).clickBreadcrumbLink(3);
            permitHistoryPage = new PermitHistoryPage();

            expect(permitHistoryPage.permitHistoryTitleElement.isDisplayed()).toBe(true, 'Expected breadcrumb to arrive at Permit History page');
        });

        it('Should navigate to Company Details page using breadcrumbs.', () => {
            permitHistoryPage.clickBreadcrumbLink(2);
            companyDetailPage = new CompanyDetailPage();
            browser.wait(protractor.ExpectedConditions.invisibilityOf(companyDetailPage.loadingMessageElement));

            expect(companyDetailPage.companyDetailsPageTitleElement.isDisplayed()).toBe(true, 'Expected breadcrumb to arrive at Company Details page');
        });

        it('Should navigate to Quarterly Assessment page using breadcrumbs.', () => {
            companyDetailPage.clickBreadcrumbLink(1);
            quarterlyAssessmentPage = new QuarterlyAssessmentPage();
            browser.wait(protractor.ExpectedConditions.invisibilityOf(quarterlyAssessmentPage.loadingMessageElement));

            expect(quarterlyAssessmentPage.pageTitleElement.isDisplayed()).toBe(true, 'Expected breadcrumb to arrive at Quarterly Assessment page');
        });

        it('Should navigate to Assessment page using breadcrumbs.', () => {
            quarterlyAssessmentPage.clickBreadcrumbLink(0);
            assessmentPage = new AssessmentsPage();
            browser.wait(protractor.ExpectedConditions.invisibilityOf(assessmentPage.loadingQAElement));
            browser.wait(protractor.ExpectedConditions.invisibilityOf(assessmentPage.loadingCAElement));
            browser.wait(protractor.ExpectedConditions.invisibilityOf(assessmentPage.loadingATAElement));

            expect(assessmentPage.pageTitleElement.isDisplayed()).toBe(true, 'Expected breadcrumb to arrive at Assessment page');
        });

        it('Should navigate from Reconciliation Report review link to Permit History page using breadcrumbs.', () => {
            browser.sleep(3000);
            assessmentPage.secondlatestYearFilterCheckboxElement.click();
            quarterlyAssessmentPage = assessmentPage.clickAssesmentTableLink(0);
            quarterlyAssessmentPage.clickReconciliationReportButton();
            quarterlyAssessmentPage.clickReconReportTableReview(0).clickBreadcrumbLink(3);
            permitHistoryPage = new PermitHistoryPage();

            expect(permitHistoryPage.permitHistoryTitleElement.isDisplayed()).toBe(true, 'Expected breadcrumb to arrive at Permit History page');
        });

        it('Should navigate to Company Details page using breadcrumbs.', () => {
            permitHistoryPage.clickBreadcrumbLink(2);
            companyDetailPage = new CompanyDetailPage();

            expect(companyDetailPage.companyDetailsPageTitleElement.isDisplayed()).toBe(true, 'Expected breadcrumb to arrive at Company Details page');
        });

        it('Should navigate to Quarterly Assessment page using breadcrumbs.', () => {
            companyDetailPage.clickBreadcrumbLink(1);
            quarterlyAssessmentPage = new QuarterlyAssessmentPage();

            expect(quarterlyAssessmentPage.pageTitleElement.isDisplayed()).toBe(true, 'Expected breadcrumb to arrive at Quarterly Assessment page');
        });

        it('Should navigate to Assessment page using breadcrumbs.', () => {
            quarterlyAssessmentPage.clickBreadcrumbLink(0);
            assessmentPage = new AssessmentsPage();

            expect(assessmentPage.pageTitleElement.isDisplayed()).toBe(true, 'Expected breadcrumb to arrive at Assessment page');
        });
        
        it('Should display error messages when both values are not entered while creating Quarterly assessment.', () => {
           createQuarterlyAssessmentPage = assessmentPage.clickCreateQuarterlyAssessmentButton();
           createQuarterlyAssessmentPage.clickCreateButton();

           expect(createQuarterlyAssessmentPage.fiscalYearRequiredErrorElement.isDisplayed()).toBe(true, 'Expected Fiscal Year error message to be displayed');
           expect(createQuarterlyAssessmentPage.quarterRequiredErrorElement.isDisplayed()).toBe(true, 'Expected quarter error message to be displayed');
        });

        it('Should display error message when Quarter dropdown is not selected while creating Quarterly assessment.', () => {
            createQuarterlyAssessmentPage.fiscalYearFieldElement.sendKeys('2015');
            createQuarterlyAssessmentPage.clickCreateButton();
            
            expect(createQuarterlyAssessmentPage.quarterRequiredErrorElement.isDisplayed()).toBe(true, 'Expected Quarter error message to be displayed');
         });

         it('Should display error message when Fiscal Year is not entered while creating Quarterly assessment.', () => {
            createQuarterlyAssessmentPage.fiscalYearFieldElement.clear();
            createQuarterlyAssessmentPage.quarterDropdownElement.click();
            createQuarterlyAssessmentPage.q1OptionElement.click();
            createQuarterlyAssessmentPage.clickCreateButton();
            
            expect(createQuarterlyAssessmentPage.fiscalYearRequiredErrorElement.isDisplayed()).toBe(true, 'Expected Fiscal Year error message to be displayed');
         });

         //TODO: quarterly assesment creation scenario is not feasible, because once it's created it can no longer be created again.

        it('Should show assesment existing error message when trying to create already existing assesment.', () => {
            createQuarterlyAssessmentPage.fiscalYearFieldElement.sendKeys('2014');
            createQuarterlyAssessmentPage.clickCreateButton();
            
            expect(createQuarterlyAssessmentPage.assesmentAlreadyExistingErrorElement.isDisplayed()).toBe(true, 'Expected error message stating assesment already exists');
         });
    
    });

});
import { browser } from "../../../node_modules/protractor";
import { SideNavigtionComponent } from "../component/navigation.co";
import { AssessmentsPage } from "../page/assessments/assessments.po";
import { DailyOperationsPage } from "../page/daily-operations.po";
import { LoginPage } from "../page/login.po";
import { LogoutPage } from "../page/logout.po";
import { StartPage } from "../page/start.po";


describe('Side Navigation Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let sideNavigtionComponent: SideNavigtionComponent;
    let assessmentPage: AssessmentsPage;

    let logoutPage: LogoutPage;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            dailyOperationsPage = loginPage.clickConfirmButton();
            sideNavigtionComponent = new SideNavigtionComponent();
        });
    });

    it('Should show Super User navigation only', () => {
        expect(sideNavigtionComponent.dashboardNavElement.isDisplayed()).toBe(true, 'Expected Dashboard nav item to show');
        expect(sideNavigtionComponent.dailyOperationsNavElement.isDisplayed()).toBe(true, 'Expected Daily Operations nav item to show');
        expect(sideNavigtionComponent.assessmentsNavElement.isDisplayed()).toBe(true, 'Expected Assessments nav item to show');
        expect(sideNavigtionComponent.permitManagmentNavElement.isDisplayed()).toBe(true, 'Expected Permit Managment nav item to show');
        expect(sideNavigtionComponent.companyTrackingNavElement.isDisplayed()).toBe(true, 'Expected Company Traking nav item to show');
        expect(sideNavigtionComponent.ratesCodesNavElement.isDisplayed()).toBe(true, 'Expected Rates and Codes nav item to show');
        expect(sideNavigtionComponent.logoutNavElement.isDisplayed()).toBe(true, 'Expected Logout nav item to show');
    });

    // it('Should navigate to Dashboard', () => {
    //     sideNavigtionComponent.clickDashboardNavElement();
    // });

    it('Should navigate to Daily Operations', () => {
        sideNavigtionComponent.clickDailyOperationsNavElement();
    });

    it('Should navigate to Assessments', () => {
        assessmentPage = sideNavigtionComponent.clickAssessmentsNavElement();
        expect(assessmentPage.createQuarterlyAssessmentButton.isDisplayed()).toBe(true, 'Expect Create Quarterly Assessment button to be displayed');
    });

    // it('Should navigate to Permit Managment', () => {
    // sideNavigtionComponent.clickPermitManagmentNavElement();
    // });

    // it('Should navigate to Company Tracking', () => {
    // sideNavigtionComponent.clickCompanyTrackingNavElement();
    // });

    // it('Should navigate to Rates and Codes', () => {
    // sideNavigtionComponent.clickRatesCodesNavElement();
    // });

    it('Should navigate to Logout', () => {
        logoutPage = sideNavigtionComponent.clickLogoutNavElement();
        expect(logoutPage.clickHereLinkElement.isDisplayed()).toBe(true, 'Expected Logout page should be displayed');
    });
});
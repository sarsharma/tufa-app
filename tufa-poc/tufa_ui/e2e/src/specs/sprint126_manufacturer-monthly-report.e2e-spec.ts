import { browser, Ptor } from 'protractor';
import { Form3852Component } from '../component/form-3852.co';
import { Form7501Component } from '../component/form-7501.co';
import { TobaccoClassSelectionComponent } from '../component/tobacco-class-selection.co';
import { DailyOperationsPage } from '../page/daily-operations.po';
import { LoginPage } from '../page/login.po';
import { MonthlyReportPage } from '../page/monthly-report.po';
import { StartPage } from '../page/start.po';
import { Form5210Component } from '../component/form-5210.co';
import { Form5000Component } from '../component/form-5000.co';
import { TestValues } from "../util/test-values";
import { MarketShareReadyModalComponent } from '../component/assessments/market-share-ready-modal.co';
export declare let protractor: Ptor;

describe('Manufacturer Monthly Report Scenarios', () => {
    let startPage: StartPage;
    let loginPage: LoginPage;
    let dailyOperationsPage: DailyOperationsPage;
    let monthlyReportPage: MonthlyReportPage;
    let tobaccoClassSelection: TobaccoClassSelectionComponent;
    let form3852Component: Form3852Component;
    let form5210Component: Form5210Component;
    let form7501Component: Form7501Component;
    let form5000Component: Form5000Component;

    beforeAll(() => {
        browser.waitForAngularEnabled(false).then(function () {
            startPage = new StartPage().get(process.env.npm_config_url);
            loginPage = startPage.goToLoginPage();
            dailyOperationsPage = loginPage.clickConfirmButton();
            dailyOperationsPage.setSearchText(TestValues.MANUFACTURER_EIN);
            dailyOperationsPage.clickSearchButton();
            dailyOperationsPage.clickExtendDisplayButton();
            dailyOperationsPage.clickJanuaryMonthCheckbox();
            monthlyReportPage = dailyOperationsPage.clickPeriodOfActivity(TestValues.MANUFACTURER_REPORT);
            tobaccoClassSelection = new TobaccoClassSelectionComponent();
            form3852Component = new Form3852Component();
            form5210Component = new Form5210Component();
            form7501Component = new Form7501Component();
            form5000Component = new Form5000Component();
        });
    });

    it('Should be able to navigate to a Monthly Report.', () => {
        expect(monthlyReportPage.pageTitleElement.isDisplayed()).toBe(true, 'Expect Monthly Report title to be displayed');
    });
    
    it('Should be able to see all the Enable Adjustment checkbox for all Tobacco Classes', () => {
        tobaccoClassSelection.resetSelection();
        monthlyReportPage.clickSaveReport();
        tobaccoClassSelection.clickSelectAllSelection();
        expect(form5000Component.enableAllAdjustmentsElement.isDisplayed()).toBe(true, 'Expect Enable All Adjustments checkbox in Form 5000.24 to be displayed');
        expect(form5000Component.enableAdjustmentCigarettesElement.isDisplayed()).toBe(true, 'Expect Enable Adjustments for Cigarettes in Form 5000.24 to be displayed');
        expect(form5000Component.enableAdjustmentCigarsElement.isDisplayed()).toBe(true, 'Expect Enable Adjustments for Cigars in Form 5000.24 to be displayed');
        expect(form5000Component.enableAdjustmentChewSnuffElement.isDisplayed()).toBe(true, 'Expect Enable Adjustments for Chew and Snuff in Form 5000.24 to be displayed');
        expect(form5000Component.enableAdjustmentPipeRYOElement.isDisplayed()).toBe(true, 'Expect Enable Adjustments for Pipe and RYO in Form 5000.24 to be displayed');
    });


    it('Should be able enable Enable Adjustment checkbox for all Tobacco Classes', () => {
        form5000Component.enableAllAdjustmentsElement.click();
        expect(form5000Component.enableAllAdjustmentsElement.isEnabled()).toBe(true, 'Expect Enable All Adjustments checkbox in Form 5000.24 to be enabled');
        expect(form5000Component.enableAdjustmentCigarettesElement.isEnabled()).toBe(true, 'Expect Enable Adjustments for Cigarettes in Form 5000.24 to be enabled');
        expect(form5000Component.enableAdjustmentCigarsElement.isEnabled()).toBe(true, 'Expect Enable Adjustments for Cigars in Form 5000.24 to be enabled');
        expect(form5000Component.enableAdjustmentChewSnuffElement.isEnabled()).toBe(true, 'Expect Enable Adjustments for Chew and Snuff in Form 5000.24 to be enabled');
        expect(form5000Component.enableAdjustmentPipeRYOElement.isEnabled()).toBe(true, 'Expect Enable Adjustments for Pipe and RYO in Form 5000.24 to be enabled');
        form3852Component.setCigarettesRemovalsAmount('100000');
        form3852Component.setCigarsRemovalsAmount('1000');
        form3852Component.setCigarsExciseTax('700');
        form3852Component.setSnuffRemovalsAmount('1000');     
        form3852Component.setChewRemovalsAmount('10000');           
        form3852Component.setPipeRemovalsAmount('10000');
        form3852Component.setRollYourOwnRemovalsAmount('100');
    });

    it('Should be able to display Adjustment Panel and View button for all Tobacco Classes', () => {
        expect(form5000Component.totalSchAAdjElements.isPresent()).toBe(true, 'Expect Total Sch A Adj in Form 5000.24 to be displayed');
        expect(form5000Component.totalSchBAdjElements.isPresent()).toBe(true, 'Expect Total Sch B Adj in Form 5000.24 to be displayed');
        expect(form5000Component.adjustmentTotalElements.isPresent()).toBe(true, 'Expect Adjustments Total in Form 5000.24 to be displayed');
        expect(form5000Component.viewButtonElement.count()).toBe(4, 'Expect all 4 View buttons in Form 5000.24 to be displayed');
    });

    it('Should be able to display Tax Amount Breakdown Tax Return Id', () => {
        expect(form5000Component.taxReturnBreakdownID1Element.getText()).toContain(TestValues.MANUFACTURER_5000_TAXRETURNID1, 'Expect Tax Return Id 1 in Form 5000.24 to be displayed');
        expect(form5000Component.taxReturnBreakdownID2Element.getText()).toContain(TestValues.MANUFACTURER_5000_TAXRETURNID2, 'Expect Tax Return Id 2 in Form 5000.24 to be displayed');
    });

    it('Should be able to display all Schedule A/B Adjustments fields', () => {
        form5000Component.viewButtonElement.get(0).click();
        //Code to check the Titles
        expect(form5000Component.scheduleABAdjustmentsTitleElement.isPresent()).toBe(true, 'Expect Schedule A/B Adjustment Pop Up to be displayed');
        expect(form5000Component.scheduleABAdjustmentsTitleElement.isPresent()).toBe(true, 'Expect Schedule A/B Adjustment Title to be displayed');
        expect(form5000Component.scheduleAAdjustmentsElement.isPresent()).toBe(true, 'Expect Schedule A Adjustments Section to be displayed');
        expect(form5000Component.scheduleBAdjustmentsElement.isPresent()).toBe(true, 'Expect Schedule B Adjustments Section to be displayed');

        //Code to check Table Titles and Filters
        expect(form5000Component.lineNumTableElement.count()).toBe(2, 'Expect Schedule A/B Adjustment Line Number Title to be displayed');
        expect(form5000Component.taxReturnTableElement.count()).toBe(2, 'Expect Schedule A/B Adjustment Tax Return Id Title to be displayed');
        expect(form5000Component.amountTableElement.count()).toBe(2, 'Expect Schedule A/B Adjustment Amount Title to be displayed');
        expect(form5000Component.showDeleteTableElement.count()).toBe(2, 'Expect Schedule A/B Adjustment Show Delete Title to be displayed');
        expect(form5000Component.lineNumAFilterElement.isPresent()).toBe(true, 'Expect Schedule A Adjustment Line Number Filter to be displayed');
        expect(form5000Component.taxReturnAFilterElement.isPresent()).toBe(true, 'Expect Schedule A Adjustment Tax Return Id Filter to be displayed');
        expect(form5000Component.amountAFilterElement.isPresent()).toBe(true, 'Expect Schedule A Adjustment Amount Filter to be displayed');
        expect(form5000Component.showDeleteToggleAElement.isPresent()).toBe(true, 'Expect Schedule A Adjustment Show Delete Toggle to be displayed');
        expect(form5000Component.lineNumBFilterElement.isPresent()).toBe(true, 'Expect Schedule B Adjustment Line Number Filter to be displayed');
        expect(form5000Component.taxReturnBFilterElement.isPresent()).toBe(true, 'Expect Schedule B Adjustment Tax Return Id Filter to be displayed');
        expect(form5000Component.amountBFilterElement.isPresent()).toBe(true, 'Expect Schedule B Adjustment Amount Filter to be displayed');
        expect(form5000Component.showDeleteToggleBElement.isPresent()).toBe(true, 'Expect Schedule B Adjustment Show Delete Toggle to be displayed');
        
        //Code to check Input Fields for add Schedule A and B 
        form5000Component.showDeleteToggleAElement.click();
        form5000Component.showDeleteToggleBElement.click();
        
        expect(form5000Component.lineNumDataAElement.isPresent()).toBe(true, 'Expect Schedule A Adjustment Line Number Field to be displayed');
        expect(form5000Component.lineDescriptionDataAElement.isPresent()).toBe(true, 'Expect Schedule A Adjustment Line Description field to be displayed');
        expect(form5000Component.taxReturnDropdownAElement.isPresent()).toBe(true, 'Expect Schedule A Adjustment Tax Return Id dropdown to be displayed');
        expect(form5000Component.amountInputAElement.isPresent()).toBe(true, 'Expect Schedule A Adjustment amount field to be displayed');
        expect(form5000Component.addButtonAElement.isPresent()).toBe(true, 'Expect Schedule A Adjustment add button to be displayed');
        expect(form5000Component.deleteButtonAElement.isPresent()).toBe(true, 'Expect Schedule A Adjustment delete button to be displayed');
        expect(form5000Component.subTotalScheduleAElement.isPresent()).toBe(true, 'Expect SubTotal Schedule A Adjustment Amount to be displayed');

        expect(form5000Component.lineNumDataBElement.isPresent()).toBe(true, 'Expect Schedule B Adjustment Line Number Field to be displayed');
        expect(form5000Component.lineDescriptionDataBElement.isPresent()).toBe(true, 'Expect Schedule B Adjustment Line Description field to be displayed');
        expect(form5000Component.taxReturnDropdownBElement.isPresent()).toBe(true, 'Expect Schedule B Adjustment Tax Return Id dropdown to be displayed');
        expect(form5000Component.amountInputBElement.isPresent()).toBe(true, 'Expect Schedule B Adjustment amount field to be displayed');
        expect(form5000Component.addButtonBElement.isPresent()).toBe(true, 'Expect Schedule B Adjustment add button to be displayed');
        expect(form5000Component.deleteButtonBElement.isPresent()).toBe(true, 'Expect Schedule B Adjustment delete button to be displayed');
        expect(form5000Component.subTotalScheduleBElement.isPresent()).toBe(true, 'Expect SubTotal Schedule B Adjustment Amount to be displayed');

    });

    it('Should be able to add new row Schedule A/B Adjustments', () => {
        form5000Component.addButtonAElement.click();
        form5000Component.addButtonBElement.click();
        expect(form5000Component.lineNumDataAElement.getText()).toEqual('2', 'Expect Schedule A Adjustment new row to be added');
        expect(form5000Component.lineNumDataBElement.getText()).toEqual('2', 'Expect Schedule B Adjustment new row to be added');
    });

    it('Should be able to delete a row Schedule A/B Adjustments', () => {
        form5000Component.deleteButtonAElement.click();
        form5000Component.deleteButtonBElement.click();
        expect(form5000Component.lineNumDataAElement.getText()).toEqual('1', 'Expect Schedule A Adjustment row to be deleted');
        expect(form5000Component.lineNumDataBElement.getText()).toEqual('1', 'Expect Schedule B Adjustment row to be deleted');
    });

    it('Should be able to save Schedule A/B Adjustments for Cigar', () => {

        form5000Component.selectOptionAdjA('1',TestValues.MANUFACTURER_5000_TAXRETURNID1);
        form5000Component.amountInputAElement.clear().then(() => {form5000Component.amountInputAElement.sendKeys('1000');});
        form5000Component.lineDescriptionDataAElement.clear().then(()=>{form5000Component.lineDescriptionDataAElement.sendKeys('Schedule A Adjustment');});
        form5000Component.subTotalScheduleAElement.getText().then((value) => {expect(value).toContain('$1,000.00', 'Expect Sub Total Schedule A Adjustment to be 1000');});

        form5000Component.selectOptionAdjB('1',TestValues.MANUFACTURER_5000_TAXRETURNID2);
        form5000Component.amountInputBElement.clear().then(() => {form5000Component.amountInputBElement.sendKeys('300');});
        form5000Component.lineDescriptionDataBElement.clear().then(()=>{form5000Component.lineDescriptionDataBElement.sendKeys('Schedule B Adjustment');});
        form5000Component.subTotalScheduleBElement.getText().then((value) => {expect(value).toContain('$300.00', 'Expect Sub Total Schedule A Adjustment to be 300');});

        form5000Component.scheduleABAdjustmentTotalElement.getText().then((value) => {expect(value).toContain('$700.00', 'Expect Schedule A/B Adjustment Total to be 700');});
        form5000Component.saveAdjustmentsElement.click();
        form5000Component.waitForPleaseWait();
        form5000Component.getTotalAdjA('3').getText().then((value) => {expect(value).toContain('$1,000.00', 'Expect total Schedule A Adjustment to be 1000');});
        form5000Component.getTotalAdjB('3').getText().then((value) => {expect(value).toContain('$300.00', 'Expect total Schedule A Adjustment to be 300');});
        form5000Component.getAdjustmentTotal('3').getText().then((value) => {expect(value).toContain('$700.00', 'Expect total Schedule A Adjustment to be 700');});
        form5000Component.getGrandTotal('3').getText().then((value) => {expect(value).toContain('$700.00', 'Expect Grand Total for Cigar to be 700');});
        form5000Component.getDifference('3').getText().then((value) => {expect(value).toContain('$0.00', 'Expect Difference for Cigar to be 0');});
        //Valid Scenario
        form5000Component.getActivity('3').getText().then((value) => {expect(value).toContain('Valid', 'Expect Activity for Cigar to be Valid');});
        //Error Scenario
        form3852Component.setCigarsExciseTax('701');
        form5000Component.getActivity('3').getText().then((value) => {expect(value).toContain('Error', 'Expect Activity for Cigar to be Error');});  
    });

    it('Should be able to save Schedule A/B Adjustments for Cigarettes', () => {
        form5000Component.viewButtonElement.get(1).click();
        browser.sleep(2000);
        expect(form5000Component.scheduleABAdjustmentsTitleElement.isPresent()).toBe(true, 'Expect Schedule A/B Adjustment Pop Up to be displayed for Cigarettes');
        form5000Component.selectOptionAdjA('1',TestValues.MANUFACTURER_5000_TAXRETURNID1);
        form5000Component.amountInputAElement.clear().then(() => {form5000Component.amountInputAElement.sendKeys('5100');});
        form5000Component.lineDescriptionDataAElement.clear().then(()=>{form5000Component.lineDescriptionDataAElement.sendKeys('Schedule A Adjustment');});
        form5000Component.subTotalScheduleAElement.getText().then((value) => {expect(value).toContain('$5,100.00', 'Expect Sub Total Schedule A Adjustment to be 5100');});

        form5000Component.selectOptionAdjB('1',TestValues.MANUFACTURER_5000_TAXRETURNID2);
        form5000Component.amountInputBElement.clear().then(() => {form5000Component.amountInputBElement.sendKeys('67');});
        form5000Component.lineDescriptionDataBElement.clear().then(()=>{form5000Component.lineDescriptionDataBElement.sendKeys('Schedule B Adjustment');});
        form5000Component.subTotalScheduleBElement.getText().then((value) => {expect(value).toContain('$67.00', 'Expect Sub Total Schedule A Adjustment to be 67');});
        form5000Component.scheduleABAdjustmentTotalElement.getText().then((value) => {expect(value).toContain('$5,033.00', 'Expect Schedule A/B Adjustment Total to be 5033');});
        form5000Component.saveAdjustmentsElement.click();
        form5000Component.waitForPleaseWait();
        
        form5000Component.getTotalAdjA('4').getText().then((value) => {expect(value).toContain('$5,100.00', 'Expect total Schedule A Adjustment to be 5100');});
        form5000Component.getTotalAdjB('4').getText().then((value) => {expect(value).toContain('$67.00', 'Expect total Schedule A Adjustment to be 67');});
        form5000Component.getAdjustmentTotal('4').getText().then((value) => {expect(value).toContain('$5,033.00', 'Expect total Schedule A Adjustment to be 5033');});
        form5000Component.getGrandTotal('4').getText().then((value) => {expect(value).toContain('$5,033.00', 'Expect Grand Total for Cigarettes to be 5033');});
        form5000Component.getDifference('4').getText().then((value) => {expect(value).toContain('$0.00', 'Expect Difference for Cigarettes to be 0');});
        //Valid Scenario
        form5000Component.getActivity('4').getText().then((value) => {expect(value).toContain('Valid', 'Expect Activity for Cigarettes to be Valid');});
        //Error Scenario
        form3852Component.setCigarettesRemovalsAmount('200000');
        form5000Component.getActivity('4').getText().then((value) => {expect(value).toContain('Error', 'Expect Activity for Cigarettes to be Error');});  
    });

    it('Should be able to save Schedule A/B Adjustments for Chew and Snuff', () => {
        form5000Component.viewButtonElement.get(2).click();
        browser.sleep(2000);
        expect(form5000Component.scheduleABAdjustmentsTitleElement.isPresent()).toBe(true, 'Expect Schedule A/B Adjustment Pop Up to be displayed for Chew/Snuff');
        
        form5000Component.selectOptionAdjA('1',TestValues.MANUFACTURER_5000_TAXRETURNID1);
        form5000Component.amountInputAElement.clear().then(() => {form5000Component.amountInputAElement.sendKeys('6600');});
        form5000Component.lineDescriptionDataAElement.clear().then(()=>{form5000Component.lineDescriptionDataAElement.sendKeys('Schedule A Adjustment');});
        form5000Component.subTotalScheduleAElement.getText().then((value) => {expect(value).toContain('$6,600.00', 'Expect Sub Total Schedule A Adjustment to be 6600');});

        form5000Component.selectOptionAdjB('1',TestValues.MANUFACTURER_5000_TAXRETURNID2);
        form5000Component.amountInputBElement.clear().then(() => {form5000Component.amountInputBElement.sendKeys('57');});
        form5000Component.lineDescriptionDataBElement.clear().then(()=>{form5000Component.lineDescriptionDataBElement.sendKeys('Schedule B Adjustment');});
        form5000Component.subTotalScheduleBElement.getText().then((value) => {expect(value).toContain('$57.00', 'Expect Sub Total Schedule A Adjustment to be 57');});

        form5000Component.scheduleABAdjustmentTotalElement.getText().then((value) => {expect(value).toContain('$6,543.00', 'Expect Schedule A/B Adjustment Total to be 6543');});
        form5000Component.saveAdjustmentsElement.click();
        form5000Component.waitForPleaseWait();

        form5000Component.getTotalAdjA('5').getText().then((value) => {expect(value).toContain('$6,600.00', 'Expect total Schedule A Adjustment to be 6600');});
        form5000Component.getTotalAdjB('5').getText().then((value) => {expect(value).toContain('$57.00', 'Expect total Schedule A Adjustment to be 57');});
        form5000Component.getAdjustmentTotal('5').getText().then((value) => {expect(value).toContain('$6,543.00', 'Expect total Schedule A Adjustment to be 6543');});
        form5000Component.getGrandTotal('5').getText().then((value) => {expect(value).toContain('$6,543.00', 'Expect Grand Total for Chew/Snuff to be 6543');});
        form5000Component.getDifference('5').getText().then((value) => {expect(value).toContain('$0.00', 'Expect Difference for Chew/Snuff to be 0');});
        //Valid Scenario
        form5000Component.getActivity('5').getText().then((value) => {expect(value).toContain('Valid', 'Expect Activity for Chew/Snuff to be Valid');});
        //Error Scenario
        form3852Component.setSnuffRemovalsAmount('100001');
        form5000Component.getActivity('5').getText().then((value) => {expect(value).toContain('Error', 'Expect Activity for Chew/Snuff to be Error');});  
    });

    it('Should be able to save Schedule A/B Adjustments for Pipe and RYO', () => {
        
        form5000Component.viewButtonElement.get(3).click();
        browser.sleep(2000);
        expect(form5000Component.scheduleABAdjustmentsTitleElement.isPresent()).toBe(true, 'Expect Schedule A/B Adjustment Pop Up to be displayed for Pipe/RYO');
        
        form5000Component.selectOptionAdjA('1',TestValues.MANUFACTURER_5000_TAXRETURNID1);
        form5000Component.amountInputAElement.clear().then(() => {form5000Component.amountInputAElement.sendKeys('31000');});
        form5000Component.lineDescriptionDataAElement.clear().then(()=>{form5000Component.lineDescriptionDataAElement.sendKeys('Schedule A Adjustment');});
        form5000Component.subTotalScheduleAElement.getText().then((value) => {expect(value).toContain('$31,000.00', 'Expect Sub Total Schedule A Adjustment to be 31000');});

        form5000Component.selectOptionAdjB('1',TestValues.MANUFACTURER_5000_TAXRETURNID2);
        form5000Component.amountInputBElement.clear().then(() => {form5000Component.amountInputBElement.sendKeys('211');});
        form5000Component.lineDescriptionDataBElement.clear().then(()=>{form5000Component.lineDescriptionDataBElement.sendKeys('Schedule B Adjustment');});
        form5000Component.subTotalScheduleBElement.getText().then((value) => {expect(value).toContain('$211.00', 'Expect Sub Total Schedule A Adjustment to be 211');});

        form5000Component.scheduleABAdjustmentTotalElement.getText().then((value) => {expect(value).toContain('$30,789.00', 'Expect Schedule A/B Adjustment Total to be 30789');});
        form5000Component.saveAdjustmentsElement.click();
        form5000Component.waitForPleaseWait();

        form5000Component.getTotalAdjA('6').getText().then((value) => {expect(value).toContain('$31,000.00', 'Expect total Schedule A Adjustment to be 31000');});
        form5000Component.getTotalAdjB('6').getText().then((value) => {expect(value).toContain('$211.00', 'Expect total Schedule A Adjustment to be 211');});
        form5000Component.getAdjustmentTotal('6').getText().then((value) => {expect(value).toContain('$30,789.00', 'Expect total Schedule A Adjustment to be 30789');});
        form5000Component.getGrandTotal('6').getText().then((value) => {expect(value).toContain('$30,789.00', 'Expect Grand Total for Pipe/RYO to be 30789');});
        form5000Component.getDifference('6').getText().then((value) => {expect(value).toContain('$0.00', 'Expect Difference for Pipe/RYO to be 0');});
        //Valid Scenario
        form5000Component.getActivity('6').getText().then((value) => {expect(value).toContain('Valid', 'Expect Activity for Pipe/RYO to be Valid');});
        //Error Scenario
        form3852Component.setPipeRemovalsAmount('100001');
        form5000Component.getActivity('6').getText().then((value) => {expect(value).toContain('Error', 'Expect Activity for Pipe/RYO to be Error');});  
    });   

    it('Should be able to see all the 5210.5 Enable Adjustment checkbox for all Tobacco Classes', () => {

        form3852Component.setCigarettesRemovalsAmount('100000');
        form3852Component.setCigarsRemovalsAmount('1000');
        form3852Component.setCigarsExciseTax('700');
        form3852Component.setSnuffRemovalsAmount('1000');     
        form3852Component.setChewRemovalsAmount('10000');           
        form3852Component.setPipeRemovalsAmount('10000');
        form3852Component.setRollYourOwnRemovalsAmount('100');
        expect(form5000Component.enableAllAdjustments5210Element.isDisplayed()).toBe(true, 'Expect Enable All Adjustments checkbox in Form 5210.5 to be displayed');
        expect(form5000Component.cigarsEnableAdjustments5210Element.isDisplayed()).toBe(true, 'Expect Enable Adjustments for Cigarettes in Form 5210.5 to be displayed');
        expect(form5000Component.cigarettesEnableAdjustments5210Element.isDisplayed()).toBe(true, 'Expect Enable Adjustments for Cigars in Form 5210.5 to be displayed');
        expect(form5000Component.chewEnableAdjustments5210Element.isDisplayed()).toBe(true, 'Expect Enable Adjustments for Chew in Form 5210.5 to be displayed');
        expect(form5000Component.snuffEnableAdjustments5210Element.isDisplayed()).toBe(true, 'Expect Enable Adjustments for Snuff in Form 5210.5 to be displayed');
        expect(form5000Component.pipeEnableAdjustments5210Element.isDisplayed()).toBe(true, 'Expect Enable Adjustments for Pipe in Form 5210.5 to be displayed');
        expect(form5000Component.ryoEnableAdjustments5210Element.isDisplayed()).toBe(true, 'Expect Enable Adjustments for RYO in Form 5210.5 to be displayed');
   
    });
    
    
    it('Should be able to see Total Sticks for Cigar and Cigarattes in Form 5210.5', () => {

        expect(form5000Component.cigarTotalSticks5210Element.isPresent()).toBe(true, 'Expect Total sticks for Cigars Form 5210.5 to be displayed');
        expect(form5000Component.cigarettesTotalSticks5210Element.isPresent()).toBe(true, 'Expect Total sticks for Cigarettes in Form 5210.5 to be displayed');
   
    });

    it('Should be able enable Form 5210.5 Enable Adjustment checkbox for all Tobacco Classes', () => {
        
        form5000Component.enableAllAdjustments5210Element.click();        
        expect(form5000Component.enableAllAdjustments5210Element.isEnabled()).toBe(true, 'Expect Enable All Adjustments checkbox in Form 5210.5 to be enabled');
        expect(form5000Component.cigarsEnableAdjustments5210Element.isEnabled()).toBe(true, 'Expect Enable Adjustments for Cigarettes in Form 5210.5 to be enabled');
        expect(form5000Component.cigarettesEnableAdjustments5210Element.isEnabled()).toBe(true, 'Expect Enable Adjustments for Cigars in Form 5210.5 to be enabled');
        expect(form5000Component.chewEnableAdjustments5210Element.isEnabled()).toBe(true, 'Expect Enable Adjustments for Chew in Form 5210.5 to be enabled');
        expect(form5000Component.snuffEnableAdjustments5210Element2.isEnabled()).toBe(true, 'Expect Enable Adjustments for Snuff in Form 5210.5 to be enabled');
        expect(form5000Component.pipeEnableAdjustments5210Element.isEnabled()).toBe(true, 'Expect Enable Adjustments for Pipe in Form 5210.5 to be enabled');
        expect(form5000Component.ryoEnableAdjustments5210Element.isEnabled()).toBe(true, 'Expect Enable Adjustments for RYO in Form 5210.5 to be enabled');

    });

    it('Should not be able to see Total Sticks for Cigar and Cigarattes in Form 5210.5', () => {

        expect(form5000Component.cigarTotalSticks5210Element.isPresent()).toBe(false, 'Expect Total sticks for Cigars Form 5210.5 not to be displayed');
        expect(form5000Component.cigarettesTotalSticks5210Element.isPresent()).toBe(false, 'Expect Total sticks for Cigarettes in Form 5210.5 not to be displayed');
   
    });

    it('Should be able see Line Description and Grand Total in Form 5210.5 for all Tobacco Classes', () => {
        
        expect(form5000Component.cigarLineAdjustment5210Element.isEnabled()).toBe(true, 'Expect Enable Adjustments for Cigarettes in Form 5210.5 to be enabled');
        expect(form5000Component.cigarettesLineAdjustment5210Element.isEnabled()).toBe(true, 'Expect Enable Adjustments for Cigars in Form 5210.5 to be enabled');
        expect(form5000Component.chewLineAdjustment5210Element.isEnabled()).toBe(true, 'Expect Enable Adjustments for Chew in Form 5210.5 to be enabled');
        expect(form5000Component.snuffLineAdjustment5210Element.isEnabled()).toBe(true, 'Expect Enable Adjustments for Snuff in Form 5210.5 to be enabled');
        expect(form5000Component.pipeLineAdjustment5210Element.isEnabled()).toBe(true, 'Expect Enable Adjustments for Pipe in Form 5210.5 to be enabled');
        expect(form5000Component.ryoLineAdjustment5210Element.isEnabled()).toBe(true, 'Expect Enable Adjustments for RYO in Form 5210.5 to be enabled');

    });

    it('Should be able add Line Adjustment for Valid Activity for Cigar', () => {

        form5000Component.getGrandTotable('Cigars', '2').getText().then((value) => {expect(value).toContain('1,000', 'Expect Grand Total for Cigars to be 1,000');});        
        form5000Component.getGrandTotable('Cigars', '3').getText().then((value) => {expect(value).toContain('0', 'Expect Difference Removal for Cigars to be 0');});
        form5000Component.getGrandTotable('Cigars', '4').getText().then((value) => {expect(value).toContain('Valid', 'Expect Activity for Cigars to be Valid');});

    });

    it('Should be able add Line Adjustment for Error Activity for Cigar', () => {

        form5000Component.cigarLineAdjustment5210Element.clear().then(() => {form5000Component.cigarLineAdjustment5210Element.sendKeys('10000');});
        form5000Component.getGrandTotable('Cigars', '2').getText().then((value) => {expect(value).toContain('11,000', 'Expect Grand Total for Cigars to be 11,000');});        
        form5000Component.getGrandTotable('Cigars', '3').getText().then((value) => {expect(value).toContain('(10,000)', 'Expect Difference Removal for Cigars to be (10,000)');});
        form5000Component.getGrandTotable('Cigars', '4').getText().then((value) => {expect(value).toContain('Error', 'Expect Activity for Cigars to be Error');});
    });

    it('Should be able add Line Adjustment for Valid Activity for Cigarettes', () => {

        form5000Component.getGrandTotable('Cigarettes', '2').getText().then((value) => {expect(value).toContain('100,000', 'Expect Grand Total for Cigarettes to be 100,000');});        
        form5000Component.getGrandTotable('Cigarettes', '3').getText().then((value) => {expect(value).toContain('0', 'Expect Difference Removal for Cigarettes to be 0');});
        form5000Component.getGrandTotable('Cigarettes', '4').getText().then((value) => {expect(value).toContain('Valid', 'Expect Activity for Cigarettes to be Valid');});

    });

    it('Should be able add Line Adjustment for Error Activity for Cigarettes', () => {

        form5000Component.cigarettesLineAdjustment5210Element.clear().then(() => {form5000Component.cigarettesLineAdjustment5210Element.sendKeys('1000');});
        form5000Component.getGrandTotable('Cigarettes', '2').getText().then((value) => {expect(value).toContain('101,000', 'Expect Grand Total for Cigarettes to be 101,000');});        
        form5000Component.getGrandTotable('Cigarettes', '3').getText().then((value) => {expect(value).toContain('(1,000)', 'Expect Difference Removal for Cigarettes to be 1000');});
        form5000Component.getGrandTotable('Cigarettes', '4').getText().then((value) => {expect(value).toContain('Error', 'Expect Activity for Cigarettes to be Error');});
    });

    it('Should be able add Line Adjustment for Valid Activity for Chew', () => {

        form5000Component.getGrandTotable('Chew', '2').getText().then((value) => {expect(value).toContain('10,000.000', 'Expect Grand Total for Chew to be 10,000.000');});        
        form5000Component.getGrandTotable('Chew', '3').getText().then((value) => {expect(value).toContain('0.000', 'Expect Difference Removal for Chew to be 0.000');});
        form5000Component.getGrandTotable('Chew', '4').getText().then((value) => {expect(value).toContain('Valid', 'Expect Activity for Chew to be Valid');});
    });

    it('Should be able add Line Adjustment for Error Activity for Chew', () => {

        form5000Component.chewLineAdjustment5210Element.clear().then(() => {form5000Component.chewLineAdjustment5210Element.sendKeys('100001');});
        form5000Component.getGrandTotable('Chew', '2').getText().then((value) => {expect(value).toContain('110,001.000', 'Expect Grand Total for Chew to be 110,001.000');});        
        form5000Component.getGrandTotable('Chew', '3').getText().then((value) => {expect(value).toContain('(100,001.000)', 'Expect Difference Removal for Chew to be 100,001.000');});
        form5000Component.getGrandTotable('Chew', '4').getText().then((value) => {expect(value).toContain('Error', 'Expect Activity for Chew to be Error');});
    });

    it('Should be able add Line Adjustment for Valid Activity for Snuff', () => {

        form5000Component.getGrandTotable('Snuff', '2').getText().then((value) => {expect(value).toContain('1,000.000', 'Expect Grand Total for Snuff to be 1,000.000');});        
        form5000Component.getGrandTotable('Snuff', '3').getText().then((value) => {expect(value).toContain('0.000', 'Expect Difference Removal for Snuff to be 0.000');});
        form5000Component.getGrandTotable('Snuff', '4').getText().then((value) => {expect(value).toContain('Valid', 'Expect Activity for Snuff to be Valid');});

    });

    it('Should be able add Line Adjustment for Error Activity for Snuff', () => {

        form5000Component.snuffLineAdjustment5210Element.clear().then(() => {form5000Component.snuffLineAdjustment5210Element.sendKeys('100');});
        form5000Component.getGrandTotable('Snuff', '2').getText().then((value) => {expect(value).toContain('1,100.000', 'Expect Grand Total for Snuff to be 1,100.000');});        
        form5000Component.getGrandTotable('Snuff', '3').getText().then((value) => {expect(value).toContain('(100.000)', 'Expect Difference Removal for Snuff to be 100.000');});
        form5000Component.getGrandTotable('Snuff', '4').getText().then((value) => {expect(value).toContain('Error', 'Expect Activity for Snuff to be Error');});
    });

    it('Should be able add Line Adjustment for Valid Activity for Pipe', () => {

        form5000Component.getGrandTotable('Pipe', '2').getText().then((value) => {expect(value).toContain('10,000.000', 'Expect Grand Total for Pipe to be 10,000.000');});        
        form5000Component.getGrandTotable('Pipe', '3').getText().then((value) => {expect(value).toContain('0.000', 'Expect Difference Removal for Pipe to be 0.000');});
        form5000Component.getGrandTotable('Pipe', '4').getText().then((value) => {expect(value).toContain('Valid', 'Expect Activity for Pipe to be Valid');});

    });

    it('Should be able add Line Adjustment for Error Activity for Pipe', () => {

        form5000Component.pipeLineAdjustment5210Element.clear().then(() => {form5000Component.pipeLineAdjustment5210Element.sendKeys('100');});
        form5000Component.getGrandTotable('Pipe', '2').getText().then((value) => {expect(value).toContain('10,100.000', 'Expect Grand Total for Pipe to be 10,100.000');});        
        form5000Component.getGrandTotable('Pipe', '3').getText().then((value) => {expect(value).toContain('(100.000)', 'Expect Difference Removal for Pipe to be 100.000');});
        form5000Component.getGrandTotable('Pipe', '4').getText().then((value) => {expect(value).toContain('Error', 'Expect Activity for Pipe to be Error');});
    });

    it('Should be able add Line Adjustment for Valid Activity for Roll Your Own', () => {

        form5000Component.getGrandTotable('Roll Your Own', '2').getText().then((value) => {expect(value).toContain('100.000', 'Expect Grand Total for Roll Your Own to be 100.000');});        
        form5000Component.getGrandTotable('Roll Your Own', '3').getText().then((value) => {expect(value).toContain('0.000', 'Expect Difference Removal for Roll Your Own to be 0.000');});
        form5000Component.getGrandTotable('Roll Your Own', '4').getText().then((value) => {expect(value).toContain('Valid', 'Expect Activity for Roll Your Own to be Valid');});

    });

    it('Should be able add Line Adjustment for Error Activity for Roll Your Own', () => {

        form5000Component.ryoLineAdjustment5210Element.clear().then(() => {form5000Component.ryoLineAdjustment5210Element.sendKeys('10');});
        form5000Component.getGrandTotable('Roll Your Own', '2').getText().then((value) => {expect(value).toContain('110.000', 'Expect Grand Total for Roll Your Own to be 110.000');});        
        form5000Component.getGrandTotable('Roll Your Own', '3').getText().then((value) => {expect(value).toContain('(10.000)', 'Expect Difference Removal for Roll Your Own to be 10.000');});
        form5000Component.getGrandTotable('Roll Your Own', '4').getText().then((value) => {expect(value).toContain('Error', 'Expect Activity for Roll Your Own to be Error');});
    });
    
    it('Save the report', () => {
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            expect(monthlyReportPage.getReportStatusElement('Error')).toEqual('Error', 'Save Report Completed');
        });
    });
});
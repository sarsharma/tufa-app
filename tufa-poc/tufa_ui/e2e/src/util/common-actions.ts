import { browser } from 'protractor';
import { SideNavigtionComponent } from "../component/navigation.co";
import { TobaccoClassSelectionComponent } from "../component/tobacco-class-selection.co";
import { MonthlyReportPage } from '../page/monthly-report.po';
import { StartPage } from "../page/start.po";

/**
 * Used for common actions that are not needed to be tested
 */
export class CommonActions {

    static login() {
        let startPage = new StartPage().get(process.env.npm_config_url);
        let loginPage = startPage.goToLoginPage();
        return loginPage.clickConfirmButton();
    }

    static navigateToMonthlyReport(ein: string, periodOfActivity: string) {
        let navigation: SideNavigtionComponent = new SideNavigtionComponent();
        let dailyOperationsPage = navigation.clickDailyOperationsNavElement();
        dailyOperationsPage.setSearchText(ein);
        dailyOperationsPage.clickSearchButton();
        dailyOperationsPage.clickExtendDisplayButton();
        return dailyOperationsPage.clickPeriodOfActivity(periodOfActivity);
    }

    static resetMonthlyReport(monthlyReportPage: MonthlyReportPage) {
        new TobaccoClassSelectionComponent().resetSelection();
        monthlyReportPage.clickSaveReport();
        browser.waitForAngular().then(function () {
            expect(monthlyReportPage.getReportStatusElement('Not Started')).toEqual('Not Started', 'Reset Report Completed');
        });
    }
}
import * as fs from 'fs';
import * as path from 'path';
import { browser } from 'protractor';

export class ScriptUtils {
  static generateRandomEmail() {
    return this.generateRandomString(20) + "@mail.com";
  }

  static generateRandomString(stringLength : number) {
    let strValues = "abcdefghijk123456789";
    let randomString = "";
    for (let i = 0; i < stringLength; i++) {
      randomString = randomString + strValues.charAt(Math.round(strValues.length * Math.random()));
    }
    return randomString;
  }

  static generateUniqueEin() {
    var timestamp = new Date().getTime().toString();
    return timestamp.substring(0, 9);
    // return ('9' + timestamp.substring(1, 9));
  }

  static writeScreenShot(data: string, filename: string) {
    let datetime = new Date().getTime().toString();
    filename = `./${filename}.${datetime}.png`;

    let stream = fs.createWriteStream(filename);
    stream.write(new Buffer(data, 'Base64'));
    stream.end();
  }

  static pause(time: number) {
    browser.sleep(time).then(function () {
      console.log(`Slept ${time / 1000} seconds`);
    });
  }

  /**
   * Custom wait condition which waits for file download before proceeding.
   */
  static fileToBeDownloaded = async function() {
    let folderfiles = fs.readdirSync(path.resolve(__dirname, '../../data/downloaded-files/'));
    return folderfiles.some(file => file.endsWith(".csv"));
  };

  static currentDate(){ //format : mm/dd/yyyy
    var todayDate = new Date();
        var dd = todayDate.getDate();
        var mm = todayDate.getMonth()+1;
        var yyyy = todayDate.getFullYear();
        var ddText, mmText
        if(dd<10)  ddText='0'+dd.toString(); else ddText = dd.toString();
        if(mm<10)  mmText='0'+mm.toString(); else mmText = mm.toString();
        return mmText+'/'+ddText+'/'+yyyy;
  }
}



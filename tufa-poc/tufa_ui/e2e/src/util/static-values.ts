/** Use this class for any constant values through test cases */
export class StaticValues {
    static readonly CIGARETTES = "Cigarettes";
    static readonly CIGARS = "Cigars";
    static readonly PIPE = "Pipe";
    static readonly ROLL_YOUR_OWN = "Roll Your Own";
    static readonly CHEW = "Chew";
    static readonly SNUFF = "Snuff";
    static readonly PIPE_ROLL_YOUR_OWN = "Pipe/Roll-Your-Own";
    static readonly CHEW_SNUFF = "Chew/Snuff"

    static readonly IMPORTER_LONG = "Importer";
    static readonly IMPORTER_SHORT = "IMPT";
    static readonly MANUFACTURE_LONG = "Manufacture";
    static readonly MANUFACTURE_SHORT = "MANU";

    static readonly FISCAL_YEAR = "2008";

    //Download Scenarios - Expected Files
    static readonly MANUFACTURER_EXPECTED_FORM_3852_FILENAME = 'manufacturer-expected-form-3852.csv';
    static readonly MANUFACTURER_EXPECTED_FORM_5210_FILENAME = 'manufacturer-expected-form-5210.csv';
    static readonly MANUFACTURER_EXPECTED_FORM_5000_FILENAME = 'manufacturer-expected-form-5000.csv';
    static readonly IMPORTER_EXPECTED_FORM_3852_FILENAME = 'importer-expected-form-3852.csv';
    static readonly IMPORTER_EXPECTED_FORM_5220_FILENAME = 'importer-expected-form-5220.csv';
    static readonly IMPORTER_EXPECTED_FORM_7501_CIGARETTES_FILENAME = 'importer-expected-form-7501-cigarettes.csv';
    static readonly IMPORTER_EXPECTED_FORM_7501_CIGARS_FILENAME = 'importer-expected-form-7501-cigars.csv';
    static readonly IMPORTER_EXPECTED_FORM_7501_SNUFF_FILENAME = 'importer-expected-form-7501-snuff.csv';
    static readonly IMPORTER_EXPECTED_FORM_7501_CHEW_FILENAME = 'importer-expected-form-7501-chew.csv';
    static readonly IMPORTER_EXPECTED_FORM_7501_PIPE_FILENAME = 'importer-expected-form-7501-pipe.csv';
    static readonly IMPORTER_EXPECTED_FORM_7501_ROLL_YOUR_OWN_FILENAME = 'importer-expected-form-7501-rollyourown.csv';
    static readonly QUARTERLY_ASSESSMENT_EXPECTED_CIGARETTES_SCENARIO = 'quarterly-assessment-cigarettes.csv';
    static readonly QUARTERLY_ASSESSMENT_EXPECTED_CHEW_SCENARIO = 'quarterly-assessment-chew.csv';
    static readonly QUARTERLY_ASSESSMENT_EXPECTED_SNUFF_SCENARIO = 'quarterly-assessment-snuff.csv';
    static readonly QUARTERLY_ASSESSMENT_EXPECTED_PIPE_SCENARIO = 'quarterly-assessment-pipe.csv';
    static readonly QUARTERLY_ASSESSMENT_EXPECTED_ROLL_YOUR_OWN_SCENARIO = 'quarterly-assessment-rollyourown.csv';
    static readonly CIGAR_ASSESSMENT_EXPECTED_Q1_FILENAME = 'cigar-assessment-q1.csv';
    static readonly CIGAR_ASSESSMENT_EXPECTED_Q2_FILENAME = 'cigar-assessment-q2.csv';
    static readonly CIGAR_ASSESSMENT_EXPECTED_Q3_FILENAME = 'cigar-assessment-q3.csv';
    static readonly CIGAR_ASSESSMENT_EXPECTED_Q4_FILENAME = 'cigar-assessment-q4.csv';

    //Download Scenarios - Downloaded filenames
    static readonly MANUFACTURER_DOWNLOADED_FORM_3852_FILENAME = 'TP-FL-2323_January_FY_2016_ERROR_3852.csv';
    static readonly MANUFACTURER_DOWNLOADED_FORM_5210_FILENAME = 'TP-FL-2323_January_FY_2016_ERROR_5210-5.csv';
    static readonly MANUFACTURER_DOWNLOADED_FORM_5000_FILENAME = 'TP-FL-2323_January_FY_2016_ERROR_5000.csv';
    static readonly IMPORTER_DOWNLOADED_FORM_3852_FILENAME = 'FL-TI-2323_April_FY_2016_ERROR_3852.csv';
    static readonly IMPORTER_DOWNLOADED_FORM_5220_FILENAME = 'FL-TI-2323_April_FY_2016_ERROR_5220-6.csv';
    static readonly IMPORTER_DOWNLOADED_FORM_7501_CIGARETTES_FILENAME = 'FL-TI-2323_April_FY_2016_ERROR_7501.csv';
    static readonly IMPORTER_DOWNLOADED_FORM_7501_CIGARS_FILENAME = 'FL-TI-2323_April_FY_2016_ERROR_7501.csv';
    static readonly IMPORTER_DOWNLOADED_FORM_7501_SNUFF_FILENAME = 'FL-TI-2323_April_FY_2016_ERROR_7501.csv';
    static readonly IMPORTER_DOWNLOADED_FORM_7501_CHEW_FILENAME = 'FL-TI-2323_April_FY_2016_ERROR_7501.csv';
    static readonly IMPORTER_DOWNLOADED_FORM_7501_PIPE_FILENAME = 'FL-TI-2323_April_FY_2016_ERROR_7501.csv';
    static readonly IMPORTER_DOWNLOADED_FORM_7501_ROLL_YOUR_OWN_FILENAME = 'FL-TI-2323_April_FY_2016_ERROR_7501.csv';

}

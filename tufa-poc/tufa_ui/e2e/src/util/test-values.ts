/** This class may be used to reference any values used throughtout E2E scripts which could change in the future.
 *  i.e. in Quarterly Assessments page, we are referencing a specific year and quarter, and these values could possibly be subject to change.
 */
export class TestValues {
    static readonly QUARTERLY_ASSESSMENT_YEAR = '2050';
    static readonly QUARTERLY_ASSESSMENT_MONTH = 'October';
    static readonly QUARTERLY_ASSESSMENT_MONTH_FY_YEAR = 'October FY 2050';

    static readonly CIGAR_ASSESSMENT_MONTH_FY_YEAR_1 = 'October FY 2050';
    static readonly CIGAR_ASSESSMENT_MONTH_FY_YEAR_2 = 'February FY 2050';
    static readonly CIGAR_ASSESSMENT_MONTH_FY_YEAR_3 = 'April FY 2050';
    static readonly CIGAR_ASSESSMENT_MONTH_FY_YEAR_4 = 'July FY 2050';
    static readonly CIGAR_ASSESSMENT_YEAR = '2050';
    static readonly CIGAR_ASSESSMENT_MONTH_1 = 'October';
    static readonly CIGAR_ASSESSMENT_MONTH_2 = 'February';
    static readonly CIGAR_ASSESSMENT_MONTH_3 = 'April';
    static readonly CIGAR_ASSESSMENT_MONTH_4 = 'July';

    static readonly MANUFACTURER_EIN ='ADJUSTMENT5094';
    static readonly MANUFACTURER_REPORT ='January FY 2003';
    static readonly MANUFACTURER_5000_TAXRETURNID1 = 'TR2003-01';
    static readonly MANUFACTURER_5000_TAXRETURNID2 = 'TR2003-02';

}

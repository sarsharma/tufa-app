import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { isNil } from "ramda";
import { Observable, throwError } from "rxjs";
import { Router } from "../../../node_modules/@angular/router";
import { catchError, retry } from "../../../node_modules/rxjs/operators";
import { AuthService } from "../login/services/auth.service";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
	sessionToken: string;
	constructor(public auth: AuthService, public router: Router) {}
	intercept(
		request: HttpRequest<any>,
		next: HttpHandler
	): Observable<HttpEvent<any>> {
		this.sessionToken = this.auth.getAccessToken();		
		if(request.url.includes("api/auth/token")){
			this.sessionToken = this.auth.getRefreshToken();		
		}
		let headers = request.headers
			// .set("Content-Type", "application/json")
			.set("Cache-control", "no-cache")
			.set("Cache-control", "no-store")
			.set("Pragma", "no-cache")
			.set("Expires", "0");

		if (!isNil(this.sessionToken)) {
			headers = headers
				.set("X-Authorization", `Bearer ${this.sessionToken}`)
				.set("X-Requested-With", "XMLHttpRequest");
		}
		request = request.clone({ headers });
		return next.handle(request).pipe(
			// If there was an error try the request again
			retry(1),
			// If the retry faild deal with the error
			catchError((err) => {
				// 401 (Failed Authentication) kick user out
				if(err.status === 401 && err.url.indexOf("/api/auth/login") > -1) {
					return throwError(err.error);
				} else if (err.status === 401) {
					this.router.navigate(['./../logout']);
				}
				// Else grab the error message and pass it back up to be handeled elsewhere
				else {
					var error = err;
					if(!error.error.data) {
						var error = err.error.message || err.error.ex || err.statusText;
					}
					return throwError(error);
				}
			})
		);
	}
}

import { AdminService } from './../services/admin.service';
import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { User } from '../models/user.model';
import { Subscription } from 'rxjs';

import * as _ from 'lodash';

declare var jQuery: any;

// con
@Component({
    selector: 'admin-dashboard',
    templateUrl: './admin-dashboard.template.html',
    styleUrls: ['./admin-dashboard.styles.scss'],
    providers: [AdminService],
    encapsulation: ViewEncapsulation.None
})

export class AdminDashboardPage implements OnInit, OnDestroy  {

    router: Router;
    data: any[];
    adminCount: number;
    superuserCount: number;
    taxRateData: any[];
    htsCodes: any[] = [];
    htscode: any; newhtscode = {"htsCodeId" : null,
                                "htsCode" : null,
                                "tobaccoClassName" : 'Select Tobacco Class',
                                "tobaccoClassId": null};
    cloneTaxRateData: any[];
    rolesWait: Subscription;
    cigarTypes = ['Large', 'Small', 'Ad Valorem'];
    cigaretteTypes = ['Large', 'Small'];
    chewTypes = ['Chew', 'Snuff'];
    ryoTypes = ['Roll-Your-Own', 'Pipe'];
    nonConversionTypes = ['Cigars - Large', 'Cigars - Small', 'Cigars - Ad Valorem',
                        'Large', 'Small', 'Ad Valorem', 'Cigarettes'];
    keyTobaccoClasses = ['Chew', 'Snuff', 'Pipe', 'Roll-Your-Own', 'Cigarettes', 'Cigars', 'Non-Taxable'];
    dropdownData = [];
    alerts: Array<Object>;
    effectiveDate: Date;
    sortBy = "htsCode";

    constructor(private _adminService: AdminService, router: Router) {

        this.alerts = [
            {
                type: 'success',
                msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Successfully generated 12 monthly reports</span>'
            }
        ];

        this.router = router;

    }

    ngOnInit() {
        let searchInput = jQuery('#table-search-input, #search-countries');
        searchInput
        .focus((e) => {
          jQuery(e.target).closest('.input-group').addClass('focus');
        })
        .focusout((e) => {
          jQuery(e.target).closest('.input-group').removeClass('focus');
        });

        // load the users
        this.loadUsers();
        this.loadRates();

   }

    ngOnDestroy() {
        if (this.rolesWait) {
            this.rolesWait.unsubscribe();
        }
    }

   loadUsers() {
     this.rolesWait = this._adminService.getUsers().subscribe(roles => {
        if (roles) {
          this.data = <any[]>roles;
          this.adminCount = _.size(_.filter(this.data, item => item.role === 'Administrator'));
          this.superuserCount = _.size(_.filter(this.data, item => item.role === 'Super User'));
        }
      });
   }

   loadRates () {
     this._adminService.getTobaccoClasses().subscribe(tobaccoClasses => {
        if (tobaccoClasses) {
          this.taxRateData = <any[]>tobaccoClasses;
          this.effectiveDate = this.taxRateData[0].taxRateEffectiveDate;
          this.dropdownData = [{"tobaccoClass": 'Select Tobacco Class', "tobaccoClassId": null}];
          let strtId = Math.max.apply(Math, (<any[]>tobaccoClasses).map(function(o){return o.tobaccoClassId; }));
          //load dropdown
          for (let i in this.taxRateData) {
            if (this.keyTobaccoClasses.indexOf(this.taxRateData[i].tobaccoClassNm) !== -1) {
              let j = this.keyTobaccoClasses.indexOf(this.taxRateData[i].tobaccoClassNm);
              this.dropdownData.push({"tobaccoClass": this.keyTobaccoClasses[j],
                                      "tobaccoClassId": this.tobaccoReturnType(this.keyTobaccoClasses[j], this.taxRateData).tobaccoClassId });
              j++;
            }
          }
          // Initialize the class ids first
          for (let i in this.taxRateData) {
            if (this.taxRateData[i])
                this.taxRateData[i].tobaccoClassId = Number(i) + 1; // Number(strtId) + Number(i) + 1 send back the tobacco class id's exactly as is
          }

          // Assign parent child mapping now
          for (let i in this.taxRateData) {
            if (this.cigarTypes.indexOf(this.taxRateData[i].tobaccoClassNm) !== -1 && this.taxRateData[i].taxRate){
               this.taxRateData[i].tobaccoClassNm = "Cigars - " + this.taxRateData[i].tobaccoClassNm;
               this.taxRateData[i].parentClassId = this.tobaccoReturnType("Cigars", this.taxRateData).tobaccoClassId;
            } else if(this.cigaretteTypes.indexOf(this.taxRateData[i].tobaccoClassNm) !== -1){
               this.taxRateData[i].parentClassId = this.tobaccoReturnType("Cigarettes", this.taxRateData).tobaccoClassId;
            } else if(this.chewTypes.indexOf(this.taxRateData[i].tobaccoClassNm) !== -1 && this.taxRateData[i].taxRate){
               this.taxRateData[i].parentClassId = this.tobaccoReturnType("Chew-and-Snuff", this.taxRateData).tobaccoClassId;
            } else if(this.ryoTypes.indexOf(this.taxRateData[i].tobaccoClassNm) !== -1 && this.taxRateData[i].taxRate){
               this.taxRateData[i].parentClassId = this.tobaccoReturnType("Pipe-RYO", this.taxRateData).tobaccoClassId;
            }
          }

          // Fetch HTS codes
          this._adminService.getHTSCodes().subscribe(htscodes => {
            if (htscodes) {
              //Initialize hts data
              this.htsCodes = [];
              for (let i in htscodes) {
                if (htscodes[i])
                  this.htsCodes.push({"htsCodeId" : htscodes[i].htsCodeId,
                                      "htsCode" : +htscodes[i].htsCode,
                                      "tobaccoClassName" : this.returnTobaccoClassNm(htscodes[i].tobaccoClassId, tobaccoClasses),
                                      "tobaccoClassId": htscodes[i].tobaccoClassId});
              }
            }
          });
        }
      });
   }

   tobaccoReturnType(className: string, rateObj: any) {
     for (let key in rateObj) {
       if (rateObj.hasOwnProperty(key) && rateObj[key].tobaccoClassNm === className) {
         return rateObj[key];
       }
     }
   }

   returnTobaccoClassNm(id: number, rateObj: any) {
     for (let key in rateObj) {
       if (rateObj.hasOwnProperty(key) && rateObj[key].tobaccoClassId % this.taxRateData.length === id % this.taxRateData.length) {
         return rateObj[key].tobaccoClassNm;
       }
     }
   }

   selectRates(cloneTaxRateData: any[]){
     this.cloneTaxRateData = JSON.parse(JSON.stringify(cloneTaxRateData));
   }

    selectCode(codeData: any) {
     this.htscode = JSON.parse(JSON.stringify(codeData));
   }

   reloadUsers(reload: boolean){
     if (reload) {
       this.loadUsers();
     }

   }

   reloadTaxRates(reload: boolean) {
     if (reload) {
       this.loadRates();
     }
   }
}


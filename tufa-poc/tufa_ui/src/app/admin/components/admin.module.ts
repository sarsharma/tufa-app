import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AdminService } from "../../admin/services/admin.service";
import { SharedModule } from "../../shared/shared.module";
import { AuthService } from "./../../login/services/auth.service";
import { CreateUserPopup } from "./create-user-popup.component";
import { MonthlyReportsPage } from "./monthly-reports.component";
import { AdminDashboardPage } from "./admin-dashboard.component";
import { EditUserPage } from "./edit-user.component";
import { TaxRatesPopup } from "./tax-conversions.component";
import { HTSCodesPopup } from "./hts-codes.component";
import { ConfirmDirective } from "../../shared/globalDirectives/confirm.directive";

export const routes = [
    { path: 'monreport', component: MonthlyReportsPage },
    { path: 'dashboard', component: AdminDashboardPage },
    { path: 'userprofile', component: EditUserPage },
    { path: 'userprofile/:id', component: EditUserPage }
];

@NgModule({
    imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
    declarations: [MonthlyReportsPage, AdminDashboardPage, ConfirmDirective,
    EditUserPage, CreateUserPopup, TaxRatesPopup, HTSCodesPopup],
    providers: [AuthService, AdminService]
})

export class AdminModule {
    static routes = routes;
}

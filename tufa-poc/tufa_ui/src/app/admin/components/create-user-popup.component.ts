/*
@author : Deloitte
this is Component for creating user
*/
import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { User } from "../models/user.model";
import { AdminService } from "../services/admin.service";
declare var jQuery: any;

@Component({
	selector: "[create-user-popup]",
	templateUrl: "./create-user-popup.template.html",
	styleUrls: ["./create-user-popup.styles.scss"],
	encapsulation: ViewEncapsulation.None
})

/**
 *
 *
 * @export
 * @class CreateUserPopup
 * @implements {OnInit}
 */
export class CreateUserPopup implements OnInit {

  router: Router;
  showErrorFlag: boolean;
  alerts: Array<Object>;
  user = new User();
  @Output() reloadUsers = new EventEmitter<boolean>();
  userRoles = ['', 'Administrator', 'Super User'];
  date3: Date;
  enddateid: string = "end-date";

  // initialize the datetime control.
  datepickerOpts = {
    autoclose: true,
    todayBtn: 'linked',
    todayHighlight: true,
    assumeNearbyYear: true,
    requiredErrorMsg : "Error: End date is required",
    title: 'Enter End Date in MM/DD/YYYY format'
  };

  ngOnInit(): void {
    this.showErrorFlag = false;
  }

  constructor(router: Router,
    private _adminService: AdminService) {
    this.router = router;

    this.date3 = new Date(new Date().setFullYear(new Date().getFullYear() + 1));

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages When Create Company Fails. e.g Duplicate EIN'
      }
    ];

    this.showErrorFlag = false;
  }

  // Create a new user
  createUser(user: User) {
    jQuery('#adduserform').parsley().validate();

    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#adduserform').parsley().isValid()) {
      user.role = user.role === "Administrator" ? "ROLE_ADMIN" : "ROLE_SUPERUSER";
      user.email = user.email.toLowerCase();
      if (this.date3) {
        user.endDt = this.date3.toString();
      }

      this._adminService.createUser(user).subscribe(data => {
        if (data) {
          jQuery('#create-user').modal('hide');
          this.reset();
          this.reloadUsers.emit(true);
        }
      }, error => {
        if(error){
          if (error.indexOf('USER_EMAIL_UK') > -1) {
            error = "User already exists";
          }
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({ type: 'warning', msg: error });
        }
      });
    }

  }

  reset() {
    jQuery('#create-user').on('hidden').find('#first-name').val('');
    jQuery('#create-user').on('hidden').find('#last-name').val('');
    jQuery('#create-user').on('hidden').find('#phone-number').val('');
    jQuery('#create-user').on('hidden').find('input[type="email"]').val('');
    jQuery('#adduserform').parsley().reset();
    jQuery('#create-user').modal('hide');
    this.showErrorFlag = false;
  }

}

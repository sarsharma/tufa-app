/*
@author : Deloitte

this is Component for creating user.
*/

import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { User } from '../models/user.model';
import { Subscription } from 'rxjs';
import { NgBusyModule } from 'ng-busy';

declare var jQuery: any;

@Component({
  selector: '[edit-user]',
  templateUrl: './edit-user.template.html',
  styleUrls: ['./edit-user.styles.scss'],
  encapsulation: ViewEncapsulation.None
})

/**
 *
 *
 * @export
 * @class EditUserPage
 * @implements {OnInit}
 */

export class EditUserPage implements OnInit, OnDestroy {

  showErrorFlag: boolean;
  alerts: Array<Object>;
  user: User;
  usermodel = new User();
  statusList = ['Active', 'Inactive'];
  userRoles = ['Administrator', 'Super User'];
  sub: any;
  userid: any;
  busy: Subscription;
  date2: Date;
  userStatus: any;

  // initialize the datetime control.
  datepickerOpts = {
    autoclose: true,
    todayBtn: 'linked',
    todayHighlight: true,
    assumeNearbyYear: true,
    requiredErrorMsg : "Error: End date is required",
    title: 'Enter End Date in MM/DD/YYYY format'
  };

  ngOnInit(): void {
    this.showErrorFlag = false;
    // this.userInfoLoad();
  }

  ngOnDestroy() {
      if (this.busy) {
          this.busy.unsubscribe();
      }
  }

  constructor(
    private router: Router,
    private _adminService: AdminService,
    private route: ActivatedRoute) {

    this.userInfoLoad();
    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages When Create Company Fails. e.g Duplicate EIN'
      }
    ];

    this.showErrorFlag = false;
  }

  userInfoLoad() {

    this.sub = this.route.params.subscribe(params => {
      if (params['id']) {
        this.userid = params['id'];
        this.busy = this._adminService.getUser(this.userid)
          .subscribe(data => {
            if (data) {
              this.usermodel = <User>data;
              this.usermodel.role = ((<any>data).role === "ROLE_ADMIN") ? "Administrator" : "Super User";
              this.usermodel.enabled = ((<any>data).enabled === "Y") ? "Active" : "Inactive";
              if(this.usermodel.endDt) this.date2 = new Date(this.usermodel.endDt);
            }
          });

      } else {
        this.usermodel = new User();
      }
    });

  }

  /**
   *
   *
   * @param {User} user
   *
   * @memberOf Edituser
   */
  updateUser(user: User) {
    jQuery('#edituserform').parsley().validate();

    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#edituserform').parsley().isValid()) {
      user.role = (user.role === "Administrator") ? "ROLE_ADMIN" : "ROLE_SUPERUSER";
      user.email = user.email.toLowerCase();
      if(this.date2) user.endDt = this.date2.toString();
      user.enabled = (user.enabled === "Active") ? "Y" : "N";

      this._adminService.updateUser(user).subscribe(data => {
        if (data) {
          this.gotoAdminDashboard();
        }
      }, error => {
        if(error) {
          if (error.indexOf('USER_EMAIL_UK') > -1) {
            error = "User already exists";
          }
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({ type: 'warning', msg: error });
        }
        });
    }

  }

  gotoAdminDashboard() {
    this.router.navigate(['/app/admin/dashboard']);
  }

}

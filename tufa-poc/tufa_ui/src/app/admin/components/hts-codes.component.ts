/*
@author : Deloitte
this is Componentfor adding contact as a popup.
*/
import { Component, Input, Output, EventEmitter, ViewEncapsulation, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../services/admin.service';
declare var jQuery: any;

@Component({
  selector: '[hts-codes]',
  templateUrl: './hts-codes.template.html',
  encapsulation: ViewEncapsulation.None
})

/**
 *
 *
 * @export
 * @class EditUserPage
 * @implements {OnInit}
 */

export class HTSCodesPopup implements OnInit {

  router: Router;
  showErrorFlag: boolean;
  alerts: Array<Object>;
  @Input() dropDownData: any[];
  @Input() codeModel: any[]; classId: any[]; cloneTaxRates: any[];
  code: any = {};
  @Output() reloadCodes = new EventEmitter<boolean>();

  ngOnInit(): void {
    this.showErrorFlag = false;
  }

  constructor(router: Router,
    private _adminService: AdminService) {
    this.router = router;
    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span>'
      }
    ];

    this.showErrorFlag = false;
  }

  saveCode(code: any) {
    jQuery('#htscodeform').parsley().validate();

    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#htscodeform').parsley().isValid()) {
        this.code.tobaccoClassId = this.tobaccoReturnType(code.tobaccoClassName, this.dropDownData).tobaccoClassId;
        this.code.htsCode = code.htsCode;
        this._adminService.editHTSCode(this.code).subscribe(success => {
          this.reloadCodes.emit(true);
          this.reset();
        }, error => {
          jQuery('#htscode').parsley().removeError('HTS-Exists');
            error = "Error: HTS code " + this.code.htsCode + " already exists";
            jQuery('#htscode').parsley().addError('HTS-Exists',  { message: error } );
        });
    }

  }

reset() {
    jQuery('#htscodeform').parsley().reset();
    jQuery('#hts-code').modal('hide');
    this.showErrorFlag = false;
  }

resetError() {
  jQuery('#htscodeform').parsley().reset();
}

tobaccoReturnType(className: string, rateObj: any) {
     for (let key in rateObj) {
       if (rateObj.hasOwnProperty(key) && rateObj[key].tobaccoClass === className) {
         return rateObj[key];
       }
     }
   }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "codeModel") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.code.htsCodeId = chg.currentValue.htsCodeId;
          this.code.tobaccoClassId = chg.currentValue.tobaccoClassId;
          this.code.tobaccoClassName = chg.currentValue.tobaccoClassName;
          this.code.htsCode = chg.currentValue.htsCode;
        }
      }
    }
  }
}

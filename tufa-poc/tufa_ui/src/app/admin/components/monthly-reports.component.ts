import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { CreateReports } from '../services/create-reports.service';

declare var jQuery: any;
declare var window: any;
@Component({
    selector: 'monthly-reports',
    templateUrl: './monthly-reports.template.html',
    styleUrls: ['./monthly-reports.styles.scss'],
    providers: [CreateReports],
    encapsulation: ViewEncapsulation.None
})

export class MonthlyReportsPage implements OnInit {

    calMonth: string;
    calYear: string;
    monthYear: string;
    router: Router;
    alerts: Array<Object>;
    showAlertFlag: boolean = false;
    // tslint:disable-next-line:max-line-length
    monthList: any[] = ['Month', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    constructor(private _createMonthlyReports: CreateReports, router: Router) {

        this.alerts = [
            {
                type: 'success',
                msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Successfully generated 12 monthly reports</span>'
            }
        ];

        this.router = router;
    }

    ngOnInit() {
        this.calMonth = 'Month';
        this.showAlertFlag = false;

         // Parsley custom error message
        window.Parsley.addValidator('year', {
            requirementType: 'integer',
            validateNumber: function(value) {
                return value > 0;
            },
            messages: {
                en: "Calendar Year requires a positive value"
            }
        });
    }

    createMonthly(x, y) {
        y = this.monthList.indexOf(y);
        if (y < 10)
            y = '0' + y;

        this.showAlertFlag = false;
        jQuery('#createreports-form').parsley().validate();

        this.monthYear = y + '-' + x;
        if (jQuery('#createreports-form').parsley().isValid()) {
            this._createMonthlyReports.saveMonthlyReports(this.monthYear).subscribe(data => {
                if (data.toString() === '0') {
                    this.alerts[0]["type"] = 'danger';
                    // tslint:disable-next-line:max-line-length
                    this.alerts[0]["msg"] = '<i class="fa fa-circle text-danger" ></i><span class="alert-text">Reports were already generated</span>';
                } else if (data.toString() === '-1') {
                    this.alerts[0]["type"] = 'danger';
                    // tslint:disable-next-line:max-line-length
                    this.alerts[0]["msg"] = '<i class="fa fa-circle text-danger" ></i><span class="alert-text">Missing criteria: Unable to generate reports</span>';
                } else {
                    this.alerts[0]["type"] = 'success';
                    // tslint:disable-next-line:max-line-length
                    this.alerts[0]["msg"] = '<i class="fa fa-circle text-success" ></i><span class="alert-text">Successfully generated ' + data + ' monthly reports</span>';
                }
                this.showAlertFlag = true;
            }, error => {
                this.alerts[0]["type"] = 'danger';
                // tslint:disable-next-line:max-line-length
                this.alerts[0]["msg"] = '<i class="fa fa-circle text-danger" ></i><span class="alert-text">Missing criteria: Unable to generate reports</span>' +
                        ' ' + error;
                this.showAlertFlag = true;
            });
        }
    }

    setboxfocus(elementID: String): void {

        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let searchInput = jQuery(elementID);
        searchInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

    closeAlert(): void {
        this.showAlertFlag = false;
    }

    gotoDailyOperations(){
        this.router.navigate(["/app/company/search"]);
    }

}

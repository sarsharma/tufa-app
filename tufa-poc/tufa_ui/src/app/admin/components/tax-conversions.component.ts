/*
@author : Deloitte
this is Componentfor adding contact as a popup.
*/
import { Component, Input, Output, EventEmitter, ViewEncapsulation, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../services/admin.service';
declare var jQuery: any;

@Component({
  selector: '[tax-conversions]',
  templateUrl: './tax-conversions.template.html',
  styleUrls: ['./tax-conversions.styles.scss'],
  encapsulation: ViewEncapsulation.None
})

/**
 *
 *
 * @export
 * @class EditUserPage
 * @implements {OnInit}
 */

export class TaxRatesPopup implements OnInit {

  router: Router;
  showErrorFlag: boolean;
  alerts: Array<Object>;
  @Input() ratesModel: any[]; taxRates: any[]; cloneTaxRates: any[];
  @Output() reloadTaxRates = new EventEmitter<boolean>();
  userRoles = ['', 'Administrator', 'Super User'];
  cigarTypes = ['Large', 'Small', 'Ad Valorem'];
  cigarTypesExisting = ['Cigars - Large', 'Cigars - Small', 'Cigars - Ad Valorem'];
  nonConversionTypes = ['Cigars - Large', 'Cigars - Small', 'Cigars - Ad Valorem',
                        'Large', 'Small', 'Ad Valorem', 'Cigarettes'];
  // tslint:disable-next-line:max-line-length
  tobaccoClassMap = {'Cigars - Large': "Large",
                     'Cigars - Small': "Small",
                     'Cigars - Ad Valorem': "AdValorem",
                     'Cigarettes': "Cigarettes", 'Chew': "Chew",
                     'Snuff': "Snuff", 'Pipe': "Pipe", 'Roll-Your-Own': "RollYourOwn"};
  date3: Date = new Date();
  sortBy = "tobaccoClassNm";
  btnDisable: Boolean = false;
  activedateid: string = "active-date";

  // initialize the datetime control.
  datepickerOpts = {};

  ngOnInit(): void {
    this.showErrorFlag = false;
  }

  constructor(router: Router,
    private _adminService: AdminService) {
    this.router = router;
    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span>'
      }
    ];

    this.showErrorFlag = false;
  }

  // new rates
  editRates(taxrates: any) {
    jQuery('#taxRatesform').parsley().validate();

    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#taxRatesform').parsley().isValid()) {
        this.btnDisable = true;
        this.taxRates = taxrates;
        for (let rate in this.taxRates) {
            this.taxRates[rate].taxRateEffectiveDate = this.date3;
            let typeIndex = this.cigarTypesExisting.indexOf(this.taxRates[rate].tobaccoClassNm);
            if (typeIndex !== -1)
                this.taxRates[rate].tobaccoClassNm = this.cigarTypes[typeIndex];
        }
        this._adminService.editTobaccoClasses(this.taxRates).subscribe(success => {
          this.reloadTaxRates.emit(true);
          this.reset();
          this.btnDisable = false;
        });
    }

  }

  setTaxRates(tobaccoClass) {
    this.tobaccoReturnType(tobaccoClass, this.taxRates).taxRate = tobaccoClass === "Cigars - Ad Valorem" ?
          jQuery("#" + this.tobaccoClassMap[tobaccoClass] + "-tax-rate").val() / 100 :
          jQuery("#" + this.tobaccoClassMap[tobaccoClass] + "-tax-rate").val();
  }

  setConversionRates(tobaccoClass) {
    this.tobaccoReturnType(tobaccoClass, this.taxRates).conversionRate = jQuery("#" + this.tobaccoClassMap[tobaccoClass] + "-conversion").val();
  }

  tobaccoReturnType(className: string, rateObj: any) {
     for (let key in rateObj) {
       if (rateObj.hasOwnProperty(key) && rateObj[key].tobaccoClassNm === className) {
         return rateObj[key];
       }
     }
   }

  reset() {
    jQuery('#taxRatesform').parsley().reset();
    jQuery('#tax-conversions').modal('hide');
    this.showErrorFlag = false;
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "ratesModel") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.taxRates = chg.currentValue;
          this.cloneTaxRates = jQuery.extend(true, {}, this.taxRates);
          this.date3 = new Date(this.taxRates[0].taxRateEffectiveDate);
          this.date3.setDate(this.date3.getDate() + 1);
          this.datepickerOpts = {
            startDate: this.date3,
            autoclose: true,
            requiredErrorMsg : "Error: Effective date is required",
            title: 'Enter Effective Date in MM/DD/YYYY format'
          };
        }
      }
    }
  }

}


export class HTSCode {
htsCodeId: number;
tobaccoClassId: number;
htsCode: string;
htsCodeNotes: string;
createdBy: string;
createdDt: Date;
modifiedBy: string;
modifiedDt: Date;
}
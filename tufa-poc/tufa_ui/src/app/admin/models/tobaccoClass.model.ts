export class TobaccoClass {
    tobaccoClassId: number;
    parentClassId: number;
    tobaccoClassNm: string;
    classTypeCd: string;
    taxRateEffectiveDate: Date;
    taxRate: number;
    conversionRate: number;
    createdBy: string;
    createdDt: Date;
    modifiedBy: string;
    modifiedDt: Date;
}
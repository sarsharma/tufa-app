
export class User {
userId: number;
email: string;
password: string;
firstname: string;
lastname: string;
role: string;
gender: string;
dob: string;
phone1: string;
selectedstate: string;
state: string;
zipcode: string;
address: string;
lastLoginDt: string;
endDt: string;
enabled: string;
}

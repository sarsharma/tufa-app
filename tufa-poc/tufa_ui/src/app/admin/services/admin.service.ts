/*
@author : Deloitte
Service class to perform Admin related operations.
*/
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { User } from "../models/user.model";
import { environment } from "./../../../environments/environment";

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

/**
 *
 *
 * @export
 * @class AdminService
 */
@Injectable()
export class AdminService {
	private retrieveuserurl = environment.apiurl + "/api/useradmin/retrieve";
    private getusersurl = environment.apiurl + '/api/useradmin/users';
    private createupdateuserurl = environment.apiurl + '/api/useradmin/user';
    private getTobaccoClassesurl = environment.apiurl + '/api/v1/permit/taxrates';
    private htsCodeurl = environment.apiurl + '/api/v1/trueup/htscodes';

    constructor(private _httpClient: HttpClient) { }

    getUsers(){
        return this._httpClient.get(this.getusersurl, httpOptions);
    }

     getTobaccoClasses() {
        return this._httpClient.get(this.getTobaccoClassesurl, httpOptions);
    }

    editTobaccoClasses(taxrates: any): Observable<string> {
        return this._httpClient.put<string>(this.getTobaccoClassesurl, (taxrates), httpOptions).pipe(
			tap(_ => this.log("getUsers")),
			catchError(this.handleError<any>("getUsers"))
		);
    }

    getHTSCodes() {
        return this._httpClient.get(this.htsCodeurl, httpOptions);
    }

    editHTSCode(code: any): Observable<string> {
        return this._httpClient.post(this.htsCodeurl, (code), httpOptions).pipe(
			tap(_ => this.log("editHTSCode")),
			catchError(this.handleError<any>("editHTSCode"))
		);
    }

    createUser(user: User): Observable<User> {
        return this._httpClient.post(this.createupdateuserurl, JSON.stringify(user), httpOptions).pipe(
			tap(_ => this.log("createUser")),
			catchError(this.handleError<any>("createUser"))
		);
    }

    getUser(id) {
        return this._httpClient.get(this.createupdateuserurl + "/"+ id, httpOptions);
    }

    updateUser(user: User): Observable<User> {
        return this._httpClient.put(this.createupdateuserurl + "/"+ user.userId, JSON.stringify(user), httpOptions).pipe(
			tap(_ => this.log("createUser")),
			catchError(this.handleError<any>("createUser"))
		);
    }

	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { environment } from '../../../environments/environment';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

// export const routes = [
//   { path: 'dashboard', component: MonthlyReportsPage},
// ];

// @NgModule({
//     imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
//     declarations: [MonthlyReportsPage, ConfirmDirective],
//     providers: [AuthService],
// })
@Injectable()
export class CreateReports { 
    private monthlyReportsUrl = environment.apiurl + '/api/v1/companies/period/create';

    constructor(private _httpClient: HttpClient) { }

     saveMonthlyReports(monthYear: String): Observable<String> {
        return this._httpClient.put(this.monthlyReportsUrl+'/'+monthYear,this.monthlyReportsUrl+'/'+monthYear , httpOptions).pipe(
			tap(_ => this.log("saveMonthlyReports")),
			catchError(this.handleError<any>("saveMonthlyReports"))
		);
    }

    	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}

import { LocalStorageService } from "ngx-webstorage";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { AuthService } from "./login/services/auth.service";
import { Component, OnInit, OnDestroy, HostListener, NgZone } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { RefreshTokenService } from './shared/service/refresh.token.service';

declare var window: any;
declare var jQuery: any;

@Component({
	selector: "app-root",
	templateUrl: "./app.template.html",
	styleUrls: ["./app.style.scss"],
	providers: [LocalStorageService]
})
export class AppComponent implements OnInit, OnDestroy {
	// subscription: Subscription;
	msg: string = "Your session is about to expire.  Click close to continue.";
	title: string = "Session Timeout";

	constructor(
		public auth: AuthService,
		private storage: LocalStorageService,
		private titleService: Title,
		private router: Router,
		private refreshTokenService: RefreshTokenService,
		private zone: NgZone
	) { }

	ngOnInit() {
		this.setTitle("TUFA");
		let context = this;
		window.addEventListener("beforeunload", function (e) {
			// context.subscription.unsubscribe();
			// context.auth.expiring.unsubscribe();
			context.onClickLogout();
		});
	}

	ngOnDestroy() {
		// this.subscription.unsubscribe();
		// this.auth.expiring.unsubscribe();
		this.onClickLogout();
	}

	public clearLocalStorageContent(modPrefix) { }

	public onClickLogout() {
		this.storage.clear("LOGGEDUSERID");
		this.storage.clear("LOGGEDUSEREMAIL");
	}

	public onClose() {
	}

	public setTitle(newTitle: string) {
		this.titleService.setTitle(newTitle);
	}

	public closeAnnouncement() {
		jQuery("#announcement").hide();
	}

	public getLoggedUser() {
		return this.auth.getfullName();
	}

	@HostListener('window:click', ['$event.type']) onClick(type: string) {
		this.setEvent(type);
	}

	// @HostListener('mouseenter', ['$event.type']) onMouseEnter(type: string) {
	// 	this.setEvent(type);
	// }
	@HostListener('mousedown', ['$event.type']) onMouseDown(type: string) {
		this.setEvent(type);
	}
	// @HostListener('mouseleave', ['$event.type']) onMouseLeave(type: string) {
	// 	this.setEvent(type);
	// }
	// @HostListener('mousemove', ['$event.type']) onMouseMove(type: string) {
	// 	this.setEvent(type);
	// }
	// @HostListener('mouseover', ['$event.type']) onMouseOver(type: string) {
	// 	this.setEvent(type);
	// }
	@HostListener('mouseup', ['$event.type']) onMouseUp(type: string) {
		this.setEvent(type);
	}
	@HostListener('document:keydown', ['$event.type']) onKeyDown(type: string) {
		this.setEvent(type);
	}

	@HostListener('document:keypress', ['$event.type']) onKeyPress(type: string) {
		this.setEvent(type);
	}
	@HostListener('document:keyup', ['$event.type']) onKeyUp(type: string) {
		this.setEvent(type);
	}

	public setEvent(type: string) {
		this.zone.runOutsideAngular(() => {
			if (this.auth.loggedIn()) {
				this.refreshTokenService.setEventToQueue(type);
			}
		})
	}
}

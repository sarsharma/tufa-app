import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from "@angular/common/http";
import { NgModule, ErrorHandler } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { PreloadAllModules, RouterModule } from "@angular/router";
//import { CookieService, CookieOptions } from "angular2-cookie/core";

import { CookieModule } from 'ngx-cookie';

import { AppComponent } from "./app.component";
import { AppConfig } from "./app.config";
import { ROUTES } from "./app.routes";
import { AuthorizationHelper } from "./authentication/util/authorization.helper.util";
import { ErrorComponent } from "./error/error.component";
import { AuthGuard } from "./login/services/auth-guard.service";
import { AuthService } from "./login/services/auth.service";
import { JwtUtil } from "./login/util/jwt.util";
import { JwtInterceptor, ErrorInterceptor } from "./_helpers";
import { CoreModule } from "./core/core.module";
import { RefreshTokenService } from './shared/service/refresh.token.service';
import { TufaErrorHandler } from "./layout/error.handler.global.component";
import { RouterGlobalService } from "./shared/service/router.global.service";

import { ToastrModule } from 'ngx-toastr';

//Tried importing this through angular.json but the app would not read from it
import 'jasny-bootstrap/js/fileinput.js';

const APP_PROVIDERS = [AppConfig];

@NgModule({
	bootstrap: [AppComponent],
	declarations: [
		AppComponent,
		ErrorComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		HttpClientModule,
		CookieModule.forRoot(),
		CoreModule.forRoot({ userName: "System User" }),
		RouterModule.forRoot(ROUTES, {
			useHash: false,
			preloadingStrategy: PreloadAllModules
		}),
		ToastrModule.forRoot({positionClass: 'toast-top-center'})
	],
	providers: [
		APP_PROVIDERS,
		AuthGuard,
		AuthService,
		JwtUtil,
		AuthorizationHelper,
		RouterGlobalService,
		RefreshTokenService,
		HttpClient,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: JwtInterceptor,
			multi: true
		},
		{
		  provide: ErrorHandler,
		  useClass: TufaErrorHandler,
		},
		
	]
})
export class AppModule {}

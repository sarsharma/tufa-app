import { Routes } from "@angular/router";
import { ErrorComponent } from "./error/error.component";
import { AuthGuard } from "./login/services/auth-guard.service";


export const ROUTES: Routes = [
  {
    path: 'login', loadChildren: './login/login.module#LoginModule'
  },{
   path: '', redirectTo: '/login', pathMatch: 'full'
  },{
    path: 'logout', loadChildren: './logout/logout.module#LogoutModule'
  },{
    path: 'app',   loadChildren:'./layout/layout.module#LayoutModule', canActivate: [AuthGuard]
  }, {
    path: 'error', component: ErrorComponent
  }, {
    path: '**',    component: ErrorComponent
  }
];

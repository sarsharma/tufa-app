import {Injectable} from '@angular/core';
import {UserDetails} from '../../login/model/userdetails.model'
import {Credentials} from '../../login/model/credentials.model'
import {Headers} from '@angular/http';

@Injectable()
export class AuthorizationHelper {

saveUserDetails(userDetails: UserDetails){
  sessionStorage['userDetails']= userDetails;
}
        
 getUserDetails(){
  return sessionStorage['userDetails'];
 }
 
 removeUserDetails(){
     sessionStorage.removeItem('userDetails');
 }
 
 appendAuthorizationHeader(credentials: Credentials, headers: Headers) {
    if(!headers)
      headers = new Headers();
      
    if(credentials){
        headers.append('Authorization', 'Basic ' +
        btoa(credentials.username+':'+credentials.password)); 
     }
     return headers;
   }
   
  canAccessUrl(resourceUrl){
    if(this.getUserDetails()){
        var  resources = this.getUserDetails().getResources();
        for(var resource in resources){
            var url = resource;
            if( resourceUrl.search(url) !== -1)
              return true;
        }
    }
    return false;
  }
  
  hasPrivilege(resourceUrl,httpMethod){
    
     if(this.getUserDetails()){
        var  functionGrps = this.getUserDetails().getFuncGrps();
        for(var resource in functionGrps){
            if(resource.search(resourceUrl) !== -1){
               var privileges = functionGrps[resource];
               if(privileges.search(httpMethod) !== -1)
                 return true;
          }
        }
      }
      
      return false;
  }
  
  
}
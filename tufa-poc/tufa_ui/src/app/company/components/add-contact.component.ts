/*
@author : Deloitte
this is Componentfor adding contact as a popup.
*/
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Contact } from '../../model/contact.model';
import { Company } from '../models/company.model';
import { ContactService } from '../services/contact.service';
import { Router } from '@angular/router';

declare var jQuery: any;

@Component({
  selector: '[add-contact]',
  templateUrl: './add-contact.template.html',
  providers: [ContactService]
})

export class AddContactPopup {

  contact = new Contact();
  @Input() companyModel: Company;
  @Output() onCompanyEdit = new EventEmitter<boolean>();

  showErrorFlag: boolean;
  alerts: Array<Object>;
  public mask = ['+', /\d/, /\d/, /\d/ ];
  public extmask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/ ];


  constructor(private contactService: ContactService) {
    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];
    this.contact.cntryDialCd = "1";
    this.contact.cntryFaxDialCd = "1";
    this.showErrorFlag = false;
  }

  ngOnInit():void{
  }
  /*    
  ngAfterViewInit(): void {
    jQuery('#addcontactform').parsley().on('form:validate', function (formInstance) {
        // If any of these fields are valid
        if (jQuery("#add-first-name").parsley().isValid() || 
            jQuery("#add-last-name").parsley().isValid() || 
            jQuery("#add-email-address").parsley().isValid() || 
            jQuery("#add-phone-number").parsley().isValid() ||
            jQuery("#add-ext-optional").parsley().isValid() ||
            jQuery("#add-fax-optional").parsley().isValid()) 
        {        
            // Remove the required validation from all of them, so the form gets submitted
            // We already validated each field, so one is filled.
            // Also, destroy parsley's object
            jQuery("#add-first-name").removeAttr('required').parsley().destroy();
            jQuery("#add-last-name").removeAttr('required').parsley().destroy();
            jQuery("#add-email-address").removeAttr('required').parsley().destroy();
            jQuery("#add-phone-number").removeAttr('required').parsley().destroy();
            jQuery("#add-ext-optional").removeAttr('required').parsley().destroy();
            jQuery("#add-fax-optional").removeAttr('required').parsley().destroy(); 
            return;
        }
        
        // If none is valid, add the validation to them all
        jQuery("#add-first-name").attr('required', 'required').parsley();
        jQuery("#add-last-name").attr('required', 'required').parsley();
        jQuery("#add-phone-number").attr('required', 'required').parsley();
        jQuery("#add-ext-optional").attr('required', 'required').parsley();
        jQuery("#add-fax-optional").attr('required', 'required').parsley();
        return;
    });
  }*/
  // Create a new contact 
  createContact(contact: Contact) {
    jQuery('#addcontactform').parsley().validate();

    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#addcontactform').parsley().isValid()) {
      contact.company = this.companyModel;
      this.contactService.createContact(contact).subscribe(data => {
        if (data) {
          jQuery('#add-contact').modal('hide');
          this.reset(true);
        }
      },
        error => {
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({ type: 'warning', msg: error });
        });
    }

  }

  reset(flag) {
    jQuery('#add-contact').on('hidden').find('input[type="text"]').val('');
    jQuery('#add-contact').on('hidden').find('input[type="email"]').val('');
    jQuery('#addcontactform').parsley().reset();
    jQuery('#add-contact').modal('hide');
    this.showErrorFlag = false;
    this.onCompanyEdit.emit(flag);
  }
}

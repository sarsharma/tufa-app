import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { finalize } from '../../../../../node_modules/rxjs/operators';
import { AuthService } from '../../../login/services/auth.service';
import { CommentModel } from '../../../permit/models/comment/comment.model';
import { PermitExcludeService } from '../../services/permit-exclude.service';

declare var jQuery: any;
@Component({
    selector: '[comments-grid-company]',
    templateUrl: './comments-grid-company.template.html',
    styleUrls: ['./comments-grid-company.style.scss'],
    // encapsulation: ViewEncapsulation.None
})

export class CommentGridCompanyComponent implements OnInit {

    @Input() comments: CommentModel[];
    @Input() handleSave: boolean = true;
    @Input() showAttachments: boolean = false;
    @Input() commentArea: string;
    @Input() checked = false;
    @Output() commentAreaChange = new EventEmitter<String>();

    sortResultsBy = "commentDate";
    sortResultsOrder = ["desc"];
    showCommentModal: boolean = false;
    data: CommentModel[] = [];
    showEditArray: boolean[] = [];
    commentEdit: string = "";
    subscriptions: Subscription[] = [];
    showDocumentUploadPopup: boolean;
    commentSeq: number;

    constructor(
        private authService: AuthService,
        private exludeService: PermitExcludeService,
        private changeDetectorRef: ChangeDetectorRef) {
            this.changeDetectorRef.reattach();            
        }

    ngOnInit(): void {

    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "comments") {
                let chg = changes[propName];
                if (chg.currentValue) {
                    this.data = chg.currentValue;
                    this.showEditArray = [];
                    this.data.forEach(c => this.showEditArray.push(false));
                }
            }
        }
    }

    ngOnDestroy() {
        for (let i in this.subscriptions) {
            this.subscriptions[i].unsubscribe();
        }
    }

    onInlineEditCommentClick(index, commentText) {
        this.resetShowEditArray();
        this.showEditArray[index] = true;
        this.commentEdit = commentText;
    }

    onSaveCommentClick(comment: CommentModel) {
        comment.userComment = this.commentEdit;
        this.saveComment(comment);
    }

    onEditCommentClick(comment: CommentModel) {
        comment.userComment = this.commentEdit;
        comment.modifiedBy = this.authService.getSessionUser();
        this.saveComment(comment);
    }

    onCancelCommentClick(index) {
        this.resetShowEditArray();
        this.commentEdit = "";
    }

    private saveComment(comment: CommentModel) {
        this.exludeService.updateExcludeComment(comment).pipe(
            finalize(() => {
            })
        ).subscribe(
            data => {
                this.commentEdit = "";
                this.resetShowEditArray();
                this.changeDetectorRef.markForCheck();
            }
        );
    }

    private resetShowEditArray() {
        this.showEditArray.forEach((bool, index) => this.showEditArray[index] = false);
    }

    canEditComment(commentUser) {
        let sessionUser = this.authService.getSessionUser();
        return commentUser == sessionUser || (commentUser === "CTP_TUFA_ADMIN");
    }
    openPopup(commentSeq: number) {
        this.commentSeq = commentSeq;
        this.showDocumentUploadPopup = true;
    }

    updateAttacmnetsCount(count: number) {
        this.comments.forEach(com => {
            if(com.commentSeq == this.commentSeq) {
                com.attachmentsCount = count;
            }
        });
    }
}

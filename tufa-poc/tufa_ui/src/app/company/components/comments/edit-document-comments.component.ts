/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ShareDataService } from '../../../core/sharedata.service';
import { Document } from '../../../permit/models/document.model';
import { DownloadSerivce } from '../../../shared/service/download.service';
import { PermitExcludeService } from '../../services/permit-exclude.service';

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class EditDocumentCommentsPopup
 */
@Component({
  selector: '[edit-document-comments]',
  templateUrl: './edit-document-comments.template.html',
  styleUrls: ['./edit-document-comments.style.scss'],
 
})

export class EditDocumentCommentsPopup implements OnInit, OnDestroy {
  data: any[] = [];
  @Input() documentModel: Document;
  @Input() commentSeq:number;
  @Output() documentCount: EventEmitter<number> = new EventEmitter<number>();
  @Output() onDocumentClose: EventEmitter<boolean> = new EventEmitter<boolean>();
  doc = new Document();
 // @Output() onDocumentEdit = new EventEmitter<boolean>();
  filesToUpload: Array<File> = [];
  shareData: ShareDataService;
  downloading: Subscription;
  periodId: any;
  docFileName: string;
  busy: Subscription;
  edit: Subscription;
  types: string[] = [];
  selFormTypes: string[];
  btnDisable: Boolean = false;
  selectAllFlag: Boolean = false;
  warningFlag: Boolean = false;
  isExtensionNotPDF: Boolean = false;
  commentDocData: any[] = [];
  showConfirmDeletePopup: Boolean = false;
  @ViewChild('editattachmentform') documentUplaod: NgForm;

  ngOnInit() {
    console.log("EditDocumentCommentsPopup"); 
    //this.loadDocs(this.commentSeq);
  }

  constructor(
    private exludeService: PermitExcludeService, 
    private _shareData: ShareDataService,
    private downloadService: DownloadSerivce) {
    this.shareData = _shareData;
    this.warningFlag = false;
  }
  loadDocs(commentSeq: number) {
    this.commentDocData = [];
    this.busy = this.exludeService.loadPermitDocs(commentSeq).subscribe(
      data => {
        if (data) {
          let nd = data;
          let newData = [];
          for (let i = 0; i < nd.length; i++) {
            newData.push(nd[i]);
          }
          this.commentDocData = newData;
          this.documentCount.emit(this.commentDocData.length);
        }

      });
  }
  ngOnDestroy() {
    if (this.busy) {
      this.busy.unsubscribe();
    }

    if (this.edit) {
      this.edit.unsubscribe();
    }
    if (this.downloading) {
      this.downloading.unsubscribe();
    }
  }

 

  fileChangeEvent(event) {
    this.filesToUpload = <Array<File>>event.target.files;
    if (this.filesToUpload[0]) {
      this.docFileName = this.filesToUpload[0].name;

      let fileExt = this.docFileName.split('.').pop();
      if (fileExt.toUpperCase() !== "PDF") {
        this.isExtensionNotPDF = true;
      } else {
        this.isExtensionNotPDF = false;
      }

    } else {
      this.docFileName = "";
      this.warningFlag = false;
      this.isExtensionNotPDF = false;
    }

  }

  uploadAttachment() {
    jQuery('#editattachmentform').parsley().validate();
    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#editattachmentform').parsley().isValid() && !this.isExtensionNotPDF) {
      this.btnDisable = true;
      if (this.filesToUpload.length > 0) {
        let formData: FormData = new FormData();
        formData.append('file', this.filesToUpload[0]);

        let document = new Document();
        document.docDesc = this.docFileName;
        document.commentId = this.commentSeq;
        document.docFileNm = "ATCH";
        document.docStatusCd = "INCP";
        document.formTypes = this.types.join(", ");

        formData.append('metaData',JSON.stringify(document));
        this.edit=this.uploadImpl(formData);
        
      } else {
        let document = new Document();
        document.documentId = this.doc.documentId;
        document.docDesc = this.docFileName;
        document.formTypes = this.types.join(", ");
        //this.edit = this.updateAttachment(document);
      }
    }
  }

  uploadImpl(document) {
    return this.exludeService.uploadPermitAttachment(document).subscribe(data => {
    if (data) {
       // this.onDocumentEdit.emit(true);
        this.reset();
        this.loadDocs(this.commentSeq);
        this.btnDisable = false;
      }
    });
  }

  updateAttachment(document: Document) {
    
  }

  reset() {
    this.documentUplaod.resetForm();
    jQuery('.fileinput').fileinput('clear')
  }

  cancel() {
    this.reset();
    jQuery('#edit-document-comments').modal('hide');
  }
  
  /*
    ngOnChanges() used to capture changes on the input properties.
  */
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "documentModel") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.doc = chg.currentValue;
          this.docFileName = chg.currentValue.docDesc;
          this.types = chg.currentValue.formTypes ? chg.currentValue.formTypes.split(", ") : [];
        }
      }
      for (let propName in changes) {
        if (propName === "commentSeq") {
            let chg = changes[propName];
            if (chg.currentValue) {
              this.loadDocs(this.commentSeq);
            }
        }


    }
    }
  }
 
  downloadAttach(doc: Document) {
    this.downloading = this.exludeService.downloadPermitAttachment(doc.documentId).subscribe(
        data => {
           if (data) {
               this.downloadService.download(atob(data.reportPdf), data.docDesc, "application/pdf");
         }
        });
 }


 deleteDocument(doc:Document){
  this.showConfirmDeletePopup = true;
  this.btnDisable = true;
  this.busy = this.exludeService.deletePermitAttachment(doc.documentId).subscribe(
    data => {
       if (data) {
        console.log('Successfully deleted!');
        this.btnDisable = false;
     }
     this.loadDocs(this.commentSeq);
    });
 }

 enableLinkAuth(flag) {
  this.exludeService.setLinkAuthentication(flag);
}

}

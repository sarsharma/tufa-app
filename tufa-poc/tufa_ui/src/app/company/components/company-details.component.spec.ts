/*
@author : Deloitte
this is Component for company details
*/
import { ComponentFixture, async, inject, TestBed } from '@angular/core/testing';
import { CompanyDetailsPage } from './company-details.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core'; 
import { CommonModule }  from '@angular/common';
import { RouterModule, Routes, provideRoutes, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../shared/shared.module'
import { HttpModule } from '@angular/http';
import { AuthorizationHelper } from '../../authentication/util/authorization.helper.util';
import { CompanyService } from '../services/company.service';
import { ContactService } from '../services/contact.service';
import { HttpClient } from '../../../../node_modules/@angular/common/http';

declare var jQuery: any;
// let activatedRoute: ActivatedRouteStub;

describe('Company Details', () => {
  let comp:    CompanyDetailsPage;
  let fixture: ComponentFixture<CompanyDetailsPage>;

  // provide our implementations or mocks to the dependency injector
  beforeEach( async( () => {
    //activatedRoute = new ActivatedRouteStub();
    TestBed.configureTestingModule({
      imports: [ SharedModule, HttpModule, RouterTestingModule ],
      declarations: [CompanyDetailsPage],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        //{ provide: ActivatedRoute, useValue: activatedRoute },
        HttpClient, AuthorizationHelper, CompanyService, ContactService,
        provideRoutes([{path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTesting',
            pathMatch: 'full'}])
      ]
    }).compileComponents();
  }));


  beforeEach(() => {
    //activatedRoute.testParams = { id: 84 };
    fixture = TestBed.createComponent(CompanyDetailsPage);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the company details', function() {
    expect(comp).toBeTruthy();
  });

  it('should validate ui elements', function() {
      expect(comp).toBeTruthy();
    // validate title 

    // validate create permit ButtonsModule

    // validate permits table 

    // validate company address box


    // validate add contact ButtonsModule

  });

});

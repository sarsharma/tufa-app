/*
@author : Deloitte
this is Component for showing company details.
*/
import { Component, OnInit, OnDestroy, Input, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { CompanyService } from '../services/company.service';
import { ContactService } from '../services/contact.service';
import { Company } from '../models/company.model';
import { Permit } from '../../permit/models/permit.model';
import { Contact } from '../../model/contact.model';
import { Address } from '../../model/address.model';
import { Subscription } from 'rxjs';
import { ShareDataService } from '../../core/sharedata.service';
import { AddressService } from '../services/address.service';

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class CompanyDetailsPage
 * @implements {OnInit}
 * @implements {OnDestroy}
 */
@Component({
  selector: '[company-details]',
  templateUrl: './company-details.template.html',
  styleUrls: ['./company-details.style.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CompanyDetailsPage implements OnInit, OnDestroy {

  data: any[];
  permitSetData: any[];
  alerts: Array<Object>;
  contacts: any[];
  company: Company;
  companyStatus: any;
  error: any;
  sub: any;
  permits: any[];
  compid: any;
  contact = new Contact();
  busy: Subscription;
  busyPrimary: Subscription;
  showConfirmPopup: Boolean = false;

  docId: any;

  shareData: ShareDataService;
  // Recon params
  previousRoute: string;
  quarter: number;
  fiscalYear: number;
  createdDate: string;
  submitted: string;
  cigarsubmitted: string;
  months: string[];
  breadcrumb: string;
  qatabfrom: string;

  public showEditCompanyFlag: boolean = false;
  public showEditPermitFlag: boolean = false;
  public showEditAddressFlag: boolean = false;
  public showEditContactFlag: boolean = false;
  public showAddContactFlag: boolean = false;
  public showCompareDetails: boolean = true;
  
  public sortBy: string = "permitNum";
  public sortOrder: string[] = ["asc"];

  // public sortPermitResultsBy = "permitNum";
  // public sortPermitResultsOrder = "asc";

  public cmpymodel: Company;
  public deleteConfirmMsg: string = "Are you sure to delete?";
  public selectedContact: Contact;
  public primaryAddress: Address;
  public alternateAddress: Address;
  public selectedAddress: Address;
  public defCountry: string;
  public defState: string;
  public alternateAddressFlag : boolean = true;


  /**
   * Creates an instance of CompanyDetailsPage.
   *
   * @param {CompanyService} companyService
   * @param {ContactService} contactService
   * @param {Router} router
   * @param {ActivatedRoute} route
   *
   * @memberOf CompanyDetailsPage
   */
  constructor(
    private addressService: AddressService,
    private companyService: CompanyService,
    private contactService: ContactService,
    private router: Router,
    private route: ActivatedRoute,
    private _shareData: ShareDataService
  ) {
        this.shareData = _shareData;

    this.alerts = [
      {
        type: 'success',
        // tslint:disable-next-line:max-line-length
        msg: '<span class="fw-semi-bold">&nbsp;&nbsp;Success:</span><span>&nbsp;&nbsp;&nbsp;Marlboros Primary Address Information has been updated.</span>'
      }
    ];

  }

  /**
   *
   *
   *
   * @memberOf CompanyDetailsPage
   */
  ngOnDestroy() {
      if (this.busy) {
          this.busy.unsubscribe();
      }
      if(this.busyPrimary){
        this.busyPrimary.unsubscribe();
      }
  }

  /**
   *
   *
   *
   * @memberOf CompanyDetailsPage
   */
  addAlert(): void {
    this.alerts.push({ type: 'warning', msg: 'Another alert!' });
  };

  /**
   *
   *
   * @param {any} index
   *
   * @memberOf CompanyDetailsPage
   */
  closeAlert(index): void {
    this.alerts.splice(index, 1);
  };

  /**
   *
   *
   *
   * @memberOf CompanyDetailsPage
   */
  ngOnInit(): void {
    this.getParamsForReconNavigation();
    let searchInput = jQuery('#table-search-input, #search-permits');
    searchInput
      .focus((e) => {
        jQuery(e.target).closest('.input-group').addClass('focus');
      })
      .focusout((e) => {
        jQuery(e.target).closest('.input-group').removeClass('focus');
      });

    this.cmpyInfoLoad();

  }

  /*
      cmpyInfoRefresh() is called when information related to a company is successfully
      updated and the grid on the company details page has to be refreshed to get the
      new updates on the company info.

  */
  /**
   *
   *
   * @param {boolean} isCmpyEdited
   *
   * @memberOf CompanyDetailsPage
   */
  cmpyInfoRefresh(isCmpyEdited: boolean, mode: string ) {

    if (isCmpyEdited && this.cmpymodel.companyId) {
      this.busy = this.companyService.getCompany(this.cmpymodel.companyId)
        .subscribe(data => {
          if (data) {
            this.cmpymodel = <Company>data;
            this.data = (<Company>data).permitSet;
            this.permitSetData = (<Company>data).permitSet;
            this.contacts =(<Company>data).contactSet;
            this.primaryAddress = (<Company>data).primaryAddress;
            if (!this.primaryAddress) {
              this.primaryAddress = new Address();
            }
            this.primaryAddress.addressTypeCd = "PRIM";
            this.primaryAddress.companyId = this.cmpymodel.companyId;
            this.alternateAddress = (<Company>data).alternateAddress;
            if (!this.alternateAddress) {
              this.alternateAddress = new Address();
              this.alternateAddressFlag = true;
            }
            else
              this.alternateAddressFlag = false;
            this.alternateAddress.addressTypeCd = "ALTN";
            this.alternateAddress.companyId = this.cmpymodel.companyId;
          }
        });
    }

    switch (mode) {
        case 'edit-contact':
          this.showEditContactFlag = false;
          break;

        case 'add-contact':
          this.showAddContactFlag = false;
          break;

        case 'edit-address':
          this.showEditAddressFlag = false;
          break;

        case 'edit-permit':
          this.showEditPermitFlag = false;
          break;

        case 'edit-company':
          this.showEditCompanyFlag = false;
          break;

        default:
          break;
    }
  }

  /*
      cmpyInfoLoad() calls the backend server to get the company info by
      sending a companyId along with the request. The received  info is used to set the
      data in the data table or the grid on the company details page.
  */
  /**
   *
   *
   *
   * @memberOf CompanyDetailsPage
   */
  cmpyInfoLoad() {

    this.sub = this.route.params.subscribe(params => {
      if (params['id']) {
        this.compid = params['id'];
        this.busy = this.companyService.getCompany(this.compid)
          .subscribe(data => {
            this.callBack(data);
          });

      } else if (params['ein']) {
        let ein = params['ein'];
        ein = ein.replace(/[^\w\s]/gi, '');
        this.busy = this.companyService.getCompanyEIN(ein)
          .subscribe(data => {
            this.callBack(data);
          });
      } else {
        this.cmpymodel = new Company();
      }
    });

  }

  cmpyInfoLoadExclude() {
    setTimeout(() => {    
    this.sub = this.route.params.subscribe(params => {
      if (params['id']) {
        this.compid = params['id'];
        this.busy = this.companyService.getCompany(this.compid)
          .subscribe(data => {
            this.callBackPermitSet(data);
          });
      }
      else if (params['ein']) {
        let ein = params['ein'];
        ein = ein.replace(/[^\w\s]/gi, '');
        this.busy = this.companyService.getCompanyEIN(ein)
          .subscribe(data => {
            this.callBackPermitSet(data);
          });
      } else {
        this.cmpymodel = new Company();
      }
    });
  }, 500);
  }

  callBack(data: any) {
    if (data) {
      this.cmpymodel = data;
      this.data = data.permitSet;
      this.permitSetData = data.permitSet;
      this.contacts = data.contactSet;
      this.primaryAddress = data.primaryAddress;
      if (!this.primaryAddress) {
        this.primaryAddress = new Address();
      }
      this.primaryAddress.addressTypeCd = "PRIM";
      this.primaryAddress.companyId = this.cmpymodel.companyId;
      this.alternateAddress = data.alternateAddress;
      if (!this.alternateAddress) {
        this.alternateAddress = new Address();
        this.alternateAddressFlag = true;
      }
      else
        this.alternateAddressFlag = false;
      this.alternateAddress.addressTypeCd = "ALTN";
      this.alternateAddress.companyId = this.cmpymodel.companyId;
    }
  }

  callBackPermitSet(data: any) {
    if (data) {
      this.cmpymodel = data;
      this.data = data.permitSet;
     // this.permitSetData = data.permitSet;
      this.contacts = data.contactSet;
      this.primaryAddress = data.primaryAddress;
      if (!this.primaryAddress) {
        this.primaryAddress = new Address();
      }
      this.primaryAddress.addressTypeCd = "PRIM";
      this.primaryAddress.companyId = this.cmpymodel.companyId;
      this.alternateAddress = data.alternateAddress;
      if (!this.alternateAddress) {
        this.alternateAddress = new Address();
        this.alternateAddressFlag = true;
      }
      else
        this.alternateAddressFlag = false;
      this.alternateAddress.addressTypeCd = "ALTN";
      this.alternateAddress.companyId = this.cmpymodel.companyId;
    }
  }
  
  /**
   *
   *
   *
   * @memberOf CompanyDetailsPage
   */
  ok(): void {
    jQuery('#edit-contact').modal('hide');
  }

  /**
   *
   *
   * @param {boolean} added
   *
   * @memberOf CompanyDetailsPage
   */
  onAddContact(added: boolean) {
    let contact = new Contact();
    this.contacts.push(contact);
  }

  /**
   * Delete a contact
   *
   * @param {Contact} contact
   *
   * @memberOf CompanyDetailsPage
   */
  deleteContact(contact: Contact) {
    contact.company = this.cmpymodel;
    this.contactService.deleteContact(contact).subscribe(data => {
      if (data) {
        this.cmpyInfoRefresh(true, 'delete-contact');
        this.showConfirmPopup = false;
        jQuery('#confirm-popup').modal('hide');
      }
    });
  }

  /**
   *
   *
   * @param {Contact} company
   *
   * @memberOf CompanyDetailsPage
   */
  setCompany(company: Company) {
    this.cmpymodel = Object.assign({}, company);
    this.showEditCompanyFlag = true;
  }

  /**
   *
   *
   * @param {Contact} contact
   *
   * @memberOf CompanyDetailsPage
   */
  setSelectedContact(contact: Contact, mode: string) {
    this.selectedContact = Object.assign({}, contact);
    if (mode === 'edit') {
      this.showEditContactFlag = true;
    } else if (mode === 'delete') {
      this.showConfirmPopup = true;
    }
  }

  onclicktoAddContact() {
    this.showAddContactFlag = true;
  }

  /**
   *
   *
   * @param {Address} address
   *
   * @memberOf CompanyDetailsPage
   */
  setSelectedAddress(address: Address) {
    if (!address.countryCd) {address.countryNm = 'UNITED STATES'; }
    if (!address.state) {address.state = ''; }
    this.selectedAddress = Object.assign({}, address);
    this.showEditAddressFlag = true;
  }

  setAsPrimaryAddress(address: Address){
    address.countryCd = address.countryNm ? address.countryNm : 'UNITED STATES';
    address.state = address.countryCd === 'UNITED STATES' ? address.state : '';
    address.province = address.countryCd === 'UNITED STATES' ? '' : address.province;
    address.addressTypeCd = 'PRIM';
    
    if(this.primaryAddress.countryCd !== undefined){
      this.primaryAddress.countryCd = this.primaryAddress.countryNm ? this.primaryAddress.countryNm : 'UNITED STATES';
      this.primaryAddress.state = this.primaryAddress.countryCd === 'UNITED STATES' ? this.primaryAddress.state : '';
      this.primaryAddress.province = this.primaryAddress.countryCd === 'UNITED STATES' ? '' : this.primaryAddress.province;
      this.primaryAddress.addressTypeCd = 'ALTN';
    }
    this.addressService.saveOrUpdateAddress(address).subscribe(data => {
      if (data) {
        if(this.primaryAddress.countryCd !== undefined){
          this.addressService.saveOrUpdateAddress(this.primaryAddress).subscribe(data => {
            if (data) {
              this.cmpyInfoRefresh(true, address.companyId);
            }
          });
        }else{
          this.addressService.deleteAddress(this.primaryAddress, 'ALTN').subscribe(data => {
              this.cmpyInfoRefresh(true, address.companyId);
          });
        }        
      }
    });
  }

  setAsPrimaryContact(contact: Contact){
    contact.company = this.cmpymodel;
    this.contacts[0].primaryContactSequence = null;
    this.contacts[0].company = this.cmpymodel;
    contact.primaryContactSequence = 1;
    this.contactService.setAsPrimaryContact(false, this.contacts[0]).subscribe(data => {
      if (data) {
        this.contactService.setAsPrimaryContact(true, contact).subscribe(data => {
          if (data) {
           this.cmpyInfoRefresh(true, contact.companyId.toString());
          }
        });
      }
    });     
  }

  onClicktoAddPermit() {
    this.showEditPermitFlag = true;
  }

  /**
   *
   *
   * @param {Company} company
   *
   * @memberOf CompanyDetailsPage
   */
  gotoCompany(company: Company) {
    this.router.navigate(["/app/company/details", company.companyId]);
  }

  toQuarterlyAssessment() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "quarter": this.quarter,
        "fiscalyear": this.fiscalYear,
        "createdDt": this.createdDate,
        "submitted": this.submitted,
        "months": this.months
      }
    };
    this.setParamsForReconNavigation();
    this.router.navigate(["/app/assessments/quarterly"]);
  }

  toAssessments() {
    this.setParamsForReconNavigation();
    this.router.navigate(["/app/assessments"]);
  }

  toCigarAssessment() {
    this.setParamsForReconNavigation();
    this.router.navigate(["/app/assessments/cigars"]);
  }

  toAnnualTrueup(){
    this.setParamsForReconNavigation();
    this.router.navigate(["/app/assessments/annual"]);
  }

  toCompareDetails() {
   if(this.shareData.permitType === 'Importer'){
        if(this.shareData.originalEIN){
          this.shareData.originalEIN = this.cmpymodel.einNumber;
        }else{
          this.shareData.ein = this.cmpymodel.einNumber;
        }
      }
      if (this.shareData.detailsSrc.indexOf('noncigar') != -1)
          this.router.navigate(["/app/assessments/comparenoncigar"]);
      else if (this.shareData.detailsSrc.indexOf('cigar') != -1)
          this.router.navigate(["/app/assessments/comparecigar"]);
  }

  toCompanyTracking() {
    this.router.navigate(["/app/company/tracking"]);
  }

  toFileMgmt() {
    this.setParamsForReconNavigation();
    this.router.navigate(["app/permit-mgmt/address-mgmt", {docId: this.docId}]);
  }

  setParamsForReconNavigation(){
        let filetab = this.shareData.filemgmttab;
        this.shareData.reset();
        this.shareData.filemgmttab = filetab;
        this.shareData.previousRoute = this.previousRoute;
        this.shareData.breadcrumb = this.breadcrumb;
        this.shareData.qatabfrom = this.qatabfrom;
        this.shareData.quarter = this.quarter;
        this.shareData.fiscalYear = this.fiscalYear;
        this.shareData.createdDate = this.createdDate;
        this.shareData.submitted = this.submitted;
        this.shareData.cigarsubmitted = this.cigarsubmitted;
        this.shareData.months = this.months;
        this.shareData.docId = this.docId;
        
    }

    getParamsForReconNavigation(){
        this.previousRoute = this.shareData.previousRoute;
        this.breadcrumb = this.shareData.breadcrumb;
        this.qatabfrom = this.shareData.qatabfrom;
        this.quarter = this.shareData.quarter;
        this.fiscalYear = this.shareData.fiscalYear;
        this.submitted = this.shareData.submitted;
        this.cigarsubmitted = this.shareData.cigarsubmitted;
        this.createdDate = this.shareData.createdDate;
        this.months = this.shareData.months;
        this.docId = this.shareData.docId;
       
    }

    //sort by last name
    sortContactslname(setData: any[]): any[] {
      return setData.slice(0, 1).concat( setData.slice(1, setData.length).sort(function(a, b){
        if (a.lastNm.toLowerCase() < b.lastNm.toLowerCase()) return -1;
        if (a.lastNm.toLowerCase() > b.lastNm.toLowerCase()) return 1;
        return 0;
      }));
    }

    activeDt = (permit:any) => {
        let actveDt = permit.issueDt.split("/")
        return actveDt[2]+""+actveDt[0]+""+actveDt[1];
    }

    issueDt = (permit:any) => {
        let actveDt = permit.issueDt.split("/")
        return actveDt[2]+""+actveDt[0]+""+actveDt[1];
    }

    setFlag(event){
      this.cmpyInfoLoadExclude();
    }
}

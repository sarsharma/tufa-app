import { Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { LocalStorageService } from "ngx-webstorage";
import { Router } from "../../../../node_modules/@angular/router";
import { Subscription } from "../../../../node_modules/rxjs";
import { ShareDataService } from "../../core/sharedata.service";
import { CompanySearchCriteria } from "../../permit/models/company-search-criteria.model";
import { PaginationAttributes } from "../../shared/datatable/pagination.attributes.model";
import { Company } from "../models/company.model";
import { CompanyService } from "../services/company.service";

@Component({
    selector: '[company-tracking]',
    templateUrl: './company-tracking.template.html',
    styleUrls: ['./search-criteria.style.scss', './company-tracking.style.scss'],
    providers: [CompanyService],
    encapsulation: ViewEncapsulation.None
})

/**
 * Table displays permit report results
 *
 * @export
 * @class SearchPermitResultsComponent
 * @implements {OnInit}
 */
export class CompanyTrackingComponent implements OnInit, OnDestroy {
    pgAttrs: PaginationAttributes;
    router: Router;
    showErrorFlag: boolean;
    rightalignClass = "floatright padright";
    shareData: ShareDataService;
    companyResults: Company[];
    busy: Subscription;
    busyExport: Subscription;
    criteria: CompanySearchCriteria = new CompanySearchCriteria();
    sortBy: string = "legalName";
    sortOrder: string[] = ["asc"];
    // sortResultsOrder: string = 'asc';
    selectAll: boolean = false;
    createdDtFilter: Date;
    welcomeLetterSentDtFilter: Date;
    newCompaniesExportdata: any;
    modPrefix: string = 'CT'

    columnMapping = { legalName: 1, ein: 2, addedDt: 3, emailFlag: 4, addressFlag: 5, welcomeLetterFlag: 6 };
    companyresults: any; public columns: Array<any> = [
        { name: 'legalName', type: 'input', filtering: { filterString: '', columnName: 'legalName', placeholder: 'Enter Company Name' } },
        { name: 'ein', type: 'input', filtering: { filterString: '', columnName: 'ein', placeholder: 'Enter EIN No' } },
        { name: 'addedDt', type: 'date', filtering: { filterString: '', columnName: 'addedDt', placeholder: 'Enter Date Added' } },
        { name: 'welcomeLetterSentDt', type: 'date', filtering: { filterString: '', columnName: 'welcomeLetterSentDt', placeholder: 'Enter Date Welcome Letter Sent' } },
        {
            name: 'emailFlag', type: 'select', defaultvalue: '',
            options: [
                { name: '', value: '' },
                { name: 'Yes', value: 'Y' },
                { name: 'No', value: 'N' },
            ],
            filtering: { filterString: '', columnName: 'emailFlag', placeholder: 'Filter by Email Flag' }
        },
        {
            name: 'addressFlag', type: 'select', defaultvalue: '',
            options: [
                { name: '', value: '' },
                { name: 'Yes', value: 'Y' },
                { name: 'No', value: 'N' },
            ],
            filtering: { filterString: '', columnName: 'addressFlag', placeholder: 'Filter by Address Flag' }
        },
        {
            name: 'welcomeLetterFlag', type: 'select', defaultvalue: 'N',
            options: [
                { name: '', value: '' },
                { name: 'Yes', value: 'Y' },
                { name: 'No', value: 'N' },
            ],
            filtering: { filterString: '', columnName: 'welcomeLetterFlag', placeholder: 'Filter by Welcom Letter Flag' }
        }
    ];

    constructor(
        router: Router,
        private storage: LocalStorageService,
        private companyservice: CompanyService,
        private _shareData: ShareDataService) {

        this.pgAttrs = new PaginationAttributes(1, 25, null, null, null, null);

        this.criteria.welcomeLetterFlagFilter = 'N';
        this.router = router;
        this.shareData = _shareData;
    }

    ngOnDestroy(): void {
        if (this.busy) {
            this.busy.unsubscribe();
        }

        if (this.busyExport) {
            this.busyExport.unsubscribe()
        }
    }
    ngOnInit(): void {
        this.getCompanyResults();
    }

    public onPageChange(event) {
        this.pgAttrs.pageSize = event.rowsOnPage;
        this.pgAttrs.activePage = event.activePage;
        this.getCompanyResults();
    }

    public onSortOrder(event) {
        this.pgAttrs.sortOrder = event;
        this.pgAttrs.sortBy = [];
        this.pgAttrs.sortBy.push(this.columnMapping[this.sortBy]);
        this.getCompanyResults();
    }

    public onChangeTable(filterinfo): void {
        if (filterinfo) {
            if (filterinfo.columnName === "legalName") {
                this.criteria.companyNameFilter = filterinfo.filterString;
                this.criteria.companyNameFilter = this.criteria.companyNameFilter.trim();
            }
            if (filterinfo.columnName === "ein") {
                this.criteria.companyEINFilter = filterinfo.filterString;
                this.criteria.companyEINFilter = this.criteria.companyEINFilter.trim();
            }
            if (filterinfo.refresh) {
                this.getCompanyResults();
            }
        }
    }

    filterSelectionChanged(event, filterColumnName) {


        if (filterColumnName === "emailFlag") {
            this.criteria.emailAddressFlagFilter = event;
            this.getCompanyResults();
        }

        if (filterColumnName === "addressFlag") {
            this.criteria.physicalAddressFlagFilter = event;
            this.getCompanyResults();
        }

        if (filterColumnName === "welcomeLetterFlag") {
            this.criteria.welcomeLetterFlagFilter = event;
            this.getCompanyResults();
        }
    }

    filterDateChanged(filterColumnName) {
        if (filterColumnName === "addedDt") {
            this.criteria.companyCreatedDtFilter = this.createdDtFilter;
            this.getCompanyResults();
        }

        if (filterColumnName === "welcomeLetterSentDt") {
            this.criteria.welcomeLetterSentDtFilter = this.welcomeLetterSentDtFilter;
            this.getCompanyResults();
        }
    }

    getCompanyResults() {
        if (this.busy) {
            this.busy.unsubscribe();
        }

        this.busy = this.companyservice.getCompanyTrackingInformation(this.criteria, this.pgAttrs).subscribe(
            data => {
                if (data) {
                    this.companyResults = data.results;
                    this.pgAttrs.itemsTotal = data.itemsTotal;
                }
            }
        );
    }

    updateCompanyResults(selectedCompanies: Company[]) {
        if (selectedCompanies && selectedCompanies.length > 0) {
            if (this.busy) {
                this.busy.unsubscribe();
            }

            this.busy = this.companyservice.updateComapnyTrackingInformation(selectedCompanies).subscribe(
                data => {
                    if (data) {
                        this.getCompanyResults();
                    }
                }
            );
        }
    }

    selectAllCompanies() {
        this.selectAll = !this.selectAll;
        if (this.selectAll) {
            for (let comp of this.companyResults) {
                comp.selected = true;
            }
        } else {
            for (let comp of this.companyResults) {
                comp.selected = false;
            }
        }
    }

    setMultiEmailAddressFlag() {
        let selectedCompanies: Company[] = this.getSelectedCompanies();
        for (let comp of selectedCompanies) {
            comp.emailAddressFlag = 'Y'
        }
        this.updateCompanyResults(selectedCompanies);
    }

    setMultiPysicalAddressFlag() {
        let selectedCompanies: Company[] = this.getSelectedCompanies();
        for (let comp of selectedCompanies) {
            comp.physicalAddressFlag = 'Y'
        }
        this.updateCompanyResults(selectedCompanies);
    }

    setMultiWelcomeLetterFlag() {
        let selectedCompanies: Company[] = this.getSelectedCompanies();
        for (let comp of selectedCompanies) {
            comp.welcomeLetterFlag = 'Y'
        }
        this.updateCompanyResults(selectedCompanies);
    }

    setSingleEmailAddressFlag(company: Company) {
        company.emailAddressFlag = this.inverseFlag(company.emailAddressFlag);
        this.updateCompanyResults([company]);
    }

    setSinglePhysicalAddressFlag(company: Company) {
        company.physicalAddressFlag = this.inverseFlag(company.physicalAddressFlag);
        this.updateCompanyResults([company]);
    }

    setSingleWelcomeLetterFlag(company: Company) {
        company.welcomeLetterFlag = this.inverseFlag(company.welcomeLetterFlag);
        this.updateCompanyResults([company]);
    }

    inverseFlag(flag: string): string {
        if ('Y' === flag) {
            flag = 'N';
        } else {
            flag = 'Y';
        }
        return flag;
    }

    getSelectedCompanies(): Company[] {
        let selectedCompanies: Company[] = [];
        for (let comp of this.companyResults) {
            if (comp.selected) {
                selectedCompanies.push(comp);
            }
        }
        return selectedCompanies;
    }

    toCompanyDetails(company: Company) {
        this.shareData.breadcrumb = 'company-tracking';
        this.router.navigate(['/app/company/details', company.companyId]);
    }

    exportNewCompaniesDetails() {




        this.busyExport = this.companyservice.exportNewCompaniesData().subscribe(
            data => {
                if (data) {
                    this.newCompaniesExportdata = data;
                    this.download(this.newCompaniesExportdata.csv, this.newCompaniesExportdata.title, 'csv/text');
                }
            });
    }

    download(data: any, strFileName: any, strMimeType: any) {

        let self: any = window, // this script is only for browsers anyway...
            defaultMime = 'application/octet-stream', // this default mime also triggers iframe downloads
            mimeType = strMimeType || defaultMime,
            payload = data,
            url = !strFileName && !strMimeType && payload,
            anchor = document.createElement('a'),
            toString = function (a) { return String(a); },
            myBlob = (self.Blob || self.MozBlob || self.WebKitBlob || toString),
            fileName = strFileName || 'download',
            blob,
            reader;
        myBlob = myBlob.call ? myBlob.bind(self) : Blob;

        if (String(this) === 'true') { // reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
            payload = [payload, mimeType];
            mimeType = payload[0];
            payload = payload[1];
        }

        // go ahead and download dataURLs right away
        if (/^data\:[\w+\-]+\/[\w+\-\.]+[,;]/.test(payload)) {

            if (payload.length > (1024 * 1024 * 1.999) && myBlob !== toString) {
                payload = dataUrlToBlob(payload);
                mimeType = payload.type || defaultMime;
            } else {
                return navigator.msSaveBlob ?  // IE10 can't do a[download], only Blobs:
                    navigator.msSaveBlob(dataUrlToBlob(payload), fileName) :
                    saver(payload, false); // everyone else can save dataURLs un-processed
            }

        } else {// not data url, is it a string with special needs?
            if (/([\x80-\xff])/.test(payload)) {
                let i = 0, tempUiArr = new Uint8Array(payload.length), mx = tempUiArr.length;
                for (i; i < mx; ++i) tempUiArr[i] = payload.charCodeAt(i);
                payload = new myBlob([tempUiArr], { type: mimeType });
            }
        }
        blob = payload instanceof myBlob ?
            payload :
            new myBlob([payload], { type: mimeType });


        function dataUrlToBlob(strUrl) {
            let parts = strUrl.split(/[:;,]/),
                type = parts[1],
                decoder = parts[2] === 'base64' ? atob : decodeURIComponent,
                binData = decoder(parts.pop()),
                mx = binData.length,
                i = 0,
                uiArr = new Uint8Array(mx);

            for (i; i < mx; ++i) uiArr[i] = binData.charCodeAt(i);

            return new myBlob([uiArr], { type: type });
        }

        function saver(url: any, winMode: any) {

            if ('download' in anchor) { // html5 A[download]
                anchor.href = url;
                anchor.setAttribute('download', fileName);
                anchor.className = 'download-js-link';
                anchor.innerHTML = 'downloading...';
                anchor.style.display = 'none';
                document.body.appendChild(anchor);
                setTimeout(function () {
                    anchor.click();
                    document.body.removeChild(anchor);
                    if (winMode === true) { setTimeout(function () { self.URL.revokeObjectURL(anchor.href); }, 250); }
                }, 66);
                return true;
            }

            // handle non-a[download] safari as best we can:
            if (/(Version)\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//.test(navigator.userAgent)) {
                if (/^data:/.test(url)) url = 'data:' + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
                if (!window.open(url)) { // popup blocked, offer direct download:
                    if (confirm('Displaying New Document\n\nUse Save As... to download, then click back to return to this page.')) {
                        location.href = url;
                    }
                }
                return true;
            }

            // do iframe dataURL download (old ch+FF):
            let f = document.createElement('iframe');
            document.body.appendChild(f);

            if (!winMode && /^data:/.test(url)) { // force a mime that will download:
                url = 'data:' + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
            }
            f.src = url;
            setTimeout(function () { document.body.removeChild(f); }, 333);

        } // end saver

        if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or URL)
            return navigator.msSaveBlob(blob, fileName);
        }

        if (self.URL) { // simple fast and modern way using Blob and URL:
            saver(self.URL.createObjectURL(blob), true);
        } else {
            // handle non-Blob()+non-URL browsers:
            if (typeof blob === 'string' || blob.constructor === toString) {
                try {
                    return saver('data:' + mimeType + ';base64,' + self.btoa(blob), false);
                } catch (y) {
                    return saver('data:' + mimeType + ',' + encodeURIComponent(blob), false);
                }
            }

            // Blob but not URL support:
            reader = new FileReader();
            reader.onload = function (e) {
                saver(this.result, false);
            };
            reader.readAsDataURL(blob);
        }
        return true;
    };



}
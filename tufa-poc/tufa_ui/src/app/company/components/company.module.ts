import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

import { CompanyService } from '../../company/services/company.service';
import { ContactService } from '../../company/services/contact.service';
import { AddressService } from '../../company/services/address.service';

// Page that belonged to the module
import { CreateCompanyPage } from './create-company.component';
import { SearchCriteriaPage } from './search-criteria.component';
import { EditCompanyPopup } from './edit-company.component';
import { CompanyDetailsPage } from './company-details.component';
import { EditContactPopup } from './edit-contact.component';
import { EditAddressPopup } from './edit-address.component';
import { AddContactPopup } from './add-contact.component';
import { EditPermitPopup } from '../../permit/components/edit-permit.component';
import { SearchPermitResultsComponent } from './search-permit-results.component';
import { SearchCompanyResultsComponent } from './search-company-results.component';

import { SearchCompanyPipe } from '../../company/util/search-company-pipe';
import { PermitsCSVPipe } from '../../company/util/permits-csv-format.pipe';
import { PermitReportsDataFilterPipe } from '../../company/util/permit-report-filter.pipe';
import { CompanyTrackingComponent } from './company-tracking.component';
import { AnnualTrueUpComparisonEventService } from './../../recon/annual/services/at-compare.event.service';
import { PermitExcludeMarketShareComponent } from './permit-marketshare-exclude.component';
import { CommentGridCompanyComponent } from './comments/comments-grid-company.component';
import { EditDocumentCommentsPopup } from './comments/edit-document-comments.component';

export const routes = [
  { path: '', component: SearchCriteriaPage, pathMatch: 'full' },
  { path: 'create', component: CreateCompanyPage},
  { path: 'search', component: SearchCriteriaPage},
  { path: 'details/:id', component: CompanyDetailsPage},
  { path: 'details', component: CompanyDetailsPage},
  { path: 'tracking', component: CompanyTrackingComponent}
];

@NgModule({
    imports: [ CommonModule, SharedModule, RouterModule.forChild(routes) ],
    declarations: [ PermitExcludeMarketShareComponent,CreateCompanyPage, SearchCriteriaPage, SearchCompanyPipe, CompanyDetailsPage,
        EditContactPopup, EditAddressPopup, EditPermitPopup, AddContactPopup, EditCompanyPopup, SearchPermitResultsComponent,
        SearchCompanyResultsComponent, PermitsCSVPipe, PermitReportsDataFilterPipe, CompanyTrackingComponent, CommentGridCompanyComponent,EditDocumentCommentsPopup],
    providers: [CompanyService, ContactService, AddressService, AnnualTrueUpComparisonEventService]
})

export class CompanyModule {
  static routes = routes;
}

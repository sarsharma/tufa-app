import { ComponentFixture, async, inject, tick, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { provideRoutes, RouterLink } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { Company } from '../models/company.model';
import { HttpModule } from '@angular/http';

import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AuthorizationHelper } from '../../authentication/util/authorization.helper.util';
import { CompanyService } from '../../company/services/company.service';
import '../../shared/datetime/vendor/bootstrap-datepicker.min.js';
import '../../shared/datetime/vendor/bootstrap-timepicker.min.js';

// Page that belonged to the module
import { CreateCompanyPage } from './create-company.component';
import { HttpClient } from '../../../../node_modules/@angular/common/http';

var jQuery: any;

describe('Create Company', () => {
  let comp:    CreateCompanyPage;
  let fixture: ComponentFixture<CreateCompanyPage>;
  let de:      DebugElement;
  let el:      HTMLElement;

  // provide our implementations or mocks to the dependency injector
  beforeEach( async(() => {
    TestBed.configureTestingModule({
      imports: [ CommonModule, SharedModule, HttpModule, RouterTestingModule],
      declarations: [ CreateCompanyPage ],
      providers: [ CompanyService, HttpClient, AuthorizationHelper,
        provideRoutes([{path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTesting',
          pathMatch: 'full'}])
      ]
    }).compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(CreateCompanyPage);
    comp = fixture.componentInstance;

    // fixture.componentInstance.datepickerOpts = {
    //   autoclose: true,
    //   todayBtn: 'linked',
    //   todayHighlight: true,
    //   assumeNearbyYear: true
    // };

    fixture.detectChanges();

  });

  it('should create component', () => {
    expect(comp).toBeTruthy();
    // Verify if control exists
    expect(fixture.debugElement.nativeElement.querySelector('#company-name') === null).toBe(false);
    expect(fixture.debugElement.nativeElement.querySelector('#ein-number') === null).toBe(false);
    expect(fixture.debugElement.nativeElement.querySelector('#ttb-permit') === null).toBe(false);

  });

  it('should construct company service', inject([CompanyService], (service) => {
    expect(service).toBeDefined();
  }));

  it('check validation', () => {
    // enter bad data and see if component throws validation messages
    let deFirstname = fixture.debugElement.query(By.css('#company-name'));
    let elFirstname = deFirstname.nativeElement;

    elFirstname.value = 'TEST COMPANY';
    elFirstname.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    expect(comp.company.legalName).toEqual('TEST COMPANY');
    // comp.company.einNumber = '12345';
    // fixture.detectChanges();

    // let deEIN: DebugElement = fixture.debugElement.query(By.css('#ein-number'));
    // expect(deEIN.nativeElement.value).toEqual('12345');

    // fixture.detectChanges();
    // fixture.whenStable().then(() => {
    //   comp.company.einNumber = '1234567890';
    //   comp.PermitNumber = 'AB-CD-12345';
    //   comp.permitType = '';
    //   comp.company.activeDate = '12/28/2016';
    // });
  });

  it('should behave when create company is filled', () => {
      // let input = fixture.nativeElement.querySelector('#company-name');
      // input.value = "John";

      // let evt = document.createEvent('Event');
      // evt.initEvent('input', true, false);
      // input.dispatchEvent(evt);
      // tick(50);

      // expect(fixture.componentInstance.company.legalName).toEqual('John');      


    // // Getting the elements
    // let compiled = fixture.debugElement.nativeElement;
    // let username = compiled.querySelector('input[type="text"]');

    // fixture.detectChanges();

     // expect(companyname.text).toEqual('Dummy');    

    // // Change value
    // username.value = 'my username';

    // // dispatch input event
    // dispatchEvent(username, 'input');

    // compiled.querySelector('button').click();
    // expect(mockOAuthService.login).toHaveBeenCalled();

      // fixture.debugElement.query(By.css('ttt-board')).triggerEventHandler('endgame', '_');
  });
    
});

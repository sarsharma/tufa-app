/*
@author : Deloitte

this is Component for creating company.
*/

import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Company } from '../models/company.model';
import { CompanyService } from '../services/company.service';
import { Subscription } from 'rxjs';

declare var jQuery: any;

@Component({
  selector: 'create-company',
  templateUrl: './create-company.template.html',
  styleUrls: ['./create-company.style.scss'],
  encapsulation: ViewEncapsulation.None
})

/**
 *
 *
 * @export
 * @class CreateCompanyPage
 * @implements {OnInit}
 */
export class CreateCompanyPage implements OnInit, OnDestroy{
  errorMessage: string;
  router: Router;
  company: Company = new Company();
  showErrorFlag: boolean;
  alerts: Array<Object>;
  permitNumber: string = '';  // use local variable to bind instead of model company.permitNumber
  permitType: string;
  activedateid: string = "active-date";
  busy: Subscription;
  maxDate: Date = new Date();

  public mask = [/[A-Za-z]/, /[A-Za-z]/, '-', /[A-Za-z]/, /[A-Za-z]/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];
  public einmask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];

  // initialize the datetime control.
  datepickerOpts = {
    autoclose: true,
    todayBtn: 'linked',
    todayHighlight: true,
    assumeNearbyYear: true,
    endDate: new Date(),
    requiredErrorMsg: "Error: TUFA Active Date is required",
    title: 'Enter Active Date in MM/DD/YYYY format',
    icon: 'fa fa-th'
  };

  permitTypeList = ['Select Type','Manufacturer','Importer'];

  /**
   * Creates an instance of CreateCompanyPage.
   *
   * @param {Router} router
   * @param {CompanyService} _companyService
   *
   * @memberOf CreateCompanyPage
   */
  constructor(router: Router,
    private _companyService: CompanyService) {
    this.router = router;

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages When Create Company Fails. e.g Duplicate EIN'
      }
    ];

    this.showErrorFlag = false;
  }

  /**
   *
   *
   *
   * @memberOf CreateCompanyPage
   */
  ngOnInit(): void {
    this.showErrorFlag = false;
    this.permitType = "Select Type";
  }

  ngOnDestroy() {
      if (this.busy) {
          this.busy.unsubscribe();
      }
  }

  /**
   * createCompany() calls backend server to create a new company.
   * After a successful return the flow navigates to company details page where
   * newly created company and permit is shown on the grid.
   *
   * @param {Company} company
   *
   * @memberOf CreateCompanyPage
   */
  createCompany(company: Company) {
    // Since the button is not type submit button, we validate using jquery
    // Get finer control on what we want to do
    this.showErrorFlag = false;
    jQuery('#createcompany-form').parsley().validate();

    // unmask permit number before proceding ahead
    // transforming the local bound variable to the model. if we modify the model directly then
    // RegEx pattern validation fails on the UI side.
    let permitNum: string = this.permitNumber;
    company.permitNumber = permitNum = '' ? '' : permitNum.replace(/-/g , "");
    company.permitTypeCd = this.permitType === "Manufacturer" ? "MANU" : "IMPT";

    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#createcompany-form').parsley().isValid()) {
      this.busy = this._companyService.createCompany(company).subscribe(data => {
        if (data) {
          this.company = data;
          this.router.navigate(["/app/company/details", this.company.companyId]);
        }
      }
      , error => {
        if(error){
          jQuery('#ein-number').parsley().removeError('EIN-Exists');
          jQuery('#ttb-permit').parsley().removeError('TTB-Exists');
          if (error.indexOf('EIN') > - 1 ) {
            error = "Error: " + error;
              jQuery('#ein-number').parsley().addError('EIN-Exists',  { message: error } );
            }else if (error.indexOf('TTB') > - 1 ) {
              error = "Error: " + error;
              jQuery('#ttb-permit').parsley().addError('TTB-Exists', { message: error } );
            }
        }
      }
    );
    }
  }
  selectType() {
    if (/^tp/i.test(this.permitNumber)) this.permitType = 'Manufacturer';
    else if (/^\w{2}.ti/i.test(this.permitNumber)) this.permitType = 'Importer';
    else this.permitType = 'Select Type';
  }
}

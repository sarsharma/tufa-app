/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, Input, OnInit, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Address } from '../../model/address.model';
import { Company } from '../models/company.model';
import { AddressService } from '../services/address.service';

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class EditAddressPopup
 */
@Component({
  selector: '[edit-address]',
  templateUrl: './edit-address.template.html'
})

export class EditAddressPopup {
  @Input() addressModel: Address;
  @Input() companyModel: Company;
  @Output() onCompanyEdit = new EventEmitter<boolean>();
  countryList: any;
  stateList: any;
  showErrorFlag: boolean;
  alerts: Array<Object>;

  address = new Address();
  /**
   *
   *
   *
   * @memberOf EditAddressPopup
   */
  ngOnInit() {
    this.addressService.getCountryNames().subscribe(data => {
      if (data)
        this.countryList = data;
    });
    this.addressService.getStateNames().subscribe(data => {
      if (data)
        this.stateList = [''].concat(<string>data);
    });
  }

  /**
   * Creates an instance of EditAddressPopup.
   *
   * @param {AddressService} addressService
   *
   * @memberOf EditAddressPopup
   */
  constructor(private addressService: AddressService) {
    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];

    this.showErrorFlag = false;
  }

  // save/update a address
  /**
   *
   *
   * @param {Address} address
   * @param {string} selectedCountry
   *
   * @memberOf EditAddressPopup
   */
  updateAddress(address: Address) {
    jQuery('#editaddressform').parsley().validate();
    address.countryCd = address.countryNm ? address.countryNm : 'UNITED STATES';
    address.state = address.countryCd === 'UNITED STATES' ? address.state : '';
    address.province = address.countryCd === 'UNITED STATES' ? '' : address.province;

    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#editaddressform').parsley().isValid()) {

      this.addressService.saveOrUpdateAddress(address).subscribe(data => {
        if (data) {
          this.reset(true);
        }
      },
        error => {
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({ type: 'warning', msg: error });
        });
    }

  }

  /**
   *
   *
   *
   * @memberOf EditAddressPopup
   */
  reset(flag) {
    jQuery('#edit-address').modal('hide');
    this.showErrorFlag = false;
    this.onCompanyEdit.emit(flag);
  }

  /*
    ngOnChanges() used to capture changes on the input properties.
*/
  /**
   *
   *
   * @param {SimpleChanges} changes
   *
   * @memberOf EditAddressPopup
   */
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "addressModel") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.address = chg.currentValue;
        }
      }
    }
  }

}

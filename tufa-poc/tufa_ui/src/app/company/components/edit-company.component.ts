/*
@author : Deloitte

this is Component for editing company.

*/

import { Component, Input, Output, EventEmitter, OnInit, SimpleChange } from '@angular/core';
import { Company } from '../models/company.model';
import { CompanyService } from '../services/company.service';
import { CreateCompanyPage } from './create-company.component';
import { Router } from '@angular/router';

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class EditCompanyPopup
 */
@Component({
  selector: '[edit-company]',
  templateUrl: './edit-company.template.html'
})
export class EditCompanyPopup {

  showErrorFlag: boolean;
  alerts: Array<Object>;

  company: Company = new Company();
  public einmask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
  /*
    input field model used to pass information from parent company details page to
    edit company popup modal.
  */
  @Input() model: Company;

  /*
     output field onCompanyEdit used to raise an event from child edit company popup
     modal to parent page company details page. The event carries information as required
     from child to listening parent page.
  */
  @Output() onCompanyEdit = new EventEmitter<boolean>();

  /**
   * Creates an instance of EditCompanyPopup.
   *
   * @param {Router} _router
   * @param {CompanyService} _companyService
   *
   * @memberOf EditCompanyPopup
   */
  constructor(
    private _router: Router,
    private _companyService: CompanyService) {
      this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages When Create Company Fails. e.g Duplicate EIN'
      }
    ];

    this.showErrorFlag = false;
  }

  /**
   * ngOnChanges() used to capture changes on the input properties.
   *
   * @param {{ [propertyName: string]: SimpleChange }} changes
   *
   * @memberOf EditCompanyPopup
   */
  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    for (let propName in changes) {
      if (propName === "model") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.company.companyId = chg.currentValue.companyId;
          this.company.legalName = chg.currentValue.legalName;
          this.company.einNumber = chg.currentValue.einNumber;
          this.company.companyStatus = chg.currentValue.companyStatus;
          this.company.createdDt = chg.currentValue.createdDt;
          this.company.createdBy = chg.currentValue.createdBy;
          this.company.companyComments = chg.currentValue.companyComments;
        }
      }

    }
  }

  /**
   * updateCompany() calls backend server to make updates to the company information.
   * After a successful return an event is raised to the parent company details page
   * indicative of a successful company update and refreshing of the grid on the company
   * details page.
   *
   * @param {Company} company
   *
   * @memberOf EditCompanyPopup
   */
  updateCompany(company: Company) {
    this.showErrorFlag = false;

    // Since the button is not type submit button, we validate using jquery
    // Get finer control on what we want to do
    jQuery('#editcompany-form').parsley().validate();

    if (jQuery('#editcompany-form').parsley().isValid()) {
      this._companyService.updateCompany(company).subscribe(data => {
        if (data) {
          this.company = data;
          this.reset(true);
         }
      }, error => {
        if(error) {
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({type: 'warning', msg: error});
        }
      });
    }
  }

  reset(flag) {
    jQuery('#editcompany-form').parsley().reset();
    jQuery("#edit-company").modal("hide");
    this.onCompanyEdit.emit(flag);
  }
}

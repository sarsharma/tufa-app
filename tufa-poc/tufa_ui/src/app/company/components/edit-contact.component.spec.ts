/*
@author : Deloitte
this is Component for adding contact as a popup.
*/
import { ComponentFixture, async, inject, TestBed } from '@angular/core/testing';
import { EditContactPopup } from './edit-contact.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core'; 
import { CommonModule }  from '@angular/common';
import { RouterModule, Routes, provideRoutes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../shared/shared.module'
import { HttpModule } from '@angular/http';
import { AuthorizationHelper } from '../../authentication/util/authorization.helper.util';
import { ContactService } from '../services/contact.service';
import { HttpClient } from '../../../../node_modules/@angular/common/http';

declare var jQuery: any;

describe('Edit Contact', () => {
  let comp:    EditContactPopup;
  let fixture: ComponentFixture<EditContactPopup>;
  let de:      DebugElement;
  let el:      HTMLElement;

  // provide our implementations or mocks to the dependency injector
  beforeEach( async( () => {
    TestBed.configureTestingModule({
      imports: [ SharedModule, HttpModule, RouterTestingModule ],
      declarations: [EditContactPopup],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [HttpClient, AuthorizationHelper, ContactService,
        provideRoutes([{path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTesting', pathMatch:'full'}])
      ]
    }).compileComponents();
  }));

   beforeEach(() => {
      fixture = TestBed.createComponent(EditContactPopup);
      comp = fixture.componentInstance;
      fixture.detectChanges();
  });


  it('should create component', () => {
    expect(comp).toBeTruthy();
  });

});

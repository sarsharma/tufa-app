/*
@author : Deloitte

this is Component for editing contacts.
*/

import { Component, Input, OnInit, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Contact } from '../../model/contact.model';
import { Company } from '../models/company.model';
import { ContactService } from '../services/contact.service';
import { Router } from '@angular/router';

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class EditContactPopup
 */
@Component({
  selector: '[edit-contact]',
  templateUrl: './edit-contact.template.html',
  providers: [ContactService]
})

export class EditContactPopup{

  @Input() contactModel: Contact;

  @Input() companyModel: Company;
  @Output() onCompanyEdit = new EventEmitter<boolean>();

  showErrorFlag: boolean;
  alerts: Array<Object>;
  public mask = ['+', /\d/, /\d/, /\d/ ];
  public extmask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/ ];

  contact = new Contact();

  /**
   * Creates an instance of EditContactPopup.
   *
   * @param {ContactService} contactService
   *
   * @memberOf EditContactPopup
   */
  constructor(private contactService: ContactService) {
    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];
  }

  ngOnInit():void{
  }
    
  /*  
  ngAfterViewInit(): void {
    jQuery('#editcontactform').parsley().on('form:validate', function (formInstance) {
        // If any of these fields are valid
        if (jQuery("#first-name").parsley().isValid() || 
            jQuery("#last-name").parsley().isValid() || 
            jQuery("#email-address").parsley().isValid() || 
            jQuery("#phone-number").parsley().isValid() ||
            jQuery("#ext-optional").parsley().isValid() ||
            jQuery("#fax-optional").parsley().isValid()) 
        {        
            // Remove the required validation from all of them, so the form gets submitted
            // We already validated each field, so one is filled.
            // Also, destroy parsley's object
            jQuery("#first-name").removeAttr('required').parsley().destroy();
            jQuery("#last-name").removeAttr('required').parsley().destroy();
            jQuery("#email-address").removeAttr('required').parsley().destroy();
            jQuery("#phone-number").removeAttr('required').parsley().destroy();
            jQuery("#ext-optional").removeAttr('required').parsley().destroy();
            jQuery("#fax-optional").removeAttr('required').parsley().destroy();       
            
            return;
        }
        
        // If none is valid, add the validation to them all
        jQuery("#first-name").attr('required', 'required').parsley();
        jQuery("#last-name").attr('required', 'required').parsley();
        jQuery("#phone-number").attr('required', 'required').parsley();
        jQuery("#ext-optional").attr('required', 'required').parsley();
        jQuery("#fax-optional").attr('required', 'required').parsley();  
        return;
    });
  }*/

  /**
   *
   *
   * @param {Contact} contact
   *
   * @memberOf EditContactPopup
   */
  updateContact(contact: Contact) {
    jQuery('#editcontactform').parsley().validate();

    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#editcontactform').parsley().isValid()) {
      contact.company = this.companyModel;
      this.contactService.updateContact(contact).subscribe(data => {
        if (data) {
          this.reset(true);
        }
      },
        error => {
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({ type: 'warning', msg: error });
        });
    }

  }

  /**
   *
   *
   *
   * @memberOf EditContactPopup
   */
  reset(flag) {
    jQuery('#editcontactform').parsley().reset();
    jQuery('#edit-contact').modal('hide');
    this.onCompanyEdit.emit(flag);
    this.showErrorFlag = false;
  }

  /**
   * ngOnChanges() used to capture changes on the input properties.
   *
   * @param {SimpleChanges} changes
   *
   * @memberOf EditContactPopup
   */
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "contactModel") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.contact.contactId = chg.currentValue.contactId;
          this.contact.companyId = chg.currentValue.companyId;
          this.contact.cntryDialCd = chg.currentValue.cntryDialCd;
          this.contact.emailAddress = chg.currentValue.emailAddress;
          this.contact.faxNum = chg.currentValue.faxNum;
          this.contact.firstNm = chg.currentValue.firstNm;
          this.contact.lastNm = chg.currentValue.lastNm;
          this.contact.phoneExt = chg.currentValue.phoneExt;
          this.contact.phoneNum = chg.currentValue.phoneNum;
          this.contact.cntryFaxDialCd = chg.currentValue.cntryFaxDialCd;
          this.contact.primaryContactSequence = chg.currentValue.primaryContactSequence;
        }
      }
    }
  }
}

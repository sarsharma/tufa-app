import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, SimpleChanges, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { AuthService } from '../../login/services/auth.service';
import { CommentModel } from '../../permit/models/comment/comment.model';
import { PermitExcludeDTO } from '../models/permit-exclude-dto.model';
import { PermitExcludeModel } from "../models/permit-exclude.model";
import { PermitExcludeService } from "../services/permit-exclude.service";
import { mergeMap } from "../../../../node_modules/rxjs/operators";
import { IfStmt } from '@angular/compiler';

declare var jQuery: any;

@Component({
  // changeDetection: ChangeDetectionStrategy.OnPush,
  selector: '[permit-marketshare-exclude]',
  templateUrl: './permit-marketshare-exclude.template.html',
  styleUrls: ['./permit-marketshare-exclude.style.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [PermitExcludeService]
})

export class PermitExcludeMarketShareComponent implements OnDestroy {

  //Input Properties 
  @Input() permitSet: any;
  @Input() objectId: number;

  exclusionType: string = "FULL";
  customExclude: string = 'OFF';
  excludeComment: string;
  permitId: number;
  quarter: number;
  fiscalYear: string = '';

  newComment: string;
  excludeFlagChanged: boolean = false;
  isExclusionAffected: boolean = false;
  isInclusionAffected: boolean = false;
  isPermitAffected: boolean = false;
  isOverRideExclude: boolean = false;
  showSelectPermit: boolean = true;
  isExcludeExists: boolean = false;
  isqtrgreaterforExcludeExist: boolean = false;
  mainqtr1flag: boolean = false;
  mainqtr2flag: boolean = false;
  mainqtr3flag: boolean = false;
  mainqtr4flag: boolean = false;
  showLatestPermitExcluded: boolean = false;
  includeMap: Map<string, string> = new Map();
  excludeMap: Map<string, string> = new Map();
  isPermitValidForExclude: boolean = false;
  permitExcludeInvalidMessage: String;
  globalPermitId: number;

  busy: Subscription;
  data: any[];
  reverData: any[];
  comments: any = [];



  //Model Initialization
  public permitExcludeModel: PermitExcludeModel[] = [];
  public model: PermitExcludeModel[] = [];
  public includemodel: PermitExcludeModel[] = [];
  public permitExcludeDetails: PermitExcludeModel = new PermitExcludeModel();
  @Output() excludeFlag = new EventEmitter<{ permitId: number, exclusionType: string }>();


  constructor(
    private PermitExcludeService: PermitExcludeService,
    private changeDetectorRef: ChangeDetectorRef,
    private authService: AuthService,
  ) {
    this.changeDetectorRef.reattach();

  }

  ngOnInit() {
    this.customExclude = 'OFF';
    this.exclusionType = 'FULL';

  }

  // This binary sort should go into a new function -Code needs to be clean 
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "permitSet") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.permitSet = chg.currentValue;
          let date1: Date;
          let date2: Date;
          let date: Date;
          let permitId1;
          let permitId2;
          if (this.globalPermitId == undefined) {
            for (let i in this.permitSet) {
              if (this.permitSet[i].permitExcluded) {
                this.showLatestPermitExcluded = false;
                if (null == date1) {
                  date1 = this.permitSet[i].dateExcluded;
                  permitId1 = this.permitSet[i].permitId;
                } else {
                  date2 = this.permitSet[i].dateExcluded;
                  permitId2 = this.permitSet[i].permitId
                }
                if (null != date1 && null != date2) {
                  if (date1 > date2) {
                    this.permitId = permitId1;
                    date = date1;
                  } else {
                    this.permitId = permitId2;
                    date = date2;
                  }
                  date1 = null;
                  date2 = null;
                  permitId1 = null;
                  permitId2 = null;
                  this.showLatestPermitExcluded = true;
                }
                if (null != date && null != date1) {
                  if (date > date1) {
                    //this.permitId = permitId;
                    date = date;
                  } else {
                    this.permitId = permitId1;
                    date = date1;
                  }
                  date1 = null;
                  date2 = null;
                  permitId1 = null;
                  permitId2 = null;
                  this.showLatestPermitExcluded = true;
                }
                if (null != date1 && date2 == null && date == null) {
                  //this is true when only one permit and excluded or having multiple permit and only one excluded
                  this.showLatestPermitExcluded = true;
                  this.permitId = permitId1;
                }
              }
            }
          }
          this.globalPermitId = this.permitId;
          this.getAuditTrail();
          this.getComments();
        }
      }
    }
  }


  isDisabled(): boolean {
    if (this.exclusionType == 'INCLUDE' && this.excludeComment)
      return false;
    else {
      if (!this.isExcludeExists && this.fiscalYear !== null && !isNaN(this.permitId) && !this.showSelectPermit && this.excludeComment &&
        jQuery("#fullexclusionform input:checkbox:checked").length > 0) {
        if (this.fiscalYear.toString().length == 4)
          return false;
        else
          return true;
      }

      else
        return true;
    }

  }

  isDisabledforPartial(): boolean {

    if (this.exclusionType == 'INCLUDE' && this.excludeComment)
      return false;
    else {
      if (!this.isExcludeExists && !isNaN(this.permitId) && this.excludeComment && !this.showSelectPermit &&
        jQuery("#partialexclusionform input:checkbox:checked").length > 0) {
        if (this.permitExcludeModel[0].fiscalYear === undefined || this.permitExcludeModel[0].fiscalYear === null || this.permitExcludeModel[0].fiscalYear === '')
          return true;
        else if (this.permitExcludeModel[0].fiscalYear.toString().length == 4)
          return false;
        else
          return true;
      }

      else
        return true;
    }
  }

  filterSelectionChanged(event) {

    this.includeMap.clear();
    this.excludeMap.clear();
    this.removeErrorMessages();

    this.permitId = Number(event);
    this.clearcheckboxesforfull();

    if (!isNaN(this.permitId)) {
      this.showSelectPermit = false;
      this.getAuditTrail();
      this.getComments();
    }
    else
      this.data = [];

    this.fullExcludeChanges();

  }

  // Calls backend to get data 
  checkforWarningMessages(year, quarter) {

    this.isExclusionAffected = false;
    this.isPermitAffected = false;
    this.isInclusionAffected = false;

    if (year && quarter) {
      this.busy = this.PermitExcludeService.getaffectedAssWarningMessage(this.permitId, this.objectId, year, quarter, this.exclusionType).subscribe(
        data => {
          if (data) {
            if (this.exclusionType == 'FULL' || this.exclusionType == 'PARTIAL')
              this.isExclusionAffected = true;

            if (this.exclusionType == 'INCLUDE')
              this.isInclusionAffected = true;

          }

        }, (error) => {
          console.log(error);

        }
      )

      this.busy = this.PermitExcludeService.getexcludepermitWarningMessage(this.objectId, this.permitId, quarter, year, this.exclusionType).subscribe(
        data => {
          if (data)
            this.isPermitAffected = true;

        }, (error) => {
          console.log(error);

        }
      )
    }

  }

  clearFullQtrs() {
    this.mainqtr1flag = false;
    this.mainqtr2flag = false;
    this.mainqtr3flag = false;
    this.mainqtr4flag = false;
    jQuery('#mainqtr1').prop('disabled', false);
    jQuery('#mainqtr2').prop('disabled', false);
    jQuery('#mainqtr3').prop('disabled', false);
    jQuery('#mainqtr4').prop('disabled', false);
    jQuery('#mainqtr1').removeClass('disabled');
    jQuery('#mainqtr2').removeClass('disabled');
    jQuery('#mainqtr3').removeClass('disabled');
    jQuery('#mainqtr4').removeClass('disabled');
  }

  yearChanged(year: String) {
    this.quarter = null;
    this.removeErrorMessages();
    if (this.mainqtr1flag) this.quarter = 1
    if (this.mainqtr2flag) this.quarter = 2
    if (this.mainqtr3flag) this.quarter = 3
    if (this.mainqtr4flag) this.quarter = 4

    if (year != null && year.toString().length == 4 && this.quarter) {
      this.permitExcludeDetails.fiscalYear = year.toString();
      this.showErrorMessage(year, this.quarter)
      this.checkforWarningMessages(year, this.quarter);
    }
  }


  partialYearChanged(year: String, i) {
    this.removeErrorMessages();
    let qtr = this.permitExcludeModel[i].quarterselected

    if (year != null && year.toString().length == 4 && qtr.length > 0) {
      this.showErrorMessage(year, qtr)
      this.checkforWarningMessages(year, qtr);

    }

  }
  // Partial Exclusion
  checkboxSelected(event: any, qtr: number, index: number) {

    jQuery('#partialexclusionform').parsley().validate();
    this.removeErrorMessages();

    if (event) {
      this.permitExcludeModel[index].quarterselected.push(qtr.toString());


    }
    else {
      _.remove(this.permitExcludeModel[index].quarterselected, item => item == qtr.toString());

    }

    if (this.permitExcludeModel[index].quarterselected.length != 0) {
      this.checkforWarningMessages(this.permitExcludeModel[index].fiscalYear, this.permitExcludeModel[index].quarterselected);
      this.showErrorMessage(this.permitExcludeModel[index].fiscalYear, this.permitExcludeModel[index].quarterselected)
    }
  }

  // Refactoring needed here, could have done better implementing the Lockdown for Full and Partial 
  showErrorMessage(year, quarter: any) {

    var self = this;
    self.isExcludeExists = false;
    self.isOverRideExclude = false;
    self.isqtrgreaterforExcludeExist = false;
    if (year != null && year != '' && quarter != null && quarter.length !== 0) {

      // excludeMap should have the sort order with exlcuded first and then excluded +
      this.excludeMap.forEach(function (value, key) {

        let auditQtr = key.toString().split(',')[0];
        let auditYear = key.toString().split(',')[1];
        let status = value.toString();

        if (auditQtr == 'Q1') auditQtr = "1";
        if (auditQtr == 'Q2') auditQtr = "2";
        if (auditQtr == 'Q3') auditQtr = "3";
        if (auditQtr == 'Q4') auditQtr = "4";


        // over-ridden exclude  
        if (self.customExclude == 'OFF') {

          if (Number(auditYear) > year && status == 'EXCLUDED +') {
            self.isOverRideExclude = true;
          }
          if (Number(auditYear) == year && status == 'EXCLUDED +' && quarter < Number(auditQtr))
            self.isOverRideExclude = true;

          if (Number(auditYear) > year && status == 'EXCLUDED') {
            self.isOverRideExclude = true;
          }

          if (Number(auditYear) == year && status == 'EXCLUDED' && quarter <= Number(auditQtr))
            self.isOverRideExclude = true;
        }


        //Lock down of excludes starts from here 
        if (Number(auditYear) < year && status == 'EXCLUDED +') {
          self.isExcludeExists = true;
          return;
        }

        //  if (Number(auditYear) < year && status == 'EXCLUDED') {
        //   self.isExcludeExists = false;
        // }

        if (self.customExclude == 'OFF') {

          if (Number(auditYear) <= year && quarter >= Number(auditQtr) && status == 'EXCLUDED +')
            self.isExcludeExists = true;
        }

        //Partail Exclude warning messages  
        if (self.customExclude == 'ON') {

          if (Number(auditYear) == year && quarter == Number(auditQtr) && status == 'EXCLUDED') {
            self.isExcludeExists = true;
          }

          if (quarter.length == 1) {
            var qtr = quarter.toString();
            if (qtr > auditQtr && Number(auditYear) <= year)
              self.isqtrgreaterforExcludeExist = true;

          }

          //if (Number(auditYear) <= year && quarter.includes(auditQtr))
          //self.isExcludeExists = true;

          if (Number(auditYear) <= year && status == 'EXCLUDED +' && (quarter.includes(auditQtr) || self.isqtrgreaterforExcludeExist))
            self.isExcludeExists = true;
        }

      });
    }
  }


  removeErrorMessages() {
    this.isExclusionAffected = false;
    this.isPermitAffected = false;
    this.isExcludeExists = false;
    this.isOverRideExclude = false;
    this.isInclusionAffected = false;
    this.isPermitValidForExclude = false;
  }
  // Full exclusion 
  maincheckboxSelected(event: any, qtr: number) {

    jQuery('#fullexclusionform').parsley().validate();
    this.removeErrorMessages();
    this.quarter = qtr;
    this.permitExcludeDetails.fiscalYear = this.fiscalYear;
    this.clearFullQtrs();

    if (event && this.exclusionType == "FULL") {
      if (this.quarter == 1) {
        this.mainqtr1flag = true;
        this.mainqtr2flag = false;
        this.mainqtr3flag = false;
        this.mainqtr4flag = false;
        this.permitExcludeDetails.quarterselected = ["1"];
        jQuery('#mainqtr2').prop('disabled', true);
        jQuery('#mainqtr3').prop('disabled', true);
        jQuery('#mainqtr4').prop('disabled', true);
        jQuery('#mainqtr2').addClass('disabled');
        jQuery('#mainqtr3').addClass('disabled');
        jQuery('#mainqtr4').addClass('disabled');
      }
      else if (this.quarter == 2) {
        this.mainqtr2flag = true;
        this.mainqtr3flag = false;
        this.mainqtr4flag = false;
        this.permitExcludeDetails.quarterselected = ["2"];
        jQuery('#mainqtr3').prop('disabled', true);
        jQuery('#mainqtr4').prop('disabled', true);
        jQuery('#mainqtr3').addClass('disabled');
        jQuery('#mainqtr4').addClass('disabled');

      }
      else if (this.quarter == 3) {

        this.mainqtr3flag = true;
        this.mainqtr1flag = false;
        this.mainqtr2flag = false;
        this.mainqtr4flag = false;
        this.permitExcludeDetails.quarterselected = ["3"];
        jQuery('#mainqtr4').prop('disabled', true);
        jQuery('#mainqtr4').addClass('disabled');
      }
      else if (this.quarter == 4) {
        this.mainqtr4flag = true;
        this.mainqtr1flag = false;
        this.mainqtr2flag = false;
        this.mainqtr3flag = false;
        this.permitExcludeDetails.quarterselected = ["4"];
      }

      this.showErrorMessage(this.fiscalYear, this.quarter);
      this.checkforWarningMessages(this.fiscalYear, this.quarter);

    }

  }


  addPermitExclude() {
    this.permitExcludeModel.push(new PermitExcludeModel());

  }

  partialExcludeChanges() {
    this.permitExcludeModel = [];
    this.exclusionType = "PARTIAL";
    this.customExclude = "ON";

    if (this.permitExcludeModel.length == 0)
      this.permitExcludeModel.push(new PermitExcludeModel());

  }

  fullExcludeChanges() {
    this.exclusionType = "FULL";
    this.customExclude = "OFF";
    this.excludeFlagChanged = false;
    this.fiscalYear = '';
    this.permitExcludeDetails.quarterselected = [];

  }

  trackChanges(flag: boolean) {
    this.clearcheckboxesforfull();
    this.removeErrorMessages();
    if (flag) {
      this.excludeFlagChanged = true;
      this.partialExcludeChanges();
    } else {
      this.excludeFlagChanged = false;
      this.fullExcludeChanges();
    }
    this.getAuditTrail();

  }

  ngOnDestroy() {
    if (this.busy) {
      this.busy.unsubscribe();
    }
  }


  clearcheckboxesforfull() {

    this.fiscalYear = '';
    this.excludeComment = '';
    this.clearFullQtrs();

  }


  saveexclude() {
    if (this.exclusionType == "FULL")
      this.model.push(this.permitExcludeDetails);
    else if (this.exclusionType == "PARTIAL") {

      _.remove(this.permitExcludeModel, item => (item.quarterselected.length == 0 || item.fiscalYear == ''));
      this.model = this.permitExcludeModel
    }
    else if (this.exclusionType == "INCLUDE")
      this.model = this.includemodel;

    let comment = new CommentModel();
    comment.userComment = this.excludeComment;
    comment.author = this.authService.getSessionUser();

    let action: PermitExcludeDTO = new PermitExcludeDTO(this.model, comment);

    this.busy = this.PermitExcludeService.saveExcludedPermitDetails(this.permitId, this.objectId, this.exclusionType, action).subscribe(
      () => {
        this.globalPermitId = this.permitId;
        this.model = [];
        this.includemodel = [];
        this.clearcheckboxesforfull();
        if (this.exclusionType == 'PARTIAL')
          this.partialExcludeChanges();

        this.excludeMap.clear();
        this.getAuditTrail();
        this.getComments();
        this.removeErrorMessages();
        this.excludeFlag.emit({ permitId: this.permitId, exclusionType: this.exclusionType });
      }, (error) => {
        console.log(error);
        this.model = [];
        this.includemodel = [];
        this.excludeComment = "";
        this.removeErrorMessages();
        if (error === "Selected Permit trying to exclude is not active in Exclusion Range") {
          this.isPermitValidForExclude = true;
          this.permitExcludeInvalidMessage = error;
        }

      }
    )

    if (this.customExclude == 'ON')
      this.exclusionType = 'PARTIAL';

    if (this.customExclude == 'OFF')
      this.exclusionType = 'FULL';

  }

  includePermit(event: any, data: any) {

    let qtr = data.quarter;
    let include = new PermitExcludeModel();
    include.fiscalYear = data.assessmentYear;
    include.quarterselected = [qtr];


    if (event.target.checked) {
      this.includemodel.push(include);
      this.exclusionType = 'INCLUDE';
      this.checkforWarningMessages(data.assessmentYear, data.quarter);
    } else {
      _.remove(this.includemodel, item => (item.fiscalYear == data.assessmentYear &&
        item.quarterselected.includes(data.quarter)));
      this.removeErrorMessages();
    }

    if (this.includemodel.length == 0) {
      if (this.customExclude == 'ON')
        this.exclusionType = 'PARTIAL';

      if (this.customExclude == 'OFF')
        this.exclusionType = 'FULL';
    }

  }

  getAuditTrail() {

    if (!isNaN(this.permitId)) {
      this.busy = this.PermitExcludeService.getPermitAuditTrail(this.permitId, this.objectId).subscribe(
        data => {
          this.data = data;
          this.reverData = this.data;

          for (let item of this.reverData) {
            if (item.status == 'EXCLUDED')
              this.excludeMap.set(item.assessmentPeriod, item.status);

            if (item.status == 'INCLUDED')
              this.includeMap.set(item.assessmentPeriod, item.quarter);
          }

          //Order is important for the map as to which line goes in first for the warning messsages 
          for (let item of this.reverData) {
            if (item.status == 'EXCLUDED +')
              this.excludeMap.set(item.assessmentPeriod, item.status);
          }


          if (this.data.length > 0)
            this.showSelectPermit = false;

        })
        , (error) => {
          console.log(error);
          this.includeMap.clear();
          this.excludeMap.clear();
        }
    }
  }


  getComments() {
    if (isNaN(this.permitId)) {
      this.comments = [];
      return;
    }
    this.PermitExcludeService.getExcludeComments(this.objectId, this.permitId).subscribe(
      data => {
        this.comments = data;
        this.changeDetectorRef.markForCheck();
      }
    )
  }

}



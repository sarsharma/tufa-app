
import { Component, OnInit, OnDestroy, Input, ViewEncapsulation, SimpleChanges } from '@angular/core';
import { CompanyService } from '../services/company.service';
import { PermitsCSVPipe } from '../../company/util/permits-csv-format.pipe';
import { PermitHistoryCriteria } from '../../permit/models/permit-history-criteria.model';
import { CompanyPermits } from '../models/company-permits.model';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs';
import { NgBusyModule } from 'ng-busy';
import { ShareDataService } from '../../core/sharedata.service';

declare var jQuery: any;


@Component({
    selector: '[search-company-results]',
    templateUrl: './search-company-results.template.html',
    styleUrls: ['./search-criteria.style.scss'],
    providers: [CompanyService],
    encapsulation: ViewEncapsulation.None
})


/**
 *
 *
 * @export
 * @class SearchCompanyResultsComponent
 * @implements {OnInit}
 */
export class SearchCompanyResultsComponent implements OnInit, OnDestroy{
    public router: Router;
    public data: any[];
    public busy: Subscription;

    public sortCompanyResultsBy = "companyName";
    public sortCompanyResultsOrder = ["asc"];
    public pageNumCompanyResults: number = 1;

    @Input() companyResults: any;

    shareData: ShareDataService;

    constructor(private companyService: CompanyService,
        private storage: LocalStorageService,
        router: Router,
        private _shareData: ShareDataService
    ) {
        this.shareData = _shareData;
        this.shareData.reset();
        this.router = router;
    }

    /**
     *
     *
     *
     * @memberOf SearchCompanyResultsComponent
     */
    ngOnInit() {
        this.data = this.companyResults;
        this.loadPreferences();
    }


    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }
    }

    loadPreferences(){
        this.sortCompanyResultsBy = (this.storage.retrieve('sortCompanyResultsBy') != null) ?
                this.storage.retrieve('sortCompanyResultsBy') : "companyName";
        this.sortCompanyResultsOrder = (this.storage.retrieve('sortCompanyResultsOrder') != null) ?
                this.storage.retrieve('sortCompanyResultsOrder') : "asc";
    }

    savePreferences() {
        this.storage.store('sortCompanyResultsBy', this.sortCompanyResultsBy);
        this.storage.store('sortCompanyResultsOrder', this.sortCompanyResultsOrder);
    }

    /**
     *
     *
     * @param {SimpleChanges} changes
     *
     * @memberOf SearchCompanyResultsComponent
     */
    ngOnChanges(changes: SimpleChanges){
        for (let propName in changes) {
            if (propName === "companyResults") {
                this.data = changes[propName].currentValue;
            }
        }
    }
}
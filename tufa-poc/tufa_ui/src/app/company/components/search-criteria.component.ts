
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs';
import { CompanySearchCriteria } from '../../permit/models/company-search-criteria.model';
import { PermitHistoryCriteria } from '../../permit/models/permit-history-criteria.model';
import { PermitPeriodService } from '../../permit/services/permit-period.service';
import { PaginationAttributes } from '../../shared/datatable/pagination.attributes.model';
import { DialogService } from '../../shared/service/dialog.service';
import { PermitReportFilter } from '../models/permitreport-filter.model';
import { CompanyService } from '../services/company.service';

declare var jQuery: any;

export enum SearchPageMode {
    DAILY_OPERATIONS = 1,
    NAVIGATED_BACK,
    NEW_SEARCH
};

@Component({
    selector: '[search-criteria]',
    templateUrl: './search-criteria.template.html',
    styleUrls: ['./search-criteria.style.scss'],
    providers: [CompanyService, PermitPeriodService],
    encapsulation: ViewEncapsulation.None
})

/**
 * SearchCriteriaPage is the Daily Operations Landing Page
 *
 * @export
 * @class SearchCriteriaPage
 * @implements {OnInit}
 */
export class SearchCriteriaPage implements OnInit, OnDestroy {
    public route: ActivatedRoute;
    public router: Router;
    public reportData: any[];
    public companyData: any[];
    public keywordSearch: string;
    public phmodel: PermitHistoryCriteria = new PermitHistoryCriteria();
    public comodel: CompanySearchCriteria = new CompanySearchCriteria();
    public busy: Subscription;
    public pagemode: SearchPageMode;

    public showReportFilter: boolean;
    public showErrorFlag: boolean;
    public alerts: Array<Object>;

    public selectedSearchType: string;
    public searchTypeList = ['REPORT', 'COMPANY'];

    public selectedSearchStatus: string;
    public searchStatusList: any;
    public companyStatusList = ['All', 'Active', 'Inactive'];

    public reportFilter: PermitReportFilter = new PermitReportFilter();

    public permitItemsTotal: number;

    public permitColMapping = { permitNum: 4, companyName: 1, month: 8, fiscalYr: 10, reportStatusCd: 11, companyEIN: 12, monthAndYear: 13 }

    public permitPgAttr;

    public isNewSearch: boolean;

    // a.	Valid filters for monthFilters are JAN, FEB, MAR, etc.
    // b.	Valid filters for permitStatusFilters are ACTV, CLSD.
    // c.	Valid filters for reportStatusFilters are NSTD, COMP, ERRO.
    // d.	No widget appears on the screen yet for permitTypeFilters.  Valid filters for this are: MANU and IMPT.
    public reportStatusOptions = [{ name: 'Not Started', value: 'NSTD' }, { name: 'Error', value: 'ERRO' }, { name: 'Complete', value: 'COMP' }];
    public reportStatusOptionsMap = { NSTD: false, ERRO: false, COMP: false };

    public yearOptions: Array<string>;
    public chkyearOptions: string[];

    public monthOptions = [{ name: 'Jan', value: 'JAN' }, { name: 'Feb', value: 'FEB' }, { name: 'Mar', value: 'MAR' },
    { name: 'Apr', value: 'APR' }, { name: 'May', value: 'MAY' }, { name: 'Jun', value: 'JUN' },
    { name: 'Jul', value: 'JUL' }, { name: 'Aug', value: 'AUG' }, { name: 'Sep', value: 'SEP' },
    { name: 'Oct', value: 'OCT' }, { name: 'Nov', value: 'NOV' }, { name: 'Dec', value: 'DEC' }];
    public monthOptionsMap = {
        JAN: false, FEB: false, MAR: false, APR: false, MAY: false, JUN: false,
        JUL: false, AUG: false, SEP: false, OCT: false, NOV: false, DEC: false
    };

    public companySearchHeaderList = ['Company Name', 'Active TTB Permits', 'Company Status'];
    public permitSearchHeaderList = ['TTB Permit', 'Company Name', 'Period of Activity', 'Report Status'];

    public monthrows: any;

    public showMoreLimit = 5;

    public searchTxt;

    //Datatable stores pagination attributes for permit results with the following key name.
    public permitResultsPgAttrKey = 'pgAttr-do-permit-results'

    constructor(private companyService: CompanyService,
        private storage: LocalStorageService,
        private _permitPeriodService: PermitPeriodService,
        private _route: ActivatedRoute,
        router: Router,
        private ds: DialogService
    ) {
        this.route = _route;
        this.router = router;
        this.showErrorFlag = false;
        this.pagemode = SearchPageMode.DAILY_OPERATIONS;
        this.monthrows = Array.from(Array(Math.ceil(this.monthOptions.length / 3)).keys());
    }

    /**
     *
     * ngOnInit that initializes the Search Criteria Page
     *
     * @memberOf SearchCriteriaPage
     */
    ngOnInit(): void {

        this.cosmeticFix('#keyword-search-input');
        this.showErrorFlag = false;
        this.chkyearOptions = [];

        this.findPageLaunchMode();

        // Initialize the search criteria page component
        this.initializeSearchCriteria();

        // Loads user settings from local storage
        this.loadSearchCriteriaFromLocalStorage();

        if (this.pagemode === SearchPageMode.NAVIGATED_BACK) {
            this.userNavigatedBack();
        }
    }


    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }
    }

    /**
     * Determines how this daily operations page was launched
     * -  if param q=new was passed then this page was launched via the
     *    left navigation menu.
     * -  When no param exists, then the user navaigated back to the
     *    Daily operations page
     *
     * @memberOf SearchCriteriaPage
     */
    findPageLaunchMode() {
        this.pagemode = SearchPageMode.NAVIGATED_BACK;

        this.route.params.forEach((params: Params) => {
            // Daily operations from side navigation menu
            if (params['q']) {
                this.pagemode = SearchPageMode.DAILY_OPERATIONS;
            }
        });
    }

    /**
     * When user navigates back - the following this should happen
     *      - Retain SearchType
     *      - Retain keyword
     *      - Retain SearchStatus if Company
     *      - Execute Search
     *      -   If Report
     *      -       Retain filter settings
     *      -       Apply filter
     *      - Retain sort order
     *      - Retain active page
     * Pre-condition: If the keyword empty and when user nagivates back to the daily operations
     * don't execute the search
     *
     * @memberOf SearchCriteriaPage
     */
    userNavigatedBack() {
        if ((this.keywordSearch) && (this.keywordSearch.length > 0)) {
            this.executeSearch();
        }
    }

    /**
     * This method executs the Search
     *
     *
     * @memberOf SearchCriteriaPage
     */
    executeSearch() {
        if ((this.keywordSearch || this.keywordSearch !== 'undefined') || (this.keywordSearch.length > 0)) {
            this.fetchSearchResults(this.keywordSearch);
        }
    }

    /**
     * This method updates the report filter arrays from the checkbox status object
     * - report status and month filters are static list
     * - filter year is a dynamically generated list
     *
     * @memberOf SearchCriteriaPage
     */
    updateReportFilters() {
        // initialize report status filters array
        this.phmodel.reportStatusFilters = [];
        for (let x in this.reportStatusOptionsMap) {
            if (this.reportStatusOptionsMap[x]) {
                this.phmodel.reportStatusFilters.push(x);
            }
        }

        // initialize month filters array
        this.phmodel.monthFilters = [];
        for (let x in this.monthOptionsMap) {
            if (this.monthOptionsMap[x]) {
                this.phmodel.monthFilters.push(x);
            }
        }

        // initialize year filters array
        this.phmodel.yearFilters = [];
        for (let x of this.chkyearOptions) {
            this.phmodel.yearFilters.push(x);
        }

    }

    /**
     * Get unique years only when search type is "REPORT"
     * - User has executed the search
     * - Bring years applicable for entry in the keyword text box including empty string
     *
     * @memberOf SearchCriteriaPage
     */
    getFilterFiscalYears(fiscalYrs) {

        //let unique = new Set(this.reportData.map(item => item.fiscalYear));
        //let calYears = Array.from(unique);
        // // Don't reset years if the user is in navigation back mode
        // if(this.pagemode != SearchPageMode.NAVIGATED_BACK){
        //     this.chkyearOptions = [];
        // }
        if (fiscalYrs) {
            this.yearOptions = [];
            let fys = fiscalYrs.split(";");
            for (let year of fys) {
                this.yearOptions.push(year);
            }
        }
    }

    /**
     * Initialize the search criteria - Should default the SearchType to "REPORT"
     *
     *
     * @memberOf SearchCriteriaPage
     */
    initializeSearchCriteria() {
        // Initialize to permit Search
        this.selectedSearchType = 'REPORT';
        this.selectedSearchStatus = 'All';
    }

    /**
     * Reads all search criteria and filter selection from local storage
     * Following user selections are stored
     * SearchType - (REPORT or COMPANY)
     * KeywordSearch
     *
     * For Report Search
     *      - Month, Year and Report Status selection
     *
     * For Company Search
     *      -  CompanyStatus selection
     *
     * @memberOf SearchCriteriaPage
     */
    loadSearchCriteriaFromLocalStorage() {
        this.selectedSearchType = (this.storage.retrieve('SearchType') != null) ? this.storage.retrieve('SearchType') : 'REPORT';

        if (this.selectedSearchType === 'REPORT') {
            this.keywordSearch = (this.storage.retrieve('SearchReport') != null) ? this.storage.retrieve('SearchReport') : '';
            if (this.storage.retrieve('ReportStatusFilter') != null) {
                this.reportStatusOptionsMap = this.storage.retrieve('ReportStatusFilter');
            }
            if (this.storage.retrieve('ReportMonthFilter') != null) {
                this.monthOptionsMap = this.storage.retrieve('ReportMonthFilter');
            }
            if (this.storage.retrieve('ReportYearFilter') != null) {
                this.chkyearOptions = this.storage.retrieve('ReportYearFilter');
            }
            this.updateReportFilters();
        }

        if (this.selectedSearchType === 'COMPANY') {
            this.searchStatusList = this.companyStatusList;
            this.keywordSearch = (this.storage.retrieve('SearchCompany') != null) ? this.storage.retrieve('SearchCompany') : '';
            this.selectedSearchStatus = (this.storage.retrieve('CompanyStatus') != null) ? this.storage.retrieve('CompanyStatus') : 'Active';
        }
    }

    /**
     * Saves all search criteria and filter selection from local storage
     * Following user selections are stored
     * SearchType - (REPORT or COMPANY)
     * KeywordSearch
     *
     * For Report Search
     *      - Month, Year and Report Status selection
     *
     * For Company Search
     *      -  CompanyStatus selection
     *
     * @memberOf SearchCriteriaPage
     */
    saveSearchCriteriaToLocalStorage() {
        this.storage.store('SearchType', this.selectedSearchType);

        if (this.selectedSearchType === 'REPORT') {
            this.storage.store('SearchReport', this.keywordSearch);
            this.storage.store('ReportStatusFilter', this.reportStatusOptionsMap);
            this.storage.store('ReportMonthFilter', this.monthOptionsMap);
            this.storage.store('ReportYearFilter', this.chkyearOptions);
        }

        if (this.selectedSearchType === 'COMPANY') {
            this.storage.store('SearchCompany', this.keywordSearch);
            this.storage.store('CompanyStatus', this.selectedSearchStatus);
        }
    }

    /**
     * Clears the saved report filter items from Local Storage
     *
     *
     * @memberOf SearchCriteriaPage
     */
    clearReportFiltersFromLocalStorage() {
        this.storage.clear('ReportStatusFilter');
        this.storage.clear('ReportMonthFilter');
        this.storage.clear('ReportYearFilter');
        this.storage.clear('sortPermitResultsBy');
        this.storage.clear('sortPermitResultsOrder');
        // this.storage.clear('sortCompanyResultsBy');
        // this.storage.clear('sortCompanyResultsOrder');
    }

    /**
     * Clears the saved company preferences from Local Storage
     *
     *
     * @memberOf SearchCriteriaPage
     */
    clearCompanyPreferencesFromLocalStorage() {
        this.storage.clear('sortCompanyResultsBy');
        this.storage.clear('sortCompanyResultsOrder');
    }

    /**
     *
     *
     * @param {String} searchtxt
     *
     * @memberOf SearchCriteriaPage
     */
    fetchPermitReportSearchResults(searchtxt: String) {
        //searchtxt = (searchtxt == null) ? '' : searchtxt.replace(/-/g, "");

        let phc = this.phmodel;

        if (this.pagemode == SearchPageMode.NEW_SEARCH)
            phc = new PermitHistoryCriteria();

        phc.permitNameFilter = searchtxt.trim();
        this.showReportFilter = false;

        // since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
        if (this.busy) {
            this.busy.unsubscribe();
        }

        let pgAttrs = this.getPaginationAttributes();
        this.busy = this.companyService.getPermitReports(phc, pgAttrs).subscribe(data => {
            if (data) {
                this.phmodel = (<PermitHistoryCriteria>data);
                this.reportData = (<any>data).results;
                this.permitItemsTotal = (<any>data).itemsTotal;

                let pgAttrs: PaginationAttributes = this.getPaginationAttributes();
                pgAttrs.itemsTotal = (<any>data).itemsTotal;
                this.savePaginationAttributes(pgAttrs);

                this.getFilterFiscalYears((<any>data).fiscalYears);

                this.showReportFilter = true;
                this.refreshReportFilterObject();
                this.pagemode = SearchPageMode.DAILY_OPERATIONS;
            }
        },
            error => {
                this.showErrorFlag = true;
                this.alerts = [];
                this.alerts.push({ type: 'warning', msg: error });
            });
    }

    /**
     *
     *
     * @param {String} searchtxt
     *
     * @memberOf SearchCriteriaPage
     */
    fetchCompanySearchResults(searchtxt: String) {
        searchtxt = (searchtxt == null) ? '' : searchtxt;
        let csc = new CompanySearchCriteria();
        csc.companyNameFilter = searchtxt.trim();
        if (this.selectedSearchStatus === 'Active' || this.selectedSearchStatus === 'All') {
            csc.companyStatusFilters.push('ACTV');
        }
        if (this.selectedSearchStatus === 'Inactive' || this.selectedSearchStatus === 'All') {
            csc.companyStatusFilters.push('INAC');
        }

        // since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
        if (this.busy) {
            this.busy.unsubscribe();
        }

        this.busy = this.companyService.getCompanies(csc).subscribe(data => {
            if (data) {
                this.comodel = <CompanySearchCriteria>data;
                this.companyData = this.comodel.results;
            }
        },
            error => {
                this.showErrorFlag = true;
                this.alerts = [];
                this.alerts.push({ type: 'warning', msg: error });
            });
    }

    /**
     *
     *
     * @param {String} searchtxt
     *
     * @memberOf SearchCriteriaPage
     */
    fetchSearchResults(searchtxt: String) {

        this.showErrorFlag = false;

        // reset the table first
        this.reportData = null;
        this.companyData = null;

        // search for reports
        if (this.selectedSearchType === 'REPORT') {
            this.clearReportFiltersFromLocalStorage();
            this.fetchPermitReportSearchResults(searchtxt);
        }

        if (this.selectedSearchType === 'COMPANY') {
            this.clearCompanyPreferencesFromLocalStorage();
            this.fetchCompanySearchResults(searchtxt);
        }

        this.saveSearchCriteriaToLocalStorage();
    }

    /**
     * triggered by user
     * reset all filters and storage. A brand new search
     * @param {String} searchtxt
     *
     * @memberOf SearchCriteriaPage
     */
    onClickSearch(searchtxt: String) {
        this.pagemode = SearchPageMode.NEW_SEARCH;
        this.showMoreLimit = 5;
        this.resetPaginationAttributes();
        this.clearAllReportFilters();
        this.clearReportFiltersFromLocalStorage();
        this.clearCompanyPreferencesFromLocalStorage();
        this.fetchSearchResults(searchtxt);
        this.searchTxt = searchtxt;
    }

    /**
     * Navigate to create company
     *
     *
     * @memberOf SearchCriteriaPage
     */
    gotoCreateCompany() {
        this.saveSearchCriteriaToLocalStorage();
        this.router.navigate(['/app/company/create']);
    }

    /**
     * Navigation to create monthly reports
     *
     *
     * @memberOf SearchCriteriaPage
     */
    gotoCreateMonthlyReports() {
        this.saveSearchCriteriaToLocalStorage();
        this.router.navigate(['/app/admin/monreport']);
    }

    /**
     * Preconditino:
     * User Changes Type from Report to Company. Keyword entry is retained. Right Filter Hides. Search Box Status Dropdown Appears. No Results.
     *      i.  If switch back to Report from Company. Keyword entry is retained. Right filter appears. Search Box Status Dropdown Hides. No Results
     *      ii. Back and forth between TYPE (Company and Report). No results should be displayed. Filter will hide/appear appropriately.
     *          Search dropdown Status will hide/appear appropriately
     * @param {any} newValue
     *
     * @memberOf SearchCriteriaPage
     */
    onChangeSearchType(newValue) {
        if (newValue === 'REPORT') {
            this.selectedSearchType = newValue;
            this.companyData = null;
        }

        if (newValue === 'COMPANY') {
            this.showReportFilter = false;
            this.searchStatusList = this.companyStatusList;
            this.selectedSearchType = newValue;
            this.selectedSearchStatus = 'Active';
            this.reportData = null;
        }
    }

    /**
     *
     *
     * @param {any} newValue
     *
     * @memberOf SearchCriteriaPage
     */
    onChangeSearchStatus(newValue) {
        this.selectedSearchStatus = newValue;
    }


    /**
     * This cosmetic fix is to allow the blue focus highlight for the input group including
     * the search icon on the textbox
     * @param {*} elementID
     *
     * @memberOf SearchCriteriaPage
     */
    cosmeticFix(elementID: any) {
        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let searchInput = jQuery(elementID);
        searchInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

    /**
     * Clears all report filters
     *
     *
     * @memberOf SearchCriteriaPage
     */
    clearAllReportFilters() {
        // tslint:disable-next-line:forin
        for (let x in this.reportStatusOptionsMap) {
            this.reportStatusOptionsMap[x] = false;
        }
        // tslint:disable-next-line:forin
        for (let x in this.monthOptionsMap) {
            this.monthOptionsMap[x] = false;
        }
        this.chkyearOptions = [];
    }

    /**
     * Angular2 triggers a change only when immutable object
     * Since reportFilter is immutable, we cannot update just one its property in place
     * and expect change detection to happen. Instead, we have to create a new reportFilter object.
     * https://vsavkin.com/immutability-vs-encapsulation-90549ab74487#.8uf2g43in
     * @memberOf SearchCriteriaPage
     */
    refreshReportFilterObject() {
        this.reportFilter = null;
        this.reportFilter = new PermitReportFilter();
        this.reportFilter.reportStatus = this.phmodel.reportStatusFilters;
        this.reportFilter.reportMonths = this.phmodel.monthFilters;
        this.reportFilter.reportYears = this.phmodel.yearFilters;
    }

    /**
     * This method is triggered when user selects or deselects Report Status filter
     *
     * @param {any} option
     * @param {any} event
     *
     * @memberOf SearchCriteriaPage
     */
    updateCheckedReportStatusOptions(option, event) {
        this.reportStatusOptionsMap[option] = event.target.checked;

        this.phmodel.reportStatusFilters = [];
        for (let x in this.reportStatusOptionsMap) {
            if (this.reportStatusOptionsMap[x]) {
                this.phmodel.reportStatusFilters.push(x);
            }
        }
        this.saveSearchCriteriaToLocalStorage();
        this.refreshReportFilterObject();
        //this.resetPaginationAttributes();
        this.getPaginationAttributes();
        this.fetchSearchResults(this.keywordSearch);


    }

    /**
     * This method is triggered when user selects or deselects Report Months filter
     *
     * @param {any} option
     * @param {any} event
     *
     * @memberOf SearchCriteriaPage
     */
    updateCheckedMonthOptions(option, event) {
        this.monthOptionsMap[option] = event.target.checked;
        this.phmodel.monthFilters = [];
        for (let x in this.monthOptionsMap) {
            if (this.monthOptionsMap[x]) {
                this.phmodel.monthFilters.push(x);
            }
        }
        this.saveSearchCriteriaToLocalStorage();
        this.refreshReportFilterObject();
        //this.resetPaginationAttributes();
        this.getPaginationAttributes();
        this.fetchSearchResults(this.keywordSearch);


    }

    /**
     * This method is triggered when user selects or deselects Report Years filter
     *
     * @param {any} option
     * @param {*} $event
     *
     * @memberOf SearchCriteriaPage
     */
    onChangeYearSelection(option, $event: any) {
        if ($event.target.checked) {
            this.chkyearOptions.push($event.target.value);
        } else {
            this.chkyearOptions.splice(this.chkyearOptions.indexOf(option), 1);
        }

        this.phmodel.yearFilters = [];
        for (let x of this.chkyearOptions) {
            this.phmodel.yearFilters.push(x);
        }
        this.saveSearchCriteriaToLocalStorage();
        this.refreshReportFilterObject();
        //this.resetPaginationAttributes();
        this.getPaginationAttributes();
        this.fetchSearchResults(this.keywordSearch);


    }

    /**
     * This method binds checked year status options
     *
     * @param {any} option
     * @returns
     *
     * @memberOf SearchCriteriaPage
     */
    isYearChecked(option) {
        if (this.chkyearOptions) {
            return (this.chkyearOptions.indexOf(option) !== -1);
        }
        return false;
    }


    onSortPermitResults(pgAttributes: PaginationAttributes) {
        this.fetchPermitReportSearchResults(this.keywordSearch);
    }

    getPaginationAttributes() {

        let pgAttrs: PaginationAttributes = this.storage.retrieve(this.permitResultsPgAttrKey);
        if (pgAttrs) {
            pgAttrs = new PaginationAttributes(pgAttrs.activePage, pgAttrs.pageSize, null, null, pgAttrs.sortBy, pgAttrs.sortOrder);
        } else
            pgAttrs = new PaginationAttributes(1, 15, null, null, [this.permitColMapping["permitNum"], this.permitColMapping["monthAndYear"]], ["desc;desc"]);

        return pgAttrs;

    }

    savePaginationAttributes(pgAttrs: PaginationAttributes) {
        this.storage.store(this.permitResultsPgAttrKey, pgAttrs);
    }

    resetPaginationAttributes() {
        let pgAttrs = new PaginationAttributes(1, 15, null, null, [this.permitColMapping["permitNum"], this.permitColMapping["monthAndYear"]], ["desc;desc"]);
        this.storage.store(this.permitResultsPgAttrKey, pgAttrs);
    }

    private s2ab(s) {
        let buf = new ArrayBuffer(s.length);
        let view = new Uint8Array(buf);
        for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xff;
        return buf;
    }

    throwError() {

    }
}

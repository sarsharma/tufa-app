
/*
@author : Deloitte
This is the component that displays Permit Report search results

*/
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs';
import { PaginationAttributes } from '../../shared/datatable/pagination.attributes.model';
import { CompanyPermitHistory } from '../models/company-permit-history.model';
import { PermitReportFilter } from '../models/permitreport-filter.model';
import { CompanyService } from '../services/company.service';
import { ShareDataService } from './../../core/sharedata.service';

declare var jQuery: any;

@Component({
    selector: '[search-permit-results]',
    templateUrl: './search-permit-results.template.html',
    styleUrls: ['./search-criteria.style.scss'],
    providers: [CompanyService],
    encapsulation: ViewEncapsulation.None
})

/**
 * Table displays permit report results
 *
 * @export
 * @class SearchPermitResultsComponent
 * @implements {OnInit}
 */
export class SearchPermitResultsComponent implements OnInit, OnDestroy {

    public router: Router;
    public data: any[];
    public filterOptions: PermitReportFilter;
    public busy: Subscription;

    public pageNumPermitResults: number = 2;

    shareData: ShareDataService;

    // Results from Search are passed from the parent component
    @Input() permitResults: any;
    @Input() permitReportFilter: PermitReportFilter;
    @Input() itemsTotal: number;
    @Input() pagemode;

    @Output() sortPermitResults: EventEmitter<any> = new EventEmitter();
    @Output() initPgAttr: EventEmitter<any> = new EventEmitter();

    navigateToPeriodofActivity: boolean;
    navigateToCompanyDetail: boolean;
    navigateToPermitHistory: boolean;


    columnMapping = { permitNum: 4, companyName: 1, month: 8, year: 9, reportStatusCd: 11, companyEIN: 12, sortMonthYear: 13 }

    pgAttrs: PaginationAttributes;

     //Datatable stores pagination attributes for permit results with the following key name.
     public permitResultsPgAttrKey = 'pgAttr-do-permit-results'

    constructor(private companyService: CompanyService,
        private storage: LocalStorageService,
        router: Router,
        _shareData: ShareDataService,
    ) {
        this.router = router;
        this.shareData = _shareData;
        this.navigateToPeriodofActivity = false;
        this.navigateToCompanyDetail = false;
        this.navigateToPermitHistory = false;

        this.loadPageAttributes();

        /*
        this will reset all the params present in ShareDataService object  
        */
        this.shareData.reset();

    }

    /**
     *
     *
     *
     * @memberOf SearchPermitResultsComponent
     */
    ngOnInit() {
        // Populates the table data
        this.data = this.permitResults;
        this.filterOptions = this.permitReportFilter;
        this.loadPreferences();
    }

    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }

        // if (!this.navigateToPeriodofActivity&&!this.navigateToPermitHistory&&!this.navigateToCompanyDetail)
        //     this.storage.clear(this.permitResultsPgAttrKey);
    }

    /**
     *
     *
     * @param {SimpleChanges} changes
     *
     * @memberOf SearchPermitResultsComponent
     */
    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "permitResults") {
                this.data = changes[propName].currentValue;
                this.pgAttrs = this.storage.retrieve(this.permitResultsPgAttrKey);
            }

            if (propName === "itemsTotal") {
                this.pgAttrs.itemsTotal = changes[propName].currentValue;
            }

            if (propName === "permitReportFilter") {
                this.filterOptions = changes[propName].currentValue;
            }

            if (propName === "pagemode") {
                let pgMd = changes[propName].currentValue
                if (pgMd == 3) {
                    this.pgAttrs.activePage = 1;
                    this.pgAttrs.pageSize = 15;
                }
            }

        }
    }

    loadPageAttributes() {

        if (!this.storage.retrieve(this.permitResultsPgAttrKey)) {
            this.pgAttrs = new PaginationAttributes(1, 15, null, null,  [this.columnMapping["permitNum"],this.columnMapping["monthAndYear"]], ["desc;desc"]);
            this.storage.store(this.permitResultsPgAttrKey, this.pgAttrs);
        } else
            this.pgAttrs = this.storage.retrieve(this.permitResultsPgAttrKey);

    }


    loadPreferences() {
        // this.sortPermitResultsBy = (this.storage.retrieve('sortPermitResultsBy') != null) ?
        //     this.storage.retrieve('sortPermitResultsBy') : "permitNum";
        // this.sortPermitResultsOrder = (this.storage.retrieve('sortPermitResultsOrder') != null) ?
        //     this.storage.retrieve('sortPermitResultsOrder') : "desc";
    }

    savePreferences() {
        // this.storage.store('sortPermitResultsBy', this.sortPermitResultsBy);
        // this.storage.store('sortPermitResultsOrder', this.sortPermitResultsOrder);
    }

    toPermitHistory(){
            this.navigateToPermitHistory= true;
    }

    toCompanyDetail(){
            this.navigateToCompanyDetail = true;
    }
    /**
     *
     *
     * @param {CompanyPermitHistory} history
     *
     * @memberOf SearchPermitResultsComponent
     */
    toPeriodofActivity(history: CompanyPermitHistory) {
        this.navigateToPeriodofActivity = true;
        this.savePreferences();
        this.shareData.reset();
        this.shareData.companyName = history.companyName;
        this.shareData.permitNum = history.permitNum;
        this.shareData.period = history.displayMonthYear;
        this.shareData.permitId = history.permitId;
        this.shareData.periodId = history.reportPeriodId;
        this.shareData.periodStatus = history.reportStatusCd;
        this.shareData.companyId = history.companyId;
        this.shareData.ein = history.companyEIN;
    }

    public onPageChange(event) {
        this.pgAttrs.pageSize = event.rowsOnPage;
        this.pgAttrs.activePage = event.activePage;
        this.storage.store(this.permitResultsPgAttrKey, this.pgAttrs);
        this.sortPermitResults.emit(this.pgAttrs);
    }

    public onSortOrder(event) { 
        this.pgAttrs.sortOrder = [];

        if(_.indexOf( this.pgAttrs.sortBy,this.columnMapping['permitNum'])!=-1){
            this.pgAttrs.sortOrder.push(event[0]+";desc");
        }else
            this.pgAttrs.sortOrder.push(event[0]);
        this.storage.store(this.permitResultsPgAttrKey, this.pgAttrs);
        this.sortPermitResults.emit(this.pgAttrs);
    }

    public onSortBy(event){
        this.pgAttrs.sortBy = [];
        if(event=== 'permitNum'){
            this.pgAttrs.sortBy.push(this.columnMapping[event]);
            this.pgAttrs.sortBy.push(this.columnMapping['sortMonthYear']);
        }else 
         this.pgAttrs.sortBy.push(this.columnMapping[event]);
    }
}
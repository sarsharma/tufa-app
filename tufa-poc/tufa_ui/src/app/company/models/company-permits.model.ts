/*
@author : Deloitte
This is a model to hold Company permit history details.

*/
/**
 * 
 * 
 * @export
 * @class CompanyPermits
 */
export class CompanyPermits {
    companyName: string;
    companyId: number;
    permits: string;
    companyStatus: string;
}
/*
@author : Deloitte
This is a model to hold Company details.

*/

import { Address } from '../../model/address.model';
import { Contact } from '../../model/contact.model';
import { PermitUpdate } from '../../permit-mgmt/components/ingestion/models/permit-update.model';
import { Permit } from '../../permit/models/permit.model';
import { CompanyPermitHistory } from './company-permit-history.model';

/**
 * 
 * 
 * @export
 * @class Company
 */
export class Company {
    companyId: string;
    legalName: string;
    einNumber: string;
    companyCreateDt: string;
    permitNumber: string;
    permitTypeCd: string;
    activeDate: string;
    inactiveDate: string;
    permitList: Permit[];
    permitSet: Permit[];
    permitHistoryList: CompanyPermitHistory[];
    selectedContact: Contact;
    primaryAddress: Address;
    alternateAddress: Address;
    companyStatus: string;
    createdDt: Date;
    createdBy: String;
    companyComments: string;
    permitAudit: PermitUpdate;
    permitStatusTypeCd: string;

    emailAddressFlag: string;
    physicalAddressFlag: string;
    welcomeLetterFlag: string;
    welcomeLetterSentDt: Date;

    selected: boolean;
    contactSet: Contact[];

}
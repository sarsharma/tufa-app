import { CommentModel } from "../../permit/models/comment/comment.model";
import { PermitExcludeModel } from "../../permit/models/permit-exclude.model";

/**
 * Used as a DTO for the excludes and eclude comment 
 */
export class PermitExcludeDTO {
    excludes: PermitExcludeModel[];
    comment: CommentModel;

    constructor(excludes: PermitExcludeModel[], comment: CommentModel) {
        this.excludes = excludes;
        this.comment = comment;
    }
}
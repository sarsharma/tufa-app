/*
@author : Deloitte
This is a model to hold Permit Report filter.

*/

/**
 * 
 * 
 * @export
 * @class PermitReportFilter
 */
export class PermitReportFilter {
    private _reportStatus: any;
    private _reportYears: any;
    private _reportMonths: any;    

    get reportStatus(): any {
        return this._reportStatus;
    }
    set reportStatus(value: any) {
        this._reportStatus = value;
    }
    
    get reportYears(): any {
        return this._reportYears;
    }

    set reportYears(value: any) {
        this._reportYears = value;
    }

    get reportMonths(): any {
        return this._reportMonths;
    }

    set reportMonths(value: any) {
        this._reportMonths = value;
    }
}
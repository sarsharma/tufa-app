/*
@author : Deloitte
Service class to perform address related operations.
*/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import * as _ from 'lodash';
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { Address } from '../../model/address.model';
import { environment } from './../../../environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
};


/**
 * 
 * 
 * @export
 * @class AddressService
 */
@Injectable()
export class AddressService {

    private createupdateaddressurl = environment.apiurl + '/api/v1/companies/address';
    private getcountrynmurl = environment.apiurl + '/api/v1/companies/address/cntryNm';
    private getcountrycdurl = environment.apiurl + '/api/v1/companies/address/cntryCd';
    private getstatenmurl = environment.apiurl + '/api/v1/companies/address/stateNm';
    private getstatecdurl = environment.apiurl + '/api/v1/companies/address/stateCd';
    private getreportaddressurl = environment.apiurl + '/api/v1/companies/address/';
    private deleteaddressurl = environment.apiurl + '/api/v1/companies/address/';
    
    constructor(private _httpClient: HttpClient) { }

    /**
     * 
     * 
     * @param {Address} address
     * @returns {Observable<Address>}
     * 
     * @memberOf AddressService
     */
    saveOrUpdateAddress(address: Address): Observable<Address> {

        let addressUpper = _.mapValues(address, function(s){ return _.isNull(s) ? null : _.isString(s) ? s.toUpperCase() : s; });

        return this._httpClient.put(this.createupdateaddressurl, addressUpper, httpOptions).pipe(
            tap(_ => this.log("saveOrUpdateAddress")),
            catchError(this.handleError<any>("saveOrUpdateAddress"))
        );

    }

     /**
     * 
     * 
     * @param {Contact} contact
     * @returns {Observable<Contact>}
     * 
     * @memberOf ContactService
     */
    deleteAddress(address: Address, addressType: string): Observable<string> {
        return this._httpClient.delete(this.deleteaddressurl + address.companyId + "/" +  addressType,  httpOptions).pipe(
			tap(_ => this.log("deleteContact")),
			catchError(this.handleError<any>("deleteContact"))
		);

    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf AddressService
     */
    getCountryNames() {
        return this._httpClient.get(this.getcountrynmurl, httpOptions);
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf AddressService
     */
    getCountryCodes() {
        return this._httpClient.get(this.getcountrycdurl, httpOptions);
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf AddressService
     */
    getStateNames() {
        return this._httpClient.get(this.getstatenmurl, httpOptions);
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf AddressService
     */
    getStateCodes() {
        return this._httpClient.get(this.getstatecdurl, httpOptions);
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf AddressService
     */
    getReportAddress(companyId: any, permitId: any, periodId: any, ein:any) {
        let cmpId = companyId ? companyId : ein;
        return this._httpClient.get(this.getreportaddressurl + cmpId + "/" + permitId + "/" + periodId, httpOptions);
    }

    /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log("${operation} failed: ${error.message}");

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Attribute Service message with the MessageService */
    private log(message: string) {
        //this.messageService.add("Profile Service: ${message}");
    }

}

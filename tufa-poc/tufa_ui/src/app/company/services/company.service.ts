/*
@author : Deloitte
Service class to perform company related operations.
*/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { CompanySearchCriteria } from '../../permit/models/company-search-criteria.model';
import { PermitHistoryCriteria } from '../../permit/models/permit-history-criteria.model';
import { Permit } from '../../permit/models/permit.model';
import { CompanyPermitHistory } from '../models/company-permit-history.model';
import { Company } from '../models/company.model';
import { environment } from './../../../environments/environment';
import { PaginationAttributes } from './../../shared/datatable/pagination.attributes.model';



const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
};

/**
 * 
 * 
 * @export
 * @class CompanyService
 */
@Injectable()
export class CompanyService {

    private saveupdatecompanyurl = environment.apiurl + '/api/v1/companies';
    private getcompanyurl = environment.apiurl + '/api/v1/companies/';
    private getcompanysearchurl = environment.apiurl + '/api/v1/companies/search';
    private getpermithistoryurl = environment.apiurl + '/api/v1/permit/history/';
    private getcriteriaurl = environment.apiurl + '/api/v1/permit/history';
    private getmonthlyreporturl = environment.apiurl + '/api/v1/companies/68/permits/history';
    private getcompnaytrackingurl = environment.apiurl + '/api/v1/companies/tracking/'
    private savepermiturl = environment.apiurl + 'api/v1/companies/26/permits';
    private deletemonthlyreporturl = environment.apiurl + '/api/v1/permit/deletemonthlyreport/';

    constructor(private _httpClient: HttpClient) { }

    /**
     * 
     * 
     * @param {Company} company
     * @returns {Observable<Company>}
     * 
     * @memberOf CompanyService
     */
    createCompany(company: Company): Observable<Company> {
        return this._httpClient.post<Company>(this.saveupdatecompanyurl, (company), httpOptions).pipe(
            tap(_ => this.log("getUserbyEmail"))
        );
    }

    /**
     * 
     * 
     * @param {Company} company
     * @returns {Observable<Company>}
     * 
     * @memberOf CompanyService
     */
    updateCompany(company: Company): Observable<Company> {

        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this._httpClient.put<Company>(this.saveupdatecompanyurl, (company), httpOptions).pipe(
            tap(_ => this.log("getUserbyEmail"))
        );
    }

    /**
     * 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf CompanyService
     */
    getCompany(id): Observable<Company> {
        return this._httpClient.get<Company>(this.getcompanyurl + id, httpOptions);
    }

    getCompanyEIN(ein) {
        return this._httpClient.get<Company>(this.getcompanyurl + 'ein/' + ein, httpOptions);
    }

    getActiveImptCompanyByEIN(ein): Observable<Company> {
        return this._httpClient.get<Company>(this.getcompanyurl + 'impt/active/ein/' + ein, httpOptions).pipe(
            tap(_ => this.log("getActiveImptCompanyByEIN"))
        );
    }
    getAssociatedCompanyByEIN(ein,fiscalYear,fiscalQtr,tobaccoClassNm): Observable<String>  {
        return this._httpClient.get<String>(this.getcompanyurl 
            + 'impt/associated/ein/' + ein +'/fiscalYear/'+ fiscalYear + '/fiscalQtr/'+ fiscalQtr + '/tobaccoClassNm/' + tobaccoClassNm , httpOptions).pipe(
            tap(_ => this.log("getAssociatedCompanyByEIN"))
        );

        
    }
    getCompanyTrackingInformation(criteria: CompanySearchCriteria, pgAttrs?: PaginationAttributes): any {
        let url: string = pgAttrs ? this.getcompnaytrackingurl + '?' + pgAttrs.getQueryParamString() : this.getcompnaytrackingurl;
        return this._httpClient.put(url, (criteria), httpOptions);
    }

    updateComapnyTrackingInformation(selectedCompanies: Company[]): any {
        let url: string = this.getcompnaytrackingurl + 'update/';
        return this._httpClient.post(url, (selectedCompanies), httpOptions);
    }

    /**
     * 
     * 
     * @param {Permit} permit
     * @returns {Observable<Company>}
     * 
     * @memberOf CompanyService
     */
    createPermit(permit: Permit): Observable<Company> {
        let savepermiturl = environment.apiurl + '/api/v1/companies/' + permit.companyId + '/permits';
        return this._httpClient.post<Company>(savepermiturl, permit, httpOptions).pipe(
            tap(_ => this.log("createPermit"))
        );
    }

    /**
     * 
     * 
     * @param {string} companyId
     * @param {string} permitId
     * @returns
     * 
     * @memberOf CompanyService
     */
    getPermit(companyId: string, permitId: string) {
        let getupdatepermiturl = environment.apiurl + '/api/v1/companies/' + companyId + '/permits/' + permitId;
        return this._httpClient.get(getupdatepermiturl).pipe(
            tap(_ => this.log("getPermit"))
        );
    }

    /**
     * 
     * 
     * @param {Permit} permit
     * @returns {Observable<Company>}
     * 
     * @memberOf CompanyService
     */
    updatePermit(permit: Permit): Observable<Company> {
        let getupdatepermiturl = environment.apiurl + '/api/v1/companies/' + permit.company.companyId + '/permits/' + permit.permitId;

        return this._httpClient.put<Company>(getupdatepermiturl, permit, httpOptions).pipe(
            tap(_ => this.log("updatePermit"))
        );
    }

    // Given permitId return Permit History
    /**
     * 
     * 
     * @param {string} permitId
     * @returns
     * 
     * @memberOf CompanyService
     */
    getPermitHistory(permitId: string) {
        return this._httpClient.get(this.getpermithistoryurl + permitId, httpOptions);

    }

    /**
     * 
     * 
     * @param {PermitHistoryCriteria} permitHistoryCritera
     * @returns
     * 
     * @memberOf CompanyService
     */
    getPermitReports(permitHistoryCritera: PermitHistoryCriteria, pgAttrs?: PaginationAttributes) {
        let url = pgAttrs ? this.getcriteriaurl + "?" + pgAttrs.getQueryParamString() : this.getcriteriaurl;
        return this._httpClient.put(url, permitHistoryCritera, httpOptions);
    }

    /**
     * 
     * 
     * @param {CompanySearchCriteria} companySearchCriteria
     * @returns
     * 
     * @memberOf CompanyService
     */
    getCompanies(companySearchCriteria: CompanySearchCriteria) {
        return this._httpClient.put(this.getcompanysearchurl, companySearchCriteria, httpOptions);
    }

    deleteMonthlyReport(permitHistory: CompanyPermitHistory) {
        return this._httpClient.delete(this.deletemonthlyreporturl + permitHistory.companyId + '/' + permitHistory.permitId + '/' + permitHistory.reportPeriodId, httpOptions);
    }

    exportNewCompaniesData() {
        let url = this.saveupdatecompanyurl + "/export";
        return this._httpClient.get(url, httpOptions);
    }

    // /**
    //  * Handle Http operation that failed.
    //  * Let the app continue.
    //  * @param operation - name of the operation that failed
    //  * @param result - optional value to return as the observable result
    //  */
    // private handleError<T>(operation = "operation", result?: T) {
    // 	return (error: any): Observable<T> => {
    // 		// TODO: send the error to remote logging infrastructure
    // 		console.error(error); // log to console instead

    // 		// TODO: better job of transforming error for user consumption
    // 		this.log("${operation} failed: ${error.message}");

    // 		// Let the app keep running by returning an empty result.
    // 		return of(result as T);
    // 	};
    // }

    /** Log a Attribute Service message with the MessageService */
    private log(message: string) {
        //this.messageService.add("Profile Service: ${message}");
    }



}
/*
@author : Deloitte
Service class to perform contacts related operations.
*/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { environment } from "../../../environments/environment";
import { Contact } from '../../model/contact.model';
import { Company } from '../models/company.model';
import _ from "lodash";

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};


/**
 * 
 * 
 * @export
 * @class ContactService
 */
@Injectable()
export class ContactService {

    private createupdatecontacturl = environment.apiurl + '/api/v1/companies/contact';
    private readcontacturl = environment.apiurl + '/api/v1/companies/contact/';
    private deletecontacturl = environment.apiurl + '/api/v1/companies/contact/';
    private getreportcontacturl = environment.apiurl + '/api/v1/companies/contact/';

    constructor(private _httpClient: HttpClient) { }

    /**
     * 
     * 
     * @param {Contact} contact
     * @returns {Observable<Contact>}
     * 
     * @memberOf ContactService
     */
    createContact(contact: Contact): Observable<Contact> {

        let contactUpper = _.mapValues(contact, function(s){ return _.isNull(s) ? null : _.isString(s) ? s.toUpperCase() : s; });

        return this._httpClient.post(this.createupdatecontacturl, contactUpper, httpOptions).pipe(
			tap(_ => this.log("getUserbyEmail")),
			catchError(this.handleError<any>("getUserbyEmail"))
		);

    }

    /**
     * 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf ContactService
     */
    readContact(id) {
        return this._httpClient.get(this.readcontacturl + id, httpOptions);
    }

    /**
     * 
     * 
     * @param {Contact} contact
     * @returns {Observable<Contact>}
     * 
     * @memberOf ContactService
     */
    updateContact(contact: Contact): Observable<Contact> {
        /**
         * this is done to remove circular parsing in ()
         */
        let contactObj = new Contact();
        let company = new Company();
        company.companyId = contact.company.companyId;
        contactObj = contact;
        contactObj.company = null;
        contactObj.company = company;

        let contactUpper = _.mapValues(contactObj, function(s){ return _.isNull(s) ? null : _.isString(s) ? s.toUpperCase() : s; });

        return this._httpClient.put(this.createupdatecontacturl, contactUpper, httpOptions).pipe(
			tap(_ => this.log("updateContact")),
			catchError(this.handleError<any>("updateContact"))
		);

    }

    /**
     * 
     * 
     * @param {Contact} contact
     * @returns {Observable<Contact>}
     * 
     * @memberOf ContactService
     */
    setAsPrimaryContact(setPrimary: boolean, contact: Contact): Observable<Contact> {
        /**
         * this is done to remove circular parsing in ()
         */
        let contactObj = new Contact();
        let company = new Company();
        company.companyId = contact.company.companyId;
        contactObj = contact;
        contactObj.company = null;
        contactObj.company = company;

        let contactUpper = _.mapValues(contactObj, function(s){ return _.isNull(s) ? null : _.isString(s) ? s.toUpperCase() : s; });

        return this._httpClient.put(this.createupdatecontacturl + "/" + setPrimary, contactUpper, httpOptions).pipe(
			tap(_ => this.log("updateContact")),
			catchError(this.handleError<any>("updateContact"))
		);

    }
    

    /**
     * 
     * 
     * @param {Contact} contact
     * @returns {Observable<Contact>}
     * 
     * @memberOf ContactService
     */
    deleteContact(contact: Contact): Observable<Contact> {
        return this._httpClient.delete(this.deletecontacturl + contact.contactId, httpOptions).pipe(
			tap(_ => this.log("deleteContact")),
			catchError(this.handleError<any>("deleteContact"))
		);

    }

    /**
     * 
     * 
     * @param {*} companyId
     * @param {*} permitId
     * @param {*} periodId
     * @returns
     * 
     * @memberOf ContactService
     */
    getReportContacts(companyId: any, permitId: any, periodId: any ){
        return this._httpClient.get(this.getreportcontacturl + companyId + "/" +permitId + "/" + periodId, httpOptions);
    }

    	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}

}

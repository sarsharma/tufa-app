import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { PermitExcludeModel } from '../models/permit-exclude.model';
import { environment } from "./../../../environments/environment";
import { CommentModel } from "../../permit/models/comment/comment.model";
import { PermitExcludeDTO } from "../models/permit-exclude-dto.model";
import { Credentials } from '../../login/model/credentials.model';
import { AuthService } from '../../login/services/auth.service';

import { Document } from "../../permit/models/document.model";


const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class PermitExcludeService {

    private getPermitExcludeurl = environment.apiurl + '/api/v1/companies/excludepermit';
    private getauditTrailsurl = environment.apiurl + '/api/v1/companies/getexcludepermitdetails';
    private uploadpermitcommentattachmenturl = environment.apiurl + '/api/v1/companies/excludepermit/comment/attachs/';
    private getpermitcommentattachmentsurl = environment.apiurl + '/api/v1/companies/excludepermit/comment/attachs/';
    private deletepermitcommentattachmenturl = environment.apiurl + '/api/v1/companies/excludepermit/comment/attachs/';
    private getWarningMessageurl = environment.apiurl + '/api/v1/companies/affectedassmt/warning';
    private getWarningforexcludePermit = environment.apiurl + '/api/v1/companies/excludepermit/warning';

    private credential: Credentials;
    constructor(private _httpClient: HttpClient, private authService: AuthService
    ) { }

    saveExcludedPermitDetails(permitId: any, CompanyId: any, excludeFlag: string, action: PermitExcludeDTO) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let updatedurl = this.getPermitExcludeurl + "/" + CompanyId + "/" + permitId + "/" + excludeFlag;

        return this._httpClient.post(updatedurl, action, httpOptions);
    }

    getPermitAuditTrail(permitId: any, companyid: any): any {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let updatedurl = this.getauditTrailsurl + "/" + companyid + "/" + permitId;
        return this._httpClient.get(updatedurl, httpOptions);
    }

    getaffectedAssWarningMessage(permitid: any,companyid: any, fiscalyear: any, quarter: any, exclusionType: any): any {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let updatedurl = this.getWarningMessageurl + "/" + permitid + "/" + companyid + "/" + fiscalyear + "/" + quarter + "/" + exclusionType;
        return this._httpClient.get(updatedurl, httpOptions);
    }

    getexcludepermitWarningMessage(companyid: any, permitId: any,quarter: any, fiscalyear: any, exclusionType: any): any {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let updatedurl = this.getWarningforexcludePermit + "/" + companyid + "/" + permitId + "/" + quarter + "/" + fiscalyear + "/" + exclusionType ;
        return this._httpClient.get(updatedurl, httpOptions);
    }

    IncludeExcludedPermitDetails(missingForms: any[], periodId: string, permitId: string) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let updatedurl = this.getPermitExcludeurl + "/" + periodId + "/" + permitId + "/forms";
        return this._httpClient.post(updatedurl, missingForms, httpOptions);
    }

    getExcludeComments(companyid: any, permitId: any): Observable<CommentModel[]> {
        let getUrl = this.getPermitExcludeurl + "/comment/" + companyid + "/" + permitId;
        return this._httpClient.get<CommentModel[]>(getUrl, httpOptions);
    }

    updateExcludeComment(comment: CommentModel) {
        let updateUrl = this.getPermitExcludeurl + "/comment";
        return this._httpClient.post(updateUrl, comment, httpOptions);
    }

    loadPermitDocs(commentId: number): Observable<string> {
        return this._httpClient.get(this.getpermitcommentattachmentsurl + 'commentId/' + commentId, httpOptions).pipe(
            tap(_ => this.log("loadDocs")),
            catchError(this.handleError<any>("loadDocs"))
        );
    }

    uploadPermitAttachment(document: any): Observable<string> {
        let params = new HttpParams();

        const options = {
            params: params,
            reportProgress: true,
        };

        return this._httpClient.post(this.uploadpermitcommentattachmenturl, (document), options).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }

    /**
    * Handle Http operation that failed.
    * Let the app continue.
    * @param operation - name of the operation that failed
    * @param result - optional value to return as the observable result
    */
    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log("${operation} failed: ${error.message}");

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Attribute Service message with the MessageService */
    private log(message: string) { }

    downloadPermitAttachment(docid: number): Observable<Document> {
        return this._httpClient.get(this.deletepermitcommentattachmenturl + docid, httpOptions).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }
    deletePermitAttachment(docid: number): Observable<string> {
        return this._httpClient.delete(this.deletepermitcommentattachmenturl + docid, httpOptions).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }
    setLinkAuthentication(flag: boolean) {
        flag ? this.authService.setAccessTokenCookie() : this.authService.removeAccessTokenCookie();
    }
}
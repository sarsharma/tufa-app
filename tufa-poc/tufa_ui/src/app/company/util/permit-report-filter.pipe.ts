/*
@author : Deloitte
This is a Pipe that filters Permit Report results

*/
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { PermitReportFilter } from '../models/permitreport-filter.model';



/**
 * 
 * 
 * @export
 * @class PermitReportsDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "reportpipe"
})
export class PermitReportsDataFilterPipe implements PipeTransform {

    /**
     * 
     * 
     * @param {any[]} data
     * @param {PermitReportFilter} filter
     * @returns {*}
     * 
     * @memberOf PermitReportsDataFilterPipe
     */
    transform(data: any[], filter: PermitReportFilter): any {
        var results = data;

        if (data) {
            if(filter){
                // filter by years
                if(filter.reportYears.length > 0){
                   results =  _.filter(results, function(item) {
                        return _.includes(Array.from(filter.reportYears), item.fiscalYear);
                    });
                }
                // filter by months
                if(filter.reportMonths.length > 0){
                   results =  _.filter(results, function(item) {
                        return _.includes(Array.from(filter.reportMonths), item.month);
                    });
                }                
                // filter by status
                if(filter.reportStatus.length > 0){
                   results =  _.filter(results, function(item) {
                        return _.includes(Array.from(filter.reportStatus), item.reportStatusCd);
                    });
                }                                
            }
        }
        return results;
    }
}
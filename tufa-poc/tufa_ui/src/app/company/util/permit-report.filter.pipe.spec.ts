import { PermitReportsDataFilterPipe } from './permit-report-filter.pipe';
import { PermitReportFilter } from '../models/permitreport-filter.model';
import data from './permit-report.json';

xdescribe('Pipe: PermitReportsDataFilterPipe', () => {
  let pipe = new PermitReportsDataFilterPipe();
  let filter = new PermitReportFilter();

  //specs
  xit('test permit report filter', () => {
    //must use arrow function for expect to capture exception
    // expect(()=>pipe.transform(data, filter)).toThrow();
    // expect(()=>pipe.transform(data, filter)).toThrowError('Requires a String as input');
    // expect(pipe.transform(data, filter)).toEqual('');
    // expect(pipe.transform(data, filter)).toEqual('WOW');  
  });
}) 
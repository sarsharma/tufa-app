import { PermitsCSVPipe } from './permits-csv-format.pipe';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

xdescribe('Pipe: PermitsCSVPipe', () => {
    let _sanitizer: DomSanitizer;
    let pipe = new PermitsCSVPipe(_sanitizer);
    let data:string;

  //specs
  xit('test permit report filter', () => {
    //must use arrow function for expect to capture exception
    expect(()=>pipe.transform('')).toThrow();
    expect(()=>pipe.transform(null)).toThrowError('Requires a String as input');
    expect(pipe.transform(data)).toEqual('');
    expect(pipe.transform(data)).toEqual('WOW');  
  });
}) 
import { Pipe, PipeTransform } from "@angular/core";
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Pipe({ name: "permitsCSVPipe" })
export class PermitsCSVPipe implements PipeTransform {
       
  constructor(private _sanitizer: DomSanitizer){}
  
  /**
   * 
   * 
   * @param {string} value
   * @returns {SafeHtml}
   * 
   * @memberOf PermitsCSVPipe
   */
  transform(value: string): SafeHtml {
    if(value){
      var str_array = value.split(',');
      if(str_array){
        for(var i = 0; i < str_array.length; i++) {
          var substr_array = str_array[i].split(':');
          if(substr_array[0] && substr_array[1]){
            var url = 'app/permit/history/' + substr_array[1];
            //str_array[i] = '<a href="' + url + '">' + (substr_array[0].substring(0,2) +"-"+ substr_array[0].substring(2,4) +"-"+ substr_array[0].substring(4)).toUpperCase().trim() + '</a>';
            str_array[i] = (substr_array[0].substring(0,2) +"-"+ substr_array[0].substring(2,4) +"-"+ substr_array[0].substring(4)).toUpperCase().trim();
          }
        }
      }
      value = str_array.join(', ');
    }

    return value;
  }   
}
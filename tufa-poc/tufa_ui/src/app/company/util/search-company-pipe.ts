import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'SearchCompanyPipe'
})

export class SearchCompanyPipe implements PipeTransform {

  transform(value, args?): Array<any> {
    let searchText = new RegExp(args, 'ig');
    if (value) {
      return value.filter(history => {
        if (history.companyName) {
          return history.companyName.search(searchText) !== -1;
        }
      });
    }
  }
}

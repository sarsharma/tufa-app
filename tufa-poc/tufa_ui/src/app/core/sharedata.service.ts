import { Injectable, Optional } from "@angular/core";
import { TrueUpComparisonResult } from "../recon/annual/models/at-comparisonresult.model";

export class ShareDataServiceConfig {
    userName = "App User";
}

let nextId = 1;

@Injectable()
export class ShareDataService {
    id = nextId++;
    companyName: string;
    ein: string;
    originalEIN: string;
    originalName: string;
    permitNum: string;
    permitType: string;
    tobaccoClass: string;
    status: string;

    period: string;

    companyId: number;
    periodId: number;
    permitId: number;
    assessmentId: number;
    detailsSrc: string;

    periodStatus: string; // alias reportstatus

    previousRoute: string;
    breadcrumb: string;
    quarter: number;
    fiscalYear: number;
    isRecon: boolean;
    reportType: string;
    acceptanceFlag: string;

    qatabfrom: string;
    filemgmttab: string;

    submitted: string;
    submittedDt: string;
    cigarsubmitted: string;
    createdDate: string;
    months: string[];
    docId: number;
    selectedDt: string;
    compareResult: TrueUpComparisonResult;

    //annual true up details properties
    disableAmendApprove: boolean;

    deltaExciseTaxFlag: string;

    prevTrueupInfoExpanded: boolean;
    prevTrueupInfoLoaded:boolean;

    /**
     * quarter -> side (I or t) -> month/other
     */
    expandedSectionsMap: Map<number, Map<'Fda' | 'Ingested', Map<number, boolean>>>;
    expandedSectionsMapManu: Map<number, Map<string, boolean>>;
    userExpanded: boolean = false;
    prevQuarter: number;


    private _userName = 'Sherlock Holmes';

    constructor(@Optional() config: ShareDataServiceConfig) {
        if (config) {
            this._userName = config.userName;
        }
        this.reset();

    }

    get userName() {
        // Demo: add a suffix if this service has been created more than once
        const suffix = this.id > 1 ? ` times ${this.id}` : '';
        return this._userName + suffix;
    }

    reset() {
        this.companyName = '';
        this.permitNum = '';
        this.permitType = '';
        this.period = '';
        this.reportType = '';
        this.acceptanceFlag = '';
        this.tobaccoClass = '';
        this.status = '';

        this.companyId = -1;
        this.periodId = -1;
        this.permitId = -1;
        this.assessmentId = -1;

        this.periodStatus = '';
        this.previousRoute = '';
        this.breadcrumb = '';
        this.quarter = -1;
        this.fiscalYear = -1;
        this.isRecon = false;
        this.qatabfrom = '';
        this.filemgmttab = '';

        this.submitted = '';
        this.cigarsubmitted = '';
        this.createdDate = '';
        this.months = [];
        this.detailsSrc = '';
        //this.submittedDt= '';
        this.docId = -1;
        this.deltaExciseTaxFlag = '';

    }

    public clearExpansionMaps() {
        if (this.expandedSectionsMap) {
            this.expandedSectionsMap.clear();
        } else {
            this.expandedSectionsMap = new Map();
        }
        if (this.expandedSectionsMapManu) {
            this.expandedSectionsMapManu.clear();
        } else {
            this.expandedSectionsMapManu = new Map();
        }
        this.userExpanded = false;
    }
    publicResetPrevTrueupInfoExpanded(){
        this.prevTrueupInfoExpanded = false;
    }
}

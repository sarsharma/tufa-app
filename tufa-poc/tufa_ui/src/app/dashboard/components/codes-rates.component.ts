import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin/services/admin.service';

declare var jQuery: any;

@Component({
    selector: 'codesrates',
    providers: [AdminService],
    templateUrl: './codes-rates.template.html'
})

export class CodesNrates implements OnInit {
    activeSheet: any;
    taxRateData: any[];
    htsCodes: any[] = [];
    effectiveDate: Date;
    sortBy = "htsCode";
    taxsorter = "tobaccoClassNm";
    nonConversionTypes = ['Cigars - Large', 'Cigars - Small', 'Cigars - Ad Valorem',
                        'Large', 'Small', 'Ad Valorem', 'Cigarettes'];
    cigarTypes = ['Large', 'Small', 'Ad Valorem'];

    constructor(private _adminService: AdminService) {
    }

    ngOnInit() {
        this.loadRates();
    }

    loadRates () {
     this._adminService.getTobaccoClasses().subscribe(tobaccoClasses => {
        if (tobaccoClasses) {
          this.taxRateData = <any[]>tobaccoClasses;
          this.effectiveDate = this.taxRateData[0].taxRateEffectiveDate;
          let strtId = Math.max.apply(Math, (<any[]>tobaccoClasses).map(o=>o.tobaccoClassId));
          // Initialize the class ids first
          for (let i in this.taxRateData) {
            if (this.taxRateData[i])
                this.taxRateData[i].tobaccoClassId = Number(strtId) + Number(i) + 1;
          }

          // Fetch HTS codes
          this._adminService.getHTSCodes().subscribe(htscodes => {
            if (htscodes) {
              //Initialize hts data
              this.htsCodes = [];
              for (let i in htscodes) {
                if (htscodes[i])
                  this.htsCodes.push({"htsCodeId" : htscodes[i].htsCodeId,
                                      "htsCode" : +htscodes[i].htsCode,
                                      "tobaccoClassName" : htscodes[i].tobaccoClass.tobaccoClassNm,
                                      "tobaccoClassId": htscodes[i].tobaccoClassId});
              }
            }
          });

           // Renaming the subtypes
          for (let i in this.taxRateData) {
            if (this.cigarTypes.indexOf(this.taxRateData[i].tobaccoClassNm) !== -1 && this.taxRateData[i].taxRate) {
               this.taxRateData[i].tobaccoClassNm = "Cigars - " + this.taxRateData[i].tobaccoClassNm;
            }
          }

          
        }
      });
   }
}

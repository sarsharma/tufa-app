import { Component, OnInit, OnDestroy } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { DashboardService } from './services/dashboard.service';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

declare var jQuery: any;
declare var tableau: any;

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.template.html'
})

export class Dashboard implements OnInit, OnDestroy {
    workbook: any;
    viz: any;
    activeSheet: any;
    ticket: any;
    trustedURL: any = environment.url + '/tableau/trusted';

    public dashboardReportTypes = [
        // { value:'CompanyPermitDetailMainDashboard',  display:'Company Permit Detail Main Dashboard' },
        // { value:'TaxesPaidStatisticsMainDashboard',  display:'Taxes Paid Statistics Main Dashboard' },
        // { value:'RemovalQuantityMainDashboard',  display:'Removal Quantity Main Dashboard' },
        // { value:'HighRemovalQuantityCompaniesMainDashboard',  display:'High Removal Quantity Companies Main Dashboard' },
        // { value:'ReportStatusMainDashboard',  display:'Report Status Main Dashboard' },        
        // { value:'CompanySubmissionMainDashboard',  display:'Company Submission Main Dashboard' },                
        // { value:'CompanyClassificationMainDashboard',  display:'Company Classification Main Dashboard' },                        
        // { value:'CompanyMarketShareMainDashboard',  display:'Company Market Share Main Dashboard' },
        // { value:'PermitsReportsMainDashboard',  display:'Permits Report Main Dashboard' }, 
        // { value:'MissingReportsMainDashboard',  display:'Missing Reports Main Dashboard' },
        // { value:'NotStartedReportsMainDashboard', display:'Not Started Reports Main Dashboard' }, 
        // { value:'PermitInformationDashboard', display:'Permit Information Dashboard' } 
    ];

    constructor(private _httpClient: HttpClient,
        private storage: LocalStorageService, private dashboardService: DashboardService) {

    }

    ngOnInit() {
        this.dashboardService.getTableauDashboardRptTypes().subscribe(
            data => {
                if (data) {
                    let dtypes = [];
                    for (let i in data) {
                        if (data[i].enable) {
                            dtypes.push(data[i]);
                        }
                    }
                    this.dashboardReportTypes = dtypes;
                    if (environment.apiurl !== 'http://localhost:8080') {
                        this.fetchTableauTicket();
                    }
                }
            });


    }

    ngOnDestroy() {
        this.disposeViz();
    }

    // Connect to Trusted Tableau Server and fetch the ticket number
    fetchTableauTicket() {
        var self = this;
        jQuery.post(this.trustedURL, { username: "Jason" })
            .done(function (data) {
                self.renderDefaultDashboard(data);
            }
            );
    }


    renderDefaultDashboard(data) {
        this.ticket = data;
        this.renderDashboard("ZeroReportsDashboard");
    }

    renderDashboard(reportName: string) {
       // document.getElementsByTagName('dashboard').item(0).getElementsByTagName('div').item(3)
       var vizContainer: HTMLElement = document.getElementById("vizContainer");
       // Check to see if the vizContainer is present on the DOM. If it isn't and we don't check it appends to the <body>
       if(vizContainer) {
            var url = this.trustedURL + '/' + this.ticket + "/views/FDATUFADashboards/" + reportName,
            options = {
                hideTabs: true,
                width: "1250px",
                height: "1000px",
                onFirstInteractive: function () {
                    //this.workbook = this.viz.getWorkbook();
                    //this.activeSheet = this.workbook.getActiveSheet();
                    //var sheets = this.workbook.getPublishedSheetsInfo();                    
                    //console.log("Run this code when the viz has finished loading.");
                }
            };

            // Create a viz object and embed it in the container div.
            this.viz = new tableau.Viz(vizContainer, url, options);
        }
    }

    // initViz(newurl) {
    //     let containerDiv = document.getElementById("vizContainer"),
    //         url = newurl,
    //         options = {
    //             hideTabs: true,                
    //             width: "1250px",
    //             height: "1000px", 
    //             onFirstInteractive: function() {
    //             }
    //         };

    //     this.viz = new tableau.Viz(containerDiv, url, options);
    // }

    renderSelectedDashboard(data, selectedReport) {
        this.ticket = data;
        this.renderDashboard(selectedReport);
    }

    resetViz(selectedReport) {
        this.disposeViz();
        var self = this;
        jQuery.post(this.trustedURL, { username: "Jason" })
            .done(function (data) {
                self.renderSelectedDashboard(data, selectedReport);
            }
            );
    };

    getSheetsAlertText(sheets) {
        let alertText = [];

        for (let i = 0, len = sheets.length; i < len; i++) {
            let sheet = sheets[i];
            alertText.push("  Sheet " + i);
            alertText.push(" (" + sheet.getSheetType() + ")");
            alertText.push(" - " + sheet.getName());
            alertText.push("\n");
        }

        return alertText.join("");
    }

    querySheets() {
        this.workbook = this.viz.getWorkbook();
        this.activeSheet = this.workbook.getActiveSheet();
        let sheets = this.workbook.getPublishedSheetsInfo();
        let text = this.getSheetsAlertText(sheets);
        text = "Sheets in the workbook:\n" + text;
    }

    changeReport(selectedReport: string) {
        if (selectedReport) {
            let newurl = this.trustedURL + '/' + this.ticket + "/views/FDATUFADashboards/" + selectedReport;
            this.resetViz(selectedReport);
        }
    }
    
    disposeViz() {
        if(this.viz != null){
            this.viz.dispose();
        }
    }
}

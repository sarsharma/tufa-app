import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AdminService } from '../admin/services/admin.service';
import { AuthService } from '../login/services/auth.service';
import { SharedModule } from '../shared/shared.module';
import { CodesNrates } from './components/codes-rates.component';
import { Dashboard } from './dashboard.component';
import { DashboardService } from './services/dashboard.service';

export const routes = [
    { path: '', component: Dashboard, pathMatch: 'full' },
    { path: 'codesrates', component: CodesNrates}
];

@NgModule({
    imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
    declarations: [Dashboard, CodesNrates],
    providers: [AdminService, AuthService, DashboardService]
})
  
export class DashboardModule {
    static routes = routes;
}

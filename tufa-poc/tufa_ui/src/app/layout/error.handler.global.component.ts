import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Component, ErrorHandler, Injector, ViewEncapsulation } from "@angular/core";
import { environment } from "../../environments/environment";

declare var jQuery: any;
@Component({
    selector: '[error-message]',
    templateUrl: './error.handler.global.modal.html',
    encapsulation: ViewEncapsulation.None,
})
export class TufaErrorHandler implements ErrorHandler {

    private errorUrl = environment.apiurl + '/api/v1/error/log';
    private modalShowing = false;
    refreshLink: string = environment.apiurl;
    errorMsg: string = '';
    showRefreshLink: boolean = true;
    
    constructor(private injector: Injector) {
        // let storedErros = sessionStorage.getItem('errors')
        // this.postError(storedErros);
    }

    handleError(error: Error | Response | String | string) {
        // always console log the error
        console.error(error);

        const date = new Date().toISOString();
        
        if(error instanceof Error) {
            //Some sort of client issue
            this.errorMsg = error.message;
        } else if (error instanceof Response) {
            this.errorMsg = error.statusText;
        } else if (error instanceof String || typeof error === 'string') {
            this.errorMsg = error.toString();
        } else {
            this.errorMsg = 'Error has occured but was not handled.';
        }
        this.errorMsg.substring(0, 1000);
        this.errorMsg = date + " " + this.errorMsg;

        console.debug(this.errorMsg);

        this.postError();
    }

    private postError() {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const http: HttpClient = this.injector.get(HttpClient);
        // const body = new String(error);

        this.showModal();

        let errors = this.getErrors();
        errors.push(this.errorMsg);

        http.post(this.errorUrl, errors, {headers: headers}).subscribe(
            data => {
                this.clearStoredErrors();
            },
            error => {
                //Save to local storage
                this.saveError(this.errorMsg);
                this.saveError(error);
            }
        );
    }

    private saveError(error) {
        let errors = this.getErrors();
        errors.push(error);
        sessionStorage["errors"] = errors;
    }

    private getErrors(): string[] {
        return sessionStorage.getItem("errors") ? [sessionStorage.getItem("errors")] : [];
    }

    private clearStoredErrors() {
        sessionStorage.removeItem('errors');
    }

    showModal() {
        let modal = jQuery("#error-message-modal");
        if (!this.modalShowing){
            this.modalShowing = true;
            modal.modal({
                toggle: 'show',
                backdrop: 'static',
                keyboard: false
            })
        }
    }

    hideModal() {
        let modal = jQuery("#error-message-modal");
        this.modalShowing = false;
        if (modal.is(':visible'))
            modal.modal("hide");
    }
}

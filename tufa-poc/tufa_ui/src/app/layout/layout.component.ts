import {
  Component,
  ViewEncapsulation,
  ElementRef, Renderer2,
  NgZone,
  ViewChild, HostBinding, OnInit
} from '@angular/core';
import {
  Router,
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router';
import { AppConfig } from '../app.config';

declare let jQuery: any;
declare let Hammer: any;

@Component({
  selector: 'app-layout',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./layout.style.scss'],
  templateUrl: './layout.template.html',
})
export class LayoutComponent implements OnInit {
  @HostBinding('class.nav-static') navStatic: boolean = true;
  @HostBinding('class.chat-sidebar-opened') chatOpened: boolean = false;
  @HostBinding('class.app') appClass: boolean = true;
  config: any;
  configFn: any;
  $sidebar: any;
  el: ElementRef;
  router: Router;
  @ViewChild('spinnerElement') spinnerElement: ElementRef;
  @ViewChild('routerComponent') routerComponent: ElementRef;

  navCollapsed: boolean = false;

  constructor(config: AppConfig,
              el: ElementRef,
              router: Router,
              private renderer: Renderer2,
              private ngZone: NgZone) {
    this.el = el;
    this.config = config.getConfig();
    this.configFn = config;
    this.router = router;
  }

  ngOnInit(): void {
    
  }

  private _hideSpinner(): void {
    // We wanna run this function outside of Angular's zone to
    // bypass change detection,
    this.ngZone.runOutsideAngular(() => {

      // For simplicity we are going to turn opacity on / off
      // you could add/remove a class for more advanced styling
      // and enter/leave animation of the spinner
      this.renderer.setStyle(
        this.spinnerElement.nativeElement,
        'opacity',
        '0'
      );
      this.renderer.setStyle(
        this.routerComponent.nativeElement,
        'opacity',
        '1'
      );
    });
  }
}

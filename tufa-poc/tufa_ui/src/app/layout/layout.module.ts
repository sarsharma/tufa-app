import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import 'jquery-slimscroll';
import { BsDropdownModule, TooltipModule } from 'ngx-bootstrap';
import { LayoutComponent } from './layout.component';
import { ROUTES } from './layout.routes';
import { NavbarComponent } from './navbar/navbar.component';
import { NotificationsLoadDirective } from './notifications/notifications-load.directive';
import { NotificationsComponent } from './notifications/notifications.component';
import { SessionExpiredComponent } from './session.expired.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TufaErrorHandler } from './error.handler.global.component';

@NgModule({
  imports: [
    CommonModule,
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    ROUTES,
    FormsModule
  ],
  declarations: [
    LayoutComponent,
    SidebarComponent,
    NavbarComponent,
    NotificationsComponent,
    NotificationsLoadDirective,
    SessionExpiredComponent,
    TufaErrorHandler
  ]
})
export class LayoutModule {
}

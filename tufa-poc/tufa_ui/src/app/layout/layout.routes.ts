import { Routes, RouterModule }  from '@angular/router';
import { LayoutComponent } from './layout.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  { path: '', component: LayoutComponent, children: [
    { path: 'dashboard', loadChildren: '../dashboard/dashboard.module#DashboardModule' },
    { path: 'company', loadChildren: '../company/components/company.module#CompanyModule' },
    { path: 'permit', loadChildren: '../permit/components/permit.module#PermitModule' },
    { path: 'assessments', loadChildren: '../recon/recon.module#ReconciliationModule' },
    { path: 'report', loadChildren: '../report/report.module#ReportModule' },
    { path: 'admin', loadChildren: '../admin/components/admin.module#AdminModule'},
    { path: 'permit-mgmt', loadChildren: '../permit-mgmt/permit-mgmt.module#PermitMgmtModule'}
  ]}
];

export const ROUTES = RouterModule.forChild(routes);

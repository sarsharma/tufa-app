import { AuthService } from "./../../login/services/auth.service";
import { Component, OnInit, ElementRef, Output } from "@angular/core";
import { AppConfig } from "../../app.config";
import { Router } from "@angular/router";
declare let jQuery: any;

@Component({
	selector: "app-navbar",
	styleUrls: ["./navbar.style.scss"],
	templateUrl: "./navbar.template.html"
})
export class NavbarComponent implements OnInit {
	$el: any;
	config: any;

	constructor(
		el: ElementRef,
		private router: Router,
		config: AppConfig,
		private auth: AuthService
	) {
		this.$el = jQuery(el.nativeElement);
		this.config = config.getConfig();
	}

	ngOnInit(): void {
		jQuery(".navbar .nav-item .nav-link").on("click", function() {
			jQuery(".navbar")
				.find(".active")
				.removeClass("active");
			jQuery(this).addClass("active");
		});
	}

	logout() {
		this.router.navigate(["/login"]);
	}

	setNavItemActive(elemID: any) {
		// console.log(elemID);
	}

	skip2Main(): void {
		jQuery('main.content').find('a,input,textarea,select,button').filter(':visible:first').focus();
	  }
}

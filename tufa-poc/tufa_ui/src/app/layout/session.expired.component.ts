import { Component, ViewEncapsulation } from '@angular/core';
import { AuthService } from '../login/services/auth.service';
import { Subscription, Observable, timer } from 'rxjs';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { RefreshTokenService } from '../shared/service/refresh.token.service';

declare var jQuery: any;

@Component({
    selector: '[session-expired-modal]',
    templateUrl: './session.expired.modal.html',
    encapsulation: ViewEncapsulation.None,
})

export class SessionExpiredComponent {

    timeToExpire: string;
    timeToExpireUOM: string;

    private showSessionExpiredModal: boolean = false;
    private countDownSeqGenerator: Observable<any>;
    private period: number = 1000;
    private timeBeforeExp: number;
    private timeToCntDwnSeq: number;

    private sessionExpSubscription: Subscription;
    private routerSubscription: Subscription;
    private callRefreshSessionSubscription: Subscription;

    private subscriptions: Subscription[] = [];

    private UOM_MINUTE = "minutes";
    private UOM_SECOND = "seconds";

    constructor(
        private authService: AuthService,
        private router: Router,
        private refreshTokenService: RefreshTokenService
    ) {
        this.routerSubscription = this.router.events.pipe(
            filter(event => event instanceof NavigationEnd)
        ).subscribe((e: NavigationEnd) => {
            if (e.url.includes("login"))
                return;

            if (this.authService.loggedIn())
                this.refreshSession();
        });
        this.subscriptions.push(this.routerSubscription);
        this.callRefreshSessionSubscription = this.refreshTokenService.callrefereshSession$.subscribe(
            () => {
                let isRefresh = true;
                let modal = jQuery("#session-expired-modal");
                if (modal && modal.is(':visible')) {
                    isRefresh = false;
                }
                if (this.authService.loggedIn() && isRefresh) {
                    this.refreshSession();
                }
            });
        this.subscriptions.push(this.callRefreshSessionSubscription);    
        this.timeBeforeExp = this.authService.getTokenExpWarnTime();
        this.createSessionExpirationTimer();
    }

    ngOnInit() { }

    ngOnDestroy() {
        // tslint:disable-next-line:forin
        for (let i in this.subscriptions) {
            this.subscriptions[i].unsubscribe();
        }

        if (this.routerSubscription)
            this.routerSubscription.unsubscribe();
    }


    showModal() {
        let modal = jQuery("#session-expired-modal");
        if (!modal.is(':visible')) {
            modal.modal({
                toggle: 'show',
                backdrop: 'static',
                keyboard: false
            })
        }
    }

    hideModal() {
        //let modal = jQuery("#session-expired-modal");
        //if (modal.is(':visible'))
        //  modal.modal("hide");
        jQuery('#session-expired-modal').modal('hide');
        jQuery('#session-expired-modal-content').removeAttr('style');
        jQuery('.modal-backdrop').remove();


    }

    hideErrorModal() {
        let modal = jQuery("#error-message-modal");
        if (modal && modal.is(':visible'))
            modal.modal("hide");
    }

    removeAbandonBackdrops() {
        let modalBackDrop = jQuery(".modal-backdrop");
        if (modalBackDrop)
            modalBackDrop.remove();
    }

    hideAllModals() {
        this.hideErrorModal();
        this.hideModal();
        this.removeAbandonBackdrops();
    };
    refreshSession() {
        this.cleanupSessionExpiration();
        this.authService.refreshToken().subscribe(
            data => {
                let body: any = data;
                if (body.token) {
                    this.authService.setAccessToken(body.token);
                    this.createSessionExpirationTimer();
                } else {
                    this.authService.removeToken();
                    this.router.navigate(['./../logout']);
                }
            }, error => {
                this.authService.removeToken();
                this.router.navigate(['./../logout']);
            });
    }

    logout() {
        this.cleanupSessionExpiration();
        // this.authService.logout();
        this.authService.removeToken();
        this.router.navigate(['./../logout']);
    }

    cleanupSessionExpiration() {
        this.hideModal();
        // tslint:disable-next-line:forin
        // for (let i in this.subscriptions) {
        //     this.subscriptions[i].unsubscribe();
        // }
        
        if (this.sessionExpSubscription)
            this.sessionExpSubscription.unsubscribe();
    }

    private createSessionExpirationTimer() {

        if (this.sessionExpSubscription != undefined)
            this.sessionExpSubscription.unsubscribe();

        let expirationTime = this.authService.getTokenExpirationTime();
        this.timeToCntDwnSeq = expirationTime - this.timeBeforeExp;
        this.countDownSeqGenerator = timer(this.timeToCntDwnSeq, this.period);
        this.sessionExpSubscription = this.countDownSeqGenerator.subscribe(
            cnt => {
                let timeToExp: number = (this.timeBeforeExp / 1000) - cnt;
                if ((timeToExp / 60) > 1) {
                    let secondsPart: string = String(timeToExp % 60);
                    if (secondsPart.length === 1) secondsPart = "0" + secondsPart;
                    this.timeToExpire = Math.floor(timeToExp / 60) + ":" + secondsPart;
                    this.timeToExpireUOM = this.UOM_MINUTE;
                } else if (timeToExp >= 0) {
                    this.timeToExpire = timeToExp.toString();
                    this.timeToExpireUOM = this.UOM_SECOND;
                }

                if (timeToExp < 0) {
                    this.cleanupSessionExpiration();
                    this.logout();
                } else {
                    this.showModal();
                }
            }
        );

        //this.subscriptions.push(this.sessionExpSubscription);

    }

}
import {Component, OnInit, ElementRef, AfterViewInit, Output, EventEmitter, Input, HostListener} from '@angular/core';
import { Router, NavigationEnd, RoutesRecognized, NavigationStart, NavigationCancel } from '@angular/router';
import { Location } from '@angular/common';
import { AppConfig } from '../../app.config';
import { LocalStorageService } from 'ngx-webstorage';
import { AuthService } from '../../login/services/auth.service';
import { ShareDataService } from '../../core/sharedata.service';
import { Subscription } from '../../../../node_modules/rxjs';
import { environment } from '../../../environments/environment';
import { timeout } from '../../../../node_modules/rxjs/operators';
declare let jQuery: any;

@Component({
  selector: '[sidebar]',
  templateUrl: './sidebar.template.html',
  styleUrls:['./sidebar.style.scss'],
  providers: [LocalStorageService]
})

export class SidebarComponent implements OnInit {
  $el: any;
  config: any;
  router: Router;
  location: Location;
  routerSubscription: Subscription;
  shareData: ShareDataService;
  @Input() navCollapsed: boolean = false;
  @Output() navCollapsedChange: EventEmitter<boolean> = new EventEmitter();

  constructor(
        private storage: LocalStorageService,
        config: AppConfig,
        el: ElementRef,
        router: Router,
        location: Location,
        public authService: AuthService,
        private _shareData: ShareDataService) {
    this.$el = jQuery(el.nativeElement);
    this.config = config.getConfig();
    this.router = router;
    this.location = location;
    this.shareData = _shareData;
  }

  @HostListener('window:resize')
  onResize($event) {
    if(window.innerWidth < 960) {
      this.navCollapsed = true;
      this.navCollapsedChange.emit(true);
    }
  }

  initSidebarScroll(): void {
    let $sidebarContent = this.$el.find('.js-sidebar-content');
    if (this.$el.find('.slimScrollDiv').length !== 0) {
      $sidebarContent.slimscroll({
        destroy: true
      });
    }
    $sidebarContent.slimscroll({
      height: window.innerHeight,
      size: '4px'
    });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.closeSidebar();
    }, 2000);
  }

  ngOnInit(): void {
    jQuery(window).on('sn:resize', this.initSidebarScroll.bind(this));
    this.initSidebarScroll();
  }


  /**
   * User goes the Daily Operations Landing page from the Side Navigation Menu
   * We need to clear the keyword search and other user settings from the local storage
   * 
   * @memberOf Sidebar
   */
  gotoSearchCompany() {
    // Clear all Search Criteria items from the local Storage
    this.storage.clear('SearchCompany');
    this.storage.clear('SearchReport');
    this.storage.clear('ReportStatusFilter');
    this.storage.clear('ReportMonthFilter');
    this.storage.clear('ReportYearFilter');
    this.storage.clear('sortPermitResultsBy');
    this.storage.clear('sortPermitResultsOrder');
    this.storage.clear('sortCompanyResultsBy');
    this.storage.clear('sortCompanyResultsOrder');

    // Since the Search Criteria default should be Report - we will save the default in the 
    // local Storage
    this.storage.store('SearchType', "REPORT");
  }

  hasRole(rolename): boolean{
      return this.authService.hasRole("ROLE_" + rolename);
  }

  toggleSidebar(): void {
    this.navCollapsed = !this.navCollapsed;
    this.navCollapsedChange.emit(this.navCollapsed);
  }

  closeSidebar(): void {
    this.navCollapsed = true;
    this.navCollapsedChange.emit(this.navCollapsed);
  }

  openSidebar(): void {
    this.navCollapsed = false;
    this.navCollapsedChange.emit(this.navCollapsed);
  }
 
}

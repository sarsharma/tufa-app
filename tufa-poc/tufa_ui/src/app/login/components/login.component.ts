import {
	AfterViewInit,
	Component,
	OnDestroy,
	OnInit,
	ViewEncapsulation
} from "@angular/core";
import { Router } from "@angular/router";
import { BsModalService } from "ngx-bootstrap/modal";
import { LocalStorageService } from "ngx-webstorage";
import { Credentials } from "../model/credentials.model";
import { AuthService } from "../services/auth.service";

declare var jQuery: any;

// Step 1: User logs in
// Step 2:
@Component({
	selector: "login",
	styleUrls: ["./login.style.scss"],
	templateUrl: "./login.template.html",
	encapsulation: ViewEncapsulation.None,
	providers: [BsModalService],
	host: {
		class: "login-page app"
	}
})

export class LoginComponent implements AfterViewInit {
  model= new Credentials('', '');
  showErrorFlag: boolean;
  alerts: Array<Object>;
  currentYear: string;
  showConfirmPopup: boolean = false;
  loginData: any;

  constructor(
    private storage: LocalStorageService,
    private authService: AuthService,
    private router: Router) {

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Error Logging in'
      }
      ];

      this.showErrorFlag = false;
      this.currentYear = new Date().getFullYear().toString();
    }

  ngOnInit(): void {
    this.login();  
    this.showErrorFlag = false;
  }

  ngAfterViewInit() {
      jQuery('#uname').focus();
  }

  /**
   * User goes the Daily Operations Landing page from the Side Navigation Menu
   * We need to clear the keyword search and other user settings from the local storage
   *
   * @memberOf Sidebar
   */
  clearLocalStorageContent() {
    // Clear all Search Criteria items from the local Storage
    this.storage.clear('SearchCompany');
    this.storage.clear('SearchReport');
    this.storage.clear('ReportStatusFilter');
    this.storage.clear('ReportMonthFilter');
    this.storage.clear('ReportYearFilter');
    this.storage.clear('sortPermitResultsBy');
    this.storage.clear('sortPermitResultsOrder');
    this.storage.clear('sortCompanyResultsBy');
    this.storage.clear('sortCompanyResultsOrder');

    // Since the Search Criteria default should be Report - we will save the default in the
    // local Storage
    this.storage.store('SearchType', "REPORT");
  }

  login() {
    this.authService.login(this.model)
    .subscribe(
      data => {
          if (data){
              this.loginData = data;
              jQuery('#splash-dialog').modal({backdrop: 'static', keyboard: false}); 
              jQuery('#splash-dialog').modal('show');  
              this.authService.isLoggedIn = true;
          }
      }, error => {
        let body = error;
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({type: 'warning', msg: body.message});
      });
  }

  logout() {
  	this.router.navigate(['logout']);
    this.authService.removeToken();
  }
  
  accessFDA(){
    let body = this.loginData;
	  this.authService.setAccessToken(body.accesstoken);
	  this.authService.setRefreshToken(body.refreshtoken);    
	
	  if (this.authService.hasRole('ROLE_ADMIN')) {
    	this.router.navigate(['app/admin/dashboard']);
  	}else {
    	this.clearLocalStorageContent();
    	this.router.navigate(['app/company/search', { q: 'new'}]);   	
  	}	
   	jQuery('#splash-dialog').modal('hide');  	
  }

}

import { Component, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Credentials } from '../model/credentials.model';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { UserDetails } from '../model/userdetails.model';

declare var jQuery: any;
@Component({
  selector: '[logout]',
  styleUrls: ['./login.style.scss'],
  templateUrl: './logout.template.html',
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'login-page app'
  }
})

export class LogoutComponent {
  showErrorFlag: boolean;
  alerts: Array<Object>;
  currentYear: string;
  
  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute) {

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Error Logging in'
      }
    ];

    this.showErrorFlag = false;
    this.currentYear = new Date().getFullYear().toString();    
  }

  ngOnInit(): void {
    this.showErrorFlag = false;
    this.authService.clearStorage();
  }

  login() {
    this.router.navigate(['app/admin/dashboard']);
  }

  logout() {
    this.router.navigate(['logout']);
  }
}

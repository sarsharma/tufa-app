/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, Input, OnInit, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';

declare var jQuery: any;

/**
 * 
 * 
 * @export
 * @class ConfirmPopup
 */
@Component({
  selector: '[splash-dialog]',
  templateUrl: './splash-dialog.template.html'
})

export class SplashDialog implements OnInit {
  busy: Subscription;
  // @Input() model: any;
  @Output() callBack = new EventEmitter<boolean>();
  @Output() hideModal = new EventEmitter<boolean>();
  // currentModel: any;
  @Input() msg: string;
  @Input() title: string;

  ngOnInit() {
  }

  constructor() {

  }

  reset() {
    jQuery('#splash-dialog').modal('hide');
    this.hideModal.emit(true);
  }

  delete() {
    this.callBack.emit();
  }

}

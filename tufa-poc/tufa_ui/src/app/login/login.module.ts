import { CommonModule, TitleCasePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { SharedModule } from "./../shared/shared.module";
import { LoginComponent } from "./components/login.component";
import { SplashDialog } from "./components/splash-dialog.component";
import { LoginMessageBox } from "./components/lg-msgbox.component";
import { LogoutComponent } from "./components/logout.component";

export const routes = [
  {path: '', component: LoginComponent}
];

@NgModule({
  declarations: [
    LoginComponent, SplashDialog, LoginMessageBox, LogoutComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  providers: [
  ]
})
export class LoginModule {
	static routes = routes;
}

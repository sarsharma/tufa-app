export class UserDetails{
    
    private _resources= [];
    private _functionGrps;
    private  _role;
    private _username;
    private _funcGrpsJsonGrid = [];
    
    public roles = [];

    constructor(){
        
        /*var roles = this._userdetails[0];
        this._role = roles.name;
        this._username = roles.username;
        var  funcGrps = this._functionGrps = roles.resources;
               
        for(var resource in funcGrps){
            var item = {};
            item["resource"] = resource;
            item["privilege"] = funcGrps[resource];
            this._funcGrpsJsonGrid.push(item);
            this._resources.push(resource);
        }*/
    }
    
    public getResources(){
        return this._resources;
    }
    
    public getFuncGrps(){
        return this._functionGrps;
    }
    
    public getRoleName(){
        return this._role;
    }
    
    public getUsername(){
        return this._username;
    }
    
    public getfuncGrpsJsonGrid(){
        return this._funcGrpsJsonGrid;
    }

    public setRoles(roles){
            let rolesArry = roles.split(",");
            for(var i in rolesArry){
                    this.roles.push(rolesArry[i]);
            }
    }

    public getRoles(): String []{
        return this.roles;
    }
    public hasRole(roleName) : boolean{
            for(var i in this.roles){
                    if(this.roles[i] === roleName)
                        return true;
            }
            return false;
    }
}
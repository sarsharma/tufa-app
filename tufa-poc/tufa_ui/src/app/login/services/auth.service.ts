import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";
//import { CookieService } from "../../../../node_modules/angular2-cookie/core";
import { CookieService } from 'ngx-cookie';
import { Observable, of, Subject } from "rxjs";
import { Credentials } from "../model/credentials.model";
import { environment } from "./../../../environments/environment";


const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

const httpLoginOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json", 'oam_remote_user': 'user@test.com' })
};

@Injectable()
export class AuthService {
	setAccessTokenCookie(): any {
		// throw new Error("Method not implemented.");
		this.cookieService.put('X-Authorization', 'Bearer ' + this.getAccessToken());
	}
	removeAccessTokenCookie(): any {
		// throw new Error("Method not implemented.");
		this.cookieService.remove('X-Authorization');
	}
	accessToken: string = null;
	isLoggedIn: boolean = false;
	credentialPromise: Promise<any>;
	public email_verified: boolean = true;

	jwtHelper = new JwtHelperService();

	private authloginurl = environment.apiurl + "/api/auth/login";
	private authlogouturl = environment.apiurl + "/logout";
	private tokenRefreshUrl = environment.apiurl + '/api/auth/token';

	/* session timeout */
	public expiring: Subject<boolean> = new Subject<boolean>();

	constructor(private http: HttpClient, private cookieService: CookieService) {
	}

	public login(credentials: Credentials): Observable<any> {
		return this.http.post(this.authloginurl, credentials, httpLoginOptions);
	}


	public logout() {
		this.http.get(this.authlogouturl, httpOptions).subscribe();
	}

	public getResourceId() {
		let token = sessionStorage.getItem("access_token_id");
		let resourceid = this.jwtHelper.decodeToken(token).resourceid;
		return resourceid;
	}

	public getEmployeeId() {
		let token = sessionStorage.getItem("access_token_id");
		let resourceid = this.jwtHelper.decodeToken(token).employeeid;
		return resourceid;
	}

	private getLoggedUsername() {
		let token = sessionStorage.getItem("access_token_id");
		let resourceid = this.jwtHelper.decodeToken(token).fullname;
		return resourceid;
	}

	public setAccessToken(accesstoken) {
		sessionStorage.setItem("access_token_id", accesstoken);
	}

	public getAccessToken() {
		return sessionStorage.getItem("access_token_id");
	}

	public removeAccessToken() {
		sessionStorage.removeItem("access_token_id");
	}

	public removeToken() {
		sessionStorage.removeItem("access_token_id");
		sessionStorage.removeItem("refresh_token_id");
	}

	public setRefreshToken(refreshtoken) {
		sessionStorage.setItem("refresh_token_id", refreshtoken);
	}

	public getRefreshToken() {
		return sessionStorage.getItem("refresh_token_id");
	}

	public loggedIn() {
		let token = sessionStorage.getItem("access_token_id");
		if (!token) return false;
		return !this.jwtHelper.isTokenExpired(token);
	}

	public getScopes(): any[] {
		let token = sessionStorage.getItem("access_token_id");
		if (token) {
			let scopes = this.jwtHelper.decodeToken(token).scopes;
			return scopes;
		} else {
			return [];
		}
	}

	public refreshToken() {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('X-Authorization', 'Bearer ' + this.getRefreshToken());
		return this.http.get(this.tokenRefreshUrl, httpOptions);
	}

	public getTokenExpirationTime(): number {
		let token = sessionStorage.getItem("access_token_id");
		let expTime: number = this.jwtHelper.decodeToken(token).exp;
		let issuedAtTime: number = this.jwtHelper.decodeToken(token).iat;
		return (expTime - issuedAtTime) * 1000;
	}

	public getTokenExpWarnTime(): number {
		let token = sessionStorage.getItem("access_token_id");
		let expWarnTime: number = this.jwtHelper.decodeToken(token).expWarnTime;
		return Number(expWarnTime) * 60 * 1000;
	}

	public hasRole(roleName): boolean {
		let scopes = this.getScopes();
		for (let i in scopes) {
			if (scopes[i] === roleName) return true;
		}
		return false;
	}

	getSessionUser(): string {
		let token = sessionStorage.getItem("access_token_id");
		let user = this.jwtHelper.decodeToken(token).sub;
		return user;
	}

	public clearStorage() {
		sessionStorage.clear();
	}


	public getfullName() {
		let token = sessionStorage.getItem("access_token_id");
		let fullname = this.jwtHelper.decodeToken(token).fullname;
		return fullname;
	}

	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		// this.messageService.add('Attribute Service: ${message}');
	}
}

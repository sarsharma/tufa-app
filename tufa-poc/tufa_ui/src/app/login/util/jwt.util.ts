import {Injectable} from '@angular/core';
// import { tokenNotExpired } from '@uath0/angular-jwt';

@Injectable()
export class JwtUtil {
    
    createJWTAuthorizationHeader(config) {
        var token = this.getToken();
        if (token) {
            config.headers.Authorization = 'Bearer ' + token;
        }
        return config;
    }

    parseJwt(token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(atob(base64));
    }

    saveToken(token) {
        localStorage['id_token'] = token;
    };

    getToken() {
        return localStorage['id_token'];
    }

    removeToken() {
        localStorage.removeItem('id_token');
    }

    // tokenExpired():boolean{
    //     return tokenNotExpired();
    // }
}
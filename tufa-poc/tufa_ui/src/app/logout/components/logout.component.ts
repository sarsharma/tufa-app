import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../login/services/auth.service';

declare var jQuery: any;
@Component({
  selector: '[logout]',
  styleUrls: ['./login.style.scss'],
  templateUrl: './logout.template.html',
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'login-page app'
  }
})

export class LogoutComponent {
  showErrorFlag: boolean;
  alerts: Array<Object>;
  currentYear: string;

  constructor(
    private router: Router,
    private authService: AuthService,
    private route: ActivatedRoute, ) {

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Error Logging in'
      }
    ];

    this.showErrorFlag = false;
    this.authService.removeToken();
  }

  ngOnInit(): void {
    // this.authService.logout();
    this.showErrorFlag = false;
    this.authService.removeToken();
  }

  login() {
    this.router.navigate(['app/admin/dashboard']);
  }

  logout() {
    this.router.navigate(['logout']);
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from './../shared/shared.module';
import { LogoutComponent } from './components/logout.component';
import { JwtUtil } from './util/jwt.util';

export const routes = [
  {path: '', component: LogoutComponent}
];

@NgModule({
  declarations: [ LogoutComponent ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    JwtUtil
  ]
})
export class LogoutModule {
  static routes = routes;
}

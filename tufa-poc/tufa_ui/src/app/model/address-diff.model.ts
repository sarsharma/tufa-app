export class AddressDiff {
    attentionDiff: boolean;
    streetAddressDiff: boolean;
    suiteDiff: boolean;
    cityDiff: boolean;
    stateDiff: boolean;
    provinceDiff: boolean;
    postalCdDiff: boolean;
    faxNumDiff: boolean;
    countryCdDiff: boolean;
    countryNmDiff: boolean;

    AddressDiff(){
        this.attentionDiff = false;
        this.streetAddressDiff = false;
        this.suiteDiff = false;
        this.cityDiff = false;
        this.stateDiff = false;
        this.provinceDiff = false;
        this.postalCdDiff = false;
        this.faxNumDiff = false;
        this.countryCdDiff = false;
        this.countryNmDiff = false;     
    }
}

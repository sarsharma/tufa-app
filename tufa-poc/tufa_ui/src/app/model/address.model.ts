/*
@author : Deloitte
Class to hold address info
*/

/**
 * 
 * 
 * @export
 * @class Address
 */
export class Address {
    companyId: string;
    addressTypeCd: string;
    attention: string;
    streetAddress: string;
    suite: string;
    city: string;
    state: string;
    province: string;
    postalCd: string;
    faxNum: number;
    countryCd: string;
    countryNm: string;
    isIngestedStateValid:boolean;
}

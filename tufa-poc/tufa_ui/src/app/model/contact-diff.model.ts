export class ContactDiff {

    lastNmDiff: boolean;
    firstNmDiff: boolean;
    phoneNumDiff: boolean;
    phoneExtDiff: boolean;
    emailAddressDiff: boolean;
    countryCdDiff: boolean;
    cntryDialCdDiff: boolean;
    faxNumDiff: boolean;
    cntryFaxDialCdDiff: boolean;

    ContactDiff(){
        this.lastNmDiff = false;
        this.firstNmDiff = false;
        this.phoneNumDiff = false;
        this.phoneExtDiff = false;
        this.emailAddressDiff = false;
        this.countryCdDiff = false;
        this.cntryDialCdDiff = false;
        this.faxNumDiff = false;
        this.cntryFaxDialCdDiff = false;
    }
  
  }
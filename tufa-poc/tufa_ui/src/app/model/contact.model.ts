/*
Class to hold contact info
*/

import { Company } from '../company/models/company.model';

/**
 * 
 * 
 * @export
 * @class Contact
 */
export class Contact {

  contactId: number;
  companyId: number;
  lastNm: string;
  firstNm: string;
  phoneNum: string;
  phoneExt: string;
  emailAddress: string;
  countryCd: string;
  cntryDialCd: string;
  faxNum: number;
  cntryFaxDialCd: string;
  company: Company;
  primaryContactSequence: number;

  setAsPrimary: boolean = false;
}

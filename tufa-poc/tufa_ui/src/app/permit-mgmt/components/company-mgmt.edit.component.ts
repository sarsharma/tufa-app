import { Component, EventEmitter, Input, Output, SimpleChange, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { Company } from '../../company/models/company.model';
import { CompanyService } from '../../company/services/company.service';
import { PermitMgmtService } from '../services/permit-mgmt.service';
import { PermitUpdate } from './ingestion/models/permit-update.model';

declare var jQuery: any;

@Component({
    selector: '[company-mgmt-edit]',
    templateUrl: './company-mgmt.edit.template.html',
    encapsulation: ViewEncapsulation.None

})

export class CompanyMgmtEditComponent {

    @Input() permitUpdate: PermitUpdate;
    @Output() public mvNextRecord = new EventEmitter<PermitUpdate>();

    disableButton: boolean = false;
    companyCreateDt: string = '';

    busy: Subscription;

    alerts = [
        {
            type: 'success',
            msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Successfully Updated Company Name.</span>'
        }
    ];

    constructor(private companyService: CompanyService,
        private permitMgmtService: PermitMgmtService) {

    }


    public ngOnChanges(changes: { [key: string]: SimpleChange }): any {

        if (changes["permitUpdate"]) {
            let permitUpdate: PermitUpdate = changes["permitUpdate"].currentValue;
            if (permitUpdate && permitUpdate.companyStatusChangeAction && !permitUpdate.companyStatusChangeAction.includes('Conflict Ignored')) {
                this.disableButton = true;
            } else
                this.disableButton = false;
        }


    }



    hidePanel() {
        this.mvNextRecord.emit(null);
    }

    public updateCompany() {

        let company: Company = this.permitUpdate.companyTufa;
        this.companyCreateDt = this.permitUpdate.companyTufa.companyCreateDt;
        this.permitUpdate.companyTufa = null;
        this.permitUpdate.permitTufa = null;
        company.permitAudit = this.permitUpdate;

        company.legalName = this.permitUpdate.companyName;
        company.einNumber = this.permitUpdate.einNum ? this.permitUpdate.einNum.replace("-", "") : this.permitUpdate.einNum;
        company.permitAudit.companyStatusChangeAction = 'Company Name Changed to ' + this.permitUpdate.companyName;
        company.permitSet = null;
        company.companyCreateDt = this.companyCreateDt;

        //let companyUpdate = _.pickBy(company, function(prop){ if(prop) return true;});

        this.busy = this.companyService.updateCompany(company).subscribe(
            data => {

                if (data) {
                    let company: Company = <Company>data;
                    this.permitUpdate = company.permitAudit;
                    this.mvNextRecord.emit(this.permitUpdate)
                }
            }
        )

    }

    public ignoreCompanyStatus() {

        this.permitUpdate.companyStatusChangeAction = "Conflict Ignored";
        this.busy = this.permitMgmtService.insertPermitUpdate(this.permitUpdate).subscribe(data => {
            if (data) {
                let pUpdate = data;
                this.mvNextRecord.emit(pUpdate);
            }
        })

    }

    skipCompanyStatus() {
        this.permitUpdate.uiAction = "SKIP";
        this.mvNextRecord.emit(this.permitUpdate);
    }
    getFullText(abbrv) {
        if (abbrv == 'ACT' || abbrv == 'ACTV') return 'ACTIVE';
        else if (abbrv == 'CLS' || abbrv == 'CLSD') return 'CLOSED';
        else return 'N/A';
    }

    ngDestroy() {
        if (this.busy)
            this.busy.unsubscribe();
    }

}
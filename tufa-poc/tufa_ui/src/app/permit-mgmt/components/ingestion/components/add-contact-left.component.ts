/*
@author : Deloitte
this is Componentfor adding contact as a popup.
*/
import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { Company } from '../../../../company/models/company.model';
import { Contact } from '../../../../model/contact.model';
import { CompanyEventService } from '../services/company.event.service';
import { CompanyService } from '../services/company.service';
import { ContactService } from '../services/contact.service';

declare var jQuery: any;

@Component({
  selector: '[add-contact-left]',
  templateUrl: './add-contact-left.template.html',
  providers: [ContactService]
})

export class AddContactLeftComponent {

  @Input() companyData: any;
  @Input() contacts: Contact[] = [];
  @Output() updatedId = new EventEmitter<any>();

  selContact = new Contact();
  contactsCount = 0;
  selContactIndex = 0;

  // @Input() dataModel: any;
  // @Input() company: any;
  // @Input() contactId: any;
  // @Output() onSaving = new EventEmitter<any>();
  // @Output() cancel = new EventEmitter<any>();
  index: any;
  stageId: any;
  showErrorFlag: boolean;
  contactStatus: any = "";
  alerts: Array<Object>;
  public mask = ['+', /\d/, /\d/, /\d/];
  public extmask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/];


  constructor(private contactService: ContactService, private companyService: CompanyService, private companyEventService: CompanyEventService) {
    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];
    this.selContact.cntryDialCd = "1";
    this.selContact.cntryFaxDialCd = "1";
    this.showErrorFlag = false;
    this.companyEventService.contactLoad$.subscribe(
      index => {
        if (index) {
          this.index = index;
        }
      }
    );
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "companyData") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.companyData = new Company();
          this.companyData.legalName = chg.currentValue.legalName;
          this.companyData.companyId = chg.currentValue.companyId;
        }
      }
      if (propName === "contacts") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.contacts = [];
          this.contacts = chg.currentValue;
          this.contactsCount = this.contacts.length;
          this.selContact = new Contact();
          if (this.contactsCount > 0) {
            this.selContact = this.contacts[0];
            this.selContactIndex = 1;
          }
        }
      }
    }
  }

  gotoFirst() {
    if (this.contactsCount > 0) {
      this.selContact = this.contacts[0];
      this.selContactIndex = 1;
      this.updatedId.emit(this.selContact.contactId);
    }
  }

  gotoNext() {
    if (this.selContactIndex < this.contactsCount) {
      this.selContact = this.contacts[this.selContactIndex++];
      this.updatedId.emit(this.selContact.contactId);
    }
  }

  gotoPrev() {
    if (this.selContactIndex > 1) {
      this.selContact = this.contacts[--this.selContactIndex - 1];
      this.updatedId.emit(this.selContact.contactId);
    }
  }

  gotoLast() {
    if (this.contactsCount > 0) {
      this.selContact = this.contacts[this.contacts.length - 1];
      this.selContactIndex = this.contacts.length;
      this.updatedId.emit(this.selContact.contactId);
    }
  }
}


/*
@author : Deloitte
this is Componentfor adding contact as a popup.
*/
import { ComponentFixture, async, inject, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core'; 
import { CommonModule }  from '@angular/common';
import { RouterModule, Routes, provideRoutes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ContactService } from '../services/contact.service';
import { SharedModule } from '../../../../shared/shared.module';
import { HttpModule } from '../../../../../../node_modules/@angular/http';
import { AddContactRightComponent } from './add-contact-right.component';
import { HttpClient } from '../../../../../../node_modules/@angular/common/http';
import { AuthorizationHelper } from '../../../../authentication/util/authorization.helper.util';

declare var jQuery: any;

describe('Add Contact', () => {
  let comp:    AddContactRightComponent;
  let fixture: ComponentFixture<AddContactRightComponent>;

  let locFirstName = By.css('#first-name');
  let locLastName = By.css('#last-name');
  let locSaveButton = By.css('#btn-save');
  let locEmail = By.css('#email-address');
  let locPhCountryCode = By.css('#phone-countrycode');
  let locPhoneNumber = By.css('#phone-number');
  let locExtOptional = By.css('#ext-optional');
  let locFaxOptional = By.css('#fax-optional');

  // provide our implementations or mocks to the dependency injector
  beforeEach( async( () => {
    TestBed.configureTestingModule({
      imports: [ SharedModule, HttpModule, RouterTestingModule ],
      declarations: [AddContactRightComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [HttpClient, AuthorizationHelper, ContactService,
          provideRoutes([{path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTesting',
          pathMatch: 'full'}])
      ]
    }).compileComponents();
  }));

   beforeEach(() => {
    fixture = TestBed.createComponent(AddContactRightComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create component', () => {
    expect(comp).toBeTruthy();
    // Verify if control exists
    expect(fixture.debugElement.nativeElement.querySelector('#first-name') === null).toBe(false);
    expect(fixture.debugElement.nativeElement.querySelector('#last-name') === null).toBe(false);
    expect(fixture.debugElement.nativeElement.querySelector('#email-address') === null).toBe(false);

  });

  it('should behave when invalid data is entered...', function(){

    // validate if all required fields are empty
    let deFirstname = fixture.debugElement.query(locFirstName);
    deFirstname.nativeElement.value = '';
    deFirstname.nativeElement.dispatchEvent(new Event('input'));

    let deLastname = fixture.debugElement.query(locLastName);
    deLastname.nativeElement.value = '';
    deLastname.nativeElement.dispatchEvent(new Event('input'));

    let deEmail = fixture.debugElement.query(locEmail);
    deEmail.nativeElement.value = '';
    deEmail.nativeElement.dispatchEvent(new Event('input'));

    expect(comp.contact.firstNm).toBe('');
    expect(comp.contact.lastNm).toBe('');
    expect(comp.contact.emailAddress).toBe('');

    let debtnSave = fixture.debugElement.query(locSaveButton);
    debtnSave.nativeElement.dispatchEvent(new Event('click'));

    expect(deFirstname.nativeElement.className).toContain('parsley-error');
    expect(deLastname.nativeElement.className).toContain('parsley-error');
    expect(deEmail.nativeElement.className).toContain('parsley-error');

    deEmail.nativeElement.value = 'test';
    debtnSave.nativeElement.dispatchEvent(new Event('click'));
    expect(deEmail.nativeElement.className).toContain('parsley-error');

    deFirstname.nativeElement.value = 'John';
    deFirstname.nativeElement.dispatchEvent(new Event('input'));
    expect(comp.contact.firstNm).toBe('John');
    debtnSave.nativeElement.dispatchEvent(new Event('click'));
    expect(deFirstname.nativeElement.className).toContain('parsley-success');

    deLastname.nativeElement.value = 'Smith';
    deLastname.nativeElement.dispatchEvent(new Event('input'));
    expect(comp.contact.lastNm).toBe('Smith');
    debtnSave.nativeElement.dispatchEvent(new Event('click'));
    expect(deLastname.nativeElement.className).toContain('parsley-success');

    deEmail.nativeElement.value = 'test@email.com';
    debtnSave.nativeElement.dispatchEvent(new Event('click'));
    expect(deEmail.nativeElement.className).toContain('parsley-success');

  });

});

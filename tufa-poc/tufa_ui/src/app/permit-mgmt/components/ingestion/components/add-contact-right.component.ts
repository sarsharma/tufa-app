/*
@author : Deloitte
this is Componentfor adding contact as a popup.
*/
import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { ContactDiff } from '../../../../model/contact-diff.model';
import { Contact } from '../../../../model/contact.model';
import { CompanyEventService } from '../services/company.event.service';
import { CompanyService } from '../services/company.service';
import { ContactService } from '../services/contact.service';

declare var jQuery: any;

@Component({
  selector: '[add-contact-right]',
  templateUrl: './add-contact-right.template.html',
  styleUrls: ['./add-contact-right.style.scss'],
  providers: [ContactService]
})

export class AddContactRightComponent {

  @Input() diffIndicator: any;
  @Input() dataModel: any;
  @Input() company: any;
  @Input() contactId: any;
  @Output() onSaving = new EventEmitter<any>();
  @Output() cancel = new EventEmitter<any>();

  public contact = new Contact();
  public diffmodel = new ContactDiff();
  public index: any;
  public stageId: any;
  public showErrorFlag: boolean;
  public contactStatus: any = "";
  public alerts: Array<Object>;
  public mask = ['+', /\d/, /\d/, /\d/];
  public extmask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
  public busy: Subscription;

  constructor(private contactService: ContactService, private companyService: CompanyService, private companyEventService: CompanyEventService) {
    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];
    this.contact.cntryDialCd = "1";
    this.contact.cntryFaxDialCd = "1";
    this.showErrorFlag = false;
    this.companyEventService.contactMatch$.subscribe(contact => {
      if (contact) this.loadContact(contact);
    });
    this.companyEventService.contactLoad$.subscribe(
      index => {
        if (index) {
          this.index = index;
        }
      }
    );
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "dataModel") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.contact = new Contact();
          if (chg.currentValue.email) {
            this.contact.emailAddress = chg.currentValue.email.trim();
          }
          this.contact.phoneNum = chg.currentValue.premisePhone;
          this.determineCntryDialCd();
          this.contactStatus = chg.currentValue.contactStatus;
          this.contact.phoneExt = "";
          this.contact.cntryFaxDialCd = "";
        }
      }

      if (propName === "diffIndicator") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.diffmodel = chg.currentValue;
        }
      }
    }
  }

  ngOnDestroy() {
    if (this.busy) {
      this.busy.unsubscribe();
    }
  }

  loadContact(contact: Contact) {
    this.contact.firstNm = contact.firstNm ? contact.firstNm : this.contact.firstNm;
    this.contact.lastNm = contact.lastNm ? contact.lastNm : this.contact.lastNm;
    if (contact.emailAddress == "") this.contact.emailAddress = "";
    else { this.contact.emailAddress = contact.emailAddress ? contact.emailAddress : this.contact.emailAddress; }
    if (contact.phoneNum == "") this.contact.phoneNum = "";
    else { this.contact.phoneNum = contact.phoneNum ? contact.phoneNum : this.contact.phoneNum; }
    if (contact.phoneExt == "") this.contact.phoneExt = "";
    else { this.contact.phoneExt = contact.phoneExt ? contact.phoneExt : this.contact.phoneExt; }
    this.contact.faxNum = contact.faxNum ? contact.faxNum : this.contact.faxNum;
    this.determineCntryDialCd();
  }

  save(saveStatus) {

    if (saveStatus === "PRIMARY") {
      this.contact.setAsPrimary = true;
      saveStatus = "ADDED";
    }

    if (saveStatus !== "SKIP" && saveStatus !== "IGNR") {
      this.contact.emailAddress = this.contact.emailAddress ? (this.contact.emailAddress).replace(/ /g, '') : this.contact.emailAddress;
      jQuery('#contactright').parsley().validate();
      // Toggle to edit mode or navigate to the next screen if validation passes
      if (jQuery('#contactright').parsley().isValid()) {
        this.dataModel.contactStatus = saveStatus !== "SKIP" ? saveStatus : this.dataModel.contactStatus;
        this.contact.company = this.company;
        this.contact.phoneNum = this.contact.phoneNum ? (this.contact.phoneNum).replace(/[^0-9]/g, '') : this.contact.phoneNum;
        if (saveStatus !== "ADDED") {
          this.contact.contactId = this.contactId;
          this.busy = this.contactService.updateContact(this.contact).subscribe(data => {
            if (data) {
              this.onSaving.emit(this.dataModel);
            }
          },
            error => {
              this.showErrorFlag = true;
              this.alerts = [];
              this.alerts.push({ type: 'warning', msg: error });
            });
        } else {
          this.busy = this.contactService.createContact(this.contact).subscribe(data => {
            if (data) {
              this.onSaving.emit(this.dataModel);
            }
          },
            error => {
              this.showErrorFlag = true;
              this.alerts = [];
              this.alerts.push({ type: 'warning', msg: error });
            });
        }
      }
    } else if (saveStatus === "SKIP") { this.onSaving.emit("SKIP"); }
    else { this.dataModel.contactStatus = saveStatus; this.onSaving.emit(this.dataModel); }
  }

  checkDiff(str1, str2) {
    return String(jQuery("div#contact-right").find("#" + str2).val().trim()).toLowerCase() == String(jQuery("div#contact-left").find("#" + str1).val().trim()).toLowerCase();
  }

  determineCntryDialCd() {
    if (this.contact.phoneNum) {
      this.contact.cntryDialCd = this.contact.phoneNum.replace(/-/g, '').length === 10 ? "1" : "";
    } else {
      this.contact.cntryDialCd = "";
    }
  }
}

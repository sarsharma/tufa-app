
import { Component, Input, OnDestroy, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from "lodash";
import { Subscription } from 'rxjs';
import { ShareDataService } from '../../../../core/sharedata.service';
import { AddressDiff } from '../../../../model/address-diff.model';
import { Address } from '../../../../model/address.model';
import { PermitPeriodService } from '../../../../permit/services/permit-period.service';
import { CompanyPermitHistory } from '../models/company-permit-history.model';
import { AddressService } from '../services/address.service';
import { CompanyEventService } from '../services/company.event.service';
import { CompanyService } from '../services/company.service';
import { FileDataFilterPipe } from '../util/file.filter.pipe';
import * as statesJson from './states.json';
import { Permit } from '../../../../permit/models/permit.model';

declare var jQuery: any;

@Component({
    selector: '[address-mgmt]',
    templateUrl: './address-mgmt.template.html',
    styleUrls: ['./address-mgmt.style.scss'],
    providers: [CompanyService, AddressService, FileDataFilterPipe],
    encapsulation: ViewEncapsulation.None
})

/**
 * SearchCriteriaPage is the Daily Operations Landing Page
 *
 * @export
 * @class SearchCriteriaPage
 * @implements {OnInit}
 */
export class AddressMgmtPage implements OnInit, OnDestroy {

    @Input() companyData: any;
    @Input() switch;
    public route: ActivatedRoute;
    public router: Router;
    public reportData: any[];
    public addressData: any;
    public addressId: number;
    public nextInd: number;
    addressType: String = "Primary";
    public busy: Subscription;
    public cBusy: Subscription;
    public uBusy: Subscription;
    public initalBusy: Subscription;
    public addressCd;
    public companyEin: any;
    public cmpymodel: any;
    public data: any;
    public contacts: any;
    public primaryAddress: any;
    public alternateAddress: any;
    public closePanel: boolean;
    public filteredResults: any;
    public permitId: any;
    public permitTypeCd: any;
    public shareData: ShareDataService;
    public permit: any;
    public addressDiff: AddressDiff;

    public tufa3852DocId: number;

    public cmpypermit: any;

    private address = new Address();
    setPrimaryCase: String;

    constructor(private companyService: CompanyService,
        private _route: ActivatedRoute, private companyEventService: CompanyEventService,
        router: Router, private _shareData: ShareDataService, private filterPipe: FileDataFilterPipe,
        private _permitPeriodService: PermitPeriodService) {
        this.route = _route;
        this.router = router;
        this.shareData = _shareData;
        this.addressDiff = new AddressDiff();
        this.companyEventService.addressesLoad$.subscribe(
            vo => {
                let _this = this;
                if (vo.company) {
                    setTimeout(function () {
                        _this.loadAddresses(vo);
                    }, 100);
                };
            });
        this.setPrimaryCase = "";
    }

    loadAddresses(vo) {
        let company = vo.company;
        this.cmpypermit = company;
        let einNum = company.einNum.replace(/[^\w\s]/gi, '');

        if (this.cBusy) this.cBusy.unsubscribe();
        if (this.busy) this.busy.unsubscribe();
        this.cBusy = this.companyService.getCompanyEIN(einNum).subscribe(data => {
            if (data) {
                this.associateAddress(company.permitUpdateId);
                this.cmpymodel = data;

                let permit = _.find<Permit>(data.permitSet, { permitNum: company.permitNum });
                this.permitId = permit.permitId;
                this.permitTypeCd = permit.permitTypeCd;
                this.primaryAddress = data.primaryAddress;
                //console.log("AddressMgmtPage setPrimaryCase"+this.setPrimaryCase);

                if (data.primaryAddress && data.alternateAddress) {
                    this.setPrimaryCase = "case1";
                } else if (data.primaryAddress && !data.alternateAddress) {
                    this.setPrimaryCase = "case2";
                } else if (!data.primaryAddress && data.alternateAddress) {
                    this.setPrimaryCase = "case3";
                } else if (!data.primaryAddress && !data.alternateAddress) {
                    this.setPrimaryCase = "case4";
                }
                //console.log("AddressMgmtPage setPrimaryCase after setting"+this.setPrimaryCase);
                if (!this.primaryAddress) {
                    this.primaryAddress = new Address();
                }
                this.primaryAddress.addressTypeCd = "PRIM";
                //if (this.primaryAddress.suite === "  ")
                // this.primaryAddress.suite = "";

                this.primaryAddress.companyId = this.cmpymodel.companyId;
                this.alternateAddress = data.alternateAddress;

                if (!this.alternateAddress) {
                    this.alternateAddress = new Address();
                }
                this.alternateAddress.addressTypeCd = "ALTN";
                this.alternateAddress.companyId = this.cmpymodel.companyId;
                this.busy = this.companyService.getPermitHistory3852(this.permitId)
                    .subscribe(data => {
                        this.data = data.permitHistoryList;
                        this.permit = _.findLast(_.sortBy(this.data, (obj) => parseInt(obj.sortMonthYear, 10)), function (o) {
                            return o.reportStatusCd !== "Not Started";
                        });
                        //scroll to the top of the parent page once addresses loaded successfully
                        this.companyEventService.pgScrollTrigger.next(vo.scroll);
                    });
                this.addressType = "Primary";
                this.compareIngestedAddresstoTUFAAddress(this.addressType);
            }
        });
    }

    getAddresses(docId) {
        if(this.initalBusy) this.initalBusy.unsubscribe();
        this.primaryAddress = null;
        this.nextInd = -1;
        this.initalBusy = this.companyService.getCompanies(docId).subscribe(data => {
            if (data) {
                this.companyData = data;
            }
        });
    }

    /**
     *
     * ngOnInit that initializes the Search Criteria Page
     *
     * @memberOf SearchCriteriaPage
     */
    ngOnInit(): void {
        this.cosmeticFix('#keyword-search-input');
    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "switch") {
                let chg = changes[propName];
                if (!chg.currentValue) {
                    this.close();
                }
            }

            if (propName === 'companyData') {
                this.close();
                let chg = changes[propName];
                JSON.stringify(chg.currentValue);
            }
        }
    }

    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }
        if (this.cBusy) {
            this.cBusy.unsubscribe();
        }
        if (this.uBusy) {
            this.uBusy.unsubscribe();
        }
        if (this.initalBusy) {
            this.initalBusy.unsubscribe();
        }
    }

    cosmeticFix(elementID: any) {
        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let searchInput = jQuery(elementID);
        searchInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

    reload(dataModel) {

        if (dataModel !== "SKIP") {
            if (dataModel.addressStatus) {
                this.companyEventService.loadMovetoNextRecord(this.addressData);
                this.uBusy = this.companyService.updatePermit(dataModel).subscribe(data => {
                    this.companyData = data;
                    // this.companyEventService.loadMovetoNextRecord(this.addressData);
                });
            }
        } else {
            this.addressData.uiAction="SKIP";
            this.companyEventService.loadMovetoNextRecord(this.addressData);
        }
    }

    close() {
        let _this = this;
        setTimeout(function () {
            _this.primaryAddress = null;
            _this.nextInd = -1;
        }, 100);
    }

    trackChanges($event) {
        this.addressCd = $event === 'ALTN' ? this.alternateAddress : this.primaryAddress;
    }

    associateAddress($event) {
        this.addressData = _.find(this.companyData, { permitUpdateId: $event });
        this.nextInd = $event;
        this.loadAddress();
    }

    loadAddress() {
        this.address = new Address();
        if (this.addressData) {
            this.address.city = this.addressData.mailingCity;
            this.address.streetAddress = this.addressData.mailingStreet;
            this.address.state = this.addressData.mailingState;
            this.address.postalCd = this.addressData.mailingPostalCd;
            this.address.countryNm = "UNITED STATES";
            this.address.isIngestedStateValid = this.addressData.ingestedStateValid === 'Y' ? true : false;
        }
    }

    toPeriodofActivity(history: CompanyPermitHistory) {
        this.shareData.companyName = history.companyName;
        this.shareData.permitNum = history.permitNum;
        this.shareData.period = history.displayMonthYear;
        this.shareData.permitId = history.permitId;
        this.shareData.periodId = history.reportPeriodId;
        this.shareData.companyId = history.companyId;
        this.shareData.periodStatus = history.reportStatusCd;
        this.shareData.filemgmttab = "address";
        this.shareData.breadcrumb = "permit-mgmt";
    }

    updateResults($event) {
        if ($event) {
            this.filteredResults = this.filterPipe.transform(this.companyData, $event);
        }
    }

    copyLines() {
        this.address.attention = this.getActiveAddress('attention-left');
        this.address.suite = this.getActiveAddress('suite-left');
        this.address.city = this.getActiveAddress('city-left');
        this.address.streetAddress = this.getActiveAddress('street-address-left');
        this.address.state = this.getActiveAddress('state-left');
        this.address.postalCd = this.getActiveAddress('postalCode-left');
        this.address.countryNm = this.getActiveAddress('country-left');
        this.copyLine();
    }

    copyLine() {
        this.companyEventService.matchAddress(this.address);
        this.compareIngestedAddresstoTUFAAddress(this.addressType);
    }

    private compareIngestedAddresstoTUFAAddress(selAddress) {
        let selectedAddress: Address = selAddress === "Primary" ? this.primaryAddress : this.alternateAddress;
        this.addressDiff.attentionDiff = this.checkNullUndefs(selectedAddress.attention, this.address.attention);
        this.addressDiff.streetAddressDiff = this.checkNullUndefs(selectedAddress.streetAddress, this.address.streetAddress);
        this.addressDiff.suiteDiff = this.checkNullUndefs(selectedAddress.suite, this.address.suite);
        this.addressDiff.cityDiff = this.checkNullUndefs(selectedAddress.city, this.address.city);
        if (selectedAddress.state === null && this.address.state === null)
            this.addressDiff.stateDiff = true;
        else
            this.addressDiff.stateDiff = _.invert(statesJson)[selectedAddress.state] === this.address.state;
        // this.addressDiff.stateDiff = this.checkNullUndefs(selectedAddress.state, this.address.state);
        this.addressDiff.provinceDiff = this.checkNullUndefs(selectedAddress.province, this.address.province);
        this.addressDiff.postalCdDiff = this.checkNullUndefs(selectedAddress.postalCd, this.address.postalCd);
        this.addressDiff.countryNmDiff = this.checkNullUndefs(selectedAddress.countryNm, 'UNITED STATES');
    }

    enableLinkAuth(flag) {
        this._permitPeriodService.setLinkAuthentication(flag);
    }

    getActiveAddress(str) {
        return jQuery("div#address-left").find("#" + str).val();
    }

    checkDiff(str1, str2) {
        return jQuery("div#address-left").find("#" + str1).val() === jQuery("div#address-right").find("#" + str2).val();
    }

    checkNullUndefs(ob1: String, ob2: String) {
        if (ob1 === " ")
            ob1 = undefined;

        if (!ob1 && !ob2) {
            return true;
        } else if (ob1 && ob2) return ob1.toLowerCase() === ob2.toLowerCase();
        return false;
    }
}

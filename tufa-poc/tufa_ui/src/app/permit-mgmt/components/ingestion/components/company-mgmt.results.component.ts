import { Component, ComponentFactoryResolver, Input, IterableDiffer, IterableDiffers, ViewEncapsulation, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'bootstrap-multiselect/dist/css/bootstrap-multiselect.css';
import 'bootstrap-multiselect/dist/js/bootstrap-multiselect.js';
import { LocalStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs';
import { ShareDataService } from '../../../../core/sharedata.service';
import { CompanyFilter, CompanyFilterData } from '../models/companyfilter.model';
import { PermitUpdate } from '../models/permit-update.model';
import { CompanyService } from '../services/company.service';
import { NavigationAttributes } from '../../../../shared/datatable/navigation.attributes.model';
import * as _ from "lodash";

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class ConfirmPopup
 */
@Component({
    selector: '[company-mgmt]',
    templateUrl: './company-mgmt.results.template.html',
    styleUrls: ['./search-criteria.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CompanyMgmtResultsComponent {

    formfilter: any;

    public dataTableData: any[];
    public data: any[];

    @Input() docId;
    shareData: ShareDataService;
    public busy: Promise<any>;

    navigateToCompanydetail: boolean;

    formFilter = new CompanyFilter();
    formFilterSet = { company: this.formFilter };
    filterData = new CompanyFilterData();

    private diff: IterableDiffer<{}>;

    statusesActionReq = ['CONFLICT'];
    actionReqCnt = 0;

    public columns: Array<any> = [
        { name: 'companyName', type: 'input', filtering: { filterString: '', columnName: 'companyName', placeholder: 'Company Name', class: 'company' } },
        { name: 'einNum', type: 'input', filtering: { filterString: '', columnName: 'einNum', placeholder: 'EIN', class: 'company' } },
        { name: 'companyStatus', type: 'input', filtering: { filterString: '', columnName: 'companyStatus', placeholder: 'Company Status', class: 'company' } }
    ];

    private routerSubscription: Subscription;
    private navigatedFrmCmpyDetails: boolean = false;

    private displayEditPanel = false;

    companyUpdate: PermitUpdate;
    isDisplayPanel: boolean = true;

    sortBy = ["companyActionFunction", 'companyName'];
    sortOrder = ['asc', 'asc'];
    sortFunction = [{
        name: "companyActionFunction",
        func: function (item: PermitUpdate) {
            //Only sort entries whos actions are unchanged conflicts or invalid data
            if (item.companyAction.includes('INVALID') || (!item.companyStatusChangeAction && item.companyAction.includes('CONFLICT'))) {
                return item.companyAction;
            }
        }
    },
    {
        name: "companyStatusFunction",
        func: (item) => {
            if (item.companyStatus== 'ACTV')
                return 'ACTIVE';
            else if (item.companyStatus== 'INAC')
                return 'INACTIVE';    
            else return 'N/A';
        }
    }]
    pageSize = 25;
    selectedRow: number;
    navigationAttrs: NavigationAttributes;

    filterSortedData: any[];

    //Custom primary sorting
    //String or Function
    primarySort: any;
    actionSortFunction = function (item: PermitUpdate) {
        //Only sort entries whos actions are unchanged conflicts or invalid data
        if (item.companyAction.includes('INVALID') || (!item.companyStatusChangeAction && item.companyAction.includes('CONFLICT'))) {
            return item.companyAction;
        }
    }
    nextRecord: any;

    constructor(private companyService: CompanyService,
        private shareDataService: ShareDataService,
        private storage: LocalStorageService,
        private router: Router,
        private _route: ActivatedRoute,
        private componentFactoryResolver: ComponentFactoryResolver,
        private differs: IterableDiffers
    ) {
        this.shareData = shareDataService;
        this.router = router;
    }


    ngOnInit(): void {
        var self = this;
        jQuery('#multiselect').multiselect({
            buttonWidth: '250px',
            nonSelectedText: 'Select Action',
            includeSelectAllOption: true,
            onChange: function (element, checked) {
                var cmpactions = jQuery('#multiselect option:selected');
                var selected = [];
                jQuery(cmpactions).each(function (index, cmpaction) {
                    selected.push([jQuery(this).val()]);
                });
                self.updateactionfilter(selected);
            }
        });

        this.selectAllActions();
    }

    private selectAllActions() {
        let alloptions = '#multiselect option';
        var values = [];
        jQuery(alloptions).each(function () {
            values.push(jQuery(this).val());
        });

        jQuery('#multiselect').multiselect('select', values, false);
    }

    updateactionfilter(selected) {
        if (selected !== undefined) {
            this.formFilter.companyActionFilter = selected;
            this.refreshFormFilterObject(this.formFilter);
            jQuery('#multiselect').multiselect('deselectAll', false);
            jQuery('#multiselect').multiselect('select', selected, false);
        }
    }

    getPermitUpdates(docId, reset: boolean) {
        if (reset) this.resetPanel();
        this.busy = this.companyService.getPermitUpdatesByDocument(docId).toPromise().then(
            data => {

                let filtrdCompanyUpdates: any[] = data.filter((cmpyUpdate, index, self) => {
                    return index === self.findIndex((cUpdate) => {
                        return cmpyUpdate.einNum == cUpdate.einNum;
                    })
                });

                this.dataTableData = filtrdCompanyUpdates;
                this.data = filtrdCompanyUpdates;
                this.calcActionReqItemsCount();
                if (this.actionReqCnt == 0) {
                    this.resetPanel();
                }
            });

    }


    calcActionReqItemsCount() {

        let cnt = 0;
        for (let i in this.data) {
            let cAction = this.data[i].companyAction;
            let cLastAction = this.data[i].companyStatusChangeAction;
            for (let j in this.statusesActionReq) {
                let status = this.statusesActionReq[j];
                if (cAction.includes(status) && !cLastAction)
                    cnt++;
            }
        }
        this.actionReqCnt = cnt;
    }

    getPermitStatus(permitUpdate) {
        return (permitUpdate.closeDt) ? 'CLOSED' : 'ACTIVE';
    }

    toCompany(einNum) {
        this.navigateToCompanydetail = true;
        this.shareData.breadcrumb = "permit-mgmt";
        this.shareData.filemgmttab = 'company';
        this.router.navigate(["/app/company/details", { ein: einNum }]);
    }

    updateFilters() {
        for (let col of this.columns) {
            let filterinfo = col.filtering;
            if (filterinfo) {
                if (filterinfo.columnName === "companyName") {
                    this.formFilter.companyNameFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "einNum") {
                    this.formFilter.einNumFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "companyStatus") {
                    this.formFilter.companyStatusFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "companyAction") {
                    this.formFilter.companyActionFilter = filterinfo.filterString.trim();
                }
            }
        }
    }

    isValidString(str) {
        return ((str != null) && (typeof str !== 'undefined') && (str.length >= 0));
    }

    public onChangeTable(filterinfo): void {
        if (filterinfo) {
            if (filterinfo.columnName === "companyName") {
                this.formFilterSet[filterinfo.class].companyNameFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "einNum") {
                this.formFilterSet[filterinfo.class].einNumFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "companyStatus") {
                this.formFilterSet[filterinfo.class].companyStatusFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "companyAction") {
                this.formFilter.companyActionFilter = filterinfo.filterString.trim();
            }

            if (filterinfo.refresh) {
                if (filterinfo.class === "company")
                    this.refreshFormFilterObject(this.formFilterSet[filterinfo.class]);
            }
        }
    }

    restoreFilterData(filterData: CompanyFilterData) {
        if (!filterData) return;
        this.columns.forEach(column => {
            switch (column.name) {
                case "companyName":
                    column.filtering.filterString = filterData._companyNameFilter;
                    this.formFilter.companyNameFilter = filterData._companyNameFilter;
                    break;
                case "einNum":
                    column.filtering.filterString = filterData._einNumFilter;
                    this.formFilter.einNumFilter = filterData._einNumFilter;
                    break;
                case "companyStatus":
                    column.filtering.filterString = filterData._companyStatusFilter;
                    this.formFilter.companyStatusFilter = filterData._companyStatusFilter;
                default:
                    break;
            }
        });

        if (filterData._companyActionFilter)
            this.updateactionfilter(filterData._companyActionFilter);
        else
            this.refreshFormFilterObject(this.formFilter);
    }
    applyFilter(){
        this.refreshFormFilterObject(this.formFilter);
    }
   
    refreshFormFilterObject(formFilter: CompanyFilter) {
            this.filterData = null;
            this.filterData = new CompanyFilterData();
            this.filterData.businessNameFilter = formFilter.companyNameFilter;
            this.filterData.einNumFilter = formFilter.einNumFilter;
            this.filterData.companyStatusFilter = formFilter.companyStatusFilter;
            this.filterData.companyActionFilter = formFilter.companyActionFilter;
             
    }

    ngOnDestroy() {
        if (this.routerSubscription)
            this.routerSubscription.unsubscribe();

        this.hidePanel(true);
    }

    isDormant(status) {
        return status ? false : true;
    }

    getCompanyStatus(companyUpdate) {
        if (companyUpdate && companyUpdate.companyStatus) {
            if (companyUpdate.companyStatus.toUpperCase() == 'ACTV')
                return 'ACTIVE';
            else if (companyUpdate.companyStatus.toUpperCase() == 'CLSD')
                return 'CLOSED';
        }

        return 'N/A';

    }

    getTooltipContent(companyUpdate) {
        if (companyUpdate) {

            let currentRow = this.data.findIndex(function (cmpyUpdate) {
                return cmpyUpdate.einNum == companyUpdate.einNum;
            });

            if (this.data[currentRow].errors) {
                let errors = this.companyService.getErrorsHTML(this.data[currentRow].errors, 'COMPANY_ERROR');
                if (this.data[currentRow].companyStatusChangeAction) {
                    let lastActionHtml = '<br><p class="no-margin" ><strong>Last Action: ' + this.data[currentRow].companyStatusChangeAction + '</strong></p>';
                    errors = errors.concat(lastActionHtml)
                }
                return errors;
            }

        }
    }

    getIconColor(companyUpdate: any): string {

        let iconColor = '';

        if (companyUpdate != null) {
            if (companyUpdate.companyStatusChangeAction) {
                iconColor = 'text-success';
            } else if (!companyUpdate.companyStatusChangeAction && companyUpdate.companyAction && companyUpdate.companyAction.includes('CONFLICT')) {
                iconColor = 'text-danger';
            } else if (!companyUpdate.companyStatusChangeAction && companyUpdate.companyAction && !companyUpdate.companyAction.includes('CONFLICT')) {
                iconColor = 'text-warning'
            } else {
                iconColor = '';
            }
        }

        return iconColor;
    }

    scrollTop() {
        jQuery('html, body, .content-wrap').animate({
            scrollTop: 0
        }, 2000);

    }

    reviewCompanyUpdateClick(companyUpdate, index) {

        if (!companyUpdate) return;

        this.companyUpdate = companyUpdate;

        if (companyUpdate.companyAction && companyUpdate.companyAction.includes('CONFLICT - NAME MISMATCH')) {
            this.scrollTop();
            if (index || index == 0) {
                if (this.selectedRow == index) {
                    this.resetPanel()
                }
                else {

                    this.selectedRow = index;
                    this.hidePanel(false);
                }
            }
        }

    }

    onNavigationEvent(event) {
        
        this.resetPanel();

        if (event.eventtype === 'nextrecord') {
            this.nextRecord = event.rowdata;
            if (!this.nextRecord) this.resetPanel();
            return;
        }

        if (event.eventtype == 'mvtorecord') {
            let companyUpdate = event.rowdata;
            let rowIndex = event.rowindex;

            this.selectedRow = rowIndex;

            let thisref = this;
            setTimeout(function () {
                thisref.companyUpdate = companyUpdate;
                if (companyUpdate && thisref.actionReqCnt != 0)
                    thisref.hidePanel(false);
                else
                    thisref.resetPanel();
            }, 500);
        }
    }

    afterSortOrderChange($event) {
        if (this.nextRecord) {
            this.triggerMoveToRecordEvent();
        }
    }

    triggerMoveToRecordEvent() {
        let thisref = this;
        //setTimeout needed to trigger change detection
        setTimeout(function () {
            // move to nextRecord; onNavigationEvent(event) will be called to set the 
            // nextRecord in the  queue and highligted on the grid
            let einSearchVal = thisref.nextRecord?thisref.nextRecord["einNum"]:"";
            let nvgAttr =
            {
                rowdata: thisref.nextRecord,
                cols: ["einNum"],
                colSearch: "einNum",
                colSearchVal: [einSearchVal],
                action: ['mvtorecord']

            };
            thisref.updateNavigationAttrs(nvgAttr);
            //purge nextRecord for next record navigation done on a company action
            thisref.nextRecord = null;
        }, 500);
    }

   async  moveToNextRecord(permitUpdate: PermitUpdate) {
        if (permitUpdate) {
            // get and set the next record; onNavigationEvent(event) callback  will be called to set the nextRecord

            let nvgAttr =
            {
                rowdata: permitUpdate,
                rowIgnoreCol: "companyStatusChangeAction",
                cols: ["einNum"],
                colSearch: "companyAction",
                colSearchVal: ["CONFLICT"],
                action: ['nextrecord']
            };
            this.updateNavigationAttrs(nvgAttr);

            // reload the grid and upon sort completion afterSortOrderChange($event) callback will be called 
            // to move to next record
            if (permitUpdate.uiAction !== "SKIP")
                await this.getPermitUpdates(this.docId, false);
            else
                // In case no action taken (e.g. SKIP) just move to the next record
                this.triggerMoveToRecordEvent();
        } else 
            this.resetPanel();
    }

    private updateNavigationAttrs(nvgAttr: NavigationAttributes) {

        if (!nvgAttr.rowdata) {
            this.resetPanel();
            return;
        }
        this.navigationAttrs = nvgAttr;
    }

    hidePanel(hide) {
        this.isDisplayPanel = hide;
    }

    resetPanel() {
        this.selectedRow = -1;
        this.hidePanel(true);
    }

}
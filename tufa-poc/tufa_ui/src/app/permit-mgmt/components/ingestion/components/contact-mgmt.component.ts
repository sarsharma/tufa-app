
import { Component, Input, OnDestroy, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from "lodash";
import { LocalStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs';
import { Company } from '../../../../company/models/company.model';
import { ShareDataService } from '../../../../core/sharedata.service';
import { ContactDiff } from '../../../../model/contact-diff.model';
import { Contact } from '../../../../model/contact.model';
import { Permit } from '../../../../permit/models/permit.model';
import { PermitPeriodService } from '../../../../permit/services/permit-period.service';
import { CompanyPermitHistory } from '../models/company-permit-history.model';
import { CompanyEventService } from '../services/company.event.service';
import { CompanyService } from '../services/company.service';
import { FileDataFilterPipe } from '../util/file.filter.pipe';

declare var jQuery: any;

@Component({
    selector: '[contact-mgmt]',
    templateUrl: './contact-mgmt.template.html',
    providers: [CompanyService, FileDataFilterPipe],
    encapsulation: ViewEncapsulation.None
})

/**
 * SearchCriteriaPage is the Daily Operations Landing Page
 *
 * @export
 * @class SearchCriteriaPage
 * @implements {OnInit}
 */
export class ContactMgmtPage implements OnInit, OnDestroy {
    public route: ActivatedRoute;
    public router: Router;
    public reportData: any[];
    @Input() companyData;
    @Input() switch;
    public busy: Subscription;
    public initalBusy: Subscription;
    contactData: any; nextInd;
    public showErrorFlag: boolean;
    public alerts: Array<Object>;
    public permitItemsTotal: number;
    public cmpymodel: Company;
    public data: any;
    public contacts: any;
    public primaryAddress: any;
    public alternateAddress: any;
    public contactId;
    public closePanel: boolean;
    public permitId: any;
    public permitTypeCd: any;
    public shareData: ShareDataService;
    public permit: any;
    public filteredResults: any;
    public contactDiff: ContactDiff;

    public contact = new Contact();

    public cmpypermit: any;
    private cBusy: Subscription;
    uBusy: Subscription;

    constructor(private companyService: CompanyService,
        private companyEventService: CompanyEventService,
        private storage: LocalStorageService,
        private _route: ActivatedRoute,
        private _permitPeriodService: PermitPeriodService,
        router: Router, private _shareData: ShareDataService, private filterPipe: FileDataFilterPipe
    ) {
        this.route = _route;
        this.router = router;
        this.showErrorFlag = false;
        this.shareData = _shareData;
        this.contactDiff = new ContactDiff();
        this.companyEventService.contactsLoad$.subscribe(
            vo => {
                if (vo) {
                    let _this = this;
                    setTimeout(function () {
                        _this.loadContacts(vo);
                    }, 100)
                };
            });

    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "switch") {
                let chg = changes[propName];
                if (!chg.currentValue) {
                    this.close();
                }
            }
        }
    }

    getContacts(docId) {
        if (this.initalBusy) this.initalBusy.unsubscribe();
        this.contacts = null;
        this.nextInd = -1;
        this.initalBusy = this.companyService.getContacts(docId).subscribe(data => {
            if (data) {
                this.companyData = data;
            }
        });
    }

    loadContacts(vo) {
        let company = vo.company;
        this.cmpypermit = company;
        let einNum = company.einNum.replace(/[^\w\s]/gi, '');
        if (this.cBusy) this.cBusy.unsubscribe();
        if (this.busy) this.busy.unsubscribe();
        this.cBusy = this.companyService.getCompanyEIN(einNum).subscribe(data => {
            if (data) {
                this.associateContact(company.permitUpdateId);
                this.cmpymodel = data;
                this.contacts = data.contactSet;
                if (!this.contacts) {
                    this.contacts = new Contact();
                }
                this.contacts.companyId = this.cmpymodel.companyId;
                this.contactId = this.contacts[0] ? this.contacts[0].contactId : null;
                this.permitId = _.find<Permit>(data.permitSet, { permitNum: company.permitNum }).permitId;
                this.permitTypeCd = data.permitSet[0].permitTypeCd;
                this.busy = this.companyService.getPermitHistory3852(this.permitId).subscribe(data => {
                    this.data = data.permitHistoryList;
                    this.contact = new Contact();
                    this.permit = _.findLast(_.sortBy(this.data, (obj) => parseInt(obj.sortMonthYear, 10)), function (o) {
                        return o.reportStatusCd !== "Not Started";
                    });
                     //scroll to the top of the parent page once addresses loaded successfully
                     this.companyEventService.pgScrollTrigger.next(vo.scroll);
                });

                this.compareIngestedContacttoTUFAContacts();
            }
        });
    }

    /**
     *
     * ngOnInit that initializes the Search Criteria Page
     *
     * @memberOf SearchCriteriaPage
     */
    ngOnInit(): void {

        this.cosmeticFix('#keyword-search-input');
        this.showErrorFlag = false;
    }

    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }
        if (this.cBusy) {
            this.cBusy.unsubscribe();
        }
        if (this.uBusy) {
            this.uBusy.unsubscribe();
        }
        if (this.initalBusy) {
            this.initalBusy.unsubscribe();
        }
    }

    /**
     * This cosmetic fix is to allow the blue focus highlight for the input group including
     * the search icon on the textbox
     * @param {*} elementID
     *
     * @memberOf SearchCriteriaPage
     */
    cosmeticFix(elementID: any) {
        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let searchInput = jQuery(elementID);
        searchInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

    reload(dataModel) {

        if (dataModel !== "SKIP") {
            this.companyEventService.loadMovetoNextRecord(this.contactData);
            if (dataModel.contactStatus) {
                this.uBusy = this.companyService.updatePermitcontacts(dataModel).subscribe(data => {
                    this.companyData = data;
                    // this.companyEventService.loadMovetoNextRecord(this.contactData);
                });
            }
        } else {
            this.contactData.uiAction="SKIP";
            this.companyEventService.loadMovetoNextRecord(this.contactData);
        }
    }

    close() {
        let _this = this;
        setTimeout(function () {
            _this.contacts = null;
            _this.nextInd = -1;
        }, 100);
    }

    associateContact($event) {
        this.contactData = _.filter(this.companyData, function (company) {
            return company.permitUpdateId === $event;
        })[0];
        this.nextInd = $event;
        this.loadContact();
    }

    loadContact() {
        this.contact = new Contact();
        if (this.contactData) {
            this.contact.firstNm = this.contactData.firstNm;
            this.contact.lastNm = this.contactData.lastNm;
            this.contact.emailAddress = this.contactData.email;
            this.contact.phoneNum = this.contactData.phoneNum;
            this.contact.phoneExt = this.contactData.phoneExt;
            this.contact.faxNum = this.contactData.faxNum;
            this.contact.cntryDialCd = this.contactData.cntryDialCd;
            this.contact.cntryFaxDialCd = this.contactData.cntryFaxDialCd;
        }
    }

    toPeriodofActivity(history: CompanyPermitHistory) {
        this.shareData.companyName = history.companyName;
        this.shareData.permitNum = history.permitNum;
        this.shareData.period = history.displayMonthYear;
        this.shareData.permitId = history.permitId;
        this.shareData.periodId = history.reportPeriodId;
        this.shareData.companyId = history.companyId;
        this.shareData.periodStatus = history.reportStatusCd;
        this.shareData.filemgmttab = "contact";
        this.shareData.breadcrumb = "permit-mgmt";
    }

    private compareIngestedContacttoTUFAContacts() {


        let selectedContact: Contact = this.contacts
        // this.primaryAddress
        this.contactDiff.lastNmDiff = true;
        this.contactDiff.firstNmDiff = true;
        this.contactDiff.phoneNumDiff = this.checkNullUndefs(selectedContact.phoneExt, this.contact.phoneExt);
        this.contactDiff.phoneExtDiff = this.checkNullUndefs(selectedContact.phoneNum, this.contact.phoneNum);
        this.contactDiff.emailAddressDiff = this.checkNullUndefs(selectedContact.emailAddress, this.contact.emailAddress);
        this.contactDiff.countryCdDiff = true;
        this.contactDiff.cntryDialCdDiff = true;
        this.contactDiff.faxNumDiff = true;
        this.contactDiff.cntryFaxDialCdDiff = true;
    }

    enableLinkAuth(flag) {
        this._permitPeriodService.setLinkAuthentication(flag);
    }

    checkNullUndefs(ob1: String, ob2: String) {
        if (!ob1 && !ob2) {
            return true;
        } else if (ob1 && ob2) return ob1.toLowerCase() === ob2.toLowerCase();
        return false;
    }


    updateResults($event) {
        if ($event) {
            this.filteredResults = this.filterPipe.transform(this.companyData, $event);
        }
    }

    copyLines() {
        this.contact.firstNm = this.getActiveAddress('add-first-name-left');
        this.contact.lastNm = this.getActiveAddress('add-last-name-left');
        this.contact.emailAddress = this.getActiveAddress('add-email-address-left');
        this.contact.phoneNum = this.getActiveAddress('add-phone-number-left');
        this.contact.phoneExt = this.getActiveAddress('add-ext-optional-left');
        this.contact.faxNum = this.getActiveAddress('add-fax-optional-left');
        this.contact.cntryDialCd = this.getActiveAddress('phone-countrycode-left');
        this.contact.cntryFaxDialCd = this.getActiveAddress('fax-countrycode-left');
        this.copyLine();
    }

    copyLine() {
        this.companyEventService.matchContact(this.contact);
        this.compareIngestedContacttoTUFAContacts();
    }

    getActiveAddress(str) {
        return jQuery("div#contact-left").find("#" + str).val();
    }

    trackChanges($event) {
        this.contactId = $event;
    }
}

/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import * as _ from "lodash";
import { Company } from '../../../../company/models/company.model';
import { Address } from '../../../../model/address.model';
import { AddressService } from '../services/address.service';
import * as statesJson from './states.json';

declare var jQuery: any;

@Component({
  selector: '[edit-address-left]',
  templateUrl: './edit-address-left.template.html'
})

export class EditAddressMgmtLeft {
  @Input() companyData: any;
  @Input() primaryAddress: any;
  @Input() secondaryAddress: any;
  @Output() resetIcon = new EventEmitter<String>();

  selAddress: any = new Address();
  @Input() selAddressType: string;

  addressStatus: any = "";
  countryList: any;
  stateList: any;
  showErrorFlag: boolean;
  alerts: Array<Object>;
  statesUSA: any;

  private address = new Address();
  /**
   *
   *
   *
   * @memberOf EditAddressPopup
   */
  ngOnInit() {
    this.addressService.getCountryNames().subscribe(data => {
      if (data)
        this.countryList = [''].concat(<any>data);
    });
    this.addressService.getStateCodes().subscribe(data => {
      if (data)
        this.stateList = [''].concat(<any>data);
    });
  }

  /**
   * Creates an instance of EditAddressPopup.
   *
   * @param {AddressService} addressService
   *
   * @memberOf EditAddressPopup
   */
  constructor(private addressService: AddressService) {
    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];

    this.showErrorFlag = false;
  }

  /**
   *
   *
   * @param {SimpleChanges} changes
   *
   * @memberOf EditAddressPopup
   */
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "companyData") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.companyData = new Company();
          this.companyData.legalName = chg.currentValue.legalName;
          this.companyData.companyId = chg.currentValue.companyId;
        }
      }
      if (propName === "primaryAddress") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.primaryAddress = new Address();
          this.primaryAddress.attention = chg.currentValue.attention;
          this.primaryAddress.streetAddress = chg.currentValue.streetAddress;
          this.primaryAddress.suite = chg.currentValue.suite;
          this.primaryAddress.city = chg.currentValue.city;
          this.primaryAddress.state = _.invert(statesJson)[chg.currentValue.state];
          this.primaryAddress.postalCd = chg.currentValue.postalCd;
          this.primaryAddress.countryNm = chg.currentValue.countryNm;
          this.selAddress = this.primaryAddress;
        }
      }
      if (propName === "secondaryAddress") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.secondaryAddress = new Address();
          this.secondaryAddress.attention = chg.currentValue.attention;
          this.secondaryAddress.streetAddress = chg.currentValue.streetAddress;
          this.secondaryAddress.suite = chg.currentValue.suite;
          this.secondaryAddress.city = chg.currentValue.city;
          this.secondaryAddress.state = _.invert(statesJson)[chg.currentValue.state];
          this.secondaryAddress.postalCd = chg.currentValue.postalCd;
          this.secondaryAddress.countryNm = chg.currentValue.countryNm;
        }
      }
    }
  }

  isPrimary() {
    return (this.selAddressType === "Primary");
  }

  gotoPrimaryAddress() {
    this.selAddressType = "Primary";
    this.selAddress = this.primaryAddress;
    this.resetIcon.emit(this.selAddressType);
  }

  gotoSecondaryAddress() {
    this.selAddressType = "Alternate";
    this.selAddress = this.secondaryAddress;
    this.resetIcon.emit(this.selAddressType);
  }

}

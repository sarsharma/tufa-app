/*
@author : Deloitte
this is Component for adding address as a popup.
*/
import { ComponentFixture, async, inject, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core'; 
import { CommonModule }  from '@angular/common';
import { RouterModule, Routes, provideRoutes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AddressService } from '../services/address.service';
import { SharedModule } from '../../../../shared/shared.module';
import { HttpModule } from '../../../../../../node_modules/@angular/http';
import { HttpClient } from '../../../../../../node_modules/@angular/common/http';
import { AuthorizationHelper } from '../../../../authentication/util/authorization.helper.util';
import { EditAddressMgmtRight } from './edit-address-right.component';

declare var jQuery: any;

describe('Edit Address', () => {
  let comp:    EditAddressMgmtRight;
  let fixture: ComponentFixture<EditAddressMgmtRight>;

  // provide our implementations or mocks to the dependency injector
  beforeEach( async(() => {
    TestBed.configureTestingModule({
      imports: [ SharedModule, HttpModule, RouterTestingModule ],
      declarations: [EditAddressMgmtRight],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [HttpClient, AuthorizationHelper, AddressService,
        provideRoutes([{path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTesting',
        pathMatch: 'full'}])
      ]
    }).compileComponents();
  }));

   beforeEach(() => {
    fixture = TestBed.createComponent(EditAddressMgmtRight);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(comp).toBeTruthy();
  });

});

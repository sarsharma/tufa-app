/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, EventEmitter, Input, Output } from '@angular/core';
import 'jquery-ui/ui/widgets/draggable.js';
import { AddressService } from '../services/address.service';
import { CompanyEventService } from '../services/company.event.service';
import { AddressDiff } from '../../../../model/address-diff.model';
import { Address } from '../../../../model/address.model';
import { Subscription } from 'rxjs';

declare var jQuery: any;

@Component({
  selector: '[edit-address-right]',
  templateUrl: './edit-address-right.template.html',
  styleUrls: ['./edit-address-right.style.scss']
})

export class EditAddressMgmtRight {
  @Input() diffIndicator: AddressDiff;
  @Input() dataModel: any;
  @Input() companyId: any;
  @Input() typeCd: any;
  @Output() onSaving = new EventEmitter<any>();
  @Output() cancel = new EventEmitter<any>();
  @Output() resetIcon = new EventEmitter<boolean>();
  @Input() primaryAddress: any;
  @Input() secondaryAddress: any;
  @Input() setPrimaryCase: any;
   
  public busy: Subscription;
  public countryList: any;
  public stateList: any;
  public showErrorFlag: boolean;
  public alerts: Array<Object>;
  public updatePrimToAltnFlag: String;
  public setAltAddress: String;

  showSelAltAddressFlag: boolean = false;

  @Input() address: Address;
  /**
   *
   *
   *
   * @memberOf EditAddressPopup
   */
  ngOnInit() {
    this.addressService.getCountryNames().subscribe(data => {
      if (data)
        this.countryList = [''].concat(data);
    });
    this.addressService.getStateCodes().subscribe(data => {
      if (data)
        this.stateList = [''].concat(data);
    });
    this.address.countryNm = 'UNITED STATES';
    this.updatePrimToAltnFlag = 'N';
    this.setAltAddress = "NA";
    
  }

  ngOnDestroy() {
    if(this.busy){
      this.busy.unsubscribe();
    }
  }

  /**
   * Creates an instance of EditAddressPopup.
   *
   * @param {AddressService} addressService
   *
   * @memberOf EditAddressPopup
   */
  constructor(private addressService: AddressService, private companyEventService: CompanyEventService) {
    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];
    this.companyEventService.addressMatch$.subscribe(
      data => { if(data) this.loadAddress(data);
    });
    this.showErrorFlag = false;
  }

  loadAddress(address: Address) {
    this.address.attention = address.attention ? address.attention: this.address.attention;
    this.address.city = address.city ? address.city: this.address.city;
    this.address.streetAddress = address.streetAddress ? address.streetAddress:this.address.streetAddress;
    this.address.state = address.state ? address.state:this.address.state;
    this.address.postalCd = address.postalCd?address.postalCd:this.address.postalCd;    
    this.address.countryNm = 'UNITED STATES';
  }

  save(addStatus){
   // console.log("setPrimaryCase"+this.setPrimaryCase);
    // Toggle to edit mode or navigate to the next screen if validation passes
    
      if(addStatus !== "SKIP" && addStatus !== "IGNR"){        
        jQuery('#editaddressform').parsley().validate();
        if (jQuery('#editaddressform').parsley().isValid()) {
          this.dataModel.addressStatus = addStatus !== "SKIP" ? addStatus : this.dataModel.addressStatus;
          this.address.companyId = this.companyId;
          this.address.countryCd = this.address.countryNm ? this.address.countryNm : 'UNITED STATES';
          if(addStatus === "ADDED"){
            this.address.addressTypeCd = "PRIM";
            this.setshowSelAltAddressFlag();
          }else{
            this.address.addressTypeCd = jQuery("div#address-left").find(".btn-primary").text() === "Primary"?"PRIM":"ALTN";
            this.busy = this.addressService.saveOrUpdateAddress(this.address).subscribe(data => {
              if (data) {
                this.onSaving.emit(this.dataModel);
              }
            },
            error => {
              this.showErrorFlag = true;
              this.alerts = [];
              this.alerts.push({ type: 'warning', msg: error });
            });
          }
         }
      } else if(addStatus === "SKIP") {this.onSaving.emit("SKIP");} 
      else {this.dataModel.addressStatus = addStatus;this.onSaving.emit(this.dataModel);}

    
  }
  setshowSelAltAddressFlag(){
    if("case1" === this.setPrimaryCase){
      this.showSelAltAddressFlag = true;
     // console.log("showSelAltAddressFlag"+this.showSelAltAddressFlag);
      this.draggthepopup();
     }else{
       this.saveOrUpdatePrimaryAddress("");
     }
  }
  saveOrUpdatePrimaryAddress(selAddressTypeCd){
   // console.log("saveOrUpdatePrimaryAddress "+selAddressTypeCd);
   if("case1" === this.setPrimaryCase){
      this.updatePrimToAltnFlag = 'Y';
      this.setAltAddress = selAddressTypeCd;
     // console.log("saveOrUpdatePrimaryAddress case1");
    }else if("case2" === this.setPrimaryCase){
      this.updatePrimToAltnFlag = 'Y';
      this.setAltAddress = "NA";
     // console.log("saveOrUpdatePrimaryAddress case2");
    }else{
      this.updatePrimToAltnFlag = 'N';
      this.setAltAddress = "NA";
      // console.log("saveOrUpdatePrimaryAddress case3");
    }
    this.busy = this.addressService.saveOrUpdatePrimaryAddress(this.address,this.updatePrimToAltnFlag, this.setAltAddress ).subscribe(data => {
      if (data) {
        this.address = new Address(); 
        this.address.isIngestedStateValid = true;
        this.onSaving.emit(this.dataModel);
      }
    },
    error => {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: 'warning', msg: error });
    });
  }
  
 
  disableAdd() {
    if(this.typeCd){
      return Object.keys(this.typeCd).length>2;
    }
    return false;
  }

  popUpClose() {
    jQuery('#select-altnaddress').modal('hide');
    jQuery('#select-altnaddress').removeAttr('style');
   // this.resetPopUpState();
  }

  draggthepopup() {
  jQuery(document).ready(function () {
      jQuery('body').on('mousedown', '#select-altnaddress-content', function (ev) {
        if(jQuery('#select-altnaddress-content')){
          jQuery('#select-altnaddress-content').draggable();
        }
         
      });
  });

}



  resetIcons() { this.resetIcon.emit(true);}
}

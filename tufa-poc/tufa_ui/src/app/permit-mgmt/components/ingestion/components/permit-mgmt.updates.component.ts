import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'bootstrap-multiselect/dist/css/bootstrap-multiselect.css';
import 'bootstrap-multiselect/dist/js/bootstrap-multiselect.js';
import { ShareDataService } from '../../../../core/sharedata.service';
import { NavigationAttributes } from '../../../../shared/datatable/navigation.attributes.model';
import { PermitUpdate } from '../models/permit-update.model';
import { PermitFilter, PermitFilterData } from '../models/permitfilter.model';
import { CompanyService } from '../services/company.service';
import { Subscription } from 'rxjs';

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class ConfirmPopup
 */
@Component({
    selector: '[permit-mgmt]',
    templateUrl: './permit-mgmt.updates.template.html',
    styleUrls: ['./search-criteria.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class PermitUpdatesMgmtComponent {

    public data: any[];
    @Input() docId;
    shareData: ShareDataService;
    router: Router;
    formFilter = new PermitFilter();
    formFilterSet = { permit: this.formFilter };
    filterData = new PermitFilterData();
    public busy: Subscription;

    displayEditPermitStatusPanel: boolean = false;
    displayAddPermitPanel: boolean = false;

    //permitUpdate: PermitUpdate;
    editPermitUpdate: PermitUpdate;
    addPermitUpdate: PermitUpdate;

    sortBy = ["permitActionFunction", 'companyName'];
    sortOrder = ["asc", "asc"];
    sortFunction = [{
        name: "permitActionFunction",
        func: function (item: PermitUpdate) {
            //Only sort entries whos actions are unchanged conflicts or invalid data
            if (item.permitAction.includes('INVALID') || (!item.permitStatusChangeAction && item.permitAction.includes('CONFLICT'))) {
                return item.permitAction
            }
        }
    }, {
        name: "permitStatusFunction",
        func: (permitUpdate) => {
            if (permitUpdate.permitStatus == 'CLSD' || permitUpdate.permitStatus == 'CLS')
                return 'CLOSED';
            else if (permitUpdate.permitStatus == 'ACTV' || permitUpdate.permitStatus == 'ACT')
                return 'ACTIVE';
            else return 'N/A';
        }
    }] ;
    
    pageSize = 25;
    selectedRow: number;


    navigationAttrs: NavigationAttributes;

    isDisplayEditPanel: boolean = true;
    isDisplayAddPanel: boolean = true;

    statusesActionReq = ['CONFLICT'];
    actionReqCnt: number = 0;
    //String or Function
   

    public columns: Array<any> = [
        { name: 'companyName', type: 'input', filtering: { filterString: '', columnName: 'companyName', placeholder: 'Company Name', class: 'permit' } },
        { name: 'einNum', type: 'input', filtering: { filterString: '', columnName: 'einNum', placeholder: 'EIN', class: 'permit' } },
        { name: 'permitNum', type: 'input', filtering: { filterString: '', columnName: 'permitNum', placeholder: 'Permit #', class: 'permit' } },
        {
            name: 'permitStatus', type: 'select', defaultvalue: '',
            options: [{ name: 'All', value: '' }, { name: 'ACTIVE', value: 'ACTIVE' }, { name: 'CLOSED', value: 'CLOSED' }, { name: 'N/A', value: 'N/A' }],
            filtering: { filterString: '', columnName: 'type', placeholder: 'Permit Status', class: 'permit' }
        }
    ];
    nextRecord: any;

    constructor(private companyService: CompanyService,
        private shareDataService: ShareDataService,
        private _route: ActivatedRoute,
        router: Router
    ) {
        this.shareData = shareDataService;
        this.router = router;

        //this.pgEvent = { activePage: 1, rowsOnPage: 25, dataLength: 1000 };

    }



    ngOnDestroy() {

        if (this.busy) {
            this.busy.unsubscribe();
        }

    }



    ngOnInit(): void {
        let docId = this.docId;
        // this.getPermitUpdates();

        var self = this;

        jQuery('#permitmultiselect').multiselect({
            buttonWidth: '250px',
            // maxHeight: 100,
            includeSelectAllOption: true,
            nonSelectedText: 'Select Action',
            onChange: function (element, checked) {
                var permitactions = jQuery('#permitmultiselect option:selected');
                var selected = [];
                jQuery(permitactions).each(function (index, permitaction) {
                    selected.push([jQuery(this).val()]);
                });
                self.updateactionfilter(selected);
            }
        });

        this.selectAllActions();
    }

    private selectAllActions() {
        let alloptions = '#permitmultiselect option';
        var values = [];
        jQuery(alloptions).each(function () {
            values.push(jQuery(this).val());
        });

        jQuery('#permitmultiselect').multiselect('select', values, false);
    }

    updateactionfilter(selected) {

        if (selected !== undefined) {
            this.formFilter.permitActionFilter = selected;
            this.refreshFormFilterObject(this.formFilter);
            jQuery('#permitmultiselect').multiselect('deselectAll', false);
            jQuery('#permitmultiselect').multiselect('select', selected, false);
        }
    }

    getPermitUpdates(docId, reset: boolean) {
        if (reset) this.resetPanel();
        this.busy = this.companyService.getPermitUpdatesByDocument(docId).subscribe(
            data => {

                let filtrdCompanyUpdates: any[] = data.filter((permitUpdate, index, self) => {
                    return permitUpdate.permitAction ? true : false;
                });

                this.data = filtrdCompanyUpdates;
                this.calcActionReqItemsCount();

                if (this.actionReqCnt == 0) {
                    this.resetPanel();
                }
            });
    }


    calcActionReqItemsCount() {

        let cnt = 0;
        for (let i in this.data) {
            let pAction = this.data[i].permitAction;
            let pLastAction = this.data[i].permitStatusChangeAction
            for (let j in this.statusesActionReq) {
                let status = this.statusesActionReq[j];
                if (pAction.includes(status) && !pLastAction)
                    cnt++;
            }
        }
        this.actionReqCnt = cnt;
    }

    getPermitStatus(permitUpdate) {
        
        if (permitUpdate.permitStatus == 'CLSD' || permitUpdate.permitStatus == 'CLS')
            return 'CLOSED';
        else if (permitUpdate.permitStatus == 'ACTV' || permitUpdate.permitStatus == 'ACT')
            return 'ACTIVE';
        else return 'N/A';
    }

    permitStatus = (permitUpdate) => {
        if (permitUpdate.permitStatusTufa == 'CLSD')
            return 'CLOSED';
        else if (permitUpdate.permitStatusTufa == 'ACTV')
            return 'ACTIVE';
        else return 'N/A';
    }

    toCompany(einNum) {
        this.shareData.breadcrumb = "permit-mgmt";
        this.shareData.filemgmttab = 'permit';
        this.router.navigate(["/app/company/details", { ein: einNum }]);
    }

    updateFilters() {
        for (let col of this.columns) {
            let filterinfo = col.filtering;
            if (filterinfo) {
                if (filterinfo.columnName === "companyName") {
                    this.formFilter.companyNameFilter = filterinfo.filterString.trim();
                }
                else if (filterinfo.columnName === "einNum") {
                    this.formFilter.einNumFilter = filterinfo.filterString.trim();
                }
                else if (filterinfo.columnName === "permitNum") {
                    this.formFilter.permitNumFilter = filterinfo.filterString.trim();
                }
                else if (filterinfo.columnName === "permitStatus") {
                    this.formFilter.permitStatusFilter = filterinfo.filterString.trim();
                }
                else if (filterinfo.columnName === "permitAction") {
                    this.formFilter.permitActionFilter = filterinfo.filterString.trim();
                }
            }
        }
    }

    isValidString(str) {
        return ((str != null) && (typeof str !== 'undefined') && (str.length >= 0));
    }

    restoreFilterData(filterData: PermitFilterData) {
        if (!filterData) return;

        this.filterData = new PermitFilterData();
        this.filterData = jQuery.extend(true, this.filterData, filterData);
        this.columns.forEach(column => {
            switch (column.name) {
                case "companyName":
                    column.filtering.filterString = filterData._companyNameFilter;
                    this.formFilter.companyNameFilter = filterData._companyNameFilter;
                    break;
                case "einNum":
                    column.filtering.filterString = filterData._einNumFilter;
                    this.formFilter.einNumFilter = filterData._einNumFilter;
                    break;
                case "permitNum":
                    column.filtering.filterString = filterData._permitNumFilter;
                    this.formFilter.permitNumFilter = filterData._permitNumFilter;
                    break;
                case "permitStatus":
                    column.filtering.filterString = filterData._permitStatusFilter;
                    this.formFilter.permitStatusFilter = filterData._permitStatusFilter;
                default:
                    break;
            }
        });

        if (filterData._permitActionFilter)
            this.updateactionfilter(filterData._permitActionFilter);
        else
            this.refreshFormFilterObject(this.formFilter);
    }
    applyFilter(){
        this.refreshFormFilterObject(this.formFilter);
    }
    
    refreshFormFilterObject(formFilter: PermitFilter) {
        this.filterData = null;
        this.filterData = new PermitFilterData();
        this.filterData.companyNameFilter = formFilter.companyNameFilter;
        this.filterData.einNumFilter = formFilter.einNumFilter;
        this.filterData.permitNumFilter = formFilter.permitNumFilter;
        this.filterData.permitStatusFilter = formFilter.permitStatusFilter;
        this.filterData.permitActionFilter = formFilter.permitActionFilter;

        // this.pgEvent.activePage = 1;
        // this.resetState();
    }

    public onChangeTable(filterinfo): void {
        if (filterinfo) {
            if (filterinfo.columnName === "companyName") {
                this.formFilterSet[filterinfo.class].companyNameFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "einNum") {
                this.formFilterSet[filterinfo.class].einNumFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "permitNum") {
                this.formFilterSet[filterinfo.class].permitNumFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "permitStatus") {
                this.formFilterSet[filterinfo.class].permitStatusFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "permitAction") {
                this.formFilterSet[filterinfo.class].permitActionFilter = filterinfo.filterString.trim();
            }

            if (filterinfo.refresh) {
                if (filterinfo.class === "permit")
                    this.refreshFormFilterObject(this.formFilterSet[filterinfo.class]);
            }
        }
    }

    FilterSelectionChanged(event, filterColumnName) {
        if (filterColumnName === "permitStatus") {
            this.formFilter.permitStatusFilter = event;
            this.refreshFormFilterObject(this.formFilter);
        }

        if (filterColumnName === "permitAction") {
            this.formFilter.permitActionFilter = event;
            this.refreshFormFilterObject(this.formFilter);
        }
    }

    getTooltipContent(permitUpdate) {

        if (permitUpdate) {

            let currentRow = this.data.findIndex(function (pUpdate) {
                return pUpdate.permitNum == permitUpdate.permitNum && pUpdate.einNum == permitUpdate.einNum && pUpdate.permitAuditId == permitUpdate.permitAuditId;
            });

            if (this.data[currentRow].errors) {
                let errors = this.companyService.getErrorsHTML(this.data[currentRow].errors, 'PERMIT_ERROR');
                if (this.data[currentRow].permitStatusChangeAction) {
                    let lastActionHtml = '<br><p class="no-margin" ><strong>Last Action: ' + this.data[currentRow].permitStatusChangeAction + '</strong></p>';
                    errors = errors.concat(lastActionHtml)
                }
                return errors;
            }

        }
    }

    getIconColor(permitUpdate: PermitUpdate): string {

        let iconColor = '';

        if (permitUpdate != null) {
            if (permitUpdate.permitStatusChangeAction) {
                iconColor = 'text-success';
            } else if (!permitUpdate.permitStatusChangeAction && permitUpdate.permitAction && permitUpdate.permitAction.includes('CONFLICT')) {
                iconColor = 'text-danger';
            } else if (!permitUpdate.permitStatusChangeAction && permitUpdate.permitAction && !permitUpdate.permitAction.includes('CONFLICT')) {
                iconColor = 'text-warning'
            } else {
                iconColor = '';
            }
        }

        return iconColor;
    }

    scrollTop() {
        jQuery('html, body, .content-wrap').animate({
            scrollTop: 0
        }, 2000);

    }

    reviewPermitUpdateClick(permitUpdate, index) {

        if (!permitUpdate) return;

        let pUpdate = new PermitUpdate();

        jQuery.extend(true, pUpdate, permitUpdate);
        if (permitUpdate.permitAction && permitUpdate.permitAction.includes('CONFLICT - PERMIT STATUS')) {
            this.scrollTop();
            this.editPermitUpdate = pUpdate;
            this.hideAddPanel(true);


            if (index || index == 0) {
                if (this.selectedRow == index) {
                    this.resetPanel();
                }
                else {
                    this.selectedRow = index;
                    this.hideEditPanel(false);
                }
            }


        }
        else if (permitUpdate.permitAction && permitUpdate.permitAction.includes('CONFLICT - PERMIT EXISTS')) {
            this.scrollTop();
            this.addPermitUpdate = pUpdate;
            this.hideEditPanel(true);

            if (index || index == 0) {
                if (this.selectedRow == index) {
                    this.resetPanel();
                }
                else {
                    this.selectedRow = index;
                    this.hideAddPanel(false);
                }
            }


        }

    }

    onNavigationEvent(event) {

        this.resetPanel();

        if (event.eventtype === 'nextrecord') {
            this.nextRecord = event.rowdata;
            if (!this.nextRecord) this.resetPanel();
            return;
        }

        if (event.eventtype == 'mvtorecord') {
            let permitUpdate = event.rowdata;
            let rowIndex = event.rowindex;

            this.selectedRow = rowIndex;
            // if (rowIndex == -1) {
            let thisref = this;
            setTimeout(function () {
                if (permitUpdate && permitUpdate.permitAction && permitUpdate.permitAction.includes('CONFLICT - PERMIT STATUS') && thisref.actionReqCnt != 0) {
                    thisref.editPermitUpdate = permitUpdate;
                    thisref.hideAddPanel(true);
                    thisref.hideEditPanel(false);
                } else if (permitUpdate && permitUpdate.permitAction && permitUpdate.permitAction.includes('CONFLICT - PERMIT EXISTS') && thisref.actionReqCnt != 0) {
                    thisref.addPermitUpdate = permitUpdate;
                    thisref.hideAddPanel(false);
                    thisref.hideEditPanel(true);
                } else {
                    thisref.editPermitUpdate = permitUpdate;
                    thisref.addPermitUpdate = permitUpdate
                    thisref.resetPanel();
                }
            }, 500);
        }
    }

    navigateToPg(url) {
        this.router.navigate(url);
    }

    afterSortOrderChange($event) {
        if (this.nextRecord) {
            this.triggerMoveToRecordEvent();
        }
    }

    triggerMoveToRecordEvent() {
        let thisref = this;
        //setTimeout needed to trigger change detection
        setTimeout(function () {
            // move to nextRecord; onNavigationEvent(event) will be called to set the 
            // nextRecord in the  queue and highligted on the grid

            // let einSrchVal = thisref.nextRecord ? thisref.nextRecord["einNum"] : "";
            let pNumSrchVal = thisref.nextRecord ? thisref.nextRecord["permitNum"] : "";

            let nvgAttr =
            {
                rowdata: thisref.nextRecord,
                cols: ["einNum", "permitNum"],
                colSearch: "permitNum",
                colSearchVal: [pNumSrchVal],
                action: ['mvtorecord']
            };
            thisref.updateNavigationAttrs(nvgAttr);
            //purge nextRecord for next record navigation done on a permit action
            thisref.nextRecord = null;
        }, 500);
    }
    // moveToNextRecord(permitUpdate) {
    //     this.nextRecord = permitUpdate;
    //     //Update the navigation attributes to get the next record
    //     this.updateNavigationAttrs();
    //     if (permitUpdate) {
    //         //Then update the results to update the sort
    //         this.getPermitUpdates(this.docId, false);
    //     }
    // }

    async  moveToNextRecord(permitUpdate: PermitUpdate) {
        if (permitUpdate) {
            // get and set the next record; onNavigationEvent(event) callback  will be called to set the nextRecord
            let nvgAttr =
            {
                rowdata: permitUpdate,
                cols: ["einNum", "permitNum"],
                colSearch: "permitAction",
                colSearchVal: ["ANY"],
                action: ['nextrecord']
            };
            this.updateNavigationAttrs(nvgAttr);

            // reload the grid and upon sort completion afterSortOrderChange($event) callback will be called 
            // to move to next record
            if (permitUpdate.uiAction !== "SKIP")
                await this.getPermitUpdates(this.docId, false);
            else
                // In case no action taken (e.g. SKIP) just move to the next record
                this.triggerMoveToRecordEvent();
        } else
            this.resetPanel();
    }

    private updateNavigationAttrs(nvgAttr: NavigationAttributes) {

        if (!nvgAttr.rowdata) {
            this.resetPanel();
            return;
        }
        this.navigationAttrs = nvgAttr;
    }

    hideEditPanel(hide) {
        this.isDisplayEditPanel = hide;
    }

    hideAddPanel(hide) {
        this.isDisplayAddPanel = hide;
    }

    resetPanel() {
        this.selectedRow = -1;
        this.hideAddPanel(true);
        this.hideEditPanel(true);
    }

    //has to be removed 
    formatPermit(value: string) {

        let prt1 = value.substring(0, 2);
        let prt2 = value.substring(2, 4);
        let prt3 = value.substring(4);
        return prt1 + "-" + prt2 + "-" + prt3;
    }

}
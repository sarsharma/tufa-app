/*
@author : Deloitte
this is Component for search company results
*/
import { ComponentFixture, async, inject, TestBed } from '@angular/core/testing';
import { PermitSearchCompanyResultsComponent } from './search-company-results.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core'; 
import { CommonModule }  from '@angular/common';
import { RouterModule, Routes, provideRoutes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpModule } from '@angular/http';
import { CompanyService } from '../services/company.service';
import { SharedModule } from '../../../../shared/shared.module';
import { PermitsCSVPipe } from '../../../../company/util/permits-csv-format.pipe';
import { HttpClient } from '../../../../../../node_modules/@angular/common/http';
import { AuthorizationHelper } from '../../../../authentication/util/authorization.helper.util';

declare var jQuery: any;

describe('Search company results', () => {
  let comp:    PermitSearchCompanyResultsComponent;
  let fixture: ComponentFixture<PermitSearchCompanyResultsComponent>;
  let de:      DebugElement;
  let el:      HTMLElement;

  // provide our implementations or mocks to the dependency injector
  beforeEach( async( () => {
    TestBed.configureTestingModule({
      imports: [ SharedModule, HttpModule, RouterTestingModule ],
      declarations: [PermitSearchCompanyResultsComponent, PermitsCSVPipe],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [HttpClient, AuthorizationHelper, CompanyService,
        provideRoutes([{path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTesting', pathMatch:'full'}])
      ]
    }).compileComponents();
  }));

   beforeEach(() => {
      fixture = TestBed.createComponent(PermitSearchCompanyResultsComponent);
      comp = fixture.componentInstance;
      fixture.detectChanges();
  });

  it('should create the search company results component', function() {
    expect(comp).toBeTruthy();
  });

});


import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from "lodash";
import { LocalStorageService } from 'ngx-webstorage';
import { Subscription, Subject } from 'rxjs';
import { ShareDataService } from '../../../../core/sharedata.service';
import { NavigationAttributes } from '../../../../shared/datatable/navigation.attributes.model';
import { PaginationAttributes } from '../../../../shared/datatable/pagination.attributes.model';
import { FileFilter, FileFilterData } from '../models/form7501filter.model';
import { CompanyEventService } from '../services/company.event.service';
import { AddressContactVO } from '../services/company.event.service';
import { CompanyService } from '../services/company.service';

declare var jQuery: any;

@Component({
    selector: '[permit-search-company-results]',
    templateUrl: './search-company-results.template.html',
    styleUrls: ['./search-criteria.style.scss'],
    providers: [CompanyService],
    encapsulation: ViewEncapsulation.None
})


/**
 *
 *
 * @export
 * @class SearchCompanyResultsComponent
 * @implements {OnInit}
 */
export class PermitSearchCompanyResultsComponent implements OnInit, OnDestroy {
    public router: Router;
    public data: any[];
    public busy: Subscription;
    selectedRec: Number;
    public pageNumCompanyResults: number = 1;
    @Input() selected: any;
    @Input() companyResults: any;
    @Input() decider: any;
    @Input() datatableName;

    @Output() getAssociateAddress = new EventEmitter<any>();
    @Output() getAssociateContact = new EventEmitter<any>();
    @Output() updateContactResults = new EventEmitter<any>();
    @Output() updateAddressResults = new EventEmitter<any>();
    @Output() close = new EventEmitter<any>();
    pgAttrs: PaginationAttributes;
    private routerSubscription: Subscription;
    activePage: number = 1; rowsOnPage: number = 25;
    contactItems: number; addressItems: number; totalItems: number;
    formFilter = new FileFilter();
    shareData: ShareDataService;
    formFilterSet = { file: this.formFilter };
    filterData = new FileFilterData();
    navigateToCompanydetail: boolean;
    private navigatedFrmCmpyDetails: boolean = false;
    private mfAmountOfRows = 0;

    itemsCntFlag = false;

    public navigationAttrs: NavigationAttributes;
    public nextRecord;

    public subscriptions: Subscription[] = [];


    //subscription to be used for call back on grid setting.
    public mvRecOnSortSubject = new Subject();
    public currentAction: string;


    sortBy = ["statusSortFunction", "businessName"];
    sortOrder = ["desc", "asc"];
    sortFunction = [{
        name: "statusSortFunction",
        func: (company: any) => {
            return this.decider === 'address' ? company.addressStatus : company.contactStatus;
        }
    }];

    public columns: Array<any> = [
        { name: 'businessName', type: 'input', filtering: { filterString: '', columnName: 'businessName', placeholder: 'Business Name', class: 'file' } },
        { name: 'einNum', type: 'input', filtering: { filterString: '', columnName: 'einNum', placeholder: 'EIN', class: 'file' } },
        { name: 'permitNum', type: 'input', filtering: { filterString: '', columnName: 'permitNum', placeholder: 'Permit #', class: 'file' } },
        { name: 'resultsStatus', type: 'input', filtering: { filterString: '', columnName: 'resultsStatus', placeholder: 'Status', class: 'file' } },
    ];

    constructor(private companyService: CompanyService,
        private companyEventService: CompanyEventService,
        private storage: LocalStorageService,
        router: Router,
        private _shareData: ShareDataService
    ) {
        this.shareData = _shareData;
        this.router = router;
        if (this.shareData.previousRoute.includes("details")
            || this.shareData.previousRoute.includes("history")
            || this.shareData.previousRoute.includes("impreport")
            || this.shareData.previousRoute.includes("manreport"))
            this.navigatedFrmCmpyDetails = true;

        this.pgAttrs = new PaginationAttributes(1, 25, null, null, [], "asc");

        this.navigateToCompanydetail = false;

        //Conditional Scrolling to the top of the page. If actions buttons
        //(REPLACE,IGNORE,SKIP etc.) are clicked do not scroll to the top of the page
        //("scroll" will be passed as "false" in subscription) else if "Review" link is 
        //clicked on the grid scroll to the top ("scroll" will be passed as "true" in the subscription).
        this.subscriptions.push(this.companyEventService.pgScrollTrigger$.subscribe(scroll => {
            if (scroll)
                this.scrollTop();
        }));

        this.subscriptions.push(this.mvRecOnSortSubject.subscribe(nextRecord => {
            this.loadAddressPOCQueue({ company: nextRecord, scroll: false });
            this.moveToNextRecord();
            //On action completion
            this.currentAction = "";
        }));

        // Called when action button (REPLACE,IGNORE,SKIP etc.) clicked in the queue panel. Following is the psuedo code run.
        // 1. Find and set the next record in the grid by calling this.nextRecordToNavigate().
        // 2. Move to next record on afterSortOrderChange() callback or if this.currentAction = "SKIP" in  onNavigationEvent(event)
        //    by invoking  this.mvRecOnSortSubject.next(this.nextRecord);
        // 3. Inside mvRecOnSortSubject<any>() subscription, load the queue data to display next record and highlight the next record on the grid.
        this.companyEventService.moveToNextRecord$.subscribe(
            company => {
                if (company.uiAction === "SKIP")
                    this.currentAction = "SKIP";
                //set the next record to be navigated
                this.nextRecordToNavigate(company);
            }
        );
    }

    /**
     *
     * @memberOf SearchCompanyResultsComponent
     */
    ngOnInit() {
        this.data = this.companyResults;
    }

    ngAfterViewInit() {
        for (let col of this.columns) {
            if (col.filtering) {
                this.cosmeticFix('#file-filter-' + col.name);
            }
        }
    }

    cosmeticFix(elementID: any) {
        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let filterInput = jQuery(elementID);
        filterInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

    /**
     *
     *
     * @param {SimpleChanges} changes
     *
     * @memberOf SearchCompanyResultsComponent
     */
    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "companyResults") {
                this.data = changes[propName].currentValue;
                if (this.data) {
                    this.totalItems = this.data.length;
                    this.addressItems = this.totalItems - _.size(_.filter(this.data, function (record) { return record.addressStatus ? true : false; }));
                    this.contactItems = this.totalItems - _.size(_.filter(this.data, function (record) { return record.contactStatus ? true : false; }));
                    this.itemsCntFlag = ((this.decider === "address" && this.addressItems) || (this.decider !== "address" && this.contactItems)) ? true : false;
                }
            } else if (propName === "selected")
                this.selectedRec = changes[propName].currentValue;
        }
    }

    loadData(company, index) {
        if (!company || (!index && index != 0)) return;

        this.loadAddressPOCQueue({ company: company, scroll: true });
        this.nextRecordToNavigate(company);
    }

    private moveToNextRecord() {

        if (!this.nextRecord) return;

        let statusCol: string = this.decider === "address" ? "addressStatus" : "contactStatus";
        let searchVal = this.nextRecord.permitUpdateId;

        let nvgAttr =
        {
            rowdata: this.nextRecord,
            cols: ["permitUpdateId"],
            colSearch: "permitUpdateId",
            colSearchVal: [searchVal],
            action: ['mvtorecord']
        };

        this.updateNavigationAttrs(nvgAttr);
        //purge record once navigation complete
        this.nextRecord = null;

    }

    private nextRecordToNavigate(company) {

        let statusCol: string = this.decider === "address" ? "addressStatus" : "contactStatus";

        let nvgAttr =
        {
            rowdata: company,
            cols: ["permitUpdateId"],
            colSearch: statusCol,
            colSearchVal: [null, "IGNR", "ADDED", "REPLD"],
            action: ["nextrecord"]
        };

        this.updateNavigationAttrs(nvgAttr);

    }

    private updateNavigationAttrs(nvgAttr: NavigationAttributes) {

        if (!nvgAttr.rowdata) {
            return;
        }
        this.navigationAttrs = nvgAttr;
    }

    afterSortOrderChange($event) {
        if (this.nextRecord)
            this.mvRecOnSortSubject.next(this.nextRecord);

    }

    loadAddressPOCQueue(vo: AddressContactVO) {
        if (vo.company) {
            if (this.decider === "address") {
                this.companyEventService.loadAssociatedAddresses(vo);
            }
            else {
                this.companyEventService.loadAssociatedContacts(vo);
            }
            this.navigateToCompanydetail = true;

        } else {
            this.close.emit();
        }
    }


    onNavigationEvent(event) {
        //callback to set next record or set selected index once move to record event complete        
        let company = event.rowdata;
        if (event.eventtype === 'nextrecord') {
            this.nextRecord = event.rowdata;
            if (this.currentAction == "SKIP")
                this.mvRecOnSortSubject.next(this.nextRecord);
            return;
        }

        if (event.eventtype === 'mvtorecord') {
            this.selectedRec = company.permitUpdateId;
            this.nextRecord = null;
            return;
        }
    }


    isActive(item) {
        return this.selectedRec === item;
    };

    toCompany(einNum) {
        this.navigateToCompanydetail = true;
        this.shareData.breadcrumb = "permit-mgmt";
        this.shareData.filemgmttab = this.decider === "address" ? 'address' : 'contact';
        this.router.navigate(["/app/company/details", { ein: einNum }]);
    }

    updateFilters() {
        for (let col of this.columns) {
            let filterinfo = col.filtering;
            if (filterinfo) {
                if (filterinfo.columnName === "businessName") {
                    this.formFilter.businessNameFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "einNum") {
                    this.formFilter.einNumFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "permitNum") {
                    this.formFilter.permitNumFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "resultsStatus") {
                    this.formFilter.resultsStatusFilter = filterinfo.filterString.trim();
                    this.formFilter.fileTypeFilter = this.decider;
                }
            }
        }
    }

    isValidString(str) {
        return ((str != null) && (typeof str !== 'undefined') && (str.length >= 0));
    }

    public onChangeTable(filterinfo): void {
        if (filterinfo) {
            if (filterinfo.columnName === "businessName") {
                this.formFilterSet[filterinfo.class].businessNameFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "einNum") {
                this.formFilterSet[filterinfo.class].einNumFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "permitNum") {
                this.formFilterSet[filterinfo.class].permitNumFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "resultsStatus") {
                this.formFilterSet[filterinfo.class].resultsStatusFilter = filterinfo.filterString.trim();
                this.formFilterSet[filterinfo.class].fileTypeFilter = this.decider;
            }

            if (filterinfo.refresh) {
                if (filterinfo.class === "file")
                    this.refreshFormFilterObject(this.formFilterSet[filterinfo.class]);
            }
        }
    }

    scrollTop() {
        jQuery('html, body, .content-wrap').animate(
            { scrollTop: 0 }, 500
        );
    }


    restoreFilterData(filterData: FileFilterData) {


        if (!filterData) return;
        this.filterData = new FileFilterData();

        for (let i in this.columns) {

            if (this.columns[i].name == "businessName") {
                this.columns[i].filtering.filterString = filterData._businessNameFilter;
                this.filterData._businessNameFilter = filterData._businessNameFilter;
            }

            if (this.columns[i].name == "einNum") {
                this.columns[i].filtering.filterString = filterData._einNumFilter;
                this.filterData._einNumFilter = filterData._einNumFilter;

            }

            if (this.columns[i].name == "permitNum") {
                this.columns[i].filtering.filterString = filterData._permitNumFilter;
                this.filterData._permitNumFilter = filterData._permitNumFilter;

            }

            if (this.columns[i].name == "resultsStatus") {
                this.columns[i].filtering.filterString = filterData._resultsStatusFilter;
                this.filterData._resultsStatusFilter = filterData._resultsStatusFilter;
                if (this.decider === "address")
                    this.filterData.fileTypeFilter = "address";
                else
                    this.filterData.fileTypeFilter = "contact";

            }

        }
    }
    applyFilter() {
        this.refreshFormFilterObject(this.formFilter);
    }
    refreshFormFilterObject(formFilter: FileFilter) {
        this.filterData = null;
        this.filterData = new FileFilterData();
        this.filterData.businessNameFilter = formFilter.businessNameFilter;
        this.filterData.einNumFilter = formFilter.einNumFilter;
        this.filterData.permitNumFilter = formFilter.permitNumFilter;
        this.filterData.resultsStatusFilter = formFilter.resultsStatusFilter;
        this.filterData.fileTypeFilter = formFilter.fileTypeFilter;
        if (this.decider === "address") {
            this.updateAddressResults.emit(this.filterData);
        } else this.updateContactResults.emit(this.filterData);
    }

    ngOnDestroy() {

        if (!this.navigateToCompanydetail)
            this.storage.clear('apAttributes');

        if (this.routerSubscription)
            this.routerSubscription.unsubscribe();

        for (let i in this.subscriptions)
            this.subscriptions[i].unsubscribe();
    }
}
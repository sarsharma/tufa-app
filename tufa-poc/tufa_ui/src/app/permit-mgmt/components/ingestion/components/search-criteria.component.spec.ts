/*
@author : Deloitte
this is test component for Search Criteria page or Daily Operations page.
*/
import { ComponentFixture, async, inject, TestBed } from '@angular/core/testing';
import { SearchCriteriaPage } from './search-criteria.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core'; 
import { CommonModule }  from '@angular/common';
import { RouterModule, Routes, provideRoutes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpModule } from '@angular/http';
import { ContactService } from '../services/contact.service';
import { SharedModule } from '../../../../shared/shared.module';
import { HttpClient } from '../../../../../../node_modules/@angular/common/http';
import { AuthorizationHelper } from '../../../../authentication/util/authorization.helper.util';

declare var jQuery: any;

describe('Daily operations', () => {
  let comp:    SearchCriteriaPage;
  let fixture: ComponentFixture<SearchCriteriaPage>;
  let de:      DebugElement;
  let el:      HTMLElement;

  // provide our implementations or mocks to the dependency injector
  beforeEach( async( () => {
    TestBed.configureTestingModule({
      imports: [ SharedModule, HttpModule, RouterTestingModule ],
      declarations: [SearchCriteriaPage],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [HttpClient, AuthorizationHelper, ContactService,
        provideRoutes([{path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTesting', pathMatch:'full'}])
      ]
    }).compileComponents();
  }));

   beforeEach(() => {
      fixture = TestBed.createComponent(SearchCriteriaPage);
      comp = fixture.componentInstance;
      fixture.detectChanges();
  });
 
  it('should create daily operations page', function() {
    expect(comp).toBeTruthy();
  });

  it('user switches the search type', async(() => {     
    expect(fixture.debugElement.nativeElement.querySelector('#keyword-search-input') === null).toBe(false);
    expect(fixture.debugElement.nativeElement.querySelector('#search-type') === null).toBe(false);
    expect(fixture.debugElement.nativeElement.querySelector('#search-status') === null).toBe(true);    

    let elSearchType:HTMLSelectElement = fixture.debugElement.nativeElement.querySelector('#search-type');    
    if(elSearchType){
        // it has two options
        fixture.detectChanges();   
        expect(elSearchType.options.length).toEqual(2);
        // comp.selectedSearchType = "COMPANY";
        fixture.detectChanges();   
        fixture.whenStable().then(() => {        
          expect(elSearchType.value).toEqual("COMPANY"); 
          // dispatchEvent(elSearchType, 'change');
          fixture.detectChanges();          
          fixture.whenStable().then(() => {
            // comp.selectedSearchType = "REPORT";
            fixture.detectChanges();  
            fixture.whenStable().then(() => {              
              expect(elSearchType.value).toEqual("REPORT");   
            })
          })
        })
    }
  }));

  it('user able to see create company and monthly report button', async(() => {
    let elCreateCompany:HTMLButtonElement = fixture.debugElement.nativeElement.querySelector('#create-company'); 
    if(elCreateCompany){
      //elCreateCompany.click();
    }        
  }));


  it('user able to see click create company', async(() => {
    expect(fixture.debugElement.nativeElement.querySelector('#create-company') === null).toBe(false);
    expect(fixture.debugElement.nativeElement.querySelector('#create-monthly') === null).toBe(false);
  }));  
  

  //http://stackoverflow.com/questions/39793405/how-to-change-value-of-a-select-box-in-angular2-unit-test

    // it('should be navigate to correct url for each option in navmenu',
    //     async(inject([Router, Location], (router: Router, location: Location) => {

    //     let fixture = TestBed.createComponent(AppComponent);
    //     fixture.detectChanges();

    //     fixture.debugElement.query(By.css('span.hlink[routerLink="/profile"]')).nativeElement.click();
    //     fixture.whenStable().then(() => {
    //     expect(location.path()).toEqual('/profile');
    //     expect(fixture.debugElement.query(By.css('span.hlink[routerLink="/profile"]')).classes['active']).toBeTruthy();
    //     expect(fixture.debugElement.nativeElement.querySelectorAll('span.hlink.active').length).toEqual(1)

    //     fixture.debugElement.query(By.css('span.hlink[routerLink="/login"]')).nativeElement.click();
    //     return fixture.whenStable();
    //     }).then(() => {
    //     expect(location.path()).toEqual('/login');
    //     expect(fixture.debugElement.query(By.css('span.hlink[routerLink="/login"]')).classes['active']).toBeTruthy();
    //     expect(fixture.debugElement.nativeElement.querySelectorAll('span.hlink.active').length).toEqual(1)

    //     fixture.debugElement.query(By.css('span.hlink[routerLink="/signup"]')).nativeElement.click();
    //     return fixture.whenStable();
    //     }).then(() => {
    //     expect(location.path()).toEqual('/signup');
    //     expect(fixture.debugElement.query(By.css('span.hlink[routerLink="/signup"]')).classes['active']).toBeTruthy();
    //     expect(fixture.debugElement.nativeElement.querySelectorAll('span.hlink.active').length).toEqual(1)

    //     fixture.debugElement.query(By.css('span.hlink[routerLink="/"]')).nativeElement.click();
    //     return fixture.whenStable();
    //     }).then(() => {
    //     expect(location.path()).toEqual('/');
    //     expect(fixture.debugElement.query(By.css('span.hlink[routerLink="/"]')).classes['active']).toBeTruthy();
    //     expect(fixture.debugElement.nativeElement.querySelectorAll('span.hlink.active').length).toEqual(1)
    //     });

    // }))
    // );
  

});

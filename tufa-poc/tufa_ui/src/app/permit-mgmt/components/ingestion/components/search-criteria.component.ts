
import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ShareDataService } from '../../../../core/sharedata.service';
import { PermitPeriodService } from '../../../../permit/services/permit-period.service';
import { CompanyService } from '../services/company.service';
import { PermitUpdatesMgmtComponent } from './permit-mgmt.updates.component';
import { CompanyMgmtResultsComponent } from './company-mgmt.results.component';
import { TabDirective, TabsetComponent } from '../../../../../../node_modules/ngx-bootstrap';
import { AddressMgmtPage } from './address-mgmt.component';
import { ContactMgmtPage } from './contact-mgmt.component';
import { TTBPermitExcludeComponent} from './ttb-permit-exclude/ttb-permit-exclude.component';

declare var jQuery: any;
export enum SearchPageMode {
    DAILY_OPERATIONS = 1,
    NAVIGATED_BACK,
    NEW_SEARCH
};

@Component({
    selector: '[search-criteria]',
    templateUrl: './search-criteria.template.html',
    styleUrls: ['./search-criteria.style.scss'],
    providers: [CompanyService, PermitPeriodService],
    encapsulation: ViewEncapsulation.None
})

/**
 * SearchCriteriaPage is the Daily Operations Landing Page
 *
 * @export
 * @class SearchCriteriaPage
 * @implements {OnInit}
 */
export class SearchCriteriaPage implements OnInit, OnDestroy {

    @ViewChild(PermitUpdatesMgmtComponent) permitmgmtcomponent: PermitUpdatesMgmtComponent;
    @ViewChild(CompanyMgmtResultsComponent) cmpymgmtcomponent: CompanyMgmtResultsComponent;
    @ViewChild(TTBPermitExcludeComponent)  ttbPermitExcludeComponent: TTBPermitExcludeComponent;
    @ViewChild(AddressMgmtPage) addressMgmtPage: AddressMgmtPage;
    @ViewChild(ContactMgmtPage) contactMgmtPage: ContactMgmtPage;
    @ViewChild("permitMgmtTabs") permitMgmtTabs: TabsetComponent;

    public route: ActivatedRoute;
    public router: Router;
    public companyAddressData: any[];
    public companycontactData: any[];

    public keywordSearch: string;
    public busy: Subscription;
    public pagemode: SearchPageMode;
    shareData: ShareDataService;
    public showReportFilter: boolean;
    public showErrorFlag: boolean;
    public alerts: Array<Object>;
    switchFilter: number;
    public docId;
    filemgmttab: any;


    constructor(private companyService: CompanyService,
        private _permitPeriodService: PermitPeriodService,
        private _route: ActivatedRoute,
        router: Router,
        private _shareData: ShareDataService
    ) {
        this.route = _route;
        this.router = router;
        this.shareData = _shareData;
        this.showErrorFlag = false;
        this.filemgmttab = this.shareData.filemgmttab;
    }

    /**
     *
     * ngOnInit that initializes the Search Criteria Page
     *
     * @memberOf SearchCriteriaPage
     */
    ngOnInit(): void {
        //this.filemgmttab = this.shareData.filemgmttab;
        this.cosmeticFix('#keyword-search-input');
        this.showErrorFlag = false;

        this.fetchSearchResults();


    }

    // filemgmttab determines the source of origin of navigation. Depending on reconciliation or quarterly assessment
    // appropriate tabs will be shown as active.
    // ngAfterViewInit() {
    //     if (this.filemgmttab === 'address') {
    //         jQuery('.nav-pills a[href="#tab3"]').tab('show');
    //         this.switchFilter = 3;
    //     } else if (this.filemgmttab === 'permit') {
    //         jQuery('.nav-pills a[href="#tab2"]').tab('show');
    //         this.switchFilter = 2;
    //     } else if (this.filemgmttab === 'company') {
    //         jQuery('.nav-pills a[href="#tab1"]').tab('show');
    //         this.switchFilter = 1;
    //     } else {
    //         jQuery('.nav-pills a[href="#tab4"]').tab('show');
    //         this.switchFilter = 4;
    //     }
    // }

    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }
    }

    /**
     * Get data dependent on the tab selected
     * @param data tab data
     * @throws Error is the provided heading does not match any of the tabs
     */
    onSelect(data: TabDirective): void {
        //When tab is set to active from fetchCompanySearchResults this is called and there is not heading data
        if(data.heading) {
            switch (data.heading) {
                case "Addresses":
                    this.getcompanyaddress();
                    break;
                case "Permits":
                    this.getPermits();
                    break;
                case "Exclude Tab":
                    this.getTTBPermitExclusion()
                    break;
                case "Companies":
                    this.getCompanies();
                    break;
                case "Contacts":
                    this.getcompanycontacts();
                    break;
                
                default:
                    throw (new Error("FileManagement - No such tab: " + data.heading));
            }
        }
    }

    getcompanyaddress() {
        this.route.params.subscribe(params => {
            if (params['docId']) {
                let docId = params['docId'];
                this.docId = docId;
                this.shareData.docId = docId;
                this.addressMgmtPage.getAddresses(docId);
            }
        });
    }

    getcompanycontacts() {
        this.route.params.subscribe(params => {
            if (params['docId']) {
                let docId = params['docId'];
                this.docId = docId;
                this.shareData.docId = docId;
                this.contactMgmtPage.getContacts(docId);
            }
        });
    }

    getCompanies() {
        this.route.params.subscribe(params => {
            let docId = params['docId'];
            this.docId = docId;
            this.shareData.docId = docId;
            this.cmpymgmtcomponent.getPermitUpdates(docId, true);
        });
    }

    getPermits() {
        this.route.params.subscribe(params => {
            let docId = params['docId'];
            this.docId = docId;
            this.shareData.docId = docId;
            this.permitmgmtcomponent.getPermitUpdates(docId, true);
        });
    }

    getTTBPermitExclusion(){
        this.route.params.subscribe(params => {
            let docId = params['docId'];
            this.docId = docId;
            this.shareData.docId = docId;
            this.ttbPermitExcludeComponent.getTTBPermitExclusions(docId);
        });

    }
    /**
     *
     *
     * @param {String} searchtxt
     *
     * @memberOf SearchCriteriaPage
     */
    fetchCompanySearchResults() {

        // since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
        if (this.busy) {
            this.busy.unsubscribe();
        }

        // if (this.filemgmttab === 'address') {
        //     this.getcompanyaddress();
        // } else if (this.filemgmttab === 'permit') {
        //     this.getPermits();
        // } else if (this.filemgmttab === 'company') {
        //     this.getCompanies();
        // } else {
        //     this.getcompanycontacts();
        // }

        if (this.filemgmttab === 'company') {
            this.getCompanies();
            this.permitMgmtTabs.tabs[0].active = true;
        } else if (this.filemgmttab === 'permit') {
            this.getPermits();
            this.permitMgmtTabs.tabs[1].active = true;
        } else if(this.filemgmttab === 'exclude') {
            this.getTTBPermitExclusion();
            this.permitMgmtTabs.tabs[2].active = true;
        }else if (this.filemgmttab === 'address') {
            this.getcompanyaddress();
            this.permitMgmtTabs.tabs[3].active = true;
        } else {
            this.getcompanycontacts();
            this.permitMgmtTabs.tabs[4].active = true;
        }
    }

    /**
     *
     *
     * @param {String} searchtxt
     *
     * @memberOf SearchCriteriaPage
     */
    fetchSearchResults() {

        this.showErrorFlag = false;
        this.companycontactData = null;
        this.companyAddressData = null;

        // search for reports
        this.fetchCompanySearchResults();
    }

    /**
     * This cosmetic fix is to allow the blue focus highlight for the input group including
     * the search icon on the textbox
     * @param {*} elementID
     *
     * @memberOf SearchCriteriaPage
     */
    cosmeticFix(elementID: any) {
        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let searchInput = jQuery(elementID);
        searchInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

}

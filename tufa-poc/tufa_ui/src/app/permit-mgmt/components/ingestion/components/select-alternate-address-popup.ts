/*
@author : Deloitte
this is Componentfor adding contact as a popup.
*/
import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';


declare var jQuery: any;

@Component({
  selector: '[select-altnaddress]',
  templateUrl: './select-alternate-address-popup.template.html'
})

export class SelectAlternateAddressPopup implements OnInit, OnDestroy {

  @Input() primaryAddress: any;
  @Input() alternateAddress: any;

  @Output() onSelectingAddr = new EventEmitter<any>();

  selAddressTypeCd: string = "PRIM"; // deafult set to prim

  constructor() {

  }

  ngOnInit(): void {

  }

  saveSelAddress() {
    // console.log("Inside Save"+this.selAddressTypeCd);
    this.onSelectingAddr.emit(this.selAddressTypeCd);
    this.popUpClose();
    //console.log("Completed Save");
  }

  popUpClose() {
    //console.log("popUpClose");
    this.selAddressTypeCd = "PRIM";
    jQuery('#select-altnaddress').modal('hide');
    jQuery('#select-altnaddress-content').removeAttr('style');
  }
  private resetPopUpState() {
    this.selAddressTypeCd = "PRIM";
  }

  ngOnDestroy() {
    this.popUpClose();
    jQuery('.modal-backdrop').removeClass('modal-backdrop');

  }

  onAddressSelectedChange() {
    // console.log("onAddressSelectedChange");
  }
}

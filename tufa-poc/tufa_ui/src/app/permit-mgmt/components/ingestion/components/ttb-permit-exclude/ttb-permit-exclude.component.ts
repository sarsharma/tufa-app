
/*
@author : Deloitte
This component displays TTB permit list with exclusion conflicts 
*/
import { Component, OnDestroy, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { CompanyService } from '../../services/company.service';
import { ShareDataService } from '../../../../../core/sharedata.service';
import { AgGridStateMgmtService } from '../../../../../shared/service/ag-grid.state.mgmt.service';

import { PermitMgmtService } from '../../../../services/permit-mgmt.service';

import { AgGridComponent } from '../../../../../shared/components/ag-grid/tufa.ag-grid.component';
import { environment } from "../../../../../../environments/environment";
import { serverSideDataSourceProvider } from '../../../../../shared/providers/ag-grid/ag-grid.providers';
import { agGridStateMgmtProvider } from '../../../../../shared/providers/ag-grid/ag-grid.providers';
import { ServerSideDataSourceService } from '../../../../../shared/service/ag-grid.datasource.service';

import { CheckBoxCellRenderer } from '../../../../../shared/ag-grid-cell-renderers/check-box-cell-renderer.component';
import { CustomTooltip } from '../../../../../shared/ag-grid-cell-renderers/tool-tip-cell-renderer.component';
import { RouterLinkCellRenderer } from '../../../../../shared/ag-grid-cell-renderers/routerlink-cell-renderer.component';
import { IconCellRenderer } from '../../../../../shared/ag-grid-cell-renderers/icon-cell-renderer.component';

import { RouterGlobalService } from "../../../../../shared/service/router.global.service";
import { LocalStorageService } from 'ngx-webstorage';

import 'ag-grid-enterprise';
import { PermitUpdate } from '../../models/permit-update.model';

@Component({
    selector: '[ttb-permit-exclude]',
    templateUrl: './ttb-permit-exclude.template.html',
    providers: [CompanyService, agGridStateMgmtProvider, serverSideDataSourceProvider],
    encapsulation: ViewEncapsulation.None
})

// TODO: ng lint, Comments
export class TTBPermitExcludeComponent extends AgGridComponent implements OnInit, OnDestroy {

    public router: Router;
    public data: any;
    public busy: Subscription;

    public actionReqCnt: number;
    public shareData: ShareDataService;

    public serverDataSourceUrl = environment.apiurl + "/api/v1/permit/aggrid/history";
    public gridName = "ttb_permit_exclude";

    public rowData = [];
    private rowGroupPanelShow = 'always';

    private versionName: string;
    private selectgridversion: number;

    public serverSidePaginationEnabled;

    public columnDefs;

    public frameworkComponents;
    public context;

    public defaultExportParams;
    
    onGridReady(params) {
        super.onGridReady(params, this.serverDataSourceUrl, this.gridName);
    }

    constructor(
        private _shareData: ShareDataService,
        public _agGridStateMgmtService: AgGridStateMgmtService,
        public _serverSideDataSource: ServerSideDataSourceService,
        private _permitMgmtService: PermitMgmtService,
        public routerService: RouterGlobalService,
        public storage: LocalStorageService

    ) {

        super(_agGridStateMgmtService, _serverSideDataSource,routerService,storage);
        this.shareData = _shareData;

        this.serverSidePaginationEnabled = false;

        this.columnDefs = [
            {
                headerName: 'Company Name', field: 'companyName', enableRowGroup: true, colId: "companyname",
                cellRendererParams: {
                    routerLink: '/app/company/details',
                    fieldParams: ['einNum'],
                    queryParams: ['ein']
                },
                cellRenderer: "routerLinkCellRenderer",
                width: 250
            },
            { headerName: 'EIN', field: 'einNum', enableRowGroup: true, colId: "ein", width: 150 },
            {
                headerName: 'Permit', field: 'permitNum', colId: "permitnum",
                valueFormatter: this.permitFormatter,
                width: 150
            },
            {
                headerName: 'Permit Status', field: 'permitStatus', colId: "permitstatus"
                , valueGetter: function (params) {

                    if (!params.data) return;

                    if (params.data.permitStatus == 'ACT') return 'ACTIVE'
                    else if (params.data.permitStatus == 'CLS') return 'CLOSED'
                    else return params.data.permitStatus;
                },
                width: 100
            },
            {
                headerName: 'Exclude Action', field: 'action', colId: "excludeaction",
                cellRenderer: 'iconCellRenderer',
                cellRendererParams: {
                    colorRenderCondition: function (params) {
                        if (params.data.permitExclResolved) return 'success'
                        else return 'danger';
                    },
                    iconType: 'circle-icon'
                },
                tooltipField: "action",
                tooltipComponentParams: { color: "#ececec" }
                ,width:200
            },
            {
                headerName: "Resolved?",
                field: 'permitExclResolved',
                cellRenderer: "checkBoxRenderer",
                width: 100
            }

        ];

        this.context = { componentParent: this };

        this.frameworkComponents = {
            checkBoxRenderer: CheckBoxCellRenderer,
            toolTipRenderer: CustomTooltip,
            routerLinkCellRenderer: RouterLinkCellRenderer,
            iconCellRenderer: IconCellRenderer
        };

        this.defaultColDef["tooltipComponent"] = "toolTipRenderer";
        this.urlsSaveState= ["/permit/impreport","/permit/manreport","/company/details","/permit/history","/permit-mgmt/address-mgmt"];
        this.storeGridState= true;
        this.defaultExportParams = {fileName:"Export Tab"}
    }


    getTTBPermitExclusions(docId) {

       let subs =  this._permitMgmtService.getTTBPermitExclusionUpdates(docId).subscribe(
            data => {
                let key = "grid-state-" + this.gridName + "-page";
                if(this.storage.retrieve(key)==null)
                    super.saveGridStateLocal();
                this.data = data;
                this.calcUnresolvedConflicts();
            }
        );

        this.subscriptions.push(subs);
    }

    onCheckBoxClck(data) {
        this.gridApi.showLoadingOverlay();
        let pUpdate = new PermitUpdate();
        pUpdate.permitAuditId = data.permitAuditId;
        pUpdate.permitExclResolved = data.permitExclResolved;
        let resolveSubs = this._permitMgmtService.resolveTTBPermitExclusion(pUpdate).subscribe(
            result => {
                this.calcUnresolvedConflicts();
                this.gridApi.redrawRows();
                this.gridApi.hideOverlay();
            }
        )
        this.subscriptions.push(resolveSubs);
    }

    calcUnresolvedConflicts() {

        let conflictArry: any[] = this.data;
        let total: number = 0;
        conflictArry.forEach((currentRw, index) => {
            if (!currentRw.permitExclResolved) total++;
        });
        this.actionReqCnt = total;
    }

    saveRoutingParams(rowData) {
        this.shareData.breadcrumb = "permit-mgmt";
        this.shareData.filemgmttab = "exclude";
    }

}
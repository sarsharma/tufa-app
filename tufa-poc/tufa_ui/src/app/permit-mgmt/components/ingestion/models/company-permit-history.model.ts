/*
@author : Deloitte
This is a model to hold Company permit history details.

*/
/**
 * 
 * 
 * @export
 * @class CompanyPermitHistory
 */
export class CompanyPermitHistory {
    companyName: string;
    companyId: number;
    permitId: number;
    reportPeriodId: number;
    permitTypeCd: string;
    permitStatusCd: string;
    permitNum: string;
    month: string;
    year: string;
    fiscalYear: string;
    displayMonthYear: string;
    sortMonthYear: string;
    reportStatusCd: string;
    activity: string;
    doc3852Id: number;
}
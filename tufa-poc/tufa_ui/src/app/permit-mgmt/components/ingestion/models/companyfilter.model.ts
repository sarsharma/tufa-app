import { DatatableFilter } from "../../../../shared/datatable/DataTableFilter";

export class CompanyFilter {
    companyNameFilter: String;
    einNumFilter: String;    
    companyStatusFilter: String;
    companyActionFilter: any[];
    results: any;
}

export class CompanyFilterData implements DatatableFilter{
     _companyNameFilter: String;    
     _einNumFilter: String;
     _companyStatusFilter: String;
     _companyActionFilter: any[];
     _resultsStatusFilter: String;

    get businessNameFilter(): any {
        return this._companyNameFilter;
    }
    set businessNameFilter(value: any) {
        this._companyNameFilter = value;
    }

    get einNumFilter(): any {
        return this._einNumFilter;
    }
    set einNumFilter(value: any) {
        this._einNumFilter = value;
    }

    get companyStatusFilter(): any {
        return this._companyStatusFilter;
    }
    set companyStatusFilter(value: any) {
        this._companyStatusFilter = value;
    }

    get resultsStatusFilter(): any {
        return this._resultsStatusFilter;
    }
    set resultsStatusFilter(value: any) {
        this._resultsStatusFilter = value;
    }

    get companyActionFilter(): any {
        return this._companyActionFilter;
    }
    set companyActionFilter(val: any) {
        this._companyActionFilter = val;
    }

    public isFilterSelected(): boolean{
        return (this._companyActionFilter||this._companyNameFilter||this._companyStatusFilter||this._einNumFilter||this._resultsStatusFilter)?true:false;
    }
 }

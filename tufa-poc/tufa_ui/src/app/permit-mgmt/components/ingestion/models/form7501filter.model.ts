import { DatatableFilter } from "../../../../shared/datatable/DataTableFilter";



export class FileFilter {
    businessNameFilter: String;
    einNumFilter: String;    
    permitNumFilter: String;
    resultsStatusFilter: String;
    fileTypeFilter: String;
    results: any;
}

export class FileFilterData implements DatatableFilter{
     _businessNameFilter: String;    
     _einNumFilter: String;
     _permitNumFilter: String;
     _resultsStatusFilter: String;
     _fileTypeFilter: String;

    get businessNameFilter(): any {
        return this._businessNameFilter;
    }
    set businessNameFilter(value: any) {
        this._businessNameFilter = value;
    }

    get einNumFilter(): any {
        return this._einNumFilter;
    }
    set einNumFilter(value: any) {
        this._einNumFilter = value;
    }

    get permitNumFilter(): any {
        return this._permitNumFilter;
    }
    set permitNumFilter(value: any) {
        this._permitNumFilter = value;
    }

    get resultsStatusFilter(): any {
        return this._resultsStatusFilter;
    }
    set resultsStatusFilter(value: any) {
        this._resultsStatusFilter = value;
    }

    get fileTypeFilter(): any {
        return this._fileTypeFilter;
    }
    set fileTypeFilter(value: any) {
        this._fileTypeFilter = value;
    }

     public isFilterSelected(): boolean{
        return (this._businessNameFilter||this._einNumFilter||this._permitNumFilter||this._resultsStatusFilter||this._fileTypeFilter)?true:false;
    }
 }

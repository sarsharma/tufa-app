import { Company } from "../../../../company/models/company.model";
import { Permit } from "../../../../permit/models/permit.model";

/*
@author : Deloitte
This is a model to hold Company details.

*/

/**
 * 
 * 
 * @export
 * @class Company
 */
export class PermitUpdate {
      permitAuditId: string;
      companyName: string;
      companyId: number;
      einNum: string;
      permitNum: string;
      permitAction: string;
      permitStatus: string;
      permitStatusTufa: string;
      docId: string;
      issueDt: string;
      closeDt: string;
      createdDt: string;
      actionDt: string;
      errors: string;
      companyStatus: string;
      companyAction: string;
      permitStatusChangeAction: string;
      companyStatusChangeAction: string;
      permitTypeCd: string;
      permitTufa: Permit;
      companyTufa: Company;
      lastRptDt: string;
      permitExclResolved: boolean;

      //value not use at the  backend
      uiAction: string;
}



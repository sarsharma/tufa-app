import { DatatableFilter } from "../../../../shared/datatable/DataTableFilter";



export class PermitFilter {
    companyNameFilter: String;
    einNumFilter: String;    
    permitNumFilter: String;
    permitStatusFilter: String;
    permitActionFilter: any[];
    results: any;
}

export class PermitFilterData implements DatatableFilter {
     _companyNameFilter: String;    
     _einNumFilter: String;
     _permitNumFilter: String;
     _permitStatusFilter: String;
     _permitActionFilter: any[];
     _resultsStatusFilter: String;

    get companyNameFilter(): any {
        return this._companyNameFilter;
    }
    set companyNameFilter(value: any) {
        this._companyNameFilter = value;
    }

    get einNumFilter(): any {
        return this._einNumFilter;
    }
    set einNumFilter(value: any) {
        this._einNumFilter = value;
    }

    get permitNumFilter(): any {
        return this._permitNumFilter;
    }
    set permitNumFilter(value: any) {
        this._permitNumFilter = value;
    }

    get resultsStatusFilter(): any {
        return this._resultsStatusFilter;
    }
    set resultsStatusFilter(value: any) {
        this._resultsStatusFilter = value;
    }

    get permitStatusFilter(): any {
        return this._permitStatusFilter;
    }
    set permitStatusFilter(value: any) {
        this._permitStatusFilter = value;
    }
    
    get permitActionFilter(): any {
        return this._permitActionFilter;
    }
    set permitActionFilter(value: any) {
        this._permitActionFilter = value;
    }

     public isFilterSelected(): boolean{
        return (this._companyNameFilter||this._einNumFilter||this._permitNumFilter||this._permitStatusFilter||this._permitActionFilter||this._resultsStatusFilter)?true:false;
    }

 }

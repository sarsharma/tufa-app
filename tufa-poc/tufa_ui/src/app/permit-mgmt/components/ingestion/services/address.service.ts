/*
@author : Deloitte
Service class to perform address related operations.
*/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { Address } from "./../../../../model/address.model";
import { environment } from "../../../../../environments/environment";
import { forEach } from "@angular/router/src/utils/collection";
import _ from "lodash";

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};
/**
 * 
 * 
 * @export
 * @class AddressService
 */
@Injectable()
export class AddressService {

    private createupdateaddressurl = environment.apiurl + '/api/v1/companies/address';
    private createupdateamultipleaddressurl = environment.apiurl + '/api/v1/companies/address/copyaddresstotufa';
    private createupdateprimaddressurl = environment.apiurl + '/api/v1/companies/address/setPrimaryAddress/';
    private getcountrynmurl = environment.apiurl + '/api/v1/companies/address/cntryNm';
    private getcountrycdurl = environment.apiurl + '/api/v1/companies/address/cntryCd';
    private getstatenmurl = environment.apiurl + '/api/v1/companies/address/stateNm';
    private getstateEntityurl = environment.apiurl + '/api/v1/companies/address/states';
    private getstatecdurl = environment.apiurl + '/api/v1/companies/address/stateCd';
    private getreportaddressurl = environment.apiurl + '/api/v1/companies/address/';

    constructor(private _httpClient: HttpClient) { }

    /**
     * 
     * 
     * @param {Address} address
     * @returns {Observable<Address>}
     * 
     * @memberOf AddressService
     */
    saveOrUpdateAddress(address: Address): Observable<Address> {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        let addressUpper = _.mapValues(address, function(s){ return _.isNull(s) ? null : _.isString(s) ? s.toUpperCase() : s; });
        return this._httpClient.put(this.createupdateaddressurl,addressUpper , httpOptions).pipe(
			tap(_ => this.log("saveOrUpdateAddress")),
			catchError(this.handleError<any>("saveOrUpdateAddress"))
		);

    }
    saveOrUpdatePrimaryAddress(address: Address,updatePrimToAltnFlag: String, setAltAddress: String): Observable<Address> {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        let addressUpper = _.mapValues(address, function(s){ return _.isNull(s) ? null : _.isString(s) ? s.toUpperCase() : s; });
        let setPrimAddresUrl = this.createupdateprimaddressurl+ updatePrimToAltnFlag +'/' + setAltAddress;
        return this._httpClient.put(setPrimAddresUrl, addressUpper, httpOptions).pipe(
			tap(_ => this.log("saveOrUpdateAddress")),
			catchError(this.handleError<any>("saveOrUpdateAddress"))
		);

    }

    saveOrUpdateMultipleAddress(address: Address[]): Observable<Address> {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        let addressupper : any=[];
        for(let address1 in address){
            addressupper[address1]= _.mapValues(address[address1], function(s){ return _.isNull(s) ? null : _.isString(s) ? s.toUpperCase() : s; });
        }
        return this._httpClient.put(this.createupdateamultipleaddressurl, (addressupper), httpOptions).pipe(
			tap(_ => this.log("saveOrUpdateAddress")),
			catchError(this.handleError<any>("saveOrUpdateAddress"))
		);

    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf AddressService
     */
    getCountryNames(): Observable<ConcatArray<string>>{

        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this._httpClient.get<ConcatArray<string>>(this.getcountrynmurl, httpOptions);
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf AddressService
     */
    getCountryCodes(){

        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this._httpClient.get(this.getcountrycdurl, httpOptions);
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf AddressService
     */
    getStateNames(){

        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this._httpClient.get(this.getstatenmurl, httpOptions);
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf AddressService
     */
    getStateEntities(){

        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this._httpClient.get(this.getstateEntityurl, httpOptions);
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf AddressService
     */
    getStateCodes(): Observable<string[]>{

        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this._httpClient.get<string[]>(this.getstatecdurl, httpOptions);
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf AddressService
     */
    getReportAddress(companyId: any, permitId: any, periodId: any ){
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        return this._httpClient.get(this.getreportaddressurl + companyId +"/" + permitId +"/" +periodId, httpOptions);
    }

    
	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}

}

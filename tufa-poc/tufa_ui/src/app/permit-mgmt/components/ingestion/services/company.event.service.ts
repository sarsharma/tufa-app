/*
@author : Deloitte
Service class to perform company related operations.
*/

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

export interface AddressContactVO {
    company: any;
    scroll:boolean;
}
/**
 * 
 * 
 * @export
 * @class CompanyEventService
 */
@Injectable()
export class CompanyEventService {
  private contactsLoad = new Subject<AddressContactVO>();
  private addressesLoad = new Subject<AddressContactVO>();

  private addressMatch = new Subject<any>();
  private contactMatch = new Subject<any>();

  contactMatch$ = this.contactMatch.asObservable();
  addressMatch$ = this.addressMatch.asObservable();
  contactsLoad$ = this.contactsLoad.asObservable();
  addressesLoad$ = this.addressesLoad.asObservable();

  private associateContactLoad = new Subject<any>();
  private associateAddressLoad = new Subject<any>();

  contactLoad$ = this.associateContactLoad.asObservable();
  addressLoad$ = this.associateAddressLoad.asObservable();

  private moveToNextRecord = new Subject<any>();
  moveToNextRecord$ = this.moveToNextRecord.asObservable();
  
  public pgScrollTrigger = new Subject<any>();
  pgScrollTrigger$ = this.pgScrollTrigger.asObservable();

  public loadAssociatedContacts(vo:AddressContactVO) {
    this.contactsLoad.next(vo);
  }

  public loadAssociatedAddresses(vo:AddressContactVO) {
    this.addressesLoad.next(vo);
  }

   public loadAssociatedContact(index: any) {
    this.associateContactLoad.next(index);
  }

  public loadAssociatedAddress(index: any) {
    this.associateAddressLoad.next(index);
  }

  public matchAddress(address: any) {
    this.addressMatch.next(address);
  }

  public matchContact(contact: any) {
    this.contactMatch.next(contact);
  }

  /**
   * Tell the shared grid Search-Company-Results
   * to navigate to the next record following
   * currentRecord
   * 
   * @argument currentRecord currntly selected record
   */
  public loadMovetoNextRecord(currentRecord: any) {
    this.moveToNextRecord.next(currentRecord);
  }
}
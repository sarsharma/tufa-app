/*
@author : Deloitte
Service class to perform company related operations.
*/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { environment } from "../../../../../environments/environment";
import { Company } from "../../../../company/models/company.model";
import { PermitHistoryCriteria } from "../../../../permit/models/permit-history-criteria.model";
import { PaginationAttributes } from "../../../../shared/datatable/pagination.attributes.model";
import { PermitUpdate } from "../models/permit-update.model";
import { Permit } from "./../../../../permit/models/permit.model";

const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
};

/**
 * 
 * 
 * @export
 * @class CompanyService
 */
@Injectable()
export class CompanyService {

    private saveupdatecompanyurl = environment.apiurl + '/api/v1/companies';
    private getcompanyurl = environment.apiurl + '/api/v1/companies/';
    private getcompanysearchurl = environment.apiurl + '/api/v1/companies/search';
    private getpermithistoryurl = environment.apiurl + '/api/v1/permit/history/';
    private getcriteriaurl = environment.apiurl + '/api/v1/permit/history';
    private getmonthlyreporturl = environment.apiurl + '/api/v1/companies/68/permits/history';
    private savepermiturl = environment.apiurl + 'api/v1/companies/26/permits';
    private deletemonthlyreporturl = environment.apiurl + '/api/v1/permit/deletemonthlyreport/';

    private basereconurl = environment.apiurl + '/api/v1/permitmgmt/permits';
    private getaddressnurl = environment.apiurl + '/api/v1/permitmgmt/permits/address';
    private getcontacturl = environment.apiurl + '/api/v1/permitmgmt/permits/contacts';

    private getPermitUpdatesUrl = environment.apiurl + '/api/v1/permitmgmt/permits/audit';


    constructor(private _httpClient: HttpClient) { }

    /**
     * 
     * 
     * @param {Company} company
     * @returns {Observable<Company>}
     * 
     * @memberOf CompanyService
     */
    createCompany(company: Company): Observable<Company> {
        return this._httpClient.post(this.saveupdatecompanyurl, (company), httpOptions).pipe(
            tap(_ => this.log("getUsers")),
            catchError(this.handleError<any>("getUsers"))
        );
    }

    /**
     * 
     * 
     * @param {Company} company
     * @returns {Observable<Company>}
     * 
     * @memberOf CompanyService
     */
    updateCompany(company: Company): Observable<Company> {

        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this._httpClient.put(this.saveupdatecompanyurl, (company), httpOptions).pipe(
            tap(_ => this.log("getUsers")),
            catchError(this.handleError<any>("getUsers"))
        );
    }

    /**
     * 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf CompanyService
     */
    getCompany(id) {
        return this._httpClient.get(this.getcompanyurl + id, httpOptions);
    }

    /**
    * 
    * 
    * @param {any} ein
    * @returns
    * 
    * @memberOf CompanyService
    */
    getCompanyEIN(ein: string): Observable<Company> {
        return this._httpClient.get(this.getcompanyurl + 'ein/' + ein, httpOptions).pipe(
            tap(_ => this.log("getCompanyEIN")),
            catchError(this.handleError<any>("getCompanyEIN"))
        );
    }

    /**
     * 
     * 
     * @param {Permit} permit
     * @returns {Observable<Company>}
     * 
     * @memberOf CompanyService
     */
    createPermit(permit: Permit){
        let savepermiturl = environment.apiurl + '/api/v1/companies/' + permit.companyId + '/permits';
        return this._httpClient.post(savepermiturl, permit, httpOptions);
    }


    /**
     * 
     * 
     * @param {string} companyId
     * @param {string} permitId
     * @returns
     * 
     * @memberOf CompanyService
     */
    getPermit(companyId: string, permitId: string) {
        let getupdatepermiturl = environment.apiurl + '/api/v1/companies/' + companyId + '/permits/' + permitId;
        return this._httpClient.get(getupdatepermiturl).pipe(
            tap(_ => this.log("getPermit")),
            catchError(this.handleError<any>("getPermit"))
        );
    }

    // Given permitId return Permit History
    /**
     * 
     * 
     * @param {string} permitId
     * @returns
     * 
     * @memberOf CompanyService
     */
    getPermitHistory(permitId: string) {
        return this._httpClient.get(this.getpermithistoryurl + permitId, httpOptions);

    }

    // Given permitId return Permit History with reports containing 3852 forms
    /**
     * 
     * 
     * @param {string} permitId
     * @returns
     * 
     * @memberOf CompanyService
     */
    getPermitHistory3852(permitId: string): Observable<Permit> {
        return this._httpClient.get(this.getpermithistoryurl + "report3852/" + permitId, httpOptions).pipe(
            tap(_ => this.log("getPermitHistory3852")),
            catchError(this.handleError<any>("getPermitHistory3852"))
        );

    }

    /**
     * 
     * 
     * @param {PermitHistoryCriteria} permitHistoryCritera
     * @returns
     * 
     * @memberOf CompanyService
     */
    getPermitReports(permitHistoryCritera: PermitHistoryCriteria, pgAttrs?: PaginationAttributes) {
        let url = pgAttrs ? this.getcriteriaurl + "?" + pgAttrs.getQueryParamString() : this.getcriteriaurl;
        return this._httpClient.put(url, (permitHistoryCritera), httpOptions);
    }

    /**
     * 
     * 
     * @param {CompanySearchCriteria} companySearchCriteria
     * @returns
     * 
     * @memberOf CompanyService
     */
    getCompanies(companySearchCriteria): Observable<any[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        return this._httpClient.put<any[]>(this.getaddressnurl, (companySearchCriteria), httpOptions);
    }

    /**
  * 
  * 
  * @param {CompanySearchCriteria} companySearchCriteria
  * @returns
  * 
  * @memberOf CompanyService
  */
    getContacts(companySearchCriteria): Observable<any[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        return this._httpClient.put<any[]>(this.getcontacturl, (companySearchCriteria), httpOptions);
    }


    /**
     * 
     * 
     * @param {CompanySearchCriteria} update
     * @returns
     * 
     * @memberOf CompanyService
     */
    updatePermit(updatedPermit) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        return this._httpClient.put(this.basereconurl + '/update', (updatedPermit), httpOptions);
    }

    updatePermitcontacts(updatedPermit) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        return this._httpClient.put(this.basereconurl + '/update/contacts', (updatedPermit), httpOptions);
    }

    getPermitUpdatesByDocument(documentId): Observable<PermitUpdate[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let url = this.getPermitUpdatesUrl + "/" + documentId;
        return this._httpClient.get(url, httpOptions).pipe(
            tap(_ => this.log("getPermitUpdatesByDocument")),
            catchError(this.handleError<any>("getPermitUpdatesByDocument"))
        );
    }


    /**
    * 
    * 
    * @param {Permit} permit
    * @returns {Observable<Company>}
    * 
    * @memberOf CompanyService
    */
    updateTufaPermit(permit: Permit): Observable<Company> {

        let getupdatepermiturl = environment.apiurl + '/api/v1/companies/' + permit.companyId + '/permits/' + permit.permitId;

        return this._httpClient.put(getupdatepermiturl, (permit), httpOptions).pipe(
            tap(_ => this.log("updateTufaPermit")),
            catchError(this.handleError<any>("updateTufaPermit"))
        );
    }



    getErrorsHTML(errorStr: string, errorType: string) {

        // errorStr = 'Row #:469 ; VALIDATION_ERRORS :[premisePhone:Invalid premise phone number] [email: Invalid email]'
        //     + '; COMPANY_ERROR: [Company already exists in TUFA]; COMPANY_ERROR: [ Company name mismatch ]'
        //     + '; PERMIT_ERROR: [Permit with same State exists]';

        let errorMap = this.getErrorMap(errorStr);

        let errors: string = errorMap[errorType];

        if (!errors) return;

        let rowNum = errorMap['Row #'];

        let htmlArry: string[] = [];

        let rowNoHTML = '<p class="no-margin" ><strong> Row #' + rowNum + '</strong></p>';
        htmlArry.push(rowNoHTML);

        let matches = errors.match(/\[.+?\]/g) || [];

        for (let i in matches) {
            let submatchHTML = matches[i];
            if (submatchHTML) {
                let err: string[] = submatchHTML.substring(submatchHTML.indexOf('[') + 1, submatchHTML.indexOf(']')).split(':');
                htmlArry.push('<p class="no-margin" ><strong>');

                if (err[1]) {
                    htmlArry.push(err[0].replace(/([A-Z])/g, ' $1')
                        .replace(/^./, function (str) { return str.toUpperCase(); }) + ":");
                    htmlArry.push('</strong><span class="breathe-left fw-normal">');
                    htmlArry.push(err[1]);
                    htmlArry.push('</span></p>');
                } else {
                    htmlArry.push(err[0]);
                    htmlArry.push('</strong></p>')
                }

            }
        }

        return htmlArry.join(' ');
    }


    getErrorMap(errorStr: string) {
        let errorMap = new Object();
        if (errorStr) {

            let errorTypes: string[] = errorStr.split(';');
            for (var i in errorTypes) {
                let indx = errorTypes[i].indexOf(':');
                let key = errorTypes[i].slice(0, indx).trim();
                let value = errorTypes[i].slice(indx + 1);

                errorMap[key] = (errorMap[key] ? errorMap[key] : '').concat(value);
            }

        }

        return errorMap;
    }


	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log("${operation} failed: ${error.message}");

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Attribute Service message with the MessageService */
    private log(message: string) {
        //this.messageService.add("Profile Service: ${message}");
    }
}
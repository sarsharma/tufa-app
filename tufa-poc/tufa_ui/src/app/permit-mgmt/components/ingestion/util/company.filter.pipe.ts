import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { CompanyFilterData } from "../models/companyfilter.model";

/**
 * 
 * 
 * @export
 * @class CompanyDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "companyPipe"
})
export class CompanyDataFilterPipe implements PipeTransform {

    /**
     * 
     * 
     * @param {any[]} data
     * @param {Form7501filter} filter
     * @returns {*}
     * 
     * @memberOf Form7501DataFilterPipe
     */
    transform(data: any[], filter: CompanyFilterData): any {
        let results = data;

        if (data) {
            if (filter) {
                // filter by businessName #
                if (typeof filter.businessNameFilter !== 'undefined' && filter.businessNameFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1 = String(item.companyName).toLowerCase();
                        let val2 = String(filter.businessNameFilter).toLowerCase();
                        return String(val1).indexOf(val2) != -1;
                    });
                }
                // filter by ein Number #
                if (typeof filter.einNumFilter !== 'undefined' && filter.einNumFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1 = String(item.einNum).replace(/[^\w\s]/gi, '');
                        let val2 = String(filter.einNumFilter).replace(/[^\w\s]/gi, '');
                        return String(val1).indexOf(val2) != -1;
                    });
                }
                // filter by company status
                if (typeof filter.companyStatusFilter !== 'undefined' && filter.companyStatusFilter.length > 0) {
                    results = _.filter(results, function (item) {

                        let val1;

                        if (item) {
                            if (item.companyStatus == 'CLSD' || item.companyStatus == 'CLS' || item.companyStatus == 'CLOSED')
                                val1 = 'closed';
                            else if (item.companyStatus == 'ACTV' || item.companyStatus == 'ACT' || item.companyStatus == 'ACTIVE')
                                val1 = 'active';
                            else val1 = 'n/a';
                        }

                        //let val1 = String(item.companyStatus).toLowerCase();
                        let val2 = String(filter.companyStatusFilter).toLowerCase();

                        return String(val1).indexOf(val2) != -1;
                    });
                }
                // filter by company action
                if (typeof filter.companyActionFilter !== 'undefined' && filter.companyActionFilter.length > 0) {
                    results = _.filter(results, function (item) {              
                       let val1 = String(item.companyAction).toUpperCase();
                          if (item) {
                            if(item.companyAction == 'CONFLICT - NAME MISMATCH')
                                    val1 = 'CONFLICT';               
                            }
                            if(item.companyAction.includes('INVALID') )
                            {
                                val1 = 'INVALID'; 
                            }
                           let val2 = filter.companyActionFilter.toString().toUpperCase();
                           return val2.includes(val1);
                    });
                }
                if(typeof filter.companyActionFilter !== 'undefined' && filter.companyActionFilter.length == 0)
                {
                      return null;
                }
            }
        }
        return results;
    }
    // tslint:disable-next-line:eofline
}
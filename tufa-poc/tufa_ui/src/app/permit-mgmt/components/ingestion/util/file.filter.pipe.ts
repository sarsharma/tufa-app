import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { FileFilterData } from "../models/form7501filter.model";

/**
 * 
 * 
 * @export
 * @class PermitReportsDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "filepipe"
})
export class FileDataFilterPipe implements PipeTransform {

    /**
     * 
     * 
     * @param {any[]} data
     * @param {Form7501filter} filter
     * @returns {*}
     * 
     * @memberOf Form7501DataFilterPipe
     */
    transform(data: any[], filter: FileFilterData): any {
        let results = data;

        if (data) {
            if (filter) {
                // filter by businessName #
                if (typeof filter.businessNameFilter !== 'undefined' && filter.businessNameFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1 = String(item.businessName).toLowerCase();
                        let val2 = String(filter.businessNameFilter).toLowerCase();
                        return String(val1).indexOf(val2) != -1;
                    });
                } 
                // filter by ein Number #
                if (typeof filter.einNumFilter !== 'undefined' && filter.einNumFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1 = String(item.einNum).replace(/[^\w\s]/gi, '');
                        let val2 = String(filter.einNumFilter).replace(/[^\w\s]/gi, '');
                        return String(val1).indexOf(val2) != -1;
                    });
                } 
                // filter by permitNumber
                if (typeof filter.permitNumFilter !== 'undefined' && filter.permitNumFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1 = String(item.permitNum).toLowerCase();
                        let val2 = String(filter.permitNumFilter).toLowerCase().replace(/[^\w\s]/gi, '');
                        return String(val1).indexOf(val2) != -1;
                    });
                }   
                // filter by tax diff #
                if (typeof filter.resultsStatusFilter !== 'undefined' && filter.resultsStatusFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1 = String(filter.fileTypeFilter === "address" ? item.addressStatus : item.contactStatus).toLowerCase();
                        if( val1 === "repld") 
                            val1 = "replaced";
                        else if( val1 === "ignr") 
                            val1 = "ignored";
                        let val2 = String(filter.resultsStatusFilter).toLowerCase();
                        return String(val1).indexOf(val2) != -1;
                    });
                }                                                                        
            }
        }
        return results;
    }
// tslint:disable-next-line:eofline
}
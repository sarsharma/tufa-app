import data from './permit-report.json';
import { PermitReportsDataFilterPipe } from '../../../../company/util/permit-report-filter.pipe';
import { PermitReportFilter } from '../../../../company/models/permitreport-filter.model';

xdescribe('Pipe: PermitReportsDataFilterPipe', () => {
  let pipe = new PermitReportsDataFilterPipe();
  let filter = new PermitReportFilter();

  //specs
  xit('test permit report filter', () => {
    //must use arrow function for expect to capture exception
    // expect(()=>pipe.transform(data, filter)).toThrow();
    // expect(()=>pipe.transform(data, filter)).toThrowError('Requires a String as input');
    // expect(pipe.transform(data, filter)).toEqual('');
    // expect(pipe.transform(data, filter)).toEqual('WOW');  
  });
}) 
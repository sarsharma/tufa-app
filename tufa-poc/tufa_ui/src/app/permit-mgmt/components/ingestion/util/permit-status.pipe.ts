
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "permitStatusPipe"
})
export class UpdatePermitStatusPipe implements PipeTransform {

    transform(value): any {
         if (value === "ADDED") {
            return("ADDED");
        } else if( value === "REPLD") {
            return ("REPLACED");
        } else if( value === "IGNR") {
            return ("IGNORED");
        }
    }
}
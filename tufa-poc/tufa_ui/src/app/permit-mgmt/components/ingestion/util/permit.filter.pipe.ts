import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { PermitFilterData } from "../models/permitfilter.model";

/**
 * 
 * 
 * @export
 * @class CompanyDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "permitPipe"
})
export class PermitDataFilterPipe implements PipeTransform {

    /**
     * 
     * 
     * @param {any[]} data
     * @param {Form7501filter} filter
     * @returns {*}
     * 
     * @memberOf Form7501DataFilterPipe
     */
    transform(data: any[], filter: PermitFilterData): any {
        let results = data;

        if (data) {
            if (filter) {
                // filter by businessName #
                if (typeof filter.companyNameFilter !== 'undefined' && filter.companyNameFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1 = String(item.companyName).toLowerCase();
                        let val2 = String(filter.companyNameFilter).toLowerCase();
                        return String(val1).indexOf(val2) != -1;
                    });
                }
                // filter by ein Number #
                if (typeof filter.einNumFilter !== 'undefined' && filter.einNumFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1 = String(item.einNum).replace(/[^\w\s]/gi, '');
                        let val2 = String(filter.einNumFilter).replace(/[^\w\s]/gi, '');
                        return String(val1).indexOf(val2) != -1;
                    });
                }
                // filter by permitNumber
                if (typeof filter.permitNumFilter !== 'undefined' && filter.permitNumFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1 = String(item.permitNum).toLowerCase();
                        let val2 = String(filter.permitNumFilter).toLowerCase().replace(/[^\w\s]/gi, '');
                        return String(val1).indexOf(val2) != -1;
                    });
                }
                // filter by permitStatus
                if (typeof filter.permitStatusFilter !== 'undefined' && filter.permitStatusFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1;
                        if (item) {
                            if (item.permitStatus == 'CLSD' || item.permitStatus == 'CLS' || item.permitStatus == 'CLOSED')
                                val1 = 'closed';
                            else if (item.permitStatus == 'ACTV' || item.permitStatus == 'ACT' || item.permitStatus == 'ACTIVE')
                                val1 = 'active';
                            else val1 = 'n/a';
                        }
                        let val2 = String(filter.permitStatusFilter).toLowerCase();
                        return String(val1).indexOf(val2) != -1;
                    });
                }
                // filter by permitAction
                if (typeof filter.permitActionFilter !== 'undefined' && filter.permitActionFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1 = String(item.permitAction).toUpperCase();

                        if (item) {
                            if (item.permitAction.includes('CONFLICT'))
                                val1 = 'CONFLICT';
                        }
                        if (item.permitAction.includes('INVALID')) {
                            val1 = 'INVALID';
                        }
                        if (item.permitAction.includes('ADDED') || item.permitAction.includes('CLOSED')) {
                            val1 = 'ADDED'
                        }
                        if (item.permitAction.includes('DATE CHANGE')) {
                            val1 = 'DATE CHANGE'
                        }

                        let val2 = filter.permitActionFilter.toString().toUpperCase();
                        return val2.includes(val1);
                    });
                }
                if (typeof filter.permitActionFilter !== 'undefined' && filter.permitActionFilter.length == 0) {
                    return null;
                }
            }
        }
        return results;
    }

    // tslint:disable-next-line:eofline
}
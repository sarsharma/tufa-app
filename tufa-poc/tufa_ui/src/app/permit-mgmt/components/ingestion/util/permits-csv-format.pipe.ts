import { Pipe, PipeTransform } from "@angular/core";
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Pipe({ name: "permitsCSVPipe2" })
export class PermitsCSVPipe2 implements PipeTransform {
       
  constructor(private _sanitizer: DomSanitizer){}
  
  /**
   * 
   * 
   * @param {string} value
   * @returns {SafeHtml}
   * 
   * @memberOf PermitsCSVPipe
   */
  transform(value: string): SafeHtml {
    return value?(value.substring(0,2) +"-"+ value.substring(2,4) +"-"+ value.substring(4)).toUpperCase().trim():"";
  }   
}
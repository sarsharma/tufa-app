import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'SearchCompanyPipeTst'
})

export class SearchCompanyPipe2 implements PipeTransform {

  transform(value, args?): Array<any> {
    let searchText = new RegExp(args, 'ig');
    if (value) {
      return value.filter(history => {
        if (history.companyName) {
          return history.companyName.search(searchText) !== -1;
        }
      });
    }
  }
}

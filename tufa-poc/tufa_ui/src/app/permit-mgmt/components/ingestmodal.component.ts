import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { IngestFileDocModel } from '../models/ingestfiledocumentmodel';
import { PermitMgmtService } from '../services/permit-mgmt.service';
import { ProgressBarDynamicComponent } from './progressbar.component';
import { NgForm } from '../../../../node_modules/@angular/forms';

declare var jQuery: any;

@Component({
    selector: '[ingest-file-modal]',
    templateUrl: './ingestmodal.template.html',
    styleUrls: ['./ingestmodal.style.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: []
})

export class IngestModalComponent implements OnInit, OnDestroy {

    @Input() documentModel: IngestFileDocModel;
    doc = new IngestFileDocModel();
    @Output() onDocumentEdit = new EventEmitter<boolean>();

    filesToUpload: Array<File> = [];
    docFileName: string;
    docId: number;

    timer: Observable<number>;
    entries = [];
    selectedEntry: any = {
        value: null,
        description: null
    };


    showErrorFlag: boolean;
    alerts: any[] = [];


    @Input() refreshToken;
    @Input() multiple: boolean = true;

    uploadInProgress: boolean = false;
    progressbarMaxOut: boolean = false;
    showIngestFileErrorPopup: boolean = false;

    trueupDocUpdateSubscription: Subscription;
    private _elementRef: ElementRef;

    @ViewChild('permitFileIngest') permitFileIngest: NgForm;

    constructor(private PermitMgmtService: PermitMgmtService,
        // private PermitMgmtEventService: PermitMgmtEventService
        private progressBarDynamicComponent: ProgressBarDynamicComponent,
        elementRef: ElementRef
    ) {
        this._elementRef = elementRef;


        // This is the bootstrap on shown event where i call the initialize method
        jQuery(this._elementRef.nativeElement).on("shown.bs.modal", () => {
            this.initializeModal();
        });

        jQuery(this._elementRef.nativeElement).on("hidden.bs.modal", () => {
            this.cleanupModal();
        });
    }




    /*
    ngOnChanges() used to capture changes on the input properties.
  */
    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "documentModel") {
                let chg = changes[propName];
                if (chg.currentValue) {
                    this.doc = chg.currentValue;
                     if(chg.currentValue.fileNm !== undefined)
                    {this.docFileName = chg.currentValue.fileNm.endsWith(".xlsx") ? chg.currentValue.fileNm : (chg.currentValue.fileNm + ".xlsx")};
                    this.docId = chg.currentValue.docId;

                }
            }
        }
    }


    ngOnInit() {
        this.resetAlerts();
        this.showErrorFlag = false;

    }

    initializeModal() {
    }

    // Free up memory and stop any event listeners/hubs
    cleanupModal() {

    }


    ngOnDestroy() {

    }

    reset() {
        if(this.permitFileIngest) {
            this.permitFileIngest.reset();
        }
        jQuery('#ingest-modal-file').modal('hide');
    }

    cancel() {
        if (this.doc.docFileNm) {
            // jQuery('#ingestfilemodalform').parsley().reset();
            jQuery('#ingest-modal-file').modal('hide');
        } else {
            this.reset();
        }
        this.onDocumentEdit.emit(false);
    }

    getFilePathExtension(path) {
        let filename = path.split('\\').pop().split('/').pop();
        return filename.substr((Math.max(0, filename.lastIndexOf(".")) || Infinity) + 1);
    }


    fileChangeEvent(event) {
        this.filesToUpload = <Array<File>>event.target.files;
        
        if(this.filesToUpload[0] && this.filesToUpload[0].name !== undefined) {
            this.docFileName = this.filesToUpload[0].name.endsWith(".xlsx") ? this.filesToUpload[0].name: (this.filesToUpload[0].name + ".xlsx");
        } else {
            this.docFileName = "";
        }

        

    }


    alertAdded(name: string): boolean {
        let alertPresent = false;
        this.alerts.forEach(function (item, index, array) {
            if (item.name === name) {
                alertPresent = true;
            }
        });
        return alertPresent;
    }

    resetAlerts() {
        this.alerts = [];
    }

    uploadImpl(formData: FormData) {
        return this.PermitMgmtService.ingestFile(formData).then((data) => {
            if (data) {
                this.onDocumentEdit.emit(true);
                this.reset();
                this.progressbarMaxOut = true;

            }
        }, err => {
            this.uploadInProgress = false;
            this.progressbarMaxOut = false;
            this.showErrorFlag = true;
            this.resetAlerts();
            jQuery('#ingest-modal-file').find('input').val('');
            this.alerts.push({ type: 'warning', msg: 'Unable to Ingest file. Please check file contents' });
            //console.error(error);
        });
    }



    ingestFile() {
        // console.log(this.fileType);
        jQuery('#ingestfilemodalform').parsley().validate();

        if (!jQuery('#ingestfilemodalform').parsley().isValid()) return;

        let file: File = this.filesToUpload[0];
        let formData: FormData = new FormData();
        this.uploadInProgress = true;
        let fileReader = new FileReader();
        if (this.filesToUpload.length > 0) {

            formData.append('fileType', "PERMIT Update");

            if (this.docFileName)
                formData.append('permitListDocument', file, this.docFileName);
            else
                formData.append('permitListDocument', file, file.name);

            formData.append('aliasFileNm', this.docFileName);
            this.uploadfile(formData);
            this.filesToUpload = [];
           
        }
        else {

            let updatedoc = new IngestFileDocModel();
            updatedoc.docId = this.docId;
            updatedoc.docFileNm = this.docFileName;
            this.saveFile(updatedoc);
        }


    }

    onSelectionChange(entry) {
        // clone the object for immutability
        this.selectedEntry = Object.assign({}, this.selectedEntry, entry);
    }

    public saveFile(doc: IngestFileDocModel) {

        this.showErrorFlag = false;
        this.resetAlerts();

        this.PermitMgmtService.updateDocument(doc, doc.docId, doc.docFileNm).subscribe(data => {
            if (data) {
                this.onDocumentEdit.emit(true);
                this.progressbarMaxOut = true;
                this.reset();
            }
        }, (error) => {
            this.uploadInProgress = false;
            this.progressbarMaxOut = false;
            this.showErrorFlag = true;
            this.resetAlerts();
            this.alerts.push({ type: 'warning', msg: 'Unable to Ingest file. Please check file contents' });
            console.error(error);
        });

    }


    public uploadfile(formData: FormData) {

        this.showErrorFlag = false;
        this.resetAlerts();

        this.PermitMgmtService.ingestFile(formData).then((result) => {
            if (result) {
                this.onDocumentEdit.emit(true);
                this.reset();
                this.progressbarMaxOut = true;
            }
        }, (error) => {
            this.uploadInProgress = false;
            this.progressbarMaxOut = false;
            this.showErrorFlag = true;
            this.resetAlerts();
            this.alerts.push({ type: 'warning', msg: 'Unable to Ingest file. Please check file contents' });
            console.error(error);
        });
    }

    exportedIngestErrorFile(flag) {
        this.showIngestFileErrorPopup = false;
        jQuery('#ingest-error-dialog').modal('hide');
        jQuery('#ingest-modal-file').modal('toggle');
    }
}
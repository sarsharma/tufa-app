import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, Output, SimpleChange, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Company } from '../../company/models/company.model';
import { ShareDataService } from '../../core/sharedata.service';
import { Permit } from '../../permit/models/permit.model';
import { PermitMgmtService } from '../services/permit-mgmt.service';
import { PermitUpdate } from './ingestion/models/permit-update.model';
import { CompanyService } from './ingestion/services/company.service';




declare var jQuery: any;

/**
 *
 *
 * @export
 * @class ConfirmPopup
 */
@Component({
    selector: '[permit-updates-add-permit]',
    templateUrl: './permit-updates.add.permit.template.html',
    encapsulation: ViewEncapsulation.None
})

export class PermitUpdatesAddPermitComponent {

    @Input() addPermitUpdate: PermitUpdate;
    @Output() public mvNextRecord = new EventEmitter<PermitUpdate>();
    @Output() public navigateToPg = new EventEmitter<any[]>();

    shareData: ShareDataService;

    selectedTufaPermitCloseDate: Date;

    permitClose: Permit;

    ttbDate: Date;
    selectedTTBDateType: string;
    disableButton: boolean = false;

    invalidClsDt: boolean = false;
    rptsOutsideClsDt: boolean = false;

    displayCaculatedClsDt: string;

    busy: Subscription;

    alerts = [
        {
            type: 'success',
            msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Successfully Added the Permit.</span>'
        }
    ];


    datepickerttbOpts = {
        autoclose: true,
        todayBtn: 'linked',
        todayHighlight: true,
        assumeNearbyYear: true,
        // requiredErrorMsg: "Error: TTB Close Date is required",
        title: 'Enter TTB  Date in MM/DD/YYYY format'
    };

    ttbIngestDates = [{ name: 'Select Date Type', value: '' }, { name: 'TTB ISSUE DATE', value: 'TTB_START_DT' }, { name: 'TTB CLOSE DATE', value: 'TTB_END_DT' }];
    errorMsg: string;


    constructor(private companyService: CompanyService,
        private permitMgmtService: PermitMgmtService,
        private datePipe: DatePipe,
        private router: Router,
        private shareDataService: ShareDataService) {
    }

    public ngOnChanges(changes: { [key: string]: SimpleChange }): any {

        if (changes["addPermitUpdate"]) {
            let addPermitUpdate = changes["addPermitUpdate"].currentValue
            if (addPermitUpdate && addPermitUpdate.permitStatusChangeAction && !addPermitUpdate.permitStatusChangeAction.includes('Conflict Ignored')) {
                this.disableButton = true;
            } else
                this.disableButton = false;
        }

    }

    hidePanel() {
        this.mvNextRecord.emit(null);
    }


    public addAndClosePermit() {

        /** Create new Permit */
        let newPermit = new Permit();
        newPermit.permitNum = this.addPermitUpdate.permitNum;
        newPermit.ttbstartDt = this.addPermitUpdate.issueDt;
        newPermit.ttbendDt = this.addPermitUpdate.closeDt;
        newPermit.permitTypeCd = this.addPermitUpdate.permitTypeCd;
        newPermit.permitStatusTypeCd = "ACTV";
        newPermit.companyId = this.addPermitUpdate.companyId;
        newPermit.duplicateFlag = true;
        newPermit.permitAction = 'Add';

        /** Close the permit in Tufa */
        let closePermit: Permit = new Permit();
        Object.assign(closePermit, this.addPermitUpdate.permitTufa);
        closePermit.companyId = Number(this.addPermitUpdate.companyId);
        closePermit.permitAudit = new PermitUpdate();
        Object.assign(closePermit.permitAudit, this.addPermitUpdate);
        closePermit.permitAudit.permitTufa = null;
        closePermit.permitNum = closePermit.permitNum.replace(/-/g, "");
        closePermit.permitAction = 'Update';
        closePermit.permitStatusTypeCd = 'CLSD';

        let permits: Permit[] = [];
        //The order of inserting permits in array is crucial 
        permits.push(newPermit);
        permits.push(closePermit);

        this.busy = this.permitMgmtService.addAndClosePermits(permits, newPermit.companyId).subscribe(
            data => {
                if (data) {
                    let company: Company = <Company>data;
                    this.addPermitUpdate = company.permitAudit;
                    this.mvNextRecord.emit(this.addPermitUpdate);
                }
            }
            , (error) => {
                if (error) {
                    this.errorMsg = error;
                } else {
                    this.errorMsg = 'An error occured when attempting to perform the Add/Close action.'
                }
                this.showModal();
            }
        );

    }

    public addPermit() {

        let newPermit = new Permit();

        newPermit.permitNum = this.addPermitUpdate.permitNum;
        newPermit.issueDt = this.addPermitUpdate.issueDt;
        newPermit.closeDt = this.addPermitUpdate.closeDt;
        newPermit.permitTypeCd = this.addPermitUpdate.permitTypeCd;
        newPermit.permitStatusTypeCd = "ACTV";
        newPermit.companyId = this.addPermitUpdate.companyId;
        newPermit.permitAudit = this.addPermitUpdate;
        newPermit.duplicateFlag = true;
        newPermit.permitAudit.permitStatusChangeAction = 'TTB Permit ' + this.formatPermit(newPermit.permitNum) + ' Added';

        this.busy = this.companyService.createPermit(newPermit).subscribe(
            data => {

                if (data) {
                    let company: Company = <Company>data;
                    this.addPermitUpdate = company.permitAudit;
                    this.mvNextRecord.emit(this.addPermitUpdate)

                }
            }
            ,(error) => {
                if (error) {
                    this.errorMsg = error;
                } else {
                    this.errorMsg = 'An error occured when attempting to perform the Add action.'
                }
                this.showModal();
              }
        );

    }

    // navigateToPermitDetails() {

    //     this.hideModal();
    //     this.shareDataService.breadcrumb = "permit-mgmt";
    //     this.shareDataService.filemgmttab = 'permit';

    //     this.navigateToPg.emit(['/app/permit/history', this.permitClose.permitId]);
    // }

    showModal() {
        let modal = jQuery("#permit-update-warn-modal");
        modal.appendTo("body");
        if (!modal.is(':visible')) {
            modal.modal({
                toggle: 'show',
                backdrop: 'static',
                keyboard: false
            })
        }
    }

    hideModal() {
        this.rptsOutsideClsDt = false;
        this.invalidClsDt = false;

        // jQuery('body>#permit-update-warn-modal').modal('hide');
        jQuery('body>#permit-update-warn-modal').hide();
        jQuery('.modal-backdrop').hide();
        jQuery('.modal-backdrop').remove();
        jQuery("body").removeClass(".modal-open");
    }


    public ignorePermitStatus() {

        this.addPermitUpdate.permitStatusChangeAction = "Conflict Ignored";
        this.busy = this.permitMgmtService.insertPermitUpdate(this.addPermitUpdate).subscribe(data => {
            if (data) {
                let pUpdate = data;
                this.mvNextRecord.emit(pUpdate);
            }
        })

    }

    skipPermitStatus() {
        this.hideModal();
        this.addPermitUpdate.uiAction = "SKIP";
        this.mvNextRecord.emit(this.addPermitUpdate);
    }

    getFullText(abbrv) {
        if (abbrv == 'ACT' || abbrv == 'ACTV') return 'ACTIVE';
        else if (abbrv == 'CLS' || abbrv == 'CLSD') return 'CLOSED';
        else return 'N/A';
    }

    formatPermit(value: string) {
        if (!value) return;
        let val = value.replace(/-/g, '');
        let prt1 = val.substring(0, 2);
        let prt2 = val.substring(2, 4);
        let prt3 = val.substring(4);
        return prt1 + "-" + prt2 + "-" + prt3;
    }


    ngDestroy() {
        this.hideModal();
        if (this.busy)
            this.busy.unsubscribe();
    }
}
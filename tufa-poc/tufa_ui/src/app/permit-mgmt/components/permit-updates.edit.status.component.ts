import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, Output, SimpleChange, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { Company } from '../../company/models/company.model';
import { Permit } from '../../permit/models/permit.model';
import { PermitMgmtService } from '../services/permit-mgmt.service';
import { PermitUpdate } from './ingestion/models/permit-update.model';
import { CompanyService } from './ingestion/services/company.service';

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class ConfirmPopup
 */
@Component({
    selector: '[permit-updates-edit-status]',
    templateUrl: './permit-updates.edit.status.template.html',
    encapsulation: ViewEncapsulation.None
})

export class PermitUpdatesEditStatusComponent {

    @Input() editPermitUpdate: PermitUpdate;
    @Output() public mvNextRecord = new EventEmitter<PermitUpdate>();

    disableButton: boolean = false;

    selectedTTBDate: Date;
    selectedTTBDateType: string;
    selectedPermitStatusType: string;

    ttbdateid: string = 'ttb-date';

    busy: Subscription;

    //@Output() isDisplay =  new EventEmitter<boolean>();

    alerts = [
        {
            type: 'success',
            msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Successfully Updated Permit TTB Date.</span>'
        }
    ];

    datepickerttbOpts = {
        autoclose: true,
        todayBtn: 'linked',
        todayHighlight: true,
        assumeNearbyYear: true,
        // requiredErrorMsg: "Error: TTB Close Date is required",
        title: 'Enter TTB  Date in MM/DD/YYYY format'
    };


    pStatusColumns = { options: [{ name: 'ACTIVE', value: 'TTB_START_DT' }, { name: 'CLOSED', value: 'TTB_END_DT' }] };

    pTTBDatesColumns = { options: [{ name: 'TTB ISSUE DATE', value: 'TTB_START_DT' }, { name: 'TTB CLOSE DATE', value: 'TTB_END_DT' }] };

    constructor(private companyService: CompanyService,
        private permitMgmtService: PermitMgmtService,
        private datePipe: DatePipe) {

    }

    hidePanel() {
        this.mvNextRecord.emit(null);
    }


    public ngOnChanges(changes: { [key: string]: SimpleChange }): any {

        if (changes["editPermitUpdate"]) {
            let currentVal = changes["editPermitUpdate"].currentValue;
            if (currentVal) {
                this.selectedPermitStatusType = 'TTB_START_DT';
                this.setTTBDateType('TTB_START_DT');

                let editPermitUpdate = currentVal;
                if (editPermitUpdate && editPermitUpdate.permitStatusChangeAction && !editPermitUpdate.permitStatusChangeAction.includes('Conflict Ignored')) {
                    this.disableButton = true;
                } else
                    this.disableButton = false;
            }
        }

    }


    public replaceActiveDate() {

        jQuery('#ttb-date-form').parsley().validate();

        if (jQuery('#ttb-date-form').parsley().isValid()) {

            let permit: Permit = this.editPermitUpdate.permitTufa;
            permit.companyId = Number(this.editPermitUpdate.companyId);
            this.editPermitUpdate.permitTufa = null;
            permit.permitAudit = this.editPermitUpdate;

            let dateEntered = this.datePipe.transform(this.selectedTTBDate, 'MM/dd/yyyy');
            if (this.selectedTTBDateType === 'TTB_START_DT') {
                permit.ttbstartDt = dateEntered;
                permit.permitStatusTypeCd = 'ACTV';
                permit.permitAudit.permitStatusChangeAction = 'Permit ' + this.formatPermit(permit.permitNum) + ' has been activated with TTB Issue Date of ' + dateEntered;
            }
            else if (this.selectedTTBDateType === 'TTB_END_DT') {
                permit.ttbendDt = dateEntered;
                permit.permitAudit.permitStatusChangeAction = 'TTB End Date Changed to ' + dateEntered;
            }

            this.busy = this.companyService.updateTufaPermit(permit).subscribe(
                data => {
                    if (data) {
                        let company: Company = data;
                        this.editPermitUpdate = company.permitAudit;
                        this.mvNextRecord.emit(this.editPermitUpdate)
                    }
                }
            )

        }
    }


    public ignorePermitStatus() {
        this.editPermitUpdate.permitStatusChangeAction = "Conflict Ignored";
        this.busy = this.permitMgmtService.insertPermitUpdate(this.editPermitUpdate).subscribe(data => {
            if (data) {
                let pUpdate = data;
                this.mvNextRecord.emit(pUpdate);
            }
        })

    }

    skipPermitStatus() {
        this.editPermitUpdate.uiAction = 'SKIP';
        this.mvNextRecord.emit(this.editPermitUpdate);
    }

    setTTBDateType(selectpstatus) {

        this.selectedTTBDateType = selectpstatus;
        if (selectpstatus == 'TTB_START_DT' && this.editPermitUpdate)
            this.selectedTTBDate = new Date(this.editPermitUpdate.issueDt);
        else if (selectpstatus == 'TTB_END_DT')
            this.selectedTTBDate = null;

    }


    getFullText(abbrv) {
        if (abbrv == 'ACT' || abbrv == 'ACTV') return 'ACTIVE';
        else if (abbrv == 'CLS' || abbrv == 'CLSD') return 'CLOSED';
        else return 'N/A';
    }

    formatPermit(value: string) {

        let prt1 = value.substring(0, 2);
        let prt2 = value.substring(2, 4);
        let prt3 = value.substring(4);
        return prt1 + "-" + prt2 + "-" + prt3;
    }

    ngDestroy() {
        if (this.busy)
            this.busy.unsubscribe();
    }
}
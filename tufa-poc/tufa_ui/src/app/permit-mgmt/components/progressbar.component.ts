import { Component, ViewEncapsulation, OnInit, Input, SimpleChange, OnDestroy } from '@angular/core';
import { Observable, Subscription, timer } from 'rxjs';

@Component({
  selector: '[progressbar]',
  templateUrl: './progressbar.template.html',
  encapsulation: ViewEncapsulation.None
})

export class ProgressBarDynamicComponent implements OnInit, OnDestroy {
  public max: number = 200;
  public showWarning: boolean;
  public dynamic: number = 0;
  public type: string;
  public constIncrement: number= 10;

  @Input() maxout: boolean;

  private dynamicSubscription: Subscription;

  public constructor() {
    this.max = 200;
    this.dynamic = 0;
    this.constIncrement = 10;
  }

  ngOnInit() {
    this.max = 200;
    this.dynamic = 0;
    this.constIncrement = 10;

    let pg_timer = timer(1000, 1000);
    this.dynamicSubscription = pg_timer.subscribe(t => this.value(t));
  }

  ngOnDestroy(){
    if(this.dynamicSubscription){
      this.dynamicSubscription.unsubscribe();
    }
  }

  public value(value: number) {

    if (this.maxout)
       this.dynamic = this.max;

    if (this.dynamic >= this.max)
      this.dynamicSubscription.unsubscribe();

    if (this.dynamic < this.max)
      this.dynamic = value + this.constIncrement;

  }

}

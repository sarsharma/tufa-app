export class IngestFileDocModel {
    docId:number;
    docDesc: string;
    createdDt: string;
    createdBy: string;
    modifiedDt: string;
    modifiedBy: string;
    errorCount: number;
    document: string;
    docFileNm: string;
    aliasFileNm:string;
}




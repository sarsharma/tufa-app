import { AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs';
import { ShareDataService } from '../core/sharedata.service';
import { IngestFileDocModel } from './models/ingestfiledocumentmodel';
import { HttpClient } from '@angular/common/http';
import { PermitMgmtService } from './services/permit-mgmt.service';
import { Contact } from '../model/contact.model';
import { AddressService } from './components/ingestion/services/address.service';
import { ContactService } from './components/ingestion/services/contact.service';
import { Address } from '../model/address.model';

declare var jQuery: any;

@Component({
    selector: '[permit-mgmt-landing]',
    templateUrl: './permit-mgmt-landing.template.html',
    styleUrls: ['./permit-mgmt-landing.style.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [AddressService, ContactService]
})

export class PermitMgmtLandingPage implements OnInit, AfterViewInit, OnDestroy {

    managepermittabfrom: string;
    shareData: ShareDataService;
    selectedDocument: IngestFileDocModel;
    newDoc: IngestFileDocModel = new IngestFileDocModel();

    showDocumentPopup: boolean = true;
    downloading: Subscription;
    busy: Subscription;
    data: any[] = [];
    documentdata: any[] =[];
    address: Address[];

    constructor(private router: Router,
        private httpClient: HttpClient,
        private storage: LocalStorageService,
        private pemitservice: PermitMgmtService,
        private _shareData: ShareDataService,
        private addressService: AddressService,
        private contactService: ContactService) {
        this.router = router;


        this.shareData = _shareData;
    }

    ngOnInit() {

        this.loadDocs();
        this.managepermittabfrom = this.shareData.qatabfrom;


    }
    ngAfterViewInit() {
        if (this.managepermittabfrom === 'ingestpermitfile') {
            jQuery('.nav-pills a[href="#tab1]').tab('show');
        }
        else {
            jQuery('.nav-pills a[href="#tab2]').tab('show');
        }
    }

    ngOnDestroy() {

        this.busy.unsubscribe();

    }

    updateIgnrStatusandDelete()
    {
            this.pemitservice.updateIgnrandDeleteContact().subscribe(
            data => {
                if (data) {}
            });

            this.pemitservice.updateIgnrandDeleteAddress().subscribe(
            data => {});

    }


    documentsGridRefresh(flag: boolean, mode: string) {
        if (flag) {
            this.loadDocs();
        }
        if (mode === 'ingest' || mode === 'edit') {
            this.showDocumentPopup = false;
        }

    }


    setSelectedDocument(doc: IngestFileDocModel, mode: string) {

        this.selectedDocument = Object.assign({}, doc);

        if (mode === 'ingest' || mode === 'edit') {
            this.showDocumentPopup = true;
        }
    }

    loadDocs() {

        this.updateIgnrStatusandDelete();
        this.busy = this.pemitservice.loadDocs().subscribe(
            data => {
                let nd = data;
                let newData = [];
                for (let i = 0; i < nd.length; i++) {
                    newData.push(nd[i]);
                }
                this.data = newData;
                this.documentdata = newData;
            });

            this.getIngestedAddress();
            this.getIngestedContacts();
    }


 //This method will add address to tufa from the ingested address data is no tufa_data found 
    getIngestedAddress()
    {
      
       this.pemitservice.getIngestedAddress().subscribe(
            data => {
                if (typeof data !== 'undefined' && data.length > 0) {               
                 this.addAddresstoTufa(data);          
                }
            });
           

    }

    getIngestedContacts()
    {
      
       this.pemitservice.getIngestedContacts().subscribe(
            data => {
                if (typeof data !== 'undefined' && data.length > 0) {               
                     this.addContactstoTufa(data);          
                }
            })         

    }

   addAddresstoTufa(addresses: Address[])
   {
        this.addressService.saveOrUpdateMultipleAddress(addresses).subscribe(data => {
        if (data) {
          
            }
      });
    }
   
    addContactstoTufa(contacts: Contact[])
   {
        this.contactService.createmultipleContactsinTufa(contacts).subscribe(data => {
        if (data) {
          
            }
      });
    }

    downloadSelectedDocument(doc: IngestFileDocModel) {

        this.downloadAttach(doc);
    }

    downloadAttach(doc: IngestFileDocModel) {
        this.downloading = this.pemitservice.downloadAttachment(doc.docId).subscribe(
            data => {
                if (data) {
                   let docName =  data.fileNm.endsWith(".xlsx")?data.fileNm:(data.fileNm+".xlsx");
                   this.download(atob(data.document),docName, "application/json");
                }
            });
    }

    exportPermitUpdates() {

        this.pemitservice.exportPermitUpdates().subscribe(data => {
            if (data) {
                this.download(data.csv, data.filename, 'csv/text');
            }
        });
    }

    download(data: any, strFileName: any, strMimeType: any) {

        let self: any = window, // this script is only for browsers anyway...
            defaultMime = "application/octet-stream", // this default mime also triggers iframe downloads
            mimeType = strMimeType || defaultMime,
            payload = data,
            url = !strFileName && !strMimeType && payload,
            anchor = document.createElement("a"),
            toString = function (a) { return String(a); },
            myBlob = (self.Blob || self.MozBlob || self.WebKitBlob || toString),
            fileName = strFileName || "download",
            blob,
            reader;
        myBlob = myBlob.call ? myBlob.bind(self) : Blob;

        if (String(this) === "true") { // reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
            payload = [payload, mimeType];
            mimeType = payload[0];
            payload = payload[1];
        }


        // go ahead and download dataURLs right away
        if (/^data\:[\w+\-]+\/[\w+\-\.]+[,;]/.test(payload)) {

            if (payload.length > (1024 * 1024 * 1.999) && myBlob !== toString) {
                payload = dataUrlToBlob(payload);
                mimeType = payload.type || defaultMime;
            } else {
                return navigator.msSaveBlob ?  // IE10 can't do a[download], only Blobs:
                    navigator.msSaveBlob(dataUrlToBlob(payload), fileName) :
                    saver(payload, false); // everyone else can save dataURLs un-processed
            }

        } else {// not data url, is it a string with special needs?
            if (/([\x80-\xff])/.test(payload)) {
                let i = 0, tempUiArr = new Uint8Array(payload.length), mx = tempUiArr.length;
                for (i; i < mx; ++i) tempUiArr[i] = payload.charCodeAt(i);
                payload = new myBlob([tempUiArr], { type: mimeType });
            }
        }
        blob = payload instanceof myBlob ?
            payload :
            new myBlob([payload], { type: mimeType });


        function dataUrlToBlob(strUrl) {
            let parts = strUrl.split(/[:;,]/),
                type = parts[1],
                decoder = parts[2] === "base64" ? atob : decodeURIComponent,
                binData = decoder(parts.pop()),
                mx = binData.length,
                i = 0,
                uiArr = new Uint8Array(mx);

            for (i; i < mx; ++i) uiArr[i] = binData.charCodeAt(i);

            return new myBlob([uiArr], { type: type });
        }

        function saver(url: any, winMode: any) {

            if ('download' in anchor) { // html5 A[download]
                anchor.href = url;
                anchor.setAttribute("download", fileName);
                anchor.className = "download-js-link";
                anchor.innerHTML = "downloading...";
                anchor.style.display = "none";
                document.body.appendChild(anchor);
                setTimeout(function () {
                    anchor.click();
                    document.body.removeChild(anchor);
                    if (winMode === true) { setTimeout(function () { self.URL.revokeObjectURL(anchor.href); }, 250); }
                }, 66);
                return true;
            }

            // handle non-a[download] safari as best we can:
            if (/(Version)\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//.test(navigator.userAgent)) {
                if (/^data:/.test(url)) url = "data:" + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
                if (!window.open(url)) { // popup blocked, offer direct download:
                    // tslint:disable-next-line:max-line-length
                    if (confirm("Displaying New Document\n\nUse Save As... to download, then click back to return to this page.")) { location.href = url; }
                }
                return true;
            }

            // do iframe dataURL download (old ch+FF):
            var f = document.createElement("iframe");
            document.body.appendChild(f);

            if (!winMode && /^data:/.test(url)) { // force a mime that will download:
                url = "data:" + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
            }
            f.src = url;
            setTimeout(function () { document.body.removeChild(f); }, 333);

        }// end saver




        if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or URL)
            return navigator.msSaveBlob(blob, fileName);
        }

        if (self.URL) { // simple fast and modern way using Blob and URL:
            saver(self.URL.createObjectURL(blob), true);
        } else {
            // handle non-Blob()+non-URL browsers:
            if (typeof blob === "string" || blob.constructor === toString) {
                try {
                    return saver("data:" + mimeType + ";base64," + self.btoa(blob), false);
                } catch (y) {
                    return saver("data:" + mimeType + "," + encodeURIComponent(blob), false);
                }
            }

            // Blob but not URL support:
            reader = new FileReader();
            reader.onload = function (e) {
                saver(this.result, false);
            };
            reader.readAsDataURL(blob);
        }
        return true;
    };

}

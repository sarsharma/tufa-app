import { CommonModule, DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
// import 'twitter-bootstrap-wizard/jquery.bootstrap.wizard.js';
import { AdminService } from '../admin/services/admin.service';
import { AddressService } from '../company/services/address.service';
import { CompanyService } from '../company/services/company.service';
import { ContactService } from '../company/services/contact.service';
import { AuthService } from '../login/services/auth.service';
import { SharedModule } from '../shared/shared.module';
import { CompanyMgmtEditComponent } from './components/company-mgmt.edit.component';
import { IngestError } from './components/ingesterror.component';
import { AddContactLeftComponent } from './components/ingestion/components/add-contact-left.component';
import { AddContactRightComponent } from './components/ingestion/components/add-contact-right.component';
import { AddressMgmtPage } from './components/ingestion/components/address-mgmt.component';
import { CompanyMgmtResultsComponent } from './components/ingestion/components/company-mgmt.results.component';
import { ContactMgmtPage } from './components/ingestion/components/contact-mgmt.component';
import { EditAddressMgmtLeft } from './components/ingestion/components/edit-address-left.component';
import { EditAddressMgmtRight } from './components/ingestion/components/edit-address-right.component';
import { PermitUpdatesMgmtComponent } from './components/ingestion/components/permit-mgmt.updates.component';
import { PermitSearchCompanyResultsComponent } from './components/ingestion/components/search-company-results.component';
import { SearchCriteriaPage } from './components/ingestion/components/search-criteria.component';
import { SelectAlternateAddressPopup } from './components/ingestion/components/select-alternate-address-popup';
import { CompanyEventService } from './components/ingestion/services/company.event.service';
import { CompanyDataFilterPipe } from './components/ingestion/util/company.filter.pipe';
import { FileDataFilterPipe } from './components/ingestion/util/file.filter.pipe';
import { UpdatePermitStatusPipe } from './components/ingestion/util/permit-status.pipe';
import { PermitDataFilterPipe } from './components/ingestion/util/permit.filter.pipe';
import { PermitsCSVPipe2 } from './components/ingestion/util/permits-csv-format.pipe';
import { IngestModalComponent } from './components/ingestmodal.component';
import { PermitUpdatesAddPermitComponent } from './components/permit-updates.add.permit.component';
import { PermitUpdatesEditStatusComponent } from './components/permit-updates.edit.status.component';
import { ProgressBarDynamicComponent } from './components/progressbar.component';
import { PermitMgmtLandingPage } from './permit-mgmt-landing.component';
import { PermitMgmtService } from './services/permit-mgmt.service';
import { IngestPermitFile } from './components/ingestpermitfile.component';
import { PermitUpdatesOutputComponent } from './components/permit-updates.output.component';
import { SearchCompanyPipe2 } from './components/ingestion/util/search-company-pipe';


// TTB Permit Exclude module's components and services 
import {TTBPermitExcludeComponent} from './components/ingestion/components/ttb-permit-exclude/ttb-permit-exclude.component';


export const routes = [
    { path: '', component: PermitMgmtLandingPage, pathMatch: 'full' },
    { path: 'address-mgmt', component: SearchCriteriaPage }
];

@NgModule({
    imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
    declarations: [PermitMgmtLandingPage, IngestModalComponent, ProgressBarDynamicComponent, IngestPermitFile, PermitUpdatesOutputComponent,
        IngestError, SearchCriteriaPage, PermitSearchCompanyResultsComponent, EditAddressMgmtLeft, EditAddressMgmtRight, PermitDataFilterPipe,
        AddressMgmtPage, AddContactLeftComponent, AddContactRightComponent, ContactMgmtPage, UpdatePermitStatusPipe, PermitsCSVPipe2, SearchCompanyPipe2,
        CompanyMgmtResultsComponent, PermitUpdatesMgmtComponent, FileDataFilterPipe, CompanyDataFilterPipe, PermitUpdatesEditStatusComponent,
        PermitUpdatesAddPermitComponent, CompanyMgmtEditComponent, SelectAlternateAddressPopup,TTBPermitExcludeComponent],
    providers: [HttpClient, AuthService, AdminService, PermitMgmtService, ProgressBarDynamicComponent, IngestError,
        AddressService, CompanyService, ContactService, CompanyEventService, FileDataFilterPipe,
        CompanyDataFilterPipe, PermitDataFilterPipe, DatePipe, PermitsCSVPipe2]

})

export class PermitMgmtModule {
    static routes = routes;
}
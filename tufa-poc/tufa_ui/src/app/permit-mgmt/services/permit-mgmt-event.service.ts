 import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
 //import { AnnualTrueUpDocModel } from "./../../../recon/annual/models/at-doc.model";
 //import {AnnualTrueUpSaveButtonStatus} from "./../../../recon/annual/models/at-save.button.status.model"
// /**
//  *
//  *
//  * @export
//  * @class PermitMgmtEventService
//  */
// @Injectable()
export class PermitMgmtEventService {

//   private fileIngestedSource = new Subject<AnnualTrueUpDocModel>();
//   private trueupDocsUpdateSource = new Subject<AnnualTrueUpDocModel[]>();

//   private savedPermitInclReset = new Subject<boolean>();
//   private updatePermitIncl = new Subject<boolean>();

//   fileIngestedSourceLoaded$ = this.fileIngestedSource.asObservable();
//   trueupDocsUpdateSource$ = this.trueupDocsUpdateSource.asObservable();

//   savedPermitInclReset$ = this.savedPermitInclReset.asObservable();
//   updatePermitIncl$ = this.updatePermitIncl.asObservable();

//   public triggerFileIngested(fileIngestedModelArry: AnnualTrueUpDocModel) {
//     this.fileIngestedSource.next(fileIngestedModelArry);
//   }

//   public triggerTrueupDocsUpdated(trueupDocs: AnnualTrueUpDocModel[]) {
//     this.trueupDocsUpdateSource.next(trueupDocs);
//   }

//   /** 
//    * Permit Inclusion related events  
//    * 
//   */
//   public resetSavedPermitInclStatus(reset: boolean) {
//     this.savedPermitInclReset.next(reset);
//   }

//   public updateSavedPermitInclStatus(update: boolean) {
//     this.updatePermitIncl.next(update);
//   }

//   /**
//    *  Company Association related events 
//    * 
//   */

//   /** Subjects */
//   private savedCompanyAssocReset = new Subject<boolean>();
//   private updateCompanyAssoc = new Subject<boolean>();
//   private updateSingleCmpyAssoc = new Subject<boolean>();

//   /** Observables */
//   savedCompanyAssocReset$ = this.savedCompanyAssocReset.asObservable();
//   updateCompanyAssoc$ = this.updateCompanyAssoc.asObservable();
//   updateSingleCmpyAssoc$ = this.updateSingleCmpyAssoc.asObservable();

//   public updateSingleCmpyAssocStatus(update: boolean) {
//     this.updateSingleCmpyAssoc.next(update);
//   }
  
//   public resetSavedCompanyAssocStatus(reset: boolean) {
//     this.savedCompanyAssocReset.next(reset);
//   }

//   public updateSavedCompanyAssocStatus(update: boolean) {
//     this.updateCompanyAssoc.next(update);
//   }

//   /**
//    *  HTS Missing Code association related events 
//    * 
//   */

//   /** Subjects */
//   private updateHTSMissAssoc = new Subject<boolean>();
//   private savedHTSMissAssocReset = new Subject<boolean>();
//   private updateSingleHTSMissAssoc = new Subject<boolean>();

//   /** Observables */
//   updateHTSMissAssoc$ = this.updateHTSMissAssoc.asObservable();
//   savedHTSMissAssocReset$ = this.savedHTSMissAssocReset.asObservable();
//   updateSingleHTSMissAssoc$ = this.updateSingleHTSMissAssoc.asObservable();

//   public updateSavedHTSMissAssocStatus(update: boolean) {
//     this.updateHTSMissAssoc.next(update);
//   }

//   public resetSavedHTSMissAssocStatus(reset: boolean) {
//     this.savedHTSMissAssocReset.next(reset);
//   }

//   public updateSingleHTSMissAssocStatus(update: boolean) {
//     this.updateSingleHTSMissAssoc.next(update);
//   }


//   /** 
//    * HTS Code association related events 
//    * 
//    * */

//   /** Subjects */
//   private updateHtSAssoc = new Subject<boolean>();
//   private savedHTSAssocReset = new Subject<boolean>();
//   private updateSingleHTSAssoc = new Subject<boolean>();

//   /** Observables */
//   updateHTSAssoc$ = this.updateHtSAssoc.asObservable();
//   savedHTSAssocReset$ = this.savedHTSAssocReset.asObservable();
//   updateSingleHTSAssoc$ = this.updateSingleHTSAssoc.asObservable();

//   public updateSavedHtsAssocStatus(update: boolean) {
//     this.updateHtSAssoc.next(update);
//   }

//   public resetSavedHtsAssocStatus(reset: boolean) {
//     this.savedHTSAssocReset.next(reset);
//   }

//   public updateSingleHTSAssocStatus(update: boolean) {
//     this.updateSingleHTSAssoc.next(update);
//   }


//   /** Include Company in Market Share related events */

//   /** Subjects */
//   private updateInclCmpyMktSh = new Subject<boolean>();
//   private savedInclCmpyMktShReset = new Subject<boolean>();
//   private updateSingleInclCmpyMktSh = new Subject<AnnualTrueUpSaveButtonStatus>();

//   /** Observables */
//   updateInclCmpyMktSh$ = this.updateInclCmpyMktSh.asObservable();
//   savedInclCmpyMktShReset$ = this.savedInclCmpyMktShReset.asObservable();
//   updateSingleInclCmpyMktSh$ = this.updateSingleInclCmpyMktSh.asObservable();

//   public updateSavedInclCmpyMktShStatus(update: boolean) {
//     this.updateInclCmpyMktSh.next(update);
//   }

//   public resetInclCmpyMktShStatus(reset: boolean) {
//     this.savedInclCmpyMktShReset.next(reset);
//   }

//   public updateSingleInclCmpyMktShStatus(status: AnnualTrueUpSaveButtonStatus) {
//     this.updateSingleInclCmpyMktSh.next(status);
//   }

//   //for create permit
// 	private atCreatePermitSource = new Subject<any>();
// 	atCreatePermitSource$=this.atCreatePermitSource.asObservable();

// 	public triggerAtCreatePermit(data: any) {
// 		this.atCreatePermitSource.next(data);
// 	}

//   //refresh permit bucket
//   private refreshPermitBucket = new Subject<boolean>();
//   refreshPermitBucket$ = this.atCreatePermitSource.asObservable();

//   public atRefreshPermitBucket(refresh: boolean) {
//     this.refreshPermitBucket.next(refresh);
//   }

//   //create Company
// 	private atCreateCompanySource = new Subject<any>();
// 	atCreateCompanySource$=this.atCreateCompanySource.asObservable();

//   private atCompanyCreatedSource = new Subject<any>();
// 	atCompanyCreatedSource$=this.atCompanyCreatedSource.asObservable();

// 	public triggerAtCreateCompany(data: any) {
// 		this.atCreateCompanySource.next(data);
// 	}

//   public triggerAtCompanyCreated(companyCreated: boolean) {
// 		this.atCompanyCreatedSource.next(companyCreated);
// 	}

 }



import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { AuthService } from "../../login/services/auth.service";
import { PermitAuditsExport } from "../models/permit-audit-export.model";
import { environment } from './../../../environments/environment';
import { PermitUpdate } from './../../permit-mgmt/components/ingestion/models/permit-update.model';
import { IngestFileDocModel } from "./../../permit-mgmt/models/ingestfiledocumentmodel";
import { Permit } from './../../permit/models/permit.model';

const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class PermitMgmtService {

    private baseurl = environment.apiurl + '/api/';
    private basereconurl = environment.apiurl + '/api/v1/permitmgmt';

    private ingestFileUrl = environment.apiurl + '/api/v1/permitmgmt/ingestFile';

    private getIngestErrorDocumentUrl = environment.apiurl + '/api/v1/permitmgmt';
    private getdocs = environment.apiurl + '/api/v1/permitmgmt/permits/documents';

    private removeIngestion = environment.apiurl + '/api/v1/permitmgmt/ingestfile';

    private downloadattachmenturl = environment.apiurl + '/api/v1/permitmgmt/document/'
    private exportPermitUpdatesUrl = environment.apiurl + '/api/v1/permitmgmt/permits/audit';

    private getPermitUpdatesUrl = environment.apiurl + '/api/v1/permitmgmt/permits/audit';

    private insertPermitUpdateUrl = environment.apiurl + '/api/v1/permitmgmt/permits/audit';

    private ingestedAdddress = environment.apiurl + '/api/v1/permitmgmt/permits/ingestedaddress';
    private ingestedContacts = environment.apiurl + '/api/v1/permitmgmt/permits/ingestedcontacts';

    private updateIngrcontactStatus = environment.apiurl + '/api/v1/permitmgmt/permits/contacts/updignrstatusanddelete';

    private updateIngraddressStatus = environment.apiurl + '/api/v1/permitmgmt/permits/address/updignrstatusanddelete';

    private addAndClosePermitsUrl = environment.apiurl + '/api/v1/permitmgmt/addandclose';


    private ttbPermitExclusionUpdatesUrl = environment.apiurl + '/api/v1/permitmgmt/permits/exclusion';
    private resolveTTBPermitExclusionUrl = environment.apiurl + '/api/v1/permitmgmt/permits/exclusion/resolve';


    constructor(private _httpClient: HttpClient, private authService: AuthService) { }

    makeFileRequest(httpMethod: string, url: string, formData: FormData) {
        return new Promise((resolve, reject) => {

            let xhr = new XMLHttpRequest();
            xhr.open("POST", url, true);

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.response);
                    } else {
                        reject(xhr.response);
                    }
                }
            }

            // Append JWT token


            let token = this.authService.getAccessToken();
            xhr.setRequestHeader('X-Authorization', 'Bearer ' + token);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send(formData);
        });
    }

    //ingest file
    ingestFile(formData: FormData): Promise<any> {
        return this.makeFileRequest("Post", this.ingestFileUrl, formData);
    }

    // get all the documents on page load
    loadDocs(): Observable<string> {
        return this._httpClient.get(this.getdocs, httpOptions).pipe(
            tap(_ => this.log("loadDocs")),
            catchError(this.handleError<any>("loadDocs"))
        );
    }

    // download each doc 
    downloadAttachment(docid: number): Observable<any> {
        return this._httpClient.get(this.downloadattachmenturl + docid, httpOptions);
    }


    //update Ignr Contact Status
    updateIgnrandDeleteContact(): Observable<any> {
        return this._httpClient.get(this.updateIngrcontactStatus, httpOptions);

    }

    //update Ignr Address Status
    updateIgnrandDeleteAddress(): Observable<any> {
        return this._httpClient.get(this.updateIngraddressStatus, httpOptions);

    }

    //update save
    updateDocument(document: IngestFileDocModel, docid: number, filename: string): Observable<any> {
        return this._httpClient.put(this.downloadattachmenturl + docid + "/" + filename, null, httpOptions);
    }

    removeIngestedFile(fiscalYr: any, doctype: any) {
        let url = this.removeIngestion + '/' + doctype + '/' + fiscalYr;
        return this._httpClient.delete(url, httpOptions);
    }

    getIngestedAddress(): Observable<any> {
        let url = this.ingestedAdddress;
        return this._httpClient.get(url, httpOptions);
    }

    getIngestedContacts(): Observable<any> {
        let url = this.ingestedContacts;
        return this._httpClient.get(url, httpOptions);
    }

    getIngestExportErrorDocument(fiscalYear: string, doctype: string): Observable<any> {
        let url = this.getIngestErrorDocumentUrl + "/" + fiscalYear + "/errors/" + doctype;
        return this._httpClient.get(url, httpOptions);
    }

    exportPermitUpdates(): Observable<PermitAuditsExport> {
        let url = this.exportPermitUpdatesUrl;
        return this._httpClient.get(url, httpOptions).pipe(
            tap(_ => this.log("exportPermitUpdates")),
            catchError(this.handleError<any>("exportPermitUpdates"))
        );
    }

    getPermitUpdatesByDocument(documentId) {
        let url = this.getPermitUpdatesUrl + "/" + documentId;
        return this._httpClient.get(url, httpOptions);
    }

    insertPermitUpdate(permitUpdate: PermitUpdate): Observable<PermitUpdate> {
        let url = this.insertPermitUpdateUrl;
        return this._httpClient.post(url, permitUpdate, httpOptions).pipe(
            tap(_ => this.log("insertPermitUpdate")),
            catchError(this.handleError<any>("insertPermitUpdate"))
        );
    }

    addAndClosePermits(permits: Permit[], companyId) {
        let url = this.addAndClosePermitsUrl + '/company/' + companyId;
        return this._httpClient.put(url, permits, httpOptions);
    }

    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log("${operation} failed: ${error.message}");

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Attribute Service message with the MessageService */
    private log(message: string) {
        //this.messageService.add("Profile Service: ${message}");
    }


    public getTTBPermitExclusionUpdates(docId): Observable<PermitUpdate> {
        let url = this.ttbPermitExclusionUpdatesUrl + "/" + docId;
        return this._httpClient.get(url, httpOptions).pipe(
            tap(_ => this.log("getTTBPermitExclusionUpdates")),
            catchError(this.handleError<any>("getTTBPermitExclusionUpdates"))
        );
    }

    public resolveTTBPermitExclusion(permitUpdate: PermitUpdate): Observable<PermitUpdate> {
        let url = this.resolveTTBPermitExclusionUrl;
        return this._httpClient.put(url, permitUpdate, httpOptions).pipe(
            tap(_ => this.log("resolveTTBPermitExclusion")),
            catchError(this.handleError<any>("resolveTTBPermitExclusion"))
        );
    }


}
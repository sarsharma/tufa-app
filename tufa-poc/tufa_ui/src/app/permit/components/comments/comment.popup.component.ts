/*
@author : Deloitte

this is Component for adding comment.

*/

import { Component, Input, Output, EventEmitter, OnInit, SimpleChange, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {MonthlyReportCommentService} from '../../services/monthly.report.comment.service';
import {MonthlyReportEventService} from '../../services/monthly.report.event.service'

import { TobaccoSubType } from '../../models/tobacco-subtype.model';
import { CommentModel } from '../../models/comment/comment.model';
import { PermitPeriod } from '../../models/permit-period.model';

import { Subscription } from 'rxjs';
import * as _ from 'lodash';
import { PermitHistoryCommentsService } from '../../services/permit.history.comments';

declare var jQuery: any;
let nextId = 0;

/**
 *
 *
 * @export
 * @class AddCommentPopup
 */

@Component({
  selector: '[add-comment]',
  templateUrl: './comment.popup.template.html',
  encapsulation: ViewEncapsulation.None,
  providers: [PermitHistoryCommentsService]
  // styleUrls: ['./busy.css']
})
export class MonthlyReportCommentPopup implements OnInit, OnDestroy{

  showErrorFlag: boolean;
  alerts: Array<Object>;

  @Input() title = `monthrpt-popup-title-${nextId++}`;
  @Input() commentdesc = `monthrpt-popup-cmmdesc-${nextId++}`;
  @Input() newComment: CommentModel;
  @Input() commentAddOrEdit: CommentModel;
  @Input() commentAction: string;
  @Input() callingpage: string;
  @Input() addCommnetPermit:number;
  @Output() hideModal = new EventEmitter<boolean>();
  @Output() commentAdded = new EventEmitter<CommentModel>();
  comment: CommentModel = new CommentModel();

  permitperiod: PermitPeriod;

  busy: Subscription;
  length:number;
  
  constructor(
    private _router: Router,
    private commentService: MonthlyReportCommentService,
    private monthlyReportService:MonthlyReportEventService,
    private permitHistoryCommentService :PermitHistoryCommentsService) {
    this.showErrorFlag = false;

    this.monthlyReportService.permitperiodSourceLoaded$.subscribe(
      permitperiod => {
              this.permitperiod = permitperiod;
      });

  }

  isEmpty(str) {
    return (!str || 0 === str.length);
  }

  ngOnInit(): void {
    this.monthlyReportService.triggerChildViewLoaded();
    if(null!=this.comment.userComment) {
      this.length = this.comment.userComment.length;
    }
  }

  ngOnDestroy() {
      if (this.busy) {
          this.busy.unsubscribe();
      }
  }

  /**
   * ngOnChanges() used to capture changes on the input properties.
   *
   * @param {{ [propertyName: string]: SimpleChange }} changes
   *
   * @memberOf AddCommentPopup
   */
  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    for (let propName in changes) {
      if (propName === "newComment") {
        let chg = changes[propName];
        if (chg.currentValue) {
            jQuery.extend(true, this.comment, chg.currentValue);
        }
      }

      if (propName === "commentAddOrEdit") {
        let chg = changes[propName];
        if (chg.currentValue) {
           this.comment = new CommentModel();
          _.merge(this.comment, chg.currentValue);
        }
      }

    }
  }

  addComment(event) {
    this.saveOrUpdateComment(event);
  }

  editComment(event) {
    this.saveOrUpdateComment(event);
  }

  saveOrUpdateComment(event) {

    jQuery('#addcomment-form').parsley().validate();
    if (!jQuery('#addcomment-form').parsley().isValid()) return;


    let targetId = event.currentTarget.id;
    let modal = jQuery("#" + targetId).closest("#add-comment");
    if(this.callingpage==="permithistory") {
      this.busy = this.permitHistoryCommentService.savepermitHistoryComment(this.comment, this.addCommnetPermit).subscribe(
        data => {
          let comment = new CommentModel();
          jQuery.extend(true, comment, data);
          this.comment = new CommentModel();
          modal.modal('toggle');
          this.commentAdded.emit(comment);
          /** clear the comment popup error messages */
          jQuery('#addcomment-form').parsley().reset();
          this.hideModal.emit(true);
        });
    } 
    else {
      this.busy = this.commentService.saveMonthlyReportComment(this.comment, this.permitperiod).subscribe(
        data => {
          let comment = new CommentModel();
          jQuery.extend(true, comment, data);
          this.monthlyReportService.triggerCommentAdded(comment);
          this.comment = new CommentModel();
          modal.modal('toggle');
          /** clear the comment popup error messages */
          jQuery('#addcomment-form').parsley().reset();
          this.hideModal.emit(true);
        });
    }
    
  }

 
  cancelComment(event) {
    let targetId = event.currentTarget.id;
    if (!this.isEmpty(targetId)) {
      if((this.comment.userComment != null && !(this.length==this.comment.userComment.length))) {
        
        if(confirm("WARNING: You have unsaved changes. Press Cancel to go back and save these changes, or OK to lose these changes.")) {
          let modal = jQuery("#" + targetId).closest("#add-comment");
          modal.modal('toggle');
          /** clear the comment popup error messages */
          jQuery('#addcomment-form').parsley().reset();
          this.hideModal.emit(true);
        }
      } else {
        let modal = jQuery("#" + targetId).closest("#add-comment");
        modal.modal('toggle');
        /** clear the comment popup error messages */
        jQuery('#addcomment-form').parsley().reset();
        this.hideModal.emit(true);
      }
      
      
    }
  }

}

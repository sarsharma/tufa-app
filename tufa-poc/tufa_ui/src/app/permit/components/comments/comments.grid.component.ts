import { Component, OnInit, AfterViewInit, OnDestroy, Input, SimpleChanges, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { CommentModel } from '../../models/comment/comment.model';
import { TobaccoSubType } from '../../models/tobacco-subtype.model';
import { PermitPeriod } from '../../models/permit-period.model';
import { Subscription } from 'rxjs';

import { MonthlyReportCommentService } from '../../services/monthly.report.comment.service';
import { MonthlyReportEventService } from '../../services/monthly.report.event.service'
import { AuthService } from '../../../login/services/auth.service';

declare var jQuery: any;
import * as _ from 'lodash';
import { PermitHistoryCommentsService } from '../../services/permit.history.comments';
import { flattenStyles } from '@angular/platform-browser/src/dom/dom_renderer';

@Component({
    selector: '[comments-grid]',
    templateUrl: './comments.grid.template.html',
    styleUrls: ['./comments.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class MonthlyReportCommentGridComponent implements OnInit {

    public router: Router;
    @Input() comments: CommentModel[];
    @Input() tobaccoType: TobaccoSubType;
    @Input() formTypeCd: string;
    @Input() page:string
    @Input() permitIds :number;
    @Output() commentAddedForPermitHistory = new EventEmitter<CommentModel>();
    public sortResultsBy = "commentDate";
    public sortResultsOrder = "desc";
    permitCommentId:number;

    newComment: CommentModel;
    showCommentModal: boolean = false;

    permitperiod: PermitPeriod;;

    data: CommentModel[] = [];

    // Checkbox function
    @Input() checked = false;


    commentAction: string;
    comment: CommentModel;
    callingPage: String;
    busy: Subscription;

    subscriptions: Subscription[] = [];
    unResolvecommentFlag:boolean;
    constructor(router: Router,
        private monthlyReportService: MonthlyReportEventService,
        private commentService: MonthlyReportCommentService,
        public authService: AuthService,
        public permitHistoryCommentService:PermitHistoryCommentsService) {
        this.router = router;
        this.data = this.comments;

        this.monthlyReportService.permitperiodSourceLoaded$.subscribe(
            permitperiod => {
                this.permitperiod = permitperiod;
            });

    }

    getUpdatedComment(data) {
        this.commentAddedForPermitHistory.emit(data);
    }

    ngOnInit(): void {
        this.monthlyReportService.triggerChildViewLoaded();
        if(this.page == "permithistory") {
            this.unResolvecommentFlag = true;
        } else {
            this.unResolvecommentFlag = false;
        }
         
    }

    onAddCommentClick() {
        this.commentAction = 'add';
        this.showCommentModal = true;
        this.comment = new CommentModel();
        
        
        if (this.tobaccoType)
            this.comment.tobaccoClassId = this.tobaccoType.tobaccoClassId;

        this.comment.formTypeCd = this.formTypeCd;
    }

    onEditCommentClick(comment) {
        this.commentAction = 'edit';
        this.showCommentModal = true;
        this.comment = new CommentModel();
        _.merge(this.comment, comment);

        if (this.tobaccoType)
            this.comment.tobaccoClassId = this.tobaccoType.tobaccoClassId;

        this.comment.formTypeCd = this.formTypeCd;
    }

    onResolveCommentChk(comment: CommentModel, chkbx) {
        comment.resolveComment = chkbx.checked;
        this.comment = new CommentModel();
        _.merge(this.comment, comment);

        if (this.tobaccoType)
            this.comment.tobaccoClassId = this.tobaccoType.tobaccoClassId;

        this.comment.formTypeCd = this.formTypeCd;
        this.resolveComment();
    }

    resolveComment() {
        if(this.page === "permithistory") {
            
            this.busy = this.permitHistoryCommentService.savepermitHistoryComment(this.comment, this.permitIds).subscribe(
                data => {
                    let comment = new CommentModel();
                    jQuery.extend(true, comment, data);
                    this.monthlyReportService.triggerCommentAdded(comment);
                }); 
        } else {
            
            this.busy = this.commentService.saveMonthlyReportComment(this.comment, this.permitperiod).subscribe(
                data => {
                    let comment = new CommentModel();
                    jQuery.extend(true, comment, data);
                    this.monthlyReportService.triggerCommentAdded(comment);
                }); 
        }
        

        this.subscriptions.push(this.busy);
    }

    canEditComment(commentUser) {
        let sessionUser = this.authService.getSessionUser();
        if (commentUser == sessionUser || (this.page === "permithistory" && commentUser==="CTP_TUFA_MVP12"))
            return true;
        return false;
    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "comments") {
                let chg = changes[propName];
                if (chg.currentValue) {
                    this.data = chg.currentValue;
                }
            }
           

        }
    }

    ngOnDestroy(){
        for(let i in this.subscriptions){
                this.subscriptions[i].unsubscribe();
        }
    }
}

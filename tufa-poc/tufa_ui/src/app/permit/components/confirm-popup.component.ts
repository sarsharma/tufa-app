/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { Document } from '../models/document.model';
import { PermitPeriodService } from '../services/permit-period.service';
import { ShareDataService } from './../../core/sharedata.service';

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class ConfirmPopup
 */
@Component({
  selector: '[confirm-popup]',
  templateUrl: './confirm-popup.template.html',
  encapsulation: ViewEncapsulation.None
})

export class ConfirmPopup implements OnInit, OnDestroy {
  @Input() model: Document;
  @Output() onDocumentEdit = new EventEmitter<boolean>();
  doc: Document;
  documentDesc: string;
  busy: Subscription;
  btnDisable: Boolean = false;

  shareData: ShareDataService;
  ngOnInit() {
     this.documentDesc = 'Are you sure you want to delete this document?';
  }

  constructor (private _permitPeriodService: PermitPeriodService, private _shareData: ShareDataService) {
    this.shareData = _shareData;
  }

  ngOnDestroy() {
      if (this.busy) {
          this.busy.unsubscribe();
      }
  }

  reset(flag: boolean) {
    jQuery('#confirm-popup').modal('hide');
    this.onDocumentEdit.emit(flag);
  }

  deleteAttachment(docid: number) {
        this.btnDisable = true;
        this.busy = this._permitPeriodService.deleteAttachment(docid).subscribe(data => {
            console.log('Successfully deleted!');
            this.onDocumentEdit.emit(true);
            this.reset(true);
            this.btnDisable = false;
        });
    }

    /*
      ngOnChanges() used to capture changes on the input properties.
    */
    ngOnChanges(changes: SimpleChanges) {
      for (let propName in changes) {
        if (propName === "model") {
          let chg = changes[propName];
          if (chg.currentValue) {
            this.doc = chg.currentValue;
          }
        }
      }
    }

}

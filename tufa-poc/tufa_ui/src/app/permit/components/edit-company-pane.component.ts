/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, Input, OnInit, OnDestroy, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Address } from '../../model/address.model';
import { Company } from '../../company/models/company.model';
import { CompanyService } from '../../company/services/company.service';
import { AddressService } from '../../company/services/address.service';
import { ContactService } from '../../company/services/contact.service';

declare var jQuery: any;

@Component({
  selector: '[edit-company-pane]',
  templateUrl: './edit-company-pane.template.html',
  styleUrls: ['./edit-company-panel.style.scss'],
  providers: [CompanyService, AddressService, ContactService]
})

export class EditCompanyPane implements OnInit, OnDestroy {

  @Input() compId: any;
  @Input() permitId: any;
  @Input() periodId: any;
  @Input() ein: any;

  showErrorFlag: boolean;
  alerts: Array<Object>;

  countryList: any;
  contacts: any[];
  addresses: any[];

  priAddress = new Address();
  alternateAddress = new Address();
  company: Company;

  constructor(private companyService: CompanyService,
      private addressService: AddressService,
      private contactService: ContactService
    ) {

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];

    this.showErrorFlag = false;
  }

  registerError(error: string){
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({type: 'warning', msg: error});
  }

  ngOnInit(): void {

    this.getCountries();
    this.getAddress();
    this.getContact();

    if (this.compId !== 0) {
      this.companyService.getCompany(this.compId).subscribe(data => {
        if (data) {
          this.company = <Company>data;
          this.priAddress = (<any>data).primaryAddress;
          if (!this.priAddress) {
            this.priAddress = new Address();
          }
          this.alternateAddress = (<any>data).alternateAddress;
          if (!this.alternateAddress) {
            this.alternateAddress = new Address();
          }
        }
      },
      error => {
        this.registerError(error);
      });
    }else{
      this.companyService.getCompanyEIN(this.ein).subscribe(data => {
        if (data) {
          this.company = <Company>data;
          this.priAddress = (<any>data).primaryAddress;
          if (!this.priAddress) {
            this.priAddress = new Address();
          }
          this.alternateAddress = (<any>data).alternateAddress;
          if (!this.alternateAddress) {
            this.alternateAddress = new Address();
          }
        }
      },
      error => {
        this.registerError(error);
      });
    }

  }

  getCountries() {
    this.addressService.getCountryNames().subscribe(data => {
      if (data) {
        this.countryList = data;
      }
      },
      error => {
        this.registerError(error);
      });
  }

  getAddress() {
    if ((this.compId || this.ein) && this.periodId) {
      this.addressService.getReportAddress(this.compId, this.permitId, this.periodId,this.ein).subscribe(data => {
        if (data) {
          this.addresses = <any[]>data;
        }
      },
      error => {
        this.registerError(error);
      });
    }
  }

  getContact() {
    if (this.compId && this.periodId) {
      this.contactService.getReportContacts(this.compId, this.permitId, this.periodId).subscribe(data => {
        if (data) {
          this.contacts = <any[]>data;

        }
      },
      error => {
        this.registerError(error);
      });
    }
  }

  ngOnDestroy() {
    // this.sub.unsubscribe();
  }

  /*
   ngOnChanges() used to capture changes on the input properties.
*/
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "companyId") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.compId = chg.currentValue;
        }
      }
    }
    for (let propName in changes) {
      if (propName === "permitId") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.permitId = chg.currentValue;
        }
      }
    }
    for (let propName in changes) {
      if (propName === "periodId") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.periodId = chg.currentValue;
        }
      }
    }
  }
}

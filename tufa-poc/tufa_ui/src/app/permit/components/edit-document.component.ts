/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgForm } from '../../../../node_modules/@angular/forms';
import { Document } from '../models/document.model';
import { PermitPeriodService } from '../services/permit-period.service';
import { ShareDataService } from './../../core/sharedata.service';

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class EditDocumentPopup
 */
@Component({
  selector: '[edit-document]',
  templateUrl: './edit-document.template.html'
})

export class EditDocumentPopup implements OnInit, OnDestroy {
  @Input() documentModel: Document;
  doc = new Document();
  @Output() onDocumentEdit = new EventEmitter<boolean>();
  filesToUpload: Array<File> = [];
  shareData: ShareDataService;
  periodId: any;
  permitId: any;
  permitNumber: any;
  docFileName: string;
  busy: Subscription;
  edit: Subscription;
  types: string[] = [];
  selFormTypes: string[];
  btnDisable: Boolean = false;
  selectAllFlag: Boolean = false;
  warningFlag: Boolean = false;
  isExtensionNotPDF: Boolean = false;
  isFileTooLarge: boolean;


  @ViewChild('editattachmentform') documentUplaod: NgForm;

  ngOnInit() {
    let formTypes = this.shareData.reportType === "Importer" ? ['3852', '5220.6', '7501']
      : ['3852', '5210.5', '5000.24'];

    // tslint:disable-next-line:max-line-length
    this.selFormTypes = ['FDA ' + formTypes[0], 'TTB ' + formTypes[1], this.shareData.reportType === "Importer" ? 'CBP ' + formTypes[2] : 'TTB ' + formTypes[2], 'Other'];
    this.periodId = this.shareData.periodId;
    this.permitId = this.shareData.permitId;
    this.permitNumber = this.shareData.permitNum;
  }

  constructor(private _permitPeriodService: PermitPeriodService, private _shareData: ShareDataService) {
    this.shareData = _shareData;
    this.warningFlag = false;
  }

  ngOnDestroy() {
    if (this.busy) {
      this.busy.unsubscribe();
    }

    if (this.edit) {
      this.edit.unsubscribe();
    }
  }

  seeNTypes(s: any, obj: any) {
    if (!s.target.checked || this.types.indexOf(obj) === -1) {
      s.target.checked ? (this.types.push(obj)) : this.types.splice(this.types.indexOf(obj), 1);
    }
    console.log(this.types);
  }

  selectAll(s: any) {
    this.selectAllFlag = s.target.checked;
    for (let type of this.selFormTypes) {
      if (type !== "Other") {
        this.seeNTypes(s, type);
      }
    }
  }

  fileChangeEvent(event) {
    this.filesToUpload = <Array<File>>event.target.files;
    if (this.filesToUpload[0]) {
      this.docFileName = this.filesToUpload[0].name;
      let fileNmalphanumeric = this.docFileName.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
      //to check if the uploaded file contains the current permitnumber
      if (fileNmalphanumeric.indexOf(this.permitNumber.toLowerCase()) < 0) {
        this.warningFlag = true;
      } else {
        this.warningFlag = false;
      }

      let fileExt = this.docFileName.split('.').pop();
      if (fileExt.toUpperCase() !== "PDF") {
        this.isExtensionNotPDF = true;
      } else {
        this.isExtensionNotPDF = false;
      }

      this.isFileTooLarge = this.filesToUpload[0].size > 25000000;

    } else {
      this.docFileName = "";
      this.warningFlag = false;
      this.isExtensionNotPDF = false;
      this.isFileTooLarge = false;
    }

  }

  fileNameCHnaged(event) {
    if (this.docFileName != null) {
      let fileNmalphanumeric = this.docFileName.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
      //to check if the uploaded file contains the current permitnumber
      if (fileNmalphanumeric === this.permitNumber.toLowerCase()) {
        this.warningFlag = false;
      } else {
        this.warningFlag = true;
      }
    }

  }

  uploadAttachment() {
    jQuery('#editattachmentform').parsley().validate();
    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#editattachmentform').parsley().isValid() && !this.isExtensionNotPDF && !this.isFileTooLarge) {
      this.btnDisable = true;
      if (this.filesToUpload.length > 0) {
        let formData: FormData = new FormData();
        formData.append('file', this.filesToUpload[0]);

        let document = new Document();
        document.docDesc = this.docFileName;
        document.periodId = this.periodId;
        document.permitId = this.permitId;
        document.docFileNm = "ATCH";
        document.docStatusCd = "INCP";
        document.formTypes = this.types.join(", ");

        formData.append('metaData', JSON.stringify(document));
        this.edit = this.uploadImpl(formData);
      } else {
        let document = new Document();
        document.documentId = this.doc.documentId;
        document.docDesc = this.docFileName;
        document.formTypes = this.types.join(", ");
        this.edit = this.updateAttachment(document);
      }
    }
  }

  uploadImpl(document) {
    return this._permitPeriodService.uploadAttachment(document, this.periodId, this.permitId).subscribe(data => {
      if (data) {
        this.onDocumentEdit.emit(true);
        this.reset();
        this.btnDisable = false;
      }
    });
  }

  updateAttachment(document: Document) {
    return this._permitPeriodService.updateAttachment(document, this.periodId, this.permitId).subscribe(data => {
      if (data) {
        this.onDocumentEdit.emit(true);
        this.reset();
        this.btnDisable = false;
      }
    });
  }

  reset() {
    this.documentUplaod.resetForm();
    jQuery('#edit-document').modal('hide');
  }

  cancel() {
    this.reset();
    this.onDocumentEdit.emit(false);
  }

  /*
    ngOnChanges() used to capture changes on the input properties.
  */
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "documentModel") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.doc = chg.currentValue;
          this.docFileName = chg.currentValue.docDesc;
          this.types = chg.currentValue.formTypes ? chg.currentValue.formTypes.split(", ") : [];
        }
      }
    }
  }

}

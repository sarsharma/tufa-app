/*
@author : Deloitte
this is Component for edit permit component
*/
import { ComponentFixture, async, inject, TestBed } from '@angular/core/testing';
import { EditPermitPopup } from './edit-permit.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core'; 
import { CommonModule }  from '@angular/common';
import { RouterModule, Routes, provideRoutes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../shared/shared.module'
import { HttpModule } from '@angular/http';
import { AuthorizationHelper } from '../../authentication/util/authorization.helper.util';
import { CompanyService } from '../../company/services/company.service';
import { HttpClient } from '../../../../node_modules/@angular/common/http';

declare var jQuery: any;

describe('Edit Permit Popup', () => {
  let comp:    EditPermitPopup;
  let fixture: ComponentFixture<EditPermitPopup>;

  // provide our implementations or mocks to the dependency injector
  beforeEach( async( () => {
    TestBed.configureTestingModule({
      imports: [ SharedModule, HttpModule, RouterTestingModule ],
      declarations: [EditPermitPopup],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [HttpClient, AuthorizationHelper, CompanyService,
        provideRoutes([{path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTesting', pathMatch:'full'}])
      ]
    }).compileComponents();
  }));

   beforeEach(() => {
      fixture = TestBed.createComponent(EditPermitPopup);
      comp = fixture.componentInstance;
      fixture.detectChanges();
  });

});

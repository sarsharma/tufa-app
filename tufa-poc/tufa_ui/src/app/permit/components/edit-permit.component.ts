/*
@author : Deloitte

this is Component for adding permit.
*/

import { Component, OnInit, ViewEncapsulation, SimpleChange, Input, Output, EventEmitter } from '@angular/core';
import { Permit } from '../models/permit.model';
import { Company } from '../../company/models/company.model';
import { CompanyService } from '../../company/services/company.service';

declare var jQuery: any;

@Component({
  selector: '[edit-permit]',
  templateUrl: './edit-permit.template.html',
  styleUrls: ['./edit-permit.style.scss'],
  providers: [CompanyService],
  encapsulation: ViewEncapsulation.None
})

export class EditPermitPopup implements OnInit {

  model = new Permit();
  companyId: any;
  permitId: any;
  date: string;
  showErrorFlag: boolean;
  alerts: Array<Object>;
  permitNumber: string;  // use local variable to bind instead of model
  permitType: string;
  activedateid: string = "active-date";
  public mask = [/[A-Za-z]/, /[A-Za-z]/,'-',/[A-Za-z]/, /[A-Za-z]/,'-', /\d/, /\d/, /\d/, /\d/, /\d/ ];

  company: Company = new Company();
  @Input() companyModel: Company;
  @Output() onCompanyEdit = new EventEmitter<boolean>();

  permittypeList = ['Select Type', 'Manufacturer', 'Importer'];

  // initialize the datetime control.
  datepickerOpts = {
    autoclose: true,
    todayBtn: 'linked',
    todayHighlight: true,
    endDate: new Date(),
    assumeNearbyYear: true,
    requiredErrorMsg: "Error: TUFA Active Date is required",
    title: 'Enter Active Date in MM/DD/YYYY format'
  };

  constructor(private companyService: CompanyService) {
    this.model.permitStatusTypeCd = "ACTV";

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];

    this.showErrorFlag = false;
  }

  ngOnInit(): void {
    this.permitType = "Select Type";
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {

    for (let propName in changes) {
      if (propName === "companyModel") {
        let chg = changes[propName];
      if (chg.currentValue) {
          // this.companyId = chg.currentValue.companyId;
          this.company.companyId = chg.currentValue.companyId;
          this.company.legalName = chg.currentValue.legalName;
          this.company.einNumber = chg.currentValue.einNumber;
        }
      }

    }
  }

  createPermit(permit: Permit): void {

    jQuery('#permitForm').parsley().validate();
    permit.companyId = +this.company.companyId; // the unary + converts to number

    // unmask permit number before proceding ahead
    // transforming the local bound variable to the model. if we modify the model directly then
    // RegEx patter validation fails on the UI side.
    let curpermitNumber: string = this.permitNumber;
    permit.permitNum = curpermitNumber = '' ? '' : curpermitNumber.replace(/-/g , "");
    permit.duplicateFlag = this.showErrorFlag;
    permit.permitStatusTypeCd = "ACTV";
    permit.permitTypeCd = this.permitType === "Manufacturer" ? "MANU" : "IMPT";
    permit.ttbstartDt =  this.model.issueDt;
    if (jQuery('#permitForm').parsley().isValid()) {
       this.companyService.createPermit(permit).subscribe(data => {
        if (data) {
          this.reset(true);
        }
      },
      error => {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({type: 'warning', msg: error});
      });
    }
  }

  reset(flag) {
    jQuery('#permitForm').parsley().reset();
    this.permitType = "Select Type";
    this.showErrorFlag = false;
    jQuery('#edit-permit').modal('hide');
    jQuery('#edit-permit').on('hidden').find('input[type="text"]').val('');
    this.onCompanyEdit.emit(flag);
  }

  selectType() {
    if (/^tp/i.test(this.permitNumber)) {
      this.permitType = 'Manufacturer';
    } else if (/^\w{2}.ti/i.test(this.permitNumber)) {
      this.permitType = 'Importer';
    } else {
      this.permitType = 'Select Type';
    }
  }
}

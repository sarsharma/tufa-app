/*
@author : Deloitte

this is Component for FDA form 3852.
*/

import { Component, Input, Output, EventEmitter, ViewEncapsulation, SimpleChanges } from '@angular/core';
import { TobaccoType } from '../../models/imptobacco-type.model';
import { ImporterTobaccoSubType } from '../../models/imptobacco-subtype.model';
import { ErrorHandlingService } from '../../../shared/service/error.handling.service';
import { FormsCalculationService } from '../../../shared/service/forms.calculation.service';

declare var jQuery: any;

@Component({
  selector: '[impform-3852]',
  templateUrl: './impform-3852.template.html',
  styleUrls: ['./impform-3852.style.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ImpForm3852 {

  @Input() model: TobaccoType;
  @Input() errorMsgs: any[];
  @Input() isTTSelected: number;

  @Output() hasChanges = new EventEmitter<boolean>();
  @Output() download = new EventEmitter<number>();

  quantity: number;

  myCalcType = "tax";

  numberValidationError: boolean;
  decimalValidationError: boolean;

  numberValidationErrorMessage = "Data Type Error: provide valid entry";

  private _regexNumber = /^\d+(,\d+)*$/;
  //private _regexDecimalNumber=/^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$/;
  private _regexDecimalNumber = /^(?:^|\s)(\d*\.?\d+|\d{1,3}(?:,\d{3})*(?:\.\d+)?)(?!\S)$/;
  private _regexCurrency = /^\$?\d+(,\d{3})*\.?[0-9]{0,2}?$/;

  form3852DataValidError: boolean = false;
  formName: string;
  formTypeCd = "3852";

  formErrorMsg = {
    name: 'form3852',
    msg: 'Invalid information in form FDA 3852',
    type: 'warning'
  };

  constructor(private errorHandlingService: ErrorHandlingService, private formCalculationService: FormsCalculationService) {
    this.formName = 'form3852';
    errorHandlingService.impFormChanged$.subscribe(
      param => {
        this.setGlobalError(this.checkForm3852InError());
      });
  }

  ngOnInit(): void { }

  ngOnDestory() { }

  /*public validateInput(value: string,ttypeName, fieldType) {

    let ttsubType: ImporterTobaccoSubType = this.model.getTobaccoType(ttypeName);
    
    if(!value){
      ttsubType.isRmvlQty3852Valid = true;
      ttsubType.isTaxAmt3852StrValid = true;
      this.form3852DataValidError = false;
      return;
    }
    
    if (ttsubType) {
      if (fieldType == "qtynumber") {
        ttsubType.isRmvlQty3852Valid = this._regexNumber.test(value.replace(',',"")) ;
      } else if (fieldType == "amtdecimal") {
        ttsubType.isTaxAmt3852StrValid = this._regexCurrency.test(value);
      } else if (fieldType == "qtydecimal") {
       ttsubType.isRmvlQty3852Valid = this._regexDecimalNumber.test(value);
      }
      
      this.form3852DataValidError= !ttsubType.isRmvlQty3852Valid||!ttsubType.isTaxAmt3852StrValid;
    }
    
  }*/

  public updateCalc3852(ttypeName: string) {

    var model: ImporterTobaccoSubType = this.model.getTobaccoType(ttypeName);
    var rmvlQty3852: number;
    rmvlQty3852 = model.rmvlQty3852 ? model.rmvlQty3852 : 0;
    this.hasChanges.emit(true);
    if (!model.isRmvlQty3852Valid || !model.isTaxAmt3852StrValid) {

      if (model.isTaxAmt3852Pristine) {
        model.taxAmt3852 = null;
        model.taxDiff3852 = null;
      }

      if (model.isQty52206Pristine) {
        model.largeSticks = null;
        model.smallSticks = null;
        model.qty52206 = null;
      }

      return;
    }

    /**calculation for form 3852 **/

    if (model.tobaccoSubTypeName !== "Cigars") {
      model.calculateTaxAmount3852();
      model.calculateTaxDiff3852();
    }
    /** calculation for form 5220 **/
    model.calculateQty5220();
    //model.calculateTaxDiff5220();
    model.calculateQtyDiff5220();

  }

  private calcTaxDiff3852(ttypeName: string) {
    var model = this.model.getTobaccoType(ttypeName);
    this.hasChanges.emit(true);
    model.isTaxAmt3852Pristine = false;
    model.taxAmt3852 = model.taxAmt3852 ? model.taxAmt3852 : 0;
    model.calculateTaxDiff3852();
    //model.calculateTaxDiff5220();
    model.calculateQtyDiff5220();
  }


  public checkForm3852InError(): boolean {
    var isError: boolean = false;
    var tTypes: ImporterTobaccoSubType[] = this.model.getTobaccoTypes();
    for (var i in tTypes) {
      isError = tTypes[i].is3852Error();
      if (isError) break;
    }

    return isError;
  }

  public setGlobalError(isError: boolean) {

    let errorPresent: boolean = false;

    this.errorMsgs.forEach(function (item, index, array) {
      if (item.name === this.formName) {
        errorPresent = true;
        if (!isError) array.splice(index, 1);
      }
    }, this);

    if (isError && !errorPresent) this.errorMsgs.push(this.formErrorMsg);

  }

}

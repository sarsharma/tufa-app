/*
@author : Deloitte
this is Component for Importer form 5220.
*/
import { ComponentFixture, async, inject, TestBed } from '@angular/core/testing';
import { ImpForm5220 } from './impform-5220.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core'; 
import { CommonModule }  from '@angular/common';
import { RouterModule, Routes, provideRoutes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../../shared/shared.module'
import { HttpModule } from '@angular/http';
import { AuthorizationHelper } from '../../../authentication/util/authorization.helper.util';
import { TobaccoType } from '../../models/imptobacco-type.model';
import { ImporterTobaccoSubType } from '../../models/imptobacco-subtype.model';
import { ErrorHandlingService } from '../../../shared/service/error.handling.service';
import { FormsCalculationService } from '../../../shared/service/forms.calculation.service';
import { CustomNumberPipe } from '../../../shared/util/custom.number.pipe';
import { HttpClient } from '../../../../../node_modules/@angular/common/http';

declare var jQuery: any;

describe('Importer Form 5220', () => {
  let comp:    ImpForm5220;
  let fixture: ComponentFixture<ImpForm5220>;

  // provide our implementations or mocks to the dependency injector
  beforeEach( async( () => {
    TestBed.configureTestingModule({
      imports: [ SharedModule, HttpModule, RouterTestingModule ],
      declarations: [ImpForm5220, CustomNumberPipe],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [HttpClient, AuthorizationHelper, CustomNumberPipe, 
        provideRoutes([{path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTesting', pathMatch:'full'}])
      ]
    }).compileComponents();
  }));

   beforeEach(() => {
    fixture = TestBed.createComponent(ImpForm5220);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

});

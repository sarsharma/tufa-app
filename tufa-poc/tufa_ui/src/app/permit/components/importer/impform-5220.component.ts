/*
@author : Deloitte

this is Component for editing company address.
*/

import {Component, Input, Output, EventEmitter, ViewEncapsulation,SimpleChanges } from '@angular/core';
import {TobaccoType} from '../../models/imptobacco-type.model';
import {ImporterTobaccoSubType} from '../../models/imptobacco-subtype.model';
import {ErrorHandlingService} from '../../../shared/service/error.handling.service';
import {MonthlyReportEventService} from '../../services/monthly.report.event.service';

declare var jQuery: any;

@Component({
  selector: '[impform-5220]',
  templateUrl: './impform-5220.template.html',
  styleUrls: ['./impform-5220.style.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ImpForm5220 {
  @Input() model: TobaccoType;  
  @Input() errorMsgs: any [];
  @Output() hasChanges = new EventEmitter<boolean>();
  @Output() download = new EventEmitter<number>();

  includeReport: boolean = true;
  
  form5220Error: boolean;
  form5220DataValidError: boolean = false;
  formName: string;
  
  formErrorMsg =  { 
            name:'form5220',
            msg:'Invalid information in form TTB 5220.6',
            type: 'warning'
        };
        
  totalQty: number;
  formTypeCd = "5220.6";

  iscalcTypeTax:boolean = true;
  private _regexNumber = /^\d+(,\d+)*$/;
  private _regexDecimalNumber = /^(?:^|\s)(\d*\.?\d+|\d{1,3}(?:,\d{3})*(?:\.\d+)?)(?!\S)$/;
  //private _regexDecimalNumber=/^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$/;

  numberValidationErrorMessage="Data Type Error: provide valid entry";

  defaultStr = 'Disabled';
  cigsLvalid = { product: this.defaultStr };
  cigsLproducts: string[] = [
    'Action',
    'Mark Correct'
  ];
  cigsSvalid = { product: this.defaultStr };
  cigsSproducts: string[] = [
      'Action',
    'Mark Correct'
  ];
  cigarsLvalid = { product: this.defaultStr };
  cigarsLproducts: string[] = [
    'Action',
    'Mark Correct'
  ];
  cigarsSvalid = { product: this.defaultStr };
  cigarsSproducts: string[] = [
      'Action',
    'Mark Correct'
  ];
  snuffvalid = { product: this.defaultStr };
  snuffproducts: string[] = [
    'Action',
    'Mark Correct'
  ];
  chewvalid = { product: this.defaultStr };
  chewproducts: string[] = [
      'Action',
    'Mark Correct'
  ];
  pipevalid = { product: this.defaultStr };
  pipeproducts: string[] = [
    'Action',
    'Mark Correct'
  ];
  rollvalid = { product: this.defaultStr };
  rollproducts: string[] = [
      'Action',
    'Mark Correct'
  ];
  
  constructor(private errorHandlingService: ErrorHandlingService,
              private monthlyReportService: MonthlyReportEventService){
    
    this.formName = 'form5220';
    
    errorHandlingService.impFormChanged$.subscribe(
      param => {
        this.setGlobalError(this.checkForms5220InError());
      });
  }


  ngOnInit(): void {
        this.includeReport = this.model.include5220Form;
  }
  
  public validateInput(value: string,ttypeName, fieldType,size) {

    let ttsubType: ImporterTobaccoSubType = this.model.getTobaccoType(ttypeName);
    
    if(!value){
      ttsubType.isQuantity52206StrValid = true;
      this.form5220DataValidError = false;
      return;
    }
    
    if (ttsubType) {
      if (fieldType === "qtynumber") {
        if(size && size === "small")
          ttsubType.isQuantity52206StrValid = ttsubType.isQuantity52206StrValidSml = this._regexNumber.test(value);
        else if (size && size === "large") 
          ttsubType.isQuantity52206StrValid = ttsubType.isQuantity52206StrValidLrg = this._regexNumber.test(value);
        else 
            ttsubType.isQuantity52206StrValid = this._regexNumber.test(value);
    } else if (fieldType === "qtydecimal") {
            ttsubType.isQuantity52206StrValid = this._regexDecimalNumber.test(value);
      }
      
      this.form5220DataValidError = !ttsubType.isQuantity52206StrValid;
    }
  }

   public  updateCalc5220(ttypeName: string){
     
    let ttsubType: ImporterTobaccoSubType = this.model.getTobaccoType(ttypeName);
    this.hasChanges.emit(true);
    ttsubType.isQty52206Pristine=false;
    //ttsubType.calculateTaxDiff5220();
    ttsubType.calculateQtyDiff5220();
  }

  // User may not fill form 3852 but can enter large and small sticks  
  public updateTotalSticks(model: ImporterTobaccoSubType ){
     if(!model.isQuantity52206StrValid) return;
     this.hasChanges.emit(true);
     model.qty52206 = Number(model.largeSticks?model.largeSticks:0) + Number(model.smallSticks?model.smallSticks:0);
     model.isQty52206Pristine=false;
     //model.calculateTaxDiff5220();
     model.calculateQtyDiff5220();
  }

  public includeForm5220(){
        this.model.include5220Form = this.includeReport;
  }

  public largecigs(model : ImporterTobaccoSubType){
    if(model.largeSticks > 0)
    this.cigsLvalid.product='Action';
  }

  public smallcigs(model : ImporterTobaccoSubType){
    if(model.smallSticks > 0)
    this.cigsSvalid.product='Action';
  }

  public largecigars(model : ImporterTobaccoSubType){
    if(model.largeSticks > 0)
    this.cigarsLvalid.product='Action';
  }

  public smallcigars(model : ImporterTobaccoSubType){
    if(model.smallSticks > 0)
    this.cigarsSvalid.product='Action';
  }

  public chew(model : ImporterTobaccoSubType){
    if(model.qty52206 > 0)
    this.chewvalid.product='Action';
  }

  public snuff(model : ImporterTobaccoSubType){
    if(model.qty52206 > 0)
    this.snuffvalid.product='Action';
  }

  public pipe(model : ImporterTobaccoSubType){
    if(model.qty52206 > 0)
    this.pipevalid.product='Action';
  }
    
  public roll(model : ImporterTobaccoSubType){
    if(model.qty52206 > 0)
    this.rollvalid.product='Action';
  }

  public numericVal(event){    
    if ((event.which >= 48 && event.which <= 57) || event.which === 188) 
      return true;
    else
    {
       event.preventDefault();
       return false;
     }
  }
  
  public checkForms5220InError(): boolean{
    if(!this.includeReport) return false;
    var isError:boolean = false;
    var tTypes = this.model.getTobaccoTypes();
    for(var i in tTypes){
         isError = tTypes[i].is52206Error();
         if(isError) break;
    }
    return isError;
  }

  public setGlobalError(isError: boolean){
      
    let errorPresent: boolean = false;
    
    this.errorMsgs.forEach(function(item,index, array){
              if(item.name === this.formName){
                 errorPresent = true;
                 if(!isError)array.splice(index,1);
              }
        },this);
          
        if(isError&&!errorPresent)this.errorMsgs.push(this.formErrorMsg);

  }

}

/*
@author : Deloitte
this is Component for Importer form 7501.
*/
import { ComponentFixture, async, inject, TestBed } from '@angular/core/testing';
import { ImpForm7501 } from './impform-7501.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core'; 
import { CommonModule }  from '@angular/common';
import { RouterModule, Routes, provideRoutes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../../shared/shared.module'
import { HttpModule } from '@angular/http';
import { AuthorizationHelper } from '../../../authentication/util/authorization.helper.util';
import { TobaccoType } from '../../models/imptobacco-type.model';
import { ImporterTobaccoSubType } from '../../models/imptobacco-subtype.model';
import { ErrorHandlingService } from '../../../shared/service/error.handling.service';
import { FormsCalculationService } from '../../../shared/service/forms.calculation.service';
import { HttpClient } from '../../../../../node_modules/@angular/common/http';
import { CustomNumberPipe } from '../../../shared/util/custom.number.pipe';

declare var jQuery: any;

describe('Importer Form 7501', () => {
  let comp:    ImpForm7501;
  let fixture: ComponentFixture<ImpForm7501>;

  // provide our implementations or mocks to the dependency injector
  beforeEach( async( () => {
    TestBed.configureTestingModule({
      imports: [ SharedModule, HttpModule, RouterTestingModule ],
      declarations: [ImpForm7501, CustomNumberPipe],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [HttpClient, AuthorizationHelper, CustomNumberPipe, 
        provideRoutes([{path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTesting', pathMatch:'full'}])
      ]
    }).compileComponents();
  }));

   beforeEach(() => {
    fixture = TestBed.createComponent(ImpForm7501);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

});

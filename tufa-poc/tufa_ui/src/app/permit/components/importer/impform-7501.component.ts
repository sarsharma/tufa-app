/*
@author : Deloitte

this is Component for form 7501.
*/

import { Component, EventEmitter, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { DataTable } from '../../../shared/datatable/DataTable';
import { ErrorHandlingService } from '../../../shared/service/error.handling.service';
import { FormsCalculationService } from '../../../shared/service/forms.calculation.service';
import { Form7501Filter, Form7501FilterData } from '../../models/form7501filter.model';
import { TobaccoType } from '../../models/imptobacco-type.model';

declare var jQuery: any;
var form7501Comp;
@Component({
    selector: '[impform-7501]',
    templateUrl: './impform-7501.template.html',
    styleUrls: ['./impform-7501.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ImpForm7501 {
    @Input() model: TobaccoType;
    @Input() errorMsgs: any[];
    @Output() hasChanges = new EventEmitter<boolean>();
    @Output() download = new EventEmitter<number>();
    Math: any;

    isCalcTypeCost: boolean = true;

    showryodelete: boolean = false;
    showpipedelete: boolean = false;
    showchewdelete: boolean = false;
    showsnuffdelete: boolean = false;
    showcigardelete: boolean = false;
    showcigarettedelete: boolean = false;

    sortOrderRyoDesc: boolean = false;
    sortOrderPipeDesc: boolean = false;
    sortOrderChewDesc: boolean = false;
    sortOrderSnuffDesc: boolean = false;
    sortOrderCigarDesc: boolean = false;
    sortOrderCigaretteDesc: boolean = false;

    sortByRyo: string = '';
    sortByPipe: string = '';
    sortByChew: string = '';
    sortBySnuff: string = '';
    sortByCigar: string = '';
    sortByCigarette: string = '';

    @ViewChild('mf') cigaretteTable: DataTable;
    @ViewChild('mfc') cigarTable: DataTable;
    @ViewChild('mfs') snuffTable: DataTable;
    @ViewChild('mfch') chewTable: DataTable;
    @ViewChild('mfp') pipeTable: DataTable;
    @ViewChild('mfryo') ryoTable: DataTable;

    paginationCigarette: {
        activePage: number,
        rowsOnPage: number,
        dataLength: number
    } = {
            activePage: 1,
            rowsOnPage: 10,
            dataLength: 0
        };


    sortBy: string = '';

    formFilter = new Form7501Filter();
    cigarFormFilter = new Form7501Filter();
    snuffFormFilter = new Form7501Filter();
    chewFormFilter = new Form7501Filter();
    pipeFormFilter = new Form7501Filter();
    ryoFormFilter = new Form7501Filter();
    formFilterSet = { cig: this.formFilter, cigar: this.cigarFormFilter, chew: this.chewFormFilter, snuff: this.snuffFormFilter, pipe: this.pipeFormFilter, ryo: this.ryoFormFilter };

    cigFilterData = new Form7501FilterData();
    cigarFilterData = new Form7501FilterData();
    chewFilterData = new Form7501FilterData();
    snuffFilterData = new Form7501FilterData();
    pipeFilterData = new Form7501FilterData();
    ryoFilterData = new Form7501FilterData();

    cigarlist: string[] = ['Select Type', 'None', 'Small', 'Large', 'Ad Valorem'];



    //Cigarettes
    public columns: Array<any> = [
        { name: 'lineNum', type: 'input', filtering: { filterString: '', columnName: 'lineNum', placeholder: 'Line #', class: 'cig' } },
        { name: 'qty', type: 'input', filtering: { filterString: '', columnName: 'qty', placeholder: 'Quantity', class: 'cig' } },
        { name: 'taxAmt', type: 'input', filtering: { filterString: '', columnName: 'taxAmt', placeholder: 'Tax Amount', class: 'cig' } },
        { name: 'calTaxRate', type: 'input', filtering: { filterString: '', columnName: 'calTaxRate', placeholder: 'Tax Rate', class: 'cig' } },
        { name: 'taxDiff', type: 'input', filtering: { filterString: '', columnName: 'taxDiff', placeholder: 'Tax Difference', class: 'cig' } }
    ];
    //Cigars
    public columnsCigars: Array<any> = [
        { name: 'lineNum', type: 'input', filtering: { filterString: '', columnName: 'lineNum', placeholder: 'Line #', class: 'cigar' } },
        {
            name: 'type', type: 'select', defaultvalue: '',
            options: [{ name: 'All', value: '' }, { name: 'Ad Valorem', value: 'Ad Valorem' }, { name: 'Small', value: 'Small' }, { name: 'Large', value: 'Large' }],
            filtering: { filterString: '', columnName: 'type', placeholder: 'Cigar Type', class: 'cigar' }
        },
        { name: 'qty', type: 'input', filtering: { filterString: '', columnName: 'qty', placeholder: 'Quantity', class: 'cigar' } },
        { name: 'taxAmt', type: 'input', filtering: { filterString: '', columnName: 'taxAmt', placeholder: 'Tax Amount', class: 'cigar' } },
        { name: 'calTaxRate', type: 'input', filtering: { filterString: '', columnName: 'calTaxRate', placeholder: 'Tax Rate', class: 'cigar' } },
        { name: 'taxDiff', type: 'input', filtering: { filterString: '', columnName: 'taxDiff', placeholder: 'Tax Difference', class: 'cigar' } }
    ];

    //Snuff
    public columnsSnuff: Array<any> = [
        { name: 'lineNum', type: 'input', filtering: { filterString: '', columnName: 'lineNum', placeholder: 'Line #', class: 'snuff' } },
        { name: 'qty', type: 'input', filtering: { filterString: '', columnName: 'qty', placeholder: 'Quantity', class: 'snuff' } },
        { name: 'taxAmt', type: 'input', filtering: { filterString: '', columnName: 'taxAmt', placeholder: 'Tax Amount', class: 'snuff' } },
        { name: 'calTaxRate', type: 'input', filtering: { filterString: '', columnName: 'calTaxRate', placeholder: 'Tax Rate', class: 'snuff' } },
        { name: 'taxDiff', type: 'input', filtering: { filterString: '', columnName: 'taxDiff', placeholder: 'Tax Difference', class: 'snuff' } }
    ];
    //Chew
    public columnsChew: Array<any> = [
        { name: 'lineNum', type: 'input', filtering: { filterString: '', columnName: 'lineNum', placeholder: 'Line #', class: 'chew' } },
        { name: 'qty', type: 'input', filtering: { filterString: '', columnName: 'qty', placeholder: 'Quantity', class: 'chew' } },
        { name: 'taxAmt', type: 'input', filtering: { filterString: '', columnName: 'taxAmt', placeholder: 'Tax Amount', class: 'chew' } },
        { name: 'calTaxRate', type: 'input', filtering: { filterString: '', columnName: 'calTaxRate', placeholder: 'Tax Rate', class: 'chew' } },
        { name: 'taxDiff', type: 'input', filtering: { filterString: '', columnName: 'taxDiff', placeholder: 'Tax Difference', class: 'chew' } }
    ];
    //RYO
    public columnsRYO: Array<any> = [
        { name: 'lineNum', type: 'input', filtering: { filterString: '', columnName: 'lineNum', placeholder: 'Line #', class: 'ryo' } },
        { name: 'qty', type: 'input', filtering: { filterString: '', columnName: 'qty', placeholder: 'Quantity', class: 'ryo' } },
        { name: 'taxAmt', type: 'input', filtering: { filterString: '', columnName: 'taxAmt', placeholder: 'Tax Amount', class: 'ryo' } },
        { name: 'calTaxRate', type: 'input', filtering: { filterString: '', columnName: 'calTaxRate', placeholder: 'Tax Rate', class: 'ryo' } },
        { name: 'taxDiff', type: 'input', filtering: { filterString: '', columnName: 'taxDiff', placeholder: 'Tax Difference', class: 'ryo' } }
    ];
    //Pipe
    public columnsPipe: Array<any> = [
        { name: 'lineNum', type: 'input', filtering: { filterString: '', columnName: 'lineNum', placeholder: 'Line #', class: 'pipe' } },
        { name: 'qty', type: 'input', filtering: { filterString: '', columnName: 'qty', placeholder: 'Quantity', class: 'pipe' } },
        { name: 'taxAmt', type: 'input', filtering: { filterString: '', columnName: 'taxAmt', placeholder: 'Tax Amount', class: 'pipe' } },
        { name: 'calTaxRate', type: 'input', filtering: { filterString: '', columnName: 'calTaxRate', placeholder: 'Tax Rate', class: 'pipe' } },
        { name: 'taxDiff', type: 'input', filtering: { filterString: '', columnName: 'taxDiff', placeholder: 'Tax Difference', class: 'pipe' } }
    ];

    formName: string;
    formTypeCd = "7501";
    private modPrefix = "F7501";
    filterinformation: any;

    formErrorMsg = {
        name: 'form7501',
        msg: 'Invalid information in form CBP 7501',
        type: 'warning'
    };

    constructor(private errorHandlingService: ErrorHandlingService,
        private storage: LocalStorageService,
        private formsCalculationService: FormsCalculationService) {

        this.formName = 'form7501';
        this.Math = Math;
        errorHandlingService.impFormChanged$.subscribe(
            param => {
                this.setGlobalError(this.checkForm7501InError());
            });

        formsCalculationService.impformChanged$.subscribe(
            param => {
                this.model.getTobaccoTypes().forEach(function (item, index, array) {
                    item.totalAndCalculatedAmt7501();
                });
            }
        );
    }

    pageChange(event) {
        this.paginationCigarette = event;
        console.log(this.paginationCigarette)
    }

    ngOnInit(): void {
        jQuery('#popover4').popover();
        jQuery('#popover5').popover();
        this.updateFilters();

        this.showryodelete = false;
        this.showchewdelete = false;
        this.showpipedelete = false;
        this.showcigarettedelete = false;
        this.showsnuffdelete = false;
        this.showcigardelete = false;


        this.sortOrderRyoDesc = false;
        this.sortOrderPipeDesc = false;
        this.sortOrderChewDesc = false;
        this.sortOrderSnuffDesc = false;
        this.sortOrderCigarDesc = false;
        this.sortOrderCigaretteDesc = false;

    }

    ngAfterViewInit() {
        for (let col of this.columns) {
            if (col.filtering) {
                this.cosmeticFix('#cig-filter-' + col.name);
            }
        }
        for (let col of this.columnsCigars) {
            if (col.filtering) {
                this.cosmeticFix('#cigar-filter-' + col.name);
            }
        }
        for (let col of this.columnsSnuff) {
            if (col.filtering) {
                this.cosmeticFix('#snuff-filter-' + col.name);
            }
        }
        for (let col of this.columnsPipe) {
            if (col.filtering) {
                this.cosmeticFix('#pipe-filter-' + col.name);
            }
        }
        for (let col of this.columnsChew) {
            if (col.filtering) {
                this.cosmeticFix('#chew-filter-' + col.name);
            }
        }
        for (let col of this.columnsRYO) {
            if (col.filtering) {
                this.cosmeticFix('#ryo-filter-' + col.name);
            }
        }
    }

    cosmeticFix(elementID: any) {
        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let filterInput = jQuery(elementID);
        filterInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

    setFocus(tobaccoType: string, selector: string) {

        let index = this.model.getTobaccoType(tobaccoType).addFooterInput7501(tobaccoType, selector);
        this.onChangeTable(this.filterinformation);
        this.setFocusBasedonSorting(tobaccoType, selector, index);
        this.hasChanges.emit(true);
    }

    setFocusonTop(selector, index) {
        window.setTimeout(function () {
            jQuery(`[id^=${selector}]`).first().focus();
        }, 500);
    }
    setFocusonBottom(selector, index) {
        window.setTimeout(function () {
            jQuery(`[id^=${selector}]`).last().focus();
        }, 500);
    }



    setFocusBasedonSorting(tobaccoType: string, selector: string, index: Number) {
        if (tobaccoType == 'Snuff') {
            if (this.sortOrderSnuffDesc) {
                this.snuffTable.gotoLastPage();
                this.setFocusonBottom(selector, index);
            }
            else {
                this.snuffTable.gotoFirstPage();
                this.sortOrderSnuffDesc = false;
                this.setFocusonTop(selector, index);
            }
        }

        if (tobaccoType == 'Chew') {
            if (this.sortOrderChewDesc) {
                this.chewTable.gotoLastPage();
                this.setFocusonBottom(selector, index);
            }
            else {
                this.chewTable.gotoFirstPage();
                this.setFocusonTop(selector, index);
                this.sortOrderChewDesc = false;

            }

        }
        if (tobaccoType == 'RollYourOwn') {
            if (this.sortOrderRyoDesc) {
                this.ryoTable.gotoLastPage();
                this.setFocusonBottom(selector, index);
            }
            else {
                this.ryoTable.gotoFirstPage();
                this.setFocusonTop(selector, index);
                this.sortOrderRyoDesc = false;
            }

        }
        if (tobaccoType == 'Pipe') {
            if (this.sortOrderPipeDesc) {
                this.pipeTable.gotoLastPage();
                this.setFocusonBottom(selector, index);
            }
            else {
                this.pipeTable.gotoFirstPage();
                this.setFocusonTop(selector, index);
                this.sortOrderRyoDesc = false;
            }

        }

        if (tobaccoType == 'Cigarettes') {
            if (this.sortOrderCigaretteDesc) {
                this.cigaretteTable.gotoLastPage();
                this.setFocusonBottom(selector, index);
            }
            else {
                this.cigaretteTable.gotoFirstPage();
                this.setFocusonTop(selector, index);
                this.sortOrderCigaretteDesc = false;
            }

        }

        if (tobaccoType == 'Cigars') {
            if (this.sortOrderCigarDesc) {
                this.cigarTable.gotoLastPage();
                this.setFocusonBottom(selector, index);
            }
            else {
                this.cigarTable.gotoFirstPage();
                this.setFocusonTop(selector, index);
                this.sortOrderCigarDesc = false;
            }
        }

    }

    private determineDesc(sortOrder: string): boolean {
        return (sortOrder === 'desc' && this.sortBy === 'qty') || (sortOrder !== 'desc' && this.sortBy !== 'qty');
    }

    public onSortOrderRyo(event, ttype) {
        this.sortOrderRyoDesc = this.determineDesc(event.toString());
    }

    public onSortOrderPipe(event, ttype) {
        this.sortOrderPipeDesc = this.determineDesc(event.toString());
    }

    public onSortOrderSnuff(event, ttype) {
        this.sortOrderSnuffDesc = this.determineDesc(event.toString());
    }

    public onSortOrderChew(event, ttype) {
        this.sortOrderChewDesc = this.determineDesc(event.toString());
    }
    public onSortOrderCigars(event, ttype) {
        this.sortOrderCigarDesc = this.determineDesc(event.toString());
    }
    public onSortOrderCig(event, ttype) {
        this.sortOrderCigaretteDesc = this.determineDesc(event.toString());
    }

    public onSortBy(event) {
        this.sortBy = event;
    }



    deleterow(tobaccoType: string, row: string, linenum: string) {

        this.model.getTobaccoType(tobaccoType).deleteFooterInput7501(linenum);
        this.hasChanges.emit(true);
        if (tobaccoType === "Cigarettes")
            this.refreshFormFilterObject(this.formFilterSet["cig"]);
        if (tobaccoType === "Cigars")
            this.refreshCigarFormFilterObject(this.formFilterSet["cigar"]);
        if (tobaccoType === "Snuff")
            this.refreshSnuffFormFilterObject(this.formFilterSet["snuff"]);
        if (tobaccoType === "Chew")
            this.refreshChewFormFilterObject(this.formFilterSet["chew"]);
        if (tobaccoType === "Pipe")
            this.refreshPipeFormFilterObject(this.formFilterSet["pipe"]);
        if (tobaccoType === "RollYourOwn")
            this.refreshRYOFormFilterObject(this.formFilterSet["ryo"]);

    }

    public checkForm7501InError(): boolean {
        var isError: boolean = false;
        var tTypes = this.model.getTobaccoTypes();
        for (var i in tTypes) {
            isError = tTypes[i].is7501Error();
            if (isError) break;
        }
        return isError;
    }

    public setGlobalError(isError: boolean) {

        let errorPresent: boolean = false;

        this.errorMsgs.forEach(function (item, index, array) {
            if (item.name === this.formName) {
                errorPresent = true;
                if (!isError) array.splice(index, 1);
            }
        }, this);

        if (isError && !errorPresent) this.errorMsgs.push(this.formErrorMsg);
    }

    public trackChanges() {
        this.hasChanges.emit(true);
    }

    updateFilters() {
        for (let col of this.columns) {
            let filterinfo = col.filtering;
            if (filterinfo) {
                if (filterinfo.columnName === "lineNum") {
                    this.formFilter.lineNumFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "qty") {
                    this.formFilter.qtyFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "taxAmt") {
                    this.formFilter.taxAmtFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "calTaxRate") {
                    this.formFilter.taxAmtFilter = filterinfo.filterString.trim();
                }
            }
        }
        for (let col of this.columnsCigars) {
            let filterinfo = col.filtering;
            if (filterinfo) {
                if (filterinfo.columnName === "lineNum") {
                    this.cigarFormFilter.lineNumFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "qty") {
                    this.cigarFormFilter.qtyFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "taxAmt") {
                    this.cigarFormFilter.taxAmtFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "taxDiff") {
                    this.cigarFormFilter.taxDiffFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "calTaxRate") {
                    this.cigarFormFilter.taxAmtFilter = filterinfo.filterString.trim();
                }
            }
        }
        for (let col of this.columnsSnuff) {
            let filterinfo = col.filtering;
            if (filterinfo) {
                if (filterinfo.columnName === "lineNum") {
                    this.snuffFormFilter.lineNumFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "qty") {
                    this.snuffFormFilter.qtyFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "taxAmt") {
                    this.snuffFormFilter.taxAmtFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "calTaxRate") {
                    this.snuffFormFilter.taxAmtFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "taxDiff") {
                    this.snuffFormFilter.taxDiffFilter = filterinfo.filterString.trim();
                }
            }
        }
        for (let col of this.columnsChew) {
            let filterinfo = col.filtering;
            if (filterinfo) {
                if (filterinfo.columnName === "lineNum") {
                    this.chewFormFilter.lineNumFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "qty") {
                    this.chewFormFilter.qtyFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "taxAmt") {
                    this.chewFormFilter.taxAmtFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "calTaxRate") {
                    this.chewFormFilter.taxAmtFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "taxDiff") {
                    this.chewFormFilter.taxDiffFilter = filterinfo.filterString.trim();
                }
            }
        }
        for (let col of this.columnsPipe) {
            let filterinfo = col.filtering;
            if (filterinfo) {
                if (filterinfo.columnName === "lineNum") {
                    this.pipeFormFilter.lineNumFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "qty") {
                    this.pipeFormFilter.qtyFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "taxAmt") {
                    this.pipeFormFilter.taxAmtFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "calTaxRate") {
                    this.pipeFormFilter.taxAmtFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "taxDiff") {
                    this.pipeFormFilter.taxDiffFilter = filterinfo.filterString.trim();
                }
            }
        }
        for (let col of this.columnsRYO) {
            let filterinfo = col.filtering;
            if (filterinfo) {
                if (filterinfo.columnName === "lineNum") {
                    this.ryoFormFilter.lineNumFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "qty") {
                    this.ryoFormFilter.qtyFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "taxAmt") {
                    this.ryoFormFilter.taxAmtFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "calTaxRate") {
                    this.ryoFormFilter.taxAmtFilter = filterinfo.filterString.trim();
                }
                if (filterinfo.columnName === "taxDiff") {
                    this.ryoFormFilter.taxDiffFilter = filterinfo.filterString.trim();
                }

            }
        }
    }

    isValidString(str) {
        return ((str != null) && (typeof str !== 'undefined') && (str.length >= 0));
    }

    public onChangeTable(filterinfo): void {

        this.filterinformation = filterinfo;

        if (filterinfo) {
            if (filterinfo.columnName === "lineNum") {
                this.formFilterSet[filterinfo.class].lineNumFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "taxAmt") {
                this.formFilterSet[filterinfo.class].taxAmtFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "qty") {
                this.formFilterSet[filterinfo.class].qtyFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "taxDiff") {
                this.formFilterSet[filterinfo.class].taxDiffFilter = filterinfo.filterString.trim();
            }
            if (filterinfo.columnName === "calTaxRate") {
                this.formFilterSet[filterinfo.class].taxRateFilter = filterinfo.filterString.trim();
            }

            if (filterinfo.refresh) {
                if (filterinfo.class === "cig")
                    this.refreshFormFilterObject(this.formFilterSet[filterinfo.class]);
                if (filterinfo.class === "cigar")
                    this.refreshCigarFormFilterObject(this.formFilterSet[filterinfo.class]);
                if (filterinfo.class === "snuff")
                    this.refreshSnuffFormFilterObject(this.formFilterSet[filterinfo.class]);
                if (filterinfo.class === "chew")
                    this.refreshChewFormFilterObject(this.formFilterSet[filterinfo.class]);
                if (filterinfo.class === "pipe")
                    this.refreshPipeFormFilterObject(this.formFilterSet[filterinfo.class]);
                if (filterinfo.class === "ryo")
                    this.refreshRYOFormFilterObject(this.formFilterSet[filterinfo.class]);
            }
        }

    }

    FilterSelectionChanged(event, filterColumnName) {
        if (filterColumnName === "type") {
            this.cigarFormFilter.cigarTypeFilter = event;
            this.refreshCigarFormFilterObject(this.cigarFormFilter);
        }
    }

    refreshFormFilterObject(formFilter: Form7501Filter) {
        this.cigFilterData = null;
        this.cigFilterData = new Form7501FilterData();
        this.cigFilterData.lineNumFilter = formFilter.lineNumFilter;
        this.cigFilterData.taxRateFilter = formFilter.taxRateFilter;
        this.cigFilterData.qtyFilter = formFilter.qtyFilter;
        this.cigFilterData.taxAmtFilter = formFilter.taxAmtFilter;
        this.cigFilterData.taxDiffFilter = formFilter.taxDiffFilter;
    }
    refreshCigarFormFilterObject(formFilter: Form7501Filter) {
        this.cigarFilterData = null;
        this.cigarFilterData = new Form7501FilterData();
        this.cigarFilterData.lineNumFilter = formFilter.lineNumFilter;
        this.cigarFilterData.taxRateFilter = formFilter.taxRateFilter;
        this.cigarFilterData.qtyFilter = formFilter.qtyFilter;
        this.cigarFilterData.taxAmtFilter = formFilter.taxAmtFilter;
        this.cigarFilterData.taxDiffFilter = formFilter.taxDiffFilter;
        this.cigarFilterData.cigarTypeFilter = formFilter.cigarTypeFilter;
    }
    refreshSnuffFormFilterObject(formFilter: Form7501Filter) {
        this.snuffFilterData = null;
        this.snuffFilterData = new Form7501FilterData();
        this.snuffFilterData.lineNumFilter = formFilter.lineNumFilter;
        this.snuffFilterData.taxRateFilter = formFilter.taxRateFilter;
        this.snuffFilterData.qtyFilter = formFilter.qtyFilter;
        this.snuffFilterData.taxAmtFilter = formFilter.taxAmtFilter;
        this.snuffFilterData.taxDiffFilter = formFilter.taxDiffFilter;
    }
    refreshChewFormFilterObject(formFilter: Form7501Filter) {
        this.chewFilterData = null;
        this.chewFilterData = new Form7501FilterData();
        this.chewFilterData.lineNumFilter = formFilter.lineNumFilter;
        this.chewFilterData.taxRateFilter = formFilter.taxRateFilter;
        this.chewFilterData.qtyFilter = formFilter.qtyFilter;
        this.chewFilterData.taxAmtFilter = formFilter.taxAmtFilter;
        this.chewFilterData.taxDiffFilter = formFilter.taxDiffFilter;
    }
    refreshPipeFormFilterObject(formFilter: Form7501Filter) {
        this.pipeFilterData = null;
        this.pipeFilterData = new Form7501FilterData();
        this.pipeFilterData.lineNumFilter = formFilter.lineNumFilter;
        this.pipeFilterData.taxRateFilter = formFilter.taxRateFilter;
        this.pipeFilterData.qtyFilter = formFilter.qtyFilter;
        this.pipeFilterData.taxAmtFilter = formFilter.taxAmtFilter;
        this.pipeFilterData.taxDiffFilter = formFilter.taxDiffFilter;
    }
    refreshRYOFormFilterObject(formFilter: Form7501Filter) {
        this.ryoFilterData = null;
        this.ryoFilterData = new Form7501FilterData();
        this.ryoFilterData.lineNumFilter = formFilter.lineNumFilter;
        this.ryoFilterData.taxRateFilter = formFilter.taxRateFilter;
        this.ryoFilterData.qtyFilter = formFilter.qtyFilter;
        this.ryoFilterData.taxAmtFilter = formFilter.taxAmtFilter;
        this.ryoFilterData.taxDiffFilter = formFilter.taxDiffFilter;
    }
}

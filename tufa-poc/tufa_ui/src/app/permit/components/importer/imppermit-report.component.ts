import { AfterContentChecked, AfterViewInit, Component, HostListener, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { mergeMap } from '../../../../../node_modules/rxjs/operators';
import { CompanyService } from '../../../company/services/company.service';
import { DownloadSerivce } from '../../../shared/service/download.service';
import { ErrorHandlingService } from '../../../shared/service/error.handling.service';
import { FormsCalculationService } from '../../../shared/service/forms.calculation.service';
import { CommentModel } from '../../models/comment/comment.model';
import { Document } from '../../models/document.model';
import { PermitImpReport } from '../../models/importer/permit.imp.report.model';
import { TobaccoType } from '../../models/imptobacco-type.model';
import { MissingForms } from '../../models/missingForms.model';
import { PeriodActivity } from '../../models/period-activity.model';
import { PermitPeriod } from '../../models/permit-period.model';
import { MonthlyReportCommentService } from '../../services/monthly.report.comment.service';
import { MonthlyReportEventService } from '../../services/monthly.report.event.service';
import { ComponentCanDeactivate } from '../../services/pending-changes.guard';
import { PermitPeriodService } from '../../services/permit-period.service';
import { PermitImpReportService } from '../../services/permit.imp.report.service';
import { ShareDataService } from './../../../core/sharedata.service';
import { AuthService } from './../../../login/services/auth.service';


declare var jQuery: any;

@Component({
    selector: '[imppermit-report]',
    templateUrl: './imppermit-report.template.html',
    styleUrls: ['./imppermit-report.style.scss'],
    providers: [PermitPeriodService, CompanyService],
    encapsulation: ViewEncapsulation.None
})

// Two file uploader examples:
// Good example code is available in this website
// http://valor-software.com/ng2-file-upload/
// Like this one better
// http://ng2-uploader.com/docs
export class ImpPermitPeriodPage implements OnInit, AfterContentChecked, AfterViewInit, OnDestroy, ComponentCanDeactivate {
    canDeactivate(flag?: boolean): Observable<boolean> | boolean {
        if (this.authService.loggedIn()) {
            if (this.reportChanged)
                return false;
        }
        return true;
    }

    // @HostListener allows us to also guard against browser refresh, close, etc.
    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (!this.canDeactivate()) {
            $event.returnValue = "Save changes before you leave this page";
        }
    }

    reportChanged: boolean = false;
    periodId: any;
    permitId: any;
    paramsSub: any;
    companyId: any;
    ein: any;
    router: Router;
    reconStatusData: any = {};
    alerts: Array<Object>;
    shareData: ShareDataService;


    permitPeriod = new PermitPeriod();
    periodAct = new PeriodActivity();
    periodStatus: string;
    breadcrumb: string;
    qatabfrom: string;
    isRecon: boolean;
    busy: Subscription;
    chbxbusy: Subscription;
    downloading: Subscription;
    saveReportBusy: Subscription;

    selectedDocument: Document;
    newDoc: Document = new Document();



    data: any[] = [];
    zeroReportFlag: boolean = true;
    ttmodel = new TobaccoType();
    docId: any;
    showConfirmDeletePopup: boolean = false;
    showEditDocumentPopup: boolean = false;

    cmpycomments: string;
    permitComments: CommentModel[];
    zerocomments: any[] = [];

    // Recon params
    previousRoute: string;
    quarter: number;
    fiscalYear: number;
    createdDate: string;
    submitted: string;
    cigarsubmitted: string;
    months: string[];
    isZeroFlag: boolean;
    is3852CheckedatInit: boolean;

    // Missing form params
    mscbp7501: any;
    msfda3852: boolean = false;
    msttb5220: boolean = false;
    missingFileErrorMsgs = [];
    reportMissingForms: MissingForms[] = [];
    formMap = { 'FDA 3852': "3852", 'TTB 5220.6': "2206", 'CBP 7501': "7501" };
    reverseFormMap = { '3852': this.msfda3852, '2206': this.msttb5220, '7501': this.mscbp7501 };
    eventMap = { 'true': 'true', 'false': 'fals', 'Select': null, 'partial': "PART", 'all': "ALL" };
    reverseEventMap = { 'true': true, 'fals': false, 'PART': 'partial', 'ALL': "all" };

    permitImpReport: PermitImpReport = new PermitImpReport(this.monthlyReportService);

    globalFormError = {
        name: 'glbError',
        msg: '<span class="fw-semi-bold">Error: </span>This form will be saved as Error',
        show: false,
        type: 'warning'
    };

    globalFormSuccessMsg = {
        name: 'glbSuccessMsg',
        msg: '<span class="fw-semi-bold">Complete: </span> This form will be saved as Complete',
        show: false,
        type: 'success'
    };

    globalFormNotStartedMsg = {
        name: 'glbNotStarted',
        msg: '<span class="fw-semi-bold">Not Started: </span> This form will be saved as Not Started',
        show: false,
        type: 'warning'
    };

    reportInfoErrorMsg = {
        name: 'notStarted',
        msg: 'No report information was provided.',
        type: 'warning',
        show: false
    }

    documentErrorMsg = {
        name: 'document',
        msg: 'No documentation was provided',
        type: 'warning',
        show: false
    };

    errorMsgs = [];
    errorMsgsNotStarted = [];
    isFormEmpty: boolean = true;

    FORM_7501 = "7501";
    FORM_3852 = "3852";
    FORM_5220 = "5220.6";

    subcriptions: Subscription[] = [];

    constructor(router: Router,
        private _permitPeriodService: PermitPeriodService,
        private permitImpReportService: PermitImpReportService,
        private activatedRoute: ActivatedRoute,
        private companyService: CompanyService,
        private errorHandlingService: ErrorHandlingService,
        private formsCalculationService: FormsCalculationService,
        private _shareData: ShareDataService,
        private monthlyReportService: MonthlyReportEventService,
        private monthlyReportCommentService: MonthlyReportCommentService,
        private authService: AuthService,
        private downloadService: DownloadSerivce
    ) {
        this.router = router;
        this.shareData = _shareData;
    }

    ngOnInit() {
        this.is3852CheckedatInit = false;
        this.periodAct.companyName = this.shareData.companyName;
        this.periodAct.permitNum = this.shareData.permitNum;
        this.periodAct.period = this.shareData.period;
        this.periodId = this.shareData.periodId;
        this.permitId = this.shareData.permitId;
        this.periodStatus = this.shareData.periodStatus;
        this.companyId = this.shareData.companyId;
        if(this.shareData.originalEIN){
            this.ein = this.shareData.originalEIN;
            this.companyId = 0;
        }else{
            this.ein = this.shareData.ein;
        }
        

        this.getcompanycomments();
        this.getZeroReportscomments();
        this.getParamsForReconNavigation();
        this.shareData.reportType = jQuery("#per-type").text();
        this.loadPermitPeriod();
        this.loadDocs(this.periodId, this.permitId);
        this.loadFormDetails();
        this.loadMissingForms();

        // missing form defaults
        this.mscbp7501 = 'Select';

        /** comment related event hook ups */

        let permitPeriod = new PermitPeriod();
        permitPeriod.periodId = this.periodId;
        permitPeriod.permitId = this.permitId;
        this.monthlyReportService.setPermitPeriod(permitPeriod);
        this.monthlyReportService.childViewSourceLoaded$.subscribe(
            evnt => {
                this.monthlyReportService.triggerPermitPeriodUpdate();
            }
        );

        this.monthlyReportService.commentAddedSource$.subscribe(
            comment => {
                /**
                 *  set the formId from the newly created comment
                 *  assuming the formId does not exist for the form itself
                 *  and gets created first when a comment is added to the form comments grid.
                 */

                this.ttmodel.setFormId(String(comment.formId), comment.formTypeCd, comment.tobaccoClassId);
                /** add the comment to the comment grid */
                this.ttmodel.addComment(comment);
                this.getZeroReportscomments();
            }
        )
    }

    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }

        if (this.chbxbusy) {
            this.chbxbusy.unsubscribe();
        }

        if (this.downloading) {
            this.downloading.unsubscribe();
        }

        if (this.paramsSub) {
            this.paramsSub.unsubscribe();
        }

        // tslint:disable-next-line:one-line
        // tslint:disable-next-line:forin
        for (let i in this.subcriptions) {
            let subcription = this.subcriptions[i];
            subcription.unsubscribe();
        }

        this.enableLinkAuth(false);

    }

    ngAfterViewInit() {
        if (this.qatabfrom === "qa-reconreport" || this.qatabfrom === "ca-reconreport" || this.qatabfrom === "at-reconreport") {
            jQuery('html, body, .content-wrap').animate({
                /*Scroll to the top of element in respect to the
                 '.content-wrap' since the value of .top will depend 
                 on scroll position*/
                scrollTop: jQuery("#rpt-method").offset().top - jQuery('.content-wrap').offset().top
            }, 2000);
        }
    }

    ngAfterContentChecked() {
        this.isFormEmpty = this.checkEmptyReport('impforms');
        this.setGlobalError(this.isReportInfoError(), this.errorMsgsNotStarted,
            this.reportInfoErrorMsg);
        this.checkDocumentError();
        this.updateFormErrors();
        this.updateFormCalculations();
    }

    checkDocumentError() {
        this.setGlobalError(this.isDocumentError(), this.errorMsgsNotStarted,
            this.documentErrorMsg);
        this.setGlobalError(this.isDocumentError(), this.errorMsgs, this.documentErrorMsg);
    }

    setSelectedDocument(doc: Document, mode: string) {
        this.selectedDocument = Object.assign({}, doc);
        if (mode === 'edit') {
            this.showEditDocumentPopup = true;
        } else if (mode === 'delete') {
            this.showConfirmDeletePopup = true;
        }
    }

    downloadSelectedDocument(doc: Document) {
        // this.selectedDocument = Object.assign({}, doc);
        this.downloadAttach(doc);
    }

    downloadAttach(doc: Document) {
        this.downloading = this._permitPeriodService.downloadAttachment(doc.documentId).subscribe(
            data => {
                if (data) {
                    this.download(atob(data.reportPdf), data.docDesc, "application/pdf");
                }
            });
    }
    public isDocumentError(): boolean {
        if (this.data && this.data.length > 0) {
            return false;
        }
        return true;
    }

    public isReportInfoError(): boolean {
        return (!this.ttmodel.isTTSelected || this.isFormEmpty) ? true : false;
    }

    checkEmptyReport(parentDivId: string): boolean {
        if (!parentDivId) {
            return true;
        }

        let searchString = "#" + parentDivId + " input:text";
        let inputs: any[] = jQuery(searchString);
        let cntInput = 0;
        let empty = jQuery(searchString).filter(function () {
            ++cntInput;
            return jQuery.trim(this.value) === "";
        });

        return (empty.length === cntInput) ? true : false;
    }

    selectAllTobaccooTypeCheckboxSelected(event): void {
        if (event) {
            this.checkAllTobaccooTypes();
        } else {
            this.uncheckAllTobaccooTypes();
        }
    }

    private checkAllTobaccooTypes(): void {
        this.ttmodel.showCigarettes = true;
        this.ttmodel.showCigars = true;
        this.ttmodel.showChews = true;
        this.ttmodel.showPipes = true;
        this.ttmodel.showRollYourOwn = true;
        this.ttmodel.showSnuffs = true;
        this.callCheckboxSelectedForAllTobaccooType();
    }

    private uncheckAllTobaccooTypes(): void {
        this.ttmodel.showCigarettes = false;
        this.ttmodel.showCigars = false;
        this.ttmodel.showChews = false;
        this.ttmodel.showPipes = false;
        this.ttmodel.showRollYourOwn = false;
        this.ttmodel.showSnuffs = false;
        this.callCheckboxSelectedForAllTobaccooType();
    }

    private callCheckboxSelectedForAllTobaccooType(): void {
        this.checkboxSelected('Cigarettes');
        this.checkboxSelected('Cigars');
        this.checkboxSelected('Snuff');
        this.checkboxSelected('Chew');
        this.checkboxSelected('Pipe');
        this.checkboxSelected('RollYourOwn');
    }

    checkboxSelected(ttName: string) {

        let ttypeClicked = this.ttmodel.getTobaccoType(ttName);
        this.ttmodel.addTobaccoType(ttName);

        // /** trigger event to delete comments if tobaccotype is unchecked */

        // /**
        //  * If there is still a tobaccotype selected then just delete comments
        //  *  for 7501 form for the tobaccotype unchecked else if no tobaccotype is
        //  * selected on the monthly report delete comments on all the forms.
        // */

        let formIdsArry = [];

        if (this.ttmodel.isTTSelected) {
            // /**check if tobacco type was unchecked */
            let ttype = this.ttmodel.getTobaccoType(ttName);
            // /** if tobacco type has been unchecked */
            if (!ttype) {
                let frm7501 = this.ttmodel.getFormId(this.FORM_7501, ttypeClicked.tobaccoClassId);
                if (frm7501) formIdsArry.push(frm7501);
            }
        } else if (ttypeClicked) {

            let frm7501 = this.ttmodel.getFormId(this.FORM_7501, ttypeClicked.tobaccoClassId);
            let frm3852 = this.ttmodel.getFormId(this.FORM_3852);
            let frm5220 = this.ttmodel.getFormId(this.FORM_5220);

            if (frm7501) formIdsArry.push(frm7501);
            if (frm3852) formIdsArry.push(frm3852);
            if (frm5220) formIdsArry.push(frm5220);
        }

        if (formIdsArry.length > 0) {
            this.chbxbusy = this.monthlyReportCommentService.deleteMonthlyReportComments(formIdsArry).subscribe(
                data => {
                    if (data) {
                        /** delete comments locally */
                        this.ttmodel.deleteComments(this.FORM_7501, Number(ttypeClicked.tobaccoClassId));
                        this.ttmodel.deleteComments(this.FORM_3852);
                        this.ttmodel.deleteComments(this.FORM_5220);
                    }

                }
            );
        }
    }


    //    /** delete comments on the forms that were not saved to the database side **/
    //     /** This method will be called if the user adds comments and navigates to some other page
    //      *  without saving the report
    //      **/
    deleteOrphanedComments() {

        let formIdsArry = [];
        let ttClassIds = [];
        if (this.ttmodel.isTTSelected) {

            if (!this.ttmodel.showChews) {
                let tt = this.ttmodel.getTobaccoTypeConst("Chew");
                let comments = this.ttmodel.getComments(this.FORM_7501, tt.tobaccoSubTypeName, tt.tobaccoClassId);
                if (comments && comments.length > 0) {
                    formIdsArry.push(comments[0].formId);
                    ttClassIds.push(tt.tobaccoClassId);
                }
            }

            if (!this.ttmodel.showSnuffs) {
                let tt = this.ttmodel.getTobaccoTypeConst("Snuff");
                let comments = this.ttmodel.getComments(this.FORM_7501, tt.tobaccoSubTypeName, tt.tobaccoClassId);
                if (comments && comments.length > 0) {
                    formIdsArry.push(comments[0].formId);
                    ttClassIds.push(tt.tobaccoClassId);
                }

            }

            if (!this.ttmodel.showRollYourOwn) {
                let tt = this.ttmodel.getTobaccoTypeConst("RollYourOwn");
                let comments = this.ttmodel.getComments(this.FORM_7501, tt.tobaccoSubTypeName, tt.tobaccoClassId);
                if (comments && comments.length > 0) {
                    formIdsArry.push(comments[0].formId);
                    ttClassIds.push(tt.tobaccoClassId);
                }
            }

            if (!this.ttmodel.showPipes) {
                let tt = this.ttmodel.getTobaccoTypeConst("Pipe");
                let comments = this.ttmodel.getComments(this.FORM_7501, tt.tobaccoSubTypeName, tt.tobaccoClassId);
                if (comments && comments.length > 0) {
                    formIdsArry.push(comments[0].formId);
                    ttClassIds.push(tt.tobaccoClassId);
                }
            }

            if (!this.ttmodel.showCigarettes) {
                let tt = this.ttmodel.getTobaccoTypeConst("Cigarettes");
                let comments = this.ttmodel.getComments(this.FORM_7501, tt.tobaccoSubTypeName, tt.tobaccoClassId);
                if (comments && comments.length > 0) {
                    formIdsArry.push(comments[0].formId);
                    ttClassIds.push(tt.tobaccoClassId);
                }
            }

            if (!this.ttmodel.showCigars) {
                let tt = this.ttmodel.getTobaccoTypeConst("Cigars");
                let comments = this.ttmodel.getComments(this.FORM_7501, tt.tobaccoSubTypeName, tt.tobaccoClassId);
                if (comments && comments.length > 0) {
                    formIdsArry.push(comments[0].formId);
                    ttClassIds.push(tt.tobaccoClassId);
                }
            }
        } else {
            let formComments = this.ttmodel.formComments;

            let comments3852 = this.ttmodel.getComments(this.FORM_3852);
            if (comments3852 && comments3852.length > 0)
                formIdsArry.push(comments3852[0].formId);

            let comments5220 = this.ttmodel.getComments(this.FORM_5220);
            if (comments5220 && comments5220.length > 0)
                formIdsArry.push(comments5220[0].formId);

            let comments7501 = formComments[this.FORM_7501];
            // tslint:disable-next-line:forin
            for (let i in comments7501) {
                // tslint:disable-next-line:one-line
                let comments = comments7501[i];
                if (comments && comments.length > 0) {
                    formIdsArry.push(comments[0].formId);
                    ttClassIds.push(comments[0].tobaccoClassId);
                }
            }

            //clear form ids 
            this.ttmodel.clearFormIds();
        }

        if (formIdsArry.length > 0)
            this.monthlyReportCommentService.deleteMonthlyReportOrphanedComments(formIdsArry).subscribe(
                data => {
                    //    /** delete comments locally **/
                    if (this.ttmodel.isTTSelected) {
                        // tslint:disable-next-line:forin
                        for (let i in ttClassIds)
                            this.ttmodel.deleteComments(this.FORM_7501, ttClassIds[i]);

                    } else {
                        // tslint:disable-next-line:forin
                        for (let i in ttClassIds)
                            this.ttmodel.deleteComments(this.FORM_7501, ttClassIds[i]);
                        this.ttmodel.deleteComments(this.FORM_3852);
                        this.ttmodel.deleteComments(this.FORM_5220);
                    }
                });
    }

    loadPermitPeriod() {
        this._permitPeriodService.loadPermitPeriod(this.periodId, this.permitId).subscribe(
            data => {
                this.permitPeriod.fillFromJSON(JSON.stringify(data));
                this.isZeroFlag = this.permitPeriod.zeroFlag;
            }
        );
    }

    loadMissingForms() {
        this._permitPeriodService.getMissingForms(this.permitId, this.periodId).subscribe(
            data => {
                this.reportMissingForms = data;
                for (let i in data) {
                    if (data[i].formTypeCd === "3852") {
                        this.msfda3852 = this.reverseEventMap[data[i].defectStatusCd.trim()];
                        this.is3852CheckedatInit = this.msfda3852;
                        this.checkMissingForms(this.msfda3852, "FDA 3852");
                    } else if (data[i].formTypeCd === "7501") {
                        this.mscbp7501 = this.reverseEventMap[data[i].defectStatusCd.trim()];
                        this.checkMissingForms(this.mscbp7501, "CBP 7501");
                    } else if (data[i].formTypeCd === "2206") {
                        this.msttb5220 = this.reverseEventMap[data[i].defectStatusCd.trim()];
                        this.checkMissingForms(this.msttb5220, "TTB 5220.6");
                    }
                }
            }
        );
    }

    documentsGridRefresh(flag: boolean, mode: string) {
        if (flag) {
            this.loadDocs(this.periodId, this.permitId);
        }

        if (mode === 'edit') {
            this.showEditDocumentPopup = false;
        } else if (mode === 'delete') {
            this.showConfirmDeletePopup = false;
        }
    }

    savePermitPeriod() {
        this.savePermitImpReportFormData(this.getGlobalReportStatus());
    }

    loadDocs(periodId: string, permitId: string) {
        this.busy = this._permitPeriodService.loadDocs(periodId, permitId).subscribe(
            data => {
                let nd = data;
                let newData = [];
                for (let i = 0; i < nd.length; i++) {
                    newData.push(nd[i]);
                }
                this.data = newData;
            });
    }

    public setGlobalError(isError: boolean, errorMsgArry: any[], errorMsgObj) {

        let errorPresent: boolean = false;

        errorMsgArry.forEach(function (item, index, array) {
            if (item.name === errorMsgObj.name) {
                errorPresent = true;
                if (!isError) array.splice(index, 1);
            }
        }, this);

        if (isError && !errorPresent) errorMsgArry.push(errorMsgObj);
    }

    public getGlobalReportStatus() {
        if (this.permitPeriod.zeroFlag)
            return (this.isDocumentError() || this.missingFileErrorMsgs.length > 0) ? "Error" : "Complete";
        if ((this.errorMsgs.length > 0 || this.missingFileErrorMsgs.length > 0) && !this.isFormEmpty)
            return "Error";
        if (this.errorMsgs.length === 0 && !this.isFormEmpty && this.missingFileErrorMsgs.length === 0)
            return "Complete";
        if (this.isFormEmpty)
            return "Not Started";
    }

    public updateFormErrors() {
        this.errorHandlingService.impFormChanged(null);
    }

    public updateFormCalculations() {
        this.formsCalculationService.impformChanged();
    }

    toCompanyDetails(companyId: any) {
        this.setParamsForReconNavigation();
        this.router.navigate(["/app/company/details", companyId]);
    }

    toPermitHistory(permitId: any) {
        this.setParamsForReconNavigation();
        this.router.navigate(["/app/permit/history", permitId]);
    }

    toAssessments() {
        this.setParamsForReconNavigation();
        this.router.navigate(["/app/assessments"]);
    }

    toQuarterlyAssessment() {
        this.setParamsForReconNavigation();
        this.router.navigate(["/app/assessments/quarterly"]);
    }

    toCigarAssessment() {
        this.setParamsForReconNavigation();
        this.router.navigate(["/app/assessments/cigars"]);
    }

    toAnnualTrueup() {
        this.setParamsForReconNavigation();
        this.router.navigate(["/app/assessments/annual"]);
    }

    toCompareDetails() {
        //this.setParamsForReconNavigation();
        if (this.shareData.detailsSrc.indexOf('noncigar') != -1)
            this.router.navigate(["/app/assessments/comparenoncigar"]);
        else if (this.shareData.detailsSrc.indexOf('cigar') != -1)
            this.router.navigate(["/app/assessments/comparecigar"]);
    }

    onReconApprovedEvent(isApproved: boolean) {
        if (this.breadcrumb === "ca-reconreport") {
            this.toCigarAssessment();
        } else if (this.breadcrumb === "qa-reconreport") {
            this.toQuarterlyAssessment();
        } else {
            this.toAnnualTrueup();
        }
    }

    toFileMgmt() {
        this.setParamsForReconNavigation();
        this.router.navigate(["app/permit-mgmt/address-mgmt", { docId: this.docId }]);
    }

    getcompanycomments() {

        if (this.companyId !== 0) {
            this.companyService.getCompany(this.companyId).subscribe(data => {
                if (data) {
                    this.cmpycomments = (<any>data).companyComments;
                }
            });
        }else{
            this.companyService.getCompanyEIN(this.ein).subscribe(data => {
                if (data) {
                    this.cmpycomments = (<any>data).companyComments;
                    this.companyId = (<any>data).companyId;
                }
            });
        }
    }

    getZeroReportscomments() {
        //  if (this.permitPeriod.zeroFlag){
        this.monthlyReportCommentService.getZeroReportComment(this.permitId, this.periodId).subscribe(data => {
            if (data) {
                this.zerocomments = data;
            }
        });
        // }
    }

    download(data: any, strFileName: any, strMimeType: any) {

        let self: any = window, // this script is only for browsers anyway...
            defaultMime = "application/octet-stream", // this default mime also triggers iframe downloads
            mimeType = strMimeType || defaultMime,
            payload = data,
            url = !strFileName && !strMimeType && payload,
            anchor = document.createElement("a"),
            toString = function (a) { return String(a); },
            myBlob = (self.Blob || self.MozBlob || self.WebKitBlob || toString),
            fileName = strFileName || "download",
            blob,
            reader;
        myBlob = myBlob.call ? myBlob.bind(self) : Blob;

        if (String(this) === "true") { // reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
            payload = [payload, mimeType];
            mimeType = payload[0];
            payload = payload[1];
        }


        // go ahead and download dataURLs right away
        if (/^data\:[\w+\-]+\/[\w+\-\.]+[,;]/.test(payload)) {

            if (payload.length > (1024 * 1024 * 1.999) && myBlob !== toString) {
                payload = dataUrlToBlob(payload);
                mimeType = payload.type || defaultMime;
            } else {
                return navigator.msSaveBlob ?  // IE10 can't do a[download], only Blobs:
                    navigator.msSaveBlob(dataUrlToBlob(payload), fileName) :
                    saver(payload, false); // everyone else can save dataURLs un-processed
            }

        } else {// not data url, is it a string with special needs?
            if (/([\x80-\xff])/.test(payload)) {
                let i = 0, tempUiArr = new Uint8Array(payload.length), mx = tempUiArr.length;
                for (i; i < mx; ++i) tempUiArr[i] = payload.charCodeAt(i);
                payload = new myBlob([tempUiArr], { type: mimeType });
            }
        }
        blob = payload instanceof myBlob ?
            payload :
            new myBlob([payload], { type: mimeType });


        function dataUrlToBlob(strUrl) {
            let parts = strUrl.split(/[:;,]/),
                type = parts[1],
                decoder = parts[2] === "base64" ? atob : decodeURIComponent,
                binData = decoder(parts.pop()),
                mx = binData.length,
                i = 0,
                uiArr = new Uint8Array(mx);

            for (i; i < mx; ++i) uiArr[i] = binData.charCodeAt(i);

            return new myBlob([uiArr], { type: type });
        }

        function saver(url: any, winMode: any) {

            if ('download' in anchor) { // html5 A[download]
                anchor.href = url;
                anchor.setAttribute("download", fileName);
                anchor.className = "download-js-link";
                anchor.innerHTML = "downloading...";
                anchor.style.display = "none";
                document.body.appendChild(anchor);
                setTimeout(function () {
                    anchor.click();
                    document.body.removeChild(anchor);
                    if (winMode === true) { setTimeout(function () { self.URL.revokeObjectURL(anchor.href); }, 250); }
                }, 66);
                return true;
            }

            // handle non-a[download] safari as best we can:
            if (/(Version)\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//.test(navigator.userAgent)) {
                if (/^data:/.test(url)) url = "data:" + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
                if (!window.open(url)) { // popup blocked, offer direct download:
                    // tslint:disable-next-line:max-line-length
                    if (confirm("Displaying New Document\n\nUse Save As... to download, then click back to return to this page.")) { location.href = url; }
                }
                return true;
            }

            // do iframe dataURL download (old ch+FF):
            var f = document.createElement("iframe");
            document.body.appendChild(f);

            if (!winMode && /^data:/.test(url)) { // force a mime that will download:
                url = "data:" + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
            }
            f.src = url;
            setTimeout(function () { document.body.removeChild(f); }, 333);

        }// end saver


        if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or URL)
            return navigator.msSaveBlob(blob, fileName);
        }

        if (self.URL) { // simple fast and modern way using Blob and URL:
            saver(self.URL.createObjectURL(blob), true);
        } else {
            // handle non-Blob()+non-URL browsers:
            if (typeof blob === "string" || blob.constructor === toString) {
                try {
                    return saver("data:" + mimeType + ";base64," + self.btoa(blob), false);
                } catch (y) {
                    return saver("data:" + mimeType + "," + encodeURIComponent(blob), false);
                }
            }

            // Blob but not URL support:
            reader = new FileReader();
            reader.onload = function (e) {
                saver(this.result, false);
            };
            reader.readAsDataURL(blob);
        }
        return true;
    };

    setParamsForReconNavigation() {
        let localSrc = this.shareData.detailsSrc;
        let localflag = this.shareData.acceptanceFlag;
        let localtype = this.shareData.permitType;
        let localClass = this.shareData.tobaccoClass;
        let localCompany = this.shareData.companyName;
        let filetab = this.shareData.filemgmttab;
        let originalEIN = this.shareData.originalEIN;
        let ein = this.shareData.ein;
        this.shareData.reset();
        this.shareData.filemgmttab = filetab;
        this.shareData.companyId = this.companyId;
        this.shareData.periodId = this.periodId;
        this.shareData.permitId = this.permitId;
        this.shareData.companyName = localCompany;
        this.shareData.acceptanceFlag = localflag;
        this.shareData.permitType = localtype;
        this.shareData.tobaccoClass = localClass;
        this.shareData.detailsSrc = localSrc;
        this.shareData.previousRoute = this.previousRoute;
        this.shareData.breadcrumb = this.breadcrumb;
        this.shareData.qatabfrom = this.qatabfrom;
        this.shareData.quarter = this.quarter;
        this.shareData.fiscalYear = this.fiscalYear;
        this.shareData.createdDate = this.createdDate;
        this.shareData.submitted = this.submitted;
        this.shareData.cigarsubmitted = this.cigarsubmitted;
        this.shareData.months = this.months;
        this.shareData.docId = this.docId;
        this.shareData.originalEIN = originalEIN;
        this.shareData.ein = ein;
    }

    getParamsForReconNavigation() {
        this.previousRoute = this.shareData.previousRoute;
        this.breadcrumb = this.shareData.breadcrumb;
        this.qatabfrom = this.shareData.qatabfrom;
        this.quarter = this.shareData.quarter;
        this.fiscalYear = this.shareData.fiscalYear;
        this.submitted = this.shareData.submitted;
        this.cigarsubmitted = this.shareData.cigarsubmitted;
        this.createdDate = this.shareData.createdDate;
        this.months = this.shareData.months;
        this.isRecon = this.shareData.isRecon;
        this.docId = this.shareData.docId;
    }

    enableLinkAuth(flag) {
        this._permitPeriodService.setLinkAuthentication(flag);
    }
    // /**** Forms save and update */

    public loadFormDetails() {
        // get the forms details
        this.ttmodel.permitId = this.permitId;
        this.ttmodel.periodId = this.periodId;
        this.ttmodel.period = this.periodAct.period;
        this.ttmodel.permitNumber = this.periodAct.permitNum;


        this.chbxbusy = this.permitImpReportService.getPermitImpReportDetails(this.ttmodel).subscribe(
            data => {
                if (data) {
                    this.permitImpReport.populateFormDetails(data, this.ttmodel);
                    let commentArray: CommentModel[] = (<any>data).permit.permitComments;
                    this.permitComments = [];
                    if (commentArray && commentArray.length > 0) {
                        commentArray.forEach(comment => {
                            if (!comment.resolveComment) this.permitComments.push(comment);
                        })
                    }
                    if (this.permitComments.length == 0) this.permitComments = null;
                    this.deleteOrphanedComments();
                }
            });

        if (this.chbxbusy)
            this.subcriptions.push(this.chbxbusy);
    }


    public savePermitImpReportFormData(status) {
        this.reportChanged = false;
        let ppClone = (JSON.parse(JSON.stringify(this.permitPeriod)));
        ppClone.zeroFlag = ppClone.zeroFlag ? 'Y' : 'N';
        //console.log(" Saving report as Zero report " + ppClone.zeroFlag + " periodId " + ppClone.periodId + " permitId " + ppClone.permitId)
        // self.busy = self._permitPeriodService.savePermitPeriod(ppClone).subscribe(data => {
        //     if (data) {
        //         console.log(" Updated permit period ");
        //     }
        // });  


        this._permitPeriodService.saveMissingForms(this.reportMissingForms, this.periodId, this.permitId).pipe(mergeMap(
            () => this._permitPeriodService.savePermitPeriod(ppClone))
        ).subscribe(
            data => console.debug(" Updated zero flag and missing forms"),
        );

        let permitImpReportFormData = {};
        permitImpReportFormData["permitId"] = this.permitId;
        permitImpReportFormData["periodId"] = this.periodId;
        permitImpReportFormData["status"] = status;

        if (this.permitPeriod.zeroFlag) {
            permitImpReportFormData["submittedForms"] = [];
            this.ttmodel.clearFormIds();
        } else if (this.ttmodel.getTobaccoTypes() && this.ttmodel.getTobaccoTypes().length > 0) {
            permitImpReportFormData["submittedForms"] = this.ttmodel.getFormDetails().reverse();
        } else {
            this.ttmodel.clearFormIds();
        }

        this.periodStatus = status;
        let is3852modified = false;

        if ((this.is3852CheckedatInit != this.msfda3852) || (!this.isZeroFlag && this.permitPeriod.zeroFlag)) {
            is3852modified = true;
        }

        permitImpReportFormData["is3852modified"] = is3852modified;
        // this._permitPeriodService.saveMissingForms(this.reportMissingForms, this.periodId, this.permitId).subscribe(missingForms => { });
        this.saveReportBusy = this.permitImpReportService.savePermitImpReportDetails(permitImpReportFormData).subscribe(
            data => {
                if (data) {
                    this.permitImpReport.populateForms((<any>data).submittedForms);
                    this._permitPeriodService.getPermitReportReconStatus(this.permitId, this.periodId)
                        .subscribe(
                            resp => {
                                if (resp)
                                    this.reconStatusData = resp;
                                jQuery("html, body, .content-wrap").animate({ scrollTop: 0 }, "slow");
                            });
                }
            });

        if (this.saveReportBusy)
            this.subcriptions.push(this.saveReportBusy);

        return false;
    }

    checkMissingForms(event: any, formTitle: string, update?: boolean) {
        if (update) this.reportChanged = true;

        let missingFormsError = {
            name: 'document',
            msg: 'Missing ' + formTitle + ' form(s)',
            type: 'warning',
            show: false
        };

        let formMissing: MissingForms = {
            periodId: this.periodId,
            permitId: this.permitId,
            formTypeCd: this.formMap[formTitle],
            defectDesc: null,
            defectStatusCd: this.eventMap[event]
        };

        this.missingFileErrorMsgs = this.missingFileErrorMsgs.filter(function (el) {
            return el.msg !== missingFormsError.msg;
        });

        this.reportMissingForms = this.reportMissingForms.filter(function (el) {
            return el.formTypeCd !== formMissing.formTypeCd;
        });

        if (event && event !== "Select") {
            this.reportMissingForms.push(formMissing);
            this.missingFileErrorMsgs.push(missingFormsError);
        }

        this.alerts = [
            {
                type: 'warning',
                msg: '<span class="fw-semi-bold">Warning:</span> Missing form(s)'
            }
        ];
    }

    trackChanges(flag: boolean) {
        this.reportChanged = flag;
    }

    downloadForm(form: string, tobaccoType: number = 0) {
        // Get export data
        let fileName = this.shareData.permitNum.replace(/[A-z]{2}/g, "$&-") + "_" + this.shareData.period.replace(/ /g, "_") + "_ERROR_" + form + ".csv";
        this._permitPeriodService.getFormExport(this.permitId, this.periodId, form, tobaccoType).subscribe(
            data => {
                if (data) {
                    this.downloadService.download(data.csv, fileName, "csv/txt");
                }
            }
        )
    }
}


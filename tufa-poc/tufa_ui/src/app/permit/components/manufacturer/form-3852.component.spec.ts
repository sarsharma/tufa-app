/*
@author : Deloitte
this is Component for Manufacturer form 3852.
*/
import { ComponentFixture, async, inject, TestBed } from '@angular/core/testing';
import { Form3852 } from './form-3852.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core'; 
import { CommonModule }  from '@angular/common';
import { RouterModule, Routes, provideRoutes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../../shared/shared.module'
import { HttpModule } from '@angular/http';
import { AuthorizationHelper } from '../../../authentication/util/authorization.helper.util';
import { TobaccoType } from '../../models/imptobacco-type.model';
import { ImporterTobaccoSubType } from '../../models/imptobacco-subtype.model';
import { ErrorHandlingService } from '../../../shared/service/error.handling.service';
import { FormsCalculationService } from '../../../shared/service/forms.calculation.service';
import { HttpClient } from '../../../../../node_modules/@angular/common/http';

declare var jQuery: any;

describe('Manufacturer Form 3852', () => {
  let comp:    Form3852;
  let fixture: ComponentFixture<Form3852>;

  // provide our implementations or mocks to the dependency injector
  beforeEach( async( () => {
    TestBed.configureTestingModule({
      imports: [ SharedModule, HttpModule, RouterTestingModule ],
      declarations: [Form3852],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [HttpClient, AuthorizationHelper,
        provideRoutes([{path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTesting', pathMatch:'full'}])
      ]
    }).compileComponents();
  }));

   beforeEach(() => {
    fixture = TestBed.createComponent(Form3852);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

});

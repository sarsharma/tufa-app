/*
@author : Deloitte

this is Component for FDA form 3852.
*/

import { Component, Input, Output, EventEmitter, ViewEncapsulation, SimpleChanges, OnInit, OnDestroy } from '@angular/core';
import { TobaccoType } from '../../models/tobacco-type.model';
import { TobaccoSubType } from '../../models/tobacco-subtype.model';
import { ErrorHandlingService } from '../../../shared/service/error.handling.service';
import { FormsCalculationService } from '../../../shared/service/forms.calculation.service';

declare var jQuery: any;

@Component({
  selector: '[form-3852]',
  templateUrl: './form-3852.template.html',
  styleUrls: ['./form-3852.style.scss'],
  encapsulation: ViewEncapsulation.None
})

export class Form3852 implements OnInit, OnDestroy  {

  @Input() model: TobaccoType;
  @Input() errorMsgs: any[];
  @Input() isTTSelected: number;
  @Output() hasChanges = new EventEmitter<boolean>();
  @Output() download = new EventEmitter<number>();

  quantity: number;

  formTypeCd="3852";
  myCalcType = "tax";

  numberValidationError: boolean;
  decimalValidationError: boolean;

  numberValidationErrorMessage = "Data Type Error: provide valid entry";

  private _regexNumber = /^\d+(,\d+)*$/;
  //private _regexDecimalNumber=/^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$/;
  private _regexDecimalNumber = /^(?:^|\s)(\d*\.?\d+|\d{1,3}(?:,\d{3})*(?:\.\d+)?)(?!\S)$/;
  private _regexCurrency = /^\$?\d+(,\d{3})*\.?[0-9]{0,2}?$/;

  form3852DataValidError: boolean = false;
  formName: string;

  formErrorMsg = {
    name: 'form3852',
    msg: 'Invalid information in form FDA 3852',
    type: 'warning'
  };

  constructor(private errorHandlingService: ErrorHandlingService, private formCalculationService: FormsCalculationService) {
    this.formName = 'form3852';

    errorHandlingService.formsChanged$.subscribe(
      param => {
        this.setGlobalError(this.checkForm3852InError());
      });

  }

  ngOnInit(): void { }

  ngOnDestroy() {
  }

  public updateCalc3852(ttypeName: string, tt?: TobaccoSubType) {

    var model: TobaccoSubType = tt ? tt : this.model.getTobaccoType(ttypeName);
    this.hasChanges.emit(true);
    if (this.form3852DataValidError) {

      if (model.isTaxAmt3852Pristine) {
        model.taxAmt3852 = null;
        model.taxDiff3852 = null;
      }

      if (model.isQty5210Pristine) {
        model.largeSticks = null;
        model.smallSticks = null;
        model.qty52105 = null;
      }

      return;
    }

    /**calculation for form 3852 **/

    if (model.tobaccoSubTypeName !== "Cigars") {
      model.calculateTaxAmount3852();
      model.calculateTaxDiff3852();
    }

    /** calculation for form 5210 **/
    model.calculateQty5210();
    //model.calculateTaxDiff5210();
    model.calculateQtyDiff5210();

    /**  calculation for form 5000 **/
    model.calculateTaxAmount5000();
    model.calculateTaxDiff5000();

  }


  private calcTaxDiff3852(ttypeName: string) {
    this.hasChanges.emit(true);
    let model = this.model.getTobaccoType(ttypeName);
    model.isTaxAmt3852Pristine = false;
    model.taxAmt3852 = model.taxAmt3852 ? model.taxAmt3852 : 0;

    if (model.tobaccoSubTypeName !== "Cigars")
      model.calculateTaxDiff3852();

    model.calculateTaxAmount5000();
    model.calculateTaxDiff5000();

  }

  public checkForm3852InError(): boolean {
    if (!this.model) return;

    var isError: boolean = false;
    var tTypes = this.model.getTobaccoTypes();
    for (var i in tTypes) {
      isError = tTypes[i].is3852Error();
      if (isError) break;
    }

    return isError;
  }

  public setGlobalError(isError: boolean) {

    let errorPresent: boolean = false;

    this.errorMsgs.forEach(function (item, index, array) {
      if (item.name === this.formName) {
        errorPresent = true;
        if (!isError) array.splice(index, 1);
      }
    }, this);

    if (isError && !errorPresent) this.errorMsgs.push(this.formErrorMsg);
  }

}

/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, EventEmitter, Input, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { ErrorHandlingService } from '../../../shared/service/error.handling.service';
import { TobaccoSubType } from '../../models/tobacco-subtype.model';
import { TobaccoType } from '../../models/tobacco-type.model';
import { SaveTTBAdjustments, TTBAdjustmentDetail, TTBAdjustmentDetailFilter, TTBAdjustmentDetailFilterData, TTBAdjustments } from '../../models/ttb-adjustment.model';
import { MonthlyReportEventService } from '../../services/monthly.report.event.service';
import { PermitMfgReportService } from '../../services/permit.mfg.report.service';

declare var jQuery: any;
const _BAD_VALUE = /^[.,]{2,}$/;

@Component({
  selector: '[form-5000]',
  templateUrl: './form-5000.template.html',
  styleUrls: ['./form-5000.style.scss'],
  encapsulation: ViewEncapsulation.None
})

export class Form5000 {
  
  @Input() model: TobaccoType;
  @Input() errorMsgs: any[];
  @Input() isTTSelected: number;
  @Output() hasChanges = new EventEmitter<boolean>();
  @Output() download = new EventEmitter<number>();

  iscalcTypeTax: boolean = true;
  formDisplayName = "TTB 5000.24";
  formTypeCd = "5000.24";

  values = '';
  public numbermask = [/\d*/];

  numberValidationErrorMessage = "Data Type Error: provide valid entry";

  form5000Error: boolean;
  form5000DataValidError: boolean = false;
  formName: string;
  adjustmentflagCigars:boolean=false;
  adjustmentflagCigrattes:boolean=false;
  adjustmentflagCheAndSunff:boolean=false;
  adjustmentflagPipeRYO:boolean=false;
  adjustmentflagAll:boolean;
  showThirdRow: boolean = false;
  scheduleAdjustmentA: number;
  ScheduleAdjustmentB: number;
  scheduleAdjustmentTotal: number;

  formErrorMsg = {
    name: 'form5000',
    msg: 'Invalid information in form TTB 5000.24',
    type: 'warning'
  };

  private _regexNumber = /^\d+(,\d+)*$/;
  private _regexCurrency = /^\$?\d+(,\d{3})*\.?[0-9]?[0-9]?$/;
  private _regexDecimalNumber = /^(?:^|\s)(\d*\.?\d+|\d{1,3}(?:,\d{3})*(?:\.\d+)?)(?!\S)$/;

  public columns: Array<any> = [
    { name: 'adjLineNum', type: 'input', filtering: { filterString: '', columnName: 'adjLineNum', placeholder: 'Line #'} },
    { name: 'taxReturnId', type: 'input', filtering: { filterString: '', columnName: 'taxReturnId', placeholder: 'Tax Return'} },
    { name: 'taxAmount', type: 'input', filtering: { filterString: '', columnName: 'taxAmount', placeholder: 'Amount'} }
  ];

  public columnsB: Array<any> = [
    { name: 'adjLineNum', type: 'input', filtering: { filterString: '', columnName: 'adjLineNum', placeholder: 'Line #'} },
    { name: 'taxReturnId', type: 'input', filtering: { filterString: '', columnName: 'taxReturnId', placeholder: 'Tax Return'} },
    { name: 'taxAmount', type: 'input', filtering: { filterString: '', columnName: 'taxAmount', placeholder: 'Amount'} }
  ];

showDeleteA: boolean = false;
showDeleteB: boolean = false;

chewScheduleAInfo: Array<any> = [
  {lineNum: 1, taxReturn: 'TR2015-01', amount: 100, lineDescription: "add schedule A"},
  {lineNum: 2, taxReturn: 'TR2015-02', amount: 200, lineDescription: "add schedule AA"}
];

taxReturnType: string[]  =[''];
adjustmentsScheduleA: TTBAdjustmentDetail[] = [];
adjustmentsScheduleB: TTBAdjustmentDetail[] = [];
sortByA: string = "adjLineNum";
sortOrderA: string[] = ["desc"];
sortByB: string = "adjLineNum";
sortOrderB: string[] = ["desc"];
tobaccoClassNameForPopUp: string;
tobaccoClassIdForPopUp: number;
subTotalScheduleA: number;
subTotalScheduleB: number;
scheduleABAdjustmentTotal: number;
adjustmentsScheduleAFilter = new TTBAdjustmentDetailFilter();
adjustmentsScheduleBFilter = new TTBAdjustmentDetailFilter();
adjustmentsScheduleAFilterData = new TTBAdjustmentDetailFilterData();
adjustmentsScheduleBFilterData = new TTBAdjustmentDetailFilterData();
filterAinformation: any;
filterBinformation: any;
busy: Subscription;
tobaccoClassNameTitlePopUp: string

  constructor(private errorHandlingService: ErrorHandlingService, private permitMfgReportService: PermitMfgReportService
    ,private monthlyReportService: MonthlyReportEventService) {
    this.formName = 'form5000';
    
    errorHandlingService.formsChanged$.subscribe(
      param => {
        this.setGlobalError(this.checkForm5000InError());
      });
  }

  // tslint:disable-next-line:member-ordering
  forms5000InError = [{
    id: "form5000-Cigarettes",
    isInError: -1
  }, {
    id: "form5000-Cigars",
    isInError: -1
  }, {
    id: "form5000-Chew",
    isInError: -1
  }, {
    id: "form5000-Pipe",
    isInError: -1
  }];

  ngOnInit(): void {
    if (this.model) {
      
      if(this.model.getTobaccoType('Cigars') != null && this.model.getTobaccoType('Cigars').isEnableAdjutment) {
        this.adjustmentflagCigars  = true;
      } 
      if(this.model.getTobaccoType('Cigarettes') !=null && this.model.getTobaccoType('Cigarettes').isEnableAdjutment) {
        this.adjustmentflagCigrattes = true;
      }
      if(this.model.getTobaccoTypeParent('ChewandSnuff')!=null && this.model.getTobaccoTypeParent('ChewandSnuff').isEnableAdjutment) {
        this.adjustmentflagCheAndSunff = true;
      }
      if(this.model.getTobaccoTypeParent('PipeRYO') !=null && this.model.getTobaccoTypeParent('PipeRYO').isEnableAdjutment) {
        this.adjustmentflagPipeRYO = true;
      }

    }
    
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "isTTSelected") {
        let chg = changes[propName];
        if (chg.currentValue) {
          if(this.model.getTobaccoType('Cigars') != null && (this.model.getTobaccoType('Cigars').isEnableAdjutment || this.adjustmentflagAll)) {
            this.adjustmentflagCigars  = true;
            this.model.getTobaccoType('Cigars').isEnableAdjutment = true;
          } else if(this.model.getTobaccoType('Cigars') != null && !this.model.getTobaccoType('Cigars').isEnableAdjutment && !this.adjustmentflagAll) {
            this.adjustmentflagCigars  = false;
            this.model.getTobaccoType('Cigars').isEnableAdjutment = false;
          }
          if(this.model.getTobaccoType('Cigarettes') !=null && (this.model.getTobaccoType('Cigarettes').isEnableAdjutment|| this.adjustmentflagAll)) {
            this.adjustmentflagCigrattes = true;
            this.model.getTobaccoType('Cigarettes').isEnableAdjutment = true;
          } else if (this.model.getTobaccoType('Cigarettes') !=null && !this.model.getTobaccoType('Cigarettes').isEnableAdjutment &&  !this.adjustmentflagAll) {
            this.adjustmentflagCigrattes = false;
            this.model.getTobaccoType('Cigarettes').isEnableAdjutment = false;
          }
          if(this.model.getTobaccoTypeParent('ChewandSnuff')!=null && (this.model.getTobaccoTypeParent('ChewandSnuff').isEnableAdjutment || this.adjustmentflagAll)) {
            this.adjustmentflagCheAndSunff = true;
            this.model.getTobaccoTypeParent('ChewandSnuff').isEnableAdjutment = true;
          } else if(this.model.getTobaccoTypeParent('ChewandSnuff')!=null && !this.model.getTobaccoTypeParent('ChewandSnuff').isEnableAdjutment && !this.adjustmentflagAll) {
            this.adjustmentflagCheAndSunff = false;
            this.model.getTobaccoTypeParent('ChewandSnuff').isEnableAdjutment = false;
          }
          if(this.model.getTobaccoTypeParent('PipeRYO') !=null && (this.model.getTobaccoTypeParent('PipeRYO').isEnableAdjutment || this.adjustmentflagAll)) {
            this.adjustmentflagPipeRYO = true;
            this.model.getTobaccoTypeParent('PipeRYO').isEnableAdjutment = true;
          } else if(this.model.getTobaccoTypeParent('PipeRYO') !=null && !this.model.getTobaccoTypeParent('PipeRYO').isEnableAdjutment &&  !this.adjustmentflagAll) {
            this.adjustmentflagPipeRYO = false;
            this.model.getTobaccoTypeParent('PipeRYO').isEnableAdjutment = false;
          }
        }
      }
    }
  }


  public updateCalc5000(model: TobaccoSubType) {
    model.calculateTaxDiff5000();
  }

  isThirdRowEnabled() {
    if (this.model) {
      return this.model.periodThreeTaxId_500024!=null;
    }
  }

  private summationTaxAmount(event, type: string, index: number) {
    if (this.form5000DataValidError) return;
    this.hasChanges.emit(true);
    this.values = (<HTMLInputElement>event.target).value;
    let numberVal: number;
    if (this.values === '') {
      this.values = null;
      numberVal = null;
    }
    else{
        numberVal = Number(this.values.replace(/,/g, ""));
    }
    
    if (isNaN(numberVal)) {
      return;
    }
    let ttsubType: TobaccoSubType;
    if (type === 'ChewandSnuff' || type === 'PipeRYO') {
      ttsubType = this.model.getTobaccoTypeParent(type);
    } else
      ttsubType = this.model.getTobaccoType(type);

    if (index < 3) {
      ttsubType.summationBox[index] = numberVal;
      ttsubType.taxAmountTotal5000 = 0;
      ttsubType.taxAmountGrandTotal5000 = 0;
      for (let i = 0; i < ttsubType.summationBox.length; i++) {
        if (!isNaN(ttsubType.summationBox[i])) {
          ttsubType.taxAmountTotal5000 = this.roundDecimal(ttsubType.taxAmountTotal5000 + Number(ttsubType.summationBox[i]));
        }
      }
      //this for adding total adjustment.
      let adjTotal = 0;
      if(ttsubType.isEnableAdjutment) {
        adjTotal = ttsubType.taxAmountTotalAdjustment?ttsubType.taxAmountTotalAdjustment:0
      }
      
      ttsubType.taxAmountGrandTotal5000 = this.roundDecimal(ttsubType.taxAmountGrandTotal5000+ttsubType.taxAmountTotal5000 +adjTotal);
    }

    // update tax difference calculations
    this.updateCalc5000(ttsubType);

  }

  private formatCurrencyField(event) {

    if (this.form5000DataValidError) return;

    if (event.target.value) {
      let dispVal: Number = Number(event.target.value.replace(/,/g, ""));
      event.target.value = dispVal.toLocaleString();
    }
  }

  // tslint:disable-next-line:member-ordering
  public checkForm5000InError(): boolean {
    let isError: boolean = false;
    if (!this.model) return;

    let tTypes = this.model.getTobaccoTypes();

    // tslint:disable-next-line:forin
    for (var i in tTypes) {
      isError = tTypes[i].is5000Error();
      if (isError) break;
    }
    return isError;
  }

  public setGlobalError(isError: boolean) {

    let errorPresent: boolean = false;
    if (!this.errorMsgs) return;
    this.errorMsgs.forEach(function (item, index, array) {
      if (item.name === this.formName) {
        errorPresent = true;
        if (!isError) array.splice(index, 1);
      }
    }, this);

    if (isError && !errorPresent) this.errorMsgs.push(this.formErrorMsg);
  }

  public roundDecimal(value, fractionSize = 2): number {
    let val = value ? value : 0;
    return Number(Math.round(Number(val + 'e' + fractionSize)) + 'e-' + fractionSize);
  }

  activateThirdTaxBreakdownRow(tobaccoTypeName: string) {
    this.showThirdRow = true;
    this.taxReturnType.push(this.model.periodOneTaxId_500024);
    this.taxReturnType.push(this.model.periodTwoTaxId_500024);
    if(this.model.periodThreeTaxId_500024!==null)
      this.taxReturnType.push(this.model.periodThreeTaxId_500024);

    if(tobaccoTypeName === 'Cigars' || tobaccoTypeName === 'Cigarettes'){
      if(this.model.getTobaccoType(tobaccoTypeName)){
        this.tobaccoClassNameForPopUp = tobaccoTypeName;
        this.tobaccoClassIdForPopUp = this.model.getTobaccoType(tobaccoTypeName).tobaccoClassId;
        let adjustments = JSON.parse(JSON.stringify(this.model.getTobaccoType(tobaccoTypeName).adjustmentDetails));
        for(let adj of adjustments){
          if(adj.adjType==='A')
            this.adjustmentsScheduleA.push(adj);
          else if(adj.adjType==='B')
            this.adjustmentsScheduleB.push(adj);        
          }
      }
    }else{
      if(this.model.getTobaccoTypeParent(tobaccoTypeName)){
        this.tobaccoClassNameForPopUp = tobaccoTypeName;
        this.tobaccoClassIdForPopUp = this.model.getTobaccoTypeParent(tobaccoTypeName).tobaccoClassId;
        let adjustments = JSON.parse(JSON.stringify(this.model.getTobaccoTypeParent(tobaccoTypeName).adjustmentDetails));
        for(let adj of adjustments){
          if(adj.adjType==='A')
            this.adjustmentsScheduleA.push(adj);
          else if(adj.adjType==='B')
            this.adjustmentsScheduleB.push(adj);        
        }
      }
    }
    if(this.adjustmentsScheduleA.length==0){
      this.addRowPopUp('A',this.tobaccoClassIdForPopUp);
    }
    if(this.adjustmentsScheduleB.length==0){
      this.addRowPopUp('B',this.tobaccoClassIdForPopUp);
    }

    if(this.tobaccoClassNameForPopUp === 'PipeRYO')
      this.tobaccoClassNameTitlePopUp = 'Pipe/Roll Your Own'
    else if(this.tobaccoClassNameForPopUp === 'ChewandSnuff')
      this.tobaccoClassNameTitlePopUp = 'Chew/Snuff'
    else if(this.tobaccoClassNameForPopUp === 'Cigarettes')
      this.tobaccoClassNameTitlePopUp = 'Cigarettes'
    else if(this.tobaccoClassNameForPopUp === 'Cigars')
      this.tobaccoClassNameTitlePopUp = 'Cigars'

    this.calculateScheduleAB();
    jQuery("#scheduled_adjustment_popup").modal('show');
  }

  calculateScheduleAB(){
    this.subTotalScheduleA = null;
    this.subTotalScheduleB = null;
    for(let A of this.adjustmentsScheduleA){
      if (A.taxAmount) {
        this.subTotalScheduleA = this.subTotalScheduleA + (isNaN(A.taxAmount)?0:A.taxAmount);
      }
    }
    for(let B of this.adjustmentsScheduleB){
      if (B.taxAmount) {
        this.subTotalScheduleB = this.subTotalScheduleB + (isNaN(B.taxAmount)?0:B.taxAmount);
      }
    }

    if (this.subTotalScheduleA || this.subTotalScheduleB) {
      this.scheduleABAdjustmentTotal = (isNaN(this.subTotalScheduleA)?0:this.subTotalScheduleA) - (isNaN(this.subTotalScheduleB)?0:this.subTotalScheduleB);
    } else {
      this.scheduleABAdjustmentTotal = null;
    }
  }

  changeScheduleTaxAmount(event, scheduleType: string, adjLineNum: number){
    this.values = event;
    let numberVal: number;
    if (this.values === '') {
      numberVal = null;
    }
    else{
      numberVal = Number(this.values.replace(/,/g, "")); 
    }  
    if (isNaN(numberVal)) {
      return;
    } 
    if(scheduleType === 'A')
      this.adjustmentsScheduleA.find(A=>A.adjLineNum===adjLineNum).taxAmount = numberVal;
    else if(scheduleType === 'B')
      this.adjustmentsScheduleB.find(B=>B.adjLineNum===adjLineNum).taxAmount = numberVal;
    this.calculateScheduleAB();
  }

  updateInput($event: any, scheduleType: string, adjLineNum: number){
    if(scheduleType === 'A')
      this.adjustmentsScheduleA.find(A=>A.adjLineNum===adjLineNum).taxReturnId = $event.target.value;
    else if(scheduleType === 'B')
      this.adjustmentsScheduleB.find(B=>B.adjLineNum===adjLineNum).taxReturnId = $event.target.value;
  }

  selectAll(event) {
    this.adjustmentflagAll = event;
      if (event) {
          this.checkAllBoxes();
      } else {
        this.uncheckAllBoxes();
      }
     
  }
  checkAllBoxes() {
    this.hasChanges.emit(true);
    (this.model.showSnuffs || this.model.showChews)?this.adjustmentflagCheAndSunff = true:this.adjustmentflagCheAndSunff=false;
     this.model.showCigars?this.adjustmentflagCigars = true:this.adjustmentflagCigars=false;
     this.model.showCigarettes?this.adjustmentflagCigrattes = true:this.adjustmentflagCigrattes = false;
     (this.model.showRollYourOwn|| this.model.showPipes)?this.adjustmentflagPipeRYO = true:this.adjustmentflagPipeRYO = false;
    
     if(this.model.getTobaccoType('Cigars') != null) this.adjustmentflagCigars?this.model.getTobaccoType('Cigars').isEnableAdjutment = true:this.model.getTobaccoType('Cigars').isEnableAdjutment = false;
     if(this.model.getTobaccoType('Cigarettes') != null)this.adjustmentflagCigrattes?this.model.getTobaccoType('Cigarettes').isEnableAdjutment = true:this.model.getTobaccoType('Cigarettes').isEnableAdjutment = false;
     if(this.model.getTobaccoTypeParent('ChewandSnuff') != null)this.adjustmentflagCheAndSunff?this.model.getTobaccoTypeParent('ChewandSnuff').isEnableAdjutment = true:this.model.getTobaccoTypeParent('ChewandSnuff').isEnableAdjutment = false;  
     if(this.model.getTobaccoTypeParent('PipeRYO') != null)this.adjustmentflagPipeRYO? this.model.getTobaccoTypeParent('PipeRYO').isEnableAdjutment = true: this.model.getTobaccoTypeParent('PipeRYO').isEnableAdjutment = false; 
   
  }

  uncheckAllBoxes() {
    this.hasChanges.emit(true);
    this.adjustmentflagCheAndSunff = false;
    this.adjustmentflagCigars = false;
    this.adjustmentflagCigrattes = false;
    this.adjustmentflagPipeRYO = false;
    if(this.model.getTobaccoType('Cigars') != null) this.adjustmentflagCigars?this.model.getTobaccoType('Cigars').isEnableAdjutment = true:this.model.getTobaccoType('Cigars').isEnableAdjutment = false;
    if(this.model.getTobaccoType('Cigarettes') != null)this.adjustmentflagCigrattes?this.model.getTobaccoType('Cigarettes').isEnableAdjutment = true:this.model.getTobaccoType('Cigarettes').isEnableAdjutment = false;
    if(this.model.getTobaccoTypeParent('ChewandSnuff') != null)this.adjustmentflagCheAndSunff?this.model.getTobaccoTypeParent('ChewandSnuff').isEnableAdjutment = true:this.model.getTobaccoTypeParent('ChewandSnuff').isEnableAdjutment = false;  
    if(this.model.getTobaccoTypeParent('PipeRYO') != null)this.adjustmentflagPipeRYO? this.model.getTobaccoTypeParent('PipeRYO').isEnableAdjutment = true: this.model.getTobaccoTypeParent('PipeRYO').isEnableAdjutment = false; 
  }

  isAdjustmentSectionEnabled() {
    return this.adjustmentflagAll || this.adjustmentflagCheAndSunff || this.adjustmentflagCigars || this.adjustmentflagCigrattes || this.adjustmentflagPipeRYO;
  }

  isNonCigarPresent() {
    return this.model.showChews || this.model.showSnuffs || this.model.showPipes || this.model.showRollYourOwn || this.model.showCigarettes;
  }

  checkForSelectAll(event,type: string) {
    this.hasChanges.emit(true);
    if(!(this.adjustmentflagCheAndSunff || this.adjustmentflagCigars || this.adjustmentflagCigrattes || this.adjustmentflagPipeRYO) && this.adjustmentflagAll) {
      this.adjustmentflagAll = false;
    }
    if(this.model.getTobaccoType('Cigars') != null) this.adjustmentflagCigars?this.model.getTobaccoType('Cigars').isEnableAdjutment = true:this.model.getTobaccoType('Cigars').isEnableAdjutment = false;
    if(this.model.getTobaccoType('Cigarettes') != null)this.adjustmentflagCigrattes?this.model.getTobaccoType('Cigarettes').isEnableAdjutment = true:this.model.getTobaccoType('Cigarettes').isEnableAdjutment = false;
    if(this.model.getTobaccoTypeParent('ChewandSnuff') != null)this.adjustmentflagCheAndSunff?this.model.getTobaccoTypeParent('ChewandSnuff').isEnableAdjutment = true:this.model.getTobaccoTypeParent('ChewandSnuff').isEnableAdjutment = false;  
    if(this.model.getTobaccoTypeParent('PipeRYO') != null)this.adjustmentflagPipeRYO? this.model.getTobaccoTypeParent('PipeRYO').isEnableAdjutment = true: this.model.getTobaccoTypeParent('PipeRYO').isEnableAdjutment = false; 
  }

   // this will check the latest adjustments for the selected tobacco types.
   checkLatestScheduleAdjForTT(type: string, ttbAdjustments: TTBAdjustments) {
    let ttsubType: TobaccoSubType;
    if (type === 'ChewandSnuff' || type === 'PipeRYO') {
      ttsubType = this.model.getTobaccoTypeParent(type);
    } else
      ttsubType = this.model.getTobaccoType(type);
    ttsubType.taxAmountAdjusmentA = ttbAdjustments.taxAmountAdjusmentA;
    ttsubType.taxAmountAdjustmentB = ttbAdjustments.taxAmountAdjusmentB;
    ttsubType.taxAmountTotalAdjustment = ttbAdjustments.taxAmountTotalAdjustment;
    ttsubType.adjustmentDetails = ttbAdjustments.ttbAdjustmentDetail;
    //adding latest summention after adding adjustment and API return
    if((this.model.getTobaccoType(type) != null && this.model.getTobaccoType(type).isEnableAdjutment) || (this.model.getTobaccoTypeParent(type) != null && this.model.getTobaccoTypeParent(type).isEnableAdjutment)) {
    if(this.model.getTobaccoType(type) != null && this.model.getTobaccoType(type).isEnableAdjutment) {
      this.model.getTobaccoType(type).taxAmountGrandTotal5000 = (this.model.getTobaccoType(type).taxAmountTotal5000?this.model.getTobaccoType(type).taxAmountTotal5000:0) + (this.model.getTobaccoType(type).taxAmountTotalAdjustment?this.model.getTobaccoType(type).taxAmountTotalAdjustment:0);
      
    } else {
      this.model.getTobaccoTypeParent(type).taxAmountGrandTotal5000 = (this.model.getTobaccoTypeParent(type).taxAmountTotal5000?this.model.getTobaccoTypeParent(type).taxAmountTotal5000:0) + (this.model.getTobaccoTypeParent(type).taxAmountTotalAdjustment?this.model.getTobaccoTypeParent(type).taxAmountTotalAdjustment:0);
      
    }
    }
     this.updateCalc5000(ttsubType);
     this.hasChanges.emit(true);
   }

   checkSummationTaxAmount(event,type: string) {
    if((this.model.getTobaccoType(type) != null && !this.model.getTobaccoType(type).isEnableAdjutment) || (this.model.getTobaccoTypeParent(type) != null && !this.model.getTobaccoTypeParent(type).isEnableAdjutment)) {
      if(this.model.getTobaccoType(type) != null && !this.model.getTobaccoType(type).isEnableAdjutment) {
        this.model.getTobaccoType(type).taxAmountGrandTotal5000 = this.model.getTobaccoType(type).taxAmountTotal5000;
        // this.model.getTobaccoType(type).taxAmountAdjusmentA = 0;
        // this.model.getTobaccoType(type).taxAmountAdjustmentB = 0;
        // this.model.getTobaccoType(type).taxAmountTotalAdjustment = 0;
        this.updateCalc5000(this.model.getTobaccoType(type));
      } else {
        this.model.getTobaccoTypeParent(type).taxAmountGrandTotal5000 = this.model.getTobaccoTypeParent(type).taxAmountTotal5000;
        // this.model.getTobaccoTypeParent(type).taxAmountAdjusmentA = 0;
        // this.model.getTobaccoTypeParent(type).taxAmountAdjustmentB = 0;
        // this.model.getTobaccoTypeParent(type).taxAmountTotalAdjustment = 0;
        this.updateCalc5000( this.model.getTobaccoTypeParent(type));
      }
      
    } else if((this.model.getTobaccoType(type) != null && this.model.getTobaccoType(type).isEnableAdjutment) || (this.model.getTobaccoTypeParent(type) != null && this.model.getTobaccoTypeParent(type).isEnableAdjutment)) {
      if(this.model.getTobaccoType(type) != null && this.model.getTobaccoType(type).isEnableAdjutment) {
        this.model.getTobaccoType(type).taxAmountGrandTotal5000 = this.model.getTobaccoType(type).taxAmountTotal5000 + (this.model.getTobaccoType(type).taxAmountTotalAdjustment?this.model.getTobaccoType(type).taxAmountTotalAdjustment:0);
      } else {
        this.model.getTobaccoTypeParent(type).taxAmountGrandTotal5000 = this.model.getTobaccoTypeParent(type).taxAmountTotal5000 + (this.model.getTobaccoTypeParent(type).taxAmountTotalAdjustment?this.model.getTobaccoTypeParent(type).taxAmountTotalAdjustment:0);
      }
    }
   }

  //  updateEditable(element, adjType: string, adjLineNum: number) {
  //   if(element !== ""){
  //     jQuery('#scheduleTax' + adjType + '-' + adjLineNum.toString()).attr.('disabled', 'disabled');
  //   }
  // }

  checkCustom(taxReturnOption: string){
    if(taxReturnOption === ''){
      jQuery("#editable-select").focus();
    }
   }
 
   public addRowPopUp(scheduleType: string, tobaccoClassId: number ){
    if(scheduleType === 'A'){
      let adjLineNum = this.adjustmentsScheduleA.length > 0 ? Math.max.apply(Math, this.adjustmentsScheduleA.map(function(o){ return o.adjLineNum})) + 1 : 1;
      let newLine : TTBAdjustmentDetail = new TTBAdjustmentDetail();
      if (this.tobaccoClassNameForPopUp === 'ChewandSnuff' || this.tobaccoClassNameForPopUp === 'PipeRYO') {
        newLine.formId  = this.model.getTobaccoTypeParent(this.tobaccoClassNameForPopUp) !=null? this.model.getTobaccoTypeParent(this.tobaccoClassNameForPopUp).formId : null;
      } else
         newLine.formId  = this.model.getTobaccoType(this.tobaccoClassNameForPopUp) !=null? this.model.getTobaccoType(this.tobaccoClassNameForPopUp).formId : null;
      newLine.tobaccoClassId = tobaccoClassId;
      newLine.adjLineNum = adjLineNum;
      newLine.adjType ='A';
      newLine.taxReturnId ='';
      newLine.taxAmount = null;
      this.adjustmentsScheduleA.push(newLine);
    }
    else if(scheduleType === 'B'){
      let adjLineNum = this.adjustmentsScheduleB.length > 0 ? Math.max.apply(Math, this.adjustmentsScheduleB.map(function(o){ return o.adjLineNum})) + 1 : 1;
      let newLine : TTBAdjustmentDetail = new TTBAdjustmentDetail();
      if (this.tobaccoClassNameForPopUp === 'ChewandSnuff' || this.tobaccoClassNameForPopUp === 'PipeRYO') {
        newLine.formId  = this.model.getTobaccoTypeParent(this.tobaccoClassNameForPopUp) !=null? this.model.getTobaccoTypeParent(this.tobaccoClassNameForPopUp).formId : null;
      } else
         newLine.formId  = this.model.getTobaccoType(this.tobaccoClassNameForPopUp) !=null? this.model.getTobaccoType(this.tobaccoClassNameForPopUp).formId : null;
      newLine.tobaccoClassId = tobaccoClassId;
      newLine.adjLineNum = adjLineNum;
      newLine.adjType ='B';
      newLine.taxReturnId ='';
      newLine.taxAmount = null;
      this.adjustmentsScheduleB.push(newLine);
    }
    //this.calculateScheduleAB();
}

public deleteRowPopUp(adjLineNum: number, scheduleType: string) {
  if(scheduleType === 'A'){
    for (var i = 0; i < this.adjustmentsScheduleA.length; i++) {
      if(this.adjustmentsScheduleA[i].adjLineNum === adjLineNum)
        this.adjustmentsScheduleA.splice(i, 1);
    }
    if(this.adjustmentsScheduleA.length === 0)
      this.addRowPopUp('A', this.tobaccoClassIdForPopUp)
  }
  else if(scheduleType === 'B'){
    for (var i = 0; i < this.adjustmentsScheduleB.length; i++) {
      if(this.adjustmentsScheduleB[i].adjLineNum === adjLineNum)
        this.adjustmentsScheduleB.splice(i, 1);
    }
  }
  if(this.adjustmentsScheduleB.length === 0)
    this.addRowPopUp('B', this.tobaccoClassIdForPopUp)
  this.calculateScheduleAB();
}

popUpClose(){
  this.sortByA = "adjLineNum";
  this.sortOrderA = ["desc"];
  this.sortByB = "adjLineNum";
  this.sortOrderB = ["desc"];
  this.adjustmentsScheduleA = [];
  this.adjustmentsScheduleB = [];
  this.tobaccoClassNameForPopUp = null;
  this.tobaccoClassIdForPopUp = null;
  this.subTotalScheduleA;
  this.subTotalScheduleB;
  this.scheduleABAdjustmentTotal;
  this.showDeleteA = false;
  this.showDeleteB = false;
  this.taxReturnType = [''];
  this.adjustmentsScheduleAFilter = new TTBAdjustmentDetailFilter();
  this.adjustmentsScheduleBFilter = new TTBAdjustmentDetailFilter();
  this.adjustmentsScheduleAFilterData = new TTBAdjustmentDetailFilterData();
  this.adjustmentsScheduleBFilterData = new TTBAdjustmentDetailFilterData();
  this.filterAinformation = null;
  this.filterBinformation = null;
  this.columns.forEach((element, index)=>this.columns[index].filtering.filterString = '');
  this.columnsB.forEach((element, index)=>this.columnsB[index].filtering.filterString = '');
  jQuery("#scheduled_adjustment_popup").modal('hide');
}

saveAdjustments(){
  let allAdjustmentsDetails : TTBAdjustments = new TTBAdjustments();

  let adjustmentsAB : TTBAdjustmentDetail[] = [];
  for(let adj of this.adjustmentsScheduleA)
  {
   // if(adj.taxAmount === null) adj.taxAmount = 0;
    adjustmentsAB.push(adj);
  }    
  for(let adj of this.adjustmentsScheduleB)
  {
    //if(adj.taxAmount === null) adj.taxAmount = 0;
    adjustmentsAB.push(adj);
  }  
  allAdjustmentsDetails.ttbAdjustmentDetail = adjustmentsAB;
  allAdjustmentsDetails.taxAmountAdjusmentA = this.subTotalScheduleA;
  allAdjustmentsDetails.taxAmountAdjusmentB = this.subTotalScheduleB;
  allAdjustmentsDetails.taxAmountTotalAdjustment = this.scheduleABAdjustmentTotal;
  allAdjustmentsDetails.formId = adjustmentsAB[0].formId!==undefined ? adjustmentsAB[0].formId : null ;
  allAdjustmentsDetails.tobaccoClassId = adjustmentsAB[0].tobaccoClassId;

  //call save pop up API here and pass allAdjustments
  let saveTTBAdjustment: SaveTTBAdjustments = new SaveTTBAdjustments();
  saveTTBAdjustment.ttbAdjustmentList[0] = allAdjustmentsDetails;
  saveTTBAdjustment.periodId = this.model.periodId;
  saveTTBAdjustment.permitId = this.model.permitId;

  this.busy = this.permitMfgReportService.saveTTBAdjustments(saveTTBAdjustment).subscribe(
    data =>{
      let ttb = new TTBAdjustments();
      jQuery.extend(true, ttb, data);
      this.monthlyReportService.triggerAdjustmentAdded(ttb);
      this.checkLatestScheduleAdjForTT(this.tobaccoClassNameForPopUp, ttb);
      this.popUpClose();
    }
  );  
}

public onSortBy(event) {
  this.sortByA = event;
}

refreshAdjustmentsFilterObject() {
  this.adjustmentsScheduleAFilterData = null;
  this.adjustmentsScheduleBFilterData = null;

  this.adjustmentsScheduleAFilterData = new TTBAdjustmentDetailFilterData();
  this.adjustmentsScheduleBFilterData = new TTBAdjustmentDetailFilterData();

  this.adjustmentsScheduleAFilterData.adjLineNumFilter = this.adjustmentsScheduleAFilter.adjLineNumFilter;
  this.adjustmentsScheduleAFilterData.taxAmountFilter = this.adjustmentsScheduleAFilter.taxAmountFilter;
  this.adjustmentsScheduleAFilterData.taxReturnIdFilter = this.adjustmentsScheduleAFilter.taxReturnIdFilter;

  this.adjustmentsScheduleBFilterData.adjLineNumFilter = this.adjustmentsScheduleBFilter.adjLineNumFilter;
  this.adjustmentsScheduleBFilterData.taxAmountFilter = this.adjustmentsScheduleBFilter.taxAmountFilter;
  this.adjustmentsScheduleBFilterData.taxReturnIdFilter = this.adjustmentsScheduleBFilter.taxReturnIdFilter;
}

public onChangeTableA(filterinfo): void {

  this.filterAinformation = filterinfo;

  if (filterinfo) {
      if (filterinfo.columnName === "adjLineNum") {
          this.adjustmentsScheduleAFilter.adjLineNumFilter = filterinfo.filterString.trim();
      }
      if (filterinfo.columnName === "taxAmount") {
        this.adjustmentsScheduleAFilter.taxAmountFilter = filterinfo.filterString.trim();
      }
      if (filterinfo.columnName === "taxReturnId") {
        this.adjustmentsScheduleAFilter.taxReturnIdFilter = filterinfo.filterString.trim();
      }

      this.refreshAdjustmentsFilterObject();
  }
}

  public onChangeTableB(filterinfo): void {

    this.filterBinformation = filterinfo;
  
    if (filterinfo) {
        if (filterinfo.columnName === "adjLineNum") {
            this.adjustmentsScheduleBFilter.adjLineNumFilter = filterinfo.filterString.trim();
        }
        if (filterinfo.columnName === "taxAmount") {
          this.adjustmentsScheduleBFilter.taxAmountFilter = filterinfo.filterString.trim();
        }
        if (filterinfo.columnName === "taxReturnId") {
          this.adjustmentsScheduleBFilter.taxReturnIdFilter = filterinfo.filterString.trim();
        }
  
        this.refreshAdjustmentsFilterObject();
    }

}

}


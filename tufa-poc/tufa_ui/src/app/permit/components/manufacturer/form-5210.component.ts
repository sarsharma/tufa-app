/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, Input, Output, ViewEncapsulation,OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { TobaccoType } from '../../models/tobacco-type.model';
import { TobaccoSubType } from '../../models/tobacco-subtype.model';
import { ErrorHandlingService } from '../../../shared/service/error.handling.service';


declare var jQuery: any;

@Component({
  selector: '[form-5210]',
  templateUrl: './form-5210.template.html',
  styleUrls: ['./form-5210.style.scss'],
  encapsulation: ViewEncapsulation.None
})

export class Form5210 {
  @Input() model: TobaccoType;
  @Input() errorMsgs: any[];
  @Input() isTTSelected: number;
  @Input() triggerErrorChk: boolean;
  @Output() hasChanges = new EventEmitter<boolean>();
  @Output() download = new EventEmitter<number>();

  form5210Error: boolean;
  form5210DataValidError: boolean = false;
  formName: string;

  formErrorMsg = {
    name: 'form5210',
    msg: 'Invalid information in form TTB 5210.5',
    type: 'warning'
  };

  totalQty: number;

  formTypeCd = "5210.5";
  iscalcTypeTax: boolean = true;
  private _regexNumber = /^\d+(,\d+)*$/;
  private _regexDecimalNumber = /^(?:^|\s)(\d*\.?\d+|\d{1,3}(?:,\d{3})*(?:\.\d+)?)(?!\S)$/;
  //private _regexDecimalNumber=/^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$/;

  numberValidationErrorMessage = "Data Type Error: provide valid entry";

  defaultStr = 'Disabled';
  cigsLvalid = { product: this.defaultStr };
  cigsLproducts: string[] = [
    'Action',
    'Mark Correct'
  ];
  cigsSvalid = { product: this.defaultStr };
  cigsSproducts: string[] = [
    'Action',
    'Mark Correct'
  ];
  cigarsLvalid = { product: this.defaultStr };
  cigarsLproducts: string[] = [
    'Action',
    'Mark Correct'
  ];
  cigarsSvalid = { product: this.defaultStr };
  cigarsSproducts: string[] = [
    'Action',
    'Mark Correct'
  ];
  snuffvalid = { product: this.defaultStr };
  snuffproducts: string[] = [
    'Action',
    'Mark Correct'
  ];
  chewvalid = { product: this.defaultStr };
  chewproducts: string[] = [
    'Action',
    'Mark Correct'
  ];
  pipevalid = { product: this.defaultStr };
  pipeproducts: string[] = [
    'Action',
    'Mark Correct'
  ];
  rollvalid = { product: this.defaultStr };
  rollproducts: string[] = [
    'Action',
    'Mark Correct'
  ];
  adjFlagAll: boolean = false;
  adjFlagCigars: boolean = false;
  adjFlagCigarette: boolean =  false;
  adjFlagChew: boolean  = false;
  adjFlagSnuff: boolean  = false;
  adjFlagPipe: boolean = false;
  adjFlagRyn: boolean  = false;
  

  constructor(private errorHandlingService: ErrorHandlingService) {
    this.formName = 'form5210';

    //need to be removed so as to subscribe to only one observable.
    /*errorHandlingService.form3852Changed$.subscribe(
       param =>{
        this.setGlobalError(this.checkForms5210InError());
      });*/

    errorHandlingService.formsChanged$.subscribe(
      param => {
        this.setGlobalError(this.checkForm5210InError());
      });

  }

  ngOnInit(): void {
    if (this.model) {
      
      if(this.model.getTobaccoType('Cigars') != null && this.model.getTobaccoType('Cigars').isEnableAdjustment5210) {
        this.adjFlagCigars  = true;
      } 
      if(this.model.getTobaccoType('Cigarettes') !=null && this.model.getTobaccoType('Cigarettes').isEnableAdjustment5210) {
        this.adjFlagCigarette = true;
      }
      if(this.model.getTobaccoType('Chew') != null && this.model.getTobaccoType('Chew').isEnableAdjustment5210) {
        this.adjFlagChew  = true;
      } 
      if(this.model.getTobaccoType('Snuff') !=null && this.model.getTobaccoType('Snuff').isEnableAdjustment5210) {
        this.adjFlagSnuff = true;
      }
      if(this.model.getTobaccoType('Pipe') != null && this.model.getTobaccoType('Pipe').isEnableAdjustment5210) {
        this.adjFlagPipe  = true;
      } 
      if(this.model.getTobaccoType('RollYourOwn') != null && this.model.getTobaccoType('RollYourOwn').isEnableAdjustment5210) {
        this.adjFlagRyn  = true;
      } 
      this.setEnableAllAdjustment();
    }
  }
  
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "isTTSelected") {
        let chg = changes[propName];
        if (chg.currentValue) { 
          if(this.model.getTobaccoType('Cigars') != null && (this.model.getTobaccoType('Cigars').isEnableAdjustment5210 || this.adjFlagAll)) {
            this.adjFlagCigars  = true;
            this.model.getTobaccoType('Cigars').isEnableAdjustment5210 = true;
          } else if(this.model.getTobaccoType('Cigars') != null && !this.model.getTobaccoType('Cigars').isEnableAdjustment5210 && !this.adjFlagAll) {
            this.adjFlagCigars  = false;
            this.model.getTobaccoType('Cigars').isEnableAdjustment5210 = false;
          }
          if(this.model.getTobaccoType('Cigarettes') !=null && (this.model.getTobaccoType('Cigarettes').isEnableAdjustment5210|| this.adjFlagAll)) {
            this.adjFlagCigarette = true;
            this.model.getTobaccoType('Cigarettes').isEnableAdjustment5210 = true;
          } else if (this.model.getTobaccoType('Cigarettes') !=null && !this.model.getTobaccoType('Cigarettes').isEnableAdjustment5210 &&  !this.adjFlagAll) {
            this.adjFlagCigarette = false;
            this.model.getTobaccoType('Cigarettes').isEnableAdjustment5210 = false;
          }
          if(this.model.getTobaccoType('Chew')!=null && (this.model.getTobaccoType('Chew').isEnableAdjustment5210 || this.adjFlagAll)) {
            this.adjFlagChew = true;
            this.model.getTobaccoType('Chew').isEnableAdjustment5210 = true;
          } else if(this.model.getTobaccoType('Chew')!=null && !this.model.getTobaccoType('Chew').isEnableAdjustment5210 && !this.adjFlagAll) {
            this.adjFlagChew = false;
            this.model.getTobaccoType('Chew').isEnableAdjustment5210 = false;
          }
		     if(this.model.getTobaccoType('Snuff')!=null && (this.model.getTobaccoType('Snuff').isEnableAdjustment5210 || this.adjFlagAll)) {
            this.adjFlagSnuff = true;
            this.model.getTobaccoType('Snuff').isEnableAdjustment5210 = true;
          } else if(this.model.getTobaccoType('Snuff')!=null && !this.model.getTobaccoType('Snuff').isEnableAdjustment5210 && !this.adjFlagAll) {
            this.adjFlagSnuff = false;
            this.model.getTobaccoType('Snuff').isEnableAdjustment5210 = false;
          }
          if(this.model.getTobaccoType('Pipe') !=null && (this.model.getTobaccoType('Pipe').isEnableAdjustment5210 || this.adjFlagAll)) {
            this.adjFlagPipe = true;
            this.model.getTobaccoType('Pipe').isEnableAdjustment5210 = true;
          } else if(this.model.getTobaccoType('Pipe') !=null && !this.model.getTobaccoType('Pipe').isEnableAdjustment5210 &&  !this.adjFlagAll) {
            this.adjFlagPipe = false;
            this.model.getTobaccoType('Pipe').isEnableAdjustment5210 = false;
          }
		     if(this.model.getTobaccoType('RollYourOwn') !=null && (this.model.getTobaccoType('RollYourOwn').isEnableAdjustment5210 || this.adjFlagAll)) {
            this.adjFlagRyn = true;
            this.model.getTobaccoType('RollYourOwn').isEnableAdjustment5210 = true;
          } else if(this.model.getTobaccoType('RollYourOwn') !=null && !this.model.getTobaccoType('RollYourOwn').isEnableAdjustment5210 &&  !this.adjFlagAll) {
            this.adjFlagRyn = false;
            this.model.getTobaccoType('RollYourOwn').isEnableAdjustment5210 = false;
          }
        }
      }
    }
  }


  /*public validateInput(value: string, ttypeName, fieldType, size) {

    let ttsubType: TobaccoSubType = this.model.getTobaccoType(ttypeName);

    if (!value) {
      ttsubType.isQuantity52105StrValid = true;
      this.form5210DataValidError = false;
      return;
    }

    if (ttsubType) {
      if (fieldType == "qtynumber") {
        if (size && size == "small")
          ttsubType.isQuantity52105StrValid = ttsubType.isQuantity52105StrValidSml = this._regexNumber.test(value);
        else if (size && size == "large")
          ttsubType.isQuantity52105StrValid = ttsubType.isQuantity52105StrValidLrg = this._regexNumber.test(value);
        else
          ttsubType.isQuantity52105StrValid = this._regexNumber.test(value);
      } else if (fieldType == "qtydecimal") {
        ttsubType.isQuantity52105StrValid = this._regexDecimalNumber.test(value);
      }

      //this.form5210DataValidError = !ttsubType.isQuantity52105StrValid;
    }
  }*/

  public updateCalc5210(ttypeName: string) {

    let ttsubType: TobaccoSubType = this.model.getTobaccoType(ttypeName);
    if (!ttsubType.isQuantity52105StrValid) return;
    this.hasChanges.emit(true);
    ttsubType.isQty5210Pristine = false;
    ttsubType.calculateQtyDiff5210();
  }

  public updateTotalSticks(model: TobaccoSubType) {

    if (!model.isQuantity52105StrValid) return;
    this.hasChanges.emit(true);
    model.qty52105 = Number(model.largeSticks ? model.largeSticks : 0) + Number(model.smallSticks ? model.smallSticks : 0);
    model.isQty5210Pristine = false;
    model.calculateQtyDiff5210();
  }

  public largecigs(model: TobaccoSubType) {
    if (model.largeSticks > 0)
      this.cigsLvalid.product = 'Action';
  }

  public smallcigs(model: TobaccoSubType) {
    if (model.smallSticks > 0)
      this.cigsSvalid.product = 'Action';
  }

  public largecigars(model: TobaccoSubType) {
    if (model.largeSticks > 0)
      this.cigarsLvalid.product = 'Action';
  }

  public smallcigars(model: TobaccoSubType) {
    if (model.smallSticks > 0)
      this.cigarsSvalid.product = 'Action';
  }

  public chew(model: TobaccoSubType) {
    if (model.qty52105 > 0)
      this.chewvalid.product = 'Action';
  }

  public snuff(model: TobaccoSubType) {
    if (model.qty52105 > 0)
      this.snuffvalid.product = 'Action';
  }

  public pipe(model: TobaccoSubType) {
    if (model.qty52105 > 0)
      this.pipevalid.product = 'Action';
  }

  public roll(model: TobaccoSubType) {
    if (model.qty52105 > 0)
      this.rollvalid.product = 'Action';
  }

  public numericVal(event) {
    if ((event.which >= 48 && event.which <= 57) || event.which === 188)
      return true;
    else {
      event.preventDefault();
      return false;
    }
  }

  public checkForm5210InError(): boolean {
    var isError: boolean = false;
    if(!this.model) return;

    var tTypes = this.model.getTobaccoTypes();
    for (var i in tTypes) {
      isError = tTypes[i].is5210Error();
      if (isError) break;
    }
    return isError;
  }

  public setGlobalError(isError: boolean) {

    let errorPresent: boolean = false;
    if(!this.errorMsgs)return;
    this.errorMsgs.forEach(function (item, index, array) {
      if (item.name === this.formName) {
        errorPresent = true;
        if (!isError) array.splice(index, 1);
      }
    }, this);

    if (isError && !errorPresent) this.errorMsgs.push(this.formErrorMsg);
  }
  
selectAllAdj(event) {
  this.adjFlagAll = event;
    if (event) {
      this.checkAllBoxes();
    } else {
      this.uncheckAllBoxes();
    }
   
}
checkAllBoxes() {
  this.hasChanges.emit(true);
  this.model.showSnuffs?this.adjFlagSnuff = true:this.adjFlagSnuff=false;
  this.model.showChews?this.adjFlagChew = true:this.adjFlagChew=false;
  this.model.showCigars?this.adjFlagCigars = true:this.adjFlagCigars=false;
  this.model.showCigarettes?this.adjFlagCigarette = true:this.adjFlagCigarette = false;
  this.model.showRollYourOwn?this.adjFlagRyn = true:this.adjFlagRyn = false;
  this.model.showPipes?this.adjFlagPipe = true:this.adjFlagPipe = false;
  this.setEnableAdjustmentFlag();
   
}
setEnableAdjustmentFlag(){
  if(this.model.getTobaccoType('Cigars') != null){
    this.model.getTobaccoType('Cigars').isEnableAdjustment5210 = this.adjFlagCigars;
    if(!this.model.getTobaccoType('Cigars').isEnableAdjustment5210){
      this.model.getTobaccoType('Cigars').adjustmentRemoval = null;
      this.updateCalc5210('Cigars');
      this.model.getTobaccoType('Cigars').isQty5210Pristine = true;
    }
  }
  if(this.model.getTobaccoType('Cigarettes') != null){
    this.model.getTobaccoType('Cigarettes').isEnableAdjustment5210 = this.adjFlagCigarette;
    if(!this.model.getTobaccoType('Cigarettes').isEnableAdjustment5210){
      this.model.getTobaccoType('Cigarettes').adjustmentRemoval = null;
      this.updateCalc5210('Cigarettes');
      this.model.getTobaccoType('Cigarettes').isQty5210Pristine = true;
    }
  }
  if(this.model.getTobaccoType('Chew') != null){
    this.model.getTobaccoType('Chew').isEnableAdjustment5210 = this.adjFlagChew;
    if(!this.model.getTobaccoType('Chew').isEnableAdjustment5210){
      this.model.getTobaccoType('Chew').adjustmentRemoval = null;
      this.updateCalc5210('Chew');
      this.model.getTobaccoType('Chew').isQty5210Pristine = true;
    }
  }
  if(this.model.getTobaccoType('Snuff') != null){
    this.model.getTobaccoType('Snuff').isEnableAdjustment5210 = this.adjFlagSnuff; 
    if(!this.model.getTobaccoType('Snuff').isEnableAdjustment5210){
      this.model.getTobaccoType('Snuff').adjustmentRemoval = null;
      this.updateCalc5210('Snuff');
      this.model.getTobaccoType('Snuff').isQty5210Pristine = true;
    } 
  }
  if(this.model.getTobaccoType('Pipe') != null){
   this.model.getTobaccoType('Pipe').isEnableAdjustment5210 = this.adjFlagPipe;
    if(!this.model.getTobaccoType('Pipe').isEnableAdjustment5210){
      this.model.getTobaccoType('Pipe').adjustmentRemoval = null;
      this.updateCalc5210('Pipe');
      this.model.getTobaccoType('Pipe').isQty5210Pristine = true;
    } 
  }
  if(this.model.getTobaccoType('RollYourOwn') != null){
    this.model.getTobaccoType('RollYourOwn').isEnableAdjustment5210 = this.adjFlagRyn; 
    if(!this.model.getTobaccoType('RollYourOwn').isEnableAdjustment5210){
      this.model.getTobaccoType('RollYourOwn').adjustmentRemoval = null;
      this.updateCalc5210('RollYourOwn');
      this.model.getTobaccoType('RollYourOwn').isQty5210Pristine = true;
    } 
  }
}
uncheckAllBoxes() {
  this.hasChanges.emit(true);
  this.adjFlagCigars = false;
  this.adjFlagCigarette = false;
  this.adjFlagChew = false;
  this.adjFlagSnuff = false;
  this.adjFlagPipe = false;
  this.adjFlagRyn = false;
  this.setEnableAdjustmentFlag();   
}


checkForSelectAll(event,type: string) {
  this.hasChanges.emit(true);
  this.setEnableAllAdjustment();
  this.setEnableAdjustmentFlag();
}
setEnableAllAdjustment(){
  if((this.adjFlagCigars || !this.model.showCigars) && (this.adjFlagCigarette || !this.model.showCigarettes) 
      && (this.adjFlagChew ||!this.model.showChews) && (this.adjFlagSnuff|| !this.model.showSnuffs) 
       && (this.adjFlagPipe || !this.model.showPipes) && (this.adjFlagRyn || !this.model.showRollYourOwn)) {
        this.adjFlagAll = true;
      }else{
      this.adjFlagAll = false;
    }

}

}

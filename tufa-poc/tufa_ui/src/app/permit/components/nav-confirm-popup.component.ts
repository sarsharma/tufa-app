/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class ConfirmPopup
 */
@Component({
  selector: '[nav-confirm-popup]',
  templateUrl: './nav-confirm-popup.template.html',
  encapsulation: ViewEncapsulation.None
})

export class NavConfirmPopup implements OnInit {
  @Input() navEnd;
  @Input() navStrt;
  documentDesc: string;
  router: Router;

  ngOnInit() {
     this.documentDesc = 'Are you sure you want to leave this report with unsaved changes?';
  }

  constructor( private _router: Router) {
    this.router = _router;
   }


  reset(flag: boolean) {
    jQuery('#nav-confirm-popup').modal('hide');
    if(flag)
      this.router.navigate(this.navEnd);
    else
    this.router.navigate(this.navStrt);

  }


}

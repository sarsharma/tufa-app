
/*
@author : Deloitte

this is Component for edit permit history.
*/

import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CompanyPermitHistory } from '../../company/models/company-permit-history.model';
import { Company } from '../../company/models/company.model';
import { CompanyService } from '../../company/services/company.service';
import { Document } from "../models/document.model";
import { Permit } from '../models/permit.model';
import { PermitExcludeService } from "../services/permit-exclude.service";
import { PermitPeriodService } from '../services/permit-period.service';
import { PermitHistoryCommentsService } from '../services/permit.history.comments';
import { ShareDataService } from './../../core/sharedata.service';


declare var jQuery: any;

@Component({
  selector: '[permit-history]',
  templateUrl: './permit-history.template.html',
  styleUrls: ['./permit-history.style.scss'],
  providers: [CompanyService, PermitHistoryCommentsService, PermitPeriodService, PermitExcludeService],
  encapsulation: ViewEncapsulation.None
})

export class PermitHistoryPage implements OnInit, OnDestroy {
  router: Router;
  data: any[] = [];
  permitDocData: any[] = [];


  sub: any;
  model = new Permit();
  selectedMonthlyReport = new CompanyPermitHistory();
  msg: string = "Are you sure you want to delete this monthly report?";
  title: string = "Delete Monthly Report";
  company = new Company();
  permitId: any;
  contacts: any[];
  permitStatus: any[];
  permitHistory: any[];
  showErrorFlag: boolean;
  alerts: Array<Object>;
  busy: Subscription;
  docId: any;

  //Sort Contents in DataTable
  public permithistory = "permithistory";
  sortBy: string = "sortMonthYear";
  sortOrder: string[] = ["desc"];
  // Recon params
  previousRoute: string;
  quarter: number;
  fiscalYear: number;
  createdDate: string;
  submitted: string;
  cigarsubmitted: string;
  months: string[];
  breadcrumb: string;
  qatabfrom: string;

  shareData: ShareDataService;

  permitTypeCd: string;
  permitNumber: string;  // use local variable to bind instead of model
  public mask = [/[A-Za-z]/, /[A-Za-z]/, '-', /[A-Za-z]/, /[A-Za-z]/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];
  closeddateid: string = "close-date";
  activedateid: string = "active-date";
  ttbissuedateid: string = "ttb-issue-date";
  showConfirmPopup: Boolean = false;
  showInactiveerror: Boolean = false;
  permitHistorycomments: any[] = []; showEditDocumentPopup: boolean = false;
  showConfirmDeletePopup: boolean = false; date2: Date;
  date3: Date;
  date4: Date;
  date5: Date;
  selectedDocument: Document;
  downloading: Subscription;
  newDoc: Document = new Document();

  datepickerOptsClsd = {
    autoclose: true,
    todayBtn: 'linked',
    todayHighlight: true,
    assumeNearbyYear: true,
    requiredErrorMsg: "Error:TUFA Close Date is required",
    title: 'Enter TUFA Close Date in MM/DD/YYYY format'
  };

  datepickerOptsActv = {
    autoclose: true,
    todayBtn: 'linked',
    todayHighlight: true,
    assumeNearbyYear: true,
    endDate: new Date(),
    requiredErrorMsg: "Error:TUFA Active Date is required",
    title: 'Enter TUFA Active Date in MM/DD/YYYY format'
  };

  datepickerttbOptsClsd = {
    autoclose: true,
    todayBtn: 'linked',
    todayHighlight: true,
    assumeNearbyYear: true,
    requiredErrorMsg: "Error: TTB Close Date is required",
    title: 'Enter TTB Close Date in MM/DD/YYYY format'
  };

  datepickerttbOptsActv = {
    autoclose: true,
    todayBtn: 'linked',
    todayHighlight: true,
    assumeNearbyYear: true,
    endDate: new Date(),
    requiredErrorMsg: "Error: TTB Active Date is required",
    title: 'Enter TTB Active Date in MM/DD/YYYY format'
  };

  excludeStatusFlag: boolean = false;
  constructor(private companyService: CompanyService,
    private _permitPeriodService: PermitPeriodService,
    router: Router,
    private route: ActivatedRoute,
    private _shareData: ShareDataService,
    private PermitExcludeService: PermitExcludeService,
    private permitHistoryCommentsService: PermitHistoryCommentsService) {
    this.router = router;
    this.shareData = _shareData;
    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages When Create Company Fails. e.g Duplicate EIN'
      }
    ];

    this.showErrorFlag = false;

  }


  ngOnInit() {
    this.getParamsForReconNavigation();
    this.sub = this.route.params.subscribe(params => {
      if (params['permitId'] !== undefined) {
        this.permitId = params['permitId'];
        //4844 this.loadDocs(this.permitId);
        this.busy = this.companyService.getPermitHistory(this.permitId)
          .subscribe(data => {
            if (data) {
              this.model = <Permit>data;
              // since we are using PermitNumber for databinding - we need to copy from model to local variable
              this.permitNumber = this.formatPermitNumberWithHypen(this.model.permitNum);
              if (this.model.issueDt) this.date2 = new Date(this.model.issueDt);
              if (this.model.closeDt) this.date3 = new Date(this.model.closeDt);
              if (this.model.ttbstartDt) this.date4 = new Date(this.model.ttbstartDt);
              if (this.model.ttbendDt) this.date5 = new Date(this.model.ttbendDt);
              this.permitTypeCd = this.model.permitTypeCd;
              this.company = this.model.company;
              this.data = this.model.permitHistoryList;
              this.getExcludeStatus(this.company.companyId, this.permitId);

            }
          });
      } else {
        this.model = new Permit();
        this.permitNumber = "";
      }
    });
    this.permitHistoryCommentsService.getPermitHistoryComments(this.permitId).subscribe(data => {
      if (data) {
        this.permitHistorycomments = data;
        console.log(this.permitHistorycomments);
      }
    });
    this.showErrorFlag = false;
  }

  getLatestComment(data) {
    this.permitHistorycomments = [];
    this.permitHistoryCommentsService.getPermitHistoryComments(this.permitId).subscribe(data => {
      if (data) {
        this.permitHistorycomments = data;
        console.log(this.permitHistorycomments);
      }
    });
  }


  ngOnDestroy() {
    if (this.busy) {
      this.busy.unsubscribe();
    }
    if (this.downloading) {
      this.downloading.unsubscribe();
    }



  }
  /**
   * loading the permit documents
   * @param permitId 
   */
  loadDocs(permitId: string) {

    this.busy = this._permitPeriodService.loadPermitDocs(permitId).subscribe(
      data => {
        if (data) {
          let nd = data;
          let newData = [];
          for (let i = 0; i < nd.length; i++) {
            newData.push(nd[i]);
          }
          this.permitDocData = newData;
        }

      });
  }
  setSelectedDocument(doc: Document, mode: string) {
    this.selectedDocument = Object.assign({}, doc);
    if (mode === 'edit') {
      this.showEditDocumentPopup = true;
    } else if (mode === 'delete') {
      this.showConfirmDeletePopup = true;
    }
  }
  documentsGridRefresh(flag: boolean, mode: string) {

    this.shareData.permitId = this.permitId;
    if (flag) {
      this.loadDocs(this.permitId);
    }

    if (mode === 'edit') {
      this.showEditDocumentPopup = false;
    } else if (mode === 'delete') {
      this.showConfirmDeletePopup = false;
    }
  }

  formatPermitNumberWithHypen(val: String) {
    let formattedValue = val.substring(0, 2) + "-" + val.substring(2, 4) + "-" + val.substring(4);
    return formattedValue.toUpperCase();
  }

  /*
      permitInfoRefresh() is called when information related to a permit is successfully
      updated such as permit comments

  */
  /**
   *
   *permit
   * @param {boolean} ispermitEdited
   *
   * @memberOf PermitHistoryPage
   */
  permitInfoRefresh(ispermitEdited: boolean) {
    if (ispermitEdited && this.permitId) {
      this.busy = this.companyService.getPermitHistory(this.permitId)
        .subscribe(data => {
          if (data) {
            this.model = <Permit>data;
            // since we are using PermitNumber for databinding - we need to copy from model to local variable
            this.permitNumber = this.formatPermitNumberWithHypen(this.model.permitNum);
            if (this.model.issueDt) this.date2 = new Date(this.model.issueDt);
            if (this.model.closeDt) this.date3 = new Date(this.model.closeDt);
            if (this.model.ttbstartDt) this.date4 = new Date(this.model.ttbstartDt);
            if (this.model.ttbendDt) this.date5 = new Date(this.model.ttbendDt);
            this.permitTypeCd = this.model.permitTypeCd;
            this.company = this.model.company;
            this.data = this.model.permitHistoryList;
            if (jQuery('#confirm-popup')) {
              jQuery('#confirm-popup').modal('hide');
            }
          }
        });
    }
  }

  SavePermitHistory(permit: Permit) {
    jQuery('#permitHistoryForm').parsley().validate();

    // unmask permit number before proceding ahead
    // transforming the local bound variable to the model. if we modify the model directly then
    // RegEx pattern validation fails on the UI side.
    let permitNum: string = this.permitNumber;
    permit.permitNum = permitNum = '' ? '' : permitNum.replace(/-/g, "");

    if (jQuery('#permitHistoryForm').parsley().isValid()) {
      //TUFA Inactive date is earlier than the TUFA Active date.
      if (this.date2 != null && this.date3 != null && (this.date3 <= this.date2)) {
        this.showInactiveerror = true;
        return;
      }
      if (this.date2) {
        permit.issueDt = this.date2.toString();
        permit.ttbstartDt = this.date4.toString();
      }
      if (permit.permitStatusTypeCd === "CLSD" && this.date3) {
        permit.closeDt = this.date3.toString();
        permit.ttbendDt = this.date5.toString();
      }
      this.companyService.updatePermit(permit).subscribe(data => {
        if (data) {
          this.router.navigate(["/app/company/details", permit.company.companyId]);
        }
      }, error => {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({ type: 'warning', msg: error });
      });
    }
  }

  toPeriodofActivity(history: CompanyPermitHistory) {
    this.setParamsForReconNavigation();
    this.shareData.companyName = history.companyName;
    this.shareData.permitNum = history.permitNum;
    this.shareData.period = history.displayMonthYear;
    this.shareData.permitId = history.permitId;
    this.shareData.periodId = history.reportPeriodId;
    this.shareData.companyId = history.companyId;
    this.shareData.periodStatus = history.reportStatusCd;
    this.shareData.ein = this.shareData.ein;
  }


  toggle(permit: Permit) {
    if (permit.permitStatusTypeCd === 'ACTV')
      this.showErrorFlag = false;
  }

  gotoCompany(company: Company) {
    this.router.navigate(["/app/company/details", company.companyId]);
  }

  gotoSearchCompany() {
    this.router.navigate(["/app/company/search"]);
  }

  toAssessments() {
    this.setParamsForReconNavigation();
    this.router.navigate(["/app/assessments"]);
  }

  toQuarterlyAssessment() {
    this.setParamsForReconNavigation();
    this.router.navigate(["/app/assessments/quarterly"]);
  }

  toCigarAssessment() {
    this.setParamsForReconNavigation();
    this.router.navigate(["/app/assessments/cigars"]);
  }

  toAnnualTrueup() {
    this.setParamsForReconNavigation();
    this.router.navigate(["/app/assessments/annual"]);
  }

  toCompareDetails() {
    if (this.shareData.detailsSrc.indexOf('noncigar') != -1)
      this.router.navigate(["/app/assessments/comparenoncigar"]);
    else if (this.shareData.detailsSrc.indexOf('cigar') != -1)
      this.router.navigate(["/app/assessments/comparecigar"]);
  }

  setParamsForReconNavigation() {
    let localSrc = this.shareData.detailsSrc;
    let localflag = this.shareData.acceptanceFlag;
    let localtype = this.shareData.permitType;
    let localClass = this.shareData.tobaccoClass;
    let localCompany = this.shareData.companyName;
    let filetab = this.shareData.filemgmttab;
    this.shareData.reset();
    this.shareData.filemgmttab = filetab;
    this.shareData.companyName = localCompany;
    this.shareData.acceptanceFlag = localflag;
    this.shareData.permitType = localtype;
    this.shareData.tobaccoClass = localClass;
    this.shareData.detailsSrc = localSrc;
    this.shareData.previousRoute = this.previousRoute;
    this.shareData.breadcrumb = this.breadcrumb;
    this.shareData.quarter = this.quarter;
    this.shareData.fiscalYear = this.fiscalYear;
    this.shareData.createdDate = this.createdDate;
    this.shareData.submitted = this.submitted;
    this.shareData.cigarsubmitted = this.cigarsubmitted;
    this.shareData.months = this.months;
    this.shareData.qatabfrom = this.qatabfrom;
    this.shareData.docId = this.docId;
  }

  getParamsForReconNavigation() {
    this.previousRoute = this.shareData.previousRoute;
    this.breadcrumb = this.shareData.breadcrumb;
    this.quarter = this.shareData.quarter;
    this.fiscalYear = this.shareData.fiscalYear;
    this.submitted = this.shareData.submitted;
    this.cigarsubmitted = this.shareData.cigarsubmitted;
    this.createdDate = this.shareData.createdDate;
    this.months = this.shareData.months;
    this.qatabfrom = this.shareData.qatabfrom;
    this.docId = this.shareData.docId;
  }

  deleteMonthlyReport(history: CompanyPermitHistory) {
    this.busy = this.companyService.deleteMonthlyReport(history)
      .subscribe(data => {
        if (data) {
          this.permitInfoRefresh(true);
          this.showConfirmPopup = false;
          jQuery('#confirm-popup').modal('hide');
        }
      });
  }

  setSelectedMR(companyPermitHistory: CompanyPermitHistory) {
    this.selectedMonthlyReport = Object.assign({}, companyPermitHistory);
    this.showConfirmPopup = true;
  }

  toFileMgmt() {
    this.setParamsForReconNavigation();
    this.router.navigate(["app/permit-mgmt/address-mgmt", { docId: this.docId }]);
  }
  downloadSelectedDocument(doc: Document) {
    //this.selectedDocument = Object.assign({}, doc);
    this.downloadAttach(doc);
  }


  getExcludeStatus(companyId, permitNumber) {
    this.busy = this.PermitExcludeService.getExcludedStatus(companyId, permitNumber).subscribe(
      data => {
        if (data.length) {
          let excludeDetails: any[] = data;
          excludeDetails.forEach(element => {
            if (element.excludeScopeId != 3) {
              this.excludeStatusFlag = true;
            }

          });
        }
      }, (error) => {
        console.log(error);
      })
  }
  downloadAttach(doc: Document) {
    this.downloading = this._permitPeriodService.downloadPermitAttachment(doc.documentId).subscribe(
      data => {
        if (data) {
          this.download(atob(data.reportPdf), data.docDesc, "application/pdf");
        }
      });
  }
  download(data: any, strFileName: any, strMimeType: any) {

    let self: any = window, // this script is only for browsers anyway...
      defaultMime = "application/octet-stream", // this default mime also triggers iframe downloads
      mimeType = strMimeType || defaultMime,
      payload = data,
      url = !strFileName && !strMimeType && payload,
      anchor = document.createElement("a"),
      toString = function (a) { return String(a); },
      myBlob = (self.Blob || self.MozBlob || self.WebKitBlob || toString),
      fileName = strFileName || "download",
      blob,
      reader;
    myBlob = myBlob.call ? myBlob.bind(self) : Blob;

    if (String(this) === "true") { // reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
      payload = [payload, mimeType];
      mimeType = payload[0];
      payload = payload[1];
    }


    // go ahead and download dataURLs right away
    if (/^data\:[\w+\-]+\/[\w+\-\.]+[,;]/.test(payload)) {

      if (payload.length > (1024 * 1024 * 1.999) && myBlob !== toString) {
        payload = dataUrlToBlob(payload);
        mimeType = payload.type || defaultMime;
      } else {
        return navigator.msSaveBlob ?  // IE10 can't do a[download], only Blobs:
          navigator.msSaveBlob(dataUrlToBlob(payload), fileName) :
          saver(payload, false); // everyone else can save dataURLs un-processed
      }

    } else {// not data url, is it a string with special needs?
      if (/([\x80-\xff])/.test(payload)) {
        let i = 0, tempUiArr = new Uint8Array(payload.length), mx = tempUiArr.length;
        for (i; i < mx; ++i) tempUiArr[i] = payload.charCodeAt(i);
        payload = new myBlob([tempUiArr], { type: mimeType });
      }
    }
    blob = payload instanceof myBlob ?
      payload :
      new myBlob([payload], { type: mimeType });


    function dataUrlToBlob(strUrl) {
      let parts = strUrl.split(/[:;,]/),
        type = parts[1],
        decoder = parts[2] === "base64" ? atob : decodeURIComponent,
        binData = decoder(parts.pop()),
        mx = binData.length,
        i = 0,
        uiArr = new Uint8Array(mx);

      for (i; i < mx; ++i) uiArr[i] = binData.charCodeAt(i);

      return new myBlob([uiArr], { type: type });
    }

    function saver(url: any, winMode: any) {

      if ('download' in anchor) { // html5 A[download]
        anchor.href = url;
        anchor.setAttribute("download", fileName);
        anchor.className = "download-js-link";
        anchor.innerHTML = "downloading...";
        anchor.style.display = "none";
        document.body.appendChild(anchor);
        setTimeout(function () {
          anchor.click();
          document.body.removeChild(anchor);
          if (winMode === true) { setTimeout(function () { self.URL.revokeObjectURL(anchor.href); }, 250); }
        }, 66);
        return true;
      }

      // handle non-a[download] safari as best we can:
      if (/(Version)\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//.test(navigator.userAgent)) {
        if (/^data:/.test(url)) url = "data:" + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
        if (!window.open(url)) { // popup blocked, offer direct download:
          // tslint:disable-next-line:max-line-length
          if (confirm("Displaying New Document\n\nUse Save As... to download, then click back to return to this page.")) { location.href = url; }
        }
        return true;
      }

      // do iframe dataURL download (old ch+FF):
      var f = document.createElement("iframe");
      document.body.appendChild(f);

      if (!winMode && /^data:/.test(url)) { // force a mime that will download:
        url = "data:" + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
      }
      f.src = url;
      setTimeout(function () { document.body.removeChild(f); }, 333);

    }// end saver


    if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or URL)
      return navigator.msSaveBlob(blob, fileName);
    }

    if (self.URL) { // simple fast and modern way using Blob and URL:
      saver(self.URL.createObjectURL(blob), true);
    } else {
      // handle non-Blob()+non-URL browsers:
      if (typeof blob === "string" || blob.constructor === toString) {
        try {
          return saver("data:" + mimeType + ";base64," + self.btoa(blob), false);
        } catch (y) {
          return saver("data:" + mimeType + "," + encodeURIComponent(blob), false);
        }
      }

      // Blob but not URL support:
      reader = new FileReader();
      reader.onload = function (e) {
        saver(this.result, false);
      };
      reader.readAsDataURL(blob);
    }
    return true;
  };

}

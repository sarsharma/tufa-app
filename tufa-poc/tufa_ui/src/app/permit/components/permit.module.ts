/*
@author : Deloitte

this is Component for holding permit module.
*/

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FileUploadModule } from 'ng2-file-upload';
import { CompanyService } from '../../company/services/company.service';
// Service Components
import { AuthService } from '../../login/services/auth.service';
import { SharedModule } from '../../shared/shared.module';
import { MonthlyReportCommentService } from '../services/monthly.report.comment.service';
import { MonthlyReportEventService } from '../services/monthly.report.event.service';
import { PendingChangesGuard } from '../services/pending-changes.guard';
import { PermitImpReportService } from '../services/permit.imp.report.service';
import { PermitMfgReportService } from '../services/permit.mfg.report.service';
import { Form7501DataFilterPipe } from '../util/form7501.pipe';
import { Form5000ABPipe } from '../util/form5000AB.pipe';
import { MonthlyReportCommentGridComponent } from './comments/comments.grid.component';
import { ConfirmPopup } from './confirm-popup.component';
import { ConfirmPopupPermit } from './confirm-popup-permit.component';
import { EditCompanyPane } from './edit-company-pane.component';
import { EditDocumentPopup } from './edit-document.component';
import { EditDocumentPermitPopup } from './edit-document-permit.component';
import { ImpForm3852 } from './importer/impform-3852.component';
import { ImpForm5220 } from './importer/impform-5220.component';
import { ImpForm7501 } from './importer/impform-7501.component';
import { ImpPermitPeriodPage } from './importer/imppermit-report.component';
// Form Components
import { Form3852 } from './manufacturer/form-3852.component';
import { Form5000 } from './manufacturer/form-5000.component';
import { Form5210 } from './manufacturer/form-5210.component';
import { PermitPeriodPage } from './manufacturer/permit-report.component';
// Page Components
import { PermitHistoryPage } from './permit-history.component';
import { PermitReportReconReport } from './permit.report.recon.component';
import { PreviewPage } from './preview-page.component';
import { MonthlyReportCommentPopup } from './comments/comment.popup.component';
import { HttpClient } from '@angular/common/http';
import { NavConfirmPopup } from './nav-confirm-popup.component';

export const routes = [
  { path: '', component: PermitHistoryPage, pathMatch: 'full' },
  { path: 'history/:permitId', component: PermitHistoryPage },
  { path: 'manreport', component: PermitPeriodPage, canDeactivate: [PendingChangesGuard]},
  { path: 'impreport', component: ImpPermitPeriodPage, canDeactivate: [PendingChangesGuard] },
  { path: 'document/:docId', component: PreviewPage }
];

@NgModule({
    imports: [CommonModule, SharedModule, FileUploadModule, RouterModule.forChild(routes)],
    declarations: [PermitHistoryPage, PermitPeriodPage, Form3852, NavConfirmPopup,
    Form5000, Form5210, EditCompanyPane, PermitReportReconReport, ImpForm3852, ImpForm7501, ImpForm5220, PreviewPage,
    ImpPermitPeriodPage, EditDocumentPopup,EditDocumentPermitPopup,ConfirmPopup,ConfirmPopupPermit, MonthlyReportCommentGridComponent, MonthlyReportCommentPopup, Form7501DataFilterPipe, Form5000ABPipe],
    providers: [CompanyService, PermitMfgReportService, PermitImpReportService, HttpClient, AuthService, PendingChangesGuard,
    MonthlyReportCommentGridComponent, MonthlyReportCommentPopup, MonthlyReportEventService, MonthlyReportCommentService]
})

export class PermitModule {
  static routes = routes;
}

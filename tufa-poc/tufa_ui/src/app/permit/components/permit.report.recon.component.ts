import { Component, Input, OnInit, ViewEncapsulation, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { PermitPeriodService } from '../services/permit-period.service';

@Component({
    selector: '[permitreport-recon]',
    templateUrl: './permit.report.recon.template.html',
    encapsulation: ViewEncapsulation.None

})
export class PermitReportReconReport implements OnInit {

    @Input() permitId;
    @Input() periodId;
    @Input() reconStatusData;

    @Output() onReconApproval: EventEmitter<any> = new EventEmitter();

    periodData: any = {};
    disableBtn: boolean = false;

    constructor(private permitPeriodService: PermitPeriodService) { }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "reconStatusData") {
                let chng = changes[propName];
                if(chng.currentValue && chng.currentValue.reconStatusTypeCd) {
                    this.periodData = chng.currentValue;
                    this.disableBtn = !chng.currentValue.reconStatusTypeCd.includes("Not");
                }
            }
        }
    }

    ngOnInit() {
        this.getReconStatus();
    }


    getReconStatus() {
        this.permitPeriodService.getPermitReportReconStatus(this.permitId, this.periodId)
            .subscribe(
            data => {
                if (data) {
                    this.periodData = data;
                    if (!data.reconStatusTypeCd.includes('Not Approved')){
                            this.disableBtn = true;
                    }
                }
            }
            );
    }

    updateReconStatus() {
        this.permitPeriodService.updatePermitReportReconStatus(this.permitId, this.periodId).subscribe(
            data => {
                if (data) {
                    this.periodData = data;
                    this.disableBtn = true;
                    this.onReconApproval.emit(true);
                }
            }
        );
    }
}
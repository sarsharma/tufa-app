/*
@author : JBS

this is Component for editing company address.
*/

import { Component, Input, ViewEncapsulation, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { PermitPeriodService } from '../services/permit-period.service';
import { Subscription } from 'rxjs';

declare var jQuery: any;
/**
 * 
 * 
 * @export
 * @class ConfirmPopup
 */
@Component({
  selector: '[preview-page]',
  templateUrl: './preview-page.template.html',
  styleUrls: ['./preview-page.style.scss'],
  providers: [PermitPeriodService],
  encapsulation: ViewEncapsulation.None
})

export class PreviewPage implements OnInit, OnDestroy  {
  router: Router;
  pdfSource: any;
  pdfData: any;
  docId: number;
  rotation: number = 0;
  zoom: number = 1.5;
  originalSize: boolean = true;
  showAll: boolean = true;
  renderText: boolean = true;
  load: Subscription;

  constructor(private _permitPeriodService: PermitPeriodService, private route: ActivatedRoute, router: Router) {
     this.router = router;
  }

  ngOnInit() {
    jQuery('nav.sidebar').hide();
    this.route.params.subscribe(params => {
      if (params['docId'] !== undefined) {
        this.docId = params['docId'];
        this.load = this._permitPeriodService.downloadAttachment(params['docId']).subscribe(
            data => {
                if (data) {
                   this.pdfData = data;
                   let reader = new FileReader();
                   reader.onload = (e) => {
                       this.pdfSource = reader.result;
                   };
                   reader.readAsArrayBuffer(this.viewContent(atob(data.reportPdf), data.docDesc, "application/json"));
                }
            });
      }
    });
  }

  ngOnDestroy() {
    jQuery('nav.sidebar').show();
    if(this.load){
        this.load.unsubscribe();
    }
  }

  incrementZoom(amount: number) {
    this.zoom += amount;
  }

  rotate(angle: number) {
    this.rotation += angle;
  }

 viewContent(data: any, strFileName: any, strMimeType: any): any {

        let self: any = window,
            mimeType = strMimeType || "application/octet-stream",
            payload = data,
            toString = (a) => { return String(a); },
            myBlob = (self.Blob || self.MozBlob || self.WebKitBlob || toString),
            fileName = strFileName || "download";
            myBlob = myBlob.call ? myBlob.bind(self) : Blob;

        if (String(this) === "true") {
            payload = [payload, mimeType];
            mimeType = payload[0];
            payload = payload[1];
        }
        function dataUrlToBlob(strUrl) {
            let parts = strUrl.split(/[:;,]/),
                type = parts[1],
                decoder = parts[2] === "base64" ? atob : decodeURIComponent,
                binData = decoder(parts.pop()),
                mx = binData.length,
                i = 0,
                uiArr = new Uint8Array(mx);

            for (i; i < mx; ++i) uiArr[i] = binData.charCodeAt(i);

            return new myBlob([uiArr], { type: type });
        }

        return dataUrlToBlob(payload);
    }


    downloadAttach() {
        if (this.pdfData) {
            return this.download(atob(this.pdfData.reportPdf), this.pdfData.docDesc, "application/json");
        }
    }

     download(data: any, strFileName: any, strMimeType: any): any {

        let self: any = window,
            defaultMime = "application/octet-stream",
            mimeType = strMimeType || defaultMime,
            payload = data,
            url = !strFileName && !strMimeType && payload,
            anchor = document.createElement("a"),
            toString = function (a) { return String(a); },
            myBlob = (self.Blob || self.MozBlob || self.WebKitBlob || toString),
            fileName = strFileName || "download",
            blob,
            reader;
        myBlob = myBlob.call ? myBlob.bind(self) : Blob;

        if (String(this) === "true") {
            payload = [payload, mimeType];
            mimeType = payload[0];
            payload = payload[1];
        }


        //go ahead and download dataURLs right away
        if (/^data\:[\w+\-]+\/[\w+\-\.]+[,;]/.test(payload)) {

            if (payload.length > (1024 * 1024 * 1.999) && myBlob !== toString) {
                payload = dataUrlToBlob(payload);
                mimeType = payload.type || defaultMime;
            } else {
                return navigator.msSaveBlob ?  // IE10 can't do a[download], only Blobs:
                    navigator.msSaveBlob(dataUrlToBlob(payload), fileName) :
                    saver(payload, false); // everyone else can save dataURLs un-processed
            }

        } else {//not data url, is it a string with special needs?
            if (/([\x80-\xff])/.test(payload)) {
                let i = 0, tempUiArr = new Uint8Array(payload.length), mx = tempUiArr.length;
                for (i; i < mx; ++i) tempUiArr[i] = payload.charCodeAt(i);
                payload = new myBlob([tempUiArr], { type: mimeType });
            }
        }
        blob = payload instanceof myBlob ?
            payload :
            new myBlob([payload], { type: mimeType });

        function dataUrlToBlob(strUrl) {
            let parts = strUrl.split(/[:;,]/),
                type = parts[1],
                decoder = parts[2] === "base64" ? atob : decodeURIComponent,
                binData = decoder(parts.pop()),
                mx = binData.length,
                i = 0,
                uiArr = new Uint8Array(mx);

            for (i; i < mx; ++i) uiArr[i] = binData.charCodeAt(i);

            return new myBlob([uiArr], { type: type });
        }

        function saver(url: any, winMode: any) {

            if ('download' in anchor) {
                anchor.href = url;
                anchor.setAttribute("download", fileName);
                anchor.className = "download-js-link";
                anchor.innerHTML = "downloading...";
                anchor.style.display = "none";
                document.body.appendChild(anchor);
                setTimeout(function () {
                    anchor.click();
                    document.body.removeChild(anchor);
                    if (winMode === true) { setTimeout(function () { self.URL.revokeObjectURL(anchor.href); }, 250); }
                }, 66);
                return true;
            }

            // handle non-a[download] safari as best we can:
            if (/(Version)\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//.test(navigator.userAgent)) {
                if (/^data:/.test(url)) url = "data:" + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
                if (!window.open(url)) { // popup blocked, offer direct download:
                    // tslint:disable-next-line:max-line-length
                    if (confirm("Displaying New Document\n\nUse Save As... to download, then click back to return to this page.")) { location.href = url; }
                }
                return true;
            }

            //do iframe dataURL download (old ch+FF):
            let f = document.createElement("iframe");
            document.body.appendChild(f);

            if (!winMode && /^data:/.test(url)) { // force a mime that will download:
                url = "data:" + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
            }
            f.src = url;
            setTimeout(function () { document.body.removeChild(f); }, 333);

        }//end saver

        if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or URL)
            return navigator.msLaunchUri(blob, fileName);
        }

        if (self.URL) { // simple fast and modern way using Blob and URL:
            saver(self.URL.createObjectURL(blob), true);
        } else {
            // handle non-Blob()+non-URL browsers:
            if (typeof blob === "string" || blob.constructor === toString) {
                try {
                    return saver("data:" + mimeType + ";base64," + self.btoa(blob), false);
                } catch (y) {
                    return saver("data:" + mimeType + "," + encodeURIComponent(blob), false);
                }
            }

            // Blob but not URL support:
            reader = new FileReader();
            reader.onload = function (e) {
                saver(this.result, false);
            };
            reader.readAsDataURL(blob);
        }
        return true;
    };

}

export class CommentModel {

    formId: number;
    tobaccoClassId:number;
    formTypeCd: string;
    commentTitle: string;
    userComment: string;
    author:string;
    commentDate: Date;
    commentSeq: number;
    resolveComment:boolean;
    attachmentsCount: number = 0;
    modifiedBy: string;
    
    constructor(){}

}
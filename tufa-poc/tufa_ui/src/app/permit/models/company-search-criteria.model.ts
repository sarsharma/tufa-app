/*
@author : tgunter
This is a model to hold Permit History Search criteria and results.
*/
import {CompanyPermits} from '../../company/models/company-permits.model';

export class CompanySearchCriteria {
    companyNameFilter: string;
    companyIdFilter: number;
    companyStatusFilters: string[]=[];
    companyEINFilter: string;
    companyCreatedDtFilter: Date;
    welcomeLetterSentDtFilter: Date;
    emailAddressFlagFilter: string;
    physicalAddressFlagFilter: string;
    welcomeLetterFlagFilter: string;
    results: CompanyPermits[];
}
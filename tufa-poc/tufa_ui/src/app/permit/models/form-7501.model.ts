export class Form7501Model {
    ttname: string;
    tobaccoClassId: number;
    formId: number;
    formNum: string;
    lineNum: number;

    valorem: number;
    qty: number;
    taxAmt: number;
    calTaxAmt: number;
    calcTaxRate: number;
    type: string;
    filerCode: string;
    amtpristine: boolean = true;
    taxAmtPristine: number;
    taxDiff: number;

    constructor(){}
}

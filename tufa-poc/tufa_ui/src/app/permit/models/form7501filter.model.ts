import { Form7501Model } from "./form-7501.model";


export class Form7501Filter {
    lineNumFilter: String;
    taxDiffFilter: String;    
    cigarTypeFilter: String;
    taxAmtFilter: String;
    qtyFilter: String;
    taxRateFilter: String;
    results: Form7501Model[];
}

export class Form7501FilterData {
    private _taxDiffFilter: String;    
    private _taxAmtFilter: String;
    private _qtyFilter: String;
    private _lineNumFilter: String;
    private _taxRateFilter: String;
    private _cigarTypeFilter: String;

    get taxDiffFilter(): any {
        return this._taxDiffFilter;
    }
    set taxDiffFilter(value: any) {
        this._taxDiffFilter = value;
    }

    get taxAmtFilter(): any {
        return this._taxAmtFilter;
    }
    set taxAmtFilter(value: any) {
        this._taxAmtFilter = value;
    }

    get qtyFilter(): any {
        return this._qtyFilter;
    }
    set qtyFilter(value: any) {
        this._qtyFilter = value;
    }

    get lineNumFilter(): any {
        return this._lineNumFilter;
    }
    set lineNumFilter(value: any) {
        this._lineNumFilter = value;
    }

    get taxRateFilter(): any {
        return this._taxRateFilter;
    }
    set taxRateFilter(value: any) {
        this._taxRateFilter = value;
    }

    get cigarTypeFilter(): any {
        return this._cigarTypeFilter;
    }
    set cigarTypeFilter(value: any) {
        this._cigarTypeFilter = value;
    }
 }

import { TobaccoTypeConst } from './tobaccotype.const';
import { ImporterTobaccoSubType } from '../imptobacco-subtype.model';
import { TobaccoType } from '../imptobacco-type.model';
import { Form7501Model } from '../form-7501.model';
import { CommentModel } from '../comment/comment.model';
import { MonthlyReportEventService } from '../../services/monthly.report.event.service';

const CIGARETTES = 'Cigarettes';
const CIGARS = 'Cigars';
const CHEW = 'Chew';
const SNUFF = 'Snuff';
const PIPE = 'Pipe';
const RYO = 'RollYourOwn';
const PIPE_RYO = 'PipeRYO';
const CHEW_SNUFF = 'ChewandSnuff';
const LARGE = 'Large';
const SMALL = 'Small';
const VALOREM = 'Ad Valorem';
const FORM_3852 = '3852';
const FORM_52206 = '5220.6';
const FORM_7501 = '7501';
const VALID = 'Valid';
const ERROR = 'Error';
const NO_SELECTION = 'None';

declare var jQuery;

export class PermitImpReport {

    public tobaccoType: TobaccoType;
    private ttNamesArray = [CIGARETTES, CIGARS, CHEW, SNUFF, PIPE, RYO];
    public tobaccoClassMap = {};


    constructor(private monthlyReportService: MonthlyReportEventService) { }

    public populateFormDetails(data, ttype: TobaccoType) {

        this.tobaccoType = ttype;
        let permitId = data.permitId;
        let periodId = data.periodId;
        this.tobaccoType.permitId = permitId;
        this.tobaccoType.periodId = periodId;

        let tobaccoClasses = data.permit.tobaccoClasses;
        let submittedForms = data.submittedForms;

        if (tobaccoClasses)
            this.createTobaccoTypes(tobaccoClasses);
        else {
            console.error('tobacco class constants are empty or undefined');
            return;
        }

        this.populateForms(submittedForms);

        /** Calculate tax or qty difference calculations on each form as indicated by the
         flag. This neeeds to be done if tax or qty difference valuse are coming in as
         null or undefined **/

        let ttypes: ImporterTobaccoSubType[] = this.tobaccoType.getTobaccoTypes();

        // tslint:disable-next-line:forin
        for (let i in ttypes) {
            let tobt = ttypes[i];
            if (tobt.trggTaxDiffCalc3852) tobt.calculateTaxDiff3852();
            if (tobt.trggQtyDiffCalc5220) tobt.calculateQtyDiff5220();
            if (tobt.trggTaxDiffCalc7501) tobt.calculateTaxAmount7501(tobt.taxAmtTotal7501);
        }

        return this.tobaccoType;
    }


    public createTobaccoTypes(tobaccoClasses: any[]) {

        // tslint:disable-next-line:forin
        for (let tclassKey in tobaccoClasses) {
            //populate the tobacco class map 
            this.tobaccoClassMap[tobaccoClasses[tclassKey].tobaccoClassId] = tobaccoClasses[tclassKey].tobaccoClassNm;

            let ttName: string = tobaccoClasses[tclassKey].tobaccoClassNm;
            let ttClassRate: number = tobaccoClasses[tclassKey].tobaccoClassNm === "Cigarettes" ?
                tobaccoClasses[tclassKey].taxRate / 1000 : tobaccoClasses[tclassKey].taxRate;
            let ttConversionRate: number = tobaccoClasses[tclassKey].conversionRate;
            let ttClassId: number = tobaccoClasses[tclassKey].tobaccoClassId;
            let ttParentClassId: number = tobaccoClasses[tclassKey].parentClassId;
            let ttThreshold: number = tobaccoClasses[tclassKey].thresholdValue;
            this.tobaccoType.addTobaccoTypeConsts(new TobaccoTypeConst(ttName, ttClassRate, ttClassId, ttParentClassId, ttConversionRate, ttThreshold));
        }

        // set tt class for large and small, roll, pipe, chew and snuff in the parent class
        this.tobaccoType.ttConstantsArray.forEach(function (item: TobaccoTypeConst, index, array) {
            let parent: TobaccoTypeConst;
            if (item.parentTobaccoClassId) {
                parent = this.tobaccoType.getTTConstByClassId(item.parentTobaccoClassId);
                parent.childTobaccoTypeConst.push(item);
            }
        }, this);
    }

    public populateForms(submittedForms: any[]) {

        let isForm5220Include = (submittedForms && submittedForms.length > 0) ? false : true;

        //clear up form ids before setting formIds retrieved from the backend.
        if(this.tobaccoType)
            this.tobaccoType.clearFormIds();
            
        // tslint:disable-next-line:forin
        for (let form in submittedForms) {

            switch (submittedForms[form].formTypeCd) {
                case FORM_3852:
                    this.tobaccoType.setFormId(submittedForms[form].formId, submittedForms[form].formTypeCd);
                    this.populateForm3852Details(submittedForms[form].formDetails);
                    this.populateFormComments(submittedForms[form].formComments, submittedForms[form].formTypeCd);
                    break;
                case FORM_52206:
                    isForm5220Include = true;
                    this.tobaccoType.setFormId(submittedForms[form].formId, submittedForms[form].formTypeCd);
                    this.populateForm52206Details(submittedForms[form].formDetails);
                    this.populateFormComments(submittedForms[form].formComments, submittedForms[form].formTypeCd);
                    break;
                case FORM_7501:
                    let ttClassId;
                    if (submittedForms[form].formDetails.length > 0) {
                        let tt = this.getParentTT7501(submittedForms[form].formDetails[0]);
                        ttClassId = tt.tobaccoClassId;
                    }
                    this.tobaccoType.setFormId(submittedForms[form].formId, submittedForms[form].formTypeCd, ttClassId);
                    this.populateForm7501Details(submittedForms[form].formDetails);
                    this.populateFormComments(submittedForms[form].formComments, submittedForms[form].formTypeCd);
                    break;
                default:
                    break;
            }
        }

        this.tobaccoType.include5220Form = isForm5220Include;

    }


    public populateForm3852Details(formDetails: any[]) {

        // tslint:disable-next-line:forin
        for (let detail in formDetails) {
            //log an error message if tobacco class in the form is not present in tobacco class constants
            if (!this.tobaccoClassMap[formDetails[detail].tobaccoClassId]) {
                console.error(' tobacco class Id ' + formDetails[detail].tobaccoClassId + ' in form 3852 not present in tobacco class constants ');
                continue;
            }

            let newTT = this.getOrAddTType(formDetails[detail].tobaccoClassId);
            if (newTT) {
                this.tobaccoType.displayTTypeByName(newTT.tobaccoSubTypeName, true);
                newTT.rmvlQty3852 = formDetails[detail].removalQty;
                newTT.taxAmt3852 = formDetails[detail].taxesPaid;
                newTT.isTaxAmt3852Pristine = true;

                /**
                 * if tax difference is undefined then calculate it
                 * The tax difference should only be calculated after removalQty
                 * and taxesPaid have been set on the tobacco type
                **/
                if (!formDetails[detail].dolDifference && formDetails[detail].dolDifference !== 0) {
                    newTT.trggTaxDiffCalc3852 = true;
                } else {
                    newTT.taxDiff3852 = formDetails[detail].dolDifference;
                    newTT.taxDiffErr3852 = formDetails[detail].activityCd === VALID ? false : true;
                    newTT.activityCd3852 = formDetails[detail].activityCd;
                    newTT.trggTaxDiffCalc3852 = false;
                }

                newTT.formId3852 = formDetails[detail].formId;
                newTT.formNum3852 = formDetails[detail].formNum;

            }
        }
    }

    public populateForm52206Details(formDetails: any[]) {

        for (let detail in formDetails) {
            //log an error message if tobacco class in the form is not present in tobacco class constants
            if (!this.tobaccoClassMap[formDetails[detail].tobaccoClassId]) {
                console.error(' tobacco class Id ' + formDetails[detail].tobaccoClassId + ' in form 5210 not present in tobacco class constants ');
                continue;
            }

            if (formDetails[detail].tobaccoClassId) {
                let ttTypeConst: TobaccoTypeConst = this.tobaccoType.getTTConstByClassId(formDetails[detail].tobaccoClassId);
                if (ttTypeConst) {
                    // check if large or small tobacco class type
                    // this could be cigar or cigarettes
                    let parentTTConst: TobaccoTypeConst = this.tobaccoType.getTTConstByClassId(ttTypeConst.parentTobaccoClassId);

                    if (parentTTConst && (parentTTConst.tobaccoSubTypeName === CIGARS || parentTTConst.tobaccoSubTypeName === CIGARETTES)) {
                        let parentTType: ImporterTobaccoSubType = this.getOrAddTType(parentTTConst.tobaccoClassId);
                        // set large or small qty on the parent
                        if (ttTypeConst.tobaccoSubTypeName === LARGE) {
                            parentTType.largeSticks = formDetails[detail].removalQty;
                        } else if (ttTypeConst.tobaccoSubTypeName === SMALL) {
                            parentTType.smallSticks = formDetails[detail].removalQty;
                        }
                        // tslint:disable-next-line:max-line-length
                        parentTType.qty52206 = Number(parentTType.largeSticks ? parentTType.largeSticks : 0) + Number(parentTType.smallSticks ? parentTType.smallSticks : 0);
                        parentTType.isQty52206Pristine = true;
                    } else {
                        let tt: ImporterTobaccoSubType = this.getOrAddTType(ttTypeConst.tobaccoClassId);
                        tt.qty52206 = formDetails[detail].removalQty;
                        tt.isQty52206Pristine = true;
                        tt.taxAmt52206 = formDetails[detail].taxesPaid;


                        /**
                         * If quantity difference is undefined then calculate it
                         * The qty difference should only be calculated after qty for 3852
                         * and qty for 5220 have been set on the tobacco type
                        */
                        if (!formDetails[detail].dolDifference && formDetails[detail].dolDifference !== 0) {
                            tt.trggQtyDiffCalc5220 = true;
                        } else {
                            tt.qtyDiff52206 = formDetails[detail].dolDifference;
                            tt.qtyDiffErr52206 = formDetails[detail].activityCd === VALID ? false : true;
                            tt.activityCd52206 = formDetails[detail].activityCd;
                            tt.trggQtyDiffCalc5220 = false;
                        }

                        tt.formId52206 = formDetails[detail].formId;
                        tt.formNum52206 = formDetails[detail].formNum;
                    }
                }

            }
        }
    }

    public populateForm7501Details(formDetails: any[]) {

        if (!formDetails || !(formDetails.length > 0)) return;

        // clear the form 7501 data types.
        let tt: ImporterTobaccoSubType = this.getParentTT7501(formDetails[0]);
        tt.forms7501 = [];
        tt.footerInput7501 = new Form7501Model();
        tt.lineNums7501 = [];


        for (let detail in formDetails) {
            if (Number(formDetails[detail].lineNum) === 0) {
                //log an error message if tobacco class in the form is not present in tobacco class constants
                if (!this.tobaccoClassMap[formDetails[detail].tobaccoClassId]) {
                    console.error(' tobacco class Id ' + formDetails[detail].tobaccoClassId + ' in form 7501 not present in tobacco class constants ');
                    continue;
                }

                this.populateTotalsForForm7501(formDetails[detail]);
            } else if (Number(formDetails[detail].lineNum) > 0) {
                this.populateTaxBrkDownForForm7501(formDetails[detail]);
            }
        }


    }

    private populateTotalsForForm7501(formDetail) {

        let tt: ImporterTobaccoSubType;
        tt = this.getOrAddTType(formDetail.tobaccoClassId);

        tt.formId7501 = formDetail.formId;
        tt.formNum7501 = formDetail.formNum;

        tt.taxDiff7501 = formDetail.dolDifference;
        tt.taxDiffErr7501 = formDetail.activityCd === VALID ? false : true;
        tt.activityCd7501 = formDetail.activityCd;
        tt.taxAmtTotal7501 = formDetail.taxesPaid;

        if (!formDetail.dolDifference && formDetail.dolDifference !== 0) {
            tt.trggTaxDiffCalc7501 = true;
        } else {
            tt.trggTaxDiffCalc7501 = false;
        }
    }

    private populateTaxBrkDownForForm7501(formDetail) {

        let tt: ImporterTobaccoSubType;

        let ttConst: TobaccoTypeConst = this.tobaccoType.getTTConstByClassId(formDetail.tobaccoClassId);

        tt = this.getParentTT7501(formDetail);

        let form7501Model = new Form7501Model();
        form7501Model.formId = formDetail.formId;
        form7501Model.formNum = formDetail.formNum;
        form7501Model.lineNum = formDetail.lineNum;

        tt.lineNums7501.push(formDetail.lineNum);

        form7501Model.valorem = formDetail.valoremDol;
        form7501Model.qty = formDetail.removalQty;

        if (ttConst.tobaccoSubTypeName === CIGARS)
            form7501Model.type = NO_SELECTION;
        else
            form7501Model.type = ttConst.tobaccoSubTypeName;

        form7501Model.taxAmt = formDetail.taxesPaid;
        form7501Model.taxAmtPristine = formDetail.taxesPaid;

        form7501Model.tobaccoClassId = formDetail.tobaccoClassId;
        form7501Model.ttname = formDetail.tobaccoClassNm;
        form7501Model.filerCode = formDetail.filerEntryNum;

        tt.forms7501.push(form7501Model);
    }

    private getParentTT7501(formDetail) {
        let tt: ImporterTobaccoSubType;

        let ttConst: TobaccoTypeConst = this.tobaccoType.getTTConstByClassId(formDetail.tobaccoClassId);
        let parentConst: TobaccoTypeConst;

        if (ttConst.parentTobaccoClassId)
            parentConst = this.tobaccoType.getTTConstByClassId(ttConst.parentTobaccoClassId);

        if (parentConst && parentConst.tobaccoSubTypeName === CIGARS) {
            tt = this.getOrAddTType(parentConst.tobaccoClassId);
        } else
            tt = this.getOrAddTType(ttConst.tobaccoClassId);

        return tt;
    }

    /*** populate comments on each form */
    // tslint:disable-next-line:member-ordering
    public populateFormComments(comments: any[], formTypeCd) {
        
        if(!comments || !(comments.length>0)) return;

        let commentArry = [];
        // tslint:disable-next-line:forin
        for (let i in comments) {
            let commentModel = new CommentModel();
            let comment = comments[i];
            jQuery.extend(true, commentModel, comment);
            commentArry.push(commentModel);
        }
        if (!this.tobaccoType.formComments[formTypeCd])
            this.tobaccoType.formComments[formTypeCd] = [];
        this.tobaccoType.formComments[formTypeCd].push(commentArry);
    }

    private getOrAddTType(tobaccoClassId: number): ImporterTobaccoSubType {
        let ttype = this.tobaccoType.getTTypeByClassId(tobaccoClassId);
        if (!ttype)
            ttype = this.tobaccoType.addTTConstToTTArray(tobaccoClassId);

        return ttype;
    }

    private isTTName(ttname: string) {
        let len = this.ttNamesArray.length;
        for (let i = 0; i < len; i++)
            if (this.ttNamesArray[i] === ttname) return true;

        return false;
    }
}
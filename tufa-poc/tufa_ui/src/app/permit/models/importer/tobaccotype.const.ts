export class TobaccoTypeConst {

    tobaccoSubTypeName: string;
    tobaccoClassId: number;
    parentTobaccoClassId: number;
    classRate: number;
    conversionRate: number;
    thresholdValue: number;

    childTobaccoTypeConst: TobaccoTypeConst[] =[];

    constructor(ttname: string, ttClassRate: number, ttClassId: number, ttParentClassId: number, conversionRate: number, thresholdValue: number) {
        this.tobaccoSubTypeName = ttname.replace(/-/g, "");
        this.classRate = ttClassRate;
        this.tobaccoClassId = ttClassId;
        this.parentTobaccoClassId = ttParentClassId;
        this.conversionRate = conversionRate;
        this.thresholdValue = thresholdValue;
    }
}
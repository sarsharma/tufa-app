import { Form7501Model } from './form-7501.model';
import { TobaccoTypeConst } from './importer/tobaccotype.const';

export class ImporterTobaccoSubType {

    tobaccoSubTypeName: string;
    tobaccoClassId: number;
    parentTobaccoClassId: number;
    classRate: number;
    conversionRate: number;

    childTobaccoTypeConst: TobaccoTypeConst[] = [];

    cigarSubtypes: ImporterTobaccoSubType[];


    /**
     * Large and Small class types for Cigars and Cigarettes
     */
    ttSubClassTypes: ImporterTobaccoSubType[] = [];

    /**Threshold value */
    thresholdValue: number;
    /**
    * form 3852 properties
    */
    formId3852: string;
    formNum3852: string;

    rmvlQty3852: number;
    taxAmt3852: number;
    taxDiff3852: number;

    isTaxAmt3852Pristine: boolean = true;
    taxDiffErr3852: boolean = false;
    isRmvlQty3852Valid: boolean = true;
    isTaxAmt3852StrValid: boolean = true;

    activityCd3852: string;
    /**
     * form 5220.6 properties
     */
    formId52206: string;
    formNum52206: string;

    qty52206: number;
    taxAmt52206: number;
    taxDiff52206: number;
    qtyDiff52206: number;

    isQty52206Pristine: boolean = true;
    isQuantity52206Valid: boolean = true;
    isQuantity52206StrValidLrg: boolean = true;
    isQuantity52206StrValidSml: boolean = true;
    isQuantity52206StrValid: boolean = true;

    qtyDiffErr52206: boolean;
    taxDiffErr52206: boolean;
    activityCd52206: string;

    // Save the fields for Form CBP 7501
    formId7501: string;
    formNum7501: string;

    forms7501: Form7501Model[] = [];
    footerInput7501: Form7501Model = new Form7501Model();
    lineNums7501: number[] = [];

    rmvlQty7501: number;
    taxAmtTotal7501: number;
    taxDiff7501: number;
    taxDiffErr7501: boolean = false;
    calculatedTaxAmt7501: number;

    activityCd7501: string;

    diffRmvlErr7501: boolean = false;
    activityCdDiffRmvl7501: string;
    diffTtlAndCalcTxErr7501: boolean = false;
    activityCdDiffTtlAndCalcTxErr7501: string;

    /**
     * For Cigars and Cigarettes
     */
    largeSticks: number;
    smallSticks: number;

    /**
     * Business Rules parameters
     */
    diffTaxBR: number;

    DECIMAL_SEPARATOR = ".";
    FRACTION_SIZE: number = 4;
    CURRENCY_FRACTION_SIZE: number = 2;
    PADDING = "000000";
    /**
     * is showing on form
     */
    isDisplayed: boolean;

    VALOREM = "Ad Valorem";
    POUND_CONVERSION: number = 2.20462262185;

    /** Tobacco Types */
    CIGARS = "Cigars";
    CIGARETTES = "Cigarettes";
    SNUFF = "Snuff";
    CHEW = "Chew";
    PIPE = "Pipe";
    RPY = "RollYourOwn";

    /*** other constants */
    NO_SELECTION = "None";
    SELECT_TYPE = "Select Type";

    /** to be only set or used when monthly report is loaded */
    trggTaxDiffCalc3852: boolean = false;
    trggQtyDiffCalc5220: boolean = false;
    trggTaxDiffCalc7501: boolean = false;

    constructor() { }
    // /**
    // * calculations for form 3852
    // */
    public calculateTaxAmount3852() {
        if (!this.isTaxAmt3852Pristine) return;

        let rmvlQty3852: number;
        if (this.rmvlQty3852 || this.rmvlQty3852 === 0) {
            rmvlQty3852 = this.rmvlQty3852;
            let txAmt = rmvlQty3852 * this.classRate;
            this.taxAmt3852 = this.roundDecimal(txAmt);
        } else
            this.taxAmt3852 = this.rmvlQty3852;
    }


    public calculatedTaxDiff3852() {

        let rmvlQty3852 = this.rmvlQty3852 ? this.rmvlQty3852 : 0;

        let calcTaxDiff3852;
        //calculate the Calculated Tax Difference 
        if (this.rmvlQty3852 && this.rmvlQty3852 != 0 && this.taxAmt3852) {

            let calcExciseTax = this.classRate * this.rmvlQty3852;
            let rndExciseTax = this.roundDecimal(calcExciseTax);
            let txAmt3852 = (rndExciseTax == this.taxAmt3852) ? calcExciseTax : this.taxAmt3852;

            if (this.tobaccoSubTypeName === this.CIGARETTES) {
                calcTaxDiff3852 = this.roundDecimal(txAmt3852 / rmvlQty3852 * 1000, 4);
            } else
                calcTaxDiff3852 = this.roundDecimal(txAmt3852 / rmvlQty3852, 6);
        }


        return calcTaxDiff3852;

    }

    public calculateTaxDiff3852() {

        let rmvlQty3852 = this.rmvlQty3852 ? this.rmvlQty3852 : 0;
        let txAmt = rmvlQty3852 * this.classRate;
        let taxAmt3852 = this.taxAmt3852 ? this.taxAmt3852 : 0;

        this.taxDiff3852 = this.roundDecimal(taxAmt3852) - this.roundDecimal(txAmt);
        this.taxDiffErr3852 = (Math.abs(this.taxDiff3852) >= 1) ? true : false;
        this.activityCd3852 = this.taxDiffErr3852 ? "Error" : "Valid";
    }

    // /**
    // * calculations for form 5220
    // */
    public calculateQty5220() {
        if (!this.isQty52206Pristine)
            return;

        if (this.tobaccoSubTypeName === this.CIGARETTES) {
            this.smallSticks = this.rmvlQty3852;
            let largeSticks: number = this.largeSticks ? this.largeSticks : 0;
            this.qty52206 = Number(this.smallSticks) + largeSticks;
        } else if (this.tobaccoSubTypeName === this.CIGARS) {
            this.largeSticks = this.rmvlQty3852;
            let smallSticks: number = this.smallSticks ? this.smallSticks : 0;
            this.qty52206 = Number(this.largeSticks) + smallSticks;
        } else {
            this.qty52206 = this.rmvlQty3852;
        }
    }

    public calculateTaxDiff5220() {
        let taxAmt3852: number;
        taxAmt3852 = (!this.taxAmt3852) ? 0 : this.taxAmt3852;

        let qty52206: number;
        qty52206 = (this.qty52206) ? this.qty52206 : 0;
        this.taxDiff52206 = this.roundDecimal(qty52206 * this.classRate) - this.roundDecimal(taxAmt3852);
        this.taxDiffErr52206 = this.taxDiff52206 !== 0 ? true : false;
        this.activityCd52206 = this.taxDiffErr52206 ? "Error" : "Valid";
    }

    public calculateQtyDiff5220() {
        let qty3852 = this.rmvlQty3852 ? this.rmvlQty3852 : 0;
        let qty52206 = this.qty52206 ? this.qty52206 : 0;
        let thresholdValue = this.thresholdValue ? this.thresholdValue : 0;
        this.qtyDiff52206 = this.roundDecimal(qty3852 - qty52206, 3);
        this.qtyDiffErr52206 = Math.abs(this.qtyDiff52206) >= thresholdValue ? true : false;
        /*if(this.tobaccoSubTypeName === this.CIGARS || this.tobaccoSubTypeName === this.CIGARETTES) {
            this.qtyDiffErr52206 = this.qtyDiff52206 !== 0 ? true : false;
        }*/
        this.activityCd52206 = this.qtyDiffErr52206 ? "Error" : "Valid";
    }

    /**
     * calculations for form 7501
     */
    public calculateTaxAmount7501(taxAmountTotal?: number) {

        if (this.isDefined(taxAmountTotal)) this.taxAmtTotal7501 = this.roundDecimal(taxAmountTotal);
        let txAmt3852: number;
        //if (this.isDefined(this.taxAmt3852)) txAmt3852 = this.taxAmt3852;

        if (this.isDefined(taxAmountTotal) || this.isDefined(this.taxAmt3852)) {
            let v1 = this.taxAmt3852 ? this.roundDecimal(this.taxAmt3852) : 0;
            let v2 = this.taxAmtTotal7501 ? this.roundDecimal(this.taxAmtTotal7501) : 0;
            this.taxDiff7501 = v1 - v2;
        }

        if (this.isDefined(this.taxDiff7501)) {
            this.taxDiffErr7501 = (Math.abs(this.taxDiff7501) >= 1) ? true : false;
            this.activityCd7501 = this.taxDiffErr7501 ? "Error" : "Valid";
        }

    }

    public calculatedTaxAmount7501(calculatedTaxAmt: number) {
        if (this.isDefined(calculatedTaxAmt))
            this.calculatedTaxAmt7501 = this.roundDecimal(calculatedTaxAmt);
        else this.calculatedTaxAmt7501 = 0;
    }


    public totalAndCalculatedAmt7501() {
        let totalTxAmt: number = 0;
        let txAmtsDefined: boolean = false;
        let calTxAmt = 0;
        let calTxAmtDefined: boolean = false;
        let forms7501 = this.forms7501;

        /*
            Go throug each line and calculate:
            1) Calcultaed Rate
            2) Difference
            3) Total tax amount
            4) Total caluclated tax amount
        */
        for (var i in forms7501) {

            // Check if the tax amount feild is defined
            // Then sum up totalTaxAmount for all lines if the value is defined
            if (this.isDefined(forms7501[i].taxAmt)) {
                totalTxAmt += Number(forms7501[i].taxAmt)
                txAmtsDefined = true;
            }

            // Check to see if any of the volume fields are defined 
            if (this.isDefined(forms7501[i].qty) || this.isDefined(forms7501[i].valorem)) {
                calTxAmtDefined = true;
            }

            // Get the net quantity, 0 if it deosn't exist
            let qty = forms7501[i].qty ? forms7501[i].qty : 0;
            let classRate;

            /* Class rate is defined by tobaco class
                - Cigars: this.cigarClassrate
                - Cogarettes: this.classRate * 1000
                - Others: this.clasRate */
            if (this.tobaccoSubTypeName === this.CIGARS && forms7501[i].type) {
                classRate = this.cigarClassRate(forms7501[i].type);
            } else if (this.tobaccoSubTypeName === this.CIGARETTES) {
                classRate = this.classRate * 1000;
            } else classRate = this.classRate;

            // If class rate is not defined then skip to the next line
            if (!classRate) continue;

            let calculateRate = 0;

            // If advolorem set the calclated reate to quantity tome 402.6
            if (forms7501[i].type === this.VALOREM) {
                calculateRate = forms7501[i].qty ? forms7501[i].qty * 402.6 : 0;
                // calculateRate = Math.min(forms7501[i].valorem?Number(forms7501[i].valorem)*classRate:calculateRate, 
                //                          forms7501[i].qty?Number(forms7501[i].qty)*402.6:calculateRate);
                calculateRate = Math.min(forms7501[i].valorem ? this.roundDecimal(forms7501[i].valorem * classRate) : calculateRate,
                    forms7501[i].qty ? this.roundDecimal(forms7501[i].qty * 402.6) : calculateRate);
                calculateRate = !isNaN(calculateRate) ? calculateRate : forms7501[i].calTaxAmt;
                calTxAmt += calculateRate;
            }
            // For combined tobacco classes 
            else if (this.isCombinedTobaccoType(this.tobaccoSubTypeName)) {
                calTxAmt += Number(this.roundDecimal(this.conversionRate * qty));
                calculateRate = Number(this.roundDecimal(this.conversionRate * qty));
            }
            //Cigarettes and other Cigar classes
            else {
                calTxAmt += Number(this.roundDecimal(this.kgtolbsConversion(this.tobaccoSubTypeName) * qty * classRate));
                calculateRate = Number(this.roundDecimal(this.kgtolbsConversion(this.tobaccoSubTypeName) * qty * classRate));
            }

            if (this.isDefined(forms7501[i].taxAmt))
                this.diffUpdatePristine7501(i, calculateRate);
        }

        /** include footer to the calculated and total tax amount calulations */
        if (this.isDefined(this.footerInput7501.taxAmt)) {
            totalTxAmt += Number(this.footerInput7501.taxAmt)
            txAmtsDefined = true;
        }
        //totalTxAmt += (this.footerInput7501.taxAmt ? Number(this.footerInput7501.taxAmt) : 0);
        if (this.isDefined(this.footerInput7501.qty) || this.isDefined(this.footerInput7501.valorem))
            calTxAmtDefined = true;

        let qtyFooter = this.footerInput7501.qty ? this.footerInput7501.qty : 0;

        let classRt;
        if (this.tobaccoSubTypeName === this.CIGARS) {
            if (this.footerInput7501.type)
                classRt = this.cigarClassRate(this.footerInput7501.type);
        } else if (this.tobaccoSubTypeName === this.CIGARETTES) {
            classRt = this.classRate * 1000;
        } else
            classRt = this.classRate;

        if (classRt) {
            let qty = this.footerInput7501.qty ? this.footerInput7501.qty : 0;
            if (this.footerInput7501.type === this.VALOREM) {
                calTxAmt += Number(this.footerInput7501.calTaxAmt);
            } else if (this.isCombinedTobaccoType(this.tobaccoSubTypeName))
                calTxAmt += Number(this.roundDecimal(this.conversionRate * qty));
            else {
                calTxAmt += Number(this.roundDecimal(this.kgtolbsConversion(this.tobaccoSubTypeName) * qtyFooter * classRt));
            }
        }

        this.calculateTaxAmount7501(txAmtsDefined ? totalTxAmt : null);
        this.calculatedTaxAmount7501(calTxAmtDefined ? calTxAmt : null);
    }

    public addFooterInput7501(tobaccoType: string, selector: string): number {

        //Get the maximum line number + 1
        let index = this.forms7501.length > 0 ? Math.max.apply(Math, this.forms7501.map(function (o) { return o.lineNum })) + 1 : 1;
        this.footerInput7501.lineNum = Number(index);
        this.footerInput7501.qty = Number(this.footerInput7501.qty ? this.footerInput7501.qty : 0);
        if (tobaccoType !== "Cigars") this.footerInput7501.type = tobaccoType;
        this.forms7501.unshift(this.footerInput7501);
        this.footerInput7501 = new Form7501Model();
        return this.forms7501.length;

    }

    public deleteFooterInput7501(lineNum) {

        for (var i = 0; i < this.forms7501.length; i++) {
           
            if (this.forms7501[i].lineNum === lineNum)
                this.forms7501.splice(i, 1);
        }

        // this.forms7501.splice(lineNum, 1)  

    }

    taxAmtUpdateFooter7501() {

        let footer7501 = this.footerInput7501;

        let qtyChange: number;

        let footerType = footer7501.type;
        let footerValorem = footer7501.valorem;
        let footerQty = footer7501.qty;
        let footerTaxAmt = footer7501.taxAmt;
        let ttname = this.tobaccoSubTypeName;

        if (ttname === this.CIGARS && !footerType) return;
        if (footerType === this.VALOREM) {
            qtyChange = footerValorem ? footerValorem : 0;
            //footer7501.qty = null;
        } else
            qtyChange = footer7501.qty ? footer7501.qty : 0;

        let classRateSelected: number;

        if (ttname === this.CIGARS) {
            classRateSelected = this.cigarClassRate(footerType) ? this.cigarClassRate(footerType) : 0;
        } else if (this.tobaccoSubTypeName === this.CIGARETTES) {
            classRateSelected = this.classRate * 1000;
        } else
            classRateSelected = this.classRate;

        let taxAmt: number;

        if (this.isCombinedTobaccoType(this.tobaccoSubTypeName))
            taxAmt = this.roundDecimal(this.conversionRate * qtyChange);
        else
            taxAmt = this.roundDecimal(this.kgtolbsConversion(ttname) * qtyChange * classRateSelected);
        footer7501.calTaxAmt = taxAmt;
        //Pick the minimum result
        if (footerType === this.VALOREM) {
            if (!footerValorem && footer7501.qty) {
                taxAmt = footer7501.qty * 402.6;
            } else if (footerValorem && footer7501.qty) {
                taxAmt = Math.min(taxAmt, footer7501.qty * 402.6);
            }
            footer7501.calTaxAmt = taxAmt;
            footer7501.taxDiff = taxAmt - footer7501.calTaxAmt;
        }

        footer7501.taxAmt = taxAmt;
        /*if (footer7501.amtpristine)
            footer7501.taxAmtPristine = taxAmt;*/
        this.taxDiffUpdateFooter7501();

    }


    getCalculatedTaxRate7501Footer() {
        let tobaccoType = this.footerInput7501;
        return this.getCalculatedTaxRate7501(tobaccoType)
    }

    getCalculatedTaxRate7501(tobaccoType) {
        if (this.isDefined(tobaccoType.qty) && this.isDefined(tobaccoType.taxAmt)) {
            let classRt = (this.tobaccoSubTypeName === this.CIGARS) ? this.cigarClassRate(tobaccoType.type) : this.classRate;
            let calcTxAmt;
            if (this.tobaccoSubTypeName === this.CIGARETTES)
                calcTxAmt = tobaccoType.qty * classRt * 1000;
            else
                calcTxAmt = tobaccoType.qty * classRt;

            let taxAmt = (this.roundDecimal(calcTxAmt) == tobaccoType.taxAmt) ? calcTxAmt : tobaccoType.taxAmt;
            //7501 removals need to be converted to lbs for Chew, Snuff, RYO, and Pipe
            let calTxRt = 0;
            if (this.isCombinedTobaccoType(this.tobaccoSubTypeName))
                calTxRt = taxAmt / (tobaccoType.qty * this.POUND_CONVERSION);
            else
                calTxRt = taxAmt / tobaccoType.qty;

            return this.roundDecimal(calTxRt,6);
        }
    }


    taxDiffUpdateFooter7501() {
        let footer7501 = this.footerInput7501;
        let footerTaxAmt = footer7501.taxAmt;
        footer7501.taxDiff = footerTaxAmt - footer7501.calTaxAmt;
    }

    getRmvls7501() {

        let forms7501 = this.forms7501;
        let totalNetQty = 0;
        let qtyDefined = false;
        for (var i in forms7501) {
            if (this.isDefined(forms7501[i].qty)) {
                totalNetQty += Number(forms7501[i].qty);
                qtyDefined = true;
            }
        }
        if (this.isDefined(this.footerInput7501.qty)) {
            totalNetQty += Number(this.footerInput7501.qty);
            qtyDefined = true;
        }

        if (this.isCombinedTobaccoType(this.tobaccoSubTypeName))
            totalNetQty = this.roundDecimal(totalNetQty * this.POUND_CONVERSION, 3);
        else
            totalNetQty = this.roundDecimal(Number(totalNetQty * 1000), 0);

        return qtyDefined ? totalNetQty : null;

    }

    isDefined(val) {
        if (val || val == 0) return true;
        return false;
    }

    getDiffRmvls7501() {

        let rmvlQty3852 = this.rmvlQty3852;
        let rmvl7501 = this.getRmvls7501();

        let diffRmvl;
        if (this.isDefined(rmvlQty3852) || this.isDefined(rmvl7501)) {
            if (this.isCombinedTobaccoType(this.tobaccoSubTypeName))
                diffRmvl = this.roundDecimal(rmvlQty3852 ? rmvlQty3852 : 0, 3) - this.roundDecimal(rmvl7501 ? rmvl7501 : 0, 3);
            else
                diffRmvl = this.roundDecimal(rmvlQty3852 ? rmvlQty3852 : 0) - this.roundDecimal(rmvl7501 ? rmvl7501 : 0);
        }

        if (this.isDefined(diffRmvl)) {
            diffRmvl = this.roundDecimal(diffRmvl, 3);
            this.diffRmvlErr7501 = Math.abs(diffRmvl) >= this.thresholdValue ? true : false;
            this.activityCdDiffRmvl7501 = Math.abs(diffRmvl) >= this.thresholdValue ? 'Error' : 'Valid';
        }

        return diffRmvl;
    }

    getDiffTtlAndCalcTx7501() {
        let diffTandC;
        if (this.isDefined(this.taxAmtTotal7501) || this.isDefined(this.calculatedTaxAmt7501))
            diffTandC = this.roundDecimal((this.taxAmtTotal7501 ? this.taxAmtTotal7501 : 0) - (this.calculatedTaxAmt7501 ? this.calculatedTaxAmt7501 : 0));
        if (this.isDefined(diffTandC)) {
            this.diffTtlAndCalcTxErr7501 = Math.abs(diffTandC) >= 1 ? true : false;
            this.activityCdDiffTtlAndCalcTxErr7501 = Math.abs(diffTandC) >= 1 ? 'Error' : 'Valid';
        }
        return this.roundDecimal(diffTandC);
    }


    qtyUpdate7501(form7501: Form7501Model) {

        let qtyChange = form7501.qty ? form7501.qty : 0;

        let classRateSelected: number;
        if (this.tobaccoSubTypeName === 'Cigars') {
            classRateSelected = this.cigarClassRate(form7501.type) ? this.cigarClassRate(form7501.type) : 0;
            if (form7501.type === this.VALOREM)
                qtyChange = form7501.valorem ? form7501.valorem : 0;
        } else if (this.tobaccoSubTypeName === this.CIGARETTES) {
            classRateSelected = this.classRate * 1000;
        } else
            classRateSelected = this.classRate;

        if (form7501.amtpristine) {
            let taxAmt: number;
            if (this.isCombinedTobaccoType(this.tobaccoSubTypeName))
                taxAmt = this.roundDecimal(this.conversionRate * qtyChange);
            else
                taxAmt = this.roundDecimal(this.kgtolbsConversion(this.tobaccoSubTypeName) * qtyChange * classRateSelected);
            form7501.calTaxAmt = taxAmt;
            //Pick the minimum result
            if (form7501.type === this.VALOREM) {
                if (!form7501.valorem && form7501.qty) {
                    taxAmt = form7501.qty * 402.6;
                } else if (form7501.valorem && form7501.qty) {
                    taxAmt = Math.min(taxAmt, form7501.qty * 402.6);
                }
                form7501.calTaxAmt = taxAmt;
                form7501.taxDiff = taxAmt - form7501.calTaxAmt;
            }

            form7501.taxAmt = taxAmt;
            //form7501.taxAmtPristine = taxAmt;
        }

        if (!form7501.type || form7501.type == 'None' || form7501.type == 'Select Type') {
            form7501.taxDiff = null;
        }

    }

    diffUpdate7501(index) {
        let form7501 = this.forms7501[index];
        let footerTaxAmt = form7501.taxAmt;
        if (!form7501.type || form7501.type == 'None' || form7501.type == 'Select Type') {
            form7501.taxDiff = null;
        } else {
            form7501.taxDiff = footerTaxAmt - form7501.calTaxAmt;
        }
    }

    diffUpdatePristine7501(index, calTxAmt) {
        let form7501 = this.forms7501[index];
        form7501.taxDiff = Number(form7501.taxAmt) - calTxAmt;
    }

    taxAmtUpdate7501(form7501: Form7501Model) {
        form7501.amtpristine = false;
    }

    vlrmUpdate7501(index) {
        let form7501 = this.forms7501[index];
        let valoremAmt = form7501.valorem;
        let classRate: number = this.cigarClassRate(form7501.type);
        if (form7501.amtpristine) {
            form7501.taxAmt = valoremAmt * classRate;
            if (form7501.qty)
                form7501.taxAmt = Math.min(form7501.taxAmt, form7501.qty * 402.6);
        }
    }

    set7501TaxAmtDirty() {
        this.footerInput7501.amtpristine = false;
    }

    private isCombinedTobaccoType(ttname: string): boolean {
        if (ttname === 'Chew' || ttname === 'Snuff' || ttname === 'Pipe' || ttname === 'RollYourOwn')
            return true;
        return false;
    }

    private kgtolbsConversion(ttname: string): number {
        if (ttname === 'Chew' || ttname === 'Snuff' || ttname === 'Pipe' || ttname === 'RollYourOwn')
            return this.POUND_CONVERSION;
        else return 1;
    }
    private cigarClassRate(classRateType: string): number {

        var classRateTypes = this.childTobaccoTypeConst;
        var classRate: number;
        for (var i in classRateTypes) {
            if (classRateTypes[i].tobaccoSubTypeName === classRateType) {
                classRate = classRateTypes[i].classRate;
                break;
            }
        }
        return classRate;
    }



    /** Return errors**/
    public is7501Error(): boolean {
        let isError1 = false;
        let isError2 = false;
        let isError3 = false;

        isError1 = this.activityCd7501 ? this.taxDiffErr7501 : true;
        isError2 = this.activityCdDiffRmvl7501 ? this.diffRmvlErr7501 : true;
        isError3 = this.activityCdDiffTtlAndCalcTxErr7501 ? this.diffTtlAndCalcTxErr7501 : true;

        return isError1 || isError2 || isError3;
    }


    public is52206Error(): boolean {
        // TODO: Can't this just return qtyDiffErr52206?
        return this.qtyDiffErr52206;
    }

    public is3852Error(): boolean {
        if (this.tobaccoSubTypeName === this.CIGARS) return false;
        return this.activityCd3852 ? this.taxDiffErr3852 : true;
    }


    public getTTSubClassByName(ttSubName: string) {
        var ttArray = this.ttSubClassTypes;
        for (var i in ttArray) {
            if (ttArray[i].tobaccoSubTypeName === ttSubName)
                return ttArray[i];
        }
    }


    public getForm3852Detail() {

        var formDetail = {};
        formDetail["tobaccoClassId"] = this.tobaccoClassId;
        formDetail["removalQty"] = this.rmvlQty3852;
        formDetail["taxesPaid"] = (this.taxAmt3852 !== 0 && !this.taxAmt3852) ? null : this.roundDecimal(this.taxAmt3852);
        formDetail["dolDifference"] = this.roundDecimal(this.taxDiff3852);
        formDetail["activityCd"] = this.activityCd3852;
        formDetail["calcTaxRate"] = this.calculatedTaxDiff3852();

        return formDetail;
    }

    public getForm52206Detail(): any[] {
        var formDetails = [];
        var formDetail = {};

        formDetail["tobaccoClassId"] = this.tobaccoClassId;
        formDetail["removalQty"] = this.qty52206;
        // formDetail["taxesPaid"] = this.roundDecimal(this.taxAmt52206);
        formDetail["dolDifference"] = this.roundDecimal(this.qtyDiff52206, 3);
        formDetail["activityCd"] = this.activityCd52206;
        formDetails.push(formDetail);

        for (var subclass in this.childTobaccoTypeConst) {
            let formDetail = {};
            if (this.childTobaccoTypeConst[subclass].tobaccoSubTypeName === "Large") {
                formDetail["tobaccoClassId"] = this.childTobaccoTypeConst[subclass].tobaccoClassId;
                formDetail["removalQty"] = this.largeSticks;
                formDetails.push(formDetail);
            } else if (this.childTobaccoTypeConst[subclass].tobaccoSubTypeName === "Small") {
                formDetail["tobaccoClassId"] = this.childTobaccoTypeConst[subclass].tobaccoClassId;
                formDetail["removalQty"] = this.smallSticks;
                formDetails.push(formDetail);
            }
        }

        return formDetails;
    }

    public getForm7501Detail() {

        var formDetails = [];
        /** Form detail from total tax paid */
        var formDetail = {};
        formDetail["tobaccoClassId"] = this.tobaccoClassId;
        formDetail["taxesPaid"] = this.roundDecimal(this.taxAmtTotal7501);
        formDetail["dolDifference"] = this.roundDecimal(this.taxDiff7501, 6);
        formDetail["activityCd"] = this.activityCd7501;
        formDetail["lineNum"] = 0;

        formDetail["calcTxActCd7501"] = this.diffTtlAndCalcTxErr7501 ? "Error" : "Valid";

        // Volume Data
        formDetail["removalQty"] = this.getRmvls7501();
        formDetail["dolRemovalDifference"] = this.getDiffRmvls7501();
        formDetail["removalActivity"] = this.activityCdDiffRmvl7501;

        formDetails.push(formDetail)

        let taxAmtTotal7501: number = 0;
        /** Form details from tax break down for Cigars and other tobaccco types**/

        for (var i in this.forms7501) {
            var formDetailRw = {};
            var form7501 = this.forms7501[i];

            if (this.checkFormDetail7501Empty(form7501)) continue;

            if (this.tobaccoSubTypeName === 'Cigars') {
                if (!form7501.type || form7501.type === this.SELECT_TYPE || form7501.type === this.NO_SELECTION)
                    formDetailRw["tobaccoClassId"] = this.tobaccoClassId;
                else
                    formDetailRw["tobaccoClassId"] = this.getChildTobaccoClass(form7501.type);
            } else {
                formDetailRw["tobaccoClassId"] = this.tobaccoClassId;
            }

            if (this.tobaccoSubTypeName === 'Cigars')
                formDetailRw["valoremDol"] = form7501.valorem;

            formDetailRw["removalQty"] = form7501.qty;

            /*if (form7501.amtpristine) {
                taxAmtTotal7501 += form7501.taxAmtPristine;
                formDetailRw["taxesPaid"] = this.roundDecimal(form7501.taxAmtPristine, 6);
            }
            else {
                taxAmtTotal7501 += form7501.taxAmt;
                formDetailRw["taxesPaid"] = this.roundDecimal(form7501.taxAmt);
            }*/

            formDetailRw["calcTaxRate"] = this.getCalculatedTaxRate7501(form7501);
            formDetailRw["taxesPaid"] = this.roundDecimal(form7501.taxAmt);
            formDetailRw["filerEntryNum"] = form7501.filerCode;
            formDetailRw["dolDifference"] = form7501.taxDiff;
            formDetailRw["lineNum"] = form7501.lineNum;
            formDetails = formDetails.concat(formDetailRw);
        }

        // add footerInput details
        if (!this.checkFormDetail7501Empty(this.footerInput7501)) {
            let formDetailFt = {};
            if (this.tobaccoSubTypeName === 'Cigars') {
                if (!this.footerInput7501.type || this.footerInput7501.type === this.SELECT_TYPE || this.footerInput7501.type === this.NO_SELECTION)
                    formDetailFt["tobaccoClassId"] = this.tobaccoClassId;
                else
                    formDetailFt["tobaccoClassId"] = this.getChildTobaccoClass(this.footerInput7501.type);
            } else
                formDetailFt["tobaccoClassId"] = this.tobaccoClassId;

            if (this.tobaccoSubTypeName === 'Cigars')
                formDetailFt["valoremDol"] = this.footerInput7501.valorem;

            formDetailFt["removalQty"] = this.footerInput7501.qty;
            formDetailFt["removalUom"] = "Sticks";

            /* if (this.footerInput7501.amtpristine) {
                 taxAmtTotal7501 += this.footerInput7501.taxAmtPristine;
                 formDetailFt["taxesPaid"] = this.roundDecimal(this.footerInput7501.taxAmtPristine, 6);
             }
             else {
                 taxAmtTotal7501 += this.footerInput7501.taxAmt;
                 formDetailFt["taxesPaid"] = this.roundDecimal(this.footerInput7501.taxAmt, 6);
             }*/

            formDetailFt["taxesPaid"] = this.roundDecimal(this.footerInput7501.taxAmt);
            formDetailFt["filerEntryNum"] = this.footerInput7501.filerCode;
            formDetails = formDetails.concat(formDetailFt);
        }

        //formDetail["taxesPaid"] = this.roundDecimal(taxAmtTotal7501,6);

        return formDetails;
    }

    checkFormDetail7501Empty(formDetail) {
        if (this.tobaccoSubTypeName === 'Cigars') {
            if (!formDetail.filerCode && !formDetail.taxAmt && !formDetail.type && !formDetail.qty) {
                return true;
            }
            return false;
        }

        if (!formDetail.filerCode && !formDetail.taxAmt && !formDetail.qty)
            return true;

        return false;
    }


    public getChildTobaccoClass(childType: string): number {
        for (let child in this.childTobaccoTypeConst) {
            if (this.childTobaccoTypeConst[child].tobaccoSubTypeName === childType) {
                return this.childTobaccoTypeConst[child].tobaccoClassId;
            }
        }
    }


    public formatDecimalNumber(value: string, fractionSize: number = 2): number {
        let [integer, fraction = ""] = (value || "").split(this.DECIMAL_SEPARATOR);

        // integer = integer.replace(new RegExp(this.THOUSANDS_SEPARATOR, "g"), "");

        fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
            ? this.DECIMAL_SEPARATOR + (fraction + this.PADDING).substring(0, fractionSize)
            : "";

        return Number(integer + fraction);
    }

    public roundDecimal(value, fractionSize = 2): number {
        let val = value ? value : 0;
        return Number(Math.round(Number(val + 'e' + fractionSize)) + 'e-' + fractionSize);
    }

}
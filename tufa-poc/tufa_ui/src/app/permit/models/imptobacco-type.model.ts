import { CommentModel } from './comment/comment.model';
import { TobaccoTypeConst } from './importer/tobaccotype.const';
import { ImporterTobaccoSubType } from './imptobacco-subtype.model';

const CIGARETTES = 'Cigarettes';
const CIGARS = 'Cigars';
const CHEW = 'Chew';
const SNUFF = 'Snuff';
const PIPE = 'Pipe';
const ROY = 'RollYourOwn';

declare var jQuery;

export class TobaccoType {

    showCigarettes: boolean;
    showCigars: boolean;
    showSnuffs: boolean;
    showPipes: boolean;
    showChews: boolean;
    showRollYourOwn: boolean;

    isTTSelected: number;

    permitId: number;
    periodId: number;
    permitNumber: string;

    period: string;

    //initialize tt constants on page load
    ttConstantsArray: TobaccoTypeConst[] = [];

    include5220Form: boolean = true;

    FORM_3852 = "3852";
    FORM_52206 = "5220.6";
    FORM_7501 = "7501";

    formComments = {};

    private tobaccoTypes: ImporterTobaccoSubType[] = [];  

    private formIds = { 3852: "", 5220.6: "", 7501: { Cigars: "", Cigarettes: "", Snuff: "", Chew: "", Pipe: "", RollYourOwn: "" } };

    constructor() {
        this.showCigarettes = false;
        this.showCigars = false;
        this.showSnuffs = false;
        this.showPipes = false;
        this.showChews = false;
        this.showRollYourOwn = false;
        this.isTTSelected = 0;
        // this.addTobaccoType();
    }


    public addTobaccoType(ttName: string) {

        this.setTTSelected();
        (this.displayTTypeByName(ttName)) ? this.addTTConstToTTArray(null, this.getTobaccoTypeConst(ttName)) : this.removeTTFromArray(ttName);

        if (this.displayTTypeByName(ttName)) this.getTobaccoType(ttName).isDisplayed = true;
        return this.displayTTypeByName(ttName);
    }


    public addTTConstToTTArray(typeClassId: number, ttConst?: TobaccoTypeConst): ImporterTobaccoSubType {
        // get tobacco type from tobacco type const array
        if (!ttConst)
            ttConst = this.getTTConstByClassId(typeClassId);
        // create new tobacco type
        let newTT = new ImporterTobaccoSubType();
        jQuery.extend(true, newTT, ttConst);

        this.addToTTArray(newTT);
        return newTT;
    }

    public addToTTArray(ttype: ImporterTobaccoSubType) {
        if (this.isTobaccoType(ttype.tobaccoSubTypeName)) return;
        this.tobaccoTypes.push(ttype);
    }


    public removeTTFromArray(typeName: string) {
        this.tobaccoTypes.forEach(function (item: ImporterTobaccoSubType, index, array) {
            if (item.tobaccoSubTypeName === typeName) {
                // remove tobacco type
                array.splice(index, 1);
            }
        }, this);
    }


    public addTobaccoTypeConsts(ttypeconst: TobaccoTypeConst) {
        this.ttConstantsArray.push(ttypeconst);
    }


    public getTobaccoTypeConst(typeName: string): TobaccoTypeConst {
        for (let i in this.ttConstantsArray) {
            if (this.ttConstantsArray[i].tobaccoSubTypeName === typeName)
                return this.ttConstantsArray[i];
        }
    }

    public getTobaccoType(typeName: string): ImporterTobaccoSubType {

        let tobaccoSubType;
        this.tobaccoTypes.forEach(function (arrayItem: ImporterTobaccoSubType) {
            if (arrayItem.tobaccoSubTypeName === typeName) {
                tobaccoSubType = arrayItem;
            }
        });
        return tobaccoSubType;
    }

    public getTTConstByClassId(ttClassId: number): TobaccoTypeConst {
        for (let i in this.ttConstantsArray) {
            if (this.ttConstantsArray[i].tobaccoClassId === ttClassId)
                return this.ttConstantsArray[i];
        }
    }

    public getTTypeByClassId(ttClassId: number): ImporterTobaccoSubType {
        for (let i in this.tobaccoTypes) {
            if (this.tobaccoTypes[i].tobaccoClassId === ttClassId)
                return this.tobaccoTypes[i];
        }
    }


    public isTobaccoType(typeName: string) {

        let isttPresent: boolean = false;
        this.tobaccoTypes.forEach(function (arrayItem: ImporterTobaccoSubType) {
            if (arrayItem.tobaccoSubTypeName === typeName)
                isttPresent = true;
        });

        return isttPresent;
    }

    public getTobaccoTypes(): ImporterTobaccoSubType[] {
        return this.tobaccoTypes;
    }

    public getTobaccoTypesConst() {
        return this.ttConstantsArray;
    }


    public displayTTypeByName(ttName: string, display?: boolean): boolean {

        let changeType: boolean = false;
        switch (ttName) {
            case CIGARETTES:
                if (display) this.showCigarettes = display;
                changeType = this.showCigarettes;
                break;
            case CIGARS:
                if (display) this.showCigars = display;
                changeType = this.showCigars;
                break;
            case CHEW:
                if (display) this.showChews = display;
                changeType = this.showChews;
                break;
            case SNUFF:
                if (display) this.showSnuffs = display;
                changeType = this.showSnuffs;
                break;
            case PIPE:
                if (display) this.showPipes = display;
                changeType = this.showPipes;
                break;
            case ROY:
                if (display) this.showRollYourOwn = display;
                changeType = this.showRollYourOwn;
                break;
            default:
                break;
        }
        this.setTTSelected();
        return changeType;
    }

    private setTTSelected() {
        // tslint:disable-next-line:max-line-length
        this.isTTSelected = (this.showCigarettes ? 1 : 0) + (this.showCigars ? 1 : 0) + (this.showChews ? 1 : 0) + (this.showSnuffs ? 1 : 0) + (this.showPipes ? 1 : 0) + (this.showRollYourOwn ? 1 : 0);
    }

    // tslint:disable-next-line:member-ordering
    setFormId(formId: string, formTypeCd: string, ttClass?: number) {
        if (ttClass && formTypeCd === this.FORM_7501) {
            let tt = this.getTTConstByClassId(ttClass);
            let form = this.formIds[formTypeCd];
            for (let i in form) {
                if (tt && i === tt.tobaccoSubTypeName) {
                    form[i] = formId;
                }
            }
        } else if (formTypeCd !== this.FORM_7501){
            this.formIds[formTypeCd] = formId;
        }
    }

    getFormId(formTypeCd: string, ttClass?: number): string {
        if (ttClass) {
            let tt = this.getTTConstByClassId(ttClass);
            let form = this.formIds[formTypeCd];
            for (let i in form) {
                if (tt && i === tt.tobaccoSubTypeName) {
                    return form[i];
                }
            }
        }
        return this.formIds[formTypeCd];
    }

    clearFormIds(){
            this.formIds[this.FORM_3852]="";
            this.formIds[this.FORM_52206]="";
            let tt= this.formIds[this.FORM_7501];
            for (let i in tt) {
                    tt[i] = "";
            }
    }

    /**
     * return all form related data.
     * Note: getFormDetails() method is called when the report is getting saved
     */
    public getFormDetails() {

        // tslint:disable-next-line:max-line-length
        let selectedTobaccoTypes = (this.showCigarettes ? "CIG_" : "") + (this.showCigars ? "CGR_" : "") + (this.showChews ? "CHE_" : "") + (this.showSnuffs ? "SNU_" : "") + (this.showPipes ? "PIP_" : "") + (this.showRollYourOwn ? "ROL_" : "");

        let submittedForms = [];
        let formDetails3852 = [];
        let formDetails52206 = [];
        let formDetails7501 = [];

        let submittedForm3852 = {};
        let submittedForm52206 = {};


        // tslint:disable-next-line:forin
        for (let tt in this.tobaccoTypes) {
            formDetails3852.push(this.tobaccoTypes[tt].getForm3852Detail());
            formDetails52206 = formDetails52206.concat(this.tobaccoTypes[tt].getForm52206Detail());

            let formDetails7501ForEachTT = [];
            formDetails7501ForEachTT = this.tobaccoTypes[tt].getForm7501Detail();
            formDetails7501.push(formDetails7501ForEachTT);
        }


        let formId3852 = this.getFormId(this.FORM_3852);
        submittedForm3852["periodId"] = this.periodId;
        submittedForm3852["permitId"] = this.permitId;
        submittedForm3852["formId"] = formId3852;
        submittedForm3852["formTypeCd"] = this.FORM_3852;
        submittedForm3852["selectedTobaccoTypes"] = selectedTobaccoTypes;
        submittedForm3852["formDetails"] = formDetails3852;
        submittedForms.push(submittedForm3852);

        if (this.include5220Form) {
            let formId52206 = this.getFormId(this.FORM_52206);
            submittedForm52206["periodId"] = this.periodId;
            submittedForm52206["permitId"] = this.permitId;
            submittedForm52206["formId"] = formId52206;
            submittedForm52206["formTypeCd"] = this.FORM_52206;
            submittedForm52206["formDetails"] = formDetails52206;
            submittedForms.push(submittedForm52206);
        }

        if (!this.include5220Form) {
            this.formIds[this.FORM_52206] = '';
            this.deleteComments(this.FORM_52206);
        }

        // tslint:disable-next-line:forin
        for (let i in formDetails7501) {
            let submittedForm7501 = {};
            submittedForm7501["periodId"] = this.periodId;
            submittedForm7501["permitId"] = this.permitId;
            submittedForm7501["formTypeCd"] = this.FORM_7501;
            submittedForm7501["formId"] = this.getFormId(this.FORM_7501, formDetails7501[i][0].tobaccoClassId);
            submittedForm7501["formDetails"] = formDetails7501[i];
            submittedForms.push(submittedForm7501);
        }

        return submittedForms;
    }

     /** get comments for a form and a particular tobaccotype */
    public getComments(formTypeCd, tobaccotype?: string, tobaccoClassId?: number) {

        let ttClassId;
        if (formTypeCd === this.FORM_7501) {

            if (!tobaccoClassId) {
                if(!tobaccotype) return;
                let ttype: ImporterTobaccoSubType = this.getTobaccoType(tobaccotype);
                ttClassId = ttype.tobaccoClassId;
            } else {
                ttClassId = tobaccoClassId;
            }

            // get all comments for the form type code
            let allComments: any[] = this.formComments[formTypeCd];

            // tslint:disable-next-line:forin
            for (let i in allComments) {
                let comments: CommentModel[] = allComments[i];
                if (comments.length > 0 && comments[0].tobaccoClassId === ttClassId)
                    return comments;
            }
        } else {
            let allComments: any[] = this.formComments[formTypeCd];
            if (allComments && allComments.length > 0)
                return allComments[0];
        }

        /*let allComments: any[] = [];
        this.formComments[formTypeCd].push(allComments);
        return allComments;*/
    }

    /** add comment. To be called only when none exists for a formTypeCd**/

    public addComment(comment: CommentModel) {

        let ttype: ImporterTobaccoSubType;
        let comments;
        if (comment.tobaccoClassId !== 0) {
            // ttype = this.getTTypeByClassIdFromConsts(comment.tobaccoClassId);
            comments = this.getComments(comment.formTypeCd, null, comment.tobaccoClassId);
        } else
            comments = this.getComments(comment.formTypeCd);

        if (comments) {
            let index = this.commentIndex(comment, comments);
            if (index)
                comments[index] = comment;
            else
                comments.push(comment);
        } else {
            let commentArry: CommentModel[] = [];
            commentArry.push(comment);
            if (!this.formComments[comment.formTypeCd])
                this.formComments[comment.formTypeCd] = [];
            this.formComments[comment.formTypeCd].push(commentArry);
        }
    }

    commentIndex(searchComment, comments): string{

        for (let i in comments) {
            let comment: CommentModel = comments[i];
            if (comment.commentSeq === searchComment.commentSeq)
                return  i;
        }

        return null;
    }

    public deleteComments(formTypeCd: string, tobaccoClassId?: number) {
        if (tobaccoClassId) {
                let comments: any [] = this.formComments[formTypeCd];
                for (let i in comments) {
                    if (comments[i].length > 0) {
                        if (comments[i][0].tobaccoClassId === tobaccoClassId) {
                            comments[i] = [];
                            break;
                        }
                    }
                }
        }else {
            this.formComments[formTypeCd] = [];
        }
    }

}

/*
@author : rnasina
This is a model to hold Activity details.
*/
export class MissingForms {
    periodId: number;
    permitId: number;
    formTypeCd: string;
    defectDesc: string;
    defectStatusCd: string;
}
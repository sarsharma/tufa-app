/*
@author : JBS
This is a model to hold Activity details.
*/

export class PeriodActivity {
    companyName: string;
    permitNum: string;
    period: string;
}
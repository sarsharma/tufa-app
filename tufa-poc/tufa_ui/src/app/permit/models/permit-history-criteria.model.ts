/*
@author : tgunter
This is a model to hold Permit History Search criteria and results.
*/
import { CompanyPermitHistory } from '../../company/models/company-permit-history.model';

export class PermitHistoryCriteria {
    companyNameFilter: string;
    permitNameFilter: string;
    companyIdFilter: number;
    monthFilters: string[]=[];
    yearFilters: string[]=[];
    permitTypeFilters: string[]=[];
    permitStatusFilters: string[]=[];
    reportStatusFilters: string[]=[];
    results: CompanyPermitHistory[];
}
/*
@author : Deloitte
This is a model to hold Permit details.
*/

export class PermitHistory {
    reportPeriodId: number;
    month: string;
    year: string;
    fiscalYear: string;
    displayMonthYear: string;
    sortMonthYear: string;
    statusCd: string;
    activity: string;
    attachments: string;
    attachmentsId: number;
}
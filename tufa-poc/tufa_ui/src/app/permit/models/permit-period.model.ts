/*
 @author : JBS
 This is a model to hold Permit Period details.
*/

export class PermitPeriod {
    permitId: number;
    periodId: number;
    sourceCd: string;
    zeroFlag: boolean;
    createdBy: string;
    createdDt: Date;
    modifiedBy: string;
    modifiedDt: Date;

     fillFromJSON(json: string) {
        let jsonObj = JSON.parse(json);
        for (var propName in jsonObj) {
            this[propName] = jsonObj[propName];
            if(propName === 'zeroFlag')
            {
                if(jsonObj[propName] === 'y' || jsonObj[propName] === 'Y')
                {
                    this[propName] = true;
                }
                else
                {
                    this[propName] = false;
                }
            }
            if(propName === 'sourceCd')
            {
                this[propName] = jsonObj[propName];
            }
        }
    }
}

export class TestPermitPeriod {
    permitId: string;
    periodId: string;
    sourceCd: string;
    zeroFlag: string;

    constructor() {
        this.permitId = '55';
        this.periodId = '9';
        this.sourceCd = 'EMAI';
        this.zeroFlag = 'y';
    }
}

import { TobaccoType } from './tobacco-type.model';
import { TobaccoSubType } from './tobacco-subtype.model';
import { CommentModel } from './comment/comment.model';
import { TTBAdjustmentDetail, TTBAdjustments } from './ttb-adjustment.model';


const CIGARETTES = 'Cigarettes';
const CIGARS = 'Cigars';
const CHEW = 'Chew';
const SNUFF = 'Snuff';
const PIPE = 'Pipe';
const RYO = 'RollYourOwn';
const PIPE_RYO = 'PipeRYO';
const CHEW_SNUFF = 'ChewandSnuff';
const LARGE = 'Large';
const SMALL = 'Small';
const FORM_3852 = '3852';
const FORM_5210 = '5210.5';
const FORM_5000 = '5000.24';
const VALID = 'Valid';
const ERROR = 'Error';



declare var jQuery: any;

export class PermitMfgReport {

    public tobaccoType: TobaccoType;
    private ttNamesArray = [CIGARETTES, CIGARS, CHEW, SNUFF, PIPE, RYO];

    public tobaccoClassMap = {};
    constructor() {
    }

    ngOnInit(): void { }

    public populateFormDetails(data, ttype: TobaccoType) {

        this.tobaccoType = ttype;
        let permitId = data.permitId;
        let periodId = data.periodId;
        this.tobaccoType.permitId = permitId;
        this.tobaccoType.periodId = periodId;
        // reset form details
        this.tobaccoType.formDetails = [];

        let tobaccoClasses = data.permit.tobaccoClasses;
        let submittedForms = data.submittedForms;

        if (tobaccoClasses)
            this.createTobaccoTypes(tobaccoClasses);
        else {
            console.error('tobacco class constants are empty or undefined');
            return;
        }

        this.populateForms(submittedForms);

        // /** Calculate tax or qty difference calculations on each form as indicated by the
        //   flag. This neeeds to be done if tax or qty difference valuse are coming in as
        //   null or undefined **/

        let ttypes: TobaccoSubType[] = this.tobaccoType.getTobaccoTypes();

        // tslint:disable-next-line:forin
        for (let i in ttypes) {
            let tobt = ttypes[i];
            if (tobt.trggTaxDiffCalc3852) tobt.calculateTaxDiff3852();
            if (tobt.trggQtyDiffCalc5210) tobt.calculateQtyDiff5210();
            if (tobt.trggTaxDiffCalc5000) tobt.calculateTaxDiff5000();
        }

        return this.tobaccoType;
    }


    public createTobaccoTypes(tobaccoClasses: any[]) {

        // tslint:disable-next-line:forin
        for (let tclassKey in tobaccoClasses) {
            //populate the tobacco class map 
            this.tobaccoClassMap[tobaccoClasses[tclassKey].tobaccoClassId] = tobaccoClasses[tclassKey].tobaccoClassNm;

            let ttName: string = tobaccoClasses[tclassKey].tobaccoClassNm;
            let ttClassRate: number = tobaccoClasses[tclassKey].tobaccoClassNm === "Cigarettes" ?
                tobaccoClasses[tclassKey].taxRate / 1000 : tobaccoClasses[tclassKey].taxRate;
            let ttClassId: number = tobaccoClasses[tclassKey].tobaccoClassId;
            let ttParentClassId: number = tobaccoClasses[tclassKey].parentClassId;
            let ttThreshold: number = tobaccoClasses[tclassKey].thresholdValue;
            this.tobaccoType.addTobaccoTypeConsts(new TobaccoSubType(ttName, ttClassRate, 0, ttClassId, ttParentClassId, ttThreshold));
        }

        // set tt class  for large and small, roll-pipe and chew-snuff  in the parent class
        this.tobaccoType.getTobaccoTypesConst().forEach(function (item, index, array) {
            let parent: TobaccoSubType;
            if (item.ttParentConstClassId) {
                parent = this.tobaccoType.getTTypeByClassIdFromConsts(item.ttParentConstClassId);
                parent.ttSubClassTypeConsts.push(item);
                item.parentConst = parent;
            }
        }, this);

    }

    public populateForms(submittedForms: any[]) {

        this.updateFormIds(submittedForms);
        let adjustmentDetails: TTBAdjustments[] = [];
        

        // tslint:disable-next-line:forin
        for (let form in submittedForms) {

            switch (submittedForms[form].formTypeCd) {
                case FORM_3852:
                    this.populateForm3852Details(submittedForms[form].formDetails);
                    this.populateFormComments(submittedForms[form].formComments, submittedForms[form].formTypeCd);
                    break;
                case FORM_5210:
                    this.populateForm5210Details(submittedForms[form].formDetails);
                    this.populateFormComments(submittedForms[form].formComments, submittedForms[form].formTypeCd);
                    break;
                case FORM_5000:
                    adjustmentDetails = submittedForms[form].ttbAdjustments;
                    this.populateForm5000Details(submittedForms[form].formDetails, adjustmentDetails);
                    this.populateFormComments(submittedForms[form].formComments, submittedForms[form].formTypeCd);
                    //this.populateAdjustments(adjustmentDetailMock)//(submittedForms[form].adjustmentDetails)
                    break;
                default:
                    break;
            }
        }

    }
    populateAdjustments(adjustmentDetails : Array<TTBAdjustmentDetail[]>){
        let ttType: TobaccoSubType;
        let adjustmentDetailTemp : TTBAdjustmentDetail[] =[];
        for(let details in adjustmentDetails){
            ttType = this.tobaccoType.getTTypeByClassIdFromConsts(adjustmentDetails[details][0].tobaccoClassId);
            if (ttType.tobaccoSubTypeName === PIPE_RYO || ttType.tobaccoSubTypeName === CHEW_SNUFF) {
                let parentTT = ttType;
                parentTT = new TobaccoSubType(parentTT.tobaccoSubTypeName, parentTT.classRate, parentTT.taxDiffThresh, parentTT.tobaccoClassId, parentTT.ttParentConstClassId, parentTT.thresholdValue);
                parentTT.adjustmentDetails = adjustmentDetails[details];
            }
            else {
                let tt: TobaccoSubType = this.getOrAddTType(adjustmentDetails[details][0].tobaccoClassId);
                tt.adjustmentDetails = adjustmentDetails[details];
            }
        }
    }

    public populateForm3852Details(formDetails: any[]) {

        // tslint:disable-next-line:forin
        for (let detail in formDetails) {
            //log an error message if tobacco class in the form is not present in tobacco class constants
            if (!this.tobaccoClassMap[formDetails[detail].tobaccoClassId]) {
                console.error(' tobacco class Id ' + formDetails[detail].tobaccoClassId + ' in form 3852 not present in tobacco class constants ');
                continue;
            }

            let newTT = this.getOrAddTType(formDetails[detail].tobaccoClassId);
            if (newTT) {
                this.tobaccoType.displayTTypeByName(newTT.tobaccoSubTypeName, true);
                newTT.qty3852 = formDetails[detail].removalQty;
                newTT.taxAmt3852 = formDetails[detail].taxesPaid;
                newTT.isTaxAmt3852Pristine = true;

                // if tax difference is undefined then calculate it
                if (!formDetails[detail].dolDifference && formDetails[detail].dolDifference !== 0) {
                    // /**
                    //     The tax difference should only be calculated after removalQty
                    //     and taxesPaid have been set on the tobacco type
                    // **/
                    newTT.trggTaxDiffCalc3852 = true;
                } else {
                    newTT.taxDiff3852 = formDetails[detail].dolDifference;
                    newTT.taxDiffErr3852 = (!formDetails[detail].activityCd || formDetails[detail].activityCd === VALID) ? false : true;
                    newTT.activityCd3852 = newTT.taxDiffErr3852 ? ERROR : VALID;
                    newTT.trggTaxDiffCalc3852 = false;
                }

                newTT.formId = formDetails[detail].formId;
                newTT.formNum = formDetails[detail].formNum;

                /** reset summation for 5000 forms */
                newTT.summationBox = [];
                newTT.summationBoxCigar = [];
            }
        }
    }

    public populateForm5210Details(formDetails: any[]) {

        for (let detail in formDetails) {
            //log an error message if tobacco class in the form is not present in tobacco class constants
            if (!this.tobaccoClassMap[formDetails[detail].tobaccoClassId]) {
                console.error(' tobacco class Id ' + formDetails[detail].tobaccoClassId + ' in form 5210 not present in tobacco class constants ');
                continue;
            }

            if (formDetails[detail].tobaccoClassId) {
                let ttType: TobaccoSubType = this.tobaccoType.getTTypeByClassIdFromConsts(formDetails[detail].tobaccoClassId);
                if (ttType) {
                    // check if large or small tobacco class type
                    // this could be cigar or cigarettes
                    let parentTT: TobaccoSubType = ttType.parentConst;

                    if (parentTT && (parentTT.tobaccoSubTypeName === CIGARS || parentTT.tobaccoSubTypeName === CIGARETTES)) {
                        let parentTType: TobaccoSubType = this.getOrAddTType(parentTT.tobaccoClassId);
                        // set large or small qty on the parent
                        if (ttType.tobaccoSubTypeName === LARGE) {
                            parentTType.largeSticks = formDetails[detail].removalQty;
                        } else if (ttType.tobaccoSubTypeName === SMALL) {
                            parentTType.smallSticks = formDetails[detail].removalQty;
                        }
                        // tslint:disable-next-line:max-line-length
                        parentTType.qty52105 = Number(parentTType.largeSticks ? parentTType.largeSticks : 0) + Number(parentTType.smallSticks ? parentTType.smallSticks : 0);
                        parentTType.isQty5210Pristine = true;
                    } else {
                        let tt: TobaccoSubType = this.getOrAddTType(ttType.tobaccoClassId);
                        tt.qty52105 = formDetails[detail].removalQty;
                        tt.isQty5210Pristine = true;
                        tt.taxAmt5210 = formDetails[detail].taxesPaid;

                        tt.adjustmentRemoval = formDetails[detail].adjustmentRemoval;
                        tt.grandRemovalTotal = formDetails[detail].grandRemovalTotal;
                        tt.isEnableAdjustment5210 = formDetails[detail].enableAdj==='Y'?true:false;
                        // if quantity difference is undefined then set a flag to calculate it later
                        if (!formDetails[detail].dolDifference && formDetails[detail].dolDifference !== 0) {
                            // /**
                            //     The qty difference should only be calculated after qty for 3852
                            //     and qty for 5210  have been set on the tobacco type
                            // **/
                            tt.trggQtyDiffCalc5210 = true;
                        } else {
                            tt.qtyDiff5210 = formDetails[detail].dolDifference;
                            tt.qtyDiffErr5210 = (!formDetails[detail].activityCd || formDetails[detail].activityCd === VALID) ? false : true;
                            tt.activityCd5210 = tt.qtyDiffErr5210 ? ERROR : VALID;
                            tt.trggQtyDiffCalc5210 = false;
                        }


                        tt.formId = formDetails[detail].formId;
                        tt.formNum = formDetails[detail].formNum;
                    }
                }

            }
        }
    }

    public populateForm5000Details(formDetails: any[], adjustmentDetails :TTBAdjustments[]) {
        let ttType: TobaccoSubType;
        // tslint:disable-next-line:forin
        for (let detail in formDetails) {
            ttType = this.tobaccoType.getTTypeByClassIdFromConsts(formDetails[detail].tobaccoClassId);
            if (Number(formDetails[detail].lineNum) === 0) {
                //log an error message if tobacco class in the form is not present in tobacco class constants
                if (!this.tobaccoClassMap[formDetails[detail].tobaccoClassId]) {
                    console.error(' tobacco class Id ' + formDetails[detail].tobaccoClassId + ' in form 5000 not present in tobacco class constants ');
                    continue;
                }
                this.populateTotalsForForm5000(ttType, formDetails[detail], adjustmentDetails);
            } else if (Number(formDetails[detail].lineNum) > 0) {
                this.populateTaxBrkDownForForm5000(ttType, formDetails[detail]);
            }
        }
    }

    private populateTotalsForForm5000(ttType: TobaccoSubType, formDetail, adjustmentDetails : TTBAdjustments[]) {

        if (ttType.tobaccoSubTypeName === PIPE_RYO || ttType.tobaccoSubTypeName === CHEW_SNUFF) {
            // get parent instance for Pipe-RYO and CHEW_SNUFF from child type
            // This assumes that child types Pipe,RYO,Chew or Snuff have already been added;
            let parentTT = ttType;

            // tslint:disable-next-line:max-line-length
            parentTT = new TobaccoSubType(parentTT.tobaccoSubTypeName, parentTT.classRate, parentTT.taxDiffThresh, parentTT.tobaccoClassId, parentTT.ttParentConstClassId, parentTT.thresholdValue);
            parentTT.formId = formDetail.formId;
            parentTT.formNum = formDetail.formNum;
            parentTT.taxDiff5000 = formDetail.dolDifference;
            parentTT.taxDiffErr5000 = (!formDetail.activityCd || formDetail.activityCd === VALID) ? false : true;
            parentTT.activityCd5000 = parentTT.taxDiffErr5000 ? ERROR : VALID;
            parentTT.taxAmountAdjusmentA = formDetail.adjATotal;
            parentTT.taxAmountAdjustmentB = formDetail.adjBTotal;
            parentTT.taxAmountTotalAdjustment = formDetail.adjustmentTotal;
            parentTT.taxAmountGrandTotal5000 = formDetail.grandTotal;
            parentTT.isEnableAdjutment = formDetail.enableAdj==='Y'?true:false;
            for(let details in adjustmentDetails){
                if(adjustmentDetails[details].ttbAdjustmentDetail[0].tobaccoClassId === parentTT.tobaccoClassId)
                    parentTT.adjustmentDetails = adjustmentDetails[details].ttbAdjustmentDetail;
            }
            let childArry: TobaccoSubType[] = this.tobaccoType.getChildTypes(ttType);
            // tslint:disable-next-line:forin
            for (let i in childArry) {
                parentTT.taxAmt5000Chld[childArry[i].tobaccoSubTypeName] = childArry[i].taxAmt3852;
                parentTT.taxAmt5000 = (parentTT.taxAmt5000 ? parentTT.taxAmt5000 : 0) + childArry[i].taxAmt3852;
                childArry[i].parent = parentTT;

                if (!formDetail.dolDifference && formDetail.dolDifference !== 0) {
                    childArry[i].trggTaxDiffCalc5000 = true;
                } else {
                    childArry[i].trggTaxDiffCalc5000 = false;
                }
            }
        } else {
            let tt: TobaccoSubType = this.getOrAddTType(ttType.tobaccoClassId);
            tt.formId = formDetail.formId;
            tt.formNum = formDetail.formNum;
            tt.taxDiff5000 = formDetail.dolDifference;
            tt.taxDiffErr5000 = (!formDetail.activityCd || formDetail.activityCd === VALID) ? false : true;
            tt.activityCd5000 = tt.taxDiffErr5000 ? ERROR : VALID;
            tt.taxAmt5000 = tt.taxAmt3852;
            tt.taxAmountAdjusmentA = formDetail.adjATotal;
            tt.taxAmountAdjustmentB = formDetail.adjBTotal;
            tt.taxAmountTotalAdjustment = formDetail.adjustmentTotal;
            tt.taxAmountGrandTotal5000 = formDetail.grandTotal;
            tt.isEnableAdjutment = formDetail.enableAdj==='Y'?true:false;
            for(let details in adjustmentDetails){
                if(adjustmentDetails[details].ttbAdjustmentDetail[0].tobaccoClassId === tt.tobaccoClassId)
                    tt.adjustmentDetails = adjustmentDetails[details].ttbAdjustmentDetail;             
            }
            if (!formDetail.dolDifference && formDetail.dolDifference !== 0) {
                tt.trggTaxDiffCalc5000 = true;
            } else {
                tt.trggTaxDiffCalc5000 = false;
            }

        }

    }

    private populateTaxBrkDownForForm5000(ttype: TobaccoSubType, formDetail) {

        if (ttype.tobaccoSubTypeName === PIPE_RYO || ttype.tobaccoSubTypeName === CHEW_SNUFF) {
            let parent = this.tobaccoType.getParentFromChildTypes(ttype);
            parent.summationBox.push(formDetail.taxesPaid);
            parent.taxAmountTotal5000 = parent.summationBox.reduce(function (a, b) {
                return a + b;
            }, 0);
            // parent.taxAmountAdjusmentA = formDetail.adjATotal;
            // parent.taxAmountAdjustmentB = formDetail.adjBTotal;
            // parent.taxAmountTotalAdjustment = formDetail.adjustmentTotal;
            // parent.taxAmountGrandTotal5000 = parent.taxAmountTotal5000+parent.taxAmountAdjusmentA-parent.taxAmountAdjustmentB;
        } else {
            let tt = this.getOrAddTType(ttype.tobaccoClassId);
            tt.summationBox.push(formDetail.taxesPaid);
            tt.taxAmountTotal5000 = tt.summationBox.reduce(function (a, b) {
                return a + b;
            }, 0);
            // tt.taxAmountAdjusmentA = formDetail.adjATotal;
            // tt.taxAmountAdjustmentB = formDetail.adjBTotal;
            // tt.taxAmountTotalAdjustment = formDetail.adjustmentTotal
            // //this can be removed or read directly from API
            // tt.taxAmountGrandTotal5000 = tt.taxAmountTotal5000+tt.taxAmountAdjusmentA-tt.taxAmountAdjustmentB;
        }

    }

    // /*** populate comments on each form */
    // tslint:disable-next-line:member-ordering
    public populateFormComments(comments: any[], formTypeCd) {
        let commentArry = [];
        // tslint:disable-next-line:forin
        for (let i in comments) {
            let commentModel = new CommentModel();
            let comment = comments[i];
            jQuery.extend(true, commentModel, comment);
            commentArry.push(commentModel);
        }
        this.tobaccoType.formComments[formTypeCd] = commentArry;
    }


    private pushLineNum(formDetail, summationArry: any[]) {
        // /** set formId and formNum*/
        let formId = {};
        formId["lineNum"] = formDetail.lineNum;
        summationArry.push(formId);
    }


    // tslint:disable-next-line:member-ordering
    public updateFormIds(submittedForms) {

        // tslint:disable-next-line:forin
        for (let form in submittedForms) {
            let formId = submittedForms[form].formId;
            let formTypeCd = submittedForms[form].formTypeCd;
            let formNum = submittedForms[form].formNum;

            let frmDetail;

            /* if (formTypeCd === FORM_5000) {
                 let tobaccoClassId;
                 if (submittedForms[form].formDetails && submittedForms[form].formDetails.length > 0)
                     tobaccoClassId = submittedForms[form].formDetails[0].tobaccoClassId;
 
                 frmDetail = this.tobaccoType.getForm(formTypeCd, tobaccoClassId);
                 if (!frmDetail) {
                     frmDetail = {};
                     this.tobaccoType.addForm(frmDetail);
                 }
 
                 frmDetail["formTypeCd"] = formTypeCd;
                 frmDetail["formId"] = formId;
                 frmDetail["tobaccoClassId"] = tobaccoClassId;
                 frmDetail["formNum"] = formNum;
             } else {*/
            frmDetail = this.tobaccoType.getForm(formTypeCd);

            if (!frmDetail) {
                frmDetail = {};
                this.tobaccoType.addForm(frmDetail);
            }

            frmDetail["formTypeCd"] = formTypeCd;
            frmDetail["formId"] = formId;
            frmDetail["formNum"] = formNum;
            //}

        }
    }

    private getOrAddTType(tobaccoClassId: number): TobaccoSubType {
        let ttype: TobaccoSubType = this.tobaccoType.getTTypeByClassId(tobaccoClassId);
        if (!ttype) {
            ttype = this.tobaccoType.getTTypeByClassIdFromConsts(tobaccoClassId);
            // ttype = this.tobaccoType.addTTConstsToTTByClassId(tobaccoClassId);
            ttype = this.tobaccoType.addFromTTypeConstsToTTypes(null, ttype);
        }
        return ttype;
    }

    private isTTName(ttname: string) {
        let len = this.ttNamesArray.length;
        for (let i = 0; i < len; i++)
            if (this.ttNamesArray[i] === ttname) return true;

        return false;
    }
}
/*
@author : Deloitte
This is a model to hold Permit details.

*/

import { CompanyPermitHistory } from '../../company/models/company-permit-history.model';
import { Company } from '../../company/models/company.model';

export class Permit {
    permitId: number;
    permitNum: string;
    /** TUFA Active Date */
    issueDt: string;
    /** TUFA Inactive Date */
    closeDt: string;
    /** TTB Issued Date */
    ttbstartDt: string;
    /** TTB Closed Date */
    ttbendDt: string;
    permitTypeCd: string;
    permitStatusTypeCd: string;
    companyId: number;
    permitComments: Comment[];
    permitHistoryList: CompanyPermitHistory[];
    company: Company;
    duplicateFlag: boolean;

    permitAudit: any;
    permitAction: string;
}
export class SelectTobaccoType {
    showCigarettes: boolean;
    showCigars: boolean;
    showSnuffs: boolean;
    showPipes: boolean;
    showChews: boolean;
    showRollYourOwn: boolean;

    constructor() {
        this.showCigarettes = false;
        this.showCigars = false;
        this.showSnuffs = false;
        this.showPipes = false;
        this.showChews = false;
        this.showRollYourOwn = false;
    }

    public isAllSelected() {
        return this.showCigarettes &&
            this.showChews &&
            this.showCigars &&
            this.showPipes &&
            this.showRollYourOwn &&
            this.showSnuffs;
    }

    public isOneSelected() {
        return this.showCigarettes ||
            this.showChews ||
            this.showCigars ||
            this.showPipes ||
            this.showRollYourOwn ||
            this.showSnuffs;
    }
}
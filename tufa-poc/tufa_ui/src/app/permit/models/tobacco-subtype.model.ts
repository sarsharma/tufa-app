import { TTBAdjustmentDetail } from "./ttb-adjustment.model";

export class TobaccoSubType {

    tobaccoSubTypeName: string;
    tobaccoSubTypeId: string;
    tobaccoClassId: number;
    ttParentConstClassId: number;
    formId: number;
    formNum: number;
    unit: string;
    classRate: number;



    /** parent instance to be used for PiPe-RYO and Chew-Snuff type**/
    parent: TobaccoSubType;
    parentAddedToFormDetails: boolean = false;

    /** if this instance is PiPe-RYO or Chew-Snuff type store the taxamt5000 in*/
    /** this is a hack need to change */
    taxAmt5000Chld = { "Chew": 0, "Snuff": 0, "Pipe": 0, "RollYourOwn": 0 };


    /** parent and children constant reference*/
    parentConst: TobaccoSubType;
    /**
     * Large and Small class types for Cigars and Cigarettes
     */

    ttSubClassTypeConsts: TobaccoSubType[] = [];

    /**
     * form 3852 properties
     */
    qty3852: number;
    taxAmt3852: number;
    taxDiff3852: number;

    isTaxAmt3852Pristine: boolean = true;

    activityCd3852: string;
    /**
     * form 5210 properties
     */
    qty52105: number;
    taxAmt5210: number;
    taxDiff5210: number = null;
    qtyDiff5210: number;
    qtyDiffErr5210: boolean;
    
    thresholdValue: number;

    isQty5210Pristine: boolean = true;
    /**
     * For Cigars and Cigarettes
     */
    largeSticks: number;
    smallSticks: number;
    line17Adjustments: number;

    activityCd5210: string;

    /**
     * form 5000 properties
     */

    taxAmt5000: number;
    taxDiff5000: number;

    combinedTaxAmount5000: number;
    combinedTaxDiff5000: number;

    activityCd5000: string;

    taxAmountTotal5000: number;
    summationBox = [];
    taxAmountTotal5000Cigar: number;
    summationBoxCigar = [];

    /** array for keeping formIds and formNum**/
    summationBoxFormIds = [];
    summationBoxCigarFormIds = [];

    isTaxBrkDwn5000Pristine: boolean = true;

    /**chnages for US  CTPTUFA-5094,CTPTUFA-6705**/
    taxAmountGrandTotal5000: number;
    taxAmountAdjusmentA: number;
    taxAmountAdjustmentB: number;
    taxAmountTotalAdjustment: number;
    isEnableAdjutment: boolean = false;
    isEnableAdjustment5210: boolean = false;
    adjustmentDetails: TTBAdjustmentDetail[] =[];
    grandRemovalTotal:number;
    adjustmentRemoval:number;

    // Save the fields for Form CBP 7501
    removalsAmount7501: number;
    taxAmt7501: number;

    /**
     * For showing validation error messages
     **/

    isQuantity3852Valid: boolean = true;
    istaxAmountStrValid: boolean = true;
    isAmountOfTaxStrValid: boolean = true;

    isQuantity52105Valid: boolean = true;
    isQuantity52105StrValidLrg: boolean = true;
    isQuantity52105StrValidSml: boolean = true;
    isQuantity52105StrValid: boolean = true;

    taxDiffErr3852: boolean;
    taxDiffErr5210: boolean;
    taxDiffErr5000: boolean;

    /** to be only set or used when monthly report is loaded */
    trggTaxDiffCalc3852: boolean = false;
    trggQtyDiffCalc5210: boolean = false;
    trggTaxDiffCalc5000 : boolean = false;


    /**
     * Business Rules parameters
     */
    taxDiffThresh: number;

    DECIMAL_SEPARATOR = ".";
    FRACTION_SIZE: number = 4;
    CURRENCY_FRACTION_SIZE: number = 2;
    PADDING = "000000";

    ERROR = 'Error';
    VALID = 'Valid';
    /**
     * is showing on form
     */
    isDisplayed: boolean;

    /** Tobacco Types */
    CIGARS = "Cigars";
    CIGARETTES = "Cigarettes";
    SNUFF = "Snuff";
    CHEW = "Chew";
    PIPE = "Pipe";
    RPY = "RollYourOwn";

    constructor(typeName: string, classRate: number, taxDiffThresh: number, tobaccoClassId: number, ttParentConstClassId: number, ttThreshold: number) {
        this.tobaccoSubTypeName = typeName.replace(/-/g, "");
        this.classRate = classRate;
        this.tobaccoClassId = tobaccoClassId;
        this.taxDiffThresh = taxDiffThresh;
        this.ttParentConstClassId = ttParentConstClassId;
        this.thresholdValue = ttThreshold;
    }


    public setTTSubClassTypeConsts(ttSubClassTypeConsts: TobaccoSubType[]) {
        this.ttSubClassTypeConsts = ttSubClassTypeConsts;
    }
    /**
     * calculations for form 3852
     */
    public calculateTaxAmount3852() {
        if (!this.isTaxAmt3852Pristine) return;

        let qty3852;
        if (this.qty3852 || this.qty3852 === 0) {
            qty3852 = this.qty3852;
            let txAmt = qty3852 * this.classRate;
            this.taxAmt3852 = this.roundDecimal(txAmt);
        } else
            this.taxAmt3852 = this.qty3852;
    }

    public calculatedTaxDiff3852() {

        let qty3852 = this.qty3852 ? this.qty3852 : 0;
        let calcTaxDiff3852;

        if (this.qty3852 && this.qty3852 != 0 && this.taxAmt3852) {
            
                let calcExciseTax = this.classRate * this.qty3852;
                let rndExciseTax = this.roundDecimal(calcExciseTax);
                let txAmt3852 = (rndExciseTax == this.taxAmt3852)? calcExciseTax:this.taxAmt3852;
                    
                if (this.tobaccoSubTypeName === this.CIGARETTES) {
                    calcTaxDiff3852 = this.roundDecimal(txAmt3852 / qty3852 * 1000, 4);
                } else
                    calcTaxDiff3852 = this.roundDecimal(txAmt3852 / qty3852, 6);
        }


        return calcTaxDiff3852;
    }

    public calculateTaxDiff3852() {

        let qty3852 = this.qty3852 ? this.qty3852 : 0;
        let txAmt = qty3852 * this.classRate;
        let taxAmt3852 = this.taxAmt3852 ? this.taxAmt3852 : 0;

        this.taxDiff3852 = this.roundDecimal(taxAmt3852) - this.roundDecimal(txAmt);
        this.taxDiffErr3852 = (Math.abs(this.taxDiff3852) >= 1) ? true : false;
        this.activityCd3852 = this.taxDiffErr3852 ? "Error" : "Valid";
    }

    /**
     * calculations for form 5210
     */
    public calculateQty5210() {
        if (!this.isQty5210Pristine) return;
        if (this.tobaccoSubTypeName === this.CIGARETTES) {
            this.smallSticks = this.qty3852;
            let largeSticks: number = this.largeSticks ? this.largeSticks : 0;
            this.qty52105 = Number(this.smallSticks) + largeSticks;
        } else if (this.tobaccoSubTypeName === this.CIGARS) {
            this.largeSticks = this.qty3852;
            let smallSticks: number = this.smallSticks ? this.smallSticks : 0;
            this.qty52105 = Number(this.largeSticks) + smallSticks;
        } else {
            this.qty52105 = this.qty3852;
        }
    }

    public calculateTaxDiff5210() {
        let taxAmt3852 = (!this.taxAmt3852) ? 0 : this.taxAmt3852;
        let qty52105 = this.qty52105 ? this.qty52105 : 0;
        let adjustmentRemoval = this.adjustmentRemoval ? this.adjustmentRemoval : 0;
        let grandTotal = (Number(qty52105) +  Number(adjustmentRemoval)) * this.classRate;

        let taxDiff5210 = this.roundDecimal(grandTotal) - this.roundDecimal(taxAmt3852);
        this.taxDiff5210 = this.roundDecimal(taxDiff5210);
        this.taxDiffErr5210 = this.taxDiff5210 !== 0 ? true : false;
        this.activityCd5210 = this.taxDiffErr5210 ? "Error" : "Valid";
    }


    public calculateQtyDiff5210() {
        var qty3852 = this.qty3852 ? this.qty3852 : 0;
        var qty5210 = this.qty52105 ? this.qty52105 : 0;
        var adjustmentRemoval = this.adjustmentRemoval ? this.adjustmentRemoval : 0;
        this.grandRemovalTotal = (Number(qty5210) +  Number(adjustmentRemoval));
        var qty5210Total = this.grandRemovalTotal;
        this.qtyDiff5210 = this.roundDecimal(qty3852 - qty5210Total, 3);
        this.qtyDiffErr5210 = Math.abs(this.qtyDiff5210) >= this.thresholdValue ? true : false;
        this.activityCd5210 = this.qtyDiffErr5210 ? "Error" : "Valid";
    }
    // /**
    // * calculations for form 5000
    // */
    public calculateTaxAmount5000() {

        if (this.parent) {
            let taxAmt5000 = this.parent.taxAmt5000 ? this.roundDecimal(this.parent.taxAmt5000) : 0;
            this.parent.taxAmt5000 = (taxAmt5000 += this.roundDecimal(this.taxAmt3852) - this.parent.taxAmt5000Chld[this.tobaccoSubTypeName]);
            this.parent.taxAmt5000Chld[this.tobaccoSubTypeName] = this.roundDecimal(this.taxAmt3852);
            return;
        }

        let taxAmt3852;
        taxAmt3852 = (!this.taxAmt3852) ? 0 : this.taxAmt3852;
        this.taxAmt5000 = this.roundDecimal(taxAmt3852);

    }

    public calculateTaxDiff5000() {

        // if parent exists i.e. ChewandSnuff or PipeRYO set taxDiff and taxDiffError on parent
        if (this.parent) {
            // if(this.isUndefined(this.parent.taxAmountTotal5000)) return;

            let p = this.parent;
            let taxAmountTotal5000 = (p.taxAmountGrandTotal5000 ? p.taxAmountGrandTotal5000 : 0);
            let taxAmt5000 = (p.taxAmt5000 ? p.taxAmt5000 : 0);
            // p.taxDiff5000 = this.formatDecimalNumber(taxAmountTotal5000.toString(),this.FRACTION_SIZE) - taxAmt5000;
            p.taxDiff5000 = this.roundDecimal(taxAmt5000) - this.roundDecimal(taxAmountTotal5000);
            p.taxDiffErr5000 = Math.abs(p.taxDiff5000) >= 1 ? true : false;
            p.isTaxBrkDwn5000Pristine = (p.taxAmountTotal5000) ? false : true;
            p.activityCd5000 = p.taxDiffErr5000 ? "Error" : "Valid";
            return;

        }

        // if(this.isUndefined(this.taxAmt5000))return;

        let taxAmt5000 = (!this.taxAmt5000) ? 0 : this.taxAmt5000;
        let taxAmountTotal5000 = this.taxAmountGrandTotal5000 ? this.taxAmountGrandTotal5000 : 0;
        let taxBrkDwnTotal = taxAmountTotal5000 + (this.taxAmountTotal5000Cigar ? this.taxAmountTotal5000Cigar : 0);
        // this.taxDiff5000 = this.formatDecimalNumber(taxBrkDwnTotal.toString(),this.FRACTION_SIZE) - taxAmt5000;
        this.taxDiff5000 = this.roundDecimal(taxAmt5000) - this.roundDecimal(taxBrkDwnTotal);
        this.taxDiffErr5000 = Math.abs(this.taxDiff5000) >= 1 ? true : false;
        this.isTaxBrkDwn5000Pristine = taxBrkDwnTotal ? false : true;
        this.activityCd5000 = this.taxDiffErr5000 ? "Error" : "Valid";
    }

    public is5000Error(): boolean {
        let taxDiffErr5000 = this.parent ? this.parent.taxDiffErr5000 : this.taxDiffErr5000;
        let activityCd5000 = this.parent ? this.parent.activityCd5000 : this.activityCd5000;
        return activityCd5000 ? taxDiffErr5000 : true;
    }


    public is5210Error(): boolean {
        return this.activityCd5210 ? this.qtyDiffErr5210 : true;
    }

    public is3852Error(): boolean {
        if (this.tobaccoSubTypeName === "Cigars") return false;
        return this.activityCd3852 ? this.taxDiffErr3852 : true;
    }

    public isUndefined(value): boolean {
        if (value === "" || value === 0) return false;
        return (value) ? false : true;
    }

    public getForm3852Detail() {

        var formDetail = {};
        formDetail["tobaccoClassId"] = this.tobaccoClassId;
        formDetail["removalQty"] = this.qty3852;
        formDetail["taxesPaid"] = (this.taxAmt3852 !== 0 && !this.taxAmt3852) ? null : this.roundDecimal(this.taxAmt3852);
        formDetail["dolDifference"] = this.roundDecimal(this.taxDiff3852);
        formDetail["activityCd"] = (this.taxDiffErr3852) ? this.ERROR : this.VALID;
        if(this.tobaccoClassId != 8) 
            formDetail["calcTaxRate"] = this.calculatedTaxDiff3852();

        return formDetail;
    }

    public getForm5210Detail(): any[] {
        var formDetails = [];
        var formDetail = {};

        formDetail["tobaccoClassId"] = this.tobaccoClassId;
        formDetail["removalQty"] = this.qty52105;
        formDetail["taxesPaid"] = (this.qty52105 ? this.qty52105 : 0) * this.classRate;
        formDetail["dolDifference"] = this.roundDecimal(this.qtyDiff5210, 3);
        formDetail["activityCd"] = (this.qtyDiffErr5210) ? this.ERROR : this.VALID;
        formDetail["adjustmentRemoval"]  = this.adjustmentRemoval;
        let grandRemovalTotal = Number(this.qty52105 ? this.qty52105 : 0) + Number((this.adjustmentRemoval ? this.adjustmentRemoval : 0));
        formDetail["grandRemovalTotal"]  = grandRemovalTotal;
        formDetail["grandTotal"] = (grandRemovalTotal) * this.classRate;
        formDetail["enableAdj"] = this.isEnableAdjustment5210?'Y':'N';
        formDetails.push(formDetail);

        for (var subclass in this.ttSubClassTypeConsts) {
            let formDetail = {};
            if (this.ttSubClassTypeConsts[subclass].tobaccoSubTypeName === "Large") {
                formDetail["tobaccoClassId"] = this.ttSubClassTypeConsts[subclass].tobaccoClassId;
                formDetail["removalQty"] = this.largeSticks;
                formDetails.push(formDetail);
            } else if (this.ttSubClassTypeConsts[subclass].tobaccoSubTypeName === "Small") {
                formDetail["tobaccoClassId"] = this.ttSubClassTypeConsts[subclass].tobaccoClassId;
                formDetail["removalQty"] = this.smallSticks;
                formDetails.push(formDetail);
            }
        }

        return formDetails;
    }

    public getForm5000Detail() {

        var formDetails = [];
        /** Form detail from total tax paid */
        var ttClass5000: TobaccoSubType;  // = this.getTTClassForm5000();
        if (this.tobaccoSubTypeName === "Chew" || this.tobaccoSubTypeName === "Snuff" || this.tobaccoSubTypeName === "Pipe" || this.tobaccoSubTypeName === "RollYourOwn") {
            if (this.parent && !this.parent.parentAddedToFormDetails) {
                var formDetail = {};
                ttClass5000 = this.parent;
                formDetail["tobaccoClassId"] = ttClass5000.tobaccoClassId;
                formDetail["taxesPaid"] = ttClass5000.taxAmountTotal5000 + (ttClass5000.taxAmountTotal5000Cigar ? ttClass5000.taxAmountTotal5000Cigar : 0);
                formDetail["dolDifference"] = this.roundDecimal(ttClass5000.taxDiff5000);
                formDetail["activityCd"] = (ttClass5000.taxDiffErr5000) ? this.ERROR : this.VALID;
                formDetail["adjATotal"]  = ttClass5000.taxAmountAdjusmentA;
                formDetail["adjBTotal"]  = ttClass5000.taxAmountAdjustmentB;
                formDetail["adjustmentTotal"]  = ttClass5000.taxAmountTotalAdjustment;
                formDetail["grandTotal"] = ttClass5000.taxAmountGrandTotal5000;
                formDetail["enableAdj"] = ttClass5000.isEnableAdjutment?'Y':'N';
                formDetails.push(formDetail);
            }
        } else {
            var formDetail = {};
            formDetail["tobaccoClassId"] = this.tobaccoClassId;
            formDetail["taxesPaid"] = this.taxAmountTotal5000 + (this.taxAmountTotal5000Cigar ? this.taxAmountTotal5000Cigar : 0);
            formDetail["dolDifference"] = this.roundDecimal(this.taxDiff5000);
            formDetail["activityCd"] = (this.taxDiffErr5000) ? this.ERROR : this.VALID;
            formDetail["adjATotal"]  = this.taxAmountAdjusmentA;
            formDetail["adjBTotal"]  = this.taxAmountAdjustmentB;
            formDetail["adjustmentTotal"]  = this.taxAmountTotalAdjustment;
            formDetail["grandTotal"] = this.taxAmountGrandTotal5000;
            formDetail["enableAdj"] = this.isEnableAdjutment?'Y':'N';
            formDetails.push(formDetail);
        }

        /** Form details from tax break down for Cigars and other tobaccco types**/
        /*if (this.tobaccoSubTypeName == "Cigars") {
            for (var subclass in this.ttSubClassTypeConsts) {
                let formDetail = {};
                if (this.ttSubClassTypeConsts[subclass].tobaccoSubTypeName == "Large") {
                    let txBrkFormDetails = this.getTaxBrkDownForm5000(this.ttSubClassTypeConsts[subclass], this.summationBox, this.summationBoxFormIds);
                    formDetails = formDetails.concat(txBrkFormDetails);
                }
            }
        } else*/ 
        if (this.tobaccoSubTypeName === "Chew" || this.tobaccoSubTypeName === "Snuff" || this.tobaccoSubTypeName === "Pipe" || this.tobaccoSubTypeName === "RollYourOwn") {
            if (this.parent && !this.parent.parentAddedToFormDetails) {
                this.parent.parentAddedToFormDetails = true;
                let txBrkFormDetails = this.getTaxBrkDownForm5000(this.getTTClassForm5000(), this.getTTClassForm5000().summationBox, this.getTTClassForm5000().summationBoxFormIds);
                formDetails = formDetails.concat(txBrkFormDetails);
            }
        } else {
            let txBrkFormDetails = this.getTaxBrkDownForm5000(this.getTTClassForm5000(), this.getTTClassForm5000().summationBox, this.getTTClassForm5000().summationBoxFormIds);
            formDetails = formDetails.concat(txBrkFormDetails);
        }

        return formDetails;
    }


    private getTaxBrkDownForm5000(tobaccoClass: TobaccoSubType, taxBreakDwnArray: any[], formIdArray: any[]): any[] {

        var formDetails = [];
        for(var i = 0 ; i<taxBreakDwnArray.length; i++){
            if(taxBreakDwnArray[i]!== 'undefined'){ //&& taxBreakDwnArray[i] !== 0 
                var formDetail = {};
                formDetail["tobaccoClassId"] = tobaccoClass.tobaccoClassId;
                // if (formIdArray.length > 0) formDetail["lineNum"] = formIdArray[i].lineNum;
                formDetail["taxesPaid"] = taxBreakDwnArray[i];
                formDetails.push(formDetail);
            }
            else{
                var formDetail = {};
                formDetail["tobaccoClassId"] = tobaccoClass.tobaccoClassId;
                formDetail["taxesPaid"] = null;
                formDetails.push(formDetail);
            }
        }
        return formDetails;
    }

    private getTTClassForm5000() {
        if (this.tobaccoSubTypeName === "Chew" || this.tobaccoSubTypeName === "Snuff" || this.tobaccoSubTypeName === "Pipe" || this.tobaccoSubTypeName === "RollYourOwn") {
            return this.parent;
        }
        else
            return this;
    }

    public formatDecimalNumber(value: string, fractionSize: number = 2): number {
        let [integer, fraction = ""] = (value || "").split(this.DECIMAL_SEPARATOR);

        // integer = integer.replace(new RegExp(this.THOUSANDS_SEPARATOR, "g"), "");

        fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
            ? this.DECIMAL_SEPARATOR + (fraction + this.PADDING).substring(0, fractionSize)
            : "";

        return Number(integer + fraction);
    }

    public roundDecimal(value, fractionSize = 2): number {
        let val = (value) ? value : 0;
        return Number(Math.round(Number(val + 'e' + fractionSize)) + 'e-' + fractionSize);
    }
}
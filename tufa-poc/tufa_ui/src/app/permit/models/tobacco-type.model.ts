import { DoCheck } from '@angular/core';
import { TobaccoSubType } from './tobacco-subtype.model';
import { CommentModel } from './comment/comment.model';
import { TTBAdjustmentDetail } from './ttb-adjustment.model';

const CIGARETTES = 'Cigarettes';
const CIGARS = 'Cigars';
const CHEW = 'Chew';
const SNUFF = 'Snuff';
const PIPE = 'Pipe';
const ROY = 'RollYourOwn';


export class TobaccoType {

    showCigarettes: boolean;
    showCigars: boolean;
    showSnuffs: boolean;
    showPipes: boolean;
    showChews: boolean;
    showRollYourOwn: boolean;

    isTTSelected: number;

    permitId: number;
    periodId: number;
    period: string;
    permitNumber: string;
    periodOneTaxId_500024:string;
    periodTwoTaxId_500024:string;
    periodThreeTaxId_500024:string;
    
    FORM_3852 = "3852";
    FORM_5210 = "5210.5";
    FORM_5000 = "5000.24";

    private tobaccoTypes: TobaccoSubType[] = [];
    // initialize tt constants on page load
    private ttConstantsArray: TobaccoSubType[] = [];
    public formDetails: any[] = [];
    public adjustmentDetails: Array<TTBAdjustmentDetail[]> =[];

    public formComments = {};

    constructor() {
        this.showCigarettes = false;
        this.showCigars = false;
        this.showSnuffs = false;
        this.showPipes = false;
        this.showChews = false;
        this.showRollYourOwn = false;
        this.isTTSelected = 0;
    }


    ngDoCheck() {
    }

    addTobaccoType(ttName: string): boolean {

        this.setTTSelected();
        (this.displayTTypeByName(ttName)) ? this.addFromTTypeConstsToTTypes(ttName) : this.removeFromTobaccoTypes(ttName);

        if (this.displayTTypeByName(ttName)) this.getTobaccoType(ttName).isDisplayed = true;
        return this.displayTTypeByName(ttName);
    }


    public addFromTTypeConstsToTTypes(typeName?: string, ttype?: TobaccoSubType): TobaccoSubType {

        if (!ttype)
            // get tobacco type from tobacco type const array
            ttype = this.getTobaccoTypeConsts(typeName);

        // create new tobacco type
        let newTT = new TobaccoSubType(ttype.tobaccoSubTypeName, ttype.classRate, ttype.taxDiffThresh, ttype.tobaccoClassId, ttype.parentConst
            ? ttype.parentConst.ttParentConstClassId : null, ttype.thresholdValue);

        // set subTypes consts for newTT
        newTT.setTTSubClassTypeConsts(ttype.ttSubClassTypeConsts);

        // get parent instance if already set in a child else create one and add it to the child.

        if (ttype.parentConst) {
            let parentTT: TobaccoSubType;
            // set parent const property for newTT
            newTT.parentConst = ttype.parentConst;
            parentTT = this.getParentFromChildType(ttype.parentConst);
            if (!parentTT) {
                let p = ttype.parentConst;
                parentTT = new TobaccoSubType(p.tobaccoSubTypeName, p.classRate, p.taxDiffThresh, p.tobaccoClassId, p.ttParentConstClassId, p.thresholdValue);
                newTT.parent = parentTT;
            } else
                newTT.parent = parentTT;

            // set the newTT taxAmt5000 in parent
            parentTT.taxAmt5000Chld[newTT.tobaccoSubTypeName] = 0;
        }

        this.addToTobaccoTypes(newTT);
        return newTT;
    }

    private getParentFromChildType(parentToSearch: TobaccoSubType) {
        if (parentToSearch) {
            let parentTT = this.getParentFromChildTypes(parentToSearch);
            return parentTT;
        }
    }

    public addTTConstsToTTByClassId(typeClassId: number): TobaccoSubType {
        let ttype: TobaccoSubType = this.getTTypeByClassIdFromConsts(typeClassId);
        let newTT = new TobaccoSubType(ttype.tobaccoSubTypeName, ttype.classRate, ttype.taxDiffThresh, ttype.tobaccoClassId, ttype.parentConst ? ttype.parentConst.ttParentConstClassId : null, ttype.thresholdValue);
        // set subTypes consts for newTT
        newTT.setTTSubClassTypeConsts(ttype.ttSubClassTypeConsts);
        // set parent for newTT
        newTT.parentConst = ttype.parentConst;
        this.addToTobaccoTypes(newTT);
        return newTT;
    }

    public addToTobaccoTypes(ttype: TobaccoSubType) {
        if (this.isTobaccoType(ttype.tobaccoSubTypeName)) return;
        this.tobaccoTypes.push(ttype);
    }

    public removeFromTobaccoTypes(typeName: string) {
        this.tobaccoTypes.forEach(function (item: TobaccoSubType, index, array) {
            if (item.tobaccoSubTypeName === typeName) {
                if (item.parent) {
                    item.parent.taxAmt5000 -= item.parent.taxAmt5000Chld[item.tobaccoSubTypeName];
                    item.parent.taxDiff5000 = item.parent.taxDiff5000 - item.parent.taxAmt5000Chld[item.tobaccoSubTypeName];
                    item.parent.taxAmt5000Chld[item.tobaccoSubTypeName] = 0;
                    item.parent.taxDiffErr5000 = Math.abs(item.parent.taxDiff5000) >= 1 ? true : false;
                    item.parent.activityCd5000 = item.parent.taxDiffErr5000 ? "Error" : "Valid";
                }
                // remove the 5000 form Id
                /* this.formDetails.forEach(function(it,index,array){
                             let tobaccoId = item.parent?item.parent.tobaccoClassId: item.tobaccoClassId;
                             if(it.tobaccoClassId == tobaccoId)
                                 array.splice(index,1);
                 });*/

                // remove taboccotype
                array.splice(index, 1);
            }
        }, this);
    }

    public addTobaccoTypeConsts(ttype: TobaccoSubType) {
        this.ttConstantsArray.push(ttype);
    }


    public getTobaccoTypeConsts(typeName: string): TobaccoSubType {
        let tobaccoSubType;
        this.ttConstantsArray.forEach(function (arrayItem: TobaccoSubType) {
            if (arrayItem.tobaccoSubTypeName === typeName) {
                tobaccoSubType = arrayItem;
            }
        });
        return tobaccoSubType;
    }

    public getTobaccoType(typeName: string): TobaccoSubType {

        let tobaccoSubType;
        this.tobaccoTypes.forEach(function (arrayItem: TobaccoSubType) {
            if (arrayItem.tobaccoSubTypeName === typeName) {
                tobaccoSubType = arrayItem;
            }
        });
        return tobaccoSubType;
    }

    public getTobaccoTypeParent(parentTypeName: string): TobaccoSubType {
        let tt = this.getTobaccoTypeConsts(parentTypeName);
        let parent: TobaccoSubType;
        this.ttConstantsArray.forEach(function (item, index, array) {
            if (item.ttParentConstClassId === tt.tobaccoClassId) {
                let child = this.getTobaccoType(item.tobaccoSubTypeName);
                if (child) parent = child.parent;
            }
        }, this);

        return parent;
    }

    /*public getChildTobaccoTypesFromConsts(parenttId: number, parent?:TobaccoSubType): TobaccoSubType[]{
        let children = [];
        if(!parent)
          parent = this.getTTypeByClassIdFromConsts(parenttId);

        this.getTobaccoTypesConst().forEach(function (item, index, array) {
              if (item.ttParentClassId == parenttId) {
                  children.push(item);
              }
          }, this);
        return children;
    }*/

    public getTTypeByClassIdFromConsts(ttClassId: number) {
        let tobaccoSubType;
        this.ttConstantsArray.forEach(function (arrayItem: TobaccoSubType) {
            if (arrayItem.tobaccoClassId === ttClassId) {
                tobaccoSubType = arrayItem;
            }
        });
        return tobaccoSubType;
    }

    public getTTypeByClassId(ttClassId: number) {
        let tobaccoSubType;
        this.tobaccoTypes.forEach(function (arrayItem: TobaccoSubType) {
            if (arrayItem.tobaccoClassId === ttClassId) {
                tobaccoSubType = arrayItem;
            }
        });
        return tobaccoSubType;
    }


    public isTobaccoType(typeName: string) {

        let isttPresent: boolean = false;
        this.tobaccoTypes.forEach(function (arrayItem: TobaccoSubType) {
            if (arrayItem.tobaccoSubTypeName === typeName)
                isttPresent = true;
        });

        return isttPresent;
    }

    public getTobaccoTypes() {
        return this.tobaccoTypes;
    }

    public getTobaccoTypesConst() {
        return this.ttConstantsArray;
    }

    public getParentFromChildTypes(parentTType: TobaccoSubType): TobaccoSubType {

        let parent: TobaccoSubType;
        // check if child types have been added. If so get the parent instance from any child type and return it.

        let childArry: TobaccoSubType[] = [];
        childArry = this.getChildTypes(parentTType);

        for (let i in childArry) {
            parent = childArry[i].parent;
            if (parent) break;
        }

        return parent;
    }

    public getChildTypes(parentTType: TobaccoSubType): TobaccoSubType[] {
        let ttSubArry: TobaccoSubType[] = parentTType.ttSubClassTypeConsts;
        let childArry: TobaccoSubType[] = [];
        for (let i in ttSubArry) {
            let childType = this.getTTypeByClassId(ttSubArry[i].tobaccoClassId);
            if (childType) childArry.push(childType);
        }
        return childArry;
    }

    public getSiblingTTSelected(tobaccoClassId: number, parentTT: TobaccoSubType, tobaccoSubTypeName?: string) {
        if (parentTT) {
            let children: TobaccoSubType[] = this.getChildTypes(parentTT);
            for (let child in children) {
                if (children[child].tobaccoClassId !== tobaccoClassId)
                    return children[child];
            }
        }
    }

    public displayTTypeByName(ttName: string, display?: boolean): boolean {

        let changeType: boolean = false;
        switch (ttName) {
            case CIGARETTES:
                if (display) this.showCigarettes = display;
                changeType = this.showCigarettes;
                break;
            case CIGARS:
                if (display) this.showCigars = display;
                changeType = this.showCigars;
                break;
            case CHEW:
                if (display) this.showChews = display;
                changeType = this.showChews;
                break;
            case SNUFF:
                if (display) this.showSnuffs = display;
                changeType = this.showSnuffs;
                break;
            case PIPE:
                if (display) this.showPipes = display
                changeType = this.showPipes;
                break;
            case ROY:
                if (display) this.showRollYourOwn = display;
                changeType = this.showRollYourOwn;
                break;
            default:
                break;
        }
        this.setTTSelected();
        return changeType;
    }

    private setTTSelected() {
        this.isTTSelected = (this.showCigarettes ? 1 : 0) + (this.showCigars ? 1 : 0) + (this.showChews ? 1 : 0) + (this.showSnuffs ? 1 : 0) + (this.showPipes ? 1 : 0) + (this.showRollYourOwn ? 1 : 0);
    }

    addForm(form) {
        this.formDetails.push(form);
    }

    removeForm(formTypecode: string, tobaccoClassId?: string) {
        this.formDetails.forEach(function (item, index, array) {
            if (tobaccoClassId && item.tobaccoClassId === tobaccoClassId && item.formTypeCd === formTypecode)
                array.splice(index, 1);

            if (!tobaccoClassId && item.formTypeCd === formTypecode)
                array.splice(index, 1);
        });
    }

    public getForm(formTypecode: string, tobaccoClassId?: number) {

        if (tobaccoClassId) {
            for (let i in this.formDetails) {
                if (this.formDetails[i].formTypeCd === formTypecode && this.formDetails[i].tobaccoClassId === tobaccoClassId)
                    return this.formDetails[i];
            }
        } else {
            for (let i in this.formDetails) {
                if (this.formDetails[i].formTypeCd === formTypecode)
                    return this.formDetails[i];
            }
        }
    }

    public clearFormIds() {
        this.formDetails = [];
    }
    public getFormDetails() {

        let selectedTobaccoTypes = (this.showCigarettes ? "CIG_" : "") + (this.showCigars ? "CGR_" : "") + (this.showChews ? "CHE_" : "") + (this.showSnuffs ? "SNU_" : "") + (this.showPipes ? "PIP_" : "") + (this.showRollYourOwn ? "ROL_" : "");

        let submittedForms = [];
        let formDetails3852 = [];
        let formDetails5210 = [];
        let formDetails5000 = [];

        let submittedForm3852 = {};
        let submittedForm5210 = {};


        for (let tt in this.tobaccoTypes) {
            formDetails3852.push(this.tobaccoTypes[tt].getForm3852Detail());
            formDetails5210 = formDetails5210.concat(this.tobaccoTypes[tt].getForm5210Detail());

             formDetails5000 = formDetails5000.concat(this.tobaccoTypes[tt].getForm5000Detail());
            //let formDetails5000ForEachTT = [];
            //formDetails5000ForEachTT = this.tobaccoTypes[tt].getForm5000Detail();
            //if (formDetails5000ForEachTT && formDetails5000ForEachTT.length > 0)
            //formDetails5000.push(formDetails5000ForEachTT);
        }


        let form3852 = this.getForm(this.FORM_3852);
        submittedForm3852["periodId"] = this.periodId;
        submittedForm3852["permitId"] = this.permitId;
        submittedForm3852["formId"] = form3852 ? form3852.formId : null;
        submittedForm3852["formNum"] = form3852 ? form3852.formNum : null;
        submittedForm3852["formTypeCd"] = this.FORM_3852;
        submittedForm3852["selectedTobaccoTypes"] = selectedTobaccoTypes;
        submittedForm3852["formDetails"] = formDetails3852;


        let form5210 = this.getForm(this.FORM_5210);
        submittedForm5210["periodId"] = this.periodId;
        submittedForm5210["permitId"] = this.permitId;
        submittedForm5210["formId"] = form5210 ? form5210.formId : null;
        submittedForm5210["formNum"] = form5210 ? form5210.formNum : null;
        submittedForm5210["formTypeCd"] = this.FORM_5210;
        submittedForm5210["formDetails"] = formDetails5210;

        submittedForms.push(submittedForm3852);
        submittedForms.push(submittedForm5210);

        //for (let i in formDetails5000) {
        let submittedForm5000 = {};
        let form5000 = this.getForm(this.FORM_5000);
        submittedForm5000["periodId"] = this.periodId;
        submittedForm5000["permitId"] = this.permitId;
        submittedForm5000["formTypeCd"] = this.FORM_5000;
        submittedForm5000["formNum"] = form5000 ? form5000.formNum : null;
        submittedForm5000["formId"] = form5000 ? form5000.formId : null;
        submittedForm5000["formDetails"] = formDetails5000;
        submittedForms.push(submittedForm5000);
        //}

        return submittedForms;
    }


    /** get comments for a form */
    public getComments(formTypeCd) {
        return this.formComments[formTypeCd];
    }

    /** add comment**/
    public addComment(comment: CommentModel) {

        let comments = this.getComments(comment.formTypeCd);

        if (comments) {
            let index = this.commentIndex(comment, comments);
            if (index)
                comments[index] = comment;
            else
                comments.push(comment);
        }
        else {
            let commentArry: CommentModel[] = [];
            commentArry.push(comment);
            this.formComments[comment.formTypeCd] = commentArry;
        }
    }

    commentIndex(searchComment, comments): string {

        for (let i in comments) {
            let comment: CommentModel = comments[i];
            if (comment.commentSeq === searchComment.commentSeq)
                return i;
        }

        return null;
    }

    public deleteComments(formTypeCd: string, tobaccoClassId?: number) {
        this.formComments[formTypeCd] = [];
    }

}

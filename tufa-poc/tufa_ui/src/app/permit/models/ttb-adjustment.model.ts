import { DatatableFilter } from "./../../../app/shared/datatable/DataTableFilter";

export class TTBAdjustmentDetail {
    adjustmentFormId: number;
    formId: number;
    tobaccoClassId: number;
    adjLineNum: number;
    taxAmount: number;
    taxReturnId: string;
    adjType: string;
    lineDesc: string;
}

export class TTBAdjustments {
    ttbAdjustmentDetail : TTBAdjustmentDetail[] = [];
    taxAmountAdjusmentA: number;
    taxAmountAdjusmentB: number;
    taxAmountTotalAdjustment: number;
    tobaccoClassId: number;
    formId: number;
}

export class SaveTTBAdjustments {
    ttbAdjustmentList : TTBAdjustments[] = [];
    periodId: number;
    permitId: number;
}

export class TTBAdjustmentDetailFilter {
    adjLineNumFilter: string;
    taxAmountFilter: string;
    taxReturnIdFilter:string; 
    results: TTBAdjustmentDetail[];
}


export class TTBAdjustmentDetailFilterData implements DatatableFilter{
    private _adjLineNumFilter: string;
    private _taxAmountFilter: string;
    private _taxReturnIdFilter: string;
   
    public get adjLineNumFilter(): string {
        return this._adjLineNumFilter;
    }
    public set adjLineNumFilter(value: string) {
        this._adjLineNumFilter = value;
    }
    
    public get taxAmountFilter(): string {
        return this._taxAmountFilter;
    }
    public set taxAmountFilter(value: string) {
        this._taxAmountFilter = value;
    }
    
    public get taxReturnIdFilter(): string {
        return this._taxReturnIdFilter;
    }
    public set taxReturnIdFilter(value: string) {
        this._taxReturnIdFilter = value;
    }
    
    public isFilterSelected() {
        return (this._adjLineNumFilter||this._taxAmountFilter||this._taxReturnIdFilter)?true:false;
    }
}
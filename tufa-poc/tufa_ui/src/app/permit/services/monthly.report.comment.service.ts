import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { CommentModel } from '../models/comment/comment.model';
import { PermitPeriod } from '../models/permit-period.model';
import { MonthlyReportEventService } from './monthly.report.event.service';
import { environment } from "../../../environments/environment";
import { RequestOptions, RequestMethod } from "@angular/http";

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class MonthlyReportCommentService {

    private baseurl = environment.apiurl + '/api/v1/';

    constructor(private _httpClient: HttpClient,
        private monthlyReportService: MonthlyReportEventService) { }

    public saveMonthlyReportComment(comment: CommentModel, permitperiod: PermitPeriod) {
        let url = this.baseurl + 'permit/' + permitperiod.permitId + '/period/' + permitperiod.periodId + '/comment';
        return this._httpClient.post(url, comment, httpOptions);
    }

    public getZeroReportComment(permitId ,periodId): Observable<CommentModel[]> 
    {
        let url = this.baseurl + 'permit/' + permitId + '/period/' + periodId + '/zerocomments';;
        return this._httpClient.get(url, httpOptions).pipe(
			tap(_ => this.log("getZeroReportComment")),
			catchError(this.handleError<any>("getZeroReportComment"))
		);
    }

    public deleteMonthlyReportComments(formIds: string[]) {
        let permitperiod: PermitPeriod = this.monthlyReportService.getPermitPeriod();
        let url = this.baseurl + 'permit/' + permitperiod.permitId + '/period/' + permitperiod.periodId + '/comment';
        let options = new RequestOptions({
            body: formIds,
            method: RequestMethod.Delete
		});
		const header: HttpHeaders = new HttpHeaders().append('Content-Type', 'application/json; charset=UTF-8');
		const httpOptions2 = {
			headers: header,
			body: formIds
		};		
        return this._httpClient.delete(url, httpOptions2);
    }

	public deleteMonthlyReportOrphanedComments(formIds: string[]) {
        let permitperiod: PermitPeriod = this.monthlyReportService.getPermitPeriod();
        let url = this.baseurl + 'permit/' + permitperiod.permitId + '/period/' + permitperiod.periodId + '/orphancomments';
        let options = new RequestOptions({
            body: formIds,
            method: RequestMethod.Delete
		});
		const header: HttpHeaders = new HttpHeaders().append('Content-Type', 'application/json; charset=UTF-8');
		const httpOptions2 = {
			headers: header,
			body: formIds
		};		
        return this._httpClient.delete(url, httpOptions2);
    }
    	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { PermitPeriod } from '../models/permit-period.model';
import { CommentModel } from '../models/comment/comment.model';

/**
 *
 *
 * @export
 * @class MonthlyReportService
 */
@Injectable()
export class MonthlyReportService {
  private permitperiod;
  private newComment;
  private permitperiodSource = new Subject<PermitPeriod>();
  private childViewSourceLoaded = new Subject<string>();
  private commentAddedSource = new Subject<CommentModel>();
  private commentDeleteSource = new Subject<any[]>();
  private form5220IncudedSource = new Subject<boolean>();

  // tslint:disable-next-line:member-ordering
  permitperiodSourceLoaded$ = this.permitperiodSource.asObservable();
  childViewSourceLoaded$ = this.childViewSourceLoaded.asObservable();
  commentAddedSource$ = this.commentAddedSource.asObservable();
  commentDeleteSource$ = this.commentDeleteSource.asObservable();
  form5220IncudedSource$ = this.form5220IncudedSource.asObservable();

  public setPermitPeriod(permitperiod){
      this.permitperiod = permitperiod;
  }

  public getPermitPeriod():PermitPeriod{
      return this.permitperiod;
  }

  public setNewComment(comment:CommentModel){
    this.newComment = comment;
  }

 public getNewComment():CommentModel{
   return this.newComment;
 }
  /**triggered by the child component */
  public triggerChildViewLoaded(){
        this.childViewSourceLoaded.next();
  }

  /** triggered by the parent component */
  public triggerPermitPeriodUpdate() {
    this.permitperiodSource.next(this.permitperiod);
  }

  /** triggered by comment popup component when a comment is succesfully added **/
  public triggerCommentAdded(comment:CommentModel){
    this.commentAddedSource.next(comment);
  }

  /** trigger comments delete event */
  public  triggerCommentsDelete(formIds:any []){
    this.commentDeleteSource.next(formIds);
  }

  public triggerIncludeForm5220(include5220: boolean){
      this.form5220IncudedSource.next(include5220);
  }
}


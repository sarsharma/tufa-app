import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { PermitExcludeModel } from '../models/permit-exclude.model';
import { PermitPeriod } from "../models/permit-period.model";
import { environment } from "./../../../environments/environment";


const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class PermitExcludeService {

    private getPermitExcludeurl = environment.apiurl + '/api/v1/permit/excludepermit';
    private getauditTrailsurl = environment.apiurl + '/api/v1/permit/getexcludepermitdetails';
    private getcompanyurl = environment.apiurl + '/api/v1/companies/getexcludepermitdetails';

    constructor(private _httpClient: HttpClient
    ) { }

    saveExcludedPermitDetails(permitId: any, CompanyId: any, excludeFlag:string, model: PermitExcludeModel[]) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let updatedurl = this.getPermitExcludeurl + "/" + CompanyId + "/" + permitId + "/" + excludeFlag; 
        return this._httpClient.post(updatedurl, model, httpOptions);
    }

    getPermitAuditTrail(permitId: any, companyid: any): any {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let updatedurl = this.getauditTrailsurl + "/" + companyid + "/" + permitId ;
        return this._httpClient.get(updatedurl, httpOptions);
    }

    IncludeExcludedPermitDetails(missingForms: any[], periodId: string, permitId: string) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let updatedurl = this.getPermitExcludeurl + "/" + periodId + "/" + permitId + "/forms";
        return this._httpClient.post(updatedurl, missingForms, httpOptions);
    }

    getExcludedStatus(companyID: any, permitId: any): any {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let updatedurl = this.getcompanyurl + "/" + companyID + "/" + permitId;
        return this._httpClient.get(updatedurl, httpOptions);
    }
}
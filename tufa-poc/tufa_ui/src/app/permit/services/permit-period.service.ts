/*
 @author : Deloitte
 Service class to perform PermitPeriod related operations.
 */

import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { Credentials } from '../../login/model/credentials.model';
import { AuthService } from '../../login/services/auth.service';
import { Document } from "../models/document.model";
import { PermitPeriod, TestPermitPeriod } from "../models/permit-period.model";
import { environment } from "./../../../environments/environment";
import { MissingForms } from "../models/missingForms.model";

const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class PermitPeriodService {

    private getPermitPeriodurl = environment.apiurl + '/api/v1/companies/period';
    private getattachmentsurl = environment.apiurl + '/api/reports/attachs';
    private pdfUrl = environment.apiurl + '/api/reports/view/attachs';
    private deleteattachmenturl = environment.apiurl + '/api/reports/attachs/';
    private savePermitPeriodurl = environment.apiurl + '/api/v1/companies/period';
    private isPermitPeriodzerourl = environment.apiurl + '/api/v1/companies/permit/zero/{id}';
    private uploadattachmenturl = environment.apiurl + '/api/reports/attachs';
    private updateattachmenturl = environment.apiurl + '/api/reports/update/attachs';
    private getFiscalYearsurl = environment.apiurl + '/api/v1/permit/fiscalyears';
    private getCalendarYearsurl = environment.apiurl + '/api/v1/permit/calendaryears';
    private getpermitattachmentsurl = environment.apiurl + '/api/reports/permit/attachs/';
    private uploadpermitattachmenturl = environment.apiurl + '/api/reports/permit/attachs/';
    private deletepermitattachmenturl = environment.apiurl + '/api/reports/permit/attachs/';

    private updaterptreconstatusurl = environment.apiurl + '/api/v1/companies/period';

    private credential: Credentials;

    constructor(private _httpClient: HttpClient,
        private authService: AuthService) { }

    uploadAttachment(document: any, periodId: any, permitId: any): Observable<string> {
        let params = new HttpParams();

        const options = {
            params: params,
            reportProgress: true,
        };

        return this._httpClient.post(this.uploadattachmenturl + '/' + periodId + '/' + permitId, (document), options).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }
    

    updateAttachment(document: any, periodId: any, permitId: any): any {
        return this._httpClient.post(this.updateattachmenturl + '/' + periodId + '/' + permitId, (document), httpOptions).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }

    deleteAttachment(docid: number): Observable<string> {
        return this._httpClient.delete(this.deleteattachmenturl + docid, httpOptions).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }

    deletePermitAttachment(docid: number): Observable<string> {
        return this._httpClient.delete(this.deletepermitattachmenturl + docid, httpOptions).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }
    downloadAttachment(docid: number): Observable<Document> {
        return this._httpClient.get(this.deleteattachmenturl + docid, httpOptions).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }
    
    downloadPermitAttachment(docid: number): Observable<Document> {
        return this._httpClient.get(this.deletepermitattachmenturl + docid, httpOptions).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }
    savePermitPeriod(permitPeriod: PermitPeriod): Observable<PermitPeriod> {
        return this._httpClient.put(this.savePermitPeriodurl, (permitPeriod), httpOptions).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }

    loadPermitPeriod(periodId: string, permitId: string): Observable<string> {
        if (!periodId) {
            // return of(new TestPermitPeriod()).map(o => (o));
            return of(new TestPermitPeriod()).pipe(
                tap(_ => this.log("loadPermitPeriod")),
                catchError(this.handleError<any>("loadPermitPeriod"))
            );
        } else {
            return this._httpClient.get(this.getPermitPeriodurl + '/' + periodId + '/' + permitId, httpOptions).pipe(
                tap(_ => this.log("loadPermitPeriod")),
                catchError(this.handleError<any>("loadPermitPeriod"))
            );
        }
    }

    loadDocs(periodId: string, permitId: string): Observable<string> {
        return this._httpClient.get(this.getattachmentsurl + '/' + periodId + '/' + permitId, httpOptions).pipe(
            tap(_ => this.log("loadDocs")),
            catchError(this.handleError<any>("loadDocs"))
        );
    }
    

    // Calls the REST that returns all FiscalYears
    getCalendarYears(): Observable<string> {
        return this._httpClient.get(this.getCalendarYearsurl, httpOptions).pipe(
            tap(_ => this.log("getCalendarYears")),
            catchError(this.handleError<any>("getCalendarYears"))
        );

    }

    // Calls the REST that returns all FiscalYears
    getFiscalYears(): Observable<string> {
        return this._httpClient.get(this.getFiscalYearsurl, httpOptions).pipe(
            tap(_ => this.log("getFiscalYears")),
            catchError(this.handleError<any>("getFiscalYears"))
        );
    }

    updatePermitReportReconStatus(permitId, periodId) {
        let updatereconurl = this.updaterptreconstatusurl + "/" + periodId + "/permit/" + permitId;
        return this._httpClient.put(updatereconurl, null, httpOptions).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }

    getPermitReportReconStatus(permitId, periodId) {
        let updatereconurl = this.updaterptreconstatusurl + "/" + periodId + "/permit/" + permitId;
        return this._httpClient.get(updatereconurl, httpOptions).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }

    getMissingForms(permitId, periodId): Observable<MissingForms[]> {
        let updatedurl = this.getPermitPeriodurl + "/" + periodId + "/" + permitId + "/forms";
        return this._httpClient.get(updatedurl, httpOptions).pipe(
            tap(_ => this.log("getMissingForms")),
            catchError(this.handleError<any>("getMissingForms"))
        );
    }

    saveMissingForms(missingForms: any[], periodId: string, permitId: string) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let updatedurl = this.getPermitPeriodurl + "/" + periodId + "/" + permitId + "/forms";
        return this._httpClient.post(updatedurl, missingForms, httpOptions);
    }

    getFormExport(permitId: any, periodId: any, form: any, tobaccoType: any): any {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let updatedurl = this.getPermitPeriodurl + "/export/" + permitId + "/" + periodId + "/" + form + "/" + tobaccoType;
        return this._httpClient.get(updatedurl, httpOptions);
    }

    setLinkAuthentication(flag: boolean) {
        flag ? this.authService.setAccessTokenCookie() : this.authService.removeAccessTokenCookie();
    }

    /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log("${operation} failed: ${error.message}");

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Attribute Service message with the MessageService */
    private log(message: string) { }

    loadPermitDocs(permitId: string): Observable<string> {
        return this._httpClient.get(this.getpermitattachmentsurl+'permitId/' + permitId, httpOptions).pipe(
            tap(_ => this.log("loadDocs")),
            catchError(this.handleError<any>("loadDocs"))
        );
    }
    uploadPermitAttachment(document: any): Observable<string> {
        let params = new HttpParams();

        const options = {
            params: params,
            reportProgress: true,
        };

        return this._httpClient.post(this.uploadpermitattachmenturl , (document), options).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }


}

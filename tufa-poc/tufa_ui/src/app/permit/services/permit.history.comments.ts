import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of, Subject } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { environment } from "../../../environments/environment";
import { RequestOptions, RequestMethod } from "@angular/http";
import { CommentModel } from "../models/comment/comment.model";
import { PermitPeriod } from "../models/permit-period.model";

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
	providedIn: 'root'
  })
export class PermitHistoryCommentsService{
    private baseurl = environment.apiurl + '/api/v1/permit/';
	constructor(private _httpClient: HttpClient){}
	
	private commentAddedSource = new Subject<CommentModel>();

    public getPermitHistoryComments(permitId): Observable<CommentModel[]>  {
        let url = this.baseurl + 'getallcomments/'+permitId;
        return this._httpClient.get(url, httpOptions).pipe(
			tap(_ => this.log("getPermitHistoryComments")),
			catchError(this.handleError<any>("getPermitHistoryComments"))
		);
	}
	
	public savepermitHistoryComment(comment: CommentModel, addCommnetPermit: number) {
        let url = this.baseurl + 'history/' + addCommnetPermit + '/addupdatecomment';
        return this._httpClient.post(url, comment, httpOptions);
	}
	
	public updatepermitHistoryComment(comment: CommentModel, commentId: number) {
        let url = this.baseurl + 'history/updatecomment/' + commentId;
        return this._httpClient.post(url, comment, httpOptions);
	}
	
	/** triggered by comment popup component when a comment is succesfully added **/
	public triggerCommentAdded(comment:CommentModel){
		this.commentAddedSource.next(comment);
	  }

    	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}


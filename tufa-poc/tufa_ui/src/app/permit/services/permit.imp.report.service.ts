import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { TobaccoType } from '../../permit/models/imptobacco-type.model';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class PermitImpReportService {

    private baseurl = environment.apiurl + '/api/v1/';

    constructor(private _httpClient: HttpClient) { }

    public getPermitImpReportDetails(ttmodel: TobaccoType){
        let url = this.baseurl + 'permit/' + ttmodel.permitId + '/period/' + ttmodel.periodId;
        return this._httpClient.get(url, httpOptions);
    }

    public savePermitImpReportDetails(mfgreportdata){
        let url = this.baseurl + 'permit/' + mfgreportdata.permitId + '/period/' + mfgreportdata.periodId;
        return this._httpClient.put(url, mfgreportdata, httpOptions);
    }
}

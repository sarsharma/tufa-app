import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { environment } from "../../../environments/environment";
import { TobaccoType } from '../../permit/models/tobacco-type.model';
import { PermitPeriod } from "../models/permit-period.model";
import { TTBAdjustments, SaveTTBAdjustments } from "../models/ttb-adjustment.model";

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class PermitMfgReportService {
    private baseurl = environment.apiurl + '/api/v1/';

    constructor(private _httpClient: HttpClient) { }

    public getPermitMfgReportDetails(ttmodel: TobaccoType):Observable<any>{
        let url = this.baseurl + 'permit/' + ttmodel.permitId + '/period/' + ttmodel.periodId;
        return this._httpClient.get(url, httpOptions).pipe(
			tap(_ => this.log("getPermitMfgReportDetails")),
			catchError(this.handleError<any>("getPermitMfgReportDetails"))
		);

    }

    public savePermitMfgReportDetails(mfgreportdata){
        let url = this.baseurl + 'permit/' + mfgreportdata.permitId + '/period/' + mfgreportdata.periodId;
        return this._httpClient.put(url, mfgreportdata, httpOptions);
	}
	
	public saveTTBAdjustments(ttbAdjustmentList: SaveTTBAdjustments){
        let url = this.baseurl + 'permit/' + 'saveTTBAdjustments/' + ttbAdjustmentList.permitId + '/' + ttbAdjustmentList.periodId;
        return this._httpClient.put(url, ttbAdjustmentList.ttbAdjustmentList, httpOptions);
    }
    
    	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}

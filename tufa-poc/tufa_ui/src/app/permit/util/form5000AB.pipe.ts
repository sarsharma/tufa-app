import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { TTBAdjustmentDetailFilterData } from "../models/ttb-adjustment.model";

/**
 * 
 * 
 * @export
 * @class PermitReportsDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "form5000ABPipe"
})
export class Form5000ABPipe implements PipeTransform {

    /**
     * 
     * 
     * @param {any[]} data
     * @param {Form7501filter} filter
     * @returns {*}
     * 
     * @memberOf Form7501DataFilterPipe
     */
    transform(data: any[], filter: TTBAdjustmentDetailFilterData): any {
        let results = data;

        if (data) {
            if (filter) {
                // filter by line Number #
                if (typeof filter.adjLineNumFilter !== undefined && filter.adjLineNumFilter !== undefined && filter.adjLineNumFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1: string = item.adjLineNum.toString();
                        let val2: string = filter.adjLineNumFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                } 
                
                // filter by tax amt
                if (typeof filter.taxAmountFilter !== undefined && filter.taxAmountFilter !== undefined && filter.taxAmountFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1 = Number(Math.round(Number(item.taxAmount + 'e' + 2)) + 'e-' + 2);
                        let val2 = Number(filter.taxAmountFilter.replace(/[^0-9.]/g,''));
                        let operator = filter.taxAmountFilter.replace(/[0-9.]/g,'');
                        switch (operator) {
                        case '>': return val1 > val2;
                        case '<': return val1 < val2;
                        case '>=': return val1 >= val2;
                        case '<=': return val1 <= val2;
                        case '!=':
                        case '<>': return val1 != val2;
                        default: return val1 == val2;
                        }
                    });
                }   
                
                // filter by cigar type #
                if (typeof filter.taxReturnIdFilter !== undefined && filter.taxReturnIdFilter !== undefined && filter.taxReturnIdFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1 = item.taxReturnId;
                        let val2 = filter.taxReturnIdFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) != -1;
                    });
                }                                                            
            }
        }
        return results;
    }
// tslint:disable-next-line:eofline
}
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { Form7501FilterData } from "./../models/form7501filter.model";

/**
 * 
 * 
 * @export
 * @class PermitReportsDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "form7501pipe"
})
export class Form7501DataFilterPipe implements PipeTransform {

    /**
     * 
     * 
     * @param {any[]} data
     * @param {Form7501filter} filter
     * @returns {*}
     * 
     * @memberOf Form7501DataFilterPipe
     */
    transform(data: any[], filter: Form7501FilterData): any {
        let results = data;

        if (data) {
            if (filter) {
                // filter by line Number #
                if (typeof filter.lineNumFilter !== 'undefined' && filter.lineNumFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1 = item.lineNum; 
                        let val2 = Number(filter.lineNumFilter.replace(/[^0-9.]/g,''));
                        let operator = filter.lineNumFilter.replace(/[0-9.]/g,'');
                        switch (operator) {
                        case '>': return val1 > val2;
                        case '<': return val1 < val2;
                        case '>=': return val1 >= val2;
                        case '<=': return val1 <= val2;
                        case '!=':
                        case '<>': return val1 != val2;
                        default: return val1 == val2;
                        }
                    });
                } 
                // filter by qty #
                if (typeof filter.qtyFilter !== 'undefined' && filter.qtyFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1 = Number(Math.round(Number(item.qty + 'e' + 2)) + 'e-' + 2); 
                        let val2 = Number(filter.qtyFilter.replace(/[^0-9.]/g,''));
                        let operator = filter.qtyFilter.replace(/[0-9.]/g,'');
                        switch (operator) {
                        case '>': return val1 > val2;
                        case '<': return val1 < val2;
                        case '>=': return val1 >= val2;
                        case '<=': return val1 <= val2;
                        case '!=':
                        case '<>': return val1 != val2;
                        default: return val1 == val2;
                        }
                    });
                } 
                // filter by tax amt
                if (typeof filter.taxAmtFilter !== 'undefined' && filter.taxAmtFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1 = Number(Math.round(Number(item.taxAmt + 'e' + 2)) + 'e-' + 2);
                        let val2 = Number(filter.taxAmtFilter.replace(/[^0-9.]/g,''));
                        let operator = filter.taxAmtFilter.replace(/[0-9.]/g,'');
                        switch (operator) {
                        case '>': return val1 > val2;
                        case '<': return val1 < val2;
                        case '>=': return val1 >= val2;
                        case '<=': return val1 <= val2;
                        case '!=':
                        case '<>': return val1 != val2;
                        default: return val1 == val2;
                        }
                    });
                }   
                // filter by tax diff #
                if (typeof filter.taxDiffFilter !== 'undefined' && filter.taxDiffFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1 = Number(Math.round(Number(item.taxDiff + 'e' + 2)) + 'e-' + 2); 
                        let val2 = Number(filter.taxDiffFilter.replace(/[^0-9.]/g,''));
                        let operator = filter.taxDiffFilter.replace(/[0-9.]/g,'');
                        switch (operator) {
                        case '>': return val1 > val2;
                        case '<': return val1 < val2;
                        case '>=': return val1 >= val2;
                        case '<=': return val1 <= val2;
                        case '!=':
                        case '<>': return val1 != val2;
                        default: return val1 == val2;
                        }
                    });
                }  
                // filter by cal tax rate #
                if (typeof filter.taxRateFilter !== 'undefined' && filter.taxRateFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1 = 0;
                        if(item.qty && item.taxAmt) {
                            if(item.type != 'Snuff' && item.type != 'Chew' && item.type != 'Pipe' && item.type != 'RollYourOwn'){
                                val1 = Number(Math.round(Number(+item.taxAmt/+item.qty + 'e' + 2)) + 'e-' + 2); 
                            }
                            else val1 = Number(Math.round(Number(+item.taxAmt/+(item.qty * 2.20462262185) + 'e' + 6)) + 'e-' + 6);
                        }
                        let val2 = Number(filter.taxRateFilter.replace(/[^0-9.]/g,''));
                        let operator = filter.taxRateFilter.replace(/[0-9.]/g,'');
                        switch (operator) {
                        case '>': return val1 > val2;
                        case '<': return val1 < val2;
                        case '>=': return val1 >= val2;
                        case '<=': return val1 <= val2;
                        case '!=':
                        case '<>': return val1 != val2;
                        default: return val1 == val2;
                        }
                    });
                }  
                // filter by cigar type #
                if (typeof filter.cigarTypeFilter !== 'undefined' && filter.cigarTypeFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1 = item.type;
                        let val2 = filter.cigarTypeFilter;
                        return String(val1).indexOf(val2) != -1;
                    });
                }                                                            
            }
        }
        return results;
    }
// tslint:disable-next-line:eofline
}
import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AnnualTrueUp } from "./models/at.model";
import { AnnualTrueUpCreationService } from "./services/at-create.service";
import { ShareDataService } from '../../core/sharedata.service';

declare var jQuery: any;
declare var window: any;

@Component({
    selector: '[at-create]',
    templateUrl: './at-create.template.html',
    styleUrls:['at-create.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class AnnualTrueUpCreationPage implements OnInit, OnDestroy {

    fiscalYear: string;
    router: Router;
    trueUp: AnnualTrueUp = new AnnualTrueUp();
    showReportAlerts: boolean;
    busy: Subscription;

    alerts: Array<Object>;

    tabfrom: string;
    shareData: ShareDataService;

    public jQuery: any;

    constructor(private _createTrueUpSvc: AnnualTrueUpCreationService,
            router: Router,
            private _shareData: ShareDataService) {

        this.alerts = [
            {
                type: 'success',
                msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Successfully generated annual true-up.</span>'
            }
        ];

        this.router = router;
        this.showReportAlerts = false;
        this.shareData = _shareData;
    }

    ngOnInit() {
        this.alerts = [];
        this.showReportAlerts = false;
        this.getParamsForReconNavigation();
    }

    ngOnDestroy() {

        if (this.busy) {
            this.busy.unsubscribe();
        }
               
    }

    createTrueUp(x) {
        jQuery('#create-true-up-form').parsley().validate();

        this.showReportAlerts = false;
        this.trueUp.fiscalYear = x;
        if (jQuery('#create-true-up-form').parsley().isValid()) {
           this.busy = this._createTrueUpSvc.createTrueUpAssessment(this.trueUp).subscribe(data => {
                if (data.toString() === '0') {
                    this.alerts = [];
                    this.showReportAlerts = true;
                    // tslint:disable-next-line:max-line-length
                    let msgtoDisplay = '<i class="fa fa-circle text-danger" ></i><span class="alert-text">FY' + x + ' Annual True-Up already exists</span>';
                    this.alerts.push({ type: 'danger', msg: msgtoDisplay });
                } else {
                    this.alerts = [];
                    this.showReportAlerts = true;
                    // tslint:disable-next-line:max-line-length
                    let msgtoDisplay = '<i class="fa fa-circle text-success" ></i><span class="alert-text">Successfully generated the FY' + x + ' Annual True-Up</span>';
                    this.alerts.push({ type: 'success', msg: msgtoDisplay });
                }
            }, err => {
                this.alerts = [];
                this.showReportAlerts = true;
                // tslint:disable-next-line:max-line-length
                let msgtoDisplay = '<i class="fa fa-circle text-danger" ></i><span class="alert-text">Missing criteria: Unable to generate Annual True-Up</span>';
                this.alerts.push({ type: 'danger', msg: msgtoDisplay });
            });
        }
    }

    setboxfocus(elementID: String): void {

        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let searchInput = jQuery(elementID);
        searchInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

    closeAlert(): void {
        // jQuery('.reportAlerts').hide();
        this.showReportAlerts = false;
    }

    gotoAssessments() {
        this.setParamsForReconNavigation();
    }

    setParamsForReconNavigation() {
        this.shareData.reset();
        this.shareData.qatabfrom = 'recon-createat';
    }

    getParamsForReconNavigation() {
        this.tabfrom = this.shareData.qatabfrom;
    }
}

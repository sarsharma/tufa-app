import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, forkJoin } from 'rxjs';
import { ShareDataService } from '../../core/sharedata.service';
import { AnnualTrueUpComparisonService } from './services/at-compare.service';
import { AnnualTrueUpCompareComponent } from "./compare/at-compare.component";
import { AnnualTrueUpEventService } from "./services/at-event.service";
import { TabsetComponent, TabDirective } from '../../../../node_modules/ngx-bootstrap';
import { AnnualTrueUpReconComponent } from "./reconcile/at-recon.component";
import { AtIngestTabPaneComponent } from "./ingest/at-ingest-tab-pane.component";
import { AnnualTrueUpMapTabPane } from "./map/at-map.component";
import { AnnualTrueUpMarketShareComponent } from "./marketshare/at-marketshare.component";
import * as _ from 'lodash';
import { AnnualTrueUpService } from "./../../recon/annual/services/at.service";
import { finalize } from '../../../../node_modules/rxjs/operators';
//recon/annual/services/at.service";

declare var jQuery: any;

@Component({
    selector: '[at-home]',
    templateUrl: './at-home.template.html',
    styleUrls: ['at-home.style.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: []
})

export class AnnualTrueUpHomePage implements OnInit, OnDestroy {
    @ViewChild(AtIngestTabPaneComponent) ingestComponent: AtIngestTabPaneComponent;
    @ViewChild(AnnualTrueUpMapTabPane) mapComponent: AnnualTrueUpMapTabPane;
    @ViewChild(AnnualTrueUpCompareComponent) comparecomponent: AnnualTrueUpCompareComponent;
    @ViewChild(AnnualTrueUpReconComponent) reconComponent: AnnualTrueUpReconComponent;
    @ViewChild(AnnualTrueUpMarketShareComponent) marketShareComponent: AnnualTrueUpMarketShareComponent;

    @ViewChild('atTabs') tabs: TabsetComponent;

    submitted: string;
    context: string;
    submittedDt: string;
    fiscalYear: number;
    createdDt: string;
    qatabfrom: string;
    shareData: ShareDataService;
    doctype: string;
    permitInc: any;
    companyInc: any;
    busy: Subscription;
    subscriptionArray: Subscription[] = [];

    refreshBuckets: any = 0;
    alerts: any = "";
    ingestAlert: string = "";
    in1: string = "";
    in2: string = "";
    in3: string = "";
    in4: string = "";
    cntAllDeltaReview: number = 0;
    cntAcceptApprv: number = 0;

    ingestProcessCompleted: boolean = false;
    compareProcessCompleted: boolean = false;
    reconcileProcessCompleted: boolean = false;
    submitMarketShareCompleted: boolean = false;
    showActionModal: boolean = true;
    unApprovedCount: number;
    dataLoaded: any;
    ingestCountNumber: number;
    isIngestCountRx: boolean = false;
    incomingPage:string ='MarketShare';

    //callForMarketShare: boolean = false;



    constructor(private router: Router,
        private _shareData: ShareDataService,
        private route: ActivatedRoute,
        private annualTrueUpEventService: AnnualTrueUpEventService,
        private atCmpSvc: AnnualTrueUpComparisonService,
        private atTrueUpService: AnnualTrueUpService) {
        this.subscriptionArray.push(this.annualTrueUpEventService.atCreatePermitSource$.subscribe(data => {
            if (data) {
                this.permitInc = data;
            }
        }));

        this.subscriptionArray.push(this.annualTrueUpEventService.atCreateCompanySource$.subscribe(data => {
            if (data) {
                this.companyInc = data;
            }
        }));

        this.shareData = _shareData;
        //  this.callForMarketShare = false;
        //console.log("constructor in home");
    }

    ngOnInit() {
        //console.log("ngOnInit at-home");
        this.fiscalYear = this.shareData.fiscalYear;
        this.createdDt = this.shareData.createdDate;
        this.submittedDt = this.shareData.submittedDt;
        this.getParamsForReconNavigation();
        this.setActiveTab();
        this.annualTrueUpEventService.ingestedLegacySource$.subscribe((ingestedLegacy: boolean) => {
            //console.log("at-home ingestedLegacySource");
            if (ingestedLegacy) {
                //console.log("Map page is ready: " + ingestedLegacy);
                if (this.tabs.tabs.length > 0) {
                    this.tabs.tabs[1].active = true;
                    this.ingestComponent.loadIngestedDocuments();
                    this.scrollTop();
                }
            };
        });
    }

    public setActiveTab() {
        //console.log("setActiveTab :"+this.qatabfrom);
        if (this.qatabfrom === 'at-ingest') {
            this.tabs.tabs[0].active = true;
            this.ingestComponent.loadIngestedDocuments();
        } else if (this.qatabfrom === 'at-map') {
            this.tabs.tabs[1].active = true;
        } else if (this.qatabfrom === 'at-compare') {
            this.tabs.tabs[2].active = true;
        } else if (this.qatabfrom === 'at-reconreport') {
            this.tabs.tabs[3].active = true;
        }
        this.scrollTop();
    }


    scrollTop() {
        jQuery('html, body, .content-wrap').animate({
            scrollTop: 0
        }, 2000);

    }

    ngAfterViewInit() {
        //console.log("ngAfterViewInit");
        setTimeout(() => {

        }, 100);
    }

    ngOnDestroy() {
        //console.log("ngOnDestroy");
        if (this.busy) {
            this.busy.unsubscribe();
        }
        if (this.subscriptionArray) {
            this.subscriptionArray.forEach(subscription => {
                subscription.unsubscribe();
            });
        }
    }
    callIngestRefresh(event: any) {
        //console.log("callIngestRefresh: "+event);
        if (!(event instanceof TabDirective)) return;
        //console.log("callIngestRefresh in home");
        this.ingestComponent.loadIngestedDocuments();
    }
    callMapRefresh(event: any) {
        //console.log("callMapRefresh: "+event);
        if (!(event instanceof TabDirective)) return;
        //console.log("callMapRefresh in home");
        this.mapComponent.loadMapPage();
    }

    callCompareRefresh(event: any) {
        //console.log("callCompareRefresh");
        if (!(event instanceof TabDirective)) return;
        this.comparecomponent.getAnnualComparisonResults(true);
    }
    callReconRefresh(event: any) {
        //console.log("callReconRefresh: "+event);
        if (!(event instanceof TabDirective)) return;
        //console.log("getAnnualReconcilationReport in home");
        this.reconComponent.getAnnualReconcilationReport();
    }
    callMarketShare(event: any) {
        //console.log("callMarketShare: "+event);
        if (!(event instanceof TabDirective)) return;
        //console.log("callMarketShare in home");
        // this.callForMarketShare = true;
        this.alerts = "";
        this.ingestAlert = "";
        this.isIngestCountRx = false;
        // this.getUnapprovedCount();
        this.getActionCounts()
        this.mapComponent.loadMapPage();
        this.marketShareComponent.loadGenerateMarketShare(true);
        // this.reconComponent.getAnnualReconcilationReport(true);
        // this.getCounts();

    }
    gotoAssessments() {
        //console.log("gotoAssessments");
        this.setParamsForReconNavigation();
    }

    onSubmitted(data: any) {
        //console.log("onSubmitted");
        // this.submitted = data ? "Submitted" : this.submitted;
        this.submittedDt = data ? "Submitted" : null;
    }

    // deltaCount($event) {
    //     this.in1 = $event > 0 ? "<li>" + $event + " Delta(s) need to be Accepted or Amended on Compare in order to Submit the Market Share</li>" : "";
    //     this.finalAlert();
    // }





    ingestCount($event) {
        //console.log("ingestCount");
        this.isIngestCountRx = true;
        this.ingestCountNumber = $event


    }

    contextChanged($event){   
      this.context = $event;
    }

    getActionCounts() {
        this.unApprovedCount = 0;
        this.busy = forkJoin([
            this.atTrueUpService.getReconReportDetailsForFiscalYear(this.fiscalYear.toString()),
            this.atCmpSvc.getAllComparisonResultsCounts(this.fiscalYear, this.incomingPage)
        ]).subscribe(
            data => {
                if (data && data[0]) {
                    this.dataLoaded = data[0];
                    this.unApprovedCount = _.size(_.filter(this.dataLoaded, { reconStatus: 'Not Approved' }));
                    //console.log("unapprovedCount"+this.unApprovedCount);
                    if (this.unApprovedCount > 0) {
                        this.in2 = "<li>" + this.unApprovedCount + " Monthly Report(s) need to be approved on Reconciliation in order to Submit the Market Share</li>";
                    }
                }
                if (data && data[1]) {

                    let ttlcount: number =  parseInt(data[1].ttlCntDeltaChng) + parseInt(data[1].ttlCntAccptApprv);
                    this.in1 = data[1].ttlCntAccptApprv || data[1].ttlCntDeltaChng > 0 ? "<li>" + ttlcount +  " Delta(s) need to be Accepted or Amended on Compare in order to Submit the Market Share</li>" : "";
                    this.in3 = data[1].ttlDeltaCntInReview > 0 ? "<li>" + data[1].ttlDeltaCntInReview + " Delta(s) needs to be Reviewed on All Deltas Grid in order to Submit the Market Share</li>" : "";
                    //this.in4 = data[1].ttlCntDeltaChng > 0 ? "<li>" + data[1].ttlCntDeltaChng + " Delta(s) have changed and need to be Accepted or Amended on Compare Matched grid in order to Submit the Market Share</li>" : "";
                    this.alerts = this.in1.concat(this.in2).concat(this.in3).concat(this.in4);
                    if (this.isIngestCountRx) {
                        this.ingestAlert = this.ingestCountNumber > 0 ? " " + this.ingestCountNumber + " Decision(s) have not been made on Ingest & Map" : "";
                        //console.log("ingestAlert :"+ this.ingestAlert);
                    }
                }
            },
            error => {
                this.alerts = [];
                this.alerts.push({ type: 'warning', msg: error });
            }
        );
    }

    setParamsForReconNavigation() {
        //console.log("setParamsForReconNavigation");
        this.shareData.reset();
        // this.shareData.assessmentId = this.assessmentId;
        // this.shareData.months = this.months;
        // this.shareData.quarter = this.quarter;
        this.shareData.fiscalYear = this.fiscalYear;
        this.shareData.submitted = this.submitted;
        this.shareData.createdDate = this.createdDt;
        this.shareData.submittedDt = this.submittedDt;
        this.shareData.qatabfrom = this.qatabfrom;
    }

    getParamsForReconNavigation() {
        //console.log("getParamsForReconNavigation");
        //   this.assessmentId = this.shareData.assessmentId;
        //   this.months = this.shareData.months;
        //   this.quarter = this.shareData.quarter;
        this.fiscalYear = this.shareData.fiscalYear;
        this.submitted = this.shareData.submitted;
        this.submittedDt = this.shareData.submittedDt;
        this.createdDt = this.shareData.createdDate;
        this.qatabfrom = this.shareData.qatabfrom;
    }


    getDocType($event) {
        //console.log("getDocType");
        this.doctype = $event;
    }

    updateBuckets(event) {
        //console.log("updateBuckets");
        if (event) {
            this.refreshBuckets++;
        }
    }

    flagIngestProcessCompleted(event) {
        //console.log("flagIngestProcessCompleted");
        this.ingestProcessCompleted = event;
    }

    flagCompareProcessCompleted(event) {
        //console.log("flagCompareProcessCompleted");
        this.compareProcessCompleted = event;
    }

    flagReconProcessCompleted(event) {
        //console.log("flagReconProcessCompleted");

    }

    flagMarketShareProcessCompleted(event) {
        //console.log("flagMarketShareProcessCompleted");
        this.submitMarketShareCompleted = event;
    }
}

import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { ShareDataService } from '../../core/sharedata.service';

declare var jQuery: any;

@Component({
    selector: '[at-results]',
    templateUrl: './at-results.template.html',
    providers: []
})

export class AnnualTrueUpResultsComponent implements OnInit {

    public router: Router;
    data: any[];
    shareData: ShareDataService;

    @Input() annualData: any[];
    @Input() filterOptions: any;
    @Input() annualActions: number[];
    @Input() annualActionBusy: boolean;
    filterData: any;
    sortBy: string = "fiscalYear";
    sortOrder: string[] = ["desc"];

    showErrorFlag: boolean;
    alerts: Array<Object>;

    constructor( router: Router,
        private _shareData: ShareDataService
    ) {
        this.router = router;
        this.shareData = _shareData;
        this.showErrorFlag = false;
    }

    ngOnInit(): void {
        this.data = this.annualData;
        
    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "annualData") {
                this.data = changes[propName].currentValue;
            } else if (propName === "filterOptions") {
                this.filterData = changes[propName].currentValue;
            } else if (propName === "annualActions") {
                this.annualActions = changes[propName].currentValue;
            } else if (propName === "annualActionBusy") {
                this.annualActionBusy = changes[propName].currentValue;
            }
        }
    }

    toAnnualTrueUp(trueup) {
        
        this.shareData.reset();
        this.shareData.submittedDt = trueup.submittedDt;
        this.shareData.fiscalYear = trueup.fiscalYear;
        this.shareData.createdDate = trueup.createdDt;
        this.shareData.submitted = trueup.submittedInd;
        this.shareData.qatabfrom = 'at-ingest';
        // this.router.navigate(["/app/annual"]);
    }
}
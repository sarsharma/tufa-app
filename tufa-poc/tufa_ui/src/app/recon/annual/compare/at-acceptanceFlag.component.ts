import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import { Subscription } from 'rxjs';
import { ShareDataService } from '../../../core/sharedata.service';
import { AcceptanceFlagState } from '../models/at-acceptance-flag-state.model';
import { MixedAction, MixedActionManu } from '../models/at-mixedaction.model';
import { MixedActionQuarterDetails, MixedActionQuarterDetailsManufacturer } from '../models/at-mixedactiondetailsquarter.model';
import { taxes } from '../models/at-taxes.model';
import { TTBTaxesVol } from '../models/at-ttbtaxesvol.model';
import { AnnualTrueUpComparisonEventService } from '../services/at-compare.event.service';
import { AnnualTrueUpComparisonService } from '../services/at-compare.service';

declare var jQuery: any;

@Component({
    selector: '[fda3852flag-panel]',
    templateUrl: './at-acceptanceFlag.template.html',
    styleUrls: ['./at-acceptanceFlag.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FDA3852FlagPanel implements OnInit, OnDestroy {


    @Input() fiscalYear: number;
    @Input() companyId: number;
    @Input() companyName: string;
    @Input() quarter: number;
    @Input() tobaccoClass: string;
    @Input() permitType: string;
    @Input() isValid: boolean;
    @Input() inProgressDisabled: boolean;
    @Input() acceptanceFlag: string;
    @Input() inprogressflag: string;
    @Input() taxes: taxes;

    @Input() deltaExciseTaxFlag: any;

    @Input() delta1: any;
    @Input() delta2: any;
    @Input() delta3: any;
    @Input() delta4: any;
    @Input() mixedActionQuarterMap: MixedActionQuarterDetails[];
    @Input() mixedActionTax: string;
    @Input() mixedActionQuantityRemoved: string;
    @Input() mixedActionQuarterManuMap: MixedActionQuarterDetailsManufacturer[];
    @Input() mixedActionTaxManu: number;
    @Input() mixedActionQuantityRemovedManu: number;


    compareData = [];
    @Output() accept3852flag = new EventEmitter<any>();
    @Output() acceptedFlag = new EventEmitter<boolean>();
    @Output() progressedflag = new EventEmitter<boolean>();
    @Output() mixedActionFlag = new EventEmitter<any>();
    @Output() mixedActionFlagManu = new EventEmitter<any>();

    flag: string = '';

    fileTypeSelected: string = '';

    disableAmendApprove: boolean = false;
    showingestedtaxpopup: boolean = false;

    hasLoaded: boolean = false;
    show: boolean = false;
    hasVolumeData: boolean = false;

    activeqtr;
    router: Router;
    shareData: ShareDataService;
    atcomparisonservice: AnnualTrueUpComparisonService;
    cbpexportdata: any;
    showCommentsErrorFlag: boolean;
    alertsComments: Array<Object>;
    busy: Subscription;
    fileType: string;

    reviewdata: any;
    context: any;
    cbpAmmendedtotaltax: any;
    cbpAmmendedtotalVol: any;
    cbpSummTaxAmount: number;
    cbpDetailTaxAmount: number;
    cbpDetailVolume: number;
    taxRateSummary: number;
    tufaTaxRate: number;
    continueClicked: boolean = false;
    CIGAR_QUARTER = 5;
    unitMeasurement: any;

    showMixedActionPopUp: boolean = false;
    mixedQuarterSelectedMap: MixedActionQuarterDetails[] = [];
    volumeRemovedDisplay: number;
    volumeRemoved: number;
    taxAmount: number;
    volumeRemovedPanelDisplay: number;
    taxAmountPanelDisplay: number;

    showMixedActionManuPopUp: boolean = false;
    mixedQuarterSelectedMapManu: MixedActionQuarterDetailsManufacturer[] = [];
    volumeRemovedDisplayManu: number;
    volumeRemovedManu: number;
    taxAmountManu: number;
    volumeRemovedDisplay1: number;
    volumeRemoved1: number;
    taxAmount1: number;
    volumeRemovedDisplay2: number;
    volumeRemoved2: number;
    taxAmount2: number;
    taxAmountPanelDisplayManu: number;    
    volumeRemovedPanelDisplayManu: number;
    taxAmountPanelDisplayManu1: number;    
    volumeRemovedPanelDisplayManu1: number;
    taxAmountPanelDisplayManu2: number;    
    volumeRemovedPanelDisplayManu2: number;
    tobaccoClass1: string;
    tobaccoClass2: string;
    mixedClass: boolean;

    ttIdxMp = { "Cigars": 0, "Cigarettes": 0, "Chew-Snuff": 0, "Chew": 1, "Snuff": 2, "Pipe-Roll Your Own": 0, "Pipe": 1, "Roll-Your-Own": 2 }
    months = { "Jan" : "January", "Feb" : "February", "Mar" : "March", "Apr" : "April", "May" : "May", "Jun" : "June", "Jul" : "July", "Aug" : "August", "Sep" : "September", "Oct" : "October", "Nov" : "November", "Dec" : "December" }
    compoundTTTaxesVolume: TTBTaxesVol[] = [];

    @ViewChild('closeAcceptModal') closeAcceptModal: ElementRef;

    @ViewChild('closeMixedModal') closeMixedModal: ElementRef;

    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }
    }


    constructor(
        private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
        public _shareData: ShareDataService, atcomparisonservice: AnnualTrueUpComparisonService, router: Router) {

        this.alertsComments = [
            {
                type: 'warning',
                msg: '<span class="fw-semi-bold">Warning:</span> Error: Saving Comments'
            }
        ];

        this.showCommentsErrorFlag = false;
        this.shareData = _shareData;
        this.atcomparisonservice = atcomparisonservice;
        this.router = router;

        this.annualTrueUpComparisonEventService.atCompareAmendmentsLoaded$.subscribe(
            data => {
                this.hasLoaded = data;
            }
        );
    }

    ngOnChanges(changes: SimpleChanges) {

        for (let propName in changes) {
            let qtr = this.quarter;
            if (propName === "acceptanceFlag") {
                this.acceptanceFlag = changes[propName].currentValue;
                this.disableAmendApprove = this.acceptanceFlag === AcceptanceFlagState.DELTA_CHANGE_TRUE_ZERO;
                console.debug(" this.quarter "+ this.quarter + " this.acceptanceFlag " + this.acceptanceFlag);

            }

            if (propName === "permitType") {
                this.permitType = "Importer";
            }

            if (propName === "inProgressDisabled") {
                this.inProgressDisabled = changes[propName].currentValue;

            } if (propName === "quarter") {
                if (this.shareData.permitType === "Manufacturer") {
                    this.getNonCigarManuDetails();
                }
            }
            if (propName === "taxes") {
                this.getNonCigarManuDetails();
            }
            if (propName === "mixedActionQuarterMap") {
                this.mixedQuarterSelectedMap = [];
                this.mixedQuarterSelectedMap = JSON.parse(JSON.stringify(this.mixedActionQuarterMap));
                this.setTaxVolumeMixedAction(true);
            }
            if(propName === "mixedActionQuantityRemoved"){
                this.taxAmount = Number(this.mixedActionTax);
                this.volumeRemoved = Number(this.mixedActionQuantityRemoved);
                this.setTaxVolumeMixedAction(true);
            }
            if (propName === "mixedActionQuarterManuMap") {
                this.mixedQuarterSelectedMapManu = [];
                this.mixedQuarterSelectedMapManu = JSON.parse(JSON.stringify(this.mixedActionQuarterManuMap));
                this.setTaxVolumeMixedActionManu(true);
                this.checkMixedClass(); 
            }
            if(propName === "mixedActionQuantityRemovedManu"){
                this.taxAmount = Number(this.mixedActionTaxManu);
                this.volumeRemoved = Number(this.mixedActionQuantityRemovedManu);
                //this.setTaxVolumeMixedActionManu(true);
                this.checkMixedClass();
            }
        }
    }

    ngOnInit() { 
        this.checkMixedClass();        
        jQuery(document).ready(function () {
                    let modalContent: any = jQuery('.modal-content');
                    modalContent.draggable({
                      handle: '.modal-header,.modal-body'
                    });
                  }); 
     }

    checkMixedClass(){
        if(this.shareData.tobaccoClass === "Chew/Snuff"){
            this.tobaccoClass1 = "Chew";
            this.tobaccoClass2 = "Snuff";
            this.mixedClass = true;
        }else if(this.shareData.tobaccoClass === "Pipe/Roll Your Own"){
            this.tobaccoClass1 = "Pipe";
            this.tobaccoClass2 = "Roll Your Own";
            this.mixedClass = true;
        }else{
            this.tobaccoClass1 = this.shareData.tobaccoClass;
            this.tobaccoClass2 = null;
            this.mixedClass = false;
        }

    }
    acceptFDA3852() {
        // this.acceptanceFlag = AcceptanceFlagState.ACCEPT_FDA;
        // this.isValid = false;
        this.accept3852flag.emit({ flag: AcceptanceFlagState.ACCEPT_FDA, ttbVolTaxes: null });
        this.inProgressDisabled = false;
    }


    private getSummDetailData() {

        if (this.tobaccoClass == 'Cigarettes' || this.tobaccoClass == 'Cigars')
            this.unitMeasurement = 'K';
        else
            this.unitMeasurement = 'Kgs'


        this.busy = this.atcomparisonservice.getImporterSummDetailsData("ein", this.shareData.ein,
            this.fiscalYear, this.quarter.toString(), this.tobaccoClass).subscribe(
                data => {
                    if (data) {
                        this.reviewdata = data;
                        this.cbpSummTaxAmount = this.reviewdata.cbpSummaryTaxAmount;
                        this.cbpDetailTaxAmount = this.reviewdata.cbpDetailTaxAmount;
                        this.cbpDetailVolume = this.reviewdata.cbpDetailVolume;
                        this.tufaTaxRate = this.reviewdata.tufaTaxRate;

                    }
                }
            );

    }

    onFileTypeSelChanged(event: any) {

        if (event === 'Summary') {

            if (this.cbpSummTaxAmount !== undefined && this.cbpDetailVolume !== null) {
                this.cbpAmmendedtotaltax = this.cbpSummTaxAmount;
                this.cbpAmmendedtotalVol = this.cbpDetailVolume;
                this.fileTypeSelected = 'Summary';
            }
        }
        if (event === 'Details') {

            if (this.cbpDetailTaxAmount !== undefined && this.cbpDetailVolume !== null) {
                this.cbpAmmendedtotaltax = this.cbpDetailTaxAmount;
                this.cbpAmmendedtotalVol = this.cbpDetailVolume;
                this.fileTypeSelected = 'Details';
            }

        }


    }


    acceptReviewIngestedContinue() {
        this.continueClicked = true;
        this.acceptReviewIngested();
    }

    acceptReviewIngested() {

        if (this.quarter == 1)
            this.deltaExciseTaxFlag = this.delta1;

        if (this.quarter == 2)
            this.deltaExciseTaxFlag = this.delta2;

        if (this.quarter == 3)
            this.deltaExciseTaxFlag = this.delta3;

        if (this.quarter == 4)
            this.deltaExciseTaxFlag = this.delta4;

        if (this.deltaExciseTaxFlag.includes("Review")) {

            this.showingestedtaxpopup = true;
            this.getSummDetailData();
            this.cbpAmmendedtotaltax = this.cbpSummTaxAmount;
            this.cbpAmmendedtotalVol = this.cbpDetailVolume;
            this.inProgressDisabled = false;


        }
        else {
            this.showingestedtaxpopup = false;
            this.acceptIngested();

        }

    }

    acceptIngested() {

        if (this.continueClicked)
            this.flag = AcceptanceFlagState.ACCEPT_INGESTED_CONTINUE;
        else
            this.flag = AcceptanceFlagState.ACCEPT_INGESTED;

        if (!this.showEsimateTaxPopup()) {
            this.accept3852flag.emit({ flag: this.flag, ttbVolTaxes: null });
            this.inProgressDisabled = false;
        }

    }

    // when accept is clicked on pop up 
    confirmReviewAcceptIngested() {

        if (this.continueClicked)
            this.flag = AcceptanceFlagState.ACCEPT_INGESTED_CONTINUE;
        else
            this.flag = AcceptanceFlagState.ACCEPT_INGESTED;

        this.accept3852flag.emit({
            flag: this.flag,
            ttbVolTaxes: null,
            cbpAmmendedtotaltax: this.cbpAmmendedtotaltax, cbpAmmendedtotalVol: this.cbpAmmendedtotalVol
        });

        this.inProgressDisabled = false;
        this.progressedflag.emit(false);
        this.fileTypeSelected = '';
        this.closeAcceptModal.nativeElement.click();
    }

    InProgressclicked() {
        this.progressedflag.emit(true);
        this.inProgressDisabled = true;
    }

    acceptFDA3852Continue() {
        this.accept3852flag.emit({ flag: AcceptanceFlagState.ACCEPT_FDA_CONTINUE, ttbVolTaxes: null });
        this.progressedflag.emit(false);
    }

    acceptIngestedContinue() {
        this.flag = AcceptanceFlagState.ACCEPT_INGESTED_CONTINUE;
        if (this.compoundTTTaxesVolume.length > 0) {
            this.showEsimateTaxPopup();
        } else {
            this.accept3852flag.emit({ flag: this.flag, ttbVolTaxes: null });
            this.progressedflag.emit(false);
        }
    }

    confirmAcceptIngested() {
        this.accept3852flag.emit({ flag: this.flag, ttbVolTaxes: this.compoundTTTaxesVolume });
        if (AcceptanceFlagState.ACCEPT_INGESTED_CONTINUE == this.flag) {
            this.progressedflag.emit(false);
        }
        this.closeAcceptModal.nativeElement.click();
    }

    closePopUpforReview() {
        this.fileTypeSelected = '';
    }


    showEsimateTaxPopup() {
        return this.show = this.compoundTTTaxesVolume.length > 0;
    }

    toAnnualTrueUp() {

        this.router.navigate(["/app/assessments/annual/" + this.shareData.fiscalYear]);
    }

    isNonCigar(tobaccoClass: any) {
        if (tobaccoClass.toUpperCase() === "CIGARS") {
            return false;
        }
        return true;
    }

    //Get Volume Data and Tax Rates for Compound Tobacco Types

    private getNonCigarManuDetails() {

        let tobaccoClassName = this.shareData.tobaccoClass.replace(/ /g, "-").replace(/\//g, " ");
        this.busy = this.atcomparisonservice.getTTBVolTaxesPerTobaccoType(this.shareData.ein, this.shareData.fiscalYear, this.quarter, tobaccoClassName).subscribe(

            data => {
                this.compoundTTTaxesVolume = [];
                let ttbVolTaxes: TTBTaxesVol[] = data;
                for (let i in ttbVolTaxes) {
                    let ttbVolTx = new TTBTaxesVol();
                    jQuery.extend(true, ttbVolTx, ttbVolTaxes[i]);
                    this.compoundTTTaxesVolume.push(ttbVolTx);
                    //if (ttbVolTaxes[i].amendedTax == null) {
                        if (ttbVolTaxes.length > 1 && ttbVolTaxes[i].pounds !== null)
                            ttbVolTx.amendedTax = Math.abs((ttbVolTaxes[i].estimatedTax / (ttbVolTaxes[0].estimatedTax + ttbVolTaxes[1].estimatedTax)) * Number(this.taxes.totalIngestedTax));
                        else if (ttbVolTaxes.length > 1 && ttbVolTaxes[i].pounds == null)
                            ttbVolTx.amendedTax = null;
                        else
                            ttbVolTx.amendedTax = this.roundDecimal(Math.abs(Number(this.taxes.totalIngestedTax)));
                    //}
                }
                if (this.compoundTTTaxesVolume.length > 1 && ttbVolTaxes[0].pounds == null && ttbVolTaxes[1].pounds == null)
                    this.hasVolumeData = false;
                else if (this.compoundTTTaxesVolume.length == 1 && ttbVolTaxes[0].pounds == null)
                    this.hasVolumeData = false;
                else
                    this.hasVolumeData = this.compoundTTTaxesVolume.length > 0;
            }
        )
    }

    public roundDecimal(value, fractionSize = 2): number {
        let val = (value) ? value : 0;
        return Number(Math.round(Number(val + 'e' + fractionSize)) + 'e-' + fractionSize);
    }
    isCompundTobaccoType(): boolean {
        return this.shareData.tobaccoClass === "Pipe/Roll Your Own" || this.shareData.tobaccoClass === "Chew/Snuff";
    }
    private getNoIngestedVolumeToolTip() {
        return '<br><p class="no-margin" ><strong>No Ingested Volume Exists</strong></p>';
    }

    mixedActionClicked(){
        this.mixedQuarterSelectedMap = [];
        this.mixedQuarterSelectedMap = JSON.parse(JSON.stringify(this.mixedActionQuarterMap));
        this.setTaxVolumeMixedAction(false);
        //this.showMixedActionPopUp = true;
        jQuery("#mixed_action_tax_popup-" + this.quarter).modal('show')
    }

    mixedActionManuClicked(){
        this.mixedQuarterSelectedMapManu = [];
        this.mixedQuarterSelectedMapManu = JSON.parse(JSON.stringify(this.mixedActionQuarterManuMap));
        this.setTaxVolumeMixedActionManu(false);
        //this.showMixedActionPopUp = true;
        jQuery("#mixed_action_tax_popup-manu-" + this.quarter).modal('show')
    }

    mixedFlagSelectedMonth(event: any, flagSelected: string, month: string){
        this.mixedQuarterSelectedMap.forEach((element, index) => {
            if(this.mixedQuarterSelectedMap[index].month === month){
                if(flagSelected==='F' && event){
                    this.mixedQuarterSelectedMap[index].monthlyStatusFlag = 'F';
                    this.mixedQuarterSelectedMap[index].acceptFDAFlag = true;
                    this.mixedQuarterSelectedMap[index].acceptIngestedFlag = false;
                    this.setTaxVolumeMixedAction(false);
                }else if(flagSelected==='F' && !event){
                    this.mixedQuarterSelectedMap[index].monthlyStatusFlag = null;
                    this.mixedQuarterSelectedMap[index].acceptFDAFlag = false;
                    this.mixedQuarterSelectedMap[index].acceptIngestedFlag = false;
                    this.setTaxVolumeMixedAction(false);
                }else if(flagSelected==='I' && event){
                    this.mixedQuarterSelectedMap[index].monthlyStatusFlag = 'I';
                    this.mixedQuarterSelectedMap[index].acceptFDAFlag = false;
                    this.mixedQuarterSelectedMap[index].acceptIngestedFlag = true;
                    this.setTaxVolumeMixedAction(false);
                }else if(flagSelected==='I' && !event){
                    this.mixedQuarterSelectedMap[index].monthlyStatusFlag = null;
                    this.mixedQuarterSelectedMap[index].acceptFDAFlag = false;
                    this.mixedQuarterSelectedMap[index].acceptIngestedFlag = false;
                    this.setTaxVolumeMixedAction(false);
                }
            }
        });
    }

    popUpClose() {
        jQuery('#mixed_action_tax_popup-' + this.quarter).modal('hide');
        this.mixedQuarterSelectedMap = JSON.parse(JSON.stringify(this.mixedActionQuarterMap));
        this.setTaxVolumeMixedAction(true);
        this.resetPopUpState();
    }

    mixedFlagSelectedMonthManu(event: any, flagSelected: string, month: string){
        this.mixedQuarterSelectedMapManu.forEach((element, index) => {
            if(this.mixedQuarterSelectedMapManu[index].month === month){
                if(flagSelected==='F' && event){
                    this.mixedQuarterSelectedMapManu[index].monthlyStatusFlag = 'F';
                    this.mixedQuarterSelectedMapManu[index].acceptFDAFlag = true;
                    this.mixedQuarterSelectedMapManu[index].acceptIngestedFlag = false;
                    this.setTaxVolumeMixedActionManu(false);
                }else if(flagSelected==='F' && !event){
                    this.mixedQuarterSelectedMapManu[index].monthlyStatusFlag = null;
                    this.mixedQuarterSelectedMapManu[index].acceptFDAFlag = false;
                    this.mixedQuarterSelectedMapManu[index].acceptIngestedFlag = false;
                    this.setTaxVolumeMixedActionManu(false);
                }else if(flagSelected==='I' && event){
                    this.mixedQuarterSelectedMapManu[index].monthlyStatusFlag = 'I';
                    this.mixedQuarterSelectedMapManu[index].acceptFDAFlag = false;
                    this.mixedQuarterSelectedMapManu[index].acceptIngestedFlag = true;
                    this.setTaxVolumeMixedActionManu(false);
                }else if(flagSelected==='I' && !event){
                    this.mixedQuarterSelectedMapManu[index].monthlyStatusFlag = null;
                    this.mixedQuarterSelectedMapManu[index].acceptFDAFlag = false;
                    this.mixedQuarterSelectedMapManu[index].acceptIngestedFlag = false;
                    this.setTaxVolumeMixedActionManu(false);
                }
            }
        });
    }


    popUpCloseManu() {
        jQuery('#mixed_action_tax_popup-manu-' + this.quarter).modal('hide');
        this.mixedQuarterSelectedMapManu = JSON.parse(JSON.stringify(this.mixedActionQuarterManuMap));
        this.setTaxVolumeMixedActionManu(true);
        this.resetPopUpState();
    }
    
    private resetPopUpState() {
        this.mixedQuarterSelectedMap = [];
        this.mixedQuarterSelectedMapManu = [];
    }

    setTaxVolumeMixedAction(changePanelDisplay: boolean){
        this.taxAmount = 0;
        this.volumeRemoved = 0;
        this.volumeRemovedDisplay = 0;
        if(this.shareData.tobaccoClass.toUpperCase() === 'CIGARS' || this.shareData.tobaccoClass.toUpperCase() === 'CIGARETTES')
            this.unitMeasurement = 'Sticks';
        else
            this.unitMeasurement = 'lbs';
        this.mixedQuarterSelectedMap.forEach((element, index) => {
            if(element.acceptFDAFlag && Number(element.tufaTax)>=0 && Number(element.tufaVolume)>=0){
                if(this.shareData.tobaccoClass.toUpperCase() === 'CIGARS' || this.shareData.tobaccoClass.toUpperCase() === 'CIGARETTES')           
                    this.volumeRemoved += Number(element.tufaVolume) / 1000;
                else
                    this.volumeRemoved +=  Number(element.tufaVolume) / 2.204;
                this.taxAmount += Number(element.tufaTax);
                this.volumeRemovedDisplay += Number(element.tufaVolume);
            }
            else if(element.acceptIngestedFlag && Number(element.ingestedTax)>=0 && Number(element.ingestedVolume)>=0){
                if(this.shareData.tobaccoClass.toUpperCase() === 'CIGARS' || this.shareData.tobaccoClass.toUpperCase() === 'CIGARETTES')           
                    this.volumeRemovedDisplay += Number(element.ingestedVolume) * 1000;
                else
                    this.volumeRemovedDisplay +=  Number(element.ingestedVolume) * 2.204;
                this.taxAmount += Number(element.ingestedTax);
                this.volumeRemoved += Number(element.ingestedVolume);
            }
        });
        if(changePanelDisplay){
            this.taxAmountPanelDisplay = this.taxAmount;
            this.volumeRemovedPanelDisplay = this.volumeRemovedDisplay;
        }
    }

    setTaxVolumeMixedActionManu(changePanelDisplay: boolean){
        this.taxAmountManu = 0;
        this.volumeRemovedManu = 0;
        this.volumeRemovedDisplayManu = 0;
        this.taxAmount1 = 0;
        this.volumeRemoved1 = 0;
        this.volumeRemovedDisplay1 = 0;
        this.taxAmount2 = 0;
        this.volumeRemoved2 = 0;
        this.volumeRemovedDisplay2 = 0;
        if(this.shareData.tobaccoClass.toUpperCase() === 'CIGARS' || this.shareData.tobaccoClass.toUpperCase() === 'CIGARETTES')
            this.unitMeasurement = 'Sticks';
        else
            this.unitMeasurement = 'lbs';
        this.mixedQuarterSelectedMapManu.forEach((element, index) => {
            if(element.acceptFDAFlag && Number(element.tufaTotalTax)>=0 && Number(element.tufaTotalVolume)>=0){
                if(this.shareData.tobaccoClass.toUpperCase() === 'CIGARS' || this.shareData.tobaccoClass.toUpperCase() === 'CIGARETTES'){          
                    this.volumeRemovedManu += Number(element.tufaTotalVolume);
                    this.volumeRemoved1 += Number(element.tufaVolume1);
                }
                else{
                    this.volumeRemovedManu +=  Number(element.tufaTotalVolume);
                    this.volumeRemoved1 += Number(element.tufaVolume1);
                    this.volumeRemoved2 += Number(element.tufaVolume2);
                }
                    
                this.taxAmountManu += Number(element.tufaTotalTax);
                this.volumeRemovedDisplayManu += Number(element.tufaTotalVolume);
                this.taxAmount1 += Number(element.tufaTax1);
                this.volumeRemovedDisplay1 += Number(element.tufaVolume1);
                this.taxAmount2 += Number(element.tufaTax2);
                this.volumeRemovedDisplay2 += Number(element.tufaVolume2);
            }
            else if(element.acceptIngestedFlag && Number(element.ingestedTotalTax)>=0 && Number(element.ingestedTotalVolume)>=0){
                if(this.shareData.tobaccoClass.toUpperCase() === 'CIGARS' || this.shareData.tobaccoClass.toUpperCase() === 'CIGARETTES'){           
                    this.volumeRemovedDisplayManu += Number(element.ingestedTotalVolume);
                    this.volumeRemovedDisplay1 += Number(element.ingestedVolume1);
                }
                else{
                    this.volumeRemovedDisplayManu +=  Number(element.ingestedTotalVolume);
                    this.volumeRemovedDisplay1 +=  Number(element.ingestedVolume1);
                    this.volumeRemovedDisplay2 +=  Number(element.ingestedVolume2);

                }
                this.taxAmountManu += Number(element.ingestedTotalTax);
                this.volumeRemovedManu += Number(element.ingestedTotalVolume);
                this.taxAmount1 += Number(element.ingestedTax1);
                this.volumeRemoved1 += Number(element.ingestedVolume1);
                this.taxAmount2 += Number(element.ingestedTax2);
                this.volumeRemoved2 += Number(element.ingestedVolume2);
            }
        });
        if(changePanelDisplay){
            this.taxAmountPanelDisplayManu = this.taxAmountManu;
            this.volumeRemovedPanelDisplayManu = this.volumeRemovedDisplayManu;
            this.taxAmountPanelDisplayManu1 = this.taxAmount1;
            this.volumeRemovedPanelDisplayManu1 = this.volumeRemovedDisplay1;
            this.taxAmountPanelDisplayManu2 = this.taxAmount2;
            this.volumeRemovedPanelDisplayManu2 = this.volumeRemovedDisplay2;     
        }
    }

    saveMixedAction(){
        this.flag = AcceptanceFlagState.MIXEDACTION;
        let mixedAction: MixedAction = new MixedAction();
        this.mixedQuarterSelectedMap.forEach((element, index) => {
           mixedAction.mixedActionDetailsMap.set(element.month.toUpperCase(), element.monthlyStatusFlag);
        });

        mixedAction.amendedTotalVolume = this.volumeRemoved;
        mixedAction.amendedTotalTax = this.taxAmount;
        mixedAction.qtr = this.quarter;
        this.mixedActionFlag.emit({flag: this.flag, mixedAction: mixedAction}); 
        this.popUpClose();
        }

    saveMixedActionManu(){
        this.flag = AcceptanceFlagState.MIXEDACTION;
        let mixedActionManu: MixedActionManu = new MixedActionManu();
        this.mixedQuarterSelectedMapManu.forEach((element, index) => {
            mixedActionManu.mixedActionDetailsMap.set(element.month.toUpperCase(), element.monthlyStatusFlag);
        });
        if(this.shareData.tobaccoClass.toUpperCase() === 'CIGARS' || this.shareData.tobaccoClass.toUpperCase() === 'CIGARETTES')
            mixedActionManu.amendedTotalVolume1 = this.volumeRemoved1/1000;
        else
            mixedActionManu.amendedTotalVolume1 = this.volumeRemoved1;
            
        mixedActionManu.amendedTotalTax1 = this.taxAmount1;
        if(this.shareData.tobaccoClass.toUpperCase() !== 'CIGARS' || this.shareData.tobaccoClass.toUpperCase() !== 'CIGARETTES'){  
            mixedActionManu.amendedTotalVolume2 = this.volumeRemoved2;
            mixedActionManu.amendedTotalTax2 = this.taxAmount2;
        }
        mixedActionManu.qtr = this.quarter;
        this.mixedActionFlagManu.emit({flag: this.flag, mixedActionManu: mixedActionManu}); 
        this.popUpCloseManu();
    }
    
        enableMixActionSave(){
            if(this.mixedQuarterSelectedMap.length>0){
                if(this.quarter<5){
                    if(this.mixedQuarterSelectedMap[0].monthlyStatusFlag!==null && this.mixedQuarterSelectedMap[1].monthlyStatusFlag!==null && this.mixedQuarterSelectedMap[2].monthlyStatusFlag!==null)
                        return false;
                }
                else if(this.quarter==5){
                    let count = 0
                    this.mixedQuarterSelectedMap.forEach(element=>{ 
                        if(element.monthlyStatusFlag!==null) 
                            count++; });
                    if(count===12) return false;
                    else return true
                }
            }
            return true;
        }

        enableMixActionSaveManu(){
            if(this.shareData.permitType === 'Importer'){
            if(this.mixedQuarterSelectedMapManu.length>0){
                if(this.quarter<5){
                    if(this.mixedQuarterSelectedMapManu[0].monthlyStatusFlag!==null && this.mixedQuarterSelectedMapManu[1].monthlyStatusFlag!==null && this.mixedQuarterSelectedMapManu[2].monthlyStatusFlag!==null)
                        return false;
                }
                else if(this.quarter==5){
                    let count = 0
                    this.mixedQuarterSelectedMapManu.forEach(element=>{ 
                        if(element.monthlyStatusFlag!==null) 
                            count++; });
                    if(count===12) return false;
                    else return true
                }
            }
            return true;
        }
        else{
            let mapsize = this.mixedQuarterSelectedMapManu.length;

            let count = 0
            this.mixedQuarterSelectedMapManu.forEach(element=>{ 
                if(element.monthlyStatusFlag!==null) 
                    count++; });
            if(count===mapsize) return false;
            else return true;
        }
        }

        continueclicked(){
            this.router.navigate(["./app/assessments/annual/" + this._shareData.fiscalYear]);
        }
        acceptIngestDisable(){
            
            return !this.hasLoaded || this.isValid||this.disableAmendApprove||
                this.acceptanceFlag==='DeltaChngZ' || this.acceptanceFlag==='ExcludeChange' || this.acceptanceFlag==='DeltaChangeTrueZero' ||
                    (this.isCompundTobaccoType() && !this.hasVolumeData);
                    
                   
        }
}
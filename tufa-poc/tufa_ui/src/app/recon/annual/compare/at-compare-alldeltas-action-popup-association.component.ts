import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { Subscription } from "rxjs";
import { CompanyService } from "../../../company/services/company.service";
import { CBPAssociationModel } from "../models/at-cbpassociation.model";
import { TrueUpComparisonResult } from "../models/at-comparisonresult.model";
import { AnnualTrueUpService } from "../services/at.service";

declare var jQuery: any;

@Component({
    selector: '[at-compare-alldeltas-action-popup-association]',
    templateUrl: './at-compare-alldeltas-action-popup-association.template.html',
    providers: [AnnualTrueUpService, CompanyService]
})

export class AnnualTrueUpAllDeltasPopUpAssociationComponent implements OnInit {

    associateEIN: string = '';
    checked: boolean = false;
    busy: Subscription;
    thinking: boolean = false;
    hasError: boolean = false;
    association: CBPAssociationModel = new CBPAssociationModel();
    alerts: { type: string; msg: string; }[] = [];
    public einmask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
   // deltaComment: string = "";

    @Input() popupRptData: TrueUpComparisonResult;

    @Output() onClose = new EventEmitter<string>();
    @Output() onAssociate = new EventEmitter<string>();

    constructor(private service: AnnualTrueUpService, private companyService: CompanyService) {
        
    }

    ngOnInit(): void {
        this.setAssociation();
       // this.deltaComment = this.popupRptData.deltaComment;
        if(this.isValidString(this.popupRptData.originalEIN)){
            this.associateEIN = this.popupRptData.ein;
            this.checkEIN();
        }
        // this.getCurrentAssociation()
        jQuery('#associatecompany-form').parsley({
            successClass: 'has-success',
            errorClass: 'has-error',
            classHandler: function(el) {
              return el.$element.closest(".form-group");
            },
            errorsContainer: function(el) {
                return el.$element.closest('.form-group');
            }
            ,errorsWrapper: '<span class="form-check-inline error-block"></span>',
            errorTemplate: "<span></span>"
        })
    }

    ngOnDestroy(): void {
        if(this.busy){
            this.busy.unsubscribe;
        }
    }

    private close(): void {
        this.association = new CBPAssociationModel();
        this.onClose.emit("Associate");
    }

    checkEIN() {
        if(this.checked){
            jQuery('#ein-number').parsley().removeError('EIN-Invalid',{updateClass: true});
            jQuery('#associatecompany-form').parsley().validate();
        }
        // jQuery('#associatecompany-form').parsley().validate();

        //Determine if the service is already being called to prevent error
        if(this.busy == null || this.busy.closed){
            this.thinking = false;
        } else {
            this.thinking = true;
        }

        if(this.associateEIN.length === 9 && !this.thinking) {
            this.busy = this.companyService.getActiveImptCompanyByEIN(this.associateEIN).subscribe(data => {
                if(data){
                    this.association.parentCmpyEIN = data.einNumber;
                    this.association.parentCmpyName = data.legalName;
                    this.checked = true;
                    this.checkCompany();
                }
            });
        } else {
            this.checked = false;
        }
    }

    private checkCompany() {
        jQuery('#ein-number').parsley().removeError('EIN-Invalid');
        if(!this.isValidString(this.association.parentCmpyEIN)){
            var error = "Error: Employer Identification Number (EIN) is invalid";
            jQuery('#ein-number').parsley().addError('EIN-Invalid',  { message: error, updateClass: true}  );
            this.hasError = true;
        } else {
            this.hasError = false;
        }
    }

    updateAllAssociation() {
        jQuery('#associatecompany-form').parsley().validate();
        if (jQuery('#associatecompany-form').parsley().isValid()) {
           this.association.deltaComment = this.popupRptData.userComments[0].userComment;
           this.association.deltaActionSpan = "multiple";
           this.busy = this.service.associateEntryToParent(this.association).subscribe(data => {
                if(data){
                    this.onAssociate.emit('');
                }
            });
        } else {
            this.fillErrors();
        }
        
    }

    updateAssociation() {
        jQuery('#associatecompany-form').parsley().validate();
        if (jQuery('#associatecompany-form').parsley().isValid()) {
            this.association.deltaComment = this.popupRptData.userComments[0].userComment;
            this.association.deltaActionSpan = "single";
            this.busy = this.service.associateEntryToParent(this.association).subscribe(data => {
                if(data){
                    this.onAssociate.emit('');
                }
            });
        } else {
            this.fillErrors();
        }
    }

    resetAssociation() {
        this.busy = this.service.resetAssotiation(this.association).subscribe(data => {
            if(data){
                this.onAssociate.emit('');
            }
        }, error => {
            this.fillErrors();
        });
    }

    private setAssociation() {
        // this.association.parentCmpyName = this.parentCompany.legalName;
        // this.association.parentCmpyEIN = this.parentCompany.einNumber;
        //This will be the record data
        if(this.isValidString(this.popupRptData.originalEIN)){
            this.association.currentCmpyEIN = this.popupRptData.originalEIN;
        } else {
            this.association.currentCmpyEIN = this.popupRptData.ein;
        }
        this.association.tobaccoClass = this.popupRptData.tobaccoClass;
        this.association.fiscalQtr = this.popupRptData.quarter;
        this.association.fiscalYr = this.popupRptData.fiscalYr;
        this.association.delta = this.popupRptData.totalDeltaTax;
        this.association.permitType = this.popupRptData.permitType;
        this.association.deltaComment = this.popupRptData.userComments[0].userComment;
    }

    fillErrors(): any {
        this.alerts = [];
        if(!this.checked){
            this.alerts.push({
                type: 'danger',
                msg: '<span class="alert-danger"><strong>ACTION REQUIRED</strong>: No EIN entered. Please enter the EIN of the parent company</span>'
            });
        }

        if(this.checked && this.association.parentCmpyName == null) {
            this.alerts.push({
                type: 'danger',
                msg: '<span class="alert-danger"><strong>ACTION REQUIRED</strong>: Invalid company. The entered company either is not in TUFA or does not have an active Importer permit</span>'
            });
        }
    }

    isValidString(str) {
        return ((str != null) && (typeof str !== 'undefined') && (str.length >= 0));
    }
}
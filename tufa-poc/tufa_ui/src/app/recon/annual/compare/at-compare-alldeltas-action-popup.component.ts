/*
@author : Priti
*/

import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Company } from '../../../company/models/company.model';
import { AnnualTrueUpService } from '../services/at.service';

declare var jQuery: any;

/*** 
 * @export
 * @class 
 */
@Component({
    selector: '[at-compare-alldeltas-action-popup]',
    templateUrl: './at-compare-alldeltas-action-popup.template.html',
    styleUrls: ['at-compare-alldeltas-action-popup.style.scss'],
    providers: [AnnualTrueUpService]
})

export class AnnualTrueUpAllDeltasPopUpComponent implements OnInit {

    actionType: string;
    busy: Subscription;
    data: any = [];
    actiontypeList = ['Select Type', 'Association', 'Exclude', 'Market Share Ready', 'In Progress'];
    @Input() model: Company;
    company: any;

    ngOnInit() {
        this.actionType = "Select Type";
        //  jQuery("#at-alldeltas-popup").draggable();
    }

    constructor(private service: AnnualTrueUpService, ) {



    }


    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }
    }

    reset() {
        jQuery('#at-alldeltas-popup').modal('hide');
    }




}
import { AfterViewInit, Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import 'jquery-ui-dist/jquery-ui.js';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { NgControl } from '../../../../../node_modules/@angular/forms';
import { SessionStorageService } from '../../../../../node_modules/ngx-webstorage';
import { finalize } from '../../../../../node_modules/rxjs/operators';
import { Company } from '../../../company/models/company.model';
import { CompanyService } from "../../../company/services/company.service";
import { ShareDataService } from "../../../core/sharedata.service";
import { SelectTobaccoType } from '../../../permit/models/sel-tobacco-type.model';
import { DataTable } from '../../../shared/datatable/DataTable';
import { PaginationAttributes } from '../../../shared/datatable/pagination.attributes.model';
import { DownloadSerivce } from '../../../shared/service/download.service';
import { IngestionWorkFlowService } from '../../../shared/service/ingestion-workflow.service';
import { CompareSearchCriteria } from '../models/at-compare.search.criteria';
import { allDeltaUserComment, TrueUpComparisonResult } from '../models/at-comparisonresult.model';
import { TTBTaxesVol } from '../models/at-ttbtaxesvol.model';
import { AnnualTrueUpComparisonService } from '../services/at-compare.service';
import { AnnualTrueUpService } from '../services/at.service';
import { AnnualTrueUpAllDeltasPopUpAssociationComponent } from './at-compare-alldeltas-action-popup-association.component';

declare let jQuery: any;

@Component({
    selector: '[at-compare]',
    templateUrl: './at-compare.template.html',
    styleUrls: ['at-compare.style.scss', './details/at-compare-cigardetails.style.scss'],
    providers: [CompanyService],
    encapsulation: ViewEncapsulation.None
})



export class AnnualTrueUpCompareComponent implements OnInit, OnDestroy, AfterViewInit {

    @Input() fiscalYear;
    @Input() submitted;
    @Input() createdDate;
    @Input() ingestionContext;
    @Output() processComplete: EventEmitter<any> = new EventEmitter();
    @Output() nullStatusCount: EventEmitter<any> = new EventEmitter();

    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('associateEinInput') associateForm: NgControl;

    showActionModal: boolean = false;

    selOneTobaccoClass: boolean = false;
    selectAllFlag: boolean = false;

    context = '';


    router: Router;
    alerts: { type: string; msg: string; }[];
    tcalerts: { type: string; msg: string; }[];
    compareresults: any;
    shareData: ShareDataService;
    sortBy: string = "ein";
    sortOrder: string[] = ["desc"];
    busy: Subscription;
    chbxbusy: Subscription;
    busyGen: Subscription;
    busyRun: Subscription;
    rightalignClass = "floatright padright";
    csc = new CompareSearchCriteria();

    countofInProgressReports: number = 0;
    cntDeltaToAmendApprove: number = 0;
    cntDeltaChanged: number = 0;
    cntAllDeltaReview: number = 0;
    cntAllDeltaInProgress: number = 0;

    //Delta Action Popup Data
    popupRptData: TrueUpComparisonResult = new TrueUpComparisonResult();
    ein: string;
    permitType: string;
    dataType: string;
    actionType: string;
    companyname: string;
    tobaccoClass: string;
    delta: string;
    quarter: string;
    originalName: string;
    originalEIN: string;
    allDeltaStatus: string;
    resetAlerts: any;
    excludeAlerts: any;
    associateAlerts: any;
    msrAlerts: any;
    msrNoVolAlerts: any;
    affectedDeltaCnt: number = 0;
    affectedTTDeltasGrid: TrueUpComparisonResult[] = [];
    commentModel: any;
    affectedDeltaCntBusy: Subscription;
    exportbusy: Subscription;
    saveDeltaBusy: Subscription;
    deltaComment: string = "";
    fileType: string;
    reviewdata: any;
    cmpAllDeltaStsData: any;
    cbpSummTaxAmount: number;
    cbpDetailTaxAmount: number;
    cbpDetailVolume: number;
    taxRateSummary: number;
    tufaTaxRate: number;
    companyId: string;
    cmpAllDeltaId: number;
    totalDeltaCount: number;

    selectDeltas: boolean = false;
    invisible: boolean = false;
    deltaAsReview: boolean = false;
    fileTypeSelected: string = '';
    inComingPage: string = 'compare';
    tobaccoClassNames: any;
    amendedTax1: number;
    amendedTax2: number;
    validationNum: any;


    subscriptionArry: Subscription[] = [];

    @ViewChild(AnnualTrueUpAllDeltasPopUpAssociationComponent) popUpAssociationComponent: AnnualTrueUpAllDeltasPopUpAssociationComponent;


    //Temporary hack. Needs to be removed after code for Pagination/filtering is handled by DataTable. 
    comparePgLoaded: boolean = false;


    modPrefix = "ATCM";
    actiontypeList = [];
    impIngestActiontypeList = ['Select Type', 'Association', 'Exclude', 'Market Share Ready', 'Reset'];
    otherActiontypeList = ['Select Type', 'Exclude', 'Market Share Ready', 'Reset'];

    columnMapping = {};
    matchedColumnMapping = { legalName: 1, tobaccoClass: 2, quarter: 3, abstotalDeltaTax: 4, permitType: 5, data: 6, status: 7, ein: 9, permitNum: 10 };
    allDeltasColumnMapping = { legalName: 1, tobaccoClass: 2, quarter: 3, abstotalDeltaTax: 4, permitType: 5, data: 6, ein: 7, originalName: 8, alldeltastatus: 9, permitNum: 12 };

    public columns: Array<any> = [
        { name: 'legalName', type: 'input', filtering: { filterString: '', columnName: 'legalName', placeholder: 'Enter Company Name' } },
        { name: 'ein', type: 'input', filtering: { filterString: '', columnName: 'ein', placeholder: 'EIN' } },
        { name: 'permitNum', type: 'input', filtering: { filterString: '', columnName: 'permitNum', placeholder: 'Permit' } },
        { name: 'originalName', type: 'input', filtering: { filterString: '', columnName: 'originalName', placeholder: 'Original Name' } },
        { name: 'tobaccoClass', type: 'input', filtering: { filterString: '', columnName: 'tobaccoClass', placeholder: 'Class' } },
        { name: 'quarter', type: 'input', filtering: { filterString: '', columnName: 'quarter', placeholder: 'Qtr' } },
        { name: 'abstotalDeltaTax', type: 'input', filtering: { filterString: '', columnName: 'abstotalDeltaTax', placeholder: 'Expression' } },
        {
            name: 'permitType', type: 'select', defaultvalue: '',
            options: [
                { name: 'All', value: '' },
                { name: 'Importer', value: 'IMPT' },
                { name: 'Manufacturer', value: 'MANU' }
            ],
            filtering: { filterString: '', columnName: 'permitType', placeholder: 'Filter by Permit Type' }
        },
        {
            name: 'data', type: 'select', defaultvalue: '',
            options: [
                { name: 'All', value: '' },
                { name: 'TUFA', value: 'TUFA' },
                { name: 'Ingested', value: 'Ingested' },
                { name: 'Matched', value: 'Matched' }
            ],
            filtering: { filterString: '', columnName: 'data', placeholder: 'Filter by data' }
        },

    ];

    pgAttrs: PaginationAttributes;

    compoundTTTaxesVolume: TTBTaxesVol[] = [];
    isCompoundTobbaccoTypeDelta: boolean = false;
    navigateAway: boolean;
    isFromBreadcrumbs: boolean = false;
    associateEIN: string;
    einIsChecked: boolean;
    hasError: boolean;
    thinking: boolean;
    associationCompany: Company = new Company();
    public einmask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
    einCheckbusy: Subscription;
    IngestedSummaryDeltas: any;
    IngestedDetailsDeltas: any;
    downloadLoading: boolean = false;
    downloadDetailsLoading: boolean = false;
    unitMeasurement: string;

    summDetailsDataBusy: Subscription;
    loadCommentsBusy: Subscription;


    constructor(
        router: Router,
        // private storage: LocalStorageService,
        private storage: SessionStorageService,
        private atcomparisonservice: AnnualTrueUpComparisonService,
        private _shareData: ShareDataService,
        private _downloadService: DownloadSerivce,
        private _companyService: CompanyService,
        private atService: AnnualTrueUpService,
        private ingestionWorkflowsvc: IngestionWorkFlowService
    ) {

        this.setColumnMapping();
        this.pgAttrs = new PaginationAttributes(1, 15, null, null, [this.columnMapping["ein"]], ["desc"]);

        this.alerts = [
            {
                type: 'danger',
                // tslint:disable-next-line:max-line-length
                msg: '<span class="alert-danger"><strong>ACTION REQUIRED</strong>: Updates found. Please select refresh to regenerate comparison</span>'
            }
        ];

        this.resetAlerts = [
            {
                type: 'warning',
                // tslint:disable-next-line:max-line-length
                msg: 'Resetting a delta(s) will return the entry line to its original state'
            }
        ];

        this.msrAlerts = [
            {
                type: 'warning',
                // tslint:disable-next-line:max-line-length
                msg: 'The action will mark the delta(s) as Market Share Ready'
            }
        ];

        this.msrNoVolAlerts = [
            {
                type: 'warning',
                // tslint:disable-next-line:max-line-length
                msg: 'No ingested volume data exists for this delta.'
            }
        ];

        this.excludeAlerts = [
            {
                type: 'warning',
                // tslint:disable-next-line:max-line-length
                msg: 'Excluding will remove the entries from the Market Share'
            }
        ];

        this.associateAlerts = [
            {
                type: 'warning',
                // tslint:disable-next-line:max-line-length
                msg: 'The action will associate the delta(s) to the inputed company'
            }
        ];

        this.router = router;
        this.shareData = _shareData;
        this.navigateAway = false;

        this.actionType = 'Select Type';
    }

    ngOnInit() {
        this.comparePgLoaded = true;
        // Call subscribe
        this.actionType = "Select Type";

        if (!this.fiscalYear)
            this.fiscalYear = this.shareData.fiscalYear;

        this.initializemultiselectmatched();
        this.initializemultiselectalldeltas();

        //this.onSortOrder(["desc"]);


    }

    ngAfterViewInit() {
        for (let col of this.columns) {
            if (col.filtering) {
                this.cosmeticFix('#filter-' + col.name);
            }
        }
        this.draggthepopup();

        var self = this;
    }

    ngOnDestroy() {
        this.comparePgLoaded = false;
        if (this.busy) {
            this.busy.unsubscribe();
        }
        if (this.chbxbusy) {
            this.chbxbusy.unsubscribe();
        }
        if (this.busyGen) {
            this.busyGen.unsubscribe();
        }
        if (this.einCheckbusy) {
            this.einCheckbusy.unsubscribe();
        }
        if (this.exportbusy)
            this.exportbusy.unsubscribe();

        if(this.busyRun){
            this.busyRun.unsubscribe();
        }    

        this.subscriptionArry.forEach(sub => sub.unsubscribe());

        if (!this.navigateAway) {
            this.clearstorage();
        }

        jQuery('.modal-backdrop').removeClass('modal-backdrop');
    }

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        this.ngOnDestroy();
    }

    getModPrefix() {
        if (this.csc.grid == 'MATCHED') {
            this.modPrefix = 'ATCM';
        } else {
            this.modPrefix = 'ATCD';
        }
    }

    clearFilters() {

        this.csc.clear()

        this.updateFilterTextBox('ein', "");
        this.updateFilterTextBox('permitNum', "");
        this.updateFilterTextBox('legalName', "");
        this.updateFilterTextBox('tobaccoClass', "");
        this.updateFilterTextBox('quarter', "");
        this.updateFilterTextBox('abstotalDeltaTax', "");
        this.updateFilterDropDown('permitType', "");
        this.updateFilterTextBox('originalName', "");
        this.updateFilterCheckBox('selTobaccoClass', "");
        jQuery('#filter-permitType').val('');

        jQuery('#multiselectalldeltas').val('');
        jQuery('#multiselectmatched').val('');
        this.updateFilterDropDown('data', "");

    }

    hideColumn() {
        this.invisible = !this.invisible;
    }

    initializemultiselectmatched() {
        var self = this;

        if (this.csc.grid == 'MATCHED') {

            jQuery('#multiselectalldeltas').hide();
            jQuery('#multiselectalldeltas').next().hide();

            jQuery("#multiselectmatched").on('change', function () {
                var status = jQuery('#multiselectmatched option:selected');
                var selected = [];
                jQuery(status).each(function (index, cmpaction) {
                    selected.push(jQuery(this).val());
                    if (jQuery(this).val() == "DELTACHANGE") {
                        selected.push("EXCLUDECHANGE");
                    }

                });
                self.updateactionfilter(selected, false);
            });


            jQuery('#multiselectmatched').multiselect({
                nonSelectedText: 'Select Action',
                includeSelectAllOption: true
            });

            //If there was a selection set by the storage then don't select all
            if (jQuery('#multiselectmatched')[0].selectedIndex < 0) {
                this.selectAllOptions('#multiselectmatched');
            }
        }

    }

    initializemultiselectalldeltas() {

        var self = this;
        if (this.csc.grid === "ALLDELTAS") {

            jQuery('#multiselectmatched').hide();
            jQuery('#multiselectmatched').next().hide();

            jQuery("#multiselectalldeltas").on('change', function () {
                var status = jQuery('#multiselectalldeltas option:selected');
                var selected = [];
                jQuery(status).each(function (index, cmpaction) {
                    selected.push(jQuery(this).val());
                    if (jQuery(this).val() == "EXCLUDED") {
                        selected.push("EXCLUDED X");
                    }

                });
                self.updateadeltasctionfilter(selected, false);
            });


            jQuery('#multiselectalldeltas').multiselect({

                nonSelectedText: 'Select Action',
                includeSelectAllOption: true
            });

            // If there was a selection set by the storage then don't select all
            if (jQuery('#multiselectalldeltas')[0].selectedIndex < 0) {
                this.selectAllOptions('#multiselectalldeltas');
            }

        }

    }

    private selectAllOptions(multiselectId: string) {
        let alloptions = multiselectId + ' option';
        var values = [];
        jQuery(alloptions).each(function () {
            values.push(jQuery(this).val());
        });

        jQuery(multiselectId).multiselect('select', values, true);
    }

    updateactionfilter(selected, isSetValue) {
        if (selected !== undefined && selected.length > 0) {
            this.csc.statusTypeFilter = selected;
            //setting the selected values in multiselect dropdown
            if (isSetValue) {
                var matchfil = selected;
                matchfil.forEach(function (e) {
                    jQuery('#multiselectmatched option[value="' + e + '"]').prop('selected', true);
                });

            } else {
                this.pgAttrs.activePage = 1;
                this.refreshCompareGrid();
            }

        }
        else {
            this.compareresults = [];
        }
    }

    updateadeltasctionfilter(selected, isSetValue) {
        if (selected !== undefined && selected.length > 0) {

            if (selected.includes('IN PROGRESS'))
                selected.push('IN PROGRESS X');// for exclusion

            this.csc.allDeltaStatusTypeFilter = selected;
            //setting the selected values in multiselect dropdown
            if (isSetValue) {
                var matchfil = selected;
                matchfil.forEach(function (e) {
                    jQuery('#multiselectalldeltas option[value="' + e + '"]').prop('selected', true);
                });
            } else {
                this.pgAttrs.activePage = 1;
                this.refreshCompareGrid();
            }
        }
        else {
            this.compareresults = [];
        }
    }

    updateFilterTextBox(colName, colText) {
        let rowIndex = _.findIndex(this.columns, function (o) { return (o.name && o.name.toString() === colName) });
        if (rowIndex !== -1) {
            let selRow = this.columns[rowIndex];
            if (selRow.filtering) {
                selRow.filtering.filterString = colText;
            }
        }
    }
    updateFilterCheckBox(colName, selTobaccoClass) {
        if (selTobaccoClass != '') {
            if (this.csc.selTobaccoTypes.isAllSelected()) {
                this.selectAllFlag = true;
            }
        } else {
            this.selectAllFlag = false;
        }
    }

    updateFilterDropDown(colName, colText) {
        let rowIndex = _.findIndex(this.columns, function (o) { return (o.name && o.name.toString() === colName) });
        if (rowIndex !== -1) {
            let selRow = this.columns[rowIndex];
            if (selRow.filtering) {
                selRow.filtering.filterString = colText;
                selRow.defaultvalue = colText;
            }
        }
    }


    saveSearchCriteriaToLocalStorage() {
        this.storage.store('compare-filters', this.csc);
    }

    loadSearchCriteriaFromLocalStorage() {
        let filterFromStorage = this.storage.retrieve('compare-filters');
        if (filterFromStorage) {
            Object.assign(this.csc, filterFromStorage);
            let selTobaccoTypes = this.csc.selTobaccoTypes;
            this.csc.selTobaccoTypes = new SelectTobaccoType();
            Object.assign(this.csc.selTobaccoTypes, selTobaccoTypes);
        } else {
            this.csc = new CompareSearchCriteria();
        }

        this.updateFilterInputs();
    }

    updateFilterInputs() {
        if (this.csc.companyFilter != null) this.updateFilterTextBox('legalName', this.csc.companyFilter);
        if (this.csc.einFilter != null) this.updateFilterTextBox('ein', this.csc.einFilter);
        if (this.csc.permitNumFilter != null) this.updateFilterTextBox('permitNum', this.csc.permitNumFilter);
        if (this.csc.tobaccoClassFilter != null) this.updateFilterTextBox('tobaccoClass', this.csc.tobaccoClassFilter);
        if (this.csc.quarterFilter != null) this.updateFilterTextBox('quarter', this.csc.quarterFilter);
        if (this.csc.deltaTaxFilter != null) this.updateFilterTextBox('abstotalDeltaTax', this.csc.deltaTaxFilter);
        if (this.csc.originalNameFilter != null) this.updateFilterTextBox('originalName', this.csc.originalNameFilter);

        if (this.csc.permitTypeFilter != null) this.updateFilterDropDown('permitType', this.csc.permitTypeFilter);
        if (this.csc.dataTypeFilter != null) this.updateFilterDropDown('data', this.csc.dataTypeFilter);

        if (this.csc.selTobaccoTypes != null) this.updateFilterCheckBox('selTobaccoClass', this.csc.selTobaccoTypes);

        if (this.csc.statusTypeFilter != null) this.updateactionfilter(this.csc.statusTypeFilter, true);
        if (this.csc.allDeltaStatusTypeFilter != null) this.updateadeltasctionfilter(this.csc.allDeltaStatusTypeFilter, true);

    }

    clearstorage() {
        this.storage.clear('apAttributes');
        this.storage.clear('compare-filters');
    }

    isValidString(str) {
        return ((str != null) && (typeof str !== 'undefined') && (str.length >= 0));
    }
    isValidArr(arr) {
        return ((arr != null) && (typeof arr !== 'undefined') && (arr.length >= 0));
    }

    /**
     * This method gets all annual comparison results whether TUFA or Ingested matches or not
     * CTPTUFA-3382: Annual Compare Page should show all delta
     */
    public getAllAnnualComparisonResults() {

        if (this.isOneTobaccoClassSelected()) {
            this.selOneTobaccoClass = false;
        } else {
            this.compareresults = [];
            this.pgAttrs.itemsTotal = 0;
            this.cntAllDeltaInProgress = 0;
            this.cntAllDeltaReview = 0;
            return;
        }

        // since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
        if (this.busy) {
            this.busy.unsubscribe();
        }
        if (this.chbxbusy) {
            this.chbxbusy.unsubscribe();
        }

        this.chbxbusy = this.atcomparisonservice.getAllComparisonResultsCounts(this.fiscalYear, this.inComingPage).subscribe(
            data => {
                if (data) {
                    this.cntAllDeltaInProgress = data.ttlDeltaCntInProgress;
                    this.cntAllDeltaReview = data.ttlDeltaCntInReview;
                }
            },
            error => {
                this.alerts = [];
                this.alerts.push({ type: 'warning', msg: error });
            }
        );

        this.saveSearchCriteriaToLocalStorage();
        this.busy = this.atcomparisonservice.getAllComparisonResults(this.fiscalYear, this.csc, this.pgAttrs).subscribe(data => {
            if (data) {
                let resultArray = data.results;
                if (!(resultArray.length == 1 && resultArray[0].ein == null)) {
                    this.compareresults = data.results;
                    this.pgAttrs.itemsTotal = data.itemsTotal;
                } else {
                    this.compareresults = [];
                }
                // this.calculateReviewInProgressDeltaCounts(data.results, data.itemsTotal);
            }
        },
            error => {
                this.alerts = [];
                this.alerts.push({ type: 'warning', msg: error });
            });
    }





    /**
     * This method gets annual comparison results that matches on both TUFA and Ingested.
     */
    getAnnualComparisonResults(selected: boolean = false) {

        if (selected) {
            //Load search riteria if coming back to the page
            this.loadSearchCriteriaFromLocalStorage();
        }

        if (!this.fiscalYear)
            this.fiscalYear = this.shareData.fiscalYear;

        if (this.csc.grid === "ALLDELTAS") {
            this.getAllAnnualComparisonResults();
            return;
        }


        // since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
        if (this.busy) {
            this.busy.unsubscribe();
        }

        if (this.chbxbusy) {
            this.chbxbusy.unsubscribe();
        }



        this.saveSearchCriteriaToLocalStorage();


        this.busy = this.atcomparisonservice.getComparisonResults(this.fiscalYear, this.csc, this.pgAttrs).subscribe(
            data => {
                if (data) {
                    let resultArray = data.results;
                    if (!(resultArray.length == 1 && resultArray[0].ein == null)) {
                        this.compareresults = data.results;
                        this.pgAttrs.itemsTotal = data.filteredRows;
                        this.totalDeltaCount = data.itemsTotal;
                    } else {
                        this.compareresults = [];
                        this.totalDeltaCount = 0;
                    }
                    this.chbxbusy = this.atcomparisonservice.getAllComparisonResultsCounts(this.fiscalYear, 'Matched').subscribe(
                        data => {
                            if (data) {
                                this.calculateAmendApproveDeltaCounts(data, this.totalDeltaCount);
                            }
                        },
                        error => {
                            this.alerts = [];
                            this.alerts.push({ type: 'warning', msg: error });
                        }
                    );
                    //this.compareresults = data.results;
                    // this.pgAttrs.itemsTotal = data.filteredRows;
                    //this.calculateAmendApproveDeltaCounts(data.results, data.itemsTotal);
                }
            },
            error => {
                this.alerts = [];
                this.alerts.push({ type: 'warning', msg: error });
            }
        );



    }

    calculateAmendApproveDeltaCounts(data, itemsTotal) {
        //Calculate the Amend and Approve Counts.
        let ttlCntDeltaChng = 0;
        let ttlCntDeltaChngZero = 0;
        let ttlCntAccptApprv = 0;

        // if (results.length > 0) {
        ttlCntDeltaChng = data.ttlCntDeltaChng;
        ttlCntDeltaChngZero = data.ttlCntDeltaChngZero;
        ttlCntAccptApprv = data.ttlCntAccptApprv;
        this.countofInProgressReports = data.ttlCntInProgress;
        //}


        this.cntDeltaToAmendApprove = itemsTotal - (ttlCntDeltaChngZero + ttlCntAccptApprv);
        this.nullStatusCount.emit(this.cntDeltaToAmendApprove + ttlCntDeltaChng);
        this.cntDeltaChanged = ttlCntDeltaChng;
    }


    calculateReviewInProgressDeltaCounts(results, itemsTotal) {

        let ttlCntAllDeltaReview = 0;
        let ttlCntAllDeltaInProgress = 0;

        if (results.length > 0) {

            ttlCntAllDeltaReview = results[0].ttlDeltaCntInReview;
            ttlCntAllDeltaInProgress = results[0].ttlDeltaCntInProgress;

        }

        this.cntAllDeltaInProgress = ttlCntAllDeltaInProgress;
        this.cntAllDeltaReview = ttlCntAllDeltaReview;
    }



    toAnnualCompareDetailsPage(rptStatus) {
        this.shareData.compareResult = rptStatus;
        this.navigateAway = true;
        this.shareData.reset();
        this.shareData.fiscalYear = this.fiscalYear;
        this.shareData.breadcrumb = 'at-compare';
        this.shareData.qatabfrom = 'at-compare';

        //for associated line
        this.shareData.ein = rptStatus.ein;
        this.shareData.companyName = rptStatus.legalName;
        this.shareData.companyId = rptStatus.companyId;
        this.shareData.originalEIN = rptStatus.originalEIN;
        this.shareData.originalName = rptStatus.originalName;
        this.shareData.permitType = rptStatus.permitType;
        this.shareData.tobaccoClass = rptStatus.tobaccoClass.replace(/-/g, ' ');
        this.shareData.status = rptStatus.status;
        this.shareData.quarter = rptStatus.quarter === "1-4" ? 5 : Number(rptStatus.quarter);
        this.shareData.createdDate = this.createdDate;
        this.shareData.submitted = this.submitted;
        this.shareData.disableAmendApprove = (rptStatus.status === 'DeltaChange' && rptStatus.totalDeltaTax == 0);
        if (rptStatus.deltaexcisetaxFlag === 'Review') {
            this.shareData.deltaExciseTaxFlag = rptStatus.deltaexcisetaxFlag;
        }
        this.shareData.clearExpansionMaps();
    }

    public refreshCompareGrid() {
        if (this.sortBy === "" || this.sortOrder.length === 0) {
            this.sortBy = "ein";
            this.onSortOrder(["desc"]);
            // this.getAnnualComparisonResults();
        }
        else {
            this.onSortOrder(this.sortOrder);
        }
    }

    public onPageChange(event) {
        this.pgAttrs.pageSize = event.rowsOnPage;
        this.pgAttrs.activePage = event.activePage;

        this.getAnnualComparisonResults();

    }

    public onSortOrder(event) {
        this.pgAttrs.sortOrder = event;
        this.pgAttrs.sortBy = [];
        this.setColumnMapping();
        this.pgAttrs.sortBy.push(this.columnMapping[this.sortBy]);
        this.getAnnualComparisonResults();
        if (this.comparePgLoaded) {
            this.comparePgLoaded = false;
        }
    }

    applyFilter() {
        this.getAnnualComparisonResults();
    }

    public onChangeTable(filterinfo): void {
        if (filterinfo) {
            if (filterinfo.columnName === "legalName") {
                this.csc.companyFilter = filterinfo.filterString;
                this.csc.companyFilter = this.csc.companyFilter.trim();
            } else if (filterinfo.columnName === "ein") {
                this.csc.einFilter = filterinfo.filterString;
                this.csc.einFilter = this.csc.einFilter.trim().replace(/-/g, "");
            } else if (filterinfo.columnName === "permitNum") {
                this.csc.permitNumFilter = filterinfo.filterString;
                this.csc.permitNumFilter = this.csc.permitNumFilter.trim().replace(/-/g, "");
            } else if (filterinfo.columnName === "tobaccoClass") {
                this.csc.tobaccoClassFilter = filterinfo.filterString;
                this.csc.tobaccoClassFilter = this.csc.tobaccoClassFilter.trim();
            } else if (filterinfo.columnName === "quarter") {
                this.csc.quarterFilter = filterinfo.filterString;
                this.csc.quarterFilter = this.csc.quarterFilter.trim();
            } else if (filterinfo.columnName === "abstotalDeltaTax") {
                this.csc.deltaTaxFilter = filterinfo.filterString;
                this.csc.deltaTaxFilter = this.csc.deltaTaxFilter.trim();
            } else if (filterinfo.columnName === "originalName") {
                this.csc.originalNameFilter = filterinfo.filterString;
                this.csc.originalNameFilter = this.csc.originalNameFilter.trim();
            }

            if (filterinfo.refresh) {
                this.resetPagination();
                this.getAnnualComparisonResults();
            }
        }
    }

    filterSelectionChanged(event, filterColumnName) {

        if (filterColumnName === "permitType") {
            this.csc.permitTypeFilter = event;
            this.getAnnualComparisonResults();
        }


        if (filterColumnName === "data") {
            this.csc.dataTypeFilter = event;
            this.getAnnualComparisonResults();
        }

    }

    /**
     * This cosmetic fix is to allow the blue focus highlight for the input group including
     * the search icon on the textbox
     * @param {*} elementID
     *
     * @memberOf SearchCriteriaPage
     */
    cosmeticFix(elementID: any) {
        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let filterInput = jQuery(elementID);
        filterInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

    isOneTobaccoClassSelected() {
        // At least one tobacco class should be selected
        return this.csc.selTobaccoTypes.isOneSelected();
    }

    isSelectAllFlag() {
        // At least one tobacco class not selected, then remove the All select checkbox
        return this.csc.selTobaccoTypes.isAllSelected();

    }
    /**
     * When user changes selection method 
     *  - MATCHED
     *  - ALL DELTAS
     */
    public onCompareSelectionChange() {
        this.selOneTobaccoClass = false;
        // Reset the grid
        this.compareresults = [];
        this.pgAttrs.itemsTotal = 0;
        this.pgAttrs.activePage = 1;

        if (this.busyGen) {
            this.busyGen.unsubscribe();
        }
        if(this.busyRun){
            this.busyRun.unsubscribe();
        }
        if (this.csc.grid === "ALLDELTAS") {

            if (this.modPrefix == 'ATCM') {
                this.clearstorage();
                this.clearFilters();
            }

            this.modPrefix = 'ATCD';

            // At least one tobacco class should be selected
            if (this.isOneTobaccoClassSelected() === false) {
                this.selOneTobaccoClass = true;
            }

            this.selectAll(false, false);


            jQuery('#multiselectalldeltas').next().show();
            jQuery('#multiselectmatched').next().hide();
            this.initializemultiselectalldeltas();
        }

        if (this.csc.grid === "MATCHED") {


            if (this.modPrefix = 'ATCD') {
                this.clearstorage();
                this.clearFilters();
            }
            this.modPrefix = 'ATCM';

            jQuery('#multiselectalldeltas').next().hide();
            jQuery('#multiselectmatched').next().show();
            this.initializemultiselectmatched();
        }
        //this.refreshCompareGrid();
        this.sortBy = "ein";
        this.onSortOrder(["desc"]);
    }

    isColumnFilteringAllowed(col: any) {
        if (col.filtering) {
            if (this.csc.grid === 'MATCHED' && col.name === 'originalName') {
                return false;
            }
            if (this.csc.grid === 'MATCHED' && col.name === 'alldeltastatus') {
                return false;
            }
            if (this.csc.grid === 'ALLDELTAS' && col.name === 'status') {
                return false;
            }
            if (this.csc.grid === 'MATCHED' && col.name === 'data') {
                return false;
            }
            return true;
        }
        return false;
    }

    checkboxSelected(ttName: string) {
        switch (ttName) {
            case "Cigarettes":
                this.csc.selTobaccoTypes.showCigarettes = !this.csc.selTobaccoTypes.showCigarettes;
                break;

            case "Cigars":
                this.csc.selTobaccoTypes.showCigars = !this.csc.selTobaccoTypes.showCigars;
                break;

            case "Snuff":
                this.csc.selTobaccoTypes.showSnuffs = !this.csc.selTobaccoTypes.showSnuffs;
                break;

            case "Chew":
                this.csc.selTobaccoTypes.showChews = !this.csc.selTobaccoTypes.showChews;
                break;

            case "Pipe":
                this.csc.selTobaccoTypes.showPipes = !this.csc.selTobaccoTypes.showPipes;
                break;

            case "RollYourOwn":
                this.csc.selTobaccoTypes.showRollYourOwn = !this.csc.selTobaccoTypes.showRollYourOwn;
                break;

            default:
                break;
        }
        this.isSelectAllFlag();
        this.resetPagination();
        // Priti - Unnecesary call commented out  (trying to hit the api twice)
        //this.getAllAnnualComparisonResults();
        this.onSortOrder(["desc"]);
    }

    // Popup logic below

    private resetPagination() {
        this.pgAttrs.activePage = 1;
    }

    popUpClose() {
        jQuery('#at-alldeltas-popup').modal('hide');
        jQuery('#at-alldeltas-popup-content').removeAttr('style');
        this.resetPopUpState();
        this.fileTypeSelected = '';
        this.refreshCompareGrid();
        this.storage.clear('pgattr-compare-comments');
    }

    private resetPopUpState() {
        this.deltaComment = null;
        this.actionType = 'Select Type';
        this.compoundTTTaxesVolume = [];
        this.affectedTTDeltasGrid = [];
        this.affectedDeltaCnt = 0;
        this.selectDeltas = false;
        this.associationCompany = new Company();
        this.hasError = false;
        this.einIsChecked = false;
        this.deltaAsReview = false;
        this.amendedTax1 = null;
        this.amendedTax2 = null;
        this.validationNum = null;
    }

    refreshOnReset() {
        this.popUpClose();
        this.refreshCompareGrid();
    }

    toalldeltaspopup(rptStatus: TrueUpComparisonResult) {

        //All these setter should be obsolete

        this.commentModel = null;

        this.ein = rptStatus.ein;
        this.companyId = rptStatus.companyId;
        this.dataType = rptStatus.dataType;
        this.permitType = rptStatus.permitType;
        this.companyname = rptStatus.legalName;
        this.quarter = rptStatus.quarter === "1-4" || rptStatus.quarter === "5" ? "1-4" : rptStatus.quarter;
        this.tobaccoClass = rptStatus.tobaccoClass;

        this.tobaccoClassNames = this.tobaccoClass.split("/");
        this.delta = rptStatus.totalDeltaTax;

        if (rptStatus.deltaexcisetaxFlag === 'Review')
            this.delta = 'Review';



        this.originalName = rptStatus.originalName;
        this.originalEIN = rptStatus.originalEIN;
        this.allDeltaStatus = rptStatus.allDeltaStatus;

        this.popupRptData = rptStatus;
        if (this.popupRptData.originalEIN) {
            this.associationCompany.einNumber = this.popupRptData.ein;
            this.checkEIN();
        }
        //Keep the comment seperate
        this.originalEIN = rptStatus.originalEIN;
        this.allDeltaStatus = rptStatus.allDeltaStatus;

        this.popupRptData = rptStatus;

        if (this.delta === 'Review')
            this.deltaAsReview = true;

        if (this.popupRptData.originalEIN) {
            this.associationCompany.einNumber = this.popupRptData.ein;
            this.checkEIN();
        }
        //Keep the comment seperate
        // this.deltaComment = rptStatus.userComments[0].userComment;
        this.popupRptData.fiscalYr = this.fiscalYear;
        this.getAlldeltasComments();

        if (this.popupRptData.tobaccoClass === 'Chew/Snuff' || this.popupRptData.tobaccoClass === 'Pipe/Roll-Your-Own') {
            this.isCompoundTobbaccoTypeDelta = true;
            this.getNonCigarManuDetails(this.popupRptData);
        } else
            this.isCompoundTobbaccoTypeDelta = false;


        if (this.isValidString(this.allDeltaStatus) && ['Review', 'Confilcted', 'In Progress', 'In Progress X'].indexOf(this.allDeltaStatus) < 0) {
            this.actionType = this.getMappedActionFromStatus(this.allDeltaStatus);

            this.actiontypeList = [this.actionType];

            if (this.allDeltaStatus !== 'Excluded X') {
                this.actiontypeList.push('Reset');
                this.getAffectedDeltas();
            }
        } else if (this.dataType === 'INGESTED' && this.permitType === 'Importer') {
            this.actiontypeList = this.impIngestActiontypeList;
        } else {
            this.actiontypeList = this.otherActiontypeList;
        }
        this.context = 'alldeltas';
        this.draggthepopup();

        if (this.deltaAsReview)
            this.getSummDetailData();

    }

    getAlldeltasComments() {

        let tobaccoClassName = this.tobaccoClass.replace(/\//g, "-");
        let quarter = this.quarter == '1-4' ? '0' : this.quarter;

        let mainEin = null;

        if (this.allDeltaStatus == 'Associated')
            mainEin = this.originalEIN;
        else
            mainEin = this.ein;

        this.loadCommentsBusy = this.atcomparisonservice.getAlldeltasComments(this.fiscalYear, mainEin, tobaccoClassName, quarter, this.permitType).subscribe(
            data => {
                if (data) {
                    this.commentModel = _.orderBy(data[0].userComments, 'commentDate', 'desc');
                }
                else
                    this.commentModel = null;
            }
            , (error) => {
                throw (error);
            });

        this.subscriptionArry.push(this.loadCommentsBusy);
    }

    onFileTypeSelChanged(event: any = null) {
        if (this.fileTypeSelected === 'Summary') {
            if (this.cbpSummTaxAmount !== undefined && this.cbpSummTaxAmount !== null)
                this.popupRptData.totalDeltaTax = this.cbpSummTaxAmount.toString();
        }
        else {
            if (this.cbpDetailTaxAmount !== undefined && this.cbpDetailTaxAmount !== null)
                this.popupRptData.totalDeltaTax = this.cbpDetailTaxAmount.toString();
        }
    }

    public selectAll(event, refresh: boolean = true) {
        this.selectAllFlag = event;
        if (event) {
            this.checkAllBoxes();
        }
        else {
            this.uncheckAllBoxes();
        }
        if (refresh) {
            this.refreshCompareGrid();
        }
        // Priti - Unnecesary call commented out  (trying to hit the api twice)       
        // this.onSortOrder(["desc"]);
    }

    checkAllBoxes() {
        this.csc.selTobaccoTypes.showCigarettes = true;
        this.csc.selTobaccoTypes.showCigars = true;
        this.csc.selTobaccoTypes.showSnuffs = true;
        this.csc.selTobaccoTypes.showChews = true;
        this.csc.selTobaccoTypes.showPipes = true;
        this.csc.selTobaccoTypes.showRollYourOwn = true;
    }

    uncheckAllBoxes() {
        this.csc.selTobaccoTypes.showCigarettes = false;
        this.csc.selTobaccoTypes.showCigars = false;
        this.csc.selTobaccoTypes.showSnuffs = false;
        this.csc.selTobaccoTypes.showChews = false;
        this.csc.selTobaccoTypes.showPipes = false;
        this.csc.selTobaccoTypes.showRollYourOwn = false;
        this.cntAllDeltaReview = 0;
        this.cntAllDeltaInProgress = 0;

    }


    draggthepopup() {
        jQuery(document).ready(function () {
            jQuery('#at-alldeltas-popup-content').draggable();
        });

    }


    save() {
        if (this.actionType === 'Association') {
            this.associate();
        } else if (this.actionType === 'Exclude') {
            this.exclude();
        } else if (this.actionType === 'Market Share Ready' || this.popupRptData.allDeltaStatus === 'Market Share Ready') {
            this.mktShrRdyDeltaAction();
        }

    }

    saveAll() {
        if (this.actionType === 'Association') {
            this.associateAll()
        } else if (this.actionType === 'Exclude') {
            this.excludeAll();
        } else if (this.actionType === 'Market Share Ready' || this.popupRptData.allDeltaStatus === 'Market Share Ready') {
            this.mktShrRdyAllDeltaActions();
        }

    }

    resetAction() {
        if (this.actionType === 'Association') {
            this.popUpAssociationComponent.resetAssociation();
        }
    }

    associate() {
        this.associateForm.control.markAsTouched();
        if (this.associateForm.valid && !this.hasError) {
            let updateRptData = this.getDeltaStsChngObject("Associated", "single");
            this.updateCompareAllDeltaStatus(updateRptData, true);
        }

    }

    associateAll() {
        this.associateForm.control.markAsTouched();
        if (this.associateForm.valid && !this.hasError) {
            let updateRptData = this.getDeltaStsChngObject("Associated", "multiple");
            this.updateCompareAllDeltaStatus(updateRptData, true);
        }

    }

    exclude() {
        let updateRptData = this.getDeltaStsChngObject("Excluded", "single");
        this.updateCompareAllDeltaExclude(updateRptData);
    }

    excludeAll() {
        let updateRptData = this.getDeltaStsChngObject("Excluded", "multiple");
        this.updateCompareAllDeltaExclude(updateRptData);
    }


    updateCompareAllDeltaExclude(exludeData: any) {
        this.busy = this.atcomparisonservice.updateCompareAllDeltaExclude(exludeData, this.fiscalYear).subscribe(
            data => {
                if (data) {
                    this.refreshCompareGrid();
                    this.popUpClose();
                }
            }
        );

    }

    private setColumnMapping() {
        switch (this.csc.grid) {
            case 'MATCHED': {
                this.columnMapping = this.matchedColumnMapping;
                break;
            }
            case 'ALLDELTAS': {
                this.columnMapping = this.allDeltasColumnMapping;
                break;
            }
        }
    }

    public mktShrRdyAllDeltaActions() {
        let updateRptData = this.getDeltaStsChngObject("MSReady", "multiple");
        this.updateCompareAllDeltaStatus(updateRptData, true);
    }

    public mktShrRdyDeltaAction() {
        let updateRptData = this.getDeltaStsChngObject("MSReady", "single");

        if (this.isCmpdIngestTT()) {
            let tobaccoSubTypesTxVol: TTBTaxesVol[] = [];
            if (this.compoundTTTaxesVolume.length > 0)
                tobaccoSubTypesTxVol = this.compoundTTTaxesVolume;
            else if (this.isCmpdIngestTT() && this.compoundTTTaxesVolume.length == 0) {
                let ttb1: TTBTaxesVol = {
                    qtr: this.quarter, ein: this.ein, tobaccoClass: this.tobaccoClassNames[0],
                    pounds: null, fiscalYr: this.fiscalYear, estimatedTax: null, amendedTax: this.amendedTax1, tobaccoSubTypeAmndTx: null
                };
                tobaccoSubTypesTxVol.push(ttb1);
                let ttb2: TTBTaxesVol = {
                    qtr: this.quarter, ein: this.ein, tobaccoClass: this.tobaccoClassNames[1],
                    pounds: null, fiscalYr: this.fiscalYear, estimatedTax: null, amendedTax: this.amendedTax2, tobaccoSubTypeAmndTx: null
                };
                tobaccoSubTypesTxVol.push(ttb2);
            }
            let tobaccoSubTypeAmndTxVol = {};

            for (let i in tobaccoSubTypesTxVol) {
                let ttTypeVoltx = tobaccoSubTypesTxVol[i];
                let txvolarry = [];
                txvolarry.push(ttTypeVoltx.amendedTax);
                txvolarry.push(ttTypeVoltx.pounds);
                tobaccoSubTypeAmndTxVol[ttTypeVoltx.tobaccoClass] = txvolarry;
            }
            updateRptData[0].tobaccoSubTypeTxVol = tobaccoSubTypeAmndTxVol;
        }

        this.updateCompareAllDeltaStatus(updateRptData, true);
    }

    public resetAllDeltaActions() {
        let updateRptData = this.getDeltaStsChngObject("resetAll", "multiple");
        this.updateCompareAllDeltaStatus(updateRptData, true);
        this.fileTypeSelected = '';
    }

    public resetDeltaAction() {
        let updateRptData = this.getDeltaStsChngObject("reset", "single");
        this.updateCompareAllDeltaStatus(updateRptData, true);
        this.fileTypeSelected = '';
    }


    public inProgressAction() {
        let updateRptData = this.getDeltaStsChngObject("In Progress", "single");
        this.updateCompareAllDeltaStatus(updateRptData, true);
    }

    //For the delta with status as 'Review' then save the comment and update the staus to inprogress for all others just update the comment
    public saveCommentAction() {
        let updateRptData: TrueUpComparisonResult[] = [];
        let isClosePopup = this.actionType === 'Select Type' || this.actionType === this.getMappedActionFromStatus(this.popupRptData.allDeltaStatus);
        if (isClosePopup && (this.deltaComment == null || this.deltaComment.trim().length == 0)) {
            this.popUpClose();
            return;
        }

        if (this.allDeltaStatus == 'Review') {
            updateRptData = this.getDeltaStsChngObject("In Progress", "single");
        } else {
            //call backend only to update the comment
            let deltaCurrentStatus = this.popupRptData.allDeltaStatus;
            updateRptData = this.getDeltaStsChngObject(deltaCurrentStatus, "single");

        }
        this.updateCompareAllDeltaStatus(updateRptData, isClosePopup);
        this.deltaComment = null;
    }

    public saveAllCommentAction() {
        let updateRptData = this.getDeltaStsChngObject("saveAllComment", "single");
        let isClosePopup = this.actionType === 'Select Type' || this.actionType === this.getMappedActionFromStatus(this.popupRptData.allDeltaStatus);
        if (isClosePopup && (this.deltaComment == null || this.deltaComment.trim().length == 0)) {
            this.popUpClose();
        } else {
            this.updateCompareAllDeltaStatus(updateRptData, isClosePopup);
            this.deltaComment = null;
        }
    }


    private getDeltaStsChngObject(deltaStatus, actionSpan, delta = this.popupRptData) {

        if (actionSpan == 'single') {
            let updateRptData = new TrueUpComparisonResult();


            updateRptData.fileType = null;
            jQuery.extend(true, updateRptData, delta);

            if (this.allDeltaStatus == 'Associated' && ['reset', 'resetAll', 'Associated'].indexOf(deltaStatus) < 0) {
                updateRptData.legalName = updateRptData.originalName;
                updateRptData.ein = updateRptData.originalEIN;

            } else if (deltaStatus == "Associated") {


                if (delta.originalEIN) {
                    //Rest parent information
                    updateRptData.ein = this.associationCompany.einNumber;
                    updateRptData.legalName = this.associationCompany.legalName;
                } else {
                    //Save orignal information
                    updateRptData.originalEIN = delta.ein;
                    updateRptData.originalName = delta.legalName;

                    //Set new parent information
                    updateRptData.ein = this.associationCompany.einNumber;
                    updateRptData.legalName = this.associationCompany.legalName;
                }
            }
            updateRptData.allDeltaCurrentStatus = this.popupRptData.allDeltaStatus;
            updateRptData.allDeltaStatus = deltaStatus;
            if (this.delta === 'Review' && deltaStatus == "MSReady")
                updateRptData.fileType = this.fileTypeSelected;

            let deltasStatusChange: TrueUpComparisonResult[] = [];

            if (this.deltaComment) {

                let userComment = [];
                let comm = new allDeltaUserComment();
                comm.userComment = this.deltaComment;
                userComment.push(comm);
                updateRptData.userComments = userComment;
                if (this.compoundTTTaxesVolume.length != 0) {
                    let tobaccoSubTypesTxVol: TTBTaxesVol[] = this.compoundTTTaxesVolume;
                    let tobaccoSubTypeAmndTxVol = {};

                    for (let i in tobaccoSubTypesTxVol) {
                        let ttTypeVoltx = tobaccoSubTypesTxVol[i];
                        let txvolarry = [];
                        txvolarry.push(ttTypeVoltx.amendedTax);
                        txvolarry.push(ttTypeVoltx.pounds);
                        tobaccoSubTypeAmndTxVol[ttTypeVoltx.tobaccoClass] = txvolarry;
                    }
                    updateRptData.tobaccoSubTypeTxVol = tobaccoSubTypeAmndTxVol;
                }
            }

            deltasStatusChange.push(updateRptData);
            return deltasStatusChange;
        }
        else if (actionSpan == 'multiple') {
            let deltasStatusChange: TrueUpComparisonResult[] = [];
            this.affectedTTDeltasGrid.forEach(delta => {
                if (delta.affectDeltaFlag) {
                    //Smae logic is used so a recursive call with the actionSpan of 'single' can be used
                    let updateRptData = this.getDeltaStsChngObject(deltaStatus, 'single', delta);
                    deltasStatusChange = deltasStatusChange.concat(updateRptData);
                }
            });

            return deltasStatusChange;
        }
    }

    private updateCompareAllDeltaStatus(updateRptData, isClosePopup) {

        this.saveDeltaBusy = this.atcomparisonservice.updateCompareAllDeltaStatus(updateRptData, this.fiscalYear).subscribe(
            data => {
                if (data) {
                    this.refreshCompareGrid();
                    setTimeout(() => {
                        this.getAlldeltasComments();
                    }, 500);
                    if (isClosePopup) {
                        this.popUpClose();
                    }

                }
            }
        );


        this.subscriptionArry.push(this.saveDeltaBusy);
    }


    //Get Volume Data and Tax Rates for Compound Tobacco Types

    private getNonCigarManuDetails(delta) {

        let tobaccoClassName = delta.tobaccoClass.replace(/\//g, " ")
        this.compoundTTTaxesVolume = [];
        this.busy = this.atcomparisonservice.getTTBVolTaxesPerTobaccoType(delta.ein, delta.fiscalYr, delta.quarter, tobaccoClassName).subscribe(

            data => {
                let ttbVolTaxes: TTBTaxesVol[] = data;
                for (let i in ttbVolTaxes) {
                    let ttbVolTx = new TTBTaxesVol();
                    jQuery.extend(true, ttbVolTx, ttbVolTaxes[i]);
                    this.compoundTTTaxesVolume.push(ttbVolTx);
                    if (ttbVolTaxes[i].amendedTax == null) {
                        if (ttbVolTaxes.length > 1 && ttbVolTaxes[i].pounds !== null && this.allDeltaStatus == 'Review')
                            ttbVolTx.amendedTax = Math.abs((ttbVolTaxes[i].estimatedTax / (ttbVolTaxes[0].estimatedTax + ttbVolTaxes[1].estimatedTax)) * Number(this.popupRptData.totalDeltaTax));
                        else if (ttbVolTaxes.length > 1 && ttbVolTaxes[i].pounds == null)
                            ttbVolTx.amendedTax = null;
                        else if (ttbVolTaxes.length > 1 && ttbVolTaxes[i].pounds !== null && this.allDeltaStatus == 'MSReady')
                            ttbVolTx.amendedTax = null;
                        else
                            ttbVolTx.amendedTax = this.roundDecimal(Math.abs(Number(this.popupRptData.totalDeltaTax)));
                    }
                }

            }
        )
        this.subscriptionArry.push(this.busy);

    }


    onActionChange() {

        this.affectedTTDeltasGrid = [];
        this.affectedDeltaCnt = 0;
        this.selectDeltas = false;

        if (this.actionType === 'Select Type')
            return;
        else if (this.actionType === 'Association') {
            if (this.isValidString(this.popupRptData.originalEIN)) {
                this.associationCompany.einNumber = this.popupRptData.ein;
                this.checkEIN();
            } else {
                this.associationCompany.einNumber = '';
            }
        }

        if (this.isCmpdIngestTT())
            return;

        this.getAffectedDeltas();

        if (this.deltaAsReview)
            this.getSummDetailData();

        // Change the unit of measurement for the volume per tobacco class
        // tslint:disable-next-line: max-line-length

        if (this.tobaccoClass == 'Cigarettes' || this.tobaccoClass == 'Cigars')
            this.unitMeasurement = 'K';
        else
            this.unitMeasurement = 'Kgs'



    }

    inputValidator(event: any) {
        const pattern = /^[0-9]\d{0,9}(?:\.\d{1,2})?$/;
        const pattern1 = /^[0-9]\d{0,9}(?:\.)?$/;
        if (event.target.value !== "") {
            if (pattern1.test(event.target.value)) {
                event.target.value = event.target.value;
                this.validationNum = event.target.value;
            }
            else if (!pattern.test(event.target.value)) {
                event.target.value = event.target.value.replace(event.target.value, this.validationNum);
            }
            this.validationNum = event.target.value;
        }
    }
    storeCurrentValue(event: any) {
        this.validationNum = event.target.value;
    }
    private getSummDetailData() {

        let tobaccoClass = this.tobaccoClass.replace(/\//g, " ");

        if (this.summDetailsDataBusy) {
            this.summDetailsDataBusy.unsubscribe();
        }
        let ein = this.popupRptData.originalEIN ? this.popupRptData.originalEIN : this.popupRptData.ein;
        let einType = this.popupRptData.originalEIN ? "original" : "ein";

        this.summDetailsDataBusy = this.atcomparisonservice.getImporterSummDetailsData(einType, ein,
            this.fiscalYear, this.quarter, tobaccoClass).subscribe(
                data => {
                    if (data) {
                        this.reviewdata = data;
                        this.cbpSummTaxAmount = this.reviewdata.cbpSummaryTaxAmount;
                        this.cbpDetailTaxAmount = this.reviewdata.cbpDetailTaxAmount;
                        this.cbpDetailVolume = this.reviewdata.cbpDetailVolume;
                        this.tufaTaxRate = this.reviewdata.tufaTaxRate;
                        this.fileTypeSelected = this.reviewdata.filetype;
                        this.onFileTypeSelChanged();
                    }
                }
            );

        this.subscriptionArry.push(this.summDetailsDataBusy);

    }

    private isCmpdIngestTT() {
        return this.popupRptData && this.actionType === "Market Share Ready" && this.isCompoundTobbaccoTypeDelta && this.popupRptData.dataType == 'INGESTED';
    }

    public hasCmpdIngestTTVol() {

        return this.isCmpdIngestTT() && this.compoundTTTaxesVolume.length > 0;

    }

    public hasNoCmpdIngestTTVol() {
        return this.isCmpdIngestTT() && this.compoundTTTaxesVolume.length == 0;
    }

    public hasNoVolAmmendTax() {
        return !((this.amendedTax1 != undefined && this.amendedTax1.toString() != "" && this.amendedTax1 >= 0)
            || (this.amendedTax2 != undefined && this.amendedTax2.toString() != "" && this.amendedTax2 >= 0));
    }

    public hasVolAmmendTax() {
        if (this.compoundTTTaxesVolume.length == 2)
            return !((this.compoundTTTaxesVolume[0].amendedTax != undefined && this.compoundTTTaxesVolume[0].amendedTax.toString() != "" && this.compoundTTTaxesVolume[0].amendedTax >= 0)
                || (this.compoundTTTaxesVolume[1].amendedTax != undefined && this.compoundTTTaxesVolume[1].amendedTax.toString() != "" && this.compoundTTTaxesVolume[1].amendedTax >= 0));
        else if (this.compoundTTTaxesVolume.length == 1)
            return !((this.compoundTTTaxesVolume[0].amendedTax != undefined && this.compoundTTTaxesVolume[0].amendedTax.toString() != "" && this.compoundTTTaxesVolume[0].amendedTax >= 0));

    }

    public hasNoVolumnData() {
        if (this.compoundTTTaxesVolume.length > 0 && this.compoundTTTaxesVolume[0].pounds == null && this.compoundTTTaxesVolume[1].pounds == null)
            return true;
        return false;
    }

    private getAffectedDeltas() {

        //if original ein is present 
        let ein = this.popupRptData.originalEIN ? this.popupRptData.originalEIN : this.popupRptData.ein;

        if (this.affectedDeltaCntBusy) {
            this.affectedDeltaCntBusy.unsubscribe();
        }

        this.affectedDeltaCntBusy = this.atcomparisonservice.getAffectedDeltasForEin(this.actionType, ein, this.popupRptData.fiscalYr).subscribe(
            data => {
                if (data.length > 0) {
                    this.affectedTTDeltasGrid = data;
                    this.affectedDeltaCnt = this.affectedTTDeltasGrid.length;
                    this.affectedTTDeltasGrid.forEach(delta => {
                        delta.affectDeltaFlag = true;
                    });
                }
            }
        );

        this.subscriptionArry.push(this.affectedDeltaCntBusy);
    }

    private getAllDeltaStatus(rptStatus) {
        let status = rptStatus.allDeltaStatus;
        if (status === 'MSReady')
            return 'MS Ready';
        else if (status === 'Excluded X')
            return 'Excluded';
        else if (status === 'In Progress X')
            return 'In Progress';
        return status;
    }

    public roundDecimal(value, fractionSize = 2): number {
        let val = (value) ? value : 0;
        return Number(Math.round(Number(val + 'e' + fractionSize)) + 'e-' + fractionSize);
    }

    toCompany(einNum) {
        this.navigateAway = true;
        this.shareData.breadcrumb = "at-compare-selMethod";
        this.shareData.qatabfrom = "at-compare";
        this.router.navigate(["/app/company/details", { ein: einNum }]);
    }

    getMappedActionFromStatus(status: string): string {
        switch (status) {
            case 'Associated': {
                return 'Association';
            }
            case 'MS Ready':
            case 'MSReady': {
                return 'Market Share Ready';
            }
            case 'Excluded':
            case 'Excluded X': {
                return 'Exclude';
            }
            default: {
                break;
            }
        }
    }

    exportRawdataforManu() {
        let ein: string = this.ein;
        let includeOriginal: boolean = false;
        if (this.originalEIN) {
            ein = this.originalEIN;
            includeOriginal = true;
        }

        let tobaccoClassName = this.popupRptData.tobaccoClass.replace(/ /g, "-").replace(/\//g, " ");
        this.exportbusy = this.atcomparisonservice.getDetailRawExport(this.fiscalYear, this.quarter, tobaccoClassName, ein, this.permitType, includeOriginal).subscribe(
            data => {
                this._downloadService.download(data.csv, data.fileName, 'csv/text');
            }
        );
    }

    exportRawdataforImp() {
        let ein: string = this.ein;
        let type = 'summary';
        let includeOriginal: boolean = false;

        if (this.ingestionContext === 'detailOnly')
            type = 'details';

        if (this.originalEIN) {
            ein = this.originalEIN;
            includeOriginal = true;
        }

        let tobaccoClassName = this.popupRptData.tobaccoClass.replace(/ /g, "-").replace(/\//g, " ");
        this.exportbusy = this.atcomparisonservice.getIngestedSummaryExport(type, this.companyId, ein, this.fiscalYear, this.quarter, this.tobaccoClass, this.ingestionContext).subscribe(
            data => {
                this._downloadService.download(data.csv, data.fileName, 'csv/text');
            }
        );
    }

    isPopUpdata(data: TrueUpComparisonResult): boolean {
        return ((this.popupRptData.ein == data.ein
            && this.popupRptData.originalEIN == data.originalEIN) || (this.popupRptData.ein == data.originalEIN))
            && this.popupRptData.quarter == data.quarter
            && this.popupRptData.tobaccoClass == data.tobaccoClass
            && (this.popupRptData.permitType.toUpperCase().includes(["MANU", "Manufacturer"].indexOf(data.permitType) >= 0 ? "MANUFACTURER" : "IMPORTER"));

    }

    affectdDeltasContainsPopUpData(): boolean {
        for (var data of this.affectedTTDeltasGrid) {
            if (this.isPopUpdata(data)) return true;
        }
        return false;
    }

    getAffectedDeltaCnt() {
        this.affectedDeltaCnt = 0;
        this.affectedTTDeltasGrid.forEach(delta => {
            if (delta.affectDeltaFlag) this.affectedDeltaCnt++;
        });
        return this.affectedDeltaCnt;
    }

    getExportData() {
        if (this.busyGen) {
            this.busyGen.unsubscribe();
        }

        this.busyGen = this.atcomparisonservice.getExportData(this.fiscalYear, this.csc, this.pgAttrs, this.csc.grid).subscribe(
            data => {
                this._downloadService.download(data.csv, data.fileName, 'csv/text');
            }
        );
    }

    exportIngestedData(fileType: string) {
        if (fileType == 'summary')
            this.downloadLoading = true;
        else
            this.downloadDetailsLoading = true;

        let ingestionType = 'detailOnly';
        if (this.ingestionContext == "summdetail")
            ingestionType = 'double';


        var ein = this.originalEIN ? this.originalEIN : this.ein;
        var companyId = this.originalEIN ? 0 : this.companyId;

        this.atService.getIngestedSummaryExport(
            fileType, companyId, ein, this.fiscalYear, this.quarter, this.tobaccoClass, ingestionType).pipe(
                finalize(() => {
                    if (fileType == 'summary')
                        this.downloadLoading = false;
                    else
                        this.downloadDetailsLoading = false;
                })
            ).subscribe(
                data => {
                    if (data) {
                        this.IngestedSummaryDeltas = data;
                        this._downloadService.download(this.IngestedSummaryDeltas.csv, this.IngestedSummaryDeltas.fileName, 'csv/text');
                        console.log('Successful Download: CBP Summary');
                    }
                },
                (error) => {
                    throw (error);
                }
            );
    }

    //Association form  logic
    checkEIN() {

        this.hasError = false;

        //Determine if the service is already being called to prevent error
        if (this.einCheckbusy == null || this.einCheckbusy.closed) {
            this.thinking = false;
        } else {
            this.thinking = true;
        }

        if (this.associationCompany.einNumber.length === 9 && !this.thinking) {
            this.einCheckbusy = this._companyService.getActiveImptCompanyByEIN(this.associationCompany.einNumber).subscribe(
                data => {
                    if (data && data.einNumber) {
                        this.associationCompany = data;
                    } else {
                        this.associationCompany.legalName = null;
                    }
                    this.einIsChecked = true;
                    this.hasError = !this.isValidString(this.associationCompany.legalName);
                });

        } else {
            this.einIsChecked = false;
        }
    }

    public trackByIndex(index, item) {
        if (!item) return null;
        return index;
    }
    public processScheduleAB(){
        if(this.busyRun){
            this.busyRun.unsubscribe();
        }
        this.busyRun = this.atcomparisonservice.processScheduleAB(this.fiscalYear).subscribe(
            data => {
                this.getAnnualComparisonResults();
            }
        );
    }
}
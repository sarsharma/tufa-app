/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ShareDataService } from '../../../../core/sharedata.service';
import { Document } from '../../../annual/models/at-cmpdetails.document.model';
import { DownloadSerivce } from '../../../../shared/service/download.service';
import { AnnualTrueUpComparisonService } from "../../../annual/services/at-compare.service";
import { AnnualTrueUpComparisonEventService } from '../../services/at-compare.event.service';

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class EditDocumentCommentsPopup
 */
@Component({
  selector: '[cmpdetail-edit-document-comments]',
  templateUrl: './cmpdetail-edit-document-comments.template.html',
  styleUrls: ['./cmpdetail-edit-document-comments.style.scss'],
 
})

export class CmpDetailEditDocumentCommentsPopup implements OnInit, OnDestroy {
  data: any[] = [];
 
  @Input() commentAttachmentInput:any;
  @Input() context:string;
  //@Output() documentCount: EventEmitter<number> = new EventEmitter<number>();
  @Output() onDocumentClose: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onDocumentEdit = new EventEmitter<boolean>();
  doc = new Document();
  filesToUpload: Array<File> = [];
  shareData: ShareDataService;
  downloading: Subscription;
  periodId: any;
  docFileName: string;
  busy: Subscription;
  edit: Subscription;
  types: string[] = [];
  selFormTypes: string[];
  btnDisable: Boolean = false;
  selectAllFlag: Boolean = false;
  warningFlag: Boolean = false;
  isExtensionNotPDF: Boolean = false;
  commentDocData: any[] = [];
  showConfirmDeletePopup: Boolean = false;
  @ViewChild('editattachmentformcmp') documentUplaod: NgForm;
  comment:any;
  cbpCommentSeq:number=0;
  ttbCommentSeq:number=0;
  allDeltaCommentSeq:number=0;
  public sortBy: string = "createdDt";
  public sortOrder: string[] = ["desc"];


  ngOnInit() { 
    this.commentDocData=[];
    this.isExtensionNotPDF = false;
    
  }

  constructor(
   // private exludeService: PermitExcludeService, 
    private _shareData: ShareDataService,
    private downloadService: DownloadSerivce,
    private annualTrueUpComparisonService: AnnualTrueUpComparisonService,
    private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService) {
    this.shareData = _shareData;
    this.warningFlag = false;
    this.isExtensionNotPDF = false;
    
    
  }
  
  loadDocs(commentSeq: number, ttbCommentSeq: number, allDeltaCommentSeq: number) {
    this.commentDocData = [];
    this.busy = this.annualTrueUpComparisonService.loadPermitDocs(commentSeq,ttbCommentSeq,allDeltaCommentSeq).subscribe(
      data => {
        if (data) {
          let nd = data;
          let newData = [];
          for (let i = 0; i < nd.length; i++) {
            newData.push(nd[i]);
          }
          this.commentDocData = newData;
         // this.documentCount.emit(this.commentDocData.length);
        }

      });
  }
  ngOnDestroy() {
    if (this.busy) {
      this.busy.unsubscribe();
    }

    if (this.edit) {
      this.edit.unsubscribe();
    }
    if (this.downloading) {
      this.downloading.unsubscribe();
    }
  }

 

  fileChangeEvent(event) {
    this.filesToUpload = <Array<File>>event.target.files;
    if (this.filesToUpload[0]) {
      this.docFileName = this.filesToUpload[0].name;
      if(this.docFileName!=''){
        if (typeof jQuery('#editattachmentformcmp').parsley() != "undefined") {
          jQuery('#editattachmentformcmp').parsley().reset();
        }
      }
      let fileExt = this.docFileName.split('.').pop();
      if (fileExt.toUpperCase() !== "PDF") {
        this.isExtensionNotPDF = true;
      } else {
        this.isExtensionNotPDF = false;
      }

    } else {
      this.docFileName = "";
      this.warningFlag = false;
      this.isExtensionNotPDF = false;
    }

  }

  uploadAttachment() {
    jQuery('#editattachmentformcmp').parsley().validate();
    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#editattachmentformcmp').parsley().isValid()) 
    //&& !this.isExtensionNotPDF) 
    {
      this.btnDisable = true;
      if (this.filesToUpload.length > 0) {
        let formData: FormData = new FormData();
        formData.append('file', this.filesToUpload[0]);

        let document = new Document();
        document.docDesc = this.docFileName;
        document.cbpCommentSeq = this.cbpCommentSeq;
        document.ttbCommentSeq = this.ttbCommentSeq;
        document.allDeltaCommentSeq = this.allDeltaCommentSeq;
        document.docFileNm = "ATCH";
        document.docStatusCd = "INCP";
       

        formData.append('metaData',JSON.stringify(document));
        this.edit=this.uploadImpl(formData);
        
      } else {
        let document = new Document();
        document.documentId = this.doc.documentId;
        document.docDesc = this.docFileName;
      }
    }
  }

  uploadImpl(document) {
    return this.annualTrueUpComparisonService.uploadPermitAttachment(document).subscribe(data => {
    if (data) {
        if(this.context === 'ttbdetail'){
          this.annualTrueUpComparisonEventService.triggerAtTTBCompareCommentsEdit();
        }else{
          this.annualTrueUpComparisonEventService.triggerAtCBPCompareCommentsEdit();
        }
        this.reset();
        this.loadDocs(this.cbpCommentSeq,this.ttbCommentSeq,this.allDeltaCommentSeq);
        this.btnDisable = false;
      }
    });
  }

  

  reset() {
    jQuery('.fileinput').fileinput('clear');
    if (typeof jQuery('#editattachmentformcmp').parsley() != "undefined") {
      jQuery('#editattachmentformcmp').parsley().reset();
    }
    this.documentUplaod.resetForm();
    this.documentUplaod.reset();
    

  }

  cancel() {
    this.sortBy = "createdDt";
    this.sortOrder =  ["desc"];
    this.reset();
    jQuery('#cmpdetail-edit-document-comments').modal('hide');
  }
  
  /*
    ngOnChanges() used to capture changes on the input properties.
  */
  ngOnChanges(changes: SimpleChanges) {
    
      for (let propName in changes) {
        if (propName === "commentAttachmentInput") {
          this.cbpCommentSeq = 0;
          this.ttbCommentSeq = 0;
          this.allDeltaCommentSeq = 0;
           let currentValue = changes[propName].currentValue;
                if(currentValue){
                if(currentValue.cbpAmendmentId && currentValue.cbpAmendmentId >0 ){
                    this.cbpCommentSeq = currentValue.commentSeq;
                   // this.comment = new CmpDetailUserComment();
                }else if(currentValue.ttbAmendmentId && currentValue.ttbAmendmentId >0 ){
                  this.ttbCommentSeq = currentValue.commentSeq;
                 // this.comment = new TTBDetailUserComment();
                }else{
                  this.allDeltaCommentSeq = currentValue.commentSeq;
                  if(this.context === "ttbdetail"){
                    this.ttbCommentSeq = currentValue.matchCommSeq?currentValue.matchCommSeq:0;
                  }else if(this.context === "cbpdetail") {
                    this.cbpCommentSeq = currentValue.matchCommSeq?currentValue.matchCommSeq:0;
                  }
                   // this.comment = new allDeltaCommentModel();
                }   
                if(this.cbpCommentSeq > 0 || this.ttbCommentSeq || 0 || this.allDeltaCommentSeq > 0){
                  this.loadDocs(this.cbpCommentSeq, this.ttbCommentSeq, this.allDeltaCommentSeq);
                }
              }
        }
    } 
  }
 
  downloadAttach(doc: Document) {
    this.downloading = this.annualTrueUpComparisonService.downloadPermitAttachment(doc.documentId).subscribe(
        data => {
           if (data) {
               this.downloadService.download(atob(data.reportPdf), data.docDesc, "application/pdf");
         }
        });
 }


 deleteDocument(doc:Document){
  this.showConfirmDeletePopup = true;
  this.btnDisable = true;
  this.busy = this.annualTrueUpComparisonService.deletePermitAttachment(doc.documentId).subscribe(
    data => {
       if (data) {
        if(this.context === 'ttbdetail'){
          this.annualTrueUpComparisonEventService.triggerAtTTBCompareCommentsEdit();
        }else{
          this.annualTrueUpComparisonEventService.triggerAtCBPCompareCommentsEdit();
        }
        this.btnDisable = false;
     }
     
     this.loadDocs(this.cbpCommentSeq,this.ttbCommentSeq, this.allDeltaCommentSeq);
    });
 }

 enableLinkAuth(flag) {
  this.annualTrueUpComparisonService.setLinkAuthentication(flag);
}
 

}

import { Component, Input, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core';
import 'bootstrap-multiselect/dist/css/bootstrap-multiselect.css';
import 'bootstrap-multiselect/dist/js/bootstrap-multiselect.js';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { AuthService } from "../../../../login/services/auth.service";
import { CBPAmendment } from "../../../annual/models/at-cbpamendment.model";
import { allDeltaCommentModel } from "../../../annual/models/at-comment.model";
import { TTBAmendment } from "../../../annual/models/at-ttbamendment.model";
import { AnnualTrueUpComparisonEventService } from "../../../annual/services/at-compare.event.service";
import { AnnualTrueUpComparisonService } from "../../../annual/services/at-compare.service";
import { AnyAaaaRecord } from 'dns';


declare var jQuery: any;

@Component({
    selector: '[cmpdetail-comments-grid]',
    templateUrl: './cmpdetail.comments.grid.template.html',
    styleUrls: ['cmpdetail.comments.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CmpDetailCommentsGridComponent implements OnInit {

    @Input() headerdata;
    @Input() quarter;
    @Input() acceptanceFlag;
    @Input() context;
    @Input() commentModel;
    @Input() ingestionContext;
   // @Input() oneSidedAcceptanceFlag: string;

    compareData = [];

    public sortResultsBy = "commentDate";
    public sortResultsOrder = "desc";

    showCommentModal: boolean = false;
    commentUpdated: any;
    whenEditClicked: boolean = false;
    CIGAR_QUARTER = 5;
    updatedComment: string;

    cigarQtrsToSelect = [];

    excludedQtrs = [];

    ttIdxMp = { "Cigars": 0, "Cigarettes": 0, "Chew-Snuff": 0, "Chew": 1, "Snuff": 2, "Pipe-Roll Your Own": 0, "Pipe": 1, "Roll-Your-Own": 2 }

    ttAliasMapping = { "Roll Your Own": "Roll-Your-Own" }

    data: any[] = [];
    alldeltaComment: any[] = [];
    allDeltaCommentArray: allDeltaCommentModel[] = [];
    commentsMergedArray:any[] = [];
    // key: prperty name , value : list of filter values
    filterData: Map<string, string[]> = new Map();
    commtQtrMp :Map<string, any>;

    commentAction: string = 'add';
    commentEdit;
    
    busy: Subscription;
    subscriptions: Subscription[] = [];
    ein: string = '';
    showDocumentUploadPopup: boolean;
    commentAttachment: any;

    constructor(
        private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
        private annualTrueUpComparisonService: AnnualTrueUpComparisonService,
        public authService: AuthService) {
        this.data = [];

        //Initialize filterData with column names used in the data grid. In this case we just have filter on quarter.
        this.filterData.set("qtr", []);

        this.compareData[0] = (this.context == 'cbpdetail') ? this.newCBPAmndArray() : this.newTTBAmndArray();
        this.compareData[1] = (this.context == 'cbpdetail') ? this.newCBPAmndArray() : this.newTTBAmndArray();;
        this.compareData[2] = (this.context == 'cbpdetail') ? this.newCBPAmndArray() : this.newTTBAmndArray();;
        this.compareData[3] = (this.context == 'cbpdetail') ? this.newCBPAmndArray() : this.newTTBAmndArray();;
        this.compareData[4] = (this.context == 'cbpdetail') ? this.newCBPAmndArray() : this.newTTBAmndArray();;

        let cbpCommentSubscription = this.annualTrueUpComparisonEventService.atCBPCompareCommentsSaveSource$.subscribe(
            data => {
                this.getCBPComments();
            }
        )


        let ttbCommentSubscription = this.annualTrueUpComparisonEventService.atCompareCommentsSaveSource$.subscribe(
            data => {
                this.getTTBComments();
            }
        )
        let cbpCommenteditSubscription = this.annualTrueUpComparisonEventService.atCBPCompareCommentsEditSource$.subscribe(
            data => {
                this.getCBPComments();
            }
        )
        let ttbCommenteditSubscription = this.annualTrueUpComparisonEventService.atTTBCompareCommentsEditSource$.subscribe(
            data => {
                this.getTTBComments();
            }
        )


        /*let ttbCommentEditSubscription = this.annualTrueUpComparisonEventService.atCompareCommentsEditSource$.subscribe(
            data => {
                this.getTTBComments();
            }
        )*/

        let cbpDltCommentSubscription = this.annualTrueUpComparisonEventService.atCBPCompareCommentsDeleteSource$.subscribe(
            data => {
                this.getCBPComments();
            }
        )

        let ttbDltCommentSubscription = this.annualTrueUpComparisonEventService.atTTBCompareCommentsDeleteSource$.subscribe(
            data => {
                this.getTTBComments();
            }
        );

        let tobaccoSelection = this.annualTrueUpComparisonEventService.tobaccoClassSelected$.subscribe(
            data => {
                if (this.context == 'cbpdetail')
                         this.getCBPComments();
                else if (this.context == 'ttbdetail')
                        this.getTTBComments();
            }
        );
       


        this.subscriptions.push(cbpCommentSubscription);
        this.subscriptions.push(ttbCommentSubscription);
        this.subscriptions.push(cbpDltCommentSubscription);
        this.subscriptions.push(ttbDltCommentSubscription);
        this.subscriptions.push(tobaccoSelection);
        this.subscriptions.push(ttbCommenteditSubscription);
        this.subscriptions.push(cbpCommenteditSubscription);
        this.commentAttachment = null;

    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "commentModel") {
                let chg = changes[propName];
                if (chg.currentValue)
                    this.data = chg.currentValue;
                else this.data = null;
            }
        }
    }

    ngOnInit(): void {
        if (this.context == 'cbpdetail')
            this.getCBPComments();
        else if (this.context == 'ttbdetail')
            this.getTTBComments();
        
    }


    ngAfterViewInit(): void {
        // this.initMultiselectFilter();
    }

    ngOnDestroy() {
        // if (this.busy) {
        //     this.busy.unsubscribe();
        // }
        for (let i in this.subscriptions)
            this.subscriptions[i].unsubscribe();
    }

    private getAllDeltasComments(){        
        let tobaccoClassName = this.headerdata.tobaccoClass.replace(/\//g, "-");
         //enabling all the quarters for all deltas
        this.excludedQtrs = [];
        this.alldeltaComment = [];
        this.allDeltaCommentArray = [];
        let permitType ="";
        if (this.context == 'cbpdetail'){
            permitType = 'Importer';
        }
        else if (this.context == 'ttbdetail'){
            permitType = 'MANU';
        }
        if (this.headerdata.originalEIN) {
                this.ein = this.headerdata.originalEIN;
        } else {
                this.ein = this.headerdata.ein
        }
    
        this.annualTrueUpComparisonService.getAlldeltasComments(this.headerdata.fiscalYear, this.ein,  tobaccoClassName, '0', permitType).subscribe(
            data => {
                let comments = [];
                if (data) {
                  this.alldeltaComment = <any>data;
                   for(let i=0 ;i < this.alldeltaComment.length; i++){
                       for(let j=0;j < this.alldeltaComment[i].userComments.length; j++){
                        let commentModel = this.alldeltaComment[i].userComments[j];
                        commentModel.cmpAllDeltaId = this.alldeltaComment[i].cmpAllDeltaId;
                        let alldeltaseq = commentModel.matchCommSeq?commentModel.matchCommSeq:"ad_"+commentModel.commentSeq
                        this.commentsMergedArray.push(commentModel);
                        if (this.headerdata.tobaccoClass === 'Cigars') {
                            this.commtQtrMp.set(String(alldeltaseq) + 5, commentModel);
                        }
                        else if (this.quarter == 0) {
                           if(!commentModel.commentQtrs){
                            let commentModelInMap = this.commtQtrMp.get(String(alldeltaseq));
                            let commentQtrDisplay = "";
                            if(commentModelInMap && commentModelInMap.commentQtrs){
                                commentQtrDisplay = commentModelInMap.commentQtrs.concat(';').concat(this.alldeltaComment[i].fiscalQtr);
                                commentModel.commentQtrs = commentQtrDisplay;
                            }else if(!commentModel.commentQtrs){
                                commentModel.commentQtrs = this.alldeltaComment[i].fiscalQtr;
                            }
                           }
                            this.commtQtrMp.set(String(alldeltaseq), commentModel);
                        }
                       }
                     }
                    
                }
                this.commtQtrMp.forEach(function (value, key, map) {
                    comments.push(value);
                });
                comments = _.orderBy(comments, 'commentDate', 'desc');
                this.updateCommentGrid(comments);
             }
            , (error) => {
                throw (error);
            });
        
        }

    private getCBPComments() {
        //this.getAllDeltasComments();
        this.data = [];
        this.commentsMergedArray = [];
        let amdmnts: CBPAmendment[] = this.getCBPAmendmentDetails();
        this.busy = this.annualTrueUpComparisonService.getCBPAmendments(Number(this.headerdata.fiscalYear), Number(this.headerdata.companyId), amdmnts).subscribe(
            data => {
               let comments = [];
                if (data) {
                    this.commtQtrMp = new Map<string, any>();
                    let cigarQtrSet = new Set();
                    for (let i in data) {
                        if (null != this.excludedQtrs && this.excludedQtrs.length == 0) {
                            this.excludedQtrs = data[i].excludedPermits;
                        }
                        if (data[i].amendmentPresent && null != data[i].userComments && data[i].userComments.length > 0) {
                            for (let c in data[i].userComments) {
                                let cmm = data[i].userComments[c];
                                cmm.cbpAmendmentId = data[i].cbpAmendmentId;
                                let qtr = (this.headerdata.tobaccoClass === 'Cigars') ? cmm.qtr : data[i].qtr;
                                if (this.headerdata.tobaccoClass === 'Cigars' || this.quarter == qtr) {
                                    cmm.qtr = qtr;
                                    this.commtQtrMp.set(String(cmm.commentSeq) + qtr, cmm);
                                }
                                else if (this.quarter == 0) {
                                    this.commtQtrMp.set(String(cmm.commentSeq), cmm);
                                }
                                // if (this.headerdata.tobaccoClass === 'Cigars')
                                //     cigarQtrSet.add(qtr);
                            }
                        }

                    }
                    if(this.ingestionContext == 'detailOnly'){
                        this.getAllDeltasComments();
                    }else{
                        this.commtQtrMp.forEach(function (value, key, map) {
                            comments.push(value);
                        });
                        comments = _.orderBy(comments, 'commentDate', 'desc');
                        this.updateCommentGrid(comments);
                    }
                    
                    // this.defaultQtrsOnMultiSelect(cigarQtrSet);
                }
            });

        this.subscriptions.push(this.busy);

    }

    // Might be best to move this into a pipe
    public getCommentQuarterDisplay(quarters: string): string {
        let commQtrDisplay = "";
        commQtrDisplay =  quarters ? quarters.replace(/;/gi, ',') : quarters;
        return commQtrDisplay;
    }


    private getTTBComments() {

        let amdmnts: TTBAmendment[] = this.getTTBAmendmentDetails();

        this.busy = this.annualTrueUpComparisonService.getTTBAmendments(Number(this.headerdata.fiscalYear), Number(this.headerdata.companyId), amdmnts).subscribe(
            data => {
                let comments = [];
                if (data) {
                    this.commtQtrMp = new Map<string, any>();
                    let cigarQtrSet = new Set();

                    for (let i in data) {
                        if (this.excludedQtrs.length == 0) {
                            this.excludedQtrs = data[i].excludedPermits;
                        }

                        if (data[i].amendmentPresent && null != data[i].userComments && data[i].userComments.length > 0) {
                            for (let c in data[i].userComments) {
                                // let cmm = data[i].userComments[c];
                                // let qtr = (this.headerdata.tobaccoClass === 'Cigars') ? cmm.qtr : data[i].qtr;
                                // cmm.qtr = qtr;
                                // if (this.headerdata.tobaccoClass === 'Cigars' || this.quarter == qtr)
                                //     commtQtrMp.set(String(cmm.commentSeq) + qtr, cmm);

                                let cmm = data[i].userComments[c];
                                cmm.ttbAmendmentId = data[i].ttbAmendmentId;
                                let qtr = (this.headerdata.tobaccoClass === 'Cigars') ? cmm.qtr : data[i].qtr;
                                if (this.headerdata.tobaccoClass === 'Cigars' || this.quarter == qtr) {
                                    cmm.qtr = qtr;
                                    this.commtQtrMp.set(String(cmm.commentSeq) + qtr, cmm);
                                }
                                else if (this.quarter == 0) {
                                    this.commtQtrMp.set(String(cmm.commentSeq), cmm);
                                }
                            }
                        }

                    }

                    /*commtQtrMp.forEach(function (value, key, map) {
                        comments.push(value);
                    });

                    this.updateCommentGrid(comments);*/
                    if(this.ingestionContext == 'detailOnly'){
                        this.getAllDeltasComments();
                    }else{
                        this.commtQtrMp.forEach(function (value, key, map) {
                            comments.push(value);
                        });
                        comments = _.orderBy(comments, 'commentDate', 'desc');
                        this.updateCommentGrid(comments);
                    }

                    // this.defaultQtrsOnMultiSelect(cigarQtrSet);

                }
            });

        this.subscriptions.push(this.busy);

    }


    onSaveCommentClick(commentSequence, index) {
        let comment = new allDeltaCommentModel
        comment.userComment = jQuery("#txtarea-" + index).val().trim();
        comment.resolveComment = false;
        comment.commentSeq = commentSequence;

        this.updateComment(comment, index);
    }

    onResolveCommentChk(commentobj: allDeltaCommentModel, commentSequence, index) {

        commentobj.resolveComment = index.checked;
        let comment = new allDeltaCommentModel
        comment.resolveComment = true;
        comment.commentSeq = commentSequence;
        this.updateComment(comment, index);
    }

    showTextArea(index) {
        jQuery('#com-' + index).hide();
        jQuery("#txtarea-" + index).val().trim();
        jQuery('#txtarea-' + index).show();
    }

    updateComment(commentUpdated, index) {

        this.busy = this.annualTrueUpComparisonService.updateComment(commentUpdated, commentUpdated.commentSeq).subscribe(
            data => {

            }, (error) => {
                console.log(error);

            });

        if (!commentUpdated.resolveComment) {
            jQuery('#txtarea-' + index).hide();
            jQuery("#com-" + index).text(commentUpdated.userComment);
            jQuery('#com-' + index).show();
        }
    }

    canEditComment(commentUser) {
        let sessionUser = this.authService.getSessionUser();
       if (commentUser != sessionUser)
            return false; 
        return true;
    }

    onAddCommentClick() {
        this.commentAction = 'add';
        this.commentEdit = null;
        this.showCommentModal = true;
    }

    onEditCommentClick(comment) {
        this.commentAction = 'edit';
        this.commentEdit = comment;
        this.showCommentModal = true;
    }

    updateCommentGrid(comments) {
        if (comments) {
            let nData = [];
            for (let i in comments) {
                nData.push(comments[i]);
            }
            this.data = nData;
            // this.totalComments = this.data.length;
        }
    }

    onCommentDeleted() {
        if (this.context == 'ttbdetail')
            this.getTTBComments();
        else if (this.context == 'cbpdetail')
            this.getCBPComments();
    }

    private newCBPAmndArray() {

        let amndForTTArry: CBPAmendment[] = [];

        amndForTTArry[0] = new CBPAmendment();
        amndForTTArry[1] = new CBPAmendment();
        amndForTTArry[2] = new CBPAmendment();
        return amndForTTArry
    }

    private newTTBAmndArray() {

        let amndForTTArry: TTBAmendment[] = [];

        amndForTTArry[0] = new TTBAmendment();
        amndForTTArry[1] = new TTBAmendment();
        amndForTTArry[2] = new TTBAmendment();
        return amndForTTArry
    }


    private getCBPAmendmentDetails(): CBPAmendment[] {
        let amendmnts: CBPAmendment[] = [];
        amendmnts.push(this.getCBPAmend());

        return amendmnts;
    }


    private getCBPAmend(qtr?): CBPAmendment {

        let amd: CBPAmendment = new CBPAmendment();
        if (this.headerdata.tobaccoClass === 'Cigars')
            amd.qtr = this.CIGAR_QUARTER;
        else
            amd.inclAllQtrs = true;

        amd.fiscalYr = Number(this.headerdata.fiscalYear);
        amd.cbpCompanyId = this.headerdata.companyId;
        if (this.ttAliasMapping[this.headerdata.tobaccoClass])
            amd.tobaccoType = this.ttAliasMapping[this.headerdata.tobaccoClass];
        else
            amd.tobaccoType = this.headerdata.tobaccoClass;

        return amd;
    }


    private getTTBAmendmentDetails(): TTBAmendment[] {
        let amendmnts: TTBAmendment[] = [];
        amendmnts = _.concat(amendmnts, this.getTTBAmend());

        return amendmnts;
    }

    private getTTBAmend(qtr?): TTBAmendment[] {
        let amdmts: TTBAmendment[] = [];

        let amd: TTBAmendment = new TTBAmendment();
        if (this.headerdata.tobaccoClass === 'Cigars')
            amd.qtr = this.CIGAR_QUARTER;
        else
            amd.inclAllQtrs = true;

        amd.fiscalYr = Number(this.headerdata.fiscalYear);
        amd.ttbCompanyId = this.headerdata.companyId;

        if (this.headerdata.tobaccoClass.indexOf("Chew") != -1) {
            let amd1: TTBAmendment = new TTBAmendment();
            jQuery.extend(true, amd1, amd);
            amd1.tobaccoType = "Chew";
            amdmts.push(amd1);

            let amd2: TTBAmendment = new TTBAmendment();
            jQuery.extend(true, amd2, amd);
            amd2.tobaccoType = "Snuff";
            amdmts.push(amd2);
        }

        else if (this.headerdata.tobaccoClass.indexOf("Pipe") != -1) {
            let amd1: TTBAmendment = new TTBAmendment();
            jQuery.extend(true, amd1, amd);
            amd1.tobaccoType = "Pipe";
            amdmts.push(amd1);

            let amd2: TTBAmendment = new TTBAmendment();
            jQuery.extend(true, amd2, amd);
            amd2.tobaccoType = "Roll-Your-Own";
            amdmts.push(amd2);
        }
        else {
            amd.tobaccoType = this.headerdata.tobaccoClass;
            amdmts.push(amd);
        }

        return amdmts;
    }
    openPopup(comment: any) {
        this.commentAttachment = comment;
        this.showDocumentUploadPopup = true;
    }
         

    hasDocuments(comment: any){
        let hasDocs = false;
        if(comment.attachmentsCount > 0){
            hasDocs = true;
        }
        return hasDocs;
    }


   
}

/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, Input, OnInit, OnDestroy, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Company } from '../../../../company/models/company.model';
import { CompanyService } from '../../../../company/services/company.service';
import { AddressService } from '../../../../company/services/address.service';
import { ContactService } from '../../../../company/services/contact.service';
import { AnnualTrueUpComparisonEventService } from "./../../services/at-compare.event.service";

declare var jQuery: any;

@Component({
  selector: '[at-company-panel]',
  templateUrl: './at-company-panel.template.html',
  styles: [String(require('./at-company-panel.style.scss'))],
  providers: [CompanyService, AddressService, ContactService]
})

export class AnnualTrueUpCompareCompanyPanel implements OnInit, OnDestroy {

  compId: any;
  @Input() headerInfo: any;
  showErrorFlag: boolean;
  alerts: Array<Object>;

  countryList: any;
  contacts: any[];
  addresses: any[];
  permitSet: any[];

  priAddress : any;
  alternateAddress : any;
  company: Company;

  constructor(private companyService: CompanyService,private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService) {

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];

    this.showErrorFlag = false;
    
  }

  registerError(error: string){
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({type: 'warning', msg: error});
  }

  ngOnInit(): void {

   
    this.compId = this.headerInfo.companyId;
   // console.log("compId :"+ this.compId);
    if(this.headerInfo.ein){
      this.companyService.getCompanyEIN(this.headerInfo.ein).subscribe(data => {
        if (data) {
          this.company = data;
          //console.log("data.primaryAddress"+ data.primaryAddress);
          this.priAddress = data.primaryAddress;
          this.alternateAddress = data.alternateAddress;
          this.contacts = data.contactSet;
          this.permitSet =  data.permitSet;
          if(this.company.companyId == '0'){
            this.annualTrueUpComparisonEventService.triggerNoCompanyInfo(0);
          }
        }

      },
      error => {
        this.registerError(error);
      });
    }else if (this.compId) {
      this.companyService.getCompany(this.compId).subscribe(data => {
        if (data) {
          this.company = data;
          //console.log("data.primaryAddress"+ data.primaryAddress);
          this.priAddress = data.primaryAddress;
          this.alternateAddress = data.alternateAddress;
          this.contacts = data.contactSet;
          this.permitSet =  data.permitSet;
        }
      },
      error => {
        this.registerError(error);
      });
    }

  }


  ngOnDestroy() {
    // this.sub.unsubscribe();
  }

  /*
   ngOnChanges() used to capture changes on the input properties.
*/
  ngOnChanges(changes: SimpleChanges) {

    for (let propName in changes) {
      if (propName === "headerdata") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.headerInfo = chg.currentValue;
        }
      }
    }
  
  }
}

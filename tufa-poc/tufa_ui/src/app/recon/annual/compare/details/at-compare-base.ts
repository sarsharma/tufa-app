/**Base file for Single File Compare details */

export class ATCompareBase {
    protected getReportStatus(status: string) {
        if (status === "NS") {
            return "NSTD";
        } else if (status === "C") {
            return "COMP";
        } else if (status === "E") {
            return "ERRO";
        }
        return "";
    }
}
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { Subscription } from '../../../../../../node_modules/rxjs';
import { ShareDataService } from '../../../../core/sharedata.service';
import { DownloadSerivce } from '../../../../shared/service/download.service';
import { IngestionWorkFlowService } from '../../../../shared/service/ingestion-workflow.service';
import { AcceptanceFlagState } from '../../models/at-acceptance-flag-state.model';
import { AtReallocationService } from '../../services/at-reallocation.service';
import { AnnualTrueUpCompareDetailsSummary } from "./../../models/at-comparedetailssummary.model";
import { AnnualTrueUpComparisonDetailsHeader } from "./../../models/at-detailsheader.model";
import { ReallocateModel } from "./../../models/at-reallocate.model";
import { AnnualTrueUpComparisonEventService } from "./../../services/at-compare.event.service";
import { AnnualTrueUpComparisonService } from "./../../services/at-compare.service";
import { CompareDetailsQuarterPanel } from './importer/single-file-compare/at-compare-details-quarter-panel.component';

declare var jQuery: any;

@Component({
    selector: '[at-compare-cigardetails]',
    templateUrl: './at-compare-cigardetails.template.html',
    styleUrls: ['./at-compare-cigardetails.style.scss'],
    encapsulation: ViewEncapsulation.None
})


export class AnnualTrueUpCompareCigarDetailsPage implements OnInit, OnDestroy {
    @Output() onReallocated = new EventEmitter<boolean>();
    @ViewChild(CompareDetailsQuarterPanel) qtrpanelComponent: CompareDetailsQuarterPanel;

    router: Router;
    alerts: { type: string; msg: string; }[];
    oneAtATime: boolean = true;
    shareData: ShareDataService;
    headerdata: AnnualTrueUpComparisonDetailsHeader = new AnnualTrueUpComparisonDetailsHeader();
    comparedetailsSummary: AnnualTrueUpCompareDetailsSummary;
    permitType: string;
    fda3852map: any = { 1: "", 2: "", 3: "", 4: "", 5: "" };
    oneSidedFlagMap: any = { 1: "", 2: "", 3: "", 4: "", 5: "" };
    lineiteminfo: ReallocateModel;

    refreshIngest: number = 0;
    acceptanceFlag: string;

    createdDate: string;
    submitted: string;

    downloadBusy: Subscription;
    busy: Subscription;
    isSingleFile: Boolean;
    file: string;
    selectedDt: string;
    private subscriptions: Subscription[] = [];
    isCompanyDisplay: boolean = true;
    dateSwitchBusy: Subscription;
    hasMixedAction: boolean = false;
    exportBusy: Subscription;

    constructor(
        router: Router,
        private atcomparisonservice: AnnualTrueUpComparisonService,
        private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
        private _shareData: ShareDataService,
        private downladService: DownloadSerivce,
        public ingestionWorkflowSvc: IngestionWorkFlowService,
        private toastr: ToastrService,
        private reallocateService: AtReallocationService) {

        this.alerts = [
            {
                type: 'success',
                msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Message</span>'
            }
        ];

        this.router = router;
        this.shareData = _shareData;
        this.shareData.detailsSrc = 'cigar';
        this.shareData.selectedDt = this.selectedDt;
        this.readfromSharedData();

        this.subscriptions.push(this.annualTrueUpComparisonEventService.atCompareSuccessfullSaveAction$.subscribe(data => {
            if (data) {
                let amend = data[0];
                if (amend.inProgressFlag == 'Y') {
                    this.fda3852map[amend.qtr] = AcceptanceFlagState.IN_PROGRESS;
                    this.toastr.success("In Progress Success");
                } else if (amend.acceptanceFlag == 'Y') {
                    this.fda3852map[amend.qtr] = AcceptanceFlagState.ACCEPT_FDA;
                    this.toastr.success("Accept FDA Success");
                } else if (amend.acceptanceFlag == 'I') {
                    this.fda3852map[amend.qtr] = AcceptanceFlagState.ACCEPT_INGESTED;
                    this.toastr.success("Accept Ingested Success");
                } else if (amend.acceptanceFlag == 'N') {
                    this.fda3852map[amend.qtr] = AcceptanceFlagState.AMEND;
                    this.toastr.success("Amend Total Success");
                } else if (amend.acceptanceFlag == 'M') {
                    this.fda3852map[amend.qtr] = AcceptanceFlagState.MIXEDACTION;
                    this.toastr.success("Mixed Action Success");
                }
                this.isMixedAction();
            }
        }));

        this.subscriptions.push(this.annualTrueUpComparisonEventService.atCompareSuccessfullSaveActionOneSided$.subscribe(data => {
            if (data) {
                let action =  data[0]['1-4'];
                for (let i in this.oneSidedFlagMap) {
                    if(i=='5' && data[0]['1-4']!=undefined)
                        this.oneSidedFlagMap[i] = data[0]['1-4'];
                    else
                        this.oneSidedFlagMap[i] = data[0][i];
                        
                    if ((i == '5' ? '1-4' : i) == this.headerdata.compareResult.quarter && this.headerdata.compareResult.allDeltaStatus == 'Associated' && data[0][(i == '5' ? '1-4' : i)] == 'In Progress') {
                        this.headerdata.compareResult.status = 'In Progress'
                        this.headerdata.compareResult.originalEIN = null;
                        this.headerdata.compareResult.originalName = null;
                    }    
                }
                this.toastr.success(action+" Action Success");
            }
        }));

        this.subscriptions.push(this.annualTrueUpComparisonEventService.nocompanyInfo$.subscribe(
            data => {
                if(data == 0){
                    this.isCompanyDisplay = false;
                   }
               
            }
        ));

        let commentManuSaveSubscription = this.annualTrueUpComparisonEventService.flagsOnCommentSaveManu$.subscribe(
            data => {
                this.readfromSharedData();
                this.toastr.success("Comment Saved Successfully");
            }
        )
        let commentImptSaveSubscription = this.annualTrueUpComparisonEventService.flagsOnCommentSaveImpt$.subscribe(
            data => {
                
                this.readfromSharedData();
                this.toastr.success("Comment Saved Successfully");
            }
        )
        this.subscriptions.push(this.annualTrueUpComparisonEventService.atRellocationNotification$.subscribe(
            data => {
                if(data){
                    if(data==='Date Switch') {
                        this.toastr.success("Date Switch Success");
                    } else {
                        this.toastr.success("Manage Action Success");
                    }
                }
               
            }
        ));
        this.subscriptions.push(commentManuSaveSubscription);
        this.subscriptions.push(commentImptSaveSubscription);
    }

    ngOnInit() {
        this.alerts = [];
        this.refreshIngest = 0;
        this.scrollTop();

    }

    ngAfterViewInit() {
        if (this.shareData.permitType == 'Importer')
            this.getSelectedDt();
    }

    isMixedAction(){
        if(this.fda3852map[5] == AcceptanceFlagState.MIXEDACTION)
            this.hasMixedAction = true;
        else
            this.hasMixedAction = false;
    }

    trackChanges(event) {
        this.shareData.selectedDt = this.selectedDt;
        this.dateSwitchBusy = this.atcomparisonservice.updateDateSelected(this.shareData.fiscalYear, this.shareData.tobaccoClass
            , this.shareData.ein, this.shareData.selectedDt).subscribe(data => {
                this.reallocateService.triggerReallocateEntrySaveEvent("Date Switch");
                this.getFDA3852flags(this.shareData.fiscalYear, this.shareData.companyId, this.headerdata.tobaccoClass, this.shareData.permitType === 'Manufacturer' ? "MANU" : "IMP");
                this.getOneSidedFlags(this.shareData.fiscalYear, this.shareData.companyId, this.headerdata.tobaccoClass, this.shareData.permitType === 'Manufacturer' ? "MANU" : "IMP");
            });
            this.subscriptions.push(this.dateSwitchBusy);
    }

    getSelectedDt() {
        this.atcomparisonservice.getSelectedDateType(this.shareData.fiscalYear, this.shareData.tobaccoClass, this.shareData.ein).subscribe(data => {
            if (data) {
                this.selectedDt = data.toString();
                this.shareData.selectedDt = this.selectedDt;
            }
        });

    }

    ngOnDestroy() {
        this.subscriptions.forEach(sub => {
            if (sub) {
                sub.unsubscribe();
            }
        });
    }

    scrollTop() {
        jQuery('html, body, .content-wrap').animate({
            scrollTop: 0
        }, 2000);

    }

    manageEntryLine(lineitem: ReallocateModel, quarter: number) {
        this.lineiteminfo = new ReallocateModel();
        this.lineiteminfo.entryId = lineitem.entryId;
        this.lineiteminfo.date = lineitem.date; // entryDate
        this.lineiteminfo.entryNum = lineitem.entryNum;
        this.lineiteminfo.lineNum = lineitem.lineNum;
        this.lineiteminfo.qty = lineitem.qty;
        this.lineiteminfo.tax = lineitem.tax;
        this.lineiteminfo.month = lineitem.month;
        this.lineiteminfo.year = this.shareData.fiscalYear;
        this.lineiteminfo.quarter = quarter;
        this.lineiteminfo.excludedFlag = lineitem.excludedFlag;
        this.refreshIngest = 0;
    }

    refreshIngestedDetails(flag: boolean) {
        this.refreshIngest = 1;
        this.getFDA3852flags(this.shareData.fiscalYear, this.shareData.companyId, this.shareData.tobaccoClass, this.shareData.permitType === 'Manufacturer' ? "MANU" : "IMP");
        this.getOneSidedFlags(this.shareData.fiscalYear, this.shareData.companyId, this.headerdata.tobaccoClass, this.shareData.permitType === 'Manufacturer' ? "MANU" : "IMP");
    }

    toAnnualTrueUp() {
        this.router.navigate(["/app/assessments/annual/" + this.shareData.fiscalYear]);
    }

    toAssessments() {
        this.router.navigate(["/app/assessments"]);
    }

    readfromSharedData() {
        this.headerdata.compareResult = this.shareData.compareResult;
        this.headerdata.fiscalYear = this.shareData.fiscalYear.toString();
        this.headerdata.tobaccoClass = this.shareData.tobaccoClass;
        this.headerdata.quarter = this.shareData.quarter;
        if (this.shareData.tobaccoClass === "Roll-Your-Own") {
            this.headerdata.tobaccoClass = "Roll Your Own";
        }
        this.headerdata.permitType = this.shareData.permitType;
        this.permitType = this.shareData.permitType;
        this.createdDate = this.shareData.createdDate;
        this.submitted = this.shareData.submitted;
        this.acceptanceFlag = this.shareData.acceptanceFlag;
        this.selectedDt = this.shareData.selectedDt;
        //To be Chnaged after the implementatin of context
        //this.shareData.ingestionContextModel.context = 'detailOnly';  
        if(this.shareData.originalEIN){
            this.headerdata.ein = this.shareData.originalEIN;
            this.headerdata.companyName = this.shareData.originalName;
            this.headerdata.companyId = 0;
        }else{
            this.headerdata.companyId = this.shareData.companyId;
            this.headerdata.ein = this.shareData.ein;
            this.headerdata.companyName = this.shareData.companyName;
            if(this.headerdata.companyId === 0){
                this.isCompanyDisplay = false;
            }
           
        } 
        this.getFDA3852flags(this.shareData.fiscalYear, this.shareData.companyId, this.headerdata.tobaccoClass, this.shareData.permitType === 'Manufacturer' ? "MANU" : "IMP");
        this.getOneSidedFlags(this.shareData.fiscalYear, this.shareData.companyId, this.headerdata.tobaccoClass, this.shareData.permitType === 'Manufacturer' ? "MANU" : "IMP");
    }

    getFDA3852flags(fiscalYr, companyid, classNm, type) {
        this.busy = this.atcomparisonservice.getFDA3852Flag(fiscalYr, companyid, classNm, type, this.headerdata.ein).subscribe(data => {
            if (data) {
                this.fda3852map[5] = '';
                for (var key in data)
                    this.fda3852map[5] = data[key];

                if (!this.fda3852map[5] && data['1-4'] != '' && !data['1-4']) {
                    this.fda3852map[5] = AcceptanceFlagState.DELTA_CHANGE_ZERO;
                } else if(this.fda3852map[5] === 'X' || this.fda3852map[5] === AcceptanceFlagState.DELTA_CHANGE_ZERO){
                    this.fda3852map[5] = AcceptanceFlagState.DELTA_CHANGE_TRUE_ZERO;
                }

                this.isMixedAction();
                this.annualTrueUpComparisonEventService.triggerAtCBPCompare3852FlagsUpdateSource(data);
            }
        });
    }

    getOneSidedFlags(fiscalYr, companyid, classNm, type) {
        this.busy = this.atcomparisonservice.getOneSidedFlag(fiscalYr, companyid, classNm, type, this.headerdata.ein).subscribe(data => {
            if (data) {
                for (let i in this.oneSidedFlagMap) {
                    if(i=='5' && data['1-4']!=undefined)
                        this.oneSidedFlagMap[i] = data['1-4'];
                    else
                        this.oneSidedFlagMap[i] = data[i];
                }
                //this.annualTrueUpComparisonEventService.triggerAtCBPCompare3852FlagsUpdateSource(data);
            }
        });
        this.subscriptions.push(this.busy);
    }

    CBPExportDetail() {

        if (this.downloadBusy) {
            this.downloadBusy.unsubscribe();
        }

        this.downloadBusy = this.atcomparisonservice.exportCBPData(this.shareData.fiscalYear, this.shareData.quarter, this.shareData.tobaccoClass, this.shareData.ein).subscribe(
            data => {
                if (data) {
                    this.downladService.download(data.csv, data.title, 'csv/text');
                }
            });
    }

    public hasAssociationStatus() {
        for (let i in this.oneSidedFlagMap) {
            if (this.oneSidedFlagMap[i] == 'Associated') {
                return true;
            }
        }
        return false;
    }

    public exportRawdata() {


        this.exportBusy = this.atcomparisonservice.getMissingEntryExport("details", this.shareData.companyId, this.shareData.ein, this.shareData.fiscalYear, this.shareData.quarter.toString(), this.shareData.tobaccoClass).subscribe(
          data => {
            if (data) {
              this.downladService.download(data.csv, data.fileName, 'csv/text');
            }
          }
        );
    
        this.subscriptions.push(this.exportBusy);
      }
}

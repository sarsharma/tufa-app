import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { Subscription } from "rxjs";
import { TabsetComponent } from '../../../../../../node_modules/ngx-bootstrap';
import { ShareDataService } from '../../../../core/sharedata.service';
import { DownloadSerivce } from '../../../../shared/service/download.service';
import { IngestionWorkFlowService } from '../../../../shared/service/ingestion-workflow.service';
import { AtReallocationService } from '../../services/at-reallocation.service';
import { AnnualTrueUpComparisonDetailsHeader } from "./../../models//at-detailsheader.model";
import { AcceptanceFlagState } from './../../models/at-acceptance-flag-state.model';
import { AnnualTrueUpCompareDetailsSummary } from "./../../models/at-comparedetailssummary.model";
import { ReallocateModel } from "./../../models/at-reallocate.model";
import { AnnualTrueUpComparisonEventService } from "./../../services/at-compare.event.service";
import { AnnualTrueUpComparisonService } from "./../../services/at-compare.service";
import { CompareDetailsQuarterPanel } from './importer/single-file-compare/at-compare-details-quarter-panel.component';

declare var jQuery: any;

@Component({
    selector: '[at-compare-noncigardetails]',
    templateUrl: './at-compare-noncigardetails.template.html',
    styleUrls: ['at-compare-noncigardetails.style.scss'],
    encapsulation: ViewEncapsulation.None
})


export class AnnualTrueUpCompareNonCigarDetailsPage implements OnInit, OnDestroy {

    @Output() onReallocated = new EventEmitter<boolean>();
    @ViewChild('quarterTabs') quarterTabs: TabsetComponent;
    @ViewChild(CompareDetailsQuarterPanel) qtrpanelComponent: CompareDetailsQuarterPanel;

    router: Router;
    alerts: { type: string; msg: string; }[];
    oneAtATime: boolean = true;
    shareData: ShareDataService;
    headerdata: AnnualTrueUpComparisonDetailsHeader = new AnnualTrueUpComparisonDetailsHeader();
    busy: Subscription;
    downloadBusy: Subscription;
    comparedetailsSummary: AnnualTrueUpCompareDetailsSummary;
    permitType: string;
    lineiteminfo: ReallocateModel;
    refreshIngest: number = 0;
    fda3852map: any = { 1: "", 2: "", 3: "", 4: "", 5: "" };
    oneSidedFlagMap: any = { 1: "", 2: "", 3: "", 4: "", 5: "" };
    tabmap: any = { 1: true, 2: true, 3: true, 4: true, 5: true };
    onesidemap: any = { 1: "", 2: "", 3: "", 4: "", 5: "" };

    isSingleFile: boolean = true;
    createdDate: string;
    submitted: string;
    quarter: number;
    acceptanceFlag: string;
    oneSidedAcceptanceFlag: string;
    selectedDt: string = 'entrySummDt';
    actqtr: number;
    isCompanyDisplay: boolean = true;

    hasMixedAction: boolean = false;
    mixedActionQuarters: any = { 1: "", 2: "", 3: "", 4: "", 5: "" };
    dateSwitchBusy: Subscription;
    private subscriptions: Subscription[] = [];
    quarterTobeFocused:string;
    quarterId:string;
    exportBusy: Subscription;
    constructor(
        router: Router,
        private atcomparisonservice: AnnualTrueUpComparisonService,
        private reallocateService: AtReallocationService,
        private _shareData: ShareDataService,
        private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
        private annualTrueUpComparisonService: AnnualTrueUpComparisonService,
        private downloadService: DownloadSerivce,
        public ingestionWorkflowSvc: IngestionWorkFlowService,
        private toastr: ToastrService) {

        this.alerts = [
            {
                type: 'success',
                msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Message</span>'
            }
        ];

        this.router = router;
        this.shareData = _shareData;
        this.shareData.detailsSrc = 'noncigar';

        this.readfromSharedData();

        this.subscriptions.push(this.annualTrueUpComparisonEventService.tobaccoClassSelected$.subscribe(
            data => {
                this.readfromSharedData();
                if (this.shareData.permitType == 'Importer')
                    this.getSelectedDt();
            }
        ));

        this.subscriptions.push(this.annualTrueUpComparisonEventService.atCompareSuccessfullSaveAction$.subscribe(data => {
            if (data) {
                let amend = data[0];
                if (amend.inProgressFlag == 'Y') {
                    this.fda3852map[amend.qtr] = AcceptanceFlagState.IN_PROGRESS;
                    this.toastr.success("In Progress Success");
                } else if (amend.acceptanceFlag == 'Y') {
                    this.fda3852map[amend.qtr] = AcceptanceFlagState.ACCEPT_FDA;
                    this.toastr.success('Accept FDA Success');
                } else if (amend.acceptanceFlag == 'I') {
                    this.fda3852map[amend.qtr] = AcceptanceFlagState.ACCEPT_INGESTED;
                    this.toastr.success('Accept Ingested Success');
                } else if (amend.acceptanceFlag == 'N') {
                    this.fda3852map[amend.qtr] = AcceptanceFlagState.AMEND;
                    this.toastr.success('Amend Total Success');
                } else if (amend.acceptanceFlag == 'M') {
                    this.fda3852map[amend.qtr] = AcceptanceFlagState.MIXEDACTION;
                    this.toastr.success('Mixed Action Success');
                    this.hasMixedAction = true;
                    this.mixedActionQuarters[amend.qtr] = true;
                } 
                this.isMixedAction();
                jQuery("button[title='Cancel']").click()
            }
        }));

        this.subscriptions.push(this.annualTrueUpComparisonEventService.atCompareSuccessfullSaveActionOneSided$.subscribe(data => {
            
            if (data) {
                let action =  data[0][data[1]];
                for (let i in this.oneSidedFlagMap) {
                    this.oneSidedFlagMap[i] = data[0][i];
                    
                    if (!this.oneSidedFlagMap[i] && data[0][i] != "") {
                        this.oneSidedFlagMap[i] = "DisableAllDeltaActions";
                    } else {
                        this.oneSidedFlagMap[i] = data[0][i];
                    }

                    
                    if ((i == '5' ? '1-4' : i) == this.headerdata.compareResult.quarter && this.headerdata.compareResult.allDeltaStatus == 'Associated' && data[0][(i == '5' ? '1-4' : i)] == 'In Progress') {
                        this.headerdata.compareResult.status = 'In Progress'
                        this.headerdata.compareResult.originalEIN = null;
                        this.headerdata.compareResult.originalName = null;
                    } 
                }
                this.toastr.success(action+' Action Success');
            }
        }));

        this.subscriptions.push(this.annualTrueUpComparisonEventService.nocompanyInfo$.subscribe(
            data => {
                if(data == 0){
                    this.isCompanyDisplay = false;
                }
               
            }
        ));
        let commentManuSaveSubscription = this.annualTrueUpComparisonEventService.flagsOnCommentSaveManu$.subscribe(
            data => {
                this.readfromSharedData();
                this.toastr.success("Comment Saved Successfully");
            }
        )
        let commentImptSaveSubscription = this.annualTrueUpComparisonEventService.flagsOnCommentSaveImpt$.subscribe(
            data => {
                this.readfromSharedData();
            }
        )
        
        this.subscriptions.push(this.annualTrueUpComparisonEventService.atRellocationNotification$.subscribe(
            data => {
                if(data){
                    if(data==='Date Switch') {
                        this.toastr.success("Date Switch Success");
                    } else {
                        this.toastr.success("Manage Action Success");
                    }
                } 
               
            }
        ));
       

        this.subscriptions.push(commentManuSaveSubscription);
        this.subscriptions.push(commentImptSaveSubscription);
        
    }

    isMixedAction(){
        for (let i in this.fda3852map) {
            if(this.fda3852map[i] == AcceptanceFlagState.MIXEDACTION)
                this.mixedActionQuarters[i] = true;
            else
                this.mixedActionQuarters[i]= false;
        }
        this.mixedActionQuarters = Object.assign({}, this.mixedActionQuarters);

        for (let i in this.fda3852map) {
            if(this.fda3852map[i] == AcceptanceFlagState.MIXEDACTION){
                this.hasMixedAction = true;
                return;
            }
            else
                this.hasMixedAction = false;
        }
    }

    ngOnInit() {
        // this.shareData.selectedDt = 'entrySummDt'
        this.alerts = [];
        this.refreshIngest = 0;
        this.scrollTop();

        if (this.shareData.permitType == 'Importer')
            this.getSelectedDt();

    }

    ngAfterViewInit() {
        jQuery(this.quarterId).animate({scrollTop: jQuery(this.quarterId).offset.top=0},'slow');
        // console.log("quarter id from ngafterviewinit"+this.quarterId);
        // document.getElementById(this.quarterId).scrollIntoView({
        //     behavior: 'smooth',
        //     block: 'center',
        //     inline : 'center'
        // });
    }

    ngOnDestroy() {
        this.subscriptions.forEach(sub => {
            if (sub) {
                sub.unsubscribe();
            }
        });
    }

    getSelectedDt() {

        this.atcomparisonservice.getSelectedDateType(this.shareData.fiscalYear, this.shareData.tobaccoClass, this.shareData.ein).subscribe(data => {
            if (data) {
                this.selectedDt = data.toString();
                this.shareData.selectedDt = this.selectedDt;
            }
        })

        // this.subscriptions.push(this.selectedDtBusy);
    }

    trackChanges(event) {
        this.selectedDt = event;
        this.shareData.selectedDt = this.selectedDt;

        this.dateSwitchBusy=this.atcomparisonservice.updateDateSelected(this.shareData.fiscalYear, this.shareData.tobaccoClass
            , this.shareData.ein, this.shareData.selectedDt).subscribe(data => {
                if (data) {
                    this.reallocateService.triggerReallocateEntrySaveEvent("Date Switch");
                    this.getFDA3852flags(this.shareData.fiscalYear, this.shareData.companyId, this.shareData.tobaccoClass, this.shareData.permitType === 'Manufacturer' ? "MANU" : "IMP")
                    this.getOneSidedFlags(this.shareData.fiscalYear, this.shareData.companyId, this.headerdata.tobaccoClass, this.shareData.permitType === 'Manufacturer' ? "MANU" : "IMP");
                }
            });
            this.subscriptions.push(this.dateSwitchBusy);

    }

    scrollTop() {
        jQuery('html, body, .content-wrap').animate({
            scrollTop: 0
        }, 2000);

    }

    manageEntryLine(lineitem: ReallocateModel, quarter: number) {
        this.lineiteminfo = new ReallocateModel();
        this.lineiteminfo.entryId = lineitem.entryId;
        this.lineiteminfo.date = lineitem.date; // entryDate
        this.lineiteminfo.entryNum = lineitem.entryNum;
        this.lineiteminfo.lineNum = lineitem.lineNum;
        this.lineiteminfo.qty = lineitem.qty;
        this.lineiteminfo.tax = lineitem.tax;
        this.lineiteminfo.month = lineitem.month;
        this.lineiteminfo.year = this.shareData.fiscalYear;
        this.lineiteminfo.quarter = quarter;
        this.lineiteminfo.excludedFlag = lineitem.excludedFlag;
        this.refreshIngest = 0;
    }

    refreshIngestedDetails(flag: boolean) {
        this.refreshIngest = 1;
        this.getFDA3852flags(this.shareData.fiscalYear, this.shareData.companyId, this.shareData.tobaccoClass, this.shareData.permitType === 'Manufacturer' ? "MANU" : "IMP");
        this.getOneSidedFlags(this.shareData.fiscalYear, this.shareData.companyId, this.headerdata.tobaccoClass, this.shareData.permitType === 'Manufacturer' ? "MANU" : "IMP");
    }

    toAnnualTrueUp() {
        this.router.navigate(["/app/assessments/annual/" + this.shareData.fiscalYear]);
    }

    toAssessments() {
        this.router.navigate(["/app/assessments"]);
    }

    readfromSharedData() {
        this.headerdata.compareResult = this.shareData.compareResult;
        this.headerdata.fiscalYear = this.shareData.fiscalYear.toString();
        this.headerdata.tobaccoClass = this.shareData.tobaccoClass;
        this.headerdata.quarter = this.shareData.quarter;

        if (this.shareData.tobaccoClass === "Roll-Your-Own") {
            this.headerdata.tobaccoClass = "Roll Your Own";
        }
        this.headerdata.permitType = this.shareData.permitType;
        this.permitType = this.shareData.permitType;
        this.createdDate = this.shareData.createdDate;
        this.submitted = this.shareData.submitted;
        this.quarter = this.shareData.quarter;
        this.selectedDt = this.shareData.selectedDt;
        //alert(this.shareData.originalEIN);
        if (this.shareData.originalEIN) {
            this.headerdata.ein = this.shareData.originalEIN;
            this.headerdata.companyName = this.shareData.originalName;
            this.headerdata.companyId = 0;
        } else {
            this.headerdata.companyId = this.shareData.companyId;
            this.headerdata.ein = this.shareData.ein;
            this.headerdata.companyName = this.shareData.companyName;
            if (this.headerdata.companyId === 0) {
                this.isCompanyDisplay = false;
            }
        }
        this.getFDA3852flags(this.shareData.fiscalYear, this.shareData.companyId, this.headerdata.tobaccoClass, this.shareData.permitType === 'Manufacturer' ? "MANU" : "IMP");
        this.getOneSidedFlags(this.shareData.fiscalYear, this.shareData.companyId, this.headerdata.tobaccoClass, this.shareData.permitType === 'Manufacturer' ? "MANU" : "IMP");
    }

    private hideTab(qtr) {
        let tab = this.quarterTabs.tabs[qtr - 1];
        if (tab) {
            tab.disabled = true;
        }
    }

    private showTab(qtr) {
        let tab = this.quarterTabs.tabs[qtr - 1];
        if (tab) {
            tab.disabled = false;
        }
    }

    toggleTab(event) {
        if (event.flag) {
            this.showTab(event.qtr);
        } else {
            this.hideTab(event.qtr);
        }
    }

    isActiveQtr(qtr) {

        let actqtr = -1;
        if (this.quarterTabs) this.quarterTabs.tabs.forEach(function (tab, index) {
            if (tab.active)
                actqtr = index + 1;
        });

        this.annualTrueUpComparisonEventService.triggerAtCompareActiveQuarterUpdateSource(actqtr);

        return (qtr == this.quarter)
    }

    isDeltaAccepted(qtr) {
        return this.fda3852map[qtr] === AcceptanceFlagState.ACCEPT_FDA || this.fda3852map[qtr] === AcceptanceFlagState.ACCEPT_INGESTED;
    }

    isDeltaAmended(qtr) {
        return this.fda3852map[qtr] === AcceptanceFlagState.AMEND;
    }

    isInProgress(qtr) {
        return this.fda3852map[qtr] === AcceptanceFlagState.IN_PROGRESS;
    }

    isDeltaActionRequired(qtr) {
        if (this.tabmap[qtr]) {
            if (!this.isDeltaAccepted(qtr) && !this.isDeltaAmended(qtr)) {
                return true;
            }
        }
        return false;
    }

    getFDA3852flags(fiscalYr, companyid, classId, type) {
        classId = classId.replace(/-/g, " ");
        this.busy = this.atcomparisonservice.getFDA3852Flag(fiscalYr, companyid, classId, type, this.headerdata.ein).subscribe(data => {
            if (data) {
                for (let i in this.fda3852map) {
                    this.fda3852map[i] = data[i];
                    
                    if(this.fda3852map[i] === 'M' || this.fda3852map[i] === AcceptanceFlagState.MIXEDACTION){
                        this.hasMixedAction = true;
                        this.mixedActionQuarters[i] = true;
                    }
                    if (!this.fda3852map[i] && data[i] != "") {
                        this.fda3852map[i] = AcceptanceFlagState.DELTA_CHANGE_ZERO;
                    }
                    else if(this.fda3852map[i] === 'X' || this.fda3852map[i] === AcceptanceFlagState.DELTA_CHANGE_ZERO){
                        this.fda3852map[i] = AcceptanceFlagState.DELTA_CHANGE_TRUE_ZERO;
                    }
                }
                this.isMixedAction();
                this.annualTrueUpComparisonEventService.triggerAtCBPCompare3852FlagsUpdateSource(data);

            }
        });
        this.subscriptions.push(this.busy);

    }

    getOneSidedFlags(fiscalYr, companyid, classId, type) {
        this.busy = this.atcomparisonservice.getOneSidedFlag(fiscalYr, companyid, classId, type, this.headerdata.ein).subscribe(data => {
            if (data) {
                for (let i in this.oneSidedFlagMap) {
                    this.oneSidedFlagMap[i] = data[i];
                    if (!this.oneSidedFlagMap[i] && data[i] != "") {
                        this.oneSidedFlagMap[i] = "DisableAllDeltaActions";
                    } else {
                        this.oneSidedFlagMap[i] = data[i];
                    }


                }
                //this.annualTrueUpComparisonEventService.triggerAtCBPCompareOneSidedFlagsUpdateSource(data);
            }
        });
        this.subscriptions.push(this.busy);
    }

    CBPExportDetail() {

        if (this.downloadBusy) {
            this.downloadBusy.unsubscribe();
        }

        this.downloadBusy = this.annualTrueUpComparisonService.exportCBPData(this.shareData.fiscalYear, this.shareData.quarter, this.shareData.tobaccoClass, this.shareData.ein).subscribe(
            data => {
                if (data) {
                    this.downloadService.download(data.csv, data.title, 'csv/text');
                }
            });
    }

    public hasAssociationStatus() {
        for (let i in this.oneSidedFlagMap) {
            if (this.oneSidedFlagMap[i] == 'Associated') {
                return true;
            }
        }
        return false;
    }

    public exportRawdata() {


        this.exportBusy = this.annualTrueUpComparisonService.getMissingEntryExport("details", this.shareData.companyId, this.shareData.ein, this.shareData.fiscalYear, this.quarter.toString(), this.shareData.tobaccoClass).subscribe(
          data => {
            if (data) {
              this.downloadService.download(data.csv, data.fileName, 'csv/text');
            }
          }
        );
    
        this.subscriptions.push(this.exportBusy);
      }
    
}

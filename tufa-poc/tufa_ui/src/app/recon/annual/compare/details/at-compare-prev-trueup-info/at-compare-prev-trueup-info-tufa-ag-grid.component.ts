import { DecimalPipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from '../../../../../../../node_modules/rxjs';
import { LocalStorageService } from '../../../../../../../node_modules/ngx-webstorage';
import { AgGridComponent } from '../../../../../../app/shared/components/ag-grid/tufa.ag-grid.component';
import { TufaCurrencyPipe } from '../../../../../../app/shared/custompipes/custom.currency.pipe';
import { MinusToParensPipe } from '../../../../../../app/shared/custompipes/minus.parentheses.pipe';
import { AgColumnDefinition, AgExportDefinition, AgGridState } from '../../../../../../app/shared/model/ag-grid.types';
import { agGridStateMgmtProvider, serverSideDataSourceProvider } from '../../../../../../app/shared/providers/ag-grid/ag-grid.providers';
import { ServerSideDataSourceService } from '../../../../../../app/shared/service/ag-grid.datasource.service';
import { AgGridStateMgmtService } from '../../../../../../app/shared/service/ag-grid.state.mgmt.service';
import { RouterGlobalService } from '../../../../../../app/shared/service/router.global.service';
import { AnnualTrueUpComparisonService } from '../../../services/at-compare.service';
import { ShareDataService } from '../../../../../core/sharedata.service';
import { AnnualTrueUpComparisonEventService } from '../../../../../recon/annual/services/at-compare.event.service';


import * as _ from 'lodash';
declare let jQuery: any;
@Component({
  selector: 'app-at-compare-prev-trueup-info-tufa-ag-grid',
  templateUrl: './at-compare-prev-trueup-info-tufa-ag-grid.component.html',
  styleUrls: ['./at-compare-prev-trueup-info-tufa-ag-grid.component.scss'],
  providers: [agGridStateMgmtProvider, serverSideDataSourceProvider, TufaCurrencyPipe, MinusToParensPipe, DecimalPipe],
})
export class AtComparePrevTrueupInfoTufaAgGridComponent extends AgGridComponent implements OnInit {
  
  
  @Input() headerInfo: any;
  columnDefs: AgColumnDefinition[];
  public gridApi;
  public gridColumnApi;
  defaultExportParams: AgExportDefinition;
  context: any;
  data = [];
  gridName: string = 'compare-details-prev-trueup-info';
  busy: Subscription;
  loaded:boolean = false;
  getRowHeight ;
  shareData: ShareDataService;
  storage: LocalStorageService;
  switchTobaccoClass:boolean = false;


  constructor(
    public _agGridStateMgmtService: AgGridStateMgmtService,
    public _serverSideDataSource: ServerSideDataSourceService,
    public routerService: RouterGlobalService,
    public _storage: LocalStorageService,
    private tufaCurrency: TufaCurrencyPipe,
    private minustoparens: MinusToParensPipe,
    private decimalPipe: DecimalPipe,
    private atCompareService: AnnualTrueUpComparisonService,
    public _shareData: ShareDataService,
    private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
  ) {
    super(_agGridStateMgmtService, _serverSideDataSource, routerService, _storage);
    
    this.subscriptions.push(this.annualTrueUpComparisonEventService.tobaccoClassSelected$.subscribe(
      data => {
        
        let panel = jQuery("#collapsePrevtrueupPanel");
        panel.collapse('hide');
        //super.saveGridStateLocal();
        this.switchTobaccoClass = true;
       
      }
    ));
   // let headerHeight = 200;
    let _this = this;
    this.columnDefs = [{
      colId: 'fiscalYear',
      field: 'fiscalYear',
      headerName: "FISCAL YEAR",
      headerTooltip: "FISCAL YEAR",
      width: 10,
      //autoHeight:true,//,
      //headerHeight: headerHeight
    }, {
      colId: 'quarter',
      field: 'quarter',
      headerName: "QUARTER",
      headerTooltip: "QUARTER",
      width: 2,
      //autoHeight:true,
      comparator: (valA, valB, n1, n2, inverse) => {
        const qtrA = valA == '1-4'?5:valA;
        const qtrB = valB== '1-4'?5:valB;
        return qtrA - qtrB;

      },
      valueGetter: function (params) {
        if (params.data != null && params.data.quarter === "5") {
          return '1-4';
        }else{
          return params.data.quarter;
        }
      }
      //,
      //headerHeight: headerHeight
    }, {
      colId: 'tobaccoClassName',
      field: 'tobaccoClassName',
      headerName: "TOBACCO CLASS",
      headerTooltip: "TOBACCO CLASS",
      width: 13,
      comparator: (valA, valB) => {
        return valA.toLowerCase().localeCompare(valB.toLowerCase());
      }
      //autoHeight:true,
      //,
      //headerHeight: headerHeight
    }, {
      colId: 'status',
      field: 'status',
      headerName: "ACTION",
      headerTooltip: "ACTION",
      width: 15,
      //autoHeight:true,//,
     // headerHeight: headerHeight
    }, {
      colId: 'comments',
      field: 'comments',
      headerName: "COMMENTS",
      headerTooltip: "COMMENTS",
      width: 50,
      sortable:false,
      cellStyle: { "white-space": "normal" },
     // wrapText:true,
      autoHeight:true,
      cellRenderer: function(param){
        let commentRet = "";
        if(param != null && param.data != null && param.data.comments!= null && param.data.comments.length >0){
          let comment = param.data.comments;
          for(let index= 0 ; index<comment.length ; index++){
            let commentwithhyphen = "- " + comment[index];
            commentRet = commentRet  + commentwithhyphen;
           if(index !== comment.length - 1){
             commentRet = commentRet + '<br/>';
            }
          }
        }
      return commentRet;
      }
        }];

    this.urlsSaveState = ["/permit/impreport", "/permit/manreport", "/company/", "/permit/history"];
    this.context = { componentParent: this };
    this.storeGridState = true;
    this.shareData = _shareData;
    this.storage = _storage;
    this.expandPrevTrueupInfo();
    this.switchTobaccoClass = false;
    /** this.getRowHeight = function (params : any) {
      return params.data.rowHeight;
    };**/
  }
  expandPrevTrueupInfo(){
    if(this.shareData.prevTrueupInfoExpanded === true){
     
    }
  }
  ngOnInit() {
    super.ngOnInit();
    //this.getPreviousTrueupInfo();
    
  }

  onGridReady(params) {
    //params.api.suppressNoRowsOverlay = true;
    super.onGridReady(params, '', this.gridName);
    this.setDefaultingSorting();
  }
  setDefaultingSorting(){
    let state: AgGridState = new AgGridState();
    state.sortState = this.gridApi.getSortModel();
    if (!state.sortState || state.sortState.length == 0) {
      this.gridApi.setSortModel([{ colId: "fiscalYear", sort: "desc" }, { colId: "quarter", sort: "desc" }]);
    }
    this.gridApi.resetRowHeights();
  }
  private getPreviousTrueupInfo() {
    this.gridApi.showLoadingOverlay();
    let fiscalYr = this.headerInfo.fiscalYear;
    let ein = this.headerInfo.ein;
    let permitType = this.headerInfo.permitType === 'Manufacturer' ? "MANU" : "IMPT";
    if (this.busy) {
      this.busy.unsubscribe();
    }
    this.busy = this.atCompareService.getPreviousTrueupInfo(permitType, fiscalYr, ein).subscribe(
      dataResponse => {
        this.loaded = true;
        this.switchTobaccoClass = false;
        if (!dataResponse || !dataResponse.length) {
          //this.gridApi.suppressNoRowsOverlay = false;
          this.gridApi.showNoRowsOverlay();
        }else {
          let offset = 100;
          this.gridApi.hideOverlay();
         // this.gridApi.resetRowHeights();
          dataResponse.forEach(function (dataItem: any, index) {  
            if(dataItem.comments == null){
              //dataItem.rowHeight = 25;
            } else{                               
              //dataItem.rowHeight = (25 * dataItem.comments.length) + offset; 
              //18 * (Math.floor(params.data.myDataField.length / 45) + 1);  
            }                 
           });
           this.data = dataResponse;
        }
      }
    );

   
  }
   toggleAccordian(event){
     if(event.currentTarget.ariaExpanded == "false"){
      this.shareData.prevTrueupInfoExpanded = true;
      if(!this.loaded){
        this.getPreviousTrueupInfo();
      } else {
        this.restoreOnPanelOpen();
      }
     }else{
      this.shareData.prevTrueupInfoExpanded = false;
      super.saveGridStateLocal();
     }
     
  }
  restoreOnPanelOpen(){
    super.restoreOnPanelOpen();
    this.gridApi.setColumnDefs(this.columnDefs);
    this.gridApi.setRowData(this.data);
    if(this.switchTobaccoClass){
      super.restoreGridStateLocalPanelLoaded();
      this.gridApi.resetRowHeights();
      this.gridApi.setSortModel([{ colId: "fiscalYear", sort: "desc" }, { colId: "quarter", sort: "desc" }]);
      this.gridApi.paginationGoToFirstPage();
      this.switchTobaccoClass = false;
    }else if(this.loaded && !this.switchTobaccoClass){
      super.restoreGridStateLocal();
      this.gridApi.resetRowHeights();
      this.gridApi.setColumnDefs(this.columnDefs);
      this.gridApi.paginationGoToFirstPage();
    }
    
  }
}

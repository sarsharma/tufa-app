import { Component, Input, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { AnnualTrueUpComparisonDetailsHeader } from '../../models/at-detailsheader.model';

declare let jQuery: any;

@Component({
    selector: '[at-comparedetailsheader]',
    templateUrl: './at-comparedetailsheader.template.html',
    styleUrls:['at-comparedetailsheader.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class AnnualTrueUpCompareDetailsHeaderComponent implements OnInit {
    @Input() headerInfo;
    headerdata: AnnualTrueUpComparisonDetailsHeader;

    constructor() {
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "headerInfo") {
                this.headerdata = changes[propName].currentValue;
            }
        }
    }
}

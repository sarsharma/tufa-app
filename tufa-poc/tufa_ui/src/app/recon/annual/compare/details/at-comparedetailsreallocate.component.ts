/*
@author : Priti
this is Component for reallocating entry line as a popup.
*/
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import * as _ from 'lodash';
import { Subscription } from '../../../../../../node_modules/rxjs';
import { IngestionWorkFlowService } from '../../../../shared/service/ingestion-workflow.service';
import { AnnualTrueUpCBPLineEntryUpdate } from '../../models/at-cbplineentryupdate.model';
import { ReallocateModel } from '../../models/at-reallocate.model';
import { AnnualTrueUpComparisonService } from '../../services/at-compare.service';
import { AtReallocationService } from '../../services/at-reallocation.service';

declare var jQuery: any;

@Component({
  selector: '[reallocate-entry]',
  templateUrl: './at-comparedetailsreallocate.template.html',
  styleUrls: ['at-comparedetailssummary.style.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ReallocatePopup implements OnInit, OnDestroy {

  @Input() entrydata: ReallocateModel;
  @Output() onReallocated = new EventEmitter<boolean>();
  @Input() selectedDt: string;
  @Input() mixedActionQuarters: any;

  entryLine: "IncludeEntryLine" | "ExcludeEntryLine";
  assignedMonth: string;
  assignedYear: number;
  showEntryDate: boolean = true;

  subscriptions: Subscription[] = [];
  entryLines: ReallocateModel[] = [];
  cbplineinfo = new ReallocateModel();
  updateinfo: AnnualTrueUpCBPLineEntryUpdate = new AnnualTrueUpCBPLineEntryUpdate();

  months: any[] = [
    { name: "October - Q1", value: "10", disable: false },
    { name: "November - Q1", value: "11", disable: false },
    { name: "December - Q1", value: "12", disable: false },
    { name: "January - Q2", value: "1", disable: false },
    { name: "February - Q2", value: "2", disable: false },
    { name: "March - Q2", value: "3", disable: false },
    { name: "April - Q3", value: "4", disable: false },
    { name: "May - Q3", value: "5", disable: false },
    { name: "June - Q3", value: "6", disable: false },
    { name: "July - Q4", value: "7", disable: false },
    { name: "August - Q4", value: "8", disable: false },
    { name: "September - Q4", value: "9", disable: false }
  ];

  busy: Subscription;

  constructor(
    private atcomparisonservice: AnnualTrueUpComparisonService,
    private reallocateService: AtReallocationService,
    private ingestionWorkflowSvc: IngestionWorkFlowService,
  ) {
    this.entryLine = "IncludeEntryLine";

    this.subscriptions.push(this.reallocateService.reallocateEntryEvent$.subscribe(
      data => {
        this.entryLines = data.entries;
        if (this.entryLines && this.entryLines.length > 0) {
          this.assignedYear = this.entryLines[0].year;
        }
      }
    ));
  }

  ngOnInit() {
  }

  disableMixedActionMonths(){
    for (let i in this.months) {
      this.months[i].disable = false;
    }
    for (let i in this.mixedActionQuarters) {
      if(this.mixedActionQuarters[i] === true && Number(i) === 1){
        this.months[0].disable = true;
        this.months[1].disable = true;
        this.months[2].disable = true;
      }
      else if(this.mixedActionQuarters[i] === true && Number(i) === 2){
        this.months[3].disable = true;
        this.months[4].disable = true;
        this.months[5].disable = true;
      }
      else if(this.mixedActionQuarters[i] === true && Number(i) === 3){
        this.months[6].disable = true;
        this.months[7].disable = true;
        this.months[8].disable = true;
      }
      else if(this.mixedActionQuarters[i] === true && Number(i) === 4){
        this.months[9].disable = true;
        this.months[10].disable = true;
        this.months[11].disable = true;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.forEach(sub => {
        if (sub) sub.unsubscribe;
      });
    }
  }

  reset(flag) {
    this.assignedYear = null;
    this.assignedMonth = '';
    jQuery('#reallocateentryform').parsley().reset();
    jQuery('#reallocate-entry').modal('hide');
    this.entryLine = "IncludeEntryLine";
    this.reallocateService.triggerResetSelectAllEntryEvent(true);
  }

  getQuarter(monthNum: string) {
    switch (monthNum) {
      case '10': case '11': case '12':
        return 1;

      case '1': case '2': case '3':
        return 2;

      case '4': case '5': case '6':
        return 3;

      case '7': case '8': case '9':
        return 4;

      default:
        return 0;

    }
  }

  getMonthName(monthNum: string) {
    switch (monthNum) {
      case '10': return "OCT";
      case '11': return "NOV";
      case '12': return "DEC";
      case '1': return "JAN";
      case '2': return "FEB";
      case '3': return "MAR";
      case '4': return "APR";
      case '5': return "MAY";
      case '6': return "JUN";
      case '7': return "JUL";
      case '8': return "AUG";
      case '9': return "SEP";
      default:
        return "";

    }
  }

  saveEntryLineChanges() {
    if (this.entryLine === "IncludeEntryLine") {
      jQuery('#reallocateentryform').parsley().validate();
      if (!jQuery('#reallocateentryform').parsley().isValid()) return;
    }

    let updateList: AnnualTrueUpCBPLineEntryUpdate[]
    if (this.entryLine === "IncludeEntryLine") {
      updateList = this.entryLines.map(entry => {
        let update = new AnnualTrueUpCBPLineEntryUpdate();
        update.cbEntryId = entry.entryId;
        update.assignedYear = entry.year;
        update.reallocateFlag = this.isReallocated(entry) ? 'Y' : 'N';
        update.excludeFlag = 'N';
        update.missingFlag = entry.missingFlag;
        update.assignedMonth = this.getMonthName(this.assignedMonth);
        update.assignedQuarter = this.getQuarter(this.assignedMonth);
        return update;
      });

    } else if (this.entryLine === "ExcludeEntryLine") {
      updateList = this.entryLines.map(entry => {
        let update = new AnnualTrueUpCBPLineEntryUpdate();
        update.cbEntryId = entry.entryId;
        update.assignedYear = entry.year;
        update.reallocateFlag = entry.reallocatedFlag;
        update.excludeFlag = 'Y';
        update.missingFlag = entry.missingFlag;
        let lineEntryMonth = entry.month;

        let exludeAssignedMonth;
        let selMonth = _.find(this.months, function (o) { return (o.name.indexOf(lineEntryMonth) > -1) });
        if (selMonth) {
          exludeAssignedMonth = selMonth.value;
        }

        update.assignedMonth = this.getMonthName(exludeAssignedMonth);
        update.assignedQuarter = this.getQuarter(exludeAssignedMonth);
        return update;
      });
    } else if(this.entryLine === "MissingEntryLine") {
      updateList = this.entryLines.map(entry => {
        let update = new AnnualTrueUpCBPLineEntryUpdate();
        update.cbEntryId = entry.entryId;
        update.assignedYear = entry.year;
        update.reallocateFlag = entry.reallocatedFlag;
        update.excludeFlag = entry.excludedFlag;
        update.missingFlag = 'Y';
        let lineEntryMonth = entry.month;

        let missingAssignedMonth;
        let selMonth = _.find(this.months, function (o) { return (o.name.indexOf(lineEntryMonth) > -1) });
        if (selMonth) {
          missingAssignedMonth = selMonth.value;
        }

        update.assignedMonth = this.getMonthName(missingAssignedMonth);
        update.assignedQuarter = this.getQuarter(missingAssignedMonth);
        return update;
      });

    } else if(this.entryLine === "ResetEntryLine") {
      updateList = this.entryLines.map(entry => {
        let update = new AnnualTrueUpCBPLineEntryUpdate();
        update.cbEntryId = entry.entryId;
        update.assignedYear = entry.year;
        update.reallocateFlag = entry.reallocatedFlag;
        update.excludeFlag = entry.excludedFlag;
        update.missingFlag = 'N';
        let lineEntryMonth = entry.month;

        let missingAssignedMonth;
        let selMonth = _.find(this.months, function (o) { return (o.name.indexOf(lineEntryMonth) > -1) });
        if (selMonth) {
          missingAssignedMonth = selMonth.value;
        }

        update.assignedMonth = this.getMonthName(missingAssignedMonth);
        update.assignedQuarter = this.getQuarter(missingAssignedMonth);
        return update;
      });

    }


    let fiscalYear = this.ingestionWorkflowSvc.getContextInfo()[0].fiscalYr;

    this.busy = this.reallocateService.updateCBPIngestedLineEntryInfo(updateList, fiscalYear).subscribe(
      data => {
        this.onReallocated.emit(true);
        this.reallocateService.triggerReallocateEntrySaveEvent("Reallocation");
        this.reset(true);
      }
    );
  }

  public test() {
    this.entryLines.forEach(line => {
      this.isReallocated(line);
    });
  }

  private isReallocated(entry: ReallocateModel): boolean {
    let date = new Date(0);
    date.setUTCMilliseconds(Number(entry.date));

    let entryDate = new Date(0);
    entryDate.setUTCMilliseconds(Number(entry.entryDate));

    let currentMonth: number = entry.secondaryDateFlag == 'N' ? date.getMonth() : entryDate.getMonth();
    if(entry.secondaryDateFlag == 'Y'){
      let year: number = entryDate.getFullYear();
      if(Number(this.assignedMonth) - 1 == currentMonth )
        return Number(this.assignedYear) != year;
      else if(Number(this.assignedMonth) - 1 != currentMonth)
        return Number(this.assignedMonth) - 1 != currentMonth;
    }
    else    
      return Number(this.assignedMonth) - 1 != currentMonth;
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "entrydata") {
        let chg = changes[propName];
        this.entryLines = [];
        if (chg.currentValue) {
          let cbplineinfo: ReallocateModel = chg.currentValue;
          this.assignedYear = cbplineinfo.year;

          let lineEntryMonth = cbplineinfo.month;
          let selMonth = _.find(this.months, function (o) { return (o.name.indexOf(lineEntryMonth) > -1) });
          if (selMonth) {
            this.assignedMonth = selMonth.value;
          }

          if (cbplineinfo.excludedFlag && cbplineinfo.excludedFlag === 'Y') {
            this.entryLine = "ExcludeEntryLine";
          }
          this.entryLines.push(cbplineinfo);
          this.showEntryDate = false;
        }
      }
      // if(propName==="mixedActionQuarters"){
      //   this.disableMixedActionMonths()
      // }
    }
  }

}

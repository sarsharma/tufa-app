import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import { ShareDataService } from '../../../../core/sharedata.service';
import { DownloadSerivce } from '../../../../shared/service/download.service';
import { SummarySet } from '../../models/at-summary-set.model';
import { ToggleTabModel } from '../../models/at-toggletab.model';
import { AnnualTrueUpService } from '../../services/at.service';

declare let jQuery: any;
// import * as cigarsummarydata from './cigarsummary.json';

@Component({
    selector: '[at-comparedetailssummary]',
    templateUrl: './at-comparedetailssummary.template.html',
    styleUrls: ['at-comparedetailssummary.style.scss'],
    encapsulation: ViewEncapsulation.None
})


export class AnnualTrueUpCompareDetailsSummaryComponent implements OnInit {
    @Input() summaryData;
    @Input() importerData : SummarySet;
    @Input() quarter;
    @Output() toggleTab = new EventEmitter<ToggleTabModel>();


    data: any[];
    ingestedData: any[];
    qtr: any[];
    router: Router;
    alerts: { type: string; msg: string; }[];
    oneAtATime: boolean = true;
    shareData: ShareDataService;
    numColumns: number
    delta: number;

    constructor(
        router: Router,
        private _shareData: ShareDataService,
        private annualTrueUpService: AnnualTrueUpService, 
        private downloadService: DownloadSerivce) {

        this.alerts = [
            {
                type: 'success',
                msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Message</span>'
            }
        ];

        this.router = router;
        this.shareData = _shareData;
        this.numColumns = 4;
    }

    ngOnInit() {
        this.alerts = [];
        if ((this.shareData.tobaccoClass === "Pipe/Roll Your Own") || (this.shareData.tobaccoClass === "Chew/Snuff")) {
            this.numColumns = 6;
        }


    }



    IsNumeric(input) {
        if (input !== null || input !== "") {
            let RE = /^-{0,1}\d*\.{0,1}\d+$/;
            return (RE.test(input));
        }
    }

    IsNullorBlank(input) {
        if (input == null || input === "")
            return " ";

    }



    IsPermit(index, input: string) {
        // Check only the first colum - Assume the first column is permit
        if (input != null) {
            if (index === 0 && input.length > 4) {
                if (!input.toUpperCase().startsWith("QUARTER") && !input.toUpperCase().startsWith("TOTAL") &&
                    !input.toUpperCase().startsWith("PERMIT") && !input.toUpperCase().startsWith("TOBACCO CLASS")
                    && !input.toUpperCase().startsWith("PERIOD")) {
                    return true;
                }
            }
        }
        return false;
    }

    boldrowforQuarter() {
        // Bold the Quarter row for cigars on the Summary Table 
        //jQuery('#summarytable tr td :contains(Quarter)').parent().parent().css('font-weight', 550);
        var cell = jQuery('#summarytable tr td, #ingestedSummarytable tr td');

        if (cell.length > 50) {
            cell.each(function () {
                var cell_value = jQuery(this).html();

                if (cell_value.includes("Quarter 2") || cell_value.includes("Jan")
                    || cell_value.includes("Feb") || cell_value.includes("Mar") ||
                    cell_value.includes("Quarter 4") || cell_value.includes("Jul")
                    || cell_value.includes("Aug") || cell_value.includes("Sep"))
                    jQuery(this).parent().css('background-color', 'Whitesmoke');

                if (cell_value.includes("Quarter"))
                    jQuery(this).parent().css('font-weight', 600);

                if (cell_value.includes('Jan') || cell_value.includes('Feb') || cell_value.includes('Mar') || cell_value.includes('Apr')
                    || cell_value.includes('May') || cell_value.includes('Jun') || cell_value.includes('Jul') || cell_value.includes('Aug')
                    || cell_value.includes('Sep') || cell_value.includes('Oct') || cell_value.includes('Nov') || cell_value.includes('Dec'))
                    jQuery(this).css('padding-left', '70px');

            });
        }

    }


    public getSummaryDelta() {
        for (let i in this.data) {
            let numCols = this.data[i]["numColumns"];
            let cellVals = this.data[i]["cellValues"];
            if (cellVals[0] != null) {
                if (cellVals[0].toUpperCase().includes("TOTAL")) {
                    this.delta = Number(cellVals[numCols - 1]);
                    return cellVals[numCols - 1];
                }
            }

        }
    }

    public exportSummaryData(fileType: string) {
        let qtr = this.shareData.quarter === 5 ? '1-4' : this.shareData.quarter;
        let ingestionType = "double";
        this.annualTrueUpService.getIngestedSummaryExport(
            fileType, this.shareData.companyId, this.shareData.ein, this.shareData.fiscalYear, qtr,
             this.shareData.tobaccoClass,ingestionType).subscribe(
                data => {
                    if(data) {
                        this.downloadService.download(data.csv,data.fileName,'csv/text');
                    }
                }
            );
    }


    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "summaryData") {
                this.data = changes[propName].currentValue;
            }

            if (propName === "importerData") {
                this.importerData = changes[propName].currentValue;
            }

            if (propName === "quarter") {
                this.qtr = changes[propName].currentValue;

            }
        }
    }
}

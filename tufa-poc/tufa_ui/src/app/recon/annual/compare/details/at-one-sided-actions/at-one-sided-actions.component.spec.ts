import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtOneSidedActionsComponent } from './at-one-sided-actions.component';

describe('AtOneSidedActionsComponent', () => {
  let component: AtOneSidedActionsComponent;
  let fixture: ComponentFixture<AtOneSidedActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtOneSidedActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtOneSidedActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgControl } from '../../../../../../../node_modules/@angular/forms';
import { Subscription } from '../../../../../../../node_modules/rxjs';
import { Company } from '../../../../../company/models/company.model';
import { CompanyService } from '../../../../../company/services/company.service';
import { ShareDataService } from '../../../../../core/sharedata.service';
import { TrueUpComparisonResult } from '../../../models/at-comparisonresult.model';
import { AnnualTrueUpComparisonDetailsHeader } from '../../../models/at-detailsheader.model';
import { OneSidedDeltaDetaisl } from '../../../models/at-one-sided-deltaDetails';
import { TTBTaxesVol } from '../../../models/at-ttbtaxesvol.model';
import { AnnualTrueUpComparisonEventService } from '../../../services/at-compare.event.service';
import { AnnualTrueUpComparisonService } from '../../../services/at-compare.service';


declare let jQuery: any;
@Component({
  selector: 'app-at-one-sided-actions',
  templateUrl: './at-one-sided-actions.component.html',
  styleUrls: ['./at-one-sided-actions.component.scss'],
  providers: [CompanyService],
})
export class AtOneSidedActionsComponent implements OnInit,OnChanges {

  @Input() panelQtr: number;
  @Input() headerData: AnnualTrueUpComparisonDetailsHeader;
  @Input() quarterDeltaForOnesidedMap: Map<number, OneSidedDeltaDetaisl>;
  @Input() headerDataMapForAllDeltas: Map<string, TrueUpComparisonResult>;
  @ViewChild('associateEinInput') associateForm: NgControl;
  @Input() oneSidedAcceptanceFlag: string;
  @Output() actionTakenOneSidedflag = new EventEmitter<any>();

  //quarterDeltaForOnesidedMap: Map<number, OneSidedDeltaDetaisl>;


  einCheckbusy: Subscription;
  affectedDeltaCntBusy: Subscription;

  hasError: boolean;
  einIsChecked: boolean = false;
  thinking: boolean;
  associationCompany: any = new Company();
  allDeltaStatus: string;
  fileTypeSelected: string;
  actionType: string;

  affectedTTDeltasGrid: any;
  affectedDeltaCnt: number;

  deltaDetails: TrueUpComparisonResult;

  subscriptionArry: Subscription[] = [];
  busy: Subscription;
  /** company details  */
  companyName;
  originalName;
  tobaccoClass;
  qtr;
  delta: string;
  tufaTaxRate: number;
  cbpDetailVolume: number;
  cbp_all_deltas_dialog_id: string = "at-cbp-alldeltas-dialog";
  alerts: { type: string; msg: string; }[];
  resetAlerts: any;
  excludeAlerts: any;
  associateAlerts: any;
  msrAlerts: any;
  msrNoVolAlerts: any;
  popupHeaderString: string;
  selectDeltas: boolean = false;
  saveDeltaBusy: Subscription;
  compoundTTTaxesVolume: TTBTaxesVol[] = [];
  amendedTax1: number;
  amendedTax2: number;
  tobaccoClassNames: any;
  quarter: string;
  isTufaOnly: boolean = false;
  isImptIngested: boolean = false;
  isAssociated: boolean = false;
  validationNum: any;
  totalDelta: string;
  subscriptions: any;
  //headerData: AnnualTrueUpComparisonDetailsHeader;
  istufaMSReady: boolean = false;
  ein:any;
  originalEIN:any;
  permitType:any;
  isAllActionSpan: boolean = false;
  public einmask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
 
  router: Router;

  constructor(private _companyService: CompanyService,
    private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
    private atcomparisonservice: AnnualTrueUpComparisonService,
    public _shareData: ShareDataService, router: Router

  ) {
    this.router = router;
    this.alerts = [
      {
        type: 'danger',
        // tslint:disable-next-line:max-line-length
        msg: '<span class="alert-danger"><strong>ACTION REQUIRED</strong>: Updates found. Please select refresh to regenerate comparison</span>'
      }
    ];

    this.resetAlerts = [
      {
        type: 'warning',
        // tslint:disable-next-line:max-line-length
        msg: 'Resetting a delta(s) will return the entry line to its original state'
      }
    ];

    this.msrAlerts = [
      {
        type: 'warning',
        // tslint:disable-next-line:max-line-length
        msg: 'The action will mark the delta(s) as Market Share Ready'
      }
    ];

    this.msrNoVolAlerts = [
      {
        type: 'warning',
        // tslint:disable-next-line:max-line-length
        msg: 'No ingested volume data exists for this delta.'
      }
    ];

    this.excludeAlerts = [
      {
        type: 'warning',
        // tslint:disable-next-line:max-line-length
        msg: 'Excluding will remove the entries from the Market Share'
      }
    ];

    this.associateAlerts = [
      {
        type: 'warning',
        msg: 'The action will associate the delta(s) to the inputed company'
      }
    ];
  }

  ngOnInit() {
    
    if (null != this.headerData.compareResult) {
      //initializing data with header data for current quarter clicked.
      this.delta = this.headerData.compareResult.totalDeltaTax;
      this.headerData.compareResult.fiscalYr = this.headerData.fiscalYear;
      this.allDeltaStatus = this.headerData.compareResult.allDeltaStatus;
      this.tobaccoClass = this.headerData.tobaccoClass;
      this.tobaccoClassNames = this.tobaccoClass.split("/");
      //this.quarter = this.headerData.quarter.toString() === "1-4" || this.headerData.quarter.toString() === "5" ? "1-4" : this.headerData.quarter.toString();
      this.permitType = this.headerData.compareResult.permitType;
    }
    this.deltaDetails = this.headerData.compareResult;
    this.cbp_all_deltas_dialog_id = this.cbp_all_deltas_dialog_id + '-' + this.panelQtr;
    this.ein = this.headerData.compareResult.ein;
    this.originalEIN = this.headerData.compareResult.originalEIN;
    if (this.quarterDeltaForOnesidedMap.size > 0) {
      //alert("this.quarterDeltaForOnesidedMap oninit");
      this.totalDelta = this.quarterDeltaForOnesidedMap.get(this.panelQtr).totalDelta;
      if (this.quarterDeltaForOnesidedMap.get(this.panelQtr).dataType === 'TUFA') {
        this.isTufaOnly = true;
        if (this.oneSidedAcceptanceFlag === 'MSReadyTufa' || this.oneSidedAcceptanceFlag === 'MSReady')
          this.istufaMSReady = true;
      }
     
    }
    
    if (this.originalEIN) {
      this.deltaDetails.ein = this.originalEIN;
      this.deltaDetails.legalName = this.deltaDetails.originalName;
    }

    
    jQuery(document).ready(function () {
      let modalContent: any = jQuery('.modal-content');
      modalContent.draggable({
        handle: '.modal-header,.modal-body'
      });
    });
  }
  isDisplayAssociation(){
       if(this.quarterDeltaForOnesidedMap && this.quarterDeltaForOnesidedMap.size > 0) {
        let  onesideDeltaDetails = this.quarterDeltaForOnesidedMap.get(this.panelQtr);        
        if (onesideDeltaDetails && (!onesideDeltaDetails.dataType ||(onesideDeltaDetails.dataType && onesideDeltaDetails.dataType.toUpperCase() === 'INGESTED'))
             && (this.permitType && this.permitType.toUpperCase() === 'IMPORTER')) {
              if(onesideDeltaDetails.allDeltaStatus == 'Associated' 
                  || onesideDeltaDetails.allDeltaStatus == 'In Progress' || onesideDeltaDetails.allDeltaStatus == 'Review' 
                  || onesideDeltaDetails.allDeltaStatus == null || onesideDeltaDetails.allDeltaStatus == '' ||onesideDeltaDetails.allDeltaStatus == "" || onesideDeltaDetails.allDeltaStatus == 'DisableAllDeltaActions'){
                  return true;
              }
        
      }
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "headerData") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.permitType = this.headerData.compareResult.permitType;
        }
      }
      else if (propName === "oneSidedAcceptanceFlag") {
        if(this.oneSidedAcceptanceFlag === 'MSReadyTufa' 
        && this.quarterDeltaForOnesidedMap.get(this.panelQtr).dataType === 'TUFA'){
          this.istufaMSReady = true
        }
        if( this.quarterDeltaForOnesidedMap.get(this.panelQtr)) {
          this.quarterDeltaForOnesidedMap.get(this.panelQtr).allDeltaStatus = this.oneSidedAcceptanceFlag;
        }
      }
    }
  }
  
  ngOnDestroy() {
    this.subscriptionArry.forEach(sub => {
        if (sub) {
            sub.unsubscribe();
        }
    });
}
  
  //Association functionality 
  displayAssociationDialog() {
    this.popupHeaderString = "Delta Association";
    this.deltaDetails = this.headerData.compareResult;
    this.allDeltaStatus = this.quarterDeltaForOnesidedMap.get(this.panelQtr).allDeltaStatus;
    this.actionType = "Association";
    this.quarter = this.panelQtr == 5 ? '1-4' : this.panelQtr.toString();
    this.deltaDetails.quarter = this.quarter; 
    if (this.isValidString(this.originalEIN)) {
      this.getAssociatedEIN(this.originalEIN);
    } else {
      this.getAssociatedEIN(this.ein);
    }
    jQuery("#at-cbp-alldeltas-dialog-" + this.panelQtr).modal('show');
    this.getAffectedDeltas();
    this.totalDelta = this.quarterDeltaForOnesidedMap.get(this.panelQtr).totalDelta;
    this.tobaccoClass = this.headerData.tobaccoClass;
  }
  getAssociatedEIN(ein:any){
    this.deltaDetails.ein= ein;
    this.associationCompany.einNumber = '';
    this.tobaccoClass = this.headerData.tobaccoClass;
    this._companyService.getAssociatedCompanyByEIN(Number(ein), Number(this.deltaDetails.fiscalYr), Number(this.panelQtr), this.tobaccoClass).subscribe(
      data => {
        if (data) {
          this.associationCompany.einNumber =  data.toString();
          this.checkEIN();
        } 
      });
  }
  //Exclude functionality 
  displayExcludeDialog() {
    this.quarter = this.panelQtr == 5 ? '1-4' : this.panelQtr.toString();
    jQuery("#at-cbp-alldeltas-dialog-" + this.panelQtr).modal('show');
    this.allDeltaStatus = this.quarterDeltaForOnesidedMap.get(this.panelQtr).allDeltaStatus;
    this.deltaDetails = this.headerData.compareResult;
    this.actionType = "Exclude";
    this.getAffectedDeltas();
    this.totalDelta = this.quarterDeltaForOnesidedMap.get(this.panelQtr).totalDelta;
    this.tobaccoClass = this.headerData.tobaccoClass;
    this.popupHeaderString = "Delta Exclude";
  }

  //MarketShareReady
  displayMSReadyDialog() {
    this.quarter = this.panelQtr == 5 ? '1-4' : this.panelQtr.toString();
    this.tobaccoClass = this.headerData.tobaccoClass;
    if (this.quarterDeltaForOnesidedMap.get(this.panelQtr).dataType === 'TUFA') {
      this.isAllActionSpan = false;
      let updateRptData = this.getDeltaStsChngObject("MSReady", "single");
      this.updateCompareAllDeltaStatus(updateRptData, true);
      this.istufaMSReady = true;
    } else {
      jQuery("#at-cbp-alldeltas-dialog-" + this.panelQtr).modal('show');
      this.deltaDetails = this.headerData.compareResult;
      this.allDeltaStatus = this.quarterDeltaForOnesidedMap.get(this.panelQtr).allDeltaStatus;
      this.actionType = "Market Share Ready";
      if (!this.isCmpdIngestTT()) {
        this.getAffectedDeltas();
        if(this.isCigarCigarrettesManu()){
          this.getNonCigarManuDetails(this.headerData.compareResult);
        }
      } else {
        this.getNonCigarManuDetails(this.headerData.compareResult);
      }
      this.totalDelta = this.quarterDeltaForOnesidedMap.get(this.panelQtr).totalDelta;
      this.popupHeaderString = "Market Share Ready";
    }

  }


  //Association logic
  checkEIN() {

    this.hasError = false;
    //Determine if the service is already being called to prevent error
    if (this.einCheckbusy == null || this.einCheckbusy.closed) {
      this.thinking = false;
    } else {
      this.thinking = true;
    }

    if (this.associationCompany.einNumber && this.associationCompany.einNumber.length === 9 && !this.thinking) {
      this.einCheckbusy = this._companyService.getActiveImptCompanyByEIN(this.associationCompany.einNumber).subscribe(
        data => {
          if (data && data.einNumber) {
            this.associationCompany = data;
          } else {
            this.associationCompany.legalName = null;
          }
          this.einIsChecked = true;
          this.hasError = !this.isValidString(this.associationCompany.legalName);
        });

    } else {
      this.einIsChecked = false;
    }
  }

  associate() {
    this.associateForm.control.markAsTouched();
    if (this.associateForm.valid && !this.hasError) {
      this.isAllActionSpan = false;
      let updateRptData = this.getDeltaStsChngObject("Associated", "single");
      this.updateCompareAllDeltaStatus(updateRptData, true);
    }

  }

  associateAll() {
    this.associateForm.control.markAsTouched();
    if (this.associateForm.valid && !this.hasError) {
      this.isAllActionSpan = true;
      let updateRptData = this.getDeltaStsChngObject("Associated", "multiple");
      this.updateCompareAllDeltaStatus(updateRptData, true);
    }

  }


  saveAll() {
    if (this.actionType === 'Association') {
      this.associateAll()
      this.istufaMSReady = false;
    } else if (this.actionType === 'Exclude') {
      this.excludeAll();
      this.istufaMSReady = false;
    } else if (this.actionType === 'Market Share Ready' || this.deltaDetails.allDeltaStatus === 'Market Share Ready') {
      this.mktShrRdyAllDeltaActions();
    }

  }

  exclude() {
    this.isAllActionSpan = false;
    let updateRptData = this.getDeltaStsChngObject("Excluded", "single");
    this.updateCompareAllDeltaExclude(updateRptData);
  }

  excludeAll() {
    this.isAllActionSpan = true;
    let updateRptData = this.getDeltaStsChngObject("Excluded", "multiple");
    this.updateCompareAllDeltaExclude(updateRptData);
  }


  updateCompareAllDeltaExclude(exludeData: any) {
    //need to add fiscal year.
    this.saveDeltaBusy = this.atcomparisonservice.updateCompareAllDeltaExclude(exludeData, this.headerData.fiscalYear).subscribe(
      data => {
        if (data) {
          this.getOneSidedFlags(this.headerData.fiscalYear, this.headerData.companyId, this.headerData.tobaccoClass, this.headerData.permitType === 'Manufacturer' ? "MANU" : "IMP");
          this.popUpClose();
        }
      }
    );
    this.subscriptionArry.push(this.saveDeltaBusy);  
  }

  public mktShrRdyAllDeltaActions() {
    this.isAllActionSpan = true;
    let updateRptData = this.getDeltaStsChngObject("MSReady", "multiple");
    this.updateCompareAllDeltaStatus(updateRptData, true);
  }
  
  private getDeltaStsChngObject(deltaStatus, actionSpan, delta = this.deltaDetails) {

    if (actionSpan == 'single') {
      let updateRptData = new TrueUpComparisonResult();
      //setting vlaues for quarter,delta and alldelta status action.

      if (this.isAllActionSpan) {
        this.deltaDetails.quarter = delta.quarter.toString() === "5" ? "1-4" : delta.quarter.toString();
        this.deltaDetails.allDeltaStatus = delta.allDeltaStatus;
        jQuery.extend(true, updateRptData, delta);
      } else {
        this.deltaDetails.quarter = this.panelQtr.toString() === "5" ? "1-4" : this.panelQtr.toString();
        let oneSidedDelta = this.quarterDeltaForOnesidedMap.get(this.panelQtr);
        this.deltaDetails.totalDeltaTax = oneSidedDelta.totalDelta;
        this.deltaDetails.abstotalDeltaTax = this.totalDelta;
        this.deltaDetails.allDeltaStatus = oneSidedDelta.allDeltaStatus;
        this.deltaDetails.tobaccoClass = this.headerData.tobaccoClass;
        this.deltaDetails.dataType = oneSidedDelta.dataType;
        jQuery.extend(true, updateRptData, this.deltaDetails);
      }

      updateRptData.fileType = null;      
      if (this.deltaDetails.allDeltaStatus == "Associated" && deltaStatus == "reset") {
        if(updateRptData.originalEIN){
          updateRptData.legalName = updateRptData.originalName;
          updateRptData.ein = updateRptData.originalEIN;
          
        }else{
          updateRptData.originalName = updateRptData.legalName;
          updateRptData.originalEIN = updateRptData.ein;
        }
        updateRptData.totalDeltaTax = "";
        updateRptData.abstotalDeltaTax= "";
      } else if (deltaStatus == "Associated") {
        if (delta.originalEIN) {
          //Rest parent information
          updateRptData.ein = this.associationCompany.einNumber;
          updateRptData.legalName = this.associationCompany.legalName;
        } else {
          //Save orignal information
          updateRptData.originalEIN = delta.ein;
          updateRptData.originalName = delta.legalName;

          //Set new parent information
          updateRptData.ein = this.associationCompany.einNumber;
          updateRptData.legalName = this.associationCompany.legalName;
        }
        updateRptData.totalDeltaTax = "";
        updateRptData.abstotalDeltaTax= "";
      }
      updateRptData.allDeltaCurrentStatus = this.deltaDetails.allDeltaStatus;
      updateRptData.allDeltaStatus = deltaStatus;
      if (this.deltaDetails.totalDeltaTax === 'Review' && deltaStatus == "MSReady")
        updateRptData.fileType = this.fileTypeSelected;
        //to push the delta status to inprogress after reset for association
      if(this.deltaDetails.allDeltaStatus == "Associated" && deltaStatus == "In Progress"){
          updateRptData.totalDeltaTax = "";
          updateRptData.originalEIN = "";
          updateRptData.originalName = "";
          updateRptData.allDeltaCurrentStatus = "Review"
          //Rest parent information
          updateRptData.ein = delta.ein;
          updateRptData.legalName = delta.legalName;
          updateRptData.abstotalDeltaTax= "";
          
        }
      let deltasStatusChange: TrueUpComparisonResult[] = [];
      deltasStatusChange.push(updateRptData);
      return deltasStatusChange;
    }
    else if (actionSpan == 'multiple') {
      let deltasStatusChange: TrueUpComparisonResult[] = [];
      this.affectedTTDeltasGrid.forEach(delta => {
        if (delta.affectDeltaFlag) {
          //Smae logic is used so a recursive call with the actionSpan of 'single' can be used
          let updateRptData = this.getDeltaStsChngObject(deltaStatus, 'single', delta);
          deltasStatusChange = deltasStatusChange.concat(updateRptData);
        }
      });

      return deltasStatusChange;
    }
  }

  popUpClose() {
    jQuery('#at-cbp-alldeltas-dialog-' + this.panelQtr).modal('hide');
    this.resetPopUpState();
    this.fileTypeSelected = '';
  }

  private resetPopUpState() {
    //this.deltaDetails = null;
    this.actionType = null;
    // this.deltaComment = null;
    // this.actionType = 'Select Type';
    this.compoundTTTaxesVolume = [];
    this.affectedTTDeltasGrid = [];
    this.affectedDeltaCnt = 0;
    this.selectDeltas = false;
    //this.associationCompany = new Company();
    this.hasError = false;
    // this.einIsChecked = false;
    // this.deltaAsReview = false;
    this.amendedTax1 = null;
    this.amendedTax2 = null;
    // this.validationNum = null;
  }


  private getAffectedDeltas() {

    let compareResult = this.headerData.compareResult;
    let fiscalYr = this.headerData.fiscalYear;
    let ein = this.deltaDetails.ein;

    if (this.affectedDeltaCntBusy) {
      this.affectedDeltaCntBusy.unsubscribe();
    }

    this.affectedDeltaCntBusy = this.atcomparisonservice.getAffectedDeltasForEin(this.actionType, ein, fiscalYr).subscribe(
      data => {
        if (data.length > 0) {
          this.affectedTTDeltasGrid = data;
          this.affectedDeltaCnt = this.affectedTTDeltasGrid.length;
          this.affectedTTDeltasGrid.forEach(delta => {
            delta.affectDeltaFlag = true;
          });
        }
      }
    );

    this.subscriptionArry.push(this.affectedDeltaCntBusy);
  }

  getAffectedDeltaCnt() {
    this.affectedDeltaCnt = 0;
    if (this.affectedTTDeltasGrid) {
      this.affectedTTDeltasGrid.forEach(delta => {
        if (delta.affectDeltaFlag) this.affectedDeltaCnt++;
      });
    }
    return this.affectedDeltaCnt;
  }

  isValidString(str) {
    return ((str != null) && (typeof str !== 'undefined') && (str.length >= 0));
  }

  isCmpdIngestTT() {
    return (this.headerData && this.actionType === "Market Share Ready" &&
      ((this.headerData.tobaccoClass === 'Chew/Snuff' || (this.headerData.tobaccoClass === 'Pipe/Roll-Your-Own') ||
        (this.headerData.tobaccoClass === 'Pipe/Roll Your Own')) ? true : false) &&
      this.quarterDeltaForOnesidedMap.get(this.panelQtr).dataType == 'INGESTED');
  }

  isManufacturerCig() {
    return this.headerData && this.headerData.tobaccoClass.toLocaleUpperCase().includes('CIG') && this.quarterDeltaForOnesidedMap.get(this.panelQtr).dataType == 'INGESTED';
  }

  hasCmpdIngestTTVol() {
    return (this.isCmpdIngestTT() && this.compoundTTTaxesVolume.length > 0);
  }

  hasNoVolAmmendTax() {
    return !((this.amendedTax1 != undefined && this.amendedTax1.toString() != "" && this.amendedTax1 >= 0)
      || (this.amendedTax2 != undefined && this.amendedTax2.toString() != "" && this.amendedTax2 >= 0));
  }

  hasNoCmpdIngestTTVol() {
    return (this.isCmpdIngestTT() && this.compoundTTTaxesVolume.length == 0);
  }

  hasVolAmmendTax() {
    if (this.compoundTTTaxesVolume.length == 2)
      return !((this.compoundTTTaxesVolume[0].amendedTax != undefined && this.compoundTTTaxesVolume[0].amendedTax.toString() != "" && this.compoundTTTaxesVolume[0].amendedTax >= 0)
        || (this.compoundTTTaxesVolume[1].amendedTax != undefined && this.compoundTTTaxesVolume[1].amendedTax.toString() != "" && this.compoundTTTaxesVolume[1].amendedTax >= 0));
    else if (this.compoundTTTaxesVolume.length == 1)
      return !((this.compoundTTTaxesVolume[0].amendedTax != undefined && this.compoundTTTaxesVolume[0].amendedTax.toString() != "" && this.compoundTTTaxesVolume[0].amendedTax >= 0));
  }

  public hasNoVolumnData() {
    if (this.compoundTTTaxesVolume.length > 0 && this.compoundTTTaxesVolume[0].pounds == null && this.compoundTTTaxesVolume[1].pounds == null)
      return true;
    return false;
  }

  affectdDeltasContainsPopUpData(): boolean {
    for (var data of this.affectedTTDeltasGrid) {
      if (this.isPopUpdata(data)) return true;
    }
    return false;
  }

  isPopUpdata(data: TrueUpComparisonResult): boolean {
    // if (this.panelQtr.toString() == data.quarter) {
    //   return true;
    // } else {
    //   return false;
    // }
    let tobaccoClassName: string = "";
    if (this.headerData.tobaccoClass === "Pipe/Roll Your Own") {
      tobaccoClassName = 'Pipe/Roll-Your-Own';
    } 
    else if (this.headerData.tobaccoClass === "Roll Your Own") {
      tobaccoClassName = 'Roll-Your-Own';
    } 
    else {
      tobaccoClassName = this.headerData.tobaccoClass;
    }
    return ((this.headerData.compareResult.ein == data.ein
      && this.headerData.compareResult.originalEIN == data.originalEIN) || (this.headerData.compareResult.ein == data.originalEIN) || (this.headerData.compareResult.ein == data.ein))
      && this.quarter.toString() == data.quarter
      && tobaccoClassName == data.tobaccoClass
      && (this.headerData.compareResult.permitType.toUpperCase().includes(["MANU", "Manufacturer"].indexOf(data.permitType) >= 0 ? "MANUFACTURER" : "IMPORTER"));

  }

  save() {
    if (this.actionType === 'Association') {
      this.associate();
      this.istufaMSReady = false;
    } else if (this.actionType === 'Exclude') {
      this.exclude();
      this.istufaMSReady = false;
    } else if (this.actionType === 'Market Share Ready' || this.headerData.compareResult.allDeltaStatus === 'Market Share Ready') {
      this.mktShrRdyDeltaAction();
    }

  }

  public mktShrRdyDeltaAction() {
    this.isAllActionSpan = false;
    let updateRptData = this.getDeltaStsChngObject("MSReady", "single");
    if (this.isCmpdIngestTT()) {
      this.tobaccoClassNames = this.headerData.tobaccoClass.split("/");
      let tobaccoSubTypesTxVol: TTBTaxesVol[] = [];
      if (this.compoundTTTaxesVolume.length > 0)
        tobaccoSubTypesTxVol = this.compoundTTTaxesVolume;
      else if (this.isCmpdIngestTT() && this.compoundTTTaxesVolume.length == 0) {
        let ttb1: TTBTaxesVol = {
          qtr: this.panelQtr.toString(), ein: this.headerData.compareResult.ein, tobaccoClass: this.tobaccoClassNames[0]==='Roll Your Own'? 'Roll-Your-Own' : this.tobaccoClassNames[0],
          pounds: null, fiscalYr: this.headerData.fiscalYear, estimatedTax: null, amendedTax: this.amendedTax1, tobaccoSubTypeAmndTx: null
        };
        tobaccoSubTypesTxVol.push(ttb1);
        let ttb2: TTBTaxesVol = {
          qtr: this.panelQtr.toString(), ein: this.headerData.compareResult.ein, tobaccoClass: this.tobaccoClassNames[1]==='Roll Your Own'? 'Roll-Your-Own' : this.tobaccoClassNames[1],
          pounds: null, fiscalYr: this.headerData.fiscalYear, estimatedTax: null, amendedTax: this.amendedTax2, tobaccoSubTypeAmndTx: null
        };
        tobaccoSubTypesTxVol.push(ttb2);
      }
      let tobaccoSubTypeAmndTxVol = {};

      for (let i in tobaccoSubTypesTxVol) {
        let ttTypeVoltx = tobaccoSubTypesTxVol[i];
        let txvolarry = [];
        txvolarry.push(ttTypeVoltx.amendedTax);
        txvolarry.push(ttTypeVoltx.pounds);
        tobaccoSubTypeAmndTxVol[ttTypeVoltx.tobaccoClass] = txvolarry;
      }
      updateRptData[0].tobaccoSubTypeTxVol = tobaccoSubTypeAmndTxVol;
    }

    this.updateCompareAllDeltaStatus(updateRptData, true);
  }

  private updateCompareAllDeltaStatus(updateRptData, isClosePopup) {

    this.saveDeltaBusy = this.atcomparisonservice.updateCompareAllDeltaStatus(updateRptData, this.headerData.fiscalYear).subscribe(
      data => {
        if (data) {
          // this.refreshCompareGrid();
          // setTimeout(() => {
          //     this.getAlldeltasComments();
          // }, 500);
          this.getOneSidedFlags(this.headerData.fiscalYear, this.headerData.companyId, this.headerData.tobaccoClass, this.headerData.permitType === 'Manufacturer' ? "MANU" : "IMP");
          //set the latest status into the map based on quarter back again with quarter
          //emit the status
          if (isClosePopup) {
            this.popUpClose();
          }
        }
      }
    );


    this.subscriptionArry.push(this.saveDeltaBusy);
  }


  getOneSidedFlags(fiscalYr, companyid, classId, type) {
    this.busy = this.atcomparisonservice.getOneSidedFlag(fiscalYr, companyid, classId, type, this.headerData.ein).subscribe(data => {
      if (data) {
        if (data['1-4'] != undefined && this.panelQtr == 5)
          this.oneSidedAcceptanceFlag = data['1-4'];
        else
          this.oneSidedAcceptanceFlag = data[this.panelQtr];

        this.annualTrueUpComparisonEventService.triggerAtCompareSuccessfullSaveActionOneSided([data,this.panelQtr]);
      }
    });
    this.subscriptionArry.push(this.busy);
  }

  private getNonCigarManuDetails(delta) {
    let classname: string = "";
    if (this.headerData.tobaccoClass.includes('Pipe')) {
      classname = 'Pipe/Roll-Your-Own';
    } else {
      classname = this.headerData.tobaccoClass;
    }
    let tobaccoClassName = classname.replace(/\//g, " ")
    this.compoundTTTaxesVolume = [];
    //TODO:need to change tobacco class name when multiple classes can be selected. currently it is taken from header data irrespective of quarter selected as 
    //class name will never change. same goes with EIN and Fiscal year as well
    this.busy = this.atcomparisonservice.getTTBVolTaxesPerTobaccoType(delta.ein, delta.fiscalYr, this.panelQtr, tobaccoClassName).subscribe(

      data => {
        let ttbVolTaxes: TTBTaxesVol[] = data;
        for (let i in ttbVolTaxes) {
          let ttbVolTx = new TTBTaxesVol();
          jQuery.extend(true, ttbVolTx, ttbVolTaxes[i]);
          this.compoundTTTaxesVolume.push(ttbVolTx);
          if (ttbVolTaxes[i].amendedTax == null) {

            if (ttbVolTaxes.length > 1 && ttbVolTaxes[i].pounds == null)
              ttbVolTx.amendedTax = null;
            else if (ttbVolTaxes.length > 1 && ttbVolTaxes[i].pounds !== null && this.allDeltaStatus == 'MSReady')
              ttbVolTx.amendedTax = null;
            else if (ttbVolTaxes.length > 1 && ttbVolTaxes[i].pounds !== null)
              ttbVolTx.amendedTax = Math.abs((ttbVolTaxes[i].estimatedTax / (ttbVolTaxes[0].estimatedTax + ttbVolTaxes[1].estimatedTax)) * Number(this.totalDelta));
            else
              ttbVolTx.amendedTax = this.roundDecimal(Math.abs(Number(this.totalDelta)));
          }
        }

      }
    )
    this.subscriptionArry.push(this.busy);

  }

  public roundDecimal(value, fractionSize = 2): number {
    let val = (value) ? value : 0;
    return Number(Math.round(Number(val + 'e' + fractionSize)) + 'e-' + fractionSize);
  }

  public inProgressAction() {
    this.isAllActionSpan = false;
    this.allDeltaStatus = this.quarterDeltaForOnesidedMap.get(this.panelQtr).allDeltaStatus;
    let oneSidedDelta = this.quarterDeltaForOnesidedMap.get(this.panelQtr);
    if (this.allDeltaStatus == 'Associated' || 
        (oneSidedDelta && oneSidedDelta.allDeltaStatus == 'Associated')) {
      this.resetAssociation();
    } else {
      let updateRptData = this.getDeltaStsChngObject("In Progress", "single");
      this.updateCompareAllDeltaStatus(updateRptData, true);
      this.istufaMSReady = false;
    }
  }
  resetAssociation() {
    let updateRptData = this.getDeltaStsChngObject("reset", "single");
    this.saveDeltaBusy = this.atcomparisonservice.updateCompareAllDeltaStatus(updateRptData, this.headerData.fiscalYear).subscribe(
      data => {
        if (data) {
          updateRptData = this.getDeltaStsChngObject("In Progress", "single");
          this.updateCompareAllDeltaStatus(updateRptData, true);
        }
      }
    );


    this.subscriptionArry.push(this.saveDeltaBusy);

  }
  inputValidator(event: any) {
    const pattern = /^[0-9]\d{0,9}(?:\.\d{1,2})?$/;
    const pattern1 = /^[0-9]\d{0,9}(?:\.)?$/;
    if (event.target.value !== "") {
      if (pattern1.test(event.target.value)) {
        event.target.value = event.target.value;
        this.validationNum = event.target.value;
      }
      else if (!pattern.test(event.target.value)) {
        event.target.value = event.target.value.replace(event.target.value, this.validationNum);
      }
      this.validationNum = event.target.value;
    }
  }
  storeCurrentValue(event: any) {
    this.validationNum = event.target.value;
  }

  isActionTakenAlreadyForQuarter() {
    //check if the quarter selected for an action is already been taken.
    return false;
  }

  isMSReadyManu(oneSidedAcceptance : string){
    if((oneSidedAcceptance === 'MSReady' || oneSidedAcceptance === 'MSReadyIngested') && this.isManufacturer())
      return true;
    else if (oneSidedAcceptance == 'MSReadyTufa' || oneSidedAcceptance === 'MSReadyIngested')
      return false;
    else if (oneSidedAcceptance === undefined)
      return true;
    else
      return true;
  }

  isManufacturer(){
    if(this.headerData.permitType.toUpperCase() === 'MANUFACTURER' || this.headerData.permitType.toUpperCase() === 'MANU')
      return true;
    else return false;
  }

  isCigarCigarrettesManu(){
    return (this.isManufacturer() && (this.tobaccoClass.toUpperCase() === 'CIGARETTES' || this.tobaccoClass.toUpperCase() === 'CIGARS'));
  }

  isCigarManu(){
    return (this.isManufacturer() && this.tobaccoClass.toUpperCase() === 'CIGARS');
  }

  continueclicked(){
    this.router.navigate(["./app/assessments/annual/" + this._shareData.fiscalYear]);
  } 
}

import { Details } from "../../../models/at-compare-details.model";
import { AnnualTrueUpComparisonDetailsHeader } from "../../../models/at-detailsheader.model";

export interface AtBaseImptCompareDetails {
    details: Details;
    headerData: AnnualTrueUpComparisonDetailsHeader;
    acceptanceFlag: string;
    quarter: number;
    selectedDt: string;
    context: string;
    oneSidedAcceptanceFlag: string;
}

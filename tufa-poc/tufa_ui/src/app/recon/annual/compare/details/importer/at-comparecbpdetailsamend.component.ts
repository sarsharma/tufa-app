import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import { Subscription } from 'rxjs';
import { AdminService } from '../../../../../admin/services/admin.service';
import { ShareDataService } from '../../../../../core/sharedata.service';
import { CustomNumberPipe } from '../../../../../shared/util/custom.number.pipe';
import { AcceptanceFlagState } from '../../../models/at-acceptance-flag-state.model';
import { CBPAmendment } from '../../../models/at-cbpamendment.model';
import { AnnualTrueUpComparisonDetailsHeader } from '../../../models/at-detailsheader.model';
import { AnnualTrueUpComparisonEventService } from '../../../services/at-compare.event.service';
import { AnnualTrueUpComparisonService } from '../../../services/at-compare.service';
import { AtReallocationService } from '../../../services/at-reallocation.service';
import { MixedAction } from '../../../models/at-mixedaction.model';

declare let jQuery: any;

@Component({
    selector: '[at-comparecbpdetailsamend]',
    templateUrl: './at-comparecbpdetailsamend.template.html',
    styleUrls: ['at-comparecbpdetailsamend.style.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [CustomNumberPipe, AdminService]
})

export class AnnualTrueUpCBPCompareDetailsAmendComponent implements OnInit, OnDestroy {
    @Input() amendInfo;
    @Input() quarterInfo;
    @Input() headerdata: AnnualTrueUpComparisonDetailsHeader;
    @Input() acceptedFlag;
    @Input() ingestRefresh;

    @Input() acceptanceFlag;

    taxRateData: any;

    savedAmendment: boolean = false;
    activeqtr;
    pristine: boolean = true;
    reviewamendedTotalTax: any;
    reviewamendedTotalVolume: any;

    @Output() amendTotalUpdated = new EventEmitter<boolean>();
    @Output() saveAmend = new EventEmitter<boolean>();
    amendTotalByQtrByTT = [];

    quarter: any;
    amenddata: any;
    amendTotal: boolean = false;
    busy: Subscription;
    fdaAcceptSubscription: Subscription;

    CIGAR_QUARTER = 5;
    //router: Router;

    disableAmendApprove: boolean = false;
    ttIdxMp = { "Cigars": 0, "Cigarettes": 1, "Chew": 2, "Snuff": 3, "Pipe": 4, "Roll Your Own": 5 }
    subscriptions: Subscription[] = [];

    mixedActionFlag: string;
    mixedAction: MixedAction;

    constructor(private annualTrueUpComparisonService: AnnualTrueUpComparisonService,
        private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
        private reallocationEventService: AtReallocationService,
        public router: Router, private numberPipe: CustomNumberPipe, private _adminService: AdminService, public _shareData: ShareDataService) {

        //Disable amend/approve button
        //this.disableAmendApprove = _shareData.disableAmendApprove;
        if (_shareData.disableAmendApprove) {
            jQuery('#amd-tot-inputs-' + this.quarter + ' input[type="text"]').prop('disabled', true);
        }
        this.amendTotalByQtrByTT[0] = this.newAmndArray();
        this.amendTotalByQtrByTT[1] = this.newAmndArray();
        this.amendTotalByQtrByTT[2] = this.newAmndArray();
        this.amendTotalByQtrByTT[3] = this.newAmndArray();
        this.amendTotalByQtrByTT[4] = this.newAmndArray();

        this.subscriptions.push(this.reallocationEventService.reallocateEntrySave$.subscribe(
            data => {
                this.loadRates();
            }
        ));

        this.subscriptions.push(this.annualTrueUpComparisonEventService.atCompareActiveQuarterUpdateSource$.subscribe(
            actqtr => {
                this.activeqtr = actqtr;
            }
        ));

        this.subscriptions.push(this.annualTrueUpComparisonEventService.atCBPCompareAcceptFDASource$.subscribe(
            data => {

                let redirectCompareLanding: Boolean = data[1];
                let progressflag = data[2];
                let fda = data[3];
                this.reviewamendedTotalTax = data[4];
                this.reviewamendedTotalVolume = data[5];

                if (progressflag) {
                    this.amendTotalUpdated.emit(false);
                    this.savedAmendment = false;
                    this.amendTotal = false;
                }

                let amdArry = [];
                if (this.headerdata.tobaccoClass !== "Cigars" && this.activeqtr != this.quarter) return;

                else if (this.headerdata.tobaccoClass === "Cigars" && this.quarter != this.CIGAR_QUARTER) return;

                if (this.headerdata.tobaccoClass.indexOf("Pipe") != -1) {
                    let amd1 = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Pipe"]], "Pipe");

                    if (progressflag) {
                        amd1.inProgressFlag = 'Y';
                        amd1.acceptanceFlag = null
                    }
                    else if (fda) {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'Y';
                    } else {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'I';
                    }

                    amd1.qtr = this.quarter;
                    amdArry.push(amd1);
                } else if (this.headerdata.tobaccoClass.indexOf("Roll Your Own") != -1) {
                    let amd1 = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Roll Your Own"]], "Roll-Your-Own");
                    if (progressflag) {
                        amd1.inProgressFlag = 'Y';
                        amd1.acceptanceFlag = null
                    }
                    else if (fda) {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'Y';
                    } else {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'I';
                    }
                    amd1.qtr = this.quarter;
                    amdArry.push(amd1);
                } else if (this.headerdata.tobaccoClass.indexOf("Chew") != -1) {
                    let amd1 = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Chew"]], "Chew");
                    if (progressflag) {
                        amd1.inProgressFlag = 'Y';
                        amd1.acceptanceFlag = null
                    }
                    else if (fda) {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'Y';
                    } else {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'I';
                    }
                    amd1.qtr = this.quarter;
                    amdArry.push(amd1);
                } else if (this.headerdata.tobaccoClass.indexOf("Snuff") != -1) {
                    let amd1 = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Snuff"]], "Snuff");
                    if (progressflag) {
                        amd1.inProgressFlag = 'Y';
                        amd1.acceptanceFlag = null
                    }
                    else if (fda) {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'Y';
                    } else {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'I';
                    }
                    amd1.qtr = this.quarter;
                    amdArry.push(amd1);
                } else if (this.headerdata.tobaccoClass.indexOf("Cigarettes") != -1) {
                    let amd1 = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Cigarettes"]], "Cigarettes");
                    if (progressflag) {
                        amd1.inProgressFlag = 'Y';
                        amd1.acceptanceFlag = null
                    }
                    else if (fda) {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'Y';
                    } else {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'I';
                    }
                    amd1.qtr = this.quarter;
                    amdArry.push(amd1);
                } else if (this.headerdata.tobaccoClass.indexOf("Cigars") != -1) {
                    let amd1 = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.quarter - 1][this.ttIdxMp["Cigars"]], "Cigars");
                    if (progressflag) {
                        amd1.inProgressFlag = 'Y';
                        amd1.acceptanceFlag = null
                    }
                    else if (fda) {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'Y';
                    } else {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'I';
                    }

                    if (fda) {
                        amd1.reviewFlag = 'N';
                    }

                    amd1.qtr = this.quarter;
                    amdArry.push(amd1);
                }

                for (let i in amdArry) {
                    if (amdArry[i].amendedTotalTax) amdArry[i].amendedTotalTax = amdArry[i].amendedTotalTax.replace(/,/g, "");
                    if (amdArry[i].amendedTotalVolume) amdArry[i].amendedTotalVolume = amdArry[i].amendedTotalVolume.replace(/,/g, "");
                }



                this.busy = this.annualTrueUpComparisonService.updateCBPCompareAcceptFDA(amdArry, this.headerdata.fiscalYear).subscribe(
                    data => {
                        if (data) {
                            for (let i in data) {
                                let qtr: number = data[i].qtr;
                                let ttname = data[i].tobaccoType.replace(/-/g, " ");
                                // let inprogress: boolean = data[i].inProgressFlag;
                                let amdmnt = new CBPAmendment();
                                jQuery.extend(true, amdmnt, data[i]);
                                this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]] = amdmnt;
                                if (amdmnt.reviewFlag = 'Y') {
                                    this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]].amendedTotalTax = null;
                                    this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]].amendedTotalVolume = null;
                                }

                                this.annualTrueUpComparisonEventService.triggerAtCompareSuccessfullSaveAction(data);
                            }
                            // this.annualTrueUpComparisonEventService.triggerAtCBPCompareCommentsUpdate(data);

                            if (redirectCompareLanding)
                                this.router.navigate(["./app/assessments/annual/" + this._shareData.fiscalYear]);
                        }
                    }
                )

            }
        ));

        this.subscriptions.push(this.annualTrueUpComparisonEventService.atCBPCompareCommentsSaveSource$.subscribe(
            data => {
                let amnds: CBPAmendment[] = data;
                for (let i in amnds) {
                    let qtr = amnds[i].qtr;
                    let ttname = amnds[i].tobaccoType.replace(/-/g, " ");
                    this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]].comments = amnds[i].comments;
                    if (!this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]].cbpAmendmentId)
                        this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]].cbpAmendmentId = amnds[i].cbpAmendmentId;
                }
            }));

        this.subscriptions.push(this.annualTrueUpComparisonEventService.tobaccoClassSelected$.subscribe(
            data => {
                this.getTotalAmendment();
            }
        ));

        this.subscriptions.push(this.annualTrueUpComparisonEventService.atCompareAcceptMixedAction$.subscribe(
            data => {
                this.mixedActionFlag = data[0];
                this.mixedAction = data[1];
                if(this.activeqtr != this.quarter) return;
                this.saveMixedAction();
            }));
    }



    private newAmndArray() {
        let amndForTTArry: CBPAmendment[] = [];
        amndForTTArry[0] = this.newAdmt();
        amndForTTArry[1] = this.newAdmt();
        amndForTTArry[2] = this.newAdmt()
        amndForTTArry[3] = this.newAdmt()
        amndForTTArry[4] = this.newAdmt()
        amndForTTArry[5] = this.newAdmt()
        return amndForTTArry;
    }

    ngOnInit() {
        this.amendTotal = false;
        this.loadRates();
    }

    ngAfterViewInit(): void {
        this.enableAmendTotal(this.amendTotal);
    }

    ngOnChanges(changes: SimpleChanges) {
        // tslint:disable-next-line:forin
        for (let propName in changes) {
            if (propName === "quarterInfo") {
                this.quarter = changes[propName].currentValue;
            }
            if (propName === "amendInfo") {
                this.amenddata = changes[propName].currentValue;
            }
            if (propName === "acceptedFlag") {
                if (changes[propName].currentValue)
                    this.savedAmendment = false;
            }

            if (propName === "acceptanceFlag") {
                this.acceptanceFlag = changes[propName].currentValue;
                this.disableAmendApprove = this.acceptanceFlag === AcceptanceFlagState.DELTA_CHANGE_ZERO;

                if ([AcceptanceFlagState.DELTA_CHANGE, AcceptanceFlagState.DELTA_CHANGE_ZERO, AcceptanceFlagState.DELTA_CHANGE_EXCLUDE].includes(changes[propName].currentValue)) {
                    this.amendTotal = false;
                    this.savedAmendment = false;
                }
            }

            if (propName === "ingestRefresh") {
                let refreshvalue = changes[propName].currentValue;
                if (refreshvalue === 1) {
                    this.amendTotal = false;
                    this.savedAmendment = false;
                    this.amendTotalUpdated.emit(false);
                }
            }
        }
    }

    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }

        if (this.fdaAcceptSubscription) {
            this.fdaAcceptSubscription.unsubscribe();
        }
        for (let i in this.subscriptions) {
            this.subscriptions[i].unsubscribe();
        }
    }


    enableAmendtotalToggle(enable: boolean) {
        //jQuery('#amend-total-'+this.quarter).bootstrapSwitch('disabled',!enable);
    }

    enableAmendTotal(event: any) {
        if (this.disableAmendApprove) {
            return;
        }
        /*
        jQuery('#btn-amd-save-' + this.quarter).prop('disabled', !event);
        jQuery('#btn-amd-cont-' + this.quarter).prop('disabled', !event);*/
        jQuery('#amd-tot-inputs-' + this.quarter + ' input[type="text"]').prop('disabled', !event);
        this.amendTotalUpdated.emit(event);
    }

    changeAmend($event) {
        this.savedAmendment = false;
        this.pristine = false;
    }

    taxAmountChange(amendment: CBPAmendment, tobaccoClassNm: string) {
        this.savedAmendment = false;
        if (this.pristine && tobaccoClassNm.indexOf("Cigars") == -1) {
            amendment.amendedTotalVolume = this.numberPipe.transform(Number(amendment.amendedTotalTax) / this.tobaccoReturnType(tobaccoClassNm, this.taxRateData).taxRate, 2);
        }
    }

    loadRates() {
        this._adminService.getTobaccoClasses().subscribe(tobaccoClasses => {
            if (tobaccoClasses) {
                this.taxRateData = tobaccoClasses;
                this.getTotalAmendment();
            }
        });
    }

    public getTotalAmendment() {

        let amdmnts: CBPAmendment[] = [];
        if (this.headerdata.tobaccoClass === "Chew") {
            amdmnts[0] = this.newAdmt("Chew", this.quarter)
        } else if (this.headerdata.tobaccoClass === "Snuff") {
            amdmnts[0] = this.newAdmt("Snuff", this.quarter);
        } else if (this.headerdata.tobaccoClass === "Pipe") {
            amdmnts[0] = this.newAdmt("Pipe", this.quarter)
        } else if (this.headerdata.tobaccoClass === "Roll Your Own") {
            amdmnts[0] = this.newAdmt("Roll-Your-Own", this.quarter);
        } else if (this.headerdata.tobaccoClass === "Cigarettes") {
            amdmnts[0] = this.newAdmt("Cigarettes", this.quarter);
        } else if (this.headerdata.tobaccoClass === "Cigars") {
            amdmnts[0] = this.newAdmt("Cigars", this.quarter);
        }

        const currentQtr = this.quarter;
        this.annualTrueUpComparisonService.getCBPAmendments(Number(this.headerdata.fiscalYear), Number(this.headerdata.companyId), amdmnts).subscribe(
            data => {
                if (data) {
                    this.amendTotalByQtrByTT = [];
                    this.amendTotalByQtrByTT[0] = this.newAmndArray();
                    this.amendTotalByQtrByTT[1] = this.newAmndArray();
                    this.amendTotalByQtrByTT[2] = this.newAmndArray();
                    this.amendTotalByQtrByTT[3] = this.newAmndArray();
                    this.amendTotalByQtrByTT[4] = this.newAmndArray();
                    for (let i in data) {
                        if (data[i].amendmentPresent) {

                            data[i].amendedTotalTax = this.numberPipe.transform(data[i].amendedTotalTax);
                            data[i].amendedTotalVolume = this.numberPipe.transform(data[i].amendedTotalVolume);


                            let ttname = data[i].tobaccoType.replace(/-/g, " ");
                            let qtr = (ttname === 'Cigars') ? this.CIGAR_QUARTER : data[i].qtr;
                            let amdmnt = new CBPAmendment();
                            jQuery.extend(true, amdmnt, data[i]);
                            this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]] = amdmnt;

                            if (data[i].reviewFlag === 'Y') {
                                this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]].amendedTotalTax = null;
                                this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]].amendedTotalVolume = null;
                            }
                            else if (this.acceptanceFlag !== 'Amended') {
                                this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]].amendedTotalTax = null;
                                this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]].amendedTotalVolume = null;
                            }

                            if (currentQtr === Number(qtr) && data[i].acceptanceFlag === 'N') {
                                this.savedAmendment = true;
                                this.enableAmendTotal(true);
                                this.amendTotal = true;
                            } else if (currentQtr === Number(qtr) && (data[i].acceptanceFlag === 'Y' || (data[i].acceptanceFlag === 'X' && !this.disableAmendApprove) || !data[i].acceptanceFlag)) {
                                this.amendTotalUpdated.emit(false);
                            }
                        }

                        if (data.length == 0) {
                            this.amendTotalUpdated.emit(false);
                        }
                        this.annualTrueUpComparisonEventService.triggerAtCBPCompareCommentsUpdate(data);
                    }

                    this.annualTrueUpComparisonEventService.triggerAtCompareAmendmentsLoaded(true);
                }

            }
        )
    }

    private newAdmt(ttname?: string, quarter?: number) {
        let amdnt = new CBPAmendment();
        if (ttname) amdnt.tobaccoType = ttname;
        if (quarter) amdnt.qtr = quarter;
        return amdnt;
    }


    public saveTotalAmendment(redirectCompareLanding?: boolean) {
        /**Temporary check to return if CBP details page. NEEDS TO BE  REMOVED when CBP Details Page User story is up for implementation */
        if (this.headerdata.permitType === 'Manufacturer') return;

        this.reviewamendedTotalTax = null;
        this.reviewamendedTotalVolume = null;

        let amndmnts: CBPAmendment[] = [];
        if (this.headerdata.tobaccoClass === "Chew") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Chew"]], "Chew");
        } else if (this.headerdata.tobaccoClass === "Snuff") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Snuff"]], "Snuff");
        } else if (this.headerdata.tobaccoClass === "Pipe") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Pipe"]], "Pipe");
        } else if (this.headerdata.tobaccoClass === "Roll Your Own") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Roll Your Own"]], "Roll-Your-Own");
        } else if (this.headerdata.tobaccoClass === "Cigarettes") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Cigarettes"]], "Cigarettes");
        } else if (this.headerdata.tobaccoClass === "Cigars") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.quarter - 1][this.ttIdxMp["Cigars"]], "Cigars", this.quarter);
        }
        for (let i in amndmnts) {
            amndmnts[i].acceptanceFlag = 'N';
            amndmnts[i].inProgressFlag = 'N';
            amndmnts[i].reviewFlag = 'N';
            if (amndmnts[i].amendedTotalTax) amndmnts[i].amendedTotalTax = amndmnts[i].amendedTotalTax.toString().replace(/,/g, "");
            if (amndmnts[i].amendedTotalVolume) amndmnts[i].amendedTotalVolume = amndmnts[i].amendedTotalVolume.toString().replace(/,/g, "");
        }


        const currentQtr = this.quarter;
        this.busy = this.annualTrueUpComparisonService.saveCBPAmendments(amndmnts, this.headerdata.fiscalYear).subscribe(
            data => {
                if (data) {
                    for (let i in data) {


                        data[i].amendedTotalTax = this.numberPipe.transform(data[i].amendedTotalTax);
                        data[i].amendedTotalVolume = this.numberPipe.transform(data[i].amendedTotalVolume);

                        let qtr: number = data[i].qtr;
                        let ttname = data[i].tobaccoType.replace(/-/g, " ");
                        let amdmnt = new CBPAmendment();
                        jQuery.extend(true, amdmnt, data[i]);
                        this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]] = amdmnt;
                        if (currentQtr === Number(qtr) && data[i].acceptanceFlag === 'N') {
                            this.savedAmendment = true;
                        } else { this.savedAmendment = false; }
                    }
                    // this.annualTrueUpComparisonEventService.triggerAtCBPCompareCommentsUpdate(data);
                    this.saveAmend.emit(true);

                    this.annualTrueUpComparisonEventService.triggerAtCompareSuccessfullSaveAction(data);

                    if (redirectCompareLanding)
                        this.router.navigate(["/app/assessments/annual/" + this.headerdata.fiscalYear]);
                }
            }
        );

    }

    public saveMixedAction() {
        /**Temporary check to return if CBP details page. NEEDS TO BE  REMOVED when CBP Details Page User story is up for implementation */
        if (this.headerdata.permitType === 'Manufacturer') return;

        if (this.activeqtr != this.quarter) return;

        this.reviewamendedTotalTax = null;
        this.reviewamendedTotalVolume = null;

        let amndmnts: CBPAmendment[] = [];
        if (this.headerdata.tobaccoClass === "Chew") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Chew"]], "Chew");
        } else if (this.headerdata.tobaccoClass === "Snuff") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Snuff"]], "Snuff");
        } else if (this.headerdata.tobaccoClass === "Pipe") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Pipe"]], "Pipe");
        } else if (this.headerdata.tobaccoClass === "Roll Your Own") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Roll Your Own"]], "Roll-Your-Own");
        } else if (this.headerdata.tobaccoClass === "Cigarettes") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Cigarettes"]], "Cigarettes");
        } else if (this.headerdata.tobaccoClass === "Cigars") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.quarter - 1][this.ttIdxMp["Cigars"]], "Cigars", this.quarter);
        }

        if(this.mixedActionFlag && this.mixedActionFlag===AcceptanceFlagState.MIXEDACTION){
            for (let i in amndmnts) {
                amndmnts[i].acceptanceFlag = 'M';
                amndmnts[i].inProgressFlag = 'N';
                amndmnts[i].reviewFlag = 'N';
                amndmnts[i].amendedTotalTax = this.mixedAction.amendedTotalTax;
                amndmnts[i].amendedTotalVolume = this.mixedAction.amendedTotalVolume;
                amndmnts[i].mixedActionQuarterDetails = {};
                this.mixedAction.mixedActionDetailsMap.forEach((value: string, key: string) =>{
                    amndmnts[i].mixedActionQuarterDetails[key] = value;
                })
            }
        }

        const currentQtr = this.quarter;
        this.busy = this.annualTrueUpComparisonService.saveCBPAmendments(amndmnts, this.headerdata.fiscalYear).subscribe(
            data => {
                if (data) {
                    for (let i in data) {


                        data[i].amendedTotalTax = this.numberPipe.transform(data[i].amendedTotalTax);
                        data[i].amendedTotalVolume = this.numberPipe.transform(data[i].amendedTotalVolume);

                        let qtr: number = data[i].qtr;
                        let ttname = data[i].tobaccoType.replace(/-/g, " ");
                        let amdmnt = new CBPAmendment();
                        jQuery.extend(true, amdmnt, data[i]);
                        this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]] = amdmnt;
                        if (currentQtr === Number(qtr) && data[i].acceptanceFlag === 'N') {
                            this.savedAmendment = true;
                        } else { this.savedAmendment = false; }
                    }
                    // this.annualTrueUpComparisonEventService.triggerAtCBPCompareCommentsUpdate(data);
                    this.saveAmend.emit(true);

                    this.annualTrueUpComparisonEventService.triggerAtCompareSuccessfullSaveAction(data);
                    this.annualTrueUpComparisonEventService.triggerAtCompareMixedActionRefresh(data);
                }
            }
        );

    }

    saveContTotalAmendment() {
        this.saveTotalAmendment(true);
    }

    public populateIfNewAmnd(amndmnt: CBPAmendment, ttname, quarter?: number) {
        amndmnt.fiscalYr = Number(this.headerdata.fiscalYear);
        amndmnt.qtr = quarter ? quarter : this.activeqtr;
        amndmnt.cbpCompanyId = this.headerdata.companyId;
        amndmnt.tobaccoType = ttname;

        if (this.reviewamendedTotalTax != null && this.reviewamendedTotalVolume != null && !this.amendTotal) {

            amndmnt.amendedTotalTax = this.reviewamendedTotalTax;
            amndmnt.amendedTotalVolume = this.reviewamendedTotalVolume;
            amndmnt.reviewFlag = 'Y';
        }
        else {

            amndmnt.reviewFlag = 'N';
        }

        return amndmnt;
    }

    continueWithoutSave() {
        this.router.navigate(["/app/assessments/annual/" + this.headerdata.fiscalYear]);


    }

    public tobaccoReturnType(className: string, rateObj: any) {
        for (let key in rateObj) {
            if (rateObj.hasOwnProperty(key) && rateObj[key].tobaccoClassNm.replace(/-/g, " ") === className) {
                return rateObj[key];
            }
        }
    }
}

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import * as _ from 'lodash';
import { Subscription } from "rxjs";
import { ShareDataService } from '../../../../../core/sharedata.service';
import { PermitPeriodService } from '../../../../../permit/services/permit-period.service';
import { TufaCurrencyPipe } from '../../../../../shared/custompipes/custom.currency.pipe';
import { MinusToParensPipe } from '../../../../../shared/custompipes/minus.parentheses.pipe';
import { DownloadSerivce } from '../../../../../shared/service/download.service';
import { AcceptanceFlagState } from '../../../models/at-acceptance-flag-state.model';
import { AnnualTrueUpImporterDetail } from '../../../models/at-compare-importerdetails.model';
import { SummarySet } from '../../../models/at-summary-set.model';
import { AnnualTrueUpComparisonDetailsHeader } from "./../../../../../recon/annual/models/at-detailsheader.model";
import { ReallocateModel } from "./../../../../../recon/annual/models/at-reallocate.model";
import { ToggleTabModel } from "./../../../../../recon/annual/models/at-toggletab.model";
import { AnnualTrueUpComparisonEventService } from "./../../../../../recon/annual/services/at-compare.event.service";
import { AnnualTrueUpComparisonService } from "./../../../../../recon/annual/services/at-compare.service";

declare let jQuery: any;

@Component({
    selector: '[at-compareimpdetails]',
    templateUrl: './at-compareimpdetails.template.html',
    styleUrls: ['at-compareimpdetails.style.scss'],
    providers: [PermitPeriodService],
    encapsulation: ViewEncapsulation.None
})


export class AnnualTrueUpCompareImporterDetailsComponent implements OnInit, OnDestroy {
    @Input() quarter;
    @Input() fda3852map;
    @Output() manageEntry = new EventEmitter<ReallocateModel>();
    @Output() toggleTab = new EventEmitter<ToggleTabModel>();
    @Input() ingestRefresh: number;

    context = 'cbpdetail';

    router: Router;
    alerts: Array<Object>;
    oneAtATime: boolean = false;
    // showReallocateEntry: boolean = false;
    showActionRequired: boolean;
    actionalerts: Array<Object>;
    shareData: ShareDataService;

    inProgressDisabled: boolean;
    isValid: boolean = false;
    flagParam: string;

    busySummary: Subscription;
    busyStatus: Subscription;

    busyFDADetails: Subscription;
    busyIngestedDetails: Subscription;
    busyHTSCodeSummary: Subscription;
    activequarter: any;

    public sortResultsBy = "lineNum";
    public sortResultsOrder = "asc";
    public pageNumPeriodDetails: number = 1;

    createdDate: string;
    submitted: string;
    summarydelta: string;
    acceptedIngestedFlag: boolean = false;
    acceptedFDAFlag: boolean = false;
    inprogressFlag: boolean = false;
    ingestedAssociationFlag: boolean = false;
    allNSReports: boolean = false;

    headerdata: AnnualTrueUpComparisonDetailsHeader = new AnnualTrueUpComparisonDetailsHeader();
    comparedetailsSummary: any;
    comparedetailsIngestedSummary: SummarySet = new SummarySet();
    comparedetailsdeltabymonth: any[];
    compareFDADetails: any;

    compareIngestedDetails: AnnualTrueUpImporterDetail[];
    compareFDADetailsTotal: any;
    compareIngestionDetailsTotal: any;
    delta: any;
    deltaExciseTaxFlag: any;
    htsCodeIngestionSummary: any;
    amendTobaccoClasses: any;
    NSStarted: any[];
    nullperiodDetails: any[];
    delta1: any;
    delta2: any;
    delta3: any;
    delta4: any;

    constructor(
        router: Router,
        private _permitPeriodService: PermitPeriodService,
        private atcomparisonservice: AnnualTrueUpComparisonService,
        private _shareData: ShareDataService,
        private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
        private _downloadService: DownloadSerivce,
        private currencyPipe: TufaCurrencyPipe,
        private minusToParenthese: MinusToParensPipe) {

        this.alerts = [
            {
                type: 'success',
                msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Message</span>'
            }
        ];

        this.showActionRequired = false;
        this.actionalerts = [];

        this.compareFDADetailsTotal = 0.0;
        this.compareIngestionDetailsTotal = 0.0;

        this.router = router;
        this.shareData = _shareData;
        this.readfromSharedData();
    }

    ngOnInit() {
        this.alerts = [];
        this.actionalerts = [];
        this.ingestRefresh = 0;
    }

    ngAfterViewInit() {
    }

    ngOnDestroy() {

        if (this.busySummary) {
            this.busySummary.unsubscribe();
        }
        if (this.busyFDADetails) {
            this.busyFDADetails.unsubscribe();
        }
        if (this.busyIngestedDetails) {
            this.busyIngestedDetails.unsubscribe();
        }
        if (this.busyHTSCodeSummary) {
            this.busyHTSCodeSummary.unsubscribe();
        }
        jQuery('.modal-backdrop').removeClass('modal-backdrop');
    }

    reallocateEntry(detail: any, month: any) {
        let lineiteminfo = new ReallocateModel();
        lineiteminfo.entryId = detail.entryId;
        lineiteminfo.date = detail.entrysummaryDate;
        lineiteminfo.entryNum = detail.entryNum;
        lineiteminfo.lineNum = detail.lineNum;
        lineiteminfo.qty = detail.qtyRemoved;
        lineiteminfo.tax = detail.taxAmount;
        lineiteminfo.year = this.shareData.fiscalYear;
        lineiteminfo.month = month;
        lineiteminfo.excludedFlag = detail.excludedFlag;
        this.manageEntry.emit(lineiteminfo);
    }

    toAnnualTrueUp() {
        this.router.navigate(["/./../../../assessments/annual/" + this.shareData.fiscalYear]);
    }

    toAssessments() {
        this.router.navigate(["/app/assessments"]);
    }

    setParamsForReconNavigation() {
        this.shareData.reset();
    }



    readfromSharedData() {
        this.headerdata.companyName = this.shareData.companyName;
        this.headerdata.fiscalYear = this.shareData.fiscalYear.toString();
        this.headerdata.tobaccoClass = this.shareData.tobaccoClass;
        this.headerdata.permitType = this.shareData.permitType;
        this.headerdata.companyId = this.shareData.companyId;
        //this.headerdata.quarter = this.shareData.quarter;
        this.createdDate = this.shareData.createdDate;
        this.submitted = this.shareData.submitted;
        this.deltaExciseTaxFlag = this.shareData.deltaExciseTaxFlag;

        // this.activequarter = this.shareData.quarter;
        if (this.headerdata.tobaccoClass === "Chew/Snuff") {
            this.amendTobaccoClasses = [{ "name": "Chew" }, { "name": "Snuff" }];
        } else if (this.headerdata.tobaccoClass === "Pipe/Roll Your Own") {
            this.amendTobaccoClasses = [{ "name": "Pipe" }, { "name": "Roll Your Own" }];
        } else if (this.headerdata.tobaccoClass === "Roll-Your-Own") {
            this.amendTobaccoClasses = [{ "name": "Roll Your Own" }];
            this.headerdata.tobaccoClass = "Roll Your Own";
        } else {
            this.amendTobaccoClasses = [{ "name": this.headerdata.tobaccoClass }];
        }
        
        if(this.shareData.originalEIN){
            this.headerdata.ein = this.shareData.originalEIN;
            this.headerdata.companyName = this.shareData.originalName;
            this.headerdata.companyId = 0;
        }else{
            this.headerdata.companyId = this.shareData.companyId;
            this.headerdata.ein = this.shareData.ein;
            this.headerdata.companyName = this.shareData.companyName;
           
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        //console.log("ngOnChanges");
        for (let propName in changes) {
            if (propName === "quarter") {
                this.activequarter = changes[propName].currentValue;

                this.getDetailsSummary();
                this.getFDA3852Details();
                // this.getDeltabyMonth();
                this.getHTSCodeSummary();
                this.associatedIngestedDeltaFlag();
                //this.isValid = false;
            }
            if (propName === "ingestRefresh") {

                let refreshvalue = changes[propName].currentValue;
                if (refreshvalue === 1) {
                    this.getDetailsSummary();
                    // this.getDeltabyMonth();
                    this.getHTSCodeSummary();
                    this.associatedIngestedDeltaFlag();
                    //this.isValid = false;                                
                }
            }
        }
    }

    isNonCigar(tobaccoClass: any) {
        if (tobaccoClass.toUpperCase() === "CIGARS") {
            return false;
        }
        return true;
    }


    disableAcceptanceApprove() {
        let amendFlag = this.fda3852map[this.quarter];
        if (((!amendFlag || amendFlag === "") && Math.abs(Number(this.comparedetailsIngestedSummary.summaryTotal.tufaAndSummaryDiff)) < 1) || this.isOneSided()) {
            this.fda3852map[this.quarter] = AcceptanceFlagState.DELTA_CHANGE_ZERO;
        }
    }

    getDetailsSummary() {

        if (this.busySummary) {
            this.busySummary.unsubscribe();
        }

        this.busySummary = this.atcomparisonservice.getImporterComparisonDetailsSummary(this.shareData.companyId,
            this.shareData.fiscalYear, this.activequarter, this.headerdata.tobaccoClass.replace(/ /gi, "-")).subscribe(
                data => {
                    if (data) {
                        this.comparedetailsIngestedSummary = data;

                        if (this.headerdata.tobaccoClass.toUpperCase() !== "CIGARS") {
                            this.getDelta();
                        }

                        this.getIngestedDetails();
                        this.disableAcceptanceApprove();
                    }
                },
                error => {
                    this.alerts = [];
                    this.alerts.push({ type: 'warning', msg: error });
                }
            );
    }

    enableLinkAuth(flag) {
        this._permitPeriodService.setLinkAuthentication(flag);
    }

    getFDA3852Details() {

        if (this.busyFDADetails) {
            this.busyFDADetails.unsubscribe();
        }
        //alert("getFDA3852Details:"+this.headerdata.ein);
        this.busyFDADetails =
            // tslint:disable-next-line:max-line-length
   
            this.atcomparisonservice.getImporterComparisonDetails(this.headerdata.ein, this.shareData.fiscalYear,
                this.activequarter, this.headerdata.tobaccoClass).subscribe(data => {
                    if (data) {
                        this.compareFDADetails = data;
                        this.nullperiodDetails = _.filter(data, s => s.periodDetails);
                        this.NSStarted = _.filter(data, { periodDetails: [{ status: 'NS' }] })
                        //   this.compareFDADetailsTotal = _.sum(_.map(this.compareFDADetails, s => s.periodTotal));
                        // this.compareFDADetailsTotal = _.sum(_.map(_.reject(this.compareFDADetails, ['periodTotal', 'NA']), s => s.periodTotal));
                        this.compareFDADetailsTotal = _.map(_.reject(this.compareFDADetails, ['periodTotal', 'NA']), s => s.periodTotal);
                        this.compareFDADetailsTotal = this.compareFDADetailsTotal.map(Number);
                        this.compareFDADetailsTotal = _.sum(this.compareFDADetailsTotal);

                    }
                },
                    error => {
                        this.alerts = [];
                        this.alerts.push({ type: 'warning', msg: error });
                    });
    }

    getIngestedDetails() {

        if (this.busyIngestedDetails) {
            this.busyIngestedDetails.unsubscribe();
        }
        this.busyIngestedDetails =
            // tslint:disable-next-line:max-line-length
            this.atcomparisonservice.getImporterComparisonIngestionDetails(this.headerdata.ein, this.shareData.fiscalYear, this.activequarter, this.headerdata.tobaccoClass).subscribe(data => {
                if (data) {
                    this.compareIngestedDetails = [];
                    //Merge the resulting data
                    data.forEach(d => {
                        this.compareIngestedDetails = this.compareIngestedDetails.concat(d);
                    });

                    //Its  looping through all the entrylines lokking for only the valid records
                    this.compareIngestionDetailsTotal = _.map(_.filter(this.compareIngestedDetails, { periodEntryDateCategory: 'Valid' }), s => s.periodTotal);
                    this.compareIngestionDetailsTotal = this.compareIngestionDetailsTotal.map(Number);

                    //If every value is 'NA' then te total is NA
                    if (this.compareIngestionDetailsTotal.every(this.checkNA))
                        this.compareIngestionDetailsTotal = "NA"
                    //Other wise just add it all up
                    else
                        this.compareIngestionDetailsTotal = _.sum(this.compareIngestionDetailsTotal);


                }

            },
                error => {
                    this.alerts = [];
                    this.alerts.push({ type: 'warning', msg: error });
                }
            );
    }

    checkNA(value) {
        return value === "NA" || value === null;
    }

    /**
     * Determine the logic for the quarter tab navigation
     */
    getDelta() {
        let deltaText = '';

        if (Math.abs(this.comparedetailsIngestedSummary.summaryTotal.tufaAndSummaryDiff) >= 1 && Math.abs(this.comparedetailsIngestedSummary.summaryTotal.summaryAndDetailDiff) >= 1) {
            deltaText = 'Review';
        } else if (this.checkNA(this.comparedetailsIngestedSummary.summaryTotal.tufaAndSummaryDiff)) {
            deltaText = 'NA';
        } else {
            deltaText = this.currencyPipe.transform(this.comparedetailsIngestedSummary.summaryTotal.tufaAndSummaryDiff, null);
            deltaText = this.minusToParenthese.transform(deltaText);
        }

        if (this.isOneSided()) {
            deltaText += " All Deltas";
        }

        this.toggleDelta(deltaText, this.comparedetailsIngestedSummary.summaryTotal.tufaAndSummaryDiff, this.isOneSided());
    }

    isOneSided() {
        return (this.checkNA(this.comparedetailsIngestedSummary.summaryTotal.tufaValue) && !this.checkNA(this.comparedetailsIngestedSummary.summaryTotal.summaryValue)) ||
            (!this.checkNA(this.comparedetailsIngestedSummary.summaryTotal.tufaValue) && this.checkNA(this.comparedetailsIngestedSummary.summaryTotal.summaryValue));
    }

    toggleDelta(delta, summarydelta, isOneSided) {
        //console.log("toggleDelta qtr :" + qtr + "delta :" + delta + " summarydelta:" + summarydelta);

        let togglevalue = new ToggleTabModel();

        switch (this.quarter) {
            case 1:
                jQuery("#delta1").text(delta);
                this.delta1 = delta;
                togglevalue.qtr = 1;
                break;
            case 2:
                jQuery("#delta2").text(delta);
                this.delta2 = delta
                togglevalue.qtr = 2;
                break;
            case 3:
                jQuery("#delta3").text(delta);
                this.delta3 = delta
                togglevalue.qtr = 3;
                break;
            case 4:
                jQuery("#delta4").text(delta);
                this.delta4 = delta
                togglevalue.qtr = 4;
                break;
        }

        //Allows for clicking of the tabs
        togglevalue.flag = false;
        if (Math.abs(summarydelta) > 0.99 && summarydelta != 'NA' && !isOneSided) {
            togglevalue.flag = true;
        }


        this.toggleTab.emit(togglevalue);

        return delta;

    }

    getHTSCodeSummary() {
        if (this.busyHTSCodeSummary) {
            this.busyHTSCodeSummary.unsubscribe();
        }

        this.busyHTSCodeSummary =
            // tslint:disable-next-line:max-line-length
            this.atcomparisonservice.getImporterComparisonHTSCodeSummary(this.headerdata.ein, this.shareData.fiscalYear,
                this.activequarter, this.headerdata.tobaccoClass).subscribe(data => {
                    if (data) {
                        this.htsCodeIngestionSummary = data;
                    }
                },
                    error => {
                        this.alerts = [];
                        this.alerts.push({ type: 'warning', msg: error });
                    });
    }

    amendTotalFlagSwitched(flag: boolean) {
        this.isValid = flag;
    }

    savedAmendment($event) {
        this.fda3852map[this.activequarter] = AcceptanceFlagState.AMEND;
        this.acceptedFDAFlag = false;
        this.acceptedIngestedFlag = false;
        this.inProgressDisabled = false
        this.inprogressFlag = false;
    }

    associatedIngestedDeltaFlag() {
        this.atcomparisonservice.checkIfAssociationExists(this.shareData.fiscalYear, this.headerdata.tobaccoClass, this.headerdata.ein,this.quarter).subscribe(
            data => {
                if (data) {
                    //console.log(data);
                    this.ingestedAssociationFlag = data;
                }
            },
            error => {
                this.alerts = [];
                this.alerts.push({ type: 'warning', msg: error });
            }
        );
    }

    acceptedFDAflag($event) {

        let acceptAndContinueFlag: Boolean = false;
        let flag: string = $event.flag;
        let reviewAmmendedTax = null;
        let reviewAmmendedvol = null;

        if (flag === AcceptanceFlagState.ACCEPT_FDA_CONTINUE || flag === AcceptanceFlagState.ACCEPT_FDA) {
            this.acceptedFDAFlag = true;
            this.acceptedIngestedFlag = false;
            this.inprogressFlag = false;
            this.inProgressDisabled = false;
        } else if (AcceptanceFlagState.ACCEPT_INGESTED_CONTINUE || flag === AcceptanceFlagState.ACCEPT_INGESTED) {
            reviewAmmendedTax = $event.cbpAmmendedtotaltax;
            reviewAmmendedvol = $event.cbpAmmendedtotalVol;

            this.acceptedFDAFlag = false;
            this.acceptedIngestedFlag = true;
            this.inprogressFlag = false;
            this.inProgressDisabled = false;
        }


        if (flag === AcceptanceFlagState.ACCEPT_FDA_CONTINUE || flag === AcceptanceFlagState.ACCEPT_INGESTED_CONTINUE) {
            acceptAndContinueFlag = true;
            this.annualTrueUpComparisonEventService.triggerAtCBPCompareAcceptFDASource([this.activequarter, acceptAndContinueFlag, this.inprogressFlag, this.acceptedFDAFlag, reviewAmmendedTax, reviewAmmendedvol]);
        }

        if (flag === AcceptanceFlagState.ACCEPT_FDA || flag === AcceptanceFlagState.ACCEPT_INGESTED) {
            this.annualTrueUpComparisonEventService.triggerAtCBPCompareAcceptFDASource([this.activequarter, acceptAndContinueFlag, this.inprogressFlag, this.acceptedFDAFlag, reviewAmmendedTax, reviewAmmendedvol]);
        }
        this.fda3852map[this.activequarter] = flag;

    }


    progressedflag($event) {
        if ($event) {
            this.fda3852map[this.activequarter] = "InProgress";
            this.inProgressDisabled = true;
            this.inprogressFlag = true;
            this.annualTrueUpComparisonEventService.triggerAtCBPCompareAcceptFDASource([this.activequarter, false, this.inprogressFlag]);
        }
    }

    toPeriodofActivity(detail) {
        let localSrc = this.shareData.detailsSrc;
        let localflag = this.shareData.acceptanceFlag;
        let localtype = this.shareData.permitType;
        let localClass = this.shareData.tobaccoClass;
        let localCompany = this.shareData.companyName;
        let quarter = this.shareData.quarter;
        let originalEIN = this.shareData.originalEIN;
        let ein = this.shareData.ein;
        this.shareData.reset();
        this.shareData.companyName = localCompany;
        this.shareData.acceptanceFlag = localflag;
        this.shareData.permitType = localtype;
        this.shareData.tobaccoClass = localClass;
        this.shareData.detailsSrc = localSrc;
        this.shareData.companyName = this.headerdata.companyName;
        this.shareData.permitNum = detail.permit;
        this.shareData.period = detail.month + ' FY ' + this.headerdata.fiscalYear;
        this.shareData.permitId = detail.permitId;
        this.shareData.periodId = detail.periodId;
        this.shareData.companyId = this.headerdata.companyId;
        if(this.headerdata.companyId == 0){
            this.shareData.ein = ein;
            this.shareData.originalEIN = originalEIN;
        }
        this.shareData.periodStatus = this.getReportStatus(detail.status);
        this.shareData.previousRoute = this.router.url;
        this.shareData.isRecon = false;
        this.shareData.breadcrumb = 'at-comparedetails';
        this.shareData.qatabfrom = 'at-compare';
        this.shareData.createdDate = this.createdDate;
        this.shareData.submitted = this.submitted;

        this.shareData.quarter = quarter;
        this.shareData.fiscalYear = Number(this.headerdata.fiscalYear);
        // this.shareData.months = this.months;
    }

    private getReportStatus(status: string) {
        if (status === "NS") {
            return "NSTD";
        } else if (status === "C") {
            return "COMP";
        } else if (status === "E") {
            return "ERRO";
        }
        return "";
    }

    exportRawdata() {
        this.atcomparisonservice.getDetailRawExport(this.shareData.fiscalYear, this.shareData.quarter.toString(), this.shareData.tobaccoClass, this.shareData.ein, "IMPT", false).subscribe(
            data => {
                this._downloadService.download(data.csv, data.fileName, 'csv/text');
            }
        );
    }
}

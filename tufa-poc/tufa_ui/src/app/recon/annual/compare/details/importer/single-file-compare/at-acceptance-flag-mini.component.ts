import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ShareDataService } from "../../../../../../core/sharedata.service";
import { AnnualTrueUpComparisonEventService } from "../../../../services/at-compare.event.service";
import { AnnualTrueUpComparisonService } from "../../../../services/at-compare.service";
import { FDA3852FlagPanel } from "../../../at-acceptanceFlag.component";

@Component({
    selector: 'acceptance-flag-mini',
    templateUrl: './at-acceptance-flag-mini.template.html'
})
export class AcceptanceFlagMini extends FDA3852FlagPanel {

    constructor(annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
        _shareData: ShareDataService, atcomparisonservice: AnnualTrueUpComparisonService, router: Router) {
            super(annualTrueUpComparisonEventService, _shareData, atcomparisonservice, router);
    }
    
}
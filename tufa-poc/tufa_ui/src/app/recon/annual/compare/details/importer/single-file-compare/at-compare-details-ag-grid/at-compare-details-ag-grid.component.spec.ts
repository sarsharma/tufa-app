import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtCompareDetailsAgGridComponent } from './at-compare-details-ag-grid.component';

describe('AtCompareDetailsAgGridComponent', () => {
  let component: AtCompareDetailsAgGridComponent;
  let fixture: ComponentFixture<AtCompareDetailsAgGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtCompareDetailsAgGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtCompareDetailsAgGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { ShareDataService } from '../../../../../../../core/sharedata.service';
import { CurrencyRenderer } from '../../../../../../../shared/ag-grid-cell-renderers/currency-renderer.component';
import { InfoIconRenderer } from '../../../../../../../shared/ag-grid-cell-renderers/info-icon-renderer.component';
import { AgGridComponent } from '../../../../../../../shared/components/ag-grid/tufa.ag-grid.component';
import { AgColumnDefinition, AgGridState } from '../../../../../../../shared/model/ag-grid.types';
import { agGridStateMgmtProvider, serverSideDataSourceProvider } from '../../../../../../../shared/providers/ag-grid/ag-grid.providers';
import { ServerSideDataSourceService } from '../../../../../../../shared/service/ag-grid.datasource.service';
import { AgGridStateMgmtService } from '../../../../../../../shared/service/ag-grid.state.mgmt.service';
import { RouterGlobalService } from '../../../../../../../shared/service/router.global.service';
import { IngestedPeriodDetail } from '../../../../../models/at-compare-details.model';
import { AtReallocationService } from '../../../../../services/at-reallocation.service';

@Component({
  selector: 'app-at-compare-details-ag-grid',
  templateUrl: './at-compare-details-ag-grid.component.html',
  styleUrls: ['./at-compare-details-ag-grid.component.scss'],
  providers: [agGridStateMgmtProvider, serverSideDataSourceProvider, DatePipe],
})
export class AtCompareDetailsAgGridComponent extends AgGridComponent implements OnInit {

  @Input() data: IngestedPeriodDetail[] = [];
  @Input() selectedDt: string;
  @Input() hideInfoColumn: boolean = false;
  @Input() gridName: string = 'compare-details-ingested-month';
  @Input() quarter: number;
  @Output() selectedChange: EventEmitter<IngestedPeriodDetail[]> = new EventEmitter<IngestedPeriodDetail[]>();
  
  @Input() fiscalYear:number;

  columnDefs: AgColumnDefinition[];
  public gridApi;
  public gridColumnApi;

  icons;
  frameworkComponents;
  context;
  selected: IngestedPeriodDetail[] = [];
  selectAll = new Set();
  sortentrySumm = [{ colId: "entrySummaryDate", sort: "asc" }, { colId: "entryNum", sort: "asc" }];
  sortentryDt = [{ colId: "entryDate", sort: "asc" }, { colId: "entryNum", sort: "asc" }];



  constructor(
    private _shareData: ShareDataService,
    public _agGridStateMgmtService: AgGridStateMgmtService,
    public _serverSideDataSource: ServerSideDataSourceService,
    public routerService: RouterGlobalService,
    public storage: LocalStorageService,
    public _datepipe: DatePipe,
    private reallocateService: AtReallocationService) {
    super(_agGridStateMgmtService, _serverSideDataSource, routerService, storage);

    this.serverSidePaginationEnabled = false;

    let headerHeight = 200;
    this.columnDefs = [{
      colId: 'mangeEntry',
      // headerName: 'Manage Entry',
      headerTooltip: "Mark for reallocation",
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
      width: 30,
      headerHeight: headerHeight,
      includeInExport: false,
    }, {
      headerName: 'ENTRY SUM DATE',
      headerTooltip: 'Entry Summary Date',
      field: 'entrysummaryDate',
      colId: 'entrySummaryDate',
      enableRowGroup: true,
      width: 100,
      headerHeight: headerHeight,
      includeInExport: true,
      valueGetter: function (params) {
        if (params.data != null)
          return _datepipe.transform(params.data.entrysummaryDate, 'MM/dd/yyyy');
      },
      valueFormatter: function (params) {
        if (params.value != null)
          return _datepipe.transform(params.value, 'MM/dd/yyyy');
      },
    },
    {
      headerName: 'ENTRY DATE',
      headerTooltip: 'Entry Date',
      field: 'entryDate',
      colId: 'entryDate',
      enableRowGroup: true,
      width: 100,
      headerHeight: headerHeight,
      includeInExport: true,
      valueGetter: function (params) {
        if (params.data != null)
          return _datepipe.transform(params.data.entryDate, 'MM/dd/yyyy');
      },
      valueFormatter: function (params) {
        if (params.value != null)
          return _datepipe.transform(params.value, 'MM/dd/yyyy');
      }
    },
    {
      headerName: 'ENTRY #',
      headerTooltip: 'Entry Number',
      field: 'entryNum',
      colId: 'entryNum',
      enableRowGroup: true,
      width: 100,
      headerHeight: headerHeight,
      includeInExport: true,
    }, {
      headerName: 'TYPE',
      headerTooltip: 'Entry Type Code',
      field: 'entryTypeCd',
      colId: 'entryTypeCd',
      enableRowGroup: true,
      width: 50,
      headerHeight: headerHeight,
      includeInExport: true,
    }, {
      headerName: 'QTY',
      headerTooltip: 'Quantity Removed',
      field: 'qtyRemoved',
      colId: 'qtyRemoved',
      enableRowGroup: true,
      width: 60,
      headerHeight: headerHeight,
      includeInExport: true,
    }, {
      headerName: 'EXCISE TAX',
      headerTooltip: 'Excise Tax',
      field: 'taxAmount',
      colId: 'taxAmount',
      enableRowGroup: true,
      comparator: this.customCurrencyComparator,
      cellRenderer: "currencyRenderer",
      cellRendererParams: {
        fieldParams: ['taxAmount']
      },
      width: 90,
      headerHeight: headerHeight,
      includeInExport: true,
    }, {
      headerName: 'EXCLUDED',
      colId: 'excluded',
      valueGetter: (params) => {
        return params.data['excludedFlag'] == 'Y' ? 'Y' : 'N';
      },
      hide: true,
      suppressToolPanel: true,
      includeInExport: false,
    }, {
      headerName: 'REALLOCATED',
      colId: 'reallocated',
      valueGetter: (params) => {
        return params.data['reallocatedFlag'] == 'Y' ? 'Y' : 'N';
      },
      hide: true,
      suppressToolPanel: true,
      includeInExport: false,
    }, {
      headerName: 'MISSING',
      colId: 'missing',
      valueGetter: (params) => {
        return params.data['missingFlag'] == 'Y' ? 'Y' : 'N';
      },
      hide: true,
      suppressToolPanel: true
    },
    {
      headerName: 'SECONDARY DATE',
      colId: 'secondaryDate',
      valueGetter: (params) => {
        return params.data['secondaryDateFlag'] == 'Y' ? 'Y' : 'N';
      },
      hide: true,
      suppressToolPanel: true
    },
    {
      headerName: 'quarter',
      field: 'quarter',
      colId: 'quarter',
      
      hide: true,
      suppressToolPanel: true,
      includeInExport: false,
    }];

    this.frameworkComponents = {
      currencyRenderer: CurrencyRenderer,
      infoIconRenderer: InfoIconRenderer
    };

    this.urlsSaveState = ["/permit/impreport", "/permit/manreport", "/company/", "/permit/history"];
    this.storeGridState = true;

    this.icons = {
      checkboxChecked: '<i class="fa fa-check-square" style="color: limegreen"/>',
      checkboxUnchecked: '<i class="fa fa-square"  style="color: lightgrey"/>'
    }

    this.context = { componentParent: this };

    this.reallocateService.reallocateEntryEvent$.subscribe(
      data => {
        if (data.quarter != this.quarter)
          this.gridApi.deselectAll();
      }
    );
    this.reallocateService.selectAllEntryEvent$.subscribe(
      data => {
        
        if (data[0]) {
          this.selectAll.add(data[1])
          let arr = this.selectAll;
          let year = this.fiscalYear;
          let dt = this.selectedDt;
          this.gridApi.forEachNode(function(node) {
            // let entryDtYear = new Date(node.data.entryDate).getFullYear();
            // console.log("year######"+entryDtYear)
            // console.log("fiscalyear######"+year)
            let entryDtYear = new Date(node.data.entryDate).getFullYear();
            let entryDtmonth = new Date(parseInt(node.data.entryDate.toString())).getMonth() + 1

            // Edge case (eg main year is 2007 and data is for 2006(oct,nov, dec))
            if (entryDtYear == year - 1 && (entryDtmonth == 10 || entryDtmonth == 11 || entryDtmonth == 12)) {
              entryDtYear = year;
            }
            // Edge case (eg main year is 2007 and data is for 2007(oct,nov, dec))
            else if (entryDtYear == year && (entryDtmonth == 10 || entryDtmonth == 11 || entryDtmonth == 12)) {
              entryDtYear = year + 1;
            }
            node.setSelected((arr.has(node.data.quarter) &&
             (dt !== 'entryDt' || (dt === 'entryDt' && entryDtYear === year) || ((dt === 'entryDt' && node.data.reallocatedFlag != null && node.data.reallocatedFlag==='Y'))) && 
             ((node.data.entryTypeCd !== 21 && node.data.entryTypeCd !== 22) ||
              (node.data.reallocatedFlag != null && node.data.reallocatedFlag==='Y' && (node.data.entryTypeCd == 21 || node.data.entryTypeCd == 22))
             )

             )) 
            //node.setSelected(true);
          });
          }
        else {
          this.selectAll.delete(data[1]);
          let arr = this.selectAll;
          this.gridApi.forEachNode(function(node) {
            node.setSelected(arr.has(node.data.quarter));
            //node.setSelected(true);
          });
          }
        }
         
      
    );
    
  }

  ngOnInit() {
    super.ngOnInit();
    if (!this.hideInfoColumn) {
      this.columnDefs.push({
        headerName: 'INFO',
        headerTooltip: 'Info about entry status',
        field: 'infosortOrder',
        colId: 'infosortOrder',
        enableRowGroup: true,
        width: 60,
        cellRenderer: "infoIconRenderer",
        cellRendererParams: {
          fieldParams: ['taxAmount']
        },
        valueGetter: (params) => {
          let actionArry = [];
          if (params.data['reallocatedFlag'] == 'Y')
            actionArry.push('Reallocated');
          if (params.data['excludedFlag'] == 'Y')
            actionArry.push('Excluded');
          if (params.data['secondaryDateFlag'] == 'Y')
            actionArry.push('Seconday Date');
          if (params.data['missingFlag'] == 'Y')
            actionArry.push('Missing');  

          return actionArry.join(';');

        },
        includeInExport: true,
      });
    }

    let exportCols: string[] = this.getColumnsForExport(this.columnDefs);

    this.defaultExportParams = {
      allColumns: true,
      columnKeys: exportCols,
    };

  }

  onGridReady(params) {
    super.onGridReady(params, '', this.gridName);
    let state: AgGridState = new AgGridState();
    state.sortState = this.gridApi.getSortModel();
    if ((!state.sortState || state.sortState.length == 0) && this.selectedDt == 'entrySummDt')
      this.gridApi.setSortModel(this.sortentrySumm);
    else if ((!state.sortState || state.sortState.length == 0) && this.selectedDt == 'entryDt')
      this.gridApi.setSortModel(this.sortentryDt);
  }


  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "selectedDt") {

        let chng = changes[propName];
        let state: AgGridState = new AgGridState();

        if (chng.currentValue == 'entrySummDt') {
          if (this.gridColumnApi != null || this.gridColumnApi != undefined) {
            state.sortState = this.gridApi.getSortModel();
            if (!state.sortState || state.sortState.length == 0 || chng.previousValue == 'entryDt')
              this.gridApi.setSortModel(this.sortentrySumm);
          }
        }
        else if (chng.currentValue == 'entryDt') {
          if (this.gridColumnApi != null || this.gridColumnApi != undefined) {
            state.sortState = this.gridApi.getSortModel();
            if (!state.sortState || state.sortState.length == 0 || chng.previousValue == 'entrySummDt')
              this.gridApi.setSortModel(this.sortentryDt);
          }
        }
      }

    }
  }

  onSelectionChanged(event) {
    this.selected = event.api.getSelectedNodes().map(
      node => {
        let selection = new IngestedPeriodDetail();
        // AG-Grid does pass the data back but this is needed to recover the object methods
        Object.assign(selection, node.data);
        return selection;
      }
    );
    this.selectedChange.emit(this.selected);
  }

}
import { CommonModule } from "@angular/common";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { HttpModule } from "@angular/http";
import { provideRoutes } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { of } from "../../../../../../../../node_modules/rxjs";
import { AdminService } from "../../../../../../admin/services/admin.service";
import { AppModule } from "../../../../../../app.module";
import { ShareDataService } from "../../../../../../core/sharedata.service";
import { SharedModule } from "../../../../../../shared/shared.module";
import { CustomNumberPipe } from "../../../../../../shared/util/custom.number.pipe";
import { AcceptanceFlagState } from "../../../../models/at-acceptance-flag-state.model";
import { CBPAmendment } from "../../../../models/at-cbpamendment.model";
import { Details } from "../../../../models/at-compare-details.model";
import { AnnualTrueUpComparisonDetailsHeader } from "../../../../models/at-detailsheader.model";
import { AnnualTrueUpComparisonEventService } from "../../../../services/at-compare.event.service";
import { AnnualTrueUpComparisonService } from "../../../../services/at-compare.service";
import { DeltaPipe } from "../../../../utils/at-delta.pipe";
import { CompareIconDirective } from "../../../../utils/at-icon.directive";
import { AcceptanceFlagMini } from "./at-acceptance-flag-mini.component";
import { AtCompareDetailsAgGridComponent } from "./at-compare-details-ag-grid/at-compare-details-ag-grid.component";
import { CompareDetailsAmend } from "./at-compare-details-amend.component";
import { CompareDetailsQuarterPanel } from "./at-compare-details-quarter-panel.component";

describe('Single File Compare Details', () => {
    let comp: CompareDetailsQuarterPanel;
    let fixture: ComponentFixture<CompareDetailsQuarterPanel>;

    let trueUpService: jasmine.SpyObj<AnnualTrueUpComparisonService>;
    let eventService = new AnnualTrueUpComparisonEventService();

    let NON_CIG_QTR: number = 3;
    let CIG_QTR: number = 5;

    let event: CBPAmendment;

    const data: Details = require('./example-json-result.json');

    beforeEach(async(() => {

        trueUpService = jasmine.createSpyObj('AnnualTrueUpComparisonService', [
            'getCompareDetails',
            'getImporterComparisonHTSCodeSummary',
            'checkIfAssociationExists'
        ]);
        trueUpService.getCompareDetails.and.returnValue(of(data));
        trueUpService.getImporterComparisonHTSCodeSummary.and.returnValue(of(null));
        trueUpService.checkIfAssociationExists.and.returnValue(of(null));

        // reallocateService = jasmine.createSpyObj('AtReallocationService', [
        //     'triggerReallocateEntryEvent'
        // ]);

        TestBed.configureTestingModule({
            imports: [AppModule, SharedModule, HttpModule, CommonModule, RouterTestingModule],
            declarations: [
                CompareDetailsQuarterPanel,
                AcceptanceFlagMini,
                CompareDetailsAmend,
                CompareIconDirective,
                DeltaPipe,
                AtCompareDetailsAgGridComponent
            ],
            providers: [
                ShareDataService,
                AnnualTrueUpComparisonEventService,
                AnnualTrueUpComparisonService,
                AdminService,
                CustomNumberPipe,
                provideRoutes([{ path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTestig', pathMatch: 'full' }]),
                { provide: AnnualTrueUpComparisonEventService, useValue: eventService },
                { provide: AnnualTrueUpComparisonService, useValue: trueUpService },
            ]
        }).compileComponents();
    }));

    beforeEach(() => {

        event = new CBPAmendment();
        event.qtr = NON_CIG_QTR;

        fixture = TestBed.createComponent(CompareDetailsQuarterPanel);
        comp = fixture.componentInstance;
        comp.quarter = NON_CIG_QTR;
        comp.headerData = new AnnualTrueUpComparisonDetailsHeader();
        fixture.detectChanges();
    });

    it('should update the acceptance flag after save event to Accept FDA', () => {
        event.acceptanceFlag = 'Y';
        eventService.triggerAtCompareSuccessfullSaveAction([event]);
        expect(comp.acceptanceFlag).toEqual(AcceptanceFlagState.ACCEPT_FDA);
    });

    it('should update the acceptance flag after save event to Accept Ingested', () => {
        event.acceptanceFlag = 'I';
        eventService.triggerAtCompareSuccessfullSaveAction([event]);
        expect(comp.acceptanceFlag).toEqual(AcceptanceFlagState.ACCEPT_INGESTED);
    });

    it('should update the acceptance flag after save event to In Progress', () => {
        event.inProgressFlag = 'Y';
        eventService.triggerAtCompareSuccessfullSaveAction([event]);
        expect(comp.acceptanceFlag).toEqual(AcceptanceFlagState.IN_PROGRESS);
    });

    it('should not update the acceptance flag after save event if the quarters do not match', () => {
        comp.acceptanceFlag = AcceptanceFlagState.ACCEPT_FDA;
        comp.quarter = CIG_QTR + 1;
        event.inProgressFlag = 'I';
        eventService.triggerAtCompareSuccessfullSaveAction([event]);
        expect([AcceptanceFlagState.IN_PROGRESS, AcceptanceFlagState.ACCEPT_INGESTED, AcceptanceFlagState.AMEND].indexOf(comp.acceptanceFlag) == -1).toBeTruthy();
    });

});
import { AfterViewInit, Component, ComponentFactoryResolver, ComponentRef, Input, OnDestroy, OnInit, SimpleChanges } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { ShareDataService } from "../../../../../../core/sharedata.service";
import { PermitPeriodService } from '../../../../../../permit/services/permit-period.service';
import { PermitNumberPipe } from "../../../../../../shared/custompipes/permit-number.pipe";
import { AcceptanceFlagState } from '../../../../models/at-acceptance-flag-state.model';
import { Details, IngestedPeriodDetail } from "../../../../models/at-compare-details.model";
import { TrueUpComparisonResult } from "../../../../models/at-comparisonresult.model";
import { AnnualTrueUpComparisonDetailsHeader } from "../../../../models/at-detailsheader.model";
import { OneSidedDeltaDetaisl } from "../../../../models/at-one-sided-deltaDetails";
import { AnnualTrueUpComparisonEventService } from "../../../../services/at-compare.event.service";
import { AnnualTrueUpComparisonService } from "../../../../services/at-compare.service";
import { AtReallocationService } from "../../../../services/at-reallocation.service";
import { DeltaPipe } from "../../../../utils/at-delta.pipe";
import { AtBaseImptCompareDetails } from "../at-base-impt-compare-details";


declare let jQuery: any;

@Component({
    selector: 'details-quarter-panel',
    providers: [PermitPeriodService, PermitNumberPipe],
    templateUrl: './at-compare-details-quarter-panel.template.html',
    styleUrls: ['at-compare-details-quarter-panel.style.scss']
})
export class CompareDetailsQuarterPanel implements OnInit, OnDestroy, AfterViewInit {
    @Input() headerData: AnnualTrueUpComparisonDetailsHeader;
    @Input() acceptanceFlag: string;
    @Input() quarter: number;
    @Input() selectedDt: string;
    @Input() context: string;
    @Input() oneSidedAcceptanceFlag: string;
    private subscriptions: Subscription[] = [];

    months: number[] = [0, 1, 2];
    details: Details = new Details();
    filteredLinesfor2122: IngestedPeriodDetail[] = [];
    outsideFiscalYearLines: IngestedPeriodDetail[] = [];
    selectedEntriesMap: Map<number, IngestedPeriodDetail[]> = new Map();
    selectedFilteredEntryList: IngestedPeriodDetail[] = [];
    selectedOutsideFiscalYearList: IngestedPeriodDetail[] = [];
    htsCodeIngestionSummary: any = [];
    loadingDetails: Subscription;
    loadingHTSCode: Subscription;
    amendInfo;
    amendmentFlag: boolean = false;
    intital: boolean = true;
    busy: Subscription;
    exportBusy: Subscription;
    allDeltaBusy: Subscription;
    permitStatus: string[][] = [];
    componentRef: ComponentRef<any>;
    quarterDeltaForOnesidedMap: Map<number, OneSidedDeltaDetaisl> = new Map();
    headerDataMapForAllDeltas: Map<String, TrueUpComparisonResult>
    trueUpList: TrueUpComparisonResult[] = [];
    delta: OneSidedDeltaDetaisl;
    isReallocation: boolean = false;
    typeOfAllocation: string;
    typeOfAction: string;
    constructor(
        public shareData: ShareDataService,
        private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
        private annualTrueUpComparisonService: AnnualTrueUpComparisonService,
        private reallocateService: AtReallocationService,
        private componentFactoryResolver: ComponentFactoryResolver,
        private deltaPipe: DeltaPipe,
        private toastr: ToastrService

    ) {
        this.subscriptions.push(this.annualTrueUpComparisonEventService.atCBPCompare3852FlagsUpdateSource$.subscribe(data => {
            this.getCompareDetails();
            this.getHTSCodeSummary();
        }));

        this.subscriptions.push(this.reallocateService.reallocateEntrySave$.subscribe(
            data => {
                this.getCompareDetails();
                this.getHTSCodeSummary();
                this.isReallocation = true;
                this.typeOfAction = data;
            }
        ));
        this.subscriptions.push(this.annualTrueUpComparisonEventService.tobaccoClassSelected$.subscribe(
            data => {
                this.shareData.userExpanded = false;
                this.shareData.expandedSectionsMap.clear();
                this.intital = true;
                let panel = jQuery("#collapseDetailsPanel-" + this.quarter);
                panel.collapse('hide');
                this.headerData.quarter = 1;
                this.getCompareDetails();
                this.getHTSCodeSummary();
            }
        ));
        this.subscriptions.push(this.annualTrueUpComparisonEventService.atCompareMixedActionRefresh$.subscribe(
            data => {
                this.getCompareDetails();
            }
        ));
    }

    ngOnInit() {
        // if (this.quarter == this.headerData.quarter) {
        //     this.setQuarterExpanded(this.headerData.quarter, true);
        // }
    }

    // jQuery need here for the quarter panel. If we want to remove this we will need to convert from the Bootstrap collapse
    ngAfterViewInit() {

        let panel = jQuery("#collapseDetailsPanel-" + this.quarter);
        let _this = this;
        jQuery(`a[href="#collapseDetailsPanel-${this.quarter}"]`).on('click', function (event) {
            if ((_this.details.deltaTotal == null || _this.details.deltaTotal == 'NA') && !_this.hasEntryLines() && _this.oneSidedAcceptanceFlag != AcceptanceFlagState.ASSOCIATED)
                event.stopPropagation();
            else {
                _this.shareData.userExpanded = true;
                jQuery('.content-wrap').animate({
                    scrollTop: jQuery(`#accordionDetailsPanel-${_this.quarter}`).offset().top + jQuery('.content-wrap').scrollTop()
                });

            }

            event.preventDefault();

        });
        panel.on('shown.bs.collapse', function (event) {
            if (!_this.shareData.userExpanded && _this.quarter == _this.shareData.quarter) {
                jQuery('.content-wrap').animate({
                    scrollTop: jQuery(`#accordionDetailsPanel-${_this.quarter}`).offset().top + jQuery('.content-wrap').scrollTop()
                });
            }
            _this.setQuarterExpanded(_this.quarter, false, true);
        });
        panel.on('hidden.bs.collapse', function () {
            _this.setQuarterExpanded(_this.quarter, false, false);
        });
    }

    ngOnDestroy() {
        this.subscriptions.forEach(sub => {
            if (sub) {
                sub.unsubscribe();
            }
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "selectedDt") {
                let chg = changes[propName];
                this.shareData.selectedDt = this.selectedDt;
            }

            if (propName === "headerData") {
                let chg = changes[propName];
                if (chg.currentValue) {
                    this.headerData = chg.currentValue;
                    this.amendInfo = [{ "name": this.headerData.tobaccoClass }];
                    if (this.componentRef) (<AtBaseImptCompareDetails>this.componentRef.instance).selectedDt = this.selectedDt;
                }
            } else if (propName === "quarter") {
                let chg = changes[propName];
                if (chg.currentValue) {
                    this.quarter = chg.currentValue;

                    let max = this.quarter == 5 ? 12 : 3
                    this.months = [];
                    for (let i = 0; i < max; i++) {
                        this.months.push(i);
                    }
                }
                if (this.componentRef) (<AtBaseImptCompareDetails>this.componentRef.instance).quarter = this.quarter;
            } else if (propName === "acceptanceFlag") {
                let chg = changes[propName];
                if (this.componentRef) (<AtBaseImptCompareDetails>this.componentRef.instance).acceptanceFlag = this.acceptanceFlag;
            }
            else if (propName === "oneSidedAcceptanceFlag") {
                let chg = changes[propName];
                if (this.componentRef) (<AtBaseImptCompareDetails>this.componentRef.instance).oneSidedAcceptanceFlag = this.oneSidedAcceptanceFlag;
                if (this.details.dataType != undefined || (chg.previousValue == 'Associated')) {
                    //this.getOneSidedAcceptanceFlag(this.details.dataType);
                    //this.quarterDeltaForOnesidedMap.get(this.quarter).allDeltaStatus = this.oneSidedAcceptanceFlag;
                    this.getCompareDetails();
                    this.getHTSCodeSummary();
                }
            }

            if (this.componentRef) this.componentRef.changeDetectorRef.detectChanges();
        }
    }

    getCompareAllResultsForEin() {
        this.allDeltaBusy = this.annualTrueUpComparisonService.getAllDeltaDetails(this.headerData.permitType, this.headerData.fiscalYear, this.shareData.ein).subscribe(
            data => {
                this.trueUpList = data;
                this.trueUpList.forEach(trueUp => {
                    if (trueUp) {
                        this.headerDataMapForAllDeltas.set(trueUp.quarter, trueUp)
                    }
                });

            }, error => { }
        );
    }

    public setQuarterExpanded(quarter: number, inital?: boolean, expanded?: boolean) {

        if (!this.shareData.expandedSectionsMap) {
            this.shareData.expandedSectionsMap = new Map();
        }

        let side = this.shareData.expandedSectionsMap.get(quarter);
        if (side && inital) {
            this.shareData.expandedSectionsMap.set(quarter, side);
        } else if (side && !expanded) {
            this.shareData.expandedSectionsMap.delete(quarter);
        } else if (!side && expanded) {
            this.shareData.expandedSectionsMap.set(quarter, new Map());
            this.shareData.expandedSectionsMap.get(quarter).set('Fda', new Map());
            this.shareData.expandedSectionsMap.get(quarter).set('Ingested', new Map());
        }
    }

    private triggerCollapseAfterDataLoad(inital: boolean) {
        this.intital = inital;
        let panel = jQuery("#collapseDetailsPanel-" + this.quarter);
        if (this.shareData.expandedSectionsMap.get(this.quarter) && !((this.details.deltaTotal == null || this.details.deltaTotal == 'NA') && !this.hasEntryLines() && this.oneSidedAcceptanceFlag != AcceptanceFlagState.ASSOCIATED)) {
            this.intital = false;
            panel.collapse('show');
        } else if (((!this.shareData.userExpanded && this.headerData.quarter == this.quarter) || (this.shareData.userExpanded && this.shareData.expandedSectionsMap.get(this.quarter))) && !((this.details.deltaTotal == null || this.details.deltaTotal == 'NA') && !this.hasEntryLines() && this.oneSidedAcceptanceFlag != AcceptanceFlagState.ASSOCIATED)) {
            panel.collapse('show');
        } else if (!this.shareData.expandedSectionsMap.get(this.quarter) || this.headerData.quarter != this.quarter || ((this.details.deltaTotal == null || this.details.deltaTotal == 'NA') && !this.hasEntryLines() && this.oneSidedAcceptanceFlag != AcceptanceFlagState.ASSOCIATED)) {
            panel.collapse('hide');
        }
    }

    public isOneSided() {
        return (this.deltaPipe.isOneSided(this.details.tufaTotal, this.details.detailTotal)
            || this.oneSidedAcceptanceFlag === AcceptanceFlagState.ASSOCIATED);
    }

    public getCompareDetails() {

        if (this.loadingDetails)
            this.loadingDetails.unsubscribe();
        //alert("getCompareDetails:"+this.headerData.ein+"companyId:"+this.headerData.companyId);
        this.busy = this.loadingDetails = this.annualTrueUpComparisonService.getCompareDetails
            (this.headerData.companyId, this.headerData.fiscalYear, this.quarter,
            this.headerData.tobaccoClass, this.headerData.ein).subscribe(
                data => {
                    this.details = data;
                    this.triggerCollapseAfterDataLoad(!this.shareData.userExpanded);
                    this.delta = new OneSidedDeltaDetaisl();
                    this.delta.dataType = this.details.dataType;
                    this.delta.allDeltaStatus = this.oneSidedAcceptanceFlag;
                    this.getOneSidedAcceptanceFlag(this.delta.dataType);
                    this.delta.totalDelta = this.details.deltaTotal;
                    this.quarterDeltaForOnesidedMap.set(this.quarter, this.delta);
                    // if((this.quarter == 4 || this.quarter == 5) && this.isReallocation) {
                    //     this.annualTrueUpComparisonEventService.triggerAtRellocationNotification(this.typeOfAction);
                    //     this.isReallocation=false;
                    // }
                }, error => { }
            );
        if ((this.quarter == 4 || this.quarter == 5) && this.isReallocation) {
            this.annualTrueUpComparisonEventService.triggerAtRellocationNotification(this.typeOfAction);
            this.isReallocation = false;
        }
    }

    public getOneSidedAcceptanceFlag(deltaDataType: string) {
        if (deltaDataType === 'TUFA' && this.oneSidedAcceptanceFlag && this.oneSidedAcceptanceFlag.includes('MSReady'))
            this.oneSidedAcceptanceFlag = AcceptanceFlagState.MSREADYTUFA
        else if (deltaDataType === 'INGESTED' && this.oneSidedAcceptanceFlag && this.oneSidedAcceptanceFlag.includes('MSReady'))
            this.oneSidedAcceptanceFlag = AcceptanceFlagState.MSREADYINGESTED
    }

    public getHTSCodeSummary() {
        if (this.loadingHTSCode) this.loadingHTSCode.unsubscribe();
        //alert("getHTSCodeSummary"+this.headerData.ein);
        this.loadingHTSCode = this.annualTrueUpComparisonService.getImporterComparisonHTSCodeSummary(this.headerData.ein, +this.headerData.fiscalYear,
            this.quarter, this.headerData.tobaccoClass).subscribe(data => {
                if (data) {
                    this.htsCodeIngestionSummary = data;
                }
            },
                error => { });
    }

    private hasEntryLines(): boolean {
        let value = false;
        if (this.details) {
            this.details.ingestedDetails.forEach(month => {
                if ((month.periodDetails && month.periodDetails.length > 0) || month.periodEntryItemCount > 0)
                    value = true;
            })
        }
        return value;
    }

}
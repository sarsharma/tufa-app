import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtCompareDetailsTufaAgGridComponent } from './at-compare-details-tufa-ag-grid.component';

describe('AtCompareDetailsTufaAgGridComponent', () => {
  let component: AtCompareDetailsTufaAgGridComponent;
  let fixture: ComponentFixture<AtCompareDetailsTufaAgGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtCompareDetailsTufaAgGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtCompareDetailsTufaAgGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

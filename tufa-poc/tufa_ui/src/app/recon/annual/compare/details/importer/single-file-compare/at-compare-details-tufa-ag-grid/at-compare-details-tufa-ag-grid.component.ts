import { DecimalPipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { LocalStorageService } from '../../../../../../../../../node_modules/ngx-webstorage';
import { Form7501Model } from '../../../../../../../permit/models/form-7501.model';
import { AgGridComponent } from '../../../../../../../shared/components/ag-grid/tufa.ag-grid.component';
import { TufaCurrencyPipe } from '../../../../../../../shared/custompipes/custom.currency.pipe';
import { MinusToParensPipe } from '../../../../../../../shared/custompipes/minus.parentheses.pipe';
import { AgColumnDefinition, AgExportDefinition, AgGridState } from '../../../../../../../shared/model/ag-grid.types';
import { agGridStateMgmtProvider, serverSideDataSourceProvider } from '../../../../../../../shared/providers/ag-grid/ag-grid.providers';
import { ServerSideDataSourceService } from '../../../../../../../shared/service/ag-grid.datasource.service';
import { AgGridStateMgmtService } from '../../../../../../../shared/service/ag-grid.state.mgmt.service';
import { RouterGlobalService } from '../../../../../../../shared/service/router.global.service';

@Component({
  selector: 'app-at-compare-details-tufa-ag-grid',
  templateUrl: './at-compare-details-tufa-ag-grid.component.html',
  styleUrls: ['./at-compare-details-tufa-ag-grid.component.scss'],
  providers: [agGridStateMgmtProvider, serverSideDataSourceProvider, TufaCurrencyPipe, MinusToParensPipe, DecimalPipe],
})
export class AtCompareDetailsTufaAgGridComponent extends AgGridComponent implements OnInit {
  @Input() data: Form7501Model[] = [];
  @Input() gridName: string = 'compare-details-tufa-month';
  @Input() tobaccoClass: string = '';

  columnDefs: AgColumnDefinition[];
  public gridApi;
  public gridColumnApi;
  defaultExportParams: AgExportDefinition;
  context: any;

  constructor(
    public _agGridStateMgmtService: AgGridStateMgmtService,
    public _serverSideDataSource: ServerSideDataSourceService,
    public routerService: RouterGlobalService,
    public storage: LocalStorageService,
    private tufaCurrency: TufaCurrencyPipe,
    private minustoparens: MinusToParensPipe,
    private decimalPipe: DecimalPipe
  ) {
    super(_agGridStateMgmtService, _serverSideDataSource, routerService, storage);

    let headerHeight = 200;
    let _this = this;
    this.columnDefs = [{
      colId: 'lineNum',
      field: 'lineNum',
      headerName: "Line #",
      headerTooltip: "Line Number",
      width: 15,
      headerHeight: headerHeight
    }, {
      colId: 'qty',
      field: 'qty',
      headerName: "Net Quantity",
      headerTooltip: "Net Quantity",
      comparator: this.customCurrencyComparator,
      width: 30,
      headerHeight: headerHeight,
      cellStyle: {
        textAlign: "right"
      },
      valueGetter: function (params) {
        if (params.data != null) {
          return _this.decimalPipe.transform(params.data.qty, '1.0-3');
        }
      },
    }, {
      colId: 'taxAmt',
      field: 'taxAmt',
      headerName: "Excise Tax",
      headerTooltip: "Excise Tax",
      comparator: this.customCurrencyComparator,
      width: 30,
      headerHeight: headerHeight,
      cellStyle: {
        textAlign: "right"
      },
      valueGetter: function (params) {
        if (params.data != null) {
          return _this.tufaCurrency.transform(params.data.taxAmt, null);
        }
      },
    }, {
      colId: 'calcTaxRate',
      field: 'calcTaxRate',
      headerName: "Calc Tax Rate",
      headerTooltip: "Calculated Tax Rate",
      width: 30,
      headerHeight: headerHeight,
      comparator: this.customCurrencyComparator,
      cellStyle: {
        textAlign: "right"
      },
      valueGetter: function (params) {
        if (params.data != null) {
          if (params.data.type != 'Ad Valorem') {
            return _this.decimalPipe.transform(params.data.calcTaxRate, '1.0-6');
          } else {
            return 'N/A';
          }
        }
      },
    }, {
      colId: 'taxDiff',
      field: 'taxDiff',
      headerName: "Tax Diff",
      headerTooltip: "Tax Diff",
      width: 30,
      headerHeight: headerHeight,
      comparator: this.customCurrencyComparator,
      cellStyle: {
        textAlign: "right"
      },
      valueGetter: function (params) {
        if (params.data != null && params.data.type != 'Cigars') {
          let val = _this.tufaCurrency.transform(params.data.taxDiff, null);
          return _this.minustoparens.transform(val);
        }
      },
    }];

    this.urlsSaveState = ["/permit/impreport", "/permit/manreport", "/company/", "/permit/history"];
    this.context = { componentParent: this };
    this.storeGridState = true;
  }

  ngOnInit() {
    super.ngOnInit();
    if (this.tobaccoClass == 'Cigars') {
      this.columnDefs.splice(1, 0, {
        colId: 'type',
        field: 'type',
        headerName: "Type",
        headerTooltip: "Type",
        width: 30,
        headerHeight: 200,
        cellStyle: {
          textAlign: "right"
        },
        valueGetter: function (params) {
          if (params.data != null && params.data.type != 'Cigars') {
            return params.data.type;
          } else {
            return 'None';
          }
        },
      });
    }
  }

  onGridReady(params) {
    super.onGridReady(params, '', this.gridName);

    let state: AgGridState = new AgGridState();
    state.sortState = this.gridApi.getSortModel();
    if (!state.sortState || state.sortState.length == 0) {
      this.gridApi.setSortModel([{ colId: "lineNum", sort: "asc" }]);
    }
  }
}

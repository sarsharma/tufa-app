import { ATCompareBase } from "../../at-compare-base";

/** Base file for Single File Importer Compare Details */
export class AtImptCompare extends ATCompareBase {

    constructor() {
        super();
    }

}
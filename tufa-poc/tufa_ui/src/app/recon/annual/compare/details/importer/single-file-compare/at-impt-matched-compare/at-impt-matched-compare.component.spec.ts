import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AcceptanceFlagState } from '../../../../../models/at-acceptance-flag-state.model';
import { IngestedPeriodDetail } from '../../../../../models/at-compare-details.model';
import { AtReallocationService } from '../../../../../services/at-reallocation.service';
import { AtImptMatchedCompareComponent } from './at-impt-matched-compare.component';


describe('AtImptMatchedCompareComponent', () => {
  let component: AtImptMatchedCompareComponent;
  let fixture: ComponentFixture<AtImptMatchedCompareComponent>;
  let reallocateService: jasmine.SpyObj<AtReallocationService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AtImptMatchedCompareComponent],
      providers: [{ provide: AtReallocationService, useValue: reallocateService },]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtImptMatchedCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should set flag to amendment after successful amendment save', () => {
    component.savedAmendment(null);

    expect(component.acceptanceFlag).toEqual(AcceptanceFlagState.AMEND);
  });

  it('should set the selected entries to the matching quarter', () => {
    let selection: IngestedPeriodDetail[] = [new IngestedPeriodDetail()];
    component.setSelectedEntriesForMonth(selection, 2);

    expect(component.selectedEntriesMap).toBeTruthy();
    expect(component.selectedEntriesMap.get(2)).toEqual(selection);
  });

  it('should set the selected entries from the 21/22 bucket', () => {
    let selection: IngestedPeriodDetail[] = [new IngestedPeriodDetail()];
    component.setSelectedEntriesForFilteredLines(selection);

    expect(component.selectedFilteredEntryList).toBeTruthy();
    expect(component.selectedEntriesMap.get(2)).toEqual(selection);
  });

  it('should set all the selected entries as ReallocateModel', () => {

    let selection: IngestedPeriodDetail[] = [new IngestedPeriodDetail()];
    component.setSelectedEntriesForFilteredLines(selection);
    component.setSelectedEntriesForMonth(selection, 2);
    component.manageEntries();

    expect(reallocateService.triggerReallocateEntryEvent.calls.mostRecent().args[0].entries.length).toEqual(2);
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtImptOneSidedCompareComponent } from './at-impt-one-sided-compare.component';

describe('AtImptOneSidedCompareComponent', () => {
  let component: AtImptOneSidedCompareComponent;
  let fixture: ComponentFixture<AtImptOneSidedCompareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtImptOneSidedCompareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtImptOneSidedCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

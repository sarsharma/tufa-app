import { Component, ComponentFactoryResolver, Input, OnChanges, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core';
import * as _ from 'lodash';
import { Router } from '../../../../../../../../../node_modules/@angular/router';
import { Subscription } from '../../../../../../../../../node_modules/rxjs';
import { ShareDataService } from '../../../../../../../core/sharedata.service';
import { PermitPeriodService } from '../../../../../../../permit/services/permit-period.service';
import { PermitNumberPipe } from '../../../../../../../shared/custompipes/permit-number.pipe';
import { TobaccoClassProvider } from '../../../../../../../shared/providers/tobacco-class.provider';
import { DownloadSerivce } from '../../../../../../../shared/service/download.service';
import { AcceptanceFlagState } from '../../../../../models/at-acceptance-flag-state.model';
import { Details, IngestedPeriodDetail, TufaPeriodDetail } from '../../../../../models/at-compare-details.model';
import { TrueUpComparisonResult } from '../../../../../models/at-comparisonresult.model';
import { AnnualTrueUpComparisonDetailsHeader } from '../../../../../models/at-detailsheader.model';
import { OneSidedDeltaDetaisl } from '../../../../../models/at-one-sided-deltaDetails';
import { ReallocateModel } from '../../../../../models/at-reallocate.model';
import { AnnualTrueUpComparisonEventService } from '../../../../../services/at-compare.event.service';
import { AnnualTrueUpComparisonService } from '../../../../../services/at-compare.service';
import { AtReallocationService } from '../../../../../services/at-reallocation.service';
import { DeltaPipe } from '../../../../../utils/at-delta.pipe';
import { AtBaseImptCompareDetails } from '../../at-base-impt-compare-details';
import { AtImptCompare } from '../at-impt-compare';



declare let jQuery: any;

@Component({
  selector: 'app-at-impt-one-sided-compare',
  templateUrl: './at-impt-one-sided-compare.component.html',
  styleUrls: ['./../at-compare-details-quarter-panel.style.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AtImptOneSidedCompareComponent extends AtImptCompare implements OnInit, OnChanges, AtBaseImptCompareDetails {
  @Input() details: Details;

  @Input() headerData: AnnualTrueUpComparisonDetailsHeader;
  @Input() acceptanceFlag: string;
  @Input() quarter: number;
  @Input() selectedDt: string;
  @Input() context: string;
  @Input() oneSidedAcceptanceFlag: string;

  @Input() htsCodeIngestionSummary1: any

  private subscriptions: Subscription[] = [];

  qtr1months: string[] = ['10', '11', '12'];
  months: number[] = [0, 1, 2];
  filteredLinesfor2122: IngestedPeriodDetail[] = [];
  outsideFiscalYearLines: IngestedPeriodDetail[] = [];
  selectedEntriesMap: Map<number, IngestedPeriodDetail[]> = new Map();
  selectedFilteredEntryList: IngestedPeriodDetail[] = [];
  selectedOutsideFiscalYearList: IngestedPeriodDetail[] = [];
  htsCodeIngestionSummary: any = [];
  loadingDetails: Subscription;
  loadingHTSCode: Subscription;
  amendInfo;
  amendmentFlag: boolean = false;
  ingestedAssociationFlag: boolean = false;
  busy: Subscription;
  exportBusy: Subscription;
  permitStatus: string[][] = [];
  @Input() quarterDeltaForOnesidedMap: Map<number, OneSidedDeltaDetaisl>;
  @Input() headerDataMapForAllDeltas: Map<string, TrueUpComparisonResult>;


  constructor(
    private router: Router,
    public shareData: ShareDataService,
    private _permitPeriodService: PermitPeriodService,
    private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
    private annualTrueUpComparisonService: AnnualTrueUpComparisonService,
    private downloadService: DownloadSerivce,
    private reallocateService: AtReallocationService,
    private permitPeriodService: PermitPeriodService,
    private tobaccoClassProvider: TobaccoClassProvider,
    private permitPipe: PermitNumberPipe,
    private componentFactoryResolver: ComponentFactoryResolver,
    private deltaPipe: DeltaPipe

  ) {
    super();
    this.subscriptions.push(this.reallocateService.resetSelectAllEntryEvent$.subscribe(data=>{
      jQuery('input[type=checkbox]').prop('checked', false);
      this.reallocateService.triggerSelectAllEntryEvent([false,this.quarter])
    }))
  }

  ngOnInit() {
    this.associatedIngestedDeltaFlag();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      if (sub) {
        sub.unsubscribe();
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    jQuery('input[type=checkbox]').prop('checked', false);
    for (let propName in changes) {

      if (propName === "details") {
        this.updateDetails();
      }

      if (propName === "htsCodeIngestionSummary1") {
        this.htsCodeIngestionSummary  = this.htsCodeIngestionSummary1;
      }

      if (propName === "selectedDt") {
        let chg = changes[propName];
        this.shareData.selectedDt = this.selectedDt;
      }

      if (propName === "headerData") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.headerData = chg.currentValue;
          this.amendInfo = [{ "name": this.headerData.tobaccoClass }];
        }
      } else if (propName === "quarter") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.quarter = chg.currentValue;

          let max = this.quarter == 5 ? 12 : 3
          this.months = [];
          for (let i = 0; i < max; i++) {
            this.months.push(i);
          }
        }
      } else if (propName === "acceptanceFlag") {
        let chg = changes[propName];
        if (chg.currentValue && [AcceptanceFlagState.DELTA_CHANGE, AcceptanceFlagState.DELTA_CHANGE_ZERO, AcceptanceFlagState.DELTA_CHANGE_EXCLUDE].includes(chg.currentValue)) {
          this.amendmentFlag = false;
        }
      }
    }
  }

  private updateDetails() {
    this.selectedEntriesMap.clear();
    this.selectedFilteredEntryList = [];
    this.selectedOutsideFiscalYearList = [];

    this.filteredLinesfor2122 = [];
    this.outsideFiscalYearLines = [];

    if (this.details.ingestedDetails.length == 0) return;

    for (var index in this.details.tufaDetails) {
      this.permitStatus[index] = [];
      for (var permitIndex in this.details.tufaDetails[index].periodDetails) {
        if (this.permitStatus[index].indexOf(this.details.tufaDetails[index].periodDetails[permitIndex].status) == -1)
          this.permitStatus[index][permitIndex] = this.details.tufaDetails[index].periodDetails[permitIndex].status;
        if ((this.permitStatus[index].indexOf('E') !== -1 || this.permitStatus[index].indexOf('C') !== -1) && this.permitStatus[index].indexOf('NS') !== -1)
          this.permitStatus[index][this.permitStatus[index].indexOf('NS')] = "";
      }
    }

    this.details.ingestedDetails.forEach(month => {
      let monthDetails = month.periodDetails;

      if (monthDetails != undefined && monthDetails != null) {
        monthDetails.forEach((value, index, array) => {

          // Add entries to 21/22 bucket 
          if ((value.entryTypeCd == 21 || value.entryTypeCd == 22) && value.excludedFlag == 'Y' && value.reallocatedFlag !== 'Y') {
            this.filteredLinesfor2122.push(value);
          }

          // Add entries to Outside fiscalyear bucket
          if (this.selectedDt == 'entryDt' && value.entryDate) {

            let entryDtYear = new Date(value.entryDate).getFullYear();
            let entryDtmonth = new Date(parseInt(value.entryDate.toString())).getMonth() + 1

            // Edge case (eg main year is 2007 and data is for 2006(oct,nov, dec))
            if (entryDtYear == this.shareData.fiscalYear - 1 && (entryDtmonth == 10 || entryDtmonth == 11 || entryDtmonth == 12)) {
              entryDtYear = this.shareData.fiscalYear;
            }
            // Edge case (eg main year is 2007 and data is for 2007(oct,nov, dec))
            else if (entryDtYear == this.shareData.fiscalYear && (entryDtmonth == 10 || entryDtmonth == 11 || entryDtmonth == 12)) {
              entryDtYear = this.shareData.fiscalYear + 1;
            }

            if (value.reallocatedFlag !== 'Y' &&
              (value.entryTypeCd !== 21 && value.entryTypeCd !== 22) &&
              (entryDtYear !== this.shareData.fiscalYear)) {
              this.outsideFiscalYearLines.push(value);

            }
          }
        });
      }

      //Remove 21/22 from the monthly buckets 
      _.remove(monthDetails, s => (s.entryTypeCd == 21 || s.entryTypeCd == 22)
        && s.excludedFlag == 'Y' && s.reallocatedFlag !== 'Y');

      // Remove Outside line entries from the monthly buckets 
      if (this.selectedDt == 'entryDt')
        _.remove(monthDetails, obj => this.outsideFiscalYearLines.includes(obj));


    });
  }

  public setMonthExpanded(quarter: number, side: 'Fda' | 'Ingested', month: number, event) {
    if (this.shareData.expandedSectionsMap && event != undefined)
      this.shareData.expandedSectionsMap.get(quarter).get(side).set(month, event);
  }

  public associatedIngestedDeltaFlag() {
    this.annualTrueUpComparisonService.checkIfAssociationExists(this.shareData.fiscalYear, this.headerData.tobaccoClass, this.headerData.ein, this.quarter).subscribe(
      data => {
        if (data) {
          this.ingestedAssociationFlag = data;
        }
      },
      error => { }
    );
  }

  public isNoEnrtriesSelected(): boolean {
    let count: number = 0;
    this.selectedEntriesMap.forEach((value) => {
      if (value) count += value.length;
    });

    count += this.selectedFilteredEntryList.length + this.selectedOutsideFiscalYearList.length;

    return count < 1;
  }

  public setSelectedEntriesForMonth($event, index) {
    this.selectedEntriesMap.set(index, $event);
  }

  public setSelectedEntriesForFilteredLines($event) {
    this.selectedFilteredEntryList = $event;
  }

  public setSelectedEntriesForOutsideFiscalLines($event) {
    this.selectedOutsideFiscalYearList = $event;

  }

  manageEntries() {
    let flattenedSelectedEntries = [];

    let it = this.selectedEntriesMap.values();
    let record = it.next();
    while (!record.done) {
      flattenedSelectedEntries = flattenedSelectedEntries.concat(record.value);
      record = it.next();
    }

    flattenedSelectedEntries = flattenedSelectedEntries.concat(this.selectedFilteredEntryList).concat(this.selectedOutsideFiscalYearList);

    let reallocationList: ReallocateModel[] = flattenedSelectedEntries.map(
      entry => entry.toReallocateModel(this.shareData.fiscalYear)
    );

    this.reallocateService.triggerReallocateEntryEvent({ quarter: this.quarter, entries: reallocationList });
  }

  toPeriodofActivity(detail) {
    let localSrc = this.shareData.detailsSrc;
    let localflag = this.shareData.acceptanceFlag;
    let localtype = this.shareData.permitType;
    let localClass = this.shareData.tobaccoClass;
    let localCompany = this.shareData.companyName;
    let quarter = this.shareData.quarter;
    let createdDate = this.shareData.createdDate;
    this.shareData.reset();
    this.shareData.createdDate = createdDate;
    this.shareData.companyName = localCompany;
    this.shareData.acceptanceFlag = localflag;
    this.shareData.permitType = localtype;
    this.shareData.tobaccoClass = localClass;
    this.shareData.detailsSrc = localSrc;
    this.shareData.companyName = this.headerData.companyName;
    this.shareData.permitNum = detail.permit;
    this.shareData.period = detail.month + ' FY ' + this.headerData.fiscalYear;
    this.shareData.permitId = detail.permitId;
    this.shareData.periodId = detail.periodId;
    this.shareData.companyId = this.headerData.companyId;

    this.shareData.periodStatus = this.getReportStatus(detail.status);
    this.shareData.previousRoute = this.router.url;
    this.shareData.isRecon = false;
    this.shareData.breadcrumb = 'at-comparedetails';
    this.shareData.qatabfrom = 'at-compare';

    this.shareData.quarter = quarter;
    this.shareData.fiscalYear = Number(this.headerData.fiscalYear);
    // this.shareData.months = this.months;
  }

  progressedflag($event) {
    this.annualTrueUpComparisonEventService.triggerAtCompareActiveQuarterUpdateSource(this.quarter);
    if ($event) {
      this.annualTrueUpComparisonEventService.triggerAtCBPCompareAcceptFDASource([this.quarter, false, true]);
    }
  }

  acceptedFDAflag($event) {

    this.annualTrueUpComparisonEventService.triggerAtCompareActiveQuarterUpdateSource(this.quarter);

    let acceptAndContinueFlag: Boolean = false;
    let flag: string = $event.flag;
    let reviewAmmendedTax = null;
    let reviewAmmendedvol = null;
    let acceptedFDAFlag: boolean;
    let inprogressFlag: boolean;

    if (flag === AcceptanceFlagState.ACCEPT_FDA) {
      acceptedFDAFlag = true;
      inprogressFlag = false;
    } else if (flag === AcceptanceFlagState.ACCEPT_INGESTED) {
      reviewAmmendedTax = $event.cbpAmmendedtotaltax;

      acceptedFDAFlag = false;
      inprogressFlag = false;
    }

    if (flag === AcceptanceFlagState.ACCEPT_FDA || flag === AcceptanceFlagState.ACCEPT_INGESTED) {
      this.annualTrueUpComparisonEventService.triggerAtCBPCompareAcceptFDASource([this.quarter, acceptAndContinueFlag, inprogressFlag, acceptedFDAFlag, reviewAmmendedTax, reviewAmmendedvol]);
    }


  }

  savedAmendment(event) {
    this.annualTrueUpComparisonEventService.triggerAtCompareActiveQuarterUpdateSource(this.quarter);
  }

  public async downloadForm(detail: TufaPeriodDetail) {
    await this.tobaccoClassProvider.load();
    let id = this.tobaccoClassProvider.getTobaccoClassId(this.shareData.tobaccoClass.replace(/ /g,'-'));

    // Get export data
    let fileName = this.permitPipe.transform(detail.permit) + "_" + detail.month + "_" + this.shareData.fiscalYear + "_ERROR_7501.csv";
    this.permitPeriodService.getFormExport(detail.permitId, detail.periodId, '7501', id).subscribe(
      data => {
        if (data) {
          this.downloadService.download(data.csv, fileName, "csv/txt");
        }
      }
    )
  }

  public exportRawdata() {


    this.exportBusy = this.annualTrueUpComparisonService.getIngestedSummaryExport("details", this.shareData.companyId, this.shareData.ein, this.shareData.fiscalYear, this.quarter.toString(), this.shareData.tobaccoClass, this.context).subscribe(
      data => {
        if (data) {
          this.downloadService.download(data.csv, data.fileName, 'csv/text');
        }
      }
    );

    this.subscriptions.push(this.exportBusy);
  }

  enableLinkAuth(flag) {
    this._permitPeriodService.setLinkAuthentication(flag);
  }

  selectEntry(event) {
    if(event) {
      this.reallocateService.triggerSelectAllEntryEvent([event.target.checked,this.quarter])
    }
  }

}

import { Component, Input, OnInit, ViewEncapsulation, SimpleChanges, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { TTBAmendment, TTBDetailUserComment } from '../../../models/at-ttbamendment.model';
import { AnnualTrueUpComparisonEventService } from '../../../services/at-compare.event.service';
import { AnnualTrueUpComparisonService } from '../../../services/at-compare.service';
import { allDeltaCommentModel } from "../../../models/at-comment.model";
declare var jQuery: any;
import * as _ from 'lodash';

@Component({
    selector: '[ttbdetails-comment]',
    templateUrl: './../../comments/cmpdetail-comment.popup.template.html',
    encapsulation: ViewEncapsulation.None
})

export class CompareCommentsPanel implements OnInit, OnDestroy {


    @Input() compareData;
    @Input() headerdata;
    @Input() quarter;
    @Input() acceptanceFlag;
    @Input() commentAction;
    @Input() commentEdit;
    @Input() excludeQuarters;
    @Input() ingestionContext;


    @Output() commentDeleted=  new EventEmitter<boolean>();;
    qtrArray:number[]=[];


    activeqtr;

    showCommentsErrorFlag: boolean;
    alertsComments: Array<Object>;
    busy: Subscription;
    context: any;

    CIGAR_QUARTER = 5;
    isQ1Exclude:boolean;
    isQ2Exclude:boolean;
    isQ3Exclude:boolean;
    isQ4Exclude:boolean;

    qtrSelected: number[] = [];

    ttIdxMp = { "Cigars": 0, "Cigarettes": 0, "Chew-Snuff": 0, "Chew": 1, "Snuff": 2, "Pipe-Roll Your Own": 0, "Pipe": 1, "Roll-Your-Own": 2 }
    ttAliasMapping = { "Roll Your Own": "Roll-Your-Own" }

    @Output() hideModal = new EventEmitter<boolean>();

    comment: any;
    allQtrSelected: boolean = false;
    ein: string;

    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }
    }

    constructor(
        private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
        private annualTrueUpComparisonService: AnnualTrueUpComparisonService
    ) {

        this.comment = new TTBDetailUserComment();

        this.alertsComments = [
            {
                type: 'warning',
                msg: '<span class="fw-semi-bold">Warning:</span> Error: Saving Comments'
            }
        ];

        this.showCommentsErrorFlag = false;
    }


    ngOnInit() {
        
        for(var index in this.excludeQuarters) {
            if(this.excludeQuarters[index] ==1) {
                this.isQ1Exclude = true;
            }
            if(this.excludeQuarters[index] ==2) {
                this.isQ2Exclude = true;
            }
            if(this.excludeQuarters[index] ==3) {
                this.isQ3Exclude = true;
            }
            if(this.excludeQuarters[index] ==4) {
                this.isQ4Exclude = true;
            }
        }
        /*if (!(this.headerdata.tobaccoClass === 'Cigars') && !this.excludeQuarters.includes(this.quarter))
            this.qtrSelected[this.quarter - 1] = this.quarter;  */
         if (!(this.headerdata.tobaccoClass === 'Cigars')) {
                if (this.quarter == -1) {
                    this.qtrSelected = [1, 2, 3, 4];
                } else if (this.quarter == 0) {
                    // leave quarter selection default to nothing
                } else if (!this.excludeQuarters.includes(this.quarter)) {
                    this.qtrSelected[this.quarter - 1] = this.quarter;
                }
        }
        if (this.headerdata.originalEIN) {
            this.ein = this.headerdata.originalEIN;
            
        } else {
            this.ein = this.headerdata.ein;  
        }      
    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "commentEdit") {
                let currentValue = changes[propName].currentValue;
                if(currentValue){
                if(currentValue.ttbAmendmentId && currentValue.ttbAmendmentId >0 ){
                    this.comment = new TTBDetailUserComment();
                }else{
                    this.comment = new allDeltaCommentModel();
                }
                _.merge(this.comment, changes[propName].currentValue);
                
                let commentQtrs: string = this.comment.commentQtrs;
                if (commentQtrs) {
                    let commentQtrArry: string[] = commentQtrs.split(";");
                    for (let i in commentQtrArry) {
                        let qtr = Number(commentQtrArry[i]);
                        this.qtrSelected[qtr-1] = Number(commentQtrArry[i]);
                    }
                }
            }else{
                this.comment = new TTBDetailUserComment();
                _.merge(this.comment, changes[propName].currentValue);

            }
            }
        }
    }
    editCommentForDetailOnly(targetId: any){
        let modal = jQuery("#" + targetId).closest("#add-comment")
        
        if( this.comment.ttbAmendmentId && this.comment.ttbAmendmentId > 0){
            let commentModel = new TTBDetailUserComment();
            commentModel = this.comment;
            this.busy = this.annualTrueUpComparisonService.editTTBCompareCommentMatched(commentModel, commentModel.commentSeq).subscribe(
                data => {
                    this.annualTrueUpComparisonEventService.triggerAtTTBCompareCommentsEdit();
    
                    modal.modal('toggle');
                    /** clear the comment popup error messages */
                    jQuery('#addcomment-form').parsley().reset();
                    this.hideModal.emit(true);
    
                }, error => {
                    this.showCommentsErrorFlag = true;
                    this.alertsComments = [];
                    this.alertsComments.push({ type: 'warning', msg: error });
                });
            
        }else if( this.comment.cmpAllDeltaId && this.comment.cmpAllDeltaId > 0 ){
            let commentModel = new allDeltaCommentModel();
            commentModel = this.comment;
            this.busy = this.annualTrueUpComparisonService.editCompareCommentAllDeltas(commentModel,"MANU").subscribe(
                data => {
                    this.annualTrueUpComparisonEventService.triggerAtTTBCompareCommentsEdit();
    
                    modal.modal('toggle');
                    /** clear the comment popup error messages */
                    jQuery('#addcomment-form').parsley().reset();
                    this.hideModal.emit(true);
    
                }, error => {
                    this.showCommentsErrorFlag = true;
                    this.alertsComments = [];
                    this.alertsComments.push({ type: 'warning', msg: error });
                });
        }
        
    }
    deleteCommentForDetailOnly(targetId: any){
        let modal = jQuery("#" + targetId).closest("#add-comment");
        let ttbAmendmentId = 0;
        let cmpAllDeltaId = 0;
        let commSeqAllDelta = 0;
        let commSeqttb = 0;
        if(this.comment.ttbAmendmentId){
            ttbAmendmentId = this.comment.ttbAmendmentId;
        }
        if(this.comment.cmpAllDeltaId){
            cmpAllDeltaId = this.comment.cmpAllDeltaId;
        } 
        if(ttbAmendmentId > 0){
            commSeqttb = this.comment.commentSeq;
        }else if( cmpAllDeltaId > 0 ){
            if(this.comment.commentSeq){
                commSeqAllDelta =  this.comment.commentSeq;
            }
           
            if(this.comment.matchCommSeq && this.comment.matchCommSeq > 0){
                commSeqttb = this.comment.matchCommSeq;
            }
        }
        this.busy = this.annualTrueUpComparisonService.deleteCommDetailOnly(ttbAmendmentId, commSeqttb,cmpAllDeltaId,commSeqAllDelta,"MANU").subscribe(
            data => {
                this.annualTrueUpComparisonEventService.triggerAtTTBCompareCommentsEdit();

                modal.modal('toggle');
                /** clear the comment popup error messages */
                jQuery('#addcomment-form').parsley().reset();
                this.hideModal.emit(true);

            }, error => {
                this.showCommentsErrorFlag = true;
                this.alertsComments = [];
                this.alertsComments.push({ type: 'warning', msg: error });
            });


    }
   
    updateComment(event) {
        let tobaccoClass = this.headerdata.tobaccoClass.replace(/\//g, "-");
        jQuery('#addcomment-form').parsley().validate();
        if (!jQuery('#addcomment-form').parsley().isValid()) return;

        let targetId = event.currentTarget.id;
        let modal = jQuery("#" + targetId).closest("#add-comment");
        if(this.commentAction == 'edit' && this.ingestionContext == 'detailOnly'){
            return this.editCommentForDetailOnly(targetId);
        }


        let amdmnts: TTBAmendment[] = [];

        if (this.headerdata.tobaccoClass === 'Cigars')
            amdmnts = this.getCigarAmendment();
        else
            amdmnts = this.getNonCigarAmendments();

        this.busy = this.annualTrueUpComparisonService.saveTTBCompareComment(amdmnts,this.ein,this.headerdata.fiscalYear,tobaccoClass).subscribe(
            data => {

                let comments: TTBDetailUserComment[] = []
                for (let i in data) {
                    let ttname = data[i].tobaccoType;
                    let qtr = (ttname === 'Cigars') ? this.CIGAR_QUARTER : data[i].qtr;
                    this.compareData[qtr - 1][this.ttIdxMp[ttname]] = data[i];
                    comments.concat(data[i].userComments);
                }
                this.annualTrueUpComparisonEventService.triggerAtCompareCommentsSave(data);
                this.annualTrueUpComparisonEventService.triggerFlagsOnCommentSaveManu();
  
                modal.modal('toggle');
                /** clear the comment popup error messages */
                jQuery('#addcomment-form').parsley().reset();
                this.hideModal.emit(true);

            }, error => {
                this.showCommentsErrorFlag = true;
                this.alertsComments = [];
                this.alertsComments.push({ type: 'warning', msg: error });
            });

    }

    deleteComment(event) {

        let targetId = event.currentTarget.id;
        let modal = jQuery("#" + targetId).closest("#add-comment");
        if(this.commentAction == 'edit' && this.ingestionContext == 'detailOnly'){
            return this.deleteCommentForDetailOnly(targetId);
        }
        let comment = this.comment;

        let amdmnts: TTBAmendment[] = [];

        if (this.headerdata.tobaccoClass === 'Cigars')
            amdmnts = this.getCigarAmendment();
        else
            amdmnts = this.getNonCigarAmendments();

        this.annualTrueUpComparisonService.deleteTTBCompareComment(amdmnts).subscribe(
            data => {

                //trigger event to refresth grid comments
                this.annualTrueUpComparisonEventService.triggerAtTTBCompareCommentsDelete(true);
                
                modal.modal('toggle');
                /** clear the comment popup error messages */
                jQuery('#addcomment-form').parsley().reset();
                this.hideModal.emit(true);
            },
            error => {
                this.showCommentsErrorFlag = true;
                this.alertsComments = [];
                this.alertsComments.push({ type: 'warning', msg: error });
            }
        );

    }
    public isSaveDisable() {
        let isDisabled = true;
        let q = 0;
        if(this.headerdata.tobaccoClass === 'Cigars'){
            isDisabled = false;
            return isDisabled;
        }
        for(let q =0; q < this.qtrSelected.length ; q++){
            if(this.qtrSelected[q] > 0){
                isDisabled = false; 
                break;
            }
        }
        return isDisabled;
    }
    public checkboxSelected(qtrSelect) {

        if (qtrSelect === 'all') {
            this.allQtrSelected = !this.allQtrSelected;
            this.qtrSelected = this.allQtrSelected ? [1, 2, 3, 4] : [];
            let arr:number[]=[];
            if(this.allQtrSelected) {
                this.qtrSelected = [1, 2, 3, 4];
                if(this.excludeQuarters && this.excludeQuarters.length > 0){
                // for(var index in this.excludeQuarters) {
                    for(var index1 in this.qtrSelected) {
                        if(!this.excludeQuarters.includes(this.qtrSelected[index1])) {
                            arr.push(this.qtrSelected[index1]);
                        } else {
                            arr.push(0);
                        }
                    // }
                }
               this.qtrSelected = arr;
            }
            } else {
                this.qtrSelected = [];
            }
            return;
        }
        this.qtrSelected[qtrSelect - 1] = this.qtrSelected[qtrSelect - 1] ? 0 : qtrSelect;
    
        //this.qtrSelected[qtrSelect - 1] = this.qtrSelected[qtrSelect - 1] && this.excludeQuarters.includes(qtrSelect) ? 0 : qtrSelect;

    }

    private getNonCigarAmendments() {

        let amdmnts: TTBAmendment[] = [];

        let bulkCommentQtrs: string[] = [];
        for (let i in this.qtrSelected) {
            if(this.qtrSelected[i]>0){
                bulkCommentQtrs.push(this.qtrSelected[i].toString());
            }
            
        }
        this.comment.commentQtrs = bulkCommentQtrs.join(";");

        for (let i in this.qtrSelected) {
            if (this.qtrSelected[i]) {
                let amdts = this.getNonCigarAmendDetails(this.qtrSelected[i]);
                for (let j in amdts) {
                    amdts[j].userComments = [];
                    // this.comment.qtr = this.qtrSelected[i].toString();
                    amdts[j].userComments.push(this.comment);
                    amdmnts.push(amdts[j]);
                }
            }
        }

        return amdmnts;
    }

    private getCigarAmendment() {

        let amdmnts: TTBAmendment[] = [];
        let amd: TTBAmendment = new TTBAmendment();

        amd.fiscalYr = Number(this.headerdata.fiscalYear);
        amd.ttbCompanyId = this.headerdata.companyId;
        amd.qtr = this.CIGAR_QUARTER;
        amd.tobaccoType = this.headerdata.tobaccoClass;

        let userComments = [];
        let comm = new TTBDetailUserComment();
        jQuery.extend(true, comm, this.comment);
        comm.qtr = amd.qtr;
        userComments.push(comm);


        amd.userComments = userComments;

        amdmnts.push(amd);
        return amdmnts;
    }

    private getNonCigarAmendDetails(qtr) {
        let amdmts: TTBAmendment[] = [];

        let amd: TTBAmendment = new TTBAmendment();
        amd.qtr = qtr;
        amd.fiscalYr = Number(this.headerdata.fiscalYear);
        amd.ttbCompanyId = this.headerdata.companyId;

        if (this.headerdata.tobaccoClass.indexOf("Chew") != -1) {
            let amd1: TTBAmendment = new TTBAmendment();
            jQuery.extend(true, amd1, amd);
            amd1.tobaccoType = "Chew";
            amdmts.push(amd1);

            let amd2: TTBAmendment = new TTBAmendment();
            jQuery.extend(true, amd2, amd);
            amd2.tobaccoType = "Snuff";
            amdmts.push(amd2);
        }

        else if (this.headerdata.tobaccoClass.indexOf("Pipe") != -1) {
            let amd1: TTBAmendment = new TTBAmendment();
            jQuery.extend(true, amd1, amd);
            amd1.tobaccoType = "Pipe";
            amdmts.push(amd1);

            let amd2: TTBAmendment = new TTBAmendment();
            jQuery.extend(true, amd2, amd);
            amd2.tobaccoType = "Roll-Your-Own";
            amdmts.push(amd2);
        }

        else {
            // let qt = (this.headerdata.tobaccoClass === 'Cigars') ? this.CIGAR_QUARTER : qtr;
            amd.qtr = qtr;
            amd.tobaccoType = this.headerdata.tobaccoClass;
            amdmts.push(amd);
        }

        return amdmts;
    }

    cancelComment(event) {
        let targetId = event.currentTarget.id;
        if (!this.isEmpty(targetId)) {
            let modal = jQuery("#" + targetId).closest("#add-comment");
            modal.modal('toggle');
            /** clear the comment popup error messages */
            jQuery('#addcomment-form').parsley().reset();
            this.hideModal.emit(true);
        }
    }

    isEmpty(str) {
        return (!str || 0 === str.length);
    }

}
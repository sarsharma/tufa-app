import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import { Subscription } from 'rxjs';
import { AdminService } from '../../../../../admin/services/admin.service';
import { ShareDataService } from '../../../../../core/sharedata.service';
import { CustomNumberPipe } from '../../../../../shared/util/custom.number.pipe';
import { AcceptanceFlagState } from '../../../models/at-acceptance-flag-state.model';
import { AnnualTrueUpComparisonDetailsHeader } from '../../../models/at-detailsheader.model';
import { TTBAmendment } from '../../../models/at-ttbamendment.model';
import { TTBTaxesVol } from '../../../models/at-ttbtaxesvol.model';
import { AnnualTrueUpComparisonEventService } from '../../../services/at-compare.event.service';
import { AnnualTrueUpComparisonService } from '../../../services/at-compare.service';
import { AtReallocationService } from '../../../services/at-reallocation.service';
import { MixedActionManu } from '../../../models/at-mixedaction.model';

declare let jQuery: any;

@Component({
    selector: '[at-comparedetailsamend]',
    templateUrl: './at-comparedetailsamend.template.html',
    styleUrls: ['at-comparedetailsamend.style.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [CustomNumberPipe]
})

export class AnnualTrueUpCompareDetailsAmendComponent implements OnInit, OnDestroy {
    @Input() amendInfo;
    @Input() quarterInfo;
    @Input() headerdata: AnnualTrueUpComparisonDetailsHeader;
    @Input() acceptedFlag;
    @Input() acceptanceFlag;
    @Input() progressedflag;

    taxRateData: any;
    pristine: boolean = true;
    savedAmendment: boolean = false;
    activeqtr;

    @Output() amendTotalUpdated = new EventEmitter<boolean>();
    @Output() saveAmend = new EventEmitter<boolean>();
    amendTotalByQtrByTT: TTBAmendment[][] = [];

    quarter: any;
    amenddata: any;
    amendTotal: boolean = false;
    busy: Subscription;

    fdaAcceptSubscription: Subscription;

    CIGAR_QUARTER = 5;
    //router: Router;

    ttIdxMp = { "Cigars": 0, "Cigarettes": 1, "Chew": 2, "Snuff": 3, "Pipe": 4, "Roll Your Own": 5 }


    subscriptions: Subscription[] = [];

    disableAmendApprove: boolean = false;

    mixedActionFlag: string;
    mixedAction: MixedActionManu;

    constructor(private annualTrueUpComparisonService: AnnualTrueUpComparisonService,
        private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
        private reallocationEventService: AtReallocationService,
        public router: Router, private numberPipe: CustomNumberPipe, private _adminService: AdminService, public _shareData: ShareDataService) {

        //Disable amend/approve button
        //this.disableAmendApprove = _shareData.disableAmendApprove;
        if (_shareData.disableAmendApprove) {
            jQuery('#amd-tot-inputs-' + this.quarter + ' input[type="text"]').prop('disabled', true);
        }

        this.amendTotalByQtrByTT[0] = this.newAmndArray();
        this.amendTotalByQtrByTT[1] = this.newAmndArray();
        this.amendTotalByQtrByTT[2] = this.newAmndArray();
        this.amendTotalByQtrByTT[3] = this.newAmndArray();
        this.amendTotalByQtrByTT[4] = this.newAmndArray();

        this.annualTrueUpComparisonEventService.atCompareActiveQuarterUpdateSource$.subscribe(
            actqtr => {
                this.activeqtr = actqtr;
            }
        );

        this.fdaAcceptSubscription = this.annualTrueUpComparisonEventService.atCompareAcceptFDASource$.subscribe(
            data => {

                let flags: Boolean[] = data.flags;
                let ttbVolTax: TTBTaxesVol[] = data.ttbVolTax;

                let redirectCompareLanding: Boolean = flags[1];
                let progressflag = flags[2];
                let fda = flags[3];

                let amdArry = [];
                if (progressflag) {
                    this.amendTotalUpdated.emit(false);
                    this.savedAmendment = false;
                    this.amendTotal = false;
                }
                if (this.headerdata.tobaccoClass !== "Cigars" && this.activeqtr != this.quarter) return;

                else if (this.headerdata.tobaccoClass === "Cigars" && this.quarter != this.CIGAR_QUARTER) return;

                if (this.headerdata.tobaccoClass.indexOf("Pipe") != -1) {
                    let amd1 = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Pipe"]], "Pipe");
                    let amd2 = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Roll Your Own"]], "Roll-Your-Own");
                    if (progressflag) {
                        amd1.inProgressFlag = 'Y';
                        amd2.inProgressFlag = 'Y';
                        amd1.acceptanceFlag = null
                        amd2.acceptanceFlag = null
                    } else if (fda) {
                        amd1.inProgressFlag = 'N';
                        amd2.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'Y';
                        amd2.acceptanceFlag = 'Y';
                    }
                    else {
                        amd1.inProgressFlag = 'N';
                        amd2.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'I';
                        amd2.acceptanceFlag = 'I';
                        amd1.acceptedIngestedTotalTax = ttbVolTax && ttbVolTax.find(t => t.tobaccoClass === 'Pipe') ? ttbVolTax.find(t => t.tobaccoClass === 'Pipe').amendedTax : 0;
                        amd2.acceptedIngestedTotalTax = ttbVolTax && ttbVolTax.find(t => t.tobaccoClass === 'Roll-Your-Own') ? ttbVolTax.find(t => t.tobaccoClass === 'Roll-Your-Own').amendedTax : 0;
                    }

                    amd1.qtr = this.quarter;
                    amd2.qtr = this.quarter;
                    amdArry.push(amd1);
                    amdArry.push(amd2);
                }
                else if (this.headerdata.tobaccoClass.indexOf("Chew") != -1) {
                    let amd1 = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Chew"]], "Chew");
                    let amd2 = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Snuff"]], "Snuff");
                    if (progressflag) {
                        amd1.inProgressFlag = 'Y';
                        amd2.inProgressFlag = 'Y';
                        amd1.acceptanceFlag = null
                        amd2.acceptanceFlag = null
                    } else if (fda) {
                        amd1.inProgressFlag = 'N';
                        amd2.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'Y';
                        amd2.acceptanceFlag = 'Y';
                    }
                    else {
                        amd1.inProgressFlag = 'N';
                        amd2.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'I';
                        amd2.acceptanceFlag = 'I';
                        amd1.acceptedIngestedTotalTax = ttbVolTax && ttbVolTax.find(t => t.tobaccoClass === 'Chew') ? ttbVolTax.find(t => t.tobaccoClass === 'Chew').amendedTax : 0;
                        amd2.acceptedIngestedTotalTax = ttbVolTax && ttbVolTax.find(t => t.tobaccoClass === 'Snuff') ? ttbVolTax.find(t => t.tobaccoClass === 'Snuff').amendedTax : 0;
                    }
                    amd1.qtr = this.quarter;
                    amd2.qtr = this.quarter;
                    amdArry.push(amd1);
                    amdArry.push(amd2);
                } else if (this.headerdata.tobaccoClass.indexOf("Cigarettes") != -1) {
                    let amd1 = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Cigarettes"]], "Cigarettes");
                    if (progressflag) {
                        amd1.inProgressFlag = 'Y';
                        amd1.acceptanceFlag = null
                    }
                    else if (fda) {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'Y';
                    } else {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'I';
                    }
                    amd1.qtr = this.quarter;
                    amdArry.push(amd1);
                } else if (this.headerdata.tobaccoClass.indexOf("Cigars") != -1) {
                    let amd1 = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.quarter - 1][this.ttIdxMp["Cigars"]], "Cigars");
                    if (progressflag) {
                        amd1.inProgressFlag = 'Y';
                        amd1.acceptanceFlag = null
                    }
                    else if (fda) {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'Y';
                    } else {
                        amd1.inProgressFlag = 'N';
                        amd1.acceptanceFlag = 'I';
                    }
                    amd1.qtr = this.quarter;
                    amdArry.push(amd1);
                }

                for (let i in amdArry) {
                    // if (amdArry[i].amendedTotalTax) amdArry[i].amendedTotalTax = amdArry[i].amendedTotalTax.toString().replace(/,/g, "");
                    // if (amdArry[i].amendedTotalVolume) amdArry[i].amendedTotalVolume = amdArry[i].amendedTotalVolume.toString().replace(/,/g, "");
                    if (amdArry[i].amendedTotalTax) amdArry[i].amendedTotalTax = null;
                    if (amdArry[i].amendedTotalVolume) amdArry[i].amendedTotalVolume = null;
                    //resetting the flag after user takes an action from the UI
                    amdArry[i].adjProcessFlag = 'N';
                    if(amdArry[i].acceptanceFlag !== 'M')
                        amdArry[i].mixedActionQuarterDetails = null;
                }

                this.busy = this.annualTrueUpComparisonService.updateTTBCompareAcceptFDA(amdArry, this.headerdata.fiscalYear).subscribe(
                    data => {

                        if (data) {
                            for (let i in data) {
                                let qtr: number = data[i].qtr;
                                let ttname = data[i].tobaccoType.replace(/-/g, " ");
                                let amdmnt = new TTBAmendment();
                                jQuery.extend(true, amdmnt, data[i]);
                                this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]] = amdmnt;
                                //this.annualTrueUpComparisonEventService.triggerAtCompareSuccessfullSaveAction(data);
                            }
                            this.annualTrueUpComparisonEventService.triggerAtCompareSuccessfullSaveAction(data);
                            // this.annualTrueUpComparisonEventService.triggerAtCompareCommentsUpdate(data);
                            if (redirectCompareLanding)
                                this.router.navigate(["./app/assessments/annual/" + this._shareData.fiscalYear]);
                        }
                    }
                )

            }
        )

        this.subscriptions.push(this.fdaAcceptSubscription);

        this.subscriptions.push(this.annualTrueUpComparisonEventService.atCompareCommentsSaveSource$.subscribe(
            data => {
                let amnds: TTBAmendment[] = data;
                for (let i in amnds) {
                    let qtr = amnds[i].qtr;
                    let ttname = amnds[i].tobaccoType.replace(/-/g, " ");
                    this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]].comments = amnds[i].comments;
                    if (!this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]].ttbAmendmentId)
                        this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]].ttbAmendmentId = amnds[i].ttbAmendmentId;
                }
            }));

        this.subscriptions.push(this.annualTrueUpComparisonEventService.tobaccoClassSelected$.subscribe(
            data => {
                this.getTotalAmendment();
            }
        ));

        this.subscriptions.push(this.annualTrueUpComparisonEventService.atCompareAcceptMixedAction$.subscribe(
            data => {
                this.mixedActionFlag = data[0];
                this.mixedAction = data[1];
                if(this.activeqtr != this.quarter) return;
                this.saveMixedAction();
            }));
    }



    private newAmndArray() {
        let amndForTTArry: TTBAmendment[] = [];
        amndForTTArry[0] = this.newAdmt();
        amndForTTArry[1] = this.newAdmt();
        amndForTTArry[2] = this.newAdmt()
        amndForTTArry[3] = this.newAdmt()
        amndForTTArry[4] = this.newAdmt()
        amndForTTArry[5] = this.newAdmt()
        return amndForTTArry;
    }

    ngOnInit() {
        this.amendTotal = false;
        this.loadRates();
    }

    ngAfterViewInit(): void {
        this.enableAmendTotal(this.amendTotal);
    }

    ngOnChanges(changes: SimpleChanges) {
        // tslint:disable-next-line:forin
        for (let propName in changes) {
            if (propName === "quarterInfo") {
                this.quarter = changes[propName].currentValue;
            }
            if (propName === "amendInfo") {
                this.amenddata = changes[propName].currentValue;
            }
            if (propName === "acceptedFlag") {
                if (changes[propName].currentValue)
                    this.savedAmendment = false;
            }
            if (propName === "acceptanceFlag") {
                this.acceptanceFlag = changes[propName].currentValue;
                this.disableAmendApprove = this.acceptanceFlag === AcceptanceFlagState.DELTA_CHANGE_ZERO;
            }

            if (propName === "progressedflag") {
                this.progressedflag = changes[propName].currentValue;

            }

        }
    }

    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }
        if (this.fdaAcceptSubscription) {
            this.fdaAcceptSubscription.unsubscribe();
        }
        for (let i in this.subscriptions)
            this.subscriptions[i].unsubscribe();
    }


    enableAmendtotalToggle(enable: boolean) {
        //jQuery('#amend-total-'+this.quarter).bootstrapSwitch('disabled',!enable);
    }

    enableAmendTotal(event: any) {
        jQuery('#amd-tot-inputs-' + this.quarter + ' input[type="text"]').prop('disabled', !event);
        this.amendTotalUpdated.emit(event);
    }

    changeAmend($event) {
        this.savedAmendment = false;
        this.pristine = false;
    }

    taxAmountChange(amendment: TTBAmendment, tobaccoClassNm: string) {
        this.savedAmendment = false;
        if (this.pristine && tobaccoClassNm.indexOf("Cigars") == -1) {
            amendment.amendedTotalVolume = this.numberPipe.transform(Number(amendment.amendedTotalTax) / this.tobaccoReturnType(tobaccoClassNm, this.taxRateData).taxRate, 2);
        }

    }

    loadRates() {
        this._adminService.getTobaccoClasses().subscribe(tobaccoClasses => {
            if (tobaccoClasses) {
                this.taxRateData = tobaccoClasses;
                this.getTotalAmendment();
            }
        });
    }

    public getTotalAmendment() {


        /**Temporary check to return if CBP details page. NEEDS TO BE  REMOVED when CBP Details Page User story is up for implementation */
        if (this.headerdata.permitType === 'Importer') return;

        let amdmnts: TTBAmendment[] = [];
        if ((this.headerdata.tobaccoClass === "Chew/Snuff") ||
            (this.headerdata.tobaccoClass === "Chew-Snuff")) {
            amdmnts[0] = this.newAdmt("Chew", this.quarter)
            amdmnts[1] = this.newAdmt("Snuff", this.quarter);
        } else if (['Pipe/Roll Your Own', 'Pipe-Roll Your Own', 'Pipe/Roll-Your-Own'].includes(this.headerdata.tobaccoClass)) {
            amdmnts[0] = this.newAdmt("Pipe", this.quarter)
            amdmnts[1] = this.newAdmt("Roll-Your-Own", this.quarter);
        } else if (this.headerdata.tobaccoClass === "Cigarettes") {
            amdmnts[0] = this.newAdmt("Cigarettes", this.quarter);
        } else if (this.headerdata.tobaccoClass === "Cigars") {
            amdmnts[0] = this.newAdmt("Cigars", this.quarter);
        }

        const currentQtr = this.quarter;

        this.busy = this.annualTrueUpComparisonService.getTTBAmendments(Number(this.headerdata.fiscalYear), Number(this.headerdata.companyId), amdmnts).subscribe(
            data => {
                if (data) {
                    for (let i in data) {
                        if (data[i].amendmentPresent) {
                            data[i].amendedTotalTax = this.numberPipe.transform(data[i].amendedTotalTax);
                            data[i].amendedTotalVolume = this.numberPipe.transform(data[i].amendedTotalVolume);
                            let ttname = data[i].tobaccoType.replace(/-/g, " ");
                            let qtr = (ttname === 'Cigars') ? this.CIGAR_QUARTER : data[i].qtr;
                            let amdmnt = new TTBAmendment();
                            jQuery.extend(true, amdmnt, data[i]);
                            this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]] = amdmnt;

                            if (currentQtr === Number(qtr) && data[i].acceptanceFlag === 'N') {
                                this.savedAmendment = true;
                                this.enableAmendTotal(true);
                                this.amendTotal = true;
                            } else if (currentQtr === Number(qtr) && (data[i].acceptanceFlag === 'Y' || (data[i].acceptanceFlag === 'X' && !this.disableAmendApprove) || !data[i].acceptanceFlag)) {
                                this.amendTotalUpdated.emit(false);
                            }
                        }


                    }

                    if (data.length == 0) {
                        this.amendTotalUpdated.emit(false);
                    }

                    // this.annualTrueUpComparisonEventService.triggerAtCompareCommentsUpdate(data);
                }
                this.annualTrueUpComparisonEventService.triggerAtCompareAmendmentsLoaded(true);
            }

        )

        this.subscriptions.push(this.busy);
    }

    private newAdmt(ttname?: string, quarter?: number) {
        let amdnt = new TTBAmendment();
        if (ttname) amdnt.tobaccoType = ttname;
        if (quarter) amdnt.qtr = quarter;
        return amdnt;
    }


    public saveTotalAmendment(flag?: boolean) {
        /**Temporary check to return if CBP details page. NEEDS TO BE  REMOVED when CBP Details Page User story is up for implementation */
        if (this.headerdata.permitType === 'Importer') return;

        let amndmnts: TTBAmendment[] = [];
        if (this.headerdata.tobaccoClass === "Chew-Snuff" || this.headerdata.tobaccoClass === "Chew/Snuff") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Chew"]], "Chew");
            amndmnts[1] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Snuff"]], "Snuff");
        } else if (this.headerdata.tobaccoClass === "Pipe-Roll Your Own" || this.headerdata.tobaccoClass === "Pipe/Roll Your Own") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Pipe"]], "Pipe");
            amndmnts[1] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Roll Your Own"]], "Roll-Your-Own");
        } else if (this.headerdata.tobaccoClass === "Cigarettes") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Cigarettes"]], "Cigarettes");
        } else if (this.headerdata.tobaccoClass === "Cigars") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.quarter - 1][this.ttIdxMp["Cigars"]], "Cigars", this.quarter);
        }

        for (let i in amndmnts) {
            amndmnts[i].acceptanceFlag = 'N';
            amndmnts[i].inProgressFlag = 'N';
            if (amndmnts[i].amendedTotalTax && (typeof amndmnts[i].amendedTotalTax === 'string' || amndmnts[i].amendedTotalTax instanceof String))
                amndmnts[i].amendedTotalTax = amndmnts[i].amendedTotalTax.toString().replace(/,/g, "");
            if (amndmnts[i].amendedTotalVolume && (typeof amndmnts[i].amendedTotalVolume === 'string' || amndmnts[i].amendedTotalVolume instanceof String))
                amndmnts[i].amendedTotalVolume = amndmnts[i].amendedTotalVolume.toString().replace(/,/g, "");
        }

        const currentQtr = this.quarter;
        this.busy = this.annualTrueUpComparisonService.saveTTBAmendments(amndmnts, this.headerdata.fiscalYear).subscribe(
            data => {
                if (data) {
                    for (let i in data) {
                        data[i].amendedTotalTax = this.numberPipe.transform(data[i].amendedTotalTax);
                        data[i].amendedTotalVolume = this.numberPipe.transform(data[i].amendedTotalVolume);
                        let qtr: number = data[i].qtr;
                        let ttname = data[i].tobaccoType.replace(/-/g, " ");
                        let amdmnt = new TTBAmendment();
                        jQuery.extend(true, amdmnt, data[i]);
                        this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]] = amdmnt;
                        if (currentQtr === Number(qtr) && data[i].acceptanceFlag === 'N') {
                            this.savedAmendment = true;
                        } else { this.savedAmendment = false; }

                        //this.annualTrueUpComparisonEventService.triggerAtCompareSuccessfullSaveAction(data);
                    }
                    this.annualTrueUpComparisonEventService.triggerAtCompareSuccessfullSaveAction(data);
                    // this.annualTrueUpComparisonEventService.triggerAtCompareCommentsUpdate(data);
                    if (flag) this.router.navigate(["/app/assessments/annual/" + this.headerdata.fiscalYear]);
                    this.saveAmend.emit(true);
                }
            }
        );

    }

    public saveMixedAction() {
        /**Temporary check to return if CBP details page. NEEDS TO BE  REMOVED when CBP Details Page User story is up for implementation */
        if (this.headerdata.permitType === 'Importer') return;

        let amndmnts: TTBAmendment[] = [];
        if (this.headerdata.tobaccoClass === "Chew-Snuff" || this.headerdata.tobaccoClass === "Chew/Snuff") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Chew"]], "Chew");
            amndmnts[1] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Snuff"]], "Snuff");
        } else if (this.headerdata.tobaccoClass === "Pipe-Roll Your Own" || this.headerdata.tobaccoClass === "Pipe/Roll Your Own") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Pipe"]], "Pipe");
            amndmnts[1] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Roll Your Own"]], "Roll-Your-Own");
        } else if (this.headerdata.tobaccoClass === "Cigarettes") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.activeqtr - 1][this.ttIdxMp["Cigarettes"]], "Cigarettes");
        } else if (this.headerdata.tobaccoClass === "Cigars") {
            amndmnts[0] = this.populateIfNewAmnd(this.amendTotalByQtrByTT[this.quarter - 1][this.ttIdxMp["Cigars"]], "Cigars", this.quarter);
        }

        if(this.mixedActionFlag && this.mixedActionFlag===AcceptanceFlagState.MIXEDACTION){
            for (let i in amndmnts) {
                amndmnts[i].acceptanceFlag = 'M';
                amndmnts[i].inProgressFlag = 'N';
                if(i === "0"){
                    amndmnts[i].amendedTotalTax = this.mixedAction.amendedTotalTax1;
                    amndmnts[i].amendedTotalVolume = this.mixedAction.amendedTotalVolume1;
                }else{
                    amndmnts[i].amendedTotalTax = this.mixedAction.amendedTotalTax2;
                    amndmnts[i].amendedTotalVolume = this.mixedAction.amendedTotalVolume2;
                }
                amndmnts[i].mixedActionQuarterDetails = {};
                this.mixedAction.mixedActionDetailsMap.forEach((value: string, key: string) =>{
                    amndmnts[i].mixedActionQuarterDetails[key] = value;
                })
            }
        }
        const currentQtr = this.quarter;
        this.busy = this.annualTrueUpComparisonService.saveTTBAmendments(amndmnts, this.headerdata.fiscalYear).subscribe(
            data => {
                if (data) {
                    for (let i in data) {
                        data[i].amendedTotalTax = this.numberPipe.transform(data[i].amendedTotalTax);
                        data[i].amendedTotalVolume = this.numberPipe.transform(data[i].amendedTotalVolume);
                        let qtr: number = data[i].qtr;
                        let ttname = data[i].tobaccoType.replace(/-/g, " ");
                        let amdmnt = new TTBAmendment();
                        jQuery.extend(true, amdmnt, data[i]);
                        this.amendTotalByQtrByTT[qtr - 1][this.ttIdxMp[ttname]] = amdmnt;
                        if (currentQtr === Number(qtr) && data[i].acceptanceFlag === 'N') {
                            this.savedAmendment = true;
                        } else { this.savedAmendment = false; }
                    }
                    this.annualTrueUpComparisonEventService.triggerAtCompareSuccessfullSaveAction(data);
                    this.saveAmend.emit(true);     
                    if(data[0].tobaccoType !== 'Cigars')              
                        this.annualTrueUpComparisonEventService.triggerAtCompareMixedActionRefresh(data);
                    else
                        this.annualTrueUpComparisonEventService.triggerAtCompareMixedActionCigarRefresh(data);
                }
            }
        );
    }


    saveContTotalAmendment() {
        this.saveTotalAmendment(true);
    }

    public populateIfNewAmnd(amndmnt: TTBAmendment, ttname, quarter?: number) {
        amndmnt.fiscalYr = Number(this.headerdata.fiscalYear);
        amndmnt.qtr = quarter ? quarter : this.activeqtr;
        amndmnt.ttbCompanyId = this.headerdata.companyId;
        amndmnt.tobaccoType = ttname;
        amndmnt.acceptedIngestedTotalTax = null;
        return amndmnt;
    }

    continueWithoutSave() {
        this.router.navigate(["/app/assessments/annual/" + this.headerdata.fiscalYear]);
    }

    public tobaccoReturnType(className: string, rateObj: any) {
        for (let key in rateObj) {
            if (rateObj.hasOwnProperty(key) && rateObj[key].tobaccoClassNm.replace(/-/g, " ") === className) {
                return rateObj[key];
            }
        }
    }
}

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import * as _ from 'lodash';
import { Subscription } from "rxjs";
import { ShareDataService } from '../../../../../core/sharedata.service';
import { PermitPeriodService } from '../../../../../permit/services/permit-period.service';
import { DownloadSerivce } from '../../../../../shared/service/download.service';
import { AcceptanceFlagState } from '../../../models/at-acceptance-flag-state.model';
import { CompareAcceptFDASourceModel } from '../../../models/at-compare-accept-fda-source.moduel';
import { AnnualTrueUpManufacturerDetail } from '../../../models/at-compare-manufacturerdetails.model';
import { taxes } from '../../../models/at-taxes.model';
import { AnnualTrueUpComparisonDetailsHeader } from "./../../../models/at-detailsheader.model";
import { ToggleTabModel } from "./../../../models/at-toggletab.model";
import { AnnualTrueUpComparisonEventService } from "./../../../services/at-compare.event.service";
import { AnnualTrueUpComparisonService } from "./../../../services/at-compare.service";

declare var jQuery: any;

@Component({
    selector: '[at-comparemandetails]',
    templateUrl: './at-comparemandetails.template.html',
    styleUrls:['at-comparemandetails.style.scss'],
    providers: [PermitPeriodService],
    encapsulation: ViewEncapsulation.None
})


export class AnnualTrueUpCompareManufacturerDetailsComponent implements OnInit, OnDestroy {
    @Input() quarter;
    @Input() fda3852map;
    flagParam: string;
    // @Output() hideTab = new EventEmitter<number>();
    @Output() toggleTab = new EventEmitter<ToggleTabModel>();
    @Output() setDelta = new EventEmitter<string>();
    context = 'ttbdetail';

    CIGAR_QUARTER = 5;
    router: Router;
    alerts: { type: string; msg: string; }[];
    collapseAll: string = "";
    shareData: ShareDataService;
    isValid: boolean = false;
    inProgressDisabled: boolean;

    busySummary: Subscription;
    busyDetails: Subscription[];
    activequarter: any;
    quarters: any[];
    expandedRowId: any;
    months: ["October", "November", "December", "January", "February", "March", "April", "May", "June", "July", "August", "September"];

    createdDate: string;
    submitted: string;
    acceptedFlag: boolean = false;
    inprogressFlag: boolean = false;

    headerdata: AnnualTrueUpComparisonDetailsHeader = new AnnualTrueUpComparisonDetailsHeader();
    comparedetailsSummary: any;
    //This should be of type AnnualTrueUpManufacturerDetail[][]. 
    comparedetails: AnnualTrueUpManufacturerDetail[][];
    amendTobaccoClasses: any;

    tcdualClass1: String;
    tcdualClass2: String;
    tcdualClassCombined: String;

    disableAmendApprove: boolean = false;

    taxes1 : taxes = {totalIngestedTax:0,totalTUFATax:0};

    calTaxes: taxes[] = [this.taxes1, this.taxes1, this.taxes1, this.taxes1, this.taxes1]

    constructor(
        router: Router,
        private atcomparisonservice: AnnualTrueUpComparisonService,
        private _shareData: ShareDataService,
        private _permitPeriodService: PermitPeriodService,
        private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
        private _downloadService: DownloadSerivce) {

        this.alerts = [
            {
                type: 'success',
                msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Message</span>'
            }
        ];

        this.router = router;
        this.shareData = _shareData;
        this.readfromSharedData();
        this.expandedRowId = "";


    }

    ngOnInit() {
        this.alerts = [];
        this.busyDetails = [];
        this.comparedetails = [];
        this.quarters = _.range(1, 5);
        
    }


    ngAfterViewInit() {
        //this.isValid = false;
        // for(var i = 0; i <= 5; i++){
        //     this.calTaxes[i] = this.getTaxesFromDetailsForQuarter(i);
        //  }
        
    }

    ngOnDestroy() {

        if (this.busySummary) {
            this.busySummary.unsubscribe();
        }

        for (let i in this.busyDetails) {
            let subcription = this.busyDetails[i];
            subcription.unsubscribe();
        }
    }




    toAnnualTrueUp() {
        this.router.navigate(["/./../../../assessments/annual/" + this.shareData.fiscalYear]);
    }

    toAssessments() {
        this.router.navigate(["/app/assessments"]);
    }

    toPeriodofActivity(period, detail) {
        let localSrc = this.shareData.detailsSrc;
        let localflag = this.shareData.acceptanceFlag;
        let localtype = this.shareData.permitType;
        let localClass = this.shareData.tobaccoClass;
        let localCompany = this.shareData.companyName;
        let quarter = this.shareData.quarter;
        this.shareData.reset();
        this.shareData.companyName = localCompany;
        this.shareData.acceptanceFlag = localflag;
        this.shareData.permitType = localtype;
        this.shareData.tobaccoClass = localClass;
        this.shareData.detailsSrc = localSrc;
        this.shareData.companyName = this.headerdata.companyName;
        this.shareData.permitNum = period.permit;
        this.shareData.period = detail.month + ' FY ' + this.headerdata.fiscalYear;
        this.shareData.permitId = period.permitId;
        this.shareData.periodId = detail.periodId;
        this.shareData.companyId = this.headerdata.companyId;

        this.shareData.periodStatus = this.getReportStatus(detail.status);
        this.shareData.previousRoute = this.router.url;
        this.shareData.isRecon = false;
        this.shareData.breadcrumb = 'at-comparedetails';
        this.shareData.qatabfrom = 'at-compare';
        this.shareData.createdDate = this.createdDate;
        this.shareData.submitted = this.submitted;

        this.shareData.quarter = quarter;
        this.shareData.fiscalYear = Number(this.headerdata.fiscalYear);
        // this.shareData.months = this.months;
    }

    setParamsForReconNavigation() {
        this.shareData.reset();
    }

    clicked(event) {
        let rowId = event.currentTarget.id;
        let isExpanded = jQuery('#' + rowId).attr("aria-expanded");
        if (isExpanded === "true") {
            jQuery('#' + rowId).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
            this.expandedRowId = "";
        } else {
            jQuery('#' + rowId).find('i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
            this.expandedRowId = rowId;
        }
        event.preventDefault();
    }

    readfromSharedData() {
        this.headerdata.companyName = this.shareData.companyName;
        this.headerdata.fiscalYear = this.shareData.fiscalYear.toString();
        this.headerdata.tobaccoClass = this.shareData.tobaccoClass;
        this.headerdata.permitType = this.shareData.permitType;
        this.headerdata.companyId = this.shareData.companyId;
        //this.headerdata.quarter = this.shareData.quarter;
        this.createdDate = this.shareData.createdDate;
        this.submitted = this.shareData.submitted;

        this.disableAmendApprove = this.shareData.disableAmendApprove;

        if ((this.headerdata.tobaccoClass === "Chew/Snuff") ||
            (this.headerdata.tobaccoClass === "Chew-Snuff")) {
            this.amendTobaccoClasses = [{ "name": "Chew" }, { "name": "Snuff" }];
            this.tcdualClass1 = "Chew";
            this.tcdualClass2 = "Snuff";
            this.tcdualClassCombined = "Chew/Snuff";
        } else if ((this.headerdata.tobaccoClass === "Pipe/Roll Your Own") || (this.headerdata.tobaccoClass === "Pipe-Roll Your Own")) {
            this.amendTobaccoClasses = [{ "name": "Pipe" }, { "name": "Roll Your Own" }];
            this.tcdualClass1 = "Pipe";
            this.tcdualClass2 = "Roll Your Own";
            this.tcdualClassCombined = "Pipe/Roll Your Own";
            this.headerdata.tobaccoClass = "Pipe/Roll Your Own";
        } else {
            this.amendTobaccoClasses = [{ "name": this.headerdata.tobaccoClass }];
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "quarter") {
                this.activequarter = changes[propName].currentValue;
                this.getDetailsSummary();
                this.getFDA3852Details();
            }
        }
    }


    disableAcceptanceApprove() {
        if (this.comparedetailsSummary) {
            let totalVals: any[] = this.comparedetailsSummary[this.comparedetailsSummary.length - 1];
            let cellValues: any[] = totalVals["cellValues"];
            let diffIngestedFDA = cellValues[cellValues.length - 1];
            let amendFlag = this.fda3852map[this.quarter];
            if ((!amendFlag || amendFlag === "") && Math.abs(Number(diffIngestedFDA)) < 1) {
                this.fda3852map[this.quarter] = AcceptanceFlagState.DELTA_CHANGE_ZERO;
            }
        }
    }

    getDetailsSummary() {
        if (this.busySummary) {
            this.busySummary.unsubscribe();
        }

        if (this.headerdata.tobaccoClass.toUpperCase() === "CIGARS") {
            this.busySummary =
                // tslint:disable-next-line:max-line-length
                this.atcomparisonservice.getCigarManufacturerComparisonDetailsSummary(this.shareData.companyId, this.shareData.fiscalYear).subscribe(data => {
                    if (data) {
                        this.comparedetailsSummary = data;
                        this.disableAcceptanceApprove();
                    }
                },
                    error => {
                        this.alerts = [];
                        this.alerts.push({ type: 'warning', msg: error });
                    });
        } else {
            this.headerdata.tobaccoClass = this.headerdata.tobaccoClass.replace(/\//g, "-");
            this.busySummary =
                // tslint:disable-next-line:max-line-length
                this.atcomparisonservice.getNonCigarManufacturerComparisonDetailsSummary(this.shareData.companyId,
                    this.shareData.fiscalYear, this.activequarter, this.headerdata.tobaccoClass).subscribe(data => {
                        if (data) {
                            this.comparedetailsSummary = data;
                            var totalnewRowIndex;
                            let totalRowIndex = _.findIndex(data, function (o) { return (o.cellValues[0] && o.cellValues[0].toString().toUpperCase() === 'TOTAL') });
                            this.disableAcceptanceApprove();
                            this.getDelta(this.comparedetailsSummary);
                        }
                    },
                    error => {
                        this.alerts = [];
                        this.alerts.push({ type: 'warning', msg: error });
                    });
        }
    }


    getFDA3852CigarDetails(quarter: number) {
        if (this.busyDetails[quarter]) {
            this.busyDetails[quarter].unsubscribe();
        }

        // Get Data For Quarter 1
        this.busyDetails[quarter] =
            // tslint:disable-next-line:max-line-length
            this.atcomparisonservice.getCigarManufacturerComparisonDetails(this.shareData.companyId, this.shareData.fiscalYear,
                quarter + 1, this.headerdata.tobaccoClass).subscribe(data => {
                    if (data) {
                        this.comparedetails[quarter] = data;
                    }
                },
                error => {
                    this.alerts = [];
                    this.alerts.push({ type: 'warning', msg: error });
                });
    }

    getFDA3852NonCigarDetails(quarter: number) {
        if (this.busyDetails[quarter]) {
            this.busyDetails[quarter].unsubscribe();
        }

        // Get Data For Quarter 1
        this.busyDetails[quarter] =
            // tslint:disable-next-line:max-line-length
            this.atcomparisonservice.getNonCigarManufacturerComparisonDetails(this.shareData.companyId, this.shareData.fiscalYear,
                quarter + 1, this.headerdata.tobaccoClass).subscribe(data => {
                    if (data) {
                        this.comparedetails[quarter] = data;
                        this.calTaxes[quarter] = this.getTaxesFromDetailsForQuarter(quarter+1);
                    }
                },
                error => {
                    this.alerts = [];
                    this.alerts.push({ type: 'warning', msg: error });
                });
    }

    getFDA3852Details() {
        this.busyDetails = [];
        this.comparedetails = [];

        // For cigars retrieve all quarters
        if (this.headerdata.tobaccoClass.toUpperCase() === "CIGARS") {
            this.getFDA3852CigarDetails(0);
            this.getFDA3852CigarDetails(1);
            this.getFDA3852CigarDetails(2);
            this.getFDA3852CigarDetails(3);
        } else {
            // For other tobacco classes retrieve only active quarters
            if (this.headerdata.tobaccoClass.toUpperCase() === "CIGARETTES") {
                this.getFDA3852CigarDetails(this.activequarter - 1);
            } else {
                this.getFDA3852NonCigarDetails(this.activequarter - 1);
            }
        }
    }

    amendTotalFlagSwitched(flag: boolean) {
        this.isValid = flag;
    }

    isDualTobaccoClass(tclass: any) {
        if (tclass.toUpperCase() === "CHEW-SNUFF" ||
            tclass.toUpperCase() === "PIPE-ROLL YOUR OWN") {
            return true;
        }
        return false;
    }

    getTotalDeltaForPeriod(qtr, permit, ttType): any {
       if (typeof this.comparedetails[qtr] !== 'undefined') {

            let permits = this.comparedetails[qtr];
            let tTaxes: number[] = [];

            if (ttType && ttType === "Cigars") {
                for (let i in permits) {
                    if (this.isNumeric(permits[i].ingestedTax))
                        tTaxes.push(0 - +permits[i].ingestedTax);

                    if (this.isNumeric(permits[i].periodTotal))
                        tTaxes.push(+permits[i].periodTotal);
                }
                return (tTaxes.length == 0) ? 'NA' : _.sum(tTaxes);
            }
           
        }
      

    }


    private isNumeric(n) {
        if ((!n && n != 0) || isNaN(n) || n == null )
            return false;

        return true;
    }


    collapseExpand() {

        if (this.collapseAll == "" || this.collapseAll == "Not Collapsed")
            this.collapseAll = "Collaspsed";
        else if (this.collapseAll == "Collaspsed")
            this.collapseAll = "Not Collapsed";
    }


    getAccordionStatus(qtr, permit, ttType) {
       if (this.collapseAll == "Collaspsed")
         {   
             return true;
         }

        if (this.collapseAll == "Not Collapsed")
        { 
           return false
        }

        let permits = this.comparedetails[qtr];
        let totalIngestedTax: number = 0;
        let totalPeriodtl: number = 0;

        for (let i in permits) {
                //for each permit
                if (permit === permits[i].permit) {
                    totalIngestedTax = +permits[i].ingestedTax;
                    if (ttType && ttType === "Cigars") {
                         totalPeriodtl = +permits[i].periodTotal;
                    }else{
                        totalPeriodtl = +permits[i].periodTotal + +(permits[i].periodTotal2 ? permits[i].periodTotal2 : "0");   
                    }
                    break;
                }
        }
        // isopen = false 
        //return  totalPeriodtl - totalIngestedTax != 0
        return  (!(Number.isNaN(totalPeriodtl)) || !(Number.isNaN(totalIngestedTax))) && !((totalPeriodtl - totalIngestedTax) == 0);
       

    }


    toggleDelta(delta, summaryDelta, isOneSided) {

        let togglevalue = new ToggleTabModel();
        let displayDelta = "";             
        if (isOneSided  &&  (summaryDelta!= 'NA'  &&  Math.abs(parseFloat(summaryDelta)) > 0.99)) {
            displayDelta = delta + " All Deltas";
        }else{
            displayDelta = delta;
        }
        switch (this.activequarter) {
            case 1:
                jQuery("#delta1").text(displayDelta);
                togglevalue.qtr = 1;
                break;
            case 2:
                jQuery("#delta2").text(displayDelta);
                togglevalue.qtr = 2;
                break;
            case 3:
                jQuery("#delta3").text(displayDelta);
                togglevalue.qtr = 3;
                break;
            case 4:
                jQuery("#delta4").text(displayDelta);
                togglevalue.qtr = 4;
                break;
        }

        delta = delta.replace('$', '');

        togglevalue.flag = false;
        if (Math.abs(parseFloat(summaryDelta)) > 0.99 && summaryDelta != 'NA' && !isOneSided) {
            togglevalue.flag = true;
        }

        this.toggleTab.emit(togglevalue);

    }

    getTaxesFromDetailsForQuarter(qtr): taxes {
        let totalIngestedTax: number = 0;
        let totalTUFATax: number = 0;
        let calTaxes1: taxes = new taxes();
        let permits = this.comparedetails[qtr - 1];

        for (let i in permits) {
           // totalIngestedTax += permits[i].ingestedTax == 'NA' ? 0 : +permits[i].ingestedTax;
            totalTUFATax += (permits[i].periodTotal == 'NA' ? 0 : +permits[i].periodTotal) + (permits[i].periodTotal2 == 'NA' || permits[i].periodTotal2 == null ? 0 : +permits[i].periodTotal2);
        }
        if (this.comparedetailsSummary && this.comparedetailsSummary.length > 1) {
            for (let i in this.comparedetailsSummary) {
                let numCols = this.comparedetailsSummary[i]["numColumns"];
                let cellVals = this.comparedetailsSummary[i]["cellValues"];

                if (cellVals[0] != null) {
                    if (cellVals[0].toUpperCase().includes("TOTAL")) {
                        totalIngestedTax = (cellVals[numCols - 2]);

                    }
                }
            }
        }
        calTaxes1.totalTUFATax = totalTUFATax;
        calTaxes1.totalIngestedTax = totalIngestedTax;
        return calTaxes1;
    }

    private getReportStatus(status: string) {
        if (status === "NS") {
            return "NSTD";
        } else if (status === "C") {
            return "COMP";
        } else if (status === "E") {
            return "ERRO";
        }
        return "";
    }

    savedAmendment($event) {
        this.fda3852map[this.activequarter] = AcceptanceFlagState.AMEND;
        this.inProgressDisabled = false
        this.acceptedFlag = false;
        this.inprogressFlag = false;
    }

   acceptedFDAflag($event: any) {
        let acceptAndContinueFlag: Boolean = false;

        if ($event.flag === AcceptanceFlagState.ACCEPT_FDA || $event.flag === AcceptanceFlagState.ACCEPT_FDA_CONTINUE) {
            this.acceptedFlag = true;
            this.inprogressFlag = false;
            this.inProgressDisabled = false;
            if ($event.flag === AcceptanceFlagState.ACCEPT_FDA_CONTINUE)
                acceptAndContinueFlag = true;
            else
                acceptAndContinueFlag = false;

            let accept: CompareAcceptFDASourceModel = new CompareAcceptFDASourceModel();
            accept.flags = [this.activequarter, acceptAndContinueFlag, this.inprogressFlag, this.acceptedFlag];
            accept.ttbVolTax = $event.ttbVolTaxes;

            this.annualTrueUpComparisonEventService.triggerAtCompareAcceptFDASource(accept);
        } else if ($event.flag === AcceptanceFlagState.ACCEPT_INGESTED || $event.flag === AcceptanceFlagState.ACCEPT_INGESTED_CONTINUE) {
            this.acceptedFlag = false;
            this.inprogressFlag = false;
            this.inProgressDisabled = false;
            if ($event.flag === AcceptanceFlagState.ACCEPT_INGESTED_CONTINUE)
                acceptAndContinueFlag = true;
            else
                acceptAndContinueFlag = false;

            let accept: CompareAcceptFDASourceModel = new CompareAcceptFDASourceModel();
            accept.flags = [this.activequarter, acceptAndContinueFlag, this.inprogressFlag, this.acceptedFlag];
            accept.ttbVolTax = $event.ttbVolTaxes;

            this.annualTrueUpComparisonEventService.triggerAtCompareAcceptFDASource(accept);
        }

        this.fda3852map[this.activequarter] = $event.flag;
    }


    progressedflag($event) {
        if ($event) {
            this.fda3852map[this.activequarter] = "InProgress";
            this.inProgressDisabled = true;
            this.inprogressFlag = true;
            
            
            let accept: CompareAcceptFDASourceModel = new CompareAcceptFDASourceModel();
            accept.flags = [this.activequarter, false, this.inprogressFlag];
            accept.ttbVolTax = $event.ttbVolTax;
            this.annualTrueUpComparisonEventService.triggerAtCompareAcceptFDASource(accept);
        }
    }


    enableLinkAuth(flag) {
        this._permitPeriodService.setLinkAuthentication(flag);
    }

    isNonCigar(tobaccoClass: any) {
        if (tobaccoClass.toUpperCase() === "CIGARS") {
            return false;
        }
        return true;
    }

    isNumDefined(value) {
        //Change if 0 considered defined 
        if (!value) return false;
        let val: number = value;
        if (typeof value === 'string')
            val = Number(value);
        //Change if 0 considered defined 
        return val ? true : false;

    }
    isDisplayDeltaForPermit(qtr) {
        let isDisplay = true;
        // If here is only one permit only show the Delta once 
       if (typeof this.comparedetails[qtr] !== 'undefined'&&  this.comparedetails[qtr].length === 1) {
          // If the quarter label in the TTB Details header has the same value as the Delta (there is only one permit) only show the Delta once 
            isDisplay = false;
        }
        return isDisplay;
    }
    exportRawdata() {
        let tobaccoClassName = this.shareData.tobaccoClass.replace(/ /g, "-").replace(/\//g, " ");
        this.atcomparisonservice.getDetailRawExport(this.shareData.fiscalYear, this.shareData.quarter.toString(), tobaccoClassName, this.shareData.ein, "MANU", false).subscribe(
            data => {
                this._downloadService.download(data.csv, data.fileName, 'csv/text');
            }
        );
    }

    getDelta(summarydata) {


        if (summarydata && summarydata.length > 1) {
            for (let i in summarydata) {
                let numCols = summarydata[i]["numColumns"];
                let cellVals = summarydata[i]["cellValues"];


                if (cellVals[0] != null) {
                    if (cellVals[0].toUpperCase().includes("TOTAL")) {
                        let summarydelta = (cellVals[numCols - 1]);

                        let newdelta = Number(summarydelta).toLocaleString(undefined, { minimumFractionDigits: 2 });
                        let newdeltatest = newdelta.toString().includes("-") ? '(' + '$' + newdelta.toString().substring(1, newdelta.toString().length) + ')' : '$' + newdelta.toString();
                        this.toggleDelta(newdeltatest, summarydelta, false);

                        //Set One sided Deltas
                        if(summarydelta != 'NA'){
                            if (numCols == 6) {
                                if ((cellVals[3] == 'NA' && cellVals[4] != 'NA') ||
                                    (cellVals[3] != 'NA' && cellVals[4] == 'NA') || newdeltatest.trim() == "") {
                                    this.toggleDelta(newdeltatest, summarydelta, true);
                                }
                            } else if (numCols == 4) {
                                if ((cellVals[1] == 'NA' && cellVals[2] != 'NA') ||
                                    (cellVals[1] != 'NA' && cellVals[2] == 'NA') || newdeltatest.trim() == "") {
                                    this.toggleDelta(newdeltatest, summarydelta, true);
                                }
                            }
                        } else {
                            this.toggleDelta('NA', 'NA', true);
                        }
                        


                    }


                } else if (cellVals[0] == "NA" || cellVals[0] == null) {
                    this.toggleDelta('NA', 'NA', false);
                }
            }

        } else {
            this.toggleDelta('NA', 'NA', false);
        }

    }

}


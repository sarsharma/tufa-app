import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtCompareManuDetailsAmendComponent } from './at-compare-manu-details-amend.component';

describe('AtCompareManuDetailsAmendComponent', () => {
  let component: AtCompareManuDetailsAmendComponent;
  let fixture: ComponentFixture<AtCompareManuDetailsAmendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtCompareManuDetailsAmendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtCompareManuDetailsAmendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

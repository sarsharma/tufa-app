import { Component } from '@angular/core';
import { Router } from '../../../../../../../../../node_modules/@angular/router';
import { AdminService } from '../../../../../../../admin/services/admin.service';
import { ShareDataService } from '../../../../../../../core/sharedata.service';
import { CustomNumberPipe } from '../../../../../../../shared/util/custom.number.pipe';
import { AnnualTrueUpComparisonEventService } from '../../../../../services/at-compare.event.service';
import { AnnualTrueUpComparisonService } from '../../../../../services/at-compare.service';
import { AtReallocationService } from '../../../../../services/at-reallocation.service';
import { AnnualTrueUpCompareDetailsAmendComponent } from '../../at-comparedetailsamend.component';

@Component({
  selector: 'manu-details-amend-mini',
  templateUrl: './at-compare-manu-details-amend.component.html',
  styleUrls: ['./at-compare-manu-details-amend.component.scss']
})
export class AtCompareManuDetailsAmendComponent extends AnnualTrueUpCompareDetailsAmendComponent {

  constructor(annualTrueUpComparisonService: AnnualTrueUpComparisonService,
    annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
    reallocationEventService: AtReallocationService,
    router: Router, numberPipe: CustomNumberPipe, _adminService: AdminService, _shareData: ShareDataService) {
    super(annualTrueUpComparisonService, annualTrueUpComparisonEventService, reallocationEventService, router, numberPipe, _adminService, _shareData);
    this.subscriptions.push(annualTrueUpComparisonEventService.atCompareSuccessfullSaveAction$.subscribe(
      data => {
        if (data) {
          let amend = data.find((amend) => {
            return amend.qtr == this.quarter;
          });
          if (amend && amend.acceptanceFlag !== 'N') {
            this.amendTotal = false;
            this.savedAmendment = false;
            this.amendTotalUpdated.emit(false);
          }
        }
      }
    ));
  }


  public save() {
    this.activeqtr = this.quarter;
    this.saveTotalAmendment();
  }

  ctrackBy(index: number, obj: any) {
    return index;
  }
}

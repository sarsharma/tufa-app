import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtCompareManuDetailsQuarterPanelComponent } from './at-compare-manu-details-quarter-panel.component';

describe('AtCompareManuDetailsQuarterPanelComponent', () => {
  let component: AtCompareManuDetailsQuarterPanelComponent;
  let fixture: ComponentFixture<AtCompareManuDetailsQuarterPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtCompareManuDetailsQuarterPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtCompareManuDetailsQuarterPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

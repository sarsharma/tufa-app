import { Component, Input, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Subscription } from '../../../../../../../../node_modules/rxjs';
import { ShareDataService } from '../../../../../../core/sharedata.service';
import { AcceptanceFlagState } from '../../../../models/at-acceptance-flag-state.model';
import { ManufactureQuarterDetails } from '../../../../models/at-compare-manufacturerdetails.model';
import { AnnualTrueUpComparisonDetailsHeader } from '../../../../models/at-detailsheader.model';
import { OneSidedDeltaDetaisl } from "../../../../models/at-one-sided-deltaDetails";
import { AnnualTrueUpComparisonEventService } from '../../../../services/at-compare.event.service';
import { AnnualTrueUpComparisonService } from '../../../../services/at-compare.service';
import { DeltaPipe } from '../../../../utils/at-delta.pipe';
declare let jQuery: any;

@Component({
  selector: 'app-at-compare-manu-details-quarter-panel',
  templateUrl: './at-compare-manu-details-quarter-panel.template.html',
  styleUrls: ['./at-compare-manu-details-quarter-panel.style.scss']
})
export class AtCompareManuDetailsQuarterPanelComponent implements OnInit, OnDestroy {

  @Input() headerData: AnnualTrueUpComparisonDetailsHeader;
  @Input() acceptanceFlag: string;
  @Input() quarter: number;
  @Input() oneSidedAcceptanceFlag: string;

  subscriptions: Subscription[] = [];
  busyDetails: Subscription;

  comparedetails: ManufactureQuarterDetails;
  quarterDeltaForOnesidedMap: Map<number, OneSidedDeltaDetaisl> = new Map();
  delta: OneSidedDeltaDetaisl;
  intital: boolean = true;

  constructor(
    private shareData: ShareDataService,
    private atcomparisonservice: AnnualTrueUpComparisonService,
    private deltaPipe: DeltaPipe,
    private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
  ) {
    this.subscriptions.push(this.annualTrueUpComparisonEventService.tobaccoClassSelected$.subscribe(
      data => {
        this.shareData.expandedSectionsMapManu.clear();
        this.shareData.userExpanded = false;
        this.intital = true;
        let panel = jQuery("#collapseDetailsPanel-" + this.quarter);
        panel.collapse('hide');
        this.headerData.quarter = 1;
        this.getFDA3852NonCigarDetails();
      }
    ));

    this.subscriptions.push(this.annualTrueUpComparisonEventService.atCompareMixedActionRefresh$.subscribe(
      data => {
          this.getFDA3852NonCigarDetails();
      }
  ));

  }

  ngOnInit() {
    this.getFDA3852NonCigarDetails();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      if (sub)
        sub.unsubscribe();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "oneSidedAcceptanceFlag") {
        if (this.delta != undefined && this.delta.dataType != undefined) {
          this.getOneSidedAcceptanceFlag(this.delta.dataType);
          this.quarterDeltaForOnesidedMap.get(this.quarter).allDeltaStatus = this.oneSidedAcceptanceFlag;
        }
      }
    }
  }

  ngAfterViewInit() {
    let panel = jQuery("#collapseDetailsPanel-" + this.quarter);
    let _this = this;
    jQuery(`a[href="#collapseDetailsPanel-${this.quarter}"]`).on('click', function (event) {
      _this.intital = false;
      if ((_this.comparedetails.delta == null || _this.comparedetails.delta == 'NA')) {
        event.stopPropagation();
      } else {
        jQuery('.content-wrap').animate({
          scrollTop: jQuery(`#accordionDetailsPanel-${_this.quarter}`).offset().top + jQuery('.content-wrap').scrollTop()
        });
      }
        event.preventDefault();
    });
    panel.on('shown.bs.collapse', function (event) {
      if (!_this.shareData.userExpanded && _this.quarter == _this.shareData.quarter) {
          jQuery('.content-wrap').animate({
              scrollTop: jQuery(`#accordionDetailsPanel-${_this.quarter}`).offset().top + jQuery('.content-wrap').scrollTop()
          });
      }
      _this.setQuarterExpanded(_this.quarter, _this.intital, true);
    });
    panel.on('hidden.bs.collapse', function () {
      _this.setQuarterExpanded(_this.quarter, _this.intital, false);
    });
    // if (this.shareData.expandedSectionsMapManu.get(this.quarter)) {
    //   panel.collapse('show');
    // }
  }

  private triggerCollapseAfterDataLoad(inital: boolean) {
    this.intital = inital;
    let panel = jQuery("#collapseDetailsPanel-" + this.quarter);
    if (this.shareData.expandedSectionsMapManu.get(this.quarter) && !(this.comparedetails.delta == null || this.comparedetails.delta == 'NA')) {
      this.intital = false;
      panel.collapse('show');
    } else if (this.intital && this.headerData.quarter == this.quarter && !(this.comparedetails.delta == null || this.comparedetails.delta == 'NA')) {
      panel.collapse('show');
    } else if (!this.shareData.expandedSectionsMapManu.get(this.quarter) || this.headerData.quarter != this.quarter || (this.comparedetails.delta == null || this.comparedetails.delta == 'NA')) {
      panel.collapse('hide');
    } 
  }

  public setQuarterExpanded(quarter: number, inital?: boolean, expanded?) {

    if (inital && this.shareData.userExpanded) {
      return;
    } else if (!inital) {
      this.shareData.userExpanded = true;
    }

    if (!this.shareData.expandedSectionsMapManu) {
      this.shareData.expandedSectionsMapManu = new Map();
    }

    let side = this.shareData.expandedSectionsMapManu.get(quarter);
    if (side && inital) {
      this.shareData.expandedSectionsMapManu.set(quarter, side);
      this.annualTrueUpComparisonEventService.triggerQuarterExpanded(this.quarter);
    } else if (side && !expanded) {
      this.shareData.expandedSectionsMapManu.delete(quarter);
    } else if (!side && expanded) {
      this.shareData.expandedSectionsMapManu.set(quarter, new Map());
      this.annualTrueUpComparisonEventService.triggerQuarterExpanded(this.quarter);
    }
  }

  getFDA3852NonCigarDetails() {
    if (this.busyDetails) {
      this.busyDetails.unsubscribe();
    }

    this.comparedetails = new ManufactureQuarterDetails();

    let tobaccoClass = this.headerData.tobaccoClass.replace(/\//g, '-');

    // Get Data For Quarter 1
    this.busyDetails = this.atcomparisonservice.getManufacturerComparisonDetails(
      this.shareData.companyId, this.shareData.fiscalYear, this.quarter, tobaccoClass).subscribe(data => {
        if (data) {
          // this.setQuarterExpanded(this.quarter, true, this.headerData.quarter == this.quarter);
          this.comparedetails = data;
          this.triggerCollapseAfterDataLoad(!this.shareData.userExpanded);
          this.delta = new OneSidedDeltaDetaisl();
          this.delta.dataType = this.comparedetails.dataType;
          this.delta.totalDelta = this.comparedetails.delta;
          this.quarterDeltaForOnesidedMap.set(this.quarter, this.delta);
          this.getOneSidedAcceptanceFlag(this.delta.dataType);
          this.delta.allDeltaStatus = this.oneSidedAcceptanceFlag;
          this.quarterDeltaForOnesidedMap.set(this.quarter, this.delta);
        }
      },
        error => {
        });
  }

  public getOneSidedAcceptanceFlag(deltaDataType: string) {
    if (deltaDataType === 'TUFA' && this.oneSidedAcceptanceFlag && this.oneSidedAcceptanceFlag.includes('MSReady'))
      this.oneSidedAcceptanceFlag = AcceptanceFlagState.MSREADYTUFA
    else if (deltaDataType === 'INGESTED' && this.oneSidedAcceptanceFlag && this.oneSidedAcceptanceFlag.includes('MSReady'))
      this.oneSidedAcceptanceFlag = AcceptanceFlagState.MSREADYINGESTED
  }

  public isOneSided() {
    return this.deltaPipe.isOneSided(this.comparedetails.tufaTotal, this.comparedetails.ingestedTotal);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtManuCigarCompareComponent } from './at-manu-cigar-compare.component';

describe('AtManuCigarCompareComponent', () => {
  let component: AtManuCigarCompareComponent;
  let fixture: ComponentFixture<AtManuCigarCompareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtManuCigarCompareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtManuCigarCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

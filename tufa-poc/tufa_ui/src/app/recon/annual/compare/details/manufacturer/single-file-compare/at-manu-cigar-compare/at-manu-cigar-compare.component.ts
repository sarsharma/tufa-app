import { Component, Input, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { forkJoin, Subscription } from '../../../../../../../../../node_modules/rxjs';
import { ShareDataService } from '../../../../../../../core/sharedata.service';
import { PermitPeriodService } from '../../../../../../../permit/services/permit-period.service';
import { DownloadSerivce } from '../../../../../../../shared/service/download.service';
import { AcceptanceFlagState } from '../../../../../models/at-acceptance-flag-state.model';
import { CompareAcceptFDASourceModel } from '../../../../../models/at-compare-accept-fda-source.moduel';
import { ManufactureQuarterDetails } from '../../../../../models/at-compare-manufacturerdetails.model';
import { AnnualTrueUpComparisonDetailsHeader } from '../../../../../models/at-detailsheader.model';
import { MixedActionManu } from '../../../../../models/at-mixedaction.model';
import { MixedActionQuarterDetailsManufacturer } from '../../../../../models/at-mixedactiondetailsquarter.model';
import { OneSidedDeltaDetaisl } from '../../../../../models/at-one-sided-deltaDetails';
import { AnnualTrueUpComparisonEventService } from '../../../../../services/at-compare.event.service';
import { AnnualTrueUpComparisonService } from '../../../../../services/at-compare.service';
import { DeltaPipe } from '../../../../../utils/at-delta.pipe';
import { AtManuCompare } from '../at-manu-compare';

declare var jQuery: any;

@Component({
  selector: 'app-at-manu-cigar-compare',
  templateUrl: './at-manu-cigar-compare.component.html',
  styleUrls: ['./at-manu-cigar-compare.component.scss'],
  providers: [PermitPeriodService],
})
export class AtManuCigarCompareComponent extends AtManuCompare implements OnInit {
  @Input() headerData: AnnualTrueUpComparisonDetailsHeader;
  @Input() acceptanceFlag: string;
  @Input() oneSidedAcceptanceFlag: string;

  @ViewChildren('months') months: QueryList<any>;

  subscriptions: Subscription[] = [];
  busyDetails: Subscription;
  exportBusy: Subscription;
  comparedetails: ManufactureQuarterDetails[] = [new ManufactureQuarterDetails(), new ManufactureQuarterDetails(), new ManufactureQuarterDetails(), new ManufactureQuarterDetails()];
  quarter = 5;
  amendmentFlag: boolean = false;
  isMatched: boolean = true;
  dataType: string = 'NA';
  totalDelta: number = 0;
  tufaTotal: number = 0;
  ingestedTotal: number = 0;
  amendTobaccoClasses: { name: string }[] = [{ name: 'Cigars' }];
  collapseAll: boolean;
  quarterDeltaForOnesidedMap: Map<number, OneSidedDeltaDetaisl> = new Map();
  delta: OneSidedDeltaDetaisl;

  noOfMonths: number[] = [0, 1, 2];
  month = { "JAN" : "January", "FEB" : "February", "MAR" : "March", "APR" : "April", "MAY" : "May", "JUN" : "June", "JUL" : "July", "AUG" : "August", "SEP" : "September", "OCT" : "October", "NOV" : "November", "DEC" : "December" }
  mixedActionQuarterDetailsManufacturer: MixedActionQuarterDetailsManufacturer[] = [];
  mixedActionIndexMap: any = [];
  mixedActionTaxManu: number;
  mixedActionQuantityRemovedManu: number;

  constructor(
    private shareData: ShareDataService,
    private annualTrueUpComparisonService: AnnualTrueUpComparisonService,
    private deltaPipe: DeltaPipe,
    private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
    private downloadService: DownloadSerivce,
    private router: Router,
    private permitPeriodService: PermitPeriodService,
  ) {
    super();
    this.subscriptions.push(this.annualTrueUpComparisonEventService.atCompareMixedActionCigarRefresh$.subscribe(
      data => {
        if(shareData.permitType === 'Manufacturer')
          this.getFDA3852CigarDetails();
      }
  ));
  }

  ngOnInit(): void {
    this.getFDA3852CigarDetails();
    this.delta = new OneSidedDeltaDetaisl();
    this.delta.dataType = this.dataType;
    this.delta.totalDelta = this.totalDelta.toString();
    this.quarterDeltaForOnesidedMap.set(this.quarter, this.delta);
    this.getOneSidedAcceptanceFlag(this.delta.dataType);
    this.quarterDeltaForOnesidedMap.get(this.quarter).allDeltaStatus = this.oneSidedAcceptanceFlag;

    // [1, 2, 3, 4].forEach(quarter => {
    //   if (!this.shareData.userExpanded) {
    //     this.setQuarterExpanded(quarter, true, true);
    //   }
    // });

  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "oneSidedAcceptanceFlag") {
        if (this.delta != undefined && this.delta.dataType != undefined) {
          this.getOneSidedAcceptanceFlag(this.delta.dataType);
          this.quarterDeltaForOnesidedMap.get(this.quarter).allDeltaStatus = this.oneSidedAcceptanceFlag;
          if (this.delta.dataType.toUpperCase() === 'INGESTED' || this.delta.dataType.toUpperCase() === 'TUFA')
            this.isMatched = false;
        }
      }      
      else if (propName === "acceptanceFlag") {
        if(this.acceptanceFlag!=AcceptanceFlagState.MIXEDACTION)
          this.getFDA3852CigarDetails();
      }
    }
  }

  ngAfterViewInit() {

    [1, 2, 3, 4].forEach(quarter => {
      let panel = jQuery("#collapse-" + quarter);
      let _this = this;

      // On click of panel determine weather to open panle or prevent it
      jQuery(`a[href="#collapse-${quarter}"]`).on('click', function (event) {
        if ((_this.comparedetails[quarter - 1].delta == null || _this.comparedetails[quarter - 1].delta == 'NA')) {
          // If no delta prevent expansion
          event.stopPropagation();
        } else {
          // Otherwise scroll to quarter and let the show/hide event happen
          jQuery('.content-wrap').animate({
            scrollTop: jQuery(`#qtr${quarter}-panel`).offset().top + jQuery('.content-wrap').scrollTop()
          });
        }
        event.preventDefault();
      });

      // On expansion
      panel.on('shown.bs.collapse', function (event) {
        // Scroll to quartr if it's been expanded
        if(quarter == 1) {
          jQuery('.content-wrap').animate({
            scrollTop: jQuery(`#qtr${quarter}-panel`).offset().top + jQuery('.content-wrap').scrollTop()
          });
        }
        // Save expansion setting
        _this.setQuarterExpanded(quarter, false, true);
        _this.comparedetails[quarter - 1].permits.forEach(permit => {
          if (permit.delta != null && permit.delta != 'NA') {
            _this.setPermitExpanded(quarter, permit.permit, false, true);
          }
        });
      });
      panel.on('hidden.bs.collapse', function (event) {
        // Save colapse setting
        _this.setQuarterExpanded(quarter, false, false);
      });
    });

    //Prevent the quarter colapse when openting/closing months
    this.months.changes.subscribe(t => {
      let subPanels = jQuery(`[id*='forms-']`);
      subPanels.on('shown.bs.collapse', function (event) {
        event.stopPropagation();
      });
      subPanels.on('hidden.bs.collapse', function (event) {
        event.stopPropagation();
      });
    });
  }

  private triggerCollapseAfterDataLoad(inital: boolean) {
    // this.intital = inital;
    jQuery('.content-wrap').animate({
      scrollTop: jQuery(`#qtr1-panel`).offset().top + jQuery('.content-wrap').scrollTop()
    });
    this.comparedetails.forEach((quarter, index) => {
      let panel = jQuery("#collapse-" + (index + 1));
      if (this.shareData.expandedSectionsMapManu.get(index) && !(this.comparedetails[index].delta == null || this.comparedetails[index].delta == 'NA')) {
        // this.intital = false;
        panel.collapse('show');
      } else if (!(this.comparedetails[index].delta == null || this.comparedetails[index].delta == 'NA')) {
        panel.collapse('show');
      } else if (!this.shareData.expandedSectionsMapManu.get(index) || (this.comparedetails[index].delta == null || this.comparedetails[index].delta == 'NA')) {
        panel.collapse('hide');
      }
    });
  }

  public setQuarterExpanded(quarter: number, inital?: boolean, expanded?) {

    if (inital && this.shareData.userExpanded) {
      return;
    } else if (!inital) {
      this.shareData.userExpanded = true;
    }

    if (!this.shareData.expandedSectionsMapManu) {
      this.shareData.expandedSectionsMapManu = new Map();
    }

    let side = this.shareData.expandedSectionsMapManu.get(quarter);
    if (side && inital) {
      this.shareData.expandedSectionsMapManu.set(quarter, side);
    } else if (side && !expanded) {
      this.shareData.expandedSectionsMapManu.delete(quarter);
    } else if (!side && expanded) {
      this.shareData.expandedSectionsMapManu.set(quarter, new Map());
    }
  }

  public setPermitExpanded(quarter: number, permit: string, inital: boolean, event: boolean) {

    if (inital && this.shareData.userExpanded) {
      return;
    } else if (!inital) {
      this.shareData.userExpanded = true;
    }

    if (this.shareData.expandedSectionsMapManu && event != undefined)
      this.shareData.expandedSectionsMapManu.get(quarter).set(permit, event);
  }

  public collapseExpand() {
    this.comparedetails.forEach((quarter, index) => {
      this.setQuarterExpanded(index + 1, false, !this.collapseAll);
      if (!this.collapseAll)
        quarter.permits.forEach(permit => {
          this.setPermitExpanded(index + 1, permit.permit, false, !this.collapseAll);
        })
    });

    this.collapseAll = !this.collapseAll;
  }

  public clicked(event) {
    let rowId = event.currentTarget.id;
    let isExpanded = jQuery('#' + rowId).attr("aria-expanded");
    if (isExpanded === "true") {
      jQuery('#' + rowId).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
    } else {
      jQuery('#' + rowId).find('i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
    }
    event.preventDefault();
  }

  getFDA3852CigarDetails() {
    this.busyDetails = this.getForkJoin().subscribe(responses => {
      this.comparedetails = responses;
      this.caculateTotalTufaandIngestedTax();
      if (this.isOneSided()) {
        this.caculateTotalTax();
        let tufaOnly: boolean = false;
        let ingestOnly: boolean = false;
        this.comparedetails.forEach(deltaInfo => {
          if (deltaInfo.dataType !== null) {
            if (deltaInfo.dataType.toUpperCase() === 'INGESTED') {
              this.dataType = 'INGESTED';
              ingestOnly = true;
            }
            else if (deltaInfo.dataType.toUpperCase() === 'TUFA') {
              this.dataType = 'TUFA';
              tufaOnly = true;
            }
          }
        });
        if (tufaOnly && ingestOnly) {
          this.dataType = 'Matched';
        }
        else {
          this.delta = new OneSidedDeltaDetaisl();
          this.delta.dataType = this.dataType;
          this.delta.totalDelta = this.totalDelta.toString();
          this.quarterDeltaForOnesidedMap.set(this.quarter, this.delta);
          this.getOneSidedAcceptanceFlag(this.dataType);
        }
      }
      else {
        this.dataType = 'Matched';
      }
      // this.comparedetails.forEach((quarter, index) => {
      //   this.setQuarterExpanded(index + 1, true, true);
      //   quarter.permits.forEach(permit => {
      //     this.setPermitExpanded(index + 1, permit.permit, true, (!['0.00', 'NA'].includes(permit.delta)));
      //   })
      // });
      if(this.dataType === 'Matched')
      this.updateMixedActionMap();
      this.triggerCollapseAfterDataLoad(true);    
    })
  }

   //For Mixed Action updating the map 
   public updateMixedActionMap() {
    let mixedActionDetailsAll: MixedActionQuarterDetailsManufacturer[] = [];
    this.comparedetails.forEach((quarterdetails, index) =>{
      for (var i = 0; i < this.noOfMonths.length; i++) {
        let mixedActionDetailsMonth = new MixedActionQuarterDetailsManufacturer();
        if(null != quarterdetails.ingestedDetails[i]) {
          mixedActionDetailsMonth.month = this.month[quarterdetails.ingestedDetails[i].month];            
        let currentMonth = this.month[quarterdetails.ingestedDetails[i].month].toUpperCase();

        if(this.acceptanceFlag===AcceptanceFlagState.MIXEDACTION && quarterdetails.mixedActionDetailsMap !== null){
          mixedActionDetailsMonth.monthlyStatusFlag = quarterdetails.mixedActionDetailsMap[currentMonth];
          if(mixedActionDetailsMonth.monthlyStatusFlag==='F'){
            mixedActionDetailsMonth.acceptFDAFlag = true;
            mixedActionDetailsMonth.acceptIngestedFlag = false;
          }             
          else if(mixedActionDetailsMonth.monthlyStatusFlag==='I'){
            mixedActionDetailsMonth.acceptIngestedFlag = true;
            mixedActionDetailsMonth.acceptFDAFlag = false;
          }
        }
        else if(this.acceptanceFlag!==AcceptanceFlagState.MIXEDACTION){
          mixedActionDetailsMonth.monthlyStatusFlag = null;
          mixedActionDetailsMonth.acceptFDAFlag = null;
          mixedActionDetailsMonth.acceptIngestedFlag = null;
        }
        mixedActionDetailsMonth.ingestedTax1 = quarterdetails.ingestedDetails[i].taxAmountTobaccoClass1Ingested;
        mixedActionDetailsMonth.ingestedVolume1 = quarterdetails.ingestedDetails[i].removalQtyTobaccoClass1Ingested;
        mixedActionDetailsMonth.ingestedTotalTax = mixedActionDetailsMonth.ingestedTax1;
        mixedActionDetailsMonth.ingestedTotalVolume = mixedActionDetailsMonth.ingestedTotalTax;
        mixedActionDetailsMonth.tufaVolume1 = quarterdetails.tufaDetails[i].removalQtyTobaccoClass1;
        mixedActionDetailsMonth.tufaTax1 = quarterdetails.tufaDetails[i].taxAmountTobaccoClass1;
        mixedActionDetailsMonth.tufaTotalTax = mixedActionDetailsMonth.tufaTax1;
        mixedActionDetailsMonth.tufaTotalVolume = mixedActionDetailsMonth.tufaVolume1;
        mixedActionDetailsAll.push(mixedActionDetailsMonth);
        }            
        
      }
      this.mixedActionQuarterDetailsManufacturer = JSON.parse(JSON.stringify(mixedActionDetailsAll));
      if(this.acceptanceFlag === AcceptanceFlagState.MIXEDACTION){
        this.mixedActionTaxManu = quarterdetails.mixedActionTax;
        this.mixedActionQuantityRemovedManu = quarterdetails.mixedActionQuantityRemoved;
      }
    });
    this.mixedActionIndexMap = [];
    this.mixedActionQuarterDetailsManufacturer.forEach((d)=>{
      this.mixedActionIndexMap.push(d.month)
    })
  }  

  amendmentFlagChange(event) {
    console.log(event)
  }

  public getOneSidedAcceptanceFlag(deltaDataType: string) {
    if (this.dataType.toUpperCase() === 'TUFA' && this.oneSidedAcceptanceFlag === 'MSReady')
      this.oneSidedAcceptanceFlag = AcceptanceFlagState.MSREADYTUFA
    else if (this.dataType.toUpperCase() === 'INGESTED' && this.oneSidedAcceptanceFlag === 'MSReady')
      this.oneSidedAcceptanceFlag = AcceptanceFlagState.MSREADYINGESTED
  }

  private getForkJoin() {
    let q1 = this.annualTrueUpComparisonService.getManufacturerComparisonDetails(this.shareData.companyId, this.shareData.fiscalYear, 1, this.headerData.tobaccoClass);
    let q2 = this.annualTrueUpComparisonService.getManufacturerComparisonDetails(this.shareData.companyId, this.shareData.fiscalYear, 2, this.headerData.tobaccoClass);
    let q3 = this.annualTrueUpComparisonService.getManufacturerComparisonDetails(this.shareData.companyId, this.shareData.fiscalYear, 3, this.headerData.tobaccoClass);
    let q4 = this.annualTrueUpComparisonService.getManufacturerComparisonDetails(this.shareData.companyId, this.shareData.fiscalYear, 4, this.headerData.tobaccoClass);
    return forkJoin([q1, q2, q3, q4]);
  }

  acceptedFDAflag($event) {

    this.annualTrueUpComparisonEventService.triggerAtCompareActiveQuarterUpdateSource(this.quarter);

    let acceptAndContinueFlag: Boolean = false;
    let flag: string = $event.flag;
    let reviewAmmendedTax = null;
    let reviewAmmendedvol = null;
    let acceptedFDAFlag: boolean;
    let inprogressFlag: boolean;

    if (flag === AcceptanceFlagState.ACCEPT_FDA) {
      acceptedFDAFlag = true;
      inprogressFlag = false;
    } else if (flag === AcceptanceFlagState.ACCEPT_INGESTED) {
      reviewAmmendedTax = $event.cbpAmmendedtotaltax;

      acceptedFDAFlag = false;
      inprogressFlag = false;
    }

    let data = new CompareAcceptFDASourceModel();
    data.flags = [false, false, inprogressFlag, acceptedFDAFlag]
    if (flag === AcceptanceFlagState.ACCEPT_FDA || flag === AcceptanceFlagState.ACCEPT_INGESTED) {
      this.annualTrueUpComparisonEventService.triggerAtCompareAcceptFDASource(data);
    }
  }

  mixedActionFlag($event){
    this.annualTrueUpComparisonEventService.triggerAtCompareActiveQuarterUpdateSource(this.quarter);
    let flag: string = $event.flag;
    let mixedAction: MixedActionManu = $event.mixedActionManu;

    if (flag === AcceptanceFlagState.MIXEDACTION) {
      this.annualTrueUpComparisonEventService.triggerAtCompareAcceptMixedAction([flag,mixedAction])
    }
  }

  progressedflag($event) {
    this.annualTrueUpComparisonEventService.triggerAtCompareActiveQuarterUpdateSource(this.quarter);
    if ($event) {
      let data = new CompareAcceptFDASourceModel();
      data.flags = [false, false, true, false]
      this.annualTrueUpComparisonEventService.triggerAtCompareAcceptFDASource(data);
    }
  }

  public exportRawdata() {
    let tobaccoClassName = this.shareData.tobaccoClass.replace(/ /g, "-").replace(/\//g, " ");
    this.exportBusy = this.annualTrueUpComparisonService.getDetailRawExport(this.shareData.fiscalYear, this.shareData.quarter.toString(), tobaccoClassName, this.shareData.ein, "MANU", false).subscribe(
      data => {
        this.downloadService.download(data.csv, data.fileName, 'csv/text');
      }
    );

    this.subscriptions.push(this.exportBusy);
  }

  public isOneSided() {
    let side: boolean = true;
    for (let i = 0; i < 4; i++) {
      side = (this.deltaPipe.isOneSided(this.comparedetails[i].tufaTotal, this.comparedetails[i].ingestedTotal) || this.deltaPipe.isNA(this.comparedetails[i].delta)) && side;
    }
    return side;
  }

  public caculateTotalTax() {
    this.comparedetails.forEach(deltaInfo => {
      if (!this.deltaPipe.isNA(deltaInfo.tufaTotal)) {
        this.totalDelta = this.totalDelta + Number(deltaInfo.tufaTotal);
      } else if (!this.deltaPipe.isNA(deltaInfo.ingestedTotal)) {
        this.totalDelta = this.totalDelta + Number(deltaInfo.ingestedTotal);
      }

    })
  }

  public caculateTotalTufaandIngestedTax() {

    this.comparedetails.forEach(deltaInfo => {
      if (!this.deltaPipe.isNA(deltaInfo.tufaTotal)) {
        this.tufaTotal = this.tufaTotal + Number(deltaInfo.tufaTotal);
      }
      if (!this.deltaPipe.isNA(deltaInfo.ingestedTotal)) {
        this.ingestedTotal = this.ingestedTotal + Number(deltaInfo.ingestedTotal);
      }

    })
  }


  isDisplayDeltaForPermit(qtr) {
    let isDisplay = true;
    // If here is only one permit only show the Delta once 
    if (typeof this.comparedetails[qtr] !== 'undefined' && this.comparedetails[qtr].permits.length === 1) {
      // If the quarter label in the TTB Details header has the same value as the Delta (there is only one permit) only show the Delta once 
      isDisplay = false;
    }
    return isDisplay;
  }

  enableLinkAuth(flag) {
    this.permitPeriodService.setLinkAuthentication(flag);
  }

  toPeriodofActivity(period, detail) {
    let localSrc = this.shareData.detailsSrc;
    let localflag = this.shareData.acceptanceFlag;
    let localtype = this.shareData.permitType;
    let localClass = this.shareData.tobaccoClass;
    let localCompany = this.shareData.companyName;
    let quarter = this.shareData.quarter;
    let createdDate = this.shareData.createdDate;
    this.shareData.reset();
    this.shareData.companyName = localCompany;
    this.shareData.acceptanceFlag = localflag;
    this.shareData.permitType = localtype;
    this.shareData.tobaccoClass = localClass;
    this.shareData.detailsSrc = localSrc;
    this.shareData.companyName = this.headerData.companyName;
    this.shareData.permitNum = period.permit;
    this.shareData.period = detail.month + ' FY ' + this.headerData.fiscalYear;
    this.shareData.permitId = period.permitId;
    this.shareData.periodId = detail.periodId;
    this.shareData.companyId = this.headerData.companyId;

    this.shareData.periodStatus = this.getReportStatus(detail.status);
    this.shareData.previousRoute = this.router.url;
    this.shareData.isRecon = false;
    this.shareData.breadcrumb = 'at-comparedetails';
    this.shareData.qatabfrom = 'at-compare';
    this.shareData.createdDate = createdDate;
    // this.shareData.submitted = this.submitted;

    this.shareData.quarter = quarter;
    this.shareData.fiscalYear = Number(this.headerData.fiscalYear);
    // this.shareData.months = this.months;
  }
}

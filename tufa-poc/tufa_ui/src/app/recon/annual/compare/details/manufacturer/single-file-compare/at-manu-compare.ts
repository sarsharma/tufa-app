import { ATCompareBase } from "../../at-compare-base";

/** Base file for Single File Manufacturer Compare Details */
export class AtManuCompare extends ATCompareBase {
    
    constructor() {
        super();
    }
}
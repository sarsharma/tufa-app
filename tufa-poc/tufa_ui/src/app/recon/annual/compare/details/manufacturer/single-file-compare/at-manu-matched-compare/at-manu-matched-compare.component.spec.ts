import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AtManuMatchedCompareComponent } from './at-manu-matched-compare.component';


describe('AtManuMathcedCompareComponent', () => {
  let component: AtManuMatchedCompareComponent;
  let fixture: ComponentFixture<AtManuMatchedCompareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtManuMatchedCompareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtManuMatchedCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

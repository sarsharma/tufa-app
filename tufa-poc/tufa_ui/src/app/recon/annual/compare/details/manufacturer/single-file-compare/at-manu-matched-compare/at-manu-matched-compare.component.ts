import { ChangeDetectorRef, Component, Input, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { Router } from '../../../../../../../../../node_modules/@angular/router';
import { Subscription } from '../../../../../../../../../node_modules/rxjs';
import { ShareDataService } from '../../../../../../../core/sharedata.service';
import { PermitPeriodService } from '../../../../../../../permit/services/permit-period.service';
import { DownloadSerivce } from '../../../../../../../shared/service/download.service';
import { AcceptanceFlagState } from '../../../../../models/at-acceptance-flag-state.model';
import { CompareAcceptFDASourceModel } from '../../../../../models/at-compare-accept-fda-source.moduel';
import { ManufactureQuarterDetails } from '../../../../../models/at-compare-manufacturerdetails.model';
import { AnnualTrueUpComparisonDetailsHeader } from '../../../../../models/at-detailsheader.model';
import { AnnualTrueUpComparisonEventService } from '../../../../../services/at-compare.event.service';
import { AnnualTrueUpComparisonService } from '../../../../../services/at-compare.service';
import { AtManuCompare } from '../at-manu-compare';
import { MixedActionQuarterDetails, MixedActionQuarterDetailsManufacturer } from '../../../../../models/at-mixedactiondetailsquarter.model';
import { MixedActionManu } from '../../../../../models/at-mixedaction.model';

declare var jQuery: any;

@Component({
  selector: 'app-at-manu-matched-compare',
  templateUrl: './at-manu-matched-compare.component.html',
  styleUrls: ['./at-manu-matched-compare.component.scss'],
  providers: [PermitPeriodService]
})
export class AtManuMatchedCompareComponent extends AtManuCompare implements OnInit {

  @Input() quarter;
  @Input() acceptanceFlag: string;
  @Input() headerData: AnnualTrueUpComparisonDetailsHeader;
  @Input() comparedetails: ManufactureQuarterDetails;

  @ViewChildren('months') months: QueryList<any>;

  subscriptions: Subscription[] = [];
  exportBusy: Subscription;

  amendTobaccoClasses: { name: string }[];
  dualClass1: string;
  dualClass2: string;
  dualClassCombined: string;
  amendmentFlag: boolean = false;

  acceptedFlag: boolean = false;

  noOfMonths: number = 3
  month = { "JAN" : "January", "FEB" : "February", "MAR" : "March", "APR" : "April", "MAY" : "May", "JUN" : "June", "JUL" : "July", "AUG" : "August", "SEP" : "September", "OCT" : "October", "NOV" : "November", "DEC" : "December" }
  mixedActionQuarterDetailsManufacturer: MixedActionQuarterDetailsManufacturer[] = [];
  mixedActionTaxManu: number;
  mixedActionQuantityRemovedManu: number;

  constructor(
    private permitPeriodService: PermitPeriodService,
    private annualTrueUpComparisonService: AnnualTrueUpComparisonService,
    private downloadService: DownloadSerivce,
    private shareData: ShareDataService,
    private annualTrueUpComparisonEventService: AnnualTrueUpComparisonEventService,
    private router: Router,
    private change: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit() {
    this.readfromSharedData();
    this.subscriptions.push(this.annualTrueUpComparisonEventService.quarterExpandedAction$.subscribe(
      data => {
        if (data && this.quarter == data && this.comparedetails.permits) {
          this.comparedetails.permits.forEach(permit => {
            this.setPermitExpanded(this.quarter, permit.permit, false, (!['0.00', 'NA'].includes(permit.delta)));
          });
          this.change.detectChanges();
        }
      }
    ));
  }

  ngOnChanges(changes: SimpleChanges) {
    //console.log("ngOnChanges");
    for (let propName in changes) {
      if (propName === "comparedetails" && changes[propName].currentValue && changes[propName].currentValue.permits) {
        changes[propName].currentValue.permits.forEach(permit => {
          this.setPermitExpanded(this.quarter, permit.permit, true, (!['0.00', 'NA'].includes(permit.delta)));
        });
        this.readfromSharedData();
        this.updateMixedActionMap();
      }
      else if (propName === "comparedetails") {
        this.updateMixedActionMap();
      }
      else if (propName === "acceptanceFlag") {
        this.updateMixedActionMap();
      }
      else if (propName === "headerData") {
        this.readfromSharedData();
      }
    }
  }

  ngAfterViewInit() {
    this.months.changes.subscribe(t => {
      let subPanels = jQuery(`[id*='forms-']`);
      subPanels.on('shown.bs.collapse', function (event) {
        event.stopPropagation();
      });
      subPanels.on('hidden.bs.collapse', function (event) {
        event.stopPropagation();
      });
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => {
      if (sub) {
        sub.unsubscribe();
      }
    })
  }

  public setPermitExpanded(quarter: number, permit: string, inital: boolean, event: boolean) {

    if ((inital && this.shareData.userExpanded) || !this.shareData.expandedSectionsMapManu || !this.shareData.expandedSectionsMapManu.get(quarter)) {
      return;
    } else if (!inital) {
      this.shareData.userExpanded = true;
    }

    if (this.shareData.expandedSectionsMapManu && event != undefined)
      this.shareData.expandedSectionsMapManu.get(quarter).set(permit, event);
  }

  readfromSharedData() {
    this.headerData.companyName = this.shareData.companyName;
    this.headerData.fiscalYear = this.shareData.fiscalYear.toString();
    this.headerData.tobaccoClass = this.shareData.tobaccoClass;
    this.headerData.permitType = this.shareData.permitType;
    this.headerData.companyId = this.shareData.companyId;

    // this.disableAmendApprove = this.shareData.disableAmendApprove;

    if ((this.headerData.tobaccoClass === "Chew/Snuff") ||
      (this.headerData.tobaccoClass === "Chew-Snuff")) {
      this.amendTobaccoClasses = [{ "name": "Chew" }, { "name": "Snuff" }];
      this.dualClass1 = "Chew";
      this.dualClass2 = "Snuff";
      this.dualClassCombined = "Chew/Snuff";
    } else if (['Pipe/Roll Your Own', 'Pipe-Roll Your Own', 'Pipe/Roll-Your-Own'].includes(this.headerData.tobaccoClass)) {
      this.amendTobaccoClasses = [{ "name": "Pipe" }, { "name": "Roll Your Own" }];
      this.dualClass1 = "Pipe";
      this.dualClass2 = "Roll Your Own";
      this.dualClassCombined = "Pipe/Roll Your Own";
    } else {
      this.amendTobaccoClasses = [{ "name": this.headerData.tobaccoClass }];
    }
  }

  toPeriodofActivity(period, detail) {
    let localSrc = this.shareData.detailsSrc;
    let localflag = this.shareData.acceptanceFlag;
    let localtype = this.shareData.permitType;
    let localClass = this.shareData.tobaccoClass;
    let localCompany = this.shareData.companyName;
    let quarter = this.shareData.quarter;
    let createdDate = this.shareData.createdDate;
    this.shareData.reset();
    this.shareData.companyName = localCompany;
    this.shareData.acceptanceFlag = localflag;
    this.shareData.permitType = localtype;
    this.shareData.tobaccoClass = localClass;
    this.shareData.detailsSrc = localSrc;
    this.shareData.companyName = this.headerData.companyName;
    this.shareData.permitNum = period.permit;
    this.shareData.period = detail.month + ' FY ' + this.headerData.fiscalYear;
    this.shareData.permitId = period.permitId;
    this.shareData.periodId = detail.periodId;
    this.shareData.companyId = this.headerData.companyId;

    this.shareData.periodStatus = this.getReportStatus(detail.status);
    this.shareData.previousRoute = this.router.url;
    this.shareData.isRecon = false;
    this.shareData.breadcrumb = 'at-comparedetails';
    this.shareData.qatabfrom = 'at-compare';
    this.shareData.createdDate = createdDate;
    // this.shareData.submitted = this.submitted;

    this.shareData.quarter = quarter;
    this.shareData.fiscalYear = Number(this.headerData.fiscalYear);
    // this.shareData.months = this.months;
  }

  public exportRawdata() {
    let tobaccoClassName = this.shareData.tobaccoClass.replace(/ /g, "-").replace(/\//g, " ");
    this.exportBusy = this.annualTrueUpComparisonService.getDetailRawExport(this.shareData.fiscalYear, this.quarter, tobaccoClassName, this.shareData.ein, "MANU", false).subscribe(
      data => {
        this.downloadService.download(data.csv, data.fileName, 'csv/text');
      }
    );

    this.subscriptions.push(this.exportBusy);
  }

  acceptedFDAflag($event) {

    this.annualTrueUpComparisonEventService.triggerAtCompareActiveQuarterUpdateSource(this.quarter);

    let acceptAndContinueFlag: Boolean = false;
    let flag: string = $event.flag;
    let reviewAmmendedTax = null;
    let reviewAmmendedvol = null;
    let acceptedFDAFlag: boolean;
    let inprogressFlag: boolean;

    if (flag === AcceptanceFlagState.ACCEPT_FDA) {
      acceptedFDAFlag = true;
      inprogressFlag = false;
    } else if (flag === AcceptanceFlagState.ACCEPT_INGESTED) {
      reviewAmmendedTax = $event.cbpAmmendedtotaltax;

      acceptedFDAFlag = false;
      inprogressFlag = false;
    }

    let data = new CompareAcceptFDASourceModel();
    data.flags = [false, false, inprogressFlag, acceptedFDAFlag]
    if (flag === AcceptanceFlagState.ACCEPT_FDA || flag === AcceptanceFlagState.ACCEPT_INGESTED) {
      this.annualTrueUpComparisonEventService.triggerAtCompareAcceptFDASource(data);
    }
  }

  mixedActionFlag($event) {
    this.annualTrueUpComparisonEventService.triggerAtCompareActiveQuarterUpdateSource(this.quarter);
    let flag: string = $event.flag;
    let mixedAction: MixedActionManu = $event.mixedActionManu;

    if (flag === AcceptanceFlagState.MIXEDACTION) {
      this.annualTrueUpComparisonEventService.triggerAtCompareAcceptMixedAction([flag,mixedAction])
    }
  }

  progressedflag($event) {
    this.annualTrueUpComparisonEventService.triggerAtCompareActiveQuarterUpdateSource(this.quarter);
    if ($event) {
      let data = new CompareAcceptFDASourceModel();
      data.flags = [false, false, true, false]
      this.annualTrueUpComparisonEventService.triggerAtCompareAcceptFDASource(data);
    }
  }

  isDualTobaccoClass(tclass: string): boolean {
    return ["CHEW/SNUFF", "PIPE/ROLL YOUR OWN"].includes(tclass.toUpperCase());
  }

  clicked(event) {
    let rowId = event.currentTarget.id;
    let isExpanded = jQuery('#' + rowId).attr("aria-expanded");
    if (isExpanded === "true") {
      jQuery('#' + rowId).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
    } else {
      jQuery('#' + rowId).find('i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
    }
    event.preventDefault();
  }

  savedAmendment(event) {
    this.annualTrueUpComparisonEventService.triggerAtCompareActiveQuarterUpdateSource(this.quarter);
  }

  enableLinkAuth(flag) {
    this.permitPeriodService.setLinkAuthentication(flag);
  }

  //For Mixed Action updating the map 
  public updateMixedActionMap() {
    this.comparedetails.ingestedDetails.forEach(monthDetail => {
      let mixedActionDetailsAll: MixedActionQuarterDetailsManufacturer[] = [];
      this.noOfMonths = this.comparedetails.ingestedDetails.length;
      for (var i = 0; i < this.noOfMonths; i++) {
        let mixedActionDetailsMonth = new MixedActionQuarterDetailsManufacturer();
        mixedActionDetailsMonth.month = this.month[this.comparedetails.ingestedDetails[i].month];
        let currentMonth = this.month[this.comparedetails.ingestedDetails[i].month].toUpperCase();
        if(this.acceptanceFlag===AcceptanceFlagState.MIXEDACTION && this.comparedetails.mixedActionDetailsMap !== null){
          mixedActionDetailsMonth.monthlyStatusFlag = this.comparedetails.mixedActionDetailsMap[currentMonth];
          if(mixedActionDetailsMonth.monthlyStatusFlag==='F'){
            mixedActionDetailsMonth.acceptFDAFlag = true;
            mixedActionDetailsMonth.acceptIngestedFlag = false;
          }             
          else if(mixedActionDetailsMonth.monthlyStatusFlag==='I'){
            mixedActionDetailsMonth.acceptIngestedFlag = true;
            mixedActionDetailsMonth.acceptFDAFlag = false;
          }
        }
        else if(this.acceptanceFlag!==AcceptanceFlagState.MIXEDACTION){
          mixedActionDetailsMonth.monthlyStatusFlag = null;
          mixedActionDetailsMonth.acceptFDAFlag = null;
          mixedActionDetailsMonth.acceptIngestedFlag = null;
        }
        mixedActionDetailsMonth.ingestedTax1 = this.comparedetails.ingestedDetails[i].taxAmountTobaccoClass1Ingested;
        mixedActionDetailsMonth.ingestedVolume1 = this.comparedetails.ingestedDetails[i].removalQtyTobaccoClass1Ingested;
        mixedActionDetailsMonth.ingestedTax2 = this.comparedetails.ingestedDetails[i].taxAmountTobaccoClass2Ingested;
        mixedActionDetailsMonth.ingestedVolume2 = this.comparedetails.ingestedDetails[i].removalQtyTobaccoClass2Ingested;
        mixedActionDetailsMonth.tufaVolume1 = this.comparedetails.tufaDetails[i].removalQtyTobaccoClass1;
        mixedActionDetailsMonth.tufaTax1 = this.comparedetails.tufaDetails[i].taxAmountTobaccoClass1;
        mixedActionDetailsMonth.tufaVolume2 = this.comparedetails.tufaDetails[i].removalQtyTobaccoClass2;
        mixedActionDetailsMonth.tufaTax2 = this.comparedetails.tufaDetails[i].taxAmountTobaccoClass2;

        mixedActionDetailsMonth.ingestedTotalTax = this.comparedetails.ingestedDetails[i].periodTotalIngested;
        mixedActionDetailsMonth.ingestedTotalVolume = this.comparedetails.ingestedDetails[i].removalQuantityIngested;
        mixedActionDetailsMonth.tufaTotalTax = this.comparedetails.tufaDetails[i].periodTotal;
        mixedActionDetailsMonth.tufaTotalVolume = this.comparedetails.tufaDetails[i].removalQuantity;
        mixedActionDetailsAll.push(mixedActionDetailsMonth);
        
      }

      this.mixedActionQuarterDetailsManufacturer = JSON.parse(JSON.stringify(mixedActionDetailsAll));
      if(this.acceptanceFlag === AcceptanceFlagState.MIXEDACTION){
        this.mixedActionTaxManu = this.comparedetails.mixedActionTax;
        this.mixedActionQuantityRemovedManu = this.comparedetails.mixedActionQuantityRemoved;
      }
    });
  }  
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtManuOneSidedCompareComponent } from './at-manu-one-sided-compare.component';

describe('AtManuOneSidedCompareComponent', () => {
  let component: AtManuOneSidedCompareComponent;
  let fixture: ComponentFixture<AtManuOneSidedCompareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtManuOneSidedCompareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtManuOneSidedCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

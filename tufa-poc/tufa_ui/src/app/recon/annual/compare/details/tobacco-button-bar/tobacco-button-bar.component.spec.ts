import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TobaccoButtonBarComponent } from './tobacco-button-bar.component';

describe('TobaccoButtonBarComponent', () => {
  let component: TobaccoButtonBarComponent;
  let fixture: ComponentFixture<TobaccoButtonBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TobaccoButtonBarComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TobaccoButtonBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

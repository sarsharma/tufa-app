import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '../../../../../../../node_modules/@angular/router';
import { Subscription } from '../../../../../../../node_modules/rxjs';
import { ShareDataService } from '../../../../../core/sharedata.service';
import { AnnualTrueUpComparisonDetailsHeader } from '../../../models/at-detailsheader.model';
import { AnnualTrueUpComparisonEventService } from '../../../services/at-compare.event.service';
import { AnnualTrueUpComparisonService } from '../../../services/at-compare.service';

@Component({
  selector: 'app-tobacco-button-bar',
  templateUrl: './tobacco-button-bar.component.html',
  styleUrls: ['./tobacco-button-bar.component.scss'],
})
export class TobaccoButtonBarComponent implements OnInit, OnDestroy {

  @Input() headerData: AnnualTrueUpComparisonDetailsHeader;

  private validTobaccoClasses: string[] = [];
  private selectedTobaccoClasses: string[] = [];
  public importerTobaccoClasses: string[] = ['Cigarettes', 'Cigars', 'Snuff', 'Chew', 'Pipe', 'Roll Your Own'];
  public manufacturerTobaccoClasses: string[] = ['Cigarettes', 'Cigars', 'Chew/Snuff', 'Pipe/Roll Your Own'];

  private subscriptions: Subscription[] = [];
  public gettingTobaccoClasses: Subscription;

  constructor(
    private trueupComparisonService: AnnualTrueUpComparisonService,
    private trueupEventService: AnnualTrueUpComparisonEventService,
    private router: Router,
    private shareData: ShareDataService,
  ) {
    const ein = this.shareData.originalEIN ? this.shareData.originalEIN : this.shareData.ein;
    this.gettingTobaccoClasses = this.trueupComparisonService
      .getValidTobaccoClasses(this.shareData.permitType, ein, this.shareData.fiscalYear.toString())
      .subscribe(
        (data) => {
          this.validTobaccoClasses = [];
          if (data) {
            data.forEach((tobaccoClass) => {
              this.validTobaccoClasses.push(tobaccoClass.replace(/-/g, ' '));
            });
          }
        },
    );

    this.subscriptions.push(this.gettingTobaccoClasses);
  }

  ngOnInit() {
    this.selectedTobaccoClasses.push(this.headerData.tobaccoClass);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => {
      if (sub) {
        sub.unsubscribe();
      }
    });
  }

  public isTobaccoClassValid(tobaccoClass: string): boolean {
    return this.validTobaccoClasses.includes(tobaccoClass);
  }

  public isTobaccoClassSelected(tobaccoClass: string): boolean {
    return this.selectedTobaccoClasses.includes(tobaccoClass);
  }

  public isButtonDisabled(tobaccoClass: string): boolean {
    return !this.isTobaccoClassValid(tobaccoClass) || this.isTobaccoClassSelected(tobaccoClass);
  }

  public selectTobaccoClass(tobaccoClass: string): void {
    if (this.isTobaccoClassSelected(tobaccoClass) || !this.isTobaccoClassValid(tobaccoClass)) {
      return;
    } else {
      this.shareData.clearExpansionMaps();
      this.shareData.prevTrueupInfoExpanded = false;
      this.selectedTobaccoClasses = [];
      this.shareData.tobaccoClass = tobaccoClass;
      this.selectedTobaccoClasses.push(tobaccoClass);
      if (tobaccoClass === 'Cigars' && !this.router.isActive('app/assessments/comparecigar', true)) {
        this.shareData.quarter = 5;
        this.router.navigate(['/app/assessments/comparecigar']);
      } else if (tobaccoClass !== 'Cigars' && !this.router.isActive('app/assessments/comparenoncigar', true)) {
        this.shareData.quarter = 1;
        this.router.navigate(['/app/assessments/comparenoncigar']);
      } else {
        this.shareData.quarter = 1;
        this.trueupEventService.triggerTobaccoClassSelected(this.selectedTobaccoClasses);
      }
    }
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CommonModule } from '../../../../../node_modules/@angular/common';
import { HttpClient, HttpHandler } from '../../../../../node_modules/@angular/common/http';
import { CookieModule, CookieService } from '../../../../../node_modules/ngx-cookie';
import { of } from '../../../../../node_modules/rxjs';
import { AppModule } from '../../../app.module';
import { AuthService } from '../../../login/services/auth.service';
import { DownloadSerivce } from '../../../shared/service/download.service';
import { SharedModule } from '../../../shared/shared.module';
import { CBPMetrics } from '../models/at-cbp-metrics.model';
import { AnnualTrueUpDocModel } from '../models/at-doc.model';
import { AtIngestFileType } from '../models/at-ingest-file-type-enum';
import { AcceptCBPDataService } from '../services/at-acceptIngestedCBP.service';
import { AnnualTrueUpEventService } from '../services/at-event.service';
import { AnnualTrueUpService } from '../services/at.service';
import { AtIngestTabPaneComponent } from './../ingest/at-ingest-tab-pane.component';
import { AnnualTrueUpIngestErrorPopup } from './file/at-ingest-error.popup.component';
import { AnnualTrueUpIngestFileComponent } from './file/at-ingestfile.component';
import { AnnualTrueUpProgressBarDynamicComponent } from './file/at-progressbar-modal.component';
import { DeleteIngest } from './file/delete-ingest.component';


describe('AtIngestTabPaneComponentComponent', () => {
  let component: AtIngestTabPaneComponent;
  let fixture: ComponentFixture<AtIngestTabPaneComponent>;
  let eventService: AnnualTrueUpEventService;
  
  let trueUpService: jasmine.SpyObj<AnnualTrueUpService>;
  let cbpDataService: jasmine.SpyObj<AcceptCBPDataService>;
  let downloadService: jasmine.SpyObj<DownloadSerivce>;

  beforeEach(async(() => {
    eventService = new AnnualTrueUpEventService();

    trueUpService = jasmine.createSpyObj('AnnualTrueUpService',[
      'getTrueupDocs',
      'getMetricData',
      'generateMetrics',
      'downloadUnmatchedRecords'
    ]);

    cbpDataService = jasmine.createSpyObj('AcceptCBPDataService',[
      'acceptIngestedCBP'
    ]);

    downloadService = jasmine.createSpyObj('DownloadSerivce',[
      'download'
    ]);

    trueUpService.getTrueupDocs.and.returnValue(of());

    TestBed.configureTestingModule({
      declarations: [
        AtIngestTabPaneComponent, 
        AnnualTrueUpIngestFileComponent, 
        DeleteIngest, 
        AnnualTrueUpProgressBarDynamicComponent, 
        AnnualTrueUpIngestErrorPopup],
      imports: [
        AppModule, 
        CommonModule, 
        SharedModule,
        CookieModule.forRoot()],
      providers: [
        {provide: AnnualTrueUpEventService, useValue: eventService}, 
        {provide: AnnualTrueUpService, useValue: trueUpService},
        {provide: AcceptCBPDataService, useValue: cbpDataService},
        {provide: DownloadSerivce, useValue: downloadService},
        HttpClient, HttpHandler, AuthService, CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtIngestTabPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('checkUploadedFiles should retrun false if no files are ingested', () => {
    component.data = [];

    expect(component.checkUploadedFiles('CBP')).toBeFalsy();
  });

  it('checkUploadedFiles should retrun false if null is passed', () => {
    component.data = [];

    expect(component.checkUploadedFiles(null)).toBeFalsy();
  });

  it('checkUploadedFiles should retrun false if null is data is null', () => {
    component.data = null;

    expect(component.checkUploadedFiles('CBP')).toBeFalsy();
  });

  it('checkUploadedFiles should retrun true if 2 CBP files are ingested', () => {
    let doc1: AnnualTrueUpDocModel = new AnnualTrueUpDocModel();
    doc1.docDesc = AtIngestFileType.DOC_CBP_DET;
    component.data.push(doc1);
    let doc2: AnnualTrueUpDocModel = new AnnualTrueUpDocModel();
    doc2.docDesc = AtIngestFileType.DOC_CBP_SUM;
    component.data.push(doc2);

    expect(component.checkUploadedFiles('CBP')).toBeTruthy();
    expect(component.checkUploadedFiles('TTB')).toBeFalsy();
  });

  it('checkUploadedFiles should retrun true if 2 TTB files are ingested', () => {
    let doc1: AnnualTrueUpDocModel = new AnnualTrueUpDocModel();
    doc1.docDesc = AtIngestFileType.DOC_TTB_TAX;
    component.data.push(doc1);
    let doc2: AnnualTrueUpDocModel = new AnnualTrueUpDocModel();
    doc2.docDesc = AtIngestFileType.DOC_TTB_VOL;
    component.data.push(doc2);

    expect(component.checkUploadedFiles('TTB')).toBeTruthy();
    expect(component.checkUploadedFiles('CBP')).toBeFalsy();
  });

  it('checkUploadedFiles should retrun false if only 1 of each file is ingested', () => {
    let doc1: AnnualTrueUpDocModel = new AnnualTrueUpDocModel();
    doc1.docDesc = AtIngestFileType.DOC_CBP_DET;
    component.data.push(doc1);
    let doc2: AnnualTrueUpDocModel = new AnnualTrueUpDocModel();
    doc2.docDesc = AtIngestFileType.DOC_TTB_TAX;
    component.data.push(doc2);

    expect(component.checkUploadedFiles('TTB')).toBeFalsy();
    expect(component.checkUploadedFiles('CBP')).toBeFalsy();
  });

  it('Should push the document model to the dataset', () => {
    let doc1: AnnualTrueUpDocModel = new AnnualTrueUpDocModel();
    doc1.docDesc = AtIngestFileType.DOC_CBP_DET;
    eventService.triggerFileIngested(doc1);

    expect(component.data[0]).toEqual(doc1);
  });

  it('Should remove the document model to the dataset', () => {
    let doc1: AnnualTrueUpDocModel = new AnnualTrueUpDocModel();
    doc1.docDesc = AtIngestFileType.DOC_CBP_DET;
    component.data.push(doc1);
    let doc2: AnnualTrueUpDocModel = new AnnualTrueUpDocModel();
    doc2.docDesc = AtIngestFileType.DOC_TTB_TAX;
    component.data.push(doc2);

    eventService.triggerFileDeleted(AtIngestFileType.DOC_CBP_DET);

    expect(component.data.find(file => file == doc1)).toBeFalsy;
    expect(component.data.find(file => file == doc2)).toBeTruthy;
  });

  it('should load metric data', () => {
    let data = new CBPMetrics();
    data.detailFileTotalTaxAmount = 5;
    
    trueUpService.getMetricData.and.returnValue(of(data));

    component.getMetricData();

    expect(component.isMetricsGenrated).toBeTruthy();
    expect(component.metrics).toEqual(data);

  });

  it('should set isMetricDataGenerate to false if no data', () => {
    trueUpService.getMetricData.and.returnValue(of(null));

    component.getMetricData();

    expect(component.isMetricsGenrated).toBeFalsy();
  });

  it('should set  metric data if it was successfuly generated', () => {
    let data = new CBPMetrics();
    data.detailFileTotalTaxAmount = 5;
    
    trueUpService.generateMetrics.and.returnValue(of(data));

    component.generateMetrics();

    expect(component.progressbarMaxOut).toBeTruthy();
    expect(component.isMetricsGenrated).toBeTruthy();
    expect(component.metrics).toEqual(data);
  });

  it('should update metrics if data was accepted', () => {
    let data = new CBPMetrics();
    data.accepted = 'Y';
    
    cbpDataService.acceptIngestedCBP.and.returnValue(of(data));

    component.acceptLegacyCBPData();

    expect(component.progressbarMaxOut).toBeTruthy();
    expect(component.isLegacyDataReady).toBeTruthy();
    expect(component.metrics).toEqual(data);
  });

  it('call the download service', () => {
    trueUpService.downloadUnmatchedRecords.and.returnValue(of(jasmine.any));

    component.exportUnmatchedData("");

    expect(downloadService.download).toHaveBeenCalled();
  });
});


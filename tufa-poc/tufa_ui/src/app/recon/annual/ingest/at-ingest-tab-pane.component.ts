import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subscription } from '../../../../../node_modules/rxjs';
import { ShareDataService } from "../../../core/sharedata.service";
import { IngestionWorkflowContext } from '../../../shared/model/ingestion-workflow.model';
import { DialogService } from '../../../shared/service/dialog.service';
import { IngestionWorkFlowService } from '../../../shared/service/ingestion-workflow.service';
import { CBPMetrics } from '../models/at-cbp-metrics.model';
import { CBPUpload } from '../models/at-cbp-upload.model';
import { AnnualTrueUpDocModel } from '../models/at-doc.model';
import { AcceptCBPDataService } from '../services/at-acceptIngestedCBP.service';
import { AnnualTrueUpEventService } from '../services/at-event.service';
import { AnnualTrueUpService } from '../services/at.service';
import { DownloadSerivce } from './../../../shared/service/download.service';


@Component({
    selector: '[at-ingest-tab-pane]',
    templateUrl: './at-ingest-tab-pane.component.html',
    styleUrls: ['./at-ingest-tab-pane.component.scss']
})
export class AtIngestTabPaneComponent implements OnInit {


    @Input() fiscalYear: any;
    @Output() contextChanged = new EventEmitter<string>();

    doctype: string = '';
    generalDocType: string = '';

    data: AnnualTrueUpDocModel[] = [];
    metrics: CBPMetrics = new CBPMetrics();
    isMetricsGenrated: boolean = false;
    isCBPUploaded: boolean = false;
    showConfirmPopup: boolean;

    subscriptions: Subscription[] = [];
    fileIngestedSubscription: Subscription;
    annualTrueUpServiceEvSubscription: Subscription;
    fileDeletedSubscription: Subscription;
    metricsSubscription: Subscription;
    generateMetricsSubscription: Subscription;
    exportSubscription: Subscription;
    fileSubscription: Subscription;

    stageLegacyData: Subscription;
    isLegacyDataReady: boolean = false;
    legacyData: CBPUpload = new CBPUpload();
    ingestionContextModel: IngestionWorkflowContext = new IngestionWorkflowContext();


    ingestionContextId: number;

    ttbOpen: boolean = false;
    cbpOpen: boolean = false;
    disabled: boolean = false;
    isLoaded: boolean = false;
    progressbarMaxOut: boolean;
    uploadedTTBFile: String;

    heading: string = "";
    shareData: ShareDataService;

    constructor(private annualTrueUpService: AnnualTrueUpService,
        private ingestionWorkflowSvc: IngestionWorkFlowService,
        private annualTrueUpEventService: AnnualTrueUpEventService, private _downloadService: DownloadSerivce,
        private acceptCBPDataService: AcceptCBPDataService, private dialogService: DialogService, private _shareData: ShareDataService) {

        this.subscriptions.push(this.fileIngestedSubscription = annualTrueUpEventService.fileIngestedSourceLoaded$.subscribe(
            fileIngestedModel => {
                if (fileIngestedModel) {
                    if (!(fileIngestedModel.errorCount > 0)) {
                        this.data.push(fileIngestedModel);

                    }
                }
            })
        );

        this.subscriptions.push(this.fileDeletedSubscription = annualTrueUpEventService.fileDeletedSourceLoaded$.subscribe(
            deleted => {
                if (deleted) {
                    this.data = this.data.filter(doc => doc.docDesc !== deleted)
                    this.loadIngestedDocuments();
                }
            }
        ));
        this.shareData = _shareData;
    }

    async ngOnInit() {

        if (!this.fiscalYear)
            this.fiscalYear = this.shareData.fiscalYear;
        // wait for the context to get set 
        await this.ingestionWorkflowSvc.initContextInfo(this.fiscalYear);

        // If no previous context set, set the default context as 'detailOnly'
        if (!this.ingestionWorkflowSvc.getContextInfo().length) {
            let currentContext: IngestionWorkflowContext = new IngestionWorkflowContext();
            currentContext.context = 'detailOnly';
            currentContext.fiscalYr = this.fiscalYear;
            //wait for the context to get set 
            await this.ingestionWorkflowSvc.saveContextInfo(currentContext);
            this.contextChanged.emit('detailOnly');
        }
        this.ingestionContextModel = this.ingestionWorkflowSvc.getContextInfo()[0];

        if (this.ingestionContextModel.context == 'detailOnly') { this.emitContext('detailOnly'); }
        else if (this.ingestionContextModel.context == 'summdetail') { this.emitContext('summdetail'); }

    }

    ngOnDestroy() {

        this.subscriptions.forEach(sub => {
            if (sub) {
                sub.unsubscribe();
            }
        })
    }

    trackChanges(event) {

        this.emitContext(event);
        this.saveContext();
    }

    emitContext(event) {
        if (event == 'summdetail') {
            this.ingestionContextModel.context = 'summdetail';
            this.contextChanged.emit('summdetail');
        }
        else {
            this.ingestionContextModel.context = 'detailOnly';
            this.contextChanged.emit('detailOnly');
        }
    }

    async saveContext() {
        this.ingestionContextModel.fiscalYr = this.fiscalYear;
        await this.ingestionWorkflowSvc.saveContextInfo(this.ingestionContextModel);
    }

    loadIngestedDocuments() {

        if (!this.fiscalYear) {
            this.fiscalYear = this.shareData.fiscalYear;
        }
        if (this.annualTrueUpServiceEvSubscription) {
            this.annualTrueUpServiceEvSubscription.unsubscribe()
            this.isLoaded = false;
        }

        this.annualTrueUpServiceEvSubscription = this.annualTrueUpService.getTrueupDocs(this.fiscalYear).subscribe(
            result => {
                if (result) {
                    this.data = result;
                    if (this.ingestionContextModel.context != 'detailOnly')
                        this.getMetricData();
                }
            }
        );
        this.isLoaded = true;

        this.subscriptions.push(this.annualTrueUpServiceEvSubscription);

    }


    setSelectedDocument(data: any) {

        this.showConfirmPopup = true;
        this.doctype = data.docDesc;
    }

    checkUploadedFiles(fileType: string): boolean {

        let fileCount: number = 0;
        if (this.data && fileType)
            this.data.forEach(file => {
                file.docDesc.indexOf(fileType) > -1 ? fileCount++ : fileCount;
            });

        return fileCount == 2;
    }

    checkCountCBP(fileType: string): boolean {

        for (let i in this.data) {
            if (this.data[i].docDesc.includes(fileType.substring(0, 3))) {
                return true
            }
        }

        return false;

    }

    cbpDetailOnly(fileType: string): boolean {

        for (let i in this.data) {
            if (this.ingestionContextModel.context == 'detailOnly' &&
                this.data[i].docDesc.includes('CBP Details')) {
                return true
            }
        }

        return false;

    }

    public getMetricData() {


        if (this.metricsSubscription) {
            this.metricsSubscription.unsubscribe()
        }

        this.subscriptions.push(this.metricsSubscription = this.annualTrueUpService.getMetricData(this.fiscalYear).subscribe(
            metrics => {
                this.isMetricsGenrated = (metrics !== null);
                this.metrics = metrics;
                this.annualTrueUpEventService.triggerMetricsLoaded(this.metrics);
            }
        ));
    }

    /**
     * Generates metrics for CBP files by fiscal year
     */
    public generateMetrics() {

        if (this.generateMetricsSubscription) {
            this.generateMetricsSubscription.unsubscribe();
        }

        this.heading = "Generating Metrics";
        this.progressbarMaxOut = false;
        this.generateMetricsSubscription = this.annualTrueUpService.generateMetrics(this.fiscalYear).subscribe(
            metrics => {
                this.progressbarMaxOut = true;
                this.isMetricsGenrated = true;
                this.metrics = metrics;
            },
            error => {
                this.progressbarMaxOut = true;
                // try {
                //     let alert: string[] = error.split(':')
                //     this.dialogService.showDialog(AlertModal, true, {title: alert[0] ,msg: alert[1]})
                // } catch (e) {
                throw (error);
                // }
            }
        );

        this.subscriptions.push(this.generateMetricsSubscription);
    }

    //Stage Legacy data
    /*TODO: Spelling */
    public acceptLegacyCBPData() {
        //console.log("acceptLegacyCBPData");

        this.heading = "Accepting Ingested Values";
        this.progressbarMaxOut = false;

        this.stageLegacyData = this.acceptCBPDataService.acceptIngestedCBP(this.fiscalYear).subscribe(
            metrics => {
                this.progressbarMaxOut = true;
                this.isLegacyDataReady = true;
                this.metrics = metrics;

                this.annualTrueUpEventService.ingestedLegacy(true);
                this.annualTrueUpEventService.triggerMetricsLoaded(this.metrics);
            },
            error => {
                this.progressbarMaxOut = true;
                // try {
                //     let alert: string[] = error.split(':')
                //     this.dialogService.showDialog(AlertModal, true, {title: alert[0] ,msg: alert[1]})
                // } catch (e) {
                throw (error);
                // }
            }
        );

        this.subscriptions.push(this.stageLegacyData);
        this.disabled = true;
    }

    /* TODO: Capatlization */
    exportUnmatchedData(recordType: string) {

        if (this.exportSubscription) {
            this.exportSubscription.unsubscribe();
        }

        this.exportSubscription = this.annualTrueUpService.downloadUnmatchedRecords(this.fiscalYear, recordType).subscribe(
            data => {
                this._downloadService.download(data.csv, data.fileName, 'csv/text');
            }
        );

        this.subscriptions.push(this.exportSubscription);
    }


}

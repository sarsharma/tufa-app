import { Component, ElementRef, Input, OnDestroy, OnInit, SimpleChanges, ViewEncapsulation, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AnnualTrueUpProgressBarDynamicComponent } from "../file/at-progressbar-modal.component";
import { AnnualTrueUpDocModel } from "./../../../../recon/annual/models/at-doc.model";
import { AnnualTrueUpEventService } from "./../../../../recon/annual/services/at-event.service";
import { AnnualTrueUpService } from "./../../../../recon/annual/services/at.service";
import { NgForm } from '../../../../../../node_modules/@angular/forms';
import { AtIngestFileType } from '../../models/at-ingest-file-type-enum';
// import { AlertModal } from '../../../../shared/components/alert-modal/alert-modal.component';
import { DialogService } from '../../../../shared/service/dialog.service';

declare var jQuery: any;

@Component({
    selector: '[at-ingestfile]',
    templateUrl: './at-ingestfile.template.html',
    styleUrls: ['at-ingestfile.style.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: []
})

export class AnnualTrueUpIngestFileComponent implements OnInit, OnDestroy {

    filesToUpload: Array<File> = [];
    docFileName: string;
    // fileType: string;
    timer: Observable<number>;
    entries = [];
    selectedEntry: any = {
        value: null,
        description: null
    };


    showErrorFlag: boolean;
    alerts: any[] = [];

    disableCBP: boolean = false;
    disableCBPS: boolean = false;
    disableCBPD: boolean = false;
    disableTTBT: boolean = false;
    disableTTBV: boolean = false;

    @Input() fiscalYear;
    @Input() refreshToken;
    @Input() context;
    @Input() multiple: boolean = true;
    @Input() generalDocType: string = '';


    uploadInProgress: boolean = false;
    progressbarMaxOut: boolean = false;
    showIngestFileErrorPopup: boolean = false;


    trueupDocUpdateSubscription: Subscription;

    private _elementRef: ElementRef;

    @ViewChild('ingestfileform') ingestfileform: NgForm;

    constructor(private annualTrueUpService: AnnualTrueUpService,
        private annualTrueUpEventService: AnnualTrueUpEventService,
        elementRef: ElementRef,
        private dialogService: DialogService
    ) {
        this._elementRef = elementRef;

        this.trueupDocUpdateSubscription = annualTrueUpEventService.trueupDocsUpdateSource$.subscribe(
            trueupDocs => {
                if (trueupDocs) {
                    this.disableCBP = false; this.disableTTBT = false; this.disableTTBV = false;
                    this.updateRadioButtonSelections(trueupDocs);
                }
            }
        );

        // This is the bootstrap on shown event where i call the initialize method
        jQuery(this._elementRef.nativeElement).on("shown.bs.modal", () => {
            this.initializeModal();
        });

        jQuery(this._elementRef.nativeElement).on("hidden.bs.modal", () => {
            this.cleanupModal();
        });
    }

    ngOnChanges(changes: SimpleChanges) {

        for (let propName in changes) {
            if (propName === "generalDocType" || propName === "context") {
                let chg = changes[propName];
                if (chg.currentValue) {
                    if (this.generalDocType === 'CBP' && this.context === 'detailOnly') {
                        this.entries = [
                        ];

                    }
                    else if (this.generalDocType === 'CBP' && this.context === 'summdetail') {

                        this.entries = [
                            {
                                description: 'CBP - Summary',
                                value: "CBP Summary"

                            },
                            {
                                description: 'CBP - Details',
                                value: "CBP Details"
                            }
                        ];

                    }
                    else {
                        this.entries = [
                            {
                                description: 'TTB - Tax',
                                value: "TTB Tax"

                            },
                            {
                                description: 'TTB - Volume',
                                value: "TTB Volume"
                            }
                        ];

                    }
                }
            }


        }
    }


    ngOnInit() {
        this.resetAlerts();
        this.showErrorFlag = false;
    }

    initializeModal() {
    }

    // Free up memory and stop any event listeners/hubs
    cleanupModal() {
        this.resetAlerts();
        this.showErrorFlag = false;
        // jQuery('.alert').alert('close');
        jQuery('.fileinput').fileinput('clear');
        jQuery('#ingest-file').find('input[type=text]').val('');
        jQuery('#ingest-file').find('input[type=radio]').prop('checked', false);
        if (typeof jQuery('#ingestfileform').parsley() != "undefined") {
            jQuery('#ingestfileform').parsley().reset();
        }
        this.selectedEntry = [];
    }


    ngOnDestroy() {
        if (this.trueupDocUpdateSubscription) {
            this.trueupDocUpdateSubscription.unsubscribe();
        }
    }

    updateRadioButtonSelections(trueupDocs: AnnualTrueUpDocModel[]) {
        this.disableCBPD = false;
        this.disableCBPS = false;
        this.disableTTBT = false;
        this.disableTTBV = false;
        trueupDocs.forEach(doc => {
            if (doc.docDesc === AtIngestFileType.DOC_CBP_DET) {
                this.disableCBPD = true;
            } else if (doc.docDesc === AtIngestFileType.DOC_CBP_SUM) {
                this.disableCBPS = true;
            } else if (doc.docDesc === AtIngestFileType.DOC_TTB_TAX) {
                this.disableTTBT = true;
            } else if (doc.docDesc === AtIngestFileType.DOC_TTB_VOL) {
                this.disableTTBV = true;
            }
        });
    }

    reset() {
        if (this.ingestfileform) {
            this.ingestfileform.reset();
        }
        if (typeof jQuery('#ingestfileform').parsley() != "undefined") {
            jQuery('#ingestfileform').parsley().reset();
        }
        jQuery('#ingest-file').modal('hide');
    }

    cancel() {
        this.reset();
    }

    getFilePathExtension(path) {
        let filename = path.split('\\').pop().split('/').pop();
        return filename.substr((Math.max(0, filename.lastIndexOf(".")) || Infinity) + 1);
    }

    fileChangeEvent(event) {
        this.showErrorFlag = false;
        this.resetAlerts();

        this.filesToUpload = <Array<File>>event.target.files;
        if (!this.filesToUpload[0]) return;

        this.docFileName = this.filesToUpload[0].name;
        let ext = this.getFilePathExtension(this.docFileName);
        jQuery('#fileIngest').parsley().removeError('Invalid-Ext');
        switch (ext.toLowerCase()) {
            case 'xls':
            case 'xlsx':
                break;

            default:
                let invalidexterror = "Error: Invalid file format";
                jQuery('#fileIngest').parsley().addError('Invalid-Ext', { message: invalidexterror });
                break;
        }
    }

    alertAdded(name: string): boolean {
        let alertPresent = false;
        this.alerts.forEach(function (item, index, array) {
            if (item.name === name) {
                alertPresent = true;
            }
        });
        return alertPresent;
    }

    resetAlerts() {
        this.alerts = [];
    }

    ingestFile() {
        // console.log(this.fileType);
        jQuery('#ingestfileform').parsley().validate();

        if (!jQuery('#ingestfileform').parsley().isValid()) return;

        // Need to fix this here for Only CBP Details Context 
        if (this.selectedEntry.value == undefined || this.selectedEntry.value == null)
            this.selectedEntry.value = 'CBP Details';

        if ((this.selectedEntry.value === AtIngestFileType.DOC_CBP_SUM && this.disableCBPS)
            || (this.selectedEntry.value === AtIngestFileType.DOC_CBP_DET && this.disableCBPD)
            || (this.selectedEntry.value === AtIngestFileType.DOC_TTB_TAX && this.disableTTBT)
            || (this.selectedEntry.value === AtIngestFileType.DOC_TTB_VOL && this.disableTTBV)) {
            this.resetAlerts();
            this.alerts.push({ type: 'warning', msg: 'Error: File Type has already been ingested. Please select a different File Type' });
            this.showErrorFlag = true;
            return;
        }

        this.uploadInProgress = true;

        if (this.filesToUpload.length > 0) {
            let file: File = this.filesToUpload[0];
            let formData: FormData = new FormData();
            let docModel = new AnnualTrueUpDocModel();
            docModel.docDesc = this.selectedEntry.value;
            docModel.trueupDocument = file;
            docModel.trueUpFilename = file.name;
            formData.append('fiscalYear', this.fiscalYear);
            formData.append('fileType', this.selectedEntry.value);
            formData.append('trueupDocument', file, file.name);
            this.uploadFile(formData);
        }
    }

    onSelectionChange(entry) {
        // clone the object for immutability
        this.selectedEntry = Object.assign({}, this.selectedEntry, entry);
    }

    public uploadFile(formData: FormData) {

        this.showErrorFlag = false;
        this.resetAlerts();

        this.annualTrueUpService.ingestFile(formData, this.fiscalYear).then((result) => {
            if (result) {
                let docModel = new AnnualTrueUpDocModel();
                jQuery.extend(true, docModel, JSON.parse(result));
                this.annualTrueUpEventService.triggerFileIngested(docModel);
                this.progressbarMaxOut = true;
                let thisComp = this;
                setTimeout(function () {
                    thisComp.uploadInProgress = false;
                    thisComp.progressbarMaxOut = false;
                    if (docModel.errorCount > 0) {
                        jQuery('#ingest-error-dialog').modal({ keyboard: false, backdrop: "static" });
                    } else {
                        jQuery('#ingest-file').modal('toggle');
                        if (typeof jQuery('#ingestfileform').parsley() != "undefined") {
                            jQuery('#ingestfileform').parsley().reset();
                        }
                    }
                }, 3000);

            }
        }, (error) => {
            this.uploadInProgress = false;
            this.progressbarMaxOut = false;
            this.showErrorFlag = true;
            this.resetAlerts();
            let erormessage
            if (null != error) {
                var obj = JSON.parse(error);
                erormessage = obj.message != null ? obj.message : 'Unable to Ingest file. Please check file contents';
            }
            this.alerts.push({ type: 'warning', msg: erormessage });
            // try {
            //     let alert: string[] = JSON.parse(error).ex.split(':')
            //     this.dialogService.showDialog(AlertModal, true, {title: alert[0] ,msg: alert[1]})
            // } catch (e) {
            console.error(error);
            // }
        });
    }

    exportedIngestErrorFile(flag) {
        this.showIngestFileErrorPopup = false;
        jQuery('#ingest-error-dialog').modal('hide');
        jQuery('#ingest-file').modal('toggle');
    }


}

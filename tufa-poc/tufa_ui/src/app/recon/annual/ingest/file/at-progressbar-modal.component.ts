import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: '[at-progressbar]',
  templateUrl: './at-progressbar-modal.template.html',
  encapsulation: ViewEncapsulation.None
})

export class AnnualTrueUpProgressBarDynamicComponent implements OnInit, OnDestroy {
  public max: number = 200;
  public showWarning: boolean;
  public dynamic: number = 0;
  public type: string;
  public constIncrement: number = 10;

  @Input() maxout: boolean;

  private dynamicSubscription: Subscription;

  public constructor() {
    this.max = 200;
    this.dynamic = 0;
    this.constIncrement = 10;
  }

  ngOnInit() {
    this.max = 200;
    this.dynamic = 0;
    this.constIncrement = 10;

    //After 0 seconds emit a value every 1 second
    let pg_timer = timer(0,1000);
    this.dynamicSubscription = pg_timer.subscribe(t => this.value(t));
  }

  ngOnDestroy() {
    if (this.dynamicSubscription) {
      this.dynamicSubscription.unsubscribe();
    }
  }

  public value(value: number) {

    //Max out the bar
    if (this.maxout)
      this.dynamic = this.max;

    //Unsubscribe once maxed out
    if (this.dynamic >= this.max) {
      this.dynamicSubscription.unsubscribe();
    }
    //Don't finish the load bar if is one increment away from finishing 
    else if (this.dynamic < this.max - this.constIncrement) {
      this.dynamic = value + this.constIncrement;
    }

  }

}

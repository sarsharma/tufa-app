/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { DialogService } from '../../../../shared/service/dialog.service';
import { AnnualTrueUpEventService } from '../../services/at-event.service';
import { AnnualTrueUpService } from '../../services/at.service';
// import { AlertModal } from '../../../../shared/components/alert-modal/alert-modal.component';
declare var jQuery: any;

/**
 * 
 * 
 * @export
 * @class delete Ingestion
 */
@Component({
  selector: '[delete-ingest]',
  templateUrl: './delete-ingest.template.html',
  providers: [AnnualTrueUpService]
})

export class DeleteIngest implements OnInit {
  @Input() fiscalYear: any;
  @Input() doctype: any;
  @Output() callBack = new EventEmitter<boolean>();
  @Input() msg: string;
  @Input() title: string;
  disableClick: boolean = false;
  busy: Subscription;
  documentDesc: string;

  ngOnInit() {
  }

  constructor(private service: AnnualTrueUpService,
    private annualTrueUpEventService: AnnualTrueUpEventService,
    private dialogService: DialogService) {

  }

  ngOnDestroy() {
    if (this.busy) {
      this.busy.unsubscribe();
    }
  }

  reset() {
    jQuery('#delete-ingest').modal('hide');
  }

  delete() {
    this.disableClick = true;
    this.busy = this.service.removeIngestedFile(this.fiscalYear, this.doctype).subscribe(data => {
      if (data) {
        this.annualTrueUpEventService.triggerFileDeleted(this.doctype);
        this.reset();
        this.disableClick = false;
      }
    }, error => {
      // try {
      //   let alert: string[] = error.split(':')
      //   this.dialogService.showDialog(AlertModal, true, { title: alert[0], msg: alert[1] })
      // } catch (e) {
        this.reset(); this.disableClick = false;
      // }
    }
    );
  }

}


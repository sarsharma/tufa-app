import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewEncapsulation, SimpleChange } from '@angular/core';

import { AnnualTrueUpCBPEntryModel } from "./../../../../../recon/annual/models/at-cbpentry.model";
import { AnnualTrueUpService } from "./../../../../../recon/annual/services/at.service";
import { AnnualTrueUpEventService } from "./../../../../../recon/annual/services/at-event.service";
import { AnnualTrueupCmpyAssocModel } from './../../../../../recon/annual/models/at-cbpimp-cmpyassc-delta.model';
import * as _ from "lodash";
import { Subscription } from '../../../../../../../node_modules/rxjs';

declare var jQuery: any;

@Component({
    selector: '[at-companyassociation]',
    templateUrl: './at-companyassociation.template.html',
    styleUrls: ['../../../compare/details/at-compare-cigardetails.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class AnnualTrueUpCompanyAssociation implements OnDestroy{

    // @Input() cmpyAssoc: AnnualTrueUpCBPEntryModel;
    @Input() cmpyAssoc: AnnualTrueupCmpyAssocModel;
    @Input() index;
    @Output() refreshBucket: EventEmitter<any> = new EventEmitter();
    @Input() busy: Subscription;
    @Output() busyChange = new EventEmitter();
    helperText: string;

    validAnswersCmpyAssoc: string[] = ["IMPT", "CONS", "EXCL"];

    consignTTBitFlag = 0;
    impTTBitFlag = 0;

    consignQtrBitFlag = 0;
    impQtrBitFlag = 0;

    quarters = ['Q1', 'Q2', 'Q3', 'Q4'];

    importerDeltas: any[] = [];
    consigneeDeltas: any[] = [];


    impColCnt: number = 0;
    consignColCnt: number = 0;
    associationTypeCdFlag:string;

    hasUnknownTT: boolean = false;
    update: Subscription;

    constructor(private annualTrueUpService: AnnualTrueUpService,
        private annualTrueUpEventService: AnnualTrueUpEventService) {
        this.helperText = "";
        this.update = this.annualTrueUpEventService.updateCompanyAssoc$.subscribe(
            update => {
                if (update)
                    this.cmpyAssoc.providedAnswer = true;
            }
        );
    }

    ngOnDestroy(): void {
        if (this.update) {
            this.update.unsubscribe();
        }
    }

    public ngOnChanges(changes: { [key: string]: SimpleChange }): any {

        if (changes["cmpyAssoc"]) {
            let cmpyAssoc = changes["cmpyAssoc"].currentValue;

            this.associationTypeCdFlag = cmpyAssoc.associationTypeCd;

            if (cmpyAssoc && cmpyAssoc.consigneeDeltas) {
                for (let i in cmpyAssoc.consigneeDeltas) {
                    let consigDelta = cmpyAssoc.consigneeDeltas[i];
                    this.consigneeDeltas.push(consigDelta);

                    let consgColDef =  ((this.isDef(consigDelta.deltaCigars) || this.isDef(consigDelta.taxesCigarsTufa) ? 1 : 0)
                            | (this.isDef(consigDelta.deltaCigarettes) || this.isDef(consigDelta.taxesCigsTufa) ? 2 : 0)
                            | (this.isDef(consigDelta.deltaChew) || this.isDef(consigDelta.taxesChewTufa) ? 4 : 0)
                            | (this.isDef(consigDelta.deltaSnuff) || this.isDef(consigDelta.taxesSnuffTufa) ? 8 : 0)
                            | (this.isDef(consigDelta.deltaPipe) || this.isDef(consigDelta.taxesPipeTufa) ? 16 : 0)
                            | (this.isDef(consigDelta.deltaRYO) || this.isDef(consigDelta.taxesRYOTufa) ? 32 : 0)
                            | (this.isDef(consigDelta.deltaUnknown) || this.isDef(consigDelta.taxesUnknownTufa) ? 64 : 0));

                    this.consignTTBitFlag = this.consignTTBitFlag | consgColDef;
                    this.consignQtrBitFlag = consgColDef>0?(this.consignQtrBitFlag|Math.pow(2,Number(i)-1)):this.consignQtrBitFlag;
                }


            }

            if (cmpyAssoc && cmpyAssoc.importerDeltas) {
                for (let i in cmpyAssoc.importerDeltas) {
                    let impDelta = cmpyAssoc.importerDeltas[i];
                    this.importerDeltas.push(impDelta);

                    let impColDef = ((this.isDef(impDelta.deltaCigars) || this.isDef(impDelta.taxesCigarsTufa) ? 1 : 0)
                            | (this.isDef(impDelta.deltaCigarettes) || this.isDef(impDelta.taxesCigsTufa) ? 2 : 0)
                            | (this.isDef(impDelta.deltaChew) || this.isDef(impDelta.taxesChewTufa) ? 4 : 0)
                            | (this.isDef(impDelta.deltaSnuff) || this.isDef(impDelta.taxesSnuffTufa) ? 8 : 0)
                            | (this.isDef(impDelta.deltaPipe) || this.isDef(impDelta.taxesPipeTufa) ? 16 : 0)
                            | (this.isDef(impDelta.deltaRYO) || this.isDef(impDelta.taxesRYOTufa) ? 32 : 0)
                            | (this.isDef(impDelta.deltaUnknown) || this.isDef(impDelta.taxesUnknownTufa) ? 64 : 0));

                    this.impTTBitFlag = this.impTTBitFlag|impColDef;
                    this.impQtrBitFlag = impColDef>0?(this.impQtrBitFlag | Math.pow(2,Number(i)-1)) : this.impQtrBitFlag;
                }

            }

            if (this.consignTTBitFlag)
                this.consignColCnt = this.getTTColumnCount(this.consignTTBitFlag);

            if (this.impTTBitFlag)
                this.impColCnt = this.getTTColumnCount(this.impTTBitFlag);

            this.updateHasUnknownTT();
        }

    }


    private getTTColumnCount(n) {
        let count: number = 0;
        while (n) {
            count = count + (n & 1);
            n = n >> 1;
        }
        return count;
    }


    isImpTT(ttFlag,assocFlag?) {
        if(assocFlag)
            return (this.impTTBitFlag & ttFlag)>0 && this.associationTypeCdFlag!='IMPT';

        return (this.impTTBitFlag & ttFlag) > 0;
    }

    isConsigTT(ttFlag,assocFlag?) {
        if(assocFlag)
            return (this.consignTTBitFlag & ttFlag) > 0 && this.associationTypeCdFlag!='CONS';

        return (this.consignTTBitFlag & ttFlag) > 0;
    }


    private isImpQtr(qtr){
        return qtr?((this.impQtrBitFlag & Math.pow(2,Number(qtr)-1))>0):false;
    }

    private isConsgQtr(qtr){
        return qtr?((this.consignQtrBitFlag & Math.pow(2,Number(qtr)-1))>0):false;
    }

    private isDef(val) {
        if (val || val == 0) return true;
        return false;
    }


    updatehelperText(selAnswer: any, existsInTufaFlag: any) {

        switch (selAnswer) {
            case "IMPT":
            case 'CONS':
                if (existsInTufaFlag === 'Y') {
                    this.helperText = "Entry Lines will be added to selected Company.";
                } else {
                    this.helperText = "Company and all Entry Lines will be added to Include Company bucket below.";
                }
                break;

            case "EXCL":
                this.helperText = "Entry Lines will be excluded from Market Share; not visible on Compare";
                break;

            default:
                this.helperText = "";
                break;
        }
    }

    resetSaveStatus(event: any, existsInTufaFlag: any) {
        this.cmpyAssoc.providedAnswer = false;
        this.annualTrueUpEventService.resetSavedCompanyAssocStatus(true);
        this.updatehelperText(event, existsInTufaFlag);
    }

    hasSelection() {
        return (this.cmpyAssoc.associationTypeCd && this.validAnswersCmpyAssoc.indexOf(this.cmpyAssoc.associationTypeCd) > -1) ? true : false;
    }

    updateHasUnknownTT(){
        this.cmpyAssoc.hasUnknownTT = this.isConsigTT(64)||this.isImpTT(64);
    }
    saveCmpyAssoc() {

        if (this.cmpyAssoc && !this.cmpyAssoc.associationTypeCd)
            return;

        let impCmpyAssocArry: AnnualTrueupCmpyAssocModel[] = [];
        impCmpyAssocArry.push(this.cmpyAssoc);
        this.busy = this.annualTrueUpService.saveCompanyAssociations(impCmpyAssocArry).subscribe(
            response => {
                if (response) {
                    this.cmpyAssoc.providedAnswer = true;
                    this.annualTrueUpEventService.updateSingleCmpyAssocStatus(true);
                    this.refreshBucket.emit(true);
                }
            }
        );
        this.busyChange.emit(this.busy);
    }
}


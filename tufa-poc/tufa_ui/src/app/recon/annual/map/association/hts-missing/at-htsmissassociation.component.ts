import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

import { AnnualTrueUpCBPEntryModel } from "./../../../../../recon/annual/models/at-cbpentry.model";
import { AnnualTrueUpService } from "./../../../../../recon/annual/services/at.service";
import { AnnualTrueUpEventService } from "./../../../../../recon/annual/services/at-event.service";
import { Subscription } from 'rxjs';

declare var jQuery: any;

@Component({
    selector: '[at-htsmissassociation]',
    templateUrl: './at-htsmissassociation.template.html',
    styleUrls:['../../../compare/details/at-compare-cigardetails.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class AnnualTrueUpHTSMissingAssociation {

    @Input() htsMissAssoc: AnnualTrueUpCBPEntryModel;
    @Input() index;
    @Input() fiscalYear;
    @Output() refreshCompany: EventEmitter<any> = new EventEmitter();
    @Input() busy: Subscription;
    @Output() busyChange = new EventEmitter();

    helperText: string;


    validAnswersHTSMissAssoc: string[] = ["Cigars", "Cigarettes", "Roll-Your-Own", "Pipe", "Snuff", "Chew", "Non-Taxable"];

    savedHTSMissAssocSuccess: boolean = false;

    subscriptions: Subscription[] = [];

    constructor(private annualTrueUpService: AnnualTrueUpService,
        private annualTrueUpEventService: AnnualTrueUpEventService) {
        this.helperText = "";

        this.subscriptions.push(this.annualTrueUpEventService.updateHTSMissAssoc$.subscribe(
            update => {
                if (update)
                    this.savedHTSMissAssocSuccess = true;
            }
        ));
    }

    ngOnInit() {

        this.helperText = "";

        if (this.htsMissAssoc.tobaccoClass && this.htsMissAssoc.tobaccoClass.tobaccoClassNm) {
            this.savedHTSMissAssocSuccess = true;
        }
    }

    ngOnDestroy() {
        if (this.subscriptions) {
            // tslint:disable-next-line:forin
            for (let i in this.subscriptions) {
                this.subscriptions[i].unsubscribe();
            }
        }
    }

    updatehelperText(tobaccoclassName: any){
        switch (tobaccoclassName) {
            case "Cigars":
            case "Cigarettes":
            case "Roll-Your-Own":
            case "Pipe":
            case "Snuff":
            case "Chew":
                this.helperText = "Entry Line will be added to associated Companies for selected Tobacco Class.";
                break;

            case "Non-Taxable":
                // tslint:disable-next-line:max-line-length
                this.helperText = "Entry Line will NOT be added to associated Companies. Data will be excluded from Market Share; not visible on Compare.";
                break;

            default:
                this.helperText = "";
                break;
        }
    }


    resetSaveStatus(event: any) {
        this.htsMissAssoc.providedAnswer = false;
        this.annualTrueUpEventService.resetSavedHTSMissAssocStatus(true);
        //this.annualTrueUpEventService.updateSingleHTSMissAssocStatus(true);
        this.savedHTSMissAssocSuccess = false;
        this.updatehelperText(event);
    }

    hasSelection() {
        // tslint:disable-next-line:max-line-length
        return (this.htsMissAssoc.tobaccoClass && this.validAnswersHTSMissAssoc.indexOf(this.htsMissAssoc.tobaccoClass.tobaccoClassNm) > -1) ? true : false;
    }

    saveHTSMissAssoc() {

        if (this.htsMissAssoc && !this.htsMissAssoc.tobaccoClass)
            return;

        let impHTSMissAssocArry: AnnualTrueUpCBPEntryModel[] = [];
        impHTSMissAssocArry.push(this.htsMissAssoc);
        this.busy = this.annualTrueUpService.saveHTSMissAssociations(impHTSMissAssocArry, this.fiscalYear).subscribe(
            response => {
                if (response) {
                    this.savedHTSMissAssocSuccess = true;
                    this.htsMissAssoc.providedAnswer = true;
                    this.annualTrueUpEventService.updateSingleHTSMissAssocStatus(true);
                    this.refreshCompany.emit(true);
                }
            }
        );
        this.busyChange.emit(this.busy);
    }


    getOtherTobaccoTypesReported() {
        // tslint:disable-next-line:max-line-length
        return (this.htsMissAssoc.cbpImporter.otherTobaccoTypesReported) ? this.htsMissAssoc.cbpImporter.otherTobaccoTypesReported.map(function (val) { if (val) return val.toUpperCase() }).join(', ') : "None";
    }

    getFiscalYear (mmddyyyy: string): string {
        let returnVal = null;
        if (mmddyyyy != null && mmddyyyy.length > 5) {
        let yearVal = 0;
        let monthVal = 0;
        let firstSlash = mmddyyyy.indexOf('/');
        let lastSlash = mmddyyyy.lastIndexOf('/');
        if (firstSlash == lastSlash) {
        return null;
        }
        let month = mmddyyyy.substring(0, firstSlash);
        let year = mmddyyyy.substring(lastSlash + 1, mmddyyyy.length);
        yearVal = Number(year);
        monthVal = Number(month);
        if (monthVal > 9){
        yearVal++;
        }
        returnVal = Number(yearVal);
        }
        return returnVal;
    }

    displayDt(dt1:string,dt2:string,ref:string):string {
        return this.getFiscalYear(dt1) === ref ? dt1:dt2;
    }
}



import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

import { AnnualTrueUpCBPEntryModel } from "./../../../../../recon/annual/models/at-cbpentry.model";
import { AnnualTrueUpService } from "./../../../../../recon/annual/services/at.service";
import { AnnualTrueUpEventService } from "./../../../../../recon/annual/services/at-event.service";
import { Subscription } from '../../../../../../../node_modules/rxjs';


declare var jQuery: any;

@Component({
    selector: '[at-htsassociation]',
    templateUrl: './at-htsassociation.template.html',
    styleUrls:['../../../compare/details/at-compare-cigardetails.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class AnnualTrueUpHTSAssociation implements OnInit, OnDestroy {

    @Input() htsAssoc: AnnualTrueUpCBPEntryModel;
    @Input() index;
    @Input() fiscalYear;
    @Output() refreshCompany: EventEmitter<any> = new EventEmitter();
    @Input() busy: Subscription;
    @Output() busyChange = new EventEmitter();

    helperText: string;

    validAnswersHtsAssoc: string[] = ["Cigars", "Cigarettes", "Roll-Your-Own", "Pipe", "Snuff", "Chew", "Non-Taxable"];


    savedHtsAssocSuccess: boolean = false;
    update: Subscription;

    constructor(private annualTrueUpService: AnnualTrueUpService,
        private annualTrueUpEventService: AnnualTrueUpEventService) {

        this.helperText = "";
        this.update = this.annualTrueUpEventService.updateHTSAssoc$.subscribe(
            update => {
                if (update)
                    this.savedHtsAssocSuccess = true;
            }
        );
    }

    ngOnDestroy(): void {
        if (this.update) {
            this.update.unsubscribe();
        }
    }

    updatehelperText(tobaccoclassName: any){
        switch (tobaccoclassName) {
            case "Cigars":
            case "Cigarettes":
            case "Roll-Your-Own":
            case "Pipe":
            case "Snuff":
            case "Chew":
                this.helperText = "Entry Lines will be added to associated Companies for selected Tobacco Class.";
                break;

            case "Non-Taxable":
                // tslint:disable-next-line:max-line-length
                this.helperText = "Entry Lines will NOT be added to associated Companies. Data will be excluded from Market Share; not visible on Compare.";
                break;

            default:
                this.helperText = "";
                break;
        }
    }

     ngOnInit() {
        if (this.htsAssoc.tobaccoClass && this.htsAssoc.tobaccoClass.tobaccoClassNm) {
            this.savedHtsAssocSuccess = true;
        }
    }

    resetSaveStatus(event: any) {
        this.htsAssoc.providedAnswer = false;
        this.annualTrueUpEventService.resetSavedHtsAssocStatus(true);
        //this.annualTrueUpEventService.updateSingleHTSAssocStatus(true);
        this.savedHtsAssocSuccess = false;
        this.updatehelperText(event);
    }

    hasSelection() {
        return (this.htsAssoc.tobaccoClass && this.validAnswersHtsAssoc.indexOf(this.htsAssoc.tobaccoClass.tobaccoClassNm) > -1) ? true : false;
    }

    saveHtsAssoc() {

        if (this.htsAssoc && !this.htsAssoc.tobaccoClass) return;

        let impHtsAssocArry: AnnualTrueUpCBPEntryModel[] = [];
        impHtsAssocArry.push(this.htsAssoc);
        this.busy = this.annualTrueUpService.saveHtsAssociations(impHtsAssocArry, this.fiscalYear).subscribe(
            response => {
                if (response) {
                    this.savedHtsAssocSuccess = true;
                    this.htsAssoc.providedAnswer = true;
                    this.annualTrueUpEventService.updateSingleHTSAssocStatus(true);
                    this.refreshCompany.emit(true);
                }
            }
        );

        this.busyChange.emit(this.busy);
    }
}


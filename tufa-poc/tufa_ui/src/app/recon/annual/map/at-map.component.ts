import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { TobaccoClass } from '../../../model/tobacco.class.model';
import { AnnualTrueUpCBPEntryModel } from "./../../../recon/annual/models/at-cbpentry.model";
import { AnnualTrueupCmpyAssocModel } from './../../../recon/annual/models/at-cbpimp-cmpyassc-delta.model';
import { AnnualTrueUpCmpyIngestionModel } from "./../../../recon/annual/models/at-cmpyingestion.model";
import { AnnualTrueUpEventService } from "./../../../recon/annual/services/at-event.service";
import { AnnualTrueUpService } from "./../../../recon/annual/services/at.service";
import { AnnualTrueUpDocModel } from '../models/at-doc.model';
import { DownloadSerivce } from '../../../shared/service/download.service';
import { AtIngestFileType } from '../models/at-ingest-file-type-enum';
import { CBPMetrics } from '../models/at-cbp-metrics.model';

declare var jQuery: any;

@Component({
    selector: '[at-map]',
    templateUrl: './at-map.template.html',
    styleUrls: ['at-map.style.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: []
})

export class AnnualTrueUpMapTabPane implements OnInit, AfterViewInit, OnDestroy {

    @Input() fiscalYear;
    @Input() refreshToken;
    @Input() context;
    @Output() processComplete: EventEmitter<any> = new EventEmitter();
    @Output() ingestCount: EventEmitter<any> = new EventEmitter();

    loaded: boolean = false;

    data: AnnualTrueUpDocModel[] = [];
    unknData = [];
    cbpexportdata: any;
    busy: Subscription;
    companyAssocSubscription: Subscription;

    annualTrueUpServiceEvSubscription: Subscription;
    fileIngestedSubscription: Subscription;

    // cmpyAssocArry: AnnualTrueUpCBPEntryModel[] = [];
    cmpyAssocArry: AnnualTrueupCmpyAssocModel[] = [];

    showConfirmPopup: Boolean = false;
    // htsAssocArry: any;
    permitIncArry: any[] = [];

    savedCmpAssocsSuccess: boolean = false;
    savedPermitInclSuccess: boolean = false;

    showCompanyAssociation: boolean = true;
    showIncludeSSNDatainMS: boolean = false;
    showIncludeTTBPermitsinMS: boolean = false;
    showCBPExport: boolean = false;

    answersRequiredCmpyAssoc: number = 0;
    answersProvidedCmpyAssoc: number = 0;

    answersRequiredPermitIncl: number = 0;
    answersProvidedPermitIncl: number = 0;

    validAnswersCmpyAssoc: string[] = ["I", "C", "X"];
    validAnswersPermitIncl: string[] = ["Y", "N"];

    // HTS  Association related properties
    htsAssocArry: AnnualTrueUpCBPEntryModel[] = [];
    validAnswersHtsAssoc: string[] = ["Cigars", "Cigarettes", "Roll-Your-Own", "Pipe", "Snuff", "Chew", "Non-Taxable"];
    answersRequiredHtsAssoc: number = 0;
    answersProvidedHtsAssoc: number = 0;
    savedHTSAssocsSuccess: boolean = false;
    showHTSAssociation: boolean = true;



    // HTS Missing Association related properties
    htsMissAssocArry: AnnualTrueUpCBPEntryModel[] = [];
    showHTSMissAssociation: boolean = true;
    answersRequiredHTSMissAssoc: number = 0;
    savedHTSMissAssocsSuccess: boolean = false;
    answersProvidedHTSMissAssoc: number = 0;
    // validAnswersHTSMissAssoc: string[] = ["Cigars", "Cigarettes", "Roll-Your-Own", "Pipe", "Snuff", "Chew", "Not-Taxable-Tobacco"];
    bucketsDifference: number = 0;

    /** Include Companies in Market Share  */
    inclCmpyMktShArry: AnnualTrueUpCmpyIngestionModel[] = [];
    answersRequiredInclCmpyMktSh: number = 0;
    answersProvidedInclCmpyMktSh: number = 0;
    validAnswersInclCmpyMktSh: string[] = ['Y', 'N'];
    savedInclCmpyMktShSuccess: boolean = false;
    showIncludeCompaniesinMS: boolean = true;
    showTTBPermitsinMS: boolean = true;
    subscriptions: Subscription[] = [];

    ingestHasUnknownTT: boolean = false;
    

    metrics: CBPMetrics;


    ingestFileAlerts: string[] = [];

    constructor(private annualTrueUpEventService: AnnualTrueUpEventService,
        private annualTrueUpService: AnnualTrueUpService,
        private downloadService: DownloadSerivce) {
        this.data = [];
        this.subscribeToTrueUpEventService();

    }
    

    ngOnChanges(changes: SimpleChanges) {

        for (let propName in changes) {
            if (propName === "context") {
                let chg = changes[propName];
                this.context = chg.currentValue;
            }
        }

    }

    ngOnInit() {
       
    }

    loadMapPage(selected: boolean = false){
      
        this.annualTrueUpService.getTrueupDocs(this.fiscalYear).subscribe(
            fileIngestedModel => {
                this.data = fileIngestedModel;
                this.updateIngestFileMessage();
              
            }
        )
            
       this.annualTrueUpService.getMetricData(this.fiscalYear).subscribe(
            response => {
                this.metrics = response;
            }
        );

        this.routine();
        this.annualTrueUpEventService.ingestedLegacySource$.subscribe((ingestedLegacy: boolean) => {
            if(ingestedLegacy){
                this.routine();
            };
        });
    }

    routine() {
      
        this.getCBPCompanyAssociationErrors();
        this.getCBPHtsAssociationErrors();
        this.getCBPHTSMissAssociationErrors();
        this.getCompaniesToInclInMktSh();
        this.getTTBInclusionPermits();
        this.unknownCompanyBucket();
    }

    updateAssociationBuckets() {
        this.getCBPHtsAssociationErrors();
        this.getCBPHTSMissAssociationErrors();
        this.getCompaniesToInclInMktSh();
        this.getCBPCompanyAssociationErrors();
    }

    adjustPanelScrollsforAccordions(accordionId) {
        jQuery(accordionId).on('shown.bs.collapse', function (e) {
            /*Scroll to the top of element in respect to the
            '.content-wrap' since the value of .top will depend 
            on scroll position*/
            let offset = jQuery(accordionId + ' .panel-title a').offset().top - jQuery('.content-wrap').offset().top;
            if (offset) {
                jQuery('html,body').animate({
                    scrollTop: offset
                }, 500);
            }
        });
    }

    ngAfterViewInit(): void {
      
        this.adjustPanelScrollsforAccordions('#accordion_ica');
        this.adjustPanelScrollsforAccordions('#accordion_ihtsa');
        this.adjustPanelScrollsforAccordions('#accordion_ihtsmc');
        this.adjustPanelScrollsforAccordions('#accordion_issnms');
        this.adjustPanelScrollsforAccordions('#accordion_icms');
        this.adjustPanelScrollsforAccordions('#accordion_ittbnms');
        this.adjustPanelScrollsforAccordions('#accordion_ittbms');
        this.adjustPanelScrollsforAccordions('#accordion_unkca');
        this.loaded = true;
    }

    ngOnDestroy() {
        if (this.annualTrueUpServiceEvSubscription) {
            this.annualTrueUpServiceEvSubscription.unsubscribe();
        }

        if (this.fileIngestedSubscription) {
            this.fileIngestedSubscription.unsubscribe();
        }

        if (this.subscriptions) {
            this.subscriptions.forEach(subscription => {
                subscription.unsubscribe();
            })
        }
    }

    calculateTotalDiff() {
        this.bucketsDifference = Math.abs(this.answersProvidedCmpyAssoc + this.answersProvidedHtsAssoc + this.answersProvidedHTSMissAssoc
            - this.answersRequiredCmpyAssoc - this.answersRequiredHtsAssoc - this.answersRequiredHTSMissAssoc);
        this.ingestCount.emit(this.bucketsDifference);
    }

    // Company Association related methods
    getCBPCompanyAssociationErrors() {
        this.companyAssocSubscription = this.annualTrueUpService.getCompanyAssociationInfo(this.fiscalYear).subscribe(
            response => {
                if (response) {

                    let cmpyAssocModelArry = [];
                    let assocdata = response;
                    // tslint:disable-next-line:forin
                    for (let i in assocdata) {
                        let cmpyassocModel = new AnnualTrueupCmpyAssocModel();
                        jQuery.extend(true, cmpyassocModel, assocdata[i]);
                        cmpyAssocModelArry.push(cmpyassocModel);
                    }
                    this.cmpyAssocArry = cmpyAssocModelArry;
                    this.calculateAnswersProvidedCmpyAssoc();
                    if (this.answersProvidedCmpyAssoc === this.answersRequiredCmpyAssoc)
                        this.savedCmpAssocsSuccess = true;
                    else this.savedCmpAssocsSuccess = false;

                    
                }
            }
        );

        this.subscriptions.push(this.companyAssocSubscription);
    }

    getTTBInclusionPermits() {
        this.annualTrueUpService.getTTBPermits(this.fiscalYear).subscribe(
            response => {
                if (response) {
                    let permitModelArry = [];
                    let errdata = response;
                    // tslint:disable-next-line:forin
                    for (let i in errdata) {
                        permitModelArry.push(errdata[i]);
                    }
                    this.permitIncArry = permitModelArry;
                    this.calculateAnswersProvidedPermitIncl();
                    if (this.answersProvidedPermitIncl === this.answersRequiredPermitIncl)
                        this.savedPermitInclSuccess = true;
                    else this.savedPermitInclSuccess = false;

                    
                }
            }
        );
    }

    calculateAnswersProvidedCmpyAssoc() {
        if (this.cmpyAssocArry.length > 0) {
            this.answersRequiredCmpyAssoc = this.cmpyAssocArry.length;
            let count = 0;
            for (let i in this.cmpyAssocArry) {
                if (this.cmpyAssocArry[i].providedAnswer) {
                    count++;
                }
            }

            this.answersProvidedCmpyAssoc = count;
        }
        else { this.answersRequiredCmpyAssoc = 0; this.answersProvidedCmpyAssoc = 0; }
        this.calculateTotalDiff();
    }

    calculateAnswersProvidedPermitIncl() {
        if (this.permitIncArry.length > 0) {
            this.answersRequiredPermitIncl = this.permitIncArry.length;
            let count = 0;
            for (let i in this.permitIncArry) {
                if (this.permitIncArry[i].providedAnswer) {
                    count++;
                }
            }
            this.answersProvidedPermitIncl = count;
        }
        else { this.answersRequiredPermitIncl = 0; this.answersProvidedPermitIncl = 0; }
    }


    hasValidSaves() {
        //Only do this after evertrhing has been loaded
        //Was getting changed after check error
        if (this.cmpyAssocArry && this.loaded) {

            let answersProvided: number = jQuery(":radio[name='associationradio']:checked").length;
            let answersRequired = this.cmpyAssocArry.length;

            let hasUnknown: boolean = false;
            this.cmpyAssocArry.forEach(item => {
                if (item.hasUnknownTT) hasUnknown = true;
            })
            return (answersProvided === answersRequired && !hasUnknown);
        }
    }

    saveCompanyAssociations() {
        this.cmpyAssocArry.forEach(item => item.providedAnswer = true);
        this.busy = this.annualTrueUpService.saveCompanyAssociations(this.cmpyAssocArry).subscribe(
            response => {
                if (response) {
                    this.savedCmpAssocsSuccess = true;
                    this.annualTrueUpEventService.updateSavedCompanyAssocStatus(true);
                    this.calculateAnswersProvidedCmpyAssoc();
                    this.updateAssociationBuckets();
                }
            }
        );
    }

    // HTS Code Association related methods
    getCBPHtsAssociationErrors() {
        this.annualTrueUpService.getHtsAssociationInfo(this.fiscalYear).subscribe(
            response => {
                if (response) {
                    let impModelArry = [];
                    let errdata = response;
                    // tslint:disable-next-line:forin
                    for (let i in errdata) {
                        let impModel = new AnnualTrueUpCBPEntryModel();
                        jQuery.extend(true, impModel, errdata[i]);
                        if (!impModel.tobaccoClass)
                            impModel.tobaccoClass = new TobaccoClass();
                        impModelArry.push(impModel);
                    }
                    this.htsAssocArry = impModelArry;
                    this.calculateAnswersProvidedHtsAssoc();
                    if (this.answersProvidedHtsAssoc === this.answersRequiredHtsAssoc)
                        this.savedHTSAssocsSuccess = true;
                    else this.savedHTSAssocsSuccess = false;

                }
            }
        );
    }

    calculateAnswersProvidedHtsAssoc() {
        if (this.htsAssocArry.length > 0) {
            this.answersRequiredHtsAssoc = this.htsAssocArry.length;
            let count = 0;
            for (let i in this.htsAssocArry) {
                if (this.htsAssocArry[i].providedAnswer) {
                    count++;
                }
            }

            this.answersProvidedHtsAssoc = count;
        }
        else { this.answersRequiredHtsAssoc = 0; this.answersProvidedHtsAssoc = 0; }
        this.calculateTotalDiff();
    }

    hasSelectionsHtsAssociation() {

        if (this.htsAssocArry) {
            let answersProvided: number = 0;
            let answersRequired = this.htsAssocArry.length;
            for (let i in this.htsAssocArry) {
                if (this.htsAssocArry[i].tobaccoClass && this.validAnswersHtsAssoc.indexOf(this.htsAssocArry[i].tobaccoClass.tobaccoClassNm) > -1) {
                    answersProvided++;
                }
            }
            return (answersProvided === answersRequired) ? true : false;
        }
    }

    saveHtsAssociations() {
        this.htsAssocArry.forEach(item => item.providedAnswer = true);
        this.busy = this.annualTrueUpService.saveHtsAssociations(this.htsAssocArry, this.fiscalYear).subscribe(
            response => {
                if (response) {
                    this.savedHTSAssocsSuccess = true;
                    this.annualTrueUpEventService.updateSavedHtsAssocStatus(true);
                    this.calculateAnswersProvidedHtsAssoc();
                    this.getCBPCompanyAssociationErrors();
                    this.getCompaniesToInclInMktSh();
                }
            }
        );
    }

    // HTS Missing Code Association related methods

    getCBPHTSMissAssociationErrors() {
        this.annualTrueUpService.getHTSMissAssociationInfo(this.fiscalYear).subscribe(
            response => {
                if (response) {
                    let impModelArry = [];
                    let errdata = response;
                    // tslint:disable-next-line:forin
                    for (let i in errdata) {
                        let impModel = new AnnualTrueUpCBPEntryModel();
                        jQuery.extend(true, impModel, errdata[i]);
                        if (!impModel.tobaccoClass)
                            impModel.tobaccoClass = new TobaccoClass();
                        impModelArry.push(impModel);
                    }
                    this.htsMissAssocArry = impModelArry;
                    this.calculateAnswersProvidedHTSMissAssoc();
                    // tslint:disable-next-line:max-line-length
                  
                    if (this.answersProvidedHTSMissAssoc === this.answersRequiredHTSMissAssoc)
                        this.savedHTSMissAssocsSuccess = true;
                    else this.savedHTSMissAssocsSuccess = false;


                }
            }
        );
    }

    calculateAnswersProvidedHTSMissAssoc() {
        if (this.htsMissAssocArry.length > 0) {
            this.answersRequiredHTSMissAssoc = this.htsMissAssocArry.length;
            let count = 0;
            for (let i in this.htsMissAssocArry) {
                if (this.htsMissAssocArry[i].providedAnswer) {
                    count++;
                }
            }

            this.answersProvidedHTSMissAssoc = count;
        }
        else { this.answersRequiredHTSMissAssoc = 0; this.answersProvidedHTSMissAssoc = 0; }
        this.calculateTotalDiff();
    }

    hasSelectionsHTSMissAssociation() {

        if (this.htsMissAssocArry) {
            let answersProvided: number = 0;
            let answersRequired = this.htsMissAssocArry.length;
            for (let i in this.htsMissAssocArry) {
                // tslint:disable-next-line:max-line-length
                if (this.htsMissAssocArry[i].tobaccoClass && this.validAnswersHtsAssoc.indexOf(this.htsMissAssocArry[i].tobaccoClass.tobaccoClassNm) > -1) {
                    answersProvided++;
                }
            }
            return (answersProvided === answersRequired) ? true : false;
        }

    }



    saveHTSMissAssociations() {
        this.htsMissAssocArry.forEach(item => item.providedAnswer = true);
        this.busy = this.annualTrueUpService.saveHTSMissAssociations(this.htsMissAssocArry, this.fiscalYear).subscribe(
            response => {
                if (response) {
                    this.savedHTSMissAssocsSuccess = true;
                    this.annualTrueUpEventService.updateSavedHTSMissAssocStatus(true);
                    this.calculateAnswersProvidedHTSMissAssoc();
                    this.getCBPCompanyAssociationErrors();
                    this.getCompaniesToInclInMktSh();
                }
            }
        );
    }

    unknownCompanyBucket() {
        this.annualTrueUpService.getUnknownCompanyBucketInfo(this.fiscalYear).subscribe(
            result => { if (result) 
                this.unknData = result; 
               
            }
        );
    }

    /****  Subscribe annual trueup events  */
    subscribeToTrueUpEventService() {

        /*** 
         *  subscriptions to reset event to toggle the status of 'Save All' button. This will be called
         *  radio buttons are toggled in the child panels. 
         */
        this.subscriptions.push(this.annualTrueUpEventService.savedCompanyAssocReset$.subscribe(
            reset => {
                if (reset) {
                    this.calculateAnswersProvidedCmpyAssoc();
                    this.savedCmpAssocsSuccess = false;
                }
            }
        ));

        this.subscriptions.push(this.annualTrueUpEventService.savedHTSMissAssocReset$.subscribe(
            reset => {
                if (reset) {
                    this.calculateAnswersProvidedHTSMissAssoc();
                    this.savedHTSMissAssocsSuccess = false;
                }
            }
        ));

        this.subscriptions.push(this.annualTrueUpEventService.savedHTSAssocReset$.subscribe(
            reset => {
                if (reset) {
                    this.calculateAnswersProvidedHtsAssoc();
                    this.savedHTSAssocsSuccess = false;
                }
            }
        ));

        /***
         * Subscription to events to recalculate the counters for answers provided and answers required.
         */
        this.subscriptions.push(this.annualTrueUpEventService.updateSingleCmpyAssoc$.subscribe(
            update => {
                if (update) {
                    this.calculateAnswersProvidedCmpyAssoc();
                    if (this.answersProvidedCmpyAssoc === this.answersRequiredCmpyAssoc)
                        this.savedCmpAssocsSuccess = true;
                    else this.savedCmpAssocsSuccess = false;
                }
            }
        ));

        this.subscriptions.push(this.annualTrueUpEventService.updateSingleHTSAssoc$.subscribe(
            increment => {
                if (increment) {
                    this.calculateAnswersProvidedHtsAssoc();
                    if (this.answersProvidedHtsAssoc === this.answersRequiredHtsAssoc) {
                        this.savedHTSAssocsSuccess = true;
                    }
                    else this.savedHTSAssocsSuccess = false;
                }
            }
        ));

        this.subscriptions.push(this.annualTrueUpEventService.updateSingleHTSMissAssoc$.subscribe(
            update => {
                if (update) {
                    this.calculateAnswersProvidedHTSMissAssoc();
                    if (this.answersProvidedHTSMissAssoc === this.answersRequiredHTSMissAssoc)
                        this.savedHTSMissAssocsSuccess = true;
                    else this.savedHTSMissAssocsSuccess = false;
                }
            }
        ));

        
        this.subscriptions.push(this.annualTrueUpEventService.atCompanyCreatedSource$.subscribe(
            cmpyCreated => {
                if (cmpyCreated) {
                    this.getCompaniesToInclInMktSh();
                    this.getTTBInclusionPermits();
                }
            }
        ));

        this.subscriptions.push(this.annualTrueUpEventService.updateExcludeCmpyMktShStatus$.subscribe(
            cmpyIngestionModel => {
                if (cmpyIngestionModel) {
                    this.saveInclCmpyMktSh(cmpyIngestionModel);
                }
            }
        ));

       

    }

    public updateIngestFileMessage() {
        this.ingestFileAlerts = [];

        let missingCBPS = true;
        let missingCBPD = true;
        let missingTTBT = true;
        let missingTTBV = true;

        if (this.data) {
            // tslint:disable-next-line:forin

            for (let i in this.data) {
                let doc = this.data[i];
                if (doc.docDesc === AtIngestFileType.DOC_CBP_SUM) {
                    missingCBPS = false;
                } else if (doc.docDesc === AtIngestFileType.DOC_CBP_DET) {
                    missingCBPD = false;
                } else if (doc.docDesc === AtIngestFileType.DOC_TTB_TAX) {
                    missingTTBT = false;
                } else if (doc.docDesc === AtIngestFileType.DOC_TTB_VOL) {
                    missingTTBV = false;
                }
            }

            if ( (this.context =='detailOnly' && missingCBPD) || 
            (this.context !='detailOnly' && (missingCBPS || missingCBPD))) {
                this.ingestFileAlerts.push("CBP file(s) are missing from Ingest");
            }
            if (missingTTBT || missingTTBV) {
                this.ingestFileAlerts.push("TTB file(s) are missing from Ingest");
            }
        }
    }

    /** Include Company in Market Share related methods **/

    getCompaniesToInclInMktSh() {
        this.showCBPExport = false;
        this.annualTrueUpService.getInclCmpyMktSh(this.fiscalYear).subscribe(
            response => {
                if (response) {
                    let impModelArry = [];
                    let errdata = response;
                    // tslint:disable-next-line:forin
                    for (let i in errdata) {
                        let impModel = new AnnualTrueUpCmpyIngestionModel();
                        jQuery.extend(true, impModel, errdata[i]);
                        impModelArry.push(impModel);
                    }
                    this.inclCmpyMktShArry = impModelArry;

                    for (let i in this.inclCmpyMktShArry) {

                        if (this.inclCmpyMktShArry[i].associationType.includes("Importer")) {
                            this.showCBPExport = true;
                        }
                    }

                    this.calculateAnswersProvidedInclCompyMktSh();
                    if (this.answersProvidedInclCmpyMktSh === this.answersRequiredInclCmpyMktSh)
                        this.savedInclCmpyMktShSuccess = true;
                    else
                        this.savedInclCmpyMktShSuccess = false;
                        
                }
            }
        );
    }


    calculateAnswersProvidedInclCompyMktSh() {

        if (this.inclCmpyMktShArry.length > 0) {
            this.answersRequiredInclCmpyMktSh = this.inclCmpyMktShArry.length;
            let count = 0;
            for (let i in this.inclCmpyMktShArry) {
                if (this.inclCmpyMktShArry[i].providedAnswer) {
                    count++;
                }
            }

            this.answersProvidedInclCmpyMktSh = count;
        }
        else { this.answersRequiredInclCmpyMktSh = 0; this.answersProvidedInclCmpyMktSh = 0; }
    }


    hasSelectionsInclCmpyMktSh() {

        if (this.inclCmpyMktShArry) {
            let answersProvided: number = 0;
            let answersRequired = this.inclCmpyMktShArry.length;
            for (let i in this.inclCmpyMktShArry) {
                if (this.inclCmpyMktShArry[i].includeFlag && this.validAnswersInclCmpyMktSh.indexOf(this.inclCmpyMktShArry[i].includeFlag) > -1) {
                    answersProvided++;
                }
            }
            return (answersProvided === answersRequired) ? true : false;
        }
    }
    
    saveInclCmpyMktSh(cmpyIngestionModel: AnnualTrueUpCmpyIngestionModel) {
        let updatedCmpyMktShArry = [];
        updatedCmpyMktShArry.push(cmpyIngestionModel);
        // this.inclCmpyMktShArry.forEach(item => item.providedAnswer = true);
        this.annualTrueUpService.saveInclCmpyMktSh(updatedCmpyMktShArry, this.fiscalYear).subscribe(
            response => {
                if (response) {
                    this.savedInclCmpyMktShSuccess = true;
                    //this.annualTrueUpEventService.updateSavedInclCmpyMktShStatus(true);
                   // this.calculateAnswersProvidedInclCompyMktSh();
                   
                }
            }
        ); 
    }

    deleteFile(data: any[]) {
        this.showConfirmPopup = false;
        jQuery('#confirm-popup').modal('hide');
    }



    CBPExportNewCompanyDetail() {

        if (this.busy) {
            this.busy.unsubscribe();
        }

        this.busy = this.annualTrueUpService.getCBPNewCompanies(this.fiscalYear).subscribe(
            data => {
                if (data) {
                    this.cbpexportdata = data;
                    this.downloadService.download(this.cbpexportdata.csv, this.cbpexportdata.title, 'csv/text');
                }
            });
    }

}

/*
@author : Deloitte
*/

import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { Company } from '../../../../../company/models/company.model';
import { CompanyService } from '../../../../../company/services/company.service';
import { Permit } from '../../../../../permit/models/permit.model';
import { AnnualTrueUpService } from '../../../services/at.service';
import { AnnualTrueUpCmpyIngestionModel } from "./../../../../../recon/annual/models/at-cmpyingestion.model";
import { AnnualTrueUpEventService } from "./../../../../../recon/annual/services/at-event.service";
declare var jQuery: any;

/*** 
 * @export
 * @class create permit
 */
@Component({
  selector: '[at-create-company-popup]',
  templateUrl: './at-create-company-popup.template.html',
  providers: [AnnualTrueUpService, CompanyService]
})

export class AnnualTrueUpCreateCompany implements OnInit {
  @Input() company: AnnualTrueUpCmpyIngestionModel;
  @Output() companyCreated: EventEmitter<any> = new EventEmitter();

  disableClick: boolean = false;
  showInactiveerror: boolean = false;
  busy: Subscription;
  vanishText: string;
  vanishFlag: boolean;
  permitNumber: string;
  activedate: Date;
  closeddate: Date;
  companyId: number;
  einNo: String;
  permitStatus: string;
  data: any = [];
  manudata: any = [];
  imptdata: any = [];
  entry: any = [];
  entrysummarydate: any[] = [];
  entrydate: any[] = [];
  newcompany: any[] = [];
  hasError: boolean = false;
  disableCreate: boolean = false;


  public einmask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
  public mask = [/[A-Za-z]/, /[A-Za-z]/, '-', /[A-Za-z]/, /[A-Za-z]/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];


  datepickerOpts = {
    autoclose: true,
    todayBtn: 'linked',
    todayHighlight: true,
    assumeNearbyYear: true,
    endDate: new Date(),
    requiredErrorMsg: "Error: TUFA Active Date is required",
    title: 'Enter Active Date in MM/DD/YYYY format'
  };


  datepickerOptsClsd = {
    autoclose: true,
    todayBtn: 'linked',
    todayHighlight: true,
    assumeNearbyYear: true,
    requiredErrorMsg: "Error:TUFA Inactive Date is required",
    title: 'Enter TUFA Inactive Date in MM/DD/YYYY format'
  };

  ngOnInit() {
  }

  constructor(private service: AnnualTrueUpService,
    private annualEventService: AnnualTrueUpEventService,
    private companyService: CompanyService) {

  }

  loadcbpcompany(imptdata) {

    this.einNo = this.imptdata.einNo;
    this.entrysummarydate = [];
    this.entrydate = [];
    //  Find the earliest entry summary date , if null find entry date

    for (let i in this.imptdata.entry) {
      if (this.imptdata.entry[i].entrySummDt != null) {
        this.entrysummarydate.push(new Date(this.imptdata.entry[i].entrySummDt.toString()));
      }
      if (this.imptdata.entry[i].entryDt != null)
        this.entrydate.push(new Date(this.imptdata.entry[i].entryDt.toString()));
    }

    var earliestsummarydate = _.min(this.entrysummarydate);
    var earliestentrydate = _.min(this.entrydate);

    if (earliestsummarydate != null)
      this.activedate = new Date(earliestsummarydate);
    else if (earliestentrydate != null)
      this.activedate = new Date(earliestentrydate);
    else
      this.activedate = null;

    this.closeddate = null;
    this.permitNumber = "";
    this.permitStatus = "ACTV";
    this.showInactiveerror = false;

  }

  toggle(event) {

    this.permitStatus = event;

  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "company") {
        this.data = [];
        let cmpy = changes[propName].currentValue;
        if (cmpy.associationType == "Manufacturer, Importer") {
          this.getImptManuDetails(cmpy);
        }
        else {
          this.busy = this.service.getNewIngestedCompany(cmpy.fiscalYr, cmpy.einNo, cmpy.associationType).subscribe(
            data => {
              if (data)

                if (cmpy.associationType == "Importer") {
                  this.imptdata = data;
                  this.manudata = [];
                  this.imptdata.associationType = "Importer";
                  this.loadcbpcompany(this.imptdata);
                }
                else {
                  this.manudata = data;
                  this.imptdata = [];
                  this.manudata.associationType = "Manufacturer";
                }
            }
          )

        }
      }
    }
  }

  getImptManuDetails(cmpy) {
    this.busy = this.service.getNewIngestedCompany(cmpy.fiscalYr, cmpy.einNo, 'Manufacturer').subscribe(
      data => {
        if (data)
          this.manudata = data;

        this.manudata.associationType = "Manufacturer, Importer";
      })

    this.busy = this.service.getNewIngestedCompany(cmpy.fiscalYr, cmpy.einNo, 'Importer').subscribe(
      data => {
        if (data)
          this.imptdata = data;

        this.imptdata.associationType = "Manufacturer, Importer";
        this.loadcbpcompany(this.imptdata);
      })
  }

  ngOnDestroy() {
    if (this.busy) {
      this.busy.unsubscribe();
    }
  }

  reset() {
    jQuery('#at-create-company').modal('hide');
    if (this.imptdata != null) {
      this.loadcbpcompany(this.imptdata);
    }
    //Resert flags
    this.hasError = false;
    this.disableCreate = false;
    this.vanishText = "";
  }

  createCompany(): void {

    if (!this.data) return;

    let company = new Company();
    let permit = new Permit();

    if (this.manudata.associationType == "Manufacturer") {

      company.legalName = this.manudata.companyName;
      company.einNumber = this.manudata.einNo;
      this.createttbCompany(company)
    }
    else if (this.imptdata.associationType == "Importer") {
      company.legalName = this.imptdata.companyName;
      company.einNumber = this.imptdata.einNo;
      this.createcbpCompany(company)
    }
    else if (this.imptdata.associationType == "Manufacturer, Importer") {

      company.legalName = this.manudata.companyName;
      company.einNumber = this.manudata.einNo;
      company.permitSet = this.manudata.permits;
      company.companyStatus = 'ACTV';

      //Create TTB Company with Permit and CBP permit. Here the Company name which is saved in the DB is from Manufacturer.

      this.createCompanyandPermit(company)

    }

  }

  createttbCompany(company: Company) {

    company.permitSet = this.manudata.permits;
    company.companyStatus = 'ACTV';
    this.busy = this.service.createNewIngestedCompany(company).subscribe(data => {
      if (data) {
        this.hasError = false;
        this.disableCreate = true;
        this.vanishText = "Company and Permit(s) Successfully Created!";
        setTimeout(function () {
          this.disableCreate = false;
          this.vanishText = "";
          jQuery('#at-create-company').modal('hide');
          this.annualEventService.triggerAtCompanyCreated(true);
        }.bind(this), 3000);
      }
    }, error => {
      this.handleError(error);
    });

  }


  createCompanyandPermit(company: Company) {

    this.showInactiveerror = false;
    if (this.closeddate != null && (this.closeddate <= this.activedate)) {
      this.showInactiveerror = true
      return;
    }

    jQuery('#createcompany-form').parsley().validate();

    if (jQuery('#createcompany-form').parsley().isValid()) {

      company.permitSet = this.manudata.permits;
      company.companyStatus = 'ACTV';
      this.busy = this.service.createNewIngestedCompany(company).subscribe(data => {
        if (data) {
          this.hasError = false;
          this.newcompany = <any>data;
          this.companyId = (<any>data).companyId;
          //Add permit for Importer here
          this.addPermit();
          setTimeout(function () {
          }.bind(this), 3000);
        }
      }, error => {
        this.handleError(error);
      });

    }

  }

  createcbpCompany(company: Company) {

    this.showInactiveerror = false;
    if (this.closeddate != null && (this.closeddate <= this.activedate)) {
      this.showInactiveerror = true
      return;
    }

    jQuery('#createcompany-form').parsley().validate();

    if (jQuery('#createcompany-form').parsley().isValid()) {

      company.companyStatus = "ACTV";
      company.permitStatusTypeCd = this.permitStatus;
      company.permitTypeCd = "IMPT"
      company.permitSet = null;
      company.activeDate = this.activedate.toString();
      if (this.permitStatus == "CLSD")
        company.inactiveDate = this.closeddate.toString();

      let permitNum: string = this.permitNumber;
      company.permitNumber = permitNum = '' ? '' : permitNum.replace(/-/g, "");

      this.busy = this.service.createCompany(company).subscribe(data => {
        if (data) {
          this.hasError = false;
          this.disableCreate = true;
          this.vanishText = "Company and Permit(s) Successfully Created!";
          setTimeout(function () {
            this.disableCreate = false;
            this.vanishText = "";
            jQuery('#at-create-company').modal('hide');
            this.annualEventService.triggerAtCompanyCreated(true);
          }.bind(this), 3000);
        }
      }, error => {
        this.handleError(error);
      })
    }
  }


  public addPermit() {

    let permit = new Permit();
    permit.companyId = this.companyId;
    let permitNum: string = this.permitNumber;
    permit.permitNum = permitNum = '' ? '' : permitNum.replace(/-/g, "");
    permit.permitStatusTypeCd = this.permitStatus;
    permit.permitTypeCd = 'IMPT'
    permit.issueDt = this.activedate.toString();
    if (this.permitStatus == "CLSD")
      permit.closeDt = this.closeddate.toString();

    this.busy = this.companyService.createPermit(permit).subscribe(data => {
      if (data) {
        this.disableCreate = true;
        this.hasError = false;
        this.vanishText = "Company and Permit(s) Successfully Created!";
        setTimeout(function () {
          this.disableCreate = false;
          this.vanishText = "";
          jQuery('#at-create-company').modal('hide');
          this.annualEventService.triggerAtCompanyCreated(true);
        }.bind(this), 3000);
      }
    }, error => {
      this.handleError(error);
    }
    )

  }

  private handleError(error) {
    this.hasError = true;
    this.vanishText = "Failed to Create Company and Permit(s)";
    // jQuery('#act-dtpicker').parsley().addError('Permit cannot be created on a future date.');
    if (error && typeof error === 'string') {
      jQuery('#ein-number').parsley().removeError('EIN-Exists');
      jQuery('#ttb-permit').parsley().removeError('TTB-Exists');
      if (error.indexOf('EIN') > - 1) {
        error = "Error: " + error;
        jQuery('#ein-number').parsley().addError('EIN-Exists', { message: error });
      } else if (error.indexOf('TTB') > - 1) {
        error = "Error: " + error;
        jQuery('#ttb-permit').parsley().addError('TTB-Exists', { message: error });
      }
    }
  }

}
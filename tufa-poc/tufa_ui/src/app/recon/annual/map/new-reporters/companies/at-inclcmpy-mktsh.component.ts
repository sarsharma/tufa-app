import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

import { AnnualTrueUpCBPEntryModel } from "./../../../../../recon/annual/models/at-cbpentry.model";
import { AnnualTrueUpSaveButtonStatus } from "./../../../../../recon/annual/models/at-save.button.status.model"
import { AnnualTrueUpCmpyIngestionModel } from "./../../../../../recon/annual/models/at-cmpyingestion.model";
import { AnnualTrueUpService } from "./../../../../../recon/annual/services/at.service";
import { AnnualTrueUpEventService } from "./../../../../../recon/annual/services/at-event.service";
import { Subscription } from 'rxjs';

declare var jQuery: any;

@Component({
    selector: '[at-inclcmpy-mktsh]',
    templateUrl: './at-inclcmpy-mktsh.template.html',
    styleUrls:['../../../compare/details/at-compare-cigardetails.style.scss'],
    encapsulation: ViewEncapsulation.None
})

export class AnnualTrueUpInclCmpyMktSh {

    @Input() inclCmpy: AnnualTrueUpCmpyIngestionModel;
    @Input() index;
    @Input() fiscalYear;

    isSaved: boolean = false;

    helperText: string;

    validAnswersInclCmpyMktSh: string[] = ['Y', 'N'];

    savedInclCmpyMktShSuccess: boolean = false;

    subscriptions: Subscription[] = [];
    incrementFlag: boolean = true;
    decrementFlag: boolean = true;

    constructor(private annualTrueUpService: AnnualTrueUpService,
        private annualTrueUpEventService: AnnualTrueUpEventService) {
        this.helperText = "";

        // this.subscriptions.push(this.annualTrueUpEventService.updateInclCmpyMktSh$.subscribe(
        //     update => {
        //         if (update) {
        //             this.savedInclCmpyMktShSuccess = true;
        //             this.incrementFlag = true;
        //             this.decrementFlag = true;
        //         }
        //     }
        // ));
    }

    ngOnInit() { }

    ngOnDestroy() {
        if (this.subscriptions) {
            this.subscriptions.forEach(subscription => {
                subscription.unsubscribe();
            })
        }
    }

    hasSelection() {
        // tslint:disable-next-line:max-line-length
        return (this.inclCmpy.includeFlag && this.validAnswersInclCmpyMktSh.indexOf(this.inclCmpy.includeFlag) > -1) ? true : false;
    }

    getOtherTobaccoTypesReported() {
        // tslint:disable-next-line:max-line-length
        return (this.inclCmpy.otherTobaccoTypesReported) ? this.inclCmpy.otherTobaccoTypesReported.map(function (val) { if (val) return val.toUpperCase() }).join(', ') : "None";
    }

    setSelectedCmpy(company: AnnualTrueUpCmpyIngestionModel) {
        company.fiscalYr = this.fiscalYear;
        this.annualTrueUpEventService.triggerAtCreateCompany(company);
    }

    getAssociationType(associationType) {

        if (associationType) {
            let assTypeArry = associationType.split(',')
            let trimArry = jQuery.map(assTypeArry,function(atype){return atype.trim()});
            if (trimArry.indexOf('Manufacturer') !== -1 && trimArry.indexOf('Importer') !== -1 )
                return 'BOTH';
            else if (trimArry.indexOf('Manufacturer') !== -1)
                return 'MANU';
            else if (trimArry.indexOf('Importer') !== -1)
                return 'IMPT';

        }
    }

    alternateExclude() {
        if(this.inclCmpy.includeFlag && this.inclCmpy.includeFlag === 'Y') {
            this.inclCmpy.includeFlag = 'N';
        } else {
            this.inclCmpy.includeFlag = 'Y';
        }

        this.annualTrueUpEventService.triggerUpdateExcludeCmpyMktShStatus(this.inclCmpy);
    }

}



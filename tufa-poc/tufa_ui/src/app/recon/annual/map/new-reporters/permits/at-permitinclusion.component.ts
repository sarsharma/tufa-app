import { Component, EventEmitter, Output, SimpleChanges, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { AnnualTrueUpCBPEntryModel } from "./../../../../../recon/annual/models/at-cbpentry.model";
import { AnnualTrueUpService } from "./../../../../../recon/annual/services/at.service";
import { AnnualTrueUpEventService } from "./../../../../../recon/annual/services/at-event.service";
import { Router, NavigationExtras, ActivatedRoute, Params, NavigationStart, NavigationEnd } from '@angular/router';
import { ShareDataService } from '../../../../../core/sharedata.service';
import { Subscription } from '../../../../../../../node_modules/rxjs';


declare var jQuery: any;

@Component({
    selector: '[at-permitinclusion]',
    templateUrl: './at-permitinclusion.template.html',
    encapsulation: ViewEncapsulation.None
})

export class AnnualTrueUpPermitInclusion implements OnDestroy {

    @Input() permitInc: any;
    @Input() index;
    helperText: string;
    shareData: ShareDataService;

    navigateToCompanydetail: boolean;

    validAnswersPermitIncl: string[] = ["Y", "N"];

    private update: Subscription;

    constructor(private annualTrueUpService: AnnualTrueUpService,
        private annualTrueUpEventService: AnnualTrueUpEventService,private router: Router, _shareData: ShareDataService) {
             
        this.router = router;
        this.shareData = _shareData;
        
        this.helperText = "";
        this.update = this.annualTrueUpEventService.updatePermitIncl$.subscribe(
            update => {
                if (update)
                    this.permitInc.providedAnswer = true;
            }
        );
    }

    ngOnDestroy(): void {
        if (this.update) {
            this.update.unsubscribe();
        }
    }

    hasSelection() {
       return (this.permitInc && this.validAnswersPermitIncl.indexOf(this.permitInc.includeFlag) > -1) ? true : false;
    }

    setSelectedPermit(data: any) {
        //this.permitInc.emit(data);
        this.annualTrueUpEventService.triggerAtCreatePermit(data);
    }

     toCompany(einNum) {

        this.navigateToCompanydetail = true;
        this.shareData.breadcrumb = "at-reconreport";
       // this.shareData.filemgmttab = 'company';
        this.router.navigate(["/app/company/details", { ein: einNum }]);
    }
}


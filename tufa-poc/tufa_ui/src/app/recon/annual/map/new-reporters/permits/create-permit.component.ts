/*
@author : Deloitte
*/

import { Component, Input, OnInit, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { AnnualTrueUpService } from '../../../services/at.service';
import { Subscription } from 'rxjs';
import { AnnualTrueUpEventService } from "./../../../../../recon/annual/services/at-event.service";
import { Permit } from './../../../../../permit/models/permit.model';
import { CompanyService } from './../../../../../company/services/company.service';
declare var jQuery: any;

/*** 
 * @export
 * @class create permit
 */
@Component({
  selector: '[create-permit]',
  templateUrl: './create-permit.template.html',
  providers: [ AnnualTrueUpService, CompanyService ]
})

export class CreatePermit implements OnInit {
  @Input() permit: Permit;
  @Output() callback = new EventEmitter<boolean>();
  disableClick: boolean = false;
  busy: Subscription;
  vanishText: string;
  vanishFlag: boolean;

  ngOnInit() {
  }

  constructor(private service: AnnualTrueUpService, 
  private annualEventService: AnnualTrueUpEventService, 
  private companyService: CompanyService) {
  }

  ngOnDestroy() {
      if (this.busy) {
          this.busy.unsubscribe();
      }
  }

  reset() {
    this.vanishFlag = false;
    jQuery('#create-permit').modal('hide');
  }

  createPermit(permit: any): void {
    let resultPermit: Permit = new Permit();
    resultPermit.duplicateFlag = true;
    resultPermit.companyId = permit.companyId;
    resultPermit.permitTypeCd = "MANU";
    resultPermit.permitStatusTypeCd = "ACTV";
    resultPermit.permitNum = permit.permitNum;
    resultPermit.issueDt = new Date(permit.tpbdDt).toString();
    resultPermit.ttbstartDt = new Date(permit.tpbdDt).toString();

    this.busy = this.companyService.createPermit(resultPermit).subscribe(data => {
      if (data) {
          this.vanishFlag = true;
          this.vanishText = "Permit Successfully Created!";
          setTimeout(function() {
              this.vanishFlag = false;
              jQuery('#create-permit').modal('hide');
              //calling to refresh the bucket
              this.annualEventService.triggerAtCompanyCreated(true);
          }.bind(this), 3000);
        }
      }, error => {
        this.vanishFlag = true;
        this.vanishText = "Permit cannot be created on a future date.";
      });
  }

}


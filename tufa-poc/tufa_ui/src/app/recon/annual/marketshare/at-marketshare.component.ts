import { Component, Input, Output, ViewEncapsulation, EventEmitter, OnInit, OnDestroy, SimpleChanges,ViewChild, } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { NgBusyModule } from 'ng-busy';
import * as _ from "lodash";
import { AnnualTrueUpMarketShareService } from "./../../../recon/annual/services/at-marketshare.service";
import { SubmissionDocument } from '../models/at-submission-doc.model';
import { AnnualTrueUpService } from "../services/at.service";
import { AnnualTrueUpComparisonService } from './../services/at-compare.service';
import * as JSZip from 'jszip';
import { ShareDataService } from '../../../core/sharedata.service';
import { AnnualTrueUp } from '../models/at.model';
import { AnnualTrueUpReconComponent } from "../reconcile/at-recon.component";

declare var jQuery: any;

@Component({
    selector: '[at-marketshare]',
    templateUrl: './at-marketshare.template.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./at-marketshare.style.scss']
})

export class AnnualTrueUpMarketShareComponent implements OnInit, OnDestroy {

    @Input() fiscalYear;
    @Input() alerts;
    @Input() ingestAlert;
    
    @Output() processComplete: EventEmitter<any> = new EventEmitter();
    @Output() onSubmitted: EventEmitter<any> = new EventEmitter();
    @ViewChild(AnnualTrueUpReconComponent) reconComponent: AnnualTrueUpReconComponent;

    busyGen: Subscription;
    busySub: Subscription;
    busyReconLoad: Subscription;
    busyCountLoad:Subscription;
     

    // support docs
    busyload: Subscription;
    busyupload: Subscription;
    busyDownload: Subscription;
    sortBy = "versionNum";

    mktsharealerts: Array<Object> = [];
    submitalerts: Array<Object>;
    submitted: any;
    submittedDt: string;
    disableSubmit: boolean = false;

    mktsharedata: any;
    versionnumbers: any;
    selVersion: string;

    shareData: ShareDataService;

    // supporting documents
    docs: any[];

    assessmentId: number;
    

    showSummary: boolean = false;
    showHistory: boolean = false;
    showSubmit: boolean = false;
    showAddDocumentPopup: boolean = false;
    addresschangesonly: boolean = false;

    zeroVersionIndex: number;
    zeroVersionDate: string;
    zeroVersionDateNumber: Date;
    zeroVersionAuthor: string;

    selVersionDate: string;
    selVersionDateNumber: Date;
    selVersionAuthor: string;

    mktshareselectqtr: string;
    marketsharetype: string;
    data: any;
    cntAllDeltaReview:number;
    cntAcceptApprv:number;
    in1: string; 
    in2: string; 
    in3: string;
    unApprovedCount: number;
    

    constructor(private marketShareService: AnnualTrueUpMarketShareService,
        private docService: AnnualTrueUpService,
        private _shareData: ShareDataService,
        private atcomparisonservice: AnnualTrueUpComparisonService) {

        this.submitalerts = [];
        this.zeroVersionIndex = -1;
        this.zeroVersionDate = "";
        this.zeroVersionAuthor = "";

        this.selVersionDate = "";
        this.selVersionAuthor = "";

        this.showSummary = false;
        this.showSubmit = false;
        this.showHistory = false;

        this.shareData = _shareData;
       

    }

    ngOnInit(): void {
       
    }
     
    loadGenerateMarketShare(selected: boolean = false){
         
        this.alerts = [];
        this.mktsharealerts =[];
        this.showSummary = false;
        this.fiscalYear = this.shareData.fiscalYear;
        this.submitted = this.shareData.submitted;
        this.submittedDt =  this.shareData.submittedDt;
        this.assessmentId = this.shareData.assessmentId;
        this.loadSupportingDocs(this.fiscalYear);
        this.getHistoricalSubmissionDetails();
    }
    
    
        
    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "alerts") {
                let chg = changes[propName];
                _.remove(this.mktsharealerts, item => (<any>item).type === 'danger');
                if (chg.currentValue) {
                    this.removeDeltaChangeError(this.mktsharealerts);
                    this.mktsharealerts.push({
                        type: 'danger',
                        msg: '<span class="alert-danger alert-text"><strong>ACTION REQUIRED:</strong>'+
                             '<ul style="padding-left:80px;margin-bottom: 0px;">' + chg.currentValue + '</ul></span>',
                    });
                }
                this.disableSubmit = !!chg.currentValue;
            }
            if (propName === "ingestAlert") {
                let chg = changes[propName];
                _.remove(this.mktsharealerts, item => (<any>item).type === 'warning'); //{ type: 'warning' });
                if (chg.currentValue) {
                    this.mktsharealerts.push({
                        type: 'warning',
                        msg: '<span class="alert-text"><strong>ACTION RECOMMENDED:</strong>' + chg.currentValue + '</span>'
                    });
                }
            }
        }
    }

    ngOnDestroy() {
        if (this.busyGen) {
            this.busyGen.unsubscribe();
        }

        if (this.busySub) {
            this.busySub.unsubscribe();
        }

        if (this.busyDownload) {
            this.busyDownload.unsubscribe();
        }
        if (this.busyCountLoad) {
            this.busyCountLoad.unsubscribe();
        }
        if (this.busyReconLoad) {
            this.busyReconLoad.unsubscribe();
        }
        if (this.busyload) {
            this.busyload.unsubscribe();
        }
        if (this.busyupload) {
            this.busyupload.unsubscribe();
        }
    }

    getVersionDateByIndex(index: number) {
        if (index !== -1) {
            if (this.mktsharedata.versions[index]) {
                if (this.mktsharedata.versions[index].createdDt) {
                    return this.mktsharedata.versions[index].createdDt;
                } else {
                    return "";
                }
            }
        }
        return "";
    }

    getVersionDateByVersionNumber(versionNum: number) {
        let index = _.findIndex(this.mktsharedata.versions, function (o) { return (<any>o).versionNum === Number(versionNum); });
        if (index !== -1) {
            if (this.mktsharedata.versions[index]) {
                if (this.mktsharedata.versions[index].createdDt) {
                    return this.mktsharedata.versions[index].createdDt;
                } else {
                    return "";
                }
            }
        }
        return "";
    }

    getAuthorByVersionNumber(versionNum: number) {
        let index = _.findIndex(this.mktsharedata.versions, function (o) { return (<any>o).versionNum === Number(versionNum); });
        if (index !== -1) {
            if (this.mktsharedata.versions[index]) {
                if (this.mktsharedata.versions[index].createdBy) {
                    return this.mktsharedata.versions[index].createdBy;
                } else {
                    return "";
                }
            }
        }
        return "";
    }

    getTobaccoExport(versionNum: number, curqtr: number) {
        if (this.mktsharedata.assessments[curqtr]) {
            let index = _.findIndex(this.mktsharedata.assessments[curqtr].versions, function (o) { return (<any>o).versionNum === Number(versionNum); });
            if (index !== -1) {
                let keys = _.keys(this.mktsharedata.assessments[curqtr].versions[index].exports);
                return keys;
            }
        }
        return null;
    }


    downloadExport(tobaccoClass: string, versionNum: number, curqtr: number) {
        if (this.mktsharedata.assessments[curqtr]) {
            let index = _.findIndex(this.mktsharedata.assessments[curqtr].versions, function (o) { return (<any>o).versionNum === Number(versionNum); });
            if (index !== -1) {
                if (this.mktsharedata.assessments[curqtr].versions[index]) {
                    if (this.mktsharedata.assessments[curqtr].versions[index].exports[tobaccoClass]) {
                        if (this.mktsharedata.assessments[curqtr].versions[index].exports[tobaccoClass][0]) {
                            this.download(this.mktsharedata.assessments[curqtr].versions[index].exports[tobaccoClass][0].csv,
                                this.mktsharedata.assessments[curqtr].versions[index].exports[tobaccoClass][0].title, 'csv/text');
                        }
                    }
                }
            }
        }
    }

    downloadAllExports(versionNum: number) {
        let zip: any = new JSZip();
        for (let curqtr = 0; curqtr < 4; curqtr++) {
            if (this.mktsharedata.assessments[curqtr]) {
                let index = _.findIndex(this.mktsharedata.assessments[curqtr].versions, function (o) { return (<any>o).versionNum === Number(versionNum); });
                if (index !== -1) {
                    if (this.mktsharedata.assessments[curqtr].versions[index]) {
                        for (let tc of this.getTobaccoExport(versionNum, curqtr)) {
                            if (this.mktsharedata.assessments[curqtr].versions[index].exports[tc]) {
                                if (this.mktsharedata.assessments[curqtr].versions[index].exports[tc][0]) {
                                    zip.file(this.mktsharedata.assessments[curqtr].versions[index].exports[tc][0].title,
                                        this.mktsharedata.assessments[curqtr].versions[index].exports[tc][0].csv);
                                }
                            }
                        }
                    }
                }
            }
        }
        var self = this;
        let zipFileName = "FY_" + this.fiscalYear.toString() + "_MSExportAll_" + versionNum.toString() + ".zip";
        zip.generateAsync({ type: "base64" }).then(function (base64) {
            self.download(atob(base64), zipFileName, "data:application/zip;base64,");
        });
    }

    getHistoricalSubmissionDetails() {

        // since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
       
        this.busyload = this.marketShareService.getMarketShare(this.fiscalYear).subscribe(data => {
            this.mktsharedata = data;
            // this.assessmentId = this.mktsharedata.assessmentId;
            // find the zero version index from the return object array
            this.zeroVersionIndex = _.findIndex(this.mktsharedata.assessments[0].versions, function (o) { return (<any>o).versionNum === 0; });
            this.zeroVersionDate = this.getVersionDateByIndex(this.zeroVersionIndex);
            this.zeroVersionDateNumber = new Date(this.zeroVersionDate);
            this.zeroVersionAuthor = this.getAuthorByVersionNumber(0);

            // retrieve version numbers and drop version 0
            this.versionnumbers = _.pull(_.map(this.mktsharedata.assessments[0].versions, 'versionNum'), 0);
            if (_.size(this.versionnumbers) > 0) {
                this.showHistory = true;
                this.selVersion = _.first(this.versionnumbers);
                this.selVersionDate = this.getVersionDateByVersionNumber(Number(this.selVersion));
                this.selVersionDateNumber = new Date(this.selVersionDate);
                this.selVersionAuthor = this.getAuthorByVersionNumber(Number(this.selVersion));
            }
            if (this.mktsharedata.approvalMessage) {
                this.mktsharealerts.push({
                    type: 'warning',
                    msg: '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + this.mktsharedata.approvalMessage + '</span>'
                });
            } else {
                //this.mktsharealerts = [];
            }
        }, error => {
            this.zeroVersionDate = "";
            this.zeroVersionAuthor = "";
            this.showSummary = false;
            //this.mktsharealerts = [];
            // tslint:disable-next-line:max-line-length
            this.mktsharealerts.push({ type: 'warning', msg: '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + error + '</span>' });
        });
    }

    generateMaketShare() {

        // since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
        if (this.busyGen) {
            this.busyGen.unsubscribe();
        }

        // this.setSubmissionFlag(this.mktshareselectqtr);
        
        this.busyGen = this.marketShareService.generateMarketShare(this.fiscalYear).subscribe(data => {
            this.mktsharedata = data;
            // find the zero version index from the return object array
            this.zeroVersionIndex = _.findIndex(this.mktsharedata.versions, function (o) { return (<any>o).versionNum === 0; });
            this.zeroVersionDate = this.getVersionDateByIndex(this.zeroVersionIndex);
            this.zeroVersionDateNumber = new Date(this.zeroVersionDate);
            this.zeroVersionAuthor = this.getAuthorByVersionNumber(0);

            // retrieve version numbers and drop version 0
            this.versionnumbers = _.pull(_.map(this.mktsharedata.versions, 'versionNum'), 0);
            if (this.zeroVersionIndex !== -1) {
                this.showSummary = true;
            }
            if (_.size(this.versionnumbers) > 0) {
                this.showHistory = true;
                this.selVersion = _.first(this.versionnumbers);
                this.selVersionDate = this.getVersionDateByVersionNumber(Number(this.selVersion));
                this.selVersionDateNumber = new Date(this.selVersionDate);
                this.selVersionAuthor = this.getAuthorByVersionNumber(Number(this.selVersion));
            }
            if (this.mktsharedata.approvalMessage) {
                let submitmsg = " Report cannot be submitted until all Monthly Reports are Approved. Please re-generate the Market Share Data after " +
                    "approving all monthly reports.";
                this.mktsharealerts = [];
                this.mktsharealerts.push({
                    type: 'warning',
                    msg: '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + this.mktsharedata.approvalMessage + '</span>'
                });
                this.submitalerts = [];
                this.submitalerts.push({ type: 'warning', msg: submitmsg });
                this.showSubmit = false;
            } else {
                //this.mktsharealerts = [];
                this.submitalerts = [];
                this.showSubmit = true;
            }
        }, error => {
            this.zeroVersionDate = "";
            this.zeroVersionAuthor = "";
            this.selVersionDate = "";
            this.selVersionAuthor = "";
            this.showSummary = false;

            this.mktsharealerts = [];
            // tslint:disable-next-line:max-line-length
            this.mktsharealerts.push({ type: 'warning', msg: '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + error + '</span>' });
        });
    }

    isMktShareDataAvailable(tobaccoClass: string, versionNum: number) {
        // let index = _.findIndex(this.mktsharedata.versions, function (o) { return o.versionNum === Number(versionNum); });
        // if (index !== -1) {
        //     if (this.mktsharedata.versions[index]) {
        //         if (this.mktsharedata.versions[index].exports[tobaccoClass])
        //             return true;
        //     }
        // }
        return false;
    }

    loadSupportingDocs(fiscalYr: number) {
        let that = this;
        this.busyload = this.docService.getSupportingDocs(fiscalYr).subscribe(data => {
            that.docs = data;
        });
    }

    setSelectedDocument() {
        this.showAddDocumentPopup = true;
    }

    documentsGridRefresh(doc: any) {
        if (doc) {
            let document = new SubmissionDocument();
            document.fiscalYr = this.fiscalYear;
            document.documentNumber = doc.documentNumber;
            document.filename = doc.filename;
            document.assessmentPdf = doc.assessmentPdf;
            document.dateUploaded = doc.dateUploaded;
            document.description = doc.description;
            document.versionNum = doc.versionNum;
            this.busyupload = this.docService.saveDoc(document).subscribe(data => {
                if (data) {
                    this.loadSupportingDocs(this.fiscalYear);
                }
            });
        }
    }

    submitMarketShare() {// since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
        if (this.busySub) {
            this.busySub.unsubscribe();
        }

        this.busySub = this.marketShareService.submitMarketShare(this.fiscalYear).subscribe(data => {

            this.removeDeltaChangeError(this.mktsharealerts);

            this.mktsharedata = data;
            this.submitalerts = [];
            this.showSummary = false;

            // find the zero version index from the return object array
            this.zeroVersionIndex = _.findIndex(this.mktsharedata.versions, o => (<any>o).versionNum === 0);
            this.zeroVersionDate = this.getVersionDateByIndex(this.zeroVersionIndex);
            this.zeroVersionDateNumber = new Date(this.zeroVersionDate);
            this.zeroVersionAuthor = this.getAuthorByVersionNumber(0);

            // retrieve version numbers and drop version 0
            this.versionnumbers = _.pull(_.map(this.mktsharedata.versions, 'versionNum'), 0);
            if (_.size(this.versionnumbers) > 0) {
                this.showHistory = true;
                this.selVersion = _.first(this.versionnumbers);
                this.selVersionDate = this.getVersionDateByVersionNumber(Number(this.selVersion));
                this.selVersionDateNumber = new Date(this.selVersionDate);
                this.selVersionAuthor = this.getAuthorByVersionNumber(Number(this.selVersion));
            }
            this.processComplete.emit(this.mktsharedata);
            this.onSubmitted.emit(this.mktsharedata);
        }, error => {
            if (error.error) {
                if (error.error.ex === "DeltaChangeError") {
                    this.removeDeltaChangeError(this.mktsharealerts);
                    let deltaChangeMssg = "<li>"+error.error.data+" deltas need to be accepted or amended under Compare in order to Submit the Market Share</li>";
                    this.mktsharealerts.push({
                        type: 'danger',
                        msg: '<span class="alert-danger alert-text"><strong>ACTION REQUIRED:</strong><ul>'+deltaChangeMssg +'</ul></span>',
                        name: error.message
                    }); 

                    this.disableSubmit = true;
                }
                 return;
            }
            this.submitalerts = [];
            this.zeroVersionAuthor = "";
            this.selVersionAuthor = "";
            this.submitalerts.push({ type: 'warning', msg: '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + error + '</span>' });
        });
    }

    removeDeltaChangeError(alertArry: any []){
        alertArry.forEach(function (item, index, array) {
                if(item.name && item.name === "DeltaChangeError")
                     alertArry.splice(index, 1);
        });
    }


    onchangeVersion(versionNum: number) {
        this.selVersionDate = this.getVersionDateByVersionNumber(versionNum);
        this.selVersionDateNumber = new Date(this.selVersionDate);
        this.selVersionAuthor = this.getAuthorByVersionNumber(versionNum);
    }


    downloadDoc(trueupSubDocId: number) {
        this.busyDownload = this.docService.downloadDocument(trueupSubDocId).subscribe(data => {
            if (data) {
                this.download(atob((<any>data).assessmentPdf), (<any>data).filename, "application/json");
            }
        });
    }

    download(data: any, strFileName: any, strMimeType: any) {

        let self: any = window, // this script is only for browsers anyway...
            defaultMime = 'application/octet-stream', // this default mime also triggers iframe downloads
            mimeType = strMimeType || defaultMime,
            payload = data,
            url = !strFileName && !strMimeType && payload,
            anchor = document.createElement('a'),
            toString = function (a) { return String(a); },
            myBlob = (self.Blob || self.MozBlob || self.WebKitBlob || toString),
            fileName = strFileName || 'download',
            blob,
            reader;
        myBlob = myBlob.call ? myBlob.bind(self) : Blob;

        if (String(this) === 'true') { // reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
            payload = [payload, mimeType];
            mimeType = payload[0];
            payload = payload[1];
        }

        // go ahead and download dataURLs right away
        if (/^data[\:\;]/.test(payload)) {
            if (payload.length > (1024 * 1024 * 1.999) && myBlob !== toString) {
                payload = dataUrlToBlob(payload);
                mimeType = payload.type || defaultMime;
            } else {
                return navigator.msSaveBlob ?  // IE10 can't do a[download], only Blobs:
                    navigator.msSaveBlob(dataUrlToBlob(payload), fileName) :
                    saver(payload, false); // everyone else can save dataURLs un-processed
            }
        } else {// not data url, is it a string with special needs?
            if (/([\x80-\xff])/.test(payload)) {
                let i = 0, tempUiArr = new Uint8Array(payload.length), mx = tempUiArr.length;
                for (i; i < mx; ++i) tempUiArr[i] = payload.charCodeAt(i);
                payload = new myBlob([tempUiArr], { type: mimeType });
            }
        }
        blob = payload instanceof myBlob ?
            payload :
            new myBlob([payload], { type: mimeType });


        function dataUrlToBlob(strUrl) {
            let parts = strUrl.split(/[:;,]/),
                type = parts[1],
                decoder = parts[2] === 'base64' ? atob : decodeURIComponent,
                binData = decoder(parts.pop()),
                mx = binData.length,
                i = 0,
                uiArr = new Uint8Array(mx);

            for (i; i < mx; ++i) uiArr[i] = binData.charCodeAt(i);

            return new myBlob([uiArr], { type: type });
        }

        function saver(url: any, winMode: any) {

            if ('download' in anchor) { // html5 A[download]
                anchor.href = url;
                anchor.setAttribute('download', fileName);
                anchor.className = 'download-js-link';
                anchor.innerHTML = 'downloading...';
                anchor.style.display = 'none';
                document.body.appendChild(anchor);
                setTimeout(function () {
                    anchor.click();
                    document.body.removeChild(anchor);
                    if (winMode === true) { setTimeout(function () { self.URL.revokeObjectURL(anchor.href); }, 250); }
                }, 66);
                return true;
            }

            // handle non-a[download] safari as best we can:
            if (/(Version)\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//.test(navigator.userAgent)) {
                if (/^data:/.test(url)) url = 'data:' + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
                if (!window.open(url)) { // popup blocked, offer direct download:
                    if (confirm('Displaying New Document\n\nUse Save As... to download, then click back to return to this page.')) {
                        location.href = url;
                    }
                }
                return true;
            }

            // do iframe dataURL download (old ch+FF):
            let f = document.createElement('iframe');
            document.body.appendChild(f);

            if (!winMode && /^data:/.test(url)) { // force a mime that will download:
                url = 'data:' + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
            }
            f.src = url;
            setTimeout(function () { document.body.removeChild(f); }, 333);

        } // end saver

        if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or URL)
            return navigator.msSaveBlob(blob, fileName);
        }

        if (self.URL) { // simple fast and modern way using Blob and URL:
            saver(self.URL.createObjectURL(blob), true);
        } else {
            // handle non-Blob()+non-URL browsers:
            if (typeof blob === 'string' || blob.constructor === toString) {
                try {
                    return saver('data:' + mimeType + ';base64,' + self.btoa(blob), false);
                } catch (y) {
                    return saver('data:' + mimeType + ',' + encodeURIComponent(blob), false);
                }
            }

            // Blob but not URL support:
            reader = new FileReader();
            reader.onload = function (e) {
                saver(this.result, false);
            };
            reader.readAsDataURL(blob);
        }
        return true;
    };

}

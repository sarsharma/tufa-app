export class AcceptanceFlagState {
    static readonly ACCEPT_FDA = "FDAaccepted";
    static readonly ACCEPT_FDA_CONTINUE = "FDAacceptedContinue";

    static readonly ACCEPT_INGESTED = "Ingestedaccepted";
    static readonly ACCEPT_INGESTED_CONTINUE = "IngestedacceptedContinue";
    
    static readonly IN_PROGRESS = "InProgress";

    static readonly AMEND = "Amended";
    static readonly NO_ACTION = "noAction";

    static readonly DELTA_CHANGE = "DeltaChange";
    static readonly DELTA_CHANGE_ZERO = "DeltaChngZ";
    static readonly DELTA_CHANGE_EXCLUDE = "ExcludeChange"

    static readonly ASSOCIATED = "Associated";
    static readonly EXCLUDED = "Excluded";
    static readonly INPROGRESS = "In Progress";
    static readonly MSREADYTUFA = "MSReadyTufa";
    static readonly MSREADYINGESTED = "MSReadyIngested";
    static readonly MIXEDACTION = "MixedAction";
    static readonly DELTA_CHANGE_TRUE_ZERO = "DeltaChangeTrueZero";
    static readonly PERMITEXCLUDED = "Excluded X";

}
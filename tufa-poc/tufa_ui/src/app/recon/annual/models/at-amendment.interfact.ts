export interface Amendment {
    acceptanceFlag: string;
    tobaccoClassId: number;
    fiscalYr: number;
    qtr: number;
    inProgressFlag: string;
}
export class CBPMetrics {
    fiscalYr: string;
    summaryFileEntrySummaryNoCount: number;
    detailFileEntrySummaryNoCount: number;
    matchedEntrySummaryNoCount: number;
    unmatchedEntrySummayNoCount: number;
    uniqueSummaryFileEntrySummaryNo: number;
    uniqueDetailFileEntrySummaryNo: number;
    detailFileTotalTaxAmount: number;
    mixedEntrySummaryNoCount: number;
    accepted: string;

    createdDt: Date;
    createdBy: string;
    modifiedDt: Date;
    modifiedBy: string;
}
export class CBPUpload{
    importerEIN: string;
    entryNo: string;
    lineNo: number;
    entrySummDate: string;
    entryDate: string;
    uom1: string;
    htsCd: string;
    qty1: number;
    estimatedTax: number;
    entryType: string;
    fiscalYr: number;

}
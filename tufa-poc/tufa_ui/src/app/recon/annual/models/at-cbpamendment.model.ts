import { Amendment } from "./at-amendment.interfact";

export class CBPAmendment implements Amendment {

    public cbpAmendmentId: number;
    public cbpCompanyId: number;
    public tobaccoClassId: number;
    public fiscalYr: number;
    public qtr: number;
    public amendedTotalTax: any;
    public amendedTotalVolume: any;
    public acceptanceFlag: string;
    public inProgressFlag: string;

    //Flag addded for Importer ingested review functionaly accept ingest
    public reviewFlag: string;

    public comments: string;
    public tobaccoType: string;
    public inclAllQtrs: boolean;
    public userComments: CmpDetailUserComment[];
    public excludedPermits: any[];
    public amendmentPresent: boolean;
    public mixedActionQuarterDetails: any;
    
    constructor() {
    }

}

export class CmpDetailUserComment {
    commentSeq: number;
    cbpAmendmentId: number;
    userComment: string;
    author: string;
    commentDate: string;
    modifiedBy: string;
    modifiedDt: string;
    qtr: number;
    commentQtrs: string;
    attachmentsCount: number = 0;
}
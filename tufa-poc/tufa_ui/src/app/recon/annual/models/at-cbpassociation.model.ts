export class CBPAssociationModel {
    //Info to update to
    parentCmpyEIN: string;
    parentCmpyName: string;

    //Current Info
    currentCmpyEIN: string;
    currentCmpyName: string;

    //Indentifying Info
    tobaccoClass: string;
    fiscalQtr: string;
    fiscalYr: string;
    delta: string;

    permitType: string;
    deltaComment: string;
    deltaActionSpan: string;
}
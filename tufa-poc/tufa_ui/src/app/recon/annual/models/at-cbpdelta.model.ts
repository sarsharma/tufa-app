export class CBPDeltaModel {

    //Current Info
    EIN: string;
    CmpyName: string;

    //Indentifying Info
    tobaccoClass: string;
    fiscalQtr: string;
    fiscalYr: string;
}

import {AnnualTrueUpCBPImpModel} from './at-cbpimp.model'
import {TobaccoClass} from '../../../model/tobacco.class.model'

export class AnnualTrueUpCBPEntryModel {
    
    //All AnnualTrueUpCBPImpModel properties to be moved to cbpImporter property
    cbpEntryId:number;
    cbpImporterId:number;
    tobaccoType: string;
    tobaccoClassId: string;
    totalTaxes: number;
    importerNm:string;
    consigneeNm: string;
    fiscalYear:string;
    excludeEntryLines:boolean;
    existsInTufaFlag:boolean;
    consignSSNFlag:boolean;
    useEntityInd: string;
    entrySummDt:string;
    entryDt: string;
    importDt:string;
    cbpImporter: AnnualTrueUpCBPImpModel;
    tobaccoClass : TobaccoClass;
    estimatedTax:number;
    companies : string [];
    associationTypeCd: string;
    providedAnswer: boolean;
    htsCd: string;

    constructor (){
    }
}
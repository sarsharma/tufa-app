export class AnnualTrueupCmpyAssocDeltaModel {

    ein: string;
    tufaLegalNm: string;
    fiscalYr: string;
    associationTypeCd: string;
    importerNm: string;
    impInTufa: string;
    consgInTufa: string;
    tufaQtr: string;
    ingestedQtr: string;
    taxesCigarsTufa: number;
    taxesCigsTufa: number;
    taxesChewTufa: number;
    taxesSnuffTufa: number;
    taxesPipeTufa: number;
    taxesRYOTufa: number;
    deltaCigars: number;
    deltaCigarettes: number;
    deltaChew: number;
    deltaSnuff: number;
    deltaPipe: number;
    deltaRYO: number;

}

export class AnnualTrueupCmpyAssocModel {

    ein: string;
    tufaLegalNm: string;
    fiscalYr: string;
    associationTypeCd: string;
    cbpImporterId: number;
    importerNm: string;
    impInTufa: string;
    existsInTufaFlag: string;
    consgInTufa: string;
    providedAnswer: boolean;
    hasUnknownTT: boolean;
    importerEIN: string;
    consigneeEIN: string;
    consigneeNm: string;
    consigneeDeltas: AnnualTrueupCmpyAssocDeltaModel[];
    importerDeltas: AnnualTrueupCmpyAssocDeltaModel[];

}


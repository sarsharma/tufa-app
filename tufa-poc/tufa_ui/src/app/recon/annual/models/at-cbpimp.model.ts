export class AnnualTrueUpCBPImpModel {

    cbpImporterId:number;
    importerNm:string;
    consigneeNm:string;
    otherTobaccoTypesReported: string[];
    associationTypeCd:string;
    totalTaxes: number;
    includeFlag: string;
}
export class AnnualTrueUpCBPLineEntryUpdate {
    cbEntryId:number;
    reallocateFlag: string;    
    excludeFlag: string;
    assignedMonth: string;
    assignedQuarter: number;    
    assignedYear: number;
    missingFlag: string;
    resetMissingFlag: string;
}
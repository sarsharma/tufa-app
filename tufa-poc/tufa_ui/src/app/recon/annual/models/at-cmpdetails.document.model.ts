export class Document {
    documentId: number;
    cbpCommentSeq: number;
    ttbCommentSeq: number;
    allDeltaCommentSeq: number;
    docFileNm: string;
    docDesc: string;
    reportPdf: string;
    docStatusCd: string;
    createdBy: string;
    createdDt: Date;
    modifiedBy: string;
    modifiedDt: Date;
}

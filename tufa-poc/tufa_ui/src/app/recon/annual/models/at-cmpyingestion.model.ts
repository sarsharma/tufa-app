export class AnnualTrueUpCmpyIngestionModel{
     companyName : string;
	 einNo : string;
     includeFlag : string;
	 totalTaxes : number;
     providedAnswer: boolean;
     otherTobaccoTypesReported : string [];
     associationType: string;
     fiscalYr:string;
}
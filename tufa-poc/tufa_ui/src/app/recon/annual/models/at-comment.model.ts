export class allDeltaStatus {
    cmpAllDeltaId: number;
    ein: string;
    fiscalYr: string;
    fiscalQtr: string;
    tobaccoClassName: string;
    permitType: string;
    deltaStatus: string;
    delta: string;
    tobaccoSubType1:string;
    tobaccoSubType1Pounds:number
    tobaccoSubType2: string;
    tobaccoSubType2Amndtx: number
    tobaccoSubType2Pounds: number
    deltaType :string;
    fileType:string;
    public userComments: allDeltaCommentModel[];
}

export class allDeltaCommentModel {
    commentSeq: number;
    userComment: string;
    author: string;
    commentDate: string;
    modifiedBy: string;
    modifiedDt: string;
    resolveComment: boolean;
    qtr: number;
    commentQtrs: string;
    matchCommSeq: number;
    cmpAllDeltaId: number;
    public deltaStatuses: allDeltaStatus[];
    attachmentsCount: number = 0;
}
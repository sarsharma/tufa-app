/** 
 * This is to be replaced by at-compare-details.model.ts 
 * */
export class ATCompareAttachmentDocument{
    documentId: number;
    docDesc: string;
    formTypes: string;
    createdDt: Date;
}
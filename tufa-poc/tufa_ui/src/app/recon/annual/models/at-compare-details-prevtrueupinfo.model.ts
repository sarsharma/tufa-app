export class PreviousTrueupInfo {
    fiscalYear: string;
    quarter: string;
    tobaccoClassName: string;
    status: string;
    comments: string[] = [];
 
}

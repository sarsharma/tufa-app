import { Form7501Model } from "../../../permit/models/form-7501.model";
import { ReallocateModel } from "./at-reallocate.model";

/**
 * Replaces:
 *  at-compare-importerperiod.model.ts
 *  at-compare-attachment.model.ts
 */
export class Details {
    tufaDetails: TufaDetail[] = [];
    ingestedDetails: IngestedDetail[] = [];
    detailDeltas: DetailDelta[] = [];
    tufaTotal: string;
    detailTotal: string;
    deltaTotal: string;
    dataType: string;
    ingestedRemovalQunatity: string;
    mixedActionDetailsMap: Map<string, string>;
    tufaRemovalQunatity: string;
    mixedActionTax: string;
    mixedActionQuantityRemoved: string;
}

export class TufaDetail {
    period: string;
    periodTotal: string;
    periodDetails: TufaPeriodDetail[];
    period7501Details: Form7501Model[];
    removalQuantity: string;
}

export class IngestedDetail {
    period: string;
    periodTotal: string;
    periodEntryDateCategory: string;
    periodEntryItemCount: number;
    periodDetails: IngestedPeriodDetail[];
    removalQuantity: string;
}

export class DetailDelta {
    period: string;
    periodDelta: string;
}

export class TufaPeriodDetail {
    status: string;
    month: string;
    quarter: number;
    permit: string;
    periodId: number;
    permitId: number;
    taxAmount: string;
    attachments: Attachment[];
    lineCount7501: number;
}

export class IngestedPeriodDetail {
    entryNum: string;
    entrysummaryDate: Date;
    entryDate: Date;
    lineNum: number;
    taxAmount: string;
    qtyRemoved: number;
    month: string;
    quarter: number;
    entryId: number;
    excludedFlag: string;
    reallocatedFlag: string;
    entryTypeCd: number;
    infosortOrder: string;
    secondaryDateFlag: string;
    missingFlag: string;

    public toReallocateModel(fiscalYear?: number): ReallocateModel {
        let reallocate = new ReallocateModel();

        reallocate.entryId = this.entryId;
        reallocate.date = (this.entrysummaryDate && this.entrysummaryDate.toString().length > 0) ? this.entrysummaryDate.toString() : this.entryDate.toString();
        reallocate.entryDate = this.entryDate ? this.entryDate.toString() : '';
        reallocate.entryNum = this.entryNum;
        reallocate.lineNum = this.lineNum.toString();
        reallocate.qty = this.qtyRemoved;
        reallocate.tax = this.taxAmount;
        reallocate.year = fiscalYear;
        reallocate.month = this.month;
        reallocate.excludedFlag = this.excludedFlag;
        reallocate.reallocatedFlag = this.reallocatedFlag;
        reallocate.entryTypeCd = this.entryTypeCd;
        reallocate.secondaryDateFlag = this.secondaryDateFlag;
        reallocate.missingFlag = this.missingFlag;
        return reallocate;
    }
}

export class Attachment {
    documentId: number;
    docDesc: string;
    formTypes: string;
    reportPdf: any;
    createdDt: number;
}
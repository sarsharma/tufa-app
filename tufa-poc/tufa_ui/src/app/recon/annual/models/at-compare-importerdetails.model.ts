import { AnnualTrueUpImporterPeriod } from "./../../../recon/annual/models/at-compare-importerperiod.model";
/** 
 * This is to be replaced by at-compare-details.model.ts 
 * */
export class AnnualTrueUpImporterDetail {
    period: String;
    periodTotal: Number;
    periodEntryDateCategory: string;
    periodEntryItemCount: number;
    periodDetails: AnnualTrueUpImporterPeriod[];
}

import { ATCompareAttachmentDocument } from "./at-compare-attachment.model";
/** 
 * This is to be replaced by at-compare-details.model.ts 
 * */
export class AnnualTrueUpImporterPeriod {
    status: String;
    month: String;
    permit: String;
    periodId: Number;
    permitId: Number;
    taxAmount: Number;
    attachments: ATCompareAttachmentDocument[];
}

import { AnnualTrueUpManufacturerPermit } from "./../../../recon/annual/models/at-compare-manufacturerpermit.model";

export class ManufactureQuarterDetails {
    quarter: number;
    tufaTobaccoClass1Total: string = 'NA';
    tufaTobaccoClass2Total: string = 'NA';
    tufaTotal: string = 'NA';
    ingestedTotal: string = 'NA';
    delta: string = 'NA';
    permits: AnnualTrueUpManufacturerDetail[];
    dataType: string;
    tufaDetails: TufaDetail[] = [];
    ingestedDetails: IngestedDetail[] = [];
    mixedActionDetailsMap: Map<string, string>;
    mixedActionQuantityRemoved: number;
    mixedActionTax: number;
}

export class AnnualTrueUpManufacturerDetail {
    quarter: number;
    permit: string;
    permitId: number;
    periodTotal: string;
    periodTotal2: string;
    periodTotalCombined: string;
    permitDetails: AnnualTrueUpManufacturerPermit[];
    ingestedTax: string;
    delta: string;
}

export class TufaDetail {
    periodId: string;
    month: string;
    periodTotal: number;
    removalQuantity: number;
	removalQtyTobaccoClass1: number;
	taxAmountTobaccoClass1: number;
	removalQtyTobaccoClass2: number;
	taxAmountTobaccoClass2: number;
}

export class IngestedDetail {
    periodId: string;
    periodTotalIngested: number;
    removalQuantityIngested: number;
    month: string;
    removalQtyTobaccoClass1Ingested: number;
    taxAmountTobaccoClass1Ingested: number;
    removalQtyTobaccoClass2Ingested: number;
    taxAmountTobaccoClass2Ingested: number;
}


export class AnnualTrueUpManufacturerPermit {
    status: String;
    month: String;
    periodId: Number;
    taxAmount: Number;
    taxAmount2: Number;
    taxTotalAmount: Number;
}

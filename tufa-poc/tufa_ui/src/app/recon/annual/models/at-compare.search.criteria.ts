import { SelectTobaccoType } from "../../../permit/models/sel-tobacco-type.model";

export class CompareSearchCriteria {
    companyFilter: String;
    einFilter: String;
    permitNumFilter: String;
    tobaccoClassFilter: String;
    quarterFilter: String;
    deltaTaxFilter: String;
    permitTypeFilter: String;
    statusTypeFilter: string[] = [];
    dataTypeFilter: string;
    selTobaccoTypes: SelectTobaccoType;
    originalNameFilter: string;
    allDeltaStatusTypeFilter: string[] = [];

    grid: string = 'MATCHED';

    constructor() {
        this.selTobaccoTypes = new SelectTobaccoType();
    }

    public clear() {
        this.einFilter = "";
        this.companyFilter = "";
        this.deltaTaxFilter = "";
        this.permitTypeFilter = "";
        this.quarterFilter = "";
        this.tobaccoClassFilter = "";
        this.originalNameFilter = "";
        this.permitNumFilter = "";
        this.statusTypeFilter = null;
        this.allDeltaStatusTypeFilter = null;
        this.dataTypeFilter = "";
        this.selTobaccoTypes = new SelectTobaccoType();
    }
}
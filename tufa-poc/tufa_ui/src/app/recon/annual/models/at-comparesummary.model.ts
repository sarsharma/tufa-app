export class AnnualTrueUpComparisonSummary{
    legalName: string;
    tobaccoClass: string;
    quarter: string;
    totalDeltaTax: number;
    abstotalDeltaTax: number;
    permitType: string;
    companyId: number;
    status: string;
}

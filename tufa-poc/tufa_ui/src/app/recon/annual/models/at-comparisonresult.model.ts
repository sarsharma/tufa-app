export class TrueUpComparisonResult {
    legalName: string;
	ein: string;
	tobaccoClass: string;
	quarter: string;
	totalDeltaTax: string;
	abstotalDeltaTax: string;
	permitType: string;
	fileType:string;
	companyId: string;
	status: string;
	allDeltaStatus: string;
	allDeltaCurrentStatus: string;
    dataType: string;
    originalName: string;
    originalEIN: string;
    fiscalYr: string;
    ttlCntDeltaChng: string;
	ttlCntDeltaChngZero: string;
    ttlCntAccptApprv: string;
	ttlCntInProgress: string;
	ttlDeltaCntInReview: string;
	ttlDeltaCntInProgress: string;
	deltaexcisetaxFlag: string;
	tobaccoSubTypeTxVol: any;
	affectedDeltaCnt:number;
	public userComments: allDeltaUserComment[];
    affectDeltaFlag: boolean = true;
}

export class allDeltaUserComment {
    commentSeq: number;
    userComment: string;
    author: string;
    commentDate: Date;
    modifiedBy: string;
    modifiedDt: string;
    qtr: number;
}
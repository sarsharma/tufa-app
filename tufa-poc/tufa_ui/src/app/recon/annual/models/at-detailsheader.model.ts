import { TrueUpComparisonResult } from "./at-comparisonresult.model";

export class AnnualTrueUpComparisonDetailsHeader {
    compareResult: TrueUpComparisonResult;
    companyName: string;
    tobaccoClass: string;
    fiscalYear: string;
    permitType: string;
    companyId: number;
    quarter: number;
    ein: string;
}

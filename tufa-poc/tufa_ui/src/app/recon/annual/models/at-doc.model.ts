export class AnnualTrueUpDocModel {
    docDesc: string;
    trueUpFilename: string;
    fiscalYear: number;
    trueupDocument: File;
    errorCount: number;

    createdDt: string;
    createdBy: string;
    modifiedDt: string;
    modifiedBy: string;
}
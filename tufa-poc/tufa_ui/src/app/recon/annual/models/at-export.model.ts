/**
 * Added to satisfy TypeScript
 * 
 * If more fields are needed for different exports then 
 * create/expand upon another class that Extends this class
 * 
 * IE: class CigarExport extends ExportData
 */
export class ExportData {
    // public tobaccoType: string;
    /**
     * Title of export
     */
    public title: string;
    /**
     * Contens of export
     * CSV: Comma-separted values
     */
    public csv: string;

    constructor(){}

}

import { AnnualTrueUp } from "./../../../recon/annual/models/at.model";

export class AnnualTrueUpFilter {
    statusFilters: string[] = [];
    fiscalYrFilters: string[] = [];
    results: AnnualTrueUp[];
}

export class AnnualTrueUpFilterData {
    private _reportStatus: any;
    private _reportYears: any;

    get reportStatus(): any {
        return this._reportStatus;
    }
    set reportStatus(value: any) {
        this._reportStatus = value;
    }

    get reportYears(): any {
        return this._reportYears;
    }

    set reportYears(value: any) {
        this._reportYears = value;
    }
 }
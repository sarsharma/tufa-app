export const enum AtIngestFileType {
    DOC_CBP = "CBP",
    DOC_CBP_SUM = "CBP Summary",
    DOC_CBP_DET = "CBP Details",
    DOC_TTB_TAX = "TTB Tax",
    DOC_TTB_VOL = "TTB Volume"
}
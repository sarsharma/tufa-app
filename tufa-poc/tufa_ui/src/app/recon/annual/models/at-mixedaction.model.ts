export class MixedAction{
    qtr: number;
    mixedActionDetailsMap: Map<string, string> = new Map();
    amendedTotalTax: Number;
    amendedTotalVolume : Number;
}

export class MixedActionManu{
    qtr: number;
    mixedActionDetailsMap: Map<string, string> = new Map();
    amendedTotalTax1: Number;
    amendedTotalVolume1 : Number;
    amendedTotalTax2: Number;
    amendedTotalVolume2 : Number;
}
export class MixedActionQuarterDetails {
    month: string;
    monthlyStatusFlag: string;
    acceptFDAFlag: boolean;
    acceptIngestedFlag: boolean;
    tufaTax: string;
    tufaVolume: string;
    ingestedTax: string;
    ingestedVolume: string;
}

export class MixedActionQuarterDetailsManufacturer {
    month: string;
    monthlyStatusFlag: string;
    acceptFDAFlag: boolean;
    acceptIngestedFlag: boolean;
    tufaTax1: number;
    tufaVolume1: number;
    tufaTax2: number;
    tufaVolume2: number;
    ingestedTax1: number;
    ingestedVolume1: number;
    ingestedTax2: number;
    ingestedVolume2: number;
    tufaTotalTax : number;
    tufaTotalVolume : number;
    ingestedTotalTax : number;
    ingestedTotalVolume : number;
}
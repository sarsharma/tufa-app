export class ReallocateModel {
    entryId: number;
    tax: string;
    qty: number;
    entryNum: string;
    lineNum: string;
    date: string;
    entryDate: string;
    year: number;
    quarter: number;
    month: string;
    excludedFlag: string;
    reallocatedFlag: string;
    entryTypeCd: number;
    secondaryDateFlag: string;
    missingFlag:string
}

import { AnnualTrueUp } from "./../../../recon/annual/models/at.model";

export class AnnualTrueUpReconFilter {
    statusFilters: string[] = [];
    companyFilter: String;
    permitFilter: String;
    monthFilter: String;    
    statusFilter: String;
    reportedTobaccoFilter: String;    
    results: AnnualTrueUp[];
}

export class AnnualTrueUpReconFilterData {
    private _reportStatus: any;
    private _companyFilter: String;
    private _permitFilter: String;
    private _monthFilter: String;
    private _statusFilter: String;
    private _reportedTobaccoFilter: String;  

    get reportStatus(): any {
        return this._reportStatus;
    }
    set reportStatus(value: any) {
        this._reportStatus = value;
    }

    get companyFilter(): any {
        return this._companyFilter;
    }
    set companyFilter(value: any) {
        this._companyFilter = value;
    }

    get permitFilter(): any {
        return this._permitFilter;
    }
    set permitFilter(value: any) {
        this._permitFilter = value;
    }

    get monthFilter(): any {
        return this._monthFilter;
    }
    set monthFilter(value: any) {
        this._monthFilter = value;
    }

    get statusFilter(): any {
        return this._statusFilter;
    }
    set statusFilter(value: any) {
        this._statusFilter = value;
    }
    get reportedTobaccoFilter(): any {
        return this._reportedTobaccoFilter;
    }
    set reportedTobaccoFilter(value: any) {
        this._reportedTobaccoFilter = value;
    }    

 }

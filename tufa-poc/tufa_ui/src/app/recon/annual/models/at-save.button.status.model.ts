export class AnnualTrueUpSaveButtonStatus {
        public isSaved: boolean;
        public isIncrmntCntr: boolean;
        public isDecrmntCntr: boolean;

        constructor(isSaved:boolean,increment: boolean,decrement: boolean){
                this.isSaved = isSaved;
                this.isIncrmntCntr = increment;
                this.isDecrmntCntr = decrement;
        }
}
export class SummarySet {
    summaryMonths: SummaryRecord[] = [];
    summaryTotal: SummaryRecord = new SummaryRecord();
    summaryQuarterTotals: SummaryRecord[] = [];
    tobaccoClass: string;
}

export class SummaryRecord {
    month: string;

    summaryValue: number;
    detailValue: number;
    tufaValue: number;

	summaryAndDetailDiff: number;
	tufaAndDetailDiff: number;
	tufaAndSummaryDiff: number;
}
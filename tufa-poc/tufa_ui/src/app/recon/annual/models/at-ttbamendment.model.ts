import { Amendment } from "./at-amendment.interfact";

export class TTBAmendment implements Amendment {

    public ttbAmendmentId: number;
    public ttbCompanyId: number;
    public tobaccoClassId: number;
    public fiscalYr: number;
    public qtr: number;
    public amendedTotalTax: any;
    public amendedTotalVolume: any;
    public acceptedIngestedTotalTax: any;
    public acceptanceFlag: string;
    public inProgressFlag: string;
    public comments: string;
    public tobaccoType: string;
    public inclAllQtrs: boolean;
    public userComments: TTBDetailUserComment[];
    public excludedPermits: any[];
    public amendmentPresent: boolean;
    public mixedActionQuarterDetails: any;
    //isAmendmentPresent
    constructor() {
    }
}
export class TTBDetailUserComment {
    commentSeq: number;
    ttbAmendmentId: number;
    userComment: string;
    author: string;
    commentDate: string;
    modifiedBy: string;
    modifiedDt: string;
    qtr: number;
    commentQtrs: string;
    attachmentsCount: number = 0;
}

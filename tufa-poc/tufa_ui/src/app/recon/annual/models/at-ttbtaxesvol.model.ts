export class TTBTaxesVol {
    qtr: string
    ein: string
    tobaccoClass: string
    pounds: number
    fiscalYr: string
    estimatedTax: number
    amendedTax: number
    tobaccoSubTypeAmndTx:any;
}
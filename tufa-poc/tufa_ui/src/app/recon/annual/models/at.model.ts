import { TrueUpVersion } from "./at-version.model";

export class AnnualTrueUp {
    fiscalYear: number;
    submittedDt: Date;
    submittedInd: string;
    versions: TrueUpVersion[];
}

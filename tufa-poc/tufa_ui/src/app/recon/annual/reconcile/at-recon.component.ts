import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs';
import { ShareDataService } from '../../../core/sharedata.service';
import { AnnualTrueUpReconFilter, AnnualTrueUpReconFilterData } from "./../../../recon/annual/models/at-reconfilter.model";
import { AnnualTrueUpService } from "./../../../recon/annual/services/at.service";

declare var jQuery: any;

@Component({
    selector: '[at-recon]',
    templateUrl: './at-recon.template.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./at-recon.style.scss']
})


export class AnnualTrueUpReconComponent implements OnInit, OnDestroy, AfterViewInit {

    @Input() fiscalYear;
    @Input() submitted;
    @Input() createdDate;
    @Output() processComplete: EventEmitter<any> = new EventEmitter();
    @Output() unapproved: EventEmitter<any> = new EventEmitter();

    router: Router;
    alerts: { type: string; msg: string; }[];
    data: any;
    shareData: ShareDataService;
    busyGen: Subscription;
    busyLoad: Subscription;
    reconexportdata: any;
    showActionRequired: boolean;
    unapprovedCount: any;
    reportedtobaccoselected:String;
    private modPrefix = "AT";
    navigateAway: boolean;

    reconstatusfilter = new AnnualTrueUpReconFilter();
    reconstatusfilterdata = new AnnualTrueUpReconFilterData();

    statusOptions = [{ name: 'Approved', value: 'Y' }, { name: 'Unapproved', value: 'N' }];
    statusOptionsMap = { 'Approved': false, 'Unapproved': true };

    public columns:Array<any> = [
        {name: 'companyName', type: 'input', filtering: {filterString: '', columnName: 'companyName', placeholder: 'Enter Company Name'}},
        {name: 'permitNum', type: 'input', filtering: {filterString: '', columnName: 'permitNum', placeholder: 'Enter TTB Permit #'}},
        {
            name: 'reportedTobacco', type: 'select', defaultvalue: 'All',
            options: [{ name: 'All', value: '' }, { name: 'Z', value: 'Z' }, { name: 'C', value: 'C' }, { name: 'NC', value: 'Z||NC' }],
            filtering: { filterString: '', columnName: 'reportedTobacco', placeholder: 'Filter by Reported' }
        },
        {name: 'month', type: 'input', filtering: {filterString: '', columnName: 'month', placeholder: 'Enter Month'}},
        {name: 'reconStatus', type: 'select', defaultvalue: 'Not Approved',
            options: [{ name: 'All', value: '' }, { name: 'Approved', value: 'Approved' }, { name: 'Unapproved', value: 'Not Approved' }], 
            filtering: {filterString: 'Not Approved', columnName: 'reconStatus', placeholder: 'Filter by Status'}},        
    ];

    constructor(
            router: Router,
            private storage: LocalStorageService,            
            private reconReportService: AnnualTrueUpService,
            private _shareData: ShareDataService) {

        this.shareData = _shareData;

        this.alerts = [];
        this.router = router;
        this.unapprovedCount = 0;
        this.showActionRequired = false;
        this.navigateAway = false;
        if(this.shareData.breadcrumb =="at-recon")

        {   this.fiscalYear = this.shareData.fiscalYear;
            this.getAnnualReconcilationReport();
        }
 
    }

    ngOnInit() {
        
    }

    ngAfterViewInit() {
        for(let col of this.columns){
            if(col.filtering){
                this.cosmeticFix('#filter-' + col.name);        
            }
        }
    }

    ngOnDestroy(){
        if(this.busyGen){
            this.busyGen.unsubscribe();
        }
        if(this.busyLoad) this.busyLoad.unsubscribe();

        if (!this.navigateAway) {
            this.clearstorage();
        }
    }
    getAnnualReconcilationReport(selected: boolean = false):any {
        this.alerts = [];
        this.unapprovedCount = 0;
        this.updateFilters();
        this.loadSearchCriteriaFromLocalStorage();        
        this.refreshReconStatusFilterObject();
        this.fiscalYear = this.shareData.fiscalYear;
        if(this.busyLoad) this.busyLoad.unsubscribe();
        //Load search riteria if coming back to the page
        
        this.busyLoad = this.reconReportService.getReconReportDetailsForFiscalYear(this.fiscalYear).subscribe(
            data => {
                if (data) {
                    this.data = data;
                    this.unapprovedCount = _.size(_.filter(this.data, {reconStatus: 'Not Approved'}));
                   
                    if (this.unapprovedCount > 0) {
                        this.showActionRequired = true;
                        this.alerts.push(
                                {
                                    type: 'danger',
                                    // tslint:disable-next-line:max-line-length
                                    msg:  '<span class="alert-danger"><strong>ACTION REQUIRED</strong>: ' + this.unapprovedCount + ' Monthly Report(s) need to be Approved in order to Submit the Market Share</span>'
                                });
                    } else {
                        this.showActionRequired = false;
                        this.processComplete.emit(true);
                        this.alerts = [];
                    }

                   
                  
                    var i = 0;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].reportedTobacco == undefined) {
                                this.data[i].reportedTobacco = "NC";
    
                            }
    
                        }

                        if(selected){
                            return this.unapprovedCount;
                        }
                }
            },
            error => {
               
                this.alerts = [];
                // tslint:disable-next-line:max-line-length
                this.alerts.push({type: 'danger', msg:  '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + error + '</span>'});
            });
    }
    updateFilterTextBox(colName, colText){
        let rowIndex = _.findIndex(this.columns, function (o) { return (o.name && o.name.toString() === colName) });
        if(rowIndex !== -1) {
            let selRow = this.columns[rowIndex];
            if(selRow.filtering){
                selRow.filtering.filterString = colText;
            }
        }
    }

    updateFilterDropDown(colName, colText){
        let rowIndex = _.findIndex(this.columns, function (o) { return (o.name && o.name.toString() === colName) });
        if(rowIndex !== -1) {
            let selRow = this.columns[rowIndex];
            if(selRow.filtering){
                selRow.filtering.filterString = colText;
                selRow.defaultvalue = colText;
            }
        }
    }        

    loadSearchCriteriaFromLocalStorage(){
        if (this.storage.retrieve(this.modPrefix + 'CompanyFilter') != null){
            this.reconstatusfilter.companyFilter = this.storage.retrieve(this.modPrefix + 'CompanyFilter');
            this.updateFilterTextBox('companyName', this.reconstatusfilter.companyFilter);
        }    
        if (this.storage.retrieve(this.modPrefix + 'PermitFilter') != null){
            this.reconstatusfilter.permitFilter = this.storage.retrieve(this.modPrefix + 'PermitFilter');
            this.updateFilterTextBox('permitNum', this.reconstatusfilter.permitFilter);                
        }  
        if (this.storage.retrieve(this.modPrefix + 'MonthFilter') != null){
            this.reconstatusfilter.monthFilter = this.storage.retrieve(this.modPrefix + 'MonthFilter');
            this.updateFilterTextBox('month', this.reconstatusfilter.monthFilter);                    
        }  
        if (this.storage.retrieve(this.modPrefix + 'ReconStatusFilter') != null){
            this.reconstatusfilter.statusFilter = this.storage.retrieve(this.modPrefix + 'ReconStatusFilter');
            this.updateFilterDropDown('reconStatus', this.reconstatusfilter.statusFilter);                 
        }    
        if (this.storage.retrieve(this.modPrefix + 'ReportedTobaccoFilter') != null) {
            this.reconstatusfilter.reportedTobaccoFilter = this.storage.retrieve(this.modPrefix + 'ReportedTobaccoFilter');
            this.updateFilterDropDown('reportedTobacco', this.reconstatusfilter.reportedTobaccoFilter);
        }                          
    }

    isValidString(str) {
        return ( (str != null) && (typeof str !== 'undefined') && (str.length >= 0));
    }

    saveSearchCriteriaToLocalStorage(){
        if(this.isValidString(this.reconstatusfilter.companyFilter)){
            this.storage.store(this.modPrefix + 'CompanyFilter', this.reconstatusfilter.companyFilter);
        }
        if(this.isValidString(this.reconstatusfilter.permitFilter)){        
            this.storage.store(this.modPrefix + 'PermitFilter', this.reconstatusfilter.permitFilter);        
        }
        if(this.isValidString(this.reconstatusfilter.monthFilter)){            
            this.storage.store(this.modPrefix + 'MonthFilter', this.reconstatusfilter.monthFilter);                
        }
        if(this.isValidString(this.reconstatusfilter.statusFilter)){          
            this.storage.store(this.modPrefix + 'ReconStatusFilter', this.reconstatusfilter.statusFilter);                        
        }    
        if (this.isValidString(this.reconstatusfilter.reportedTobaccoFilter)) {
            this.storage.store(this.modPrefix + 'ReportedTobaccoFilter', this.reconstatusfilter.reportedTobaccoFilter);
        }               
    }    
    /**
     *
     *
     * @param {any} rptStatus
     *
     * @memberOf ReconciliationReport
     */
    showReport(rptStatus) {
        this.navigateAway = true;
        this.shareData.reset();
        this.shareData.companyName = rptStatus.companyName;
        this.shareData.permitNum = rptStatus.permitNum;
        this.shareData.period = rptStatus.month + " FY " + rptStatus.fiscalYear;
        this.shareData.permitId = rptStatus.permitId;
        this.shareData.periodId = rptStatus.periodId;
        this.shareData.companyId = rptStatus.companyId;

        this.shareData.periodStatus = rptStatus.reportStatus;
        this.shareData.previousRoute = this.router.url;
        this.shareData.isRecon = true;
        this.shareData.breadcrumb = 'at-reconreport';
        this.shareData.qatabfrom = 'at-reconreport';
        this.shareData.createdDate = this.createdDate;
        this.shareData.submitted = this.submitted;

        // this.shareData.quarter = this.quarter;
        this.shareData.fiscalYear = this.fiscalYear;
        // this.shareData.months = this.months;
        return false;
    }


   refreshReconStatusFilterObject() {
        this.reconstatusfilterdata = null;
        this.reconstatusfilterdata = new AnnualTrueUpReconFilterData();
        this.reconstatusfilterdata.reportStatus = this.reconstatusfilter.statusFilters;
        this.reconstatusfilterdata.companyFilter = this.reconstatusfilter.companyFilter;
        this.reconstatusfilterdata.permitFilter = this.reconstatusfilter.permitFilter;
        this.reconstatusfilterdata.monthFilter = this.reconstatusfilter.monthFilter;
        this.reconstatusfilterdata.statusFilter = this.reconstatusfilter.statusFilter
        this.reconstatusfilterdata.reportedTobaccoFilter = this.isEmpty(this.reportedtobaccoselected) ? this.reconstatusfilter.reportedTobaccoFilter: this.reportedtobaccoselected;
        this.saveSearchCriteriaToLocalStorage();      
    }

    isEmpty(val) {
        return (val === undefined || val == null) ? true : false;
    }

    updateFilters() {
        for(let col of this.columns){
            let filterinfo = col.filtering;
            if(filterinfo){
                if(filterinfo.columnName === "companyName"){
                    this.reconstatusfilter.companyFilter = filterinfo.filterString;
                }
                if(filterinfo.columnName === "permitNum"){
                    this.reconstatusfilter.permitFilter = filterinfo.filterString;
                }
                if(filterinfo.columnName === "month"){
                    this.reconstatusfilter.monthFilter = filterinfo.filterString;
                }     
                if(filterinfo.columnName === "reconStatus"){
                    this.reconstatusfilter.statusFilter = filterinfo.filterString;
                }   
                if (filterinfo.columnName === "reportedTobacco") {
                    this.reconstatusfilter.reportedTobaccoFilter = filterinfo.reportedTobaccoFilter;
                }                              
            } 
        }        
    }

    FilterSelectionChanged(event, filterColumnName){
        if(filterColumnName === "reconStatus"){
            this.reconstatusfilter.statusFilter = event;
            this.refreshReconStatusFilterObject();
        } 
        if (filterColumnName === "reportedTobacco") {
            this.reconstatusfilter.reportedTobaccoFilter = event;
            this.reportedtobaccoselected = event;
            this.refreshReconStatusFilterObject();
        }
    }

    ApplyFilter(){
        this.refreshReconStatusFilterObject();
    }

    updateCheckedStatusOptions(option, event) {
        this.statusOptionsMap[option] = event.target.checked;
        this.updateFilters();
        this.refreshReconStatusFilterObject();
    }

    public onChangeTable(filterinfo):void {
        if(filterinfo){
            if(filterinfo.columnName === "companyName"){
                this.reconstatusfilter.companyFilter = filterinfo.filterString;
                this.reconstatusfilter.companyFilter = this.reconstatusfilter.companyFilter.trim();
            }
            if(filterinfo.columnName === "permitNum"){
                this.reconstatusfilter.permitFilter = filterinfo.filterString;
                this.reconstatusfilter.permitFilter = this.reconstatusfilter.permitFilter.trim();
            }
            if(filterinfo.columnName === "month"){
                this.reconstatusfilter.monthFilter = filterinfo.filterString;
                this.reconstatusfilter.monthFilter = this.reconstatusfilter.monthFilter.trim();
            }     
            if(filterinfo.columnName === "reconStatus"){
                this.reconstatusfilter.statusFilter = filterinfo.filterString;
                this.reconstatusfilter.statusFilter = this.reconstatusfilter.statusFilter.trim();
            }  
            if (filterinfo.columnName === "reportedTobacco") {
                this.reconstatusfilter.reportedTobaccoFilter = filterinfo.filterString;
                this.reconstatusfilter.reportedTobaccoFilter = this.reconstatusfilter.reportedTobaccoFilter.trim();
            }                               
            if(filterinfo.refresh){
                this.refreshReconStatusFilterObject();
            }            
        }
    }      

    cosmeticFix(elementID: any){
        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let filterInput = jQuery(elementID);
        filterInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

  exportReconReport() {

    if (this.busyGen) {
        this.busyGen.unsubscribe();
    }

    this.busyGen = this.reconReportService.exportReconReportData(this.fiscalYear).subscribe(
      data => {
          if (data) {
              this.reconexportdata = data;
               this.download(this.reconexportdata.csv, this.reconexportdata.title, 'csv/text');
          }
      });
  }
  clearstorage() {
    this.storage.clear('apAttributes');
    //this.storage.clear('compare-filters');
}
  /**
   *
   *
   * @param {*} data
   * @param {*} strFileName
   * @param {*} strMimeType
   * @returns
   *
   * @memberOf ReconciliationReport
   */
  download(data: any, strFileName: any, strMimeType: any) {

      let self: any = window, // this script is only for browsers anyway...
          defaultMime = 'application/octet-stream', // this default mime also triggers iframe downloads
          mimeType = strMimeType || defaultMime,
          payload = data,
          url = !strFileName && !strMimeType && payload,
          anchor = document.createElement('a'),
          toString = function (a) { return String(a); },
          myBlob = (self.Blob || self.MozBlob || self.WebKitBlob || toString),
          fileName = strFileName || 'download',
          blob,
          reader;
      myBlob = myBlob.call ? myBlob.bind(self) : Blob;

      if (String(this) === 'true') { // reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
          payload = [payload, mimeType];
          mimeType = payload[0];
          payload = payload[1];
      }

      // go ahead and download dataURLs right away
      if (/^data\:[\w+\-]+\/[\w+\-\.]+[,;]/.test(payload)) {

          if (payload.length > (1024 * 1024 * 1.999) && myBlob !== toString) {
              payload = dataUrlToBlob(payload);
              mimeType = payload.type || defaultMime;
          } else {
              return navigator.msSaveBlob ?  // IE10 can't do a[download], only Blobs:
                  navigator.msSaveBlob(dataUrlToBlob(payload), fileName) :
                  saver(payload, false); // everyone else can save dataURLs un-processed
          }

      } else {// not data url, is it a string with special needs?
          if (/([\x80-\xff])/.test(payload)) {
              let i = 0, tempUiArr = new Uint8Array(payload.length), mx = tempUiArr.length;
              for (i; i < mx; ++i) tempUiArr[i] = payload.charCodeAt(i);
              payload = new myBlob([tempUiArr], { type: mimeType });
          }
      }
      blob = payload instanceof myBlob ?
          payload :
          new myBlob([payload], { type: mimeType });


      function dataUrlToBlob(strUrl) {
          let parts = strUrl.split(/[:;,]/),
              type = parts[1],
              decoder = parts[2] === 'base64' ? atob : decodeURIComponent,
              binData = decoder(parts.pop()),
              mx = binData.length,
              i = 0,
              uiArr = new Uint8Array(mx);

          for (i; i < mx; ++i) uiArr[i] = binData.charCodeAt(i);

          return new myBlob([uiArr], { type: type });
      }

      function saver( url: any, winMode: any) {

          if ('download' in anchor) { // html5 A[download]
              anchor.href = url;
              anchor.setAttribute('download', fileName);
              anchor.className = 'download-js-link';
              anchor.innerHTML = 'downloading...';
              anchor.style.display = 'none';
              document.body.appendChild(anchor);
              setTimeout(function () {
                  anchor.click();
                  document.body.removeChild(anchor);
                  if (winMode === true) { setTimeout(function () { self.URL.revokeObjectURL(anchor.href); }, 250); }
              }, 66);
              return true;
          }

          // handle non-a[download] safari as best we can:
          if (/(Version)\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//.test(navigator.userAgent)) {
              if (/^data:/.test(url)) url = 'data:' + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
              if (!window.open(url)) { // popup blocked, offer direct download:
                  if (confirm('Displaying New Document\n\nUse Save As... to download, then click back to return to this page.')) {
                      location.href = url;
                  }
              }
              return true;
          }

          // do iframe dataURL download (old ch+FF):
          let f = document.createElement('iframe');
          document.body.appendChild(f);

          if (!winMode && /^data:/.test(url)) { // force a mime that will download:
              url = 'data:' + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
          }
          f.src = url;
          setTimeout(function () { document.body.removeChild(f); }, 333);

      } // end saver

      if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or URL)
          return navigator.msSaveBlob(blob, fileName);
      }

      if (self.URL) { // simple fast and modern way using Blob and URL:
          saver(self.URL.createObjectURL(blob), true);
      } else {
          // handle non-Blob()+non-URL browsers:
          if (typeof blob === 'string' || blob.constructor === toString) {
              try {
                  return saver('data:' + mimeType + ';base64,' + self.btoa(blob), false);
              } catch (y) {
                  return saver('data:' + mimeType + ',' + encodeURIComponent(blob), false);
              }
          }

          // Blob but not URL support:
          reader = new FileReader();
          reader.onload = function (e) {
              saver(this.result, false);
          };
          reader.readAsDataURL(blob);
      }
      return true;
  };
}

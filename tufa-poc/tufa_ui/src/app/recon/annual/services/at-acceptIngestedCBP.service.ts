import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";
import { CBPMetrics } from "../models/at-cbp-metrics.model";
import { Observable } from "rxjs";

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

/* TODO: This should be in the AnnualtrueUpService and not a seperate service */
@Injectable()
export class AcceptCBPDataService {

    private acceptCBPIngested = environment.apiurl + '/api/v1/trueup/ingested';

    constructor(private _httpClient: HttpClient) { }

    public acceptIngestedCBP(fiscalYr: string): Observable<CBPMetrics> {
        //create url
        let url = this.acceptCBPIngested + "/acceptCBPLegacy/" + fiscalYr;
        return this._httpClient.post<CBPMetrics>(url, httpOptions);
    }
}
import { Injectable } from "@angular/core";
import { Subject } from 'rxjs';
import { Amendment } from "../models/at-amendment.interfact";
import { CompareAcceptFDASourceModel } from '../models/at-compare-accept-fda-source.moduel';
import { CBPAmendment } from "./../../../recon/annual/models/at-cbpamendment.model";
import { TTBAmendment } from "./../../../recon/annual/models/at-ttbamendment.model";
import { MixedAction } from "../models/at-mixedaction.model";

@Injectable()
export class AnnualTrueUpComparisonEventService {

	private atCompareCommentsUpdateSource = new Subject<TTBAmendment[]>();
	private atCompareCommentsSaveSource = new Subject<TTBAmendment[]>();
	private atTTBCompareCommentsDeleteSource = new Subject<Boolean>();

	atCompareCommentsUpdateSource$ = this.atCompareCommentsUpdateSource.asObservable();
	atCompareCommentsSaveSource$ = this.atCompareCommentsSaveSource.asObservable();
	atTTBCompareCommentsDeleteSource$ = this.atTTBCompareCommentsDeleteSource.asObservable();

	private atCompareAcceptFDASource = new Subject<CompareAcceptFDASourceModel>();
	private atCompareActiveQuarterUpdateSource = new Subject<number>();
	atCompareAcceptFDASource$ = this.atCompareAcceptFDASource.asObservable();
	atCompareActiveQuarterUpdateSource$ = this.atCompareActiveQuarterUpdateSource.asObservable();
	private atCompareAcceptMixedAction = new Subject<any>();
	atCompareAcceptMixedAction$ = this.atCompareAcceptMixedAction.asObservable();
	private atCompareMixedActionRefresh = new Subject<any>();
	atCompareMixedActionRefresh$ = this.atCompareMixedActionRefresh.asObservable();
	private atCompareMixedActionCigarRefresh = new Subject<any>();
	atCompareMixedActionCigarRefresh$ = this.atCompareMixedActionCigarRefresh.asObservable();

	//for cbp details
	private atCBPCompareCommentsUpdateSource = new Subject<CBPAmendment[]>();
	private atCBPCompareCommentsSaveSource = new Subject<CBPAmendment[]>();
	private atCBPCompareCommentsEditSource = new Subject();
	private atTTBCompareCommentsEditSource = new Subject();
	private atCBPCompareCommentsDeleteSource = new Subject<Boolean>();
	private atCBPCompare3852FlagsUpdateSource = new Subject<any>();
	//private atCBPCompareOneSidedFlagsUpdateSource = new Subject<any>();

	atCBPCompareCommentsUpdateSource$ = this.atCBPCompareCommentsUpdateSource.asObservable();
	atCBPCompareCommentsSaveSource$ = this.atCBPCompareCommentsSaveSource.asObservable();
	atCBPCompareCommentsEditSource$ = this.atCBPCompareCommentsEditSource.asObservable();
	atTTBCompareCommentsEditSource$ = this.atTTBCompareCommentsEditSource.asObservable();
	atCBPCompareCommentsDeleteSource$ = this.atCBPCompareCommentsDeleteSource.asObservable();
	atCBPCompare3852FlagsUpdateSource$ = this.atCBPCompare3852FlagsUpdateSource.asObservable();
	//atCBPCompareOneSidedFlagsUpdateSource$ = this.atCBPCompareOneSidedFlagsUpdateSource.asObservable();

	private atCBPCompareAcceptFDASource = new Subject<Boolean[]>();
	private atCBPCompareActiveQuarterUpdateSource = new Subject<number>();
	atCBPCompareAcceptFDASource$ = this.atCBPCompareAcceptFDASource.asObservable();
	atCBPCompareActiveQuarterUpdateSource$ = this.atCBPCompareActiveQuarterUpdateSource.asObservable();

	private atCompareAmendmentsLoaded = new Subject<boolean>();
	atCompareAmendmentsLoaded$ = this.atCompareAmendmentsLoaded.asObservable();

	private atRellocationNotification = new Subject<any>();
	atRellocationNotification$ = this.atRellocationNotification.asObservable();


	public triggerAtRellocationNotification(flags: any) {
		this.atRellocationNotification.next(flags);
	}
	public triggerAtCBPCompare3852FlagsUpdateSource(flags: any) {
		this.atCBPCompare3852FlagsUpdateSource.next(flags);
	}
	/*public triggerAtCBPCompareOneSidedFlagsUpdateSource(flags: any) {
		this.atCBPCompareOneSidedFlagsUpdateSource.next(flags);
	}*/

	public triggerAtCompareCommentsUpdate(amend: TTBAmendment[]) {
		this.atCompareCommentsUpdateSource.next(amend);
	}

	public triggerAtCompareCommentsSave(amend: TTBAmendment[]) {
		this.atCompareCommentsSaveSource.next(amend);
	}

	public triggerAtTTBCompareCommentsDelete(deleted: boolean) {
		this.atTTBCompareCommentsDeleteSource.next(deleted);
	}

	public triggerAtCBPCompareCommentsUpdate(amend: CBPAmendment[]) {
		this.atCBPCompareCommentsUpdateSource.next(amend);
	}

	public triggerAtCBPCompareCommentsSave(amend: CBPAmendment[]) {
		this.atCBPCompareCommentsSaveSource.next(amend);
	}
	public triggerAtCBPCompareCommentsEdit() {
		this.atCBPCompareCommentsEditSource.next();
	}
	public triggerAtTTBCompareCommentsEdit() {
		this.atTTBCompareCommentsEditSource.next();
	}
	public triggerAtCBPCompareCommentsDelete(deleted: boolean) {
		this.atCBPCompareCommentsDeleteSource.next(deleted);
	}

	public triggerAtCompareAcceptFDASource(accept: CompareAcceptFDASourceModel) {
		this.atCompareAcceptFDASource.next(accept);
	}

	public triggerAtCompareAcceptMixedAction(accept: any) {
		this.atCompareAcceptMixedAction.next(accept);
	}

	public triggerAtCompareMixedActionRefresh(accept: any) {
		this.atCompareMixedActionRefresh.next(accept);
	}

	public triggerAtCompareMixedActionCigarRefresh(accept: any) {
		this.atCompareMixedActionCigarRefresh.next(accept);
	}

	public triggerAtCompareActiveQuarterUpdateSource(qtr: number) {
		this.atCompareActiveQuarterUpdateSource.next(qtr);
	}

	public triggerAtCBPCompareAcceptFDASource(accept: any[]) {
		this.atCBPCompareAcceptFDASource.next(accept);
	}

	public triggerAtCBPCompareActiveQuarterUpdateSource(qtr: number) {
		this.atCBPCompareActiveQuarterUpdateSource.next(qtr);
	}

	public triggerAtCompareAmendmentsLoaded(loaded: boolean): any {
		this.atCompareAmendmentsLoaded.next(loaded);
	}

	private atCompareSuccessfullSaveAction = new Subject<Amendment[]>();


	public atCompareSuccessfullSaveAction$ = this.atCompareSuccessfullSaveAction.asObservable();


	public triggerAtCompareSuccessfullSaveAction(data: Amendment[]) {
		this.atCompareSuccessfullSaveAction.next(data);
	}

	private atCompareSuccessfullSaveActionOneSided = new Subject<any>();

	public atCompareSuccessfullSaveActionOneSided$ = this.atCompareSuccessfullSaveActionOneSided.asObservable();

	public triggerAtCompareSuccessfullSaveActionOneSided(data: any) {
		this.atCompareSuccessfullSaveActionOneSided.next(data)
		
	}

	private quarterExpandedAction = new Subject<any>();
	public quarterExpandedAction$ = this.quarterExpandedAction.asObservable();
	public triggerQuarterExpanded(data: any) {
		this.quarterExpandedAction.next(data);
	}

	private tobaccoClassSelected = new Subject<string[]>();
	public tobaccoClassSelected$ = this.tobaccoClassSelected.asObservable();
	public triggerTobaccoClassSelected(data: string[]) {
		this.tobaccoClassSelected.next(data);
	}
	private nocompanyInfo = new Subject<number>();
	public nocompanyInfo$ = this.nocompanyInfo.asObservable();
	public triggerNoCompanyInfo(data: number) {
		this.nocompanyInfo.next(data);
	}

	private flagsOnCommentSaveManu = new Subject();
	public flagsOnCommentSaveManu$ = this.flagsOnCommentSaveManu.asObservable();
	public triggerFlagsOnCommentSaveManu() {
		this.flagsOnCommentSaveManu.next();
	}
	private flagsOnCommentSaveImpt = new Subject();
	public flagsOnCommentSaveImpt$ = this.flagsOnCommentSaveImpt.asObservable();
	public triggerFlagsOnCommentSaveImpt() {
		this.flagsOnCommentSaveImpt.next();
	}

}
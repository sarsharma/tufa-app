import { HttpClient, HttpHeaders,HttpParams} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { environment } from "../../../../environments/environment";
import { allDeltaCommentModel } from "../models/at-comment.model";
import { CmpDetailUserComment } from "../models/at-cbpamendment.model";
import { TTBDetailUserComment } from "../models/at-ttbamendment.model";
import { Details } from "../models/at-compare-details.model";
import { AnnualTrueUpImporterDetail } from "../models/at-compare-importerdetails.model";
import { AnnualTrueUpManufacturerDetail, ManufactureQuarterDetails } from "../models/at-compare-manufacturerdetails.model";
import { AnnualTrueUpCompareDetailsSummary } from "../models/at-comparedetailssummary.model";
import { TrueUpComparisonResult } from "../models/at-comparisonresult.model";
import { ExportData } from "../models/at-export.model";
import { ResultMap } from "../models/at-result-map.model";
import { SummarySet } from "../models/at-summary-set.model";
import { TTBTaxesVol } from "../models/at-ttbtaxesvol.model";
import { CBPAmendment } from "./../../../recon/annual/models/at-cbpamendment.model";
import { AnnualTrueUpCBPLineEntryUpdate } from "./../../../recon/annual/models/at-cbplineentryupdate.model";
import { CompareSearchCriteria } from './../../../recon/annual/models/at-compare.search.criteria';
import { TTBAmendment } from "./../../../recon/annual/models/at-ttbamendment.model";
import { PaginationAttributes } from './../../../shared/datatable/pagination.attributes.model';
import { Document } from './../../../recon/annual/models/at-cmpdetails.document.model';
import { AuthService } from './../../../login/services/auth.service';
const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class AnnualTrueUpComparisonService {
    private getTrueupUrl = environment.apiurl + '/api/v1/trueup';
    private commentAttachmentURL = environment.apiurl + '/api/v1/trueup/comparedetails/comment/attachs/';
    
    constructor(private _httpClient: HttpClient,private authService: AuthService) { }

    getAlldeltasComments(fiscalYear: number, ein: string, tobaccoClass: string, quarter: string, permitType: string) {
        let url = this.getTrueupUrl + "/compare/alldeltas/comments/" + fiscalYear + "/" + ein + "/" + quarter + "/" + tobaccoClass + "/" + permitType;
        return this._httpClient.get(url, httpOptions);

    }

    updateComment(comment: allDeltaCommentModel, commentSeq: number) {
        let url = this.getTrueupUrl + '/compareall/alldeltas/comments/updateComment/' + commentSeq;
        return this._httpClient.post(url, comment, httpOptions);
    }


    getAllComparisonResults(fiscalYear: string, compareSearchCriteria: CompareSearchCriteria, pagAttr: PaginationAttributes): Observable<ResultMap> {
        let url = this.getTrueupUrl + "/compare/results/all/" + fiscalYear + "?" + pagAttr.getQueryParamString();
        return this._httpClient.put<ResultMap>(url, (compareSearchCriteria), httpOptions);
    }

    getComparisonResults(fiscalYear: string, compareSearchCriteria: CompareSearchCriteria, pagAttr: PaginationAttributes): Observable<ResultMap> {
        let url = this.getTrueupUrl + "/compare/results/" + fiscalYear + "?" + pagAttr.getQueryParamString();
        return this._httpClient.put<ResultMap>(url, (compareSearchCriteria), httpOptions);
    }

    // compare/manu/cigars/summary/{companyId}/{fiscalYear}
    getCigarManufacturerComparisonDetailsSummary(companyId: number, fiscalYear: number) {
        let url = this.getTrueupUrl + "/compare/manu/cigars/summary/" + companyId + "/" + fiscalYear;
        return this._httpClient.get(url, httpOptions);
    }

    getCigarImporterComparisonDetailsSummary(companyId: number, fiscalYear: number) {
        let url = this.getTrueupUrl + "/compare/impt/cigars/summary/" + companyId + "/" + fiscalYear;
        return this._httpClient.get(url, httpOptions);
    }

    getCigarImporterComparisonDetailsIngestedSummary(companyId: number, fiscalYear: number): any {
        let url = this.getTrueupUrl + "/compare/impt/cigars/ingested/summary/" + companyId + "/" + fiscalYear;
        return this._httpClient.get(url, httpOptions);
    }

    updateDateSelected(fiscalYr: number, tobaccoClass: string, ein: string, selDateType: string) {
        //alert("updateDateSelected");
        let url = this.getTrueupUrl + "/compare/impt/reallocate/" + fiscalYr + "/" + tobaccoClass + "/" + ein + "/" + selDateType;
        //alert(url);
        return this._httpClient.put<ResultMap>(url, httpOptions);

    }

    getSelectedDateType(fiscalYr: number, tobaccoClass: string, ein: string) {
        let url = this.getTrueupUrl + "/compare/impt/details/selDate/" + fiscalYr + "/" + tobaccoClass + "/" + ein;
        return this._httpClient.get(url, httpOptions);
    }


    getImporterComparisonDetailsSummary(companyId: number, fiscalYear: number, quarter: number, tobaccoClass: string): Observable<SummarySet> {
        let url = this.getTrueupUrl + "/compare/impt/summary/" + companyId + "/" + fiscalYear + "/" + quarter + "/" + tobaccoClass;
        return this._httpClient.get<SummarySet>(url, httpOptions);
    }

    getImporterSummDetailsData(einType: string, ein: string, fiscalYear: number, quarter: string, tobaccoClass: string) {
        let url = this.getTrueupUrl + "/comparealldeltas/impt/review/" + einType + "/" + ein + "/" + fiscalYear + "/" + quarter + "/" + tobaccoClass;
        return this._httpClient.get(url, httpOptions);
    }

    getNonCigarManufacturerComparisonDetailsSummary(companyId: number, fiscalYear: number, quarter: number, tobaccoClass: string): Observable<AnnualTrueUpCompareDetailsSummary[]> {
        let url = this.getTrueupUrl + "/compare/manu/noncigars/summary/" + companyId + "/" + fiscalYear + "/" + quarter + "/" + tobaccoClass;
        return this._httpClient.get(url, httpOptions).pipe(
            tap(_ => this.log("getUserbyEmail")),
            catchError(this.handleError<any>("getUserbyEmail"))
        );
    }

    getImporterComparisonDetails(ein: string, fiscalYear: number, quarter: number, tobaccoClass: string): Observable<AnnualTrueUpImporterDetail[]> {
        let urlTobaccoClassRedirect = (tobaccoClass.toUpperCase() === "CIGARS") ? "/compare/impt/cigars/details/" :
            "/compare/impt/noncigars/details/";
        let url = this.getTrueupUrl + urlTobaccoClassRedirect + ein + "/" + fiscalYear + "/" + quarter + "/" + tobaccoClass;
        return this._httpClient.get(url, httpOptions).pipe(
            tap(_ => this.log("getImporterComparisonDetails")),
            catchError(this.handleError<any>("getImporterComparisonDetails"))
        );
    }

    getImporterComparisonIngestionDetails(ein: string, fiscalYear: number, quarter: number, tobaccoClass: string): Observable<AnnualTrueUpImporterDetail[]> {
        let urlTobaccoClassRedirect = (tobaccoClass.toUpperCase() === "CIGARS") ? "/compare/impt/cigars/ingestdetails/" :
            "/compare/impt/noncigars/ingestdetails/";
        let url = this.getTrueupUrl + urlTobaccoClassRedirect + ein + "/" + fiscalYear + "/" + quarter + "/" + tobaccoClass;
        return this._httpClient.get<AnnualTrueUpImporterDetail[]>(url, httpOptions);
    }

    getImporterComparisonHTSCodeSummary(ein: string, fiscalYear: number, quarter: number, tobaccoClass: string) {
        let urlTobaccoClassRedirect = (tobaccoClass.toUpperCase() === "CIGARS") ? "/compare/impt/cigars/ingesthtscodes/" :
            "/compare/impt/noncigars/ingesthtscodes/";
        let url = this.getTrueupUrl + urlTobaccoClassRedirect + ein + "/" + fiscalYear + "/" + quarter + "/" + tobaccoClass;
        return this._httpClient.get(url, httpOptions);
    }

    getCigarManufacturerComparisonDetails(companyId: number, fiscalYear: number, quarter: number, tobaccoClass: string): Observable<AnnualTrueUpManufacturerDetail[]> {
        let url = this.getTrueupUrl + "/compare/manu/cigars/details/" + companyId + "/" + fiscalYear + "/" + quarter + "/" + tobaccoClass;
        return this._httpClient.get<AnnualTrueUpManufacturerDetail[]>(url, httpOptions);
    }

    getNonCigarManufacturerComparisonDetails(companyId: number, fiscalYear: number, quarter: number, tobaccoClass: string): Observable<AnnualTrueUpManufacturerDetail[]> {
        let url = this.getTrueupUrl + "/compare/manu/noncigars/details/" + companyId + "/" + fiscalYear + "/" + quarter + "/" + tobaccoClass;
        return this._httpClient.get<AnnualTrueUpManufacturerDetail[]>(url, httpOptions);
    }

    getManufacturerComparisonDetails(companyId: number, fiscalYear: number, quarter: number, tobaccoClass: string): Observable<ManufactureQuarterDetails> {
        let url = this.getTrueupUrl + "/compare/manu/details/" + companyId + "/" + fiscalYear + "/" + quarter + "/" + tobaccoClass;
        return this._httpClient.get<ManufactureQuarterDetails>(url, httpOptions);
    }

    getTTBAmendments(fiscalYr: number, companyId: number, ttbAmendments: TTBAmendment[]) {
        let url = this.getTrueupUrl + "/compare/fiscalYr/" + fiscalYr + "/company/" + companyId + "/ttbamendments";
        return this._httpClient.post<TTBAmendment[]>(url, ttbAmendments, httpOptions);
    }

    saveTTBAmendments(ttbAmendments: TTBAmendment[], fiscalYear) {
        let url = this.getTrueupUrl + "/compare/ttbamendments/" + fiscalYear;
        return this._httpClient.put<TTBAmendment[]>(url, ttbAmendments, httpOptions);
    }

    saveTTBCompareComment(ttbAmendments: TTBAmendment[], ein: string,fiscalYear, tobaccoClass: string): Observable<TTBAmendment[]> {
        let url = this.getTrueupUrl + "/compare/comments/"+ fiscalYear + "/"+ein+ "/"+tobaccoClass;
        return this._httpClient.post<TTBAmendment[]>(url, ttbAmendments, httpOptions);
    }

    deleteTTBCompareComment(ttbAmendments: TTBAmendment[]) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let url = this.getTrueupUrl + "/compare/ttbcomments";
        return this._httpClient.put(url, ttbAmendments, httpOptions);
    }

    deleteCBPCompareComment(amends: CBPAmendment[]) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let url = this.getTrueupUrl + "/compare/cbpcomments";
        return this._httpClient.put(url, amends, httpOptions);
    }

    updateTTBCompareAcceptFDA(ttbAmendments: TTBAmendment[], fiscalYear) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let url = this.getTrueupUrl + "/compare/acceptfda/" + fiscalYear;
        return this._httpClient.put<TTBAmendment[]>(url, ttbAmendments, httpOptions);
    }

    //
    getCBPAmendments(fiscalYr: number, companyId: number, cbpAmendments: CBPAmendment[]) {
        let url = this.getTrueupUrl + "/compare/fiscalYr/" + fiscalYr + "/company/" + companyId + "/cbpamendments";
        return this._httpClient.post<CBPAmendment[]>(url, cbpAmendments, httpOptions);
    }

    saveCBPAmendments(cbpAmendments: CBPAmendment[], fiscalYear) {
        let url = this.getTrueupUrl + "/compare/cbpamendments/" + fiscalYear;
        return this._httpClient.put<CBPAmendment[]>(url, cbpAmendments, httpOptions);
    }

    saveCBPCompareComment(cbpAmendments: CBPAmendment[], ein: string,fiscalYear): Observable<CBPAmendment[]> {
        let url = this.getTrueupUrl + "/compare/cbpcomments/"+ fiscalYear + "/"+ein;
        return this._httpClient.post<CBPAmendment[]>(url, cbpAmendments, httpOptions);
    }
    
     editTTBCompareCommentMatched(comment: TTBDetailUserComment, commentSeq: number){
        return this._httpClient.post(this.getTrueupUrl + '/compare/ttbcomments/edit/matched/'+ commentSeq,comment,httpOptions);
     }
     editCompareCommentAllDeltas(comment: allDeltaCommentModel, permitType: string) {
        return this._httpClient.post(this.getTrueupUrl + '/compare/comments/edit/alldeltas/detailonly/'+ permitType ,comment,  httpOptions);
     }
     editCBPCompareCommentMatched(comment: CmpDetailUserComment, commentSeq: number){
        return this._httpClient.post(this.getTrueupUrl + '/compare/cbpcomments/edit/matched/'+ commentSeq,comment,httpOptions);
     }
         
     deleteCommDetailOnly(cbpAmendmentId: number,commSeqCbp:number,cmpAllDeltaId:number,commSeqAllDelta:number,permitType:string): Observable<string> {
        return this._httpClient.put(this.getTrueupUrl + '/compare/comments/delete/detailonly/' + cbpAmendmentId + '/' + commSeqCbp+ '/' + cmpAllDeltaId + '/' + commSeqAllDelta+ '/' +permitType,  httpOptions).pipe(
            tap(_ => this.log('deleteCBPCompareComment')),
            catchError(this.handleError<any>('deleteCBPCompareComment'))
        );
     }
    updateCBPCompareAcceptFDA(cbpAmendments: CBPAmendment[], fiscalYear) {
        cbpAmendments[0].mixedActionQuarterDetails = null;
        let url = this.getTrueupUrl + "/compare/cbpacceptfda/" + fiscalYear;
        return this._httpClient.put<CBPAmendment[]>(url, cbpAmendments, httpOptions);
    }

    updateCBPIngestedLineEntryInfo(updateinfo: AnnualTrueUpCBPLineEntryUpdate) {
        let url = this.getTrueupUrl + "/compare/updatecbpentry";
        return this._httpClient.put(url, updateinfo, httpOptions);
    }

    //get fda 3852 flag
    public getCBPIngestedLineEntryInfo(cbEntryId: number) {
        let url = this.getTrueupUrl + "/compare/getcbpentry/" + cbEntryId;
        return this._httpClient.get(url, httpOptions);
    }


    //get fda 3852 flag
    public getFDA3852Flag(fiscalYr: number, companyid: number, classNm: string, type: string,ein: string) {
        let url = this.getTrueupUrl + "/" + fiscalYr + "/" + companyid + "/" + classNm.replace("/", "-") + "/" + type +"/" + ein;
        return this._httpClient.get(url, httpOptions);
    }

    public getOneSidedFlag(fiscalYr: number, companyid: number, classNm: string, type: string, ein: string) {
        let url = this.getTrueupUrl + "/onesidedflag/" + fiscalYr + "/" + companyid + "/" + classNm.replace("/", "-")+ "/" + type +"/" + ein;
        return this._httpClient.get(url, httpOptions);
    }


    public exportCBPData(fiscalYr: number, quarter: number, tobaccoClass: string, ein: string): Observable<ExportData> {
        let url = this.getTrueupUrl + "/export/fiscalyear/" + fiscalYr + "/quarter/" + quarter + "/tobaccoClass/" + tobaccoClass + "/CompanyId/" + ein;
        return this._httpClient.get<ExportData>(url, httpOptions);
    }


    public updateCompareAllDeltaStatus(rptStatus, fiscalYear) {
        let url = this.getTrueupUrl + "/compareall/delta/status/" + fiscalYear;
        return this._httpClient.post(url, rptStatus, httpOptions);
    }

    public checkIfAssociationExists(fiscalYr: number, tobaccoClass: string, ein: string,qtr: number): Observable<boolean> {
        let url = this.getTrueupUrl + "/compare/check/association/" + ein + "/" + fiscalYr + "/" + tobaccoClass +  "/" + qtr + "";
        return this._httpClient.get(url, httpOptions).pipe(
            tap(_ => this.log("checkIfAssociationExists")),
            catchError(this.handleError<any>("checkIfAssociationExists"))
        );
    }

    public updateCompareAllDeltaExclude(rptExclude, fiscalYear) {
        let url = this.getTrueupUrl + "/compareall/delta/exclude/" + fiscalYear;
        return this._httpClient.post(url, rptExclude, httpOptions);
    }

    getAllComparisonResultsCounts(fiscalYr: number, inComingPage: string): any {
        let url = this.getTrueupUrl + "/compare/results/all/count/" + fiscalYr + "/" + inComingPage;
        return this._httpClient.get(url, httpOptions);
    }

    getTTBVolTaxesPerTobaccoType(ein: string, fiscalYear: number, quarter: number, tobaccoClass: string): Observable<TTBTaxesVol[]> {
        let url = this.getTrueupUrl + "/compareall/delta/ttbVolTax/" + fiscalYear + "/" + ein + "/" + quarter + "/" + tobaccoClass;
        return this._httpClient.get(url, httpOptions).pipe(
            tap(_ => this.log("getTTBVolTaxesPerTobaccoType")),
            catchError(this.handleError<any>("getTTBVolTaxesPerTobaccoType"))
        );
    }

    getAffectedDeltasForEin(actionType: string, ein: string, fiscalYear: string): Observable<TrueUpComparisonResult[]> {
        let url = this.getTrueupUrl + "/compareall/delta/action/" + actionType + "/" + fiscalYear + "/" + ein;
        return this._httpClient.get(url, httpOptions).pipe(
            tap(_ => this.log("getAffectedDeltasForEin")),
            catchError(this.handleError<any>("getAffectedDeltasForEin"))
        );
    }

    public getDetailRawExport(fiscalYr: number, quarter: string, tobaccoClass: string, ein: string, permitType: string, includeOriginal: boolean) {
        let url = this.getTrueupUrl + "/export/raw/" + ein + "/" + fiscalYr + "/" + quarter + "/" + tobaccoClass + "/" + permitType + "/" + includeOriginal;
        return this._httpClient.get(url, httpOptions).pipe(
            tap(_ => this.log("getExportRawData")),
            catchError(this.handleError<any>("getExportRawData"))
        );
    }

    public getExportData(fiscalYear: string, compareSearchCriteria: CompareSearchCriteria, pgAttr: PaginationAttributes, selCompareMethod: string) {

        let pgAttrTemp: PaginationAttributes = new PaginationAttributes(0, 0);
        Object.assign(pgAttrTemp, pgAttr);
        pgAttrTemp.activePage = 0;
        pgAttrTemp.pageSize = 0;
        let url = this.getTrueupUrl + "/export/results/" + fiscalYear + "/" + selCompareMethod + "?" + pgAttrTemp.getQueryParamString();
        return this._httpClient.put<ResultMap>(url, (compareSearchCriteria), httpOptions).pipe(
            tap(_ => this.log("getExportData")),
            catchError(this.handleError<any>("getExportRawData"))
        );

    }

    public exportDetailIngestedDeltas(companyId: string, tobaccoType: string, quarter: string, fiscalYr: number) {
        let url = this.getTrueupUrl + '/export/detailIngestedDeltas/' + companyId + "/" + tobaccoType + "/" + quarter + "/" + fiscalYr;
        return this._httpClient.get(url, httpOptions);
    }


    //Only used for single file ingestion 
    public getCompareDetails(companyId: number, fiscalYr: string, quarter: number, tobaccotype: string, ein: string): Observable<Details> {
        let url = this.getTrueupUrl + "/compare/impt/details";
        // Build url based on the passed in parameters 
        let args: any[] = Array.prototype.slice.call(arguments);
        args.forEach(element => {
            url += "/" + element;
        });
        return this._httpClient.get<Details>(url, httpOptions);
    }

    public getValidTobaccoClasses(permittype: string, ein: string, fiscalYear: string): Observable<string[]> {
        let url = `${this.getTrueupUrl}/compare/delta/nonzerotobaccoclass/${ein}/${["IMPT","Importer"].includes(permittype)? "IMPT":"MANU"}/${fiscalYear}`;
        return this._httpClient.get<string[]>(url, httpOptions);
        // return of(['Cigarettes']);
    }

    //Only used for single file ingestion 
    public getAllDeltaDetails(permitType: string, fiscalYr: string, ein: string): Observable<any[]> {
        let url = this.getTrueupUrl + "/compare/allDeltasDetails/" + ein + "/" + permitType + "/" + fiscalYr;
        return this._httpClient.get<any[]>(url, httpOptions);
    }
    /**
     * To get the detail file data
     * @param fileType 
     * @param companyId 
     * @param ein 
     * @param fiscalYear 
     * @param quarter 
     * @param tobaccoType 
     */
    public getIngestedSummaryExport(fileType: string, companyId: number | string, ein: string, fiscalYear: number, quarter: string | number, tobaccoType: string, ingestionType) {
        let url = this.getTrueupUrl + "/export/summary/" + fileType + "/" + companyId + "/" + ein + "/" + fiscalYear + "/" + quarter + "/" + tobaccoType + "/" + ingestionType;
        return this._httpClient.get<any>(url, httpOptions);
    }

    
    loadPermitDocs(cbpCommentSeq: number, ttbCommentSeq:number, allDeltaCommentSeq:number): Observable<string> {
        return this._httpClient.get(this.commentAttachmentURL + 'cbpCommentSeq/' + cbpCommentSeq  + '/ttbCommentSeq/' + ttbCommentSeq  + '/allDeltaCommentSeq/' + allDeltaCommentSeq, httpOptions).pipe(
            tap(_ => this.log("loadDocs")),
            catchError(this.handleError<any>("loadDocs"))
        );
    }

    uploadPermitAttachment(document: any): Observable<string> {
        let params = new HttpParams();

        const options = {
            params: params,
            reportProgress: true,
        };

        return this._httpClient.post(this.commentAttachmentURL, (document), options).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }
	downloadPermitAttachment(docid: number): Observable<Document> {
        return this._httpClient.get(this.commentAttachmentURL + docid, httpOptions).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }
    deletePermitAttachment(docid: number): Observable<string> {
        return this._httpClient.delete(this.commentAttachmentURL + docid, httpOptions).pipe(
            tap(_ => this.log("uploadAttachment")),
            catchError(this.handleError<any>("uploadAttachment"))
        );
    }
    setLinkAuthentication(flag: boolean) {
        flag ? this.authService.setAccessTokenCookie() : this.authService.removeAccessTokenCookie();
    }
    /**
     * To get the detail file data
     * @param fileType 
     * @param companyId 
     * @param ein 
     * @param fiscalYear 
     * @param quarter 
     * @param tobaccoType 
     */
    public getMissingEntryExport(fileType: string, companyId: number | string, ein: string, fiscalYear: number, quarter: string | number, tobaccoType: string) {
        let url = this.getTrueupUrl + "/export/summary/" + fileType + "/" + companyId + "/" + ein + "/" + fiscalYear + "/" + quarter + "/" + tobaccoType + "/" ;
        return this._httpClient.get<any>(url, httpOptions);
    }
    public processScheduleAB(fiscalYr: number) {
        let url = this.getTrueupUrl + "/"  + fiscalYr +"/runadjustments";
        return this._httpClient.post(url, httpOptions);
        
    }
    public getPreviousTrueupInfo(permitType:string,fiscalYr: number, ein:string) {
        let url = this.getTrueupUrl + "/compare/prevtrueupinfo/"  + permitType + "/" +fiscalYr + "/" + ein;
        return this._httpClient.get<any>(url, httpOptions);
    }
    /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log("${operation} failed: ${error.message}");

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Attribute Service message with the MessageService */
    private log(message: string) {
        //this.messageService.add("Profile Service: ${message}");
    }

}

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { environment } from "../../../../environments/environment";
import { Credentials } from "./../../../login/model/credentials.model";
import { AnnualTrueUpFilter } from "./../../../recon/annual/models/at-filter.model";
import { AnnualTrueUp } from "./../../../recon/annual/models/at.model";

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class AnnualTrueUpCreationService {
    private getTrueUpurl = environment.apiurl + '/api/v1/trueup';
    private getAnnualTrueUpsurl = environment.apiurl + '/api/v1/trueup/list';

    private credential: Credentials;

    constructor(private _httpClient: HttpClient) { }

    /**
     *
     *
     * @param {TrueUp} trueUp
     * @returns {Observable<String>}
     *
     * @memberOf CreateTrueUpService
     */
    createTrueUpAssessment(trueUp: AnnualTrueUp): Observable<String> {
        return this._httpClient.post(this.getTrueUpurl, trueUp, httpOptions).pipe(
			tap(_ => this.log("createTrueUpAssessment")),
			catchError(this.handleError<any>("createTrueUpAssessment"))
		);
    }


    getTrueUps(trueUpFilter: AnnualTrueUpFilter) {
        // let body = ({ trueUpFilter });
        // let headers = new Headers({ 'Content-Type': 'application/json' });
        // let options = new RequestOptions(httpOptions);
        // let credential = new Credentials('admin@test.com', 'admin');
        // this._authHelper.appendAuthorizationHeader(credential, headers);
        return this._httpClient.put(this.getAnnualTrueUpsurl, (trueUpFilter), httpOptions);
    }

    getFiscalYearsWithNewDeltas(): Observable<number[]> {
        return this._httpClient.get<number[]>(`${this.getTrueUpurl}/new/deltas`, httpOptions);
    }

    private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}

/*
@author : Deloitte
Service class to perform assessments related operations.
*/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { tap } from "rxjs/operators";
import { environment } from "../../../../environments/environment";

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

/**
 * 
 * 
 * @export
 * @class AnnualTrueUpMarketShareService
 */
@Injectable()
export class AnnualTrueUpMarketShareService {

    private getMarketShareurl = environment.apiurl + '/api/v1/trueup/marketshare';

    constructor(private _httpClient: HttpClient) { }

    getMarketShare(year: any): Observable<any> {
         let url = this.getMarketShareurl + "/" + year;
         return this._httpClient.get(url, httpOptions).pipe(
          tap(_ => this.log("getMarketShare"))
     );
    }

    generateMarketShare(year: any): Observable<string> {
         let url = this.getMarketShareurl + "/" + year;
         return this._httpClient.put<any>(url, null, httpOptions).pipe(
               tap(_ => this.log("generateMarketShare"))
               );
    }
  
    submitMarketShare(year: any): Observable<string> {
         let url = this.getMarketShareurl +"/submit/"+ year;
         return this._httpClient.put<any>(url, null, httpOptions).pipe(
          tap(_ => this.log("submitMarketShare"))
     );
    }

    	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}
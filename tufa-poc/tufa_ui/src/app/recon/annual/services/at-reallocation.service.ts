import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '../../../../../node_modules/@angular/common/http';
import { Subject } from '../../../../../node_modules/rxjs';
import { tap } from '../../../../../node_modules/rxjs/operators';
import { environment } from '../../../../environments/environment';
import { AnnualTrueUpCBPLineEntryUpdate } from '../models/at-cbplineentryupdate.model';
import { ReallocateModel } from '../models/at-reallocate.model';

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: 'root'
})
/** Servce for handling reallocation of ingested entry lines */
export class AtReallocationService {

  //Used for when the user click 'Mange'
  private reallocateEntryEvent = new Subject<ReallocateEventModel>();
  reallocateEntryEvent$ = this.reallocateEntryEvent.asObservable();

  //Used for when the user saves a reallocation
  private reallocateEntrySave = new Subject<any>();
  reallocateEntrySave$ = this.reallocateEntrySave.asObservable();

  private selectAllEntryEvent = new Subject<any>();
  selectAllEntryEvent$ = this.selectAllEntryEvent.asObservable();

  private resetSelectAllEntryEvent = new Subject<any>();
  resetSelectAllEntryEvent$ = this.resetSelectAllEntryEvent.asObservable();



  private trueupBaseUrl = environment.apiurl + '/api/v1/trueup';

  constructor(private _httpClient: HttpClient) { }

  /** Triger the event that will load the selected entries in the manage popup */
  public triggerReallocateEntryEvent(event: ReallocateEventModel): void {
    this.reallocateEntryEvent.next(event);
  }

  /** Trigger the event for indicating that the save was successful */
  public triggerReallocateEntrySaveEvent(action:any): void {
    this.reallocateEntrySave.next(action);
  }

  public triggerSelectAllEntryEvent(event: any): void {
    this.selectAllEntryEvent.next(event);
  }

  public triggerResetSelectAllEntryEvent(event: any): void {
    this.resetSelectAllEntryEvent.next(event);
  }


  public getCBPIngestedLineEntryInfo(cbEntryId: number) {
    let url = this.trueupBaseUrl + "/compare/getcbpentry/" + cbEntryId;
    return this._httpClient.get(url, httpOptions).pipe(tap(_ => console.debug("getCBPIngestedLineEntryInfo")));
  }

  public updateCBPIngestedLineEntryInfo(updateinfo: AnnualTrueUpCBPLineEntryUpdate[], fiscalYear) {
    let url = this.trueupBaseUrl + "/compare/updatecbpentry/" + fiscalYear;
    return this._httpClient.put(url, updateinfo, httpOptions).pipe(tap(_ => console.debug("updateCBPIngestedLineEntryInfo")));
  }
}

export class ReallocateEventModel {
  quarter: number;
  entries: ReallocateModel[];
}

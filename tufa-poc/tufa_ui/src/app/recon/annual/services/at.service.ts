
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { environment } from "../../../../environments/environment";
import { AuthService } from "../../../login/services/auth.service";
import { CBPMetrics } from "../models/at-cbp-metrics.model";
import { CBPAssociationModel } from '../models/at-cbpassociation.model';
import { AnnualTrueUpDocModel } from "../models/at-doc.model";
import { Company } from "./../../../company/models/company.model";
import { AnnualTrueUpCBPEntryModel } from "./../../../recon/annual/models/at-cbpentry.model";
import { AnnualTrueUpCmpyIngestionModel } from "./../../../recon/annual/models/at-cmpyingestion.model";
import { SubmissionDocument } from './../../../recon/annual/models/at-submission-doc.model';

const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class AnnualTrueUpService {

    private baseurl = environment.apiurl + '/api/';

    private basereconurl = environment.apiurl + '/api/v1/recon';

    private getTrueupDocsUrl = environment.apiurl + '/api/v1/trueup';

    private ingestFileUrl = environment.apiurl + '/api/v1/trueup/ingestfile';

    private getNewCBPCompanies = environment.apiurl + '/api/v1/trueup/exportNewCBPCompany/fiscalyear';

    private companyBucketURL = environment.apiurl + '/api/v1/trueup/bucket/company';
    // private getCmpyAssocErrUrl = environment.apiurl + '/api/v1/trueup/bucket/company/';
    // private saveCmpyAssociationUrl =  environment.apiurl + '/api/v1/trueup/bucket/company/';

    private unknownCompanyBucketURL = environment.apiurl + '/api/v1/trueup/bucket/unkncompany';
    // private getUnknCmpyUrl = environment.apiurl + '/api/v1/trueup/bucket/unkncompany';

    private htsAssociateBucketURL = environment.apiurl + '/api/v1/trueup/bucket/htsassociate';
    // private getHtsAssocErrUrl = environment.apiurl + '/api/v1/trueup/bucket/htsassociate';
    // private saveHtsAssociationUrl =   environment.apiurl + '/api/v1/trueup/bucket/htsassociate';

    private htsMissingBucketURL = environment.apiurl + '/api/v1/trueup/bucket/htsmissing';
    // private getHTSMissAssocErrUrl = environment.apiurl + '/api/v1/trueup/bucket/htsmissing';
    // private saveHTSMissAssociationUrl = environment.apiurl + '/api/v1/trueup/bucket/htsmissing';

    private ttbPermitsBucketURL = environment.apiurl + '/api/v1/trueup/bucket/ttbpermits';
    // private getTTBPermitsUrl = environment.apiurl + '/api/v1/trueup/bucket/ttbpermits';

    private includeCompanyInMarketShareBucketURL = environment.apiurl + '/api/v1/trueup/bucket/inclcmpymktsh';
    // private getInclCmpyMktShUrl = environment.apiurl + '/api/v1/trueup/bucket/inclcmpymktsh/';
    // private updateInclCmpyMktShUrl = environment.apiurl + '/api/v1/trueup/bucket/inclcmpymktsh/';
    // private createIngestedCompany = environment.apiurl + '/api/v1/trueup/bucket/inclcmpymktsh';
    // private getNewIngestedCompanyPermits = environment.apiurl + '/api/v1/trueup/bucket/inclcmpymktsh';

    private allDeltasAssociationURL = environment.apiurl + '/api/v1/trueup/compareall/delta/assotiation';
    // private getCompanyToAssociate = environment.apiurl + '/api/v1/trueup/compareall/delta/assotiation';

    private companyURL = environment.apiurl + '/api/v1/companies';
    // private saveupdatecompanyurl = environment.apiurl + '/api/v1/companies';

    constructor(private _httpClient: HttpClient, private authService: AuthService) { }

    makeFileRequest(httpMethod: string, url: string, formData: FormData) {
        return new Promise((resolve, reject) => {

            let xhr = new XMLHttpRequest();
            xhr.open("POST", url, true);

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.response);
                    } else {
                        reject(xhr.response);
                    }
                }
            }

            // Append JWT token


            let token = this.authService.getAccessToken();
            xhr.setRequestHeader('X-Authorization', 'Bearer ' + token);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send(formData);
        });
    }

    ingestFile(formData: FormData, fiscalYear: string): Promise<any> {
        return this.makeFileRequest("Post", this.ingestFileUrl + "/" + fiscalYear, formData);
    }

    getCBPNewCompanies(fiscalYear: string) {
        let url = this.getNewCBPCompanies + "/" + fiscalYear;
        return this._httpClient.get(url, httpOptions);
    }

    getTrueupDocs(fiscalYear: string): Observable<AnnualTrueUpDocModel[]> {
        let url = this.getTrueupDocsUrl + "/" + fiscalYear + "/documents";
        return this._httpClient.get<AnnualTrueUpDocModel[]>(url, httpOptions).pipe(
            tap(_ => this.log("getTrueupDocs")),
            catchError(this.handleError<any>("getTrueupDocs"))
        );
    }

    removeIngestedFile(fiscalYr: any, doctype: any) {
        let url = this.ingestFileUrl + '/' + doctype + '/' + fiscalYr;
        return this._httpClient.delete(url, httpOptions);
    }

    getIngestExportErrorDocument(fiscalYear: string, doctype: string): Observable<any> {
        let url = this.getTrueupDocsUrl + "/" + fiscalYear + "/errors/" + doctype;
        return this._httpClient.get(url, httpOptions);
    }

    getCompanyAssociationInfo(fiscalYr) {
        return this._httpClient.get(this.companyBucketURL + '/' + fiscalYr, httpOptions);
    }

    getUnknownCompanyBucketInfo(fiscalYr): Observable<any[]> {
        return this._httpClient.get(this.unknownCompanyBucketURL + '/' + fiscalYr, httpOptions).pipe(
            tap(_ => this.log("getUnknownCompanyBucketInfo")),
            catchError(this.handleError<any>("getUnknownCompanyBucketInfo"))
        );
    }


    saveCompanyAssociations(trueupImpArry) {
        return this._httpClient.put(this.companyBucketURL, trueupImpArry, httpOptions);
    }

    savePermitInclusions(trueupPermitArry: any[]) {
        return this._httpClient.put(this.ttbPermitsBucketURL, trueupPermitArry, httpOptions);
    }

    getHtsAssociationInfo(fiscalYr) {
        return this._httpClient.get(this.htsAssociateBucketURL + '/' + fiscalYr, httpOptions);
    }

    saveHtsAssociations(trueupImpArry: AnnualTrueUpCBPEntryModel[], fiscalYr) {
        return this._httpClient.put(this.htsAssociateBucketURL + "/" + fiscalYr, trueupImpArry, httpOptions);
    }

    getHTSMissAssociationInfo(fiscalYr) {
        return this._httpClient.get(this.htsMissingBucketURL + '/' + fiscalYr, httpOptions);
    }

    saveHTSMissAssociations(trueupImpArry: AnnualTrueUpCBPEntryModel[], fiscalYr) {
        return this._httpClient.put(this.htsMissingBucketURL + "/" + fiscalYr, trueupImpArry, httpOptions);
    }

    getTTBPermits(fiscalYr) {
        return this._httpClient.get(this.ttbPermitsBucketURL + '/' + fiscalYr, httpOptions);
    }

    public saveDoc(doc: SubmissionDocument) {
        let url = this.getTrueupDocsUrl + "/docs/" + doc.fiscalYr;
        return this._httpClient.put(url, doc, httpOptions);
    }

    public getSupportingDocs(fiscalYr: number): Observable<any[]> {
        let url = this.getTrueupDocsUrl + "/" + fiscalYr + "/docs";
        return this._httpClient.get(url, httpOptions).pipe(
            tap(_ => this.log("getSupportingDocs")),
            catchError(this.handleError<any>("getSupportingDocs"))
        );
    }

    public downloadDocument(trueupSubDocId: number) {
        let url = this.getTrueupDocsUrl + "/docs/" + trueupSubDocId;
        return this._httpClient.get(url, httpOptions);
    }

    /** Include Company in Market Share related service methods */
    public saveInclCmpyMktSh(inclCmpyMktShArry: AnnualTrueUpCmpyIngestionModel[], fiscalYear: number) {
        let url = this.includeCompanyInMarketShareBucketURL + "/" + fiscalYear;
        return this._httpClient.put(url, inclCmpyMktShArry, httpOptions);
    }

    public createCompany(company: Company): Observable<Company> {
        return this._httpClient.post<Company>(this.companyURL, (company), httpOptions).pipe(
            tap(_ => this.log("createCompany"))
        );
    }

    public getInclCmpyMktSh(fiscalYear: number) {
        let url = this.includeCompanyInMarketShareBucketURL + "/" + fiscalYear;
        return this._httpClient.get(url, httpOptions);

    }

    /**
     *
     *
     * @param {string} fiscalYr
     * @returns
     *
     * @memberOf QtrReconReportService
     */
    public getReconReportDetailsForFiscalYear(fiscalYr: string): Observable<any[]> {
        let url = this.baseurl + "reports/quarter/recon/fiscalyear/" + fiscalYr;
        return this._httpClient.get<Object[]>(url, httpOptions);
    }

    /**
     *
     *
     * @param {string} fiscalYr
     * @returns
     *
     * @memberOf QtrReconReportService
     */
    public exportReconReportData(fiscalYr: string) {
        let url = this.basereconurl + "/export/fiscalyear/" + fiscalYr;
        return this._httpClient.get(url, httpOptions);
    }

    public getNewIngestedCompany(fiscalYr: string, ein: string, type: string) {
        let url = this.includeCompanyInMarketShareBucketURL + "/" + fiscalYr + "/cmpy/" + ein + '/' + type;
        return this._httpClient.get(url, httpOptions);
    }

    public createNewIngestedCompany(company: Company) {
        let url = this.includeCompanyInMarketShareBucketURL;
        return this._httpClient.post(url, company, httpOptions);
    }

    associateEntryToParent(association: CBPAssociationModel): any {
        let url = this.allDeltasAssociationURL;
        return this._httpClient.post(url, association, httpOptions);
    }

    associateAllEntriesToParent(association: CBPAssociationModel): any {
        let url = this.allDeltasAssociationURL + '/all';
        return this._httpClient.post(url, association, httpOptions);
    }

    resetAssotiation(association: CBPAssociationModel): any {
        let url = this.allDeltasAssociationURL + '/reset';
        return this._httpClient.post(url, association, httpOptions);
    }

    /**
     * Generate metrics for Annual Ingest
     * @argument fiscalYr to generate metrics for
     * @returns Metrics data for ingested data 
     */
    public generateMetrics(fiscalYr: string): Observable<CBPMetrics> {
        //create url
        let url = this.ingestFileUrl + "/metrics/" + fiscalYr;
        return this._httpClient.post<CBPMetrics>(url, {}, httpOptions);
    }

    public getMetricData(fiscalYr: string): any {
        let url = this.ingestFileUrl + "/metrics/" + fiscalYr;
        return this._httpClient.get<CBPMetrics>(url, httpOptions);
    }

    /** Also available in at-compare.service.ts */
    public getIngestedSummaryExport(fileType: string, companyId: number | string, ein: string, fiscalYear: number, quarter: string | number, tobaccoType: string, ingestionType: string) {
        let url = this.getTrueupDocsUrl + "/export/summary/" + fileType + "/" + companyId + "/" + ein + "/" + fiscalYear + "/" + quarter + "/" + tobaccoType + "/" + ingestionType;
        return this._httpClient.get<any>(url, httpOptions);
    }

    public exportSummaryIngestedDeltas(companyId: string, tobaccoType: string, quarter: string, fiscalYr: number) {
        let url = this.getTrueupDocsUrl + '/export/summaryIngestedDeltas/' + companyId + "/" + tobaccoType + "/" + quarter + "/" + fiscalYr;
        return this._httpClient.get(url, httpOptions);
    }

    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log("${operation} failed: ${error.message}");

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Attribute Service message with the MessageService */
    private log(message: string) {
        //this.messageService.add("Profile Service: ${message}");
    }

    public downloadUnmatchedRecords(fiscalYr: string, recordType: string) {
        let url = this.getTrueupDocsUrl + "/export/unmatched/" + recordType + "/" + fiscalYr;
        return this._httpClient.get(url, httpOptions).pipe(
            tap(_ => this.log("downloadUnmatchedRecords")),
            catchError(this.handleError<any>("downloadUnmatchedRecords"))
        );
    }
}

import { Pipe, PipeTransform } from "@angular/core";
import { retry } from "rxjs/operators";

/**
 * 
 * 
 * @export
 * @class PermitReportsDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "delta"
})
export class DeltaPipe implements PipeTransform {
    transform(value: any, ...args: any[]) {
        if (args.length = 2) {
            if(args[0]==="0.00" || args[1]==="0.0" || args[1]==="0" || args[1]==="0.00" || args[0]==="0.0" || args[0]==="0")
             return value;
            else if (this.isNA(args[0]) && !this.isNA(args[1])) {
                return " Ingest Only " + value
            } else if (!this.isNA(args[0]) && this.isNA(args[1])) {
                return " TUFA Only " + value
            }
            // if (this.isOneSided(args[0], args[1])) {
            //     return value + " All Deltas"
            // }
        }

        return value;
    }

    isOneSided(arg1, arg2): boolean {
        return (this.isNA(arg1) && !this.isNA(arg2)) || (!this.isNA(arg1) && this.isNA(arg2));
    }

    isNA(value) {
        return value == null || isNaN(value) || value == 'NA';
    }

}
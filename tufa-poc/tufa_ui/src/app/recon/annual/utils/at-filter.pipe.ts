import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { AnnualTrueUpFilter, AnnualTrueUpFilterData } from "./../../../recon/annual/models/at-filter.model";

/**
 * 
 * 
 * @export
 * @class PermitReportsDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "trueuppipe"
})
export class AnnualTrueUpDataFilterPipe implements PipeTransform {

    /**
     * 
     * 
     * @param {any[]} data
     * @param {AnnualFilter} filter
     * @returns {*}
     * 
     * @memberOf PermitReportsDataFilterPipe
     */
    transform(data: any[], filter: AnnualTrueUpFilterData): any {
        let results = data;

        if (data) {
            if (filter) {
                // filter by years
                if (typeof filter.reportYears !== 'undefined' && filter.reportYears.length > 0) {
                   results =  _.filter(results, function(item) {
                        return _.includes(Array.from(filter.reportYears), item.fiscalYear);
                    });
                }
                // filter by status
                if (typeof filter.reportStatus !== 'undefined' && filter.reportStatus.length > 0) {
                   results =  _.filter(results, function(item) {
                        return _.includes(Array.from(filter.reportStatus), item.submittedInd);
                    });
                }
            }
        }
        return results;
    }
// tslint:disable-next-line:eofline
}
import { Directive, ElementRef, Input, Renderer2, SimpleChanges } from "@angular/core";
import { AcceptanceFlagState } from "../models/at-acceptance-flag-state.model";

@Directive({
    selector: '[compare-icon]'
})
export class CompareIconDirective {

    @Input() status: string;

    constructor(private renderer: Renderer2, private el: ElementRef) {
    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "status") {
                let chg = changes[propName];
               // Accpetance flag will be set to empty string when 
               //delta change through at-compare-details-quarter-panel.component.ts
                if(chg.currentValue == '')
                this.removeFaClass();

                if (chg.currentValue) {
                    this.status = chg.currentValue;
                    this.setIcon();
                }
            }
        }
    }

    private setIcon() {
        this.removeFaClass();
        if (this.status == AcceptanceFlagState.IN_PROGRESS) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-repeat');
            this.el.nativeElement.style.color = "#f0b518";
        } else if (this.status == AcceptanceFlagState.ACCEPT_FDA) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-check-circle');
            this.renderer.addClass(this.el.nativeElement, 'text-success');
        } else if (this.status == AcceptanceFlagState.ACCEPT_INGESTED) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-check-circle');
            this.renderer.addClass(this.el.nativeElement, 'text-fun');
        } else if (this.status == AcceptanceFlagState.AMEND) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-wrench');
            this.renderer.addClass(this.el.nativeElement, 'text-primary');
        } else if (this.status == AcceptanceFlagState.MIXEDACTION) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-wrench');
            this.renderer.addClass(this.el.nativeElement, 'text-primary');
        } 
        else if (this.status == AcceptanceFlagState.ASSOCIATED) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-refresh');
            this.renderer.addClass(this.el.nativeElement, 'text-primary');
        } 
        else if (this.status == AcceptanceFlagState.MSREADYTUFA) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-check-circle');
            this.renderer.addClass(this.el.nativeElement, 'text-success');
        }
        else if (this.status == AcceptanceFlagState.MSREADYINGESTED) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-check-circle');
            this.renderer.addClass(this.el.nativeElement, 'text-fun');
        } 
        else if (this.status == AcceptanceFlagState.EXCLUDED) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-ban');
            this.el.nativeElement.style.color = "#73747a";
        }
        else if (this.status == AcceptanceFlagState.INPROGRESS) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-repeat');
            this.el.nativeElement.style.color = "#f0b518";
        } 
        else if (this.status == AcceptanceFlagState.DELTA_CHANGE) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-chain-broken');
            this.el.nativeElement.style.color = "red";
        } else if (this.status == AcceptanceFlagState.DELTA_CHANGE_EXCLUDE) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-chain-broken');
            this.el.nativeElement.style.color = "darkgrey";
        } else if (this.status == AcceptanceFlagState.DELTA_CHANGE_TRUE_ZERO) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-chain-broken');
            this.el.nativeElement.style.color = "darkgrey";
        }else if (this.status == AcceptanceFlagState.PERMITEXCLUDED) {
            this.renderer.addClass(this.el.nativeElement, 'fa');
            this.renderer.addClass(this.el.nativeElement, 'fa-ban');
            this.el.nativeElement.style.color = "#73747a";
        }
    }

    private removeFaClass() {
        this.el.nativeElement.className = "";
    }
}
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { AnnualTrueUpFilter, AnnualTrueUpFilterData } from "./../../../recon/annual/models/at-filter.model";
import { AnnualTrueUpReconFilterData } from "./../../../recon/annual/models/at-reconfilter.model";

/**
 * 
 * 
 * @export
 * @class PermitReportsDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "atreconpipe"
})
export class AnnualTrueUpReconDataFilterPipe implements PipeTransform {

    /**
     * 
     * 
     * @param {any[]} data
     * @param {AnnualFilter} filter
     * @returns {*}
     * 
     * @memberOf PermitReportsDataFilterPipe
     */
    transform(data: any[], filter: AnnualTrueUpReconFilterData): any {
        let results = data;

        if (data) {
            if (filter) {
                // filter by status
                // if (typeof filter.reportStatus !== 'undefined' && filter.reportStatus.length > 0) {
                //    results =  _.filter(results, function(item) {
                //         return _.includes(Array.from(filter.reportStatus), item.reconStatus);
                //     });
                // }
                // filter by companyname
                if (typeof filter.companyFilter !== 'undefined' && filter.companyFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1:string = item.companyName
                        let val2:string = filter.companyFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                }   
                // filter by permit #
                if (typeof filter.permitFilter !== 'undefined' && filter.permitFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1:string = item.permitNum; 
                        val1 = val1.replace(/-/g, '');
                        let val2:string = filter.permitFilter;
                        val2 = val2.replace(/-/g, '');
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                }    
                // filter by month
                if (typeof filter.monthFilter !== 'undefined' && filter.monthFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1:string = item.month; 
                        let val2:string = filter.monthFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                }     
                // filter by status
                if (typeof filter.statusFilter !== 'undefined' && filter.statusFilter.length > 0) {
                   results =  _.filter(results, function(item) {
                        let val1:string = item.reconStatus; 
                        let val2:string = filter.statusFilter;
                        return (val1.toUpperCase() === val2.toUpperCase());
                    });
                }  
                
               // filter by reported Tobacco
               if (typeof filter.reportedTobaccoFilter !== 'undefined' && filter.reportedTobaccoFilter.length > 0) {
                results = _.filter(results, function (item) {
                    if (item.reportedTobacco != null) {

                        let val1: string = item.reportedTobacco;
                        let val2: string = filter.reportedTobaccoFilter;

                        if (val2.includes('||')) {
                            var newaray = filter.reportedTobaccoFilter.split("||")
                            let val3: string = newaray[0];
                            let val4: string = newaray[1]
                            return (val1.toUpperCase() === val3.toUpperCase() || val1.toUpperCase() === val4.toUpperCase())
                        }


                        return (val1.toUpperCase() === val2.toUpperCase());
                    } else {
                        return false;
                    }
                });
             }
            }
        }
        return results;
    }
// tslint:disable-next-line:eofline
}
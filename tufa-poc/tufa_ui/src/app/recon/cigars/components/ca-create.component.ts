import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { CigarsAssessmentsCreationService } from "./../../../recon/cigars/services/ca-create.service";
import { CigarsAssessment } from "./../../../recon/cigars/models/ca.model";
import { ShareDataService } from "./../../../core/sharedata.service";
import { Subscription } from 'rxjs';

declare var jQuery: any;
declare var window: any;

@Component({
    selector: '[ca-create]',
    templateUrl: './ca-create.template.html',
    providers: [CigarsAssessment],
    encapsulation: ViewEncapsulation.None
})

export class CigarsAssessmentsCreationPage implements OnInit {
    busy: Subscription;
    fiscalYear: string;
    router: Router;

    alerts: Array<Object>;
    showAlertFlag: boolean = false;

    tabfrom: string;
    shareData: ShareDataService;

    constructor(private _createQuarters: CigarsAssessmentsCreationService,
            router: Router,
            private _assessment: CigarsAssessment,
            private _shareData: ShareDataService) {

        this.alerts = [
            {
                type: 'success',
                msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Successfully generated Cigar Assessment.</span>'
            }
        ];

        this.router = router;
        this.showAlertFlag = false;
        this.shareData = _shareData;
    }

    ngOnInit() {
        this.showAlertFlag = false;
        this.getParamsForReconNavigation();
    }

    createCigarAssessment(x) {
        this.showAlertFlag = false;
        jQuery('#create-cigar-assessment-form').parsley().validate();

        this._assessment.assessmentYr = x;
        this._assessment.submittedInd = 'N';
        if (jQuery('#create-cigar-assessment-form').parsley().isValid()) {
            this._createQuarters.saveCigarAssessment(this._assessment).subscribe(data => {
                if (data.toString() === '0') {
                    this.alerts[0]['type'] = 'danger';
                    // tslint:disable-next-line:max-line-length
                    this.alerts[0]['msg'] = '<i class="fa fa-circle text-danger" ></i><span class="alert-text">FY' + x + ' Cigar Assessment already exists</span>';
                } else {
                    this.alerts[0]['type'] = 'success';
                    // tslint:disable-next-line:max-line-length
                    this.alerts[0]['msg'] = '<i class="fa fa-circle text-success" ></i><span class="alert-text">Successfully created the FY' + x + ' Cigar Assessment</span>';
                }
                this.showAlertFlag = true;
            }, err => {
                this.alerts[0]['type'] = 'danger';
                // tslint:disable-next-line:max-line-length
                this.alerts[0]['msg'] = '<i class="fa fa-circle text-danger" ></i><span class="alert-text">Missing criteria: Unable to create the Cigar Assessment</span>';
                this.showAlertFlag = true;
            });
        }
    }

    setboxfocus(elementID: String): void {

        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let searchInput = jQuery(elementID);
        searchInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

    closeAlert(): void {
        this.showAlertFlag = false;
    }

    gotoAssessments() {
        this.setParamsForReconNavigation();
    }

    setParamsForReconNavigation() {
        this.shareData.reset();
        this.shareData.qatabfrom = this.tabfrom;
    }

    getParamsForReconNavigation() {
        this.tabfrom = this.shareData.qatabfrom;
    }
}

import { AfterViewInit, Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ShareDataService } from '../../../core/sharedata.service';
import { TabsetComponent } from '../../../../../node_modules/ngx-bootstrap';

declare var jQuery: any;

@Component({
    selector: '[ca-home]',
    templateUrl: './ca-home.template.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./ca-home.style.scss']
})

export class CigarsAssessmentsHomePage implements OnInit, AfterViewInit {
    @ViewChild('caTabs') caTabs: TabsetComponent;

    paramsSub: any;

    router: Router;
    assessmentId: any;

    shareData: ShareDataService;

    months: string[];
    quarter: number;
    fiscalYear: number;
    submitted: string;
    cigarsubmitted: string;
    createdDt: string;
    qatabfrom: string;

    constructor(router: Router,
        private activatedRoute: ActivatedRoute,
        private _shareData: ShareDataService
    ) {
        this.router = router;
        this.shareData = _shareData;
    }

    ngOnInit(): void {
        this.getParamsForReconNavigation();
    }

    ngAfterViewInit() {
        setTimeout(() => {
            if (this.qatabfrom === 'ca-reconreport') {
                this.caTabs.tabs[1].active = true;
            }
        }, 100);
    }

    gotoAssessments() {
        this.setParamsForReconNavigation();
    }

    setParamsForReconNavigation() {
        this.shareData.reset();
        this.shareData.assessmentId = this.assessmentId;
        this.shareData.months = this.months;
        this.shareData.quarter = this.quarter;
        this.shareData.fiscalYear = this.fiscalYear;
        this.shareData.submitted = this.submitted;
        this.shareData.cigarsubmitted = this.cigarsubmitted;
        this.shareData.createdDate = this.createdDt;
        this.shareData.qatabfrom = this.qatabfrom;
    }

    getParamsForReconNavigation() {
        this.assessmentId = this.shareData.assessmentId;
        this.months = this.shareData.months;
        this.quarter = this.shareData.quarter;
        this.fiscalYear = this.shareData.fiscalYear;
        this.submitted = this.shareData.submitted;
        this.cigarsubmitted = this.shareData.cigarsubmitted;
        this.createdDt = this.shareData.createdDate;
        this.qatabfrom = this.shareData.qatabfrom;
    }

    onSubmitted(data: any) {
        //to look
        //this.submitted = data.submittedInd;
        //this.cigarsubmitted = data.cigarSubmittedInd;
    }

}

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from "lodash";
import { Subscription } from 'rxjs';
import { ShareDataService } from '../../../core/sharedata.service';
import { DownloadSerivce } from '../../../shared/service/download.service';
import { CigarAssessmentSupportingDocument } from '../models/ca-supportdoc.model';
import { MarketShareData, MarketShareVersion } from '../models/market-share-data.model';
import { CigarsAssessmentsService } from '../services/ca.service';
import { CigarsAssessmentsMarketShareService } from "./../../../recon/cigars/services/ca-marketshare.service";


declare var jQuery: any;

@Component({
    selector: '[ca-marketshare]',
    templateUrl: './ca-marketshare.template.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./ca-marketshare.style.scss']
})

export class CigarsAssessmentsMarketShareComponent implements OnInit, OnDestroy {

    @Input() fiscalYear;
    @Input() quarter;
    @Input() submitted;
    @Input() cigarsubmitted;
    @Output() onSubmitted = new EventEmitter<any>();

    busyGen: Subscription[] = [null, null, null, null];
    busySub: Subscription;

    // support docs
    busyload: Subscription;
    busyupload: Subscription;
    busyDownload: Subscription;
    busyHistorical: Subscription;
    sortBy = "versionNum";

    mktsharealerts: Array<Object> = [];
    submitalerts: Array<Object> = [];

    mktsharedata: MarketShareData = new MarketShareData();
    mktsharedataArrayForYear: MarketShareData[] = [];
    quarterVersions: number[][] = [];
    selVersions: MarketShareVersion[] = [null, null, null, null];
    selVersionsNumber: Date[] = [null, null, null, null];
    zeroVersions: MarketShareVersion[] = [null, null, null, null];
    zeroVersionsNumber: Date[] = [null, null, null, null];
    selMarketShareTypes: string[] = ['FULLMS', 'FULLMS', 'FULLMS', 'FULLMS'];
    showHistory: boolean[] = [false, false, false, false];
    showSummary: boolean[] = [false, false, false, false];
    isEnabled: boolean[] = [true, false, false, false];

    shareData: ShareDataService;

    // supporting documents
    docs: any[] = [];
    versionArray: any[];
    assessmentId: number;

    showSubmit: boolean = false;
    showAddDocumentPopup: boolean = false;
    submitBtnDisabled: boolean = false;

    selVersionDate: Date;
    selVersionAuthor: string;
    selVersionMarketShareType: string;
    selVersionCigarQtr: number;

    mktshareselectqtr: string;
    mktshareselectqtr2: string;
    mktshareselectqtr3: string;
    mktshareselectqtr4: string;
    marketsharetype: string;
    previousmarketsharetype: string;


    addressonlySelection: boolean;

    constructor(private marketShareService: CigarsAssessmentsMarketShareService,
        private docService: CigarsAssessmentsService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private _shareData: ShareDataService,
        private downloadService: DownloadSerivce) {

        this.shareData = _shareData;
    }

    // Returns the number of Y matches in the cigarsubmitted index
    getCigarSubmittedCount(submitindex: string) {
        let count = 0;
        if (submitindex) {
            let variable = new RegExp('Y', "gi");
            let matchstring = submitindex.match(variable);
            if (matchstring) {
                count = matchstring.length;
            }
        }
        return count;
    }

    ngOnInit(): void {
        // this.showSummary = false;
        this.quarter = this.shareData.quarter;
        this.fiscalYear = this.shareData.fiscalYear;
        this.submitted = this.shareData.submitted;
        this.cigarsubmitted = this.shareData.cigarsubmitted;
        this.assessmentId = this.shareData.assessmentId;
        this.getHistoricalSubmissionDetails();

        // If no submission took place
        if (this.getCigarSubmittedCount(this.cigarsubmitted) === 0) {

        }
        else {
            this.marketsharetype = "";
            this.mktshareselectqtr = "";
        }
    }

    ngOnDestroy() {
        this.busyGen.forEach(sub => {
            if (sub) sub.unsubscribe();
        });
        if (this.busyDownload) {
            this.busyDownload.unsubscribe();
        }
        if (this.busySub) {
            this.busySub.unsubscribe();
        }
        if (this.busyload) {
            this.busyload.unsubscribe();
        }
        if (this.busyupload) {
            this.busyupload.unsubscribe();
        }
    }

    onChangeMarketShareType(newValue) {
        if (newValue !== '') {
            this.marketsharetype = newValue;
            if (this.marketsharetype == 'ACONLY') {
                let uncheck = false;
                if (this.previousmarketsharetype != "") {
                    this.mktshareselectqtr = '';
                }
            } else if (this.marketsharetype == 'FULLMS' && this.previousmarketsharetype != "") {
                this.mktshareselectqtr = '';
            }
            for (let i = 0; i < this.zeroVersions.length; i++) {
                this.zeroVersions[i] = null;
                this.zeroVersionsNumber[i] = null;
            }
            this.showSubmit = false;
            this.previousmarketsharetype = this.marketsharetype;
        }
    }

    /**
     * Step 1: Load already submited marketshares
     * 
     * @see generateMaketShare()
     * @see submitMarketShare()
     */
    getHistoricalSubmissionDetails() {

        if (this.busyHistorical) {
            this.busyHistorical.unsubscribe();
        }

        this.busyHistorical = this.marketShareService.getMarketShareByYear(this.fiscalYear).subscribe(data => {
            let _this = this;
            this.mktsharedataArrayForYear = _.orderBy(data, ['assessmentQtr'], ['asc']);

            for (let marketShare of this.mktsharedataArrayForYear) {
                this.updateQuarterMarketshareData(marketShare);

                let versions = [];
                for (let ver of marketShare.versions) {
                    if (ver.versionNum != 0)
                        versions.push(ver.versionNum);
                }
                this.quarterVersions.push(versions);
                console.log(this.quarterVersions);
            }


            //enable AC Only selections 
            this.enableACOnlySelection();

            this.loadSupportingDocs(this.fiscalYear);

            if (this.mktsharedataArrayForYear[0] && this.mktsharedataArrayForYear[0].approvalMessage) {
                this.mktsharealerts.push({
                    type: 'warning',
                    msg: '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + this.mktsharedataArrayForYear[0].approvalMessage + '</span>'
                });
            } else {
                this.mktsharealerts = [];
            }
            this.onSubmitted.emit(this.mktsharedata);
        }, error => {
            this.mktsharealerts = [];
            // tslint:disable-next-line:max-line-length
            this.mktsharealerts.push({ type: 'warning', msg: '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + error + '</span>' });
        });
    }

    /**
     * Step 2: Generate marketshares V0
     * 
     * @see getHistoricalSubmissionDetails()
     * @see submitMarketShare()
     */
    generateMaketShare(curqtr: number) {

        // since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
        if (this.busyGen[curqtr - 1]) {
            this.busyGen[curqtr - 1].unsubscribe();
        }

        this.busyGen[curqtr - 1] = this.marketShareService.generateMarketShare(this.fiscalYear, curqtr, this.selMarketShareTypes[curqtr - 1]).subscribe(data => {
            this.mktsharedata = data;
            this.updateAfterGenerate(this.mktsharedata);

            if (this.mktsharedata.approvalMessage) {
                let submitmsg = " Report cannot be submitted until all Monthly Reports are Approved. Please re-generate the Market Share Data after " +
                    "approving all monthly reports.";

                this.mktsharealerts = [{
                    type: 'warning',
                    msg: '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + this.mktsharedata.approvalMessage + '</span>'
                }];

                this.submitalerts = [{
                    type: 'warning',
                    msg: submitmsg
                }];
                this.showSubmit = false;
            } else {
                this.mktsharealerts = [];
                this.submitalerts = [];
                this.showSubmit = true;
                this.blockSubmit();
            }
            this.onSubmitted.emit(this.mktsharedata);
        }, error => {
            this.mktsharealerts = [];
            // tslint:disable-next-line:max-line-length
            this.mktsharealerts.push({ type: 'warning', msg: '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + error + '</span>' });
        });
    }

    /**
     * Step 3: Submit marketshare V0 as the next version
     * 
     * @see getHistoricalSubmissionDetails()
     * @see generateMaketShare()
     */
    submitMarketShare(curqtr: number) {

        // since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
        if (this.busySub) {
            this.busySub.unsubscribe();
        }

        this.busySub = this.marketShareService.submitMarketShare(this.fiscalYear, curqtr, this.selMarketShareTypes[curqtr - 1]).subscribe(data => {
            this.updateQuarterMarketshareData(data);
            this.enableACOnlySelection();

            this.submitalerts = [];

            this.onSubmitted.emit(this.mktsharedata);
            this.clearUserSelectionAndDisableGenerate();

        }, error => {
            this.submitalerts = [];
            // tslint:disable-next-line:max-line-length
            this.submitalerts.push({ type: 'warning', msg: '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + error + '</span>' });
        });
    }

    enableACOnlySelection() {

        if (this.showHistory.length) {
            //check if atleast one of the qtrs has a MktSh submitted
            this.addressonlySelection = this.showHistory.reduce((previousVal, currentVal) => {
                return previousVal || currentVal;
            });
        }
    }

    /**
     * Update the history
     */
    updateQuarterMarketshareData(marketShare: MarketShareData) {
        this.resetZeroVersions();

        let index = marketShare.assessmentQtr - 1;
        marketShare.versions = _.orderBy(marketShare.versions, ['versionNum'], ['desc']);
        this.selVersions[index] = _.first(marketShare.versions);
        if(this.selVersions[index])
            this.selVersionsNumber[index] =  new Date(this.selVersions[index].createdDt);
        this.showSummary[index] = false;
        this.showHistory[index] = marketShare.versions[0] ? marketShare.versions[0].versionNum != 0 : false;
        if (index != 4) this.isEnabled[index + 1] = this.showHistory[index];

        this.mktsharedataArrayForYear[index] = marketShare;
    }

    /**
     * Update the summary
     */
    updateAfterGenerate(marketShare: MarketShareData) {
        this.resetZeroVersions();

        let index = marketShare.assessmentQtr - 1;
        let zeroVersion = _.find(marketShare.versions, function (o) { return (<any>o).versionNum === 0; });
        this.zeroVersions[index] = zeroVersion;
        this.zeroVersionsNumber[index] = new Date(this.zeroVersions[index].createdDt);
        this.showSubmit = marketShare.approvalMessage == null;
    }

    showAnyHistory(): boolean {
        let show = false;
        this.showHistory.forEach(bool => {
            show = bool || show
        });
        return show;
    }

    /**
     * Clear out all zero versions
     */
    resetZeroVersions() {
        this.zeroVersions.forEach((version, index) => {
            this.zeroVersions[index] = null;
            this.zeroVersionsNumber[index] = null;
        });
    }

    private blockSubmit() {
        if (this.mktsharedata.annualTrueUpSubmittedMsg) {
            this.submitBtnDisabled = true;
            this.submitalerts.push({ type: 'warning', msg: this.mktsharedata.annualTrueUpSubmittedMsg });
        }
    }


    clearUserSelectionAndDisableGenerate() {
        this.marketsharetype = "";
    }

    isMktShareDataAvailable(marketShareVersion: MarketShareVersion, curqtr) {
        return marketShareVersion && marketShareVersion.exports && marketShareVersion.exports['Cigars'] && marketShareVersion.exports['Cigars'][0];
    }

    downloadExport(marketShareVersion: MarketShareVersion) {
        if (marketShareVersion && marketShareVersion.exports) {
            this.downloadService.download(marketShareVersion.exports['Cigars'][0].csv,
                marketShareVersion.exports['Cigars'][0].title, 'csv/text');
        }
    }


    // ---------------------------- Document Area --------------------------------------

    setSelectedDocument() {
        this.showAddDocumentPopup = true;
    }

    loadSupportingDocs(fiscalYr: number) {
        this.busyload = this.docService.getSupportingDocs(fiscalYr).subscribe(data => {
            this.docs = <any[]>data;
        });
    }

    downloadDoc(asmntdocId: number) {
        this.busyDownload = this.docService.downloadDocument(asmntdocId).subscribe(data => {
            if (data) {
                this.downloadService.download(atob((<any>data).assessmentPdf), (<any>data).filename, "application/json");
            }
        });
    }

    deleteDoc(asmntdocId: number) {
        this.docService.deleteDocument(asmntdocId).subscribe(data => {
            if (data) {
                this.loadSupportingDocs(this.fiscalYear);
            }
        });
    }

    documentsGridRefresh(doc: any) {
        let document = new CigarAssessmentSupportingDocument();
        document.documentNumber = doc.documentNumber;
        document.filename = doc.filename;
        document.assessmentPdf = doc.assessmentPdf;
        document.dateUploaded = doc.dateUploaded;
        document.description = doc.description;
        document.cigarQtr = doc.cigarQtr;
        document.versionNum = doc.versionNum;
        if (doc) {
            for (let i = 0; i < this.mktsharedataArrayForYear.length; i++) {
                if (this.mktsharedataArrayForYear[i].assessmentQtr == doc.cigarQtr) {
                    document.assessmentId = this.mktsharedataArrayForYear[i].assessmentId
                    break;
                }
            }
            this.busyupload = this.docService.saveDoc(document, this.fiscalYear).subscribe(data => {
                if (data) {
                    this.loadSupportingDocs(this.fiscalYear);
                }
            }, err => {
                console.log("file upload failed: ");
            });
        }
    }
}

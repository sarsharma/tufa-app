import { Component, ViewEncapsulation, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { NgBusyModule } from 'ng-busy';
import { ShareDataService } from "./../../../core/sharedata.service";
import { CigarsAssessmentsService } from "./../../../recon/cigars/services/ca.service";

declare var jQuery: any;

@Component({
    selector: '[ca-reportstatus]',
    templateUrl: './ca-reportstatus.template.html',
    encapsulation: ViewEncapsulation.None,
    providers: [CigarsAssessmentsService]
})

export class CigarsAssessmentsReportStatusComponent implements OnInit, OnDestroy {

    paramsSub: any;
    showErrorFlag: boolean;
    alerts: Array<Object>;
    busy: Subscription;

    shareData: ShareDataService;

    month: string;
    months: string[];
    quarter: number;
    fiscalYear: number;
    submitted: string;
    cigarsubmitted: string;
    createdDate: string;
    assessmentId: any;

    data: any;
    result: any;

    public sortResultsBy = "permitNum";
    public sortResultsOrder = ["asc"];

    constructor(private router: Router,
        private activatedRoute: ActivatedRoute,
        private cigarReportStatusService: CigarsAssessmentsService,
        private _shareData: ShareDataService
        ) {
        this.router = router;
        this.showErrorFlag = false;
        this.shareData = _shareData;
    }

    ngOnInit(): void {
        this.showErrorFlag = false;

        this.months = this.shareData.months;
        this.fiscalYear = this.shareData.fiscalYear;
        this.submitted = this.shareData.submitted;
        this.cigarsubmitted = this.shareData.cigarsubmitted;
        this.createdDate = this.shareData.createdDate;
        this.assessmentId = this.shareData.assessmentId;

        this.getReportStatus(this.fiscalYear);
    }

    ngOnDestroy(){
        if(this.busy){
            this.busy.unsubscribe();
        }
    }

    getReportStatus(year: any) {

        this.showErrorFlag = false;

        this.busy = this.cigarReportStatusService.getReportStatus(year).subscribe(response => {
            if (response) {
                this.data = response;
            }
        },
            error => {
                this.showErrorFlag = true;
                this.alerts = [];
                this.alerts.push({ type: 'warning', msg: error });
            });
    }

    getStatus(quarterRpts, month) {
        let returnStr = '';
        for (let i = 0; i < quarterRpts.length; i++) {
            if (quarterRpts[i].month.toLowerCase() === month.substring(0, 3).toLowerCase()) {
                returnStr = quarterRpts[i].periodStatusTypeCd;
            }
        }
        return returnStr;
    }

    getPeriodId(quarterRpts, month) {
        let returnStr = '';
        for (let i = 0; i < quarterRpts.length; i++) {
            if (quarterRpts[i].month.toLowerCase() === month.substring(0, 3).toLowerCase()) {
                returnStr = quarterRpts[i].periodId;
            }
        }
        return returnStr;
    }

    getIsCigarOnly(quarterRpts, month) {
        let returnStr = "false";
        for (let i = 0; i < quarterRpts.length; i++) {
            if (quarterRpts[i].month.toLowerCase() === month.substring(0, 3).toLowerCase() && quarterRpts[i].cigarOnly === 'CGR_') {
                if (quarterRpts[i].zeroFlag && quarterRpts[i].zeroFlag !== 'Y') {
                    returnStr = "true";
                }
            }
        }
        return returnStr;
    }

    toPeriodofActivity(rptStatus: any, month: string) {

        this.shareData.reset();
        this.shareData.companyName = rptStatus.legalNm;
        this.shareData.permitNum = rptStatus.permitNum;
        this.shareData.period = month + ' FY ' + rptStatus.fiscalYr;
        this.shareData.permitId = rptStatus.permitId;
        this.shareData.periodId = Number(this.getPeriodId(rptStatus.quarterReports, month));
        this.shareData.companyId = rptStatus.companyId;

        this.shareData.periodStatus = this.getStatus(rptStatus.quarterReports, month);
        this.shareData.previousRoute = this.router.url;
        this.shareData.breadcrumb = 'ca-reportstatus';
        this.shareData.qatabfrom = 'ca-reportstatus';
        this.shareData.createdDate = this.createdDate;
        this.shareData.submitted = this.submitted;
        this.shareData.cigarsubmitted = this.cigarsubmitted;

        this.shareData.quarter = this.quarter;
        this.shareData.fiscalYear = this.fiscalYear;
        this.shareData.months = this.months;
    }

    showReport(rptStatus) {

        this.shareData.reset();
        this.shareData.companyName = rptStatus.companyName;
        this.shareData.permitNum = rptStatus.permitNum;
        this.shareData.period = rptStatus.month + " FY " + rptStatus.fiscalYear;
        this.shareData.permitId = rptStatus.permitId;
        this.shareData.periodId = rptStatus.periodId;
        this.shareData.companyId = rptStatus.companyId;

        this.shareData.periodStatus = rptStatus.reportStatus;
        this.shareData.previousRoute = this.router.url;
        this.shareData.isRecon = false;
        this.shareData.breadcrumb = 'ca-reportstatus';
        this.shareData.qatabfrom = 'ca-reportstatus';
        this.shareData.createdDate = this.createdDate;
        this.shareData.submitted = this.submitted;
        this.shareData.cigarsubmitted = this.cigarsubmitted;

        // this.shareData.quarter = this.quarter;
        this.shareData.fiscalYear = this.fiscalYear;
        this.shareData.months = this.months;
        return false;
    }
}
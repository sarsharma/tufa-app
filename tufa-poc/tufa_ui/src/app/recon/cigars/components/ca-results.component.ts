import { Component, Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { ShareDataService } from '../../../core/sharedata.service';

declare var jQuery: any;

@Component({
    selector: '[ca-results]',
    templateUrl: './ca-results.template.html',
    styleUrls:['ca-results.style.scss'],
    providers: [ ]
})

export class CigarsAssessmentsResultsComponent {
    public router: Router;
    data: any[];
    filterData: any;
    quarterFilter: any;
    shareData: ShareDataService;

    @Input() cigarAssessData: any[];
    @Input() filterOptions: any;

    showErrorFlag: boolean;
    alerts: Array<Object>;

    sortBy: string = "assessmentYr";
    sortOrder: string[] = ["desc"];

    constructor( router: Router,
        private _shareData: ShareDataService
    ) {
        this.router = router;
        this.shareData = _shareData;
        this.showErrorFlag = false;
    }
    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "cigarAssessData") {
                let chg = changes[propName];
                if (chg.currentValue) {
                    this.data = chg.currentValue;
                }
            } else if (propName === "filterOptions") {
                let chg = changes[propName];
                if (chg.currentValue) {
                    this.filterData = chg.currentValue;
                }
            }

        }
    }

    toCigarAssessmentReport(assessment) {
        this.shareData.reset();
        this.shareData.assessmentId = assessment.assessmentId;
        this.shareData.quarter = assessment.assessmentQtr;
        this.shareData.months = assessment.quarterMonths;
        this.shareData.fiscalYear = assessment.assessmentYr;
        this.shareData.submitted = assessment.submittedInd;
        this.shareData.cigarsubmitted = assessment.cigarSubmittedInd;
        this.shareData.createdDate = assessment.createdDt;
        this.shareData.qatabfrom = 'ca-reportstatus';
    }

}
import { CigarsAssessment } from './ca.model';

export class CigarsAssessmentsFilter {
    statusFilters: string[] = [];
    fiscalYrFilters: string[] = [];
    results: CigarsAssessment[];
}

export class CigarsAssessmentsFilterData {
    private _reportStatus: any;
    private _reportYears: any;

    get reportStatus(): any {
        return this._reportStatus;
    }
    set reportStatus(value: any) {
        this._reportStatus = value;
    }

    get reportYears(): any {
        return this._reportYears;
    }

    set reportYears(value: any) {
        this._reportYears = value;
    }
}


import { CigarsAssessment } from "./../../../recon/cigars/models/ca.model";
import { DatatableFilter } from "../../../shared/datatable/DataTableFilter";


export class CigarAssessmentsReconFilter {
    companyFilter: String;
    permitFilter: String;
    monthFilter: String;    
    statusFilter: String;
    reportedTobaccoFilter: String;
    results: CigarsAssessment[];
}

export class CigarAssessmentsReconFilterData implements DatatableFilter {
   _reportStatus: any;
   _companyFilter: String;
   _permitFilter: String;
   _monthFilter: String;
   _statusFilter: String;
   _reportedTobaccoFilter: String;

    get reportStatus(): any {
        return this._reportStatus;
    }
    set reportStatus(value: any) {
        this._reportStatus = value;
    }

    get companyFilter(): any {
        return this._companyFilter;
    }
    set companyFilter(value: any) {
        this._companyFilter = value;
    }

    get permitFilter(): any {
        return this._permitFilter;
    }
    set permitFilter(value: any) {
        this._permitFilter = value;
    }

    get monthFilter(): any {
        return this._monthFilter;
    }
    set monthFilter(value: any) {
        this._monthFilter = value;
    }

    get statusFilter(): any {
        return this._statusFilter;
    }
    set statusFilter(value: any) {
        this._statusFilter = value;
    }

    get reportedTobaccoFilter(): any {
        return this._reportedTobaccoFilter;
    }
    set reportedTobaccoFilter(value: any) {
        this._reportedTobaccoFilter = value;
    }    

    public isFilterSelected() {
        return (this._companyFilter||this._monthFilter||this._permitFilter ||this._reportedTobaccoFilter || this._statusFilter)?true:false;
    }  
 }

export class CigarAssessmentSupportingDocument {
    asmntDocId: number;
    assessmentId: number;
    documentNumber: number;
    filename: string;
    description: string;
    versionNum: number;
    dateUploaded: Date;
    author: string;
    assessmentPdf: string;
    createdBy: string;
    createdDt: Date;
    modifiedBy: string;
    modifiedDt: Date;
    cigarQtr: number;
}
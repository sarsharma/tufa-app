export class MarketShareData {
    annualTrueUpSubmittedMsg: string;
    approvalMessage: string;
    assessmentId: number;
    assessmentQtr: number;
    assessmentType: string;
    assessmentYr: number;
    cigarSubmittedInd: string;
    createdBy: string;
    createdDt: string;
    displayQtr: string
    modifiedBy: string
    modifiedDt: string
    quarterMonths: string[];
    submittedInd: string
    versions: MarketShareVersion[];
}

export class MarketShareVersion {
    cigarQtr: number;
    createdBy: string;
    createdDt: string;
    exports: MarketShareTobaccoExport;
    marketshareType: string;
    versionNum: number;
}

export class MarketShareTobaccoExport {
    Cigars: MarketShareExport;
}

export class MarketShareExport {
    csv: string;
    title: string;
    tobaccoType: string;
}
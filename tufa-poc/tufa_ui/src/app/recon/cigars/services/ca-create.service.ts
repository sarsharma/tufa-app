import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { environment } from '../../../../environments/environment';
import { AnnualTrueUpFilter } from "./../../../recon/annual/models/at-filter.model";
import { CigarsAssessment } from "./../../../recon/cigars/models/ca.model";

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class CigarsAssessmentsCreationService {
    private getAssessmentsurl = environment.apiurl + '/api/reports/quarter';
    private getFiscalYearsurl = environment.apiurl + '/api/reports/quarter/fiscalyears';
    private getAnnualTrueUpsurl = environment.apiurl + '/api/v1/trueup/list';

    constructor(private _httpClient: HttpClient) { }

    saveCigarAssessment(cigarAssessment: CigarsAssessment): Observable<String> {
        return this._httpClient.post(this.getAssessmentsurl + '/cigar', cigarAssessment, httpOptions).pipe(
            tap(_ => this.log("saveQuarterlyReport")),
            catchError(this.handleError<any>("saveQuarterlyReport"))
        );
    }

    /**
     * 
     * 
     * @returns {Observable<string>}
     * 
     * @memberOf CreateQuarters
     */
    getFiscalYears(): Observable<string>{
        return this._httpClient.get(this.getFiscalYearsurl, httpOptions).pipe(
            tap(_ => this.log("getFiscalYears")),
            catchError(this.handleError<any>("getFiscalYears"))
        );
    }

    getTrueUps(trueUpFilter: AnnualTrueUpFilter) {      
        return this._httpClient.put(this.getAnnualTrueUpsurl, trueUpFilter, httpOptions);
    }    

        	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}

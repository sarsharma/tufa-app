/*
@author : Deloitte
Service class to perform assessments related operations.
*/

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { environment } from '../../../../environments/environment';
import { MarketShareData } from '../models/market-share-data.model';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};
/**
 * 
 * 
 * @export
 * @class AssessmentService
 */
@Injectable()
export class CigarsAssessmentsMarketShareService {

    private getMarketShareurl = environment.apiurl + '/api/reports/quarter/export/';

    constructor(private _httpClient: HttpClient) { }

    getMarketShareByYear(year: any): Observable<MarketShareData[]> {
         let url = this.getMarketShareurl + "cigar"+ "/" + year;
         return this._httpClient.get(url, httpOptions).pipe(
          tap(_ => this.log("getMarketShare")),
          catchError(this.handleError<any>("getMarketShare"))
      );
    }

    generateMarketShare(year: any, assessQtr: any, marketsharetype: any): Observable<MarketShareData> {
         let url = this.getMarketShareurl + "0" + "/" + year + "/" + assessQtr + "/" + marketsharetype;
         return this._httpClient.put(url, null, httpOptions).pipe(
          tap(_ => this.log("generateMarketShare")),
          catchError(this.handleError<any>("generateMarketShare"))
      );
    }
  
    submitMarketShare(year: any, assessQtr: any, marketsharetype: any): Observable<MarketShareData> {
         let url = this.getMarketShareurl + "0" + "/" + year + "/" + assessQtr + "/" + marketsharetype;
         return this._httpClient.post(url, null, httpOptions).pipe(
          tap(_ => this.log("submitMarketShare")),
          catchError(this.handleError<any>("submitMarketShare"))
      );
    }

    
    	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}
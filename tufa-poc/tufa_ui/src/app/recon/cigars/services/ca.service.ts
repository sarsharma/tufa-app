import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { CigarAssessmentSupportingDocument } from '../models/ca-supportdoc.model';
import { CigarsAssessmentsFilter } from "./../../../recon/cigars/models/ca-filter.model";

const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class CigarsAssessmentsService {
    private getAssessmentsurl = environment.apiurl + '/api/cigars/assessment/list';
    private getReportStatusurl = environment.apiurl + '/api/cigars/assessment/report/';
    private getReconStatusurl = environment.apiurl + '/api/cigars/assessment/recon/';
    private cigarAssessmentBaseurl = environment.apiurl + '/api/cigars/assessment/';

    constructor(private http: HttpClient) { }

    public getCigarAssessments(cigarAssessFilter: CigarsAssessmentsFilter) {
        return this.http.put(this.getAssessmentsurl, cigarAssessFilter, httpOptions);
    }

    public getReportStatus(fiscalYr: any) {
        return this.http.get(this.getReportStatusurl + fiscalYr, httpOptions);
    }

    public getReconReportDetails(fiscalYr: any) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.get(this.getReconStatusurl + fiscalYr, httpOptions);
    }

    public exportReconReportData(fiscalYr: string) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let url = this.cigarAssessmentBaseurl + "export/fiscalyear/" + fiscalYr;
        return this.http.get(url, httpOptions);
    }

    public saveDoc(doc: CigarAssessmentSupportingDocument, fiscalYr: string) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let url = this.cigarAssessmentBaseurl + "docs/" + doc.assessmentId;
        return this.http.put(url, doc, httpOptions);
    }

    public getSupportingDocs(fiscalYr: number) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let url = this.cigarAssessmentBaseurl + "" + fiscalYr + "/docs";
        return this.http.get(url, httpOptions);
    }

    public deleteDocument(asmntdocId: number) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let url = this.cigarAssessmentBaseurl + "docs/" + asmntdocId;
        return this.http.delete(url, httpOptions);
    }

    public downloadDocument(asmntdocId: number) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let url = this.cigarAssessmentBaseurl + "docs/" + asmntdocId;
        return this.http.get(url, httpOptions);
    }
}

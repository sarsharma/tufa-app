import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { CigarsAssessmentsFilterData } from "./../../../recon/cigars/models/ca-filter.model";

/**
 * 
 * 
 * @export
 * @class PermitReportsDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "cigarassesspipe"
})

export class CigarsAssessmentsDataFilterPipe implements PipeTransform {
    
    transform(data: any[], filter: CigarsAssessmentsFilterData): any {
        let results = data;

        if (data) {
            if (filter) {
                // filter by years
                if (typeof filter.reportYears !== 'undefined' && filter.reportYears.length > 0) {
                   results =  _.filter(results, function(item) {
                        return _.includes(Array.from(filter.reportYears), item.assessmentYr);
                    });
                }
                // filter by status
                if (typeof filter.reportStatus !== 'undefined' && filter.reportStatus.length > 0) {
                   results =  _.filter(results, function(item) {
                        let variable = new RegExp('Y', "gi");
                        // convert submittedInd to match string
                        let count = 0;
                        let matchstring = item.cigarSubmittedInd.match(variable);
                        if( matchstring ) {
                            count = matchstring.length;
                        }
                        let filterString = item.cigarSubmittedInd;
                        if( count > 0 && count <= 4){
                            filterString = "Submitted (" + count + "/4)";
                        }
                        return _.includes(Array.from(filter.reportStatus), filterString);
                    });
                }
            }
        }
        return results;
    }
  
}

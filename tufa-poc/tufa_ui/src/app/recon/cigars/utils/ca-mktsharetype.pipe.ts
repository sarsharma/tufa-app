
import { Pipe, PipeTransform } from "@angular/core";


/**
 * 
 * 
 * @export
 * @class MarketShareTypePipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "cigarmstypepipe"
})
export class CigarMarketShareTypePipe implements PipeTransform {

    /**
     * 
     * 
     * @param {any[]} data
     * @returns {*}
     * 
     * @memberOf MarketShareTypePipe
     */
    transform(data: any[]): any {

        let results: string = "";

        if (data != null) {           
            if (data.toString() === "FULLMS" ) {
                results = "Full Market Share";
            }
            else if (data.toString() === "ACONLY" ){
                results = "Address Changes Only";
            }
        }
      
      return results;
    }
}
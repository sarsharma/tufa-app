
import { Pipe, PipeTransform } from "@angular/core";


/**
 * 
 * 
 * @export
 * @class PermitReportsDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "cigarstatuspipe"
})
export class CigarStatusPipe implements PipeTransform {
    transform(data: any[]): any {
        let results: string;
        if (data != null) {           
            if (data.toString() == "Not Submitted" || data.toString() == "NNNN") {
                results = "Not Submitted";
            }
            else {
                if (data) {                       
                    results = this.getCigarSubmissionStatus(data);
                    results;
                }
            }
        }
        return results;
    }

    getCigarSubmissionStatus(statusInd: any){
        if (this.count(statusInd, 'Y') == 4) {
            return "Submitted (4/4)";
        }
        else if (this.count(statusInd, 'Y') == 3) {
            return "Submitted (3/4)";

        }
        else if (this.count(statusInd, 'Y') == 2) {
            return "Submitted (2/4)";

        }
        else if (this.count(statusInd, 'Y') == 1) {
            return "Submitted (1/4)";
        }

        return statusInd;
    }

    count(string, char) {
        let count = 0;
        let variable = new RegExp(char, "gi");
        let matchstring = string.match(variable);
        if(matchstring !== null){
            count = matchstring.length;
        }
        return count;
    }
}
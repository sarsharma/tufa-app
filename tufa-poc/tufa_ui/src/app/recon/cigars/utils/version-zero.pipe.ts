import * as _ from "lodash";
import { Pipe } from "../../../../../node_modules/@angular/core";
import { MarketShareVersion } from "../models/market-share-data.model";

@Pipe({
    name: "versionzero"
})
export class VersionZeroPipe {
    transform(data: MarketShareVersion[], lookForZero:boolean = false): any {
        if(data) {
            if(lookForZero) {
                return _.filter(data, function (o) { return (<any>o).versionNum === 0; });
            } else {
                return _.filter(data, function (o) { return (<any>o).versionNum !== 0; });
            }
        } else {
            return [];
        }
    }
}
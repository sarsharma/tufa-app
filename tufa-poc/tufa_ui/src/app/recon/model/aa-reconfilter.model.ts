
import { AffectedAssessmentModel } from './../../recon/model/afffectedassessment.model';
import { DatatableFilter } from './../../../app/shared/datatable/DataTableFilter';

export class AffectedAssessmentsReconFilter {
    assessmentFYFilter: string;
    assessmentQTRFilter: string;
    assessmentTypeFilter:string;
    companyNameFilter: string;
    einFilter: string;
    permitNumberFilter: string;   
    results: AffectedAssessmentModel[];
}

export class AffectedAssessmentsReconFilterData implements DatatableFilter{
    _assessmentFYFilter: string;
    _assessmentQTRFilter: string;
    _assessmentTypeFilter: string;
    _companyNameFilter: string;
    _einFilter: string;
    _permitNumberFilter: string;

    public get assessmentFYFilter(): string {
        return this._assessmentFYFilter;
    }
    public set assessmentFYFilter(value: string) {
        this._assessmentFYFilter = value;
    }
    public get assessmentQTRFilter(): string {
        return this._assessmentQTRFilter;
    }
    public set assessmentQTRFilter(value: string) {
        this._assessmentQTRFilter = value;
    }
    public get assessmentTypeFilter(): string {
        return this._assessmentTypeFilter;
    }
    public set assessmentTypeFilter(value: string) {
        this._assessmentTypeFilter = value;
    }
    
    public get companyNameFilter(): string {
        return this._companyNameFilter;
    }
    public set companyNameFilter(value: string) {
        this._companyNameFilter = value;
    }
    
    public get einFilter(): string {
        return this._einFilter;
    }
    public set einFilter(value: string) {
        this._einFilter = value;
    }
        
    public get permitNumberFilter(): string {
        return this._permitNumberFilter;
    }
    public set permitNumberFilter(value: string) {
        this._permitNumberFilter = value;
    }
  

    public isFilterSelected() {
        return (this._assessmentFYFilter||this._assessmentQTRFilter||this._assessmentTypeFilter||this._companyNameFilter || this._einFilter || this._permitNumberFilter)?true:false;
    }  
 }

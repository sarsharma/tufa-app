import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { QuarterlyAssessmentsCreationService } from "./../../../recon/quarterly/services/qa-create.service";
import { QuarterlyAssessment } from "./../../../recon/quarterly/models/qa.model";
import { ShareDataService } from "./../../../core/sharedata.service";

declare var jQuery: any;
declare var window: any;

@Component({
    selector: '[qa-create]',
    templateUrl: './qa-create.template.html',
    styleUrls:['qa-create.style.scss'],
    providers: [ QuarterlyAssessment ],
    encapsulation: ViewEncapsulation.None
})

export class QuarterlyAssessmentsCreationPage implements OnInit {

    calQuarter: string;
    calYear: string;
    // quarterYear: string;
    router: Router;

    alerts: Array<Object>;
    showAlertFlag: boolean = false;

    tabfrom: string;
    shareData: ShareDataService;

    public jQuery: any;

    constructor(private _createQuarters: QuarterlyAssessmentsCreationService,
                    router: Router,
                    private _assessment: QuarterlyAssessment,
                    private _shareData: ShareDataService) {

        this.alerts = [
            {
                type: 'success',
                msg: '<i class="fa fa-circle text-success"></i><span class="alert-text">Successfully generated 12 Quarterly reports</span>'
            }
        ];

        this.router = router;
        this.shareData = _shareData;
    }

    QuarterList: any[] = ['Select Quarter', 'Q1 - Oct, Nov, Dec', 'Q2 - Jan, Feb, Mar', 'Q3 - Apr, May, Jun', 'Q4 - Jul, Aug, Sep'];
    ngOnInit() {
        this.calQuarter = 'Select Quarter';
        this.showAlertFlag = false;
        this.getParamsForReconNavigation();
    }

    createQuarterly(x, y) {
        this.showAlertFlag = false;
        y = this.QuarterList.indexOf(y);
        jQuery('#createquarter-form').parsley().validate();

        this._assessment.assessmentYr = x;
        this._assessment.assessmentQtr = y;
        this._assessment.submittedInd = 'N';
        if (jQuery('#createquarter-form').parsley().isValid()) {
            this._createQuarters.saveQuarterlyReport(this._assessment).subscribe(data => {
                if (data.toString() === '0') {
                    this.alerts[0]['type'] = 'danger';
                    // tslint:disable-next-line:max-line-length
                    this.alerts[0]['msg'] = '<i class="fa fa-circle text-danger" ></i><span class="alert-text">FY' + x + ' Q' + y + ' assessment already exists</span>';
                } else {
                    this.alerts[0]['type'] = 'success';
                    // tslint:disable-next-line:max-line-length
                    this.alerts[0]['msg'] = '<i class="fa fa-circle text-success" ></i><span class="alert-text">Successfully generated the assessment for selected Fiscal Year and Quarter</span>';
                }
                this.showAlertFlag = true;
            }, err => {
                this.alerts[0]['type'] = 'danger';
                // tslint:disable-next-line:max-line-length
                this.alerts[0]['msg'] = '<i class="fa fa-circle text-danger" ></i><span class="alert-text">Missing criteria: Unable to generate reports</span>';
                this.showAlertFlag = true;
            });
        }
    }

    setboxfocus(elementID: String): void {

        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let searchInput = jQuery(elementID);
        searchInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

    closeAlert(): void {
        this.showAlertFlag = false;
    }

    gotoAssessments() {
        this.setParamsForReconNavigation();
    }

    setParamsForReconNavigation() {
        this.shareData.reset();
        this.shareData.qatabfrom = this.tabfrom;
    }

    getParamsForReconNavigation() {
        this.tabfrom = this.shareData.qatabfrom;
    }

}

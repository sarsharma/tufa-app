import { AfterViewInit, Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ShareDataService } from '../../../core/sharedata.service';
import { TabsetComponent } from '../../../../../node_modules/ngx-bootstrap';

declare var jQuery: any;
/**
 *
 *
 * @export
 * @class QuarterlyAssessmentsHomePage
 */
@Component({
  selector: '[qa-home]',
  templateUrl: './qa-home.template.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./qa-home.style.scss']
})

export class QuarterlyAssessmentsHomePage implements OnInit, AfterViewInit {
  @ViewChild('qaTabs') qaTabs: TabsetComponent;

  paramsSub: any;

  router: Router;
  assessmentId: any;

  shareData: ShareDataService;

  months: string[];
  quarter: number;
  fiscalYear: number;
  submitted: string;
  createdDt: string;
  qatabfrom: string;

  /**
   * Creates an instance of QuarterlyAssessmentsHomePage.
   *
   * @param {Router} router
   * @param {ActivatedRoute} activatedRoute
      *
   * @memberOf QuarterlyAssessmentsHomePage
   */
  constructor(router: Router,
    private activatedRoute: ActivatedRoute,
    private _shareData: ShareDataService
  ) {
    this.router = router;
    this.shareData = _shareData;
  }

  /**
   *
   *
   *
   * @memberOf QuarterlyAssessmentsHomePage
   */
  ngOnInit(): void {
    this.getParamsForReconNavigation();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.qatabfrom === 'qa-reconreport') {
        this.qaTabs.tabs[1].active = true;
      }
    }, 100);
  }

  /**
   *
   *
   *
   * @memberOf QuarterlyAssessmentsHomePage
   */
  gotoAssessments() {
    this.router.navigate(['/app/assessments']);
  }

  onSubmitted(data: any) {
    this.submitted = data.submittedInd;
  }

  setParamsForReconNavigation() {
    this.shareData.reset();
    this.shareData.assessmentId = this.assessmentId;
    this.shareData.months = this.months;
    this.shareData.quarter = this.quarter;
    this.shareData.fiscalYear = this.fiscalYear;
    this.shareData.submitted = this.submitted;
    this.shareData.createdDate = this.createdDt;
    this.shareData.qatabfrom = this.qatabfrom;
  }

  getParamsForReconNavigation() {
    this.assessmentId = this.shareData.assessmentId;
    this.months = this.shareData.months;
    this.quarter = this.shareData.quarter;
    this.fiscalYear = this.shareData.fiscalYear;
    this.submitted = this.shareData.submitted;
    this.createdDt = this.shareData.createdDate;
    this.qatabfrom = this.shareData.qatabfrom;
  }
}

import { Component, Input, Output, ViewEncapsulation, EventEmitter, OnInit, OnDestroy  } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Subscription } from 'rxjs';
import { NgBusyModule } from 'ng-busy';
import * as _ from "lodash";
import { QuarterlyAssessmentsMarketShareService } from "./../../../recon/quarterly/services/qa-marketshare.service";
import { QuarterlyAssessmentsService } from '../services/qa.service';
import { QuarterlyAssessmentsSupportingDocument } from '../models/qa-supportdoc.model';

@Component({
  selector: '[qa-marketshare]',
  templateUrl: './qa-marketshare.template.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./qa-marketshare.style.scss']
})

export class QuarterlyAssessmentsMarketShareComponent implements OnInit, OnDestroy {

  @Input() fiscalYear;
  @Input() quarter;

  @Output() onSubmitted = new EventEmitter<any>();

  busyGen: Subscription;
  busySub: Subscription;

  // support docs
  busyload: Subscription;
  busyupload: Subscription;
  busyDownload: Subscription;
  sortBy = "versionNum";

  mktsharealerts: Array<Object>;
  submitalerts: Array<Object>;

  mktsharedata: any;
  versionnumbers: any;
  selVersion: string;

  //supporting documents
  docs: any[];
  assessmentId: number;

  showSummary: boolean = false;
  showHistory: boolean = false;
  showSubmit: boolean = false;
  showAddDocumentPopup: boolean = false;

  zeroVersionIndex: number;
  zeroVersionDate: Date;
  zeroVersionDateNumber: Date;
  zeroVersionAuthor: string;
  selVersionDate: Date;
  selVersionDateNumber: Date;
  author: string;
  submitBtnDisabled: boolean = false;

  constructor(private marketShareService: QuarterlyAssessmentsMarketShareService,
             private docService: QuarterlyAssessmentsService,
             private router: Router,
             private activatedRoute: ActivatedRoute) {

    this.mktsharealerts = [];
    this.submitalerts = [];

    this.zeroVersionIndex = -1;
    this.zeroVersionAuthor = "";
    this.author = "";

    this.showSummary = false;
    this.showSubmit = false;
    this.showHistory = false;
    this.submitBtnDisabled = false;

 }

  ngOnInit(): void {
      this.showSummary = false;
      this.getHistoricalSubmissionDetails();
  }

    ngOnDestroy(){
        if(this.busyGen){
            this.busyGen.unsubscribe();
        }
        if(this.busyDownload){
            this.busyDownload.unsubscribe();
        }
        if(this.busySub){
            this.busySub.unsubscribe();
        }
        if(this.busyload){
            this.busyload.unsubscribe();
        }
        if(this.busyupload){
            this.busyupload.unsubscribe();
        }
    }

  getHistoricalSubmissionDetails(){

    // since the load may take long time, when user checks multiple filter criteria, we need to
    // abort earlier calls and re-initiate a new call
    if (this.busyGen) {
        this.busyGen.unsubscribe();
    }

    this.busyGen = this.marketShareService.getMarketShare(this.fiscalYear, this.quarter).subscribe( data => {
          this.mktsharedata = data;
          this.assessmentId = this.mktsharedata.assessmentId;
          this.loadSupportingDocs(this.assessmentId);

          // find the zero version index from the return object array
          this.zeroVersionIndex = _.findIndex(this.mktsharedata.versions, function(o) { return (<any>o).versionNum === 0; });
          this.zeroVersionDate = this.getVersionDateByIndex(this.zeroVersionIndex);
          this.zeroVersionDateNumber = new Date(this.zeroVersionDate);
          this.zeroVersionAuthor = this.getVersionAuthorByVersionNumber(0);

          // retrieve version numbers and drop version 0
          this.versionnumbers = _.pull(_.map(this.mktsharedata.versions, 'versionNum'), 0);
         
          if (_.size(this.versionnumbers) > 0) {
            this.showHistory = true;
            this.selVersion = _.first(this.versionnumbers);
            this.selVersionDate = this.getVersionDateByVersionNumber(Number(this.selVersion));
            this.selVersionDateNumber = new Date(this.selVersionDate);
            this.author = this.getVersionAuthorByVersionNumber(Number(this.selVersion));
          }

          if (this.mktsharedata.approvalMessage) {
            this.mktsharealerts.push({type: 'warning',
                msg:  '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + this.mktsharedata.approvalMessage + '</span>'});
          } else {
            this.mktsharealerts = [];
          }

          this.onSubmitted.emit(this.mktsharedata);
      }, error => {
          this.zeroVersionAuthor = "";
          this.showSummary = false;
          this.mktsharealerts = [];
          this.mktsharealerts.push({type: 'warning', msg:  '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + error + '</span>'});
      });
  }

  generateMaketShare() {

    // since the load may take long time, when user checks multiple filter criteria, we need to
    // abort earlier calls and re-initiate a new call
    if (this.busyGen) {
        this.busyGen.unsubscribe();
    }

    this.busyGen = this.marketShareService.generateMarketShare(this.fiscalYear, this.quarter).subscribe( data => {
          this.mktsharedata = data;
          // find the zero version index from the return object array
          this.zeroVersionIndex = _.findIndex(this.mktsharedata.versions, function(o) { return (<any>o).versionNum === 0; });
          this.zeroVersionDate = this.getVersionDateByIndex(this.zeroVersionIndex);
          this.zeroVersionDateNumber = new Date(this.zeroVersionDate);
          this.zeroVersionAuthor = this.getVersionAuthorByVersionNumber(0);

          // retrieve version numbers and drop version 0
          this.versionnumbers = _.pull(_.map(this.mktsharedata.versions, 'versionNum'), 0);
          if (this.zeroVersionIndex !== -1) {
            this.showSummary = true;
          }

          if (_.size(this.versionnumbers) > 0) {
            this.showHistory = true;
            this.selVersion = _.first(this.versionnumbers);
            this.selVersionDate = this.getVersionDateByVersionNumber(Number(this.selVersion));
            this.selVersionDateNumber = new Date(this.selVersionDate);
            this.author = this.getVersionAuthorByVersionNumber(Number(this.selVersion));
          }

          if (this.mktsharedata.approvalMessage) {
            let submitmsg = " Report cannot be submitted until all Monthly Reports are Approved. Please re-generate the Market Share Data after " +
                    "approving all monthly reports."  ;
            this.mktsharealerts = [];
            this.mktsharealerts.push({type: 'warning',
                msg:  '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + this.mktsharedata.approvalMessage + '</span>'});
            this.submitalerts = [];
            this.submitalerts.push({type: 'warning',  msg:  submitmsg });
            this.showSubmit = false;
          } else {
            this.mktsharealerts = [];
            this.submitalerts = [];
            this.showSubmit = true;
            this.blockSubmit();
            if(!this.mktsharedata.versions[0].exports['Chew'])
                this.submitBtnDisabled = true;
          }
          this.onSubmitted.emit(this.mktsharedata);
      }, error => {
          this.zeroVersionAuthor = "";
          this.author = "";
          this.showSummary = false;

          this.mktsharealerts = [];
          this.mktsharealerts.push({type: 'warning', msg:  '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + error + '</span>'});
      });
  }

    private blockSubmit() {
       // console.log("Inside blockSubmit" + this.mktsharedata.annualTrueUpSubmittedMsg +"end");
        if (this.mktsharedata.annualTrueUpSubmittedMsg) {
            this.submitBtnDisabled = true;
            this.submitalerts.push({ type: 'warning', msg: this.mktsharedata.annualTrueUpSubmittedMsg });
        }
    }

  getVersionDateByIndex(index: number) {
      if (index !== -1) {
        if (this.mktsharedata.versions[index]) {
            if (this.mktsharedata.versions[index].createdDt) {
                return this.mktsharedata.versions[index].createdDt;
            }else{
                return "";
            }
        }
     }
     return "";
  }

  getVersionDateByVersionNumber(versionNum: number) {
      let index = _.findIndex(this.mktsharedata.versions, function(o) { return (<any>o).versionNum === Number(versionNum); });
      if (index !== -1) {
        if (this.mktsharedata.versions[index]) {
            if (this.mktsharedata.versions[index].createdDt) {
                return this.mktsharedata.versions[index].createdDt;
            }else{
                return "";
            }
        }
     }
     return "";
  }


  getVersionAuthorByVersionNumber(versionNum: number) {
      let index = _.findIndex(this.mktsharedata.versions, function(o) { return (<any>o).versionNum === Number(versionNum); });
      if (index !== -1) {
        if (this.mktsharedata.versions[index]) {
            if (this.mktsharedata.versions[index].createdBy) {
                return this.mktsharedata.versions[index].createdBy;
            }else{
                return "";
            }
        }
     }
     return "";
  }



  submitMarketShare() {

    // since the load may take long time, when user checks multiple filter criteria, we need to
    // abort earlier calls and re-initiate a new call
    if (this.busySub) {
        this.busySub.unsubscribe();
    }

    this.busySub = this.marketShareService.submitMarketShare(this.fiscalYear, this.quarter).subscribe( data => {
          this.mktsharedata = data;
          this.submitalerts = [];
          this.showSummary = false;

          // find the zero version index from the return object array
          this.zeroVersionIndex = _.findIndex(this.mktsharedata.versions, function(o) { return(<any>o).versionNum === 0; });
          this.zeroVersionDate = this.getVersionDateByIndex(this.zeroVersionIndex);
          this.zeroVersionDateNumber = new Date(this.zeroVersionDate);
          this.zeroVersionAuthor = this.getVersionAuthorByVersionNumber(0);

          // retrieve version numbers and drop version 0
          this.versionnumbers = _.pull(_.map(this.mktsharedata.versions, 'versionNum'), 0);
          if (_.size(this.versionnumbers) > 0) {
            this.showHistory = true;
            this.selVersion = _.first(this.versionnumbers);
            this.selVersionDate = this.getVersionDateByVersionNumber(Number(this.selVersion));
            this.selVersionDateNumber = new Date(this.selVersionDate);
            this.author = this.getVersionAuthorByVersionNumber(Number(this.selVersion));
          }
          this.onSubmitted.emit(this.mktsharedata);
      }, error => {
          this.submitalerts = [];
          this.zeroVersionAuthor = "";
          this.author = "";
          this.submitalerts.push({type: 'warning', msg:  '<i class="fa fa-circle text-danger" ></i><span class="alert-text">' + error + '</span>'});
      });
  }

  isMktShareDataAvailable(tobaccoClass: string, versionNum: number) {
      let index = _.findIndex(this.mktsharedata.versions, function(o) { return (<any>o).versionNum === Number(versionNum); });
      if (index !== -1) {
        if (this.mktsharedata.versions[index]) {
            if (this.mktsharedata.versions[index].exports[tobaccoClass])
                return true;
        }
      }
      return false;
  }

  onchangeVersion(versionNum: number) {
    this.selVersionDate = this.getVersionDateByVersionNumber(versionNum);
    this.selVersionDateNumber = new Date(this.selVersionDate);
    this.author = this.getVersionAuthorByVersionNumber(versionNum);
  }

  downloadExport(tobaccoClass: string, versionNum: number) {
      let index = _.findIndex(this.mktsharedata.versions, function(o) { return (<any>o).versionNum === Number(versionNum); });
      if (index !== -1) {
        if (this.mktsharedata.versions[index]) {
            if (this.mktsharedata.versions[index].exports[tobaccoClass]) {
                this.download(this.mktsharedata.versions[index].exports[tobaccoClass][0].csv,
                        this.mktsharedata.versions[index].exports[tobaccoClass][0].title, 'csv/text');
            }
        }
     }
  }


  /**
   *
   *
   * @param {*} data
   * @param {*} strFileName
   * @param {*} strMimeType
   * @returns
   *
   * @memberOf QuarterlyAssessmentsMarketShareComponent
   */
  download(data: any, strFileName: any, strMimeType: any) {

      let self: any = window, // this script is only for browsers anyway...
          defaultMime = 'application/octet-stream', // this default mime also triggers iframe downloads
          mimeType = strMimeType || defaultMime,
          payload = data,
          url = !strFileName && !strMimeType && payload,
          anchor = document.createElement('a'),
          toString = function (a) { return String(a); },
          myBlob = (self.Blob || self.MozBlob || self.WebKitBlob || toString),
          fileName = strFileName || 'download',
          blob,
          reader;
      myBlob = myBlob.call ? myBlob.bind(self) : Blob;

      if (String(this) === 'true') { // reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
          payload = [payload, mimeType];
          mimeType = payload[0];
          payload = payload[1];
      }

      // go ahead and download dataURLs right away
      if (/^data[\:\;]/.test(payload)) {

          if (payload.length > (1024 * 1024 * 1.999) && myBlob !== toString) {
              payload = dataUrlToBlob(payload);
              mimeType = payload.type || defaultMime;
          } else {
              return navigator.msSaveBlob ?  // IE10 can't do a[download], only Blobs:
                  navigator.msSaveBlob(dataUrlToBlob(payload), fileName) :
                  saver(payload, false); // everyone else can save dataURLs un-processed
          }

      } else {// not data url, is it a string with special needs?
          if (/([\x80-\xff])/.test(payload)) {
              let i = 0, tempUiArr = new Uint8Array(payload.length), mx = tempUiArr.length;
              for (i; i < mx; ++i) tempUiArr[i] = payload.charCodeAt(i);
              payload = new myBlob([tempUiArr], { type: mimeType });
          }
      }
      blob = payload instanceof myBlob ?
          payload :
          new myBlob([payload], { type: mimeType });


      function dataUrlToBlob(strUrl) {
          let parts = strUrl.split(/[:;,]/),
              type = parts[1],
              decoder = parts[2] === 'base64' ? atob : decodeURIComponent,
              binData = decoder(parts.pop()),
              mx = binData.length,
              i = 0,
              uiArr = new Uint8Array(mx);

          for (i; i < mx; ++i) uiArr[i] = binData.charCodeAt(i);

          return new myBlob([uiArr], { type: type });
      }

      function saver( url: any, winMode: any) {

          if ('download' in anchor) { // html5 A[download]
              anchor.href = url;
              anchor.setAttribute('download', fileName);
              anchor.className = 'download-js-link';
              anchor.innerHTML = 'downloading...';
              anchor.style.display = 'none';
              document.body.appendChild(anchor);
              setTimeout(function () {
                  anchor.click();
                  document.body.removeChild(anchor);
                  if (winMode === true) { setTimeout(function () { self.URL.revokeObjectURL(anchor.href); }, 250); }
              }, 66);
              return true;
          }

          // handle non-a[download] safari as best we can:
          if (/(Version)\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//.test(navigator.userAgent)) {
              if (/^data:/.test(url)) url = 'data:' + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
              if (!window.open(url)) { // popup blocked, offer direct download:
                  if (confirm('Displaying New Document\n\nUse Save As... to download, then click back to return to this page.')) {
                      location.href = url;
                  }
              }
              return true;
          }

          // do iframe dataURL download (old ch+FF):
          let f = document.createElement('iframe');
          document.body.appendChild(f);

          if (!winMode && /^data:/.test(url)) { // force a mime that will download:
              url = 'data:' + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
          }
          f.src = url;
          setTimeout(function () { document.body.removeChild(f); }, 333);

      } // end saver

      if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or URL)
          return navigator.msSaveBlob(blob, fileName);
      }

      if (self.URL) { // simple fast and modern way using Blob and URL:
          saver(self.URL.createObjectURL(blob), true);
      } else {
          // handle non-Blob()+non-URL browsers:
          if (typeof blob === 'string' || blob.constructor === toString) {
              try {
                  return saver('data:' + mimeType + ';base64,' + self.btoa(blob), false);
              } catch (y) {
                  return saver('data:' + mimeType + ',' + encodeURIComponent(blob), false);
              }
          }

          // Blob but not URL support:
          reader = new FileReader();
          reader.onload = function (e) {
              saver(this.result, false);
          };
          reader.readAsDataURL(blob);
      }
      return true;
  };

  setSelectedDocument() {
      this.showAddDocumentPopup = true;
  }

  loadSupportingDocs(assessId: number) {
      this.busyload = this.docService.getSupportingDocs(assessId).subscribe( data => {
          this.docs = data;
      });
  }

  downloadDoc(asmntdocId: number) {
      this.busyDownload = this.docService.downloadDocument(asmntdocId).subscribe(data => {
          if (data) {
                this.download(atob(data.assessmentPdf), data.filename, "application/json");
            }
      });
  }

  deleteDoc(asmntdocId: number) {
      this.docService.deleteDocument(asmntdocId).subscribe(data => {
          if (data) {
              this.loadSupportingDocs(this.assessmentId);
            }
      });
  }

  documentsGridRefresh(doc: any) {
      if (doc) {
        let document = new QuarterlyAssessmentsSupportingDocument();
        document.assessmentId = this.assessmentId;
        document.documentNumber = doc.documentNumber;
        document.filename = doc.filename;
        document.assessmentPdf = doc.assessmentPdf;
        document.dateUploaded = doc.dateUploaded;
        document.description = doc.description;
        document.versionNum = doc.versionNum;
        this.busyupload = this.docService.saveDoc(document).subscribe(data => {
            if (data) {
            this.loadSupportingDocs(this.assessmentId);
            }
        }, err => {
            console.log("file upload failed: ");
        });
      }
  }
}

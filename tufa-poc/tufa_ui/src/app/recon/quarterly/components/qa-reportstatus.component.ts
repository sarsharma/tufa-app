import { Component, ViewEncapsulation, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { NgBusyModule } from 'ng-busy';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { ShareDataService } from "./../../../core/sharedata.service";
import { QuarterlyAssessmentsService } from "./../../../recon/quarterly/services/qa.service";

declare var jQuery: any;

@Component({
  selector: '[qa-reportstatus]',
  templateUrl: './qa-reportstatus.template.html',
  styleUrls: ['./qa-home.style.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ QuarterlyAssessmentsService ]
})

export class QuarterlyAssessmentsReportStatusComponent implements OnInit, OnDestroy {

  paramsSub: any;
  showErrorFlag: boolean;
  alerts: Array<Object>;
  busy: Subscription;

  shareData: ShareDataService;

  months: string[];
  quarter: number;
  fiscalYear: number;
  submitted: string;
  createdDate: string;
  assessmentId: any;

  data: any;
  result: any;
  gridHeaders: {month:string, abv:string}[];

  constructor(private router: Router,
      private activatedRoute: ActivatedRoute,
      private assessmentService: QuarterlyAssessmentsService,
      private _shareData: ShareDataService
  ) {
    this.router = router;
    this.showErrorFlag = false;
    this.shareData = _shareData;
  }

  ngOnInit(): void {
    this.showErrorFlag = false;

    this.months = this.shareData.months;
    this.quarter = this.shareData.quarter;
    this.fiscalYear = this.shareData.fiscalYear;
    this.submitted = this.shareData.submitted;
    this.createdDate = this.shareData.createdDate;
    this.assessmentId = this.shareData.assessmentId;

    this.getReportStatus(this.quarter, this.fiscalYear);
  }

  ngOnDestroy(){
    if(this.busy){
      this.busy.unsubscribe();
    }
  }

  getReportStatus(quarter: any, year: any) {

    this.showErrorFlag = false;
    this.getGridHeaders(quarter);
    this.busy = this.assessmentService.getReportStatus(quarter, year).subscribe(data => {
      if (data) {
        this.result = data;
        this.data = this.result.result;
      }
    },
      error => {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({ type: 'warning', msg: error });
      });
  }

  getGridHeaders(quarter: any) {
    if (quarter === 1) {
      // this.gridHeaders = ['October', 'November', 'December']
      this.gridHeaders = [{month:'October', abv:'OCT'},{month:'November', abv:'NOV'},{month:'December', abv:'DEC'}];
    } else if (quarter === 2) {
      // this.gridHeaders = ['January', 'February', 'March']
      this.gridHeaders = [{month:'January', abv:'JAN'},{month:'February', abv:'FEB'},{month:'March', abv:'MAR'}];
    } else if (quarter === 3) {
      // this.gridHeaders = ['April', 'May', 'June']
      this.gridHeaders = [{month:'April', abv:'APR'},{month:'May', abv:'MAY'},{month:'June', abv:'JUN'}];
    } else {
      // this.gridHeaders = ['July', 'August', 'September']
      this.gridHeaders = [{month:'July', abv:'JUL'},{month:'August', abv:'AUG'},{month:'September', abv:'SEP'}];
    }
  }

  public sortbyPeriodStatusTypeCol1 = (a: any) => {
    if(a.quarterRpts[0]){
      return a.quarterRpts[0].periodStatusTypeCd;
    }else{
      return "";
    }
  }

  public sortbyPeriodStatusTypeCol2 = (a: any) => {
    if(a.quarterRpts[0]){    
      return a.quarterRpts[0].periodStatusTypeCd;
    }else{
      return "";
    }
  }

  public sortbyPeriodStatusTypeCol3 = (a: any) => {
    if(a.quarterRpts[0]){      
      return a.quarterRpts[0].periodStatusTypeCd;
    }else{
      return "";
    }
  }    

  getStatus(quarterRpts, monthAbv) {
    let returnStr = '';
    for (let i = 0; i < quarterRpts.length; i++) {
      if (quarterRpts[i].month.toLowerCase() === monthAbv.substring(0, 3).toLowerCase()) {
        returnStr = quarterRpts[i].periodStatusTypeCd;
      }
    }
    return returnStr;
  }

  getPeriodId(quarterRpts, month) {
    let returnStr = '';
    for (let i = 0; i < quarterRpts.length; i++) {
      if (quarterRpts[i].month.toLowerCase() === month.substring(0, 3).toLowerCase()) {
        returnStr = quarterRpts[i].periodId;
      }
    }
    return returnStr;
  }

  getIsCigarOnly(quarterRpts, monthAbv) {
    let returnStr = "false";
    for (let i = 0; i < quarterRpts.length; i++) {
      if (quarterRpts[i].month.toLowerCase() === monthAbv.toLowerCase() && quarterRpts[i].cigarOnly === 'CGR_') {
        if (quarterRpts[i].zeroFlag && quarterRpts[i].zeroFlag !== 'Y') {
          returnStr = "true";
        }
      }
    }
    return returnStr;
  }

   toPeriodofActivity(rptStatus: any, month: string) {

        this.shareData.reset();
        this.shareData.companyName = rptStatus.legalNm;
        this.shareData.permitNum = rptStatus.permitNum;
        this.shareData.period = month + ' FY ' + rptStatus.fiscalYr;
        this.shareData.permitId = rptStatus.permitId;
        this.shareData.periodId = Number(this.getPeriodId(rptStatus.quarterReports, month));
        this.shareData.companyId = rptStatus.companyId;

        this.shareData.periodStatus = this.getStatus(rptStatus.quarterReports, month);
        this.shareData.previousRoute = this.router.url;
        this.shareData.isRecon = false;
        this.shareData.breadcrumb = 'qa-reportstatus';
        this.shareData.qatabfrom = 'qa-reportstatus';
        this.shareData.createdDate = this.createdDate;
        this.shareData.submitted = this.submitted;

        this.shareData.quarter = this.quarter;
        this.shareData.fiscalYear = this.fiscalYear;
        this.shareData.months = this.months;
    }
}

import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { ShareDataService } from '../../../core/sharedata.service';

declare var jQuery: any;

@Component({
    selector: '[qa-results]',
    templateUrl: './qa-results.template.html',
    styleUrls:['qa-results.style.scss'],
    providers: [ ]
})

export class QuarterlyAssessmentsResultsComponent implements OnInit {

    public router: Router;
    data: any[];
    quarterFilter: any;
    shareData: ShareDataService;
    sortBy: string = "assessmentYr";
    sortOrder: string[] = ["desc"];

    @Input() assessmentData: any[];
    @Input() filterOptions: any;

    showErrorFlag: boolean;
    alerts: Array<Object>;

    constructor( router: Router,
        private _shareData: ShareDataService
    ) {
        this.router = router;
        this.shareData = _shareData;
        this.showErrorFlag = false;
    }

    ngOnInit(): void {
        this.data = this.assessmentData;
    }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "assessmentData") {
                this.data = changes[propName].currentValue;
            } else if (propName === "filterOptions") {
                this.quarterFilter = changes[propName].currentValue;
            }
        }
    }

    toQuarterlyAssessmentReport(assessment) {
        this.shareData.reset();
        this.shareData.assessmentId = assessment.assessmentId;
        this.shareData.quarter = assessment.assessmentQtr;
        this.shareData.months = assessment.quarterMonths;
        this.shareData.fiscalYear = assessment.assessmentYr;
        this.shareData.submitted = assessment.submittedInd;
        this.shareData.createdDate = assessment.createdDt;
        this.shareData.qatabfrom = 'qa-reportstatus';
    }
}

export class QuarterlyAssessmentsExport {
    title: string;
    csv: string;
    fillFromJSON(json: string) {
        var jsonObj = JSON.parse(json);
        for (var propName in jsonObj) {
            this[propName] = jsonObj[propName];
        }
    }

}

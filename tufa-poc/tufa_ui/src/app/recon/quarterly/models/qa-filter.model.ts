
import { QuarterlyAssessment } from "./../../../recon/quarterly/models/qa.model";

export class QuarterlyAssessmentsFilter {
    quarterFilters: string[] = [];
    statusFilters: string[] = [];
    fiscalYrFilters: string[] = [];
    results: QuarterlyAssessment[];
}
export class QuarterlyAssessmentsFilterData {
    private _reportStatus: any;
    private _reportYears: any;
    private _reportQuarter: any;

    get reportStatus(): any {
        return this._reportStatus;
    }
    set reportStatus(value: any) {
        this._reportStatus = value;
    }

    get reportYears(): any {
        return this._reportYears;
    }

    set reportYears(value: any) {
        this._reportYears = value;
    }

    get reportQuarter(): any {
        return this._reportQuarter;
    }

    set reportQuarter(value: any) {
        this._reportQuarter = value;
    }
}
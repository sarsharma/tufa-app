export class QuarterlyAssessment {
    assessmentId: number;
    assessmentYr: number;
    assessmentQtr: number;
    submittedInd: string;
    assessmentType: string;
    displayQtr: string;
    quarterMonths: string[];
}

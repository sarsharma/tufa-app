import { Injectable } from '@angular/core';
import { QuarterlyAssessment } from "./../../../recon/quarterly/models/qa.model";
import { QuarterlyAssessmentsFilter } from "./../../../recon/quarterly/models/qa-filter.model";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};


@Injectable()
export class QuarterlyAssessmentsCreationService {
    private getAssessmentsurl = environment.apiurl + '/api/reports/quarter';
    private getAnnualTrueUpsurl = environment.apiurl + '/api/v1/trueup/list';
    private getFiscalYearsurl = environment.apiurl + '/api/reports/quarter/fiscalyears';

    constructor(private http: HttpClient) { }

    /**
     * 
     * 
     * @param {Assessment} quarterlyAssessment
     * @returns {Observable<String>}
     * 
     * @memberOf CreateQuarters
     */
    saveQuarterlyReport(quarterlyAssessment: QuarterlyAssessment): Observable<String> {
        return this.http.put(this.getAssessmentsurl + '/create', quarterlyAssessment, httpOptions).pipe(
            tap(_ => this.log("saveQuarterlyReport")),
            catchError(this.handleError<any>("saveQuarterlyReport"))
        );
    }

     /**
      * 
      * 
      * @param {AssessmentFilter} assessmentFilter
      * @returns
      * 
      * @memberOf CreateQuarters
      */
     getAssessments(assessmentFilter: QuarterlyAssessmentsFilter) {
        return this.http.put(this.getAssessmentsurl + '/assessments', assessmentFilter, httpOptions);
    }

    /**
     * 
     * 
     * @returns {Observable<string>}
     * 
     * @memberOf CreateQuarters
     */
    getFiscalYears(): Observable<string>{
        return this.http.get(this.getFiscalYearsurl, httpOptions).pipe(
            tap(_ => this.log("getFiscalYears")),
            catchError(this.handleError<any>("getFiscalYears"))
        );
    }

    	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}

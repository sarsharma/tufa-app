/*
 @author : JBS
 Service class to perform PermitPeriod related operations.
 */

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from "rxjs/operators";
import { environment } from '../../../../environments/environment';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class QuarterlyAssessmentsExportService {

    private getexporturl = environment.apiurl + '/api/reports/quarter/export';

    constructor(private http: HttpClient) { }

    public loadAssessmentExport(assessmentId: string): Observable<string> {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        return this.http.get(this.getexporturl + '/' + assessmentId, httpOptions).pipe(
			tap(_ => this.log("getUsers")),
			catchError(this.handleError<any>("getUsers"))
		);
    }

    	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}

}

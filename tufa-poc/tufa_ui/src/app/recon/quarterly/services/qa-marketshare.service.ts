/*
@author : Deloitte
Service class to perform assessments related operations.
*/

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { environment } from '../../../../environments/environment';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};
/**
 * 
 * 
 * @export
 * @class AssessmentService
 */
@Injectable()
export class QuarterlyAssessmentsMarketShareService {

    private getMarketShareurl = environment.apiurl + '/api/reports/quarter/export/';

    constructor(private http: HttpClient) { }

    public getMarketShare(year: any, quarter: any): Observable<string> {
         let url = this.getMarketShareurl + quarter + "/" + year;
         return this.http.get(url, httpOptions).pipe(
          tap(_ => this.log("updateComment")),
          catchError(this.handleError<any>("updateComment"))
      );
    }

    public generateMarketShare(year: any, quarter: any): Observable<string> {
         let url = this.getMarketShareurl + quarter + "/" + year;
         return this.http.put(url, null, httpOptions).pipe(
          tap(_ => this.log("updateComment")),
          catchError(this.handleError<any>("updateComment"))
      );
    }
  
    public submitMarketShare(year: any, quarter: any): Observable<string> {
         let url = this.getMarketShareurl + quarter + "/" + year;
         return this.http.post(url, null, httpOptions).pipe(
          tap(_ => this.log("updateComment")),
          catchError(this.handleError<any>("updateComment"))
      );
    }

    	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import { environment } from '../../../../environments/environment';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class QuarterlyAssessmentsReconReportService {
    private baseurl= environment.apiurl + '/api/';
    private basereconurl = environment.apiurl + '/api/v1/recon';
    private qtrReconReporturl= '';

    constructor(private http: HttpClient) { }

     /**
      * 
      * 
      * @param {string} fiscalYr
      * @param {string} qtr
      * @returns
      * 
      * @memberOf QtrReconReportService
      */
     public getReconReportDetails(fiscalYr: string, qtr: string) {
         let headers = new Headers({ 'Content-Type': 'application/json'});
         let url = this.baseurl + "reports/quarter/recon/fiscalyear/" + fiscalYr + "/quarter/" + qtr;
         return this.http.get(url, httpOptions);
     }

     /**
      * 
      * 
      * @param {string} fiscalYr
      * @param {string} qtr
      * @returns
      * 
      * @memberOf QtrReconReportService
      */
     public exportReconReportData(fiscalYr: string, qtr: string) {
         let headers = new Headers({ 'Content-Type': 'application/json'});
         let url = this.basereconurl + "/export/fiscalyear/" + fiscalYr + "/quarter/" + qtr;
         return this.http.get(url, httpOptions);
     }

}

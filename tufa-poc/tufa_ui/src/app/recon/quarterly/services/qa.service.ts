
/*
@author : Deloitte
Service class to perform assessments related operations.
*/

import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { QuarterlyAssessmentsSupportingDocument } from '../models/qa-supportdoc.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};
/**
 *
 *
 * @export
 * @class AssessmentService
 */
@Injectable()
export class QuarterlyAssessmentsService {

    private getReportStatusurl = environment.apiurl + '/api/reports/quarter/reportstatus/';
    private quarterlyassessmentBaseurl = environment.apiurl + '/api/reports/quarter/';

    constructor(private http: HttpClient) { }

    public getReportStatus(quarter: any, year: any): Observable<string> {
        return this.http.get(this.getReportStatusurl + quarter + '/' + year, httpOptions).pipe(
            tap(_ => this.log("getReportStatus")),
            catchError(this.handleError<any>("getReportStatus"))
        );
    }

    public saveDoc(doc: QuarterlyAssessmentsSupportingDocument) {
        let url = this.quarterlyassessmentBaseurl + "docs/" + doc.assessmentId;
        return this.http.put(url, doc, httpOptions).pipe(
            tap(_ => this.log("saveDoc")),
            catchError(this.handleError<any>("saveDoc"))
        );
    }

    public getSupportingDocs(assessId: number) {
        let url = this.quarterlyassessmentBaseurl + "" + assessId + "/docs";
        return this.http.get(url, httpOptions).pipe(
            tap(_ => this.log("getSupportingDocs")),
            catchError(this.handleError<any>("getSupportingDocs"))
        );
    }

    public deleteDocument(asmntdocId: number) {
        let url = this.quarterlyassessmentBaseurl + "docs/" + asmntdocId;
        return this.http.delete(url, httpOptions).pipe(
            tap(_ => this.log("deleteDocument")),
            catchError(this.handleError<any>("deleteDocument"))
        );
    }

    public downloadDocument(asmntdocId: number) {
        let url = this.quarterlyassessmentBaseurl + "docs/" + asmntdocId;
        return this.http.get(url, httpOptions).pipe(
            tap(_ => this.log("downloadDocument")),
            catchError(this.handleError<any>("downloadDocument"))
        );
    }

    	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { QuarterlyAssessmentsFilterData } from "./../../../recon/quarterly/models/qa-filter.model";

/**
 * 
 * 
 * @export
 * @class PermitReportsDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "quarterpipe"
})
export class QuarterlyAssessmentDataFilterPipe implements PipeTransform {

    /**
     * 
     * 
     * @param {any[]} data
     * @param {AnnualFilter} filter
     * @returns {*}
     * 
     * @memberOf PermitReportsDataFilterPipe
     */
    transform(data: any[], filter: QuarterlyAssessmentsFilterData): any {
        let results = data;

        if (data) {
            if (filter) {
                // filter by years
                if (typeof filter.reportYears !== 'undefined' && filter.reportYears.length > 0) {
                   results =  _.filter(results, function(item) {
                        return _.includes(Array.from(filter.reportYears), String(item.assessmentYr));
                    });
                }
                // filter by quarter
                if (typeof filter.reportQuarter !== 'undefined' && filter.reportQuarter.length > 0) {
                   results =  _.filter(results, function(item) {
                        return _.includes(Array.from(filter.reportQuarter), String(item.assessmentQtr));
                    });
                }
                // filter by status
                if (typeof filter.reportStatus !== 'undefined' && filter.reportStatus.length > 0) {
                   results =  _.filter(results, function(item) {
                        return _.includes(Array.from(filter.reportStatus), item.submittedInd);
                    });
                }
            }
        }
        return results;
    }
// tslint:disable-next-line:eofline
}
/*
@author : Deloitte

this is Component for editing company address.
*/

import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '../../../node_modules/@angular/forms';
import { MarketShareData } from './cigars/models/market-share-data.model';
import { Document } from './recon-document.model';

declare var jQuery: any;

/**
 *
 *
 * @export
 * @class EditDocumentPopup
 */
@Component({
    selector: '[add-document]',
    templateUrl: './recon-add-document.template.html',
    styleUrls: ['./recon-add-document.style.scss']
})

export class AddDocumentPopup implements OnInit {
    @Input() versions: any[];
    @Input() quarters: MarketShareData[];
    @Input() docs: any[];
    @Output() onDocumentEdit = new EventEmitter<Document>();
    selectedQuarter: MarketShareData;
    filesToUpload: Array<File> = [];
    docFileName: string;
    selFormTypes: string[];
    description: string;
    version: string = '';
    @ViewChild('addDocumentform') addDocumentform: NgForm;

    ngOnInit() {

    }

    getFilePathExtension(path) {
        let filename = path.split('\\').pop().split('/').pop();
        return filename.substr((Math.max(0, filename.lastIndexOf(".")) || Infinity) + 1);
    }

    fileChangeEvent(event) {
        this.filesToUpload = <Array<File>>event.target.files;
        if (!this.filesToUpload[0]) return;
        this.docFileName = this.filesToUpload[0].name;
        let ext = this.getFilePathExtension(this.docFileName);
        jQuery('#fileUpload').parsley().removeError('Invalid-Ext');
        switch (ext.toLowerCase()) {
            case 'pdf':
            case 'msg':
                break;
            default:
                let invalidexterror = "Error: Invalid File Extension";
                jQuery('#fileUpload').parsley().addError('Invalid-Ext', { message: invalidexterror });
                break;
        }
    }

    fileNameEvent(event) {
        let ext = this.getFilePathExtension(event.target.value);
        jQuery('#fileNm').parsley().removeError('Invalid-Ext');
        switch (ext.toLowerCase()) {
            case 'pdf':
            case 'msg':
            case '':
                break;
            default:
                jQuery('#fileNm').parsley().addError('Invalid-Ext', { message: "Error: Invalid File Extension" });
                break;
        }
    }

    finalDocName(filename: string) {
        let finalname = filename;
        switch (this.getFilePathExtension(this.filesToUpload[0].name).toLowerCase()) {
            case 'pdf': if (finalname.toLowerCase().indexOf("pdf") === -1) finalname = finalname.trim() + ".pdf";
                return finalname;
            case 'msg': if (finalname.toLowerCase().indexOf("msg") === -1) finalname = finalname.trim() + ".msg";
                return finalname;
            default: return finalname;
        }
    }

    uploadAttachment() {
        jQuery('#addDocumentform').parsley().validate();
        // Toggle to edit mode or navigate to the next screen if validation passes
        if (jQuery('#addDocumentform').parsley().isValid()) {
            let fileReader = new FileReader();
            fileReader.onloadend = () => {
                let document = new Document();
                document.documentNumber = this.docs ? this.docs.length + 1 : 1;
                document.filename = this.finalDocName(this.docFileName);
                document.assessmentPdf = <string>fileReader.result;
                document.dateUploaded = new Date();
                document.description = this.description;
                if(this.selectedQuarter) {
                    document.cigarQtr = this.selectedQuarter.assessmentQtr;
                }
                document.versionNum = Number(this.version);
                this.onDocumentEdit.emit(document);
                this.filesToUpload = [];
                this.reset();
            };
            fileReader.readAsDataURL(this.filesToUpload[0]);
        }
    }

    reset() {
        // jQuery('#addDocumentform').parsley().reset();
        jQuery('#addDocumentform').find('input').val('');
        jQuery('#addDocumentform').find('select').val('');
        jQuery('.fileinput').fileinput('clear');
        // this.docFileName = "";
        if (this.addDocumentform) {
            this.addDocumentform.reset();
        }
        if (typeof jQuery('#addDocumentform').parsley() != "undefined") {
            jQuery('#addDocumentform').parsley().reset();
        }
        jQuery('#add-document').modal('hide');

        
        this.selectedQuarter = null;
        //this.versions = [];
       // this.version = '';
    }

    updateVersions() {
        console.log(this.selectedQuarter);
        this.versions = [];
        this.version = "";
        if (this.selectedQuarter) {
            for (let version of this.selectedQuarter.versions) {
                if (version.versionNum != 0) {
                    this.versions.push(version.versionNum);
                }
            }
        }
    }

}

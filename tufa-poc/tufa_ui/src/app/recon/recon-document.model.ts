export class Document {
    documentNumber: number;
    filename: string;
    description: string;
    versionNum: number;
    cigarQtr: number;
    dateUploaded: Date;
    author: string;
    assessmentPdf: string;
    createdBy: string;
    createdDt: Date;
    modifiedBy: string;
    modifiedDt: Date;
}
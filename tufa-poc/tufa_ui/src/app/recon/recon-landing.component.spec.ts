import { TestBed, ComponentFixture, async } from "../../../node_modules/@angular/core/testing";
import { ReconciliationLandingPage } from "./recon-landing.component";
import { SharedModule } from "../shared/shared.module";
import { HttpModule } from "../../../node_modules/@angular/http";
import { RouterTestingModule } from "../../../node_modules/@angular/router/testing";
import { NO_ERRORS_SCHEMA } from "../../../node_modules/@angular/core";
import { HttpClient } from "../../../node_modules/@angular/common/http";
import { AuthorizationHelper } from "../authentication/util/authorization.helper.util";
import { provideRoutes } from "../../../node_modules/@angular/router";
/*
@author : Deloitte
this is Component for Recon Landing Page
*/

declare var jQuery: any;

describe('Recond Creation Page', () => {
  let comp:    ReconciliationLandingPage;
  let fixture: ComponentFixture<ReconciliationLandingPage>;

  // provide our implementations or mocks to the dependency injector
  beforeEach( async( () => {
    TestBed.configureTestingModule({
      imports: [ SharedModule, HttpModule, RouterTestingModule ],
      declarations: [ReconciliationLandingPage],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [HttpClient, AuthorizationHelper,
        provideRoutes([{path: 'fakeRouteForTesting', redirectTo: 'fakeRouteForTesting', pathMatch:'full'}])
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconciliationLandingPage);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

});

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs';
import { TabsetComponent } from '../../../node_modules/ngx-bootstrap';
import { ShareDataService } from "./../core/sharedata.service";
import { AnnualTrueUpFilter, AnnualTrueUpFilterData } from "./../recon/annual/models/at-filter.model";
import { AnnualTrueUpCreationService } from "./../recon/annual/services/at-create.service";
import { CigarsAssessmentsFilter, CigarsAssessmentsFilterData } from "./../recon/cigars/models/ca-filter.model";
import { CigarsAssessmentsService } from "./../recon/cigars/services/ca.service";
import { QuarterlyAssessmentsFilter, QuarterlyAssessmentsFilterData } from "./../recon/quarterly/models/qa-filter.model";
import { QuarterlyAssessment } from "./../recon/quarterly/models/qa.model";
import { QuarterlyAssessmentsCreationService } from "./../recon/quarterly/services/qa-create.service";
import { AffectedAssessmentsReconFilter, AffectedAssessmentsReconFilterData } from './model/aa-reconfilter.model';
import { ReconAffectedAssessmentService } from './services/recon-affected-assessment.service';



declare var jQuery: any;

@Component({
    selector: '[recon-landing]',
    templateUrl: './recon-landing.template.html',
    styleUrls: ['./recon-landing.style.scss'],
    providers: [QuarterlyAssessment]
})

export class ReconciliationLandingPage implements OnInit, OnDestroy {
    @ViewChild('reconTabs') reconTabs: TabsetComponent;

    showErrorFlag: boolean;
    alerts: Array<Object>;
    busy: Subscription;
    trueUpbusy: Subscription;
    cigarAssessBusy: Subscription;
    annualActionBusy: boolean = false;
    switchFilters: Boolean = false;
    recontabfrom: string;

    shareData: ShareDataService;

    modPrefix = "";

    assessmentData: any[];
    annualData: any[];
    cigarAssessData: any[];
    annualActions: number[];

    quarterlyfilter = new QuarterlyAssessmentsFilter();
    quarterlyfilterdata = new QuarterlyAssessmentsFilterData();
    annualfilter = new AnnualTrueUpFilter();
    annualfilterdata = new AnnualTrueUpFilterData();
    cigarfilter = new CigarsAssessmentsFilter();
    cigarfilterdata = new CigarsAssessmentsFilterData();

    // tslint:disable-next-line:max-line-length
    quarterOptions = [{ name: '1 - Oct, Nov, Dec', value: '1' }, { name: '2 - Jan, Feb, Mar', value: '2' }, { name: '3 - Apr, May, Jun', value: '3' }, { name: '4 - Jul, Aug, Sep', value: '4' }];
    quarterOptionsMap = { 1: false, 2: false, 3: false, 4: false };

    statusOptions = [{ name: 'Submitted', value: 'Y' }, { name: 'Not Submitted', value: 'N' }];
    statusOptionsMap = { 'Submitted': false, 'Not Submitted': false };

    annualStatusOptions = [{ name: 'Submitted', value: 'Y' }, { name: 'Not Submitted', value: 'N' }];
    annualStatusOptionsMap = { 'Submitted': false, 'Not Submitted': false };

    cigarAssessStatusOptions = [{ name: 'Submitted (1/4)', value: 'Y' }, { name: 'Submitted (2/4)', value: 'Y' }, { name: 'Submitted (3/4)', value: 'Y' }, { name: 'Submitted (4/4)', value: 'Y' }, { name: 'Not Submitted', value: 'N' }];
    cigarAssessStatusOptionsMap = { 'Submitted (1/4)': false, 'Submitted (2/4)': false, 'Submitted (3/4)': false, 'Submitted (4/4)': false, 'Not Submitted': false };

    switchFilter = 1;  // 1- Quarterly 2 - Cigars  3 - Annual TrueUp

    yearOptions: Array<string>;
    chkyearOptions: string[];

    showMoreLimit = 5;

    affectedAssessmentData = [];
    data: any[];
    sortBy1: string = "assessmentFY";
    sortOrder1: string[] = ["desc"];
    affectedassessmentsfilter = new AffectedAssessmentsReconFilter();
    affectedassessmentsfilterdata = new AffectedAssessmentsReconFilterData();
    assessmentFYSelected: string;
    assessmentQTRSelected: string;
    assessmentTypeSelected: string;
    companyNameSelected: string;
    einSelected: string;
    permitNumberSelected: string;

    public columns: Array<any> = [
        { name: 'assessmentFY', type: 'input', filtering: { filterString: '', columnName: 'assessmentFY', placeholder: 'Assessment FY' } },
        { name: 'assessmentQTR', type: 'input', filtering: { filterString: '', columnName: 'assessmentQTR', placeholder: 'Assessment QTR' } },
        { name: 'assessmentType', type: 'input', filtering: { filterString: '', columnName: 'assessmentType', placeholder: 'Assessment Type' } },
        { name: 'companyName', type: 'input', filtering: { filterString: '', columnName: 'companyName', placeholder: 'Company Name' } },
        { name: 'ein', type: 'input', filtering: { filterString: '', columnName: 'ein', placeholder: 'EIN' } },
        { name: 'permitNumber', type: 'input', filtering: { filterString: '', columnName: 'permitNumber', placeholder: 'Permit Number' } }
    ];


    constructor(private router: Router,
        private quarterlyService: QuarterlyAssessmentsCreationService,
        private cigarAssessmentService: CigarsAssessmentsService,
        private annualtrueupService: AnnualTrueUpCreationService,
        private storage: LocalStorageService,
        private sessionStorage: SessionStorageService,
        private _shareData: ShareDataService,
        private reconAffectedAssessmentService: ReconAffectedAssessmentService) {
        this.router = router;
        this.showErrorFlag = false;
        this.shareData = _shareData;
    }

    ngOnInit(): void {

        this.showErrorFlag = false;
        this.chkyearOptions = [];

        this.initializeSearchCriteria();
        this.updateFilters();

        this.getQuarterlyAssessments();
        this.getTrueUps();
        this.getCigarAssessments();
        this.getTrueUpAction();
        this.getAffectedAssessmentData();
        this.refreshAffectedAssessmentFilterObject();

        this.cleanupSearchFilters();

        this.recontabfrom = this.shareData.qatabfrom;

        this.sessionStorage.clear('compare-filters');

        this.setActiveTab();
    }

    getAffectedAssessmentData() {
        // this.affectedAssessmentData=null;
        this.reconAffectedAssessmentService.getAffectedAssessmentsJSON().subscribe(
            data => {
                if (data) {
                    this.data = <any[]>data;
                    //this.affectedAssessmentData = <any[]>data;
                    //this.getAATabData('Quaterly');
                }
            });
    }

    ApplyFilter() {
        this.refreshAffectedAssessmentFilterObject();
    }

    public onChangeTable(filterinfo): void {
        if (filterinfo) {
            if (filterinfo.columnName === "assessmentFY") {
                this.affectedassessmentsfilter.assessmentFYFilter = filterinfo.filterString;
                this.assessmentFYSelected = filterinfo.filterString;
                this.affectedassessmentsfilter.assessmentFYFilter = this.affectedassessmentsfilter.assessmentFYFilter.trim();
            }
            if (filterinfo.columnName === "assessmentQTR") {
                this.affectedassessmentsfilter.assessmentQTRFilter = filterinfo.filterString;
                this.assessmentQTRSelected = filterinfo.filterString;
                this.affectedassessmentsfilter.assessmentQTRFilter = this.affectedassessmentsfilter.assessmentQTRFilter.trim();
            }
            if (filterinfo.columnName === "assessmentType") {
                this.affectedassessmentsfilter.assessmentTypeFilter = filterinfo.filterString;
                this.assessmentTypeSelected = filterinfo.filterString;
                this.affectedassessmentsfilter.assessmentTypeFilter = this.affectedassessmentsfilter.assessmentTypeFilter.trim();
            }
            if (filterinfo.columnName === "companyName") {
                this.affectedassessmentsfilter.companyNameFilter = filterinfo.filterString;
                this.companyNameSelected = filterinfo.filterString;
                this.affectedassessmentsfilter.companyNameFilter = this.affectedassessmentsfilter.companyNameFilter.trim();
            }
            if (filterinfo.columnName === "ein") {
                this.affectedassessmentsfilter.einFilter = filterinfo.filterString;
                this.einSelected = filterinfo.filterString;
                this.affectedassessmentsfilter.einFilter = this.affectedassessmentsfilter.einFilter.trim();
            }
            if (filterinfo.columnName === "permitNumber") {
                this.affectedassessmentsfilter.permitNumberFilter = filterinfo.filterString;
                this.permitNumberSelected = filterinfo.filterString;
                this.affectedassessmentsfilter.permitNumberFilter = this.affectedassessmentsfilter.permitNumberFilter.trim();
            }
            if (filterinfo.refresh) {
                this.refreshAffectedAssessmentFilterObject();
            }
        }
    }

    resetFilterData() {
        this.affectedassessmentsfilterdata = new AffectedAssessmentsReconFilterData();

        for (let i in this.columns) {

            if (this.columns[i].name === "assessmentFY") {
                this.updateFilterTextBox('assessmentFY', "");
            }

            if (this.columns[i].name === "assessmentQTR") {
                this.updateFilterTextBox('assessmentQTR', "");
            }

            if (this.columns[i].name === "assessmentType") {
                this.updateFilterTextBox('assessmentType', "");
            }

            if (this.columns[i].name === "companyName") {
                this.updateFilterTextBox('companyName', "");
            }

            if (this.columns[i].name === "ein") {
                this.updateFilterTextBox('ein', "");
            }

            if (this.columns[i].name === "permitNumber") {
                this.updateFilterTextBox('permitNumber', "");
            }
        }

    }

    restoreFilterData(filterData) {

        if (!filterData) return;

        this.affectedassessmentsfilterdata = new AffectedAssessmentsReconFilterData();

        for (let i in this.columns) {

            if (this.columns[i].name === "assessmentFY") {
                this.columns[i].filtering.filterString = filterData._assessmentFYFilter;
                this.affectedassessmentsfilterdata._assessmentFYFilter = filterData._assessmentFYFilter;
                this.assessmentFYSelected = filterData._assessmentFYFilter;
                this.updateFilterTextBox('assessmentFY', filterData._assessmentFYFilter);

            }

            if (this.columns[i].name === "assessmentQTR") {
                this.columns[i].filtering.filterString = filterData._assessmentQTRFilter;
                this.affectedassessmentsfilterdata._assessmentQTRFilter = filterData._assessmentQTRFilter;
                this.assessmentQTRSelected = filterData._assessmentQTRFilter;
                this.updateFilterTextBox('assessmentQTR', filterData._assessmentQTRFilter);

            }

            if (this.columns[i].name === "assessmentType") {
                this.columns[i].filtering.filterString = filterData._assessmentTypeFilter;
                this.affectedassessmentsfilterdata._assessmentTypeFilter = filterData._assessmentTypeFilter;
                this.assessmentTypeSelected = filterData._assessmentTypeFilter;
                this.updateFilterTextBox('assessmentType', filterData._assessmentTypeFilter);
            }

            if (this.columns[i].name === "companyName") {
                this.columns[i].filtering.filterString = filterData._companyNameFilter;
                this.affectedassessmentsfilterdata._companyNameFilter = filterData._companyNameFilter;
                this.companyNameSelected = filterData._companyNameFilter;
                this.updateFilterTextBox('companyName', filterData._companyNameFilter);
            }

            if (this.columns[i].name === "ein") {
                this.columns[i].filtering.filterString = filterData._einFilter;
                this.affectedassessmentsfilterdata._einFilter = filterData._einFilter;
                this.einSelected = filterData._einFilter;
                this.updateFilterTextBox('ein', filterData._einFilter);
            }

            if (this.columns[i].name === "permitNumber") {
                this.columns[i].filtering.filterString = filterData._permitNumberFilter;
                this.affectedassessmentsfilterdata._permitNumberFilter = filterData._permitNumberFilter;
                this.permitNumberSelected = filterData._permitNumberFilter;
                this.updateFilterTextBox('permitNumber', filterData._permitNumberFilter);
            }
        }
    }

    updateFilterTextBox(colName, colText) {
        let rowIndex = _.findIndex(this.columns, function (o) { return (o.name && o.name.toString() === colName) });
        if (rowIndex !== -1) {
            let selRow = this.columns[rowIndex];
            if (selRow.filtering) {
                selRow.filtering.filterString = colText;
            }
        }
    }

    isEmpty(val) {
        return (val === undefined || val == null) ? true : false;
    }

    refreshAffectedAssessmentFilterObject() {
        this.affectedassessmentsfilterdata = null;
        this.affectedassessmentsfilterdata = new AffectedAssessmentsReconFilterData();
        this.affectedassessmentsfilterdata.assessmentFYFilter = this.isEmpty(this.assessmentFYSelected) ? this.affectedassessmentsfilter.assessmentFYFilter : this.assessmentFYSelected;
        this.affectedassessmentsfilterdata.assessmentQTRFilter = this.isEmpty(this.assessmentQTRSelected) ? this.affectedassessmentsfilter.assessmentQTRFilter : this.assessmentQTRSelected;
        this.affectedassessmentsfilterdata.assessmentTypeFilter = this.isEmpty(this.assessmentTypeSelected) ? this.affectedassessmentsfilter.assessmentTypeFilter : this.assessmentTypeSelected;
        this.affectedassessmentsfilterdata.companyNameFilter = this.isEmpty(this.companyNameSelected) ? this.affectedassessmentsfilter.companyNameFilter : this.companyNameSelected;
        this.affectedassessmentsfilterdata.einFilter = this.isEmpty(this.einSelected) ? this.affectedassessmentsfilter.einFilter : this.einSelected;
        this.affectedassessmentsfilterdata.permitNumberFilter = this.isEmpty(this.permitNumberSelected) ? this.affectedassessmentsfilter.permitNumberFilter : this.permitNumberSelected;

    }

    getAATabData(option) {
        this.data = [];
        if (option === "Quaterly") {
            for (let x in this.affectedAssessmentData) {
                if (this.affectedAssessmentData[x].assessmentType === "Quaterly") {
                    this.data.push(this.affectedAssessmentData[x]);
                }
            }
        }

        else if (option === "Cigar") {
            for (let x in this.affectedAssessmentData) {
                if (this.affectedAssessmentData[x].assessmentType === "Cigar") {
                    this.data.push(this.affectedAssessmentData[x]);
                }
            }
        }

        else if (option === "Annual") {
            for (let x in this.affectedAssessmentData) {
                if (this.affectedAssessmentData[x].assessmentType === "Annual") {
                    this.data.push(this.affectedAssessmentData[x]);
                }
            }
        }
        this.resetFilterData();
    }


    public setActiveTab() {
        if (this.recontabfrom === 'at-create' || this.recontabfrom === 'at-ingest') {
            this.reconTabs.tabs[2].active = true;
            this.switchFilter = 3;
        } else if (this.recontabfrom === 'ca-create' || this.recontabfrom === 'ca-reportstatus' || this.recontabfrom === 'ca-reconreport') {
            this.reconTabs.tabs[1].active = true;
            this.switchFilter = 2;
        } else {
            this.reconTabs.tabs[0].active = true;
            this.switchFilter = 1;
        }
    }

    ngOnDestroy() {
        if (this.busy) {
            this.busy.unsubscribe();
        }

        if (this.trueUpbusy) {
            this.trueUpbusy.unsubscribe();
        }

        if (this.cigarAssessBusy) {
            this.cigarAssessBusy.unsubscribe();
        }

    }

    // recontabfrom determines the source of origin of navigation. Depending on reconciliation or quarterly assessment
    // appropriate tabs will be shown as active.
    ngAfterViewInit() {
        setTimeout(() => {

        }, 100);
    }

    initializeSearchCriteria() {

        this.quarterlyService.getFiscalYears().subscribe(
            data => {
                //TODO add if (data) { ...}
                let nd = data;
                this.yearOptions = [];
                for (let i = 0; i < nd.length; i++) {
                    this.yearOptions.push(nd[i]);
                }
            }, error => {
                this.showErrorFlag = true;
                this.alerts = [];
                this.alerts.push({ type: 'warning', msg: error });
            });
    }

    getQuarterlyAssessments() {

        this.showErrorFlag = false;
        this.assessmentData = null;

        // create a filter Object
        let filter = new QuarterlyAssessmentsFilter();

        // since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
        if (this.busy) {
            this.busy.unsubscribe();
        }

        this.busy = this.quarterlyService.getAssessments(filter).subscribe(data => {
            if (data) {
                this.quarterlyfilter = <QuarterlyAssessmentsFilter>data;
                this.assessmentData = this.quarterlyfilter.results;
            }
        },
            error => {
                this.showErrorFlag = true;
                this.alerts = [];
                this.alerts.push({ type: 'warning', msg: error });
            });
    }

    getTrueUps() {
        this.annualData = null;
        // create a filter Object
        let annualfilter = new AnnualTrueUpFilter();

        // since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
        if (this.trueUpbusy) {
            this.trueUpbusy.unsubscribe();
        }

        this.trueUpbusy = this.annualtrueupService.getTrueUps(annualfilter).subscribe(data => {
            if (data) {
                this.annualfilter = <AnnualTrueUpFilter>data;
                this.annualData = this.annualfilter.results;
            }
        },
            error => {
                this.showErrorFlag = true;
                this.alerts = [];
                this.alerts.push({ type: 'warning', msg: error });
            });
    }

    getCigarAssessments() {

        this.annualData = null;
        // create a filter Object
        let cigarfilter = new CigarsAssessmentsFilter();

        // since the load may take long time, when user checks multiple filter criteria, we need to
        // abort earlier calls and re-initiate a new call
        if (this.cigarAssessBusy) {
            this.cigarAssessBusy.unsubscribe();
        }

        this.cigarAssessBusy = this.cigarAssessmentService.getCigarAssessments(cigarfilter).subscribe(data => {
            if (data) {
                this.cigarfilter = <CigarsAssessmentsFilter>data;
                this.cigarAssessData = this.cigarfilter.results;
            }
        },
            error => {
                this.showErrorFlag = true;
                this.alerts = [];
                this.alerts.push({ type: 'warning', msg: error });
            });


    }

    getTrueUpAction() {
        this.annualActionBusy = true;
        this.annualtrueupService.getFiscalYearsWithNewDeltas().subscribe(data => {
            this.annualActionBusy = false;
            this.annualActions = data;
        }, error => {
            this.annualActionBusy = false;
            throw(error);
        });
    }

    updateFilters() {

        // initialize status filters array cigar assessment
        this.cigarfilter.statusFilters = [];
        for (let x in this.cigarAssessStatusOptionsMap) {
            if (this.cigarAssessStatusOptionsMap[x]) {
                this.cigarfilter.statusFilters.push(x);
            }
        }

        // initialize status filters array true ups
        this.annualfilter.statusFilters = [];
        for (let x in this.annualStatusOptionsMap) {
            if (this.annualStatusOptionsMap[x]) {
                this.annualfilter.statusFilters.push(x);
            }
        }

        // initialize status filters array assessments
        this.quarterlyfilter.statusFilters = [];
        for (let x in this.statusOptionsMap) {
            if (this.statusOptionsMap[x]) {
                this.quarterlyfilter.statusFilters.push(x);
            }
        }

        // initialize month filters array
        this.quarterlyfilter.quarterFilters = [];
        for (let x in this.quarterOptionsMap) {
            if (this.quarterOptionsMap[x]) {
                this.quarterlyfilter.quarterFilters.push(x);
            }
        }

        // initialize year filters array
        this.quarterlyfilter.fiscalYrFilters = [];
        for (let x of this.chkyearOptions) {
            this.quarterlyfilter.fiscalYrFilters.push(x);
        }

    }

    updateCigarAssessStatusOptions(option, event) {
        this.cigarAssessStatusOptionsMap[option] = event.target.checked;
        this.cigarfilter.statusFilters = [];
        for (let x in this.cigarAssessStatusOptionsMap) {
            if (this.cigarAssessStatusOptionsMap[x]) {
                this.cigarfilter.statusFilters.push(x);
            }
        }
        this.refreshCigarAssessFilterObject();
    }

    updateAnnualStatusOptions(option, event) {
        this.annualStatusOptionsMap[option] = event.target.checked;
        this.annualfilter.statusFilters = [];
        for (let x in this.annualStatusOptionsMap) {
            if (this.annualStatusOptionsMap[x]) {
                this.annualfilter.statusFilters.push(x);
            }
        }
        this.refreshAnnualFilterObject();
    }

    updateCheckedStatusOptions(option, event) {
        this.statusOptionsMap[option] = event.target.checked;
        this.quarterlyfilter.statusFilters = [];
        for (let x in this.statusOptionsMap) {
            if (this.statusOptionsMap[x]) {
                this.quarterlyfilter.statusFilters.push(x);
            }
        }
        this.refreshQuarterFilterObject();
    }

    updateCheckedQuarterOptions(option, event) {
        this.quarterOptionsMap[option] = event.target.checked;

        this.quarterlyfilter.quarterFilters = [];
        for (let x in this.quarterOptionsMap) {
            if (this.quarterOptionsMap[x]) {
                this.quarterlyfilter.quarterFilters.push(x);
            }
        }
        this.refreshQuarterFilterObject();
    }

    onChangeYearSelection(option, $event: any) {
        if ($event.target.checked) {
            this.chkyearOptions.push($event.target.value);
        } else {
            this.chkyearOptions.splice(this.chkyearOptions.indexOf(option), 1);
        }

        this.quarterlyfilter.fiscalYrFilters = [];
        for (let x of this.chkyearOptions) {
            this.quarterlyfilter.fiscalYrFilters.push(x);
        }
        this.refreshQuarterFilterObject();
    }

    isYearChecked(option) {
        if (this.chkyearOptions) {
            return (this.chkyearOptions.indexOf(option) !== -1);
        }
        return false;
    }

    refreshCigarAssessFilterObject() {
        this.cigarfilterdata = null;
        this.cigarfilterdata = new CigarsAssessmentsFilterData();
        this.cigarfilterdata.reportStatus = this.cigarfilter.statusFilters;
        this.cigarfilterdata.reportYears = this.cigarfilter.fiscalYrFilters;
    }

    refreshAnnualFilterObject() {
        this.annualfilterdata = null;
        this.annualfilterdata = new AnnualTrueUpFilterData();
        this.annualfilterdata.reportStatus = this.annualfilter.statusFilters;
        this.annualfilterdata.reportYears = this.annualfilter.fiscalYrFilters;
    }

    refreshQuarterFilterObject() {
        this.quarterlyfilterdata = null;
        this.quarterlyfilterdata = new QuarterlyAssessmentsFilterData();
        this.quarterlyfilterdata.reportStatus = this.quarterlyfilter.statusFilters;
        this.quarterlyfilterdata.reportYears = this.quarterlyfilter.fiscalYrFilters;
        this.quarterlyfilterdata.reportQuarter = this.quarterlyfilter.quarterFilters;
    }

    toCreateQuarterlyAssessment() {
        this.shareData.reset();
        this.shareData.breadcrumb = 'qa-create';
        this.shareData.qatabfrom = 'qa-create';
        this.router.navigate(["/app/assessments/createqa"]);
    }

    toCreateCigarAssessment() {
        this.shareData.reset();
        this.shareData.breadcrumb = 'ca-create';
        this.shareData.qatabfrom = 'ca-create';
        this.router.navigate(["/app/assessments/createca"]);
    }

    toCreateAnnualTrueUp() {
        this.shareData.reset();
        this.shareData.breadcrumb = 'at-create';
        this.shareData.qatabfrom = 'at-create';
        this.router.navigate(["/app/assessments/createat"]);
    }

    cleanupSearchFilters() {
        // Remove Annual filters
        this.modPrefix = "AT";
        this.storage.clear(this.modPrefix + 'CompanyFilter');
        this.storage.clear(this.modPrefix + 'PermitFilter');
        this.storage.clear(this.modPrefix + 'MonthFilter');
        this.storage.clear(this.modPrefix + 'ReconStatusFilter');
        // Remove Cigar filters
        this.modPrefix = "CA";
        this.storage.clear(this.modPrefix + 'CompanyFilter');
        this.storage.clear(this.modPrefix + 'PermitFilter');
        this.storage.clear(this.modPrefix + 'MonthFilter');
        this.storage.clear(this.modPrefix + 'ReconStatusFilter');
        // Remove Quarterly filters
        this.modPrefix = "QA";
        this.storage.clear(this.modPrefix + 'CompanyFilter');
        this.storage.clear(this.modPrefix + 'PermitFilter');
        this.storage.clear(this.modPrefix + 'MonthFilter');
        this.storage.clear(this.modPrefix + 'ReconStatusFilter');
        // Remove Compare filters
        this.modPrefix = "ATC";
        this.storage.clear(this.modPrefix + 'CompanyFilter');
        this.storage.clear(this.modPrefix + 'EinFilter');
        this.storage.clear(this.modPrefix + 'TobaccoClassFilter');
        this.storage.clear(this.modPrefix + 'QuarterFilter');
        this.storage.clear(this.modPrefix + 'DeltaTaxFilter');
        this.storage.clear(this.modPrefix + 'PermitTypeFilter');
        this.storage.clear(this.modPrefix + 'statusTypeFilter');

    }
}

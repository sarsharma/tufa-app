import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AdminService } from '../admin/services/admin.service';
import { AuthService } from '../login/services/auth.service';
import { CustomNumberPipe } from '../shared/util/custom.number.pipe';
import { SharedModule } from "./../shared/shared.module";
// Annual
import { AnnualTrueUpCreationPage } from "./annual/at-create.component";
import { AnnualTrueUpHomePage } from "./annual/at-home.component";
import { AnnualTrueUpResultsComponent } from "./annual/at-results.component";
//flag panel
import { FDA3852FlagPanel } from "./annual/compare/at-acceptanceFlag.component";
import { AnnualTrueUpAllDeltasPopUpAssociationComponent } from './annual/compare/at-compare-alldeltas-action-popup-association.component';
import { AnnualTrueUpAllDeltasPopUpComponent } from "./annual/compare/at-compare-alldeltas-action-popup.component";
import { AnnualTrueUpCompareComponent } from "./annual/compare/at-compare.component";
import { CmpDetailCommentsGridComponent } from './annual/compare/comments/cmpdetail.comments.grid.comment';
import { CmpDetailEditDocumentCommentsPopup } from './annual/compare/comments/cmpdetail-edit-document-comments.component';
import { AnnualTrueUpCompareCompanyPanel } from "./annual/compare/details/at-company-panel.component";
import { AnnualTrueUpCompareCigarDetailsPage } from "./annual/compare/details/at-compare-cigardetails.component";
import { AnnualTrueUpCompareNonCigarDetailsPage } from "./annual/compare/details/at-compare-noncigardetails.component";
import { AnnualTrueUpCompareDetailsHeaderComponent } from "./annual/compare/details/at-comparedetailsheader.component";
import { ReallocatePopup } from "./annual/compare/details/at-comparedetailsreallocate.component";
import { AnnualTrueUpCompareDetailsSummaryComponent } from "./annual/compare/details/at-comparedetailssummary.component";
import { AtOneSidedActionsComponent } from './annual/compare/details/at-one-sided-actions/at-one-sided-actions.component';
import { CompareCBPCommentsPanel } from "./annual/compare/details/importer/at-comparecbpcomments.component";
import { AnnualTrueUpCBPCompareDetailsAmendComponent } from "./annual/compare/details/importer/at-comparecbpdetailsamend.component";
import { AnnualTrueUpCompareImporterDetailsComponent } from "./annual/compare/details/importer/at-compareimpdetails.component";
import { AcceptanceFlagMini } from './annual/compare/details/importer/single-file-compare/at-acceptance-flag-mini.component';
import { AtCompareDetailsAgGridComponent } from './annual/compare/details/importer/single-file-compare/at-compare-details-ag-grid/at-compare-details-ag-grid.component';
import { CompareDetailsAmend } from './annual/compare/details/importer/single-file-compare/at-compare-details-amend.component';
import { CompareDetailsLegend } from './annual/compare/details/importer/single-file-compare/at-compare-details-legend.component';
import { CompareDetailsQuarterPanel } from './annual/compare/details/importer/single-file-compare/at-compare-details-quarter-panel.component';
import { AtImptMatchedCompareComponent } from './annual/compare/details/importer/single-file-compare/at-impt-matched-compare/at-impt-matched-compare.component';
import { AtImptOneSidedCompareComponent } from './annual/compare/details/importer/single-file-compare/at-impt-one-sided-compare/at-impt-one-sided-compare.component';
import { CompareCommentsPanel } from "./annual/compare/details/manufacturer/at-comparecomments.component";
import { AnnualTrueUpCompareDetailsAmendComponent } from "./annual/compare/details/manufacturer/at-comparedetailsamend.component";
import { AnnualTrueUpCompareManufacturerDetailsComponent } from "./annual/compare/details/manufacturer/at-comparemandetails.component";
import { AtCompareManuDetailsAmendComponent } from './annual/compare/details/manufacturer/single-file-compare/at-compare-manu-details-amend/at-compare-manu-details-amend.component';
import { AtCompareManuDetailsQuarterPanelComponent } from './annual/compare/details/manufacturer/single-file-compare/at-compare-manu-details-quarter-panel.component';
import { AtManuCigarCompareComponent } from './annual/compare/details/manufacturer/single-file-compare/at-manu-cigar-compare/at-manu-cigar-compare.component';
import { AtManuMatchedCompareComponent } from './annual/compare/details/manufacturer/single-file-compare/at-manu-matched-compare/at-manu-matched-compare.component';
import { AtManuOneSidedCompareComponent } from './annual/compare/details/manufacturer/single-file-compare/at-manu-one-sided-compare/at-manu-one-sided-compare.component';
import { AtIngestTabPaneComponent } from './annual/ingest/at-ingest-tab-pane.component';
import { AnnualTrueUpIngestErrorPopup } from "./annual/ingest/file/at-ingest-error.popup.component";
import { AnnualTrueUpIngestFileComponent } from "./annual/ingest/file/at-ingestfile.component";
import { AnnualTrueUpProgressBarDynamicComponent } from "./annual/ingest/file/at-progressbar-modal.component";
//Delete ingested file
import { DeleteIngest } from './annual/ingest/file/delete-ingest.component';
import { AnnualTrueUpCompanyAssociation } from "./annual/map/association/company/at-companyassociation";
import { AnnualTrueUpHTSMissingAssociation } from "./annual/map/association/hts-missing/at-htsmissassociation.component";
import { AnnualTrueUpHTSAssociation } from "./annual/map/association/hts/at-htsassociation.component";
import { AnnualTrueUpMapTabPane } from "./annual/map/at-map.component";
import { AnnualTrueUpCreateCompany } from "./annual/map/new-reporters/companies/at-create-company-popup.component";
import { AnnualTrueUpInclCmpyMktSh } from "./annual/map/new-reporters/companies/at-inclcmpy-mktsh.component";
import { AnnualTrueUpPermitInclusion } from './annual/map/new-reporters/permits/at-permitinclusion.component';
//Create permit
import { CreatePermit } from './annual/map/new-reporters/permits/create-permit.component';
import { AnnualTrueUpMarketShareComponent } from "./annual/marketshare/at-marketshare.component";
import { AnnualTrueUpReconComponent } from "./annual/reconcile/at-recon.component";
import { AcceptCBPDataService } from './annual/services/at-acceptIngestedCBP.service';
import { AnnualTrueUpComparisonEventService } from './annual/services/at-compare.event.service';
import { AnnualTrueUpComparisonService } from "./annual/services/at-compare.service";
import { AnnualTrueUpCreationService } from "./annual/services/at-create.service";
import { AnnualTrueUpEventService } from "./annual/services/at-event.service";
import { AnnualTrueUpMarketShareService } from "./annual/services/at-marketshare.service";
import { AnnualTrueUpService } from "./annual/services/at.service";
import { DeltaPipe } from './annual/utils/at-delta.pipe';
import { AnnualTrueUpDataFilterPipe } from "./annual/utils/at-filter.pipe";
import { CompareIconDirective } from './annual/utils/at-icon.directive';
import { AnnualTrueUpReconDataFilterPipe } from "./annual/utils/at-reconfilter.pipe";
// import 'twitter-bootstrap-wizard/jquery.bootstrap.wizard.js';
// Cigars
import { CigarsAssessmentsCreationPage } from "./cigars/components/ca-create.component";
import { CigarsAssessmentsHomePage } from "./cigars/components/ca-home.component";
import { CigarsAssessmentsMarketShareComponent } from "./cigars/components/ca-marketshare.component";
import { CigarsAssessmentsReconReportComponent } from "./cigars/components/ca-reconreport.component";
import { CigarsAssessmentsReportStatusComponent } from "./cigars/components/ca-reportstatus.component";
import { CigarsAssessmentsResultsComponent } from "./cigars/components/ca-results.component";
import { CigarsAssessmentsCreationService } from "./cigars/services/ca-create.service";
import { CigarsAssessmentsMarketShareService } from './cigars/services/ca-marketshare.service';
import { CigarsAssessmentsReconReportService } from "./cigars/services/ca-reconreport.service";
import { CigarsAssessmentsService } from "./cigars/services/ca.service";
import { CigarsAssessmentsDataFilterPipe } from "./cigars/utils/ca-filter.pipe";
import { CigarMarketShareTypePipe } from './cigars/utils/ca-mktsharetype.pipe';
import { CigarsAssessmentsReconDataFilterPipe } from "./cigars/utils/ca-reconfilter.pipe";
import { CigarStatusPipe } from "./cigars/utils/ca-status.pipe";
import { VersionZeroPipe } from './cigars/utils/version-zero.pipe';
// Quarterly
import { QuarterlyAssessmentsCreationPage } from "./quarterly/components/qa-create.component";
import { QuarterlyAssessmentsHomePage } from "./quarterly/components/qa-home.component";
import { QuarterlyAssessmentsMarketShareComponent } from "./quarterly/components/qa-marketshare.component";
import { QuarterlyAssessmentsReconReportComponent } from "./quarterly/components/qa-reconreport.component";
import { QuarterlyAssessmentsReportStatusComponent } from "./quarterly/components/qa-reportstatus.component";
import { QuarterlyAssessmentsResultsComponent } from "./quarterly/components/qa-results.component";
import { QuarterlyAssessmentsCreationService } from "./quarterly/services/qa-create.service";
import { QuarterlyAssessmentsMarketShareService } from "./quarterly/services/qa-marketshare.service";
import { QuarterlyAssessmentsReconReportService } from "./quarterly/services/qa-reconreport.service";
import { QuarterlyAssessmentsService } from "./quarterly/services/qa.service";
import { QuarterlyAssessmentDataFilterPipe } from "./quarterly/utils/qa-filter.pipe";
import { QuarterlyAssessmentsReconDataFilterPipe } from "./quarterly/utils/qa-reconfilter.pipe";
import { AddDocumentPopup } from "./recon-add-document.component";
// Recon
import { ReconciliationLandingPage } from "./recon-landing.component";
import { AffectedAssessmentDataFilterPipe } from './utils/aa-filter.pipe';
import { TobaccoButtonBarComponent } from './annual/compare/details/tobacco-button-bar/tobacco-button-bar.component';
import { AtCompareDetailsTufaAgGridComponent } from './annual/compare/details/importer/single-file-compare/at-compare-details-tufa-ag-grid/at-compare-details-tufa-ag-grid.component';
import { AtComparePrevTrueupInfoTufaAgGridComponent } from "./annual/compare/details/at-compare-prev-trueup-info/at-compare-prev-trueup-info-tufa-ag-grid.component";















export const routes = [
    { path: '', component: ReconciliationLandingPage, pathMatch: 'full' },
    { path: 'createqa', component: QuarterlyAssessmentsCreationPage },
    { path: 'createat', component: AnnualTrueUpCreationPage },
    { path: 'createca', component: CigarsAssessmentsCreationPage },
    { path: 'quarterly', component: QuarterlyAssessmentsHomePage },
    { path: 'quarterly/:id', redirectTo: 'quarterly', pathMatch: 'full' },
    { path: 'annual', component: AnnualTrueUpHomePage },
    { path: 'annual/:id', redirectTo: 'annual', pathMatch: 'full' },
    { path: 'cigars', component: CigarsAssessmentsHomePage },
    { path: 'cigars/:id', redirectTo: 'cigars', pathMatch: 'full' },
    { path: 'comparecigar', component: AnnualTrueUpCompareCigarDetailsPage },
    { path: 'comparecigar/:id', redirectTo: 'comparecigar', pathMatch: 'full' },
    { path: 'comparenoncigar', component: AnnualTrueUpCompareNonCigarDetailsPage },
    { path: 'comparenoncigar/:id', redirectTo: 'comparenoncigar', pathMatch: 'full' },
];

@NgModule({
    imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
    declarations: [ReconciliationLandingPage,
        QuarterlyAssessmentsCreationPage, QuarterlyAssessmentsHomePage,
        QuarterlyAssessmentsResultsComponent, QuarterlyAssessmentsReportStatusComponent,
        QuarterlyAssessmentsReconReportComponent, QuarterlyAssessmentsMarketShareComponent,
        QuarterlyAssessmentDataFilterPipe, QuarterlyAssessmentsReconDataFilterPipe,
        AnnualTrueUpCreationPage, AnnualTrueUpHomePage, AnnualTrueUpCompareCigarDetailsPage,
        AnnualTrueUpCompareNonCigarDetailsPage,
        AnnualTrueUpCompareManufacturerDetailsComponent,
        AnnualTrueUpCompareImporterDetailsComponent,
        AnnualTrueUpResultsComponent, AnnualTrueUpProgressBarDynamicComponent,
        AnnualTrueUpMapTabPane, AnnualTrueUpIngestFileComponent,
        AnnualTrueUpIngestErrorPopup, AnnualTrueUpCompanyAssociation, AnnualTrueUpPermitInclusion,
        AnnualTrueUpHTSAssociation, AnnualTrueUpHTSMissingAssociation,
        AnnualTrueUpDataFilterPipe, AnnualTrueUpCompareComponent, AnnualTrueUpReconComponent, AnnualTrueUpReconDataFilterPipe,
        AnnualTrueUpCompareDetailsSummaryComponent, AnnualTrueUpMarketShareComponent, DeleteIngest, CreatePermit,
        AnnualTrueUpCompareDetailsHeaderComponent, AnnualTrueUpCompareDetailsAmendComponent, AnnualTrueUpCBPCompareDetailsAmendComponent,
        AnnualTrueUpInclCmpyMktSh, AnnualTrueUpCreateCompany, AnnualTrueUpAllDeltasPopUpComponent, AnnualTrueUpAllDeltasPopUpAssociationComponent, CigarsAssessmentsCreationPage, CigarsAssessmentsHomePage,
        CigarsAssessmentsResultsComponent, CigarsAssessmentsReportStatusComponent,
        CigarsAssessmentsReconReportComponent, CigarsAssessmentsMarketShareComponent,
        CigarsAssessmentsDataFilterPipe, CigarStatusPipe, AddDocumentPopup, ReallocatePopup, CigarMarketShareTypePipe,
        CompareCBPCommentsPanel, CompareCommentsPanel, CigarsAssessmentsReconDataFilterPipe, FDA3852FlagPanel, CmpDetailCommentsGridComponent,CmpDetailEditDocumentCommentsPopup, AnnualTrueUpCompareCompanyPanel, AtIngestTabPaneComponent,
        AffectedAssessmentDataFilterPipe, VersionZeroPipe,
        CompareDetailsLegend, CompareDetailsQuarterPanel, AcceptanceFlagMini, CompareDetailsAmend, CompareIconDirective, DeltaPipe, AtCompareDetailsAgGridComponent, AtImptMatchedCompareComponent,
        AtImptOneSidedCompareComponent, AtOneSidedActionsComponent, AtCompareManuDetailsQuarterPanelComponent, AtManuMatchedCompareComponent, AtManuOneSidedCompareComponent, AtCompareManuDetailsAmendComponent,
        AtManuCigarCompareComponent,
        TobaccoButtonBarComponent,
        AtCompareDetailsTufaAgGridComponent,
        AtComparePrevTrueupInfoTufaAgGridComponent],
    providers: [AuthService, AdminService,
        QuarterlyAssessmentsReconReportService, QuarterlyAssessmentsMarketShareService, QuarterlyAssessmentsCreationService,
        QuarterlyAssessmentsService, AnnualTrueUpCreationService, AnnualTrueUpEventService, AnnualTrueUpService,
        AnnualTrueUpComparisonService, AnnualTrueUpProgressBarDynamicComponent,
        CigarsAssessmentsService, CigarsAssessmentsCreationService, CigarsAssessmentsReconReportService,
        CigarsAssessmentsMarketShareService, AnnualTrueUpComparisonEventService,
        AnnualTrueUpMarketShareService, AcceptCBPDataService, CustomNumberPipe, DeltaPipe],
    entryComponents: [AtImptMatchedCompareComponent, AtImptOneSidedCompareComponent]
})

export class ReconciliationModule {
    static routes = routes;
}

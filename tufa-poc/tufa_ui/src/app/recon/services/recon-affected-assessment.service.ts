import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {} from './../../../assets/response.json'
import { environment } from "./../../../environments/environment";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: 'root'
})
export class ReconAffectedAssessmentService {

  private affectedDateURL = environment.apiurl + '/api/v1/companies/getaffectedassmtdata';
  constructor(private http: HttpClient) { }

    public getAffectedAssessmentsJSON() {
        return this.http.get<any[]>(this.affectedDateURL, httpOptions);
    }
}

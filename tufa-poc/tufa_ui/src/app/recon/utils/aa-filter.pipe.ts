
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { AffectedAssessmentsReconFilter, AffectedAssessmentsReconFilterData } from './../../recon/model/aa-reconfilter.model';


/**
 * 
 * 
 * @export
 * @class PermitReportsDataFilterPipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: "aapipe"
})
export class AffectedAssessmentDataFilterPipe implements PipeTransform {

    /**
     * 
     * 
     * @param {any[]} data
     * @param {AnnualFilter} filter
     * @returns {*}
     * 
     * @memberOf PermitReportsDataFilterPipe
     */
    transform(data: any[], filter: AffectedAssessmentsReconFilterData): any {
        let results = data;


        if (data) {
            if (filter) {
                // filter by companyname
                if (typeof filter.assessmentFYFilter !== 'undefined' && filter.assessmentFYFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1: string = item.assessmentFY
                        let val2: string = filter.assessmentFYFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                }

                if (typeof filter.assessmentQTRFilter !== 'undefined' && filter.assessmentQTRFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1: string = item.assessmentQTR
                        let val2: string = filter.assessmentQTRFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                }

                
                // filter by assessmentType
                if (typeof filter.assessmentTypeFilter !== 'undefined' && filter.assessmentTypeFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1: string = item.assessmentType;
                        let val2: string = filter.assessmentTypeFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                }
                // filter by companyName
                if (typeof filter.companyNameFilter !== 'undefined' && filter.companyNameFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1: string = item.companyName;
                        let val2: string = filter.companyNameFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                }
                // filter by ein
                if (typeof filter.einFilter !== 'undefined' && filter.einFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1: string = item.ein;
                        let val2: string = filter.einFilter;
                        val2 = val2.replace(/-/g, '');
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                }
                // filter by reported Tobacco
                if (typeof filter.permitNumberFilter !== 'undefined' && filter.permitNumberFilter.length > 0) {
                    results = _.filter(results, function (item) {
                        let val1: string = item.permitNumber;
                        let val2: string = filter.permitNumberFilter;
                        val2 = val2.replace(/-/g, '');
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                }
            }
        }
        return results;
    }
    // tslint:disable-next-line:eofline
}
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RouterModule } from "@angular/router";

export const routes = [];

@NgModule({
	imports: [CommonModule, RouterModule.forChild(routes)],
	declarations: []
})
export class ReportModule {
	static routes = routes;
}


import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";


@Component({
    selector: 'floating-cell',
    template: `
    
    <div *ngIf="params.data">
        <label class="custom-control custom-checkbox">
            <input #i type="checkbox" [checked]="checked" class="custom-control-input" 
            (change)="onCheckBoxClick(i)">
            <span class="custom-control-indicator"></span>
            <span class="custom-control-label"></span>
        </label>
    </div>
    `
})
export class CheckBoxCellRenderer implements ICellRendererAngularComp {
    
    public params: any;
    public gridApi;
    public gridColumnApi;

    public checked;
    public data;

    agInit(params: any): void {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.params = params;

        if(!params.data) return;

        this.data = params.data;
        this.checked =  params.getValue();
        
    }

    public onCheckBoxClick(checkBox){
        this.checked= checkBox.checked;
        this.params.setValue(this.checked);
        this.params.context.componentParent.onCheckBoxClck(this.data);
    }

    refresh(): boolean {
        return false;
    }

}

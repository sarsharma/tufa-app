import {Component, Input,SimpleChanges } from "@angular/core";

@Component({
    selector: 'circle-icon',
    template: `
    <span><i [ngClass]="exCircleIconColor()"
                               class="fa fa-exclamation-circle fa-w-16  ml-xs"></i></span>
   `
})
export class CircleIconComponent{
    @Input()  colorRenderCondition:string;

    msgType:String ;

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            if (propName === "colorRenderCondition") {
                 this.msgType = changes[propName].currentValue;
            }

        }
    }

    exCircleIconColor(){
        if(this.msgType =='success')
             return 'text-success';
        if(this.msgType == 'warning')
             return 'text-warning';
         if(this.msgType == 'danger')   
             return 'text-danger';
    }

}
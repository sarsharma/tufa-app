import { Component, Input } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";

@Component({
    selector: 'entry-date',
    template: `
    <span>{{value | tufacurrency | minustoparens}}</span>
    `
})
export class CurrencyRenderer implements ICellRendererAngularComp {

    @Input() value;

    agInit(params: any): void {
        if (!params.data) return;
        for (let i in params.fieldParams) {
            this.value = params.data[params.fieldParams[i]];
        }
    }

    refresh(params: any): boolean {
        return false;
    }
}
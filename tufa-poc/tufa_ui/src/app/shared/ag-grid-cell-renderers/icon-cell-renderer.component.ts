
import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";


@Component({
    selector: 'floating-cell',
    template: `
     <div *ngIf="iconType =='circle-icon'">
        {{params.value}} <circle-icon [colorRenderCondition]="colorRenderCondition"></circle-icon>
     </div>
    `
})
export class IconCellRenderer implements ICellRendererAngularComp {
    
    public params: any;
    public gridApi;
    public gridColumnApi;
    public data;

    // icon specific properties
    public colorRenderCondition; 
    public iconType;

    agInit(params: any): void {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.params = params;

        if(!params.data) return ;

        // icon specific properties
        this.iconType = params.iconType;
        this.colorRenderCondition = this.params.colorRenderCondition(this.params);
    }


    refresh(): boolean {
        return false;
    }

}

import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";

@Component({
    selector: 'info-icon',
    template: `
    <div>
    <i [ngClass]="{'fa fa-calendar': date}"></i>
    <i [ngClass]="{'fa fa-exchange': reallocated}"></i>
    <i [ngClass]="{'fa fa-ban': excluded}"></i>
    <i [ngClass]="{'fa fa-exclamation-circle': missing}"></i>
    </div>
    `
})
export class InfoIconRenderer implements ICellRendererAngularComp {

    date: boolean;
    reallocated: boolean;
    excluded: boolean;
    missing: boolean;

    agInit(params: any): void {
        if (!params.data) return;
        for (let i in params.fieldParams) {
            this.date = params.data['secondaryDateFlag'] == 'Y';
            this.reallocated = params.data['reallocatedFlag'] === 'Y';
            this.excluded = params.data['excludedFlag'] === 'Y';
            this.missing = params.data['missingFlag'] === 'Y';
        }
    }

    refresh(params: any): boolean {
        return false;
    }
}
import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { Router } from '@angular/router';

@Component({
    selector: 'floating-cell',
    template: `
    <span class="fw-semi-bold blue"><a href="javascript:void(1)"  (aClick)="navigate();saveRoutingParams()">{{params.getValue()}}</a></span>
    `
})
export class RouterLinkCellRenderer implements ICellRendererAngularComp {

    public params: any;
    public data: any;
    public gridApi;
    public gridColumnApi;

    public queryParams = {};

    constructor( public router: Router){

    }

    agInit(params: any): void {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.params = params;    
        this.data = params.data;
        
        if(!params.data) return ;
        
        for(let i in params.fieldParams){
            this.queryParams[params.queryParams[i]] = params.data[ params.fieldParams[i]];
        }
    }

    public navigate(){
        this.router.navigate([this.params.routerLink,this.queryParams]);
    }

    public saveRoutingParams() {
        this.params.context.componentParent.saveRoutingParams(this.data);
    }

    refresh(): boolean {
        return false;
    }
}

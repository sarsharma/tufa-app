import { OnDestroy, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs';
import { AgColumnDefinition, AgExportDefinition, AgGridState } from '../../../shared/model/ag-grid.types';
import { AgGridStateMgmtService } from '../../../shared/service/ag-grid.state.mgmt.service';
import { ServerSideDataSourceService } from '../../service/ag-grid.datasource.service';
import { RouterGlobalService } from "../../service/router.global.service";


export abstract class AgGridComponent implements OnInit, OnDestroy {

    /**
     *  Dependencies:
     *  AgGridStateMgmtService - deps : grid name, urls
     *  ServerSideDataSource - deps :  grid server side url, gridColumnApi, (gridOptions), httpClient                    
     */

    public data: any;

    public gridApi;
    public gridColumnApi;

    public serverDataSourceUrl: string;

    public rowModelType = "serverSide";
    public maxBlocksInCache = 3;
    public cacheBlockSize = 30;
    public pagination = "true";
    public paginationPageSize = 25;

    public maxConcurrentDatasourceRequests = 1;
    public cacheOverflowSize = 1;


    public multiSortKey = "ctrl";

    public sideBar = {
        toolPanels: [
            {
                id: 'columns',
                labelDefault: 'Columns',
                labelKey: 'columns',
                iconKey: 'columns',
                toolPanel: 'agColumnsToolPanel',
            },
            {
                id: 'filters',
                labelDefault: 'Filters',
                labelKey: 'filters',
                iconKey: 'filter',
                toolPanel: 'agFiltersToolPanel',
            }
        ],
        defaultToolPanel: 'columns'

    };


    public defaultColDef = {
        sortable: true,
        filter: true,
        resizable: true,
        wrapText: true,
        autoHeight: true,
    };

    public overlayLoadingTemplate;

    // is server side pagination enabled 
    public serverSidePaginationEnabled: boolean = false;


    public defaultExportParams: AgExportDefinition = new AgExportDefinition();

    /**
     * Properties for managing grid state locally or at the backend database
     */

    public gridStates = [];
    public selectedGridVersion: number;
    public routerSubscription: Subscription;
    public saveStateUrls: string[];
    public gridName: string;
    public urlsSaveState: string[];
    public storeGridState: boolean;

    public subscriptions: Subscription[] = [];

    constructor(public _agGridStateMgmtService: AgGridStateMgmtService,
        public _serverSideDataSource: ServerSideDataSourceService,
        public routerService: RouterGlobalService,
        public storage: LocalStorageService
    ) {

        let self = this;
        this._agGridStateMgmtService.preferenceSaved$.subscribe(value => {
            if (value)
                self.updateGridStates();
        });

        this.overlayLoadingTemplate =
            '<span class="ag-overlay-loading-center">Loading ...</span>';

        this.urlsSaveState = [];
        this.storeGridState = false;

    }

    ngOnInit() {
        /**  
         * Subscription to router service in event of navigating away and back 
           to the grid and restoring states 
         **/
        if (this.storeGridState) {
            this.routerSubscription = this.routerService.navigateStartSource$.subscribe(
                event => {
                    if (this.isUrlPresent(this.urlsSaveState, event.url)) {
                        //set filter and sort state before navigating away from the page
                        //TODO: retrieving grid status from the backend database 
                        this.saveGridStateLocal();
                    }
                    else {
                        //reset state
                        this.resetGridState();
                    }
                }
            );

            this.subscriptions.push(this.routerSubscription);
        }

    }

    ngAfterViewInit() {

    }


    ngOnDestroy() {
        for (let i in this.subscriptions) {
            this.subscriptions[i].unsubscribe();
        }
    }

    private isUrlPresent(urls: string[], url: string) {

        for (let i in urls) {
            if (url.includes(urls[i]))
                return true;
                                                                                                      }
        return false;
    }
    
    restoreOnPanelOpen(){
        this.updateGridStatus();
        //this.resetGridState();
       
        
    }
    updateGridStatus(){
        let self = this;
        this._agGridStateMgmtService.preferenceSaved$.subscribe(value => {
            if (value)
                self.updateGridStates();
        });
    }
    /**
     * Initializing the grid 
     */

    onGridReady(params, serverDataSourceUrl, gridNm) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.gridApi.closeToolPanel();
        this._serverSideDataSource.setServerSideUrl(serverDataSourceUrl);
        //register Datasource
        if (this.serverSidePaginationEnabled) {
            this._serverSideDataSource.setAgGridApis(params);
            params.api.setServerSideDatasource(this._serverSideDataSource);
        }
        this.gridName = gridNm;
        this.gridApi.resetRowHeights();
        //this.restoreGridSavedState();
    }

    saveState(versionName) {
        let state: AgGridState = new AgGridState();

        state.colState = JSON.stringify(this.gridColumnApi.getColumnState());
        state.groupState = JSON.stringify(this.gridColumnApi.getColumnGroupState());
        state.sortState = JSON.stringify(this.gridApi.getSortModel());
        state.filterState = JSON.stringify(this.gridApi.getFilterModel());
        state.versionName = versionName;
        this._agGridStateMgmtService.saveGridState(state, this.gridName);
    }

    updateGridStates() {
        this._agGridStateMgmtService.getGridStates(this.gridName).subscribe(
            data => {
                this.gridStates = [...data];
            }
        );
    }

    restoreGridState() {

        if (this.selectedGridVersion == -1) return;

        this._agGridStateMgmtService.getGridStates(this.gridName).subscribe(
            data => {
                this.gridStates = [...data];
                let states: AgGridState[] = [...data];
                if (this.selectedGridVersion) {
                    let sversion = this.selectedGridVersion;
                    let state = states.find(function (value: AgGridState) {
                        return value.version == sversion;
                    });
                    this.setState(state);
                }
            }
        );
    }

    private setState(state) {
        if (!state) return;
        this.gridColumnApi.setColumnState(JSON.parse(state.colState));
        this.gridColumnApi.setColumnGroupState(JSON.parse(state.groupState));
        this.gridApi.setSortModel(JSON.parse(state.sortState));
        this.gridApi.setFilterModel(JSON.parse(state.filterState));
        this.autoSizeAll();
    }


    // sizing columns helper methods

    onFirstDataRendered(params) {
        params.api.sizeColumnsToFit();
    }

    sizeToFit() {
        this.gridApi.sizeColumnsToFit();
        this.gridApi.resetRowHeights();
    }

    public autoSizeAll() {
        var allColumnIds = [];

        this.gridColumnApi.getAllDisplayedColumnGroups().forEach(function (column) {
            allColumnIds.push(column.colId);
        });;

        this.gridColumnApi.getAllColumns().forEach(function (column) {
            allColumnIds.push(column.colId);
        });
        this.gridColumnApi.autoSizeColumns(allColumnIds);
        this.gridApi.resetRowHeights();
    }

    private resetGridState() {
        this.gridColumnApi.resetColumnState();
        this.gridColumnApi.resetColumnGroupState();
        this.gridApi.setSortModel([]);
        this.gridApi.setFilterModel({});
        //this.autoSizeAll();
    }
    
    // save/restore grid states from local storage
    //after grid is ready store previous state if any  
    public restoreGridSavedState() {
        if (this.storeGridState) {
            if (this.isUrlPresent(this.urlsSaveState, this.routerService.getPreviousUrl()) || this.isUrlPresent(this.urlsSaveState, this.routerService.getCurrentUrl())) {
                this.restoreGridStateLocal();
            }
        }
    }

    public saveGridStateLocal() {
        let state: AgGridState = new AgGridState();

        state.colState = this.gridColumnApi.getColumnState();
        state.groupState = this.gridColumnApi.getColumnGroupState();
        state.sortState = this.gridApi.getSortModel();
        state.filterState = this.gridApi.getFilterModel();

        let key = "grid-state-" + this.gridName;
        let key1 = "grid-state-" + this.gridName + "-page";

        this.storage.store(key, state);
        this.storage.store(key1, this.gridApi.paginationGetCurrentPage());
    }
    public restoreGridStateLocalPanelLoaded() {

        let key = "grid-state-" + this.gridName;
        let key1 = "grid-state-" + this.gridName + "-page";
        let state: AgGridState = this.storage.retrieve(key);
        if (state && this.gridColumnApi) {
            this.gridColumnApi.setColumnState(state.colState);
            this.gridColumnApi.setColumnGroupState(state.groupState);
            this.gridApi.setSortModel([]);
            this.gridApi.setFilterModel({});
            //this.gridApi.setSortModel(state.sortState);
            //this.gridApi.setFilterModel(state.filterState);
            //this.gridApi.paginationGoToPage(this.storage.retrieve(key1));
           
            this.storage.clear(key);
            this.storage.clear(key1);
            // this.autoSizeAll();
        }
    }
    public restoreGridStateLocal() {

        let key = "grid-state-" + this.gridName;
        let key1 = "grid-state-" + this.gridName + "-page";
        let state: AgGridState = this.storage.retrieve(key);
        if (state && this.gridColumnApi) {
            this.gridColumnApi.setColumnState(state.colState);
            this.gridColumnApi.setColumnGroupState(state.groupState);
            this.gridApi.setSortModel(state.sortState);
            this.gridApi.setFilterModel(state.filterState);
            this.gridApi.paginationGoToPage(this.storage.retrieve(key1));
            this.storage.clear(key);
            this.storage.clear(key1);
            // this.autoSizeAll();
        }
    }

    // Cell value Formatters
    //Permit Formatter
    //TODO: cell renderer to be used for formatting values using pipes
    permitFormatter(params) {
        let value = params.value;
        if (value != null && !value.includes(';')) {
            let formattedValue = value.substring(0, 2) + '-' + value.substring(2, 4) + '-' +
                value.substring(4);
            return formattedValue.toUpperCase();
        }
    }

    public customCurrencyComparator(c1, c2) {

        if (c1 == null && c2 == null)
            return 0;
        if (c1 == null)
            return -1;
        if (c2 == null)
            return 1;

        let n1 = Number(c1.replace(/[^0-9.-]+/g, ""));
        let n2 = Number(c2.replace(/[^0-9.-]+/g, ""));

        return n1 - n2;
    }

    public getColumnsForExport(columnDefs: AgColumnDefinition[]): string[] {
        let exportColumns: string[] = [];

        if(columnDefs) {
            columnDefs.forEach(column => {
                if(column.includeInExport) {
                    exportColumns.push(column.colId);
                }
            });
        }

        return exportColumns;
    }

}
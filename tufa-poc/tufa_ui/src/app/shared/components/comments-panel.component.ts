import { Component, Input, OnInit, ViewEncapsulation, SimpleChanges, Output, EventEmitter, OnDestroy } from '@angular/core';
import { CommentsService } from '../service/comments.service';
import { Subscription } from 'rxjs';
@Component({
    selector: '[comments-panel]',
    templateUrl: './comments-panel.template.html',
    encapsulation: ViewEncapsulation.None
})

export class CommentsPanel implements OnInit, OnDestroy {

    @Input() objectId;
    @Input() comment;
    @Input() objectType;
    @Input() contextInfo;

    @Output() onCommentEdit = new EventEmitter<boolean>();

    showCommentsErrorFlag: boolean;
    alertsComments: Array<Object>;
    busy: Subscription;
    context: any;

    constructor(private commentsService: CommentsService) {
        this.alertsComments = [
            {
                type: 'warning',
                msg: '<span class="fw-semi-bold">Warning:</span> Error: Saving Comments'
            }
        ];
        this.showCommentsErrorFlag = false;
     }

    ngOnChanges(changes: SimpleChanges) {
        // tslint:disable-next-line:forin
        for (let propName in changes) {
            if (propName === "comment") {
                let chng = changes[propName];
                if (chng.currentValue) {
                    this.comment = chng.currentValue;
                }
            }
            if (propName === "contextInfo") {
                let chng = changes[propName];
                if (chng.currentValue) {
                    this.context = chng.currentValue;
                }
            }
        }
    }

    ngOnInit() {
    }

    ngOnDestroy(){
        if(this.busy){
            this.busy.unsubscribe();
        }
    }

    updateComment() {
        if (this.comment != null) {
            this.busy = this.commentsService.updateComment(this.objectId, this.comment, this.objectType)
            .subscribe( data => {
                if (data) {
                    this.comment = data;
                    this.onCommentEdit.emit(true);
                }
            } , error => {
                this.showCommentsErrorFlag = true;
                this.alertsComments = [];
                this.alertsComments.push({type: 'warning', msg: error});
            });
        }
    }

}

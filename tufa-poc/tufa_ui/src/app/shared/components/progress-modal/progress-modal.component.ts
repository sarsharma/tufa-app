import { Component, OnInit, Input, ElementRef, ViewChild, SimpleChanges } from '@angular/core';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: '[progress-modal]',
  templateUrl: './progress-modal.component.html',
  styleUrls: ['./progress-modal.scss']
})
export class ProgressModalComponent implements OnInit {

  @Input() heading: string = "Accepting Ingested Values";
  @Input() ingestionMessage: string = "This process may take a few minutes";
  public max: number = 200;
  public showWarning: boolean;
  public dynamic: number = 0;
  public type: string;
  public constIncrement: number = 10;
  complete: boolean = false;

  @Input() maxout: boolean;

  private dynamicSubscription: Subscription;

  constructor() {
    this.max = 200;
    this.dynamic = 0;
    this.constIncrement = 10;
  }

  ngOnInit() {
    // this.startTimmer();
  }

  ngOnDestroy() {
    if (this.dynamicSubscription) {
      this.dynamicSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "maxout") {
        let chg = changes[propName];
        if (!chg.firstChange && !chg.currentValue) {
          this.startTimmer();
        } else if (chg.currentValue && this.dynamicSubscription.closed) {
          //If max out was set to true but the timmer already ended
          this.hideModal();
        }
      }
    }
  }

  private startTimmer() {
    this.max = 200;
    this.dynamic = 0;
    this.constIncrement = 10;

    let pg_timer = timer(1000, 1000);
    this.dynamicSubscription = pg_timer.subscribe(t => this.value(t));
  }

  public value(value: number) {

    if (this.maxout){
      this.dynamic = this.max;
      this.hideModal();
    }

    if (this.dynamic >= this.max)
      this.dynamicSubscription.unsubscribe();

    if (this.dynamic < this.max)
      this.dynamic = value + this.constIncrement;

  }

  @ViewChild('closeModal') private closeModal: ElementRef;
  public hideModal() {
    this.closeModal.nativeElement.click();
  }

}

import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: 'toggle-button-tufa',
    templateUrl: './toggle-button-tufa.template.html',
    styleUrls: ['./toggle-button-tufa.style.scss']
})

export class ToggleButton implements OnInit {
    @Input() flag: boolean = false;
    @Input() isDisabled: boolean = false;
    @Input() lables: string[] = [''];
    @Input() id;
    @Output() flagChange = new EventEmitter<boolean>();
    activeLabel: string;
    
    ngOnInit(): void {
        this.evaluateLable();
    }

    invertFlag () {
        this.flag = !this.flag;
        this.evaluateLable();
        this.flagChange.emit(this.flag);
    }

    private evaluateLable() {
        if (this.lables.length > 1 && !this.flag) {
            this.activeLabel = this.lables[1];
        } else {
            this.activeLabel = this.lables[0];
        }
    }
}
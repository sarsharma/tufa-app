import { CompanyStatusPipe } from './company-status.pipe';

describe('Pipe: CompanyStatusPipe', () => {
  let pipe = new CompanyStatusPipe();

  // specs
  it('test company status pipe', () => {
    expect(pipe.transform('ACTV')).toEqual('Active');
    expect(pipe.transform('Active')).toEqual('Active');
    expect(pipe.transform('')).toEqual('Inactive');
  });
});

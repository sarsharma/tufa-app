import { CurrencyPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

/*
 * Changes input value to currency format
*/

// const _NUMBER_FORMAT_REGEXP = /^(\d+)?\.((\d+)(-(\d+))?)?$/;
const _NUMBER_FMT_REGEX = /^(?:\d*\.\d{1,2}|\d+)$/;
const _BAD_VALUE = /^[.,]{2,}$/;

@Pipe({name: 'tufacurrency'})
export class TufaCurrencyPipe implements PipeTransform {

    constructor(private currencyPipe: CurrencyPipe) {}

    transform(value: any, args: any[]): string {

        if (!value && value !== 0) {return ' '; }

        const val = String(value).trim();
        if (value === '.' || value === ',' || _BAD_VALUE.test(val)) {
            return value;
        }
        if (!['NA','N/A'].includes(value) ||  _NUMBER_FMT_REGEX.test(val)) {
            value = parseFloat(value);
            return this.currencyPipe.transform(value, 'USD', 'symbol', '1.2-2');
        } else {
            return value;
        }
    }
}

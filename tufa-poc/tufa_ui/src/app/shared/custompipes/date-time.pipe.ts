import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateTime'
})
export class DateTimePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(!value) return "";
      return this.getFormattedDateTime(new Date( value ));
  }
  
  private getFormattedDateTime(date): string {
    return date.toLocaleString('en-US', { hour12: false });
  } 
}

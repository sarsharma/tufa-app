import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "datePipe" })
export class DatePipe implements PipeTransform {

  constructor() {
  }

  transform(value: string): string {   
    if (!value) return "";

    if (typeof value === 'string') 
      return this.getFormattedDate(new Date(parseInt(value)));
    else 
      return this.getFormattedDate(new Date(value));  
  }

  private getFormattedDate(date): string {
    let year = date.getFullYear();
    let month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    let day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
  }
}

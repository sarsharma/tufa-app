import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'einPipe'})
export class EINPipe implements PipeTransform {
  transform(value: string): string {
    let formattedValue = "";
    if (value){
      if (value.indexOf("SSN") !== -1){
        formattedValue = value;
      }else{
        formattedValue = value.substring(0, 2) + '-' + value.substring(2) ;
       }
    }
    return formattedValue;
  }
}

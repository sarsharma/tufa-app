import { Pipe, PipeTransform } from '@angular/core';

/*
 * Adds a .xlsl.
*/

@Pipe({name: 'excelpipe', pure: false})
export class ExcelPipe implements PipeTransform {
    transform(input: string): string {
        if(!input) return input;
         return   input.endsWith(".xlsx") ? input:(input+".xlsx");
        
    }
}

import { Pipe, PipeTransform } from '@angular/core';

/*
 * Changes a minus sign preceding a value to parentheses.
*/

@Pipe({name: 'minustoparens', pure: false})
export class MinusToParensPipe implements PipeTransform {
    transform(input: string): string {
        if(!input) return input;
        return input.charAt(0) === '-' ?'(' + input.substring(1, input.length) + ')' : input;
    }
}

import { Pipe, PipeTransform } from '@angular/core';

/*
 * Changes a minus sign preceding a value to parentheses.
*/

@Pipe({name: 'na', pure: false})
export class NAPipe implements PipeTransform {
    transform(input: number, ifNullValue: string = 'NA'): string | number {
        if (input == null || isNaN(input)) {
            return ifNullValue;
        }
        return input;
    }
}

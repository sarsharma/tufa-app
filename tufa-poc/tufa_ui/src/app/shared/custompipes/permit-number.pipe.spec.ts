import { PermitNumberPipe } from './permit-number.pipe';

describe('Pipe: PermitNumberPipe', () => {
  let pipe = new PermitNumberPipe();

  // specs
  it('test permit number pipe', () => {
    expect(pipe.transform('ABCD12345')).toEqual('AB-CD-12345');
  });

  // specs
  it('should work with empty string', () => {
    expect(pipe.transform('')).toEqual('--');
  });

  it('should throw with invalid values', () => {
    // must use arrow function for expect to capture exception
    expect(() => pipe.transform(undefined)).toThrow();
    // expect(() => pipe.transform()).toThrow();
    // expect(() => pipe.transform()).toThrowError('Requires a String as input');
  });

});

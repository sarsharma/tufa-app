import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';
/*
 * Format the permit number as AA-BB-1XXXX
 * Takes permit number and formats it.
 * Usage:
 *   value | permitNumber
 * Example:
 *   {{ AABB12345 |  permitNumber}}
 *   formats to: AA-BB-12345
*/
@Pipe({ name: 'permitNumberPipe' })
export class PermitNumberPipe implements PipeTransform {
  transform(value: string): string {
    if (value != null && !value.includes(';')) {
      let formattedValue = value.substring(0, 2) + '-' + value.substring(2, 4) + '-' +
        value.substring(4);
      return formattedValue.toUpperCase();
    }
    else if (value != null && value.includes(';')) {

      let array = [];
      let splitValue = value.split(';');
      for (let i = 0; i < splitValue.length; i++) {
      
        array.push(splitValue[i].trim().substring(0, 2) + '&#8209;' + splitValue[i].trim().substring(2, 4) + '&#8209;' +
          splitValue[i].trim().substring(4));

      }
      array = _.uniq(array);
      let retvalue = array.join(',  ');

      return retvalue;

    }
    else {
      return value;
    }

  }
}
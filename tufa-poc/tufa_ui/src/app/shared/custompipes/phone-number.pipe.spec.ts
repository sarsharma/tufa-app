import { PhoneNumberPipe } from './phone-number.pipe';

describe('Pipe: PhoneNumberPipe', () => {
  let pipe = new PhoneNumberPipe();

  // specs
  it('test phone number pipe', () => {
    expect(pipe.transform('3212053181', '1')).toEqual('321-205-3181');
  });

  // specs
  it('should work with empty string', () => {
    expect(pipe.transform('', '')).toEqual('');
  });

  it('should throw with invalid values', () => {
    // must use arrow function for expect to capture exception
    // expect(() => pipe.transform(undefined)).toThrow();
    // expect(() => pipe.transform()).toThrow();
    // expect(() => pipe.transform()).toThrowError('Requires a String as input');
  });

});


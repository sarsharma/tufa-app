import { Pipe, PipeTransform } from '@angular/core';

import * as _ from 'lodash';

@Pipe({
  name: 'propFilterPipe'
})
export class PropFilterPipe implements PipeTransform {

  transform(data, propNameValues: Map<string, string[]>): Array<any> {
    // let searchText = new RegExp(args, 'ig');    
    // if (data.length > 0) {
     
      var self = this;

      return data.filter( function (o) {
        
        let filterValid: boolean = false;

        propNameValues.forEach(function (value, key, map) {

          let mainString: string = o[key];
          let valuesToSearch: string[] = value;

          for (let v in valuesToSearch) {
            if (mainString.includes(valuesToSearch[v])) {
              filterValid = filterValid || true;
            } else
              filterValid = filterValid || false;
          }

        });

        return filterValid;

      });
  }

}

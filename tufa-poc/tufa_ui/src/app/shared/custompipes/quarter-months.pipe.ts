import { Pipe, PipeTransform } from '@angular/core';
import * as _ from "lodash";

/*
 * Format the quarter months display  as January, February, March
 * Takes CVS and formats it.
 * Usage:
 *   value | quarterMonthsPipe
 * Example:
 *   {{ January,February,March |  quarterMonthsPipe}}
 *   formats to: January, February, March
*/
@Pipe({ name: 'quarterMonthsPipe' })
export class QuarterMonthsPipe implements PipeTransform {
  transform(value: string[]): string {
    let formattedValue = _.join(value, ', ');
    return formattedValue;
  }
}

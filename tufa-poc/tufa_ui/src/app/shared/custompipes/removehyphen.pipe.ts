import { Pipe, PipeTransform } from '@angular/core';

/*
 * Changes the case of the first letter of a given number of words in a string.
*/

@Pipe({name: 'removeHyphen', pure: false})
export class RemoveHyphen implements PipeTransform {
    transform(input: string): string {
        if (input == null) {
            return input;
        }
        let result = [];
        let strArr = input.toString().toUpperCase().split(", ").filter(word=>word.indexOf("NON-TAXABLE") === -1);
        for (let str in strArr){
            result.push(strArr[str].replace(/-/g," "));
        }
        if(input.toString().toUpperCase().split(", ").filter(word => word.indexOf("NON-TAXABLE") !== -1).length > 0) 
            result.push(input.toString().toUpperCase().split(", ").filter(word => word.indexOf("NON-TAXABLE") !== -1).join(", "));

        return result.length > 0 ? result.join(", ").toString() : '';
    }
}

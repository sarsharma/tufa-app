import { ReportStatusPipe } from './report-status.pipe';

describe('Pipe: ReportStatusPipe', () => {
  let pipe = new ReportStatusPipe();

  // specs
  it('test report status pipe', () => {
    expect(pipe.transform('NSTD')).toEqual('Not Started');
    expect(pipe.transform('COMP')).toEqual('Complete');
    expect(pipe.transform('ERRO')).toEqual('Error');
    expect(pipe.transform('')).toEqual('Not Available');
  });
});

import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "reportStatusPipe" })
export class ReportStatusPipe implements PipeTransform {

  constructor() {
  }

  transform(value: string): string {
      if (value === "NSTD" || value === "Not Started") {
        return("Not Started");
      } else if (value === "COMP" || value === "Complete") {
        return ("Complete");
      } else if (value === "ERRO" || value === "Error") {
        return ("Error");
      } else {
        return ("Not Available");
      }
  }
}

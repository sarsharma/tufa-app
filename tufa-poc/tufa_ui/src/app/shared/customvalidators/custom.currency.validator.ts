export class CustomCurrencyValidator {

    private _regexCurrency = /^\$?\d+(,\d{3})*\.?[0-9]{0,2}?$/;

    public validate(value): boolean {
           return this._regexCurrency.test(value);
    }

}

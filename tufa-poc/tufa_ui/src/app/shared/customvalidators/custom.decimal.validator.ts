export class CustomDecimalValidator {
    private _regexDecimalNumber = /^(?:^|\s)(\d*\.?\d+|\d{1,3}(?:,\d{3})*(?:\.\d+)?)(?!\S)$/;

    public validate(value) : boolean{
           return this._regexDecimalNumber.test(value);
    }

}

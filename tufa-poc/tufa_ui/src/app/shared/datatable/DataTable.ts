import { DefaultIterableDiffer, Directive, DoCheck, EventEmitter, Input, IterableDiffer, IterableDiffers, OnChanges, Output, Pipe, SimpleChange } from "@angular/core";
import * as _ from "lodash";
import { LocalStorageService } from 'ngx-webstorage';
import { ReplaySubject, Subscription } from "rxjs";
import { RouterGlobalService } from "../service/router.global.service";
import { DatatableFilter } from './DataTableFilter';
import { NavigationAttributes } from "./navigation.attributes.model";
import { PaginationAttributes } from "./pagination.attributes.model";


declare var jQuery: any;

/*********** */
export interface NavigationEvent {
    rowdata: any;
    rowindex: number;
    eventtype?: string;
}

/*********** */
export interface SortEvent {
    sortBy: string | string[];
    sortOrder: any[];
}

export interface PageEvent {
    activePage: number;
    rowsOnPage: number;
    dataLength: number;
}

export interface DataEvent {
    length: number;
}

@Directive({
    selector: 'table[mfData]',
    exportAs: 'mfDataTable'
})
export class DataTable implements OnChanges, DoCheck {

    @Input("mfData") public inputData: any[] = [];

    @Input("mfSortBy") public sortBy: string | string[] = "";

    @Input("mfSortOrder") public sortOrder: any[] = ["asc"];
    @Output("mfSortByChange") public sortByChange = new EventEmitter<string | string[]>();
    @Output("mfSortOrderChange") public sortOrderChange = new EventEmitter<string | string[]>();
    @Output("mfOnPageChange") public onServerPageChange = new EventEmitter<PageEvent>();
    @Input("mfRowsOnPage") public rowsOnPage = 1000;
    @Input("mfActivePage") public activePage = 1;
    @Input("mfAmountOfRows") public amountOfRows = 0;
    @Input("mfIsServerPagination") public isServerPaginationage: boolean = true;

    @Input("mfRecalculatePage") public recalculateActivePg: boolean = true;

    @Input("mfStoreState") public storeState = false;
    @Input("mfStoreStateonrefresh") public storeStateonrefresh = false;
    @Input("mfNavigateUrls") public navigateUrls: string;

    public urls: string[];


    @Input("mfDataTableName") public dataTableName: string;
    @Input("mfFilters") public filters: DatatableFilter;
    @Output("restoreFilters") public restoreFilters = new EventEmitter<any>();

    @Input("mfFilterPipe") public filterPipe: Pipe;

    @Input("mfForceActivePgChg ") public forceActivePgChg = 1;
    @Output("mfOnLocalPageChange") public onPageChange = new EventEmitter<PageEvent>();

    @Input("mfSortFunction") public sortFunction: [{ name: string, func: () => any }];
    @Output("mfAfterSortOrderChange") public afterSortOrderChange = new EventEmitter<string | string[]>();

    public data: any[];

    /****** **/
    public vardata: any[];
    public cols: string[];
    public colSearch: string;
    public colSearchVal: string[];

    @Input("navigationAttrs") public navigationAttrs: NavigationAttributes;

    //@Input("mfRefreshRowData") public refreshRowData: any;

    @Output("mfNavigationEvent") public navigationEvent = new EventEmitter<NavigationEvent>();


    /*********** */

    public onSortChange = new ReplaySubject<SortEvent>(1);

    public defaultIterableDiffer: DefaultIterableDiffer<{}>;

    private mustRecalculateData = false;
    private diff: IterableDiffer<{}>;
    private routerSubscription: Subscription;

    private sortedInputDataOnNavigation: any[];

    public constructor(private differs: IterableDiffers,
        private routerService: RouterGlobalService,
        private storage: LocalStorageService
    ) {

        this.defaultIterableDiffer = new DefaultIterableDiffer();
        this.diff = differs.find([]).create(null);
    }


    ngAfterViewInit() {

        if (this.storeState) {

            if (this.isUrlPresent(this.urls, this.routerService.getPreviousUrl())) {
                this.restorePgState();
                this.restoreFilterState();
            }

        }

    }


    ngOnInit() {

        if (this.storeState) {
            this.routerSubscription = this.routerService.navigateStartSource$.subscribe(
                event => {
                    if (this.isUrlPresent(this.urls, event.url)) {
                        this.setPgState();
                        this.setFilterState();
                    }
                    else {
                        this.cleanPgState();
                        this.cleanFilterState();
                    }
                }
            );

        }

    }

    public getSort(): SortEvent {
        return { sortBy: this.sortBy, sortOrder: this.sortOrder };
    }

    public setSort(sortBy: string | string[], sortOrder: any[]): void {
        if (this.sortBy !== sortBy || this.sortOrder !== sortOrder) {
            this.sortBy = sortBy;
            this.sortOrder = sortOrder; //_.includes(["asc", "desc"], sortOrder) ? sortOrder : "asc";
            this.mustRecalculateData = true;
            this.onSortChange.next({ sortBy: sortBy, sortOrder: sortOrder });
            this.sortByChange.emit(this.sortBy);
            this.sortOrderChange.emit(this.sortOrder);
        }
    }

    public getPage(): PageEvent {
        return { activePage: this.activePage, rowsOnPage: this.rowsOnPage, dataLength: this.inputData.length };
    }

    public setPage(activePage: number, rowsOnPage: number): void {

        if (this.rowsOnPage !== rowsOnPage || this.activePage !== activePage) {
            this.activePage = this.activePage !== activePage ? activePage : this.calculateNewActivePage(this.rowsOnPage, rowsOnPage);
            this.rowsOnPage = rowsOnPage;
            this.mustRecalculateData = true;
            if (this.isServerPaginationage)
                this.onServerPageChange.emit({
                    activePage: this.activePage,
                    rowsOnPage: this.rowsOnPage,
                    dataLength: this.amountOfRows
                });
            else
                this.onPageChange.emit({
                    activePage: this.activePage,
                    rowsOnPage: this.rowsOnPage,
                    dataLength: this.inputData ? this.inputData.length : 0

                });
        }
    }

    private calculateNewActivePage(previousRowsOnPage: number, currentRowsOnPage: number): number {
        let firstRowOnPage = (this.activePage - 1) * previousRowsOnPage + 1;
        let newActivePage = currentRowsOnPage === 0 ? 1 : Math.ceil(firstRowOnPage / currentRowsOnPage);
        return newActivePage;
    }

    private recalculatePage() {

        // let lastPage
        // if (this.isServerPaginationage) {
        //  lastPage = Math.ceil(this.amountOfRows / this.rowsOnPage);
        // }
        // else{
        //  lastPage = Math.ceil(this.inputData.length / this.rowsOnPage);
        // }
        let lastPage = Math.ceil(this.amountOfRows / this.rowsOnPage);

        this.activePage = lastPage < this.activePage ? lastPage : this.activePage;
        this.activePage = this.activePage || 1;


        if (this.isServerPaginationage) {
            this.onPageChange.emit({
                activePage: this.activePage,
                rowsOnPage: this.rowsOnPage,
                dataLength: this.amountOfRows,


            });
        }
        else {
            this.onPageChange.emit({
                activePage: this.activePage,
                rowsOnPage: this.rowsOnPage,
                dataLength: this.inputData.length
            });

        }
    }

    public ngOnChanges(changes: { [key: string]: SimpleChange }): any {

        if (changes["rowsOnPage"] && !this.isServerPaginationage) {
            this.rowsOnPage = changes["rowsOnPage"].previousValue;
            this.setPage(this.activePage, changes["rowsOnPage"].currentValue);
            this.mustRecalculateData = true;
        }


        if (changes["sortBy"] || changes["sortOrder"]) {
            // if (!_.includes(["asc", "desc"], this.sortOrder)) {
            //     console.warn("angular2-datatable: value for input mfSortOrder must be one of ['asc', 'desc'], but is:", this.sortOrder);
            //     this.sortOrder = "asc";
            // }
            if (this.sortBy) {
                this.onSortChange.next({ sortBy: this.sortBy, sortOrder: this.sortOrder });
            }
            this.mustRecalculateData = true;

            /********************/
            // this.resetNavigationAttributes();
            /*******************/

        }
        if (changes["inputData"]) {

            if (this.storeStateonrefresh && this.getPgState()) {
                this.restorePgState();
            }

            this.inputData = changes["inputData"].currentValue || [];

            this.recalculatePage();
            this.mustRecalculateData = true;

            /********************/
            this.vardata = this.inputData;
            // this.resetNavigationAttributes();


            /*******************/
        }


        if (changes["navigationAttrs"]) {
            this.navigationAttrs = changes["navigationAttrs"].currentValue;
            if (this.navigationAttrs) {
                this.cols = this.navigationAttrs.cols;
                //check for  "mvnext" needs to be removed 
                if (_.indexOf(this.navigationAttrs.action, "mvnext") != -1) {
                    this.colSearch = this.navigationAttrs.colSearch;
                    this.colSearchVal = this.navigationAttrs.colSearchVal;
                    this.moveToNextRecord();
                } else if (_.indexOf(this.navigationAttrs.action, "nextrecord") != -1) {
                    this.colSearch = this.navigationAttrs.colSearch;
                    this.colSearchVal = this.navigationAttrs.colSearchVal;
                    this.nextRecord();
                } else if (_.indexOf(this.navigationAttrs.action, "mvtorecord") != -1) {
                    this.colSearch = this.navigationAttrs.colSearch;
                    this.colSearchVal = this.navigationAttrs.colSearchVal;
                    this.moveToRecord();
                }
            }
        }

        if (changes["navigateUrls"]) {
            let navigateUrl: string = changes["navigateUrls"].currentValue;
            this.urls = navigateUrl.split(';');
        }

        if (changes["filters"]) {
            if (this.filters) {
                if (typeof this.filters.isFilterSelected == "function" && this.filters.isFilterSelected())
                    this.activePage = 1;
            }
        }

        // if (changes["forceActivePgChg"]) {
        //     this.setPage(this.forceActivePgChg, this.rowsOnPage);
        // }

    }

    public ngDoCheck(): any {
        let changes = this.diff.diff(this.inputData);
        if (changes) {
            // if (this.storeStateonrefresh && this.getPgState()) {
            //     this.restorePgState();
            // }
            if (!this.isServerPaginationage)
                this.amountOfRows = this.inputData.length;
            this.recalculatePage();
            this.mustRecalculateData = true;


        }
        if (this.mustRecalculateData) {
            this.fillData();
            this.mustRecalculateData = false;
        }
    }

    private getPgState() {

        let key = "pgAttr-" + this.dataTableName;
        return this.storage.retrieve(key);

    }


    private fillData(): void {
        this.activePage = this.activePage;
        this.rowsOnPage = this.rowsOnPage;

        let offset = (this.activePage - 1) * this.rowsOnPage;
        let data = this.inputData;
        let sortBy: any = this.sortBy;
        let sortOrderSet = typeof this.sortOrder === 'string' ? [this.sortOrder] : this.sortOrder;


        if ((typeof sortBy === 'string' || sortBy instanceof String) && this.sortFunction && this.sortFunction.find(sort => sort.name == sortBy)) {
            sortBy = [sortBy];
        }

        if (!this.isServerPaginationage) {
            if (typeof sortBy === 'string' || sortBy instanceof String) {
                data = _.orderBy(data, this.caseInsensitiveIteratee(<string>sortBy), sortOrderSet);
            } else {
                if (this.sortFunction) {
                    //Copy sort by
                    sortBy = [...sortBy];
                    this.sortFunction.forEach(sort => {
                        //Find this index of the sort that matches the sort function
                        let index = sortBy.indexOf(sort.name);
                        if (index > -1) {
                            //Replace the sortBy string with the function
                            sortBy[index] = sort.func;
                        }
                    })
                }
                data = _.orderBy(data, sortBy, sortOrderSet);
            }
            /********************* */
            this.vardata = data;
            /******************* */
            data = _.slice(data, offset, offset + this.rowsOnPage);
            this.afterSortOrderChange.emit(sortBy);
        }
        this.data = data;


    }


    private caseInsensitiveIteratee(sortBy: string) {
        return (row: any): any => {
            let value = row;
            for (let sortByProperty of sortBy.split('.')) {
                if (value) {
                    //Will find the item whos property (left of =) is equal to the value (right of the =)
                    if (sortByProperty.indexOf('=') >= 0 && Array.isArray(value)) {
                        let find: string[] = sortByProperty.split('=');
                        value = value.find(item => item[find[0]] == find[1]);
                    } else {
                        value = value[sortByProperty];
                    }
                }
            }
            /*
                Sorting numbers is different than strings 
                EX: "123" is less than "2" but 2 is less than 123
            */
            if (!isNaN(value)) {
                value = Number(value);
            }
            else if (value && typeof value === 'string' || value instanceof String) {
                return value.toLowerCase();
            }
            return value;
        };
    }

    /*************
     * 
     * DataTable Navigation
     * 
     * ***********/

    moveToNextRecord() {

        let rowData = this.navigationAttrs.rowdata

        let indices = this.findNextRowDataIndex(rowData);

        if (indices && indices.curentIdx >= 0) {
            let currentRowPos = indices.curentIdx % this.rowsOnPage;
            jQuery.extend(true, this.data[currentRowPos], rowData);
        }

        if (indices.nextIdx != -1) {
            if (this.calculatePage(indices.nextIdx + 1, true)) {
                let rowpos = this.calculateRowOnPage();
                this.navigationEvent.emit(rowpos);
            } else {
                let rowidx = (indices.nextIdx % this.rowsOnPage);
                let rwdata = this.vardata[indices.nextIdx];
                this.navigationEvent.emit({ rowdata: rwdata, rowindex: rowidx });
            }
        } else
            this.navigationEvent.emit({ rowdata: null, rowindex: -1 });
    }

    nextRecord() {

        let rowData = this.navigationAttrs.rowdata

        let indices = this.findNextRowDataIndex(rowData);

        if (indices.nextIdx != -1) {
            this.navigationEvent.emit({ rowdata: this.vardata[indices.nextIdx], rowindex: indices.nextIdx, eventtype: 'nextrecord' });
        } else
            this.navigationEvent.emit({ rowdata: null, rowindex: -1, eventtype: 'nextrecord' });
    }

    moveToRecord() {

        let rowData = this.navigationAttrs.rowdata
        let currentIndex = this.getCurrentGridPosition(rowData);

        if (currentIndex != -1) {
            if (this.calculatePage(currentIndex + 1, true)) {
                let rowpos = this.calculateRowOnPage();
                this.navigationEvent.emit({ rowdata: rowData, rowindex: rowpos.rowindex, eventtype: 'mvtorecord' });
            } else {
                let rowidx = (currentIndex % this.rowsOnPage);
                let rwdata = this.vardata[currentIndex];
                this.navigationEvent.emit({ rowdata: rwdata, rowindex: rowidx, eventtype: 'mvtorecord' });
            }
        } else
            this.navigationEvent.emit({ rowdata: null, rowindex: -1, eventtype: 'mvtorecord' });
    }


    public refreshGrid() {
        let rowdata = this.navigationAttrs.rowdata;
        if (rowdata) {
            let currentIndx = this.getCurrentGridPosition(rowdata);
            jQuery.extend(true, this.vardata[currentIndx], rowdata);
        }
    }

    //called when moving to a record - moveToRecord() called.
    private calculateRowOnPage() {

        let currentPg = this.activePage;
        let rowsOnPage = this.rowsOnPage;
        let previousNoRows = (currentPg - 1) * rowsOnPage;

        for (let i = previousNoRows; i < (previousNoRows + rowsOnPage); i++) {
            let rowdata = this.vardata[i];

            if (this.colSearchVal.findIndex(val => val === rowdata[this.colSearch]) > -1)
                return { rowdata: rowdata, rowindex: i - previousNoRows }
        }
    }

    private calculatePage(index, setPage?: boolean) {

        let pgNum = Math.ceil(index / this.rowsOnPage);
        let pgChange = (this.activePage != pgNum);
        if (setPage && pgChange)
            this.setPage(pgNum, this.rowsOnPage);
        return pgChange;

    }


    private getCurrentGridPosition(rowdata) {

        let cols: any[] = this.cols;
        let isequal: boolean = true;
        let data = this.vardata;
        for (let i = 0; i < data.length; i++) {
            let rwdata = data[i];
            for (let j in cols) {
                isequal = isequal && (rwdata[cols[j]] == rowdata[cols[j]]);
            }
            if (isequal) return i;
            isequal = true;
        }
    }

    private isDuplicateRecord(orow, nrow) {

        let dupCnt = 0;
        for (let i = 0; i < this.cols.length; i++) {
            let col = this.cols[i];
            if (orow[col] === nrow[col])
                dupCnt++;
        }
        if (dupCnt == this.cols.length)
            return true;

        return false;
    }

    private findNextRowDataIndex(rowData) {

        let data = this.vardata;
        let currentIndex = this.getCurrentGridPosition(rowData);

        let nextIndex = -1;
        let thisRef = this;
        for (let i = currentIndex + 1; i < data.length; i++) {
            let rwdata = data[i];
            //Search against 'null' serach criteria
            if (this.colSearchVal.length == 1 && this.colSearchVal[0] === null) {
                if (rwdata[this.colSearch] === null) {
                    nextIndex = i;
                    break;
                } else {
                    continue;
                }
            }
            else if ((this.navigationAttrs.rowIgnoreCol && !rwdata[this.navigationAttrs.rowIgnoreCol]) || !this.navigationAttrs.rowIgnoreCol) {
                if(((_.findIndex(this.colSearchVal, function (sVal) { return (rwdata[thisRef.colSearch] == sVal) || rwdata[thisRef.colSearch].includes(sVal) }) > -1 || this.colSearchVal[0].includes('ANY'))
                        && ((!this.isDuplicateRecord(rowData, rwdata) && this.navigationAttrs.action[0] == 'nextrecord') || this.navigationAttrs.action[0] !== 'nextrecord'))) {
                    nextIndex = i;
                    break;
                }
            }
        }

        return { curentIdx: currentIndex, nextIdx: nextIndex };
    }

    public resetNavigationAttributes() {
        this.navigationEvent.emit({ rowdata: null, rowindex: -1, eventtype: null });
    }
    /***********************
     * End DataTable Navigation
     ***********************/

    public setPgState() {
        let pgEvent: PageEvent = this.getPage();
        let sortEvent: SortEvent = this.getSort();
        let pgAttr = new PaginationAttributes(pgEvent.activePage, pgEvent.rowsOnPage, pgEvent.dataLength, null, sortEvent.sortBy, sortEvent.sortOrder);

        let key = "pgAttr-" + this.dataTableName;
        this.storage.store(key, pgAttr);

    }

    public restorePgState() {
        let key = "pgAttr-" + this.dataTableName;
        let pgAttrs: PaginationAttributes = this.storage.retrieve(key);
        if (pgAttrs) {
            setTimeout(() => {
                this.setPage(pgAttrs.activePage, pgAttrs.pageSize);
                this.setSort(pgAttrs.sortBy, pgAttrs.sortOrder);
                this.amountOfRows = pgAttrs.itemsTotal;
            }, 100);
        }
    }

    private cleanPgState() {
        let key = "pgAttr-" + this.dataTableName;
        this.storage.clear(key);
    }


    private setFilterState() {
        let key = "filterAttr-" + this.dataTableName;
        this.storage.store(key, this.filters);
    }

    private restoreFilterState() {
        let key = "filterAttr-" + this.dataTableName;
        let filters = this.storage.retrieve(key);
        this.restoreFilters.emit(filters);
    }

    private cleanFilterState() {
        let key = "filterAttr-" + this.dataTableName;
        this.storage.clear(key);
    }

    private isUrlPresent(urls: string[], url: string) {

        for (let i in urls) {
            if (url.includes(urls[i]))
                return true;
        }
        return false;
    }

    public gotoLastPage() {
        if (!this.isServerPaginationage)
            this.amountOfRows = this.inputData.length;
        this.activePage = Math.ceil(this.amountOfRows / this.rowsOnPage);
    }

    public gotoFirstPage() {
        this.activePage = 1;
    }

    ngOnDestroy() {
        if (this.routerSubscription)
            this.routerSubscription.unsubscribe();
    }

}
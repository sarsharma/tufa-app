import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DataTable } from "./DataTable";
import { DefaultSorter } from "./DefaultSorter";
import { Paginator } from "./Paginator";
import { BootstrapPaginator } from "./BootstrapPaginator";
//import {DataTableNavigationModule} from "./DataTableNavigationModule"
declare var jQuery: any;


@NgModule({
    imports: [
        CommonModule,
        //DataTableNavigationModule
    ],
    declarations: [
        DataTable,
        DefaultSorter,
        Paginator,
        BootstrapPaginator
    ],
    exports: [
        DataTable,
        DefaultSorter,
        Paginator,
        BootstrapPaginator
    ]
})
export class DataTableModule {

    constructor(){}

}
import { Component, Input, OnChanges, Optional, SimpleChange } from "@angular/core";
import { DataTable, PageEvent } from "./DataTable";

@Component({
    selector: "mfPaginator",
    template: `<ng-content></ng-content>`
})

export class Paginator implements OnChanges {

    @Input("mfTable") inputMfTable: DataTable;

    private setPgState: boolean = false;
    private restorePgState: boolean = false;

    public activePage: number;
    public rowsOnPage: number;
    public dataLength: number = 0;
    public lastPage: number;

    private mfTable: DataTable;

    public constructor( @Optional() private injectMfTable: DataTable) {
    }

    public ngOnChanges(changes: { [key: string]: SimpleChange }): any {

        this.mfTable = this.inputMfTable || this.injectMfTable;
        this.onPageChangeSubscriber(this.mfTable.getPage());
        this.mfTable.onPageChange.subscribe(this.onPageChangeSubscriber);

    }

    public setPage(pageNumber: number): void {
        this.mfTable.setPage(pageNumber, this.rowsOnPage);

        
       //custom functionality
       if(this.mfTable.storeStateonrefresh)
       this.mfTable.setPgState();


        this.mfTable.resetNavigationAttributes();
    }

    public setRowsOnPage(rowsOnPage: number): void {
        this.mfTable.setPage(this.activePage, rowsOnPage);
        this.mfTable.resetNavigationAttributes();

    }

    private onPageChangeSubscriber = (event: PageEvent) => {
        this.activePage = event.activePage;
        this.rowsOnPage = event.rowsOnPage;
        this.dataLength = event.dataLength;
        this.lastPage = Math.ceil(this.dataLength / this.rowsOnPage);
    }

}

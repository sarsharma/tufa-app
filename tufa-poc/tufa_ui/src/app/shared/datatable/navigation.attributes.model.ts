export interface NavigationAttributes{

    rowdata: any;
    rowIgnoreCol?:string;
    cols:string [];
    colSearch: string;
    colSearchVal: string [];
    action: string [];


}

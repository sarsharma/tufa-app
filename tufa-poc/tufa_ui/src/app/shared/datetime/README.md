# TUFA Datepicker
The TUFA datepicker is a custom mix of NG2-datetime and UXSolutions datepicker

## NG2-datetime (Angular Interface)
https://github.com/nkalinov/ng2-datetime/tree/master/src/ng2-datetime

## UXSolutions datepicker (Bootstrap Interface)
https://github.com/uxsolutions/bootstrap-datepicker/tree/master/dist/js

import { Directive, HostListener, ElementRef, Output, EventEmitter, Renderer } from "@angular/core";

@Directive({ selector: "[aClick]" })
export class CustomAnchorClickDirective {

    @Output('aClick') callBack = new EventEmitter<boolean>();

    constructor( private elementRef: ElementRef, private _renderer: Renderer) {
    }

    ngOnInit() {
    }

    @HostListener('keypress', ['$event'])
    keyenter(event: KeyboardEvent) {
        if (event.which === 32 || event.which === 13){
            this.callBack.emit(true);
        }
    }

    @HostListener('click', ['$event'])
    mouseclick(event: MouseEvent) {
        this.callBack.emit(true);
    }
}

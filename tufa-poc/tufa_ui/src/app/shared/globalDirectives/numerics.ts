import { Directive, HostListener, ElementRef, OnInit } from "@angular/core";

@Directive({ selector: "[customNumerics]" })
export class CustomNumericsDirective implements OnInit {

    private el: HTMLInputElement;

    constructor(
        private elementRef: ElementRef
    ) {
        this.el = this.elementRef.nativeElement;
    }

    ngOnInit() {
    }

    @HostListener('keypress', ['$event'])
    keypress(event: KeyboardEvent) {
        if (((event.which >= 48 && event.which <= 57) || event.which === 46 || event.which === 44) && !event.shiftKey) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    }

    /**
   * Lisent for the user to paste. Check the value that user is attempting
   * to paste is not a number (NaN) and the combined value is NaN then prevent the paste.
   * 
   * IE hadels pasting differently than other browsers
   * 
   * @param e paste event
   * @param currentValue value currently in the input
   */
    @HostListener('paste', ['$event', '$event.target.value'])
    blockPaste(e: any, currentValue) {
        let pasteValue;

        //Neede for IE. ClipboardData may not be present in the event
        let clipboardData = e.clipboardData
            || (<any>window).clipboardData
            || window['clipboardData'];

        if (clipboardData || clipboardData.getData) {
            pasteValue = clipboardData.getData('Text') || clipboardData.getData('text/plain');
        }

        if (isNaN(pasteValue) && isNaN(pasteValue + currentValue)) {
            console.warn("The pasted value is not a valid number: " + pasteValue);
            //Needed since IE does not support preventDefault()
            e.preventDefault ? e.preventDefault() : (e.returnValue = false);
        }
    }
}

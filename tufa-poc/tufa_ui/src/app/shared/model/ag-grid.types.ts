/**
 * NOTE TO DEVELOPER:
 * Please use this fine to build documentation for the AG-GRID. This will make it easeir for other developers to work with it.
 * If you are trying to use a property or object that is not defined in this file but is in AG-GRID documention (https://www.ag-grid.com/documentation-main/documentation.php)
 * please add it here.
 * 
 * When adding a new object lead it with 'Ag*'. EX: AgColumnDefinition
 * When adding a new property make sure to make it optional using the '?'
 */


/**
 * Custom object to store ag-grid state in memory
 */
export class AgGridState {
    /** Used as grid id */
    version?: number;

    versionName?: string;

    /** String that represents the grids column state
     * @see getColumnState() https://www.ag-grid.com/javascript-grid-column-api/
     */
    colState?: string;

    /** String that represents the grids column group state
     * @see getColumnGroupState() https://www.ag-grid.com/javascript-grid-column-api/
     */
    groupState?: string;

    /** String that represents the grids sort state
     * @see getSortModel() https://www.ag-grid.com/javascript-grid-api/
     */
    sortState?: string;

    /** String that represents the grids filter state
     * @see getFilterModel() https://www.ag-grid.com/javascript-grid-api/
     */
    filterState?: string;

}


/**
 * As you use this ColumnDefinion object you may find properties that are not defined here. 
 * Please add those properties matching the proerties found here https://www.ag-grid.com/javascript-grid-column-properties/
 */
export class AgBaseColumn {
    /** Display name for column */
    headerName?: string;
}

/**
 * As you use this ColumnDefinion object you may find properties that are not defined here. 
 * Please add those properties matching the proerties found here https://www.ag-grid.com/javascript-grid-column-properties/
 */
export class AgColumnDefinition extends AgBaseColumn {
    autoHeight?:boolean;

    wrapText?:boolean;

    resizable?: boolean;
    /** Name of filed in the object */
    field?: string;

    /** Enable grouping  */
    enableRowGroup?: boolean;

    /** Id for the column */
    colId?: string;

    /** Column width */
    width?: number;

    /** Name of cell renderer */
    cellRenderer?: string | Function;

    /** */
    cellRendererParams?: any;

    /** */
    tooltipField?: string;

    /** Function used to format the value */
    valueFormatter?: (...args: any[]) => {};

    valueGetter?: (params: any) => void;

    /** Flage to allow the column to be sortable */
    sortable?: boolean = true;

    /** Check box in the header */
    headerCheckboxSelection?: boolean = false;

    /** Select only filtered results */
    headerCheckboxSelectionFilteredOnly?: boolean = false;

    /** Text for tooltip on header */
    headerTooltip?: string;

    headerHeight?: number;

    /** Show checkbox */
    checkboxSelection?: boolean = false;

    /** Hide column */
    hide?: boolean;

    /** Hide tool panel for column */
    suppressToolPanel?: boolean;

    cellStyle?: object;

    /** Boolean used to determine if the column should be
     * included in the Excel/CSV export.
     * 
     * @default true
     */
    includeInExport?: boolean = true;

    /** Function to return a string key for a value. This string is used for grouping, set filtering, and searching within cell editor dropdowns. When filtering and searching the string is exposed to the user, so return a human-readable value. */
    keyCreator?: (params: any) => {};

    /** custom sorting function */
    comparator?: (...args: any[]) => {};
    /**Disables the 'loading' overlay. */
    suppressLoadingOverlay?:boolean = false;
    /** Disables the 'no rows' overlay. */
    suppressNoRowsOverlay?:boolean = true;
    suppressSizeToFit?:boolean = false;

}

export class AgExportDefinition {

    /**
     * If true exports all coumns including hidden
     * 
     * @default true
     */
    allColumns?: boolean = true;

    /** Key's of columns to include in export */
    columnKeys?: string[];

    /**
     * Name of exported file
     */
    fileName?: string;
}
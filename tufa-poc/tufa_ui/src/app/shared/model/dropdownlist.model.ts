export class DropDownListItem{
    id: number;
    itemName: string;
    itemCode: string;
}

import { ServerSideDataSourceService } from '../../service/ag-grid.datasource.service';
import { AgGridStateMgmtService } from '../../../shared/service/ag-grid.state.mgmt.service';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "../../../login/services/auth.service";
import { LocalStorageService } from 'ngx-webstorage';


export function  AgGridStateMgmtServiceFn (authService: AuthService, localStorage: LocalStorageService, http: HttpClient): AgGridStateMgmtService {
    return new AgGridStateMgmtService(authService, localStorage, http);
};

export function agGridStateMgmtFactory(){
    return  AgGridStateMgmtServiceFn;
};


export  function ServerSideDataSourceServiceFn (http: HttpClient): ServerSideDataSourceService  {
    return new ServerSideDataSourceService(http);
};
export function  serverSideDataSourceFactory(){
    return ServerSideDataSourceServiceFn;
};


// export function agGridStateMgmtFactory(){
//     return (authService: AuthService, localStorage: LocalStorageService, http: HttpClient): AgGridStateMgmtService => {
//         return new AgGridStateMgmtService(authService, localStorage, http);
//     };
// };

// export function  serverSideDataSourceFactory(){
//      return (http: HttpClient): ServerSideDataSourceService => {
//         return new ServerSideDataSourceService(http);
//     };
// };


export let agGridStateMgmtProvider = { provide: AgGridStateMgmtService, useFactory: agGridStateMgmtFactory(), deps: [AuthService, LocalStorageService, HttpClient] };
export let serverSideDataSourceProvider = { provide: ServerSideDataSourceService, useFactory: serverSideDataSourceFactory(), deps: [HttpClient] };







import { Injectable } from "../../../../node_modules/@angular/core";
import { TobaccoClass } from "../../model/tobacco.class.model";
import { TobaccoClassService } from "../service/tobacco-class.service";

@Injectable({
    providedIn: 'root'
})
export class TobaccoClassProvider {
    private _tobaccoClasses: TobaccoClass[];

    constructor(private tobaccoTypeService: TobaccoClassService) { }

    async getTobaccoClasses() {
        await this.load();
        return this._tobaccoClasses;
    }

    public load() {
        return new Promise<TobaccoClass[]>((resolve, reject) => {
            if (!this._tobaccoClasses)
                this.tobaccoTypeService.getTobaccoTypes().subscribe(
                    data => {
                        this._tobaccoClasses = data;
                        resolve(this._tobaccoClasses);
                    }
                );
            else
                resolve(this._tobaccoClasses);

        })
    }

    public getTobaccoClassId(tobaccoClassNm: string): number {
        let found = this._tobaccoClasses.find(rec => rec.tobaccoClassNm == tobaccoClassNm);
        if (found) return found.tobaccoClassId;
        else return -1;
    }
}
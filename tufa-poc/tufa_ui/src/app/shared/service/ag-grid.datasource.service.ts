
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "../../../../node_modules/@angular/core";

const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
};
@Injectable()
export class ServerSideDataSourceService {

    private columnApi;
    private serverUrl;
    private gridApi;

    /**
     * serverUrl Provided by the component implementing the grid
     */
    constructor(private _httpClient: HttpClient) {

    }

    public setAgGridApis(params){
        this.gridApi= params.gridApi;
        this.columnApi = params.columnApi;
    }
    public setServerSideUrl(url){
            this.serverUrl = url;
    }

    //GET call 
    public getRows(params: any) {
        //make a call to the server 
        this._httpClient.put<any>(this.serverUrl, params.request, httpOptions).subscribe(
            result => {
                params.successCallback(result.data, result.lastRow);
                this.updateSecondaryColumns(params.request, result);
            }
        )

    }

    public updateSecondaryColumns(request, result) {
        let valueCols = request.valueCols;
        if (request.pivotMode && request.pivotCols.length > 0) {
            let secondaryColDefs = this.createSecondaryColumns(result.secondaryColumnFields, valueCols);
            this.columnApi.setSecondaryColumns(secondaryColDefs);
        } else {
            this.columnApi.setSecondaryColumns([]);
        }
    };

    public createSecondaryColumns(fields, valueCols) {
        let secondaryCols = [];

        function addColDef(colId, parts, res) {
            if (parts.length === 0) return [];

            let first = parts.shift();
            let existing = res.find(r => r.groupId === first);

            if (existing) {
                existing['children'] = addColDef(colId, parts, existing.children);
            } else {
                let colDef = {};
                let isGroup = parts.length > 0;
                if (isGroup) {
                    colDef['groupId'] = first;
                    colDef['headerName'] = first;
                } else {
                    let valueCol = valueCols.find(r => r.field === first);

                    colDef['colId'] = colId;
                    colDef['headerName'] = valueCol.displayName;
                    colDef['field'] = colId;
                    colDef['type'] = 'measure';
                }

                let children = addColDef(colId, parts, []);
                children.length > 0 ? colDef['children'] = children : null;

                res.push(colDef);
            }

            return res;
        }

        fields.sort();
        fields.forEach(field => addColDef(field, field.split('_'), secondaryCols));
        return secondaryCols;
    };

    numberCellFormatter(params) {
        let formattedNumber = Math.floor(Math.abs(params.value)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        return params.value < 0 ? '(' + formattedNumber + ')' : formattedNumber;
    }
}



import { HttpClient, HttpHeaders } from "@angular/common/http";
import { LocalStorageService } from 'ngx-webstorage';
import { Observable, Subject, Subscription } from "rxjs";
import { Injectable } from "../../../../node_modules/@angular/core";
import { AuthService } from "../../login/services/auth.service";
import { AgGridState } from "../model/ag-grid.types";
import { environment } from "./../../../environments/environment";

const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class AgGridStateMgmtService {

    private gridstateurl = environment.apiurl + "/api/v1/service/grid";
    public busy: Subscription;

    private preferenceSaved = new Subject<boolean>();
    public preferenceSaved$ = this.preferenceSaved.asObservable();

    private preferenceGet = new Subject<AgGridState[]>();
    public preferenceGet$ = this.preferenceGet.asObservable();

    constructor(private authService: AuthService,
        private storage: LocalStorageService,
        private http: HttpClient
    ) { }

    public saveGridState(newstate: AgGridState, gridId: string, cache: boolean = true) {
        let userId = this.authService.getSessionUser();

        let ob = this.http.post<AgGridState>(this.gridstateurl + "/" + gridId, newstate, httpOptions).subscribe(data => {

            // if (cache) {
            //     let stateId = "ag-grid-state-id-"+ gridId +"-"+userId;
            //     let states: AgGridState[] = this.storage.retrieve(stateId);
            //     if (!states)
            //         states = [];
            //     states.push(data);
            //     this.storage.store(stateId, states);
            // }

            this.preferenceSaved.next(true);
        });
        return ob;
    }


    public getGridStates(gridId): Observable<any> {

        // let userId = this.authService.getSessionUser();
        // let stateId = "ag-grid-state-id-"+gridId +"-"+ userId;
        // let states: AgGridState[] = this.storage.retrieve(stateId);

        // if (!states) {
        //retrieve states from back end using userId
        return this.http.get(this.gridstateurl + "/" + gridId, httpOptions);
    }

}
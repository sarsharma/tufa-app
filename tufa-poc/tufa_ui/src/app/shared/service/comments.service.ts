/*
 @author : Deloitte
 Service class to perform PermitPeriod related operations.
 */

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Credentials } from '../../login/model/credentials.model';
import { environment } from './../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class CommentsService {

    private updatePermitCommenturl = environment.apiurl + '/api/v1/permit';
    private updateCompanyCommenturl = environment.apiurl + '/api/v1/companies';

    private credential: Credentials;

    constructor(private http: HttpClient) { }

    public updateComment( objectId: any, comment: any, objectType: any): Observable<string>{
        if (objectType === 'COMPANY') {
            // tslint:disable-next-line:max-line-length
            return this.http.put(this.updateCompanyCommenturl + '/' + objectId + '/' + 'comment', comment, httpOptions).pipe(
                tap(_ => this.log('updateComment')),
                catchError(this.handleError<any>('updateComment'))
            );
        } else {
            return this.http.put(this.updatePermitCommenturl + '/' + objectId + '/' + 'comment', comment, httpOptions).pipe(
                tap(_ => this.log('updateComment')),
                catchError(this.handleError<any>('updateComment'))
            );
        }
    }

    
	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			// console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log('${operation} failed: ${error.message}');

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}

import { Injectable } from "@angular/core";
import * as moment from "moment";

@Injectable()
export class DateService {
	constructor() {}

	static getToday(): Date {
		return moment().toDate();
	}

	static getDateFromString(ds: string): Date {
		return moment(ds).toDate();
	}

	static isValid(d: Date) {
		return moment(d).isValid();
	}

	static resetTime(d: Date): Date {
		return moment(d)
			.startOf("day")
			.toDate();
	}

	static getDateByAddingDaysFromToday(numtoAdd: number): Date {
		return moment()
			.add(numtoAdd, "days")
			.toDate();
	}

	static getDateBySubtractingDaysFromToday(numDays: number): Date {
		return moment()
			.subtract(numDays, "days")
			.toDate();
	}

	static getDateByAddingDaysFromDate(d: Date, numDays: number): Date {
		return moment(d)
			.add(numDays, "days")
			.toDate();
	}

	static getDateBySubtractingDaysFromDate(d: Date, numDays: number): Date {
		return moment(d)
			.subtract(numDays, "days")
			.toDate();
	}

	static getDateByAddingYearsFromToday(numtoAdd: number): Date {
		return moment()
			.add(numtoAdd, "year")
			.toDate();
	}

	static getDateByAddingMonthsFromDate(d: Date, numtoAdd: number): Date {
		return moment(d)
			.add(numtoAdd, "month")
			.toDate();
	}

	static getDateBySubtractingMonthsFromDate(d: Date, numtoAdd: number): Date {
		return moment(d)
			.subtract(numtoAdd, "month")
			.toDate();
	}

	static getDateByAddingWeeksFromDate(d: Date, numtoAdd: number): Date {
		return moment(d)
			.add(numtoAdd, "week")
			.toDate();
	}

	static getDateBySubtractingWeeksFromDate(d: Date, numtoAdd: number): Date {
		return moment(d)
			.subtract(numtoAdd, "week")
			.toDate();
	}

	static getFormattedText(d: Date, formatString: string): string {
		return moment(d).format(formatString);
	}

	static getSaturdayFromDateString(ds: string): Date {
		return moment(ds)
			.day("Saturday")
			.toDate();
	}

	static getSaturdayAYearFromDateString(ds: string): Date {
		return moment(ds)
			.add(1, "year")
			.day("Saturday")
			.toDate();
	}

	static getSaturday(d: any): Date {
		if (!moment(d).isValid()) {
			console.log("invalid date passed");
		}
		let retd: Date = moment(d)
			.day("Saturday")
			.startOf("day")
			.toDate();
		// console.log('tdlplan-sheet - saturday of ' + ind.toString() + ' is ' + retd.toString() ) ;
		return retd;
	}

	static getFirstSaturdayOfMonthFromGivenDate(d: Date): Date {
		return moment(d)
			.startOf("month")
			.day("Saturday")
			.startOf("day")
			.toDate();
	}

	static getLastSaturdayOfMonthFromGivenDate(d: Date): Date {
		return moment(d)
			.endOf("month")
			.day("Saturday")
			.toDate();
	}

	static getStartOfMonthFromGivenDate(d: Date): Date {
		return moment(d)
			.startOf("month")
			.startOf("day")
			.toDate();
	}

	static getDifferenceBetweenDatesinWeeks(
		fromDate: Date,
		toDate: Date
	): number {
		return moment(toDate).diff(moment(fromDate), "week") + 1;
	}

	static getThisSaturday(): Date {
		return moment()
			.day("Saturday")
			.startOf("day")
			.toDate();
	}

	static getMonthOfDate(d: Date): number {
		return moment(d).month();
	}

	static getYearOfDate(d: Date): number {
		return moment(d).year();
	}

	static isDateBetweenDates(selDate: Date, start: Date, end: Date) {
		if (moment(selDate).isBetween(start, end, "days", "[]")) {
			return true;
		}
		return false;
	}

	static getDateFromYearMonthAndDate(year, month, day) {
		return moment([year, month, day]).toDate();
	}

	static getMonthNameFromMonthNum(monthnum: number) {
		return moment(monthnum, "M").format("MMM");
	}
}

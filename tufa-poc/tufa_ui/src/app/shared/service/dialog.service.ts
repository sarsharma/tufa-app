import { BsModalService, ModalOptions } from "../../../../node_modules/ngx-bootstrap";
import { initialState } from "../../../../node_modules/ngx-bootstrap/timepicker/reducer/timepicker.reducer";
import { Injectable } from "../../../../node_modules/@angular/core";
@Injectable()
export class DialogService {

    constructor(
        private ms: BsModalService
    ) {
        
    }

    showDialog(component: any, staticDialog: boolean = false, initialState: Object = new Object()) {
        let ops: ModalOptions  = new ModalOptions;
        ops.ignoreBackdropClick = staticDialog;
        ops.initialState = initialState;
        this.ms.show(component,ops);
    }
}
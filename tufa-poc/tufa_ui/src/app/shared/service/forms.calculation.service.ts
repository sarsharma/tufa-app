import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TobaccoType } from '../../permit/models/tobacco-type.model';

/**
 *
 *
 * @export
 * @class FormsCalculationService
 */
@Injectable()
export class FormsCalculationService {
  // Observable string sources
  private mfgformTTypeChangedSource = new Subject<TobaccoType>();

  private impformChangedSource = new Subject<string>();

  // Observable string streams
  // tslint:disable-next-line:member-ordering
  mfgformTTypeChanged$ = this.mfgformTTypeChangedSource.asObservable();
  impformChanged$ = this.impformChangedSource.asObservable();

 public mfgformTobaccoTypeChanged(ttmodel: TobaccoType) {
      this.mfgformTTypeChangedSource.next(ttmodel);
  }

  public impformChanged() {
      this.impformChangedSource.next();
  }
}

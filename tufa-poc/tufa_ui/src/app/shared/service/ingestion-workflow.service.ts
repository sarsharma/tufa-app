import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { IngestionWorkflowContext } from '../../shared/model/ingestion-workflow.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

/**
 * Created as a Singleton
 * @export
 * @class IngestionWorkFlowService 
 */
@Injectable({
    providedIn: 'root',
})
export class IngestionWorkFlowService {

    private workflowUrl = environment.apiurl + '/api/v1/trueup/ingestion/workflow';

    private currentContext: any;

    constructor(private _httpClient: HttpClient) { }

    /**
     * called when file selection toggle clicked. Saves the file selection to the backend
     * @param workflowModel 
     */
    public saveContextInfo(workflowModel: IngestionWorkflowContext) {
        return this._httpClient.put(this.workflowUrl, workflowModel, httpOptions).toPromise().then(
            cntxt => {
                this.currentContext = cntxt;
            }
        );
    }

    /**
     * called when navigating to the ingestion page. Sets the  current context. 
     * @param fiscal_yr 
     */
    public initContextInfo(fiscal_yr: string) {
        return this._httpClient.get(this.workflowUrl + '/' + fiscal_yr, httpOptions).toPromise().then(
            cntxt => {
                this.currentContext = cntxt;
            }
        );
    }

    /**
     * returns current context. To be utilized by other pages in Annual TrueUp flow such as Map, Compare, Compare details etc.
     */
    public getContextInfo() {
        return this.currentContext;
    }
}
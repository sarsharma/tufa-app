import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subscription, interval, Subject } from 'rxjs';



@Injectable()
export class RefreshTokenService implements OnDestroy {

    private sessionExpiredComponent = new Subject<any>();
    
    public callrefereshSession$ = this.sessionExpiredComponent.asObservable();
    private userActionEvents: Array<string> = [];
    private queuePollSubscription: Subscription;
    private refreshEventQueueTime: number;

    constructor() {
        //console.debug("Inside the constructor");
        this.refreshEventQueueTime = 5000 * 60;
       //keep polling the queue every 5 mins to check for user movements
       this.queuePollSubscription = interval(this.refreshEventQueueTime ).subscribe(x => {
        //    let dateTime = new Date();
        //    console.log('Polling called @'+ dateTime.toLocaleString());
            this.checkQueueForEvents();
          });
        
    }

    public setEventToQueue(event: string){
       //limiting the stored user actions to 10
        if(this.userActionEvents && this.userActionEvents.length > 9){
            this.userActionEvents.shift();
        }
        this.userActionEvents.push(event);
        
        //console.debug("added events to queue");
    }

    public checkQueueForEvents(){
        // console.debug("checkQueueForEvents");
        // console.log("Queuesize b4: "+this.userActionEvents.length); 
         //check the queue every 5 mins and if the queue has elements then refresh the session expiration time and empty the queue
         if(this.userActionEvents && this.userActionEvents.length != 0){
             this.refreshSessionAndQueue();
         }
        
    }

    public refreshSessionAndQueue(){
        // if queue has events then call 
        this.callrefereshSession();
        //after the session has been refreshed then the queue is emptied
        this.userActionEvents = [];
    }

    public callrefereshSession() {
        this.sessionExpiredComponent.next();
      }

    ngOnDestroy(){
        this.queuePollSubscription.unsubscribe();
    }
}
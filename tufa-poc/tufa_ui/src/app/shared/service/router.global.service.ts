import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras, NavigationStart, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { Subject } from 'rxjs';
import { pairwise } from '../../../../node_modules/rxjs/operators';


@Injectable()
export class RouterGlobalService {


    private navigateStartSource = new Subject<NavigationStart>();
    private navigateEndSource = new Subject<NavigationEnd>();


    public navigateStartSource$ = this.navigateStartSource.asObservable();
    public navigateEndSource$ = this.navigateEndSource.asObservable();

    public triggerRouteChanged(routeEvents: any []) {
        this.navigateEndSource.next(routeEvents[0]);
        this.navigateStartSource.next(routeEvents[1]);
    }

    routerSubscription: Subscription;

    private previousUrl: string = "";
    private currentUrl: string = "";

    constructor(private router: Router, ) {
        // this.routerSubscription = this.router.events.pairwise().subscribe(
        //     event => {
        //         if (event[0] instanceof NavigationEnd && event[1] instanceof NavigationStart) {
        //             this.previousUrl = event[0].url;
        //             this.currentUrl = event[1].url;
        //             this.triggerRouteChanged(event);
        //         }
        //     }
        // )
        router.events.pipe(pairwise()).subscribe(
            events => {
                if (events[0] instanceof NavigationEnd && events[1] instanceof NavigationStart) {
                    this.previousUrl = (<NavigationEnd> events[0]).url;
                    this.currentUrl = (<NavigationStart> events[1]).url;
                    this.triggerRouteChanged(events);
                }
                window.scrollTo(0, 0);
            }
        );
    }

    public getCurrentUrl(){
        return this.currentUrl;
    }

    public getPreviousUrl(){
        return this.previousUrl;
    }

}
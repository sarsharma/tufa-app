import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";
import { TobaccoClass } from "../../model/tobacco.class.model";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class TobaccoClassService {
    constructor(private http: HttpClient) { }

    getTobaccoTypes(): Observable<TobaccoClass[]> {
        return this.http.get<TobaccoClass[]>(environment.apiurl + '/api/v1/tobacco', httpOptions);
    }
}
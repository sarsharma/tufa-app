/*
@author : Deloitte

this is Component for shared module.
*/
import { CommonModule, CurrencyPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AgGridModule } from 'ag-grid-angular';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { TextMaskModule } from 'angular2-text-mask';
import { NgBusyModule } from 'ng-busy';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CustomFormsModule } from 'ng2-validation';
import { AccordionModule, AlertModule, BsModalRef, ButtonsModule, ModalModule, ProgressbarModule, TabsModule, TooltipModule } from 'ngx-bootstrap';
import { Ng2Webstorage } from 'ngx-webstorage';
import { AnnualTrueUpComparisonEventService } from './../recon/annual/services/at-compare.event.service';
import { LoopIndexPipe } from './../shared/custompipes/loopindex.pipe';
import { CheckBoxCellRenderer } from './ag-grid-cell-renderers/check-box-cell-renderer.component';
import { CircleIconComponent } from './ag-grid-cell-renderers/circle-icon.component';
import { CurrencyRenderer } from './ag-grid-cell-renderers/currency-renderer.component';
import { IconCellRenderer } from './ag-grid-cell-renderers/icon-cell-renderer.component';
import { InfoIconRenderer } from './ag-grid-cell-renderers/info-icon-renderer.component';
import { RouterLinkCellRenderer } from './ag-grid-cell-renderers/routerlink-cell-renderer.component';
import { CustomTooltip } from './ag-grid-cell-renderers/tool-tip-cell-renderer.component';
import { CommentsPanel } from './components/comments-panel.component';
import { ProgressModalComponent } from './components/progress-modal/progress-modal.component';
import { ToggleButton } from './components/toggle-button-tufa.component';
import { ConfirmPopupPh } from './confirmpopup/confirm-popup.ph.component';
import { InfoBoxPopup } from './confirmpopup/infobox.component';
import { AbsoluteValuePipe } from './custompipes/absolute-value.pipe';
import { CompanyStatusPipe } from './custompipes/company-status.pipe';
import { TufaCurrencyPipe } from './custompipes/custom.currency.pipe';
import { DateTimePipe } from './custompipes/date-time.pipe';
import { DatePipe } from './custompipes/date.pipe';
import { EINPipe } from './custompipes/ein-number.pipe';
import { ExcelPipe } from './custompipes/excel.pipe';
import { InactiveDatePipe } from './custompipes/inactive-datepipe';
import { MinusToParensPipe } from './custompipes/minus.parentheses.pipe';
import { NAPipe } from './custompipes/na.pipe';
import { PermitNumberPipe } from './custompipes/permit-number.pipe';
import { PhoneNumberPipe } from './custompipes/phone-number.pipe';
import { PropFilterPipe } from './custompipes/prop-filter.pipe';
import { QuarterMonthsPipe } from './custompipes/quarter-months.pipe';
import { RemoveHyphen } from './custompipes/removehyphen.pipe';
import { ReportStatusPipe } from './custompipes/report-status.pipe';
import { SearchPipe } from './custompipes/search.pipe';
import { TitleCase } from './custompipes/titlecase.pipe';
import { CustomValidatorDirective } from './customvalidators/custom.validator.directive';
import { NgTableFilteringDirective } from './datatable/datatable.filtering.directive';
import { DataTableModule } from './datatable/DataTableModule';
import { NKDatetimeModule } from './datetime/ng2-datetime-module';
import { CustomAnchorClickDirective } from './globalDirectives/anchorClick';
import { ChecklistDirective } from './globalDirectives/checklist';
import { CustomPercentDirective } from './globalDirectives/custom.percent';
import { ExternalUrlDirective } from './globalDirectives/externalurl.directive';
import { FocusDirective } from './globalDirectives/focus.directive';
import { FocusOutDirective } from './globalDirectives/focusOut.directive';
import { CustomNumericsDirective } from './globalDirectives/numerics';
import { LiveTileDirective } from './globalDirectives/tile.directive';
import { UppercaseDirective } from './globalDirectives/uppercase.directive';
import { TobaccoClassProvider } from './providers/tobacco-class.provider';
import { CommentsService } from './service/comments.service';
import { DialogService } from './service/dialog.service';
import { DownloadSerivce } from './service/download.service';
import { ErrorHandlingService } from './service/error.handling.service';
import { FormsCalculationService } from './service/forms.calculation.service';
import { IngestionWorkFlowService } from './service/ingestion-workflow.service';
import { TobaccoClassService } from './service/tobacco-class.service';
import { CustomIntegerDirective } from './util/custom-integer';
import { CustomCurrencyFormatterDirective } from './util/custom.currency.formatter';
import { CustomCurrencyPipe } from './util/custom.currency.pipe';
import { CustomDecimalFormatterDirective } from './util/custom.decimal.formatter';
import { CustomNumberFormatterDirective } from './util/custom.number.formatter';
import { CustomNumberPipe } from './util/custom.number.pipe';
import { CustomNumberVaidatorDirective } from './util/custom.number.validator';








@NgModule({
  imports: [CommonModule, FormsModule, TabsModule, AccordionModule.forRoot(), ButtonsModule, TooltipModule, TextMaskModule, CustomFormsModule, AlertModule.forRoot(),
    Ng2Webstorage, NgBusyModule, NKDatetimeModule, PdfViewerModule,
    ConfirmationPopoverModule.forRoot({
      focusButton: 'confirm'
    }),
    TooltipModule.forRoot(),
    TabsModule.forRoot(),
    ProgressbarModule.forRoot(),
    ModalModule.forRoot(),
    AgGridModule.withComponents([CheckBoxCellRenderer, CustomTooltip, RouterLinkCellRenderer, IconCellRenderer, CurrencyRenderer, InfoIconRenderer]), RouterModule],
  declarations: [PermitNumberPipe, EINPipe, ExcelPipe, InactiveDatePipe, PhoneNumberPipe, ReportStatusPipe, CompanyStatusPipe, QuarterMonthsPipe,
    TitleCase, FocusDirective, CommentsPanel, DatePipe, CustomNumericsDirective, ConfirmPopupPh, CustomDecimalFormatterDirective,
    CustomNumberFormatterDirective, CustomNumberPipe, AbsoluteValuePipe, CustomNumberVaidatorDirective, CustomPercentDirective,
    CustomIntegerDirective, RemoveHyphen, CustomAnchorClickDirective, FocusOutDirective, LoopIndexPipe, TufaCurrencyPipe, MinusToParensPipe,
    NgTableFilteringDirective, ToggleButton, CustomCurrencyFormatterDirective, CustomCurrencyPipe, ExternalUrlDirective,
    ChecklistDirective, CustomValidatorDirective, LiveTileDirective, InfoBoxPopup, UppercaseDirective, SearchPipe, PropFilterPipe, ProgressModalComponent,
    CheckBoxCellRenderer, CustomTooltip, RouterLinkCellRenderer, IconCellRenderer, CircleIconComponent, NAPipe, DateTimePipe, CurrencyRenderer, InfoIconRenderer],
  exports: [CommonModule, DataTableModule, FormsModule, TabsModule, AccordionModule, CommentsPanel, AlertModule, PdfViewerModule,
    ButtonsModule, TooltipModule, NKDatetimeModule, TextMaskModule, CustomFormsModule, InactiveDatePipe, PermitNumberPipe, ExcelPipe, EINPipe, QuarterMonthsPipe,
    ConfirmationPopoverModule, PhoneNumberPipe, NgBusyModule, ReportStatusPipe, CompanyStatusPipe, TitleCase, RemoveHyphen,
    FocusDirective, DatePipe, DateTimePipe, AbsoluteValuePipe, CustomNumericsDirective, ConfirmPopupPh, CustomDecimalFormatterDirective, CustomNumberFormatterDirective,
    CustomNumberPipe, CustomNumberVaidatorDirective, CustomIntegerDirective, CustomPercentDirective, CustomAnchorClickDirective,
    FocusOutDirective, LoopIndexPipe, TufaCurrencyPipe, MinusToParensPipe, SearchPipe, PropFilterPipe, NAPipe, NgTableFilteringDirective, ToggleButton, CustomCurrencyFormatterDirective, UppercaseDirective,
    CustomCurrencyPipe, ExternalUrlDirective, ChecklistDirective, CustomValidatorDirective, LiveTileDirective, TooltipModule, TabsModule, ProgressbarModule, ProgressModalComponent, ModalModule, AgGridModule],
  providers: [ErrorHandlingService, FormsCalculationService, CommentsService, CurrencyPipe, AnnualTrueUpComparisonEventService, DownloadSerivce, DialogService, BsModalRef, TufaCurrencyPipe, MinusToParensPipe, IngestionWorkFlowService, TobaccoClassProvider, TobaccoClassService],
  // entryComponents: [ AlertModal ]
})

// https://angular.io/docs/ts/latest/guide/ngmodule.html#!#shared-module
export class SharedModule { }

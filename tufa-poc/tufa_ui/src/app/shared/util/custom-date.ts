import * as moment from 'moment';
import { isNil } from 'ramda';

export class CustomDate {

    public static stringStandardDate(dateString: string): string {
        let returnString = null;
        if(!isNil(dateString)) {
            let utcDate = (new Date(dateString)).toUTCString();
            let m = moment(utcDate, 'ddd, DD MMM YYYY');
            returnString = m.format("MM/DD/YYYY");
        }
        return returnString;
    }

    public static dateStandardDate(thisDate: Date) : string {
        let returnString = null;
        if(!isNil(thisDate)) {
            let utcDate = thisDate.toUTCString();
            let m = moment(utcDate, 'ddd, DD MMM YYYY');
            returnString = m.format("MM/DD/YYYY");
        }
        return returnString;
    }

    public static toDate(val:string):Date{
        if (!isNil(val)) {
          return moment(val).toDate();
        }
        return null;
    }

    public static getAge(birthDay: string): number {
        return moment().diff(birthDay, 'years');
    }    

}


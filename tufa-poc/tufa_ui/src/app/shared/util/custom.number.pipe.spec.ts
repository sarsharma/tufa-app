import { CustomNumberPipe } from './custom.number.pipe';

describe('Pipe: CustomNumberPipe', () => {
  let pipe = new CustomNumberPipe();

  //specs
  it('test custom number pipe', () => {
    expect(pipe.transform('2345.45466546', 2)).toEqual('2,345.45');
    expect(pipe.transform('2345.45466546', 0)).toEqual('2,345');
    expect(pipe.transform(2345, 0)).toEqual('2,345');
    expect(pipe.transform('2345345.45466546', 0)).toEqual('2,345,345');
  });
})
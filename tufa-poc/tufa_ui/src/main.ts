import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import {LicenseManager} from "ag-grid-enterprise";

LicenseManager.setLicenseKey("SHI_International_Corp_on_behalf_of_Office_of_Management,_Center_for_Tobacco_Products_(TRLM)_Next_Generation_(NG)_version_Single_Application_4_Devs__4_December_2020_[v2]_MTYwNzA0MDAwMDAwMA==d1b68b09315a8dd6778d4fb3ce03ef01");

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));

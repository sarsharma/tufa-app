/**
 *
 */
package gov.hhs.fda.ctp.web.addressmgmt;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.biz.addressmgmt.AddressMgmtBizComp;
import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.PermitMfgReport;
import gov.hhs.fda.ctp.common.beans.PermitPeriod;
import gov.hhs.fda.ctp.persistence.model.CountryEntity;

/**
 * The Class AddressMgmtRestController.
 *
 * @author akari This controller is responsible for CRUD operations for Company
 *         Address
 */
//@CrossOrigin
@RestController
@RequestMapping("/api/v1/companies/address")
public class AddressMgmtRestController {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(AddressMgmtRestController.class);

	/** The address mgmt biz comp. */
	@Autowired
	private AddressMgmtBizComp addressMgmtBizComp;

	/**
	 * Instantiates a new address mgmt rest controller.
	 *
	 * @param addressMgmtBizComp the address mgmt biz comp
	 */
	public AddressMgmtRestController(AddressMgmtBizComp addressMgmtBizComp) {
		this.addressMgmtBizComp = addressMgmtBizComp;
	}

	/**
	 * Update address.
	 * Unit Test: testsaveOrUpdate
	 * @param model the model
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Address> saveOrUpdate(@Valid @RequestBody Address model) {
		Address retmodel = addressMgmtBizComp.saveOrUpdate(model);
		return new ResponseEntity<>(retmodel, HttpStatus.OK);
	}
	
	
	/**
	 * Update address.
	 * Unit Test: testsaveOrUpdate
	 * @param model the model
	 * @return the response entity
	 */
    @RequestMapping(value = "/copyaddresstotufa", method=RequestMethod.PUT, produces = { "application/json" })
	public ResponseEntity<List<Address>> saveOrUpdateMultiple(@Valid @RequestBody List<Address> model) {
		List<Address> retmodel = addressMgmtBizComp.saveOrUpdateMultiple(model);
		return new ResponseEntity<>(retmodel, HttpStatus.OK);
	}
    /**
	 * Update Primary address.
	 * 
	 * @param model the model
	 * @return the response entity
	 */
    @RequestMapping(value = "/setPrimaryAddress/{updatePrimToAltnFlag}/{setAltAddress}", method=RequestMethod.PUT, produces = { "application/json" })
	public ResponseEntity<Address> saveOrUpdatePrimaryAddress(@Valid @RequestBody Address model, @PathVariable("updatePrimToAltnFlag") String updatePrimToAltnFlag, @PathVariable("setAltAddress") String setAltAddress) {
    	Address retmodel = addressMgmtBizComp.saveOrUpdatePrimaryAddress(model, updatePrimToAltnFlag, setAltAddress);
		return new ResponseEntity<>(retmodel, HttpStatus.OK);
	}
	/**
	 * Read all CountryCodes.
	 * Unit Test: testGetCntryCd
	 * @return the response entity
	 */
	@RequestMapping(value = "cntryCd", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<?> readCntryCd() {
		List<String> countryCodes = addressMgmtBizComp.getCountryCd();
		return ResponseEntity.ok(countryCodes);
	}

	/**
	 * Read all Country Names.
	 * Unit Test: testGetCountryNm
	 * @return the response entity
	 */
	@RequestMapping(value = "cntryNm", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<?> readCntryNm() {
		List<String> countryNames = addressMgmtBizComp.getCountryNm();
		Collections.sort(countryNames);
		return ResponseEntity.ok(countryNames);
	}

	/**
	 * Read all StateCodes.
	 * Unit Test: testGetStateCd
	 * @return the response entity
	 */
	@RequestMapping(value = "stateCd", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<?> readStateCd() {
		List<String> stateCodes = addressMgmtBizComp.getStateCd();
		return ResponseEntity.ok(stateCodes);
	}

	/**
	 * Read all State Names.
	 * Unit Test: testGetStateNm
	 * @return the response entity
	 */
	@RequestMapping(value = "stateNm", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<?> readStateNm() {
		List<String> stateNames = addressMgmtBizComp.getStateNm();
		Collections.sort(stateNames);
		return ResponseEntity.ok(stateNames);
	}

	/**
	 * Read all countries as objects.
	 * Unit Test: testGetCountries
	 * @return the countries
	 */
    @RequestMapping(value = "/countries", method=RequestMethod.GET, produces = { "application/json" })
    public ResponseEntity<?> getCountries() {
        Set<CountryEntity> countries = addressMgmtBizComp.getCountries();
        return ResponseEntity.ok(countries);
    }

	/**
	 * get report addresses.
	 * Unit Test: testGetReportAddresses
	 * @param companyId
	 * @param permitId
	 * @param periodId
	 * @return list of report address entities
	 */
	@RequestMapping(value = "{companyId}/{permitId}/{periodId}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<?> getReportAddresses(@PathVariable("companyId") String companyId, @PathVariable("permitId") int permitId,
			@PathVariable("periodId") int periodId) {
		List<Address> addresses = addressMgmtBizComp.getReportAddresses(companyId, permitId, periodId);
		return ResponseEntity.ok(addresses);
	}

	/**
	 * Delete Contact.
	 *
	 * @param id the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{companyId}/{AddressType}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteAddress(@PathVariable("companyId") long companyId, @PathVariable("AddressType") String addressType) {
		String result = addressMgmtBizComp.delete(companyId, addressType);
		return new ResponseEntity<>(result , HttpStatus.OK);
	}
}

package gov.hhs.fda.ctp.web.authentication;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.web.config.security.UserTufa;

/**
 * REST Controller for returning authentication details to the end client which do not pose a
 * risk to security for example user roles, resources (urls) that user can access.
 *
 * @author sarsharma
 *
 */
@RestController
@RequestMapping("/api")
public class LoginRestController {

	/**
	 * After User has been authenticated/authorized Authentication object gets
	 * stored in the SecurityContextHolder and which is then returned to the client.
	 *
	 * @return the response entity
	 */
	@RequestMapping(value="/authenticate",method = RequestMethod.POST)
	public  ResponseEntity<?> user() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserTufa user = (UserTufa)auth.getPrincipal();
		return ResponseEntity.ok(user.getRoles());

	 }

	 /**
	  * Do not remove the following code snippet.
	  *
	  * The following is an example code which demonstrates how to use JSON Web Tokens (JWT).
	  * JSON Web Token is an open standard (RFC 7519) that defines a compact and
	  * self-contained way for securely transmitting information between parties as a
	  * JSON object. This information can be verified and trusted because it is digitally
	  * signed. JWTs can be signed using a secret (with the HMAC algorithm)
	  * or a public/private key pair using RSA.
	  *
	  *
	  * @Value("${jwt.header}")
    	private String tokenHeader;

    	@Autowired
    	private AuthenticationManager authenticationManagerTufa;

    	@Autowired
    	private JwtTokenUtil jwtTokenUtil;

    	@Autowired
    	private UserDetailsService userDetailsService;

	   	@RequestMapping(value="/authenticate",method = RequestMethod.POST)
	 	public ResponseEntity<?> createAuthenticationToken(@RequestBody User authenticationRequest) throws AuthenticationException {

	        // Perform the security
	        final Authentication authentication = authenticationManagerTufa.authenticate(
	                new UsernamePasswordAuthenticationToken(
	                        authenticationRequest.getEmail(),
	                        authenticationRequest.getPassword()
	                )
	        );
	        SecurityContextHolder.getContext().setAuthentication(authentication);

	        // Reload password post-security so we can generate token
	        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());
	        final String token = jwtTokenUtil.generateToken(userDetails);

	        // Return the token
	        return ResponseEntity.ok(new JwtAuthenticationResponse(token));
	    }

	    @RequestMapping(value = "/authenticate/refresh", method = RequestMethod.GET)
	    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
	        String token = request.getHeader(tokenHeader);
	        String username = jwtTokenUtil.getUsernameFromToken(token);
	        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);

	        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
	            String refreshedToken = jwtTokenUtil.refreshToken(token);
	            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
	        } else {
	            return ResponseEntity.badRequest().body(null);
	        }
	    }

	    *
	    *
	    *
	    */

}

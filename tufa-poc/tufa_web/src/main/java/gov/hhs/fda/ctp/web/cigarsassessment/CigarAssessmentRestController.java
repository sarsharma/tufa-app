package gov.hhs.fda.ctp.web.cigarsassessment;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp;
import gov.hhs.fda.ctp.common.beans.Assessment;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.CigarAssessment;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentFilter;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentRptDetail;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity;

@RestController
@RequestMapping("/api/cigars/assessment")
public class CigarAssessmentRestController {

	@Autowired
	private AssessmentBizComp assessmentBizComp;


	public CigarAssessmentRestController(AssessmentBizComp assess) {
		this.assessmentBizComp = assess;
	}

	/**
	 * Gets the cigars assessments.
	 *
	 * @param filters the filters
	 * @return the cigars assessments
	 */
	@RequestMapping(value="/list", method=RequestMethod.PUT, produces = {"application/json"})
	public ResponseEntity<CigarAssessmentFilter> getCigarsAssessments(@RequestBody CigarAssessmentFilter filters) {
		List<CigarAssessment> resultSet = this.assessmentBizComp.getCigarAssessments(filters);
		filters.setResults(resultSet);
		return new ResponseEntity<>(filters, HttpStatus.OK);
	}


	/**
	 * Gets the cigars assessments report details.
	 *
	 * @param fiscalYr the fiscal yr
	 * @return the cigars assessments report details
	 */
	@RequestMapping(value="/report/{fiscalYr}", method=RequestMethod.GET, produces = {"application/json"})
	public ResponseEntity<?> getCigarsAssessmentsReportDetails(@PathVariable("fiscalYr") long fiscalYr) {
		List<CigarAssessmentRptDetail> rptDetails = this.assessmentBizComp.getCigarAssessmentsReportDetail(fiscalYr);
		return new ResponseEntity<>(rptDetails, HttpStatus.OK);
	}

	/**
	 * Gets the cigars assessments recon details.
	 *
	 * @param fiscalYr the fiscal yr
	 * @return the cigars assessments recon details
	 */
	@RequestMapping(value="/recon/{fiscalYr}", method=RequestMethod.GET, produces = {"application/json"})
	public ResponseEntity<?> getCigarsAssessmentsReconDetails(@PathVariable("fiscalYr") long fiscalYr) {
		List<CigarAssessmentRptDetail> rptDetails = this.assessmentBizComp.getCigarAssessmentsReportDetail(fiscalYr);
		return new ResponseEntity<>(rptDetails, HttpStatus.OK);
	}

	/**
	 * Save doc.
	 *
	 * @param supportDocument the support document
	 * @param assessId the assess id
	 * @return the response entity
	 */
	@RequestMapping(value="/docs/{assessId}", method=RequestMethod.PUT, produces = {"application/json"})
	public ResponseEntity<?> saveDoc(@RequestBody SupportingDocumentEntity supportDocument,
			@PathVariable("assessId") long assessId) {
		assessmentBizComp.saveSupportDocument(supportDocument);
		return new ResponseEntity<>(JSONObject.quote("Document Saved"), HttpStatus.OK);
	}

	/**
	 * Gets the supporting docs.
	 *
	 * @param assessId the assess id
	 * @return the supporting docs
	 */
	@RequestMapping(value="/{fiscalYr}/docs", method=RequestMethod.GET, produces = {"application/json"})
	public ResponseEntity<?> getSupportingDocs(@PathVariable("fiscalYr") long fiscalYr) {
		List<SupportingDocument> docs = assessmentBizComp.getCigarSupportDocs(fiscalYr);
		return new ResponseEntity<>(docs, HttpStatus.OK);
	}

	/**
	 * Delete document.
	 *
	 * @param asmtdocId the asmtdoc id
	 * @return the response entity
	 */
	@RequestMapping(value="/docs/{asmtdocId}", method=RequestMethod.DELETE, produces = {"application/json"})
	public ResponseEntity<?> deleteDocument(@PathVariable("asmtdocId") long asmtdocId) {
		assessmentBizComp.deleteByDocId(asmtdocId);
		return new ResponseEntity<>(JSONObject.quote("Removed document"), HttpStatus.OK);
	}

	/**
	 * Gets the document.
	 *
	 * @param asmtdocId the asmtdoc id
	 * @return the document
	 */
	@RequestMapping(value="/docs/{asmtdocId}", method=RequestMethod.GET, produces = {"application/json"})
	public ResponseEntity<?> getDocument(@PathVariable("asmtdocId") long asmtdocId) {
		SupportingDocumentEntity doc = assessmentBizComp.getDocByDocId(asmtdocId);
		return new ResponseEntity<>(doc, HttpStatus.OK);
	}

	/**
	 * Get the recon export data.
	 *
	 * @param year the year
	 * @return the recon export
	 */
	@RequestMapping(value = "/export/fiscalyear/{year}", method = RequestMethod.GET)
	public ResponseEntity<?> getReconExport(@PathVariable("year") int year) {
		AssessmentExport export = assessmentBizComp.getCigarReconExport(year);
		return new ResponseEntity<>(export, HttpStatus.OK);
	}
}

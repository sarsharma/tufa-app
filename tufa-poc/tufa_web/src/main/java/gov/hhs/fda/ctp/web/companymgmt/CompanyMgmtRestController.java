/**
 *
 */
package gov.hhs.fda.ctp.web.companymgmt;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.common.beans.AffectedAssessmentbean;
import gov.hhs.fda.ctp.common.beans.Attachment;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.ExcludePermitBean;
import gov.hhs.fda.ctp.common.beans.ExcludePermitDTO;
import gov.hhs.fda.ctp.common.beans.NewCompaniesExport;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitExcludeComment;
import gov.hhs.fda.ctp.common.exception.ExcludePermitException;
import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.common.pagination.PaginationAttributes;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.PermitCommentAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitEntity;

/**
 * The Class CompanyMgmtRestController.
 *
 * @author tgunter
 */
// @CrossOrigin(origins = "http://localhost:9000")
@RestController
@RequestMapping("/api/v1/companies")
public class CompanyMgmtRestController {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(CompanyMgmtRestController.class);

	/** The company mgmt biz comp. */
	@Autowired
	private CompanyMgmtBizComp companyMgmtBizComp;
	
	@Autowired
	private PaginationAttributes paginationAttributes;
	
	@Autowired
	private SecurityContextUtil securityContextUtil;

	/**
	 * Instantiates a new company mgmt rest controller.
	 *
	 * @param companyMgmtBizComp
	 *            the company mgmt biz comp
	 */
	public CompanyMgmtRestController(CompanyMgmtBizComp companyMgmtBizComp) {
		this.companyMgmtBizComp = companyMgmtBizComp;
	}

	/**
	 * Create company.
	 *
	 * @param model
	 *            the model
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Company> create(@Valid @RequestBody Company model) {
		/* TBD Mapping this by hand until dozer is resolved */
		logger.debug("CREATE_COMPANY: Create company with name = {}, ein = {}", model.getLegalName(),
				model.getEinNumber());
		logger.debug("CREATE_PERMIT: Create permit with permitNumber = {}, activeDate = {}", model.getPermitNumber(),
				model.getActiveDate());

		/*
		 * convert the two permit fields to a permit object, add it to set for
		 * cascade save
		 */
		Permit newPermit = new Permit();
		newPermit.setIssueDt(model.getActiveDate());
		
		if(model.getinactiveDate() != null)
		{
			newPermit.setCloseDt(model.getinactiveDate());
			newPermit.setttbendDt(model.getinactiveDate());
		}
		
		
		newPermit.setttbstartDt(model.getActiveDate());
		
		newPermit.setPermitNum(model.getPermitNumber());
		newPermit.setPermitTypeCd(model.getPermitTypeCd());
		if(model.getPermitStatusTypeCd() == null)
		{newPermit.setPermitStatusTypeCd("ACTV");}
		else
		newPermit.setPermitStatusTypeCd(model.getPermitStatusTypeCd());
		
		Set<Permit> permits = new HashSet<>();
		permits.add(newPermit);
		model.setPermitSet(permits);

		Company retmodel = companyMgmtBizComp.createCompany(model);
		return new ResponseEntity<>(retmodel, HttpStatus.CREATED);
	}

	/**
	 * Get all company permit histories, filtered by criteria.
	 *
	 * @param criteria
	 *            the criteria
	 * @return the companies
	 */
	@RequestMapping(value = "/search", method = RequestMethod.PUT)
	public ResponseEntity<CompanySearchCriteria> getCompanies(@RequestBody CompanySearchCriteria criteria) {
		CompanySearchCriteria retcriteria = companyMgmtBizComp.getCompanies(criteria);
		return new ResponseEntity<>(retcriteria, HttpStatus.OK);
	}

	/**
	 * Read company.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<Company> read(@PathVariable long id) {
		Company company = companyMgmtBizComp.getCompany(id);
		return new ResponseEntity<>(company, HttpStatus.OK);
	}

	/**
	 * Read company.
	 *
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@RequestMapping(value = "ein/{ein}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<Company> readByEin(@PathVariable String ein) {
		Company company = companyMgmtBizComp.getCompanybyEIN(ein);
		return new ResponseEntity<>(company, HttpStatus.OK);
	}

	/**
	 * Update company.
	 *
	 * @param model
	 *            the model
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Company> update(@Valid @RequestBody Company model) {
		Company retmodel = companyMgmtBizComp.updateCompany(model);
		return new ResponseEntity<>(retmodel, HttpStatus.OK);
	}

	/**
	 * Method to create the permit. CompanyId is needed to associate the newly
	 * created Permit.
	 *
	 * @param permit
	 *            the permit
	 * @param companyId
	 *            the company id
	 * @return the response entity
	 */
	@RequestMapping(value = "{id}/permits", method = RequestMethod.POST)
	public ResponseEntity<Company> createPermit(@Valid @RequestBody Permit permit, @PathVariable("id") long companyId) {
		PermitEntity permitRef = companyMgmtBizComp.getPermitById(companyId,
				permit.getPermitNum().replaceAll("[^A-Za-z]+", "").toUpperCase());
		if (permitRef != null && !permit.getDuplicateFlag()) {
			throw new TufaException("Warning: Active Permit(s) with the same State and "
					+ "Type already exist for this Company. Do you still wish to save the Permit?");
		}

		permit.setCompanyId(companyId);
		Company company = companyMgmtBizComp.createPermit(permit);
		return new ResponseEntity<>(company, HttpStatus.CREATED);
	}

	/**
	 * GET Method to get Permit. Company ID and Permit ID is provided in the
	 * URI.
	 *
	 * @param permitId
	 *            the permit id
	 * @return the permit
	 */
	@RequestMapping(value = "{id}/permits/{permitId}", method = RequestMethod.GET)
	public ResponseEntity<Permit> getPermit(@PathVariable("permitId") long permitId) {
		Permit permit = companyMgmtBizComp.getPermit(permitId);
		return ResponseEntity.ok(permit);
	}

	/**
	 * PUT Method to update the Permit. The URI contains the Company ID and the
	 * Permit ID.
	 *
	 * @param permit
	 *            the permit
	 * @param permitId
	 *            the permit id
	 * @param companyId
	 *            the company id
	 * @return the response entity
	 */
	@RequestMapping(value = "{id}/permits/{permitId}", method = RequestMethod.PUT)
	public ResponseEntity<Company> updatePermit(@Valid @RequestBody Permit permit,
			@PathVariable("permitId") long permitId, @PathVariable("id") long companyId) {
		permit.setPermitId(permitId);
		permit.setCompanyId(companyId);
		if (Constants.CLOSED_CODE.equals(permit.getPermitStatusTypeCd())) {
			String closedDate = permit.getCloseDt();
			if (closedDate == null || closedDate.isEmpty()) {
				throw new TufaException("Closed permits must have a closed date.");
			}
		}
		Company company = companyMgmtBizComp.updatePermit(permit);
		return new ResponseEntity<>(company, HttpStatus.OK);
	}

	/**
	 * GET Method to get the Permits based on the status of the permit i.e.
	 * active/closed. The URI contains the Company ID and the status is sent as
	 * a URL query parameter.
	 *
	 * @param permitStatus
	 *            the permit status
	 * @param companyId
	 *            the company id
	 * @return the permits
	 */
	@RequestMapping(value = "{id}/permits", method = RequestMethod.GET)
	public ResponseEntity<List<Permit>> getPermits(@RequestParam("status") String permitStatus,
			@PathVariable("id") long companyId) {
		List<Permit> permits = companyMgmtBizComp.getPermits(permitStatus, companyId);
		return ResponseEntity.ok(permits);
	}

	/**
	 * GET Method to get Permit. Company ID and Permit ID is provided in the
	 * URI.
	 *
	 * @param companyId
	 *            the company id
	 * @return the company permit history
	 */
	@RequestMapping(value = "{companyId}/permits/history", method = RequestMethod.GET)
	public ResponseEntity<Company> getCompanyPermitHistory(@PathVariable("companyId") long companyId) {
		Company company = companyMgmtBizComp.getCompany(companyId);
		company = companyMgmtBizComp.getCompanyPermitHistory(company);
		return ResponseEntity.ok(company);
	}

	/**
	 * Save / update a comment for a company.
	 * 
	 * @param comment
	 * @param companyId
	 * @return
	 */
	@RequestMapping(value = "{id}/comment", method = RequestMethod.PUT)
	public ResponseEntity<String> updateCompanyComment(@RequestBody(required=false) String comment,
			@PathVariable("id") long companyId) {
		return ResponseEntity.ok(companyMgmtBizComp.updateCompanyComment(companyId, comment));
	}
	
	@RequestMapping(value = "impt/active/ein/{ein}", method = RequestMethod.GET)
	public ResponseEntity<Company> getActiveImptCompanyByEIN(@PathVariable("ein") String ein) {
		return ResponseEntity.ok(companyMgmtBizComp.getActvImptCompByEIN(ein));
		
	}
	@RequestMapping(value = "impt/associated/ein/{ein}/fiscalYear/{fiscalYear}/fiscalQtr/{fiscalQtr}/tobaccoClassNm/{tobaccoClassNm}", method = RequestMethod.GET)
	public ResponseEntity<String> getAssociatedCompanyByEIN(@PathVariable("fiscalYear") long fiscalYear ,@PathVariable("fiscalQtr") long fiscalQtr, @PathVariable("tobaccoClassNm") String tobaccoClassNm, @PathVariable("ein") String ein) {
		return ResponseEntity.ok(companyMgmtBizComp.getAssociatedCompanyByEIN(fiscalYear, fiscalQtr, tobaccoClassNm, ein));
	}
	
	@RequestMapping(value = "tracking", method = RequestMethod.PUT)
	public ResponseEntity<?> getCompanyTrackingInformation(@Valid @RequestBody CompanySearchCriteria criteria) {
		List<Company> results = companyMgmtBizComp.getCompanyTrackingInformation(criteria);
		Map<String, Object> resultsMap = new HashMap<>();
		Integer totalCnt = paginationAttributes.getItemsTotal();
		resultsMap.put("itemsTotal", totalCnt);
		resultsMap.put("results", results);
		return ResponseEntity.ok(resultsMap);
	}
	
	@RequestMapping(value = "tracking/update/", method = RequestMethod.POST)
	public ResponseEntity<?> updateCompanyTrackingInformation(@Valid @RequestBody List<Company> companies) {
		return ResponseEntity.ok(companyMgmtBizComp.updateCompanyTrackingInformation(companies));
	}
	

	/**
	 * 
	 * @return list of new companies
	 */
	@RequestMapping(value = "/export", method = RequestMethod.GET)
	public ResponseEntity<NewCompaniesExport> getCBPExport() {
		NewCompaniesExport export = companyMgmtBizComp.getNewCompaniesExport();
		return new ResponseEntity<>(export, HttpStatus.OK);
	}
	public PaginationAttributes getPaginationAttributes() {
		return paginationAttributes;
	}

	public void setPaginationAttributes(PaginationAttributes paginationAttributes) {
		this.paginationAttributes = paginationAttributes;
	}
	
	/**
     * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * this Endpoint responsible for saving exclusion details for the Permit
     * @param request
     * @return
     */
    @PostMapping(value="/excludepermit/{companyid}/{permitid}/{exclusiontype}")
    @ResponseBody
    public ResponseEntity excludeAllPermits(@PathVariable("companyid") long companyid,@PathVariable("permitid") long permitid,@PathVariable("exclusiontype") String exclusiontype,@RequestBody ExcludePermitDTO request) {
        //TODO:final call will tied with DB storedProcedure design
        boolean flag = companyMgmtBizComp.excludeAllPermits(companyid, permitid, exclusiontype, request.getExcludes(), request.getComment());
        if(!flag) {
            throw new ExcludePermitException("Selected Permit trying to exclude is not active in Exclusion Range");
        }
        return new ResponseEntity<>(JSONObject.quote("API called"),HttpStatus.OK);

    }

    /**
     * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * this Endpoint returns Details of the permit which was excluded for FY and and quarters from FY
     * @param permitid
     * @return
     */
    @GetMapping(value="/getexcludepermitdetails/{companyid}/{permitid}")
    public ResponseEntity<?> getExcludePermitDetails(@PathVariable("companyid") long companyid,@PathVariable("permitid") long permitid) {
        //TODO:final call will tied up with DB final design 
        List<ExcludePermitBean> list = companyMgmtBizComp.getExcludePermitDetails(permitid, companyid);
        return new ResponseEntity<>(list,HttpStatus.OK);
    }
    
    @GetMapping(value="/excludepermit/comment/{companyid}/{permitid}")
    public ResponseEntity<?> getExcludePermitComments(@PathVariable("companyid") long companyid,@PathVariable("permitid") long permitid) {
        //TODO:final call will tied up with DB final design 
        List<PermitExcludeComment> list = companyMgmtBizComp.getExcludePermitComments(permitid, companyid);
        return new ResponseEntity<>(list,HttpStatus.OK);
    }
    
    @PostMapping(value="/excludepermit/comment")
    public ResponseEntity<?> updateExludeComment(@Valid @RequestBody PermitExcludeComment comment) {
    	companyMgmtBizComp.updateExcludeCommentDetails(comment);
        return new ResponseEntity<>(JSONObject.quote("API called"),HttpStatus.OK);
    }
    /**
     * Get the list of attachment for commentId 
     * @param commentId
     * @return
     * @throws SQLException
     */
    @RequestMapping(value = "/excludepermit/comment/attachs/commentId/{commentId}", method = RequestMethod.GET)
	public ResponseEntity<?> getPermitAttachments(@PathVariable("commentId") long commentId) throws SQLException {
		List<PermitCommentAttachmentsEntity> attachs = companyMgmtBizComp.findCommentDocbyId(commentId);
		return new ResponseEntity<>(attachs, HttpStatus.OK);
	}
    /**
     * Download Attachment Id
     * @param docId
     * @return
     * @throws SQLException
     */
    @RequestMapping(value = "/excludepermit/comment/attachs/{id}", method = RequestMethod.GET)
	public ResponseEntity<Attachment> downloadPermitAttachment(@PathVariable("id") long docId) throws SQLException {
		Attachment attachment = companyMgmtBizComp.getPermitCommentAttachmentDownload(docId);
		return new ResponseEntity<>(attachment, HttpStatus.OK);
	}
    /** 
     * View document by Id
     * @param docId
     * @param response
     * @param request
     * @throws SQLException
     * @throws IOException
     */
	@RequestMapping(value = "/excludepermit/comment/view/attachs/{id}", method = RequestMethod.GET)
	public @ResponseBody void viewPermitAttachment(@PathVariable("id") long docId, HttpServletResponse response,
			HttpServletRequest request) throws SQLException, IOException {

		byte[] attachByteArry = companyMgmtBizComp.getPermitCommentAttachmentAsByteArray(docId);

		String pdfDataStr = new String(attachByteArry);
		String[] pdfDataArry = pdfDataStr.split("[:;,]");
		if (pdfDataArry.length == 4) {
			byte[] decodedFileCntByteArray = Base64.decode(pdfDataArry[3].getBytes());
			InputStream fileInStream = new ByteArrayInputStream(decodedFileCntByteArray);
			IOUtils.copy(fileInStream, response.getOutputStream());
			response.setContentType("application/pdf");
			response.setContentLength(attachByteArry.length);
			response.flushBuffer();
		} else {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
	}
	/**
	 * Uploading attachment 
	 * @param file
	 * @param metaData
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 */
	@RequestMapping(value = "/excludepermit/comment/attachs/", method = RequestMethod.POST)
	public ResponseEntity<String> addPermitAttachments(@RequestParam("file") MultipartFile file,
			@RequestParam("metaData") String metaData)
			throws JsonParseException, JsonMappingException, IOException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		PermitCommentAttachmentsEntity attachs = mapper.readValue(metaData, PermitCommentAttachmentsEntity.class);
		attachs.setCreatedDt(new Date());
		attachs.setCreatedBy((securityContextUtil.getPrincipal() != null)? securityContextUtil.getPrincipal().getUsername() : null);
		companyMgmtBizComp.addPermitCommentAttach(attachs, file);
		return new ResponseEntity<>(JSONObject.quote("Successfully Uploaded"), HttpStatus.OK);
	}
	/**
	 * Deleting attachment by docId
	 * @param docId
	 * @return
	 */
	@RequestMapping(value = "/excludepermit/comment/attachs/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deletePermitAttachment(@PathVariable("id") long docId) {
		companyMgmtBizComp.deleteDocById(docId);
		return new ResponseEntity<>(JSONObject.quote("Successfully Deleted"), HttpStatus.OK);
	}

    /**
     * CTPTUFA-4947 - Assessments: Show table under the Assessments page that lists all Permits marked as Excluded Pt2
     * @param companyid
     * @param permitid
     * @return
     */
    @GetMapping(value="/getaffectedassmtdata")
    public ResponseEntity<?> getAffectedAssessmentData() {
        List<AffectedAssessmentbean> list = companyMgmtBizComp.getAffectedAssessmentData();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
    
    /**
     * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * this will send warning message for affected assessment data for permit being excluded
     * @param companyid
     * @param fiscalyear
     * @param quarter
     * @param exclusiontype
     * @return
     */
    @GetMapping(value="/affectedassmt/warning/{permitid}/{companyid}/{fiscalyear}/{quarter}/{exclusiontype}")
    public ResponseEntity<?> getWarningForAffectedData(@PathVariable("companyid") long companyid,@PathVariable("fiscalyear") String fiscalyear,
            @PathVariable("quarter") String quarter,@PathVariable("exclusiontype") String exclusiontype,@PathVariable("permitid") long permitid) {
        boolean flag = companyMgmtBizComp.getAffectedAssmtWarning(permitid,companyid, fiscalyear, quarter, exclusiontype);
        return new ResponseEntity<>(flag,HttpStatus.OK);
    }
    
    /**
     * CTPTUFA-4964 - Permit History: Exclude Panel UI and Java Pt1
     * this will send warning message for COmpany having active permit for the period for which permit is being excluded
     * @param companyid
     * @param permitid
     * @param fiscalyear
     * @param quarter
     * @param exclusiontype
     * @return
     * @throws ParseException 
     */
    @GetMapping(value="/excludepermit/warning/{companyid}/{permitid}/{quarter}/{fiscalyear}/{exclusiontype}")
    public ResponseEntity<?> getWarningForPermitBeingExcluded(@PathVariable("companyid") long companyid,@PathVariable("permitid") long permitid,@PathVariable("fiscalyear") String fiscalyear,
            @PathVariable("quarter") String quarter,@PathVariable("exclusiontype") String exclusiontype) throws ParseException {
        boolean flag = companyMgmtBizComp.getWarningForPermitBeingExcluded(companyid, permitid, fiscalyear, quarter, exclusiontype);
        return new ResponseEntity<>(flag,HttpStatus.OK);
    }
}

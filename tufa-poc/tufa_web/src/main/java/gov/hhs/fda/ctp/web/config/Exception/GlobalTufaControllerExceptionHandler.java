package gov.hhs.fda.ctp.web.config.Exception;

import java.lang.reflect.Constructor;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.util.UrlPathHelper;

import gov.hhs.fda.ctp.common.config.ExceptionConfig;
import gov.hhs.fda.ctp.common.exception.AnnTrueupDeltaChangeException;
import gov.hhs.fda.ctp.common.exception.ExcludePermitException;
import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.web.exception.ErrorInfo;
import gov.hhs.fda.ctp.web.exception.UserNotFoundException;


/**
 * The Class GlobalTufaControllerExceptionHandler.
 */
@ControllerAdvice
public class GlobalTufaControllerExceptionHandler {

    /** The Constant logger. */
    private static final Logger logger = LoggerFactory.getLogger(GlobalTufaControllerExceptionHandler.class);

    /** The exception config. */
    /* should we auto-inject this instead? */
    @Autowired
    private ExceptionConfig exceptionConfig;

    /**
     * Handle unacceptable request.
     *
     * @param req the req
     * @param ex the ex
     * @return the error info
     */
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ExceptionHandler(TufaException.class)
    @ResponseBody ErrorInfo
    handleUnacceptableRequest(HttpServletRequest req, Exception ex) {
    	
    	logger.error("TufaException: {} {}", req.getRequestURL().toString(), ex.getLocalizedMessage());    	

		if(ex instanceof AnnTrueupDeltaChangeException){
			AnnTrueupDeltaChangeException annexp = (AnnTrueupDeltaChangeException) ex;
			return new ErrorInfo(req.getRequestURI(), annexp.getLocalizedMessage(),annexp.getData());
		}

        return new ErrorInfo(req.getRequestURI(), ex.getLocalizedMessage());
    }

	/**
	 * Handle error.
	 *
	 * @param req the req
	 * @param ex the ex
	 * @return the error info
	 */
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(UserNotFoundException.class)
	@ResponseBody ErrorInfo
    handleError(HttpServletRequest req, Exception ex) {
		logger.error("UserNotFoundException: {} {}", req.getRequestURL().toString(), ex.getLocalizedMessage());
		return new ErrorInfo(req.getRequestURL().toString(), ex.getLocalizedMessage());
    }

	/**
	 * Handler for any SQLExceptions.
	 *
	 * @param req the req
	 * @param ex the ex
	 * @return the error info
	 */
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler({SQLException.class,DataAccessException.class})
	@ResponseBody ErrorInfo
	handleDatabaseError(HttpServletRequest req, Exception ex) {

		if(ex.getCause()!= null && ex.getCause().getCause()!=null && ex.getCause().getCause() instanceof SQLSyntaxErrorException){
			logger.error("Database Error: {} {}", req.getRequestURL().toString(), " A sql syntax error has occurred. Please check the sql.");
			return new ErrorInfo(req.getRequestURL().toString(), "A sql syntax error has occurred. Please check the sql.");
		}

		logger.error("Database Error: {} {}", req.getRequestURL().toString(), ex.getLocalizedMessage());
		return new ErrorInfo(req.getRequestURL().toString(), ex.getLocalizedMessage());

	}
	/**
	 * Handle bad request.
	 *
	 * @param req the req
	 * @param ex the ex
	 * @return the error info
	 */
	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseBody ErrorInfo
	handleBadRequest(HttpServletRequest req, Exception ex) {
	    TufaException tufaException = null;
	    // get the uri that generated the exception
	    String uri = req.getRequestURI();
	    String path = new UrlPathHelper().getPathWithinApplication(req);
	    // look up the name of the new exception to create
	    String className = exceptionConfig.getClassNameForURI(path);
	    String errorMessage;
	    // create an instance of this exception
	    try {
    	    Class<?> exceptionClass = Class.forName(className);
    	    Constructor<?> ctor = exceptionClass.getConstructor(Throwable.class);
    	    tufaException = (TufaException)ctor.newInstance(new Object[] { ex });
    	    errorMessage = tufaException.getMessage();
    	    
    	    if(errorMessage.contains("EIN_UK")) {
    	    	errorMessage = "Employer Identification Number (EIN) already exists.";
    	    } else if (errorMessage.contains("PERMITNUM_UK")) {
    	    	errorMessage = "TTB Permit already exists.";
    	    }
	    }
	    catch(Exception e) {
	        // log this please
        	logger.error("error", e);
    	    errorMessage = tufaException.getMessage();
	    }
	    logger.error("Confilt: {} {}",req.getRequestURL().toString(), ex.getLocalizedMessage());
	    return new ErrorInfo(uri, errorMessage);
	}
	
	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseBody ErrorInfo
	handleConflict(HttpServletRequest req, ConstraintViolationException ex) {
	    TufaException tufaException = null;
	    // get the uri that generated the exception
	    String uri = req.getRequestURI();
	    String errorMessage = "";
	    if(ex.getConstraintName().contains("EIN_UK")) {
	    	errorMessage = "Employer Identification Number (EIN) already exists.";
	    } else if (ex.getConstraintName().contains("PERMITNUM_UK")) {
	    	errorMessage = "TTB Permit already exists.";
	    }
	    
	    logger.error("Confilt: {} {}",req.getRequestURL().toString(), ex.getLocalizedMessage());
	    return new ErrorInfo(uri, errorMessage);
	}
	
	/**
	 * Handle Authentication Exception.
	 *
	 * @param req the req
	 * @param ex the ex
	 * @return the error info
	 */
	@ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler({AuthenticationException.class, InternalAuthenticationServiceException.class})
	@ResponseBody ErrorInfo
    handleAuthenticationException(HttpServletRequest req, Exception ex) {
		logger.error("Authentication Exception: {} {}", req.getRequestURL().toString(), ex.getLocalizedMessage());
		return new ErrorInfo(req.getRequestURL().toString(), ex.getLocalizedMessage());
    }
	
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	@ExceptionHandler(ExcludePermitException.class)
	@ResponseBody ErrorInfo handleExcludePermitExcption(HttpServletRequest req, Exception ex) {
	    if(ex instanceof ExcludePermitException){
            ExcludePermitException exclException = (ExcludePermitException) ex;
            return new ErrorInfo(req.getRequestURI(), exclException.getLocalizedMessage());
        }

        return new ErrorInfo(req.getRequestURI(), ex.getLocalizedMessage());
	}
	public ExceptionConfig getExceptionConfig() {
		return exceptionConfig;
	}

	public void setExceptionConfig(ExceptionConfig exceptionConfig) {
		this.exceptionConfig = exceptionConfig;
	}
}

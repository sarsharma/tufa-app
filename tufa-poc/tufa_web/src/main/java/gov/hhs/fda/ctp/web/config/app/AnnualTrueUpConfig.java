package gov.hhs.fda.ctp.web.config.app;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerMapping;

import gov.hhs.fda.ctp.common.beans.IngestionWorkflowContext;
import gov.hhs.fda.ctp.common.ingestion.workflow.IngestionWFUserContext;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.common.util.ApplicationContextUtil;
import gov.hhs.fda.ctp.web.trueup.compare.ATCompare;
import gov.hhs.fda.ctp.web.trueup.compare.ATCompareComponentFactory;
import gov.hhs.fda.ctp.web.trueup.ingestion.ATFileIngestComponentFactory;
import gov.hhs.fda.ctp.web.trueup.ingestion.ATFileIngestion;

@Configuration
@ComponentScan(basePackages = "gov.hhs.fda.ctp")
public class AnnualTrueUpConfig {

	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private SecurityContextUtil securityContextUtil;
	
	@Bean
	public ServiceLocatorFactoryBean ATFileIngestServiceLocatorFactoryBean() {
		ServiceLocatorFactoryBean bean = new ServiceLocatorFactoryBean();
		bean.setServiceLocatorInterface(ATFileIngestComponentFactory.class);
		return bean;
	}

	@Bean
	public ServiceLocatorFactoryBean ATCompareServiceLocatorFactoryBean() {
		ServiceLocatorFactoryBean bean = new ServiceLocatorFactoryBean();
		bean.setServiceLocatorInterface(ATCompareComponentFactory.class);
		return bean;
	}
	
	@Bean
	public ATFileIngestComponentFactory atFileIngestorFactory() {
		return (ATFileIngestComponentFactory) ATFileIngestServiceLocatorFactoryBean().getObject();
	}
	
	@Bean
	public ATCompareComponentFactory atCompareFactory() {
		return (ATCompareComponentFactory) ATCompareServiceLocatorFactoryBean().getObject();
	}
	

	@Bean(name = "fileIngestor")	
	@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public ATFileIngestion fileIngestionComponent() {

		/*   
		 * Get current ingestion file context and based on it return the
		 * corresponding ingestion behavior.
		 */
		IngestionWorkflowContext ingestionWorkflowContext = this.getCurrentUserIngestionContext();
		
		String fileIngestionType = "";
		if (ingestionWorkflowContext.getContext().equalsIgnoreCase("detailOnly"))
			fileIngestionType = "ATCBPSingleFileIngestion";
		else
			fileIngestionType = "ATCBPDoubleFileIngestion";
		
		ATFileIngestion ingest = atFileIngestorFactory().getFileIngestionComponent(fileIngestionType);

		return ingest;
	}

	@Bean(name = "atCompare")	
	@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public ATCompare atCompareComponent() {

		/*
		 * Get current ingestion file context and based on it return the
		 * corresponding compare behavior.
		 */
		IngestionWorkflowContext ingestionWorkflowContext = this.getCurrentUserIngestionContext();
		
		String atCompareType = "";
		if (ingestionWorkflowContext.getContext().equalsIgnoreCase("detailOnly"))
			atCompareType = "ATCompareSingleFile";
		else
			atCompareType = "ATCompareDoubleFile";
		
		ATCompare compare = atCompareFactory().getCompareComponent(atCompareType);

		return compare;
	}

	private String getWorkflowKey() {
		HttpServletRequest request = getCurrentRequest();
		Map pathVariables = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		long fiscalYr = Long.parseLong((String) pathVariables.get("fiscalYear"));
		String username = securityContextUtil.getSessionUser();
		String key = username.concat(Long.toString(fiscalYr));
		return key;
	}

	private  IngestionWorkflowContext getCurrentUserIngestionContext(){
		String key = this.getWorkflowKey();
		IngestionWFUserContext ingestionWFUserContext = (IngestionWFUserContext)ApplicationContextUtil.getApplicationContext().getBean("ingestionWFUserContext");		 
		return ingestionWFUserContext.getUserContext(key);
		
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public SecurityContextUtil getSecurityContextUtil() {
		return securityContextUtil;
	}

	public void setSecurityContextUtil(SecurityContextUtil securityContextUtil) {
		this.securityContextUtil = securityContextUtil;
	}
	
	private HttpServletRequest getCurrentRequest(){
		
		HttpServletRequest request =null;
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
	    if (requestAttributes instanceof ServletRequestAttributes) {
	        request = ((ServletRequestAttributes)requestAttributes).getRequest();
	        return request ;
	    }
	    return request;
	}
}

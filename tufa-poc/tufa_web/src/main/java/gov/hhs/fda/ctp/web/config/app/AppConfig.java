package gov.hhs.fda.ctp.web.config.app;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = "gov.hhs.fda.ctp")
@PropertySource("classpath:/application.properties")
public class AppConfig {

}

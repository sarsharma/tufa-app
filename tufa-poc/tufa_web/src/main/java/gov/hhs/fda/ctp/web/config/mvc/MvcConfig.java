package gov.hhs.fda.ctp.web.config.mvc;

import java.util.List;

import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.common.pagination.PaginationAttributes;

/**
 * The Class MvcConfig.
 */
//@EnableWebMvc
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "gov.hhs.fda.ctp")
public class MvcConfig {
	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#configureDefaultServletHandling(org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer)
	 
	@Override
	public void configureDefaultServletHandling(final DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	 (non-Javadoc)
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#configureMessageConverters(java.util.List)
	 
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(jackson2HttpMessageConverter());
	}*/

	/**
	 * Jackson 2 http message converter.
	 *
	 * @return the mapping jackson 2 http message converter
	 */
	@Bean
	public MappingJackson2HttpMessageConverter jackson2HttpMessageConverter() {
		return new MappingJackson2HttpMessageConverter();
	}

    /**
     * Config dozer.
     *
     * @return the dozer bean mapper factory bean
     */
    @Bean(name = "mapperFactory")
    public DozerBeanMapperFactoryBean configDozer() {
        DozerBeanMapperFactoryBean mapperFactory = new DozerBeanMapperFactoryBean();
        Resource[] resources;
        try {
            resources = new PathMatchingResourcePatternResolver().getResources("classpath*:dozerBeanMapping.xml");
        }
        catch(Exception e) {
            throw new TufaException("Could not find dozerBeanMapping.xml on the classpath.", e);
        }
        mapperFactory.setMappingFiles(resources);
        return mapperFactory;
    }

	/**
	 * Jackson builder.
	 *
	 * @return the jackson 2 object mapper builder
	 */
	public Jackson2ObjectMapperBuilder jacksonBuilder() {

		Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
		builder.propertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);

		return builder;
	}

	@Bean(name="objectMapper")
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);

        return mapper;
    }

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver multipartResolver() {
	    CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
	    multipartResolver.setMaxUploadSize(150000000);
	    multipartResolver.setDefaultEncoding("utf-8");
	    return multipartResolver;
	}

	@Bean(name = "paginationAttributes")
	@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public PaginationAttributes paginationAttributes() {
			return new PaginationAttributes();
	}
	
	@Bean
	public FilterRegistrationBean<PaginationFilter> loggingFilter(){
	    FilterRegistrationBean<PaginationFilter> paginationFilterBean 
	      = new FilterRegistrationBean<PaginationFilter>();
	         
	    paginationFilterBean.setFilter(new PaginationFilter());
	    paginationFilterBean.addUrlPatterns("/*");
	         
	    return paginationFilterBean;    
	}

}

package gov.hhs.fda.ctp.web.config.mvc;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import gov.hhs.fda.ctp.common.pagination.PaginationAttributes;
import gov.hhs.fda.ctp.common.util.ApplicationContextUtil;

public class PaginationFilter implements Filter {

	@Override
	public void destroy() {
	}


	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		
		Map<String,String[]> params = request.getParameterMap();
		
		String[] activePage = params.get("activePage");
		String[] pageSize = params.get("pageSize");
		String[] calcTotalCnt = params.get("calcTotalCnt");
		String[] sortBy = params.get("sortBy[]");
		String[] sortOrder = params.get("sortOrder[]");
		String[] totalRows = params.get("itemsTotal");

		PaginationAttributes paginationAttributes = (PaginationAttributes) ApplicationContextUtil
				.getApplicationContext().getBean("paginationAttributes");
		
		if (activePage!= null && activePage.length >0 )
			paginationAttributes.setActivePage(Integer.parseInt(activePage[0]));
		if (pageSize!= null && pageSize.length >0)
			paginationAttributes.setPageSize(Integer.parseInt(pageSize[0]));
		if (calcTotalCnt!= null && calcTotalCnt.length >0)
			paginationAttributes.setCalculateTotalRows(Boolean.parseBoolean(calcTotalCnt[0]));
		if (sortBy!= null && sortBy.length >0)
			paginationAttributes.setSortBy(sortBy);
		if (sortOrder!= null && sortOrder.length >0)
			paginationAttributes.setSortOrder(sortOrder[0]);
		if (totalRows!= null && totalRows.length >0)
			paginationAttributes.setItemsTotal(Integer.parseInt(totalRows[0]));

		chain.doFilter(request, response);
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}

package gov.hhs.fda.ctp.web.config.mvc;

import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.biz.permit.PermitBizComp;
import gov.hhs.fda.ctp.biz.permitmgmt.PermitMgmtBizComp;
import gov.hhs.fda.ctp.biz.registration.RegistrationBizComp;
import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController;
import gov.hhs.fda.ctp.web.permit.PermitRestController;
import gov.hhs.fda.ctp.web.permitmgmt.PermitMgmtRestController;
import gov.hhs.fda.ctp.web.registration.RegistrationRestController;

/**
 * The Class WebTestConfig.
 */
@Profile("testing")
@Configuration
public class WebTestConfig {

    /**
     * Permit biz comp.
     *
     * @return the permit biz comp
     */
    @Bean
    @Primary
    public PermitBizComp permitBizComp() {
        return Mockito.mock(PermitBizComp.class);
    }
    
	/**
	 * Registration biz comp.
	 *
	 * @return the registration biz comp
	 */
	@Bean
	@Primary 
	public RegistrationBizComp registrationBizComp() {
		return Mockito.mock(RegistrationBizComp.class);
	}
	
	@Bean
	@Primary
	public PermitMgmtBizComp permitMgmtBizComp() {
		return Mockito.mock(PermitMgmtBizComp.class);
	}
	
	/**
	 * Registration rest controller.
	 *
	 * @return the registration rest controller
	 */
	@Bean
	@Primary
	public RegistrationRestController registrationRestController() {
		return new RegistrationRestController(this.registrationBizComp());
	}
	
	/**
	 * Company mgmt biz comp.
	 *
	 * @return the company mgmt biz comp
	 */
	@Bean
	@Primary
	public CompanyMgmtBizComp companyMgmtBizComp() {
	    return Mockito.mock(CompanyMgmtBizComp.class);
	}
	
	/**
	 * Company mgmt rest controller.
	 *
	 * @return the company mgmt rest controller
	 */
	@Bean
	@Primary
	public CompanyMgmtRestController companyMgmtRestController() {
	    return new CompanyMgmtRestController(this.companyMgmtBizComp());
	}
	
	/**
	 * Permit rest controller.
	 *
	 * @return the permit rest controller
	 */
	@Bean
	@Primary
	public PermitRestController permitRestController() {
	    return new PermitRestController(this.companyMgmtBizComp(), this.permitBizComp());
	}
	
	@Bean
	@Primary
	public PermitMgmtRestController permitMgmtRestController() {
		return new PermitMgmtRestController();
	}
	
    /**
     * Config dozer.
     *
     * @return the dozer bean mapper factory bean
     */
    @Bean(name = "mapperFactory")
    @Primary
    public DozerBeanMapperFactoryBean configDozer() {
        DozerBeanMapperFactoryBean mapperFactory = new DozerBeanMapperFactoryBean();
        Resource[] resources;
        try {
            resources = new PathMatchingResourcePatternResolver().getResources("classpath*:dozerBeanMapping.xml");
        }
        catch(Exception e) {
            throw new TufaException("Could not find dozerBeanMapping.xml on the classpath.", e);
        }
        mapperFactory.setMappingFiles(resources);
        return mapperFactory;
    }

}

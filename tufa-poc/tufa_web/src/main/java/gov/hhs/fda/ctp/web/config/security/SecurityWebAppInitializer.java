package gov.hhs.fda.ctp.web.config.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * The Class SecurityWebAppInitializer.
 */
public class SecurityWebAppInitializer extends AbstractSecurityWebApplicationInitializer {

	/**
	 * Instantiates a new security web app initializer.
	 */
	public SecurityWebAppInitializer() {
		/**
			Only required if Spring MVC/Spring has not been bootstrapped. Used to 
			instantiate ContextLoaderListner.
			super(WebSecurityConfig.class);
		**/
	}
}

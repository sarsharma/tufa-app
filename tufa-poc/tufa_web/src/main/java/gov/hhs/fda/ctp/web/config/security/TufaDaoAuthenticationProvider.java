package gov.hhs.fda.ctp.web.config.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

public class TufaDaoAuthenticationProvider extends org.springframework.security.authentication.dao.DaoAuthenticationProvider {

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication){
		//do nothing 
	}
}

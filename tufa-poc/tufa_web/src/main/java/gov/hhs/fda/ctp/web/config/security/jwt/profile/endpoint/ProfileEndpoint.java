package gov.hhs.fda.ctp.web.config.security.jwt.profile.endpoint;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.common.security.UserContext;
import gov.hhs.fda.ctp.web.config.security.jwt.security.auth.JwtAuthenticationToken;

@RestController
public class ProfileEndpoint {
    @RequestMapping(value="/api/me", method=RequestMethod.GET)
    public @ResponseBody UserContext get(JwtAuthenticationToken token) {
        return (UserContext) token.getPrincipal();
    }
}

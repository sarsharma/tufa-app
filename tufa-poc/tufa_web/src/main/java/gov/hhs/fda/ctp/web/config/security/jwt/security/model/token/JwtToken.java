package gov.hhs.fda.ctp.web.config.security.jwt.security.model.token;

public interface JwtToken {
    String getToken();
}

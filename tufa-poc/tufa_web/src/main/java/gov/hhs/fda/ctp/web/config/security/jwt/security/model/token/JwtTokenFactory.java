package gov.hhs.fda.ctp.web.config.security.jwt.security.model.token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import gov.hhs.fda.ctp.web.config.security.UserTufa;
import gov.hhs.fda.ctp.web.config.security.jwt.security.config.JwtSettings;
import gov.hhs.fda.ctp.web.config.security.jwt.security.model.Scopes;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
@PropertySource({ "classpath:jwt/jwt-security.properties" })
public class JwtTokenFactory {
    
	private final JwtSettings settings;
    
    @Autowired
	private Environment env;

    @Autowired
    public JwtTokenFactory(JwtSettings settings) {
        this.settings = settings;
    }

    /**
     * Factory method for issuing new JWT Tokens.
     * 
     * @param username
     * @param roles
     * @return
     */
    public AccessJwtToken createAccessJwtToken(UserTufa userContext) {
        if (StringUtils.isBlank(userContext.getUsername())) 
            throw new IllegalArgumentException("Cannot create JWT Token without username");

        if (userContext.getAuthorities() == null || userContext.getAuthorities().isEmpty()) 
            throw new IllegalArgumentException("User doesn't have any privileges");

        Claims claims = Jwts.claims().setSubject(userContext.getUsername());
        List<String> authorities = new ArrayList<>();
        for(GrantedAuthority ga: userContext.getAuthorities()){
        	authorities.add(ga.getAuthority());
        }
        
        claims.put("scopes",authorities);
        claims.put("expWarnTime",env.getProperty("expirationWarningTime"));
        
        DateTime currentTime = new DateTime();

        String accesstoken = Jwts.builder()
          .setClaims(claims)
          .setIssuer(env.getProperty("tokenIssuer"))
          .setIssuedAt(currentTime.toDate())
          .setExpiration(currentTime.plusMinutes(Integer.parseInt(env.getProperty("tokenExpirationTime"))).toDate())
          .signWith(SignatureAlgorithm.HS512, env.getProperty("tokenSigningKey"))
        .compact();

        return new AccessJwtToken(accesstoken, claims);
    }

    public JwtToken createRefreshToken(UserTufa userContext) {
        if (StringUtils.isBlank(userContext.getUsername())) {
            throw new IllegalArgumentException("Cannot create JWT Token without username");
        }

        DateTime currentTime = new DateTime();

        Claims claims = Jwts.claims().setSubject(userContext.getUsername());
        claims.put("scopes", Arrays.asList(Scopes.REFRESH_TOKEN.authority()));

        String token = Jwts.builder()
          .setClaims(claims)
          .setIssuer(env.getProperty("tokenIssuer"))
          .setId(UUID.randomUUID().toString())
          .setIssuedAt(currentTime.toDate())
          .setExpiration(currentTime.plusMinutes(Integer.parseInt(env.getProperty("refreshTokenExpTime"))).toDate())
          .signWith(SignatureAlgorithm.HS512, env.getProperty("tokenSigningKey"))
        .compact();

        return new AccessJwtToken(token, claims);
    }
}

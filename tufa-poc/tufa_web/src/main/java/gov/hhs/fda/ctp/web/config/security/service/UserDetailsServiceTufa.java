package gov.hhs.fda.ctp.web.config.security.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.persistence.UserAdminEntityManager;
import gov.hhs.fda.ctp.persistence.model.RoleEntity;
import gov.hhs.fda.ctp.persistence.model.UserEntity;
import gov.hhs.fda.ctp.web.config.security.UserTufa;
import gov.hhs.fda.ctp.web.config.security.jwt.security.auth.ajax.AjaxLoginProcessingFilter;

/**
 * UserDetailsServiceTufa loads user specific details if user is found in the database.
 * This includes roles,resources and privileges. If user is not found in the database
 * with the email entered an exception is thrown indicating that authentication of the 
 * user failed.
 */
@Service("userDetailsService")
@Transactional
public class UserDetailsServiceTufa implements UserDetailsService {

    private static Logger logger = LoggerFactory.getLogger(UserDetailsServiceTufa.class);

	/** The user admin entity manager. */
	private UserAdminEntityManager userAdminEntityManager;
	
	/**
	 * Instantiates a new user details service tufa.
	 */
	public UserDetailsServiceTufa() {
        super();
    }
	
	/**
	 * Sets the user admin entity manager.
	 *
	 * @param userAdminEntityManager the new user admin entity manager
	 */
	@Autowired
	public void setUserAdminEntityManager(UserAdminEntityManager userAdminEntityManager){
		this.userAdminEntityManager = userAdminEntityManager;
	}
	
	/**
	 * load user from the database with the provided email address. 
	 * Throw an exception if user not found.
	 *
	 * @param email the email
	 * @return the user details
	 * @throws UsernameNotFoundException the username not found exception
	 */
    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
    
            final UserEntity user = userAdminEntityManager.getUser(email);
            if (user == null) {
            	logger.error("No user found with username: " + email);
                throw new UsernameNotFoundException("No user found with username: " + email);
            }
            
            /*
             * Custom User object to be used during custom implementation of Access Decision Manager.
             * The User object can carry information related to resources and privileges as well.
             */ 
            
            /*
             * Send in an empty password. As this is an SSO authentication and the username coming to 
               TUFA is already authenticated. Password is not Required.
            */
            return new UserTufa(user.getEmail(), "", true, true, true, true,getGrantedAuth(user),user);
    }

    /**
     * Set up the Granted Authorities in the User Object i.e. the roles.
     *
     * @param user the user
     * @return the granted auth
     */
	private final List<GrantedAuthority> getGrantedAuth(UserEntity user) {

		List<RoleEntity> roles = user.getRoles();
		final List<GrantedAuthority> authorities = new ArrayList<>();

		for (RoleEntity role : roles) {
			authorities.add(new SimpleGrantedAuthority(role.getName()));
		}

		return authorities;
	}	
}

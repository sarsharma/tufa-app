/**
 * 
 */
package gov.hhs.fda.ctp.web.contactmgmt;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.biz.contactmgmt.ContactMgmtBizComp;
import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.persistence.model.ReportContactEntity;

/**
 * The Class ContactMgmtRestController.
 *
 * @author akari This controller is responsible for CRUD operations for point of
 *         contacts
 */
//@CrossOrigin(origins = "http://localhost:9000")
@RestController
@RequestMapping("/api/v1/companies/contact")
public class ContactMgmtRestController {
	
	/** The logger. */
	Logger logger = LoggerFactory.getLogger(ContactMgmtRestController.class);

	/** The contact mgmt biz comp. */
	@Autowired
	private ContactMgmtBizComp contactMgmtBizComp;

	/**
	 * Instantiates a new contact mgmt rest controller.
	 *
	 * @param contactMgmtBizComp the contact mgmt biz comp
	 */
	public ContactMgmtRestController(ContactMgmtBizComp contactMgmtBizComp) {
		this.contactMgmtBizComp = contactMgmtBizComp;
	}

	/**
	 * create contact.
	 *
	 * @param model the model
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Contact> create(@Valid @RequestBody Contact model) {
		logger.debug("CREATE_CONTACT: Create contact with last name = {}, first name = {}", model.getLastNm(),
				model.getFirstNm());
		Contact retmodel = contactMgmtBizComp.create(model);

		return new ResponseEntity<>(retmodel, HttpStatus.CREATED);
	}
	
	
	@RequestMapping(value = "/copycontactstotufa", method=RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<List<Contact>> saveMultipleContacts(@Valid @RequestBody List<Contact> model) {
		List<Contact> retmodel = contactMgmtBizComp.saveMultipleContacts(model);
		return new ResponseEntity<>(retmodel, HttpStatus.OK);
	}


	/**
	 * Read contact.
	 *
	 * @param id the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<Contact> read(@PathVariable long id) {
		Contact contact = contactMgmtBizComp.readById(id);
		return new ResponseEntity<>(contact, HttpStatus.OK);
	}

	/**
	 * Update contact.
	 *
	 * @param model the model
	 * @return the response entity
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Contact> update(@Valid @RequestBody Contact model) {
		Contact retmodel = contactMgmtBizComp.update(model);
		return new ResponseEntity<>(retmodel, HttpStatus.OK);
	}
	
	/**
	 * Update contact.
	 *
	 * @param model the model
	 * @return the response entity
	 */
	@RequestMapping(value = "/{setPrimary}", method = RequestMethod.PUT)
	public ResponseEntity<Contact> update(@PathVariable boolean setPrimary, @Valid @RequestBody Contact model) {
		Contact retmodel = contactMgmtBizComp.setAsPrimaryContact(setPrimary, model);
		return new ResponseEntity<>(retmodel, HttpStatus.OK);
	}

	/**
	 * Delete Contact.
	 *
	 * @param id the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Contact> delete(@PathVariable long id) {
		Contact contact = contactMgmtBizComp.readById(id);
		contactMgmtBizComp.deleteById(id);
		return new ResponseEntity<>(contact, HttpStatus.OK);
	}
	
	 /**
		 * get report contacts.
		 *
		 * @param companyId
		 * @param periodId
		 * @return list of report address entities
		 */
	    @RequestMapping(value = "{companyId}/{permitId}/{periodId}", method = RequestMethod.GET, produces = { "application/json" })
		public ResponseEntity<?> getReportContacts(@PathVariable("companyId") int companyId,
				@PathVariable("permitId") int permitId,
				@PathVariable("periodId") int periodId) {
			List<ReportContactEntity> contacts = contactMgmtBizComp.getReportContacts(companyId, permitId, periodId);
			return ResponseEntity.ok(contacts);
		}

}

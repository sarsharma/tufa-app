package gov.hhs.fda.ctp.web.exception;

/**
 * The Class ErrorInfo.
 */
public class ErrorInfo {
	
	/** The url. */
	public final String url;
    
    /** The ex. */
    public final String ex;
    
    public  String data;


    /**
     * Instantiates a new error info.
     *
     * @param url the url
     * @param exceptionMessage the exception message
     */
    public ErrorInfo(String url, String exceptionMessage) {
        this.url = url;
        this.ex = exceptionMessage;
    }
    
    public ErrorInfo(String url, String exceptionMessage,String data) {
        this.url = url;
        this.ex = exceptionMessage;
        this.data = data;
    }

}

package gov.hhs.fda.ctp.web.exception;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.common.beans.TobaccoClass;
import gov.hhs.fda.ctp.web.reporting.ReportController;

@RestController
@RequestMapping("/api/v1/error")
public class ErrorRestController {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(ErrorRestController.class);
	
	@RequestMapping(value="/log", method = RequestMethod.POST)
    public ResponseEntity<String> logError(@RequestBody String[] errors) {
		for(String error : errors) logger.error("CLIENT ERROR: {}", error);
		return new ResponseEntity<String>("Error Logged", HttpStatus.OK);
    }
}

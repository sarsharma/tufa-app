/**
 *
 */
package gov.hhs.fda.ctp.web.permit;

import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.biz.permit.PermitBizComp;
import gov.hhs.fda.ctp.common.beans.ExcludePermitRequest;
import gov.hhs.fda.ctp.common.beans.FormComment;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitHistCommentsBean;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCommentResponse;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.common.beans.PermitMfgReport;
import gov.hhs.fda.ctp.common.beans.PermitPeriod;
import gov.hhs.fda.ctp.common.beans.TTBAdjustment;
import gov.hhs.fda.ctp.common.beans.TobaccoClass;
import gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController;

/**
 * The Class PermitRestController.
 *
 * @author tgunter
 */
//@CrossOrigin(origins = "http://localhost:9000")
@RestController
@RequestMapping("/api/v1/permit")
public class PermitRestController {

    /** The logger. */
    Logger logger = LoggerFactory.getLogger(CompanyMgmtRestController.class);

    /** The company mgmt biz comp. */
    // Application layer controller.
    @Autowired
    private CompanyMgmtBizComp companyMgmtBizComp;

    /** The permit biz comp. */
    @Autowired
    private PermitBizComp permitBizComp;

    /**
     * Inject application layer controller.
     *
     * @param companyMgmtBizComp the company mgmt biz comp
     * @param permitBizComp the permit biz comp
     */
    public PermitRestController(CompanyMgmtBizComp companyMgmtBizComp, PermitBizComp permitBizComp){
        this.companyMgmtBizComp = companyMgmtBizComp;
        this.permitBizComp = permitBizComp;
    }

    /**
     * Return the permit with a list of permit history objects attached.
     *
     * @param id the id
     * @return JSON
     */
    @RequestMapping(value = "history/{id}",
    method = RequestMethod.GET,
    produces = { "application/json" })
    public ResponseEntity<Permit> getHistory(@PathVariable long id) {
        Permit permit = companyMgmtBizComp.getPermit(id);
        permit = companyMgmtBizComp.getPermitHistory(permit);
        return new ResponseEntity<>(permit, HttpStatus.OK);
    }
    
    @RequestMapping(value = "history/report3852/{id}",
    method = RequestMethod.GET,
    produces = { "application/json" })
    public ResponseEntity<Permit> getHistory3852(@PathVariable long id) {
        Permit permit = companyMgmtBizComp.getPermit(id);
        permit = companyMgmtBizComp.getPermitHistory3852(permit);
        return new ResponseEntity<>(permit, HttpStatus.OK);
    }

//    @RequestMapping(value="history/{id}/page/{page}/rows/{rows}",
//    method = RequestMethod.GET,
//    produces = { "application/json" })
//    public ResponseEntity<Permit> getPagedHistory(@PathVariable long id, @PathVariable int page, @PathVariable int rows) {
//    	Permit permit = companyMgmtBizComp.getPermit(id);
//    	permit = companyMgmtBizComp.getPaginatedPermitHistory(permit, page, rows);
//        return new ResponseEntity<>(permit, HttpStatus.OK);
//    }

    /**
     * Get a permit by permit id.
     *
     * @param id the id
     * @return JSON
     */
    @RequestMapping(value = "/{id}",
    method = RequestMethod.GET,
    produces = { "application/json" })
    public ResponseEntity<Permit> read(@PathVariable long id) {
        Permit permit = companyMgmtBizComp.getPermit(id);
        return new ResponseEntity<>(permit, HttpStatus.OK);
    }

    /**
     * Save / update comment for permit.
     * @param comment
     * @param permitId
     * @return
     */
    @RequestMapping(value="/{id}/comment", method=RequestMethod.PUT)
    public ResponseEntity<String> updatePermitComments(@RequestBody String comment, @PathVariable("id") long permitId) {
    	return ResponseEntity.ok(permitBizComp.updatePermitComments(permitId, comment));
    }

    /**
     * Get all company permit histories, filtered by criteria.
     *
     * @param criteria the criteria
     * @return the history criteria
     */
    @RequestMapping(value="history", method=RequestMethod.PUT)
    public ResponseEntity<PermitHistoryCriteria> getHistoryCriteria(@RequestBody PermitHistoryCriteria criteria) {
    	PermitHistoryCriteria retcriteria = permitBizComp.getPermitHistories(criteria);
        return new ResponseEntity<>(retcriteria, HttpStatus.OK);
    }

    /**
     * Get Permit object graph with Permit -- Contacts
     *                                     -- Company -- Addresses.
     *
     * @param permitId the permit id
     * @param periodId the period id
     * @return the permit info
     */
    @RequestMapping(value="/{permitId}/period/{periodId}", method = RequestMethod.GET)
    public ResponseEntity<PermitPeriod> getPermitInfo(@PathVariable long permitId, @PathVariable long periodId) {
        PermitPeriod permitPeriod = permitBizComp.getPermitPeriod(permitId, periodId);
        return new ResponseEntity<>(permitPeriod, HttpStatus.OK);
    }

    /**
     * Save permit period info.
     *
     * @param permitId the permit id
     * @param periodId the period id
     * @param report the report
     * @return the response entity
     */
    @RequestMapping(value="/{permitId}/period/{periodId}", method=RequestMethod.PUT)
    public ResponseEntity<PermitPeriod> savePermitPeriodInfo(@PathVariable long permitId, @PathVariable long periodId, @RequestBody PermitMfgReport report) {
        report.setPermitId(permitId);
        report.setPeriodId(periodId);
        PermitPeriod permitPeriod = permitBizComp.saveSubmittedForms(report);
        return new ResponseEntity<>(permitPeriod, HttpStatus.OK);
    }

    /**
     * Create a comment for a monthly report form.
     * @param permitId
     * @param periodId
     * @param comment
     * @return
     */
    @RequestMapping(value="/{permitId}/period/{periodId}/comment", method=RequestMethod.POST)
    public ResponseEntity<FormComment> createFormComment(@PathVariable long permitId, @PathVariable long periodId, @RequestBody FormComment comment) {
    	return new ResponseEntity<>(permitBizComp.saveFormComment(comment,permitId, periodId), HttpStatus.OK);
    }

    @RequestMapping(value="/{permitId}/period/{periodId}/zerocomments", method=RequestMethod.GET)
    public ResponseEntity<?> getZeroReportComment(@PathVariable long permitId, @PathVariable long periodId) {
    	
    	List<FormComment> reportcomment = permitBizComp.getZeroReportComment(permitId, periodId);
    	return new ResponseEntity<>(reportcomment, HttpStatus.OK);
    	
    }
    
    /**
     * Delete the comments for the given form.
     * @param permitId
     * @param periodId
     * @param formId
     * @return
     */
    @RequestMapping(value = "/{permitId}/period/{periodId}/comment",  method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteFormComment(@PathVariable long permitId, @PathVariable long periodId, @RequestBody List<Long> formIds) {
    	for(Long formId : formIds) {
    		permitBizComp.deleteFormCommentsById(formId);
    	}
    	return new ResponseEntity<>(JSONObject.quote("OK"), HttpStatus.OK);
    }
    
    
    /**
     * Delete the comments for the given form.
     * @param permitId
     * @param periodId
     * @param formId
     * @return
     */
    @RequestMapping(value = "/{permitId}/period/{periodId}/orphancomments",  method=RequestMethod.DELETE)
    public ResponseEntity<?> deleteOrphanedFormComment(@PathVariable long permitId, @PathVariable long periodId, @RequestBody List<Long> formIds) {
    	
    	permitBizComp.deleteOrphanedFormComments(formIds, permitId, periodId);
    	return new ResponseEntity<>(JSONObject.quote("OK"), HttpStatus.OK);
    }

    /**
     * Save / update permit contacts.
     *
     * @param permit the permit
     * @return the response entity
     */
    @RequestMapping(value="/contacts", method = RequestMethod.PUT)
    public ResponseEntity<Permit> savePermitContacts(@RequestBody Permit permit) {
    	Permit retpermit = permitBizComp.saveOrUpdatePermitContacts(permit);
        return new ResponseEntity<>(retpermit, HttpStatus.OK);
    }

    /**
     * Get a list of all the available fiscal years, in descending order.
     *
     * @return the available permit report fiscal years
     */
    @RequestMapping(value = "/fiscalyears", method = RequestMethod.GET)
    public ResponseEntity<?> getAvailablePermitReportFiscalYears() {
        Set<String> years = permitBizComp.getAvailablePermitReportFiscalYears();
        return ResponseEntity.ok(years);
    }

    /**
     * Get a list of all the available calendar years, in descending order.
     *
     * @return the available permit report calendar years
     */
    @RequestMapping(value = "/calendaryears", method = RequestMethod.GET)
    public ResponseEntity<?> getAvailablePermitReportCalendarYears() {
        Set<String> years = permitBizComp.getAvailablePermitReportCalendarYears();
        return ResponseEntity.ok(years);
    }

    @RequestMapping(value = "/taxrates", method = RequestMethod.GET)
    public ResponseEntity<?> getTaxRates() {
        List<TobaccoClass> tobaccoClasses = permitBizComp.getLatestTobaccoClasses();
        return new ResponseEntity<>(tobaccoClasses, HttpStatus.OK);
    }

    @RequestMapping(value="/taxrates", method = RequestMethod.PUT)
    public ResponseEntity<String> saveTaxRates(@RequestBody List<TobaccoClass> rates) {
    	permitBizComp.saveTobaccoClasses(rates);
    	return new ResponseEntity<>(JSONObject.quote("Success"), HttpStatus.OK);
    }

    /**
	 * Delete Monthly Report.
	 *
	 * @param period
	 *            id
	 * @return the response entity
	 */
	@RequestMapping(value = "/deletemonthlyreport/{companyId}/{permitId}/{periodId}", method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteMonthlyReport(@PathVariable("companyId") int companyId,
			@PathVariable("permitId") int permitId, @PathVariable("periodId") int periodId) {
		boolean delSuccess = false;
		delSuccess = permitBizComp.deleteMonthlyReport(companyId, permitId, periodId);
		return new ResponseEntity<>(delSuccess, HttpStatus.OK);
	}
	
	@PostMapping(value="/history/{permitId}/addupdatecomment")
	public ResponseEntity<PermitHistCommentsBean> saveUpdatePermitHistoryComments(@PathVariable long permitId, @RequestBody PermitHistCommentsBean permitHistComments) {
	    PermitHistCommentsBean bean  = permitBizComp.saveUpdatePermitHistoryComments(permitHistComments, permitId);
	    return new ResponseEntity<PermitHistCommentsBean>(bean,HttpStatus.OK);
	}
	
	@GetMapping(value="/getallcomments/{permitid}")
	public ResponseEntity<?> getAllCommentsForPermit(@PathVariable long permitid) {
	    PermitHistoryCommentResponse response = permitBizComp.getAllCommentsForPermit(permitid);
	    return new ResponseEntity<>(response.getCommentsList(),HttpStatus.OK);
	}
	
	@PostMapping(value="history/updatecomment/{permitcommentid}")
	public ResponseEntity<PermitHistCommentsBean> updatePermitHistoryComment(@PathVariable long permitcommentid,@RequestBody PermitHistCommentsBean permitHistComments) {
	    PermitHistCommentsBean bean = permitBizComp.updatePermitHistoryComment(permitcommentid, permitHistComments);
	    return new ResponseEntity<>(bean,HttpStatus.OK);
	}
	
	@RequestMapping(value="/saveTTBAdjustments/{permitId}/{periodId}", method=RequestMethod.PUT)
    public ResponseEntity<TTBAdjustment> saveTTBAdjustments(@PathVariable("permitId") long permitId, @PathVariable("periodId") long periodId,@RequestBody List<TTBAdjustment> ttbAdjustmentList) {
    	return new ResponseEntity<>(permitBizComp.saveTTBAdjustments(permitId,periodId,ttbAdjustmentList.get(0)), HttpStatus.OK);
    }

}

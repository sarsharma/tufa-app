package gov.hhs.fda.ctp.web.permitmgmt;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import gov.hhs.fda.ctp.biz.permitmgmt.PermitMgmtBizComp;
import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitAudit;
import gov.hhs.fda.ctp.common.beans.PermitAuditsExport;
import gov.hhs.fda.ctp.common.beans.PermitUpdateUpload;
import gov.hhs.fda.ctp.common.beans.PermitUpdatesDocument;
import gov.hhs.fda.ctp.common.ingestion.FileIngestor;
import gov.hhs.fda.ctp.common.ingestion.FileIngestorFactory;
import gov.hhs.fda.ctp.common.ingestion.FileIngestorNameMapping;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateEntity;

@RestController
@RequestMapping("/api/v1/permitmgmt")
public class PermitMgmtRestController {
	
	@Autowired
	private FileIngestorFactory fileIngestorFactory;

	@Autowired
	private FileIngestorNameMapping fileIngestorNameMapping;
	
	@Autowired
	private PermitMgmtBizComp permitMgmtBizComp;

	@Autowired
	private SecurityContextUtil securityContextUtil;
	
	@RequestMapping(value = "/ingestFile", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<PermitUpdatesDocument> ingestPermitListFile(@RequestParam("permitListDocument") MultipartFile file,
			@RequestParam("fileType") String fileType, @RequestParam("aliasFileNm") String aliasFileNm)
			throws IOException, OpenXML4JException, SAXException, SQLException {
		
				long errorCount = 0;

				if (!file.isEmpty() && fileType != null) {
					IngestionOutput output = new IngestionOutput();

					if (fileType.equalsIgnoreCase(Constants.PERMIT_UPDATE)) {
						String fileIngestorName = this.fileIngestorNameMapping.getFileIngestorName(fileType);
						FileIngestor fileIngestor = this.fileIngestorFactory.getFileIngestor(fileIngestorName);
						String filePath = generateIngestionFile(file);
						fileIngestor.parsePermitListFileSAX(filePath, output);
						errorCount = output.getErrorCount();
						permitMgmtBizComp.ingestPermitUpdates(output, file, aliasFileNm);
						deleteGeneratedIngestionFile(filePath);
					}
				}
				
				PermitUpdatesDocument doc = new PermitUpdatesDocument();
				doc.setFileNm(file.getOriginalFilename());
				doc.setCreatedBy((securityContextUtil.getPrincipal()!=null)?securityContextUtil.getPrincipal().getUsername():null);
				doc.setAliasFileNm(aliasFileNm);
				DateTime dt = new DateTime();
				DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
				String dtStr = fmt.print(dt);
				doc.setCreatedDt(dtStr);
				doc.setErrorCount(errorCount);
			
				return new ResponseEntity<PermitUpdatesDocument>(doc, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/permits/audit", method = RequestMethod.GET)
	public ResponseEntity<PermitAuditsExport> getPermitUpdates() {
		PermitAuditsExport export = permitMgmtBizComp.exportPermitUpdates();
		return new ResponseEntity<PermitAuditsExport>(export, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/permits/documents", method = RequestMethod.GET)
	public ResponseEntity<?> getPermitUpdateFiles(){
		List<PermitUpdatesDocument> permitUpdateFiles = permitMgmtBizComp.getPermitUpdateFiles();
		return new ResponseEntity<>(permitUpdateFiles, HttpStatus.OK);

	}
	
	@RequestMapping(value = "/permits/address", method = RequestMethod.PUT)
	public ResponseEntity<?> getUpdatedPermitsAddress(@RequestBody Long docId){
		List<PermitUpdateUpload> updatedPermits = this.permitMgmtBizComp.getUpdatedPermits(docId);
		return new ResponseEntity<>(updatedPermits, HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value = "/permits/ingestedaddress", method = RequestMethod.GET)
	public ResponseEntity<?> getUpdatedPermitsAddress(){
		List<Address> address = this.permitMgmtBizComp.getIngestedAddress();
		return new ResponseEntity<>(address, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/permits/ingestedcontacts", method = RequestMethod.GET)
	public ResponseEntity<?> getUpdatedPermitsContacts(){
		List<Contact> contacts = this.permitMgmtBizComp.getIngestedContacts();
		return new ResponseEntity<>(contacts, HttpStatus.OK);
		
	}
	
	
	
	@RequestMapping(value = "/permits/contacts", method = RequestMethod.PUT)
	public ResponseEntity<?> getUpdatedPermitsContacts(@RequestBody Long docId){
		List<PermitUpdateUpload> updatedPermits = this.permitMgmtBizComp.getUpdatedPermitsContacts(docId);
		return new ResponseEntity<>(updatedPermits, HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value = "/permits/contacts/updignrstatusanddelete", method = RequestMethod.GET)
	public ResponseEntity<?> saveIgnrContactStatus(){
		
		List<PermitUpdateUpload> updatedContactPermits = 	this.getPermitMgmtBizComp().setIgnrContactStatusandDelete();
		
		return new ResponseEntity<>(updatedContactPermits, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/permits/address/updignrstatusanddelete", method = RequestMethod.GET)
	public ResponseEntity<?> saveIgnrAddrStatus(){
		List<PermitUpdateUpload> updatedAddressPermits = 	this.getPermitMgmtBizComp().setIgnrAddressStatusandDelete();

		return new ResponseEntity<>(updatedAddressPermits, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/permits/update", method = RequestMethod.PUT)
	public ResponseEntity<?> saveUpdatedPermits(@RequestBody PermitUpdateEntity updatedPermit){
		List<PermitUpdateUpload> updatedPermits = this.permitMgmtBizComp.saveUpdatedPermits(updatedPermit);
		return new ResponseEntity<>(updatedPermits, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/permits/update/contacts", method = RequestMethod.PUT)
	public ResponseEntity<?> saveUpdatedContacts(@RequestBody PermitUpdateEntity updatedPermit){
		List<PermitUpdateUpload> updatedPermits = this.permitMgmtBizComp.saveUpdatedContacts(updatedPermit);
		return new ResponseEntity<>(updatedPermits, HttpStatus.OK);
		
	}
	
	private String generateIngestionFile(MultipartFile file) throws IOException {

		byte[] fileBytes = file.getBytes();
		String fileName = file.getOriginalFilename();
		File tmpFile = null;

		tmpFile = File.createTempFile(FilenameUtils.getBaseName(fileName), FilenameUtils.getExtension(fileName));
		tmpFile.deleteOnExit();

		OutputStream outStream = new FileOutputStream(tmpFile);
		outStream.write(fileBytes);
		outStream.close();

		return tmpFile.getAbsolutePath();
	}

	private void deleteGeneratedIngestionFile(String filePath) throws IOException {
		Path fileP = Paths.get(filePath);
		Files.deleteIfExists(fileP);
	}
	
	@RequestMapping(value = "document/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> downloadAttachment(@PathVariable("id") long docId) {
		PermitUpdatesDocument document = this.permitMgmtBizComp.getPermitUpdatesDocument(docId);
		return new ResponseEntity<>(document, HttpStatus.OK);
	}
	
	@RequestMapping(value = "document/{docId}/{filename}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateFileName(@PathVariable("filename") String fileName,@PathVariable("docId") long docId){
		this.permitMgmtBizComp.updateDocumentName(docId, fileName);
		PermitUpdatesDocument doc = new PermitUpdatesDocument();
		doc.setFileNm(fileName);
		doc.setDocId(docId);
		return new ResponseEntity<>(doc,HttpStatus.OK);
	}
	
	@RequestMapping(value = "permits/audit/{docId}", method = RequestMethod.GET)
	public ResponseEntity<List<PermitAudit>> getPermitUpdates(@PathVariable("docId") Long docId 
			){
		List<PermitAudit> updatedPermits =  this.permitMgmtBizComp.getPermitUpdatesByDocId(docId);
		return new ResponseEntity<>(updatedPermits, HttpStatus.OK);
	}
	
	@RequestMapping(value = "permits/audit", method = RequestMethod.POST)
	public ResponseEntity<PermitAudit> insertPermitUpdate(@RequestBody PermitAudit pAudit){
		PermitAudit permitAudit = this.permitMgmtBizComp.insertPermitAudit(pAudit);
		return new ResponseEntity<>(permitAudit,HttpStatus.OK);
	}
	
	@RequestMapping(value = "addandclose/company/{companyId}", method = RequestMethod.PUT)
	public ResponseEntity<Company> addAndClose(@Valid @RequestBody List<Permit> permits,
			@PathVariable("companyId") long companyId) {
		Company company = this.permitMgmtBizComp.addAndClose(permits, companyId);
		return new ResponseEntity<>(company,HttpStatus.OK);
	}
	
	@RequestMapping(value = "permits/exclusion/{docId}", method = RequestMethod.GET)
	public ResponseEntity<List<PermitAudit>>  getTTBPermitExclusionStatuses(@PathVariable("docId") Long docId){
		List<PermitAudit> ttbPermitsExcls = this.permitMgmtBizComp.getTTBPermitListExclEntries(docId);
		return new ResponseEntity<>(ttbPermitsExcls,HttpStatus.OK);
	}
	
	@RequestMapping(value = "permits/exclusion/resolve", method = RequestMethod.PUT)
	public ResponseEntity<PermitAudit>  getTTBPermitExclusionStatuses(@RequestBody PermitAudit permitAudit){
		PermitAudit pAudit = this.permitMgmtBizComp.resolveTTBPermitExclusion(permitAudit);
		return new ResponseEntity<>(pAudit,HttpStatus.OK);
	}
	
	public FileIngestorFactory getFileIngestorFactory() {
		return fileIngestorFactory;
	}


	public void setFileIngestorFactory(FileIngestorFactory fileIngestorFactory) {
		this.fileIngestorFactory = fileIngestorFactory;
	}


	public FileIngestorNameMapping getFileIngestorNameMapping() {
		return fileIngestorNameMapping;
	}


	public void setFileIngestorNameMapping(FileIngestorNameMapping fileIngestorNameMapping) {
		this.fileIngestorNameMapping = fileIngestorNameMapping;
	}

	public PermitMgmtBizComp getPermitMgmtBizComp() {
		return permitMgmtBizComp;
	}

	public void setPermitMgmtBizComp(PermitMgmtBizComp permitMgmtBizComp) {
		this.permitMgmtBizComp = permitMgmtBizComp;
	}

}

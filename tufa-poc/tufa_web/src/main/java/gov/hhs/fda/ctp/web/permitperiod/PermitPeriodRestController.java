package gov.hhs.fda.ctp.web.permitperiod;

import java.util.List;

import javax.validation.Valid;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.biz.permitperiod.PermitPeriodBizComp;
import gov.hhs.fda.ctp.common.beans.DocExport;
import gov.hhs.fda.ctp.common.beans.PeriodStatus;
import gov.hhs.fda.ctp.common.beans.UnitConverter;
import gov.hhs.fda.ctp.persistence.model.MissingForms;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;

/**
 * The Class PermitPeriodRestController.
 *
 * @author rnasina
 */
//@CrossOrigin(origins = "http://localhost:9000")
@RestController
@RequestMapping("/api/v1/companies/period")
public class PermitPeriodRestController {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(PermitPeriodRestController.class);

    /** The permit period biz comp. */
    @Autowired
    private PermitPeriodBizComp permitPeriodBizComp;

    /**
     * Auto wire constructor.
     *
     * @param permitPeriodBizComp the permit period biz comp
     */
    public PermitPeriodRestController(PermitPeriodBizComp permitPeriodBizComp){
        this.permitPeriodBizComp = permitPeriodBizComp;
    }

    /**
     * Bulk creation of monthly reports.
     *
     * @param monthYear the month year
     * @return the response entity
     */
    @RequestMapping(value = "create/{monthYear}", method = RequestMethod.PUT,
    produces = { "application/json" })
    public ResponseEntity<String> createRptPeriod(@PathVariable("monthYear") String monthYear) {
    	String ppe = permitPeriodBizComp.createRptPeriod(monthYear);
		return new ResponseEntity<>(ppe, HttpStatus.OK);
    }

    /**
     * Fetch a report.
     *
     * @param periodId the period id
     * @param permitId the permit id
     * @return the response entity
     */
    @RequestMapping(value = "{id}/{permitId}",
    method = RequestMethod.GET,
    produces = { "application/json" })
    public ResponseEntity<PermitPeriodEntity> read(@PathVariable("id") long periodId,@PathVariable("permitId") long permitId) {
    	PermitPeriodEntity ppe = permitPeriodBizComp.getPermitPeriodByIds(periodId, permitId);
		return new ResponseEntity<>(ppe, HttpStatus.OK);
    }

    /**
     * Save or update an Report.
     *
     * @param model the model
     * @return the response entity
     */
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<PermitPeriodEntity> saveOrUpdate(@Valid @RequestBody PermitPeriodEntity model) {
        PermitPeriodEntity retmodel = permitPeriodBizComp.updatePermitPeriod(model, null);
        return new ResponseEntity<>(retmodel, HttpStatus.OK);
    }

    /**
     * delete a report.
     *
     * @param periodId the period id
     * @param permitId the permit id
     * @return the response entity
     */
    @RequestMapping(value = "{periodId}/{permitId}/delete", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<PermitPeriodEntity> deleteReport(@PathVariable("periodId") long periodId,
    		@PathVariable("permitId") long permitId) {
    	PermitPeriodEntity ppe = permitPeriodBizComp.deleteByIds(periodId, permitId);
		return new ResponseEntity<>(ppe, HttpStatus.OK);
    }

    @RequestMapping(value = "{periodId}/permit/{permitId}", method = RequestMethod.PUT)
    public ResponseEntity<PeriodStatus> updateReconReport(@PathVariable("periodId") long periodId,
    		@PathVariable("permitId") long permitId) {
        PeriodStatus retmodel = permitPeriodBizComp.updatePeriodReconStatus(periodId, permitId);
        return new ResponseEntity<>(retmodel, HttpStatus.OK);
    }

    @RequestMapping(value = "{periodId}/permit/{permitId}", method = RequestMethod.GET)
    public ResponseEntity<PeriodStatus> getReconReportStatus(@PathVariable("periodId") long periodId,
    		@PathVariable("permitId") long permitId) {
        PeriodStatus retmodel = permitPeriodBizComp.getPeriodReconStatus(periodId, permitId);
        return new ResponseEntity<>(retmodel, HttpStatus.OK);
    }

    @RequestMapping(value = "{periodId}/{permitId}/forms", method = RequestMethod.GET)
    public ResponseEntity<?> getMissingForms(@PathVariable("periodId") long periodId,
    		@PathVariable("permitId") long permitId) {
        List<MissingForms> forms = permitPeriodBizComp.getMissingForms(permitId, periodId);
        return new ResponseEntity<>(forms, HttpStatus.OK);
    }

    @RequestMapping(value = "{periodId}/{permitId}/forms", method = RequestMethod.POST)
    public ResponseEntity<String> saveMissingForms(@Valid @RequestBody List<MissingForms> forms,
    		@PathVariable("periodId") long periodId,
    		@PathVariable("permitId") long permitId) {
        permitPeriodBizComp.saveMissingForms(forms, periodId, permitId);
        return new ResponseEntity<>(JSONObject.quote("Successfully saved the missing forms"), HttpStatus.OK);
    }

	
	/**
	 * Export form details as CSV
	 * 
	 * @param formNum
	 * */
	@RequestMapping(value = "/export/{permitId}/{periodId}/{form}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<DocExport> exportForm(@PathVariable("permitId") int permitId, @PathVariable("periodId") int periodId, @PathVariable("form") String form, @PathVariable("tobaccoClass") int tobaccoClass) {
		DocExport docExport = this.permitPeriodBizComp.getMonthlyReportExport(permitId, periodId, form, tobaccoClass);
		return new ResponseEntity<DocExport>(docExport, HttpStatus.OK);
	}
	
	@RequestMapping(value ="/converstion/{code}", method = RequestMethod.GET)
	public ResponseEntity<UnitConverter> getConversionRate(@PathVariable("code") String code){
		UnitConverter uc = this.permitPeriodBizComp.getReferenceValue("UNIT_CONVERSION", code);
		
		   
		return new ResponseEntity<UnitConverter>(uc, HttpStatus.OK);
	}
}

package gov.hhs.fda.ctp.web.quarter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.validation.Valid;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp;
import gov.hhs.fda.ctp.common.beans.Assessment;
import gov.hhs.fda.ctp.common.beans.AssessmentFilter;
import gov.hhs.fda.ctp.common.beans.AssessmentReportStatus;
import gov.hhs.fda.ctp.common.beans.QtReconRptDetail;
import gov.hhs.fda.ctp.common.beans.ReportStatus;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.persistence.model.AssessmentEntity;
import gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity;

/**
 * The Class AssessmentRestController.
 *
 * @author rnasina
 */
//@CrossOrigin(origins = "http://localhost:9000")
@RestController
@RequestMapping("/api/reports/quarter")
public class AssessmentRestController {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(AssessmentRestController.class);

	/** The assessment biz comp. */
	@Autowired
	private AssessmentBizComp assessmentBizComp;

	/**
	 * Instantiates a new assessment rest controller.
	 *
	 * @param assess
	 *            the assess
	 */
	public AssessmentRestController(AssessmentBizComp assess) {
		this.assessmentBizComp = assess;
	}

	/**
	 * Creates a quarterly assessment.
	 *
	 * @param ae
	 *            the ae
	 * @return the response entity
	 */
	@RequestMapping(value = "create", method = RequestMethod.PUT, produces = { "application/json" })
	public ResponseEntity<String> createAssessment(@Valid @RequestBody AssessmentEntity ae) {
		ae.setAssessmentType("QTRY");
		String ppe = assessmentBizComp.addAssessment(ae);
		return new ResponseEntity<>(ppe, HttpStatus.OK);
	}

	/**
	 * Create a cigar assessment.
	 *
	 * @param ae the ae
	 * @return the response entity
	 */
	@RequestMapping(value = "cigar", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> createCigarAssessment(@Valid @RequestBody AssessmentEntity ae) {
		// CTPTUFA-5249 :cigar assessments generates for whole year.
		String ppe =  "0";
		ae.setAssessmentType("CIQT");
		for(int i=1 ;i<5;i++){
			ae.setAssessmentQtr(i);
			ppe = assessmentBizComp.addAssessment(ae);
			if(ppe == "0"){
				break;
			};
		}
		return new ResponseEntity<>(ppe, HttpStatus.OK);
	}

	/**
	 * Gets the assessment.
	 *
	 * @param id
	 *            the id
	 * @return the assessment
	 */
	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<AssessmentEntity> getAssessment(@PathVariable("id") long id) {
		AssessmentEntity ppe = assessmentBizComp.getAssessment(id);
		return new ResponseEntity<>(ppe, HttpStatus.OK);
	}

	/**
	 * Gets the assessments.
	 *
	 * @param filters
	 *            the filters
	 * @return the assessments
	 */
	@RequestMapping(value = "assessments", method = RequestMethod.PUT, produces = { "application/json" })
	public ResponseEntity<AssessmentFilter> getAssessments(@RequestBody AssessmentFilter filters) {
		List<Assessment> assessments = new ArrayList<>();

		List<AssessmentEntity> resultSet = assessmentBizComp.getAssessments(filters);
		for (Iterator<AssessmentEntity> iterator = resultSet.iterator(); iterator.hasNext();) {
			AssessmentEntity assessmentEntity = iterator.next();
			Assessment assessment = new Assessment();
			assessment.setAssessmentId(assessmentEntity.getAssessmentId());
			assessment.setAssessmentQtr(assessmentEntity.getAssessmentQtr());
			assessment.setAssessmentType(assessmentEntity.getAssessmentType());
			assessment.setAssessmentYr(assessmentEntity.getAssessmentYr());
			assessment.setSubmittedInd(assessmentEntity.getSubmittedInd());
			assessment.setCreatedDt(CTPUtil.parseDateToString(assessmentEntity.getCreatedDt()));
			// add to list
			assessments.add(assessment);
		}
		filters.setResults(assessments);
		return new ResponseEntity<>(filters, HttpStatus.OK);
	}

	/**
	 * Get a list of all the available fiscal years, in descending order.
	 *
	 * @return the available fiscal years
	 */
	@RequestMapping(value = "/fiscalyears", method = RequestMethod.GET)
	public ResponseEntity<?> getAvailableFiscalYears() {
		Set<String> years = assessmentBizComp.getFiscalYears();
		return ResponseEntity.ok(years);
	}

	/**
	 * Gets the available permit report fiscal years.
	 *
	 * @param yrId the yr id
	 * @param qtrId the qtr id
	 * @return the available permit report fiscal years
	 */
	@RequestMapping(value = "recon/fiscalyear/{yrId}/quarter/{qtrId}", method = RequestMethod.GET,produces = { "application/json" })
    public ResponseEntity<?> getAvailablePermitReportFiscalYears(@PathVariable("yrId") long yrId,@PathVariable("qtrId") long qtrId) {

		List<QtReconRptDetail> reconDetails= this.assessmentBizComp.getReconRptData(yrId,qtrId);
		return new ResponseEntity<>(reconDetails,HttpStatus.OK);
    }

	/**
	 * Gets the available permit report fiscal year.
	 *
	 * @param yrId the yr id
	 * @return the available permit report fiscal year
	 */
	@RequestMapping(value = "recon/fiscalyear/{yrId}", method = RequestMethod.GET,produces = { "application/json" })
    public ResponseEntity<?> getAvailablePermitReportFiscalYear(@PathVariable("yrId") long yrId) {

		List<QtReconRptDetail> reconDetails= this.assessmentBizComp.getReconRptDataForFiscalYear(yrId);
		return new ResponseEntity<>(reconDetails,HttpStatus.OK);
    }

	/**
	 * Gets the assessments.
	 *
	 * @param quarter the quarter
	 * @param year the year
	 * @return the assessments
	 */
	@RequestMapping(value = "/reportstatus/{quarter}/{year}", method = RequestMethod.GET)
	public ResponseEntity<ReportStatus> getAssessmentsReportStatus(@PathVariable("quarter") int quarter,
			@PathVariable("year") int year) {
		ReportStatus result = new ReportStatus();
		List<AssessmentReportStatus> assessmentStatuses = new ArrayList<>();

		Map<Long, AssessmentReportStatus> resultSet = assessmentBizComp.getReportStatusData(year, quarter);
		for (Entry<Long, AssessmentReportStatus> e : resultSet.entrySet()) {
			assessmentStatuses.add(e.getValue());
		}
		result.setResult(assessmentStatuses);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	/**
	 * Gets a list of assessment versions sorted by version descending
	 * (starting with 0 if available) with the exports attached sorted by tobacco type.
	 * Brings both quarterly and cigars assessments - For cigars qtr is passed as 0
	 * @param qtr the qtr
	 * @param fiscalYear the fiscal year
	 * @return List<Assessment>
	 */
	@RequestMapping(value="/export/{qtr}/{fiscalYear}", method=RequestMethod.GET, produces = {"application/json"})
	public ResponseEntity<Assessment> getAssessment(@PathVariable("qtr") int qtr, @PathVariable("fiscalYear") int fiscalYear) {
		// check for approval message back to the user
		String unapprovedReconString = assessmentBizComp.getUnapprovedReconRecordCount(fiscalYear, qtr);
		// get any previously generated market share reports
		Assessment assessment = null;
		if(qtr == 0){
			assessment = assessmentBizComp.getCigarAssessment(qtr, fiscalYear);
		}else{
			assessment = assessmentBizComp.getQuarterlyAssessment(qtr, fiscalYear);			
		}
		assessment.setApprovalMessage(unapprovedReconString);
		return new ResponseEntity<>(assessment, HttpStatus.OK);
	}

	@RequestMapping(value="/export/cigar/{fiscalYear}", method=RequestMethod.GET, produces = {"application/json"})
	public ResponseEntity<?> getAssessmentByYear(@PathVariable("fiscalYear") int fiscalYear) {
		// check for approval message back to the user
		String unapprovedReconString = assessmentBizComp.getUnapprovedReconRecordCount(fiscalYear, 0);
		// get any previously generated market share reports
		List<Assessment> assessmentList = assessmentBizComp.getAssessmentByYear(fiscalYear,unapprovedReconString);
		return new ResponseEntity<>(assessmentList, HttpStatus.OK);
	}
	/**
	 * Gets a list of assessment versions sorted by version descending
	 * (starting with 0 if available) with the exports attached sorted by tobacco type.
	 *
	 * @param qtr the qtr
	 * @param fiscalYear the fiscal year
	 * @return List<Assessment>
	 */
	@RequestMapping(value="/export/{qtr}/{fiscalYear}", method=RequestMethod.PUT, produces = {"application/json"})
	public ResponseEntity<Assessment> generateQuarterlyMarketShare(@PathVariable("qtr") int qtr, @PathVariable("fiscalYear") int fiscalYear) {
		// check for approval message back to the user
		String unapprovedReconString = assessmentBizComp.getUnapprovedReconRecordCount(fiscalYear, qtr);
		
		//check if annual true up has been submitted for the fiscal year
		String annualTrueUpSubmittedMsg = assessmentBizComp.getAnnualTrueUpSubmittedMessage(fiscalYear);
		
		// generate a market share (version 0) but don't submit anything.
			
		assessmentBizComp.generateQuarterlyMarketShare(qtr, fiscalYear, "N");
			
		Assessment assessment = assessmentBizComp.getQuarterlyAssessment(qtr, fiscalYear);
		assessment.setApprovalMessage(unapprovedReconString);
		assessment.setAnnualTrueUpSubmittedMsg(annualTrueUpSubmittedMsg);
		return new ResponseEntity<>(assessment, HttpStatus.OK);
	}

	@RequestMapping(value="/export/{qtr}/{fiscalYear}", method=RequestMethod.POST, produces = {"application/json"})
	public ResponseEntity<Assessment> submitQuarterlyAssessment(@PathVariable("qtr") int qtr, @PathVariable("fiscalYear") int fiscalYear) {
		// this time, if we have unapproved recons, throw exception.
		String unapprovedReconString = assessmentBizComp.getUnapprovedReconRecordCount(fiscalYear, qtr);
		if(unapprovedReconString != null) {
			throw new TufaException(unapprovedReconString);
		}
	
		assessmentBizComp.generateQuarterlyMarketShare(qtr, fiscalYear, "Y");
		
		Assessment assessment = assessmentBizComp.getQuarterlyAssessment(qtr, fiscalYear);
		return new ResponseEntity<>(assessment, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/export/{qtr}/{fiscalYear}/{assessmentQtr}/{type}", method=RequestMethod.PUT, produces = {"application/json"})
	public ResponseEntity<Assessment> generateCigarMarketShare(@PathVariable("qtr") int qtr, @PathVariable("fiscalYear") int fiscalYear, @PathVariable("assessmentQtr") int assessmentQtr,
			@PathVariable("type") String marketshareType) {
		
		//check if annual true up has been submitted for the fiscal year
		String annualTrueUpSubmittedMsg = assessmentBizComp.getAnnualTrueUpSubmittedMessage(fiscalYear);
				
		// generate a market share (version 0) but don't submit anything.
			
		assessmentBizComp.generateCigarMarketShare(qtr, fiscalYear, "N", assessmentQtr, marketshareType);
		Assessment assessment = assessmentBizComp.getCigarAssessment(assessmentQtr, fiscalYear);

		//Get the unapproved report count only for Full Market Share.
		if(!"ACONLY".equalsIgnoreCase(marketshareType)){
			String unapprovedReconString = assessmentBizComp.getUnapprovedReconRecordCount(fiscalYear, qtr);
			assessment.setApprovalMessage(unapprovedReconString);
		}
		//TO BE CHECKED IF THIS STILL HOLDS TRUE
		assessment.setAnnualTrueUpSubmittedMsg(annualTrueUpSubmittedMsg);
		return new ResponseEntity<>(assessment, HttpStatus.OK);
	}
	
	
	/**
	 * Submits the quarterly assessment, then returns the list of assessment
	 * versions sorted by version descending (starting with 0 if available)
	 * with the exports attached sorted by tobacco type.
	 *
	 * @param qtr
	 * @param fiscalYear
	 * @return
	 */
	@RequestMapping(value="/export/{qtr}/{fiscalYear}/{assessmentQtr}/{type}", method=RequestMethod.POST, produces = {"application/json"})
	public ResponseEntity<Assessment> submitCigarAssessment(@PathVariable("qtr") int qtr, @PathVariable("fiscalYear") int fiscalYear, @PathVariable("assessmentQtr") int assessmentQtr,
			@PathVariable("type") String marketshareType) {
		
		if(!"ACONLY".equalsIgnoreCase(marketshareType)){
			// this time, if we have unapproved recons, throw exception.
			String unapprovedReconString = assessmentBizComp.getUnapprovedReconRecordCount(fiscalYear, qtr);
			
			if(unapprovedReconString != null) {
				throw new TufaException(unapprovedReconString);
			}
		}
		// generate a new market share and promote it to the next version
		assessmentBizComp.generateCigarMarketShare(qtr, fiscalYear, "Y" , assessmentQtr, marketshareType);

		Assessment assessment = assessmentBizComp.getCigarAssessment(assessmentQtr, fiscalYear);
		return new ResponseEntity<>(assessment, HttpStatus.OK);
	}



	/**
	 * Save doc.
	 *
	 * @param supportDocument the support document
	 * @param assessId the assess id
	 * @return the response entity
	 */
	@RequestMapping(value="/docs/{assessId}", method=RequestMethod.PUT, produces = {"application/json"})
	public ResponseEntity<?> saveDoc(@RequestBody SupportingDocumentEntity supportDocument,
			@PathVariable("assessId") long assessId) {
		assessmentBizComp.saveSupportDocument(supportDocument);
		return new ResponseEntity<>(JSONObject.quote("Document Saved"), HttpStatus.OK);
	}

	/**
	 * Gets the supporting docs.
	 *
	 * @param assessId the assess id
	 * @return the supporting docs
	 */
	@RequestMapping(value="/{assessId}/docs", method=RequestMethod.GET, produces = {"application/json"})
	public ResponseEntity<?> getSupportingDocs(@PathVariable("assessId") long assessId) {
		List<SupportingDocument> docs = assessmentBizComp.getSupportDocs(assessId);
		return new ResponseEntity<>(docs, HttpStatus.OK);
	}

	/**
	 * Delete document.
	 *
	 * @param asmtdocId the asmtdoc id
	 * @return the response entity
	 */
	@RequestMapping(value="/docs/{asmtdocId}", method=RequestMethod.DELETE, produces = {"application/json"})
	public ResponseEntity<?> deleteDocument(@PathVariable("asmtdocId") long asmtdocId) {
		assessmentBizComp.deleteByDocId(asmtdocId);
		return new ResponseEntity<>(JSONObject.quote("Removed document"), HttpStatus.OK);
	}

	/**
	 * Gets the document.
	 *
	 * @param asmtdocId the asmtdoc id
	 * @return the document
	 */
	@RequestMapping(value="/docs/{asmtdocId}", method=RequestMethod.GET, produces = {"application/json"})
	public ResponseEntity<?> getDocument(@PathVariable("asmtdocId") long asmtdocId) {
		SupportingDocumentEntity doc = assessmentBizComp.getDocByDocId(asmtdocId);
		return new ResponseEntity<>(doc, HttpStatus.OK);
	}

}

package gov.hhs.fda.ctp.web.quarter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;

@RestController
@RequestMapping("/api/v1/recon")
public class ReconciliationReportRestController {

	/** The assessment biz comp. */
	@Autowired
	private AssessmentBizComp assessmentBizComp;


	@RequestMapping(value = "/fiscalyear/{yrId}/quarter/{qtrId}", method = RequestMethod.GET)
    public ResponseEntity<?> getAvailablePermitReportFiscalYears(@PathVariable("yrId") int yrId,@PathVariable("qtrId") int qtrId) {


        //return ResponseEntity.ok(years);
		return null;
    }

	/**
	 * Get the recon export data.
	 * @param year
	 * @param quarter
	 * @return
	 */
	@RequestMapping(value = "/export/fiscalyear/{year}/quarter/{quarter}", method = RequestMethod.GET)
	public ResponseEntity<?> getReconExport(@PathVariable("year") int year, @PathVariable("quarter") int quarter) {
		AssessmentExport export = assessmentBizComp.getReconExport(year, quarter);
		return new ResponseEntity<>(export, HttpStatus.OK);
	}

	/**
	 * Get the recon export data.
	 * @param year
	 * @param quarter
	 * @return
	 */
	@RequestMapping(value = "/export/fiscalyear/{year}", method = RequestMethod.GET)
	public ResponseEntity<?> getReconExportForFiscalYear(@PathVariable("year") int year) {
		AssessmentExport export = assessmentBizComp.getReconExportForFiscalYear(year);
		return new ResponseEntity<>(export, HttpStatus.OK);
	}
}

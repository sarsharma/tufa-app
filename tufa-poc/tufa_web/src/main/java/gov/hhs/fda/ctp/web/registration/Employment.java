package gov.hhs.fda.ctp.web.registration;

/**
 * The Class Employment.
 */
public class Employment {

	/** The company name. */
	private String companyName;
	
	/** The company address. */
	private String companyAddress;
	
	/**
	 * Instantiates a new employment.
	 */
	public Employment(){
		companyName = "";
		companyAddress = "";
	}
	
	/**
	 * Instantiates a new employment.
	 *
	 * @param companyName the company name
	 * @param companyAddress the company address
	 */
	public Employment(String companyName,String companyAddress){
		this.companyName = companyName;
		this.companyAddress = companyAddress;
	}

	/**
	 * Gets the company name.
	 *
	 * @return the company name
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the company name.
	 *
	 * @param companyName the new company name
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * Gets the company address.
	 *
	 * @return the company address
	 */
	public String getCompanyAddress() {
		return companyAddress;
	}

	/**
	 * Sets the company address.
	 *
	 * @param companyAddress the new company address
	 */
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	
	
}

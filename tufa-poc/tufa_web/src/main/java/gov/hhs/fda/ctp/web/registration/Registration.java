package gov.hhs.fda.ctp.web.registration;

/**
 * The Class Registration.
 */
public class Registration {

	/** The user. */
	private User user;
	
	/** The employment. */
	private Employment employment;
	
	/**
	 * Instantiates a new registration.
	 */
	Registration()
	{
		
	}
	
	/**
	 * Instantiates a new registration.
	 *
	 * @param user the user
	 * @param employment the employment
	 */
	public Registration(User user, Employment employment){
		this.user= user;
		this.employment = employment;
	}
	
	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	
	/**
	 * Sets the user.
	 *
	 * @param user the new user
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	/**
	 * Gets the employment.
	 *
	 * @return the employment
	 */
	public Employment getEmployment() {
		return employment;
	}
	
	/**
	 * Sets the employment.
	 *
	 * @param employment the new employment
	 */
	public void setEmployment(Employment employment) {
		this.employment = employment;
	}
	
	
}

package gov.hhs.fda.ctp.web.registration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.biz.registration.EmploymentDTO;
import gov.hhs.fda.ctp.biz.registration.RegistrationBizComp;
import gov.hhs.fda.ctp.biz.registration.RegistrationDTO;
import gov.hhs.fda.ctp.biz.registration.UserDTO;
import gov.hhs.fda.ctp.web.exception.UserNotFoundException;

/**
 * The Class RegistrationRestController.
 */
@RestController
@RequestMapping("/api/registration")
public class RegistrationRestController {
	
	@Autowired
	private RegistrationBizComp registrationBizModel;
	
	/**
	 * Instantiates a new registration rest controller.
	 */
	public RegistrationRestController(){
		// Empty constructor
	}
	
	/**
	 * Instantiates a new registration rest controller.
	 *
	 * @param registrationBizModel the registration biz model
	 */
	public RegistrationRestController(RegistrationBizComp registrationBizModel){
		this.registrationBizModel= registrationBizModel;
	}
	

	/**
	 * Register user.
	 *
	 * @param registration the registration
	 */
	@RequestMapping(value="/user",method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void registerUser(@RequestBody Registration registration){
		
		RegistrationDTO regisDTO = this.assembleRegistrationDTO(registration);
		registrationBizModel.registerUser(regisDTO);
		
	}
	
	/**
	 * Gets the user.
	 *
	 * @param id the id
	 * @return the user
	 */
	@RequestMapping(value="/reguser/{id}",method = RequestMethod.GET)
	public User getUser(@PathVariable String id){
		 
		if(("-1").equalsIgnoreCase(id)){
				throw new UserNotFoundException("User not found");
		}
		
		UserDTO userDTO= registrationBizModel.getRegisteredUser(id);
		return assembleUser(userDTO);
		 
	}
	

	/**
	 * Assemble registration DTO.
	 *
	 * @param registration the registration
	 * @return the registration DTO
	 */
	private RegistrationDTO assembleRegistrationDTO(Registration registration){
		
		UserDTO userDTO = new UserDTO(registration.getUser().getEmail(),registration.getUser().getPassword());
		EmploymentDTO employeeDTO = new EmploymentDTO(registration.getEmployment().getCompanyName(),registration.getEmployment().getCompanyAddress());
		
		return new RegistrationDTO(userDTO,employeeDTO);
	}
	
	/**
	 * Assemble user.
	 *
	 * @param userDTO the user DTO
	 * @return the user
	 */
	private User assembleUser(UserDTO userDTO){
		
		User u = new User();
		u.setEmail(userDTO.getEmail());
		return u;
	}
}

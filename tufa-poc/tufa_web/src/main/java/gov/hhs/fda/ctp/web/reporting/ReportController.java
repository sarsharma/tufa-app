
package gov.hhs.fda.ctp.web.reporting;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import gov.hhs.fda.ctp.biz.reporting.AttachmentsBizComp;
import gov.hhs.fda.ctp.common.beans.Attachment;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonSummary;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.PermitAttachmentsEntity;

/**
 * The Class ReportController.
 *
 * @author rnasina
 */
// @CrossOrigin
@RestController
@RequestMapping("/api/reports")
public class ReportController {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(ReportController.class);

	/** The attach biz comp. */
	@Autowired
	private AttachmentsBizComp attachBizComp;
	
	@Autowired
	private SecurityContextUtil securityContextUtil;

	/**
	 * Instantiates a new report controller.
	 *
	 * @param attachmentsBizComp
	 *            the attachments biz comp
	 */
	public ReportController(AttachmentsBizComp attachmentsBizComp) {
		this.attachBizComp = attachmentsBizComp;
	}

	/**
	 * Read all attachments.
	 *
	 * @return the response entity
	 */
	@RequestMapping(value = "attachs/list", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<?> read() {
		List<AttachmentsEntity> attachs = attachBizComp.findAll();
		return ResponseEntity.ok(attachs);
	}

	/**
	 * Read all attachments associated to period.
	 *
	 * @param periodId
	 *            the period id
	 * @param permitId
	 *            the permit id
	 * @return the attachments
	 */
	@RequestMapping(value = "attachs/{id}/{permitId}", method = RequestMethod.GET)
	public ResponseEntity<?> getAttachments(@PathVariable("id") long periodId,
			@PathVariable("permitId") long permitId) {
		List<AttachmentsEntity> attachs = attachBizComp.findByIds(periodId, permitId);
		return ResponseEntity.ok(attachs);
	}

	/**
	 * Adds the attachments.
	 *
	 * @param attachs
	 *            the attachs
	 * @param periodId
	 *            the period id
	 * @param permitId
	 *            the permit id
	 * @return the response entity
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 * @throws SQLException
	 */
	@RequestMapping(value = "attachs/{id}/{permitId}", method = RequestMethod.POST)
	public ResponseEntity<String> addAttachments(@RequestParam("file") MultipartFile file,
			@RequestParam("metaData") String metaData, @PathVariable("id") long periodId,
			@PathVariable("permitId") long permitId)
			throws JsonParseException, JsonMappingException, IOException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		AttachmentsEntity attachs = mapper.readValue(metaData, AttachmentsEntity.class);
		attachBizComp.addAttach(attachs, file);
		return new ResponseEntity<>(JSONObject.quote("Successfully Uploaded"), HttpStatus.OK);
	}
	
	@RequestMapping(value = "update/attachs/{id}/{permitId}", method = RequestMethod.POST)
	public ResponseEntity<String> updateAttachment(@Valid @RequestBody AttachmentsEntity attachs, 
			@PathVariable("id") long periodId, @PathVariable("permitId") long permitId)
			throws IOException, SQLException {
		attachBizComp.addAttach(attachs, null);
		return new ResponseEntity<>(JSONObject.quote("Successfully Updated"), HttpStatus.OK);
	}

	/**
	 * Delete attachment.
	 *
	 * @param docId
	 *            the doc id
	 * @return the response entity
	 */
	@RequestMapping(value = "attachs/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteAttachment(@PathVariable("id") long docId) {
		attachBizComp.deleteById(docId,"");
		return new ResponseEntity<>(JSONObject.quote("Successfully Deleted"), HttpStatus.OK);
	}

	@RequestMapping(value = "view/attachs/{id}", method = RequestMethod.GET)
	public @ResponseBody void viewAttachment(@PathVariable("id") long docId, HttpServletResponse response,
			HttpServletRequest request) throws SQLException, IOException {

		byte[] attachByteArry = attachBizComp.getAttachmentAsByteArray(docId,"");

		String pdfDataStr = new String(attachByteArry);
		String[] pdfDataArry = pdfDataStr.split("[:;,]");
		if (pdfDataArry.length == 4) {
			byte[] decodedFileCntByteArray = Base64.decode(pdfDataArry[3].getBytes());
			InputStream fileInStream = new ByteArrayInputStream(decodedFileCntByteArray);
			IOUtils.copy(fileInStream, response.getOutputStream());
			response.setContentType("application/pdf");
			response.setContentLength(attachByteArry.length);
			response.flushBuffer();
		} else {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
	}

	@RequestMapping(value = "attachs/{id}", method = RequestMethod.GET)
	public ResponseEntity<Attachment> downloadAttachment(@PathVariable("id") long docId) throws SQLException {
		Attachment attachment = attachBizComp.getAttachmentDownload(docId);
		return new ResponseEntity<>(attachment, HttpStatus.OK);
	}
	
	@RequestMapping(value = "permit/attachs/permitId/{permitId}", method = RequestMethod.GET)
	public ResponseEntity<?> getPermitAttachments(@PathVariable("permitId") long permitId) throws SQLException {
		List<PermitAttachmentsEntity> attachs = attachBizComp.findPermitDocbyId(permitId);
		return new ResponseEntity<>(attachs, HttpStatus.OK);
	}
	
	@RequestMapping(value = "permit/attachs/{id}", method = RequestMethod.GET)
	public ResponseEntity<Attachment> downloadPermitAttachment(@PathVariable("id") long docId) throws SQLException {
		Attachment attachment = attachBizComp.getPermitAttachmentDownload(docId);
		return new ResponseEntity<>(attachment, HttpStatus.OK);
	}
	@RequestMapping(value = "view/permit/attachs/{id}", method = RequestMethod.GET)
	public @ResponseBody void viewPermitAttachment(@PathVariable("id") long docId, HttpServletResponse response,
			HttpServletRequest request) throws SQLException, IOException {

		byte[] attachByteArry = attachBizComp.getAttachmentAsByteArray(docId,"permit");

		String pdfDataStr = new String(attachByteArry);
		String[] pdfDataArry = pdfDataStr.split("[:;,]");
		if (pdfDataArry.length == 4) {
			byte[] decodedFileCntByteArray = Base64.decode(pdfDataArry[3].getBytes());
			InputStream fileInStream = new ByteArrayInputStream(decodedFileCntByteArray);
			IOUtils.copy(fileInStream, response.getOutputStream());
			response.setContentType("application/pdf");
			response.setContentLength(attachByteArry.length);
			response.flushBuffer();
		} else {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "permit/attachs/", method = RequestMethod.POST)
	public ResponseEntity<String> addPermitAttachments(@RequestParam("file") MultipartFile file,
			@RequestParam("metaData") String metaData)
			throws JsonParseException, JsonMappingException, IOException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		PermitAttachmentsEntity attachs = mapper.readValue(metaData, PermitAttachmentsEntity.class);
		attachs.setCreatedDt(new Date());
		attachs.setCreatedBy((securityContextUtil.getPrincipal() != null)? securityContextUtil.getPrincipal().getUsername() : null);
		attachBizComp.addPermitAttach(attachs, file);
		return new ResponseEntity<>(JSONObject.quote("Successfully Uploaded"), HttpStatus.OK);
	}
	
	@RequestMapping(value = "permit/attachs/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deletePermitAttachment(@PathVariable("id") long docId) {
		attachBizComp.deleteById(docId,"permit");
		return new ResponseEntity<>(JSONObject.quote("Successfully Deleted"), HttpStatus.OK);
	}
	
}

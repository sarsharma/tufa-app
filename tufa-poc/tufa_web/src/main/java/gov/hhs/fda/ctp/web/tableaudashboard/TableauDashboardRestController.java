/**
 *
 */
package gov.hhs.fda.ctp.web.tableaudashboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.biz.tableaudashboard.TableauDashboardBizComp;
import gov.hhs.fda.ctp.common.beans.TableauDashboardRptType;
import gov.hhs.fda.ctp.common.exception.TufaException;

/**
 * The Class TableauDashboardRestController.
 *
 * @author akari This controller is responsible for CRUD operations for Company
 *         Address
 */
@PropertySource({ "classpath:dashboard.properties" })
@RestController
@RequestMapping("/api/v1/dashboard")
public class TableauDashboardRestController {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(TableauDashboardRestController.class);
	
	@Autowired
	private TableauDashboardBizComp tableauDashboardBizComp;

	@Autowired
	private Environment env;
	
	public TableauDashboardRestController() {
	}

	@RequestMapping(value = "ticket", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> getTicket() {
		String server = env.getProperty("server");
		String user = env.getProperty("user");
		String remoteAddr = env.getProperty("remoteaddress");
		String ticket = getTrustedTicket(server, user, remoteAddr);
    	return new ResponseEntity<String>(ticket, HttpStatus.OK);
	}

    // the client_ip parameter isn't necessary to send in the POST unless you have
    // wgserver.extended_trusted_ip_checking enabled (it's disabled by default)
    private String getTrustedTicket(String wgserver, String user, String remoteAddr) 
    {
        OutputStreamWriter out = null;
        BufferedReader in = null;
        try {
            // Encode the parameters
            StringBuffer data = new StringBuffer();
            data.append(URLEncoder.encode("username", "UTF-8"));
            data.append("=");
            data.append(URLEncoder.encode(user, "UTF-8"));
            data.append("&");
            data.append(URLEncoder.encode("client_ip", "UTF-8"));
            data.append("=");
            data.append(URLEncoder.encode(remoteAddr, "UTF-8"));
            
            // Send the request
            URL url = new URL("http://" + wgserver + "/trusted");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            out = new OutputStreamWriter(conn.getOutputStream());
            out.write(data.toString());
            out.flush();
            
            // Read the response
            StringBuffer rsp = new StringBuffer();
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ( (line = in.readLine()) != null) {
                rsp.append(line);
            }
            
            return rsp.toString();
            
        } catch (Exception e) {
            throw new TufaException(e);
        }
        finally {
            try {
                if (in != null) in.close();
                if (out != null) out.close();
            }
            catch (IOException e) {}
        }
    }
    
    
	@RequestMapping(value = "/reporttypes", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<?> getDashBoardReportTypes(){
		List<TableauDashboardRptType> rptTypes =  tableauDashboardBizComp.getDashboardReportTypes();
		 return new ResponseEntity(rptTypes, HttpStatus.OK);
    }

	public TableauDashboardBizComp getTableauDashboardBizComp() {
		return tableauDashboardBizComp;
	}

	public void setTableauDashboardBizComp(TableauDashboardBizComp tableauDashboardBizComp) {
		this.tableauDashboardBizComp = tableauDashboardBizComp;
	}
}

/**
 * 
 */
package gov.hhs.fda.ctp.web.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.biz.test.TestBizComp;

/**
 * @author tgunter
 *
 */
@RestController
@RequestMapping("/api/test")
public class TestRestController {
	/** The logger. */
	Logger logger = LoggerFactory.getLogger(TestBizComp.class);
	/** The test biz comp. */
	@Autowired
	private TestBizComp testBizComp;
	
	@RequestMapping(value = "/teardown/{companyId}", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> tearDown(@PathVariable("companyId") int companyId) {
		logger.debug("tearing down test data form companyId={}", companyId);
		String response = "OK";
		testBizComp.tearDown(companyId);
		return ResponseEntity.ok(response);
	}
	
	@RequestMapping(value = "/teardown/company/{companyName}", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> tearDown(@PathVariable("companyName") String companyName) {
		logger.debug("tearing down test data form companyName={}", companyName);
		String response = "OK";
		testBizComp.tearDownByCompanyName(companyName);
		return ResponseEntity.ok(response);
	}	
}
package gov.hhs.fda.ctp.web.tobaccoclassmgmt;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.biz.tobaccoclassmgmt.TobaccoClassBizComp;
import gov.hhs.fda.ctp.persistence.model.TobaccoClassEntity;

@RestController
@RequestMapping("/api/v1/tobacco")
public class TobaccoClassRestController {
	
	@Autowired
	TobaccoClassBizComp biz;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<TobaccoClassEntity> getTobaccoClasses() {
		return this.biz.getTobaccoClasses();
	}
}

package gov.hhs.fda.ctp.web.trueup;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.biz.trueup.TrueUpBizComp;
import gov.hhs.fda.ctp.common.beans.AnnImporterGrid;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.Attachment;
import gov.hhs.fda.ctp.common.beans.CBPAmendment;
import gov.hhs.fda.ctp.common.beans.CBPEntry;
import gov.hhs.fda.ctp.common.beans.CBPImporter;
import gov.hhs.fda.ctp.common.beans.CBPIngestestedSummaryDetailsData;
import gov.hhs.fda.ctp.common.beans.CBPLineEntryUpdateInfo;
import gov.hhs.fda.ctp.common.beans.CBPMetrics;
import gov.hhs.fda.ctp.common.beans.CBPQtrComment;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.ComparisonAllDeltaComments;
import gov.hhs.fda.ctp.common.beans.ComparisonAllDeltaStatus;
import gov.hhs.fda.ctp.common.beans.DocExport;
import gov.hhs.fda.ctp.common.beans.ImporterTufaIngestionDetails;
import gov.hhs.fda.ctp.common.beans.IngestionWorkflowContext;
import gov.hhs.fda.ctp.common.beans.ManufactureQuarterDetail;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PreviousTrueupInfo;
import gov.hhs.fda.ctp.common.beans.TTBAmendment;
import gov.hhs.fda.ctp.common.beans.TTBQtrComment;
import gov.hhs.fda.ctp.common.beans.TobaccoTypeTTBVol;
import gov.hhs.fda.ctp.common.beans.TrueUp;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionHTSCodeSummary;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonSummary;
import gov.hhs.fda.ctp.common.beans.TrueUpDocument;
import gov.hhs.fda.ctp.common.beans.TrueUpErrorsExport;
import gov.hhs.fda.ctp.common.beans.TrueUpFilter;
import gov.hhs.fda.ctp.common.beans.UnknownCompanyBucket;
import gov.hhs.fda.ctp.common.exception.CreateCompanyException;
import gov.hhs.fda.ctp.common.exception.TufaException;
import gov.hhs.fda.ctp.common.pagination.PaginationAttributes;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.model.ComparisonAllDeltaStatusEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonCommentAttachmentEntity;
import gov.hhs.fda.ctp.persistence.model.HTSCodeEntity;
import gov.hhs.fda.ctp.persistence.model.PermitCommentAttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.TTBPermitBucket;
import gov.hhs.fda.ctp.persistence.model.TrueUpDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpSubDocumentEntity;
import gov.hhs.fda.ctp.web.quarter.AssessmentRestController;
import gov.hhs.fda.ctp.web.trueup.compare.ATCompare;
import gov.hhs.fda.ctp.web.trueup.ingestion.ATFileIngestion;

/**
 * @author tgunter
 *
 */
// @CrossOrigin
@RestController
@RequestMapping("/api/v1/trueup")
public class TrueUpRestController {

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(AssessmentRestController.class);

	/** The assessment biz comp. */
	@Autowired
	private TrueUpBizComp trueUpBizComp;

	@Autowired
	private PaginationAttributes paginationAttributes;

	/** The company mgmt biz comp. */
	@Autowired
	private CompanyMgmtBizComp companyMgmtBizComp;
	
	@Autowired 
	private ATFileIngestion fileIngestor;
	
	@Autowired
	private ATCompare atCompare;
	
	@Autowired
	private SecurityContextUtil securityContextUtil;


	/**
	 * Create TrueUp.
	 *
	 * @param TrueUpEntity
	 * @return (String)result
	 */
	@RequestMapping(method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> createTrueUp(@Valid @RequestBody TrueUpEntity trueUpEntity) {
		String result = trueUpBizComp.createTrueUp(trueUpEntity);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "{fiscalYear}", method = RequestMethod.GET)
	public ResponseEntity<?> getTrueUp(@PathVariable long fiscalYear) {
		return new ResponseEntity<>(trueUpBizComp.getTrueUpByYear(fiscalYear), HttpStatus.OK);
	}

	/**
	 * Gets the true-ups.
	 *
	 * @param filters
	 * @return the trueups
	 */
	@RequestMapping(value = "list", method = RequestMethod.PUT, produces = { "application/json" })
	public ResponseEntity<TrueUpFilter> getAssessments(@RequestBody TrueUpFilter filters) {
		List<TrueUp> resultSet = trueUpBizComp.getTrueUps(filters);
		filters.setResults(resultSet);
		return new ResponseEntity<>(filters, HttpStatus.OK);
	}

	@RequestMapping(value = "/ingestfile/{fiscalYear}", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<?> ingestFileUpload(@RequestParam("trueupDocument") MultipartFile file,
			@RequestParam("fileType") String fileType, @RequestParam("fiscalYear") String fiscalYear) throws Exception {
		TrueUpDocument trueupdocument = fileIngestor.ingestFile(file, fileType, fiscalYear);
		return new ResponseEntity<>(trueupdocument, HttpStatus.OK);
	}

	@RequestMapping(value = "{fiscalYear}/documents", method = RequestMethod.GET)
	public ResponseEntity<?> getTrueUpDocuments(@PathVariable long fiscalYear) {
		List<TrueUpDocumentEntity> resultSet = this.trueUpBizComp.getTrueupDocuments(fiscalYear);

		List<TrueUpDocument> trueupdocuments = new ArrayList<>();
		for (Iterator<TrueUpDocumentEntity> iterator = resultSet.iterator(); iterator.hasNext();) {
			TrueUpDocumentEntity trueupdocumentEntity = iterator.next();
			TrueUpDocument trueupdocument = new TrueUpDocument();
			trueupdocument.setTrueUpFilename(trueupdocumentEntity.getTrueUpFilename());
			trueupdocument.setDocDesc(trueupdocumentEntity.getDocDesc());
			trueupdocument.setCreatedBy(trueupdocumentEntity.getCreatedBy());
			trueupdocument.setCreatedDt(CTPUtil.parseDateToString(trueupdocumentEntity.getCreatedDt()));
			trueupdocument.setModifiedBy(trueupdocumentEntity.getModifiedBy());
			trueupdocument.setModifiedDt(CTPUtil.parseDateToString(trueupdocumentEntity.getModifiedDt()));
			// add to list
			trueupdocuments.add(trueupdocument);
		}
		return new ResponseEntity<>(trueupdocuments, HttpStatus.OK);
	}

	@RequestMapping(value = "htscodes", method = RequestMethod.GET)
	public ResponseEntity<?> getHtsCodes() {
		List<HTSCodeEntity> codes = this.trueUpBizComp.getHtsCodes();
		return new ResponseEntity<>(codes, HttpStatus.OK);
	}

	@RequestMapping(value = "htscodes", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> saveOrUpdateCode(@Valid @RequestBody HTSCodeEntity code) {
		if (trueUpBizComp.getCodeById(code.getHtsCode()) != null && code.getHtsCodeId() == null) {
			throw new TufaException("HTS code '" + code.getHtsCode() + "' already exists.");
		}
		this.trueUpBizComp.saveOrEditCodes(code);
		return new ResponseEntity<>(JSONObject.quote("Successfully saved"), HttpStatus.OK);
	}

	@RequestMapping(value = "{fiscalYear}/errors/{type}", method = RequestMethod.GET)
	public ResponseEntity<TrueUpErrorsExport> getTrueUpErrors(@PathVariable long fiscalYear,
			@PathVariable String type) {
		if ("undefined".equals(type)) {
			type = Constants.TTB_VOLUME;
		}
		TrueUpErrorsExport export = trueUpBizComp.getTrueUpErrors(fiscalYear, type);
		return new ResponseEntity<TrueUpErrorsExport>(export, HttpStatus.OK);
	}

	/**
	 * Get the list of company comparisons for CBP ingestion.
	 * 
	 * @param fiscalYear
	 * @return
	 */
	@RequestMapping(value = "bucket/company/{fiscalYear}", method = RequestMethod.GET)
	public ResponseEntity<?> getCompanyComparisonBucket(@PathVariable long fiscalYear) {
		List<CBPImporter> bucket = trueUpBizComp.getCompanyComparisonBucket(fiscalYear);
		return new ResponseEntity<>(bucket, HttpStatus.OK);
	}

	/**
	 * Get the list of company comparisons for CBP ingestion.
	 * 
	 * @param fiscalYear
	 * @return
	 */
	@RequestMapping(value = "bucket/unkncompany/{fiscalYear}", method = RequestMethod.GET)
	public ResponseEntity<?> getUnknownCompanyBucket(@PathVariable long fiscalYear) {
		List<UnknownCompanyBucket> bucket = trueUpBizComp.getUnknownCompanies(fiscalYear);
		return new ResponseEntity<>(bucket, HttpStatus.OK);
	}

	/**
	 * Update the choices made during company comparison for CBP ingestion.
	 * 
	 * @param bucket
	 * @return
	 */
	@RequestMapping(value = "bucket/company", method = RequestMethod.PUT)
	public ResponseEntity<String> updateCompanyComparisonBucket(@RequestBody List<CBPImporter> bucket) {
		trueUpBizComp.updateCompanyComparisonBucket(bucket);
		return new ResponseEntity<>(JSONObject.quote("OK"), HttpStatus.OK);
	}

	@RequestMapping(value = "/docs/{fiscalYear}", method = RequestMethod.PUT, produces = { "application/json" })
	public ResponseEntity<?> saveDoc(@RequestBody TrueUpSubDocumentEntity supportDocument,
			@PathVariable("fiscalYear") long fiscalYr) {
		trueUpBizComp.saveSupportDocument(supportDocument);
		return new ResponseEntity<>(JSONObject.quote("Document Saved"), HttpStatus.OK);
	}

	@RequestMapping(value = "/{fiscalYear}/docs", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<?> getSupportingDocs(@PathVariable("fiscalYear") long fiscalYr) {
		List<TrueUpSubDocumentEntity> docs = trueUpBizComp.getSupportDocs(fiscalYr);
		return new ResponseEntity<>(docs, HttpStatus.OK);
	}

	@RequestMapping(value = "/docs/{docId}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<?> getDocument(@PathVariable("docId") long docId) {
		TrueUpSubDocumentEntity doc = trueUpBizComp.getDocByDocId(docId);
		return new ResponseEntity<>(doc, HttpStatus.OK);
	}

	// Returns comparison results for a fiscal year
	@RequestMapping(value = "compare/results/{fiscalYear}", method = RequestMethod.PUT)
	public ResponseEntity<?> getComparisonResults(@PathVariable long fiscalYear,
			@RequestBody TrueUpCompareSearchCriteria criteria) {
		logger.debug("getComparisonResults() :: entering rest controller");
		Map<String, Object> resultsMap  = atCompare.getComparisonResults(fiscalYear, criteria);
		logger.debug("getComparisonResults() :: leaving rest controller");
		return new ResponseEntity<>(resultsMap, HttpStatus.OK);
	}

	// Returns comparison results for a fiscal year
	@RequestMapping(value = "compare/results/all/{fiscalYear}", method = RequestMethod.PUT)
	public ResponseEntity<?> getAllComparisonResults(@PathVariable long fiscalYear,
			@RequestBody TrueUpCompareSearchCriteria criteria) {

		logger.debug("getAllComparisonResults() :: entering rest controller");
		Map<String, Object> resultsMap  = atCompare.getAllComparisonResults(fiscalYear, criteria);
		logger.debug("getAllComparisonResults() :: leaving rest controller");
		return new ResponseEntity<>(resultsMap, HttpStatus.OK);
	}

	// Returns cigar fda3852 tax summary by quarters
	@RequestMapping(value = "compare/manu/cigars/summary/{companyId}/{fiscalYear}", method = RequestMethod.GET)
	public ResponseEntity<?> getCigarManufacturerComparisonSummary(@PathVariable("companyId") long companyId,
			@PathVariable long fiscalYear) {
		List<TrueUpComparisonSummary> summary = trueUpBizComp.getCigarComparisonSummary(companyId, fiscalYear, "MANU");
		return new ResponseEntity<>(summary, HttpStatus.OK);
	}

	// Returns fda3852 tax summary for tobacco class by permit total for the
	// quarter
	// cigarettes - separately ryo/pipe - combined chew/snuff-combine
	@RequestMapping(value = "compare/manu/noncigars/summary/{companyId}/{fiscalYear}/{quarter}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<?> getNonCigarManufacturerComparisonSummary(@PathVariable("companyId") long companyId,
			@PathVariable long fiscalYear, @PathVariable long quarter, @PathVariable String tobaccoClass) {
		List<TrueUpComparisonSummary> summary = trueUpBizComp.getNonCigarComparisonSummary(companyId, fiscalYear,
				quarter, "MANU", tobaccoClass);
		return new ResponseEntity<>(summary, HttpStatus.OK);
	}
	
	@RequestMapping(value = "compare/impt/summary/{companyId}/{fiscalYear}/{quarter}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<?> getImporterComparisonSummary(@PathVariable("companyId") long companyId,
			@PathVariable long fiscalYear, @PathVariable long quarter, @PathVariable String tobaccoClass) {
		AnnImporterGrid summary = trueUpBizComp.getImporterComparisonSummary(companyId, fiscalYear,
				quarter, tobaccoClass);
		return new ResponseEntity<>(summary, HttpStatus.OK);
	}

	@RequestMapping(value = "comparealldeltas/impt/review/{einType}/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<?> getImporterSummDetailData(@PathVariable("einType") String einType, @PathVariable("ein") String ein,
			@PathVariable long fiscalYear, @PathVariable String quarter, @PathVariable String tobaccoClass) {

		CBPIngestestedSummaryDetailsData reviewdetails = trueUpBizComp.getImporterSummDetailData(einType, ein,
				fiscalYear, quarter, "IMPT", tobaccoClass);

		return new ResponseEntity<>(reviewdetails, HttpStatus.OK);
	}

	// Returns cigar fda3852 tax summary by quarters
	@RequestMapping(value = "compare/impt/cigars/details/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<?> getCigarImporterComparisonDetails(@PathVariable("ein") String ein,
			@PathVariable long fiscalYear, @PathVariable long quarter, @PathVariable String tobaccoClass) {
		List<TrueUpComparisonImporterDetail> summary = trueUpBizComp.getImporterComparisonDetails(ein, fiscalYear,
				quarter, "IMPT", tobaccoClass);
		return new ResponseEntity<>(summary, HttpStatus.OK);
	}

	// Returns cigar ingested tax details by quarters
	@RequestMapping(value = "compare/impt/cigars/ingestdetails/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<?> getCigarImporterComparisonIngestionDetails(@PathVariable("ein") String ein,
			@PathVariable long fiscalYear, @PathVariable long quarter, @PathVariable String tobaccoClass) {
		List<TrueUpComparisonImporterIngestionDetail> summary = trueUpBizComp
				.getImporterComparisonIngestionDetails(ein, fiscalYear, quarter,"IMPT", tobaccoClass);
		return new ResponseEntity<>(summary, HttpStatus.OK);
	}

	@RequestMapping(value = "compare/impt/cigars/ingesthtscodes/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<?> getCigarImporterComparisonIngestionHTSCodeSummary(
			@PathVariable("ein") String ein, @PathVariable long fiscalYear, @PathVariable long quarter,
			@PathVariable String tobaccoClass) {
		List<TrueUpComparisonImporterIngestionHTSCodeSummary> summary = trueUpBizComp
				.getImporterComparisonIngestionHTSCodeSummary(ein, fiscalYear, quarter, "IMPT", tobaccoClass);
		return new ResponseEntity<>(summary, HttpStatus.OK);
	}

	// Returns fda3852 tax details for tobacco class by permit total for the
	// quarter
	// all non-cigar tobacco classes
	@RequestMapping(value = "compare/impt/noncigars/details/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<?> getNonCigarImporterComparisonDetails(@PathVariable("ein") String ein,
			@PathVariable long fiscalYear, @PathVariable long quarter, @PathVariable String tobaccoClass) {
		List<TrueUpComparisonImporterDetail> summary = trueUpBizComp.getImporterComparisonDetails(ein, fiscalYear,
				quarter, "IMPT", tobaccoClass);
		return new ResponseEntity<>(summary, HttpStatus.OK);
	}

	// Returns fda3852 tax details for tobacco class by permit total for the
	// quarter
	// all non-cigar tobacco classes
	@RequestMapping(value = "compare/impt/noncigars/ingestdetails/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<?> getNonCigarImporterComparisonIngestionDetails(@PathVariable("ein") String ein,
			@PathVariable long fiscalYear, @PathVariable long quarter, @PathVariable String tobaccoClass) {
		List<TrueUpComparisonImporterIngestionDetail> summary = trueUpBizComp
				.getImporterComparisonIngestionDetails(ein, fiscalYear, quarter, "IMPT", tobaccoClass);
		return new ResponseEntity<>(summary, HttpStatus.OK);
	}

	@RequestMapping(value = "compare/impt/noncigars/ingesthtscodes/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<?> getNonCigarImporterComparisonIngestionHTSCodeSummary(
			@PathVariable("ein") String ein, @PathVariable long fiscalYear, @PathVariable long quarter,
			@PathVariable String tobaccoClass) {
		List<TrueUpComparisonImporterIngestionHTSCodeSummary> summary = trueUpBizComp
				.getImporterComparisonIngestionHTSCodeSummary(ein, fiscalYear, quarter, "IMPT", tobaccoClass);
		return new ResponseEntity<>(summary, HttpStatus.OK);
	}

	// Returns cigar fda3852 tax summary by quarters
	@RequestMapping(value = "compare/manu/cigars/details/{companyId}/{fiscalYear}/{quarter}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<?> getCigarManufacturerComparisonDetails(@PathVariable("companyId") long companyId,
			@PathVariable long fiscalYear, @PathVariable long quarter, @PathVariable String tobaccoClass) {
		List<TrueUpComparisonManufacturerDetail> summary = trueUpBizComp.getManufacturerComparisonDetails(companyId,
				fiscalYear, quarter, "MANU", tobaccoClass);
		return new ResponseEntity<>(summary, HttpStatus.OK);
	}

	// Returns fda3852 tax details for tobacco class by permit total for the
	// quarter
	// cigarettes - separately ryo/pipe - combined chew/snuff-combine
	@RequestMapping(value = "compare/manu/noncigars/details/{companyId}/{fiscalYear}/{quarter}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<?> getNonCigarManufacturerComparisonDetails(@PathVariable("companyId") long companyId,
			@PathVariable long fiscalYear, @PathVariable long quarter, @PathVariable String tobaccoClass) {
		List<TrueUpComparisonManufacturerDetail> summary = trueUpBizComp.getManufacturerComparisonDetails(companyId,
				fiscalYear, quarter, "MANU", tobaccoClass);
		return new ResponseEntity<>(summary, HttpStatus.OK);
	}
	
	@RequestMapping(value = "compare/manu/details/{companyId}/{fiscalYear}/{quarter}/{tobaccoClass}", method = RequestMethod.GET)
	public ResponseEntity<?> getManufacturerComparisonDetails(@PathVariable("companyId") long companyId,
			@PathVariable long fiscalYear, @PathVariable long quarter, @PathVariable String tobaccoClass) {
		ManufactureQuarterDetail summary = trueUpBizComp.getManufacturerComparisonDetails(companyId,
				fiscalYear, quarter, tobaccoClass);
		return new ResponseEntity<>(summary, HttpStatus.OK);
	}

	@RequestMapping(value = "/export/fiscalyear/{year}/quarter/{quarter}/tobaccoClass/{tobaccoClass}/CompanyId/{ein}", method = RequestMethod.GET)
	public ResponseEntity<?> getCBPExport(@PathVariable("year") int year, @PathVariable("quarter") int quarter,
			@PathVariable("tobaccoClass") String tobaccoClass, @PathVariable("ein") String ein) {
		AssessmentExport export = trueUpBizComp.getCBPExport(year, quarter, tobaccoClass, ein);
		return new ResponseEntity<>(export, HttpStatus.OK);
	}

	@RequestMapping(value = "/exportNewCBPCompany/fiscalyear/{year}", method = RequestMethod.GET)
	public ResponseEntity<?> getexportNewCBPCompany(@PathVariable("year") int year) throws SQLException, IOException {
		AssessmentExport export = trueUpBizComp.getCBPNewCompanyExport(year);
		return new ResponseEntity<>(export, HttpStatus.OK);
	}

//	private String generateIngestionFile(MultipartFile file) throws IOException {
//
//		byte[] fileBytes = file.getBytes();
//		String fileName = file.getOriginalFilename();
//		File tmpFile = null;
//
//		tmpFile = File.createTempFile(FilenameUtils.getBaseName(fileName), FilenameUtils.getExtension(fileName));
//		tmpFile.deleteOnExit();
//
//		OutputStream outStream = new FileOutputStream(tmpFile);
//		outStream.write(fileBytes);
//		outStream.close();
//
//		return tmpFile.getAbsolutePath();
//	}
//
//	private void deleteGeneratedIngestionFile(String filePath) throws IOException {
//		Path fileP = Paths.get(filePath);
//		Files.deleteIfExists(fileP);
//	}

	/**
	 *
	 * @param fiscalYear
	 * @return
	 */
	@RequestMapping(value = "bucket/htsmissing/{fiscalYear}", method = RequestMethod.GET)
	public ResponseEntity<?> getHTSMissingAssociations(@PathVariable long fiscalYear) {
		List<CBPEntry> missingHTS = trueUpBizComp.getHTSMissingAssociationData(fiscalYear);
		return new ResponseEntity<>(missingHTS, HttpStatus.OK);
	}

	/**
	 *
	 * @param fiscalYear
	 * @return
	 */
	@RequestMapping(value = "bucket/htsmissing/{fiscalYear}", method = RequestMethod.PUT)
	public ResponseEntity<?> saveHTSMissingAssociations(@RequestBody List<CBPEntry> cbpEntries,
			@PathVariable long fiscalYear) {
		List<CBPEntry> returnList = trueUpBizComp.updateAssociateHTSMissingBucket(cbpEntries, fiscalYear);
		return new ResponseEntity<>(returnList, HttpStatus.OK);
	}

	/**
	 * Associate HTS codes bucket.
	 * 
	 * @param fiscalYear
	 * @return
	 */
	@RequestMapping(value = "bucket/htsassociate/{fiscalYear}", method = RequestMethod.GET)
	public ResponseEntity<?> getAssociatedHTSBucket(@PathVariable long fiscalYear) {
		List<CBPEntry> associateHTS = trueUpBizComp.getAssociateHTSBucket(fiscalYear);
		return new ResponseEntity<>(associateHTS, HttpStatus.OK);
	}

	/**
	 * Update the Associate HTS Codes bucket.
	 * 
	 * @param cbpEntries
	 * @param fiscalYear
	 * @return
	 */
	@RequestMapping(value = "bucket/htsassociate/{fiscalYear}", method = RequestMethod.PUT)
	public ResponseEntity<?> saveAssociateHTSBucket(@RequestBody List<CBPEntry> cbpEntries,
			@PathVariable long fiscalYear) {
		List<CBPEntry> associateHTS = trueUpBizComp.updateAssociateHTSBucket(cbpEntries, fiscalYear);
		return new ResponseEntity<>(associateHTS, HttpStatus.OK);
	}

	/**
	 * Get companies not in the system to be included.
	 * 
	 * @param fiscalYear
	 * @return
	 */
	@RequestMapping(value = "bucket/inclcmpymktsh/{fiscalYear}", method = RequestMethod.GET)
	public ResponseEntity<?> getIncludeCompaniesInMktShBucket(@PathVariable long fiscalYear) {
		List<CompanyIngestion> cmpaniesToIncl = trueUpBizComp.getIncludeCompaniesInMktSh(fiscalYear);
		return new ResponseEntity<>(cmpaniesToIncl, HttpStatus.OK);
	}

	/**
	 * Save companies to be included in the system.
	 * 
	 * @param companies
	 * @param fiscalYear
	 * @return
	 */
	@RequestMapping(value = "bucket/inclcmpymktsh/{fiscalYear}", method = RequestMethod.PUT)
	public ResponseEntity<?> saveIncludeCompaniesInMktShBucket(@RequestBody List<CompanyIngestion> companies,
			@PathVariable long fiscalYear) {
		trueUpBizComp.updateIncludeCompaniesInMktSh(companies, fiscalYear);
		return new ResponseEntity<>(JSONObject.quote("OK"), HttpStatus.OK);
	}

	/**
	 * 
	 * Fetch TTB permits that are not present in TUFA.
	 * 
	 * @param fiscalYear
	 * @return
	 */
	@RequestMapping(value = "bucket/ttbpermits/{fiscalYear}", method = RequestMethod.GET)
	public ResponseEntity<?> getTTBPermits(@PathVariable long fiscalYear) {
		List<TTBPermitBucket> ttbpermits = trueUpBizComp.getTTBPermits(fiscalYear);
		return new ResponseEntity<>(ttbpermits, HttpStatus.OK);
	}

	@RequestMapping(value = "bucket/ttbpermits", method = RequestMethod.PUT)
	public ResponseEntity<?> saveTTBPermits(@RequestBody List<TTBPermitBucket> ttbPermits) {
		trueUpBizComp.updateTTBPermits(ttbPermits);
		return new ResponseEntity<>(JSONObject.quote("Updated the permits"), HttpStatus.OK);
	}

	/**
	 * Delete attachment.
	 *
	 * @param docId
	 *            the doc id
	 * @return the response entity
	 */
	@RequestMapping(value = "/ingestfile/{doc}/{yr}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteIngestionFile(@PathVariable("doc") String doctype,
			@PathVariable("yr") long fiscalYr) {
		trueUpBizComp.deleteIngestionFile(fiscalYr, doctype);
		return new ResponseEntity<>(JSONObject.quote("Successfully Deleted"), HttpStatus.OK);
	}

	@RequestMapping(value = "compare/fiscalYr/{fiscalYear}/company/{companyId}/ttbamendments", method = RequestMethod.POST)
	public ResponseEntity<?> getTTBAmendments(@PathVariable("fiscalYear") long fiscalYr, @PathVariable long companyId,
			@RequestBody List<TTBAmendment> tobaccoTypes) {
		List<TTBAmendment> ttbAmendments = trueUpBizComp.getTTBAmendments(fiscalYr, companyId, tobaccoTypes);
		return new ResponseEntity<>(ttbAmendments, HttpStatus.OK);
	}

	@RequestMapping(value = "compare/ttbamendments/{fiscalYear}", method = RequestMethod.PUT)
	public ResponseEntity<?> saveTTBAmendments(@PathVariable long fiscalYear, @RequestBody List<TTBAmendment> ttbAmendments) {
		List<TTBAmendment> amendments = trueUpBizComp.saveTTBAmendment(ttbAmendments);
		return new ResponseEntity<>(amendments, HttpStatus.OK);
	}

	@RequestMapping(value = "compare/comments/{fiscalYear}/{ein}/{tobaccoClass}", method = RequestMethod.POST)
	public ResponseEntity<?> saveCompareComments(@RequestBody List<TTBAmendment> ttbAmendments,@PathVariable long fiscalYear,@PathVariable String ein,@PathVariable String tobaccoClass) {
		List<TTBAmendment> amendments = atCompare.saveTTBCompareComments(ttbAmendments, ein, tobaccoClass);
		return new ResponseEntity<>(amendments, HttpStatus.OK);
	}
	

	@RequestMapping(value = "compare/acceptfda/{fiscalYear}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateCompareAcceptFDA(@PathVariable long fiscalYear, @RequestBody List<TTBAmendment> ttbAmendments) {
		List<TTBAmendment> amendments = trueUpBizComp.updateTTBCompareFDAAcceptFlag(ttbAmendments);
		return new ResponseEntity<>(amendments, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "compare/alldeltas/comments/{fiscalYear}/{ein}/{quarter}/{tobaccoClass}/{permitType}", method = RequestMethod.GET)
	public ResponseEntity<?> getallDeltaComments(@PathVariable("fiscalYear") int fiscalYear, @PathVariable("ein") String ein,
			@PathVariable("quarter") String quarter,@PathVariable("tobaccoClass") String tobaccoClass,
			@PathVariable("permitType") String permitType) {
		
		if(permitType.equals("Importer"))permitType = "IMPT"; else permitType = "MANU";
		
		
		
		List<ComparisonAllDeltaStatus> alldeltasts = trueUpBizComp.getallDeltaComments(fiscalYear,ein,quarter,tobaccoClass,permitType);
		   
		if(alldeltasts.size() == 0 )
		alldeltasts = null;
			
		return new ResponseEntity<>(alldeltasts, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "marketshare/{fiscalYear}", method = RequestMethod.GET)
	public ResponseEntity<TrueUp> getAnnualMarketShare(@PathVariable("fiscalYear") int fiscalYear) {
		TrueUp annual = trueUpBizComp.getTrueUpByYear(fiscalYear);
		return new ResponseEntity<>(annual, HttpStatus.OK);
	}
	
	/* CBP amendments */

	@RequestMapping(value = "compare/fiscalYr/{fiscalYear}/company/{companyId}/cbpamendments", method = RequestMethod.POST)
	public ResponseEntity<?> getCBPAmendments(@PathVariable("fiscalYear") long fiscalYr, @PathVariable long companyId,
			@RequestBody List<CBPAmendment> tobaccoTypes) {
		List<CBPAmendment> cbpAmendments = trueUpBizComp.getCBPAmendments(fiscalYr, companyId, tobaccoTypes);
		return new ResponseEntity<>(cbpAmendments, HttpStatus.OK);
	}

	@RequestMapping(value = "compare/cbpamendments/{fiscalYear}", method = RequestMethod.PUT)
	public ResponseEntity<?> saveCBPAmendments(@PathVariable long fiscalYear, @RequestBody List<CBPAmendment> cbpAmendments) {
		List<CBPAmendment> amendments = trueUpBizComp.saveCBPAmendment(cbpAmendments);
		return new ResponseEntity<>(amendments, HttpStatus.OK);
	}

	@RequestMapping(value = "compare/cbpcomments/{fiscalYear}/{ein}", method = RequestMethod.POST)
	public ResponseEntity<?> saveCBPCompareComments(@PathVariable long fiscalYear,@RequestBody List<CBPAmendment> cbpAmendments,@PathVariable String ein) {
		List<CBPAmendment> amendments = atCompare.saveCBPCompareComments(cbpAmendments, ein);
		return new ResponseEntity<>(amendments, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/compare/comments/edit/alldeltas/detailonly/{permitType}", method = RequestMethod.POST)
	public ResponseEntity<?> editCBPComAlldeltasDetailsOnly(@Valid @RequestBody ComparisonAllDeltaComments comment, 
			@PathVariable("permitType") String permitType ) throws ParseException {
		long commSeqCbp = 0;
		if(comment.getMatchCommSeq() != null && comment.getMatchCommSeq() > 0) {
			commSeqCbp = comment.getMatchCommSeq();
		}
		trueUpBizComp.updateCompareCommentDetailOnly(commSeqCbp,comment.getCommentSeq(),comment.getUserComment(),permitType);
		return new ResponseEntity<>(JSONObject.quote("Successfully Updated"), HttpStatus.OK);
	}
	@RequestMapping(value = "/compare/cbpcomments/edit/matched/{commentSeq}", method = RequestMethod.POST)
	public ResponseEntity<?> editCBPComMatchedDetailsOnly(@Valid @RequestBody CBPQtrComment comment, 
			@PathVariable("commentSeq") long commenSeq) throws ParseException {
		trueUpBizComp.updateCompareCommentDetailOnly(comment.getCommentSeq(),0,comment.getUserComment(),"IMPT");
		return new ResponseEntity<>(JSONObject.quote("Successfully Updated"), HttpStatus.OK);
	}
	@RequestMapping(value = "/compare/ttbcomments/edit/matched/{commentSeq}", method = RequestMethod.POST)
	public ResponseEntity<?> editTTBComMatchedDetailsOnly(@Valid @RequestBody TTBQtrComment comment, 
			@PathVariable("commentSeq") long commenSeq) throws ParseException {
		trueUpBizComp.updateCompareCommentDetailOnly(comment.getCommentSeq(),0,comment.getUserComment(),"MANU");
		return new ResponseEntity<>(JSONObject.quote("Successfully Updated"), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/compare/comments/delete/detailonly/{cbpAmendmentId}/{commSeqCbp}/{cmpAllDeltaId}/{commSeqAllDelta}/{permitType}", method = RequestMethod.PUT)
	public ResponseEntity<?> deleteCBPComDetailsOnly(@PathVariable long cbpAmendmentId,
			@PathVariable long commSeqCbp,@PathVariable long cmpAllDeltaId, @PathVariable long commSeqAllDelta,  @PathVariable String permitType) throws ParseException {
		trueUpBizComp.deleteCompareCommentDetailOnly(commSeqCbp, commSeqAllDelta, permitType);
		return new ResponseEntity<>(JSONObject.quote("Successfully Updated"), HttpStatus.OK);
	}

	@RequestMapping(value = "compare/cbpacceptfda/{fiscalYear}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateCBPCompareAcceptFDA(@PathVariable long fiscalYear, @RequestBody List<CBPAmendment> cbpAmendments) {
		List<CBPAmendment> amendments = trueUpBizComp.updateCBPCompareFDAAcceptFlag(cbpAmendments);
		return new ResponseEntity<>(amendments, HttpStatus.OK);
	}

	@RequestMapping(value = "compare/updatecbpentry/{fiscalYear}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateCBPIngestedLineEntryInfo(@PathVariable long fiscalYear, @RequestBody List<CBPLineEntryUpdateInfo> updateinfo) {
		trueUpBizComp.updateCBPIngestedLineEntryInfo(updateinfo);
		return new ResponseEntity<>(JSONObject.quote("Successfully Updated"), HttpStatus.OK);
	}
	
	@RequestMapping(value = "compare/getcbpentry/{cbEntryId}", method = RequestMethod.GET)
	public ResponseEntity<?> getCBPIngestedLineEntryInfo(@PathVariable("cbEntryId") long cbEntryId) {
		List<CBPEntry> cbpEnity = trueUpBizComp.getCBPIngestedLineEntryInfo(cbEntryId);
		return new ResponseEntity<>(cbpEnity, HttpStatus.OK);
	}
	
	@RequestMapping(value = "compare/check/association/{ein}/{fiscalYear}/{tobaccoClass}/{qtr}", method = RequestMethod.GET)
	public ResponseEntity<?> checkIfAssociationExists(@PathVariable("ein") String ein,
			@PathVariable long fiscalYear, @PathVariable String tobaccoClass,@PathVariable long qtr) {
		Boolean flag = trueUpBizComp.checkIfAssociationExists(ein, fiscalYear, tobaccoClass, qtr);
		return new ResponseEntity<>(flag, HttpStatus.OK);
	}

	/**
	 * Get a temporary version of the market share and return the true-up object
	 * graph.
	 * 
	 * @param fiscalYear
	 * @return
	 */
	/*@RequestMapping(value = "marketshare/{fiscalYear}", method = RequestMethod.GET)
	public ResponseEntity<TrueUp> getAnnualMarketShare(@PathVariable("fiscalYear") int fiscalYear) {
		TrueUp annual = trueUpBizComp.getTrueUpByYear(fiscalYear);
		return new ResponseEntity<>(annual, HttpStatus.OK);
	}*/

	/**
	 * Generate a temporary version of the market share and return the true-up
	 * object graph.
	 * 
	 * @param fiscalYear
	 * @return
	 */
	@RequestMapping(value = "marketshare/{fiscalYear}", method = RequestMethod.PUT)
	public ResponseEntity<TrueUp> generateAnnualMarketShare(@PathVariable("fiscalYear") int fiscalYear) {
		// check for approval message back to the user

		// generate a market share (version 0) but don't submit anything.
		trueUpBizComp.generateAnnualMarketShare(fiscalYear, "N");
		TrueUp annual = trueUpBizComp.getTrueUpByYear(fiscalYear);
		/*
		 * TrueUp annual = new TrueUp(); ArrayList<Assessment> assessments = new
		 * ArrayList<>(); annual.setFiscalYear(new Long(fiscalYear)); for(int
		 * i=1;i<5;i++) { assessments.add(createAssessment(i, fiscalYear)); }
		 * annual.setAssessments(assessments); annual.initialiazeVersions();
		 */
		return new ResponseEntity<>(annual, HttpStatus.OK);
	}

	@RequestMapping(value = "marketshare/submit/{fiscalYear}", method = RequestMethod.PUT)
	public ResponseEntity<TrueUp> submitAnnualMarketShare(@PathVariable("fiscalYear") int fiscalYear) {
		// check for approval message back to the user

		// generate a market share (version 0) but don't submit anything.
		trueUpBizComp.generateAnnualMarketShare(fiscalYear, "Y");
		TrueUp annual = trueUpBizComp.getTrueUpByYear(fiscalYear);
		/*
		 * TrueUp annual = new TrueUp(); ArrayList<Assessment> assessments = new
		 * ArrayList<>(); annual.setFiscalYear(new Long(fiscalYear)); for(int
		 * i=1;i<5;i++) { assessments.add(createAssessment(i, fiscalYear)); }
		 * annual.setAssessments(assessments); annual.initialiazeVersions();
		 */
		return new ResponseEntity<>(annual, HttpStatus.OK);
	}

	@RequestMapping(value = "/{fiscalYear}/{company_id}/{classNm}/{report_type}/{ein}", method = RequestMethod.GET, produces = {
			"application/json" })
	public ResponseEntity<?> getFDA3852flag(@PathVariable("fiscalYear") long fiscalYear,
			@PathVariable("company_id") long company_id, @PathVariable("classNm") String classNm,
			@PathVariable("report_type") String reportType, @PathVariable("ein") String ein) {
		if(company_id == 0) {
			Company cmpy = companyMgmtBizComp.getCompanybyEIN(ein);
			company_id = cmpy != null?cmpy.getCompanyId():0;
		}
		Map<String, String> results = "IMP".equals(reportType)
				? atCompare.fetchFDA352IMP(fiscalYear, company_id, classNm,ein)
				: trueUpBizComp.fetchFDA352MAN(fiscalYear, company_id, classNm);
		return new ResponseEntity<>(results, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/onesidedflag/{fiscalYear}/{company_id}/{classNm}/{report_type}/{ein}", method = RequestMethod.GET, produces = {
		"application/json" })
	public ResponseEntity<?> getOneSidedflag(@PathVariable("fiscalYear") long fiscalYear,
		@PathVariable("company_id") long company_id, @PathVariable("classNm") String classNm,
		@PathVariable("report_type") String reportType, @PathVariable("ein") String ein) {
	Map<String, String> results = trueUpBizComp.fetchOneSided(fiscalYear, company_id, classNm, reportType, ein);
	return new ResponseEntity<>(results, HttpStatus.OK);
	}

	/**
	 * Get companies not in the system to be included.
	 * 
	 * @param fiscalYear
	 * @return
	 */
	@RequestMapping(value = "bucket/inclcmpymktsh/{fiscalYear}/cmpy/{ein}/{type}", method = RequestMethod.GET)
	public ResponseEntity<?> getCompanyToIncldInMktSh(@PathVariable long fiscalYear, @PathVariable String ein,
			@PathVariable String type) {

		CompanyIngestion cmpyIngst = trueUpBizComp.getCompanyToIncldInMktSh(fiscalYear, ein, type);
		return new ResponseEntity<>(cmpyIngst, HttpStatus.OK);
	}

	/**
	 * Create new company not in TUFA and Permits in it.
	 *
	 * @param model
	 *            the model
	 * @return the response entity
	 */
	@RequestMapping(value = "bucket/inclcmpymktsh", method = RequestMethod.POST)
	public ResponseEntity<Company> createIngestedCompany(@Valid @RequestBody Company model) {

		logger.debug("CREATE_COMPANY: Create company with name = {}, ein = {}", model.getLegalName(),
				model.getEinNumber());
		logger.debug("CREATE_PERMIT: Create permit with permitNumber = {}, activeDate = {}", model.getPermitNumber(),
				model.getActiveDate());

		for (Iterator<Permit> iter = model.getPermitSet().iterator(); iter.hasNext();) {
			Permit permit = iter.next();
			if (permit.getTpbdDt() != null) {
				DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
				DateTime tpbdDt = formatter.parseDateTime(permit.getTpbdDt());
				DateTime currentDt = new DateTime();

				if (tpbdDt.isBefore(currentDt) || tpbdDt.isEqual(currentDt)) {
					permit.setIssueDt(permit.getTpbdDt());
					permit.setttbstartDt(permit.getTpbdDt());
					if (permit.getPermitTypeCd().equalsIgnoreCase("Manufacturer"))
						permit.setPermitTypeCd("MANU");
					if (permit.getPermitTypeCd().equalsIgnoreCase("Importer"))
						permit.setPermitTypeCd("IMPT");
					if (permit.getPermitStatusTypeCd().equalsIgnoreCase("Active"))
						permit.setPermitStatusTypeCd("ACTV");
					if (permit.getPermitStatusTypeCd().equalsIgnoreCase("Closed"))
						permit.setPermitStatusTypeCd("CLSD");
				} else
					iter.remove();
			}
		}

		Company retmodel = null;
		if (model != null && model.getPermitSet() != null && !model.getPermitSet().isEmpty())
			retmodel = companyMgmtBizComp.createCompany(model);
		else
			throw new CreateCompanyException();

		return new ResponseEntity<>(retmodel, HttpStatus.CREATED);
	}

	/*
	 * Set the status of the delta
	 */
	
	
	 @RequestMapping(value="compareall/alldeltas/comments/updateComment/{id}", method=RequestMethod.POST)
	  public ResponseEntity<?> updateDeltaComments(@RequestBody ComparisonAllDeltaComments comment, @PathVariable("id") long commenSeq) {	    	
		 
		 ComparisonAllDeltaComments deltaComments = trueUpBizComp.updateDeltaComments(commenSeq, comment);
		 return new ResponseEntity<>(deltaComments == null ? new ComparisonAllDeltaComments() : deltaComments,
					HttpStatus.OK);
	   
	 }
	
	@RequestMapping(value = "compareall/delta/status/{fiscalYear}", method = RequestMethod.POST)
	public ResponseEntity<?> setCompareAllDeltaStatus(@PathVariable("fiscalYear") long fiscalYear, @Valid @RequestBody List<TrueUpComparisonResults> model) {
		ComparisonAllDeltaStatusEntity deltaStatus = atCompare.setCompareAllDeltaStatus(model);
		return new ResponseEntity<>(deltaStatus == null ? new ComparisonAllDeltaStatusEntity() : deltaStatus,
				HttpStatus.OK);
	}

	@RequestMapping(value = "compareall/delta/exclude/{fiscalYear}", method = RequestMethod.POST)
	public ResponseEntity<?> updateCompareAllDeltaExclude(@PathVariable("fiscalYear") long fiscalYear, @Valid @RequestBody List<TrueUpComparisonResults> model) {
		ComparisonAllDeltaStatusEntity deltaStatus = atCompare.setCompareAllDeltaStatus(model);
		return new ResponseEntity<>(deltaStatus == null ? new ComparisonAllDeltaStatusEntity() : deltaStatus,
				HttpStatus.OK);
	}

	@RequestMapping(value = "compare/results/all/count/{fiscalYear}/{inComingPage}", method = RequestMethod.GET)
	public ResponseEntity<?> getAllComparisonResultsCount(@PathVariable long fiscalYear,  @PathVariable String inComingPage) {
		TrueUpComparisonResults result = atCompare.getAllComparisonResultsCount(fiscalYear, inComingPage);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "compareall/delta/ttbVolTax/{fiscalYear}/{ein}/{qtr}/{tobaccoClassNm}", method = RequestMethod.GET)
	public ResponseEntity<?> compareAllDeltaTTBVolTax(@PathVariable("fiscalYear") long fiscalYr, @PathVariable String ein,
			@PathVariable Integer qtr, @PathVariable String tobaccoClassNm) {

		TobaccoTypeTTBVol ttbVol = new TobaccoTypeTTBVol(fiscalYr, ein, qtr, tobaccoClassNm);
		List<TobaccoTypeTTBVol> result = this.trueUpBizComp.getTTBVolumeTaxPerTobaccoType(ttbVol);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "compareall/delta/action/{allAction}/{fiscalYear}/{ein}", method = RequestMethod.GET)
	public ResponseEntity<?> getAllDeltasAffected(@PathVariable("fiscalYear") long fiscalYr, @PathVariable String ein,
			@PathVariable String allAction) {

		List<TrueUpComparisonResults> result = atCompare.getDeltasAffectedByCompareAllAction(fiscalYr, ein,
				allAction);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "export/raw/{ein}/{fiscalYear}/{qtr}/{tobaccoClassNm}/{permitType}/{showOriginalAssociation}")
	public ResponseEntity<DocExport> getDeltaRawDataExport(@PathVariable("fiscalYear") long fiscalYr, @PathVariable String ein,
			@PathVariable String qtr, @PathVariable String tobaccoClassNm, @PathVariable String permitType,
			@PathVariable Boolean showOriginalAssociation) {
		DocExport export = this.trueUpBizComp.getDeltaRawDataExport(fiscalYr, ein, qtr, tobaccoClassNm, permitType,
				showOriginalAssociation);
		return new ResponseEntity<DocExport>(export, HttpStatus.OK);
	}

	@RequestMapping(value = "compare/ttbcomments", method = RequestMethod.PUT)
	public ResponseEntity<?> deleteTTBCompareComments(@RequestBody List<TTBAmendment> ttbAmendments) {
		this.trueUpBizComp.deleteTTBCompareComment(ttbAmendments);
		return new ResponseEntity<>(ttbAmendments, HttpStatus.OK);
	}

	@RequestMapping(value = "compare/cbpcomments", method = RequestMethod.PUT)
	public ResponseEntity<?> deleteCBPCompareComments(@RequestBody List<CBPAmendment> cbpAmendments) {
		this.trueUpBizComp.deleteCBPCompareComment(cbpAmendments);
		return new ResponseEntity<>(cbpAmendments, HttpStatus.OK);
	}

	// Returns comparison results for a fiscal year
	@RequestMapping(value = "export/results/{fiscalYear}/{selCompareMethod}", method = RequestMethod.PUT)
	public ResponseEntity<?> getExportData(@PathVariable long fiscalYear, @PathVariable String selCompareMethod,
			@RequestBody TrueUpCompareSearchCriteria criteria) {

		DocExport export  = atCompare.getExportData(fiscalYear, criteria, selCompareMethod);
		return new ResponseEntity<DocExport>(export, HttpStatus.OK);
	}

	/**
	 * Generate metrics for ingested CBP data
	 */
	@RequestMapping(value = "ingestfile/metrics/{fiscalYear}", method = RequestMethod.POST)
	public ResponseEntity<?> generateMetricData(@PathVariable("fiscalYear") long fiscalYr) {
		CBPMetrics result = this.trueUpBizComp.generateMetricData(fiscalYr);
		return new ResponseEntity<CBPMetrics>(result, HttpStatus.OK);
	}

	/**
	 * Get generated metrics for ingested CBP data
	 */
	@RequestMapping(value = "ingestfile/metrics/{fiscalYear}", method = RequestMethod.GET)
	public ResponseEntity<?> getGeneratedMetricData(@PathVariable("fiscalYear") long fiscalYr) {
		CBPMetrics result = this.trueUpBizComp.getGeneratedMetricData(fiscalYr);
		if (result == null) {
			return new ResponseEntity<CBPMetrics>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<CBPMetrics>(result, HttpStatus.OK);
	}

	// Accept ingested CBP data
	@RequestMapping(value = "ingested/acceptCBPLegacy/{fiscalYear}", method = RequestMethod.POST)
	public ResponseEntity<CBPMetrics> insertAcceptedCBPLegacyData(@PathVariable("fiscalYear") long fiscalYr) {
		logger.warn("ACCEPT METRICS: Accepting metric data for fiscal year " + fiscalYr);
		CBPMetrics result = this.trueUpBizComp.insertAcceptedCBPLegacyData(fiscalYr);
		if (result == null) {
			return new ResponseEntity<CBPMetrics>(HttpStatus.NO_CONTENT);
		}
		logger.warn("ACCEPT METRICS: Successfuly accepted metric data for " + fiscalYr);
		return new ResponseEntity<CBPMetrics>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "export/unmatched/{recordType}/{fiscalYear}")
	public ResponseEntity<DocExport> getUnmatchedRecordExport(@PathVariable("fiscalYear") long fiscalYr,
			@PathVariable String recordType) {
		DocExport export = this.trueUpBizComp.getUnmatchedRecordExport(fiscalYr, recordType);
		return new ResponseEntity<DocExport>(export, HttpStatus.OK);
	}

	@RequestMapping(value = "export/summary/{fileType}/{companyId}/{ein}/{fiscalYear}/{quarter}/{tobaccoType}/{IngestionType}", method = RequestMethod.GET)
	public ResponseEntity<DocExport> getIngestedSummaryExport(@PathVariable String fileType,
			@PathVariable String companyId, @PathVariable String ein, @PathVariable String fiscalYear, @PathVariable String quarter,
			@PathVariable String tobaccoType,@PathVariable String IngestionType ) {
		DocExport export = this.trueUpBizComp.exportSummaryOfIngestedData(fileType, companyId, ein, fiscalYear, quarter,
				tobaccoType,IngestionType);
		return new ResponseEntity<DocExport>(export, HttpStatus.OK);
	}
	
	// Save Context of single file ingestion or double file ingestion.
	@RequestMapping(value = "ingestion/workflow", method = RequestMethod.PUT)

	public ResponseEntity<?> saveorUpdateIngestionWorkflowContext(@Valid @RequestBody IngestionWorkflowContext workflowModel) {		
		List<IngestionWorkflowContext> workflowContext =  this.trueUpBizComp.saveorUpdateIngestionWorkflowContext(workflowModel);
		return new ResponseEntity<>(workflowContext, HttpStatus.OK);
	}
	
	
	// get the ingestion workflow for the year .
	@RequestMapping(value = "ingestion/workflow/{fiscalYear}", method = RequestMethod.GET)
	public ResponseEntity<?> getIngestionWorkflowContext(@PathVariable("fiscalYear") long fiscal_yr) {
			
	   List<IngestionWorkflowContext> workflowContext =  this.trueUpBizComp.getIngestionWorkflowContext(fiscal_yr);
	   return new ResponseEntity<>(workflowContext, HttpStatus.OK);
	}

	public PaginationAttributes getPaginationAttributes() {
		return paginationAttributes;
	}

	public void setPaginationAttributes(PaginationAttributes paginationAttributes) {
		this.paginationAttributes = paginationAttributes;
	}

	public CompanyMgmtBizComp getCompanyMgmtBizComp() {
		return companyMgmtBizComp;
	}

	public void setCompanyMgmtBizComp(CompanyMgmtBizComp companyMgmtBizComp) {
		this.companyMgmtBizComp = companyMgmtBizComp;
	}
	/**
	 * This returns the date selection on the compare details page
	 * @param fiscalYr
	 * @param tobaccoClass
	 * @param ein
	 * @return
	 */
	@GetMapping(value="compare/impt/details/selDate/{fiscalYear}/{tobaccoClass}/{ein}",produces = "application/json")
	public @ResponseBody String  getImporterCompareDetailsSelDate(@PathVariable("fiscalYear") long fiscalYr,@PathVariable String tobaccoClass,@PathVariable String ein) {
		String selDate = trueUpBizComp.getImporterCompareDetailsSelDate(tobaccoClass, fiscalYr, ein);
	    return JSONObject.quote(selDate);
	}

	@RequestMapping(value = "compare/impt/reallocate/{fiscalYear}/{tobaccoClass}/{ein}/{selDateType}", method = RequestMethod.PUT)
	public ResponseEntity<?> saveOrUpdateDateSelection(@PathVariable("fiscalYear") long fiscalYr,
			@PathVariable String tobaccoClass,@PathVariable String ein, @PathVariable String selDateType) throws ParseException {
		trueUpBizComp.saveOrUpdateDateSelection(ein, fiscalYr, tobaccoClass, selDateType);
		return new ResponseEntity<>(JSONObject.quote("Successfully Updated"), HttpStatus.OK);
	}

	/**
	 * 
	 * @param companyId
	 * @param fiscalYr
	 * @param qtr
	 * @param tobaccoClass
	 * @param ein
	 * @param selDateType
	 * @return
	 * @throws ParseException 
	 */
	@GetMapping(value="compare/impt/details/{companyId}/{fiscalYear}/{qtr}/{tobaccoClass}/{ein}")
	public ImporterTufaIngestionDetails getImporterCompareDetailsAllTobaccoType(@PathVariable long companyId,@PathVariable("fiscalYear") long fiscalYr,
			@PathVariable int qtr,@PathVariable String tobaccoClass,@PathVariable String ein) throws ParseException {
		//trueUpBizComp.saveOrUpdateDateSelection(ein, fiscalYr, tobaccoClass, selDateType);
		ImporterTufaIngestionDetails details = trueUpBizComp.getImporterCompareDetailsAllTobaccoType(companyId, fiscalYr, qtr, "IMPT", tobaccoClass, ein);
	    return details;
	}
	
	public ATFileIngestion getFileIngestor() {
		return fileIngestor;
	}

	public void setFileIngestor(ATFileIngestion fileIngestor) {
		this.fileIngestor = fileIngestor;
	}

	public ATCompare getAtCompare() {
		return atCompare;
	}

	public void setAtCompare(ATCompare atCompare) {
		this.atCompare = atCompare;
	}
	
	@GetMapping(value="compare/allDeltasDetails/{ein}/{permitType}/{fiscalYear}")
	public List<TrueUpComparisonResults> getAllDeltaDetaisForEin(@PathVariable("ein") String ein,@PathVariable("permitType")String permitType,@PathVariable("fiscalYear")long fiscalYear) {
		if(permitType.equalsIgnoreCase("Importer")) {
			permitType = "IMPT";
		} else {
			permitType = "MANU";
		}
		return trueUpBizComp.getAllDeltasDetailsForEIN(ein, permitType, fiscalYear);
	}
	
	@GetMapping(value="compare/delta/nonzerotobaccoclass/{ein}/{permitType}/{fiscalYear}")
	public List<String> getAllNonZeroDeltaTobaccoClass(@PathVariable("ein") String ein,@PathVariable("permitType")String permitType,@PathVariable("fiscalYear")long fiscalYear) {
		return trueUpBizComp.getAllNonZeroDeltaTobaccoClass(ein, permitType, fiscalYear);
	}
	
	@RequestMapping(value = "export/summary/{fileType}/{companyId}/{ein}/{fiscalYear}/{quarter}/{tobaccoType}", method = RequestMethod.GET)
	public ResponseEntity<DocExport> getMissingEntryExport(@PathVariable String fileType,
			@PathVariable String companyId, @PathVariable String ein, @PathVariable String fiscalYear, @PathVariable String quarter,
			@PathVariable String tobaccoType) {
		DocExport export = this.trueUpBizComp.exportSummaryOfIngestedData(fileType, companyId, ein, fiscalYear, quarter,
				tobaccoType,"missing");
		return new ResponseEntity<DocExport>(export, HttpStatus.OK);
	}
	
	@GetMapping(value = "new/deltas")
	public ResponseEntity<List<Long>> getFiscalYearsWithNewDeltas() {
		List<Long> fiscalYears = this.trueUpBizComp.getFiscalYearsWithNewDeltas();
		return new ResponseEntity<List<Long>>(fiscalYears, HttpStatus.OK);
	}

    /**
     * Get the list of attachment for commentId 
     * @param commentId
     * @return
     * @throws SQLException
     */
    @RequestMapping(value = "/comparedetails/comment/attachs/cbpCommentSeq/{cbpCommentSeq}/ttbCommentSeq/{ttbCommentSeq}/allDeltaCommentSeq/{allDeltaCommentSeq}", method = RequestMethod.GET)
	public ResponseEntity<?> getPermitAttachments(@PathVariable("cbpCommentSeq") long cbpCommentSeq,
			@PathVariable("ttbCommentSeq") long ttbCommentSeq,@PathVariable("allDeltaCommentSeq") long allDeltaCommentSeq) throws SQLException {
		List<ComparisonCommentAttachmentEntity> attachs = trueUpBizComp.findCommentDocbyId(cbpCommentSeq, ttbCommentSeq, allDeltaCommentSeq);
		return new ResponseEntity<>(attachs, HttpStatus.OK);
	}
    /**
     * Download Attachment Id
     * @param docId
     * @return
     * @throws SQLException
     */
    @RequestMapping(value = "/comparedetails/comment/attachs/{id}", method = RequestMethod.GET)
	public ResponseEntity<Attachment> downloadPermitAttachment(@PathVariable("id") long docId) throws SQLException {
		Attachment attachment = trueUpBizComp.comparisonCommentAttachmentDownload(docId);
		return new ResponseEntity<>(attachment, HttpStatus.OK);
	}
    /** 
     * View document by Id
     * @param docId
     * @param response
     * @param request
     * @throws SQLException
     * @throws IOException
     */
    @RequestMapping(value = "/comparedetails/comment/view/attachs/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<InputStreamResource> viewPermitAttachment(@PathVariable("id") long docId) throws SQLException, IOException {

		byte[] attachByteArry = trueUpBizComp.getComparisonCommentAttachmenttAsByteArray(docId);

		String pdfDataStr = new String(attachByteArry);
		String[] pdfDataArry = pdfDataStr.split("[:;,]");
		InputStream fileInStream = null;
		if (pdfDataArry.length == 4) {
			byte[] decodedFileCntByteArray = Base64.decode(pdfDataArry[3].getBytes());
			fileInStream = new ByteArrayInputStream(decodedFileCntByteArray);
			/*IOUtils.copy(fileInStream, response.getOutputStream());
			if(null != pdfDataArry[1]) {
				response.setContentType(pdfDataArry[1].toString());
			}else {
				response.setContentType("application/pdf");
			}
			response.setContentLength(attachByteArry.length);
			response.flushBuffer();*/
		} else {
			//response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
		return ResponseEntity.ok()
	            .contentLength(attachByteArry.length)
	            .contentType(MediaType.parseMediaType(pdfDataArry[1].toString()))
	            .body(new InputStreamResource(fileInStream));
		
	}
	/**
	 * Uploading attachment 
	 * @param file
	 * @param metaData
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 */
	@RequestMapping(value = "/comparedetails/comment/attachs/", method = RequestMethod.POST)
	public ResponseEntity<String> addPermitAttachments(@RequestParam("file") MultipartFile file,
			@RequestParam("metaData") String metaData)
			throws JsonParseException, JsonMappingException, IOException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		ComparisonCommentAttachmentEntity attachs = mapper.readValue(metaData, ComparisonCommentAttachmentEntity.class);
		attachs.setCreatedDt(new Date());
		attachs.setCreatedBy((securityContextUtil.getPrincipal() != null)? securityContextUtil.getPrincipal().getUsername() : null);
		trueUpBizComp.addComparisonCommentAttach(attachs, file);
		return new ResponseEntity<>(JSONObject.quote("Successfully Uploaded"), HttpStatus.OK);
	}
	/**
	 * Deleting attachment by docId
	 * @param docId
	 * @return
	 */
	@RequestMapping(value = "/comparedetails/comment/attachs/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> comparedetails(@PathVariable("id") long docId) {
		trueUpBizComp.deleteDocById(docId);
		return new ResponseEntity<>(JSONObject.quote("Successfully Deleted"), HttpStatus.OK);
	}
	
	@PostMapping(value="/{fiscalYear}/runadjustments")
	public ResponseEntity<String> processAdjustmentCheck(@PathVariable String fiscalYear) {
		trueUpBizComp.processAdjustments(fiscalYear);
		return new ResponseEntity<>(JSONObject.quote("Successfully Ran Adjustments checks"), HttpStatus.OK);
	}
	
	@GetMapping(value="compare/prevtrueupinfo/{type}/{currentYear}/{ein}")
	public ResponseEntity<?> getPreviousTrueupInfo(@PathVariable String type,@PathVariable long currentYear,@PathVariable String ein) {
		List<PreviousTrueupInfo> list = trueUpBizComp.getPreviousTrueupInfo(type, currentYear, ein);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
}

package gov.hhs.fda.ctp.web.trueup.aspect;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.ParameterMode;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.dozer.Mapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.procedure.ProcedureOutputs;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gov.hhs.fda.ctp.common.beans.CompanyReallocationBeanToUpdate;
import gov.hhs.fda.ctp.common.beans.FormDetail;
import gov.hhs.fda.ctp.common.beans.IngestionWorkflowContext;
import gov.hhs.fda.ctp.common.beans.PermitPeriod;
import gov.hhs.fda.ctp.common.beans.SubmittedForm;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetailPeriod;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetailPeriod;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetailPermit;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.config.MapperWrapper;
import gov.hhs.fda.ctp.common.exception.AnnTrueupDeltaChangeException;
import gov.hhs.fda.ctp.persistence.ReferenceCodeEntityManager;
import gov.hhs.fda.ctp.persistence.SimpleSQLManager;
import gov.hhs.fda.ctp.persistence.TrueUpEntityManager;
import gov.hhs.fda.ctp.persistence.model.CBPAmendmentEntity;
import gov.hhs.fda.ctp.persistence.model.CBPImporterEntity;
import gov.hhs.fda.ctp.persistence.model.ComparisonAllDeltaStatusEntity;
import gov.hhs.fda.ctp.persistence.model.IngestionWorkflowContextEntity;
import gov.hhs.fda.ctp.persistence.model.TTBAmendmentEntity;
import gov.hhs.fda.ctp.web.trueup.compare.ATCompare;

/**
 * @author sarang sharma.
 *
 */ 
@Aspect
@Component
public class AnnualTrueUpDeltaUpdateAspect {

	Logger logger = LoggerFactory.getLogger(AnnualTrueUpDeltaUpdateAspect.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private ReferenceCodeEntityManager referenceCodeEntityManager;

	@Autowired
	private ATCompare atCompare;
	
	@Autowired
    private TrueUpEntityManager trueUpEntityManager;
	
	@Autowired
    private MapperWrapper mapperWrapper;
	
	@Autowired
	private SimpleSQLManager sqlManager;

	/*
	 * Definition of point cuts to be intercepted to run delta change advice.
	 */
	@Pointcut("execution(* gov.hhs.fda.ctp.persistence.SimpleSQLManagerImpl.getComparisonResults(..))")
	private void compareResultsOperation() {
	}

	@Pointcut("execution(* gov.hhs.fda.ctp.persistence.TrueUpEntityManagerImpl.saveTTBAmendment(..))")
	private void amendmentTTBOperation() {
	}

	@Pointcut("execution(* gov.hhs.fda.ctp.persistence.TrueUpEntityManagerImpl.updateTTBCompareFDAAcceptFlag(..))")
	private void acceptTTBFDAOperation() {
	}

	@Pointcut("execution(* gov.hhs.fda.ctp.persistence.TrueUpEntityManagerImpl.saveCBPAmendment(..))")
	private void amendmentCBPOperation() {
	}

	@Pointcut("execution(* gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.updateCompanyComparisonBucket(..))")
	private void saveCompanyAssociation() {
	}

	@Pointcut("execution(* gov.hhs.fda.ctp.persistence.TrueUpEntityManagerImpl.updateCBPCompareFDAAcceptFlag(..))")
	private void acceptCBPFDAOperation() {
	}

	@Pointcut("execution(* gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.generateAnnualMarketShare(..))")
	private void generateMarketShOperation() {
	}

	@Pointcut("execution(* gov.hhs.fda.ctp.persistence.TrueUpEntityManagerImpl.updateCBPIngestedLineEntryInfo(..))")
	private void updateCBPIngestedLineEntryInfoOperation() {

	}

	@Pointcut("execution(* gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.fetchFDA352MAN(..))")
	private void getTTBAmendFlags() {
	}

	@Pointcut("execution(* gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.fetchFDA352IMP(..))")
	private void getCBPAmendFlags() {
	}

	@Pointcut("execution(* gov.hhs.fda.ctp.biz.trueup.TrueUpBizCompImpl.fetchFDA352IMPSingle(..))")
	private void getCBPAmendFlagssingle() {
	}

	@Pointcut("execution(* gov.hhs.fda.ctp.biz.permit.PermitBizCompImpl.saveSubmittedForms(..))")
	private void getSaveSubmittedForms() {

	}
	
	@Pointcut("execution(* gov.hhs.fda.ctp.persistence.TrueUpEntityManagerImpl.saveOrUpdateDateSelection(..))")
	private void saveOrUpdateDateSelection() {

	}

	/*
	 * Advice to be run to update delta changes before returning Annual Trueup
	 * Compare results.
	 */
	@Around("compareResultsOperation()")
	public Object doCompareResultsAnnTrueUpDeltaUpdate(ProceedingJoinPoint pjp) throws Throwable {

		logger.debug(" doCompareResultsAnnTrueUpDeltaUpdate():: updating delta changes for CBP and TTB data ");

		Object[] args = pjp.getArgs();
		Long fiscalYr = (Long) args[0];

		/*
		 * ProcedureOutputs outputs = updateFDADeltaChange(fiscalYr.toString(),
		 * null, null, null, null);
		 * 
		 * Integer ttlCntDeltaChng = (Integer)
		 * outputs.getOutputParameterValue("TTL_CNT_DELTA_CHNG"); Integer
		 * ttlCntDeltaChngZero = (Integer)
		 * outputs.getOutputParameterValue("TTL_CNT_DELTA_CHNG_ZERO"); Integer
		 * ttlCntAccptApprv = (Integer)
		 * outputs.getOutputParameterValue("TTL_CNT_ACCPT_APPRV");
		 */

		Object retVal = pjp.proceed();
		List<TrueUpComparisonResults> compareResults = (List<TrueUpComparisonResults>) retVal;

		if (compareResults != null && compareResults.size() == 0) {
			TrueUpComparisonResults resultForCount = new TrueUpComparisonResults();
			compareResults.add(resultForCount);
		}

		/*
		 * for (TrueUpComparisonResults result : compareResults) {
		 * result.setTtlCntDeltaChng(ttlCntDeltaChng);
		 * result.setTtlCntDeltaChngZero(ttlCntDeltaChngZero);
		 * result.setTtlCntAccptApprv(ttlCntAccptApprv); }
		 */

		logger.debug(" doCompareResultsAnnTrueUpDeltaUpdate():: returning compare results ");

		return compareResults;
	}

	/*
	 * Advice to be run to update delta changes after TTB Amend and Approve
	 * actions.
	 */
	@AfterReturning(pointcut = "amendmentTTBOperation() || acceptTTBFDAOperation()", returning = "amendments")
	public void doAnnualTrueUpTTBDeltaUpdate(Object amendments) {

		logger.debug(" doAnnualTrueUpTTBDeltaUpdate():: updating delta changes after TTB Amend/Approve actions ");

		List<TTBAmendmentEntity> amendmnts = (List<TTBAmendmentEntity>) amendments;
		TTBAmendmentEntity admnt = amendmnts.get(0);

		String fiscalYr = admnt.getFiscalYr();
		Long company_id = admnt.getTtbCompanyId();
		String quarter = admnt.getQtr();
		String tobaccoClassNm = admnt.getTobaccoClass().getTobaccoClassNm();
		String permitTypeCd = "MANU";

		updateFDADeltaChange(fiscalYr, company_id, null, tobaccoClassNm, permitTypeCd);

	}

	/*
	 * Advice to be run to update actions.
	 */
	@AfterReturning(pointcut = "saveCompanyAssociation()", returning = "cbpimporters")
	public void doExcludedPermitUpdate(Object cbpimporters) {

		List<CBPImporterEntity> cbpimpts = (List<CBPImporterEntity>) cbpimporters;

		CBPImporterEntity cbpimp = cbpimpts.get(0);
		Long cbpimpId = cbpimp.getCbpImporterId();
		String imptEin = cbpimpts.get(0).getImporterEIN();
		String consEin = cbpimpts.get(0).getConsigneeEIN();

		String sql0 = "SELECT cbpentry.cbp_entry_id,cbpentry.fiscal_year,cbpentry.fiscal_qtr FROM tu_cbp_entry cbpentry WHERE cbpentry.cbp_importer_id =:cbpimpId ";
		Query query0 = this.sessionFactory.getCurrentSession().createSQLQuery(sql0);
		query0.setParameter("cbpimpId", cbpimpId);

		List<Object[]> cbpEntries = query0.list();

		// for each CBP entry for the importer, update record in
		// tu_cbp_ingested_excl with permit_excluded_id
		for (Object[] obj : cbpEntries) {
			long cbpentryId = 0;
			long qtr = 0;
			long year = 0;

			if (obj[0] != null)
				cbpentryId = ((BigDecimal) obj[0]).longValue();
			if (obj[1] != null)
				year = ((BigDecimal) obj[1]).longValue();
			if (obj[2] != null)
				qtr = ((BigDecimal) obj[2]).longValue();

			if (qtr == 4)
				year = year + 1;
			else
				qtr = qtr + 1;

			String sql = "SELECT vw.perm_excl_audit_id,vw.ein FROM PERMIT_EXCL_STATUS_VW VW WHERE VW.ASSMNT_YR =:year and VW.ASSMNT_QTR =:qtr and "
					+ "Excl_Scope_id in(1,2) and  (VW.EIN =:imptEin OR  VW.EIN =:consEin) ";

			Query query = this.sessionFactory.getCurrentSession().createSQLQuery(sql);

			query.setParameter("year", Long.toString(year));
			query.setParameter("qtr", Long.toString(qtr));
			query.setParameter("imptEin", imptEin);
			query.setParameter("consEin", consEin);

			List<Object[]> resultSet = query.list();

			for (Object[] objn : resultSet) {

				if (objn[0] != null) {
					this.updateExclStatusTable(((BigDecimal) objn[0]).longValue(), cbpentryId);
				}

			}

		}

	}

	private void updateExclStatusTable(long exclId, long cbpEntryId) {

		String sql = "update tu_cbp_ingested_excl set perm_excl_audit_id =:exclId where cbp_entry_id =:cbpEntryId";

		Query query = this.sessionFactory.getCurrentSession().createSQLQuery(sql);

		query.setParameter("exclId", exclId);
		query.setParameter("cbpEntryId", cbpEntryId);
		query.executeUpdate();
		this.sessionFactory.getCurrentSession().flush();
		this.sessionFactory.getCurrentSession().clear();

	}

	@Around("getCBPAmendFlags()||getTTBAmendFlags()")
	public Object doAnnualTrueUpAmendmntDeltaUpdate(ProceedingJoinPoint pjp) throws Throwable {

		logger.debug(" doAnnualTrueUpAmendmntDeltaUpdate:: updating delta changes before getting Amendments ");

		Object[] args = pjp.getArgs();
		Long fiscalYr = (Long) args[0];
		Long companyId = (Long) args[1];
		String tobaccoTypeName = (String) args[2];

		if (tobaccoTypeName.equalsIgnoreCase("Pipe-Roll Your Own") || tobaccoTypeName.equalsIgnoreCase("Chew-Snuff")) {
			int indx = tobaccoTypeName.indexOf("-");
			tobaccoTypeName = tobaccoTypeName.substring(0, indx);
		}

		updateFDADeltaChange(fiscalYr.toString(), companyId, null, tobaccoTypeName, null);

		return pjp.proceed();

	}

	@Around("getCBPAmendFlagssingle()")
	public Object doAnnualTrueUpAmendmntDeltaUpdateSingle(ProceedingJoinPoint pjp) throws Throwable {

		logger.debug(" doAnnualTrueUpAmendmntDeltaUpdate:: updating delta changes before getting Amendments ");

		Object[] args = pjp.getArgs();
		Long fiscalYr = (Long) args[0];
		Long companyId = (Long) args[1];
		String tobaccoTypeName = (String) args[2];

		if (tobaccoTypeName.equalsIgnoreCase("Pipe-Roll Your Own") || tobaccoTypeName.equalsIgnoreCase("Chew-Snuff")) {
			int indx = tobaccoTypeName.indexOf("-");
			tobaccoTypeName = tobaccoTypeName.substring(0, indx);
		}

		updateFDADeltaChange(fiscalYr.toString(), companyId, null, tobaccoTypeName, null);

		return pjp.proceed();

	}

	/*
	 * Advice to be run to update delta changes after CBP Amend and Approve
	 * actions.
	 */
	@AfterReturning(pointcut = "amendmentCBPOperation() || acceptCBPFDAOperation()", returning = "amendments")
	public void doAnnualTrueUpCBPDeltaUpdate(Object amendments) {

		logger.debug(" doAnnualTrueUpCBPDeltaUpdate():: updating delta changes after TTB Amend/Approve actions ");

		List<CBPAmendmentEntity> amendmnts = (List<CBPAmendmentEntity>) amendments;
		CBPAmendmentEntity admnt = amendmnts.get(0);

		String fiscalYr = admnt.getFiscalYr();
		Long company_id = admnt.getCbpCompanyId();
		String quarter = admnt.getQtr();
		String tobaccoClassNm = admnt.getTobaccoClass().getTobaccoClassNm();
		String permitTypeCd = "IMPT";
		updateFDADeltaChange(fiscalYr, company_id, null, tobaccoClassNm, permitTypeCd);

	}

	/*
	 * Advice to be run to check for any delta changes before submitting market
	 * share. If delta changes exist do not proceed with the market share
	 * submission.
	 */
	@Around("generateMarketShOperation()")
	public void doSubmitMarketShareDeltaUpdate(ProceedingJoinPoint pjp) throws Throwable {

		logger.debug("doSubmitMarketShareDeltaUpdate():: Checking delta changes before submitting market share");

		Object[] args = pjp.getArgs();
		Integer fiscalYr = (Integer) args[0];
		String submitFlag = (String) args[1];

		if (submitFlag.equalsIgnoreCase("Y")) {
			ProcedureOutputs outputs = updateFDADeltaChange(fiscalYr.toString(), null, null, null, null);

			Integer ttlCntCompareResults = (Integer) outputs.getOutputParameterValue("TTL_CNT_COMPARE_RSLTS");
			Integer ttlCntDeltaChngZero = (Integer) outputs.getOutputParameterValue("TTL_CNT_DELTA_CHNG_ZERO");
			Integer ttlCntAccptApprv = (Integer) outputs.getOutputParameterValue("TTL_CNT_ACCPT_APPRV");

			Integer ttleCntToAcctApprv = ttlCntCompareResults - (ttlCntDeltaChngZero + ttlCntAccptApprv);
			if (ttleCntToAcctApprv != null && ttleCntToAcctApprv > 0)
				throw new AnnTrueupDeltaChangeException("DeltaChangeError", ttleCntToAcctApprv.toString());
		}

		pjp.proceed();

	}

	/**
	 * Advice to be run to update delta change upon saving of Monthly Report.
	 * 
	 * @throws Throwable
	 */
	@AfterReturning(pointcut = "getSaveSubmittedForms()", returning = "permitPeriod")
	public PermitPeriod updateDeltaChangeOnMonthlyReportSave(Object permitPeriod) throws Throwable {
		logger.debug(" updateDeltaChangeOnMonthlyReportSave():: updating delta changes after Monthly Report Saved ");
		// For each tobacco type run delta changes

		Map<Long, String> tcMap = referenceCodeEntityManager.getTobaccoClassIdNameMap();
		PermitPeriod pp = (PermitPeriod) permitPeriod;

		Long quarter = pp.getQuarter();
		Long fiscalYr = quarter == 1 ? pp.getYear() + 1 : pp.getYear();
		Long companyId = pp.getPermit().getCompany().getCompanyId();
		String ein =  pp.getPermit().getCompany().getEinNumber();
		String permitTypeCd = pp.getPermit().getPermitTypeCd();
		for (SubmittedForm form : pp.getSubmittedForms()) {

			if (form.getFormTypeCd().equalsIgnoreCase("3852")) {
				for (FormDetail detail : form.getFormDetails()) {
					String ttclassNm = tcMap.get(detail.getTobaccoClassId()).toString();
					this.updateFDADeltaChangeOnMRChange(fiscalYr.toString(), companyId, quarter.toString(), ttclassNm,
							permitTypeCd);
				}
			}
		}
		
		/* Since the MR might not have tobacco classes selected run the check for each of the valid ones*/
		tcMap.forEach((id, name) -> {
			// Run the check for each however if MANU only run it once for Chew/Snuff and Pipe/Roll-Your-Own
			if ((id > 2 && id < 9)) {
				// Check if there's no TUFA data
				if (isQuarterFullyRemoved(id.longValue(), ein, companyId, fiscalYr.longValue(), quarter.longValue(), permitTypeCd)) {
					// If so reset TUFA sided deltas and matched amendments
					if(!("MANU".equals(permitTypeCd) && (id == 3 || id == 5))) {
						updateAllDeltaStatusRecord(fiscalYr.toString(), ein, id, quarter.intValue(), permitTypeCd, "TUFA");
					}
					if("IMPT".equals(permitTypeCd)) {
						updateCBPAmendmentRecord(fiscalYr, companyId, id, quarter);
					} else {
						updateTTBAmendmentRecord(fiscalYr, companyId, id, quarter);
					}
				} else {
					// If the is data then reset the status for ingested records
					updateAllDeltaStatusRecord(fiscalYr.toString(), ein, id, quarter.intValue(), permitTypeCd, "INGESTED");
				}
			}
		});

		return pp;

	}

	/*
	 * Advice to be run to update delta changes after CBP Ingested LineEntry
	 * Info.
	 */
	@Around("updateCBPIngestedLineEntryInfoOperation() || saveOrUpdateDateSelection()")
	public void doCBPIngestedLineEntryInfoDeltaUpdate(ProceedingJoinPoint pjp) throws Throwable {

		logger.debug(" doCBPIngestedLineEntryInfoDeltaUpdate():: updating delta changes after CBP Ingested LineEntry Info ");
		String permitTypeCd = "IMPT";
		Object obj = pjp.proceed();
		CompanyReallocationBeanToUpdate cmpyToUpdate = (CompanyReallocationBeanToUpdate) obj;
		if(null != cmpyToUpdate) {
			updateFDADeltaChange(String.valueOf(cmpyToUpdate.getFiscalYear()), cmpyToUpdate.getCompanyId(), null, null,
					permitTypeCd);
			if(cmpyToUpdate.getTobaccoClassId() !=8) {
				for(int quarter=1;quarter<=4;quarter++) {
					if(isQuarterFullyExcludeToChkAmndmt(cmpyToUpdate.getTobaccoClassId(), cmpyToUpdate.getEin(), cmpyToUpdate.getSecondaryDateFlag(), cmpyToUpdate.getFiscalYear(), quarter)) {
						updateCBPAmendmentRecord(cmpyToUpdate.getFiscalYear(), cmpyToUpdate.getCompanyId(), cmpyToUpdate.getTobaccoClassId(), quarter);
						updateAllDeltaStatusRecord(String.valueOf(cmpyToUpdate.getFiscalYear()), cmpyToUpdate.getEin(), Long.valueOf(cmpyToUpdate.getTobaccoClassId()), quarter, "IMPT", "INGESTED");
					} else {
						updateAllDeltaStatusRecord(String.valueOf(cmpyToUpdate.getFiscalYear()), cmpyToUpdate.getEin(), Long.valueOf(cmpyToUpdate.getTobaccoClassId()), quarter, "IMPT", "TUFA");
					}
				}
				
			} else {
				if(isQuarterFullyExcludeToChkAmndmt(cmpyToUpdate.getTobaccoClassId(), cmpyToUpdate.getEin(), cmpyToUpdate.getSecondaryDateFlag(), cmpyToUpdate.getFiscalYear(), 1)) {
					updateCBPAmendmentRecord(cmpyToUpdate.getFiscalYear(), cmpyToUpdate.getCompanyId(), cmpyToUpdate.getTobaccoClassId(), 5);
					updateAllDeltaStatusRecord(String.valueOf(cmpyToUpdate.getFiscalYear()), cmpyToUpdate.getEin(), Long.valueOf(cmpyToUpdate.getTobaccoClassId()), 5, "IMPT", "INGESTED");
				} else {
					updateAllDeltaStatusRecord(String.valueOf(cmpyToUpdate.getFiscalYear()), cmpyToUpdate.getEin(), Long.valueOf(cmpyToUpdate.getTobaccoClassId()), 5, "IMPT", "TUFA");
				}
			}
		}
		

	}
	
	/**
	 * @param fiscalYear
	 * @param companyId
	 * @param tobaccoClassId
	 * @param quarter
	 */
	private void updateCBPAmendmentRecord(long fiscalYear,long companyId,long tobaccoClassId,long quarter) {
		String hql = "select cbpAmdmt from CBPAmendmentEntity cbpAmdmt " + " where cbpAmdmt.fiscalYr=:fiscalYear  "
				+ " and cbpAmdmt.cbpCompanyId =:companyId and cbpAmdmt.qtr in (:quarter) and cbpAmdmt.tobaccoClassId in (:tobaccoClassId)";
	
		Query hqlQuery = this.getSessionFactory().getCurrentSession().createQuery(hql);
		hqlQuery.setLong("fiscalYear", fiscalYear);
		hqlQuery.setLong("companyId", companyId);
		hqlQuery.setLong("quarter", quarter);
		hqlQuery.setLong("tobaccoClassId", tobaccoClassId);
		if(hqlQuery.list().size()>0) {
			CBPAmendmentEntity amendentity = (CBPAmendmentEntity)hqlQuery.list().get(0);
			long amendId = amendentity.getCbpAmendmentId();
//			String sql = "delete from tu_cbp_amendment amend where amend.cbp_amendment_id=:amendId ";
			String sql = "update tu_cbp_amendment amend set acceptance_flag = NULL, in_progress_flag = 'N', EDGE_STATUS_FLAG = NULL where amend.cbp_amendment_id=:amendId";
			Query sqlQuery = this.getSessionFactory().getCurrentSession().createSQLQuery(sql);
			sqlQuery.setParameter("amendId", amendId);
			sqlQuery.executeUpdate();
			this.getSessionFactory().getCurrentSession().flush();
			this.deleteMixedActionDetails(amendId);
		}
	}
	
	private void deleteMixedActionDetails(long amendmentId) {
		String sql = "delete from TU_AMNDMNT_MIXEDACT_DTL dtl where dtl.AMENDMENT_ID=:amendmentId";
		//getCurrentSession().delete(object);
		Query query = this.getSessionFactory().getCurrentSession().createSQLQuery(sql);
		query.setParameter("amendmentId", amendmentId);
		query.executeUpdate();
		this.getSessionFactory().getCurrentSession().flush();
		this.getSessionFactory().getCurrentSession().clear();
	}
	
	/**
	 * @param fiscalYear
	 * @param companyId
	 * @param tobaccoClassId
	 * @param quarter
	 */
	private void updateTTBAmendmentRecord(long fiscalYear,long companyId,long tobaccoClassId,long quarter) {
		String hql = "select ttbAmdmt from TTBAmendmentEntity ttbAmdmt " + " where ttbAmdmt.fiscalYr=:fiscalYear  "
				+ " and ttbAmdmt.ttbCompanyId =:companyId and ttbAmdmt.qtr in (:quarter) and ttbAmdmt.tobaccoClassId in (:tobaccoClassId)";
	
		Query hqlQuery = this.getSessionFactory().getCurrentSession().createQuery(hql);
		hqlQuery.setLong("fiscalYear", fiscalYear);
		hqlQuery.setLong("companyId", companyId);
		hqlQuery.setLong("quarter", quarter);
		hqlQuery.setLong("tobaccoClassId", tobaccoClassId);
		if(hqlQuery.list().size()>0) {
			TTBAmendmentEntity amendentity = (TTBAmendmentEntity)hqlQuery.list().get(0);
			long amendId = amendentity.getTtbAmendmentId();
			String sql = "update tu_ttb_amendment amend set acceptance_flag = NULL, in_progress_flag = 'N', EDGE_STATUS_FLAG = NULL,ADJ_PROCESS_FLAG='N' where amend.ttb_amendment_id=:amendId";
			Query sqlQuery = this.getSessionFactory().getCurrentSession().createSQLQuery(sql);
			sqlQuery.setParameter("amendId", amendId);
			sqlQuery.executeUpdate();
			this.getSessionFactory().getCurrentSession().flush();		
		}
	}
	
	/**
	 * @param fiscalYear
	 * @param companyId
	 * @param tobaccoClassId
	 * @param quarter
	 */
	private void updateAllDeltaStatusRecord(String fiscalYear, String ein, Long tobaccoClassId, int quarter, String permitType, String side) {
		String tobaccoClass = sqlManager.getTobaccoClassName(tobaccoClassId);
		if ("MANU".equals(permitType)) {
			if ("Pipe".equals(tobaccoClass) || "Roll-Your-Own".equals(tobaccoClass)) {
				tobaccoClass = "Pipe/Roll-Your-Own";
			} else if ("Chew".equals(tobaccoClass) || "Snuff".equals(tobaccoClass)) {
				tobaccoClass = "Chew/Snuff";
			}
		}
		
		String quarterString = quarter == 5 ? "1-4": Integer.toString(quarter);
		
		String hql = "from ComparisonAllDeltaStatusEntity allDeltaStatus where allDeltaStatus.fiscalYr =:fiscalYear and allDeltaStatus.ein =:ein and allDeltaStatus.fiscalQtr =:quarter and allDeltaStatus.permitType =:permitType"
				+ " and allDeltaStatus.tobaccoClassName =:tobaccoClassName and allDeltaStatus.deltaType =:side";
	
		Query hqlQuery = this.getSessionFactory().getCurrentSession().createQuery(hql);
		hqlQuery.setParameter("fiscalYear", fiscalYear);
		hqlQuery.setParameter("ein", ein);
		hqlQuery.setParameter("quarter", quarterString);
		hqlQuery.setParameter("permitType", permitType);
		hqlQuery.setParameter("side", side);
		hqlQuery.setParameter("tobaccoClassName", tobaccoClass);
		if(hqlQuery.list().size()>0) {
			ComparisonAllDeltaStatusEntity amendentity = (ComparisonAllDeltaStatusEntity)hqlQuery.list().get(0);
			long statusId = amendentity.getCmpAllDeltaId();
//			String sql = "delete from tu_cbp_amendment amend where amend.cbp_amendment_id=:amendId ";
			String sql = "update tu_comparison_all_delta_sts set status = NULL where cmp_all_delta_id =:statusId and status <> 'Associated'";
			Query sqlQuery = this.getSessionFactory().getCurrentSession().createSQLQuery(sql);
			sqlQuery.setParameter("statusId", statusId);
			sqlQuery.executeUpdate();
			this.getSessionFactory().getCurrentSession().flush();		
		}
	}
	
	/**
	 * @param tobaccoClassId
	 * @param companyId
	 * @param secondaryDateFlag
	 * @param fiscalYear
	 * @param quarter
	 * @return
	 */
	private boolean isQuarterFullyExcludeToChkAmndmt(long tobaccoClassId,String ein,String secondaryDateFlag,long fiscalYear,long quarter) {
		String tobaccoClass = sqlManager.getTobaccoClassName(tobaccoClassId);
    	List<TrueUpComparisonImporterIngestionDetail> ingestDetailsList = sqlManager.getImporterComparisonIngestionDetails(ein, fiscalYear, quarter, "IMPT", tobaccoClass, secondaryDateFlag);
    	for(TrueUpComparisonImporterIngestionDetail detailBean : ingestDetailsList) {
    		if(null != detailBean.getPeriodDetails()) {
    			for(TrueUpComparisonImporterIngestionDetailPeriod trueUpDetails : detailBean.getPeriodDetails()) {
        			if(trueUpDetails.getEntryTypeCd() !=21 && trueUpDetails.getEntryTypeCd()!=22 && new BigDecimal(trueUpDetails.getTaxAmount()).longValue()>0
        					&& (StringUtils.isEmpty(trueUpDetails.getExcludedFlag()) || (StringUtils.isNotEmpty(trueUpDetails.getExcludedFlag()) && trueUpDetails.getExcludedFlag().equalsIgnoreCase("N")))) {
        				return false;
        			}
        		}
    		}
    	}
    	return true;
	}
	
	/**
	 * @param tobaccoClassId
	 * @param companyId
	 * @param secondaryDateFlag
	 * @param fiscalYear
	 * @param quarter
	 * @return
	 */
	private boolean isQuarterFullyRemoved(long tobaccoClassId,String ein, Long companyId,long fiscalYear,long quarter, String permitType) {
		String tobaccoClass = sqlManager.getTobaccoClassName(tobaccoClassId);
		
		if ("IMPT".equals(permitType)) {
			
			if ("Roll-Your-Own".equals(tobaccoClass)) {
				tobaccoClass = "Roll Your Own";
			}
			
	    	List<TrueUpComparisonImporterDetail> detailsList = sqlManager.getImporterComparisonDetails(ein, fiscalYear, quarter, "IMPT", tobaccoClass);
	    	for(TrueUpComparisonImporterDetail detailBean : detailsList) {
	    		if(null != detailBean.getPeriodDetails()) {
	    			for(TrueUpComparisonImporterDetailPeriod trueUpDetails : detailBean.getPeriodDetails()) {
	        			if (!"NA".equals(trueUpDetails.getTaxAmount()) && new BigDecimal(trueUpDetails.getTaxAmount()).longValue()>0) {
	        				return false;
	        			}
	        		}
	    		}
	    	}
	    	return true;
		} else {
			
			List<TrueUpComparisonManufacturerDetail> detailsList;
			
			if ("Roll-Your-Own".equals(tobaccoClass) || "Pipe".equals(tobaccoClass)) {
				tobaccoClass = "Pipe-Roll Your Own";
			} else if ("Chew".equals(tobaccoClass) || "Snuff".equals(tobaccoClass)) {
				tobaccoClass = "Chew-Snuff";
			}
			
			if ("CIGARETTES".equalsIgnoreCase(tobaccoClass) || "CIGARS".equalsIgnoreCase(tobaccoClass)) {
				detailsList = sqlManager.getCigarManufacturerComparisonDetails(companyId, fiscalYear, quarter, "MANU", tobaccoClass);
			} else {
				detailsList = sqlManager.getNonCigarManufacturerComparisonDetails(companyId, fiscalYear, quarter, "MANU", tobaccoClass);
			}
			
			for(TrueUpComparisonManufacturerDetail detailBean : detailsList) {
	    		if(null != detailBean.getPermitDetails()) {
	    			for(TrueUpComparisonManufacturerDetailPermit trueUpDetails : detailBean.getPermitDetails()) {
	    				String tax = "";
	    				if ("Chew-Snuff".equals(tobaccoClass) || "Pipe-Roll Your Own".equals(tobaccoClass)) {
	    					tax = trueUpDetails.getTaxTotalAmount();
	    				} else {
	    					tax = trueUpDetails.getTaxAmount();
	    				}
	    				
	        			if (!"NA".equals(tax) && new BigDecimal(tax).longValue()>0) {
	        				return false;
	        			}
	        		}
	    		}
	    	}
	    	return true;
		}
	}


	/*
	 * Call to CALC_ANN_TRUEUP_DELTA_CHANGE stored procedure to update delta
	 * changes.
	 */
	public ProcedureOutputs updateFDADeltaChange(String fiscalYr, Long company_id, String quarter,
			String tobaccoClassNm, String permitTypeCd) {
		Session session = this.getSessionFactory().getCurrentSession();
		ProcedureCall procCall = this.atCompare.updateFDADeltaChangeProcedureCall(session);
		return this.updateFDADeltaChange(fiscalYr, company_id, quarter, tobaccoClassNm, permitTypeCd, procCall);

	}

	public void updateFDADeltaChangeOnMRChange(String fiscalYr, Long company_id, String quarter, String tobaccoClassNm,
			String permitTypeCd) {

		long fiscalYear = Long.parseLong(quarter) == 1 ?Long.parseLong(fiscalYr)+1:Long.parseLong(fiscalYr);
	    // refresh all the contexts
        List<IngestionWorkflowContext> contexts = this
                .transformIngestionWorkflowContextListToBeans(this.trueUpEntityManager.getIngestionContexts());
        Optional<IngestionWorkflowContext> opt = contexts.stream().filter(a->a.getFiscalYr() == fiscalYear).findFirst();
        if(opt.isPresent()) {
            IngestionWorkflowContext cxt = opt.get();
            Session session = this.getSessionFactory().getCurrentSession();
            ProcedureCall procCall1 = session.createStoredProcedureCall("CALC_ANN_TRUEUP_DELTA_CHANGE");
            ProcedureCall procCall2 = session.createStoredProcedureCall("CALC_ANN_TRUEUP_DELTA_CHNG_SNG");
            if(cxt.getContext().equalsIgnoreCase("detailOnly")) {
                this.updateFDADeltaChange(fiscalYr, company_id, quarter, tobaccoClassNm, permitTypeCd, procCall2);
            } else {
                this.updateFDADeltaChange(fiscalYr, company_id, quarter, tobaccoClassNm, permitTypeCd, procCall1);
            }
        }

	}

	public ProcedureOutputs updateFDADeltaChange(String fiscalYr, Long company_id, String quarter,
			String tobaccoClassNm, String permitTypeCd, ProcedureCall procCall) {

		if (permitTypeCd != null && permitTypeCd.equalsIgnoreCase("MANU")) {
			if (tobaccoClassNm != null) {
				if (tobaccoClassNm.equalsIgnoreCase("Chew") || tobaccoClassNm.equalsIgnoreCase("Snuff"))
					tobaccoClassNm = "Chew/Snuff";
				else if (tobaccoClassNm.equalsIgnoreCase("Pipe") || tobaccoClassNm.equalsIgnoreCase("Roll Your Own"))
					tobaccoClassNm = "Pipe/Roll Your Own";
				procCall.registerParameter("TOBACCO_CLASS_NAME", String.class, ParameterMode.IN)
						.bindValue(tobaccoClassNm);
			}
		}

		if (permitTypeCd != null && permitTypeCd.equalsIgnoreCase("IMPT")) {
			if (tobaccoClassNm != null) {
				procCall.registerParameter("TOBACCO_CLASS_NAME", String.class, ParameterMode.IN)
						.bindValue(tobaccoClassNm);
			}
		}

		if (fiscalYr != null)
			procCall.registerParameter("P_FISCAL_YR", String.class, ParameterMode.IN).bindValue(fiscalYr);
		if (company_id != null)
			procCall.registerParameter("COMPANY_ID", Long.class, ParameterMode.IN).bindValue(company_id);
		if (quarter != null)
			procCall.registerParameter("QUARTER", String.class, ParameterMode.IN).bindValue(quarter);
		if (permitTypeCd != null)
			procCall.registerParameter("PERMIT_TYPE_CD", String.class, ParameterMode.IN).bindValue(permitTypeCd);

		procCall.registerParameter("TTL_CNT_DELTA_CHNG", Integer.class, ParameterMode.OUT);
		procCall.registerParameter("TTL_CNT_DELTA_CHNG_ZERO", Integer.class, ParameterMode.OUT);
		procCall.registerParameter("TTL_CNT_ACCPT_APPRV", Integer.class, ParameterMode.OUT);
		procCall.registerParameter("TTL_CNT_COMPARE_RSLTS", Integer.class, ParameterMode.OUT);

		ProcedureOutputs outputs = procCall.getOutputs();

		return outputs;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public ReferenceCodeEntityManager getReferenceCodeEntityManager() {
		return referenceCodeEntityManager;
	}

	public void setReferenceCodeEntityManager(ReferenceCodeEntityManager referenceCodeEntityManager) {
		this.referenceCodeEntityManager = referenceCodeEntityManager;
	}

	public ATCompare getAtCompare() {
		return atCompare;
	}

	public void setAtCompare(ATCompare atCompare) {
		this.atCompare = atCompare;
	}
	
	private List<IngestionWorkflowContext> transformIngestionWorkflowContextListToBeans(
            List<IngestionWorkflowContextEntity> entities) {

        List<IngestionWorkflowContext> beanList = new ArrayList<>();
        if (entities != null) {
            Mapper mapper = mapperWrapper.getMapper();
            for (IngestionWorkflowContextEntity entry : entities) {
                IngestionWorkflowContext entryBean = new IngestionWorkflowContext();
                mapper.map(entry, entryBean);
                beanList.add(entryBean);
            }
        }
        return beanList;
    }

}

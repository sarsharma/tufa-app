package gov.hhs.fda.ctp.web.trueup.compare;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import gov.hhs.fda.ctp.common.beans.CBPAmendment;
import gov.hhs.fda.ctp.common.beans.DocExport;
import gov.hhs.fda.ctp.common.beans.TTBAmendment;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.persistence.model.ComparisonAllDeltaStatusEntity;

public interface ATCompare {
	public Map<String, Object> getComparisonResults(long fiscalYear, TrueUpCompareSearchCriteria criteria);
	public Map<String, Object> getAllComparisonResults(long fiscalYear, TrueUpCompareSearchCriteria criteria);
	public TrueUpComparisonResults getAllComparisonResultsCount(long fiscalYear, String inComingPage);
	public DocExport getExportData(long fiscalYear, TrueUpCompareSearchCriteria criteria, String selCompareMethod);
	public Map<String, String>fetchFDA352IMP(long fiscalYr, long company_id, String classNm,String ein);
	public ProcedureCall updateFDADeltaChangeProcedureCall(Session session);
	public ComparisonAllDeltaStatusEntity setCompareAllDeltaStatus(List<TrueUpComparisonResults> model);
	public List<TrueUpComparisonResults> getDeltasAffectedByCompareAllAction(long fiscalYr, String ein, String allAction);
	public List<CBPAmendment> saveCBPCompareComments(List<CBPAmendment> cbpAmendments, String ein);
	public List<TTBAmendment> saveTTBCompareComments(List<TTBAmendment> ttbAmendments, String ein, String tobaccoClass);
}

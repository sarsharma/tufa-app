package gov.hhs.fda.ctp.web.trueup.compare;

public interface ATCompareComponentFactory {
	
	 public ATCompare getCompareComponent(String compareComponentName);

}

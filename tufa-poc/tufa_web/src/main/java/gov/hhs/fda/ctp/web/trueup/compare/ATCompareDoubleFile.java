package gov.hhs.fda.ctp.web.trueup.compare;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import gov.hhs.fda.ctp.biz.trueup.TrueUpBizComp;
import gov.hhs.fda.ctp.common.beans.CBPAmendment;
import gov.hhs.fda.ctp.common.beans.DocExport;
import gov.hhs.fda.ctp.common.beans.TTBAmendment;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.pagination.PaginationAttributes;
import gov.hhs.fda.ctp.persistence.model.ComparisonAllDeltaStatusEntity;

@Component(value = "ATCompareDoubleFile")
@ComponentScan({ "gov.hhs.fda.ctp" })
public class ATCompareDoubleFile implements ATCompare{

	@Autowired
	private PaginationAttributes paginationAttributes;;
	
	@Autowired
	private TrueUpBizComp trueUpBizComp;
	
	@Override
	public Map<String, Object> getComparisonResults(long fiscalYear, TrueUpCompareSearchCriteria criteria) {
		List<TrueUpComparisonResults> results = trueUpBizComp.getComparisonResults(fiscalYear,criteria);

		Map<String, Object> resultsMap = new HashMap<>();
		Integer totalCnt = paginationAttributes.getItemsTotal();
		resultsMap.put("itemsTotal", totalCnt);
		resultsMap.put("filteredRows", paginationAttributes.getFilteredRows());
		resultsMap.put("results", results);
		
		return resultsMap;
	}

	@Override
	public Map<String, Object> getAllComparisonResults(long fiscalYear, TrueUpCompareSearchCriteria criteria) {
		List<TrueUpComparisonResults> results = trueUpBizComp.getAllComparisonResults(fiscalYear, criteria);
		Map<String, Object> resultsMap = new HashMap<>();
		Integer totalCnt = paginationAttributes.getItemsTotal();
		resultsMap.put("itemsTotal", totalCnt);
		resultsMap.put("results", results);
		
		return resultsMap;
	}
	
	@Override
	public TrueUpComparisonResults getAllComparisonResultsCount(long fiscalYr, String inComingPage) {
		TrueUpComparisonResults result = trueUpBizComp.getAllComparisonResultsCount(fiscalYr,inComingPage);		
		return result;
	}
	
	@Override
	public DocExport getExportData(long fiscalYear, TrueUpCompareSearchCriteria criteria, String selCompareMethod) {
		DocExport result = this.trueUpBizComp.getExportData(fiscalYear, criteria, selCompareMethod);		
		return result;
	}
	
	@Override
	public Map<String, String> fetchFDA352IMP(long fiscalYr, long company_id, String classNm,String ein) {
		Map<String, String> result = this.trueUpBizComp.fetchFDA352IMP(fiscalYr,company_id, classNm, ein);		
		return result;
	}
	
	public PaginationAttributes getPaginationAttributes() {
		return paginationAttributes;
	}

	public void setPaginationAttributes(PaginationAttributes paginationAttributes) {
		this.paginationAttributes = paginationAttributes;
	}

	public TrueUpBizComp getTrueUpBizComp() {
		return trueUpBizComp;
	}

	public void setTrueUpBizComp(TrueUpBizComp trueUpBizComp) {
		this.trueUpBizComp = trueUpBizComp;
	}

	@Override
	public ProcedureCall updateFDADeltaChangeProcedureCall(Session session) {
		 return session.createStoredProcedureCall("CALC_ANN_TRUEUP_DELTA_CHANGE");
	}
	
	public ComparisonAllDeltaStatusEntity setCompareAllDeltaStatus(List<TrueUpComparisonResults> model) {
		 return trueUpBizComp.setCompareAllDeltaStatus(model);
	}
	
	public List<TrueUpComparisonResults> getDeltasAffectedByCompareAllAction(long fiscalYr, String ein, String allAction){
		return this.trueUpBizComp.getDeltasAffectedByCompareAllAction(fiscalYr, ein, allAction);
	}
	public List<CBPAmendment> saveCBPCompareComments(List<CBPAmendment> cbpAmendments, String ein) {
		return this.trueUpBizComp.saveCBPCompareComments(cbpAmendments);
	}
	public List<TTBAmendment> saveTTBCompareComments(List<TTBAmendment> ttbAmendments, String ein, String tobaccoClass) {
		return this.trueUpBizComp.saveTTBCompareComments(ttbAmendments);
	}
}

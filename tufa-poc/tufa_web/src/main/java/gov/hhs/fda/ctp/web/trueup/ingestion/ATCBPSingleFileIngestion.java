package gov.hhs.fda.ctp.web.trueup.ingestion;

import java.io.IOException;
import java.util.Date;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import gov.hhs.fda.ctp.biz.trueup.TrueUpBizComp;
import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.beans.TrueUpDocument;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.persistence.model.TrueUpDocumentEntity;

@Component(value = "ATCBPSingleFileIngestion")
@ComponentScan({ "gov.hhs.fda.ctp" })
public class ATCBPSingleFileIngestion extends ATFileIngestionMgr implements ATFileIngestion {

	@Autowired
	private TrueUpBizComp trueUpBizComp;

	@Override
	public TrueUpDocument ingestFile(MultipartFile file, String fileType, String fiscalYear)
			throws IOException, OpenXML4JException, OpenXML4JException, SAXException, Exception {

		// parse input parameters for later
		long fiscalYr = Long.parseLong(fiscalYear);

		// create true up document
		TrueUpDocumentEntity trueUpDocumentEntity = null;
		TrueUpDocument trueupdocument = null;

		if (!file.isEmpty() && fileType != null) {

			byte[] fileBytes = file.getBytes();

			trueUpDocumentEntity = new TrueUpDocumentEntity();

			trueUpDocumentEntity.setTrueupDocument(fileBytes);
			trueUpDocumentEntity.setTrueUpFilename(file.getOriginalFilename());
			trueUpDocumentEntity.setDocDesc(fileType);
			trueUpDocumentEntity.setCreatedDt(new Date());
			trueUpDocumentEntity.setCreatedBy((this.getSecurityContextUtil().getPrincipal() != null)
					? this.getSecurityContextUtil().getPrincipal().getUsername() : null);

			IngestionOutput output = new IngestionOutput(fiscalYr);
			long errorCount = 0;
			
			this.parseIngestedFile(output, fileType, file);
			errorCount = output.getErrorCount();
			trueUpDocumentEntity.setErrorCount(errorCount);

			if (fileType.equalsIgnoreCase(Constants.CBPLine)) {
				this.trueUpBizComp.ingestCBPDetailsSingleFileWF(output, trueUpDocumentEntity);
			} else if (fileType.equalsIgnoreCase(Constants.TTB)) {
				this.trueUpBizComp.ingestTTB(output, trueUpDocumentEntity);
			} else if (fileType.equalsIgnoreCase(Constants.TTB_VOLUME)) {
				this.trueUpBizComp.ingestTTBRemovals(output, trueUpDocumentEntity);
			}

			trueupdocument = new TrueUpDocument();
			trueupdocument.setTrueUpFilename(trueUpDocumentEntity.getTrueUpFilename());
			trueupdocument.setDocDesc(trueUpDocumentEntity.getDocDesc());
			trueupdocument.setCreatedBy(trueUpDocumentEntity.getCreatedBy());
			trueupdocument.setCreatedDt(CTPUtil.parseDateToString(trueUpDocumentEntity.getCreatedDt()));
			trueupdocument.setModifiedBy(trueUpDocumentEntity.getModifiedBy());
			trueupdocument.setModifiedDt(CTPUtil.parseDateToString(trueUpDocumentEntity.getModifiedDt()));
			trueupdocument.setErrorCount(trueUpDocumentEntity.getErrorCount());
		}
		
		return trueupdocument;
	}

	public TrueUpBizComp getTrueUpBizComp() {
		return trueUpBizComp;
	}

	public void setTrueUpBizComp(TrueUpBizComp trueUpBizComp) {
		this.trueUpBizComp = trueUpBizComp;
	}

}

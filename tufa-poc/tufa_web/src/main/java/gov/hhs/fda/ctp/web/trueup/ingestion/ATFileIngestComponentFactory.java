package gov.hhs.fda.ctp.web.trueup.ingestion;

public interface ATFileIngestComponentFactory {

		 public ATFileIngestion getFileIngestionComponent(String ingestComponentName);
	
}

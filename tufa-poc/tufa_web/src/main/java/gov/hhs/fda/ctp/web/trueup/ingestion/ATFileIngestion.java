package gov.hhs.fda.ctp.web.trueup.ingestion;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import gov.hhs.fda.ctp.common.beans.TrueUpDocument;

public interface ATFileIngestion {
	
	 public TrueUpDocument ingestFile(MultipartFile file,
				 String fileType,  String fiscalYear) throws IOException, OpenXML4JException, OpenXML4JException, SAXException, Exception;
}

package gov.hhs.fda.ctp.web.trueup.ingestion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import gov.hhs.fda.ctp.common.beans.IngestionOutput;
import gov.hhs.fda.ctp.common.ingestion.FileIngestor;
import gov.hhs.fda.ctp.common.ingestion.FileIngestorFactory;
import gov.hhs.fda.ctp.common.ingestion.FileIngestorNameMapping;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;

@Component
public class ATFileIngestionMgr {
	
	
	@Autowired
	private FileIngestorFactory fileIngestorFactory;

	@Autowired
	private FileIngestorNameMapping fileIngestorNameMapping;
	
	@Autowired
	private SecurityContextUtil securityContextUtil;
	
	protected void parseIngestedFile(IngestionOutput output,String fileType,MultipartFile file) throws InvalidFormatException, IOException, OpenXML4JException, SAXException{
		
		String fileIngestorName = this.fileIngestorNameMapping.getFileIngestorName(fileType);
		FileIngestor fileIngestor = this.fileIngestorFactory.getFileIngestor(fileIngestorName);
		String filePath = generateIngestionFile(file);
		fileIngestor.parseFileSAX(filePath, output);
		deleteGeneratedIngestionFile(filePath);
	}
	protected String generateIngestionFile(MultipartFile file) throws IOException {

		byte[] fileBytes = file.getBytes();
		String fileName = file.getOriginalFilename();
		File tmpFile = null;

		tmpFile = File.createTempFile(FilenameUtils.getBaseName(fileName), FilenameUtils.getExtension(fileName));
		tmpFile.deleteOnExit();

		OutputStream outStream = new FileOutputStream(tmpFile);
		outStream.write(fileBytes);
		outStream.close();

		return tmpFile.getAbsolutePath();
	}

	
	protected void deleteGeneratedIngestionFile(String filePath) throws IOException {
		Path fileP = Paths.get(filePath);
		Files.deleteIfExists(fileP);
	}

	public FileIngestorFactory getFileIngestorFactory() {
		return fileIngestorFactory;
	}

	public void setFileIngestorFactory(FileIngestorFactory fileIngestorFactory) {
		this.fileIngestorFactory = fileIngestorFactory;
	}

	public FileIngestorNameMapping getFileIngestorNameMapping() {
		return fileIngestorNameMapping;
	}

	public void setFileIngestorNameMapping(FileIngestorNameMapping fileIngestorNameMapping) {
		this.fileIngestorNameMapping = fileIngestorNameMapping;
	}
	public SecurityContextUtil getSecurityContextUtil() {
		return securityContextUtil;
	}
	public void setSecurityContextUtil(SecurityContextUtil securityContextUtil) {
		this.securityContextUtil = securityContextUtil;
	}

}

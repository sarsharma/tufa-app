package gov.hhs.fda.ctp.web.useradmin;

public class Manufacturer {

	private String firstname;
	private String lastname;
	private String gender;
	private String dob;
	private String selectedstate;
	
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getSelectedstate() {
		return selectedstate;
	}
	public void setSelectedstate(String selectedstate) {
		this.selectedstate = selectedstate;
	}
	
	@Override
	public String toString(){
		return this.firstname + " " +
				this.lastname + " " +
				this.dob + " " +
				this.gender + " " +
				this.selectedstate;
	}
}

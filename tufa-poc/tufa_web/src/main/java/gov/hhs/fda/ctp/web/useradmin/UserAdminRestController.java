package gov.hhs.fda.ctp.web.useradmin;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.hhs.fda.ctp.biz.registration.UserDTO;
import gov.hhs.fda.ctp.biz.useradmin.UserAdminBizComp;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.web.registration.User;

//@CrossOrigin(origins = "http://localhost:9000")
@RestController
@RequestMapping("/api/useradmin")
public class UserAdminRestController {
	Logger logger = LoggerFactory.getLogger(UserAdminRestController.class);

	@Autowired
	private UserAdminBizComp userAdminBizComp;

	public UserAdminRestController() {
		// Empty Constructor
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public ResponseEntity<User> registerUser(@Valid @RequestBody User user) {
		logger.debug("CREATE_USER: Create user with last name = {}, first name = {}", user.getLastname(),
				user.getFirstname());
		UserDTO userDTO = userAdminBizComp.registerUser(transformUserToDto(user));

		return new ResponseEntity<>(transformUserDtoToUser(userDTO), HttpStatus.CREATED);
	}

	private User transformUserDtoToUser(UserDTO dto) {
		User user = new User();
		user.setEmail(dto.getEmail());
		user.setEndDt(CTPUtil.parseDateToString(dto.getEndDate()));
		user.setFirstname(dto.getFirstName());
		user.setLastLoginDt(CTPUtil.parseDateToString(dto.getLastLoginDate()));
		user.setLastname(dto.getLastName());
		user.setPhone1(dto.getPhone1());
		user.setRole(dto.getRole());
		user.setEnabled(dto.getEnabled());
		user.setEnabled("Inactive".equals(dto.getEnabled()) ? "N" : "Y");
		user.setUserId(dto.getUserId());
		user.setCreatedBy(dto.getCreatedBy());
		user.setCreatedDt(dto.getCreatedDt());
		user.setModifiedBy(dto.getModifiedBy());
		user.setModifiedDt(dto.getModifiedDt());
		return user;
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ResponseEntity<?> getUsers() {
		logger.debug("Entering getUsers()");
		return ResponseEntity.ok(userAdminBizComp.getUsers());
	}

	@RequestMapping(value = "/retrieve", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public User getUser(@RequestBody String email) {
		logger.debug("Entering getUser() with email");
		return assembleUser(userAdminBizComp.getUser(email));
	}

	@RequestMapping(value = "/roles", method = RequestMethod.GET)
	public ResponseEntity<?> getRoles() {
		logger.debug("Entering getRoles()");
		return ResponseEntity.ok(userAdminBizComp.getRoles());
	}

	/**
	 * Update user.
	 *
	 * @param model the model
	 * @return the response entity
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
	public ResponseEntity<User> update(@Valid @RequestBody User user) {
		UserDTO retmodel = userAdminBizComp.update(transformUserToDto(user));
		return new ResponseEntity<>(transformUserDtoToUser(retmodel), HttpStatus.OK);
	}

	/**
	 * Read user.
	 *
	 * @param id the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<User> getUser(@PathVariable long id) {
		User user = transformUserDtoToUser(userAdminBizComp.getUser(id));
		return new ResponseEntity<>(user, HttpStatus.OK);
	}


	private User assembleUser(UserDTO user) {
		User user2 = new User();
		user2.setEmail(user.getEmail());
		user2.setFirstname(user.getFirstName());
		user2.setLastname(user.getLastName());
		return user2;
	}

	private UserDTO transformUserToDto(User user) {

		UserDTO userDTO = new UserDTO();
		userDTO.setFirstName(user.getFirstname());
		userDTO.setLastName(user.getLastname());
		userDTO.setEmail(user.getEmail());
		userDTO.setRole(user.getRole());
		if(user.getEnabled()== null){
		userDTO.setEnabled("Y");
		}else{
			userDTO.setEnabled(user.getEnabled());
		}
		userDTO.setEndDate(CTPUtil.parseStringToDate(user.getEndDt()));
		userDTO.setLastLoginDate(CTPUtil.parseStringToDate(user.getLastLoginDt()));
		userDTO.setPassword("password");
		userDTO.setPhone1(user.getPhone1());
		userDTO.setUserId(user.getUserId());

		return userDTO;
	}

}

package gov.hhs.fda.ctp.web.addressmgmt;

import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import gov.hhs.fda.ctp.biz.addressmgmt.AddressMgmtBizComp;
import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.persistence.model.CountryEntity;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

/**
 * @author rnasina
 *
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {MvcConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class})
})
public class TestAddressMgmtRestController {
	@InjectMocks
	AddressMgmtRestController controller;

	/** The address mgmt biz comp. */
	@Mock
	private AddressMgmtBizComp addressMgmtBizComp;

    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

	 @Before
	 public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
	 }

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.addressmgmt.AddressMgmtRestController#saveOrUpdate(gov.hhs.fda.ctp.common.beans.Address)}.
	 */
	@Test
	public void testSaveOrUpdate() throws Exception {
        Address mockAddress = this.createMockAddress();
        String addressJson = json(mockAddress);

        when(
                this.addressMgmtBizComp.saveOrUpdate(
                  (Address)notNull()
                )
              ).thenReturn(mockAddress);

        MvcResult result = this.mockMvc
	        .perform(put("/api/v1/companies/address")
	                .contentType(this.contentType)
	                .content(addressJson))
	        		.andExpect(status().isOk())
	                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assert(returnString.equals(addressJson));

        verify(addressMgmtBizComp, times(1)).saveOrUpdate((Address)notNull());
        verifyNoMoreInteractions(addressMgmtBizComp);

	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.addressmgmt.AddressMgmtRestController#readCntryCd()}.
	 */
	@Test
	public void testReadCntryCd() throws Exception {
		List<String> mockCntryCodes = Arrays.asList("US", "UK", "CAN");
        when(this.addressMgmtBizComp.getCountryCd()).thenReturn(mockCntryCodes);

        this.mockMvc
                .perform(get("/api/v1/companies/address/cntryCd"))
                .andExpect(status().isOk())
                .andExpect(content()
                		.contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print());
        verify(addressMgmtBizComp, times(1)).getCountryCd();
        verifyNoMoreInteractions(addressMgmtBizComp);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.addressmgmt.AddressMgmtRestController#readCntryNm()}.
	 */
	@Test
	public void testReadCntryNm() throws Exception {
		List<String> mockCntryNames = Arrays.asList("United States", "United Kingdom", "Canada");
        when(this.addressMgmtBizComp.getCountryNm()).thenReturn(mockCntryNames);

        this.mockMvc
                .perform(get("/api/v1/companies/address/cntryNm"))
                .andExpect(status().isOk())
                .andExpect(content()
                		.contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print());
        verify(addressMgmtBizComp, times(1)).getCountryNm();
        verifyNoMoreInteractions(addressMgmtBizComp);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.addressmgmt.AddressMgmtRestController#readStateCd()}.
	 */
	@Test
	public void testReadStateCd() throws Exception {
		List<String> mockStateCodes = Arrays.asList("AL", "FL", "AK");
        when(this.addressMgmtBizComp.getStateCd()).thenReturn(mockStateCodes);

        this.mockMvc
                .perform(get("/api/v1/companies/address/stateCd"))
                .andExpect(status().isOk())
                .andExpect(content()
                		.contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print());
        verify(addressMgmtBizComp, times(1)).getStateCd();
        verifyNoMoreInteractions(addressMgmtBizComp);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.addressmgmt.AddressMgmtRestController#readStateNm()}.
	 */
	@Test
	public void testReadStateNm() throws Exception {
		List<String> mockStateNames = Arrays.asList("Alabama", "Florida", "Arkansas");
        when(this.addressMgmtBizComp.getStateNm()).thenReturn(mockStateNames);

        this.mockMvc
                .perform(get("/api/v1/companies/address/stateNm"))
                .andExpect(status().isOk())
                .andExpect(content()
                		.contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print());
        verify(addressMgmtBizComp, times(1)).getStateNm();
        verifyNoMoreInteractions(addressMgmtBizComp);
	}

	/**
	 * Test get countries.
	 * Test method for {@link gov.hhs.fda.ctp.web.addressmgmt.AddressMgmtRestController#getCountries()}.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testGetCountries() throws Exception {
		Set<CountryEntity> mockCountries = new HashSet<>();
		CountryEntity ce1 = new CountryEntity();
		ce1.setCountryCd("A");
		ce1.setCountryDialCd("1");
		ce1.setCountryNm("United States");
		mockCountries.add(ce1);

		CountryEntity ce2 = new CountryEntity();
		ce2.setCountryCd("B");
		ce2.setCountryDialCd("52");
		ce2.setCountryNm("Mexico");
		mockCountries.add(ce2);

        when(this.addressMgmtBizComp.getCountries()).thenReturn(mockCountries);

        this.mockMvc
                .perform(get("/api/v1/companies/address/countries"))
                .andExpect(status().isOk())
                .andExpect(content()
                		.contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print());
        verify(addressMgmtBizComp, times(1)).getCountries();
        verifyNoMoreInteractions(addressMgmtBizComp);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.addressmgmt.AddressMgmtRestController#getReportAddresses(int, int, int)}.
	 */
	@Test
	public void testGetReportAddresses() throws Exception {
		List<Address> mockaddresses  = this.createMockAddresses();

        when(this.addressMgmtBizComp.getReportAddresses("1",1,1)).thenReturn(mockaddresses);

        this.mockMvc
                .perform(get("/api/v1/companies/address/{companyId}/{permitId}/{periodId}",1,1,1))
                .andExpect(status().isOk())
                .andExpect(content()
                		.contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print());
        verify(addressMgmtBizComp, times(1)).getReportAddresses("1",1,1);
        verifyNoMoreInteractions(addressMgmtBizComp);
	}


    @SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }


    /**
     * Create a well-formed mock Address.
     * @return
     */
    private Address createMockAddress() {
    	Address mockAddress = new Address();
    	mockAddress.setCompanyId(1L);
    	mockAddress.setCity("TEST CITY");
    	mockAddress.setAddressTypeCd("");
    	mockAddress.setAttention("");
    	mockAddress.setCountryCd("");
    	mockAddress.setCountryNm("");
    	mockAddress.setFaxNum(0L);
    	mockAddress.setPostalCd("");
    	mockAddress.setProvince("");
    	mockAddress.setState("");
    	mockAddress.setStreetAddress("");
        return mockAddress;
    }

    private List<Address> createMockAddresses() {
    	List<Address> mockAddresses = new ArrayList<>();
    	Address address1 = new Address();
    	address1.setCompanyId(1L);
    	address1.setCity("TEST CITY");
    	address1.setAddressTypeCd("");
    	address1.setAttention("");
    	address1.setCountryCd("");
    	address1.setCountryNm("");
    	address1.setFaxNum(0L);
    	address1.setPostalCd("");
    	address1.setProvince("");
    	address1.setState("FL");
    	address1.setStreetAddress("");
    	mockAddresses.add(address1);

    	Address address2 = new Address();
    	address2.setCompanyId(2L);
    	address2.setCity("TEST CITY 2");
    	address2.setAddressTypeCd("");
    	address2.setAttention("");
    	address2.setCountryCd("");
    	address2.setCountryNm("");
    	address2.setFaxNum(0L);
    	address2.setPostalCd("");
    	address2.setProvince("");
    	address2.setState("NY");
    	address2.setStreetAddress("");
    	mockAddresses.add(address2);

        return mockAddresses;
    }
}

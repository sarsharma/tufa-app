package gov.hhs.fda.ctp.web.authentication;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import gov.hhs.fda.ctp.web.authentication.LoginRestController;

/*@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class})
})*/
public class TestLoginRestController {

	@Autowired
    private WebApplicationContext wac;
	
	private MockMvc mockMvc;

	// @Autowired
	// private LoginRestController loginRestController;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(new LoginRestController())
				.setMessageConverters(
                        new MappingJackson2HttpMessageConverter())
				.alwaysExpect(status().isOk())
				.alwaysExpect(content()
				.contentTypeCompatibleWith("application/json;charset=UTF-8")).build();

		//this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	//@Test
	public void getUserCredentials() throws Exception {
		this.mockMvc
				.perform(post("/api/authenticate").content("{\"email\":\"admin@test.com\",\"password\":\"admin\"}")
						.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(jsonPath("$.name").value("admin@test.com"))
				.andReturn();
	}

}

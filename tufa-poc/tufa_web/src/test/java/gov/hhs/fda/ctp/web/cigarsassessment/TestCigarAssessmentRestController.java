package gov.hhs.fda.ctp.web.cigarsassessment;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.CigarAssessment;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentFilter;
import gov.hhs.fda.ctp.common.beans.CigarAssessmentRptDetail;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.persistence.model.AssessmentEntity;
import gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;
import gov.hhs.fda.ctp.web.quarter.AssessmentRestController;

/**
 * @author rabalasubramani3
 * Reference: http://memorynotfound.com/unit-test-spring-mvc-rest-service-junit-mockito/
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	
	@ContextConfiguration(classes = {MvcConfig.class})
})
@Transactional()
public class TestCigarAssessmentRestController {
    @Autowired
    private WebApplicationContext wac;

    @Autowired
	private CigarAssessmentRestController controller;
    
    @Autowired
	private AssessmentRestController assesmentcontroller;

	/** The address mgmt biz comp. */
    @Autowired
	private AssessmentBizComp assessmentBizComp;

  
    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

	 @Before
	 public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
	 }


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.cigarsassessment.CigarAssessmentRestController#getCigarsAssessments(gov.hhs.fda.ctp.common.beans.CigarAssessmentFilter)}.
	 */
	@Test
	public void testGetCigarsAssessments() throws Exception {

		CigarAssessmentFilter filters =  new CigarAssessmentFilter();
		
        ResponseEntity res = this.controller.getCigarsAssessments(filters);
        Assert.assertNotNull(res);
 
        HttpStatus returnString = res.getStatusCode();
        assert(returnString.equals(200));
        
	}

	
	@Test
	public void testgenerateCigarMarketShare() throws Exception {
        
		ResponseEntity res = this.assesmentcontroller.generateCigarMarketShare(0, 2018, 1, "FULLMS");
        Assert.assertNotNull(res);
 
        HttpStatus returnString = res.getStatusCode();
        assert(returnString.equals(200));      
	}
	
	
	@Test
	public void testgetCigarReconExport() throws Exception {
        
		ResponseEntity res = this.controller.getReconExport(2018);
        Assert.assertNotNull(res);
 
        HttpStatus returnString = res.getStatusCode();
        assert(returnString.equals(200));      
	}
	
	

	@Test
	public void testgetCigarAssessment() throws Exception {
        
		ResponseEntity res = this.assesmentcontroller.getAssessment(0,2018);
        Assert.assertNotNull(res);
 
        HttpStatus returnString = res.getStatusCode();
        assert(returnString.equals(200));      
	}
	
	
	
	
	@Test
	public void testgetCigarAssessmentsReportDetail() throws Exception {
        
		ResponseEntity res = this.controller.getCigarsAssessmentsReportDetails(2018);
        Assert.assertNotNull(res);
 
        HttpStatus returnString = res.getStatusCode();
        assert(returnString.equals(200));      
	}
	
	
	
	@Test
	public void testgetSupportDocs() throws Exception {
        
		ResponseEntity res = this.controller.getSupportingDocs(1);
        Assert.assertNotNull(res);
 
        HttpStatus returnString = res.getStatusCode();
        assert(returnString.equals(200)); 
        
	}
	
	
	@Test
	public void testsaveSupportDocument() throws Exception {
        		
		SupportingDocumentEntity sde = new SupportingDocumentEntity();
		sde.setAsmntDocId(1);
		sde.setAssessmentId(1);
		sde.setAuthor("TEST AUTHOR");
		AssessmentEntity assessment = new AssessmentEntity();
		assessment.setAssessmentId(2);
		assessment.setAssessmentQtr(2);
		assessment.setAssessmentType("CIG");
		assessment.setAssessmentYr(2018);
		assessment.setSubmittedInd("Not Submitted");
		sde.setAssessment(assessment);
		sde.setAssessmentPdf("TestAssessment.pdf");
		sde.setDescription("Test Description");
		sde.setDocumentNumber(1L);
		sde.setFilename("Test FileName");
		sde.setVersionNum(1);
		
		ResponseEntity res = this.controller.saveDoc(sde, 1);
        Assert.assertNotNull(res);
 
        HttpStatus returnString = res.getStatusCode();
        assert(returnString.equals(200)); 
        
	}
	
	@Test
   // Make sure to upload a document to tu_assessment_document table to pass the below test
	public void testgetDocByDocId() throws Exception {
        	
		ResponseEntity res = this.controller.getDocument(68);
        Assert.assertNotNull(res);
 
        HttpStatus returnString = res.getStatusCode();
        assert(returnString.equals(200)); 
        
	}
	
	@Test
	public void testdeleteByDocId() throws Exception {
        
		ResponseEntity res = this.controller.deleteDocument(1);
        Assert.assertNotNull(res);
 
        HttpStatus returnString = res.getStatusCode();
        assert(returnString.equals(200)); 
        
	}
	
	
	
	
	
	
	@Test	
	@Ignore
	public void testGetCigarsAssessmentsReportDetails() throws Exception {
		
		List<CigarAssessmentRptDetail> mockrptDetails = this.createMockCigarAssessmentReportDetails();
    

        this.mockMvc
                .perform(get("/api/cigars/assessment/report/{fiscalYr}",2017))
                .andExpect(status().isOk())
                .andExpect(content()
                		.contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print());
        
	}

	private List<CigarAssessmentRptDetail> createMockCigarAssessmentReportDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.cigarsassessment.CigarAssessmentRestController#getCigarsAssessmentsReconDetails(long)}.
	 */
	@Test
	public void testGetCigarsAssessmentsReconDetails() throws Exception {
		List<CigarAssessmentRptDetail> mockrptDetails = this.createMockCigarAssessmentReportDetails();
     

        this.mockMvc
                .perform(get("/api/cigars/assessment/recon/{fiscalYr}",2017))
                .andExpect(status().isOk())
                .andExpect(content()
                		.contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print());

        //verify(assessmentBizComp, times(1)).getCigarAssessmentsReportDetail(2017);
        //verifyNoMoreInteractions(assessmentBizComp);
    }

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.cigarsassessment.CigarAssessmentRestController#saveDoc(gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity, long)}.
	 */
	//@Test
	//public void testSaveDoc() throws Exception {
		//SupportingDocumentEntity mockSupportingDocument = this.createMockSupportingDocument();
       // String supportDocJson = json(mockSupportingDocument);
      

      //  MvcResult result = this.mockMvc.perform(put("/api/cigars/assessment//docs/{assessId}", 1)
          //      .contentType(this.contentType)
           //     .content(supportDocJson)).andExpect(content()
                       // .contentTypeCompatibleWith(this.contentType))
           //     .andExpect(status().isOk())
           //    .andDo(MockMvcResultHandlers.print()).andReturn();

        //verify(assessmentBizComp, times(1)).saveSupportDocument((SupportingDocumentEntity)notNull());
        //verifyNoMoreInteractions(assessmentBizComp);

       // String returnString = result.getResponse().getContentAsString();
      //  assertEquals(returnString, "Document Saved");
	//}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.cigarsassessment.CigarAssessmentRestController#getSupportingDocs(long)}.
	 */
	@Test
	public void testGetSupportingDocs() throws Exception {
		
		
		List<SupportingDocument> mockSupportingDocuments = this.createMockSupportingDocuments();
    

        this.mockMvc
                .perform(get("/api/cigars/assessment/{assessId}/docs",1))
                .andExpect(status().isOk())
                .andExpect(content()
                		.contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print());
        
	}

	private List<SupportingDocument> createMockSupportingDocuments() {
		List<SupportingDocument> docs = new ArrayList<>();
		SupportingDocument sd = new SupportingDocument();
		sd.setAsmntDocId(1);
		sd.setAssessmentId(1);
		sd.setAuthor("TEST AUTHOR");
		sd.setDescription("Test Description");
		sd.setDocumentNumber(1L);
		sd.setFilename("Test FileName");
		sd.setVersionNum(1);
		docs.add(sd);
		return docs;
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.cigarsassessment.CigarAssessmentRestController#deleteDocument(long)}.
	 */
	@Test
	@Ignore
	public void testDeleteDocument() throws Exception {
	

		 MvcResult result = mockMvc.perform(
                delete("/api/cig ars/assessment/docs/{asmtdocId}", 1))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print()).andReturn();

        //verify(assessmentBizComp, times(1)).deleteByDocId(1);
        //verifyNoMoreInteractions(assessmentBizComp);

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, "Removed document");

	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.cigarsassessment.CigarAssessmentRestController#getDocument(long)}.
	 */
	@Test
	@Ignore
	public void testGetDocument() throws Exception {
		SupportingDocumentEntity mockSupportingDocument = this.createMockSupportingDocument();
      
        this.mockMvc
                .perform(get("/api/cigars/assessment/docs/{asmtdocId}",1))
                .andExpect(status().isOk())
                .andExpect(content()
                		.contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print());
      
	}

	private SupportingDocumentEntity createMockSupportingDocument() {
		SupportingDocumentEntity sde = new SupportingDocumentEntity();
		sde.setAsmntDocId(1);
		sde.setAssessmentId(1);
		sde.setAuthor("TEST AUTHOR");
		AssessmentEntity assessment = new AssessmentEntity();
		assessment.setAssessmentId(1);
		assessment.setAssessmentQtr(1);
		assessment.setAssessmentType("CIG");
		assessment.setAssessmentYr(2017);
		assessment.setSubmittedInd("Not Submitted");
		sde.setAssessment(assessment);
		sde.setAssessmentPdf("TestAssessment.pdf");
		sde.setDescription("Test Description");
		sde.setDocumentNumber(1L);
		sde.setFilename("Test FileName");
		sde.setVersionNum(1);
		return sde;
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.cigarsassessment.CigarAssessmentRestController#getReconExport(int)}.
	 */
	@Test
	public void testGetReconExport() throws Exception {
		AssessmentExport mockasmtexport = this.createMockAssessmentExport();
       
        this.mockMvc
                .perform(get("/api/cigars/assessment/export/fiscalyear/{year}", 2017))
                .andExpect(status().isOk())
                .andExpect(content()
                		.contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print());
        //verify(assessmentBizComp, times(1)).getCigarReconExport(2017);
        //verifyNoMoreInteractions(assessmentBizComp);
	}

	private AssessmentExport createMockAssessmentExport() {
		AssessmentExport ae = new AssessmentExport();
		ae.setCsv("Test.csv");
		ae.setTitle("Test Title");
		ae.setTobaccoType("Cigars");
		return ae;
	}

}


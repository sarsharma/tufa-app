/**
 *
 */
package gov.hhs.fda.ctp.web.companymgmt;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.CompanyHistory;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.util.Constants;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;


/**
 * @author tgunter
 *
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
    @ContextConfiguration(classes = {MvcConfig.class}),
    @ContextConfiguration(classes = {WebTestConfig.class})
})
public class TestCompanyMgmtRestController {
	@InjectMocks
	CompanyMgmtRestController controller;

    @Mock
    private CompanyMgmtBizComp companyMgmtBizComp;

    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
//
//        this.mockMvc = MockMvcBuilders
//                .webAppContextSetup(this.wac)
//                .build();
    }


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController#create(gov.hhs.fda.ctp.common.beans.Company)}.
	 */
	@Test
	public void testCreate() throws Exception {
        Company mockCompany = this.createMockCompany();
        String companyJson = json(mockCompany);

        when(
                this.companyMgmtBizComp.createCompany(
                  (Company)notNull()
                )
              ).thenReturn(mockCompany);

        MvcResult result = this.mockMvc
                .perform(post("/api/v1/companies")
                .contentType(this.contentType)
                .content(companyJson))
                .andExpect(status().isCreated())
                .andDo(MockMvcResultHandlers.print()).andReturn();

        // verify(companyMgmtBizComp, times(1)).createCompany((Company)notNull());
        // verifyNoMoreInteractions(companyMgmtBizComp);

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, companyJson);
	}

    /**
     * Test creating a malformed company.
     * @throws Exception
     */
    @Test
    public void testCreateBadCompany() throws Exception {
        Company mockCompany = this.createBadMockCompany();
        String companyJson = json(mockCompany);

        when(
                this.companyMgmtBizComp.createCompany(
                  (Company)notNull()
                )
              ).thenReturn(mockCompany);

        this.mockMvc
                .perform(post("/api/v1/companies")
                .contentType(this.contentType)
                .content(companyJson))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is4xxClientError());

        // verify(companyMgmtBizComp, times(1)).createCompany((Company)notNull());
        // verifyNoMoreInteractions(companyMgmtBizComp);
    }


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController#read(long)}.
	 */
	@Test
	public void testRead() throws Exception {
        Company mockCompany = this.createMockCompany();
        given(this.companyMgmtBizComp.getCompany(1)).willReturn(mockCompany);

        this.mockMvc
                .perform(get("/api/v1/companies/{id}",1))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print());

        // verify(companyMgmtBizComp, times(1)).getCompany(1);
        // verifyNoMoreInteractions(companyMgmtBizComp);
	}



	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController#update(gov.hhs.fda.ctp.common.beans.Company)}.
     * Test updating a well formed company.
     * @throws Exception
     */
    @Test
    public void testUpdate() throws Exception {
        Company mockCompany = this.createMockCompany();
        String companyJson = json(mockCompany);
        when(
                this.companyMgmtBizComp.updateCompany(
                  (Company)notNull()
                )
              ).thenReturn(mockCompany);

        MvcResult result = this.mockMvc
        .perform(put("/api/v1/companies")
                .contentType(this.contentType)
                .content(companyJson)).andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
        		.andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print()).andReturn();

        // verify(companyMgmtBizComp, times(1)).updateCompany((Company)notNull());
        // verifyNoMoreInteractions(companyMgmtBizComp);

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, companyJson);
    }

    /**
     * Test updating a well formed company.
     * @throws Exception
     */
    @Test
    public void testUpdateBadCompany() throws Exception {
        Company mockCompany = this.createBadMockCompany();
        String companyJson = json(mockCompany);
        when(
                this.companyMgmtBizComp.updateCompany(
                  (Company)notNull()
                )
              ).thenReturn(mockCompany);

        this.mockMvc.perform(put("/api/v1/companies")
                .contentType(this.contentType)
                .content(companyJson)).andExpect(status().is4xxClientError());
    }



	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController#createPermit(gov.hhs.fda.ctp.common.beans.Permit, long)}.
     * Test creating a well formed permit.
     * @throws Exception
     */
    @Test
    public void testCreatePermit() throws Exception{

    	Company mockCompany = this.createMockCompany();
    	String companyJson = json(mockCompany);

    	Permit mockPermit  = this.createMockPermit();
        String permitJson = json(mockPermit);

        when(
                this.companyMgmtBizComp.createPermit(any(Permit.class)))
        .thenReturn(mockCompany);

        MvcResult result = this.mockMvc
                .perform(post("/api/v1/companies/{id}/permits",1)
                .contentType(this.contentType)
                .content(permitJson))
                .andExpect(status().isCreated())
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, companyJson);

    }

    /**
     * Test creating a malformed permit.
     * @throws Exception
     */
    @Test
    public void testCreateBadPermit() throws Exception{
        /*
         * Create a bad permit (12 digit permit number).
         */
        Company mockCompany = this.createMockCompany();
        Permit mockPermit  = this.createBadMockPermit();
        String permitJson = json(mockPermit);

        when(
                this.companyMgmtBizComp.createPermit(any(Permit.class)))
        .thenReturn(mockCompany);

        this.mockMvc
                .perform(post("/api/v1/companies/{id}/permits",1)
                .contentType(this.contentType)
                .content(permitJson)).
                andExpect(status().is4xxClientError())
                .andDo(MockMvcResultHandlers.print()).andReturn();
    }

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController#updatePermit(gov.hhs.fda.ctp.common.beans.Permit, long, long)}.
     * Test updating a well-formed permit.
     * @throws Exception
     */
    @Test
    public void testUpdatePermit() throws Exception{

        Company mockCompany = this.createMockCompany();
        String companyJson = json(mockCompany);

        Permit mockPermit  = this.createMockPermit();
        String permitJson = json(mockPermit);

        when(
                this.companyMgmtBizComp.updatePermit(any(Permit.class)))
        .thenReturn(mockCompany);

        MvcResult result = this.mockMvc
                .perform(put("/api/v1/companies/{id}/permits/{permitId}", 1, 1)
                .contentType(this.contentType)
                .content(permitJson))
                .andDo(MockMvcResultHandlers.print()).andReturn();
        String returnString = result.getResponse().getContentAsString();
        assert(returnString.equals(companyJson));

    }

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController#updatePermit(gov.hhs.fda.ctp.common.beans.Permit, long, long)}.
     * Test updating a malformed permit.
     * @throws Exception
     */
    @Test
    public void testUpdateBadPermit() throws Exception{
        /*
         * Create a bad permit (12 digit permit number).
         */
        Company mockCompany = this.createMockCompany();
        Permit mockPermit  = this.createBadMockPermit();
        String permitJson = json(mockPermit);

        when(
                this.companyMgmtBizComp.updatePermit(any(Permit.class)))
        .thenReturn(mockCompany);

        this.mockMvc
                .perform(post("/api/v1/companies/{id}/permits/{permitId}", 1, 1)
                .contentType(this.contentType)
                .content(permitJson))
                .andExpect(status().is4xxClientError());

    }

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController#updatePermit(gov.hhs.fda.ctp.common.beans.Permit, long, long)}.
     * Test updating a malformed permit.
     * @throws Exception
     */
    @Test
    public void testUpdateBadClosedPermit() throws Exception{
        /*
         * Create a bad permit (12 digit permit number).
         */
        Company mockCompany = this.createMockCompany();
        Permit mockPermit  = this.createClosedPermitNoDate();
        String permitJson = json(mockPermit);

        when(
                this.companyMgmtBizComp.updatePermit(any(Permit.class)))
        .thenReturn(mockCompany);

        this.mockMvc
                .perform(post("/api/v1/companies/{id}/permits/{permitId}", 1, 1)
                .contentType(this.contentType)
                .content(permitJson))
                .andExpect(status().is4xxClientError());

    }


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController#getPermit(long)}.
     * Test getting a company by company id by permit id.
     * @throws Exception
     */
    @Test
    public void testGetPermit() throws Exception {
        Permit mockPermit = this.createMockPermit();

        given(this.companyMgmtBizComp.getPermit(1)).willReturn(mockPermit);

        this.mockMvc
                .perform(get("/api/v1/companies/{id}/permits/{permitId}",1,1))
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
        		.andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController#getCompanies(gov.hhs.fda.ctp.common.beans.CompanySearchCriteria)}.
	 */
	@Test
	public void testGetCompanies() throws Exception {
		CompanySearchCriteria mockCompanySearchCriteria = this.createMockSearchCriteria();
        String searchCriteriaJson = json(mockCompanySearchCriteria);

        when(
                this.companyMgmtBizComp.getCompanies(any(CompanySearchCriteria.class)))
        .thenReturn(mockCompanySearchCriteria);

        MvcResult result = this.mockMvc
                .perform(put("/api/v1/companies/search")
                .contentType(this.contentType)
                .content(searchCriteriaJson))
        		.andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, searchCriteriaJson);
	}


	private CompanySearchCriteria createMockSearchCriteria() {
		CompanySearchCriteria csr = new CompanySearchCriteria();
		csr.setCompanyIdFilter(1L);
		csr.setCompanyNameFilter("TEST");
		Set<String> companyStatusFilters = new HashSet<>();
		companyStatusFilters.add("Submitted");
		csr.setCompanyStatusFilters(companyStatusFilters);
		List<CompanyHistory> results = new ArrayList<>();
		CompanyHistory ch = new CompanyHistory();
		ch.setCompanyId(1L);
		ch.setCompanyName("TEST");
		ch.setCompanyStatus("Active");
		ch.setPermits("TT-BB-12345");
		results.add(ch);
		csr.setResults(results);
		return csr;
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController#getPermits(java.lang.String, long)}.
	 */
	@Test
	@Ignore ("Need to figure it out")
	public void testGetPermits() throws Exception {
        List<Permit> mockPermits = this.createMockPermits();
        // String permitsJson = json(mockPermits);

        given(this.companyMgmtBizComp.getPermits("active", 1L)).willReturn(mockPermits);

        this.mockMvc
                .perform(get("/api/v1/companies/{id}/permits?status=active",1))
        		.andExpect(status().isOk());
//                .andDo(MockMvcResultHandlers.print()).andReturn();
//
//        String returnString = result.getResponse().getContentAsString();
//        assertEquals(returnString, permitsJson);
	}

	private List<Permit> createMockPermits() {
		List<Permit>  permits = new ArrayList<>();
		Permit permit = this.createMockPermit();
		permits.add(permit);
		return permits;
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController#getCompanyPermitHistory(long)}.
	 */
	@Test
	public void testGetCompanyPermitHistory() throws Exception {
		Company mockCompany = this.createMockCompany();

        given(this.companyMgmtBizComp.getCompany(1)).willReturn(mockCompany);
        when(
                this.companyMgmtBizComp.getCompanyPermitHistory(any(Company.class)))
        .thenReturn(mockCompany);

        this.mockMvc
                .perform(get("/api/v1/companies/{companyId}/permits/history",1))
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
        		.andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
	}
	
/*	@Test
	public void excludeAllPermits() throws Exception {
	
		Company mockCompany = this.createMockCompany();

        given(this.companyMgmtBizComp.getCompany(1)).willReturn(mockCompany);
        when(
                this.companyMgmtBizComp.getCompanyPermitHistory(any(Company.class)))
        .thenReturn(mockCompany);

        this.mockMvc
                .perform(get("/api/v1/companies/{companyId}/permits/history",1))
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
        		.andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	public void getExcludePermitDetails() throws Exception {
		Company mockCompany = this.createMockCompany();
		   
		 //  List<String> list = this.mockMvc(List.class);
		   List<?> someobj = new ArrayList<>();
		   String mockComments = "API Called" ;
           String commentJson = json(mockComments);
           
		 given(this.companyMgmtBizComp.getExcludePermitDetails(121, 40)).willReturn((List<?>) someobj);

	        this.mockMvc
	                .perform(get("/api/v1/companies/getexcludepermitdetails/{companyid}/{permitid}",1))
	        		.andExpect(status().isOk());
	}*/

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController#updateCompanyComment(java.lang.String, long)}.
	 */
	@Test
	public void testUpdateCompanyComment() throws Exception {
		String mockComments = "Test Comment";
        String commentJson = json(mockComments);

        when(
                this.companyMgmtBizComp.updateCompanyComment(anyLong(), anyString()))
        .thenReturn(mockComments);

        MvcResult result = this.mockMvc
                .perform(put("/api/v1/companies/{id}/comment", 1)
                .contentType(this.contentType)
                .content(commentJson))
        		.andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, mockComments);
	}
	
	@Test
	public void testGetActiveImptCompanyByEIN () throws Exception {
		Company mockCompany = this.createMockCompany();
		when(
				this.companyMgmtBizComp.getActvImptCompByEIN(anyString())).thenReturn(mockCompany);
        
		this.mockMvc
        .perform(get("/api/v1/companies/impt/active/ein/{ein}","061234567"))
        .andExpect(content()
                .contentTypeCompatibleWith(this.contentType))
		.andExpect(status().isOk())
        .andDo(MockMvcResultHandlers.print());
	}

    @SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }


    /**
     * Create a well-formed mock company.
     * @return
     */
    private Company createMockCompany() {
        Company mockCompany = new Company();
        mockCompany.setCompanyId(1);
        mockCompany.setLegalName("TESTLEGAL");
        mockCompany.setEinNumber("061234567");
        Permit mockPermit = new Permit();
        mockPermit.setCompanyId(1);
        mockPermit.setPermitNum("12345678901");
        mockPermit.setPermitTypeCd("TBD");
        mockPermit.setPermitStatusTypeCd("TBD");
        mockPermit.setIssueDt("10/10/2016");
        Set<Permit> permits = new HashSet<Permit>();
        permits.add(mockPermit);
        return mockCompany;
    }
    /**
     * Create a malformed mock company.
     * @return
     */
    private Company createBadMockCompany() {
        Company mockCompany = new Company();
        mockCompany.setCompanyId(1);
        mockCompany.setLegalName("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901");
        mockCompany.setEinNumber("");
        Permit mockPermit = new Permit();
        mockPermit.setCompanyId(1);
        mockPermit.setPermitNum("12345678901");
        mockPermit.setPermitTypeCd("TBD");
        mockPermit.setPermitStatusTypeCd("TBD");
        mockPermit.setIssueDt("12/25/2016");
        Set<Permit> permits = new HashSet<Permit>();
        permits.add(mockPermit);
        return mockCompany;
    }

    /**
     * Create a well-formed mock permit.
     * @return
     */
    private Permit createMockPermit(){
		Permit mockPermit = new Permit();
		mockPermit.setIssueDt("10/10/2016");
		mockPermit.setPermitNum("12345678901");
		mockPermit.setCompanyId(1);

		return mockPermit;
    }

    /**
     * Create a malformed mock permit (permitNum > 11 digits).
     * @return
     */
    private Permit createBadMockPermit() {
        Permit mockPermit = new Permit();
        mockPermit.setIssueDt("12/25/2095");
        mockPermit.setPermitNum("123456789012");
        mockPermit.setCompanyId(1);

        return mockPermit;
    }

    private Permit createClosedPermitNoDate() {
        Permit mockPermit = createMockPermit();
        mockPermit.setPermitStatusTypeCd(Constants.CLOSED_CODE);
        mockPermit.setCloseDt("");
        return mockPermit;
    }

}
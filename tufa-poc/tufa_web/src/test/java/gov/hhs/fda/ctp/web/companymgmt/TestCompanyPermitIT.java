package gov.hhs.fda.ctp.web.companymgmt;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.FormComment;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;
import gov.hhs.fda.ctp.web.permit.PermitRestController;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@ActiveProfiles({"persistenceingestmaptest"})
@RunWith(SpringRunner.class)
@WebAppConfiguration
@Transactional
@ContextHierarchy({
	@ContextConfiguration(classes = {PersistenceHibernateTestConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class})
})
public class TestCompanyPermitIT {
	
	@Autowired
	PermitRestController permitController;
	
	@Autowired
	CompanyMgmtRestController companyController;
	
	@Test
	public void testPermit() throws Exception{
		Permit permit = this.permitController.getHistory3852(85L).getBody();
		Set<String> fy = (Set<String>) this.permitController.getAvailablePermitReportFiscalYears().getBody();
		Set<String> cy = (Set<String>) this.permitController.getAvailablePermitReportCalendarYears().getBody();
		Permit permits = (Permit) this.permitController.getHistory(1).getBody();
		PermitHistoryCriteria criteria = new PermitHistoryCriteria();
		criteria.setYearFilters(new HashSet(Arrays.asList("2017","2018","5000")));
		criteria.setMonthFilters(new HashSet(Arrays.asList("JAN","FEB","MAR")));
		criteria.setReportStatusFilters(new HashSet(Arrays.asList("ERRO","NSTD","COMP")));
		PermitHistoryCriteria retcriteria = this.permitController.getHistoryCriteria(criteria).getBody();
		assertThat(permits.getPermitTypeCd(), is("Importer"));
		assertThat(permits.getPermitStatusTypeCd(), is("ACTV"));
		assertThat(fy.contains("2014"), is(true));
		assertThat(cy.contains("2014"), is(true));
		assert(retcriteria.getResults().size() > 14);
	}
	
	@Test
	public void testCompanyPermitHistory() throws Exception{
		Company company = this.companyController.getCompanyPermitHistory(122L).getBody();
		List<Permit> permits =  this.companyController.getPermits("ACTV", 1).getBody();
		Company compEin = this.companyController.readByEin("987654321").getBody();
		Permit permit = new Permit();
		permit.setPermitId(83);permit.setCompanyId(83);permit.setPermitStatusTypeCd("CLSD");
		permit.setPermitNum("TPCA15003");permit.setCloseDt("10/13/2017");
		Company updatePermit = this.companyController.updatePermit(permit, 83, 83).getBody();
		assertThat(company.getCompanyStatus(), is("ACTV"));
		assertThat(company.getLegalName(), is("Shanaya Testing "));
		assertThat(permits.size(), is(2));
		assertThat(compEin.getLegalName(), is("Shanaya Testing "));
	}
}




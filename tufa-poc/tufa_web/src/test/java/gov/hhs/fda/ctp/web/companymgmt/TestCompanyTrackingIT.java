package gov.hhs.fda.ctp.web.companymgmt;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.AssertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

import static org.junit.Assert.assertTrue;

@ActiveProfiles({"persistenceingestmaptest"})
@RunWith(SpringRunner.class)
@WebAppConfiguration
@Transactional
@ContextHierarchy({
	@ContextConfiguration(classes = {PersistenceHibernateTestConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class})
})
public class TestCompanyTrackingIT {
	
	@Autowired
	CompanyMgmtRestController companyController;
	
	@Before
	public void setup() {
		
	}
	
	@After
	public void teardown() {
		
	}
	
	@Test
	public void testCompanyTrackingResults() {
		CompanySearchCriteria criteria = new CompanySearchCriteria();
		ResponseEntity results =  this.companyController.getCompanyTrackingInformation(criteria);
	    
        Map<String, Object> obj = (Map<String, Object>) results.getBody(); 
        
        List<Company> companies = (List<Company>) obj.get("results");
        
        for(Company company : companies){
        	assertTrue("N".equals(company.getWelcomeLetterFlag()) || ("Y".equals(company.getWelcomeLetterFlag()) && company.getWelcomeLetterSentDt() != null));
        }
	}
	
	@Test
	public void testCompanyTrackingFilters() {
		CompanySearchCriteria criteria = new CompanySearchCriteria();
		
		//name
		criteria.setCompanyNameFilter("a");
        List<Company> companies = this.getCompanyResults(criteria);
        criteria.setCompanyNameFilter(null);
        
        for(Company company : companies){
        	assertTrue(company.getLegalName().toLowerCase().contains("a"));
        }
        
        //ein
        criteria.setCompanyEINFilter("0");
        companies = this.getCompanyResults(criteria);
        criteria.setCompanyEINFilter(null);
        
        for(Company company : companies){
        	assertTrue(company.getEinNumber().contains("0"));
        }
        
        //creadted date
        //welcome letter date
        
        //email set
        criteria.setEmailAddressFlagFilter("N");
        companies = this.getCompanyResults(criteria);
        criteria.setEmailAddressFlagFilter(null);
        
        for(Company company : companies){
        	assertTrue("N".equals(company.getEmailAddressFlag()));
        }
        
        //address set
        criteria.setPhysicalAddressFlagFilter("N");
        companies = this.getCompanyResults(criteria);
        criteria.setPhysicalAddressFlagFilter(null);
        
        for(Company company : companies){
        	assertTrue("N".equals(company.getPhysicalAddressFlag()));
        }
        
        //welcome letter set
        criteria.setWelcomeLetterFlagFilter("N");
        companies = this.getCompanyResults(criteria);
        criteria.setWelcomeLetterFlagFilter(null);
        
        for(Company company : companies){
        	assertTrue("N".equals(company.getWelcomeLetterFlag()));
        }
	}

	private List<Company> getCompanyResults(CompanySearchCriteria criteria) {

		ResponseEntity results =  this.companyController.getCompanyTrackingInformation(criteria);
	    
        Map<String, Object> obj = (Map<String, Object>) results.getBody(); 
        
        return (List<Company>) obj.get("results");
	}
}

package gov.hhs.fda.ctp.web.contactmgmt;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import gov.hhs.fda.ctp.biz.contactmgmt.ContactMgmtBizComp;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.persistence.model.ReportContactEntity;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {MvcConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class})
})
public class TestContactMgmtRestController {
    // @Autowired
    // private WebApplicationContext wac;

	@InjectMocks
	ContactMgmtRestController controller;

	@Mock
    private ContactMgmtBizComp contactMgmtBizComp;

    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
    }

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.contactmgmt.ContactMgmtRestController#create(gov.hhs.fda.ctp.common.beans.Contact)}.
	 */
	@Test
	public void testCreate() throws Exception {
		Contact mockContact = this.createMockContact();
        String contactJson = json(mockContact);

        when(
                this.contactMgmtBizComp.create(
                		(Contact)notNull()
                )
              ).thenReturn(mockContact);


        MvcResult result = this.mockMvc
                .perform(post("/api/v1/companies/contact")
                .contentType(this.contentType)
                .content(contactJson))
                .andExpect(status().isCreated())
                .andDo(MockMvcResultHandlers.print()).andReturn();

        // verify(companyMgmtBizComp, times(1)).createCompany((Company)notNull());
        // verifyNoMoreInteractions(companyMgmtBizComp);

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, contactJson);
	}

	private Company createMockCompany(){
        Company mockCompany = new Company();
        mockCompany.setCompanyId(1);
        mockCompany.setLegalName("TESTLEGAL");
        mockCompany.setEinNumber("061234567");
        Permit mockPermit = new Permit();
        mockPermit.setCompanyId(1);
        mockPermit.setPermitNum("12345678901");
        mockPermit.setPermitTypeCd("TBD");
        mockPermit.setPermitStatusTypeCd("TBD");
        mockPermit.setIssueDt("10/10/2016");
        Set<Permit> permits = new HashSet<Permit>();
        permits.add(mockPermit);
        return mockCompany;
	}

	private Contact createMockContact() {
    	Contact newContact = new Contact();
    	Company newCompany = this.createMockCompany();
    	newContact.setCompany(newCompany);
    	newContact.setCompanyId(newCompany.getCompanyId());
    	newContact.setFirstNm("UNIT");
    	newContact.setLastNm("TEST");
    	newContact.setEmailAddress("unittest@test.com");
    	newContact.setPhoneNum("6018180241");
    	newContact.setCntryDialCd("123");
    	newContact.setCountryCd("US");
    	newContact.setCntryFaxDialCd("1");
    	newContact.setFaxNum("3212337184");
    	return newContact;
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.contactmgmt.ContactMgmtRestController#read(long)}.
	 */
	@Test
	public void testRead() throws Exception {
		Contact mockContact = this.createMockContact();
        String contactJson = json(mockContact);

        given(this.contactMgmtBizComp.readById(1)).willReturn(mockContact);

        MvcResult result = this.mockMvc
                .perform(get("/api/v1/companies/contact/{id}",1))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, contactJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.contactmgmt.ContactMgmtRestController#update(gov.hhs.fda.ctp.common.beans.Contact)}.
	 */
	@Test
	public void testUpdate() throws Exception {
		Contact mockContact = this.createMockContact();
        String contactJson = json(mockContact);
        when(
                this.contactMgmtBizComp.update(
                  (Contact)notNull()
                )
              ).thenReturn(mockContact);

        MvcResult result = this.mockMvc
        .perform(put("/api/v1/companies/contact")
                .contentType(this.contentType)
                .content(contactJson)).andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
        		.andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print()).andReturn();

        // verify(companyMgmtBizComp, times(1)).updateCompany((Company)notNull());
        // verifyNoMoreInteractions(companyMgmtBizComp);

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, contactJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.contactmgmt.ContactMgmtRestController#delete(long)}.
	 */
	@Test
	public void testDelete() throws Exception {
		Contact mockContact = this.createMockContact();
        String contactJson = json(mockContact);

        when(this.contactMgmtBizComp.readById(1)).thenReturn(mockContact);
		doNothing().when(this.contactMgmtBizComp).deleteById(1);

		 MvcResult result = mockMvc.perform(
               delete("/api/v1/companies/contact/{id}", 1))
               .andExpect(status().isOk())
               .andDo(MockMvcResultHandlers.print()).andReturn();

       //verify(assessmentBizComp, times(1)).deleteByDocId(1);
       //verifyNoMoreInteractions(assessmentBizComp);

       String returnString = result.getResponse().getContentAsString();
       assertEquals(returnString, contactJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.contactmgmt.ContactMgmtRestController#getReportContacts(int, int, int)}.
	 */
	@Test
	public void testGetReportContacts() throws Exception {
		List<ReportContactEntity> mockContacts = this.createMockReportContacts();
        String contactJson = json(mockContacts);

        given(this.contactMgmtBizComp.getReportContacts(1, 1, 1)).willReturn(mockContacts);

        MvcResult result = this.mockMvc
                .perform(get("/api/v1/companies/contact/{companyId}/{permitId}/{periodId}",1, 1, 1))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, contactJson);
	}

    private List<ReportContactEntity> createMockReportContacts() {
		List<ReportContactEntity> contacts = new ArrayList<>();
		ReportContactEntity ce = new ReportContactEntity();
    	Company newCompany = this.createMockCompany();
    	ce.setCompanyId(newCompany.getCompanyId());
    	ce.setFirstNm("UNIT");
    	ce.setLastNm("TEST");
    	ce.setEmailAddress("unittest@test.com");
    	ce.setPhoneNum("6018180241");
    	ce.setCntryDialCd("123");
    	ce.setCountryCd("US");
    	ce.setCntryFaxDialCd("1");
    	ce.setFaxNum("3212337184");
    	contacts.add(ce);

		return contacts;

	}

	@SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}

package gov.hhs.fda.ctp.web.contactmgmt;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {PersistenceHibernateTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class})
})
@Transactional
public class TestContactMgmtRestControllerIT {

	@Autowired
	ContactMgmtRestController controller;
	
	@Before
	public void setup() {
		
	}
	
	@After
	public void teardown() {
		
	}
	
	@Test
	public void createAsPrimaryContact() {
		Contact mock = this.createMockContact();
		mock.setSetAsPrimary(true);
		Contact response = this.controller.create(mock).getBody();
		assertTrue(response != null);
		assertTrue(response.getPrimaryContactSequence() != null);
		assert(response.getPrimaryContactSequence().compareTo(1L) == 0);
	}
	
	private Contact createMockContact() {
		Contact mock = new Contact();
		mock.setEmailAddress("unitTest@test.com");
		mock.setPhoneNum("1234567890");
		mock.setCompanyId(1L);
		return mock;
	}
}

/**
 *
 */
package gov.hhs.fda.ctp.web.permit;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.biz.permit.PermitBizComp;
import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.CompanyPermitHistory;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.FormComment;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.common.beans.PermitMfgReport;
import gov.hhs.fda.ctp.common.beans.PermitPeriod;
import gov.hhs.fda.ctp.common.beans.SubmittedForm;
import gov.hhs.fda.ctp.common.beans.TobaccoClass;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

/**
 * @author tgunter
 *
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
    @ContextConfiguration(classes = {MvcConfig.class}),
    @ContextConfiguration(classes = {WebTestConfig.class})
})
public class TestPermitRestController {
	@InjectMocks
	PermitRestController controller;

	@Mock
    private CompanyMgmtBizComp companyMgmtBizComp;

	@Mock
    private PermitBizComp permitBizComp;

    private MockMvc mockMvc;
    @SuppressWarnings("all")
    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
    }

  	/**
	   * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#getHistory(long)}.
	   *
	   * @throws Exception the exception
	   */
	@Test
	public void testGetHistory() throws Exception {
        Permit mockPermit = this.createMockPermitWithHistory();
        String permitJson = json(mockPermit);

        when(
                this.companyMgmtBizComp.getPermit(1)
              ).thenReturn(mockPermit);
        when(
                this.companyMgmtBizComp.getPermitHistory(
                  (Permit)notNull()
                )
              ).thenReturn(mockPermit);

        MvcResult result = this.mockMvc
                .perform(get("/api/v1/permit/history/{id}",1))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, permitJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#getPagedHistory(long, int, int)}.
	 */
//	@Test
//	public void testGetPagedHistory() throws Exception {
//        Permit mockPermit = this.createMockPermitWithHistory();
//        String permitJson = json(mockPermit);
//
//        when(
//                this.companyMgmtBizComp.getPermit(1)
//              ).thenReturn(mockPermit);
//        when(
//                this.companyMgmtBizComp.getPaginatedPermitHistory(
//                  (Permit)notNull(), Mockito.anyInt(), Mockito.anyInt()                )
//              ).thenReturn(mockPermit);
//
//        MvcResult result = this.mockMvc
//                .perform(get("/api/v1/permit/history/{id}/page/{page}/rows/{rows}",1,1,1))
//                .andExpect(status().isOk())
//                .andExpect(content()
//                        .contentTypeCompatibleWith(this.contentType))
//                .andDo(MockMvcResultHandlers.print()).andReturn();
//
//        String returnString = result.getResponse().getContentAsString();
//        assertEquals(returnString, permitJson);
//	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#read(long)}.
	 */
	@Test
	public void testRead() throws Exception {
        Permit mockPermit = this.createMockPermitWithHistory();
        String permitJson = json(mockPermit);

        given(this.companyMgmtBizComp.getPermit(1)).willReturn(mockPermit);

        MvcResult result =  this.mockMvc
                .perform(get("/api/v1/permit/{id}",1))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, permitJson);
    }


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#updatePermitComments(java.lang.String, long)}.
	 */
	@Test
	public void testUpdatePermitComments() throws Exception {
		String permitComments = "Test Comment";
        String commentsJson = json(permitComments);

        when(
                this.permitBizComp.updatePermitComments(anyLong(), anyString())
              ).thenReturn(permitComments);

        MvcResult result =  this.mockMvc
	        .perform(put("/api/v1/permit/{id}/comment",1)
	        		.contentType(this.contentType)
	        		.content(commentsJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, permitComments);

	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#getHistoryCriteria(gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria)}.
	 */
	@Test
	public void testGetHistoryCriteria() throws Exception {
		PermitHistoryCriteria mockPermitHistoryCriteria = this.createMockCriteriaResult();
        String permitHistoryCriteriaJson = json(mockPermitHistoryCriteria);

        when(
                this.permitBizComp.getPermitHistories((PermitHistoryCriteria)notNull())
              ).thenReturn(mockPermitHistoryCriteria);

        MvcResult result =  this.mockMvc
	        .perform(put("/api/v1/permit/history")
	        		.contentType(this.contentType)
	        		.content(permitHistoryCriteriaJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, permitHistoryCriteriaJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#getPermitInfo(long, long)}.
	 */
	@Test
	public void testGetPermitInfo() throws Exception {
		PermitPeriod mockPermitPeriod = this.createMockPermitPeriod();
        String permitPeriodJson = json(mockPermitPeriod);

        given(this.permitBizComp.getPermitPeriod(anyLong(), anyLong())).willReturn(mockPermitPeriod);

        MvcResult result =  this.mockMvc
                .perform(get("/api/v1/permit/{permitId}/period/{periodId}",1, 1))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, permitPeriodJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#savePermitPeriodInfo(long, long, gov.hhs.fda.ctp.common.beans.PermitMfgReport)}.
	 */
	@Test
	public void testSavePermitPeriodInfo() throws Exception {
		PermitPeriod mockPermitPeriod = this.createMockPermitPeriod();
        String permitPeriodJson = json(mockPermitPeriod);

		PermitMfgReport mockPermitMfgReport= this.createMockPermitMfgReport();
        String permitMfgReportJson = json(mockPermitMfgReport);

        when(
                this.permitBizComp.saveSubmittedForms((PermitMfgReport)notNull())
              ).thenReturn(mockPermitPeriod);

        MvcResult result =  this.mockMvc
	        .perform(put("/api/v1/permit/{permitId}/period/{periodId}",1, 1)
	        		.contentType(this.contentType)
	        		.content(permitMfgReportJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, permitPeriodJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#createFormComment(long, long, gov.hhs.fda.ctp.common.beans.FormComment)}.
	 */
	@Test
	public void testCreateFormComment() throws Exception {
		FormComment mockFormComment = this.createMockFormComment();
        String formCommentJson = json(mockFormComment);

        when(
                this.permitBizComp.saveFormComment(
                  (FormComment)notNull(), anyLong(), anyLong()
                )
              ).thenReturn(mockFormComment);

        MvcResult result = this.mockMvc
                .perform(post("/api/v1/permit/{permitId}/period/{periodId}/comment", 1, 1)
                .contentType(this.contentType)
                .content(formCommentJson))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print()).andReturn();

        // verify(companyMgmtBizComp, times(1)).createCompany((Company)notNull());
        // verifyNoMoreInteractions(companyMgmtBizComp);

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, formCommentJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#deleteFormComment(long, long, java.util.List)}.
	 */
	@Test
	public void testDeleteFormComment() throws Exception {
		List<Long> mockformIds = new ArrayList<>();
		mockformIds.add(1L);
		mockformIds.add(2L);
        String formIdJson = json(mockformIds);
		doNothing().when(this.permitBizComp).deleteFormCommentsById(1);

		 MvcResult result = mockMvc.perform(
               delete("/api/v1/permit/{permitId}/period/{periodId}/comment", 1, 1)
               .contentType(this.contentType)
               .content(formIdJson))
               .andExpect(status().isOk())
               .andDo(MockMvcResultHandlers.print()).andReturn();

       //verify(assessmentBizComp, times(1)).deleteByDocId(1);
       //verifyNoMoreInteractions(assessmentBizComp);

       String returnString = result.getResponse().getContentAsString();
       assertEquals(returnString, "OK");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#savePermitContacts(gov.hhs.fda.ctp.common.beans.Permit)}.
	 */
	@Test
	public void testSavePermitContacts() throws Exception {
        Permit mockPermit = this.createMockPermitWithAddressAndPOC();
        String permitJson = json(mockPermit);

        when(
                this.permitBizComp.saveOrUpdatePermitContacts(
                        (Permit)notNull())
              ).thenReturn(mockPermit);

        MvcResult result = this.mockMvc
                .perform(put("/api/v1/permit/contacts")
                .contentType(this.contentType)
                .content(permitJson))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString,permitJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#getAvailablePermitReportFiscalYears()}.
	 */
	@Test
	public void testGetAvailablePermitReportFiscalYears() throws Exception {
		Set<String> mockyears = new HashSet<>();
		mockyears.add("2016");
		mockyears.add("2017");
        String yearsJson = json(mockyears);

        given(this.permitBizComp.getAvailablePermitReportFiscalYears()).willReturn(mockyears);

        MvcResult result =  this.mockMvc
                .perform(get("/api/v1/permit/fiscalyears"))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, yearsJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#getAvailablePermitReportCalendarYears()}.
	 */
	@Test
	public void testGetAvailablePermitReportCalendarYears() throws Exception {
		Set<String> mockyears = new HashSet<>();
		mockyears.add("2016");
		mockyears.add("2017");
        String yearsJson = json(mockyears);

        given(this.permitBizComp.getAvailablePermitReportCalendarYears()).willReturn(mockyears);

        MvcResult result =  this.mockMvc
                .perform(get("/api/v1/permit/calendaryears"))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, yearsJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#getTaxRates()}.
	 */
	@Test
	public void testGetTaxRates() throws Exception {
		List<TobaccoClass> mocktobaccoClasses = this.createMockTobaccoClasses();
        String tobaccoClassesJson = json(mocktobaccoClasses);

        given(this.permitBizComp.getLatestTobaccoClasses()).willReturn(mocktobaccoClasses);

        MvcResult result =  this.mockMvc
                .perform(get("/api/v1/permit/taxrates"))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, tobaccoClassesJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#saveTaxRates(java.util.List)}.
	 */
	@Test
	public void testSaveTaxRates() throws Exception {
		List<TobaccoClass> mocktobaccoClasses = this.createMockTobaccoClasses();
        String tobaccoClassesJson = json(mocktobaccoClasses);

        doNothing().when(this.permitBizComp).saveTobaccoClasses(
        		mocktobaccoClasses
              );

        MvcResult result = this.mockMvc
                .perform(put("/api/v1/permit/taxrates")
                .contentType(this.contentType)
                .content(tobaccoClassesJson))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, "Success");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permit.PermitRestController#deleteMonthlyReport(int, int, int)}.
	 */
	@Test
	public void testDeleteMonthlyReport() throws Exception {
		when(this.permitBizComp.deleteMonthlyReport(1, 1, 1)).thenReturn(true);

		mockMvc.perform(
		       delete("/api/v1/permit/deletemonthlyreport/{companyId}/{permitId}/{periodId}", 1, 1, 1))
		       .andExpect(status().isOk())
		       .andDo(MockMvcResultHandlers.print()).andReturn();

	}

    /*************************************************************
     * MOCK OBJECTS
     *************************************************************/
	private List<TobaccoClass> createMockTobaccoClasses() {
		List<TobaccoClass> tcentities = new ArrayList<>();
		TobaccoClass tce = new TobaccoClass();
		tce.setClassTypeCd("test");
		tce.setParentClassId(1L);
		tce.setTobaccoClassId(1L);
		tce.setTobaccoClassNm("Cigars");
		tcentities.add(tce);
		return tcentities;		
	}

    private Permit createMockPermitWithHistory() {
        Permit permit = new Permit();
        CompanyPermitHistory permitHistory = new CompanyPermitHistory();
        permitHistory.setDisplayMonthYear("January 2016");
        permitHistory.setReportStatusCd("Incomplete");
        List<CompanyPermitHistory> permitHistoryList = new ArrayList<CompanyPermitHistory>();
        permitHistoryList.add(permitHistory);

        permit.setPermitId(38);
        permit.setCompanyId(68);
        permit.setIssueDt("10/31/2015");
        permit.setPermitStatusTypeCd("Active");
        permit.setPermitTypeCd("Manufacturuer");
        permit.setPermitHistoryList(permitHistoryList);
        return permit;
    }

    private Permit createMockPermitWithAddressAndPOC() {
        Permit permit = new Permit();
        Company company = createMockCompanyWithAddresses();
        permit.setContacts(createMockContactList());
        permit.setCompany(company);
        return permit;
    }

    private Company createMockCompanyWithAddresses() {
        Company company = new Company();
        Address primaryAddress = new Address();
        Address alternateAddress = new Address();
        primaryAddress.setStreetAddress("111 PRIMARY CIRCLE");
        alternateAddress.setStreetAddress("222 ALTERNATE WAY");

        company.setEinNumber("061234567");
        company.setLegalName("TEST COMPANY");
        company.setPrimaryAddress(primaryAddress);
        company.setAlternateAddress(alternateAddress);
        return company;
    }

    private List<Contact> createMockContactList() {
        List<Contact> contacts = new ArrayList<Contact>();
        Contact contact1 = new Contact();
        Contact contact2 = new Contact();
        contact1.setFirstNm("TEST1");
        contact1.setLastNm("TESTER");
        contact2.setFirstNm("TEST2");
        contact2.setLastNm("TESTER");
        contacts.add(contact1);
        contacts.add(contact2);
        return contacts;
    }

	private FormComment createMockFormComment() {
		FormComment fc = new FormComment();
		fc.setAuthor("test author");
		fc.setCommentDate("1/1/2017");
		fc.setFormId(1L);
		fc.setFormTypeCd("test");
		fc.setTobaccoClassId(1L);
		fc.setUserComment("test comment");
		return fc;
	}

	private PermitMfgReport createMockPermitMfgReport() {
		PermitMfgReport rep = new PermitMfgReport();
		rep.setPeriodId(1L);
		rep.setPermitId(1L);
		rep.setStatus("Not Submitted");
		List<SubmittedForm> forms = new ArrayList<>();
		SubmittedForm sf = new SubmittedForm();
		sf.setFormId(1L);
		forms.add(sf);
		rep.setSubmittedForms(forms);
		return rep;
	}

	private PermitPeriod createMockPermitPeriod() {
		PermitPeriod pp = new PermitPeriod();
		pp.setMonth("Jan");
		pp.setPeriodId(1L);
		Permit permit;
		permit = this.createMockPermitWithHistory();
		pp.setPermit(permit);
		pp.setPermitId(1L);
		pp.setQuarter(1L);
		pp.setStatus("Not Started");
		List<SubmittedForm> forms = new ArrayList<>();
		SubmittedForm sf = new SubmittedForm();
		sf.setFormId(1L);
		forms.add(sf);
		pp.setSubmittedForms(forms);
		List<TobaccoClass> tobaccoClasses = new ArrayList<>();
		TobaccoClass tc = new TobaccoClass();
		tc.setClassTypeCd("test");
		tc.setConversionRate(1.0);
		tc.setTaxRate(1.0);
		tobaccoClasses.add(tc);
		pp.setYear(2017L);
		return pp;
	}

    private PermitHistoryCriteria createMockCriteriaResult() {
        PermitHistoryCriteria criteria = new PermitHistoryCriteria();
        criteria.setPermitNameFilter("TEST");
        Permit permit = createMockPermitWithHistory();
        List<CompanyPermitHistory> permitHistories = permit.getPermitHistoryList();
        criteria.setResults(permitHistories);
        return criteria;
    }

    /************************************************************
     *  JSONIZER
     ************************************************************/
    @SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}

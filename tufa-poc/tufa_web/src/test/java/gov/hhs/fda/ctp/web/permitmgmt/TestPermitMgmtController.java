package gov.hhs.fda.ctp.web.permitmgmt;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.biz.permit.PermitBizComp;
import gov.hhs.fda.ctp.biz.permitmgmt.PermitMgmtBizComp;
import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitAudit;
import gov.hhs.fda.ctp.common.beans.PermitAuditsExport;
import gov.hhs.fda.ctp.common.beans.PermitPeriod;
import gov.hhs.fda.ctp.common.beans.PermitUpdateUpload;
import gov.hhs.fda.ctp.common.beans.PermitUpdatesDocument;
import gov.hhs.fda.ctp.common.ingestion.FileIngestorFactory;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.persistence.model.PermitUpdateEntity;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@ActiveProfiles({"persistenceingestmaptest"})
@RunWith(SpringRunner.class)
@WebAppConfiguration
@Transactional
@ContextHierarchy({
	@ContextConfiguration(classes = {PersistenceHibernateTestConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class})
})
public class TestPermitMgmtController {
	@InjectMocks
	PermitMgmtRestController controller;

	@Mock
    private PermitMgmtBizComp permitBizComp;

    private MockMvc mockMvc;
    @SuppressWarnings("all")
    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
    }
    
    @Test
	public void testGetPermitUpdates() throws Exception {
    	PermitAuditsExport mockPermitAuditsExport = new PermitAuditsExport();
    	mockPermitAuditsExport.setFilename("IntegrationTest");
        String permitAuditsExportJson = json(mockPermitAuditsExport);

        given(this.permitBizComp.exportPermitUpdates()).willReturn(mockPermitAuditsExport);

        MvcResult result =  this.mockMvc
                .perform(get("/api/v1/permitmgmt/permits/audit"))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, permitAuditsExportJson);
	}
    
    @Test
	public void testGetPermitUpdateFiles() throws Exception {
    	List<PermitUpdatesDocument> permitUpdateFiles = new ArrayList();
    	PermitUpdatesDocument file =  new PermitUpdatesDocument();
    	permitUpdateFiles.add(file);
        String permitUpdateFilesJson = json(permitUpdateFiles);

        given(this.permitBizComp.getPermitUpdateFiles()).willReturn(permitUpdateFiles);

        MvcResult result =  this.mockMvc
                .perform(get("/api/v1/permitmgmt/permits/documents"))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, permitUpdateFilesJson);
	}
    
    
    @Test
    @Ignore
    // Priti working on this 
	public void testgetUpdatedPermitsAddress() throws Exception {
    	List<Address> address = new ArrayList();
    	Address add = new Address();
    	address.add(add);
        String addressJson = json(address);

        when(
                this.permitBizComp.getIngestedAddress()
              ).thenReturn(address);

        MvcResult result = this.mockMvc
                .perform(put("/api/v1/permitmgmt/permits/ingestedaddress")
                .contentType(this.contentType)
                .content(addressJson))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString,addressJson);
	}
    
    
    @Test 
    @Ignore
    // Priti working on this 
	public void testgetUpdatedPermitsContacts() throws Exception {
    	List<Contact> contacts = new ArrayList();
    	Contact cont = new Contact();
    	contacts.add(cont);
        String contactJson = json(contacts);

        when(
                this.permitBizComp.getIngestedContacts()     
              ).thenReturn(contacts);

        MvcResult result = this.mockMvc
                .perform(put("/api/v1/permitmgmt/permits/ingestedcontacts")
                .contentType(this.contentType)
                .content(contactJson))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString,contactJson);
	}
    
    
    
    @Test
    @Ignore
   
	public void testGetUpdatedPermits() throws Exception {
    	List<PermitUpdateUpload> updatedPermits = new ArrayList();
    	PermitUpdateUpload update = new PermitUpdateUpload();
    	updatedPermits.add(update);
        String permitJson = json(updatedPermits);

        when(
                this.permitBizComp.getUpdatedPermits(
                		anyLong())
              ).thenReturn(updatedPermits);

        MvcResult result = this.mockMvc
                .perform(put("/api/v1/permitmgmt/permits")
                .contentType(this.contentType)
                .content(permitJson))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString,permitJson);
	}
    
    @Test
	public void testDownloadAttachment() throws Exception {
    	PermitUpdatesDocument document = new PermitUpdatesDocument();
        String documentJson = json(document);

        given(this.permitBizComp.getPermitUpdatesDocument(anyLong())).willReturn(document);

        MvcResult result =  this.mockMvc
                .perform(get("/api/v1/permitmgmt/document/{id}",1))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, documentJson);
	}
    
    @Test
	public void testGetPermitAuditUpdates() throws Exception {
    	List<PermitAudit> audits = new ArrayList();
    	PermitAudit audit = new PermitAudit();
    	audits.add(audit);
        String documentJson = json(audits);

        given(this.permitBizComp.getPermitUpdatesByDocId(anyLong())).willReturn(audits);

        MvcResult result =  this.mockMvc
                .perform(get("/api/v1/permitmgmt/permits/audit/{docId}",1))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, documentJson);
	}
    /*
     * 
	
	
*/
    /************************************************************
     *  JSONIZER
     ************************************************************/
    @SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}

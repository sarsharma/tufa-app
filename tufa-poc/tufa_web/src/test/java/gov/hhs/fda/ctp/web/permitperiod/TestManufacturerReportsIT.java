package gov.hhs.fda.ctp.web.permitperiod;

import static org.junit.Assert.*;

import java.util.List;

import gov.hhs.fda.ctp.common.beans.FormComment;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.FormDetailEntity;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;
import gov.hhs.fda.ctp.persistence.model.SubmittedFormEntity;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;
import gov.hhs.fda.ctp.web.permit.PermitRestController;
import gov.hhs.fda.ctp.web.reporting.ReportController;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

@ActiveProfiles({"persistenceingestmaptest"})
@RunWith(SpringRunner.class)
@WebAppConfiguration
@Transactional
@ContextHierarchy({
	@ContextConfiguration(classes = {PersistenceHibernateTestConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class})
})
public class TestManufacturerReportsIT {
	@Autowired
	PermitPeriodRestController controller;
	
	@Autowired
	PermitRestController permitController;
	
	@Autowired
	ReportController reportController;
	
	@Test
	public void testMANUReportFeatures() throws Exception{
		PermitPeriodEntity ppe = this.controller.read(131, 85).getBody();
		ppe.setSourceCd("FAXX");
		for(SubmittedFormEntity subForm: ppe.getSubmittedForms()) {
			for(FormDetailEntity formDetail: subForm.getFormDetails()){
				formDetail.setRemovalQty((double) 100);
				formDetail.setTaxesPaid((double) 10);
				formDetail.setDolDifference((double) 1);
				if(formDetail.getFormId() == 200) {
					if(formDetail.getTobaccoClassId()==7 && formDetail.getLineNum()==0)
						formDetail.setActivityCd("ERRO");
				}
			}
		}
		this.controller.saveOrUpdate(ppe);
		ppe = this.controller.read(131, 85).getBody();
		for(SubmittedFormEntity subForm: ppe.getSubmittedForms()) {
			for(FormDetailEntity formDetail: subForm.getFormDetails()){
				assertThat(formDetail.getRemovalQty(), is((double)100));
				assertThat(formDetail.getTaxesPaid(), is((double) 10));
				assertThat(formDetail.getDolDifference(), is((double) 1));
				if(formDetail.getFormId() == 200){
					if(formDetail.getTobaccoClassId()==7 && formDetail.getLineNum()==0)
						assertThat(formDetail.getActivityCd(), is("ERRO"));
				}
			}
		}
		assertThat(ppe.getSourceCd(), is("FAXX"));
	}
	
	@Test
	public void testCommentsFeatures() throws Exception{
		FormComment comment = new FormComment();
		String[] formTypes = {"5024","2206","2105","7501","3852"};
		comment.setTobaccoClassId(8);
		comment.setUserComment("Integration Test");
		for(String s: formTypes) {
			comment.setFormTypeCd(s);
			comment = this.permitController.createFormComment(199, 159, comment).getBody();
			assertThat(comment.getUserComment(), is("Integration Test"));
		}
	}
	
	@Test
	public void testDocumentFeatures() throws Exception{
		AttachmentsEntity attachs = new AttachmentsEntity();
		attachs.setPermitId(199);
		attachs.setPeriodId(159);
		attachs.setFormTypes("FDA 3852");
		attachs.setDocFileNm("ATCH");
		attachs.setDocStatusCd("INCP");
		attachs.setDocDesc("Integration File");
		ObjectMapper mapper = new ObjectMapper();
		String metaData = mapper.writeValueAsString(attachs);
		
		MockMultipartFile file = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
		String result = this.reportController.addAttachments(file, metaData, 159, 199).getBody();
		assertThat(result, is("Successfully Uploaded"));
		attachs = ((List<AttachmentsEntity>) this.reportController.getAttachments(159, 199).getBody()).get(0);
		assertThat(attachs.getFormTypes(), is("FDA 3852"));
	}
	
	@Test
	public void testZEROReportFeatures() throws Exception{
		PermitPeriodEntity ppe = this.controller.read(131, 85).getBody();
		ppe.setZeroFlag("Y");
		this.controller.saveOrUpdate(ppe);
		ppe = this.controller.read(131, 85).getBody();
		List<FormComment> zero = (List<FormComment>) this.permitController.getZeroReportComment(1, 2).getBody();
		assertThat(zero.get(0).getUserComment(), is("TESTING SOMETHING"));
		//assertNull(ppe.getSubmittedForms());
		assertThat(ppe.getZeroFlag(), is("Y"));
		ppe.setZeroFlag("N");
		this.controller.saveOrUpdate(ppe);
	}
	
	
}

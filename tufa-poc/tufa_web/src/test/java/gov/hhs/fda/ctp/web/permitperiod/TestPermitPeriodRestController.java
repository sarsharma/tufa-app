/**
 *
 */
package gov.hhs.fda.ctp.web.permitperiod;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import gov.hhs.fda.ctp.biz.permitperiod.PermitPeriodBizComp;
import gov.hhs.fda.ctp.biz.reporting.AttachmentsBizComp;
import gov.hhs.fda.ctp.common.beans.PeriodStatus;
import gov.hhs.fda.ctp.common.config.FileIngestionConfig;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.MissingForms;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

/**
 * @author akari
 *
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {FileIngestionConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class})
})
public class TestPermitPeriodRestController {

	@InjectMocks
	PermitPeriodRestController controller;

	@Mock
	private AttachmentsBizComp attachBizComp;

	@Mock
    private PermitPeriodBizComp permitPeriodBizComp;

    private MockMvc mockMvc;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));


    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
    }

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permitperiod.PermitPeriodRestController#createRptPeriod(java.lang.String)}.
	 */
	@Test
	public void testCreateRptPeriod() throws Exception {
		String mockrptPeriod = "08-2017";

        when(
                this.permitPeriodBizComp.createRptPeriod(anyString())
              ).thenReturn(mockrptPeriod);

        MvcResult result =  this.mockMvc
	        .perform(put("/api/v1/companies/period/create/{monthYear}","08-2017")
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, mockrptPeriod);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permitperiod.PermitPeriodRestController#read(long, long)}.
	 */
	@Test
	public void testRead() throws Exception {
	  PermitPeriodEntity mockPermitPeriodEntity = this.createPermitPeriodEntity();
      String permitperiodJson = json(mockPermitPeriodEntity);

      when(
              this.permitPeriodBizComp.getPermitPeriodByIds(
                anyLong(), anyLong()
              )
            ).thenReturn(mockPermitPeriodEntity);

      MvcResult result = this.mockMvc
              .perform(get("/api/v1/companies/period/{id}/{permitId}",1,1))
              .andExpect(status().isOk())
              .andExpect(content()
                      .contentTypeCompatibleWith(this.contentType))
              .andDo(MockMvcResultHandlers.print()).andReturn();

      String returnString = result.getResponse().getContentAsString();
      assertEquals(returnString, permitperiodJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permitperiod.PermitPeriodRestController#saveOrUpdate(gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity)}.
	 */
	@Test
	public void testSaveOrUpdate() throws Exception {
		PermitPeriodEntity mockPermitPeriodEntity = this.createPermitPeriodEntity();
		String permitperiodJson = json(mockPermitPeriodEntity);

        when(
                this.permitPeriodBizComp.updatePermitPeriod(
                		(PermitPeriodEntity)notNull(), eq((List<AttachmentsEntity>) null))
              ).thenReturn(mockPermitPeriodEntity);

        MvcResult result =  this.mockMvc
	        .perform(put("/api/v1/companies/period")
	        		.contentType(this.contentType)
	        		.content(permitperiodJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, permitperiodJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permitperiod.PermitPeriodRestController#deleteReport(long, long)}.
	 */
	@Test
	public void testDeleteReport() throws Exception {
		PermitPeriodEntity mockPermitPeriodEntity = this.createPermitPeriodEntity();
		String permitperiodJson = json(mockPermitPeriodEntity);

        when(
                this.permitPeriodBizComp.deleteByIds(
                		anyLong(), anyLong())
              ).thenReturn(mockPermitPeriodEntity);

		 MvcResult result = mockMvc.perform(
               delete("/api/v1/companies/period/{periodId}/{permitId}/delete", 1, 1)
               .contentType(this.contentType)
               )
               .andExpect(status().isOk())
               .andDo(MockMvcResultHandlers.print()).andReturn();

       //verify(assessmentBizComp, times(1)).deleteByDocId(1);
       //verifyNoMoreInteractions(assessmentBizComp);

       String returnString = result.getResponse().getContentAsString();
       assertEquals(returnString, permitperiodJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permitperiod.PermitPeriodRestController#updateReconReport(long, long)}.
	 */
	@Test
	public void testUpdateReconReport() throws Exception {
		PeriodStatus mockPeriodStatus = this.createPeriodStatus();
		String periodStatusJson = json(mockPeriodStatus);

        when(
                this.permitPeriodBizComp.updatePeriodReconStatus(
                		anyLong(), anyLong())
              ).thenReturn(mockPeriodStatus);

        MvcResult result =  this.mockMvc
	        .perform(put("/api/v1/companies/period/{periodId}/permit/{permitId}", 1, 1)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, periodStatusJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permitperiod.PermitPeriodRestController#getReconReportStatus(long, long)}.
	 */
	@Test
	public void testGetReconReportStatus() throws Exception {
		PeriodStatus mockPeriodStatus = this.createPeriodStatus();
		String periodStatusJson = json(mockPeriodStatus);

	      when(
	              this.permitPeriodBizComp.getPeriodReconStatus(
	                anyLong(), anyLong()
	              )
	            ).thenReturn(mockPeriodStatus);

	      MvcResult result = this.mockMvc
	              .perform(get("/api/v1/companies/period/{periodId}/permit/{permitId}",1,1))
	              .andExpect(status().isOk())
	              .andExpect(content()
	                      .contentTypeCompatibleWith(this.contentType))
	              .andDo(MockMvcResultHandlers.print()).andReturn();

	      String returnString = result.getResponse().getContentAsString();
	      assertEquals(returnString, periodStatusJson);
	}



	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permitperiod.PermitPeriodRestController#getMissingForms(long, long)}.
	 */
	@Test
	public void testGetMissingForms() throws Exception {
		List<MissingForms> mockMissingForms = this.createMissingForm();
		String missingFormsJson = json(mockMissingForms);

	      when(
	              this.permitPeriodBizComp.getMissingForms(
	                anyLong(), anyLong()
	              )
	            ).thenReturn(mockMissingForms);

	      MvcResult result = this.mockMvc
	              .perform(get("/api/v1/companies/period/{periodId}/{permitId}/forms",1,1))
	              .andExpect(status().isOk())
	              .andExpect(content()
	                      .contentTypeCompatibleWith(this.contentType))
	              .andDo(MockMvcResultHandlers.print()).andReturn();

	      String returnString = result.getResponse().getContentAsString();
	      assertEquals(returnString, missingFormsJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.permitperiod.PermitPeriodRestController#saveMissingForms(java.util.List, long, long)}.
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testSaveMissingForms() throws Exception {
		List<MissingForms> mockMissingForms = this.createMissingForm();
        String missingFormsJson = json(mockMissingForms);

        doNothing().when(this.permitPeriodBizComp).saveMissingForms(
            		(List<MissingForms>)notNull(), anyLong(), anyLong()
                );

        MvcResult result = this.mockMvc
                .perform(post("/api/v1/companies/period/{periodId}/{permitId}/forms", 1, 1)
                .contentType(this.contentType)
                .content(missingFormsJson))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print()).andReturn();

        // verify(companyMgmtBizComp, times(1)).createCompany((Company)notNull());
        // verifyNoMoreInteractions(companyMgmtBizComp);

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, "Successfully saved the missing forms");
	}

    /*************************************************************
     * MOCK OBJECTS
     *************************************************************/
	private PeriodStatus createPeriodStatus() {
		PeriodStatus ps = new PeriodStatus();
		ps.setPeriodId(1L);
		ps.setPeriodStatusTypeCd("TEST");
		ps.setPermitId(1L);
		ps.setReconStatusTypeCd("TEST");
		ps.setStatusTypeTypeCd("U");
		return ps;
	}

	private PermitPeriodEntity createPermitPeriodEntity() {
		PermitPeriodEntity permitPeriodEntity = new PermitPeriodEntity();
		permitPeriodEntity.setPeriodId(1L);
		permitPeriodEntity.setSourceCd("EMAI");
		permitPeriodEntity.setZeroFlag("N");
		permitPeriodEntity.setCreatedBy("TUFAADMIN");
		permitPeriodEntity.setCreatedDt(new Date());
		permitPeriodEntity.setModifiedBy("TUFAADMIN");
		permitPeriodEntity.setModifiedDt(new Date());
        return permitPeriodEntity;
	}

	private List<MissingForms> createMissingForm() {
		MissingForms mform = new MissingForms();
		mform.setPeriodId(1L);
		mform.setPermitId(1L);
		mform.setFormTypeCd("3852");
		mform.setDefectStatusCd("true");
		List<MissingForms> forms = new ArrayList<MissingForms>();
		forms.add(mform);
		return forms;
	}

    /************************************************************
     *  JSONIZER
     ************************************************************/
    @SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }


}

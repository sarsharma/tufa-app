package gov.hhs.fda.ctp.web.quarter;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp;
import gov.hhs.fda.ctp.biz.trueup.TrueUpBizComp;
import gov.hhs.fda.ctp.common.beans.Assessment;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.AssessmentFilter;
import gov.hhs.fda.ctp.common.beans.AssessmentReportStatus;
import gov.hhs.fda.ctp.common.beans.CBPAmendment;
import gov.hhs.fda.ctp.common.beans.QtReconRptDetail;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.persistence.model.AssessmentEntity;
import gov.hhs.fda.ctp.persistence.model.CBPAmendmentEntity;
import gov.hhs.fda.ctp.persistence.model.HTSCodeEntity;
import gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;
import gov.hhs.fda.ctp.web.trueup.TrueUpRestController;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

@ActiveProfiles({"persistenceingestmaptest"})
@Transactional
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class}),
	@ContextConfiguration(classes = {PersistenceHibernateTestConfig.class})
})
public class AssessmentBusinessIT {
	@Autowired
	AssessmentBizComp bizComp;
	
	@Autowired
	AssessmentRestController controller;

	@Mock
	private AssessmentBizComp mockBizComp;
	
	private MockMvc mockMvc;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;
    
	private ObjectMapper mapper = new ObjectMapper();


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));


    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    
    @After
    public void teardown() {
    		
    }
    
    @Test
    public void testGetAssessments() throws Exception {
    	AssessmentFilter af = new AssessmentFilter();
    	List<AssessmentEntity> res = this.bizComp.getAssessments(af);
		Assert.assertNotNull (res);
    	assertTrue(res.size()>0);		
    }
    
    @Test
    public void testGetAssessment() throws Exception {
    	AssessmentEntity res = this.bizComp.getAssessment(1L);
    	Assert.assertNotNull (res);
    }

    @Test
    public void testGetFiscalYears() throws Exception {
    	Set<String> res = this.bizComp.getFiscalYears();
    	assertTrue(res.size()>0);
    }
    
    @Test
    public void testGetQuarterlyAssessment() throws Exception {
    	Assessment res = this.bizComp.getQuarterlyAssessment(1,5000);
    	Assert.assertNotNull (res);
    }
    
    @Test
    public void testGetUnapprovedReconRecordCount() throws Exception {
    	String res = this.bizComp.getUnapprovedReconRecordCount(5000L,1L);
    	Assert.assertNotNull (res);
    }
    
    @Test
    public void testGetReconRptData() throws Exception {
    	List<QtReconRptDetail> res = this.bizComp.getReconRptData(5000L,1L);
    	assertTrue(res.size()>0);
    }
    
    @Test
    public void testGetReconRptDataForFiscalYear() throws Exception {
    	List<QtReconRptDetail> res = this.bizComp.getReconRptDataForFiscalYear(5000L);
    	assertTrue(res.size()>0);
    }
    
    @Test
    public void testGetReportStatusData() throws Exception {
    	Map<Long, AssessmentReportStatus> res = this.bizComp.getReportStatusData(5000,1);
    	assertTrue(res.size()>0);
    }
    
    @Test
    public void testGetReconExport() throws Exception {
    	AssessmentExport res = this.bizComp.getReconExport(5000,1);
    	Assert.assertNotNull (res);
    }
    
    @Test
    public void testSaveSupportingDocument() throws Exception {
    	SupportingDocumentEntity doc = new SupportingDocumentEntity();
    	doc.setVersionNum(1);doc.setAssessmentId(101);doc.setDocumentNumber(2);
    	String res = null;//(String) this.controller.saveDoc(doc, 101).getBody();
    	assertThat(res, is("Document Saved"));
    }
    
    @Test
    public void testCigarAssessment() throws Exception {
    	Assessment assessment = this.controller.submitCigarAssessment(0, 2018, 0, "CIGR").getBody();
    	assertThat(assessment.getAssessmentQtr(), is(0));
    }
    
    /*
    @Test
    public void testAddAssessment() throws Exception {
    	AssessmentEntity ae = new AssessmentEntity();
    	ae.setAssessmentQtr(1);
    	ae.setAssessmentYr(5000);
    	ae.setSubmittedInd("Y");
    	ae.setAssessmentId(1L);
    	ae.setCigarSubmittedInd("N");
		when(this.mockBizComp.addAssessment((AssessmentEntity)notNull())).thenReturn("1");

		String returnString = mockBizComp.addAssessment(ae);
    	Assert.assertNotNull (returnString);
    	assertTrue (returnString.equals("1"));
    }
    
    @Test
    public void testGenerateQuarterlyMarketShare() throws Exception {
    	Assessment ae = new Assessment();
    	ae.setAssessmentQtr(1);
    	ae.setAssessmentYr(5000);
    	ae.setSubmittedInd("Y");
    	ae.setAssessmentId(1L);
    	ae.setCigarSubmittedInd("N");
		String unapprovedReconString = null;

        when(
                this.mockBizComp.getUnapprovedReconRecordCount(
                		anyInt(), anyInt())
              ).thenReturn(unapprovedReconString);

        doNothing().when(this.mockBizComp).generateQuarterlyMarketShare(
                		anyInt(), anyInt(), anyString());

        when(
                this.mockBizComp.getQuarterlyAssessment(
                		anyInt(), anyInt())
              ).thenReturn(ae);
    	doNothing().when(
                this.mockBizComp).generateQuarterlyMarketShare(
                		(Integer)notNull(), (Integer)notNull(), (String)notNull());
    }
    */
     
}

/**
 *
 */
package gov.hhs.fda.ctp.web.quarter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.ss.formula.functions.T;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import gov.hhs.fda.ctp.biz.permitperiod.PermitPeriodBizComp;
import gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.QtReconRptDetail;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

/**
 * @author rabalasubramani3
 *
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {PersistenceHibernateTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class})
})
public class ReconciliationReportRestControllerIT {

	@Autowired
	ReconciliationReportRestController controller;

	@Autowired
	AssessmentBizComp bizComp;

	@Mock
    private PermitPeriodBizComp permitPeriodBizComp;
	/**
	 * @throws java.lang.Exception
	 */
	
	private MockMvc mockMvc;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));


    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.ReconciliationReportRestController#getReconExport(int, int)}.
	 */
	@Test
    public void testGetReconExport() throws Exception {
    	AssessmentExport res = this.bizComp.getReconExport(5000,1);
    	ResponseEntity<AssessmentExport> response = (ResponseEntity<AssessmentExport>) this.controller.getReconExport(5000, 1);
    	Assert.assertNotNull (res);
    	Assert.assertNotNull (response);
    }
    
    @Test
    public void testGetReconExportForFiscalYear() throws Exception {
    	AssessmentExport res = this.bizComp.getReconExportForFiscalYear(5000);
    	ResponseEntity<AssessmentExport> response = (ResponseEntity<AssessmentExport>) this.controller.getReconExportForFiscalYear(5000);
    	Assert.assertNotNull (res);
    	Assert.assertNotNull (response);
    }
	
	@SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}

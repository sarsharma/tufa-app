/**
 *
 */
package gov.hhs.fda.ctp.web.quarter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import gov.hhs.fda.ctp.biz.permitperiod.PermitPeriodBizComp;
import gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp;
import gov.hhs.fda.ctp.common.beans.Assessment;
import gov.hhs.fda.ctp.common.beans.AssessmentFilter;
import gov.hhs.fda.ctp.common.beans.AssessmentReportStatus;
import gov.hhs.fda.ctp.common.beans.QtReconRptDetail;
import gov.hhs.fda.ctp.common.beans.ReportStatus;
import gov.hhs.fda.ctp.common.beans.SupportingDocument;
import gov.hhs.fda.ctp.persistence.model.AssessmentEntity;
import gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

/**
 * @author rabalasubramani3
 *
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {MvcConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class})
})
public class TestAssessmentRestController {

	@InjectMocks
	AssessmentRestController controller;

	@Mock
	private AssessmentBizComp assessmentBizComp;

	@Mock
    private PermitPeriodBizComp permitPeriodBizComp;

    private MockMvc mockMvc;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));


    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
    }


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#createAssessment(gov.hhs.fda.ctp.persistence.model.AssessmentEntity)}.
	 */
	@Test
	public void testCreateAssessment() throws Exception {
		AssessmentEntity mockAssessmentEntity = this.createMockAssessmentEntity();
		String assessmentEntityJson = json(mockAssessmentEntity);
		String retAddAssessment = "1";

        when(
                this.assessmentBizComp.addAssessment(
                		(AssessmentEntity)notNull())
              ).thenReturn(retAddAssessment);

        MvcResult result =  this.mockMvc
	        .perform(put("/api/reports/quarter/create")
	        		.contentType(this.contentType)
	        		.content(assessmentEntityJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, retAddAssessment);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#createCigarAssessment(gov.hhs.fda.ctp.persistence.model.AssessmentEntity)}.
	 */
	@Test
	public void testCreateCigarAssessment() throws Exception {
		AssessmentEntity mockAssessmentEntity = this.createMockAssessmentEntity();
		String assessmentEntityJson = json(mockAssessmentEntity);
		String retAddAssessment = "1";

        when(
                this.assessmentBizComp.addAssessment(
                		(AssessmentEntity)notNull())
              ).thenReturn(retAddAssessment);

        MvcResult result =  this.mockMvc
	        .perform(post("/api/reports/quarter/cigar")
	        		.contentType(this.contentType)
	        		.content(assessmentEntityJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, retAddAssessment);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#getAssessment(long)}.
	 */
	@Test
	public void testGetAssessment() throws Exception {
		AssessmentEntity mockAssessmentEntity = this.createMockAssessmentEntity();
		String assessmentEntityJson = json(mockAssessmentEntity);

	      when(
	              this.assessmentBizComp.getAssessment(
	                anyLong()
	              )
	            ).thenReturn(mockAssessmentEntity);

	      MvcResult result = this.mockMvc
	              .perform(get("/api/reports/quarter/{id}",1))
	              .andExpect(status().isOk())
	              .andExpect(content()
	                      .contentTypeCompatibleWith(this.contentType))
	              .andDo(MockMvcResultHandlers.print()).andReturn();

	      String returnString = result.getResponse().getContentAsString();
	      assertEquals(returnString, assessmentEntityJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#getAssessments(gov.hhs.fda.ctp.common.beans.AssessmentFilter)}.
	 */
	@Test
	public void testGetAssessments() throws Exception {
		AssessmentFilter mockAssessmentFilter = this.createMockAssessmentFilter();
		List<AssessmentEntity> mockassessmentEntities = this.createMockAssessmentEntities();
		String assessmentFilterJson = json(mockAssessmentFilter);

        when(
                this.assessmentBizComp.getAssessments(
                		(AssessmentFilter)notNull())
              ).thenReturn(mockassessmentEntities);

        this.mockMvc
	        .perform(put("/api/reports/quarter/assessments")
	        		.contentType(this.contentType)
	        		.content(assessmentFilterJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

        // String returnString = result.getResponse().getContentAsString();
        // assertEquals(returnString, assessmentFilterJson);
	}



	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#getAvailableFiscalYears()}.
	 */
	@Test
	public void testGetAvailableFiscalYears() throws Exception {
		Set<String> mockyears = new HashSet<>();
		mockyears.add("2016");
		mockyears.add("2017");
        String yearsJson = json(mockyears);

	      when(
	              this.assessmentBizComp.getFiscalYears(
	              )
	            ).thenReturn(mockyears);

        MvcResult result =  this.mockMvc
                .perform(get("/api/reports/quarter/fiscalyears"))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, yearsJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#getAvailablePermitReportFiscalYears(long, long)}.
	 */
	@Test
	public void testGetAvailablePermitReportFiscalYears() throws Exception {
		List<QtReconRptDetail> mockreconRptDetails = this.createMockReconDetails();
		String rptDetailsJson = json(mockreconRptDetails);
	      when(
	              this.assessmentBizComp.getReconRptData(
	                anyLong(), anyLong()
	              )
	            ).thenReturn(mockreconRptDetails);

        MvcResult result =  this.mockMvc
                .perform(get("/api/reports/quarter/recon/fiscalyear/{yrId}/quarter/{qtrId}", 2016, 1))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, rptDetailsJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#getAvailablePermitReportFiscalYear(long)}.
	 */
	@Test
	public void testGetAvailablePermitReportFiscalYear() throws Exception {
		List<QtReconRptDetail> mockreconRptDetails = this.createMockReconDetails();
		String rptDetailsJson = json(mockreconRptDetails);
	      when(
	              this.assessmentBizComp.getReconRptDataForFiscalYear(
	                anyLong()
	              )
	            ).thenReturn(mockreconRptDetails);

        MvcResult result =  this.mockMvc
                .perform(get("/api/reports/quarter/recon/fiscalyear/{yrId}", 2016))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, rptDetailsJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#getAssessmentsReportStatus(int, int)}.
	 */
	@Test
	public void testGetAssessmentsReportStatus() throws Exception {
		ReportStatus mockReportStatus = this.createMockReportStatus();
		String rptStatusJson = json(mockReportStatus);
		Map<Long, AssessmentReportStatus> resultSet = new HashMap<Long, AssessmentReportStatus>();


	      when(
	              this.assessmentBizComp.getReportStatusData(
	                anyInt(), anyInt()
	              )
	            ).thenReturn(resultSet);

        MvcResult result =  this.mockMvc
                .perform(get("/api/reports/quarter/reportstatus/{quarter}/{year}", 1, 2016))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        //String returnString = result.getResponse().getContentAsString();
        //assertEquals(returnString, rptStatusJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#getQuarterlyAssessment(int, int)}.
	 */
	@Test
	public void testGetQuarterlyAssessment() throws Exception {
		Assessment mockAssessment = this.createMockAssessment();
		String unapprovedReconString = "Action Required";
		mockAssessment.setApprovalMessage(unapprovedReconString);
		//String assmtJson = json(mockAssessment);

	      when(
	              this.assessmentBizComp.getUnapprovedReconRecordCount(
	                anyInt(), anyInt()
	              )
	            ).thenReturn(unapprovedReconString);

	      when(
	              this.assessmentBizComp.getQuarterlyAssessment(
	                anyInt(), anyInt()
	              )
	            ).thenReturn(mockAssessment);

        this.mockMvc
                .perform(get("/api/reports/quarter/export/{qtr}/{fiscalYear}", 1, 2016))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

//        String returnString = result.getResponse().getContentAsString();
//        assertEquals(returnString, assmtJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#generateMarketShare(int, int)}.
	 */
	@Test
	public void testQuarterlyGenerateMarketShare() throws Exception {
		Assessment mockAssessment = this.createMockAssessment();
		 String assessmentJson = json(mockAssessment);
		String unapprovedReconString = null;

        when(
                this.assessmentBizComp.getUnapprovedReconRecordCount(
                		anyInt(), anyInt())
              ).thenReturn(unapprovedReconString);

        doNothing().when(this.assessmentBizComp).generateQuarterlyMarketShare(
                		anyInt(), anyInt(), anyString());

        when(
                this.assessmentBizComp.getQuarterlyAssessment(
                		anyInt(), anyInt())
              ).thenReturn(mockAssessment);

        MvcResult result =  this.mockMvc
	        .perform(put("/api/reports/quarter/export/{qtr}/{fiscalYear}", 1, 2016)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

//        String returnString = result.getResponse().getContentAsString();
//        assertEquals(returnString, assessmentJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#generateMarketShare(int, int)}.
	 */
	@Test
	public void testCigarGenerateMarketShare() throws Exception {
		Assessment mockAssessment = this.createMockAssessment();
		 String assessmentJson = json(mockAssessment);
		String unapprovedReconString = null;

        when(
                this.assessmentBizComp.getUnapprovedReconRecordCount(
                		anyInt(), anyInt())
              ).thenReturn(unapprovedReconString);

        doNothing().when(this.assessmentBizComp).generateCigarMarketShare(
                		anyInt(), anyInt(), anyString(), anyInt(), anyString());

        when(
                this.assessmentBizComp.getCigarAssessment(
                		anyInt(), anyInt())
              ).thenReturn(mockAssessment);

        MvcResult result =  this.mockMvc
	        .perform(put("/api/reports/quarter/export/{qtr}/{fiscalYear}/{assessmentQtr}/{type}", 1, 2016,2, "FULLMS")
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

//        String returnString = result.getResponse().getContentAsString();
//        assertEquals(returnString, assessmentJson);
	}
	
	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#submitQuarterlyAssessment(int, int)}.
	 */
	@Test
	public void testSubmitQuarterlyAssessment() throws Exception {
		Assessment mockAssessment = this.createMockAssessment();
		// String assessmentJson = json(mockAssessment);
		String unapprovedReconString = null;

        when(
                this.assessmentBizComp.getUnapprovedReconRecordCount(
                		anyLong(), anyLong())
              ).thenReturn(unapprovedReconString);

        doNothing().when(
                this.assessmentBizComp).generateQuarterlyMarketShare(
                		anyInt(), anyInt(), anyString());

        when(
                this.assessmentBizComp.getQuarterlyAssessment(
                		anyInt(), anyInt())
              ).thenReturn(mockAssessment);

        this.mockMvc
	        .perform(post("/api/reports/quarter/export/{qtr}/{fiscalYear}", 1, 2016)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

//        String returnString = result.getResponse().getContentAsString();
//        assertEquals(returnString, assessmentJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#saveDoc(gov.hhs.fda.ctp.persistence.model.SupportingDocumentEntity, long)}.
	 */
	@Test
	public void testSaveDoc() throws Exception {
		SupportingDocumentEntity mockSupportingDoc = this.createMockSupportingDocumentEntity();
		String supportDocJson = json(mockSupportingDoc);

        doNothing().when(
                this.assessmentBizComp).saveSupportDocument(
                		(SupportingDocumentEntity)notNull());
                     
        MvcResult result =  this.mockMvc
	        .perform(put("/api/reports/quarter/docs/{assessId}", 1)
	        		.contentType(this.contentType)
	        		.content(supportDocJson)
	        		)
	        .andExpect(status().isOk())
	        .andExpect(content().contentTypeCompatibleWith(this.contentType))
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, "Document Saved");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#getSupportingDocs(long)}.
	 */
	@Test
	public void testGetSupportingDocs() throws Exception {
		List<SupportingDocument> mockSupportingDocuments = this.createMockSupportingDocuments();
		String supportDocJson = json(mockSupportingDocuments);

	      when(
	              this.assessmentBizComp.getSupportDocs(
	                anyLong()
	              )
	            ).thenReturn(mockSupportingDocuments);

	      MvcResult result =  this.mockMvc
                .perform(get("/api/reports/quarter/{assessId}/docs", 1))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, supportDocJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#deleteDocument(long)}.
	 */
	@Test
	public void testDeleteDocument() throws Exception {
        doNothing().when(
                this.assessmentBizComp).deleteByDocId(
                		anyLong());

		 MvcResult result = mockMvc.perform(
               delete("/api/reports/quarter/docs/{asmtdocId}", 1)
               .contentType(this.contentType)
               )
               .andExpect(status().isOk())
               .andDo(MockMvcResultHandlers.print()).andReturn();

       //verify(assessmentBizComp, times(1)).deleteByDocId(1);
       //verifyNoMoreInteractions(assessmentBizComp);

       String returnString = result.getResponse().getContentAsString();
       assertEquals(returnString, "Removed document");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.quarter.AssessmentRestController#getDocument(long)}.
	 */
	@Test
	public void testGetDocument() throws Exception {
		SupportingDocumentEntity mockSupportingDoc = this.createMockSupportingDocumentEntity();
		String supportDocJson = json(mockSupportingDoc);

	      when(
	              this.assessmentBizComp.getDocByDocId(
	                anyLong()
	              )
	            ).thenReturn(mockSupportingDoc);

	      MvcResult result =  this.mockMvc
                .perform(get("/api/reports/quarter/docs/{asmtdocId}", 1))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(this.contentType))
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, supportDocJson);
	}


	//	@Test
//	public void testCreatenGet() {
//		ResponseEntity<String> ae = controller.createAssessment(mockAssessment());
//		ResponseEntity<AssessmentEntity> assess = controller.getAssessment(testAssessment.getAssessmentId());
//		assert (ae != null && assess != null);
//	}
//
    /*************************************************************
     * MOCK OBJECTS
     *************************************************************/

	private SupportingDocument createMockSupportingDocument() {
		SupportingDocument sd = new SupportingDocument();
		sd.setAsmntDocId(1L);
		sd.setAssessmentId(1L);
		sd.setAuthor("test author");
		sd.setDescription("test description");
		sd.setDocumentNumber(1L);
		return sd;
	}

	private SupportingDocumentEntity createMockSupportingDocumentEntity() {
		SupportingDocumentEntity sd = new SupportingDocumentEntity();
		
		List<AssessmentEntity> results = new ArrayList<>();
		AssessmentEntity assmt = new AssessmentEntity();
		
		assmt.setAssessmentId(1L);
		assmt.setAssessmentQtr(1);
		assmt.setAssessmentType("TEST");
		assmt.setAssessmentYr(2016);
		assmt.setSubmittedInd("Not Submitted");
		
		sd.setAsmntDocId(1L);
		sd.setAssessmentId(1L);
		sd.setAuthor("test author");
		sd.setDescription("test description");
		sd.setDocumentNumber(1L);
		sd.setVersionNum(1);
		sd.setAssessment(assmt);
		return sd;
	}
	
	
	
	
	private List<SupportingDocument> createMockSupportingDocuments() {
		List<SupportingDocument> docs = new ArrayList<>();
		SupportingDocument sd = this.createMockSupportingDocument();
		docs.add(sd);
		return docs;
	}

	private ReportStatus createMockReportStatus() {
		ReportStatus rptStatus = new ReportStatus();
		List<AssessmentReportStatus> results = new ArrayList<>();
		AssessmentReportStatus ars = new AssessmentReportStatus();
		ars.setCompanyId(1L);
		ars.setFiscalYr(2016);
		ars.setLegalNm("Test Name");
		ars.setPermitId(1L);
		ars.setPermitNum("TTBB123");
		ars.setPermitStatusTypeCd("TEST");
		ars.setPermitTypeCd("TEST");
		ars.setYear(2016);
		results.add(ars);
		rptStatus.setResult(results);
		return rptStatus;
	}

	private List<QtReconRptDetail> createMockReconDetails() {
		List<QtReconRptDetail> details = new ArrayList<>();
		QtReconRptDetail rpt = new QtReconRptDetail();
		rpt.setCompanyId(1L);
		rpt.setCompanyName("Test Company");
		rpt.setFiscalYear("2017");
		rpt.setMonth("June");
		rpt.setPeriodId(1L);
		rpt.setPermitId(1L);
		rpt.setPermitNum("TTBB123");
		rpt.setPermitTypeCd("TEST");
		rpt.setReconStatus("Not Approved");
		rpt.setReportedTobacco("Cigars");
		rpt.setReportedTobacco("Cigars");
		rpt.setReportStatus("Not Submitted");
		details.add(rpt);

		return details;
	}

	private AssessmentFilter createMockAssessmentFilter() {
		AssessmentFilter af = new AssessmentFilter();
		Set<Integer> fiscalYrFilters = new HashSet<>();
		fiscalYrFilters.add(2016);
		af.setFiscalYrFilters(fiscalYrFilters);
		Set<Integer> quarterFilters = new HashSet<>();
		quarterFilters.add(1);
		quarterFilters.add(2);
		af.setQuarterFilters(quarterFilters);
		List<Assessment> results = new ArrayList<>();
		Assessment assmt = new Assessment();
		assmt.setApprovalMessage("test");
		assmt.setAssessmentId(1L);
		assmt.setAssessmentQtr(1);
		assmt.setAssessmentType("TEST");
		assmt.setAssessmentYr(2016);
		assmt.setDisplayQtr("1");
		results.add(assmt);
		af.setResults(results);
		Set<String> statusFilters = new HashSet<>();
		statusFilters.add("Submitted");
		af.setStatusFilters(statusFilters);
		return af;
	}

	private List<AssessmentEntity> createMockAssessmentEntities() {
		List<AssessmentEntity> results = new ArrayList<>();
		AssessmentEntity assmt = new AssessmentEntity();
		assmt.setAssessmentId(1L);
		assmt.setAssessmentQtr(1);
		assmt.setAssessmentType("TEST");
		assmt.setAssessmentYr(2016);
		assmt.setSubmittedInd("Not Submitted");
		results.add(assmt);
		return results;
	}

	private AssessmentEntity createMockAssessmentEntity() {
		AssessmentEntity ae = new AssessmentEntity();
		ae.setAssessmentQtr(1);
		ae.setAssessmentYr(2014);
		ae.setSubmittedInd("N");
		return ae;
	}

	private Assessment createMockAssessment() {
		Assessment ae = new Assessment();
		ae.setAssessmentQtr(1);
		ae.setAssessmentYr(2014);
		ae.setSubmittedInd("N");
		List<String> quarterMonths = new ArrayList<>();
		quarterMonths.add("October");
		quarterMonths.add("November");
		quarterMonths.add("December");
		ae.setQuarterMonths(quarterMonths);
		return ae;
	}

//
//	@Test
//	public void testGetAssessments() {
//		ResponseEntity<AssessmentFilter> assessments;
//		AssessmentFilter filter = new AssessmentFilter();
//
//		Set<Integer> quarters = new HashSet<>();
//		quarters.add(1);
//		quarters.add(2);
//		filter.setQuarterFilters(quarters);
//
//		Set<Integer> years = new HashSet<>();
//		years.add(1801);
//		years.add(2014);
//		filter.setFiscalYrFilters(years);
//
//		Set<String> status = new HashSet<>();
//		status.add("N");
//		status.add("Y");
//		filter.setStatusFilters(status);
//
//		assessments = controller.getAssessments(filter);
//		assert (assessments != null);
//	}
//
//	@Test
//	public void testGetFiscalYears() {
//		ResponseEntity<?> years;
//		years = controller.getAvailableFiscalYears();
//		assert (years != null);
//	}
	   /************************************************************
     *  JSONIZER
     ************************************************************/
    @SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}

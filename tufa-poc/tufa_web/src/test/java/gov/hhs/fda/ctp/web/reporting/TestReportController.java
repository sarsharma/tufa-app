/**
 *
 */
package gov.hhs.fda.ctp.web.reporting;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

/**
 * @author rabalasubramani3
 *
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class})
})
public class TestReportController {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.reporting.ReportController#read()}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testRead() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.reporting.ReportController#getAttachments(long, long)}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testGetAttachments() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.reporting.ReportController#addAttachments(gov.hhs.fda.ctp.persistence.model.AttachmentsEntity, long, long)}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testAddAttachments() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.reporting.ReportController#deleteAttachment(long)}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testDeleteAttachment() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.reporting.ReportController#viewAttachment(long, javax.servlet.http.HttpServletResponse, javax.servlet.http.HttpServletRequest)}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testViewAttachment() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.reporting.ReportController#downloadAttachment(long)}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testDownloadAttachment() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

}

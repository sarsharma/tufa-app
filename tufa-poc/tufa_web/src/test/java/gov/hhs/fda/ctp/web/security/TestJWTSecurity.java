package gov.hhs.fda.ctp.web.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.core.AuthenticationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import gov.hhs.fda.ctp.common.beans.JWTToken;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpDocument;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;
import gov.hhs.fda.ctp.web.config.security.jwt.security.model.token.RefreshToken;
import gov.hhs.fda.ctp.web.trueup.TrueUpRestController;
import javax.servlet.Filter;

//@ActiveProfiles({ "persistencetest" })
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({ @ContextConfiguration(classes = { WebTestConfig.class }),
		@ContextConfiguration(classes = { MvcConfig.class }) })
public class TestJWTSecurity {

	@Autowired
	TrueUpRestController controller;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private Filter springSecurityFilterChain;

	@Autowired
	private Filter paginationFilter;

	private MockMvc mockMvc;
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	private ObjectMapper mapper = new ObjectMapper();

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		List<HttpMessageConverter<?>> converterList = Arrays.asList(converters);

		for (HttpMessageConverter<?> converter : converterList) {
			if (converter instanceof MappingJackson2HttpMessageConverter)
				this.mappingJackson2HttpMessageConverter = converter;
		}

		Assert.assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setup() {

		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
				.addFilters(springSecurityFilterChain, paginationFilter).build();
	}

	@Test
	public void requestProtectedUrlWithUser() throws Exception {

		MvcResult result = this.mockMvc.perform(
				post("/api/auth/login").contentType(this.contentType).header("OAM_REMOTE_USER", "user@test.com"))
				.andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();

		String returnString = result.getResponse().getContentAsString();

		JWTToken token = mapper.readValue(returnString, JWTToken.class);

		/** 
		 * This can be any method call. The intent is just to verify that the access token 
		 * returned above can be successfully authenticated on an api call.
		 */
		this.mockMvc
				.perform(put("/api/v1/trueup/compare/results/{fiscalYear}", 2016).param("fiscalYear", "2016")
						.contentType(this.contentType).content(this.json(new TrueUpCompareSearchCriteria()))
						.header("X-Authorization", "Bearer " + token.getAccesstoken()))
				.andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();

	}

	@Test
	public void requestProtectedUrlWithInvalidJWTToken() throws Exception {

		MvcResult result = this.mockMvc.perform(
				post("/api/auth/login").contentType(this.contentType).header("OAM_REMOTE_USER", "user@test.com"))
				.andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();

		String returnString = result.getResponse().getContentAsString();

		JWTToken token = mapper.readValue(returnString, JWTToken.class);

		assertNotNull(token.getAccesstoken());
		
		this.mockMvc
				.perform(put("/api/v1/trueup/compare/results/{fiscalYear}", 2016).param("fiscalYear", "2016")
						.contentType(this.contentType).content(this.json(new TrueUpCompareSearchCriteria()))
						.header("X-Authorization", "Bearer "))
				.andExpect(status().is4xxClientError()).andDo(MockMvcResultHandlers.print()).andReturn();

	}

	@Test
	public void requestGetRefreshToken() throws Exception {

		MvcResult result = this.mockMvc.perform(
				post("/api/auth/login").contentType(this.contentType).header("OAM_REMOTE_USER", "user@test.com"))
				.andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();

		String returnString = result.getResponse().getContentAsString();

		assertNotNull(returnString);

		
		JWTToken token = mapper.readValue(returnString, JWTToken.class);
		
		assertNotNull(token.getAccesstoken());

		MvcResult refreshTokenResult = this.mockMvc
				.perform(get("/api/auth/token")
						.contentType(this.contentType)
						.header("X-Authorization", "Bearer "+token.getRefreshtoken()))
				.andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();

		String refreshTokenString = refreshTokenResult.getResponse().getContentAsString();
		
		assertNotNull(refreshTokenString);

	}

	/************************************************************
	 * JSONIZER
	 ************************************************************/
	@SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}

	public Filter getSpringSecurityFilterChain() {
		return springSecurityFilterChain;
	}

	public void setSpringSecurityFilterChain(Filter springSecurityFilterChain) {
		this.springSecurityFilterChain = springSecurityFilterChain;
	}

	public WebApplicationContext getContext() {
		return context;
	}

	public void setContext(WebApplicationContext context) {
		this.context = context;
	}

	public Filter getPaginationFilter() {
		return paginationFilter;
	}

	public void setPaginationFilter(Filter paginationFilter) {
		this.paginationFilter = paginationFilter;
	}

}

package gov.hhs.fda.ctp.web.tableau;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
//import org.powermock.modules.junit4.PowerMockRunner;
//import org.powermock.core.classloader.annotations.PowerMockIgnore;
//import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;
import gov.hhs.fda.ctp.web.tableaudashboard.TableauDashboardRestController;


////@ActiveProfiles({ "persistencetest" })
//@RunWith(SpringRunner.class)
//@WebAppConfiguration
//@ContextHierarchy({ @ContextConfiguration(classes = { WebTestConfig.class }),
//		@ContextConfiguration(classes = { MvcConfig.class }),
//		@ContextConfiguration(classes = { PersistenceHibernateTestConfig.class }) })

@ActiveProfiles("persistencetest")
//@RunWith(PowerMockRunner.class)
//@PowerMockRunnerDelegate(SpringRunner.class)
//@PowerMockIgnore({"javax.management.*"})
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = { PersistenceHibernateTestConfig.class }),
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class})
})
public class TestTableau {

	@Autowired
	TableauDashboardRestController controller;

	@Autowired
	private WebApplicationContext context;
	
	private MockMvc mockMvc;
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	private ObjectMapper mapper = new ObjectMapper();

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		List<HttpMessageConverter<?>> converterList = Arrays.asList(converters);

		for (HttpMessageConverter<?> converter : converterList) {
			if (converter instanceof MappingJackson2HttpMessageConverter)
				this.mappingJackson2HttpMessageConverter = converter;
		}

		Assert.assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setup() {

		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
				.build();
	}
	
	@Test
    public void getTableauReportTypes() throws Exception {
		
		MvcResult result = this.mockMvc
        .perform(get("/api/v1/dashboard/reporttypes")
        		.contentType(this.contentType)
        		)
        .andExpect(status().isOk())
        .andDo(MockMvcResultHandlers.print()).andReturn();

		String returnString = result.getResponse().getContentAsString();
		 Assert.assertNotNull(returnString);
		
    }

//	@Test
//    public void getTableauTicket() throws Exception {
//		
//		TableauDashboardRestController contrllr = Mockito.spy(this.controller);
//		
//		
//		MvcResult result = this.mockMvc
//        .perform(get("/api/v1/dashboard/reporttypes")
//        		.contentType(this.contentType)
//        		)
//        .andExpect(status().isOk())
//        .andDo(MockMvcResultHandlers.print()).andReturn();
//
//		String returnString = result.getResponse().getContentAsString();
//		 Assert.assertNotNull(returnString);
//    }
}

/**
 *
 */
package gov.hhs.fda.ctp.web.test;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import gov.hhs.fda.ctp.biz.addressmgmt.AddressMgmtBizComp;
import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.biz.permit.PermitBizComp;
import gov.hhs.fda.ctp.biz.permitperiod.PermitPeriodBizComp;
import gov.hhs.fda.ctp.biz.quarter.AssessmentBizComp;
import gov.hhs.fda.ctp.biz.registration.UserDTO;
import gov.hhs.fda.ctp.biz.reporting.AttachmentsBizComp;
import gov.hhs.fda.ctp.biz.test.TestBizComp;
import gov.hhs.fda.ctp.biz.useradmin.UserAdminBizComp;
import gov.hhs.fda.ctp.common.beans.Address;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.CompanyHistory;
import gov.hhs.fda.ctp.common.beans.CompanySearchCriteria;
import gov.hhs.fda.ctp.common.beans.Contact;
import gov.hhs.fda.ctp.common.beans.FormDetail;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.PermitHistoryCriteria;
import gov.hhs.fda.ctp.common.beans.PermitMfgReport;
import gov.hhs.fda.ctp.common.beans.PermitPeriod;
import gov.hhs.fda.ctp.common.beans.SubmittedForm;
import gov.hhs.fda.ctp.common.util.CTPUtil;
import gov.hhs.fda.ctp.persistence.model.AttachmentsEntity;
import gov.hhs.fda.ctp.persistence.model.CompanyEntity;
import gov.hhs.fda.ctp.persistence.model.CountryEntity;
import gov.hhs.fda.ctp.persistence.model.PermitPeriodEntity;
import gov.hhs.fda.ctp.persistence.model.RoleEntity;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.contactmgmt.ContactMgmtRestController;

/**
 * @author tgunter
 *
 */
@ActiveProfiles("testall")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({ @ContextConfiguration(classes = { MvcConfig.class }) })
public class TestAll {

    @Autowired
    private PermitBizComp permitBizComp;
    @Autowired
    private TestBizComp testBizComp;
    @Autowired
    private AddressMgmtBizComp addressMgmtBizComp;
    @Autowired
    private CompanyMgmtBizComp companyMgmtBizComp;
    @Autowired
    private PermitPeriodBizComp permitPeriodBizComp;
	@Autowired
	private ContactMgmtRestController contactController;
	@Autowired
	private AttachmentsBizComp attachBizComp;
	@Autowired
	private UserAdminBizComp userBizComp;
	@Autowired
	private AssessmentBizComp assessmentBizComp;

    private  CompanyEntity companyEntity;
    private  Company company;
    private  List<PermitPeriod> permitPeriods;
    private  Long permitId;
    private  Long periodId;

    @Before
    public  void setup() {
      // make sure previous run didn't leave any setup data
      testBizComp.tearDownByCompanyName("UNIT TEST COMPANY");
      testBizComp.tearDownByCompanyName("CHANGED NAME");
      // set up the data
      companyEntity = testBizComp.setup();
      company = testBizComp.convertToDTO(companyEntity);
      permitPeriods = testBizComp.getPermitPeriodList(company);
      PermitPeriod permitPeriod = permitPeriods.get(0);
      permitId = permitPeriod.getPermitId();
      periodId = permitPeriod.getPeriodId();
    }

    @After
    public  void tearDown() {
    	testBizComp.tearDown(companyEntity.getCompanyId());
    }

    /**
     * Create a new test method, then add it to the list of tests.
     * This will prevent setup and teardown from being called multiple times.
     * @throws IOException 
     * @throws SQLException 
     */
    @Test
    public void testAll() throws IOException, SQLException {
    	testUser();
    	testSaveSubmittedForms();
    	testPermitPeriod();
    	testAttachments();
    	testAddresses();
    	testContacts();
    	testCompany();
    	testPermit();
    	testAssessment();
    }

    /**
     * Test user admin methods.
     */
    private void testUser() {
    	// this data should always be staged
    	UserDTO user = userBizComp.getUser("user@test.com");
    	assert(user!=null);
    	user = userBizComp.getUser(user.getUserId());
    	assert(user!=null);
    	Set<RoleEntity> roles = userBizComp.getRoles();
    	assert(roles.size() > 0);
    	user.setModifiedDt(CTPUtil.parseDateToString(new Date()));
    	user = userBizComp.update(user);
    	assert(user != null);
    	Set<UserDTO> users = userBizComp.getUsers();
    	assert(users != null && users.size() > 0);
    }

    /**
     * Test submitted forms methods.
     */
    private void testSaveSubmittedForms() {
        assert(permitPeriods.size() == 1);
        PermitPeriod permitPeriod = permitPeriods.get(0);
        PermitMfgReport report = createMockPermitMfgReport(permitPeriod);
        PermitPeriod newPermitPeriod = permitBizComp.saveSubmittedForms(report);
        newPermitPeriod.getSubmittedForms();
        assert(true);
    }

    /**
     * Test permit period methods.
     */
	private void testPermitPeriod() {
	    PermitPeriodEntity permitPeriodEntity = permitPeriodBizComp.getPermitPeriodByIds(periodId, permitId);
	    assert(permitPeriodEntity != null);
	    // test add attachment
	    List<AttachmentsEntity> attachments = new ArrayList<AttachmentsEntity>();
	    AttachmentsEntity attachment = new AttachmentsEntity();
	    attachment.setDocDesc("NEW DOCUMENT");
	    attachment.setDocFileNm("myfile.pdf");
	    attachment.setDocStatusCd("INCP");
	    attachment.setPeriodId(periodId);
	    attachment.setPermitId(permitId);
	    attachment.setPermitPeriodEntity(permitPeriodEntity);
	    attachment.setReportPdf(Mockito.mock(Blob.class));
	    attachments.add(attachment);
	    permitPeriodEntity = permitPeriodBizComp.updatePermitPeriod(permitPeriodEntity, attachments);
	    assert(permitPeriodEntity != null);
	    // try to delete the permit period
//	    boolean deleted = true;
//	    try {
//	  	  permitPeriodBizComp.deleteByIds(periodId, permitId);
//	    }
//	    catch(Exception e) {
//	  	  deleted = false;
//	    }
//	    assert(deleted == true);
	    //create permit periods for a time that nobody will use
	    permitPeriodBizComp.createRptPeriod("12/1972");

	    // WE NEED A WAY TO GENERATE A FAKE USER!!!!!!!!!!!!
	    //PeriodStatus status = permitPeriodBizComp.updatePeriodReconStatus(periodId, permitId);
	    //status = permitPeriodBizComp.getPeriodReconStatus(periodId, permitId);
	    //assert(status != null);

	}

	/**
	 * Test attachment methods.
	 * @throws IOException 
	 * @throws SQLException 
	 */
	private void testAttachments() throws IOException, SQLException {
		MockMultipartFile file = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
		AttachmentsEntity attachmentsEntity = new AttachmentsEntity();
		attachmentsEntity.setPeriodId(periodId);
		attachmentsEntity.setPermitId(permitId);
		attachmentsEntity.setDocDesc("Test.pdf");
		attachmentsEntity.setDocStatusCd("INCP");
	    attachmentsEntity.setDocFileNm("attach.pdf");
	    attachmentsEntity.setDocStatusCd("INCP");
	    attachmentsEntity.setReportPdf(Mockito.mock(Blob.class));
		attachBizComp.addAttach(attachmentsEntity, file);
		List<AttachmentsEntity> listAttach = attachBizComp.findAll();
		assert(listAttach != null);
		/*List<AttachmentsEntity> docs = attachBizComp.findByIds(ae.getPeriodId(), ae.getPermitId());
		AttachmentsEntity attach = attachBizComp.getAttachment(ae.getDocumentId());
		attachBizComp.deleteById(ae.getDocumentId());
		assert(ae!=null && docs != null);*/
	}

	/**
	 * Test address methods.
	 */
      private void testAddresses() {
        Address address = company.getPrimaryAddress();
        address.setCity("CHANGED CITY");
        address.setCountryCd("UNITED STATES");
        address = addressMgmtBizComp.saveOrUpdate(address);
        assert("CHANGED CITY".equals(address.getCity()));

        Set<CountryEntity> countries = addressMgmtBizComp.getCountries();
        CountryEntity country = new CountryEntity();
        country.setCountryNm("UNITED STATES");
        assert(countries.contains(country));

        List<String> codes = addressMgmtBizComp.getCountryCd();
        assert(codes.contains("US"));

        List<String> dialCodes = addressMgmtBizComp.getCountryDialCd();
        assert(dialCodes.size() > 0);

        List<String> countryNames = addressMgmtBizComp.getCountryNm();
        assert(countryNames.contains("UNITED STATES"));
    }

    /**
     * Test contacts methods.
     */
    private void testContacts() {
    	// test create new contact
    	Contact newContact = new Contact();
    	newContact.setCompany(company);
    	newContact.setCompanyId(company.getCompanyId());
    	newContact.setFirstNm("UNIT");
    	newContact.setLastNm("TEST");
    	newContact.setEmailAddress("unittest@test.com");
    	newContact.setPhoneNum("6018180241");
    	newContact.setCntryDialCd("123");
    	newContact.setCountryCd("US");
    	newContact.setCntryFaxDialCd("1");
    	newContact.setFaxNum("3212337184");
    	ResponseEntity<Contact> response =  contactController.create(newContact);
    	newContact = response.getBody();
    	assert(newContact != null);
    	// test read contact with id
    	response = contactController.read(newContact.getContactId());
    	Contact readContact = response.getBody();
    	assert(readContact != null);
    	// test update
    	readContact.setCompany(company);
    	readContact.setLastNm("TESTED");
    	response = contactController.update(readContact);
    	newContact = response.getBody();
    	assert(newContact.getLastNm().equals("TESTED"));
    	// test delete
    	contactController.delete(newContact.getContactId());
    	assert(true); // passes if no exception
    	// test report contacts
    	ResponseEntity<?> reportContacts = contactController.getReportContacts((int)company.getCompanyId(), permitId.intValue(), periodId.intValue());
    	assert(reportContacts.hasBody());
    }

    /**
     * Test company methods.
     */
    private void testCompany() {
        // read by id
        Company testCompany = companyMgmtBizComp.getCompany(company.getCompanyId());
        assert(testCompany.getCompanyId() == company.getCompanyId());
        // update company
        testCompany.setLegalName("CHANGED NAME");
        testCompany = companyMgmtBizComp.updateCompany(testCompany);
        assert("CHANGED NAME".equals(testCompany.getLegalName()));
        // search by criteria
        CompanySearchCriteria criteria = new CompanySearchCriteria();
        criteria.setCompanyNameFilter("CHANGED NAME");
        criteria = companyMgmtBizComp.getCompanies(criteria);
        List<CompanyHistory> histories = criteria.getResults();
        assert(histories.size() == 1);
        // create new permit
        Permit newPermit = new Permit();
        newPermit.setIssueDt("10/26/2016");
        newPermit.setPermitNum("NEW12345");
        newPermit.setPermitStatusTypeCd("ACTV");
        newPermit.setPermitTypeCd("MANU");
        newPermit.setCompanyId(company.getCompanyId());
        testCompany = companyMgmtBizComp.createPermit(newPermit);
        assert(testCompany.getPermitSet().size() == 2);
        // get permits
        List<Permit> permits = companyMgmtBizComp.getPermits("ACTV", company.getCompanyId());
        assert(permits.size() == 2);
        // update permit
        Permit permit = permits.get(0);
        if(!"NEW12345".equals(permit.getPermitNum())) permit = permits.get(1);
        permit.setPermitNum("CHANGED123");
        testCompany = companyMgmtBizComp.updatePermit(permit);
        //assert(testCompany.getPermitSet().contains(permit));
        // get permit by id
        permit = companyMgmtBizComp.getPermit(permit.getPermitId());
        assert("CHANGED123".equals(permit.getPermitNum()));
        // get permit by company id and permit num
        companyMgmtBizComp.getPermitById(company.getCompanyId(), "CHANGED123");
        assert(true);
        // all permit histories for company
        // testCompany = companyMgmtBizComp.getCompanyPermitHistory(testCompany);
        assert(testCompany.getPermitHistoryList().size() > 0);
        // permit history for one permit
        companyMgmtBizComp.getPermitHistory(permit);
        // assert(historyPermit.getPermitHistoryList().size() > 0);
        assert(true);
    }

    /**
     * Test permit methods.
     */
    private void testPermit() {
        // test get permit period by id / id
        PermitPeriod permitPeriod = permitPeriods.get(0);
        permitBizComp.getPermitPeriod(permitPeriod.getPermitId(), permitPeriod.getPeriodId());
        // assert(permitPeriod.getPermitId() == newPermitPeriod.getPermitId() &&
        // permitPeriod.getPeriodId() == newPermitPeriod.getPeriodId());
        // test permit history criteria search
        assert(true);
        PermitHistoryCriteria criteria = new PermitHistoryCriteria();
        criteria.setPermitNameFilter("UN-IT-013");
        criteria = permitBizComp.getPermitHistories(criteria);
        //assert(criteria.getResults().size() == 1);
    }

    private void testAssessment() {
    	try {
    		assessmentBizComp.getQuarterlyAssessment(2, 2017);
    	}
    	catch(Exception e) {

    	}
    	assert(true);
    }

    //
    // manufacturer / importer monthly forms section
    //
    private PermitMfgReport createMockPermitMfgReport(PermitPeriod permitPeriod) {
        PermitMfgReport report = new PermitMfgReport();
        report.setPermitId(permitPeriod.getPermitId());
        report.setPeriodId(permitPeriod.getPeriodId());
        report.setStatus("Complete");
        List<SubmittedForm> submittedForms = new ArrayList<SubmittedForm>();
        // build 6 forms (3852, 5210, and 4 5000 forms)
        List<SubmittedForm> forms3852 = buildForm("3852", permitPeriod);
        List<SubmittedForm> forms5210 = buildForm("5210.5", permitPeriod);
        List<SubmittedForm> forms5000 = buildForm("5000.24", permitPeriod);
        submittedForms.addAll(forms3852);
        submittedForms.addAll(forms5210);
        submittedForms.addAll(forms5000);
        assert(submittedForms.size() == 3);
        report.setSubmittedForms(submittedForms);
        return report;
    }

    private List<SubmittedForm> buildForm(String formTypeCd, PermitPeriod permitPeriod) {
        Map<Long, Long> tobaccoParentMap = testBizComp.getTobaccoParentMap(permitPeriod.getPeriodId());
        List<SubmittedForm> newForms = new ArrayList<SubmittedForm>();
        if("7501".equals(formTypeCd)) {
            // create one form for each tobacco super type
            for(Long child : tobaccoParentMap.keySet()) {
                if(child != tobaccoParentMap.get(child)) continue;
                SubmittedForm form = new SubmittedForm();
                List<FormDetail> details = new ArrayList<FormDetail>();
                form.setFormTypeCd(formTypeCd);
                form.setPermitId(permitPeriod.getPermitId());
                form.setPeriodId(permitPeriod.getPeriodId());
                // add 5 details (arbitrary)
                for(Long line=0L;line < 5L;line++) {
                    FormDetail detail = new FormDetail();
                    detail.setActivityCd("Valid");
                    detail.setDolDifference(12345.67);
                    detail.setRemovalQty(500.0);
                    detail.setRemovalUom("Pounds");
                    detail.setTaxesPaid(12345.67);
                    detail.setTobaccoClassId(child);
                    details.add(detail);
                }
                form.setFormDetails(details);
                newForms.add(form);
            }
        }
        else {
            // create one form, and a detail for each tobacco sub-type
            SubmittedForm form = new SubmittedForm();
            List<FormDetail> details = new ArrayList<FormDetail>();
            form.setFormTypeCd(formTypeCd);
            form.setPermitId(permitPeriod.getPermitId());
            form.setPeriodId(permitPeriod.getPeriodId());
            for(Long child : tobaccoParentMap.keySet()) {
                // just add for child classes
                if(tobaccoParentMap.get(child) == child) continue;
                FormDetail detail = new FormDetail();
                detail.setActivityCd("Valid");
                detail.setDolDifference(12345.67);
                detail.setRemovalQty(500.0);
                detail.setRemovalUom("Pounds");
                detail.setTaxesPaid(12345.67);
                detail.setTobaccoClassId(child);
                details.add(detail);
            }
            form.setFormDetails(details);
            newForms.add(form);
        }
        return newForms;
    }
}

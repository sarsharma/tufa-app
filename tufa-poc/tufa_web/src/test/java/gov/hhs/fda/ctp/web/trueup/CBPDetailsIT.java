package gov.hhs.fda.ctp.web.trueup;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import gov.hhs.fda.ctp.common.beans.CBPAmendment;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonSummary;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({ 
	@ContextConfiguration(classes = { WebTestConfig.class }),
	@ContextConfiguration(classes = { MvcConfig.class })
})

public class CBPDetailsIT {

	@Autowired
	TrueUpRestController controller;

	@Autowired
	private WebApplicationContext context;

	private final Integer CNT_CMP_RESULT_SET = 2;

	private MockMvc mockMvc;
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	private ObjectMapper mapper = new ObjectMapper();

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		List<HttpMessageConverter<?>> converterList = Arrays.asList(converters);

		for (HttpMessageConverter<?> converter : converterList) {
			if (converter instanceof MappingJackson2HttpMessageConverter)
				this.mappingJackson2HttpMessageConverter = converter;
		}

		Assert.assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setup() {

		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}


	@Test
	public void testCompareCBPSummary() throws Exception {

		TrueUpCompareSearchCriteria crit  = new TrueUpCompareSearchCriteria();
		crit.setPermitTypeFilter("IMPT");
		
		List<TrueUpComparisonResults> compareResults = getComparisonResults("2015_CBP_3344Scenarios.xlsx", "CBP", 2015,
				crit);

		long fiscalYear = 2015l;

		int cnt = 0;

		for (TrueUpComparisonResults cmprslt : compareResults) {

			long companyId = cmprslt.getCompanyId();
			String qtr = cmprslt.getQuarter();
			String tobaccoClassNm = cmprslt.getTobaccoClass();
			Double delta = cmprslt.getTotalDeltaTax();
			ResponseEntity rs = new ResponseEntity<TrueUpComparisonSummary>(HttpStatus.OK);

//			if (tobaccoClassNm.equalsIgnoreCase("Cigars"))
//				rs = this.controller.getCigarImporterComparisonSummary(companyId, fiscalYear);
//			else
//				rs = this.controller.getNonCigarImporterComparisonSummary(companyId, fiscalYear, Long.parseLong(qtr),
//						tobaccoClassNm);

			Assert.assertNotNull(rs);

			List<TrueUpComparisonSummary> trueUpSummary = (List<TrueUpComparisonSummary>) rs.getBody();

			Assert.assertTrue(trueUpSummary != null && trueUpSummary.size() > 1);

			// Check the delta value getting returned in the Summary data
			TrueUpComparisonSummary cmpSummValues = trueUpSummary.get(trueUpSummary.size() - 1);
			List<String> cellValues = cmpSummValues.getCellValues();

			Assert.assertTrue(cellValues != null && cellValues.size() > 1);

			String dlt = cellValues.get(cellValues.size() - 1);

			Assert.assertEquals("Deltas from Comparison Landing Page and CBP Summary Details Page are not equal",
					Double.parseDouble(dlt), delta, 0);

			if (++cnt > this.CNT_CMP_RESULT_SET)
				break;
		}

		this.controller.deleteIngestionFile("CBP", 2015);

	}

	@Test
	public void testCompareCBPDetails() throws Exception {

		TrueUpCompareSearchCriteria crit  = new TrueUpCompareSearchCriteria();
		crit.setPermitTypeFilter("IMPT");
		
		List<TrueUpComparisonResults> compareResults = getComparisonResults("2015_CBP_3344Scenarios.xlsx", "CBP", 2015,
				crit);

		long fiscalYear = 2015l;

		int cnt = 0;

		for (TrueUpComparisonResults cmprslt : compareResults) {
			
			String companyNm = cmprslt.getLegalName();
			long companyId = cmprslt.getCompanyId();
			String qtr = cmprslt.getQuarter();
			String tobaccoClassNm = cmprslt.getTobaccoClass();
			Double delta = cmprslt.getTotalDeltaTax();
			String ein = cmprslt.getEin();
			ResponseEntity rsFDA;

			// Get FDA data
			if (tobaccoClassNm.equalsIgnoreCase("Cigars"))
				rsFDA = this.controller.getCigarImporterComparisonDetails(ein, fiscalYear, 5, tobaccoClassNm);
			else
				rsFDA = this.controller.getNonCigarImporterComparisonDetails(ein, fiscalYear, Long.parseLong(qtr),
						tobaccoClassNm);

			Assert.assertNotNull(rsFDA);

			List<TrueUpComparisonImporterDetail> cbpDetails = (List<TrueUpComparisonImporterDetail>) rsFDA.getBody();

			String totalFDATx = "0";

			Assert.assertTrue(cbpDetails != null && cbpDetails.size() > 1);

			for (TrueUpComparisonImporterDetail detail : cbpDetails)
				totalFDATx += detail.getPeriodTotal();

			// Get Ingestion Data
			ResponseEntity rsIngestion;

			if (tobaccoClassNm.equalsIgnoreCase("Cigars"))
				rsIngestion = this.controller.getCigarImporterComparisonIngestionDetails(ein, fiscalYear, 5,
						tobaccoClassNm);
			else
				rsIngestion = this.controller.getNonCigarImporterComparisonIngestionDetails(ein, fiscalYear,
						Long.parseLong(qtr), tobaccoClassNm);

			List<TrueUpComparisonImporterIngestionDetail> cbpIngestion = (List<TrueUpComparisonImporterIngestionDetail>) rsIngestion
					.getBody();

			Assert.assertTrue(cbpIngestion != null && cbpIngestion.size() > 1);

			String totalIngestionTx = "0";

			for (TrueUpComparisonImporterIngestionDetail detail : cbpIngestion)
				totalIngestionTx += detail.getPeriodTotal();

			Assert.assertEquals(
					" Deltas from Comparison Landing Page and CBP Details are not equal for " + " Company Name: "
							+ companyNm + " Tobacco Class: " + tobaccoClassNm + " quarter: " + qtr,
					(Double.valueOf(totalFDATx) - Double.valueOf(totalIngestionTx)), delta, 0);
			
			if (++cnt > this.CNT_CMP_RESULT_SET)
				break;

		}

		this.controller.deleteIngestionFile("CBP", 2015);

	}

	/*******
	 * Save CBP Amendments
	 * 
	 * @throws Exception
	 **************/

	@Test
	public void saveCBPAmendments() throws Exception {

		TrueUpCompareSearchCriteria crit  = new TrueUpCompareSearchCriteria();
		crit.setPermitTypeFilter("IMPT");
		
		List<TrueUpComparisonResults> compareResults = getComparisonResults("2015_CBP_3344Scenarios.xlsx", "CBP", 2015,
				crit);

		long fiscalYr = 2015l;
		int cnt =0;
		
		for (TrueUpComparisonResults cmprslt : compareResults) {

			long companyId = cmprslt.getCompanyId();
			String qtr = cmprslt.getQuarter();
			String tobaccoClassNm = cmprslt.getTobaccoClass();
			String status = cmprslt.getStatus();

			CBPAmendment amd = new CBPAmendment();
			amd.setTobaccoType(tobaccoClassNm);
			List<CBPAmendment> amdts = new ArrayList<>();
			amdts.add(amd);
			ResponseEntity rsAmnd = this.controller.getCBPAmendments(fiscalYr, companyId, amdts);

			List<CBPAmendment> amendments = (List<CBPAmendment>) rsAmnd.getBody();

			// Check if amendment status on compare landing indicates correctly
			// amendment status(as values entered) on CBP details.
			if (status == null)
				Assert.assertTrue(amendments == null || amendments.isEmpty());
			else if (status.equalsIgnoreCase("Amended"))
				Assert.assertTrue(amendments != null && amendments.size() > 1);
			 

			// save amendments with new tax and volume values
			if (status == null) {
				List<CBPAmendment> amndts  = getCBPAmendmentList(companyId,tobaccoClassNm,new Long(fiscalYr).toString(),qtr);
				ResponseEntity savedAmndsRs = this.controller.saveCBPAmendments(fiscalYr, amndts);
				List<CBPAmendment> svAdments = (List<CBPAmendment>) savedAmndsRs.getBody();
				Assert.assertTrue(svAdments!=null && svAdments.size()>0);
			}
			
			
			if (++cnt > this.CNT_CMP_RESULT_SET)
				break;
		}
		
		this.controller.deleteIngestionFile("CBP", 2015);


	}
	
	
	/************ Private Methods *******************/
	
	/***
	 * 
	 * 
	 * @param fileName
	 * @param ftype
	 * @param year
	 * @param criteria
	 * @return
	 * @throws Exception
	 */
	private List<TrueUpComparisonResults> getComparisonResults(String fileName, String ftype, long year,
			TrueUpCompareSearchCriteria criteria) throws Exception {

		long yr = year;
		String filenm = fileName;
		String type = ftype;

		this.controller.deleteIngestionFile(type, yr);
		// Ingest file with test case conditions 

		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(filenm);

		byte[] fileBytes = IOUtils.toByteArray(inputStream);

		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", filenm, "multipart/*", fileBytes);

		ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, type, new Long(yr).toString());
		Assert.assertNotNull(res);

		//Call on compare landing page 

		ResponseEntity results = this.controller.getComparisonResults(yr, criteria);

		Map<String, Object> obj = (Map<String, Object>) results.getBody();

		List<TrueUpComparisonResults> compareResults = (List<TrueUpComparisonResults>) obj.get("results");

		Assert.assertTrue("Compare Results is empty", compareResults != null && compareResults.size() > 0);

		return compareResults;

	}

	private List<CBPAmendment> getCBPAmendmentList(long companyId, String tobaccoClassNm, String fiscalYr, String qtr) {

		List<CBPAmendment> amendments = new ArrayList<>();
		// create new tax amendment
		CBPAmendment cbpAmd = new CBPAmendment();

		cbpAmd.setCbpCompanyId(companyId);
		cbpAmd.setTobaccoType(tobaccoClassNm);
		cbpAmd.setFiscalYr(new Long(fiscalYr).toString());
		if (tobaccoClassNm.equalsIgnoreCase("Cigars"))
			cbpAmd.setQtr("5");
		else
			cbpAmd.setQtr(qtr);
		cbpAmd.setAmendedTotalTax(500d);
		cbpAmd.setAmendedTotalVolume(600d);
		cbpAmd.setAcceptanceFlag("N");
		
		amendments.add(cbpAmd);
		return amendments;

	}

	/************************************************************
	 * JSONIZER
	 ************************************************************/
	@SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}

}

package gov.hhs.fda.ctp.web.trueup;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import gov.hhs.fda.ctp.common.beans.SelectTobaccoType;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

@ActiveProfiles({"persistenceingestmaptest"})
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {PersistenceHibernateTestConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class})
})
@Transactional
public class TestCompareAllDeltasIT {
	@Autowired
	TrueUpRestController controller;
	
	
    private MockMvc mockMvc;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;
    
	private ObjectMapper mapper = new ObjectMapper();
	
	private TrueUpCompareSearchCriteria criteria;


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));


    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
        

    	
    	this.controller.deleteIngestionFile("CBP", 2015);
        /**** Ingest file with test case conditions ****/
    	
		InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2015_CBP_3344Scenarios.xlsx");
				
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2015_CBP_3344Scenarios.xlsx","multipart/*", fileBytes);
		
        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "CBP", "2015");
    	
        criteria = new TrueUpCompareSearchCriteria();
        this.selectAllTobaccoTypes(criteria);
    }
    
    @After
    public void teardown() {
		/** Delete the ingested file after the test **/
        this.controller.deleteIngestionFile("CBP", 2015);
    }
    
	@Test
    public void testCBPIngestCompareAllDeltas() throws Exception {
        
        TrueUpCompareSearchCriteria criteria = new TrueUpCompareSearchCriteria();
        this.selectAllTobaccoTypes(criteria);
        
        ResponseEntity results =  this.controller.getAllComparisonResults(2015, criteria);
    
        Map<String, Object> obj = (Map<String, Object>) results.getBody(); 
        
        List<TrueUpComparisonResults> comparecount = (List<TrueUpComparisonResults>) obj.get("results");
        
        if (comparecount.size() > 0)
        { 
        	for (TrueUpComparisonResults item : comparecount) 
        	{
           
        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) >= 1);
        		Assert.assertTrue("INGESTED".equals(item.getDataType()) || "TUFA".equals(item.getDataType()) || "MATCHED".equals(item.getDataType()));
        		Assert.assertTrue(
        				"Review".equals(item.getAllDeltaStatus()) ||
        				"NA".equals(item.getAllDeltaStatus()) ||
        				"Excluded".equals(item.getAllDeltaStatus()) ||
        				"Associated".equals(item.getAllDeltaStatus()) ||
        				"MS Ready".equals(item.getAllDeltaStatus())
        				);
            }
        	
        }
        /**** Assert or verify  UnknownBucket according to your testcases as per expected results  *****/
		Assert.assertNotNull(obj);
		/** Delete the ingested file after the test **/
       this.controller.deleteIngestionFile("CBP", 2015);
    }
    
    @Test
    public void testCBPIngestCompareAllDeltasFilters() throws Exception {
        
        //Company Name
        criteria.setCompanyFilter("A");
        List<TrueUpComparisonResults> comparecount = this.callGetAllComparisonResults(criteria);
        criteria.setCompanyFilter(null);
        
        if (comparecount.size() > 0)
        { 
        	for (TrueUpComparisonResults item : comparecount) 
        	{
        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) >= 1);
        		Assert.assertTrue(item.getLegalName().toLowerCase().contains("a"));
            }
        	
        }
        
        //EIN
        criteria.setEinFilter("1");
        comparecount = this.callGetAllComparisonResults(criteria);
        criteria.setEinFilter(null);
        
        if (comparecount.size() > 0)
        { 
        	for (TrueUpComparisonResults item : comparecount) 
        	{
        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) >= 1);
        		Assert.assertTrue(item.getEin().contains("1"));
            }
        	
        }
        
        //Original Name
        criteria.setOriginalNameFilter("A");
        comparecount = this.callGetAllComparisonResults(criteria);
        criteria.setOriginalNameFilter(null);
        
        if (comparecount.size() > 0)
        { 
        	for (TrueUpComparisonResults item : comparecount) 
        	{
        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) >= 1);
        		Assert.assertTrue(item.getOriginalName().toLowerCase().contains("a"));
            }
        	
        }
        
        //Tobacco Class
        criteria.setTobaccoClassFilter("Cig");
        comparecount = this.callGetAllComparisonResults(criteria);
        criteria.setEinFilter(null);
        
        if (comparecount.size() > 0)
        { 
        	for (TrueUpComparisonResults item : comparecount) 
        	{
        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) >= 1);
        		Assert.assertTrue(item.getTobaccoClass().contains("Cig"));
            }
        	
        }
        
        //Permit Type
        criteria.setPermitTypeFilter("impt");
        comparecount = this.callGetAllComparisonResults(criteria);
        criteria.setPermitTypeFilter(null);
        
        if (comparecount.size() > 0)
        { 
        	for (TrueUpComparisonResults item : comparecount) 
        	{
        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) >= 1);
        		Assert.assertTrue("importer".equalsIgnoreCase(item.getPermitType()));
            }
        	
        }
        
        //Permit Type
        criteria.setQuarterFilter("2");
        comparecount = this.callGetAllComparisonResults(criteria);
        criteria.setQuarterFilter(null);
        
        if (comparecount.size() > 0)
        { 
        	for (TrueUpComparisonResults item : comparecount) 
        	{
        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) >= 1);
        		Assert.assertTrue("2".equalsIgnoreCase(item.getQuarter()));
            }
        	
        }
        
        //Delta
        criteria.setDeltaTaxFilter("1");;
        comparecount = this.callGetAllComparisonResults(criteria);
        criteria.setDeltaTaxFilter(null);
        
        if (comparecount.size() > 0)
        { 
        	for (TrueUpComparisonResults item : comparecount) 
        	{
        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) >= 1);
        		Assert.assertTrue(item.getAbstotalDeltaTax().toString().contains("1"));
            }
        	
        }
        
      //Data Type
        criteria.setDataTypeFilter("INGESTED");
        comparecount = this.callGetAllComparisonResults(criteria);
        criteria.setDataTypeFilter(null);
        
        if (comparecount.size() > 0)
        { 
        	for (TrueUpComparisonResults item : comparecount) 
        	{
        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) >= 1);
        		Assert.assertTrue("INGESTED".equals(item.getDataType()));
            }
        	
        }
        
        //Status
       // criteria.setAllDeltaStatusTypeFilter.add("Review");
        comparecount = this.callGetAllComparisonResults(criteria);
        criteria.setAllDeltaStatusTypeFilter(null);
        
        if (comparecount.size() > 0)
        { 
        	for (TrueUpComparisonResults item : comparecount) 
        	{
        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) >= 1);
        		Assert.assertTrue("Review".equals(item.getAllDeltaStatus()));
            }
        	
        }
        /**** Assert or verify  UnknownBucket according to your testcases as per expected results  *****/
//		Assert.assertNotNull(obj);
    }

    private List<TrueUpComparisonResults> callGetAllComparisonResults(TrueUpCompareSearchCriteria criteria) {
        
        ResponseEntity results =  this.controller.getAllComparisonResults(2015, criteria);
    
        Map<String, Object> obj = (Map<String, Object>) results.getBody(); 
        
        return (List<TrueUpComparisonResults>) obj.get("results");
	}

	private void selectAllTobaccoTypes(TrueUpCompareSearchCriteria criteria) {
    	criteria.setSelTobaccoTypes(new SelectTobaccoType());
		criteria.getSelTobaccoTypes().setShowChews(true);
		criteria.getSelTobaccoTypes().setShowCigarettes(true);
		criteria.getSelTobaccoTypes().setShowCigars(true);
		criteria.getSelTobaccoTypes().setShowPipes(true);
		criteria.getSelTobaccoTypes().setShowRollYourOwn(true);
		criteria.getSelTobaccoTypes().setShowSnuffs(true);
	}
}

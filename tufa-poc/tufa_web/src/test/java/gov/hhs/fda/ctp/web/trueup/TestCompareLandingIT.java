package gov.hhs.fda.ctp.web.trueup;

import gov.hhs.fda.ctp.common.beans.CBPEntry;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.TrueUp;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionHTSCodeSummary;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpFilter;
import gov.hhs.fda.ctp.common.beans.UnknownCompanyBucket;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.persistence.model.TTBPermitBucket;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

@ActiveProfiles({"persistenceingestmaptest"})
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {PersistenceHibernateTestConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class})
})
@Transactional
public class TestCompareLandingIT {
	@Autowired
	TrueUpRestController controller;
	
	
    private MockMvc mockMvc;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;
    
	private ObjectMapper mapper = new ObjectMapper();


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));


    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
    }
    
    @After
    public void teardown() {
    		
        
    }
        
    @Test
    public void deleteIngestedFile() throws Exception {
		this.controller.deleteIngestionFile("CBP", 2018);
		this.controller.deleteIngestionFile("CBP", 2016);
		this.controller.deleteIngestionFile("CBP", 2015);
		this.controller.deleteIngestionFile("TTB Tax", 2015);
        this.controller.deleteIngestionFile("TTB Volume", 2015);

    }
    
	 @Test
    public void exportTTBerrors() throws Exception {
    	/**** Ingest file with test case conditions ****/
    	
		InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2015_TTBv_ERRORS.xlsx");		
		
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2015_TTBv_ERRORS.xlsx","multipart/*", fileBytes);
		
		ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "TTB Volume", "2015");
        Assert.assertNotNull(res);
       
        /** Delete the ingested file after the test **/
        this.controller.deleteIngestionFile("TTB Volume", 2015);    	
    }
	 
	 
	    @Test
	    //E2E Code Coverage - CBP Detail and navigation to Quarter
	    public void testCBPIngestComparelanding() throws Exception {
	    	
	    	
	    	this.controller.deleteIngestionFile("CBP", 2018);
	        /**** Ingest file with test case conditions ****/
	    	
			InputStream inputStream = this.getClass().getClassLoader()
				    .getResourceAsStream("2018_CBP_3346.xlsx");
					
			byte[] fileBytes = IOUtils.toByteArray(inputStream);
			
			MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2018_CBP_3346.xlsx","multipart/*", fileBytes);
			
	        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "CBP", "2018");
	        Assert.assertNotNull(res);
	          
	        /****  Call on compare landing page ***/
	        
	        TrueUpCompareSearchCriteria criteria = new TrueUpCompareSearchCriteria();
	        
	        ResponseEntity results =  this.controller.getComparisonResults(2018, criteria);
	    
	        Map<String, Object> obj = (Map<String, Object>) results.getBody(); 
	        
	        List<TrueUpComparisonResults> comparecount = (List<TrueUpComparisonResults>) obj.get("results");
	        
	        if (comparecount.size() > 0)
	        { 
	        	for (TrueUpComparisonResults item : comparecount) 
	        	{
	                
	        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) > 1);
	        		Assert.assertTrue("Subnavigation to  Quarter. accessible", item.getQuarter() != null );
	        
	            }
	        	
	        }
	        /**** Assert or verify  UnknownBucket according to your testcases as per expected results  *****/
			Assert.assertNotNull(obj);
			/** Delete the ingested file after the test **/
	       this.controller.deleteIngestionFile("CBP", 2018);
		}

	    //E2E Code Coverage - CBP Detail and navigation to Quarter
	    @Test    
	    public void testCBPIngestNavigationtoQuarterlanding() throws Exception {
	    	
	    	
	    	  this.controller.deleteIngestionFile("CBP", 2018);
	        /**** Ingest file with test case conditions ****/
	    	
	    	
			InputStream inputStream = this.getClass().getClassLoader()
				    .getResourceAsStream("2018_CBP_3346_subnav.xlsx");
					
			byte[] fileBytes = IOUtils.toByteArray(inputStream);
			
			MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2018_CBP_3346_subnav.xlsx","multipart/*", fileBytes);
			
	        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "CBP", "2018");
	        Assert.assertNotNull(res);
	          
	        /****  Call on compare landing page ***/
	        
	        TrueUpCompareSearchCriteria criteria = new TrueUpCompareSearchCriteria();
	        
	        ResponseEntity results =  this.controller.getComparisonResults(2018, criteria);
	    
	        Map<String, Object> obj = (Map<String, Object>) results.getBody(); 
	        
	        int comparecount =  (int) obj.get("itemsTotal");
	        
	        // no data and no navigation when delta < 1 and no navigation available
	        assertThat(comparecount, is(4));	        
                    
			/** Delete the ingested file after the test **/
	        this.controller.deleteIngestionFile("CBP", 2018);
		}
	    
	    @Test
	    //E2E Code Coverage - CBP scenarios for 3344 Detail and navigation to Quarter
	    public void testCBPComparelanding() throws Exception {
	    
	    
			this.controller.deleteIngestionFile("CBP", 2015);
	        /**** Ingest file with test case conditions ****/
	    	
			InputStream inputStream = this.getClass().getClassLoader()
				    .getResourceAsStream("2015_CBP_3344Scenarios.xlsx");
					
			byte[] fileBytes = IOUtils.toByteArray(inputStream);
			
			MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2015_CBP_3344Scenarios.xlsx","multipart/*", fileBytes);
			
	        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "CBP", "2015");
	        Assert.assertNotNull(res);
	          
	        /****  Call on compare landing page ***/
	        
	        TrueUpCompareSearchCriteria criteria = new TrueUpCompareSearchCriteria();
	        
	        ResponseEntity results =  this.controller.getComparisonResults(2015, criteria);
	    
	        Map<String, Object> obj = (Map<String, Object>) results.getBody(); 
	        
	        List<TrueUpComparisonResults> comparecount = (List<TrueUpComparisonResults>) obj.get("results");
	        
	        if (comparecount.size() > 0)
	        { 
	        	for (TrueUpComparisonResults item : comparecount) 
	        	{
	           
	        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) >= 1);
	        
	            }
	        	
	        }
	        /**** Assert or verify  UnknownBucket according to your testcases as per expected results  *****/
			Assert.assertNotNull(obj);
			/** Delete the ingested file after the test **/
	       this.controller.deleteIngestionFile("CBP", 2015);
		}

	    @Test
	    //E2E Code Coverage - TTB scenarios for 3344 Detail and navigation to Quarter
	    public void testTTBComparelanding() throws Exception {
	    	
			this.controller.deleteIngestionFile("TTB Tax", 2015);
	        /**** Ingest file with test case conditions ****/
	    	
			InputStream inputStream = this.getClass().getClassLoader()
				    .getResourceAsStream("2015_TTB_3344.xlsx");
					
			byte[] fileBytes = IOUtils.toByteArray(inputStream);
			
			MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2015_TTB_3344.xlsx","multipart/*", fileBytes);
			
	        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "TTB Tax", "2015");
	        Assert.assertNotNull(res);
	          
	        /****  Call on compare landing page ***/
	        
	        TrueUpCompareSearchCriteria criteria = new TrueUpCompareSearchCriteria();
	        
	        ResponseEntity results =  this.controller.getComparisonResults(2015, criteria);
	    
	        Map<String, Object> obj = (Map<String, Object>) results.getBody(); 
	        
	        List<TrueUpComparisonResults> comparecount = (List<TrueUpComparisonResults>) obj.get("results");
	        
	        if (comparecount.size() > 0)
	        { 
	        	for (TrueUpComparisonResults item : comparecount) 
	        	{
	           
	        		Assert.assertTrue("Delta > 1", Math.abs(item.getTotalDeltaTax()) >= 1);
	        
	            }
	        	
	        }
	        /**** Assert or verify  UnknownBucket according to your testcases as per expected results  *****/
			Assert.assertNotNull(obj);
			/** Delete the ingested file after the test **/
	       this.controller.deleteIngestionFile("TTB Tax", 2015);
		}

	    @Test
	    public void getImporterComparisonIngestionDetailsIT() throws Exception{
	    	
	    	this.controller.deleteIngestionFile("CBP", 2018);
	    	
	    	  /**** Ingest file with test case conditions ****/
	    	
	    	InputStream inputStream = this.getClass().getClassLoader()
				    .getResourceAsStream("2018_CBP_3346.xlsx");
					
			byte[] fileBytes = IOUtils.toByteArray(inputStream);
			
			MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2018_CBP_3346.xlsx","multipart/*", fileBytes);
			
	        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "CBP", "2018");
	        Assert.assertNotNull(res);
	    	       
	        ResponseEntity results =  this.controller.getCigarImporterComparisonIngestionDetails("123456789", 2018, 4, "Pipe");
	        List<TrueUpComparisonImporterIngestionDetail> indetail = (List<TrueUpComparisonImporterIngestionDetail>)  results.getBody();

	    	Assert.assertNotNull(results);
	    	Assert.assertThat(indetail.size() , is(3));
	   
		    this.controller.deleteIngestionFile("CBP", 2018);
	    }
	    
	    @Test
	    public void getImporterComparisonIngestionHTSCodeSummaryIT() throws Exception{
	    	
	    	  /**** Ingest file with test case conditions ****/
	    	
	    	this.controller.deleteIngestionFile("CBP", 2018);
	    	
	    	InputStream inputStream = this.getClass().getClassLoader()
				    .getResourceAsStream("2018_CBP_3346.xlsx");
					
			byte[] fileBytes = IOUtils.toByteArray(inputStream);
			
			MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2018_CBP_3346.xlsx","multipart/*", fileBytes);
			
	        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "CBP", "2018");
	        Assert.assertNotNull(res);
	    	       
	        ResponseEntity results =  this.controller.getNonCigarImporterComparisonIngestionHTSCodeSummary("123456789", 2018, 4, "Pipe");
	        List<TrueUpComparisonImporterIngestionHTSCodeSummary> indetail = (List<TrueUpComparisonImporterIngestionHTSCodeSummary>)  results.getBody();

	    	Assert.assertNotNull(results);
	    	Assert.assertThat(indetail.size() , is(1));
	   
		    this.controller.deleteIngestionFile("CBP", 2018);
	    }
	    
	    
	    @Test
	    public void getManufacturerComparisonDetailsIT() throws Exception{
	    	
	        this.controller.deleteIngestionFile("TTB Tax", 2018);
	    	
	    	  /**** Ingest file with test case conditions ****/
	    	
	    	InputStream inputStream = this.getClass().getClassLoader()
				    .getResourceAsStream("2018_TTB_Compare.xlsx");
					
			byte[] fileBytes = IOUtils.toByteArray(inputStream);
			
			MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2018_TTB_Compare.xlsx","multipart/*", fileBytes);
			
	        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "TTB Tax", "2018");
	        Assert.assertNotNull(res);
	    	       
	        ResponseEntity results =  this.controller.getNonCigarManufacturerComparisonDetails(85, 2018, 2, "Cigarettes");
	        List<TrueUpComparisonManufacturerDetail> indetail = (List<TrueUpComparisonManufacturerDetail>)  results.getBody();

	    	Assert.assertNotNull(results);
	    	Assert.assertThat(indetail.size() , is(1));
	   
		    this.controller.deleteIngestionFile("TTB Tax", 2018);
	    }
	    
	    @Test
	    public void testGetTrueUps() throws Exception {
	    	TrueUpFilter filter = new TrueUpFilter();
	    	filter.setFiscalYrFilters(new HashSet(Arrays.asList(2017L,2018L,5000L)));
	    	TrueUpFilter trues = this.controller.getAssessments(filter).getBody();
	    	Assert.assertNotNull(trues);
	    }
	    
	    @Test
	    public void testMarketShare() throws Exception {
	    	TrueUp gshare = this.controller.generateAnnualMarketShare(2017).getBody();
	    	TrueUp mshare = this.controller.getAnnualMarketShare(2017).getBody();
	    	TrueUp subshare = this.controller.submitAnnualMarketShare(2017).getBody();
	    	Assert.assertNotNull(gshare);
	    	Assert.assertNotNull(mshare);
	    	Assert.assertNotNull(subshare);
	    }
	    
	    
	    @Test
	    public void updateAssociateHTSMissingBucketIT() throws Exception{
	    	
	    	  this.controller.deleteIngestionFile("CBP", 2018);
	    	  /**** Ingest file with test case conditions ****/
	    	
	    	InputStream inputStream = this.getClass().getClassLoader()
				    .getResourceAsStream("2018_CBP_Known HTS Codes and Mix scenarios_TEST.xlsx");
					
			byte[] fileBytes = IOUtils.toByteArray(inputStream);
			
			MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2018_CBP_Known HTS Codes and Mix scenarios_TEST.xlsx","multipart/*", fileBytes);
			
	        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "CBP", "2018");
	        Assert.assertNotNull(res);
	       
			List<CBPEntry> lsthts=  (List<CBPEntry>) this.controller.getAssociatedHTSBucket(2018).getBody();
			
	       
	        ResponseEntity results =  this.controller.saveHTSMissingAssociations(lsthts,2018);
	        List<CBPEntry>  indetail = (List<CBPEntry>)results.getBody();

	    	Assert.assertNotNull(results);
	    	//Assert.assertThat(indetail.size() , is(1));
	   
		    this.controller.deleteIngestionFile("CBP", 2018);
	    }

	    
	    
}

package gov.hhs.fda.ctp.web.trueup;

import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import gov.hhs.fda.ctp.biz.trueup.TrueUpBizComp;
import gov.hhs.fda.ctp.common.beans.CBPAmendment;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.TTBAmendment;
import gov.hhs.fda.ctp.common.beans.TrueUp;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonSummary;
import gov.hhs.fda.ctp.persistence.model.TTBPermitBucket;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

/**
 * @author Priti Upadhyaya
 *
 */



@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({ @ContextConfiguration(classes = { WebTestConfig.class }),
		@ContextConfiguration(classes = { MvcConfig.class }) })

//@Transactional
public class TestTTBSummaryandDetails_IT {

	@Autowired
	TrueUpRestController controller;
	
	@Autowired
	TrueUpBizComp bizcomp;

	@Autowired
	private WebApplicationContext context;

	private final Integer CNT_CMP_RESULT_SET = 2;

	private MockMvc mockMvc;
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	private ObjectMapper mapper = new ObjectMapper();

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		List<HttpMessageConverter<?>> converterList = Arrays.asList(converters);

		for (HttpMessageConverter<?> converter : converterList) {
			if (converter instanceof MappingJackson2HttpMessageConverter)
				this.mappingJackson2HttpMessageConverter = converter;
		}

		Assert.assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setup() {

		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	
	@Test
    public void deleteIngestedFile() throws Exception {
		
		this.controller.deleteIngestionFile("TTB Tax", 2018);
        

    }

	@Test
	//3370
	public void testCompareTTBSummary() throws Exception {
		
		this.controller.deleteIngestionFile("TTB Tax", 2018);

		TrueUpCompareSearchCriteria crit  = new TrueUpCompareSearchCriteria();
		crit.setPermitTypeFilter("MANU");
		
		List<TrueUpComparisonResults> compareResults = getComparisonResults("2018_TTB_Compare.xlsx", "TTB Tax", 2018,
				crit);

		long fiscalYear = 2018l;

		int cnt = 0;

		for (TrueUpComparisonResults cmprslt : compareResults) {

			long companyId = cmprslt.getCompanyId();
			String qtr = cmprslt.getQuarter();
			String tobaccoClassNm = cmprslt.getTobaccoClass();
			if (tobaccoClassNm.contains("Pipe"))
				tobaccoClassNm = tobaccoClassNm.replace("Pipe/Roll Your Own", "PIPE-ROLL YOUR OWN");
			if (tobaccoClassNm.contains("Chew"))
				tobaccoClassNm = tobaccoClassNm.replace("Chew/Snuff", "CHEW-SNUFF");
			Double delta = cmprslt.getTotalDeltaTax();
			ResponseEntity rs;

			if (tobaccoClassNm.equalsIgnoreCase("Cigars"))
				rs = this.controller.getCigarManufacturerComparisonSummary(companyId, fiscalYear);
			else
				rs = this.controller.getNonCigarManufacturerComparisonSummary(companyId, fiscalYear, Long.parseLong(qtr),
						tobaccoClassNm);

			Assert.assertNotNull(rs);

			List<TrueUpComparisonSummary> trueUpSummary = (List<TrueUpComparisonSummary>) rs.getBody();

			Assert.assertTrue(trueUpSummary != null && trueUpSummary.size() > 1);

			// Check the delta value getting returned in the Summary data
			TrueUpComparisonSummary ttbSummValues = trueUpSummary.get(trueUpSummary.size() - 1);
			List<String> cellValues = ttbSummValues.getCellValues();

			Assert.assertTrue(cellValues != null && cellValues.size() > 1);

			String dlt = cellValues.get(cellValues.size() - 1);

			Assert.assertEquals("Deltas from Comparison Landing Page and TTB Summary Details Page are not equal",
					Double.parseDouble(dlt), delta, 0);

			if (++cnt > this.CNT_CMP_RESULT_SET)
				break;
		}

		this.controller.deleteIngestionFile("TTB Tax", 2018);

	}

	
	@Test
	public void testCompareTTBdetails() throws Exception {
		
		this.controller.deleteIngestionFile("TTB Tax", 2018);

		TrueUpCompareSearchCriteria crit  = new TrueUpCompareSearchCriteria();
		crit.setPermitTypeFilter("MANU");
		
		List<TrueUpComparisonResults> compareResults = getComparisonResults("2018_TTB_Compare.xlsx", "TTB Tax", 2018,
				crit);

		long fiscalYear = 2018l;

		int cnt = 0;

		for (TrueUpComparisonResults cmprslt : compareResults) {
			
			String companyNm = cmprslt.getLegalName();
			long companyId = cmprslt.getCompanyId();
			String qtr = cmprslt.getQuarter();
			String tobaccoClassNm = cmprslt.getTobaccoClass();
			if (tobaccoClassNm.contains("Pipe"))
				tobaccoClassNm = tobaccoClassNm.replace("Pipe/Roll Your Own", "PIPE-ROLL YOUR OWN");
			if (tobaccoClassNm.contains("Chew"))
				tobaccoClassNm = tobaccoClassNm.replace("Chew/Snuff", "CHEW-SNUFF");
			Double delta = cmprslt.getTotalDeltaTax();
			ResponseEntity rsFDA;

			// Get FDA data
			if (tobaccoClassNm.equalsIgnoreCase("Cigars"))
				rsFDA = this.controller.getCigarManufacturerComparisonSummary(companyId, fiscalYear);
			else
				rsFDA = this.controller.getNonCigarManufacturerComparisonSummary(companyId, fiscalYear, Long.parseLong(qtr),
						tobaccoClassNm);

			Assert.assertNotNull(rsFDA);

			List<TrueUpComparisonSummary> ttbsumm = (List<TrueUpComparisonSummary>) rsFDA.getBody();

			Double totalFDATx = 0d;

			Assert.assertTrue(ttbsumm != null && ttbsumm.size() > 1);

			//for (TrueUpComparisonSummary detail : ttbDetails)
			//	totalFDATx += detail.getPeriodTotal();

			// Get Ingestion Data
			ResponseEntity rsIngestion;

			if (tobaccoClassNm.equalsIgnoreCase("Cigars"))
				rsIngestion = this.controller.getCigarManufacturerComparisonDetails(companyId, fiscalYear, 2,
						tobaccoClassNm);
			else
				rsIngestion = this.controller.getNonCigarManufacturerComparisonDetails(companyId, fiscalYear,
						Long.parseLong(qtr), tobaccoClassNm);

			List<TrueUpComparisonManufacturerDetail> ttbIngestion = (List<TrueUpComparisonManufacturerDetail>) rsIngestion
					.getBody();

			Assert.assertTrue(ttbIngestion != null);

			String totalIngestionTx = "20";

			for (TrueUpComparisonManufacturerDetail detail : ttbIngestion)
				totalIngestionTx += detail.getPeriodTotal();

			if (++cnt > this.CNT_CMP_RESULT_SET)
				break;

		}

		this.controller.deleteIngestionFile("TTB Tax", 2018);

	}

	/*******
	 * Save TTB Amendments
	 * 
	 * @throws Exception
	 **************/

	@Test	
	public void saveTTBAmendments() throws Exception {
		
		this.controller.deleteIngestionFile("TTB Tax", 2018);

		TrueUpCompareSearchCriteria crit  = new TrueUpCompareSearchCriteria();
		crit.setPermitTypeFilter("MANU");
		
		List<TrueUpComparisonResults> compareResults = getComparisonResults("2018_TTB_Compare.xlsx", "TTB Tax", 2018,
				crit);

		long fiscalYr = 2018l;
		int cnt =0;
		
		for (TrueUpComparisonResults cmprslt : compareResults) {

			long companyId = cmprslt.getCompanyId();
			String qtr = cmprslt.getQuarter();
			String tobaccoClassNm = cmprslt.getTobaccoClass();
			
			String status = cmprslt.getStatus();

			TTBAmendment amd = new TTBAmendment();
			amd.setTobaccoType(tobaccoClassNm);
			List<TTBAmendment> amdts = new ArrayList<>();
			amdts.add(amd);
			ResponseEntity rsAmnd = this.controller.getTTBAmendments(fiscalYr, companyId, amdts);

			List<TTBAmendment> amendments = (List<TTBAmendment>) rsAmnd.getBody();

			// Check if amendment status on compare landing indicates correctly
			// amendment status(as values entered) on CBP details.
			if (status == null)
				Assert.assertTrue(amendments == null || amendments.isEmpty());
			else if (status.equalsIgnoreCase("Amended"))
				Assert.assertTrue(amendments != null && amendments.size() > 1);
			 

			// save amendments with new tax and volume values
			if (status == null && tobaccoClassNm.equalsIgnoreCase("cigars")) {
				List<TTBAmendment> amndts  = getTTBAmendmentList(companyId, tobaccoClassNm, new Long(fiscalYr). toString(), qtr);
				ResponseEntity savedAmndsRs = this.controller.saveTTBAmendments(fiscalYr, amndts);
				List<TTBAmendment> svAdments = (List<TTBAmendment>) savedAmndsRs.getBody();
				Assert.assertTrue(svAdments != null && svAdments.size() > 0);
			}
			
			
		//	if (++cnt > this.CNT_CMP_RESULT_SET)
				//break;
		}
		
		this.controller.deleteIngestionFile("TTB Tax", 2018);


	}
	
	@Test
	public void testsaveTTBPermits() throws Exception {
		List<TTBPermitBucket> mockEntries = this.createMockTTBPermitBucket();
		//String helper = (String) this.controller.saveTTBPermits(mockEntries).getBody();
		//assertThat(helper,is("Updated the permits"));
		List<TTBPermitBucket> result = (List<TTBPermitBucket>) this.controller.getTTBPermits(2018).getBody();
		Assert.assertNotNull(mockEntries);
		//assertThat(mockEntries, is(result));
		List<CompanyIngestion> companies = new ArrayList();
		CompanyIngestion company = new CompanyIngestion();
		company.setAssociationType("INCL");
		company.setCompanyName("FANTASIA DISTRIBUTION INC.");
		company.setEin("711021326");
		company.setFiscalYear(2015L);
		company.setImporterEIN("711021326");
		company.setImporterNm("1 WHOLESALE LLC");
		companies.add(company);
		String response = (String) this.controller.saveIncludeCompaniesInMktShBucket(companies, 2018).getBody();
		assertThat(response, is("OK"));
	}
	
	
	@Test
	public void saveComments() throws Exception {
		
		
		this.controller.deleteIngestionFile("TTB Tax", 2018);
		
		TrueUpCompareSearchCriteria crit  = new TrueUpCompareSearchCriteria();
		crit.setPermitTypeFilter("MANU");
		
		List<TrueUpComparisonResults> compareResults = getComparisonResults("2018_TTB_Compare.xlsx", "TTB Tax", 2018,
				crit);

		long fiscalYr = 2018l;
				
		for (TrueUpComparisonResults cmprslt : compareResults) {

			long companyId = cmprslt.getCompanyId();
			String qtr = cmprslt.getQuarter();
			String tobaccoClassNm = cmprslt.getTobaccoClass();
			
			
            if(tobaccoClassNm.equalsIgnoreCase("cigars"))
            {
			   List<TTBAmendment> amendments = this.getTTBAmendmentList(companyId, tobaccoClassNm, new Long(fiscalYr). toString(), qtr);
			
			   ResponseEntity savedAmndsRs = this.controller.saveCompareComments(amendments,2018,"123456789","Pipe");
			
			   List<TTBAmendment> svAdments = (List<TTBAmendment>) savedAmndsRs.getBody();
			   
			   Assert.assertEquals("Priti testing", svAdments.get(0).getComments());

            }
		
		
		}
		this.controller.deleteIngestionFile("TTB Tax", 2018);
		
	}
	
	@Test
	public void saveAcceptFDA3852() throws Exception {
		
		
		this.controller.deleteIngestionFile("TTB Tax", 2018);
		
		TrueUpCompareSearchCriteria crit  = new TrueUpCompareSearchCriteria();
		crit.setPermitTypeFilter("MANU");
		
		List<TrueUpComparisonResults> compareResults = getComparisonResults("2018_TTB_Compare.xlsx", "TTB Tax", 2018,
				crit);

		long fiscalYr = 2018l;
				
		for (TrueUpComparisonResults cmprslt : compareResults) {

			long companyId = cmprslt.getCompanyId();
			String qtr = cmprslt.getQuarter();
			String tobaccoClassNm = cmprslt.getTobaccoClass();
			
            if(tobaccoClassNm.equalsIgnoreCase("cigars"))
            {
			   List<TTBAmendment> amendments = this.getTTBAmendmentList(companyId, tobaccoClassNm, new Long(fiscalYr). toString(), qtr);
			   amendments.get(0).setAcceptanceFlag("Y");
			   
			   ResponseEntity savedfda = this.controller.updateCompareAcceptFDA(fiscalYr, amendments);
			 
			   ResponseEntity getfda = this.controller.getFDA3852flag(fiscalYr, companyId, tobaccoClassNm, "MANU","0");
			  
			   
			   Map<String, String> obj = (Map<String, String>) getfda.getBody(); 
		        
		       String fdaaccepted = (String) obj.get("1-4");
			   	
			   Assert.assertEquals("FDAaccepted", fdaaccepted);

            }	
		}
		
		this.controller.deleteIngestionFile("TTB Tax", 2018);
		
	}
	
	@Test
	public void testMarketShare() throws Exception {
		this.bizcomp.generateAnnualMarketShare(2018, "Y");
		TrueUp annual = this.bizcomp.getTrueUpByYear(2018);
		assertThat(annual.getSubmittedInd(), is("Y"));
		assertThat(annual.getFiscalYear(), is(2018L));
		
		this.bizcomp.generateAnnualMarketShare(2018, "N");
		annual = this.bizcomp.getTrueUpByYear(2018);
		assertThat(annual.getSubmittedInd(), is("N"));
		assertThat(annual.getFiscalYear(), is(2018L));
	}
	
	
	
	/************ Private Methods *******************/
	
	/***
	 * 
	 * 
	 * @param fileName
	 * @param ftype
	 * @param year
	 * @param criteria
	 * @return
	 * @throws Exception
	 */
	private List<TrueUpComparisonResults> getComparisonResults(String fileName, String ftype, long year,
			TrueUpCompareSearchCriteria criteria) throws Exception {

		long yr = year;
		String filenm = fileName;
		String type = ftype;

		this.controller.deleteIngestionFile(type, yr);
		// Ingest file with test case conditions 

		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(filenm);

		byte[] fileBytes = IOUtils.toByteArray(inputStream);

		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", filenm, "multipart/*", fileBytes);

		ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, type, new Long(yr).toString());
		Assert.assertNotNull(res);

		//Call on compare landing page 

		ResponseEntity results = this.controller.getComparisonResults(yr, criteria);

		Map<String, Object> obj = (Map<String, Object>) results.getBody();

		List<TrueUpComparisonResults> compareResults = (List<TrueUpComparisonResults>) obj.get("results");

		Assert.assertTrue("Compare Results is empty", compareResults != null && compareResults.size() > 0);

		return compareResults;

	}

	private List<TTBAmendment> getTTBAmendmentList(long companyId, String tobaccoClassNm, String fiscalYr, String qtr) {

		List<TTBAmendment> amendments = new ArrayList<>();
		// create new tax amendment
		TTBAmendment ttbAmd = new TTBAmendment();

		ttbAmd.setTtbCompanyId(companyId);
		ttbAmd.setTobaccoType(tobaccoClassNm);
		ttbAmd.setFiscalYr(new Long(fiscalYr).toString());
		if (tobaccoClassNm.equalsIgnoreCase("Cigars"))
			ttbAmd.setQtr("5");
		else
			ttbAmd.setQtr(qtr);
		ttbAmd.setAmendedTotalTax(500d);
		ttbAmd.setAmendedTotalVolume(600d);
		ttbAmd.setAcceptanceFlag("N");
		ttbAmd.setComments("Priti testing");
		amendments.add(ttbAmd);
		return amendments;

	}
	
	private List<TTBPermitBucket> createMockTTBPermitBucket() {
		List<TTBPermitBucket> list = new ArrayList<>();
		TTBPermitBucket entry = new TTBPermitBucket();
		entry.setCompanyId(948);
		entry.setCompanyNm("FANTASIA DISTRIBUTION INC.");
		entry.setIncludeFlag("Y");
		entry.setEin("711021326");
		entry.setPermitNum("TPCA15008");
		entry.setPermitId(948);
		entry.setTpbdDt(new Date("01/01/2018"));
		list.add(entry);
		return list;
	}

	/************************************************************
	 * JSONIZER
	 ************************************************************/
	@SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}

}

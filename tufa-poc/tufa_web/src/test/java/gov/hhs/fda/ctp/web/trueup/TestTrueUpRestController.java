/**
 *
 */
package gov.hhs.fda.ctp.web.trueup;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.fasterxml.jackson.databind.ObjectMapper;
import gov.hhs.fda.ctp.biz.companymgmt.CompanyMgmtBizComp;
import gov.hhs.fda.ctp.biz.trueup.TrueUpBizComp;
import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.CBPAmendment;
import gov.hhs.fda.ctp.common.beans.CBPEntry;
import gov.hhs.fda.ctp.common.beans.CBPImporter;
import gov.hhs.fda.ctp.common.beans.CBPLineEntryUpdateInfo;
import gov.hhs.fda.ctp.common.beans.Company;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.Permit;
import gov.hhs.fda.ctp.common.beans.TTBAmendment;
import gov.hhs.fda.ctp.common.beans.TrueUp;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonImporterIngestionHTSCodeSummary;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonManufacturerDetail;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonSummary;
import gov.hhs.fda.ctp.common.beans.TrueUpDocument;
import gov.hhs.fda.ctp.common.beans.TrueUpErrorsExport;
import gov.hhs.fda.ctp.common.beans.TrueUpFilter;
import gov.hhs.fda.ctp.common.beans.UnknownCompanyBucket;
import gov.hhs.fda.ctp.common.ingestion.FileIngestorFactory;
import gov.hhs.fda.ctp.common.ingestion.FileIngestorNameMapping;
import gov.hhs.fda.ctp.common.ingestion.TTB.ExcelTTBFileIngestor;
import gov.hhs.fda.ctp.common.security.util.SecurityContextUtil;
import gov.hhs.fda.ctp.persistence.TrueUpEntityManager;
import gov.hhs.fda.ctp.common.pagination.PaginationAttributes;
import gov.hhs.fda.ctp.persistence.model.HTSCodeEntity;
import gov.hhs.fda.ctp.persistence.model.TTBPermitBucket;
import gov.hhs.fda.ctp.persistence.model.TobaccoClassEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpEntity;
import gov.hhs.fda.ctp.persistence.model.TrueUpSubDocumentEntity;
import gov.hhs.fda.ctp.persistence.model.TufaEntity;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

/**
 * @author rabalasubramani3
 *
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {MvcConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class})
})
//@ActiveProfiles("test")
//@RunWith(PowerMockRunner.class)
//@PowerMockRunnerDelegate(SpringRunner.class)
//@PowerMockIgnore({"javax.management.*"})
//@WebAppConfiguration
//@ContextHierarchy({
//	@ContextConfiguration(classes = {WebTestConfig.class}),
//	@ContextConfiguration(classes = {MvcConfig.class})
//})
public class TestTrueUpRestController {

	@InjectMocks
	TrueUpRestController controller;

	@Mock
	private TrueUpBizComp trueUpBizComp;
	
	@Mock
	private TrueUpEntityManager trueUpEntityManager;
	
	@Mock
	private FileIngestorFactory fileIngestorFactory;

	@Mock
	private PaginationAttributes  paginationattr;
	
	@Mock
	private CompanyMgmtBizComp companyBizComp;

	@Spy
	private FileIngestorNameMapping fileIngestorNameMapping = new FileIngestorNameMapping();
	
	@Mock
	private  TrueUpDocumentEntity trueUpDocumentEntity = new TrueUpDocumentEntity();
	
//	@Rule
//    public PowerMockRule rule = new PowerMockRule();
	
//	@Spy 
//	private SecurityContextUtil securityContextUtil = new SecurityContextUtil();
	
    private MockMvc mockMvc;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;
    
	private ObjectMapper mapper = new ObjectMapper();


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));


    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
    }

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#createTrueUp(gov.hhs.fda.ctp.persistence.model.TrueUpEntity)}.
	 */
	@Test
	public void testCreateTrueUp() throws Exception {
		TrueUpEntity mocktrueUpEntity = this.createMockTrueUpEntity();
		String trueUpEntityJson = json(mocktrueUpEntity);
		String retcreateTrueUp = "1";

        when(
                this.trueUpBizComp.createTrueUp(
                		(TrueUpEntity)notNull())
              ).thenReturn(retcreateTrueUp);

        MvcResult result =  this.mockMvc
    	        .perform(post("/api/v1/trueup")
    	        		.contentType(this.contentType)
    	        		.content(trueUpEntityJson)
    	        		)
    	        .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, retcreateTrueUp);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getTrueUp(long)}.
	 */
	@Test
	public void testGetTrueUp() throws Exception {
		TrueUp mockTrueUp = this.createMockTrueUp();
		String trueUpJson = json(mockTrueUp);

	      when(
	              this.trueUpBizComp.getTrueUpByYear(
	                anyLong()
	              )
	            ).thenReturn(mockTrueUp);

	      MvcResult result = this.mockMvc
	              .perform(get("/api/v1/trueup/{fiscalYear}",2017))
	              .andExpect(status().isOk())
	              .andExpect(content()
	                      .contentTypeCompatibleWith(this.contentType))
	              .andDo(MockMvcResultHandlers.print()).andReturn();

	      String returnString = result.getResponse().getContentAsString();
	      assertEquals(returnString, trueUpJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getAssessments(gov.hhs.fda.ctp.common.beans.TrueUpFilter)}.
	 */
	@Test
	public void testGetAssessments() throws Exception {
		TrueUpFilter mockTrueUpFilter = this.createMockTrueUpFilter();
		List<TrueUp> mockTrueUpList = this.createTrueUpList();
		String trueupFilterJson = json(mockTrueUpFilter);

        when(
                this.trueUpBizComp.getTrueUps(
                		(TrueUpFilter)notNull())
              ).thenReturn(mockTrueUpList);

        MvcResult result = this.mockMvc
	        .perform(put("/api/v1/trueup/list")
	        		.contentType(this.contentType)
	        		.content(trueupFilterJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         //assertEquals(returnString, trueupFilterJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#ingestFileUpload(org.springframework.web.multipart.MultipartFile, java.lang.String, java.lang.String)}.
	 */
//	@Test (expected=RuntimeException.class)
//	@Ignore("Not Implemented Yet")
//	public void testIngestFileUpload() throws Exception {
//		throw new RuntimeException("not yet implemented");
//	}

	
	@Test
	public void testNonCigarCBPSummary() throws Exception {			
		
		List<TrueUpComparisonSummary> mockTruecompareList = this.createTrueUpCompareList() ;
		
		when(
				this.trueUpBizComp.getNonCigarComparisonSummary(2, 3050, 4, "IMPT", "Pipe")				
				).thenReturn(mockTruecompareList);
	
		 MvcResult result = this.mockMvc
	              .perform(get("/api/v1/trueup/compare/impt/noncigars/summary/{companyId}/{fiscalYear}/{quarter}/{tobaccoClass}", 2 ,3050, 4,"Pipe"))
	              .andExpect(status().isOk())
	              .andExpect(content()
	            		  .contentTypeCompatibleWith(this.contentType))
	              .andDo(MockMvcResultHandlers.print()).andReturn();
		
		   String returnString = result.getResponse().getContentAsString();
		 //  assertEquals(returnString, null);
	} 
	
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testTTBSummary() throws Exception {
		throw new RuntimeException("not yet implemented");
	}
	
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testCBPDetailSummary() throws Exception {
		throw new RuntimeException("not yet implemented");
	}
	
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testTTBDetailSummary() throws Exception {
		throw new RuntimeException("not yet implemented");
	}
	
	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getTrueUpDocuments(long)}.
	 */
	@Test
	public void testGetTrueUpDocuments() throws Exception {
		List<TrueUpDocument> mockTrueUpDocuments = this.createMockTrueUpDocuments();
		List<TrueUpDocumentEntity> mockTrueUpDocumentEntities = this.createMockTrueUpDocumentEntities();
		String trueUpDocJson = json(mockTrueUpDocuments);

	      when(
	              this.trueUpBizComp.getTrueupDocuments(
	                anyLong()
	              )
	            ).thenReturn(mockTrueUpDocumentEntities);

	      MvcResult result = this.mockMvc
	              .perform(get("/api/v1/trueup/{fiscalYear}/documents",2017))
	              .andExpect(status().isOk())
	              .andExpect(content()
	                      .contentTypeCompatibleWith(this.contentType))
	              .andDo(MockMvcResultHandlers.print()).andReturn();

	      String returnString = result.getResponse().getContentAsString();
	      assert (returnString != null);
	}

	
	@Test
	public void testGetNonCigarImporterComparisonIngestionDetails() throws Exception {			
		
		List<TrueUpComparisonImporterIngestionDetail> mockTruecompareList = this.createImporterIngestion() ;
		
		when(
				this.trueUpBizComp
				.getImporterComparisonIngestionDetails("123456789", 2017, 3, "IMPT", "Pipe")				
				).thenReturn(mockTruecompareList);
	
		 MvcResult result = this.mockMvc
	              .perform(get("/api/v1/trueup/compare/impt/noncigars/ingestdetails/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}", "123456789",2017, 3,"Pipe"))
	              .andExpect(status().isOk())
	              .andExpect(content()
	            		  .contentTypeCompatibleWith(this.contentType))
	              .andDo(MockMvcResultHandlers.print()).andReturn();
		
		   String returnString = result.getResponse().getContentAsString();
		   assert (returnString != null);
	}
	
	@Test
	public void testGetNonCigarImporterComparisonIngestionHTSCodeSummary() throws Exception {
		List<TrueUpComparisonImporterIngestionHTSCodeSummary> summaries = new ArrayList<>();
		TrueUpComparisonImporterIngestionHTSCodeSummary summary = new TrueUpComparisonImporterIngestionHTSCodeSummary();
		summary.setTobaccoClass("Pipe"); summary.setTaxAmount(3000d); summary.setLineCount(100); summary.setHtsCode("1321321");
		summaries.add(summary);
		when(this.trueUpBizComp.getImporterComparisonIngestionHTSCodeSummary("123456789", 2017, 3, "IMPT", "Pipe")).thenReturn(summaries);
		MvcResult result = this.mockMvc
				.perform(get("/api/v1/trueup/compare/impt/noncigars/ingesthtscodes/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}",1,2017,3,"Pipe"))
				.andExpect(status().isOk())
	              .andExpect(content()
	            		  .contentTypeCompatibleWith(this.contentType))
	              .andDo(MockMvcResultHandlers.print()).andReturn();
		String returnString = result.getResponse().getContentAsString();
		assert (returnString != null);
	}
	
	@Test
	public void testUpdateCompareAcceptFDA () throws Exception{
		List<TTBAmendment> amendments = new ArrayList<>();
		TTBAmendment amendment = new TTBAmendment();
		amendment.setQtr("3"); amendments.add(amendment);
		String jsonRes = json(amendments);
		when(this.trueUpBizComp.updateTTBCompareFDAAcceptFlag(amendments)).thenReturn(amendments);
		MvcResult result = this.mockMvc
				.perform(put("/api/v1/trueup/compare/acceptfda")
	        		.contentType(this.contentType)
	        		.content(jsonRes)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();
		String returnString = result.getResponse().getContentAsString();
		assert (returnString != null);
	}
	

	@Test
	public void testGetCBPAmendments() throws Exception {
		List<CBPAmendment> tobaccoTypes = new ArrayList<>();
		CBPAmendment amendment = new CBPAmendment();
		amendment.setQtr("3");
		tobaccoTypes.add(amendment);
		String jsonRes = json(tobaccoTypes);

	      when(
	              this.trueUpBizComp.getCBPAmendments(2017, 1, tobaccoTypes)
	            ).thenReturn(tobaccoTypes);

	      MvcResult result = this.mockMvc
	              .perform(post("/api/v1/trueup/compare/fiscalYr/{fiscalYr}/company/{companyId}/cbpamendments",2017,1)
	            		  .contentType(this.contentType)
	  	        		.content(jsonRes))
	              .andExpect(status().isOk())
	              .andExpect(content()
	                      .contentTypeCompatibleWith(this.contentType))
	              .andDo(MockMvcResultHandlers.print()).andReturn();

	      String returnString = result.getResponse().getContentAsString();
	      assert (returnString != null);
	}
	
	@Test
	public void testSaveCBPAmendments() throws Exception {
		List<CBPAmendment> tobaccoTypes = new ArrayList<>();
		CBPAmendment amendment = new CBPAmendment();
		amendment.setQtr("3");
		tobaccoTypes.add(amendment);
		String jsonRes = json(tobaccoTypes);
		when(this.trueUpBizComp.saveCBPAmendment(tobaccoTypes)).thenReturn(tobaccoTypes);
		MvcResult result = this.mockMvc
				.perform(put("/api/v1/trueup/compare/cbpamendments")
	        		.contentType(this.contentType)
	        		.content(jsonRes)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();
		String returnString = result.getResponse().getContentAsString();
		assert (returnString != null);
	}
	
	@Test
	public void testUpdateCBPCompareAcceptFDA() throws Exception {
		List<CBPAmendment> tobaccoTypes = new ArrayList<>();
		CBPAmendment amendment = new CBPAmendment();
		amendment.setQtr("3");
		tobaccoTypes.add(amendment);
		String jsonRes = json(tobaccoTypes);
		when(this.trueUpBizComp.updateCBPCompareFDAAcceptFlag(tobaccoTypes)).thenReturn(tobaccoTypes);
		MvcResult result = this.mockMvc
				.perform(put("/api/v1/trueup/compare/cbpacceptfda")
	        		.contentType(this.contentType)
	        		.content(jsonRes)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();
		String returnString = result.getResponse().getContentAsString();
		assert (returnString != null);
	}
	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getHtsCodes()}.
	 */
//	@Test
//	public void testGetHtsCodes() throws Exception {
//		List<HTSCodeEntity> mockCodes = this.createMockHTSCodeEntity();
//		String codesJson = json(mockCodes);
//
//	      when(
//	              this.trueUpBizComp.getHtsCodes(
//	              )
//	            ).thenReturn(mockCodes);
//
//	      MvcResult result = this.mockMvc
//	              .perform(get("/api/v1/trueup/htscodes"))
//	              .andExpect(status().isOk())
//	              .andExpect(content()
//	                      .contentTypeCompatibleWith(this.contentType))
//	              .andDo(MockMvcResultHandlers.print()).andReturn();
//
//	      String returnString = result.getResponse().getContentAsString();
//	      assertEquals(returnString, codesJson);
//	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#saveOrUpdateCode(gov.hhs.fda.ctp.persistence.model.HTSCodeEntity)}.
	 */
	@Test
	public void testSaveOrUpdateCode() throws Exception {
		HTSCodeEntity mockcodeEntity = this.createMockHTSCode();
		String codeJson = json(mockcodeEntity);

        when(
                this.trueUpBizComp.getCodeById(
                		anyString())
              ).thenReturn(mockcodeEntity);

        doNothing().when(
                this.trueUpBizComp).saveOrEditCodes(
                		(HTSCodeEntity)notNull());

        MvcResult result =  this.mockMvc
	        .perform(post("/api/v1/trueup/htscodes")
	        		.contentType(this.contentType)
	        		.content(codeJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, "Successfully saved");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getTrueUpErrors(long, java.lang.String)}.
	 */
	@Test
	public void testGetTrueUpErrors() throws Exception {
		TrueUpErrorsExport mockerrExport = this.createMockTrueUpErrorsExport();
		String errJson = json(mockerrExport);

        when(
                this.trueUpBizComp.getTrueUpErrors(
                		anyLong(), anyString())
              ).thenReturn(mockerrExport);


	      MvcResult result = this.mockMvc
	              .perform(get("/api/v1/trueup/{fiscalYear}/errors/{type}", 2017, "CBP"))
	              .andExpect(status().isOk())
	              .andExpect(content()
	                      .contentTypeCompatibleWith(this.contentType))
	              .andDo(MockMvcResultHandlers.print()).andReturn();


        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, errJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getCompanyComparisonBucket(long)}.
	 */
	@Test
	public void testGetCompanyComparisonBucket() throws Exception {
		List<CBPImporter> mockbucket = this.createMockCBPImporterBucket();
		String bucketJson = json(mockbucket);

        when(
                this.trueUpBizComp.getCompanyComparisonBucket(
                		anyLong())
              ).thenReturn(mockbucket);


	      MvcResult result = this.mockMvc
	              .perform(get("/api/v1/trueup/bucket/company/{fiscalYear}", 2017))
	              .andExpect(status().isOk())
	              .andExpect(content()
	                      .contentTypeCompatibleWith(this.contentType))
	              .andDo(MockMvcResultHandlers.print()).andReturn();


        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, bucketJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getUnknownCompanyBucket(long)}.
	 */
	@Test
	public void testGetUnknownCompanyBucket() throws Exception {
		List<UnknownCompanyBucket> mockbucket = this.createMockUnknownCompanyBucket();
		String bucketJson = json(mockbucket);

        when(
                this.trueUpBizComp.getUnknownCompanies(
                		anyLong())
              ).thenReturn(mockbucket);


	      MvcResult result = this.mockMvc
	              .perform(get("/api/v1/trueup/bucket/unkncompany/{fiscalYear}", 2017))
	              .andExpect(status().isOk())
	              .andExpect(content()
	                      .contentTypeCompatibleWith(this.contentType))
	              .andDo(MockMvcResultHandlers.print()).andReturn();


        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, bucketJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#updateCompanyComparisonBucket(java.util.List)}.
	 */
	@Test
	public void testUpdateCompanyComparisonBucket() throws Exception {
		List<CBPImporter> mockbucket = this.createMockCBPImporterBucket();
		String bucketJson = json(mockbucket);

        doNothing().when(this.trueUpBizComp).updateCompanyComparisonBucket(
                		(List<CBPImporter>)notNull()
              );

        MvcResult result = this.mockMvc
	        .perform(put("/api/v1/trueup/bucket/company")
	        		.contentType(this.contentType)
	        		.content(bucketJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, "OK");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#saveDoc(gov.hhs.fda.ctp.persistence.model.TrueUpSubDocumentEntity, long)}.
	 */
	@Test
	public void testSaveDoc() throws Exception {
		TrueUpSubDocumentEntity mockdoc = this.createMockTrueUpSubDocumentEntity();
		String docJson = json(mockdoc);

        doNothing().when(this.trueUpBizComp).saveSupportDocument(
                		(TrueUpSubDocumentEntity)notNull()
              );

        MvcResult result = this.mockMvc
	        .perform(put("/api/v1/trueup/docs/{fiscalYr}", 2017)
	        		.contentType(this.contentType)
	        		.content(docJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, "Document Saved");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getSupportingDocs(long)}.
	 */
	@Test
	public void testGetSupportingDocs() throws Exception {
		List<TrueUpSubDocumentEntity> mockdoc = this.createMockTrueUpSubDocumentEntiies();
		String docJson = json(mockdoc);

        when(
                this.trueUpBizComp.getSupportDocs(
                		anyLong())
              ).thenReturn(mockdoc);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/{fiscalYr}/docs", 2017)
	        		.contentType(this.contentType)
	        		.content(docJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, docJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getDocument(long)}.
	 */
	@Test
	public void testGetDocument() throws Exception {
		TrueUpSubDocumentEntity mockdoc = this.createMockTrueUpSubDocumentEntity();
		String docJson = json(mockdoc);

        when(
                this.trueUpBizComp.getDocByDocId(
                		anyLong())
              ).thenReturn(mockdoc);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/docs/{docId}", 1L)
	        		.contentType(this.contentType)
	        		.content(docJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, docJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getComparisonResults(long)}.
	 */
	@Test
	@Ignore
	public void testGetComparisonResults() throws Exception {
		List<TrueUpComparisonResults> mockResults = this.createMockTrueupComparisonResults();
		TrueUpCompareSearchCriteria csc = null;
		Map<String, Object> resultsMap = new HashMap<>();
		resultsMap.put("itemsTotal", 100);
		resultsMap.put("results", mockResults);
		String resultsJson = json(resultsMap);
		String cscJson = json(csc);

        when(
                this.trueUpBizComp.getComparisonResults(anyLong(),(TrueUpCompareSearchCriteria)any())
              ).thenReturn(mockResults);
     
        when(
	           this.paginationattr.getItemsTotal()
	         ).thenReturn(100);
        
        
        MvcResult result = this.mockMvc
	        .perform(put("/api/v1/trueup/compare/results/{fiscalYear}", 2017)
	        		.contentType(this.contentType)
	        		.content(cscJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, resultsJson);
	}



	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getCigarManufacturerComparisonSummary(long, long)}.
	 */
	@Test
	public void testGetCigarManufacturerComparisonSummary() throws Exception {
		List<TrueUpComparisonSummary> mockSummary = this.createMockTrueUpComparisonSummary();
		String summaryJson = json(mockSummary);

        when(
                this.trueUpBizComp.getCigarComparisonSummary(
                		anyLong(), anyLong(), anyString())
              ).thenReturn(mockSummary);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/compare/manu/cigars/summary/{companyId}/{fiscalYear}", 1, 2017)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, summaryJson);
	}
	
	@Test
   public void testgetCigarImporterComparisonIngestionDetails() throws Exception{
	   
		List<TrueUpComparisonImporterIngestionDetail> mockSummary = this.createMockTrueUpImporterIngestionDetail();
		String summaryJson = json(mockSummary);
		

        when(
                this.trueUpBizComp.getImporterComparisonIngestionDetails(
                		anyString(), anyLong(), anyLong(),anyString(),anyString() )
              ).thenReturn(mockSummary);

		
	     MvcResult result = this.mockMvc
	 	        .perform(get("/api/v1/trueup/compare/impt/cigars/ingestdetails/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}", "123456789", 2017, 2 ,"Pipe")
	 	        		.contentType(this.contentType)
	 	        		)
	 	        .andExpect(status().isOk())
	            .andDo(MockMvcResultHandlers.print()).andReturn();
	     
	     String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, summaryJson);
	   
   }
	
	   @Test
	   public void testgetCigarImporterComparisonIngestionHTSCodeSummary() throws Exception{
		   
			List<TrueUpComparisonImporterIngestionHTSCodeSummary> mockSummary = this.createMockTrueUpImporterIngestionHTSCodeSummarys();
			String summaryJson = json(mockSummary);	
			
			 when(
		                this.trueUpBizComp.getImporterComparisonIngestionHTSCodeSummary(
		                		anyString(), anyLong(), anyLong(),anyString(),anyString() )
		              ).thenReturn(mockSummary);

			
		     MvcResult result = this.mockMvc
		 	        .perform(get("/api/v1/trueup/compare/impt/cigars/ingesthtscodes/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}", "123456789", 2017,1,"Cigars")
		 	        		.contentType(this.contentType)
		 	        		)
		 	        .andExpect(status().isOk())
		            .andDo(MockMvcResultHandlers.print()).andReturn();
		     
		     String returnString = result.getResponse().getContentAsString();
	         assertEquals(returnString, summaryJson);
		   
	   }
	

	
	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getCigarImporterComparisonSummary(long, long)}.
	 */
	@Test
	public void testGetCigarImporterComparisonSummary() throws Exception {
		List<TrueUpComparisonSummary> mockSummary = this.createMockTrueUpComparisonSummary();
		String summaryJson = json(mockSummary);

        when(
                this.trueUpBizComp.getCigarComparisonSummary(
                		anyLong(), anyLong(), anyString())
              ).thenReturn(mockSummary);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/compare/impt/cigars/summary/{companyId}/{fiscalYear}", 1, 2017)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, summaryJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getNonCigarManufacturerComparisonSummary(long, long, long, java.lang.String)}.
	 */
	@Test
	public void testGetNonCigarManufacturerComparisonSummary() throws Exception {
		List<TrueUpComparisonSummary> mockSummary = this.createMockTrueUpComparisonSummary();
		String summaryJson = json(mockSummary);

        when(
                this.trueUpBizComp.getNonCigarComparisonSummary(
                		anyLong(), anyLong(), anyLong(), anyString(), anyString())
              ).thenReturn(mockSummary);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/compare/manu/noncigars/summary/{companyId}/{fiscalYear}/{quarter}/{tobaccoClass}", 1, 2017,1,"Cigars")
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, summaryJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getNonCigarImporterComparisonSummary(long, long, long, java.lang.String)}.
	 */
	@Test
	public void testGetNonCigarImporterComparisonSummary() throws Exception {
		List<TrueUpComparisonSummary> mockSummary = this.createMockTrueUpComparisonSummary();
		String summaryJson = json(mockSummary);

        when(
                this.trueUpBizComp.getNonCigarComparisonSummary(
                		anyLong(), anyLong(), anyLong(), anyString(), anyString())
              ).thenReturn(mockSummary);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/compare/impt/noncigars/summary/{companyId}/{fiscalYear}/{quarter}/{tobaccoClass}", 1, 2017,1,"Cigarettes")
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, summaryJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getCigarImporterComparisonDetails(long, long, long, java.lang.String)}.
	 */
	@Test
	public void testGetCigarImporterComparisonDetails() throws Exception {
		List<TrueUpComparisonImporterDetail> mockSummary = this.createMockTrueUpComparisonImporterDetail();
		String summaryJson = json(mockSummary);

        when(
                this.trueUpBizComp.getImporterComparisonDetails(
                		anyString(), anyLong(), anyLong(), anyString(), anyString())
              ).thenReturn(mockSummary);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/compare/impt/cigars/details/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}", "123456789", 2017,1,"Cigars")
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, summaryJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getNonCigarImporterComparisonDetails(long, long, long, java.lang.String)}.
	 */
	@Test
	public void testGetNonCigarImporterComparisonDetails() throws Exception {
		List<TrueUpComparisonImporterDetail> mockSummary = this.createMockTrueUpComparisonImporterDetail();
		String summaryJson = json(mockSummary);

        when(
                this.trueUpBizComp.getImporterComparisonDetails(
                		anyString(), anyLong(), anyLong(), anyString(), anyString())
              ).thenReturn(mockSummary);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/compare/impt/noncigars/details/{ein}/{fiscalYear}/{quarter}/{tobaccoClass}", "123456789", 2017,1,"Cigarettes")
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, summaryJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getCigarManufacturerComparisonDetails(long, long, long, java.lang.String)}.
	 */
	@Test
	public void testGetCigarManufacturerComparisonDetails() throws Exception {
		List<TrueUpComparisonManufacturerDetail> mockSummary = this.createMockTrueUpComparisonManufacturerDetail();
		String summaryJson = json(mockSummary);

        when(
                this.trueUpBizComp.getManufacturerComparisonDetails(
                		anyLong(), anyLong(), anyLong(), anyString(), anyString())
              ).thenReturn(mockSummary);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/compare/manu/cigars/details/{companyId}/{fiscalYear}/{quarter}/{tobaccoClass}", 1, 2017,1,"Cigars")
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, summaryJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getNonCigarManufacturerComparisonDetails(long, long, long, java.lang.String)}.
	 */
	@Test
	public void testGetNonCigarManufacturerComparisonDetails() throws Exception {
		List<TrueUpComparisonManufacturerDetail> mockSummary = this.createMockTrueUpComparisonManufacturerDetail();
		String summaryJson = json(mockSummary);

        when(
                this.trueUpBizComp.getManufacturerComparisonDetails(
                		anyLong(), anyLong(), anyLong(), anyString(), anyString())
              ).thenReturn(mockSummary);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/compare/manu/noncigars/details/{companyId}/{fiscalYear}/{quarter}/{tobaccoClass}", 1, 2017,1,"Cigarettes")
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, summaryJson);
	}
	
	@Test
	public void testGetAnnualMarketShare() throws Exception {
		TrueUp annual = this.createMockTrueUp();
		String summaryJson = json(annual);

        when(
                this.trueUpBizComp.getTrueUpByYear(anyLong())
              ).thenReturn(annual);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/marketshare/{fiscalYear}", 2017)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, summaryJson);
	}

	@Test
	public void testGetCompanyToIncldInMktSh() throws Exception {
		CompanyIngestion cmpyIngst = new CompanyIngestion();
		cmpyIngst.setCompanyName("Marlboro");
		cmpyIngst.setEin("12345678");
		String summaryJson = json(cmpyIngst);

        when(
                this.trueUpBizComp.getCompanyToIncldInMktSh(anyLong(), anyString(),anyString())
              ).thenReturn(cmpyIngst);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/bucket/inclcmpymktsh/{fiscalYear}/cmpy/{ein}/{type}", 2017,12345678)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, summaryJson);
	}
	
	@Test
    @Ignore
	public void testCreateIngestedCompany() throws Exception {
		Company company = new Company();
		company.setEinNumber("123456788");
		company.setCompanyStatus("ACTV");
		company.setLegalName("Marlboro");
		
		Permit permit = new Permit();
		permit.setPermitNum("");
		permit.setTpbdDt("06/10/1998");
		permit.setPermitTypeCd("Manufacturer");
		permit.setPermitStatusTypeCd("Active");
		
		Set<Permit> permits = new HashSet<>();
		permits.add(permit);
		company.setPermitSet(permits);
		
		String codeJson = json(company);

		
        when(
                this.companyBizComp.createCompany((Company)notNull())
              ).thenReturn(company);

        MvcResult result =  this.mockMvc
	        .perform(post("/api/v1/trueup/bucket/inclcmpymktsh")
	        		.contentType(this.contentType)
	        		.content(codeJson)
	        		)
	        .andExpect(status().isCreated())
            .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assert(returnString != null);
	}
	
	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getHTSMissingAssociations(long)}.
	 */
	@Test
	public void testGetHTSMissingAssociations() throws Exception {
		List<CBPEntry> mockEntries = this.createMockCBPEntries();
		String entriesJson = json(mockEntries);

        when(
                this.trueUpBizComp.getHTSMissingAssociationData(
                		anyLong())
              ).thenReturn(mockEntries);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/bucket/htsmissing/{fiscalYear}", 2017)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, entriesJson);
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#saveHTSMissingAssociations(java.util.List, long)}.
	 */
	@Test
	public void testSaveHTSMissingAssociations() throws Exception {
		List<CBPEntry> mockEntries = this.createMockCBPEntries();
		String entriesJson = json(mockEntries);

        when(
                this.trueUpBizComp.updateAssociateHTSMissingBucket(
                		(List<CBPEntry>)notNull(), anyLong())
              ).thenReturn(mockEntries);

        MvcResult result = this.mockMvc
	        .perform(put("/api/v1/trueup/bucket/htsmissing/{fiscalYear}", 2017)
	        		.contentType(this.contentType)
	        		.content(entriesJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, entriesJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getAssociatedHTSBucket(long)}.
	 */
	@Test
	public void testGetAssociatedHTSBucket() throws Exception {
		List<CBPEntry> mockEntries = this.createMockCBPEntries();
		String entriesJson = json(mockEntries);

        when(
                this.trueUpBizComp.getAssociateHTSBucket(
                		 anyLong())
              ).thenReturn(mockEntries);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/bucket/htsassociate/{fiscalYear}", 2017)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, entriesJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#saveAssociateHTSBucket(java.util.List, long)}.
	 */
	@Test
	public void testSaveAssociateHTSBucket() throws Exception {
		List<CBPEntry> mockEntries = this.createMockCBPEntries();
		String entriesJson = json(mockEntries);

        when(
                this.trueUpBizComp.updateAssociateHTSBucket(
                		(List<CBPEntry>)notNull(), anyLong())
              ).thenReturn(mockEntries);

        MvcResult result = this.mockMvc
	        .perform(put("/api/v1/trueup/bucket/htsassociate/{fiscalYear}", 2017)
	        		.contentType(this.contentType)
	        		.content(entriesJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, entriesJson);
	}

	@Test
	public void testGetCBPExport() throws Exception {
		AssessmentExport export = new AssessmentExport();
		export.setTobaccoType("Pipe");
		String entriesJson = json(export);

        when(
                this.trueUpBizComp.getCBPExport(2017,3,"Pipe","1")
              ).thenReturn(export);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/export/fiscalyear/{year}/quarter/{quarter}/tobaccoClass/{tobaccoClass}/CompanyId/{CompanyId}", 2017, 3, "Pipe", 1)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, entriesJson);
	}
	
	@Test
	public void testUpdateCBPIngestedLineEntryInfo() throws Exception {
		CBPLineEntryUpdateInfo mockInfo = new CBPLineEntryUpdateInfo();
		mockInfo.setAssignedYear(2017L);
		String codeJson = json(mockInfo);

//        doNothing().when(
//                this.trueUpBizComp).updateCBPIngestedLineEntryInfo(
//                		(CBPLineEntryUpdateInfo)notNull());

        MvcResult result =  this.mockMvc
	        .perform(put("/api/v1/trueup/compare/updatecbpentry")
	        		.contentType(this.contentType)
	        		.content(codeJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

        String returnString = result.getResponse().getContentAsString();
        assertEquals(returnString, "Successfully Updated");
	}
	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getIncludeCompaniesInMktShBucket(long)}.
	 */
	@Test
	public void testGetIncludeCompaniesInMktShBucket() throws Exception {
		List<CompanyIngestion> mockEntries = this.createMockCompanyIngestion();
		String entriesJson = json(mockEntries);

        when(
                this.trueUpBizComp.getIncludeCompaniesInMktSh(
                		 anyLong())
              ).thenReturn(mockEntries);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/bucket/inclcmpymktsh/{fiscalYear}", 2017)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, entriesJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#saveIncludeCompaniesInMktShBucket(java.util.List, long)}.
	 */
	@Test
	public void testSaveIncludeCompaniesInMktShBucket() throws Exception {
		List<CompanyIngestion> mockEntries = this.createMockCompanyIngestion();
		String entriesJson = json(mockEntries);

        doNothing().when(
                this.trueUpBizComp)
                .updateIncludeCompaniesInMktSh(
                		(List<CompanyIngestion>)notNull(), anyLong());

        MvcResult result = this.mockMvc
	        .perform(put("/api/v1/trueup/bucket/inclcmpymktsh/{fiscalYear}", 2017)
	        		.contentType(this.contentType)
	        		.content(entriesJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, "OK");
	}


	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#getTTBPermits(long)}.
	 */
	@Test
	public void testGetTTBPermits() throws Exception {
		List<TTBPermitBucket> mockEntries = this.createMockTTBPermitBucket();
		String entriesJson = json(mockEntries);

        when(
                this.trueUpBizComp.getTTBPermits(
                		 anyLong())
              ).thenReturn(mockEntries);

        MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/bucket/ttbpermits/{fiscalYear}", 2017)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, entriesJson);
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.trueup.TrueUpRestController#saveTTBPermits(java.util.List)}.
	 */
	@SuppressWarnings("unchecked")
	@Test
	//@Ignore("Not Implemented Yet")
	public void testSaveTTBPermits() throws Exception {
		List<TTBPermitBucket> mockEntries = this.createMockTTBPermitBucket();
		String entriesJson = json(mockEntries);

        doNothing().when(
                this.trueUpBizComp)
                .updateTTBPermits(
                		(List<TTBPermitBucket>)notNull());

        MvcResult result = this.mockMvc
	        .perform(put("/api/v1/trueup/bucket/ttbpermits")
	        		.contentType(this.contentType)
	        		.content(entriesJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, "Updated the permits");
	}

	
	/**** Tests for  TTB,CBP, TTV ingestion ***/
	
	@Test
	public void testAnnualTrueUpTTBFileUpload() throws Exception {
		
		 doNothing().when(
	                this.trueUpEntityManager).stageIngestionData((List<TufaEntity>)notNull(), anyString());
		 doNothing().when(
	                this.trueUpEntityManager).loadTTB(anyLong());
		 doNothing().when(
	                this.trueUpEntityManager).uploadDocument(anyLong(),(TrueUpDocumentEntity)notNull());
		 
		 when(
	                this.fileIngestorFactory.getFileIngestor(
	                		 anyString())
	              ).thenReturn(new ExcelTTBFileIngestor());
		 
		 
//		 PowerMockito.mockStatic(SecurityContextUtil.class);
//		 PowerMockito.when(SecurityContextUtil.getPrincipal()).thenReturn(UserContext.create("testuser", null));

	

		InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2015_TTB_TEST.xlsx");
		
		
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "UAT2015_TTB.xlsx","multipart/*", fileBytes);
		
		MvcResult result =this.mockMvc.perform(MockMvcRequestBuilders.fileUpload("/api/v1/trueup/ingestFile").file(multiPartFile)
				.param("fileType", "TTB Tax")
				.param("fiscalYear", "2015"))
				
        .andDo(MockMvcResultHandlers.print()).andReturn();
		
		
        String returnString = result.getResponse().getContentAsString();
        
        TrueUpDocument tDoc = mapper.readValue(returnString, TrueUpDocument.class);
        
         assertEquals(tDoc.getErrorCount(), new Long(0));
		
	}
	
	
	@Test
	public void testgetexportNewCBPCompany() throws Exception {
		AssessmentExport mockEntries = this.createAssessmentExport();
		String entriesJson = json(mockEntries);
		
	      when(
	                this.trueUpBizComp.getCBPNewCompanyExport(
	                		 anyInt())
	              ).thenReturn(mockEntries);

	        MvcResult result = this.mockMvc
		        .perform(get("/api/v1/trueup/exportNewCBPCompany/fiscalyear/{year}", 2017)
		        		.contentType(this.contentType)
		        		)
		        .andExpect(status().isOk())
	            .andDo(MockMvcResultHandlers.print()).andReturn();

	         String returnString = result.getResponse().getContentAsString();
	         assertEquals(returnString, entriesJson);
	}
	
	
	@Test
	public void testAnnualTrueUpCBPIngestion() throws Exception {
		
		 doNothing().when(
	                this.trueUpEntityManager).stageIngestionData((List<TufaEntity>)notNull(), anyString());
		 doNothing().when(
	                this.trueUpEntityManager).loadTTB(anyLong());
		 doNothing().when(
	                this.trueUpEntityManager).uploadDocument(anyLong(),(TrueUpDocumentEntity)notNull());
		 
		 when(
	                this.fileIngestorFactory.getFileIngestor(
                		 anyString())
              ).thenReturn(new ExcelTTBFileIngestor());
		 
		
//		 PowerMockito.mockStatic(SecurityContextUtil.class);
//		 PowerMockito.when(SecurityContextUtil.getPrincipal()).thenReturn(UserContext.create("testuser", null));
		 
		InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2015_CBP_TEST.xlsx");
		
		
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2015_CBP_TEST.xlsx","multipart/*", fileBytes);
		
		MvcResult result =this.mockMvc.perform(MockMvcRequestBuilders.fileUpload("/api/v1/trueup/ingestFile").file(multiPartFile)
				.param("fileType", "CBP")
				.param("fiscalYear", "2015"))
				
        .andDo(MockMvcResultHandlers.print()).andReturn();
		
		
        String returnString = result.getResponse().getContentAsString();
        
        TrueUpDocument tDoc = mapper.readValue(returnString, TrueUpDocument.class);
        
        
        //
         assertEquals(tDoc.getErrorCount(), new Long(0));
		
	}

	@Test
	public void testAnnualTrueUpTTBVolIngestion() throws Exception {
		
		 doNothing().when(
	                this.trueUpEntityManager).stageIngestionData((List<TufaEntity>)notNull(), anyString());
		 doNothing().when(
	                this.trueUpEntityManager).loadTTB(anyLong());
		 doNothing().when(
	                this.trueUpEntityManager).uploadDocument(anyLong(),(TrueUpDocumentEntity)notNull());
		 
		 when(
	                this.fileIngestorFactory.getFileIngestor(
	                		 anyString())
	              ).thenReturn(new ExcelTTBFileIngestor());
		 
		
//		 PowerMockito.mockStatic(SecurityContextUtil.class);
//		 PowerMockito.when(SecurityContextUtil.getPrincipal()).thenReturn(UserContext.create("testuser", null));
		 
		InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("1981_TTB_VOL_TEST.xlsx");
		
		
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "1981_TTB_VOL_TEST.xlsx","multipart/*", fileBytes);
		
		MvcResult result =this.mockMvc.perform(MockMvcRequestBuilders.fileUpload("/api/v1/trueup/ingestFile").file(multiPartFile)
				.param("fileType", "TTB Volume")
				.param("fiscalYear", "1981"))
				
        .andDo(MockMvcResultHandlers.print()).andReturn();
		
		
        String returnString = result.getResponse().getContentAsString();
        
        TrueUpDocument tDoc = mapper.readValue(returnString, TrueUpDocument.class);
        
         assertEquals(tDoc.getErrorCount(), new Long(0));
		
	}



  
	
	
	
	@Test
	public void testdeleteIngestionFile() throws Exception {

		
		 doNothing().when(this.trueUpBizComp).deleteIngestionFile(
				 anyLong(),anyString()
       );

        MvcResult result = this.mockMvc
	        .perform(delete("/api/v1/trueup/ingestfile/{doc}/{yr}", "test.pdf", 2017)
	        		.contentType(this.contentType)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, "Successfully Deleted");
	}

	
	@SuppressWarnings("unchecked")
	@Test
	public void testgetTTBAmendments() throws Exception {
		List<TTBAmendment> mockEntries = this.createTTBAmendments();
		String entriesJson = json(mockEntries);

        when(
                this.trueUpBizComp.getTTBAmendments(
                		 anyLong(),anyLong(),(List<TTBAmendment>)any())
              ).thenReturn(mockEntries);

        MvcResult result = this.mockMvc
	        .perform(post("/api/v1/trueup/compare/fiscalYr/{fiscalYr}/company/{companyId}/ttbamendments", 2017,2)
	        		.contentType(this.contentType)
	        		.content(entriesJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, entriesJson);
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void testsaveTTBAmendments() throws Exception {
		List<TTBAmendment> mockEntries = this.createTTBAmendments();
		String entriesJson = json(mockEntries);

        when(
                this.trueUpBizComp.saveTTBAmendment(
                		 (List<TTBAmendment>)any())
              ).thenReturn(mockEntries);

        MvcResult result = this.mockMvc
	        .perform(put("/api/v1/trueup/compare/ttbamendments")
	        		.contentType(this.contentType)
	        		.content(entriesJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, entriesJson);
	}
	
	
	@Test
	public void testCompareComments() throws Exception {
		List<TTBAmendment> mockEntries = this.createTTBAmendments();
		String entriesJson = json(mockEntries);

        when(
                this.trueUpBizComp.saveTTBCompareComments(
                		 (List<TTBAmendment>)any())
              ).thenReturn(mockEntries);

        MvcResult result = this.mockMvc
	        .perform(post("/api/v1/trueup/compare/comments")
	        		.contentType(this.contentType)
	        		.content(entriesJson)
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, entriesJson);
	}
	
	
	@Test
	public void testgenerateAnnualMarketShare() throws Exception {
			
		TrueUp mockEntries = this.CreateTrueUp();
		String entriesJson = json(mockEntries);
	
		MvcResult result = this.mockMvc
	        .perform(put("/api/v1/trueup/marketshare/{fiscalYear}", 2017)
	        		.contentType(this.contentType)
	        		
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, "");
	}
	
	@Test
	public void testsubmitAnnualMarketShare() throws Exception {
			
		TrueUp mockEntries = this.CreateTrueUp();
		String entriesJson = json(mockEntries);
	
		MvcResult result = this.mockMvc
	        .perform(put("/api/v1/trueup/marketshare/submit/{fiscalYear}", 2017)
	        		.contentType(this.contentType)
	        		
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, "");
	}
	
	@Test
	@Ignore("Not Implemented Yet")
	public void testgetFDA3852flag() throws Exception {
				
		Map<String, String> mockEntries = new HashMap<>();
		
	         when(
	                this.trueUpBizComp.fetchFDA352IMP(
	                		 anyLong(), anyLong(), anyString(),anyString())
	              ).thenReturn(mockEntries);
	
		MvcResult result = this.mockMvc
	        .perform(get("/api/v1/trueup/{fiscalYr}/{company_id}/{classNm}/{report_type}/{ein}", 2017,50, "test", "IMP","123456789")
	        		.contentType(this.contentType)	       		
	        		)
	        .andExpect(status().isOk())
            .andDo(MockMvcResultHandlers.print()).andReturn();

         String returnString = result.getResponse().getContentAsString();
         assertEquals(returnString, "");
	}
	

	
	/*************************************************************
     * MOCK OBJECTS
    *************************************************************/

	private TrueUp CreateTrueUp() {
		
		TrueUp trueup = new TrueUp();
		trueup.setFiscalYear(1L);
		return trueup;
	}
		
	private List<TTBAmendment> createTTBAmendments() {
		
		List<TTBAmendment> list = new ArrayList<>();
		TTBAmendment ttbamen = new TTBAmendment();
		ttbamen.setFiscalYr("2014");
		ttbamen.setQtr("2");
		ttbamen.setTobaccoClassId(1L);
		ttbamen.setTtbCompanyId(1L);
		list.add(ttbamen);
		return list;
	
	}
		
	
	private List<TTBPermitBucket> createTTBPermitBucket() {
		
		List<TTBPermitBucket> list = new ArrayList<>();
		TTBPermitBucket ttbperm = new TTBPermitBucket();
		ttbperm.setCompanyId(12);
		ttbperm.setCompanyNm("Sheilas Company");
		ttbperm.setEin("12345678");
		ttbperm.setIncludeFlag("true");
		ttbperm.setPermitId(0);
		ttbperm.setPermitNum("AE-IO-1234");
		ttbperm.setTobaccoClasses("Pipe");
		ttbperm.setTotalTaxes(34);
		ttbperm.setTpbdDt(new Date());
		list.add(ttbperm);
		return list ; 
		
	}
	
	
	private AssessmentExport createAssessmentExport() {
		
		AssessmentExport ae = new AssessmentExport();
		ae.setCsv("csv test");
		ae.setTitle("title test");
	    ae.setTobaccoType("Cigars");
	    return ae;
	}
	
    private List<TTBPermitBucket> createMockTTBPermitBucket() {
		List<TTBPermitBucket> list = new ArrayList<>();
		TTBPermitBucket entry = new TTBPermitBucket();
		list.add(entry);
		return list;
	}

	private List<CompanyIngestion> createMockCompanyIngestion() {
		List<CompanyIngestion> list = new ArrayList<>();
		CompanyIngestion entry = new CompanyIngestion();
		list.add(entry);
		return list;
	}

	private List<CBPEntry> createMockCBPEntries() {
		List<CBPEntry> list = new ArrayList<>();
		CBPEntry entry = new CBPEntry();
		list.add(entry);
		return list;
	}

	private List<TrueUpComparisonManufacturerDetail> createMockTrueUpComparisonManufacturerDetail() {
		List<TrueUpComparisonManufacturerDetail> list = new ArrayList<>();
		TrueUpComparisonManufacturerDetail tcs = new TrueUpComparisonManufacturerDetail();
		list.add(tcs);
		return list;
	}

	private List<TrueUpComparisonImporterDetail> createMockTrueUpComparisonImporterDetail() {
		List<TrueUpComparisonImporterDetail> list = new ArrayList<>();
		TrueUpComparisonImporterDetail tcs = new TrueUpComparisonImporterDetail();
		list.add(tcs);
		return list;
	}

	private List<TrueUpComparisonSummary> createMockTrueUpComparisonSummary() {
		List<TrueUpComparisonSummary> list = new ArrayList<>();
		TrueUpComparisonSummary tcs = new TrueUpComparisonSummary();
		list.add(tcs);
		return list;
	}

	private List<TrueUpComparisonResults> createMockTrueupComparisonResults() {
		List<TrueUpComparisonResults> results = new ArrayList<>();
		TrueUpComparisonResults tcr = new TrueUpComparisonResults();
		tcr.setAbstotalDeltaTax(0.0);
		tcr.setCompanyId(1L);
		tcr.setLegalName("test");
		results.add(tcr);
		return results;
	}
	
	
    private List<TrueUpComparisonImporterIngestionDetail> createMockTrueUpImporterIngestionDetail() {
		
    	List<TrueUpComparisonImporterIngestionDetail> results = new ArrayList<>();
    	TrueUpComparisonImporterIngestionDetail tcr = new TrueUpComparisonImporterIngestionDetail();
		tcr.setPeriod("jan");
		tcr.setPeriodEntryDateCategory("05/05/2017");	  
		tcr.setPeriodEntryItemCount(1);
		tcr.setPeriodTotal("22.00");
		results.add(tcr);
		return results;
	
	}
	
/*	private TrueUpCompareSearchCriteria   createMockTrueUpcomparesearchcriteria()
	{
		TrueUpCompareSearchCriteria  csc = new TrueUpCompareSearchCriteria();
		csc.setCompanyFilter("CompanyTest");
		csc.setTobaccoClassFilter("Cigars");
		csc.setDeltaTaxFilter("test123");
		csc.setPermitTypeFilter("ACTV");
		csc.setQuarterFilter("2");
		csc.setStatusTypeFilter.add("Test");
		
		return csc;
		
	}*/

	private List<UnknownCompanyBucket> createMockUnknownCompanyBucket() {
		List<UnknownCompanyBucket> list = new ArrayList<>();
		UnknownCompanyBucket ucb = new UnknownCompanyBucket();
		ucb.setCbpImporterId(1L);
		ucb.setConsigneeEin("");
		list.add(ucb);
		return list;
	}

	private List<CBPImporter> createMockCBPImporterBucket() {
		List<CBPImporter> bucket = new ArrayList<>();
		CBPImporter imp = this.createMockCBPImporter();
		bucket.add(imp);
		return bucket;
	}

	private CBPImporter createMockCBPImporter() {
		CBPImporter imp = new CBPImporter();
		imp.setAssociationTypeCd("test");
		imp.setCbpImporterId(1L);
		imp.setConsigneeEIN("test");
//		imp.setConsigneeExistsFlag("flag");
		imp.setConsigneeNm("test");
//		imp.setExistsInTufaFlag("existsflag");
//		imp.setFiscalYear(2017L);
		imp.setImporterEIN("EIN");
		imp.setImporterNm("Importer Name");
		return imp;
	}

	private TrueUpErrorsExport createMockTrueUpErrorsExport() {
		TrueUpErrorsExport exp = new TrueUpErrorsExport();
		exp.setCsv("a,b,c");
		exp.setFilename("test.csv");
		return exp;
	}

	private List<HTSCodeEntity> createMockHTSCodeEntity() {
		List<HTSCodeEntity> codes= new ArrayList<>();
		HTSCodeEntity code = this.createMockHTSCode();
		codes.add(code);
		return codes;
	}

	private HTSCodeEntity createMockHTSCode() {
		HTSCodeEntity ce = new HTSCodeEntity();
		ce.setHtsCode("test");
		ce.setHtsCodeId(1L);
		ce.setHtsCodeNotes("test notes");
		TobaccoClassEntity tobaccoClass = new TobaccoClassEntity();
		ce.setTobaccoClass(tobaccoClass);
		ce.setTobaccoClassId(1L);
		return ce;
	}

	private List<TrueUp> createTrueUpList() {
		List<TrueUp> tulist = new ArrayList<>();
		TrueUp tu = this.createMockTrueUp();
		tulist.add(tu);
		return tulist;
	}

	private List<TrueUpComparisonSummary> createTrueUpCompareList() {
		List<TrueUpComparisonSummary> tulist = new ArrayList<>();
		TrueUpComparisonSummary tu = this.createMockTrueUpCompare();
		tulist.add(tu);
		return tulist;
	}
	
	private List<TrueUpComparisonImporterIngestionDetail> createImporterIngestion() {
		List<TrueUpComparisonImporterIngestionDetail> tulist = new ArrayList<>();
		TrueUpComparisonImporterIngestionDetail tu = this.createMockTrueUpIngestion();
		tulist.add(tu);
		return tulist;
	}
	
	private TrueUpComparisonSummary createMockTrueUpCompare()
	{
		
		TrueUpComparisonSummary tucomp = new TrueUpComparisonSummary();
		List<String> cval = new ArrayList<>();
		cval.add("Tobacco Class");
		cval.add("Pipe");
		cval.add("Pipe");
		cval.add("Pipe");
		tucomp.setNumColumns(4);
		tucomp.setCellValues(cval);	
		return tucomp;
	}
	
	private TrueUpComparisonImporterIngestionDetail createMockTrueUpIngestion() {
		TrueUpComparisonImporterIngestionDetail tu = new TrueUpComparisonImporterIngestionDetail();
		tu.setPeriodTotal("3000");
		tu.setPeriodEntryItemCount(12L);
		return tu;
		
	}
	
	
	
	private TrueUpFilter createMockTrueUpFilter() {
		TrueUpFilter tuf = new TrueUpFilter();
		Set<Long> fiscalYrFilters = new HashSet<>();
		fiscalYrFilters.add(2016L);
		Set<String> statusFilters = new HashSet<>();
		statusFilters.add("Submitted");
		tuf.setFiscalYrFilters(fiscalYrFilters);
		tuf.setStatusFilters(statusFilters);
		return tuf;
	}

	private TrueUpEntity createMockTrueUpEntity() {
		TrueUpEntity te = new TrueUpEntity();
		te.setFiscalYear(2016L);
		te.setSubmittedDt(new Date());
		Set<TrueUpDocumentEntity> documents = new HashSet<>();
		TrueUpDocumentEntity tde = this.createMockTrueUpDocumentEntity();
		documents.add(tde);
		te.setDocuments(documents);
		return te;
	}

	private List<TrueUpDocumentEntity> createMockTrueUpDocumentEntities() {
		List<TrueUpDocumentEntity> docs = new ArrayList<>();
		TrueUpDocumentEntity tde = this.createMockTrueUpDocumentEntity();
		docs.add(tde);
		return docs;
	}

	private List<TrueUpDocument> createMockTrueUpDocuments() {
		List<TrueUpDocument> docs = new ArrayList<>();
		TrueUpDocument td = this.createMockTrueUpDocument();
		docs.add(td);
		return docs;
	}

	private List<TrueUpComparisonImporterIngestionHTSCodeSummary> createMockTrueUpImporterIngestionHTSCodeSummarys() {
		List<TrueUpComparisonImporterIngestionHTSCodeSummary> docs = new ArrayList<>();
		TrueUpComparisonImporterIngestionHTSCodeSummary  ihts = new TrueUpComparisonImporterIngestionHTSCodeSummary();
		ihts.setHtsCode("123456786");
		ihts.setLineCount(2);
		ihts.setTaxAmount(22.36);
		ihts.setTobaccoClass("PIPE");	
		docs.add(ihts);
		return docs;
	}

	
	private TrueUpDocument createMockTrueUpDocument() {
		TrueUpDocument td = new TrueUpDocument();
		td.setVersionNum(1L);
		td.setDocDesc("test description");
		td.setErrorCount(0L);
		td.setFiscalYr(2016L);
		td.setTrueUpFilename("testfilename");
		td.setCreatedDt("");
		td.setCreatedBy("");
		td.setModifiedBy("");
		td.setModifiedDt("");
		return td;
	}

	private TrueUp createMockTrueUp() {
		TrueUp tu = new TrueUp();
		tu.setFiscalYear(2016L);
		tu.setSubmittedDt(new Date());
		tu.setSubmittedInd("Not Submitted");
		return tu;
	}

	private TrueUpSubDocumentEntity createMockTrueUpSubDocumentEntity() {
		TrueUpSubDocumentEntity tsde = new TrueUpSubDocumentEntity();
		tsde.setAssessmentPdf("testpdf");
		tsde.setAuthor("test author");
		tsde.setDateUploaded(new Date());
		tsde.setDescription("test description");
		tsde.setDocumentNumber(1L);
		tsde.setFilename("test filename");
		tsde.setFiscalYr(2017L);
		tsde.setVersionNum(1);
		return tsde;
	}

	private List<TrueUpSubDocumentEntity> createMockTrueUpSubDocumentEntiies() {
		List<TrueUpSubDocumentEntity> docs = new ArrayList<>();
		TrueUpSubDocumentEntity tde = this.createMockTrueUpSubDocumentEntity();
		docs.add(tde);
		return docs;
	}


	private TrueUpDocumentEntity createMockTrueUpDocumentEntity() {
		TrueUpDocumentEntity tde = new TrueUpDocumentEntity();
		tde.setDocDesc("test description");
		tde.setErrorCount(0L);
		tde.setTrueUpDocId(1L);
		tde.setTrueUpFilename("testfilename");
		tde.setCreatedDt(new Date());
		tde.setCreatedBy("");
		tde.setModifiedBy("");
		tde.setModifiedDt(new Date());
		return tde;
	}





   /************************************************************
    *  JSONIZER
    ************************************************************/
	@SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
     MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
     this.mappingJackson2HttpMessageConverter.write(
             o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
     return mockHttpOutputMessage.getBodyAsString();
 }

}

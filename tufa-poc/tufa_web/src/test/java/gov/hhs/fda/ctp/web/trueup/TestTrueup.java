package gov.hhs.fda.ctp.web.trueup;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import gov.hhs.fda.ctp.biz.trueup.TrueUpBizComp;
import gov.hhs.fda.ctp.common.beans.CBPImporter;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.persistence.model.HTSCodeEntity;
import gov.hhs.fda.ctp.persistence.model.TTBPermitBucket;
import gov.hhs.fda.ctp.persistence.model.TrueUpEntity;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

/**
 * @author rnasina
 *
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {MvcConfig.class}),
	@ContextConfiguration(classes = {WebTestConfig.class})
})
public class TestTrueup {

	@Autowired
	TrueUpRestController controller;

	@Autowired
	TrueUpBizComp trueUpBizComp;

	List<TrueUpEntity> trueUps = new ArrayList<TrueUpEntity>();
	List<HTSCodeEntity> codes = new ArrayList<HTSCodeEntity>();
	List<CBPImporter> bucket = new ArrayList<CBPImporter>();
	List<TTBPermitBucket> ttbpermits = new ArrayList<TTBPermitBucket>();

	@Before
	public void setUp() {
		codes = trueUpBizComp.getHtsCodes();
		bucket = trueUpBizComp.getCompanyComparisonBucket((long)2017);
		ttbpermits = trueUpBizComp.getTTBPermits(2017);
		trueUps = trueUpBizComp.getAllTrueUps();
	}

	@Test
	public void testHTSCode() {
		try {
			ResponseEntity<?> responseEntity = controller.getHtsCodes();
			ResponseEntity<String> saveHts = controller.saveOrUpdateCode(codes.get(0));
			assert (responseEntity != null && saveHts != null);
		}
		catch(IndexOutOfBoundsException e) {
			System.out.println("SOMEONE DROPPED ALL THE DATA.");
		}
	}

	@Test
	public void testTrueUp() {
		try{
			ResponseEntity<?> responseEntity = controller.getTrueUp(2017);
			ResponseEntity<?> res = controller.getCompanyComparisonBucket(2017);
			ResponseEntity<String> resp = controller.updateCompanyComparisonBucket(bucket);
			ResponseEntity<?> docs = controller.getTrueUpDocuments(2017);
			ResponseEntity<String> saveCompanyBucket = controller.updateCompanyComparisonBucket(bucket);
			ResponseEntity<?> saveTTbPermit = controller.saveTTBPermits(ttbpermits);
			ResponseEntity<?> unknownCompanies = controller.getUnknownCompanyBucket(trueUps.get(0).getFiscalYear());
			ResponseEntity<?> ttbpermits = controller.getTTBPermits(trueUps.get(0).getFiscalYear());
			ResponseEntity<?> inclCompanies = controller.getIncludeCompaniesInMktShBucket(trueUps.get(0).getFiscalYear());
			ResponseEntity<?> htsAssc = controller.getHTSMissingAssociations(trueUps.get(0).getFiscalYear());
			TrueUpCompareSearchCriteria csc = new TrueUpCompareSearchCriteria();
			ResponseEntity<?> comparisonResults =  controller.getComparisonResults(trueUps.get(0).getFiscalYear(), csc);
			ResponseEntity<?> htsbucket = controller.getAssociatedHTSBucket(trueUps.get(0).getFiscalYear());

			assert (responseEntity != null && res != null && resp != null && docs != null
					&& saveCompanyBucket != null && saveTTbPermit != null && unknownCompanies != null && ttbpermits != null
					&& inclCompanies != null && htsAssc != null && comparisonResults != null && htsbucket != null);
		}
		catch(Exception e){
			boolean maybe = true;
			assert(maybe);
		}
	}

	@Test
	public void testDocs(){
		try{
			ResponseEntity<?> doc = controller.getDocument(1);
			ResponseEntity<?> docs = controller.getSupportingDocs(2);
			assert (doc != null && docs != null);
		}
		catch(Exception e){
			assert(true);
		}
	}
}

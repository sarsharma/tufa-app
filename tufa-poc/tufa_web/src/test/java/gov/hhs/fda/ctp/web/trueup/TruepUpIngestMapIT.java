package gov.hhs.fda.ctp.web.trueup;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;

import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.TrueUpCompareSearchCriteria;
import gov.hhs.fda.ctp.common.beans.TrueUpComparisonResults;

import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import gov.hhs.fda.ctp.common.beans.AssessmentExport;
import gov.hhs.fda.ctp.common.beans.CBPEntry;
import gov.hhs.fda.ctp.common.beans.CBPImporter;
import gov.hhs.fda.ctp.common.beans.CompanyIngestion;
import gov.hhs.fda.ctp.common.beans.TobaccoClass;
import gov.hhs.fda.ctp.common.beans.TrueUpDocument;
import gov.hhs.fda.ctp.common.beans.UnknownCompanyBucket;
import gov.hhs.fda.ctp.hibernate.PersistenceHibernateTestConfig;
import gov.hhs.fda.ctp.persistence.model.TTBPermitBucket;
import gov.hhs.fda.ctp.persistence.model.HTSCodeEntity;
import gov.hhs.fda.ctp.web.companymgmt.CompanyMgmtRestController;
import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

@ActiveProfiles({"persistenceingestmaptest"})
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class}),
	@ContextConfiguration(classes = {PersistenceHibernateTestConfig.class})
})
public class TruepUpIngestMapIT {
	
	@Autowired
	TrueUpRestController controller;
	
	
    private MockMvc mockMvc;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;
    
	private ObjectMapper mapper = new ObjectMapper();


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));


    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

         List<HttpMessageConverter<?>>  converterList = Arrays.asList(converters);

         for(HttpMessageConverter<?> converter: converterList){
             if(converter instanceof MappingJackson2HttpMessageConverter)
                 this.mappingJackson2HttpMessageConverter = converter;
         }

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
    }
    
    @After
    public void teardown() {
    		
        
    }
    
    @Test
	public void testAnnualTrueUpTTBFileUpload() throws Exception {
		InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2015_TTB_TEST.xlsx");		
		
		this.controller.deleteIngestionFile("CBP", 2015);
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "UAT2015_TTB.xlsx","multipart/*", fileBytes);
		
		ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "TTB Tax", "2015");
		assert(res!=null);
		
		/****  Call on Unknown buckets ***/
        List<UnknownCompanyBucket> unknwBucks=  (List<UnknownCompanyBucket>) this.controller.getUnknownCompanyBucket(2015).getBody();
        
        /**** Call on new companies buckets ****/
        List<CompanyIngestion> newCompaniesBucks = (List<CompanyIngestion>) this.controller.getIncludeCompaniesInMktShBucket(2015L).getBody();
        
        /**** Call on new ttb permits buckets ****/
        List<TTBPermitBucket> newPermitsBucks = (List<TTBPermitBucket>) this.controller.getTTBPermits(2015L).getBody();
        
        assert(newCompaniesBucks != null || newPermitsBucks != null);
        
		this.controller.deleteIngestionFile("TTB Tax", 2015L);		
	}
    
    @Test
    public void deleteIngestedFile() throws Exception {
		this.controller.deleteIngestionFile("CBP", 2018);
		this.controller.deleteIngestionFile("CBP", 2016);
        this.controller.deleteIngestionFile("TTB Volume", 2015);

    }
    

 // CTPTUFA-3339-Include Buckets and Unknown Buckets 
    @Test
	public void testAnnualTrueIncludeandUnkownBuckets() throws Exception {
	
    	
        /**** Ingest file with test case conditions ****/
    	
		InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2018_CBP_Known HTS Codes and Mix scenarios_TEST.xlsx");
				
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2018_CBP_Known HTS Codes and Mix scenarios_TEST.xlsx","multipart/*", fileBytes);
		
        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "CBP", "2018");
        assert(res!=null);
    
        /****  Call on Unknown buckets ***/
        ResponseEntity bucket  =  this.controller.getUnknownCompanyBucket(2018);
        List<UnknownCompanyBucket> unknwBucks=  (List<UnknownCompanyBucket>) bucket.getBody();
        
        /****  Call for newly reported ***/
        ResponseEntity newlyreported  =  this.controller.getIncludeCompaniesInMktShBucket(2018);
        List<CompanyIngestion> newcompany = (List<CompanyIngestion>) newlyreported.getBody();
        
        /**** Assert or verify  UnknownBucket according to your testcases as per expected results  *****/
        Assert.assertNotNull (unknwBucks);
		assertThat(unknwBucks.size(), is(0));
		Assert.assertNotNull (newcompany);
		assertThat(newcompany.size(), is(1));
		Assert.assertEquals("Shanaya  new Testing", newcompany.get(0).getCompanyName());
    
        /** Delete the ingested file after the test **/
        this.controller.deleteIngestionFile("CBP", 2018);
        
	}
    
    @Test
	public void testCBP3337Scenarios() throws Exception {
	
    	this.controller.deleteIngestionFile("CBP", 2016);
        /**** Ingest file with test case conditions ****/
    	this.controller.deleteIngestionFile("CBP", 2016);
		InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2016_CBP_3337Scenarios.xlsx");
		
		
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2016_CBP_3337Scenarios.xlsx","multipart/*", fileBytes);
		
        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "CBP", "2016");
        assert(res!=null);

       
        /****  Call on Unknown buckets ***/
        List<UnknownCompanyBucket> unknwBucks=  (List<UnknownCompanyBucket>) this.controller.getUnknownCompanyBucket(2016).getBody();
      
		List<CBPEntry> lstmissinghts =  (List<CBPEntry>) this.controller.getHTSMissingAssociations(2016).getBody();
		TobaccoClass tobaccoClass = new TobaccoClass();
		tobaccoClass.setTobaccoClassNm("Roll-Your-Own");
		for(CBPEntry entry: lstmissinghts)
			entry.setTobaccoClass(tobaccoClass);
		List<CBPEntry> resultlstmissinghts = (List<CBPEntry>) this.controller.saveHTSMissingAssociations(lstmissinghts, 2016).getBody();
		List<TrueUpDocument> docs = (List<TrueUpDocument>) this.controller.getTrueUpDocuments(2016).getBody();
		List<CompanyIngestion> cmpaniesToIncl = (List<CompanyIngestion>) this.controller.getIncludeCompaniesInMktShBucket(2016).getBody();
		
		List<CBPEntry> lsthts =  (List<CBPEntry>) this.controller.getAssociatedHTSBucket(2016).getBody();
        
        /**** Assert or verify  UnknownBucket according to your testcases as per expected results  *****/
		assertThat(unknwBucks.size(), is(1));
		assertThat(unknwBucks.get(0).getImporterName(), is("Mango LLC"));
		assertThat(lstmissinghts.size(), is(1));
		assertThat(resultlstmissinghts.size(), is(2));
		assertThat(resultlstmissinghts.get(0).getTobaccoClassId(), is(6L));
		assertThat(lsthts.size(), is(1));
		assertThat(docs.get(0).getTrueUpFilename(), is("2016_CBP_3337Scenarios.xlsx"));
		assertThat(cmpaniesToIncl.get(0).getCompanyName(), is("1 WHOLESALE LLC"));
		/** Delete the ingested file after the test **/
        this.controller.deleteIngestionFile("CBP", 2016);
	}
    
    // CTPTUFA-3340-E2E Code Coverage - Known HTS Codes and Mix scenarios
    @Test
	public void testCBP3340Scenarios() throws Exception {
    	this.controller.deleteIngestionFile("CBP", 2016);
        /**** Ingest file with test case conditions ****/
    	
		InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2016_CBP_3340Scenarios.xlsx");
		
		
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2016_CBP_3340Scenarios.xlsx","multipart/*", fileBytes);
		
        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "CBP", "2016");
        assert(res!=null);

       
        /****  Call on Unknown buckets ***/
        List<UnknownCompanyBucket> unknwBucks=  (List<UnknownCompanyBucket>) this.controller.getUnknownCompanyBucket(2016).getBody();
        
        AssessmentExport cbpexport = (AssessmentExport) this.controller.getCBPExport(2016, 3, "Cigars", "1").getBody();
        
        AssessmentExport newCbp = (AssessmentExport) this.controller.getexportNewCBPCompany(2016).getBody();
      
		//List<CBPEntry> lstmissinghts=  (List<CBPEntry>) this.controller.getHTSMissingAssociations(2016).getBody();
		
		List<CBPEntry> lsthts = (List<CBPEntry>) this.controller.getAssociatedHTSBucket(2016).getBody();
		
		TobaccoClass tc = new TobaccoClass();
		tc.setTobaccoClassNm("Cigar");
		tc.setClassTypeCd("TYPE");
		tc.setTobaccoClassId(8L);
		tc.setThresholdValue(500d);
		
		lsthts.get(0).setTobaccoClass(tc);
		
		this.controller.saveAssociateHTSBucket(lsthts, 2016);
		lsthts = (List<CBPEntry>) this.controller.getAssociatedHTSBucket(2016).getBody();
		
		List<CBPImporter> comparisonBuck = (List<CBPImporter>) this.controller.getCompanyComparisonBucket(2016).getBody();
		
		comparisonBuck.get(0).setProvidedAnswer(true);
		comparisonBuck.get(0).setAssociationTypeCd("IMPT");
		this.controller.updateCompanyComparisonBucket(comparisonBuck);
		
		comparisonBuck = (List<CBPImporter>) this.controller.getCompanyComparisonBucket(2016).getBody();
		
		List<CompanyIngestion> cmpaniesToIncl = (List<CompanyIngestion>) this.controller.getIncludeCompaniesInMktShBucket(2016).getBody();
        
        /**** Assert or verify  UnknownBucket according to your testcases as per expected results  *****/
		assertThat(unknwBucks.size(), is(1));
		assertThat(unknwBucks.get(0).getImporterName(), is("Mango LLC"));
		assertThat(lsthts.size(), is(1));
		assertThat(comparisonBuck.size(), is(3));
		assertThat(comparisonBuck.get(0).getImporterNm(), is("CrabTree LLC"));
		assertThat(cmpaniesToIncl.size(), is(1));
		assertThat(comparisonBuck.get(0).getAssociationTypeCd(), is("IMPT"));
		//assertThat(lsthts.get(0).getTobaccoClass().getTobaccoClassNm(), is("Cigar"));
		Assert.assertNotNull (cbpexport);
		Assert.assertNotNull (newCbp);
		/** Delete the ingested file after the test **/
        this.controller.deleteIngestionFile("CBP", 2016);
	}

    
  
    
    // CTPTUFA-3338-E2E Code Coverage - Known HTS Codes and Mix scenar
    @Test
	public void testAnnualTrueUPCBPKnownHTSCodesandMixscenario() throws Exception {
	
        /**** Ingest file with test case conditions ****/
    	
		InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2018_CBP_Known HTS Codes and Mix scenarios_TEST.xlsx");		
		
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2018_CBP_Known HTS Codes and Mix scenarios_TEST.xlsx","multipart/*", fileBytes);
		
        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "CBP", "2018");
        Assert.assertNotNull(res);
       
        /****  Call on Unknown buckets ***/
        ResponseEntity bucket  =  this.controller.getUnknownCompanyBucket(2015);
        List<UnknownCompanyBucket> unknwBucks=  (List<UnknownCompanyBucket>) bucket.getBody();
      
	
		ResponseEntity missingHTS =  this.controller.getHTSMissingAssociations(2015);
		List<CBPEntry> lstmisisnghts=  (List<CBPEntry>) missingHTS.getBody();
		
		ResponseEntity hts = this.controller.getAssociatedHTSBucket(2015);
		List<CBPEntry> lsthts=  (List<CBPEntry>) missingHTS.getBody();
        
        /**** Assert or verify  UnknownBucket according to your testcases as per expected results  *****/
		assertThat(unknwBucks.size(), is(0));
        assertThat(lstmisisnghts.size(), is(0));
        assertThat(lsthts.size(), is(0));
    
        /** Delete the ingested file after the test **/
        this.controller.deleteIngestionFile("CBP", 2018);
        
	}

    /************** Test TrueUp Errors *******************/
    @Test
    public void getTrueupErrorsCBP() throws Exception{
    	
    	InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2018_CBP_ERRORS.xlsx");
				
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2018_CBP_ERRORS.xlsx","multipart/*", fileBytes);
		
        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "CBP", "2018");
        Assert.assertNotNull(res);
        
    	MvcResult result = this.mockMvc.perform(
				get("/api/v1/trueup/{fiscalYear}/errors/{type}",2018,"CBP")
				.contentType(this.contentType))
				.andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();

		 String returnString = result.getResponse().getContentAsString();
		 Assert.assertNotNull(returnString);
		 
		 /** Delete the ingested file after the test **/
        this.controller.deleteIngestionFile("CBP", 2018);
    }
    
    @Test
    public void getTrueupErrorsTTB() throws Exception{
    	
    	InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2015_TTB_ERRORS.xlsx");
				
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2015_TTB_ERRORS.xlsx","multipart/*", fileBytes);
		
        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "TTB", "2015");
        Assert.assertNotNull(res);
        
    	MvcResult result = this.mockMvc.perform(
				get("/api/v1/trueup/{fiscalYear}/errors/{type}",2015,"TTB Tax")
				.contentType(this.contentType))
				.andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();

		 String returnString = result.getResponse().getContentAsString();
		 Assert.assertNotNull(returnString);
		 
		 /** Delete the ingested file after the test **/
        this.controller.deleteIngestionFile("TTB", 2015);
    }
    
    @Test
    public void getTrueupErrorsTTBVolume() throws Exception{
    	
    	InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2015_TTBv_ERRORS.xlsx");
				
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2015_TTBv_ERRORS.xlsx","multipart/*", fileBytes);
		
        ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "TTB Volume", "2015");
        Assert.assertNotNull(res);
        
    	MvcResult result = this.mockMvc.perform(
				get("/api/v1/trueup/{fiscalYear}/errors/{type}",2015,"TTB Volume")
				.contentType(this.contentType))
				.andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();

		 String returnString = result.getResponse().getContentAsString();
		 Assert.assertNotNull(returnString);
		 
		 /** Delete the ingested file after the test **/
        this.controller.deleteIngestionFile("TTB Volume", 2015);
    }
    /******* TTB volume Testcases *************/
    
    
    @Test
	public void ingestTTBVolumeFile() throws Exception {
	
        /**** Ingest file with test case conditions ****/
    	
		InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2015_TTBv_TEST.xlsx");		
		
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2015_TTBv_TEST.xlsx","multipart/*", fileBytes);
		
		MvcResult result =this.mockMvc.perform(MockMvcRequestBuilders.fileUpload("/api/v1/trueup/ingestFile").file(multiPartFile)
				.param("fileType", "TTB Volume")
				.param("fiscalYear", "2015"))
				
        .andDo(MockMvcResultHandlers.print()).andReturn();
		
		
        String returnString = result.getResponse().getContentAsString();
        
        Assert.assertNotNull(returnString);
       
        /** Delete the ingested file after the test **/
        this.controller.deleteIngestionFile("TTB Volume", 2015);
        
	}
    
    @Test
    public void exportTTBerrors() throws Exception {    	

        this.controller.deleteIngestionFile("TTB Volume", 2015); 
        this.controller.deleteIngestionFile("TTB Tax", 2015); 
        
    	/**** Ingest file with test case conditions ****/
    	
		InputStream inputStream = this.getClass().getClassLoader()
			    .getResourceAsStream("2015_TTBv_ERRORS.xlsx");		
		
		byte[] fileBytes = IOUtils.toByteArray(inputStream);
		
		MockMultipartFile multiPartFile = new MockMultipartFile("trueupDocument", "2015_TTBv_ERRORS.xlsx","multipart/*", fileBytes);
		
		ResponseEntity res = this.controller.ingestFileUpload(multiPartFile, "TTB Volume", "2015");
        Assert.assertNotNull(res);
        
        InputStream inputStreamTTB = this.getClass().getClassLoader()
			    .getResourceAsStream("2015_TTB_ERRORS.xlsx");		
		
		byte[] fileBytesTTB = IOUtils.toByteArray(inputStreamTTB);
		
		MockMultipartFile multiPartFileTTB = new MockMultipartFile("trueupDocument", "2015_TTB_ERRORS.xlsx","multipart/*", fileBytesTTB);
		
		ResponseEntity resTTB = this.controller.ingestFileUpload(multiPartFileTTB, "TTB Tax", "2015");
        Assert.assertNotNull(resTTB);
       
        /** Delete the ingested file after the test **/
        this.controller.deleteIngestionFile("TTB Volume", 2015); 
        this.controller.deleteIngestionFile("TTB Tax", 2015); 
    }
}

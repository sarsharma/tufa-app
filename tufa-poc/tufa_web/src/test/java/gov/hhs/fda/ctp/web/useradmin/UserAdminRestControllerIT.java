package gov.hhs.fda.ctp.web.useradmin;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import gov.hhs.fda.ctp.web.config.mvc.MvcConfig;
import gov.hhs.fda.ctp.web.config.mvc.WebTestConfig;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	@ContextConfiguration(classes = {WebTestConfig.class}),
	@ContextConfiguration(classes = {MvcConfig.class})
})
public class UserAdminRestControllerIT {

	@Autowired
	UserAdminRestController controller;

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.useradmin.UserAdminRestController#registerUser(gov.hhs.fda.ctp.web.registration.User)}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testRegisterUser() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.useradmin.UserAdminRestController#transformUserDtoToUser(gov.hhs.fda.ctp.biz.registration.UserDTO)}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testTransformUserDtoToUser() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.useradmin.UserAdminRestController#getUsers()}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testGetUsers() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.useradmin.UserAdminRestController#getUser(java.lang.String)}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testGetUserString() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.useradmin.UserAdminRestController#getRoles()}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testGetRoles() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.useradmin.UserAdminRestController#update(gov.hhs.fda.ctp.web.registration.User)}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testUpdate() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	/**
	 * Test method for {@link gov.hhs.fda.ctp.web.useradmin.UserAdminRestController#getUser(long)}.
	 */
	@Test (expected=RuntimeException.class)
	@Ignore("Not Implemented Yet")
	public void testGetUserLong() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

}

#!/bin/bash
cd ..
cd tufa_ui
npm run ng -- build --configuration=test
cd ..
find tufa_web/src/main/webapp/ -maxdepth 1 -type f -delete
if [ -d tufa_web/src/main/webapp/assets ]; then find tufa_web/src/main/webapp/assets -delete; fi
cp -a tufa_ui/dist/. tufa_web/src/main/webapp
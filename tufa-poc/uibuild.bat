@echo off
IF /I [%1] == [tufadev] GOTO DEV
IF /I [%1] == [tufatst] GOTO TEST
IF /I [%1] == [tufauat] GOTO UAT
IF /I [%1] == [tufapreprod] GOTO PREPROD
IF /I [%1] == [tufaprod] GOTO PROD

REM Running default 
ng build --configuration=local-prod
GOTO DONE:

:DEV
REM Running Dev Build 
ng build --configuration=dev
GOTO DONE

:TEST
REM Running Test Build 
ng build --configuration=test
GOTO DONE

:UAT
REM Running Test Build 
ng build --configuration=uat
GOTO DONE

:PREPROD
REM Running Test/PreProd/Prod Build 
ng build --configuration=preprod
GOTO DONE

:PROD
REM Running Test/PreProd/Prod Build 
ng build --configuration=prod
GOTO DONE

:DONE
